<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInsertOsgSp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = "
CREATE DEFINER=`root`@`localhost` PROCEDURE `insert_osg`(
IN `playerId` VARCHAR(100),
IN `playerCurrency` VARCHAR(3),
IN `gameId` INT,
IN `gameCategory` VARCHAR(50),
IN `tranId` BIGINT(20),
IN `tranSubId` BIGINT(20),
IN `totalAmount` DECIMAL(18,4),
IN `betAmount` DECIMAL(18,4),
IN `jackpotContribution` DECIMAL(18,4),
IN `winAmount` DECIMAL(18,4),
IN `gameDate` DATETIME,
IN `platform` VARCHAR(50),
IN `created_at` DATETIME,
IN `updated_at` DATETIME
)
BEGIN

INSERT INTO `bet_osg`
(
`playerId`,
`playerCurrency`,
`gameId`,
`gameCategory`,
`tranId`,
`tranSubId`,
`totalAmount`,
`betAmount`,
`jackpotContribution`,
`winAmount`,
`gameDate`,
`platform`,
`created_at`,
`updated_at`
)
values(
_playerId,
_playerCurrency,
_gameId,
_gameCategory,
_tranId,
_tranSubId,
_totalAmount,
_betAmount,
_jackpotContribution,
_winAmount,
_gameDate,
_platform,
_created_at,
_updated_at
)ON DUPLICATE KEY UPDATE
`playerId` = _playerId,
`playerCurrency` = _playerCurrency,
`gameId` = _gameId,
`gameCategory` = _gameCategory,
`tranId` = _tranId,
`tranSubId` = _tranSubId,
`totalAmount` = _totalAmount,
`betAmount` = _betAmount,
`jackpotContribution` = _jackpotContribution,
`winAmount` = _winAmount,
`gameDate` = _gameDate,
`platform` = _platform,
`updated_at` = _updated_at;

END
";

		DB::unprepared($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP PROCEDURE `insert_osg`');
    }
}
