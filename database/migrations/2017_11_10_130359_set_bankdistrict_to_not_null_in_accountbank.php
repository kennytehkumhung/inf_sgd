<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SetBankdistrictToNotNullInAccountbank extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = "ALTER TABLE `accountbank` CHANGE `bankdistrict` `bankdistrict` CHAR(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;";

		DB::statement($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
