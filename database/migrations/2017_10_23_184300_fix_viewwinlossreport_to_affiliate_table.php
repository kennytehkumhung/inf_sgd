<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FixViewwinlossreportToAffiliateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('affiliate', function (Blueprint $table) {
            $table->unsignedTinyInteger('view_winloss_report')->after('status')->default(1);
        });

        Schema::table('affiliatesetting', function (Blueprint $table) {
            $table->dropColumn(['view_winloss_report']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('affiliate', function (Blueprint $table) {
            //
        });
        Schema::table('affiliatesetting', function (Blueprint $table) {
            //
        });
    }
}
