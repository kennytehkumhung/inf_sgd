<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBetFblTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = "CREATE TABLE `bet_fbl` (
                `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
                `logId` varchar(50) DEFAULT NULL,
                `userId` VARCHAR(50) DEFAULT NULL,
                `accId` BIGINT(20) DEFAULT NULL,
                `username` VARCHAR(32) DEFAULT NULL,
                `datePlaced` DATETIME DEFAULT NULL,
                `gameId` INT DEFAULT NULL,
                `num` VARCHAR(45) DEFAULT NULL,
                `roundId` BIGINT(20) DEFAULT NULL,
                `type` VARCHAR(20) DEFAULT NULL,
                `amount` DECIMAL(18,4) DEFAULT NULL,
                `wl` VARCHAR(10) DEFAULT NULL,
                `wlAmount` DECIMAL(18,4) DEFAULT NULL,
                `created_at` DATETIME DEFAULT NULL,
                `updated_at` DATETIME DEFAULT NULL,
                PRIMARY KEY (`id`),
                UNIQUE KEY `logId_UNIQUE` (`logId`)
              ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;";

		DB::statement($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bet_fbl');
    }
}
