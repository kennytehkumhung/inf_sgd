<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterWftTable0001 extends Migration {

	protected $table = 'bet_wft';

	public function up() {
		Schema::table($this->table, function (Blueprint $table) {
			// Add new columns.
			$table->dateTime('updated_at')->after('isOver');
			$table->dateTime('created_at')->after('updated_at');
		});
	}

	public function down() {
		Schema::table($this->table, function (Blueprint $table) {
			$table->dropColumn(['updated_at', 'created_at']);
		});
	}

}
