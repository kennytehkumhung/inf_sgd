<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIscalculatedToCashledgerTable extends Migration {

	protected $table = 'cashledger';
	
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::table($this->table, function (Blueprint $table) {
			$table->tinyInteger('iscalculated')->default(0)->after('createdpanel');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::table($this->table, function (Blueprint $table) {
			$table->dropColumn(['iscalculated']);
		});
	}

}
