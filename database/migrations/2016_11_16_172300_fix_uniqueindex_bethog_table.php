<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FixUniqueindexBethogTable extends Migration
{
    
    protected $table = 'bet_hog';
    
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table($this->table, function (Blueprint $table) {
            $table->dropUnique('betId_UNIQUE');
            $table->unique('betNo', 'betNo_UNIQUE');
        });

        DB::statement('ALTER TABLE '.$this->table.' MODIFY tableId VARCHAR(32) DEFAULT NULL');
        DB::statement('ALTER TABLE '.$this->table.' MODIFY gameId VARCHAR(32) DEFAULT NULL');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table($this->table, function (Blueprint $table) {
            $table->dropUnique('betNo_UNIQUE');
            $table->unique('betId', 'betId_UNIQUE');
        });
    }
}
