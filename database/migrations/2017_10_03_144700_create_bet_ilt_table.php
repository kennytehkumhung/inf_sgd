<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBetIltTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = "CREATE TABLE `bet_ilt` (
                    `id` bigint(20) NOT NULL AUTO_INCREMENT,
                    `bet_id` MEDIUMINT DEFAULT NULL,
                    `account` VARCHAR(30) DEFAULT NULL,
                    `userid` MEDIUMINT DEFAULT NULL,
                    `username` VARCHAR(30) DEFAULT NULL,
                    `currency` CHAR(3) DEFAULT NULL,
                    `betslipsid` MEDIUMINT DEFAULT NULL,
                    `betref` VARCHAR(30) DEFAULT NULL,
                    `eventtime` DATETIME DEFAULT NULL,
                    `league` VARCHAR(30) DEFAULT NULL,
                    `type` VARCHAR(10) DEFAULT NULL,
                    `teambet` VARCHAR(10) DEFAULT NULL,
                    `stake` DECIMAL(18,4) DEFAULT NULL,
                    `times` DECIMAL(18,4) DEFAULT NULL,
                    `mode` VARCHAR(10) DEFAULT NULL,
                    `odds` DECIMAL(18,4) DEFAULT NULL,
                    `result` TINYINT DEFAULT NULL,
                    `win` DECIMAL(18,4) DEFAULT NULL,
                    `status` TINYINT DEFAULT NULL,
                    `datecreated` DATETIME DEFAULT NULL,
                    `created_at` DATETIME DEFAULT NULL,
                    `updated_at` DATETIME DEFAULT NULL,
                PRIMARY KEY (`id`),
                UNIQUE KEY `betslipsid_UNIQUE` (`betslipsid`)
              ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;";

		DB::statement($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bet_ilt');
    }
}
