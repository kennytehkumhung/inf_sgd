<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FixInsertOsgSp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        DB::unprepared('DROP PROCEDURE `insert_osg`');
        
        $sql = "
CREATE DEFINER=`root`@`localhost` PROCEDURE `insert_osg`(
IN _playerId VARCHAR(100),
IN _playerCurrency VARCHAR(3),
IN _gameId VARCHAR(50),
IN _gameName VARCHAR(50),
IN _gameCategory VARCHAR(50),
IN _tranId BIGINT(20),
IN _tranSubId VARCHAR(50),
IN _totalAmount DECIMAL(18,4),
IN _betAmount DECIMAL(18,4),
IN _jackpotContribution DECIMAL(18,4),
IN _winAmount DECIMAL(18,4),
IN _gameDate DATETIME,
IN _platform VARCHAR(50),
IN _created_at DATETIME,
IN _updated_at DATETIME
)
BEGIN

INSERT INTO `bet_osg`
(
`playerId`,
`playerCurrency`,
`gameId`,
`gameName`,
`gameCategory`,
`tranId`,
`tranSubId`,
`totalAmount`,
`betAmount`,
`jackpotContribution`,
`winAmount`,
`gameDate`,
`platform`,
`created_at`,
`updated_at`
)
values(
_playerId,
_playerCurrency,
_gameId,
_gameName,
_gameCategory,
_tranId,
_tranSubId,
_totalAmount,
_betAmount,
_jackpotContribution,
_winAmount,
_gameDate,
_platform,
_created_at,
_updated_at
)ON DUPLICATE KEY UPDATE
`playerId` = _playerId,
`playerCurrency` = _playerCurrency,
`gameId` = _gameId,
`gameName` = _gameName,
`gameCategory` = _gameCategory,
`tranId` = _tranId,
`tranSubId` = _tranSubId,
`totalAmount` = _totalAmount,
`betAmount` = _betAmount,
`jackpotContribution` = _jackpotContribution,
`winAmount` = _winAmount,
`gameDate` = _gameDate,
`platform` = _platform,
`updated_at` = _updated_at;

END
";

		DB::unprepared($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP PROCEDURE IF EXISTS `insert_osg`');
    }
}
