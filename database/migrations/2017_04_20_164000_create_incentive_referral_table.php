<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIncentiveReferralTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = "CREATE TABLE `incentivereferral` (
                    `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT,
                    `accid` mediumint(8) UNSIGNED NOT NULL,
                    `nickname` varchar(32) NOT NULL,
                    `crccode` char(3) NOT NULL,
                    `crcrate` decimal(9,5) NOT NULL,
                    `referralid` mediumint(8) UNSIGNED NOT NULL,
                    `referralnickname` char(32) NOT NULL,
                    `referralcrccode` char(3) NOT NULL,
                    `referralcrcrate` decimal(9,5) NOT NULL,
                    `prdid` mediumint(8) NOT NULL,
                    `category` tinyint(3) NOT NULL,
                    `accountdate` date NOT NULL,
                    `amount` decimal(18,4) NOT NULL,
                    `amount_local` decimal(18,4) NOT NULL,
                    `created_at` datetime DEFAULT NULL,
                    `updated_at` datetime DEFAULT NULL,
                PRIMARY KEY (`id`)
              ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;";

		DB::statement($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('incentivereferral');
    }
}
