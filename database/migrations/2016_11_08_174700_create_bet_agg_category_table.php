<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBetAggCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = "CREATE TABLE `bet_agg_category` (
                `code` varchar(10) NOT NULL,
                `gameName` varchar(100) NOT NULL,
                `gameNameCn` varchar(100) DEFAULT NULL,
                `category` varchar(45) NOT NULL,
                `imgCode` varchar(20) NOT NULL,
                `isActive` smallint(6) NOT NULL DEFAULT '1',
                `isNew` smallint(6) NOT NULL DEFAULT '0',
                PRIMARY KEY (`code`)
              ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;";

		DB::statement($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bet_agg_category');
    }
}
