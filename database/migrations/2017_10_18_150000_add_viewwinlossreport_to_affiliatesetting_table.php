<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddViewwinlossreportToAffiliatesettingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('affiliatesetting', function (Blueprint $table) {
            $table->unsignedTinyInteger('view_winloss_report')->after('effective')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('affiliatesetting', function (Blueprint $table) {
            //
        });
    }
}
