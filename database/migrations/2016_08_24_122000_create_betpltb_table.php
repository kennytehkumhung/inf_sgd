<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBetpltbTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = "CREATE TABLE `bet_pltb` (
			  `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
			  `PlayerName` VARCHAR(50) DEFAULT NULL,
			  `Username` VARCHAR(50) DEFAULT NULL,
			  `WindowCode` VARCHAR(50) DEFAULT NULL,
			  `GameId` VARCHAR(50) DEFAULT NULL,
			  `GameCode` BIGINT(20) DEFAULT NULL,
			  `GameType` VARCHAR(50) DEFAULT NULL,
			  `GameName` VARCHAR(50) DEFAULT NULL,
			  `SessionId` VARCHAR(50) DEFAULT NULL,
			  `Bet` DECIMAL(16,4) DEFAULT NULL,
			  `Win` DECIMAL(16,4) DEFAULT NULL,
			  `ProgressiveBet` DECIMAL(16,4) DEFAULT NULL,
			  `ProgressiveWin` DECIMAL(16,4) DEFAULT NULL,
			  `Currency` CHAR(3) DEFAULT NULL,
			  `Balance` DECIMAL(16,4) DEFAULT NULL,
			  `CurrentBet` DECIMAL(16,4) DEFAULT NULL,
			  `GameDate` DATETIME DEFAULT NULL,
			  `LiveNetwork` VARCHAR(50) DEFAULT NULL,
			  `RNum` VARCHAR(50) DEFAULT NULL,
			  `created_at` DATETIME NOT NULL,
			  `updated_at` DATETIME NOT NULL,
			  PRIMARY KEY (`id`),
			  UNIQUE KEY `GameCode_UNIQUE` (`GameCode`)
			) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;";

		DB::statement($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bet_pltb');
    }
}
