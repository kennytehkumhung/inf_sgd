<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddModified extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cashbalance', function (Blueprint $table) {
            $table->dateTime('modified')->after('balance');
            $table->dateTime('created')->after('modified');
			
        });
		
		DB::statement('ALTER TABLE cashbalance ADD UNIQUE KEY `unique` (`accid`,`acccode`,`crccode`,`prdid`) USING BTREE');
		
		DB::unprepared('CREATE PROCEDURE `insert_cashbalance` (
							IN _accid MEDIUMINT (8),
							IN _acccode CHAR (32),
							IN _crccode CHAR (3),
							IN _prdid MEDIUMINT (8),
							IN _balance DECIMAL (14, 4),
							IN _modified datetime,
							IN _created datetime
						)
						BEGIN
							INSERT INTO `cashbalance` (
								`accid`,
								`acccode`,
								`crccode`,
								`prdid`,
								`balance`,
								`modified`,
								`created`
							)
						VALUES
							(
								_accid,
								_acccode,
								_crccode,
								_prdid,
								_balance,
								_modified,
								_created
							) ON DUPLICATE KEY UPDATE
						  `balance` = _balance,
							`modified` = _modified;


						END');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cashbalance', function (Blueprint $table) {
            //
        });
    }
}
