<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBetMntTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = "CREATE TABLE `bet_mnt` (
                    `id` bigint(20) NOT NULL AUTO_INCREMENT,
                    `bet_date` DATE DEFAULT NULL,
                    `player_acc` VARCHAR(30) DEFAULT NULL,
                    `accid` MEDIUMINT NULL,
                    `nickname` VARCHAR(30) DEFAULT NULL,
                    `currency` CHAR(3) DEFAULT NULL,
                    `total_income` DECIMAL(18,4) DEFAULT NULL,
                    `total_outlay` DECIMAL(18,4) DEFAULT NULL,
                    `wash_count_sum` DECIMAL(18,4) DEFAULT NULL,
                    `created_at` DATETIME DEFAULT NULL,
                    `updated_at` DATETIME DEFAULT NULL,
                PRIMARY KEY (`id`),
                UNIQUE KEY `bet_id_UNIQUE` (`bet_date`, `player_acc`)
              ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;";

		DB::statement($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bet_mnt');
    }
}
