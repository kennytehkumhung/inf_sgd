<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddInstanttransflagToCashledgerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cashledger', function (Blueprint $table) {
            $table->tinyInteger('instanttransflag')->default(0)->after('incentiveflag');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cashledger', function (Blueprint $table) {
            //
        });
    }
}
