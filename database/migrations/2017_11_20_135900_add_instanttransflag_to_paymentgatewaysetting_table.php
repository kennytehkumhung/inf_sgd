<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddInstanttransflagToPaymentgatewaysettingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('paymentgatewaysetting', function (Blueprint $table) {
            $table->tinyInteger('instant_trans_flag')->default(0)->after('status');
            $table->dropColumn(['notify_instant_trans']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('paymentgatewaysetting', function (Blueprint $table) {
            //
        });
    }
}
