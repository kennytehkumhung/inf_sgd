<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentgatewaysettingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = "CREATE TABLE `paymentgatewaysetting` (
                    `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT,
                    `code` char(3) NOT NULL,
                    `name` varchar(128) NOT NULL,
                    `url` varchar(128) NOT NULL,
                    `status` tinyint(3) NOT NULL,
                    `withdraw` tinyint(3) NOT NULL,
                    `created_at` datetime DEFAULT NULL,
                    `updated_at` datetime DEFAULT NULL,
                PRIMARY KEY (`id`)
              ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;";

		DB::statement($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
//        Schema::drop('paymentgatewaysetting');
    }
}
