<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePremierleaguematchTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = "CREATE TABLE `premier_league_match` (
                    `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT,
                    `team_a_id` mediumint(8) UNSIGNED NOT NULL,
                    `team_b_id` mediumint(8) UNSIGNED NOT NULL,
                    `team_a_name` varchar(32) NOT NULL,
                    `team_b_name` varchar(32) NOT NULL,
                    `team_a_score` tinyint(3) UNSIGNED NOT NULL,
                    `team_b_score` tinyint(3) UNSIGNED NOT NULL,
                    `status` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
                    `week_no` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
                    `media_thumbnail_id` mediumint(8) UNSIGNED NOT NULL,
                    `media_desktop_id` mediumint(8) UNSIGNED NOT NULL,
                    `bet_opened_at` datetime DEFAULT NULL,
                    `bet_closed_at` datetime DEFAULT NULL,
                    `created_at` datetime DEFAULT NULL,
                    `updated_at` datetime DEFAULT NULL,
                PRIMARY KEY (`id`)
              ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;";

		DB::statement($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
//        Schema::drop('premier_league_match');
    }
}
