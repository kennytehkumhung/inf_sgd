<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FixInsertHogSp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared('DROP PROCEDURE `insert_hog`');
        
        $sql = "
CREATE DEFINER=`root`@`localhost` PROCEDURE `insert_hog`(
IN _refId BIGINT,
IN _betStartDate DATETIME,
IN _betEndDate DATETIME,
IN _accountId VARCHAR(100),
IN _tableId VARCHAR(20),
IN _gameId VARCHAR(25),
IN _betId VARCHAR(35),
IN _betAmount DECIMAL(18,4),
IN _payout DECIMAL(18,4),
IN _currency VARCHAR(3),
IN _gameType VARCHAR(100),
IN _betSpot VARCHAR(80),
IN _betNo VARCHAR(32),
IN _created_at DATETIME,
IN _updated_at DATETIME
)
BEGIN

INSERT INTO `bet_hog`
(
`refId`,
`betStartDate`,
`betEndDate`,
`accountId`,
`tableId`,
`gameId`,
`betId`,
`betAmount`,
`payout`,
`currency`,
`gameType`,
`betSpot`,
`betNo`,
`created_at`,
`updated_at`
)
values(
_refId,
_betStartDate,
_betEndDate,
_accountId,
_tableId,
_gameId,
_betId,
_betAmount,
_payout,
_currency,
_gameType,
_betSpot,
_betNo,
_created_at,
_updated_at
)ON DUPLICATE KEY UPDATE
`betStartDate` = _betStartDate,
`betEndDate` = _betEndDate,
`accountId` = _accountId,
`tableId` = _tableId,
`gameId` = _gameId,
`betAmount` = _betAmount,
`payout` = _payout,
`currency` = _currency,
`gameType` = _gameType,
`betSpot` = _betSpot,
`betNo` = _betNo,
`updated_at` = _updated_at;

END
";

		DB::unprepared($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP PROCEDURE `insert_hog`');
    }
}
