<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FixInsertFblSp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared('DROP PROCEDURE `insert_fbl`');
        
        $sql = "
CREATE DEFINER=`root`@`localhost` PROCEDURE `insert_fbl`(
IN _logId varchar(50),
IN _userID VARCHAR(50),
IN _accId BIGINT(20),
IN _username VARCHAR(32),
IN _datePlaced DATETIME,
IN _gameId INT,
IN _num VARCHAR(45),
IN _roundId BIGINT(20),
IN _type VARCHAR(20),
IN _amount DECIMAL(18,4),
IN _wl VARCHAR(10),
IN _wlAmount DECIMAL(18,4),
IN _currency CHAR(3),
IN _created_at DATETIME,
IN _updated_at DATETIME
)
BEGIN

INSERT INTO `bet_fbl`
(
`logId`,
`userID`,
`accId`,
`username`,
`datePlaced`,
`gameId`,
`num`,
`roundId`,
`type`,
`amount`,
`wl`,
`wlAmount`,
`currency`,
`created_at`,
`updated_at`
)
values(
_logId,
_userID,
_accId,
_username,
_datePlaced,
_gameId,
_num,
_roundId,
_type,
_amount,
_wl,
_wlAmount,
_currency,
_created_at,
_updated_at
)ON DUPLICATE KEY UPDATE
`userID` = _userID,
`accId` = _accId,
`username` = _username,
`datePlaced` = _datePlaced,
`gameId` = _gameId,
`num` = _num,
`roundId` = _roundId,
`type` = _type,
`amount` = _amount,
`wl` = _wl,
`wlAmount` = _wlAmount,
`currency` = _currency,
`updated_at` = _updated_at;

END
";

		DB::unprepared($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP PROCEDURE `insert_fbl`');
    }
}
