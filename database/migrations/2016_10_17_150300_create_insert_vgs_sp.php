<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInsertVgsSp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = "
CREATE DEFINER=`root`@`localhost` PROCEDURE `insert_vgs`(
IN _refid BIGINT(20),
IN _username VARCHAR(45),
IN _currency CHAR(3),
IN _gamename VARCHAR(45),
IN _gamerefid VARCHAR(45),
IN _gametableid VARCHAR(45),
IN _betdatetime DATETIME,
IN _betamt DECIMAL(18,4),
IN _wl VARCHAR(45),
IN _wlamt DECIMAL(18,4),
IN _created_at DATETIME,
IN _updated_at DATETIME
)
BEGIN

INSERT INTO `bet_vgs`
(
`refid`,
`username`,
`currency`,
`gamename`,
`gamerefid`,
`gametableid`,
`betdatetime`,
`betamt`,
`wl`,
`wlamt`,
`created_at`,
`updated_at`
)
values(
_refid,
_username,
_currency,
_gamename,
_gamerefid,
_gametableid,
_betdatetime,
_betamt,
_wl,
_wlamt,
_created_at,
_updated_at
)ON DUPLICATE KEY UPDATE
`refid` = _refid,
`username` = _username,
`currency` = _currency,
`gamename` = _gamename,
`gametableid` = _gametableid,
`betdatetime` = _betdatetime,
`betamt` = _betamt,
`wl` = _wl,
`wlamt` = _wlamt,
`updated_at` = _updated_at;

END
";

		DB::unprepared($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP PROCEDURE `insert_vgs`');
    }
}
