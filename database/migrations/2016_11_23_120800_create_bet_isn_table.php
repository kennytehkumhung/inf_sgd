<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBetIsnTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = "CREATE TABLE `bet_isn` (
                    `id` bigint(20) NOT NULL AUTO_INCREMENT,
                    `ticketId` bigint(20) DEFAULT NULL,
                    `acctId` varchar(50) DEFAULT NULL,
                    `betDate` datetime DEFAULT NULL,
                    `drawId` varchar(10) DEFAULT NULL,
                    `betType` varchar(10) DEFAULT NULL,
                    `currency` char(3) DEFAULT NULL,
                    `cancelled` tinyint(1) DEFAULT NULL,
                    `msg` text DEFAULT NULL,
                    `msgFrom` varchar(50) DEFAULT NULL,
                    `betIp` varchar(20) DEFAULT NULL,
                    `odds` decimal(18,4) DEFAULT NULL,
                    `betAmount` decimal(18,4) DEFAULT NULL,
                    `successAmount` decimal(18,4) DEFAULT NULL,
                    `payAmount` decimal(18,4) DEFAULT NULL,
                    `wlAmount` decimal(18,4) DEFAULT NULL,
                    `win` tinyint(1) DEFAULT NULL,
                    `processDate` datetime DEFAULT NULL,
                    `created_at` datetime DEFAULT NULL,
                    `updated_at` datetime DEFAULT NULL,
                PRIMARY KEY (`id`),
                UNIQUE KEY `ticketId_UNIQUE` (`ticketId`)
              ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;";

		DB::statement($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bet_isn');
    }
}
