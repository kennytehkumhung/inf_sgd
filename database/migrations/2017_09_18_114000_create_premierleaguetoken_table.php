<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePremierleaguetokenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = "CREATE TABLE `premier_league_token` (
                    `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT,
                    `match_id` mediumint(8) UNSIGNED NOT NULL,
                    `accid` mediumint(8) UNSIGNED NOT NULL,
                    `crccode` char(3) NOT NULL,
                    `nickname` varchar(32) NOT NULL,
                    `token_amount` tinyint(8) NOT NULL,
                    `remark` varchar(100) NOT NULL,
                    `created_at` datetime DEFAULT NULL,
                    `updated_at` datetime DEFAULT NULL,
                PRIMARY KEY (`id`)
              ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;";

		DB::statement($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
//        Schema::drop('premier_league_token');
    }
}
