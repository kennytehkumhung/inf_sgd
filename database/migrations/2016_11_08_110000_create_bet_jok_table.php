<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBetJokTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = "CREATE TABLE `bet_jok` (
                    `id` bigint(20) NOT NULL AUTO_INCREMENT,
                    `refid` bigint(20) DEFAULT NULL,
                    `posttype` varchar(10) DEFAULT NULL,
                    `ocode` varchar(32) DEFAULT NULL,
                    `username` varchar(32) DEFAULT NULL,
                    `currency` char(3) DEFAULT NULL,
                    `gamecode` varchar(32) DEFAULT NULL,
                    `description` varchar(32) DEFAULT NULL,
                    `type` varchar(32) DEFAULT NULL,
                    `amount` decimal(18,4) DEFAULT NULL,
                    `result` decimal(18,4) DEFAULT NULL,
                    `time` datetime DEFAULT NULL,
                    `details` text DEFAULT NULL,
                    `appid` varchar(10) DEFAULT NULL,
                    `created_at` datetime DEFAULT NULL,
                    `updated_at` datetime DEFAULT NULL,
                PRIMARY KEY (`id`),
                UNIQUE KEY `ocode_UNIQUE` (`ocode`)
              ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;";

		DB::statement($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bet_jok');
    }
}
