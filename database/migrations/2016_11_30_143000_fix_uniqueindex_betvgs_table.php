<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FixUniqueindexBetvgsTable extends Migration
{
    
    protected $table = 'bet_vgs';
    
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table($this->table, function (Blueprint $table) {
            $table->dropUnique('gamerefid_UNIQUE');
            $table->unique('ticketid', 'ticketid_UNIQUE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table($this->table, function (Blueprint $table) {
            $table->dropUnique('ticketid_UNIQUE');
            $table->unique('gamerefid', 'gamerefid_UNIQUE');
        });
    }
}
