<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSmslogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = "CREATE TABLE `smslog` (
                    `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT,
                    `sender_accid` mediumint(8) UNSIGNED DEFAULT 0,
                    `sender_acccode` varchar(32) DEFAULT NULL,
                    `sender_ip` char(16) DEFAULT NULL,
                    `provider` varchar(10) NOT NULL,
                    `method` varchar(15) DEFAULT NULL,
                    `tel_mobile` char(16) NOT NULL,
                    `request` text DEFAULT NULL,
                    `response` text DEFAULT NULL,
                    `ref_code` char(6) DEFAULT NULL,
                    `ref_status` tinyint(3) UNSIGNED DEFAULT 0,
                    `created_at` datetime DEFAULT NULL,
                    `updated_at` datetime DEFAULT NULL,
                PRIMARY KEY (`id`)
              ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;";

		DB::statement($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('smslog');
    }
}
