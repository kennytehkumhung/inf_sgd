<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLimit extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bankaccount', function (Blueprint $table) {
			$table->mediumInteger('limit')->after('bhdid');
			$table->mediumInteger('tobnkid')->after('limit');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bankaccount', function (Blueprint $table) {
            //
        });
    }
}
