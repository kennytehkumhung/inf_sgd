<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInsertUc8Sp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = "
CREATE DEFINER=`root`@`localhost` PROCEDURE `insert_uc8`(
IN _bet_id MEDIUMINT,
IN _username VARCHAR(30),
IN _currency CHAR(3),
IN _game_type TINYINT,
IN _game_code VARCHAR(100),
IN _game_name VARCHAR(100),
IN _bet DECIMAL(18,4),
IN _win DECIMAL(18,4),
IN _report_date DATE,
IN _game_time DATETIME,
IN _status INT,
IN _created_at DATETIME,
IN _updated_at DATETIME
)
BEGIN

INSERT INTO `bet_uc8`
(
`bet_id`,
`username`,
`currency`,
`game_type`,
`game_code`,
`game_name`,
`bet`,
`win`,
`report_date`,
`game_time`,
`status`,
`created_at`,
`updated_at`
)
values(
_bet_id,
_username,
_currency,
_game_type,
_game_code,
_game_name,
_bet,
_win,
_report_date,
_game_time,
_status,
_created_at,
_updated_at
)ON DUPLICATE KEY UPDATE
`bet_id` = _bet_id,
`username` = _username,
`currency` = _currency,
`game_type` = _game_type,
`game_code` = _game_code,
`game_name` = _game_name,
`bet` = _bet,
`win` = _win,
`report_date` = _report_date,
`game_time` = _game_time,
`status` = _status,
`updated_at` = _updated_at;

END
";

		DB::unprepared($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP PROCEDURE `insert_uc8`');
    }
}
