<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTypeToPaymentgatewaysettingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('paymentgatewaysetting', function (Blueprint $table) {
            $table->tinyInteger('is_private')->default(0)->after('instant_trans_flag');
            $table->char('private_type', 5)->after('is_private');
            $table->text('private_remark')->after('private_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('paymentgatewaysetting', function (Blueprint $table) {
            //
        });
    }
}
