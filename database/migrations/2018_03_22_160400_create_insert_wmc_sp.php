<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInsertWmcSp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
	   `id` bigint(20) NOT NULL AUTO_INCREMENT,
					`user` varchar(200) DEFAULT NULL, 
                    `betId` int DEFAULT NULL,
					`betTime` DATETIME DEFAULT NULL,
					`bet` DECIMAL(18,4) DEFAULT NULL,
					`validbet` DECIMAL(18,4) DEFAULT NULL,
					`water` DECIMAL(18,4) DEFAULT NULL,
                    `result` DECIMAL(18,4) DEFAULT NULL,
                    `betResult` varchar(20) DEFAULT NULL,
					`waterbet` DECIMAL(18,4) DEFAULT NULL,
					`winLoss` varchar(20) DEFAULT NULL,
					`ip` varchar(20) DEFAULT NULL, 
					`gid` int DEFAULT NULL,
					`event` int DEFAULT NULL,
					`eventChild` int DEFAULT NULL,
					`tableId` int DEFAULT NULL,
					`gameResult` int DEFAULT NULL,
					`gname` varchar(20) DEFAULT NULL,
					`created_at` DATETIME DEFAULT NULL,
                    `updated_at` DATETIME DEFAULT NULL,
     */
    public function up()
    {
        $sql = "
CREATE DEFINER=`root`@`127.0.0.1` PROCEDURE `insert_wmc`(
IN _user VARCHAR(200),
IN _betId DECIMAL(18,4),
IN _betTime DATETIME,
IN _bet DECIMAL(18,4),
IN _validbet DECIMAL(18,4), 
IN _water DECIMAL(18,4),
IN _result DECIMAL(18,4), 
IN _betResult varchar(20),
IN _waterbet DECIMAL(18,4),
IN _winLoss varchar(20),
IN _ip varchar(20),
IN _gid int,
IN _event int,
IN _eventChild int,
IN _tableId int,
IN _gameResult int,
IN _gname varchar(20),
IN _created_at DATETIME, 
IN _updated_at DATETIME
)
BEGIN

INSERT INTO `bet_wmc`
(
  
					`user`,
                    `betId`,
					`betTime`,
					`bet`,
					`validbet`,
					`water`,
                    `result`,
                    `betResult`,
					`waterbet`,
					`winLoss`,
					`ip`, 
					`gid`,
					`event`,
					`eventChild`,
					`tableId`,
					`gameResult`,
					`gname`,
					`created_at`,
                    `updated_at`
)
values(
_user,
_betId,
_betTime,
_bet,
_validbet, 
_water,
_result, 
_betResult,
_waterbet,
_winLoss,
_ip,
_gid,
_event,
_eventChild,
_tableId,
_gameResult,
_gname,
_created_at, 
_updated_at 
)ON DUPLICATE KEY UPDATE

					`user`=_user,
                    `betId`=_betId,
					`betTime`=_betTime,
					`bet`=_bet,
					`validbet`=_validbet,
					`water`=_water,
                    `result`=_result,
                    `betResult`=_betResult,
					`waterbet`=_waterbet,
					`winLoss`=_winLoss,
					`ip`=_ip, 
					`gid`=_gid,
					`event`=_event,
					`eventChild`=_eventChild,
					`tableId`=_tableId,
					`gameResult`=_gameResult,
					`gname`=_gname,
					`updated_at`=_updated_at;

END
";

		DB::unprepared($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP PROCEDURE `insert_wmc`');
    }
}
