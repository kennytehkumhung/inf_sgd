<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBetPltbCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = "CREATE TABLE `bet_pltb_category` (
                `code` varchar(45) NOT NULL,
                `gameName` varchar(100) NOT NULL,
                `category` varchar(45) NOT NULL,
                `jpCode` varchar(45) DEFAULT NULL,
                `isActive` smallint(6) NOT NULL DEFAULT '1',
                `isBranded` smallint(6) NOT NULL DEFAULT '0',
                `isNew` smallint(6) NOT NULL DEFAULT '0',
                PRIMARY KEY (`code`)
              ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;";

		DB::statement($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bet_pltb_category');
    }
}
