<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBetMigCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = "CREATE TABLE `bet_mig_category` (
                `gameCode` varchar(50) NOT NULL,
                `gameName` varchar(50) NOT NULL,
                `gameNameCN` varchar(50) NOT NULL,
                `category` varchar(45) DEFAULT NULL,
                `isHTML5` smallint(1) NOT NULL,
                `isNew` smallint(1) NOT NULL,
				`isFlash` smallint(1) NOT NULL,
				`isViper` smallint(1) NOT NULL,
                PRIMARY KEY (`gameCode`)
              ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;";

		DB::statement($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bet_mig_category');
    }
}
