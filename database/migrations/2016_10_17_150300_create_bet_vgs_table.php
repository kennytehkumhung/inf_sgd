<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBetVgsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = "CREATE TABLE `bet_vgs` (
                    `id` bigint(20) NOT NULL AUTO_INCREMENT,
                    `refid` bigint(20) NOT NULL,
                    `username` varchar(45) NOT NULL,
                    `currency` char(3) NOT NULL,
                    `gamename` varchar(45) NOT NULL,
                    `gamerefid` varchar(45) NOT NULL,
                    `gametableid` varchar(45) NOT NULL,
                    `betdatetime` datetime NOT NULL,
                    `betamt` decimal(18,4) NOT NULL,
                    `wl` varchar(45) NOT NULL,
                    `wlamt` decimal(18,4) NOT NULL,
                    `created_at` datetime DEFAULT NULL,
                    `updated_at` datetime DEFAULT NULL,
                PRIMARY KEY (`id`),
                UNIQUE KEY `gamerefid_UNIQUE` (`gamerefid`)
              ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;";

		DB::statement($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bet_vgs');
    }
}
