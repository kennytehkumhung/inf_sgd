<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTbsTable0001 extends Migration {

	protected $table = 'bet_tbs';

	public function up() {
		// Rename columns.
		DB::statement('ALTER TABLE '.$this->table.' CHANGE BetOdd BetOdds DECIMAL(16, 4);');
		DB::statement('ALTER TABLE '.$this->table.' CHANGE OddStyle OddsStyle VARCHAR(45);');
		
		Schema::table($this->table, function (Blueprint $table) {
			// Add new columns.
			$table->integer('GameID')->nullable()->after('MatchID');
			$table->integer('SubGameID')->nullable()->after('GameID');
			$table->decimal('BackAmount', 16, 4)->nullable()->after('SubGameID');
			$table->decimal('DeductAmount', 16, 4)->nullable()->after('BackAmount');
			$table->decimal('AllWin', 16, 4)->nullable()->after('DeductAmount');
			$table->decimal('Turnover', 16, 4)->nullable()->after('AllWin');
			$table->dateTime('BetUpdateTime')->nullable()->after('Turnover');
			$table->text('BetInfo')->nullable()->after('BetUpdateTime');
			$table->integer('BetSubID')->nullable()->after('BetInfo');
			$table->integer('SportID')->nullable()->after('BetSubID');
			$table->integer('Stage')->nullable()->after('SportID');
			$table->integer('MarketID')->nullable()->after('Stage');
			$table->decimal('Odds', 16, 4)->nullable()->after('MarketID');
			$table->integer('HomeScore')->nullable()->after('Odds');
			$table->integer('AwayScore')->nullable()->after('HomeScore');
			$table->integer('HomeCard')->nullable()->after('AwayScore');
			$table->integer('AwayCard')->nullable()->after('HomeCard');
			$table->decimal('OutLevel', 16, 4)->nullable()->after('AwayCard');
		});
	}

	public function down() {
		Schema::table($this->table, function (Blueprint $table) {
			DB::statement('ALTER TABLE '.$this->table.' CHANGE BetOdds BetOdd DECIMAL(16, 4);');
			DB::statement('ALTER TABLE '.$this->table.' CHANGE OddsStyle OddStyle VARCHAR(45);');

			$table->dropColumn([
			    'GameID', 'SubGameID', 'BackAmount', 'DeductAmount', 'AllWin',
			    'Turnover', 'BetUpdateTime', 'BetInfo', 'SportID', 'Stage',
			    'MarketID', 'Odds', 'HomeScore', 'AwayScore', 'HomeCard',
			    'AwayCard', 'OutLevel', 'BetSubID'
			    ]);
		});
	}

}
