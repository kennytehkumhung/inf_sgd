<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FixToallinboxTableCreatedOnUpdateCURRENTTIMESTAMP extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = "ALTER TABLE `toallinbox`
				CHANGE `created` `created` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP;
				";

		DB::statement($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
