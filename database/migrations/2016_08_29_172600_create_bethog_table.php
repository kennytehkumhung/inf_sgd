<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBethogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = "CREATE TABLE `bet_hog` (
			  `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
			  `refId` BIGINT(20) DEFAULT NULL,
			  `betStartDate` DATETIME DEFAULT NULL,
			  `betEndDate` DATETIME DEFAULT NULL,
			  `accountId` VARCHAR(100) DEFAULT NULL,
			  `tableId` VARCHAR(16) DEFAULT NULL,
			  `gameId` VARCHAR(24) DEFAULT NULL,
			  `betId` VARCHAR(32) DEFAULT NULL,
			  `betAmount` DECIMAL(18,4) DEFAULT NULL,
			  `payout` DECIMAL(18,4) DEFAULT NULL,
			  `currency` VARCHAR(3) DEFAULT NULL,
			  `gameType` VARCHAR(100) DEFAULT NULL,
			  `betSpot` VARCHAR(80) DEFAULT NULL,
			  `betNo` VARCHAR(32) DEFAULT NULL,
			  `created_at` DATETIME NOT NULL,
			  `updated_at` DATETIME NOT NULL,
			  PRIMARY KEY (`id`),
			  UNIQUE KEY `refId_UNIQUE` (`refId`),
			  UNIQUE KEY `betId_UNIQUE` (`betId`)
			) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;";

		DB::statement($sql);
        
        // Seed config.
        $count = DB::table('config')->where('param', '=', 'SYSTEM_HOG_PULLDATE')->count();
        
        if ($count < 1) {
            DB::table('config')->insert(array(
                'groupid' => 1,
                'name' => 'SYSTEM_HOG_PULLDATE',
                'desc' => 'SYSTEM_HOG_PULLDATE',
                'param' => 'SYSTEM_HOG_PULLDATE',
                'type' => 1,
                'value' => '2016-09-19 00:00:00',
                'createdby' => 1,
                'modifiedby' => 1,
                'status' => 1,
                'created' => DB::raw('NOW()'),
                'modified' => DB::raw('NOW()'),
            ));
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bet_hog');
    }
}
