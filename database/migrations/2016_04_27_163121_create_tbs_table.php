<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = 'CREATE TABLE `bet_tbs` (
					  `BetID` bigint(20) NOT NULL,
					  `Account` varchar(45) NOT NULL,
					  `AccountID` bigint(20) NOT NULL,
					  `BetAmount` decimal(16,4) DEFAULT NULL,
					  `BetOdd` decimal(16,4) DEFAULT NULL,
					  `Win` decimal(16,4) DEFAULT NULL,
					  `Comm` decimal(16,4) DEFAULT NULL,
					  `BetType` varchar(45) DEFAULT NULL,
					  `OddStyle` varchar(45) DEFAULT NULL,
					  `Hdp` varchar(45) DEFAULT NULL,
					  `BetPos` varchar(45) DEFAULT NULL,
					  `Live` varchar(45) DEFAULT NULL,
					  `BetDate` datetime DEFAULT NULL,
					  `Status` varchar(45) DEFAULT NULL,
					  `Result` varchar(45) DEFAULT NULL,
					  `ReportDate` datetime DEFAULT NULL,
					  `SportName` varchar(45) DEFAULT NULL,
					  `LeagueID` varchar(45) DEFAULT NULL,
					  `HomeID` varchar(45) DEFAULT NULL,
					  `AwayID` varchar(45) DEFAULT NULL,
					  `BetScore` varchar(45) DEFAULT NULL,
					  `MatchDate` datetime DEFAULT NULL,
					  `BetIP` varchar(45) DEFAULT NULL,
					  `MatchID` varchar(45) DEFAULT NULL,
					  `updated_at` datetime NOT NULL,
					  `created_at` datetime NOT NULL,
					  PRIMARY KEY (`BetID`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8;';
				
		DB::statement($sql);	
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
