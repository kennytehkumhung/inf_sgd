<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPrdidToIncentive extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('incentivesetting', function (Blueprint $table) {
            $table->tinyInteger('prdid')->default(0)->after('product');
        });

        Schema::table('incentive', function (Blueprint $table) {
            $table->tinyInteger('prdid')->default(0)->after('product');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('incentivesetting', function (Blueprint $table) {
            $table->dropColumn(['prdid']);
        });

        Schema::table('incentive', function (Blueprint $table) {
            $table->dropColumn(['prdid']);
        });
    }
}
