<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePremierleaguebetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = "CREATE TABLE `premier_league_bet` (
                    `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT,
                    `match_id` mediumint(8) UNSIGNED NOT NULL,
                    `accid` mediumint(8) UNSIGNED NOT NULL,
                    `crccode` char(3) NOT NULL,
                    `nickname` varchar(32) NOT NULL,
                    `bet_a` tinyint(4) UNSIGNED NOT NULL DEFAULT 0,
                    `bet_b` tinyint(4) UNSIGNED NOT NULL DEFAULT 0,
                    `status` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
                    `created_at` datetime DEFAULT NULL,
                    `updated_at` datetime DEFAULT NULL,
                PRIMARY KEY (`id`)
              ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;";

		DB::statement($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
//        Schema::drop('premier_league_bet');
    }
}
