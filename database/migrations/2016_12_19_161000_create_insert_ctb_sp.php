<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInsertCtbSp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = "
CREATE DEFINER=`root`@`localhost` PROCEDURE `insert_ctb`(
IN _refID bigint(20),
IN _userID varchar(45),
IN _raceID int(11),
IN _raceType varchar(45),
IN _raceDate datetime,
IN _raceNo int(11),
IN _runnerNo int(11),
IN _currencyName varchar(5),
IN _status varchar(10),
IN _limit varchar(45),
IN _payout decimal(18,4),
IN _transactionDate datetime,
IN _updateDate datetime,
IN _place decimal(18,4),
IN _win decimal(18,4),
IN _created_at datetime,
IN _updated_at datetime
)
BEGIN

INSERT INTO `bet_ctb`
(
`refID`,
`userID`,
`raceID`,
`raceType`,
`raceDate`,
`raceNo`,
`runnerNo`,
`currencyName`,
`status`,
`limit`,
`payout`,
`transactionDate`,
`updateDate`,
`place`,
`win`,
`created_at`,
`updated_at`
)
values(
_refID,
_userID,
_raceID,
_raceType,
_raceDate,
_raceNo,
_runnerNo,
_currencyName,
_status,
_limit,
_payout,
_transactionDate,
_updateDate,
_place,
_win,
_created_at,
_updated_at
)ON DUPLICATE KEY UPDATE
`userID` = _userID,
`raceID` = _raceID,
`raceType` = _raceType,
`raceDate` = _raceDate,
`raceNo` = _raceNo,
`runnerNo` = _runnerNo,
`currencyName` = _currencyName,
`status` = _status,
`limit` = _limit,
`payout` = _payout,
`transactionDate` = _transactionDate,
`updateDate` = _updateDate,
`place` = _place,
`win` = _win,
`updated_at` = _updated_at;

END
";

		DB::unprepared($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP PROCEDURE `insert_ctb`');
    }
}
