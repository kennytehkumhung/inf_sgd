<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInsertJokSp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = "
CREATE DEFINER=`root`@`localhost` PROCEDURE `insert_jok`(
IN _refid BIGINT(20),
IN _posttype VARCHAR(10),
IN _ocode VARCHAR(32),
IN _username VARCHAR(32),
IN _currency CHAR(3),
IN _gamecode VARCHAR(32),
IN _description VARCHAR(32),
IN _type VARCHAR(32),
IN _amount DECIMAL(18,4),
IN _result DECIMAL(18,4),
IN _time DATETIME,
IN _details TEXT,
IN _appid VARCHAR(10),
IN _created_at DATETIME,
IN _updated_at DATETIME
)
BEGIN

INSERT INTO `bet_jok`
(
`refid`,
`posttype`,
`ocode`,
`username`,
`currency`,
`gamecode`,
`description`,
`type`,
`amount`,
`result`,
`time`,
`details`,
`appid`,
`created_at`,
`updated_at`
)
values(
_refid,
_posttype,
_ocode,
_username,
_currency,
_gamecode,
_description,
_type,
_amount,
_result,
_time,
_details,
_appid,
_created_at,
_updated_at
)ON DUPLICATE KEY UPDATE
`posttype` = _posttype,
`username` = _username,
`currency` = _currency,
`gamecode` = _gamecode,
`description` = _description,
`type` = _type,
`amount` = _amount,
`result` = _result,
`time` = _time,
`details` = _details,
`appid` = _appid,
`updated_at` = _updated_at;

END
";

		DB::unprepared($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP PROCEDURE `insert_jok`');
    }
}
