<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBetBbnTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = "CREATE TABLE `bet_bbn` (
                    `id` bigint(20) NOT NULL AUTO_INCREMENT,
                    `AccName` VARCHAR(50) DEFAULT NULL,
                    `UserName` VARCHAR(50) DEFAULT NULL,
                    `WagersID` VARCHAR(200) DEFAULT NULL,
					`WagerDate` DATETIME DEFAULT NULL,
					`SerialID` VARCHAR(200) DEFAULT NULL,
					`RoundNo` MEDIUMINT DEFAULT NULL,
					`GameType` MEDIUMINT DEFAULT NULL,
					`WagerDetail` VARCHAR(50) DEFAULT NULL,
					`GameCode` TINYINT DEFAULT NULL,
					`Result` VARCHAR(20) DEFAULT NULL,
					`ResultType` VARCHAR(10) DEFAULT NULL,
					`Card` VARCHAR(50) DEFAULT NULL,
					`BetAmount` MEDIUMINT DEFAULT NULL,
					`Origin` VARCHAR(5) DEFAULT NULL,
					`Payoff` DECIMAL(18,4) DEFAULT NULL,
					`Currency` CHAR(3) DEFAULT NULL,
					`ExchangeRate` DECIMAL(18,4) DEFAULT NULL,
					`Commissionable` TINYINT DEFAULT NULL,
                    `created_at` DATETIME DEFAULT NULL,
                    `updated_at` DATETIME DEFAULT NULL,
                PRIMARY KEY (`id`),
                UNIQUE KEY `SerialID_UNIQUE` (`SerialID`)
              ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;";

		DB::statement($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bet_bbn');
    }
}
