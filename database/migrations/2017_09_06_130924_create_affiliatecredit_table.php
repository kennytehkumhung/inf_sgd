<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAffiliatecreditTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = "CREATE TABLE `affiliatecredit` (
				  `id` 	mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
				  `userid` mediumint(8) unsigned NOT NULL,
				  `username` varchar(32) NOT NULL,
				  `crccode` char(3) NOT NULL,
				  `amount` decimal(14,4) NOT NULL,
				  `date`   date NOT NULL,
				  `createdby` mediumint(8) unsigned NOT NULL,
				  `modifiedby` mediumint(8) unsigned NOT NULL,
				  `created` datetime NOT NULL,
				  `modified` datetime NOT NULL,
				  PRIMARY KEY (`id`)
				) ENGINE=MyISAM DEFAULT CHARSET=utf8;";

		DB::statement($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
