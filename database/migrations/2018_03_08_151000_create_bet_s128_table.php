<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBetS128Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = "CREATE TABLE `bet_s128` (
                    `id` bigint(20) NOT NULL AUTO_INCREMENT,
                    `ticket_id` int(20) DEFAULT NULL,
                    `login_id` varchar(30) DEFAULT NULL,
                    `arena_code` varchar(10) DEFAULT NULL,
                    `arena_name_cn` varchar(10) DEFAULT NULL,
                    `match_no` varchar(10) DEFAULT NULL,
                    `match_type` varchar(20) DEFAULT NULL,
                    `match_date` date DEFAULT NULL,
                    `fight_no` int(20) DEFAULT NULL,
                    `fight_datetime` datetime DEFAULT NULL,
                    `meron_cock` varchar(20) DEFAULT NULL,
                    `meron_cock_cn` varchar(10) DEFAULT NULL,
                    `wala_cock` varchar(20) DEFAULT NULL,
                    `wala_cock_cn` varchar(10) DEFAULT NULL,
                    `bet_on` varchar(10) DEFAULT NULL,
                    `odds_type` varchar(10) DEFAULT NULL,
                    `odds_asked` decimal(5,3) DEFAULT NULL,
                    `odds_given` decimal(5,3) DEFAULT NULL,
                    `stake` int(20) DEFAULT NULL,
                    `stake_money` decimal(14,4) DEFAULT NULL,
                    `balance_open` decimal(14,4) DEFAULT NULL,
                    `balance_close` decimal(14,4) DEFAULT NULL,
                    `created_datetime` datetime DEFAULT NULL,
                    `fight_result` varchar(10) DEFAULT NULL,
                    `status` varchar(10) DEFAULT NULL,
                    `winloss` decimal(14,4) DEFAULT NULL,
                    `comm_earned` decimal(14,4) DEFAULT NULL,
                    `payout` decimal(14,4) DEFAULT NULL,
                    `balance_open1` decimal(14,4) DEFAULT NULL,
                    `balance_close1` decimal(14,4) DEFAULT NULL,
                    `processed_datetime` datetime DEFAULT NULL,
                    `created_at` datetime DEFAULT NULL,
                    `updated_at` datetime DEFAULT NULL,
                PRIMARY KEY (`id`),
                UNIQUE KEY `ticket_id_UNIQUE` (`ticket_id`)
              ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;";

		DB::statement($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bet_s128');
    }
}
