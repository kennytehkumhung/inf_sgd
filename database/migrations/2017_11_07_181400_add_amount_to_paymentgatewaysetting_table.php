<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAmountToPaymentgatewaysettingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('paymentgatewaysetting', function (Blueprint $table) {
            $table->integer('deposit_min')->after('withdraw');
            $table->integer('withdraw_min')->after('deposit_min');
            $table->integer('deposit_max')->after('withdraw_min');
            $table->integer('withdraw_max')->after('deposit_max');
            $table->tinyInteger('check_deposit_trans')->after('withdraw_max');
            $table->tinyInteger('check_withdraw_trans')->after('check_deposit_trans');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('paymentgatewaysetting', function (Blueprint $table) {
            //
        });
    }
}
