<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddModifiedToBadloginlog extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('badloginlog', function (Blueprint $table) {
            $table->dateTime('modified')->after('created');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('badloginlog', function (Blueprint $table) {
            $table->dropColumn(['modified']);
        });
    }
}
