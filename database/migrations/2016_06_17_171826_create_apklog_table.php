<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApklogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       $sql = 'CREATE TABLE `apklog` (
				  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
				  `ip` char(16) NOT NULL,
				  `updated_at` datetime NOT NULL,
				  `created_at` datetime NOT NULL,
				  PRIMARY KEY (`id`)
				) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;';
				
		DB::statement($sql);	
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
