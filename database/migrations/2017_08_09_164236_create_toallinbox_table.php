<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateToallinboxTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = "CREATE TABLE `toallinbox` (
				  `id` mediumint(8) NOT NULL AUTO_INCREMENT,
				  `accid` mediumint(8) unsigned NOT NULL,
				  `inboxid` mediumint(8) unsigned NOT NULL,
				  `created` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
				  `modified` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
				  PRIMARY KEY (`id`)
				) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

		DB::statement($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
