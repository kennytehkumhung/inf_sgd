<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;


class AddCashback extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       $sql = 'CREATE TABLE `cashback` (
				  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
				  `type` tinyint(3) unsigned NOT NULL,
				  `insid` mediumint(8) unsigned NOT NULL,
				  `product` tinyint(3) unsigned NOT NULL,
				  `clgid` mediumint(8) unsigned NOT NULL,
				  `accid` mediumint(8) unsigned NOT NULL,
				  `acccode` char(16) NOT NULL,
				  `crccode` char(3) NOT NULL,
				  `crcrate` decimal(9,5) NOT NULL,
				  `rate` decimal(5,2) NOT NULL,
				  `amount` decimal(10,2) unsigned NOT NULL,
				  `amountlocal` decimal(10,2) unsigned NOT NULL,
				  `totalwinlose` decimal(10,2) NOT NULL,
				  `totalwinloselocal` decimal(10,2) NOT NULL,
				  `datefrom` date NOT NULL,
				  `dateto` date NOT NULL,
				  `createdby` mediumint(8) unsigned NOT NULL,
				  `modifiedby` mediumint(8) unsigned NOT NULL,
				  `status` smallint(5) unsigned NOT NULL,
				  `downline` char(32) NOT NULL,
				  `created` datetime NOT NULL,
				  `modified` datetime NOT NULL,
				  PRIMARY KEY (`id`),
				  KEY `datefrom` (`datefrom`) USING BTREE
				) ENGINE=MyISAM DEFAULT CHARSET=utf8;';
				
		DB::statement($sql);	
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
