<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBetwftTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = "CREATE TABLE `bet_wft` (
			  `id` bigint(20) NOT NULL AUTO_INCREMENT,
			  `transId` varchar(45) NOT NULL,
			  `fetchId` varchar(45) DEFAULT NULL,
			  `lastModifiedDate` datetime DEFAULT NULL,
			  `userId` bigint(20) DEFAULT NULL,
			  `username` varchar(100) NOT NULL,
			  `betAmount` decimal(18,4) NOT NULL,
			  `winAmount` decimal(18,4) NOT NULL,
			  `commission` varchar(45) DEFAULT NULL,
			  `subcommission` varchar(45) DEFAULT NULL,
			  `ip` varchar(45) DEFAULT NULL,
			  `league` varchar(45) DEFAULT NULL,
			  `home` varchar(45) DEFAULT NULL,
			  `away` varchar(45) DEFAULT NULL,
			  `status` varchar(45) DEFAULT NULL,
			  `game` varchar(45) DEFAULT NULL,
			  `odds` varchar(45) DEFAULT NULL,
			  `side` varchar(45) DEFAULT NULL,
			  `info` varchar(45) DEFAULT NULL,
			  `half` varchar(45) DEFAULT NULL,
			  `transactionDate` datetime NOT NULL,
			  `workDate` datetime DEFAULT NULL,
			  `matchDate` varchar(45) DEFAULT NULL,
			  `runScore` varchar(45) DEFAULT NULL,
			  `score` varchar(45) DEFAULT NULL,
			  `htscore` varchar(45) DEFAULT NULL,
			  `firstLastGoal` varchar(45) DEFAULT NULL,
			  `flgStatus` varchar(45) DEFAULT NULL,
			  `gameDescription` varchar(45) DEFAULT NULL,
			  `gameResult` varchar(45) DEFAULT NULL,
			  `exchangeRate` varchar(45) DEFAULT NULL,
			  `oddsType` varchar(45) DEFAULT NULL,
			  `sportsType` varchar(45) DEFAULT NULL,
			  `isOver` smallint(6) NOT NULL DEFAULT '0',
			  PRIMARY KEY (`transId`),
			  UNIQUE KEY `id_UNIQUE` (`id`)
			) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;";
				
		DB::statement($sql);	
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bet_wft');
    }
}
