<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInsertMntSp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = "
CREATE DEFINER=`root`@`localhost` PROCEDURE `insert_mnt`(
IN _bet_date DATE,
IN _player_acc VARCHAR(30),
IN _accid MEDIUMINT,
IN _nickname VARCHAR(30),
IN _currency CHAR(3),
IN _total_income DECIMAL(18,4),
IN _total_outlay DECIMAL(18,4),
IN _wash_count_sum DECIMAL(18,4),
IN _created_at DATETIME,
IN _updated_at DATETIME
)
BEGIN

INSERT INTO `bet_mnt`
(
`bet_date`,
`player_acc`,
`accid`,
`nickname`,
`currency`,
`total_income`,
`total_outlay`,
`wash_count_sum`,
`created_at`,
`updated_at`
)
values(
_bet_date,
_player_acc,
_accid,
_nickname,
_currency,
_total_income,
_total_outlay,
_wash_count_sum,
_created_at,
_updated_at
)ON DUPLICATE KEY UPDATE
`accid` = _accid,
`nickname` = _nickname,
`currency` = _currency,
`total_income` = _total_income,
`total_outlay` = _total_outlay,
`wash_count_sum` = _wash_count_sum,
`updated_at` = _updated_at;

END
";

		DB::unprepared($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP PROCEDURE `insert_mnt`');
    }
}
