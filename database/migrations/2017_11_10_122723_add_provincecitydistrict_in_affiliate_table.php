<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProvincecitydistrictInAffiliateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = "ALTER TABLE `affiliate` ADD `province` CHAR(32) NOT NULL AFTER `remember_token`, 
				ADD `city` CHAR(32) NOT NULL AFTER `province`, 
				ADD `district` CHAR(32) NOT NULL AFTER `city`;";

		DB::statement($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
