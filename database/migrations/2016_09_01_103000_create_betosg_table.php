<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBetosgTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = "CREATE TABLE `bet_osg` (
			  `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
			  `playerId` VARCHAR(100) DEFAULT NULL,
			  `playerCurrency` VARCHAR(3) DEFAULT NULL,
			  `gameId` INT DEFAULT NULL,
			  `gameCategory` VARCHAR(50) DEFAULT NULL,
			  `tranId` BIGINT(20) DEFAULT NULL,
			  `tranSubId` BIGINT(20) DEFAULT NULL,
			  `totalAmount` DECIMAL(18,4) DEFAULT NULL,
			  `betAmount` DECIMAL(18,4) DEFAULT NULL,
			  `jackpotContribution` DECIMAL(18,4) DEFAULT NULL,
			  `winAmount` DECIMAL(18,4) DEFAULT NULL,
			  `gameDate` DATETIME DEFAULT NULL,
			  `platform` VARCHAR(50) DEFAULT NULL,
			  `created_at` DATETIME NOT NULL,
			  `updated_at` DATETIME NOT NULL,
			  PRIMARY KEY (`id`),
			  UNIQUE KEY `tranId_UNIQUE` (`tranId`)
			) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;";

		DB::statement($sql);
        
        // Seed config.
        $count = DB::table('config')->where('param', '=', 'SYSTEM_OSG_PULLDATE')->count();
        
        if ($count < 1) {
            DB::table('config')->insert(array(
                'groupid' => 1,
                'name' => 'SYSTEM_OSG_PULLDATE',
                'desc' => 'SYSTEM_OSG_PULLDATE',
                'param' => 'SYSTEM_OSG_PULLDATE',
                'type' => 1,
                'value' => '2016-10-01 00:00:00',
                'createdby' => 1,
                'modifiedby' => 1,
                'status' => 1,
                'created' => DB::raw('NOW()'),
                'modified' => DB::raw('NOW()'),
            ));
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bet_osg');
    }
}
