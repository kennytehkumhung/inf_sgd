<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCashbacksetting extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         $sql = 'CREATE TABLE `cashbacksetting` (
				  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
				  `product` tinyint(3) unsigned NOT NULL,
				  `crccode` char(3) NOT NULL,
				  `datefrom` date NOT NULL,
				  `dateto` date NOT NULL,
				  `rate` decimal(5,2) unsigned NOT NULL,
				  `maxamount` smallint(5) unsigned NOT NULL,
				  `minpayout` mediumint(8) unsigned NOT NULL,
				  `membertype` tinyint(2) unsigned NOT NULL,
				  `createdby` mediumint(8) unsigned NOT NULL,
				  `modifiedby` mediumint(8) unsigned NOT NULL,
				  `status` smallint(5) unsigned NOT NULL,
				  `created` datetime NOT NULL,
				  `modified` datetime NOT NULL,
				  PRIMARY KEY (`id`)
				) ENGINE=MyISAM DEFAULT CHARSET=utf8;';
				
		DB::statement($sql);	
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
