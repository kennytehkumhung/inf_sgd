<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBetWmcTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
	 

	 
    public function up()
    {
        $sql = "CREATE TABLE `bet_wmc` (
                    `id` bigint(20) NOT NULL AUTO_INCREMENT,
					`user` varchar(200) DEFAULT NULL, 
                    `betId` int DEFAULT NULL,
					`betTime` DATETIME DEFAULT NULL,
					`bet` DECIMAL(18,4) DEFAULT NULL,
					`validbet` DECIMAL(18,4) DEFAULT NULL,
					`water` DECIMAL(18,4) DEFAULT NULL,
                    `result` DECIMAL(18,4) DEFAULT NULL,
                    `betResult` varchar(20) DEFAULT NULL,
					`waterbet` DECIMAL(18,4) DEFAULT NULL,
					`winLoss` varchar(20) DEFAULT NULL,
					`ip` varchar(20) DEFAULT NULL, 
					`gid` int DEFAULT NULL,
					`event` int DEFAULT NULL,
					`eventChild` int DEFAULT NULL,
					`tableId` int DEFAULT NULL,
					`gameResult` int DEFAULT NULL,
					 `gname` varchar(20) DEFAULT NULL,
					`created_at` DATETIME DEFAULT NULL,
                    `updated_at` DATETIME DEFAULT NULL,
                PRIMARY KEY (`id`),
                UNIQUE KEY `autoid_UNIQUE` (`betId`)
              ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;";

		DB::statement($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bet_wmc`');
    }
}
