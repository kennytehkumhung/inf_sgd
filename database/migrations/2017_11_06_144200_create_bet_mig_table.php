<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBetMigTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = "CREATE TABLE `bet_mig` (
                    `id` bigint(20) NOT NULL AUTO_INCREMENT,
                    `AccountNumber` VARCHAR(20) DEFAULT NULL,
                    `Income` DECIMAL(18,4) DEFAULT NULL,
					`Payout` DECIMAL(18,4) DEFAULT NULL,
					`WinAmount` DECIMAL(18,4) DEFAULT NULL,
					`LoseAmount` DECIMAL(18,4) DEFAULT NULL,
					`Date` DATETIME DEFAULT NULL,
					`GameType` VARCHAR(50) DEFAULT NULL,
					`ModuleId` TINYINT DEFAULT NULL,
					`ClientId` MEDIUMINT DEFAULT NULL,
                    `created_at` DATETIME DEFAULT NULL,
                    `updated_at` DATETIME DEFAULT NULL,
                PRIMARY KEY (`id`),
                UNIQUE KEY `ClientId_UNIQUE` (`ClientId`)
              ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;";

		DB::statement($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bet_mig');
    }
}
