<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTbsTable0002 extends Migration {

	protected $table = 'bet_tbs';
	
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		
		/**
		 * "apiacccode" special for providers that require config.[currency].prefix 
		 * to differenciate member is from which project. So that easy to check which 
		 * member is register under which project while pulling bet sheet data, and 
		 * creating new Accountproduct record.
		 */
		
		Schema::table($this->table, function (Blueprint $table) {
			$table->tinyInteger('BetInfoResult')->nullable()->after('OutLevel');
			$table->tinyInteger('BetInfoStatus')->nullable()->after('BetInfoResult');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::table($this->table, function (Blueprint $table) {
			$table->dropColumn(['BetInfoResult', 'BetInfoStatus']);
		});
	}

}
