<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRevertbonussettingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = "CREATE TABLE `revertbonussetting` (
                    `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
                    `wbsid` TINYINT(3) DEFAULT NULL,
					`crccode` CHAR(3) DEFAULT NULL,
					`datefrom` DATE DEFAULT NULL,
					`dateto` DATE DEFAULT NULL,
					`rate` DECIMAL(5,2) DEFAULT NULL,
					`maxamount` BIGINT(20) DEFAULT NULL,
					`minpayout` BIGINT(20) DEFAULT NULL,
					`membertype` TINYINT(2) DEFAULT NULL,
					`createdby` MEDIUMINT DEFAULT NULL,
					`modifiedby` MEDIUMINT DEFAULT NULL,
					`status` SMALLINT DEFAULT 0,
                    `created_at` DATETIME DEFAULT NULL,
                    `updated_at` DATETIME DEFAULT NULL,
                PRIMARY KEY (`id`)
              ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;";

		DB::statement($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('revertbonussetting');
    }
}
