<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInsertIsnSp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = "
CREATE DEFINER=`root`@`localhost` PROCEDURE `insert_isn`(
IN _ticketId BIGINT(20),
IN _acctId VARCHAR(50),
IN _betDate DATETIME,
IN _drawId VARCHAR(10),
IN _betType VARCHAR(10),
IN _currency CHAR(3),
IN _cancelled TINYINT(1),
IN _msg TEXT,
IN _msgFrom VARCHAR(50),
IN _betIp VARCHAR(20),
IN _odds DECIMAL(18,4),
IN _betAmount DECIMAL(18,4),
IN _successAmount DECIMAL(18,4),
IN _payAmount DECIMAL(18,4),
IN _wlAmount DECIMAL(18,4),
IN _win TINYINT(1),
IN _processDate DATETIME,
IN _created_at DATETIME,
IN _updated_at DATETIME
)
BEGIN

INSERT INTO `bet_isn`
(
`ticketId`,
`acctId`,
`betDate`,
`drawId`,
`betType`,
`currency`,
`cancelled`,
`msg`,
`msgFrom`,
`betIp`,
`odds`,
`betAmount`,
`successAmount`,
`payAmount`,
`wlAmount`,
`win`,
`processDate`,
`created_at`,
`updated_at`
)
values(
_ticketId,
_acctId,
_betDate,
_drawId,
_betType,
_currency,
_cancelled,
_msg,
_msgFrom,
_betIp,
_odds,
_betAmount,
_successAmount,
_payAmount,
_wlAmount,
_win,
_processDate,
_created_at,
_updated_at
)ON DUPLICATE KEY UPDATE
`acctId` = _acctId,
`betDate` = _betDate,
`drawId` = _drawId,
`betType` = _betType,
`currency` = _currency,
`cancelled` = _cancelled,
`msg` = _msg,
`msgFrom` = _msgFrom,
`betIp` = _betIp,
`odds` = _odds,
`betAmount` = _betAmount,
`successAmount` = _successAmount,
`payAmount` = _payAmount,
`wlAmount` = _wlAmount,
`win` = _win,
`processDate` = _processDate,
`updated_at` = _updated_at;

END
";

		DB::unprepared($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP PROCEDURE `insert_isn`');
    }
}
