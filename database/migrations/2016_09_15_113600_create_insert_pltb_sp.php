<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInsertPltbSp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = "
CREATE DEFINER=`root`@`localhost` PROCEDURE `insert_pltb`(
IN _playerName VARCHAR(50),
IN _username VARCHAR(50),
IN _windowCode VARCHAR(50),
IN _gameId VARCHAR(50),
IN _gameCode BIGINT,
IN _gameType VARCHAR(50),
IN _gameName VARCHAR(50),
IN _sessionId VARCHAR(50),
IN _bet DECIMAL(16,2),
IN _win DECIMAL(16,2),
IN _progressiveBet DECIMAL(16,2),
IN _progressiveWin DECIMAL(16,2),
IN _currency CHAR(3),
IN _balance DECIMAL(16,2),
IN _currentBet DECIMAL(16,2),
IN _gameDate DATETIME,
IN _liveNetwork VARCHAR(50),
IN _rNum VARCHAR(50),
IN _created_at DATETIME,
IN _updated_at DATETIME
)
BEGIN

INSERT INTO `bet_pltb`
(
`playerName`,
`username`,
`windowCode`,
`gameId`,
`gameCode`,
`gameType`,
`gameName`,
`sessionId`,
`bet`,
`win`,
`progressiveBet`,
`progressiveWin`,
`currency`,
`balance`,
`currentBet`,
`gameDate`,
`liveNetwork`,
`rNum`,
`created_at`,
`updated_at`
)
values(
_playerName,
_username,
_windowCode,
_gameId,
_gameCode,
_gameType,
_gameName,
_sessionId,
_bet,
_win,
_progressiveBet,
_progressiveWin,
_currency,
_balance,
_currentBet,
_gameDate,
_liveNetwork,
_rNum,
_created_at,
_updated_at
)ON DUPLICATE KEY UPDATE
`playerName` = _playerName,
`username` = _username,
`windowCode` = _windowCode,
`gameId` = _gameId,
`gameCode` = _gameCode,
`gameType` = _gameType,
`gameName` = _gameName,
`sessionId` = _sessionId,
`bet` = _bet,
`win` = _win,
`progressiveBet` = _progressiveBet,
`progressiveWin` = _progressiveWin,
`currency` = _currency,
`balance` = _balance,
`currentBet` = _currentBet,
`gameDate` = _gameDate,
`liveNetwork` = _liveNetwork,
`rNum` = _rNum,
`updated_at` = _updated_at;

END
";

		DB::unprepared($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP PROCEDURE `insert_pltb`');
    }
}
