<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAffiliateTable0001 extends Migration {

	protected $table = 'affiliate';
	
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		
		// Add missing columns: type, level
		if (!Schema::hasColumn($this->table, 'type')) {
			Schema::table($this->table, function (Blueprint $table) {
				$table->tinyInteger('type')->after('parentid');
			});
		}
		
		if (!Schema::hasColumn($this->table, 'level')) {
			Schema::table($this->table, function (Blueprint $table) {
				$table->tinyInteger('level')->after('type');
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		// Do nothing.
	}

}
