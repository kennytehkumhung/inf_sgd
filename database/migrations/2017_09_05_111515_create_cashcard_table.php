<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCashcardTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = "CREATE TABLE `cashcard` (
				  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
				  `ccbid` mediumint(8) unsigned NOT NULL,
				  `aflid` mediumint(8) unsigned NOT NULL,
				  `accid` mediumint(8) unsigned NOT NULL,
				  `crccode` char(3) NOT NULL,
				  `crcrate` decimal(9,5) unsigned NOT NULL,
				  `amount` decimal(14,4) unsigned NOT NULL,
				  `serial` mediumint(6) NOT NULL,
				  `number` varchar(20) NOT NULL,
				  `cardid` varchar(16) NOT NULL,
				  `expiry` datetime NOT NULL,
				  `clgid` mediumint(8) unsigned NOT NULL,
				  `redeemed` decimal(14,4) NOT NULL,
				  `createdby` mediumint(8) unsigned NOT NULL,
				  `modifiedby` mediumint(8) unsigned NOT NULL,
				  `status` smallint(5) unsigned NOT NULL,
				  `created` datetime NOT NULL,
				  `modified` datetime NOT NULL,
				  PRIMARY KEY (`id`),
				  KEY `serial` (`serial`),
				  KEY `number` (`number`)
				) ENGINE=MyISAM AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8;";

		DB::statement($sql);
    }
	
	

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
