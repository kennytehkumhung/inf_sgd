<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInboxTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = "CREATE TABLE `inbox` (
				  `id` mediumint(8) NOT NULL AUTO_INCREMENT,
				  `title` varchar(255) NOT NULL,
				  `content` text NOT NULL,
				  `accid` mediumint(8) unsigned NOT NULL,
				  `status` tinyint(5) unsigned NOT NULL,
				  `created` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
				  `modified` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
				  `nickname` varchar(255) NOT NULL,
				  `is_all` tinyint(5) NOT NULL,
				  PRIMARY KEY (`id`)
				) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;";

		DB::statement($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
