<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBetascTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = "CREATE TABLE `bet_asc` (
			  `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
			  `betId` BIGINT DEFAULT NULL,
			  `account` VARCHAR(32) DEFAULT NULL,
			  `betAmount` DECIMAL(18,4) DEFAULT NULL,
			  `betOdd` DECIMAL(18,4) DEFAULT NULL,
			  `win` DECIMAL(18,4) DEFAULT NULL,
			  `comm` DECIMAL(18,4) DEFAULT NULL,
			  `betType` VARCHAR(32) DEFAULT NULL,
			  `oddStyle` VARCHAR(5) DEFAULT NULL,
			  `hdp` DECIMAL(18,4) DEFAULT NULL,
			  `betPos` VARCHAR(32) DEFAULT NULL,
			  `live` TINYINT DEFAULT NULL,
			  `betDate` DATETIME DEFAULT NULL,
			  `status` VARCHAR(32) DEFAULT NULL,
			  `result` VARCHAR(32) DEFAULT NULL,
			  `reportDate` DATETIME DEFAULT NULL,
			  `sportName` VARCHAR(32) DEFAULT NULL,
			  `leagueId` BIGINT DEFAULT NULL,
			  `homeId` BIGINT DEFAULT NULL,
			  `awayId` BIGINT DEFAULT NULL,
			  `betScore` VARCHAR(32) DEFAULT NULL,
			  `matchDate` DATETIME DEFAULT NULL,
			  `betIp` VARCHAR(32) DEFAULT NULL,
			  `matchId` BIGINT DEFAULT NULL,
			  `betUpdateTime` DATETIME DEFAULT NULL,
			  `created_at` DATETIME NOT NULL,
			  `updated_at` DATETIME NOT NULL,
			  PRIMARY KEY (`id`),
			  UNIQUE KEY `betId_UNIQUE` (`betId`)
			) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;";

		DB::statement($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bet_asc');
    }
}
