<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDailyrebateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		$sql = "CREATE TABLE `dailyrebate` ( 
		`id` MEDIUMINT(8) NOT NULL AUTO_INCREMENT , 
		`accid` MEDIUMINT(11) NOT NULL , 
		`clgid` MEDIUMINT(8) NOT NULL , 
		`nickname` VARCHAR(32) NOT NULL , 
		`date` DATE NOT NULL , 
		`crccode` CHAR(3) NOT NULL , 
		`totalstake` DECIMAL(12,2) NOT NULL , 
		`amount` DECIMAL(12,2) NOT NULL , 
		`status` SMALLINT(5) NOT NULL ,
		`created` DATETIME DEFAULT NULL DEFAULT CURRENT_TIMESTAMP,
		`modified` DATETIME DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
		PRIMARY KEY (`id`)) ENGINE = InnoDB;";

		DB::statement($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
