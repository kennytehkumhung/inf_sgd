<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBetgdxTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = "CREATE TABLE `bet_gdx` (
			  `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
			  `messageId` VARCHAR(45) DEFAULT NULL,
			  `no` INT DEFAULT NULL,
			  `userId` VARCHAR(45) DEFAULT NULL,
			  `betTime` DATETIME DEFAULT NULL,
			  `balanceTime` DATETIME DEFAULT NULL,
			  `productId` VARCHAR(100) DEFAULT NULL,
			  `gameInterface` VARCHAR(20) DEFAULT NULL,
			  `betId` VARCHAR(45) DEFAULT NULL,
			  `betType` VARCHAR(20) DEFAULT NULL,
			  `betAmount` DECIMAL(18,2) DEFAULT NULL,
			  `winLoss` DECIMAL(18,2) DEFAULT NULL,
			  `betResult` VARCHAR(10) DEFAULT NULL,
			  `startBalance` DECIMAL(18,2) DEFAULT NULL,
			  `endBalance` DECIMAL(18,2) DEFAULT NULL,
			  `gameId` VARCHAR(20) DEFAULT NULL,
			  `subBetType` VARCHAR(45) DEFAULT NULL,
			  `gameResult` VARCHAR(250) DEFAULT NULL,
			  `winningBet` VARCHAR(250) DEFAULT NULL,
			  `tableId` VARCHAR(20) DEFAULT NULL,
			  `dealerId` INT DEFAULT NULL,
			  `created_at` DATETIME NOT NULL,
			  `updated_at` DATETIME NOT NULL,
			  PRIMARY KEY (`id`),
			  UNIQUE KEY `betId_UNIQUE` (`betId`)
			) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;";
				
		DB::statement($sql);	
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bet_gdx');
    }
}
