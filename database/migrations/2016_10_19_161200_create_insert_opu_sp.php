<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInsertOpuSp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = "
CREATE DEFINER=`root`@`localhost` PROCEDURE `insert_opu`(
IN _transaction_id INT,
IN _transaction_date_time DATETIME,
IN _member_code VARCHAR(20),
IN _member_type VARCHAR(10),
IN _currency CHAR(3),
IN _balance_start DECIMAL(18,4),
IN _balance_end DECIMAL(18,4),
IN _bet DECIMAL(18,4),
IN _win DECIMAL(18,4),
IN _game_code CHAR(2),
IN _game_detail VARCHAR(255),
IN _bet_status VARCHAR(10),
IN _player_hand TEXT,
IN _game_result TEXT,
IN _game_category VARCHAR(10),
IN _vendor VARCHAR(20),
IN _draw_number INT,
IN _m88_studio VARCHAR(10),
IN _stamp_date DATETIME,
IN _bet_record_id VARCHAR(10),
IN _created_at DATETIME,
IN _updated_at DATETIME
)
BEGIN

INSERT INTO `bet_opu`
(
`transaction_id`,
`transaction_date_time`,
`member_code`,
`member_type`,
`currency`,
`balance_start`,
`balance_end`,
`bet`,
`win`,
`game_code`,
`game_detail`,
`bet_status`,
`player_hand`,
`game_result`,
`game_category`,
`vendor`,
`draw_number`,
`m88_studio`,
`stamp_date`,
`bet_record_id`,
`created_at`,
`updated_at`
)
values(
_transaction_id,
_transaction_date_time,
_member_code,
_member_type,
_currency,
_balance_start,
_balance_end,
_bet,
_win,
_game_code,
_game_detail,
_bet_status,
_player_hand,
_game_result,
_game_category,
_vendor,
_draw_number,
_m88_studio,
_stamp_date,
_bet_record_id,
_created_at,
_updated_at
)ON DUPLICATE KEY UPDATE
`transaction_date_time` = _transaction_date_time,
`member_code` = _member_code,
`member_type` = _member_type,
`currency` = _currency,
`balance_start` = _balance_start,
`balance_end` = _balance_end,
`bet` = _bet,
`win` = _win,
`game_code` = _game_code,
`game_detail` = _game_detail,
`bet_status` = _bet_status,
`player_hand` = _player_hand,
`game_result` = _game_result,
`game_category` = _game_category,
`vendor` = _vendor,
`draw_number` = _draw_number,
`m88_studio` = _m88_studio,
`stamp_date` = _stamp_date,
`bet_record_id` = _bet_record_id,
`updated_at` = _updated_at;

END
";

		DB::unprepared($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP PROCEDURE `insert_opu`');
    }
}
