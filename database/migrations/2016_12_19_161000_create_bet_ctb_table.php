<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBetCtbTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('DROP TABLE IF EXISTS `bet_ctb`');

        $sql = "CREATE TABLE `bet_ctb` (
                    `id` bigint(20) NOT NULL AUTO_INCREMENT,
                    `refID` bigint(20) DEFAULT NULL,
                    `userID` varchar(45) DEFAULT NULL,
                    `raceID` int(11) DEFAULT NULL,
                    `raceType` varchar(45) DEFAULT NULL,
                    `raceDate` date DEFAULT NULL,
                    `raceNo` int(11) DEFAULT NULL,
                    `runnerNo` int(11) DEFAULT NULL,
                    `currencyName` varchar(5) DEFAULT NULL,
                    `status` varchar(10) DEFAULT NULL,
                    `limit` varchar(45) DEFAULT NULL,
                    `payout` decimal(18,4) DEFAULT NULL,
                    `transactionDate` datetime DEFAULT NULL,
                    `updateDate` datetime DEFAULT NULL,
                    `place` decimal(18,4) DEFAULT NULL,
                    `win` decimal(18,4) DEFAULT NULL,
                    `created_at` datetime DEFAULT NULL,
                    `updated_at` datetime DEFAULT NULL,
                PRIMARY KEY (`id`),
                UNIQUE KEY `refID_UNIQUE` (`refID`)
              ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;";

		DB::statement($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bet_ctb');
    }
}
