<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInsertS128Sp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = "
CREATE DEFINER=`root`@`localhost` PROCEDURE `insert_s128`(
IN _ticket_id int(20),
IN _login_id varchar(30),
IN _arena_code varchar(10),
IN _arena_name_cn varchar(10),
IN _match_no varchar(10),
IN _match_type varchar(20),
IN _match_date date,
IN _fight_no int(20),
IN _fight_datetime datetime,
IN _meron_cock varchar(20),
IN _meron_cock_cn varchar(10),
IN _wala_cock varchar(20),
IN _wala_cock_cn varchar(10),
IN _bet_on varchar(10),
IN _odds_type varchar(10),
IN _odds_asked decimal(5,3),
IN _odds_given decimal(5,3),
IN _stake int(20),
IN _stake_money decimal(14,4),
IN _balance_open decimal(14,4),
IN _balance_close decimal(14,4),
IN _created_datetime datetime,
IN _fight_result varchar(10),
IN _status varchar(10),
IN _winloss decimal(14,4),
IN _comm_earned decimal(14,4),
IN _payout decimal(14,4),
IN _balance_open1 decimal(14,4),
IN _balance_close1 decimal(14,4),
IN _processed_datetime datetime,
IN _created_at datetime,
IN _updated_at datetime
)
BEGIN

INSERT INTO `bet_s128`
(
`ticket_id`,
`login_id`,
`arena_code`,
`arena_name_cn`,
`match_no`,
`match_type`,
`match_date`,
`fight_no`,
`fight_datetime`,
`meron_cock`,
`meron_cock_cn`,
`wala_cock`,
`wala_cock_cn`,
`bet_on`,
`odds_type`,
`odds_asked`,
`odds_given`,
`stake`,
`stake_money`,
`balance_open`,
`balance_close`,
`created_datetime`,
`fight_result`,
`status`,
`winloss`,
`comm_earned`,
`payout`,
`balance_open1`,
`balance_close1`,
`processed_datetime`,
`created_at`,
`updated_at`
)
values(
_ticket_id,
_login_id,
_arena_code,
_arena_name_cn,
_match_no,
_match_type,
_match_date,
_fight_no,
_fight_datetime,
_meron_cock,
_meron_cock_cn,
_wala_cock,
_wala_cock_cn,
_bet_on,
_odds_type,
_odds_asked,
_odds_given,
_stake,
_stake_money,
_balance_open,
_balance_close,
_created_datetime,
_fight_result,
_status,
_winloss,
_comm_earned,
_payout,
_balance_open1,
_balance_close1,
_processed_datetime,
_created_at,
_updated_at
)ON DUPLICATE KEY UPDATE
`ticket_id` = _ticket_id,
`login_id` = _login_id,
`arena_code` = _arena_code,
`arena_name_cn` = _arena_name_cn,
`match_no` = _match_no,
`match_type` = _match_type,
`match_date` = _match_date,
`fight_no` = _fight_no,
`fight_datetime` = _fight_datetime,
`meron_cock` = _meron_cock,
`meron_cock_cn` = _meron_cock_cn,
`wala_cock` = _wala_cock,
`wala_cock_cn` = _wala_cock_cn,
`bet_on` = _bet_on,
`odds_type` = _odds_type,
`odds_asked` = _odds_asked,
`odds_given` = _odds_given,
`stake` = _stake,
`stake_money` = _stake_money,
`balance_open` = _balance_open,
`balance_close` = _balance_close,
`created_datetime` = _created_datetime,
`fight_result` = _fight_result,
`status` = _status,
`winloss` = _winloss,
`comm_earned` = _comm_earned,
`payout` = _payout,
`balance_open1` = _balance_open1,
`balance_close1` = _balance_close1,
`processed_datetime` = _processed_datetime,
`updated_at` = _updated_at;

END
";

		DB::unprepared($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP PROCEDURE `insert_s128`');
    }
}
