<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInsertMigSp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = "
CREATE DEFINER=`root`@`127.0.0.1` PROCEDURE `insert_mig`(
IN _AccountNumber VARCHAR(20),
IN _Income DECIMAL(18,4),
IN _Payout DECIMAL(18,4),
IN _WinAmount DECIMAL(18,4),
IN _LoseAmount DECIMAL(18,4),
IN _Date DATETIME,
IN _GameType VARCHAR(50),
IN _ModuleId TINYINT,
IN _ClientId MEDIUMINT,
IN _created_at DATETIME,
IN _updated_at DATETIME
)
BEGIN

INSERT INTO `bet_mig`
(
`AccountNumber`,
`Income`,
`Payout`,
`WinAmount`,
`LoseAmount`,
`Date`,
`GameType`,
`ModuleId`,
`ClientId`,
`created_at`,
`updated_at`
)
values(
_AccountNumber,
_Income,
_Payout,
_WinAmount,
_LoseAmount,
_Date,
_GameType,
_ModuleId,
_ClientId,
_created_at,
_updated_at
)ON DUPLICATE KEY UPDATE
`AccountNumber` = _AccountNumber,
`Income` = _Income, 
`Payout` = _Payout,
`WinAmount` = _WinAmount,
`LoseAmount` = _LoseAmount,
`Date` = _Date,
`GameType` = _GameType,
`ModuleId` = _ModuleId,
`ClientId` = _ClientId,
`updated_at` = _updated_at;

END
";

		DB::unprepared($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP PROCEDURE `insert_mig`');
    }
}
