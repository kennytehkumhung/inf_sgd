<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBetUc8Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = "CREATE TABLE `bet_uc8` (
                    `id` bigint(20) NOT NULL AUTO_INCREMENT,
                    `bet_id` MEDIUMINT DEFAULT NULL,
                    `username` VARCHAR(30) DEFAULT NULL,
                    `currency` CHAR(3) DEFAULT NULL,
                    `game_type` TINYINT DEFAULT NULL,
                    `game_code` VARCHAR(100) DEFAULT NULL,
                    `game_name` VARCHAR(100) DEFAULT NULL,
                    `bet` DECIMAL(18,4) DEFAULT NULL,
                    `win` DECIMAL(18,4) DEFAULT NULL,
                    `report_date` DATE DEFAULT NULL,
                    `game_time` DATETIME DEFAULT NULL,
                    `status` INT DEFAULT NULL,
                    `created_at` DATETIME DEFAULT NULL,
                    `updated_at` DATETIME DEFAULT NULL,
                PRIMARY KEY (`id`),
                UNIQUE KEY `bet_id_UNIQUE` (`bet_id`)
              ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;";

		DB::statement($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bet_uc8');
    }
}
