<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchedulerSettingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = "CREATE TABLE `scheduler_setting` (
                    `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT,
                    `name` varchar(30) NOT NULL,
                    `description` varchar(255) NULL,
                    `type` varchar(20) NOT NULL DEFAULT 'exec',
                    `status` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
                    `execute` varchar(255) NOT NULL,
                    `options` varchar(255) NOT NULL,
                    `cron` varchar(20) NOT NULL DEFAULT '* * * * *',
                    `created_at` datetime DEFAULT NULL,
                    `updated_at` datetime DEFAULT NULL,
                PRIMARY KEY (`id`),
                UNIQUE KEY `name_UNIQUE` (`name`)
              ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;";

		DB::statement($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
//        Schema::drop('scheduler_setting');
    }
}
