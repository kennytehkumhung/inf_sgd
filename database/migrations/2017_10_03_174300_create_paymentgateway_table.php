<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentgatewayTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = "CREATE TABLE `paymentgateway` (
                    `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT,
                    `fdmid` mediumint(8) UNSIGNED NOT NULL,
                    `type` tinyint(3) UNSIGNED NOT NULL,
                    `pacid` mediumint(8) NOT NULL,
                    `clgid` mediumint(8) NOT NULL,
                    `paytype` mediumint(8) NOT NULL,
                    `refid` varchar(30) NOT NULL,
                    `orderid` varchar(30) NOT NULL,
                    `transid` varchar(60) NOT NULL,
                    `name` varchar(100) NOT NULL,
                    `acccode` varchar(30) NOT NULL,
                    `accid` mediumint(8) UNSIGNED NOT NULL,
                    `crccode` char(3) NOT NULL,
                    `crcrate` decimal(18,4) NOT NULL,
                    `amount` decimal(18,4) NOT NULL,
                    `amountlocal` decimal(18,4) NOT NULL,
                    `fee` decimal(18,4) NOT NULL,
                    `feelocal` decimal(18,4) NOT NULL,
                    `datetime` varchar(20) NOT NULL,
                    `userid` varchar(16) NOT NULL,
                    `systemappid` varchar(16) NOT NULL,
                    `viewid` varchar(20) NOT NULL,
                    `styleid` varchar(20) NOT NULL,
                    `succ` char(1) NOT NULL,
                    `des` varchar(255) NOT NULL,
                    `hmac` varchar(32) NOT NULL,
                    `log` text NOT NULL,
                    `createdip` char(15) NOT NULL,
                    `status` smallint(5) NOT NULL,
                    `bankaccno` varchar(32) NOT NULL,
                    `bankaccname` varchar(32) NOT NULL,
                    `bnkid` mediumint(8) NOT NULL,
                    `created_at` datetime DEFAULT NULL,
                    `updated_at` datetime DEFAULT NULL,
                PRIMARY KEY (`id`)
              ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;";

		DB::statement($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
//        Schema::drop('paymentgateway');
    }
}
