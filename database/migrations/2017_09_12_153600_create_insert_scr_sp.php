<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInsertScrSp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = "
CREATE DEFINER=`root`@`127.0.0.1` PROCEDURE `insert_scr`(
IN _playerid bigint,
IN _win DECIMAL(18,4),
IN _game_time DATE,
IN _created_at DATETIME,
IN _updated_at DATETIME
)
BEGIN

INSERT INTO `bet_scr`
(
`playerid`,
`win`,
`game_time`,
`created_at`,
`updated_at`
)
values(
_playerid,
_win,
_game_time,
_created_at,
_updated_at
)ON DUPLICATE KEY UPDATE
`playerid` = _playerid,
`win` = _win, 
`game_time` = _game_time,
`updated_at` = _updated_at;

END
";

		DB::unprepared($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP PROCEDURE `insert_scr`');
    }
}
