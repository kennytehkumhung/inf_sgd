<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAffiliateReportTable extends Migration {

	protected $table = 'affiliatereport';
	
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		$sql = "CREATE TABLE `" . $this->table . "` (
			  `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
			  `aflid` BIGINT(20),
			  `username` VARCHAR(32),
			  `crccode` VARCHAR(3),
			  `crcrate` DECIMAL(9,5),
			  `turnover` DECIMAL(14,4),
			  `valid` DECIMAL(14,4),
			  `winloss` DECIMAL(14,4),
			  `deposit` DECIMAL(14,4),
			  `withdrawal` DECIMAL(14,4),
			  `bonus` DECIMAL(14,4),
			  `incentive` DECIMAL(14,4),
			  `fee` DECIMAL(14,4),
			  `rate` DECIMAL(9,4),
			  `commission` DECIMAL(14,4),
			  `payout` DECIMAL(14,4),
			  `register` SMALLINT(5),
			  `betmember` BIGINT(20),
			  `date` DATETIME,
			  `from` DATETIME,
			  `to` DATETIME,
			  `createdby` BIGINT(20),
			  `modifiedby` BIGINT(20),
			  `status` SMALLINT(5),
			  `created` DATETIME NOT NULL,
			  `modified` DATETIME NOT NULL,
			  PRIMARY KEY (`id`)
			) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;";

		DB::statement($sql);
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::drop($this->table);
	}

}
