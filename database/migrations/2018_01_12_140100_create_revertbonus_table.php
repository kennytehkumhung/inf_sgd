<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRevertbonusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = "CREATE TABLE `revertbonus` (
                    `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
                    `type` TINYINT(3) DEFAULT NULL,
					`rbsid` MEDIUMINT(8) DEFAULT NULL,
					`product` TINYINT(3) DEFAULT NULL,
					`prdid` TINYINT(3) DEFAULT NULL,
					`clgid` MEDIUMINT(8) DEFAULT NULL,
					`accid` MEDIUMINT(8) DEFAULT NULL,
					`acccode` CHAR(16) DEFAULT NULL,
					`crccode` CHAR(3) DEFAULT NULL,
					`crcrate` DECIMAL(9,5) DEFAULT NULL,
					`rate` DECIMAL(5,2) DEFAULT NULL,
					`amount` DECIMAL(10,2) DEFAULT NULL,
					`amountlocal` DECIMAL(10,2) DEFAULT NULL,
					`totalstake` DECIMAL(10,2) DEFAULT NULL,
					`totalstakelocal` DECIMAL(10,2) DEFAULT NULL,
					`datefrom` DATE DEFAULT NULL,
					`dateto` DATE DEFAULT NULL,
					`createdby` MEDIUMINT(8) DEFAULT NULL,
					`modifiedby` MEDIUMINT(8) DEFAULT NULL,
					`status` SMALLINT(5) DEFAULT NULL,
                    `created_at` DATETIME DEFAULT NULL,
                    `updated_at` DATETIME DEFAULT NULL,
                PRIMARY KEY (`id`)
              ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;";

		DB::statement($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('revertbonus');
    }
}
