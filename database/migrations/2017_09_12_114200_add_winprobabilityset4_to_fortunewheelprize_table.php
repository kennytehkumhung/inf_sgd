<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddWinprobabilityset4ToFortunewheelprizeTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fortune_wheel_prize', function (Blueprint $table) {
            $table->tinyInteger('win_probability4')->default(0)->after('win_probability_free_token3');
            $table->tinyInteger('win_probability_free_token4')->default(0)->after('win_probability4');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
//        Schema::table('fortune_wheel_prize', function (Blueprint $table) {
//            $table->dropColumn(['win_probability4', 'win_probability_free_token4']);
//        });
    }
}
