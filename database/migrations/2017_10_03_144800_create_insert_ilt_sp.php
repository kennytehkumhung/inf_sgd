<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInsertIltSp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = "
CREATE DEFINER=`root`@`localhost` PROCEDURE `insert_ilt`(
IN _bet_id MEDIUMINT,
IN _account VARCHAR(30),
IN _userid MEDIUMINT,
IN _username VARCHAR(30),
IN _currency CHAR(3),
IN _betslipsid MEDIUMINT,
IN _betref VARCHAR(30),
IN _eventtime DATETIME,
IN _league VARCHAR(30),
IN _type VARCHAR(10),
IN _teambet VARCHAR(10),
IN _stake DECIMAL(18,4),
IN _times DECIMAL(18,4),
IN _mode VARCHAR(10),
IN _odds DECIMAL(18,4),
IN _result TINYINT,
IN _win DECIMAL(18,4),
IN _status TINYINT,
IN _datecreated DATETIME,
IN _created_at DATETIME,
IN _updated_at DATETIME
)
BEGIN

INSERT INTO `bet_ilt`
(
`bet_id`,
`account`,
`userid`,
`username`,
`currency`,
`betslipsid`,
`betref`,
`eventtime`,
`league`,
`type`,
`teambet`,
`stake`,
`times`,
`mode`,
`odds`,
`result`,
`win`,
`status`,
`datecreated`,
`created_at`,
`updated_at`
)
values(
_bet_id,
_account,
_userid,
_username,
_currency,
_betslipsid,
_betref,
_eventtime,
_league,
_type,
_teambet,
_stake,
_times,
_mode,
_odds,
_result,
_win,
_status,
_datecreated,
_created_at,
_updated_at
)ON DUPLICATE KEY UPDATE
`bet_id` = _bet_id,
`account` = _account,
`userid` = _userid,
`username` = _username,
`currency` = _currency,
`betref` = _betref,
`eventtime` = _eventtime,
`league` = _league,
`type` = _type,
`teambet` = _teambet,
`stake` = _stake,
`times` = _times,
`mode` = _mode,
`odds` = _odds,
`result` = _result,
`win` = _win,
`status` = _status,
`datecreated` = _datecreated,
`updated_at` = _updated_at;

END
";

		DB::unprepared($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP PROCEDURE `insert_ilt`');
    }
}
