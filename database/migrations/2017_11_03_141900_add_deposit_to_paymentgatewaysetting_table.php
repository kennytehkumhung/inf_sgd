<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDepositToPaymentgatewaysettingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('paymentgatewaysetting', function (Blueprint $table) {
            $table->char('code', 3)->after('id');
            $table->tinyInteger('seq')->after('name');
            $table->string('class_name')->after('seq');
            $table->tinyInteger('deposit')->after('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('paymentgatewaysetting', function (Blueprint $table) {
            //
        });
    }
}
