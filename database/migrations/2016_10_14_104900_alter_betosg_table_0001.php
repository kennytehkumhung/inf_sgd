<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterBetosgTable0001 extends Migration {

	protected $table = 'bet_osg';
	
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		
        Schema::table($this->table, function (Blueprint $table) {
            $table->string('gameName')->nullable()->after('gameId');
        });
        
        DB::statement('ALTER TABLE '.$this->table.' MODIFY gameId VARCHAR(50) DEFAULT NULL');
        DB::statement('ALTER TABLE '.$this->table.' MODIFY tranSubId VARCHAR(50) DEFAULT NULL');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
        
        Schema::table($this->table, function (Blueprint $table) {
            $table->dropColumn(['gameName']);
        });
        
        DB::statement('ALTER TABLE '.$this->table.' MODIFY gameId INT DEFAULT NULL');
        DB::statement('ALTER TABLE '.$this->table.' MODIFY tranSubId BIGINT(20) DEFAULT NULL');
	}

}
