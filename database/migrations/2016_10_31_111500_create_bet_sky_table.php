<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBetSkyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = "CREATE TABLE `bet_sky` (
                    `id` bigint(20) NOT NULL AUTO_INCREMENT,
                    `ref_id` bigint(20) NULL,
                    `user_id` varchar(20) DEFAULT NULL,
                    `username` varchar(20) DEFAULT NULL,
                    `currency` char(3) DEFAULT NULL,
                    `match_id` varchar(32) DEFAULT NULL,
                    `game_type` varchar(32) DEFAULT NULL,
                    `game_code` varchar(32) DEFAULT NULL,
                    `bet_time` datetime DEFAULT NULL,
                    `bet` decimal(18,4) DEFAULT NULL,
                    `win` decimal(18,4) DEFAULT NULL,
                    `winlose` decimal(18,4) DEFAULT NULL,
                    `jackpot` decimal(18,4) DEFAULT NULL,
                    `bet_ip` varchar(16) DEFAULT NULL,
                    `created_at` datetime DEFAULT NULL,
                    `updated_at` datetime DEFAULT NULL,
                PRIMARY KEY (`id`),
                UNIQUE KEY `match_id_UNIQUE` (`match_id`)
              ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;";

		DB::statement($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bet_sky');
    }
}
