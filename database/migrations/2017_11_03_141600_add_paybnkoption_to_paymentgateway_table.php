<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPaybnkoptionToPaymentgatewayTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('paymentgateway', function (Blueprint $table) {
            $table->string('paybnkoption', 30)->after('paytype');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('paymentgateway', function (Blueprint $table) {
            //
        });
    }
}
