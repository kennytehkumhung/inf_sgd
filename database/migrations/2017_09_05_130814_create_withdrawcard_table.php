<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWithdrawcardTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         $sql = "CREATE TABLE `withdrawcard` (
				  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
				  `aflid` mediumint(8) unsigned NOT NULL,
				  `accid` mediumint(8) unsigned NOT NULL,
				  `crccode` char(3) NOT NULL,
				  `crcrate` decimal(9,5) unsigned NOT NULL,
				  `amount` decimal(14,4) unsigned NOT NULL,
				  `number` varchar(20) NOT NULL,
				  `clgid` mediumint(8) unsigned NOT NULL,
				  `redeemed` decimal(14,4) NOT NULL,
				  `createdby` mediumint(8) unsigned NOT NULL,
				  `modifiedby` mediumint(8) unsigned NOT NULL,
				  `status` smallint(5) unsigned NOT NULL,
				  `created` datetime NOT NULL,
				  `modified` datetime NOT NULL,
				  PRIMARY KEY (`id`),
				  KEY `number` (`number`)
				) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;";

		DB::statement($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
