<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBetOpuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = "CREATE TABLE `bet_opu` (
                    `id` bigint(20) NOT NULL AUTO_INCREMENT,
                    `transaction_id` int DEFAULT NULL,
                    `transaction_date_time` datetime DEFAULT NULL,
                    `member_code` varchar(20) DEFAULT NULL,
                    `member_type` varchar(10) DEFAULT NULL,
                    `currency` char(3) DEFAULT NULL,
                    `balance_start` decimal(18,4) DEFAULT NULL,
                    `balance_end` decimal(18,4) DEFAULT NULL,
                    `bet` decimal(18,4) DEFAULT NULL,
                    `win` decimal(18,4) DEFAULT NULL,
                    `game_code` char(2) DEFAULT NULL,
                    `game_detail` varchar(255) DEFAULT NULL,
                    `bet_status` varchar(10) DEFAULT NULL,
                    `player_hand` text DEFAULT NULL,
                    `game_result` text DEFAULT NULL,
                    `game_category` varchar(10) DEFAULT NULL,
                    `vendor` varchar(20) DEFAULT NULL,
                    `draw_number` int DEFAULT NULL,
                    `m88_studio` varchar(10) DEFAULT NULL,
                    `stamp_date` datetime DEFAULT NULL,
                    `bet_record_id` varchar(10) DEFAULT NULL,
                    `created_at` datetime DEFAULT NULL,
                    `updated_at` datetime DEFAULT NULL,
                PRIMARY KEY (`id`),
                UNIQUE KEY `transaction_id_UNIQUE` (`transaction_id`)
              ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;";

		DB::statement($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bet_opu');
    }
}
