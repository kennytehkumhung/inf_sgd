<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBetsbofTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = "CREATE TABLE `bet_sbof` (
			  `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
			  `betId` BIGINT DEFAULT NULL,
			  `username` VARCHAR(32) DEFAULT NULL,
			  `matchId` MEDIUMINT DEFAULT NULL,
			  `currency` VARCHAR(5) DEFAULT NULL,
			  `oddsType` VARCHAR(100) DEFAULT NULL,
			  `oddsClass` VARCHAR(10) DEFAULT NULL,
			  `oddsSubtype` VARCHAR(10) DEFAULT NULL,
			  `oddsId` MEDIUMINT DEFAULT NULL,
			  `odds` DECIMAL(18,3) DEFAULT NULL,
			  `stake` DECIMAL(18,2) DEFAULT NULL,
			  `payout` DECIMAL(18,2) DEFAULT NULL,
			  `timestamp` DATETIME DEFAULT NULL,
			  `active` VARCHAR(10) DEFAULT NULL,
			  `remarks` VARCHAR(100) DEFAULT NULL,
			  `runningMinutes` INT DEFAULT NULL,
			  `sportId` MEDIUMINT DEFAULT NULL,
			  `sportName` VARCHAR(100) DEFAULT NULL,
			  `regionName` VARCHAR(100) DEFAULT NULL,
			  `leagueName` VARCHAR(200) DEFAULT NULL,
			  `enLeagueName` VARCHAR(200) DEFAULT NULL,
			  `matchTime` DATETIME DEFAULT NULL,
			  `teamA` VARCHAR(200) DEFAULT NULL,
			  `teamB` VARCHAR(200) DEFAULT NULL,
			  `betTimestamp` DATETIME DEFAULT NULL,
			  `recalculation` VARCHAR(10) DEFAULT NULL,
			  `betType` INT DEFAULT NULL,
			  `handicap` DECIMAL(18,2) DEFAULT NULL,
			  `goal` DECIMAL(18,2) DEFAULT NULL,
			  `scoreTeamA` INT DEFAULT NULL,
			  `scoreTeamB` INT DEFAULT NULL,
			  `created_at` DATETIME NOT NULL,
			  `updated_at` DATETIME NOT NULL,
			  PRIMARY KEY (`id`),
			  UNIQUE KEY `betId_UNIQUE` (`betId`)
			) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;";
				
		DB::statement($sql);	
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bet_sbof');
    }
}
