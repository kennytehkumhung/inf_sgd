<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddWechatToAccountdetailTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('accountdetail', function (Blueprint $table) {
            $table->char('wechat', 64)->after('line');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('accountdetail', function (Blueprint $table) {
//            $table->dropColumn(['wechat']);
        });
    }
}
