<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInsertSkySp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = "
CREATE DEFINER=`root`@`localhost` PROCEDURE `insert_sky`(
IN _ref_id BIGINT,
IN _user_id VARCHAR(20),
IN _username VARCHAR(20),
IN _currency CHAR(3),
IN _match_id VARCHAR(32),
IN _game_type VARCHAR(32),
IN _game_code VARCHAR(32),
IN _bet_time DATETIME,
IN _bet DECIMAL(18,4),
IN _win DECIMAL(18,4),
IN _winlose DECIMAL(18,4),
IN _jackpot DECIMAL(18,4),
IN _bet_ip VARCHAR(16),
IN _created_at DATETIME,
IN _updated_at DATETIME
)
BEGIN

INSERT INTO `bet_sky`
(
`ref_id`,
`user_id`,
`username`,
`currency`,
`match_id`,
`game_type`,
`game_code`,
`bet_time`,
`bet`,
`win`,
`winlose`,
`jackpot`,
`bet_ip`,
`created_at`,
`updated_at`
)
values(
_ref_id,
_user_id,
_username,
_currency,
_match_id,
_game_type,
_game_code,
_bet_time,
_bet,
_win,
_winlose,
_jackpot,
_bet_ip,
_created_at,
_updated_at
)ON DUPLICATE KEY UPDATE
`user_id` = _user_id,
`username` = _username,
`currency` = _currency,
`game_type` = _game_type,
`game_code` = _game_code,
`bet_time` = _bet_time,
`bet` = _bet,
`win` = _win,
`winlose` = _winlose,
`jackpot` = _jackpot,
`bet_ip` = _bet_ip,
`updated_at` = _updated_at;

END
";

		DB::unprepared($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP PROCEDURE `insert_sky`');
    }
}
