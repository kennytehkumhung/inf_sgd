<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDailyrebatesettingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		$sql = "CREATE TABLE `dailyrebatesetting` ( 
				`id` MEDIUMINT(8) NOT NULL AUTO_INCREMENT , 
				`turnover` INT(11) NOT NULL , 
				`amount` INT(11) NOT NULL , 
				`created` DATETIME DEFAULT NULL DEFAULT CURRENT_TIMESTAMP,
				`updated` DATETIME DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
				PRIMARY KEY (`id`)
				) ENGINE = InnoDB;";

		DB::statement($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
