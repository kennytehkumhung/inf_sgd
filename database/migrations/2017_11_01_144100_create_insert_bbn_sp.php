<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInsertBbnSp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = "
CREATE DEFINER=`root`@`127.0.0.1` PROCEDURE `insert_bbn`(
IN _AccName VARCHAR(50),
IN _UserName VARCHAR(50),
IN _WagersID VARCHAR(200),
IN _WagerDate DATETIME,
IN _SerialID VARCHAR(200),
IN _RoundNo MEDIUMINT,
IN _GameType MEDIUMINT,
IN _WagerDetail VARCHAR(50),
IN _GameCode TINYINT,
IN _Result VARCHAR(20),
IN _ResultType VARCHAR(10),
IN _Card VARCHAR(50),
IN _BetAmount MEDIUMINT,
IN _Origin VARCHAR(5),
IN _Payoff DECIMAL(18,4),
IN _Currency CHAR(3),
IN _ExchangeRate DECIMAL(18,4),
IN _Commissionable TINYINT,
IN _created_at DATETIME,
IN _updated_at DATETIME
)
BEGIN

INSERT INTO `bet_bbn`
(
`AccName`,
`UserName`,
`WagersID`,
`WagerDate`,
`SerialID`,
`RoundNo`,
`GameType`,
`WagerDetail`,
`GameCode`,
`Result`,
`ResultType`,
`Card`,
`BetAmount`,
`Origin`,
`Payoff`,
`Currency`,
`ExchangeRate`,
`Commissionable`,
`created_at`,
`updated_at`
)
values(
_AccName,
_UserName,
_WagersID,
_WagerDate,
_SerialID,
_RoundNo,
_GameType,
_WagerDetail,
_GameCode,
_Result,
_ResultType,
_Card,
_BetAmount,
_Origin,
_Payoff,
_Currency,
_ExchangeRate,
_Commissionable,
_created_at,
_updated_at
)ON DUPLICATE KEY UPDATE
`AccName` = _AccName,
`UserName` = _UserName,
`WagersID` = _WagersID, 
`WagerDate` = _WagerDate,
`SerialID` = _SerialID,
`RoundNo` = _RoundNo,
`GameType` = _GameType,
`WagerDetail` = _WagerDetail,
`GameCode` = _GameCode,
`Result` = _Result,
`ResultType` = _ResultType,
`Card` = _Card,
`BetAmount` = _BetAmount,
`Origin` = _Origin,
`Payoff` = _Payoff,
`Currency` = _Currency,
`ExchangeRate` = _ExchangeRate,
`Commissionable` = _Commissionable,
`updated_at` = _updated_at;

END
";

		DB::unprepared($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP PROCEDURE `insert_bbn`');
    }
}
