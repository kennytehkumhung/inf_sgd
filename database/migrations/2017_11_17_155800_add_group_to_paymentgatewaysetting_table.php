<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddGroupToPaymentgatewaysettingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('paymentgatewaysetting', function (Blueprint $table) {
            $table->string('group_code', 128)->after('real_name');
            $table->string('group_name', 50)->after('group_code');
            $table->tinyInteger('notify_instant_trans')->default(0)->after('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('paymentgatewaysetting', function (Blueprint $table) {
            //
        });
    }
}
