<?php
//////////////////////////////////////////////////////////////////////
//
//  All Rights Reserved (c) 2010
//
//////////////////////////////////////////////////////////////////////
/**
* @author   Tan Leng Chian <tanlc@proseniit.com>
* @version  1.0
* @package  config
*/

/**
* All the login type into the system.
*
*/
define('CBO_LOGINTYPE_USER', 							1);
define('CBO_LOGINTYPE_AFFILIATE', 						2);
define('CBO_LOGINTYPE_ADMIN', 							3);

/**
* All market type
*
*/
define('CBO_BETTYPE_ASIAHANDICAP', 						1);
define('CBO_BETTYPE_ASIAHANDICAPHT',					2);
define('CBO_BETTYPE_OVERUNDER',							3);
define('CBO_BETTYPE_OVERUNDERHT',						4);
define('CBO_BETTYPE_1X2',								5);
define('CBO_BETTYPE_1X2HT',								6);
define('CBO_BETTYPE_ODDEVEN',							7);
define('CBO_BETTYPE_ODDEVENHT',							8);
define('CBO_BETTYPE_CORRECTSCORE',						9);
define('CBO_BETTYPE_CORRECTSCOREHT',					10);
define('CBO_BETTYPE_TOTALGOAL',							11);
define('CBO_BETTYPE_FIRSTGOALLASTGOAL',					12);
define('CBO_BETTYPE_HALFTIMEFULLTIME',					13);
define('CBO_BETTYPE_MONEYLINE',							14);
define('CBO_BETTYPE_OUTRIGHT',							15);
define('CBO_BETTYPE_MIXPARLAY',							16);
define('CBO_BETTYPE_ASIAHANDICAPRB',					17);
define('CBO_BETTYPE_ASIAHANDICAPHTRB',					18);
define('CBO_BETTYPE_OVERUNDERRB',						19);
define('CBO_BETTYPE_OVERUNDERHTRB',						20);
define('CBO_BETTYPE_ONEXTWORB',							21);
define('CBO_BETTYPE_ONEXTWOHTRB',						22);

define('CBO_BETSLIPRESULT_PENDING',	 					0);
define('CBO_BETSLIPRESULT_WIN',	 						1);
define('CBO_BETSLIPRESULT_WINHALF',	 					2);
define('CBO_BETSLIPRESULT_DRAW',	 					3);
define('CBO_BETSLIPRESULT_LOSS',	 					4);
define('CBO_BETSLIPRESULT_LOSSHALF',	 				5);

/**
* All the login error code.
*
*/
define('CBO_LOGINERROR_BANNEDIP',						1);
define('CBO_LOGINERROR_INVALIDNEXTLOGIN',				2);
define('CBO_LOGINERROR_ACCOUNTEXPIRED',					3);
define('CBO_LOGINERROR_ACCOUNTSUSPENDED',				4);
define('CBO_LOGINERROR_WRONGPASSWORD',					5);
define('CBO_LOGINERROR_ACCOUNTNOTFOUND',				6);

/**
* Object log action
*
*/
define('CBO_OBJECTLOG_INSERT',							1);
define('CBO_OBJECTLOG_UPDATE',							2);
define('CBO_OBJECTLOG_DELETE',							3);

/**
* All reference object for cash ledger
*
*/
define('CBO_LEDREFOBJ_BETSLIP', 			 			1);
define('CBO_LEDREFOBJ_BANKTRANSFER', 			 		2);
define('CBO_LEDREFOBJ_ADJUSTMENT', 			 			3);
define('CBO_LEDREFOBJ_CHARGEFEE', 			 			4);
define('CBO_LEDREFOBJ_CASHLEDGER', 			 			5);
define('CBO_LEDREFOBJ_PROMOCASH', 			 			6);
define('CBO_LEDREFOBJ_BALANCEFORWARD', 		 			7);

/**
* All transaction code use in account ledger/statement
*
*/
define('CBO_CHARTCODE_DEPOSIT', 			 			1);
define('CBO_CHARTCODE_WITHDRAWAL', 			 			2);
define('CBO_CHARTCODE_BONUS', 			 				3);
define('CBO_CHARTCODE_ADJUSTMENT', 			 			4);
define('CBO_CHARTCODE_GAMETRANS', 			 			5);
define('CBO_CHARTCODE_GAMEPAYOUT', 			 			6);
define('CBO_CHARTCODE_CANCELMATCH', 			 		7);
define('CBO_CHARTCODE_CANCELWAGER', 			 		8);
define('CBO_CHARTCODE_INCENTIVE', 			 			9);
define('CBO_CHARTCODE_BALANCEFORWARD', 					10);
define('CBO_CHARTCODE_BALANCETRANSFER', 				11);
define('CBO_CHARTCODE_EVENTPAYMENT', 					12);
define('CBO_CHARTCODE_CASHBACK', 						13);
define('CBO_CHARTCODE_REVERT', 							14);
define('CBO_CHARTCODE_WITHDRAWALCHARGES', 				15);
define('CBO_CHARTCODE_REFERRALREBATE', 					16);
define('CBO_CHARTCODE_CASHCARD', 						17);
define('CBO_CHARTCODE_REVERTBONUS', 					18);
define('CBO_CHARTCODE_DAILYREBATE', 					19);

/**
* All the transaction status used in account ledger/statement
*
*/
define('CBO_LEDGERSTATUS_PENDING', 						0);
define('CBO_LEDGERSTATUS_CONFIRMED', 					1);
define('CBO_LEDGERSTATUS_CANCELLED',	 				2);
define('CBO_LEDGERSTATUS_MANPENDING', 					3);
define('CBO_LEDGERSTATUS_MANCONFIRMED', 				4);
define('CBO_LEDGERSTATUS_MANCANCELLED', 				5);
define('CBO_LEDGERSTATUS_MEMCANCELLED', 				6);
define('CBO_LEDGERSTATUS_PROCESSED', 					7);


define('CBO_HOUR_SEC', 									3600);
define('CBO_DAY_SEC', 									86400);
define('CBO_MAX_INTERVAL', 								12);
define('CBO_DEFAULT_INTERVAL', 							1);

//All the betslip type
define('CBO_BETSLIPTYPE_NORMAL',	 					1);
define('CBO_BETSLIPTYPE_BUYBACK',	 					2);

/**
*	Betslip TABLE type
*
*/
define('CBO_BETSLIP_EGZ', 								'egz');
define('CBO_BETSLIP_LGZ', 								'lgz');
define('CBO_BETSLIP_MPZ', 								'mpz');


//All the betslip term
define('CBO_BETSLIPTERM_CASH',	 						1);
define('CBO_BETSLIPTERM_CREDIT',	 						2);
define('CBO_BETSLIPTERM_PROMOCASH',	 					3);

//All the betslip channel
define('CBO_BETSLIPCHANNEL_INTERNET',	 				1);
define('CBO_BETSLIPCHANNEL_PHONE',	 					2);

//All the betslip status
define('CBO_BETSLIPSTATUS_UNSETTLED',	 				0);
define('CBO_BETSLIPSTATUS_SETTLED',	 					1);
define('CBO_BETSLIPSTATUS_CANCELLEDBYADMIN',	 			2);
define('CBO_BETSLIPSTATUS_CANCELLEDMATCH',	 			3);
define('CBO_BETSLIPSTATUS_FRAUDCASE',	 				4);
define('CBO_BETSLIPSTATUS_PENDING',	 					5);

/**
* Standard status to use for all object that has only active and suspended status
*
*/
define('CBO_STANDARDSTATUS_ACTIVE', 						1);
define('CBO_STANDARDSTATUS_SUSPENDED', 					0);

/**
* 	Admin user status
*
*/
define('CBO_ADMINSTATUS_SUSPENDED', 						0);
define('CBO_ADMINSTATUS_ACTIVE', 						1);
define('CBO_ADMINSTATUS_LOGINBLOCKED',					4);

/**
* 	Agent user status
*
*/
define('CBO_AFFILIATESTATUS_SUSPENDED', 					0);
define('CBO_AFFILIATESTATUS_ACTIVE', 					1);
define('CBO_AFFILIATESTATUS_LOGINBLOCKED',				4);
define('CBO_AFFILIATESTATUS_PENDINGACTIVATION',			2);
define('CBO_AFFILIATESTATUS_REMOVE',                     5);

/**
* 	Member user account status
*
*/
define('CBO_ACCOUNTSTATUS_SUSPENDED', 					0);
define('CBO_ACCOUNTSTATUS_ACTIVE', 						1);
define('CBO_ACCOUNTSTATUS_PENDINGACTIVATION',			2);
define('CBO_ACCOUNTSTATUS_FRAUDCASE',					3);
define('CBO_ACCOUNTSTATUS_LOGINBLOCKED',					4);
define('CBO_ACCOUNTSTATUS_EXPIRED',						5);
define('CBO_ACCOUNTSTATUS_PAUSEGAMING',					6);

/**
*	Member user account identification status
*
*/
define('CBO_ACCOUNTIDENTITY_PROVIDED', 					1);
define('CBO_ACCOUNTIDENTITY_NOPROVIDED',					0);

/**
* 	Member user account betting terms
*
*/
define('CBO_ACCOUNTTERM_CASH',	 						0x001);
define('CBO_ACCOUNTTERM_CREDIT',	 						0x002);

/**
* 	Member user account betting terms
*
*/
define('CBO_ACCOUNTGENDER_MALE',	 						1);
define('CBO_ACCOUNTGENDER_FEMALE',	 					2);

/**
* 	Member user account registration mode
*
*/
define('CBO_ACCOUNTREGMODE_WEB', 						1);
define('CBO_ACCOUNTREGMODE_PHONE', 						2);
define('CBO_ACCOUNTREGMODE_OTHER', 						3);

/**
* 	Promo Cash Status
*
*/
define('CBO_PROMOCASHSTATUS_PENDING', 					0);
define('CBO_PROMOCASHSTATUS_CONFIRMED', 					1);
define('CBO_PROMOCASHSTATUS_REJECTED',					2);
define('CBO_PROMOCASHSTATUS_CREDITED',					3);

/**
* 	Promo Campaign Status
*
*/
define('CBO_PROMOCAMPAIGNSTATUS_SUSPENDED', 				0);
define('CBO_PROMOCAMPAIGNSTATUS_ACTIVE', 				1);

/**
* 	Adjustment Status
*
*/
define('CBO_ADJUSTMENTSTATUS_PENDING', 					0);
define('CBO_ADJUSTMENTSTATUS_CONFIRMED', 				1);
define('CBO_ADJUSTMENTSTATUS_REJECTED',					2);

/**
* 	All Bets' Mode
*
*/
define('CBO_ALLBETMODE_LINEAR', 							1);
define('CBO_ALLBETMODE_FILTER', 							2);


/**
* 	All Adjustment Method 
*
*/
define('CBO_ADJUSTMENTMETHOD_FIXEDAMOUNT', 				1);
define('CBO_ADJUSTMENTMETHOD_RANGE', 					2);

/**
* 	All Adjustment TYPE 
*
*/
define('CBO_ADJUSTMENTTYPE_CREDIT', 						1);
define('CBO_ADJUSTMENTTYPE_DEBIT', 						2);

/**
* 	All Promo Cash METHOD
*
*/
define('CBO_PROMOCASHMETHOD_FIXEDAMOUNT', 				1);
define('CBO_PROMOCASHMETHOD_PERCENTAGE',	 				2);
define('CBO_PROMOCASHMETHOD_RANGE',						3);

/**
* 	All Promo Cash TYPE 
*
*/
define('CBO_PROMOCASHTYPE_CREDIT', 						1);
define('CBO_PROMOCASHTYPE_DEBIT', 						2);

/**
* 	Cash Ledger checking error code constant
*
*/
define('CBO_LEDGERERROR_CURRENCYSUSPENDED', 				1);

/**
* All IP mode
*
*/
define('CBO_MODE_ALLOWIP',								1);
define('CBO_MODE_BANNEDIP',								2);
define('CBO_MODE_EXCLUDEIP',								3);

/**
* All admin summary report
*
*/
define('CBO_ADMINSUMMARY_ANNOUNCEMENT',					1);
define('CBO_ADMINSUMMARY_USERONLINE',					2);
define('CBO_ADMINSUMMARY_MARKET',						3);
define('CBO_ADMINSUMMARY_CUSTOMERSUPPORT',				4);
define('CBO_ADMINSUMMARY_ACTIVITY',						5);


/**
* IP tracker type.
*
*/
define('CBO_IPTRACKERTYPE_ACCOUNT', 						1);
define('CBO_IPTRACKERTYPE_ACCESS', 						2);

/**
* Banned IP Status
*
*/
define('CBO_BANNEDIPSTATUS_SUSPENDED', 			 		0);
define('CBO_BANNEDIPSTATUS_ACTIVE', 						1);

/**
* Tag Status
*
*/
define('CBO_TAGSTATUS_NOTACTIVE',			 		0);
define('CBO_TAGSTATUS_ACTIVE', 						1);

/**
* All types of price adjustment
*
*/
define('CBO_ADJUSTMENTTYPE_PERCENT', 			 		1);
define('CBO_ADJUSTMENTTYPE_CENT', 						2);

/**
* Commission Report
*
*/
define('CBO_MINCOMMISSION', 								100);
define('CBO_ROLLOVERYES',								1);
define('CBO_ROLLOVERNO', 								0);
define('CBO_COMMISSIONPROFITENDINF', 					9999999999);
define('CBO_COMMISSIONMEMBERENDINF', 					999999);

/**
* 	Bank
*
*/
define('CBO_BANKTYPE_HQ', 								1);
define('CBO_BANKTYPE_BRANCH', 							2);

/**
* 	Product Category Commission Type
*
*/
define('CBO_PRODCATCOMMTYPE_TURNOVER', 					1);
define('CBO_PRODCATCOMMTYPE_INCENTIVE', 					2);

/**
* 	Negative Balance Bear type
*
*/
define('CBO_COMPANY_BEAR', 								1);
define('CBO_USER_BEAR', 									2);

/**
*	Negative Balance Status
*/
define('CBO_NGB_STATUS_UNSOLVED',	 					0);
define('CBO_NGB_STATUS_INPROGRESS',						1);
define('CBO_NGB_STATUS_RESOLVED',	 					2);
define('CBO_NGB_STATUS_CLOSED',		 					3);


/**
* 	Agent register status
*
*/
define('CBO_AFFILIATEREGISTERSTATUS_PENDING', 					0);
define('CBO_AFFILIATEREGISTERSTATUS_APPROVE', 					1);
define('CBO_AFFILIATEREGISTERSTATUS_REJECT',				2);


/**
*	Special tag 
*/
define('CBO_SPECIALTAG_NO', 					0);
define('CBO_SPECIALTAG_YES', 						1);

?>
