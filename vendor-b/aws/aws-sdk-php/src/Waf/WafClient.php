<?php
namespace Aws\Waf;

use Aws\AwsClient;

/**
 * This client is used to interact with the **AWS WAF** service.
 */
class WafClient extends AwsClient {}
