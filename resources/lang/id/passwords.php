<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Password Reminder Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are the default lines which match reasons
	| that are given by the password broker for a password update attempt
	| has failed, such as for an invalid token or invalid new password.
	|
	*/

	"password" 	=> "Kata Sandi harus minimal enam karakter dan cocok konfirmasi.",
	"user" 		=> "Kami tidak dapat menemukan pengguna dengan alamat e-mail.",
	"token" 	=> "Ini tanda reset password tidak sah.",
	"sent" 		=> "Kami telah mengirim e-mail link untuk mereset kata sandi baru!",
	"reset" 	=> "Kata Sandi Anda telah kami perbaruhui!",
	"failed" 	=> "Nama Pengguna tidak sah!",

];
