<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines contain the default error messages used by
	| the validator class. Some of these rules have multiple versions such
	| as the size rules. Feel free to tweak each of these messages here.
	|
	*/

	"accepted"             => "The :attribute harus diterima.",
	"active_url"           => "The :attribute bukan URL yang sah.",
	"after"                => "The :attribute harus menjadi tanggal setelah :Tanggal.",
	"alpha"                => "The :attribute hanya berisi huruf.",
	"alpha_dash"           => "The :attribute hanya dapat berisi huruf, angka, dan tanda hubung.",
	"alpha_num"            => "The :attribute hanya dapat berisi huruf dan angka.",
	"array"                => "The :attribute harus disusun.",
	"before"               => "The :attribute harus menjadi tanggal setelah :Tanggal.",
	"between"              => [
		"numeric" 		   => "The :attribute harus diantara :minimum dan :maksimum.",
		"file"    			 => "The :attribute harus diantara :minimum dan :maksimum kilobytes.",
		"string"  			 => "The :attribute harus diantara :minimum dan :maksimum karakter.",
		"array"   			 => "The :attribute harus diantara :minimum dan :maksimum barang.",
	],                    
	"boolean"              => "The :attribute bidang harus benar atau salah.",
	"confirmed"            => "The :attribute Konfirmasi tidak sesuai.",
	"date"                 => "The :attribute tanggal tidak sah.",
	"date_format"          => "The :attribute tidak sesuai format :format.",
	"different"            => "The :attribute dan :lainnya harus berbeda.",
	"digits"               => "The :attribute harus :digit digit.",
	"digits_between"       => "The :attribute harus diantara :min dan :max digit.",
	"email"                => "The :attribute alamat email harus sah.",
	"filled"               => "The :attribute kosong harus di lengkapi.",
	"exists"               => "The selected :attribute tidak sah.",
	"image"                => "The :attribute harus diikuti gambar.",
	"in"                   => "The selected :attribute tidak sah.",
	"integer"              => "The :attribute harus menggunakan bilangan bulat.",
	"ip"                   => "The :attribute harus menjadi alamat IP yang sah.",
	"max"                  => [
		"numeric" 			 => "The :attribute tidak boleh lebih :besar.",
		"file"    			 => "The :attribute tidak mungkin lebih besar dari :maksimum kilobytes.",
		"string"  			 => "The :attribute tidak mungkin lebih besar dari :maksimum Karakter.",
		"array"   			 => "The :attribute tidak mungkin memiliki lebih dari :maksimum barang.",
	],			          
	"mimes"       			 => "The :attribute harus bisa menjadi : :nilai-nilai.",
	"min"         			 => [
		"numeric" 			 => "The :attribute harus minimal :min.",
		"file"    			 => "The :attribute harus minimal :minimum kilobytes.",
		"string"  			 => "The :attribute harus minimal :minimum Karakter.",
		"array"   			 => "The :attribute harus minimal :minimum barang.",
	],                    
	"not_in"               => "The selected :attribute tidak sah.",
	"numeric"              => "The :attribute harus berupa angka.",
	"regex"                => "The :attribute format tidak sah.",
	"required"             => "The :attribute kosong diperlukan.",
	"required_if"          => "The :attribute kosong diperlukan saat :lainnya adalah :nilai.",
	"required_with"        => "The :attribute kosong diperlukan saat :nilai hadir.",
	"required_with_all"    => "The :attribute kosong diperlukan saat :nilai hadir.",
	"required_without"     => "The :attribute kosong diperlukan saat :nilai tidak hadir.",
	"required_without_all" => "The :attribute kosong diperlukan saat ketika tidak ada :nilai hadir.",
	"same"                 => "The :attribute dan :lainnya harus sesuai.",
	"size"                 => [
		"numeric" 			 => "The :attribute harus :ukuran.",
		"file"    			 => "The :attribute harus :ukuran kilobytes.",
		"string"  			 => "The :attribute harus :ukuran karakter.",
		"array"   			 => "The :attribute harus terisi :Ukuran barang.",
	],                    
	"unique"               => "The :attribute sudah diambil.",
	"url"                  => "The :attribute format tidak sah.",
	"timezone"             => "The :attribute harus menjadi zona yang sah.",
	"reserved_str"         => "Invalid :attribute.",
	"username_exist"       => ":attribute Ada.",
	"email_exist"     	   => ":attribute Ada.",
	"phone"     	  	   => "Nomor telepon tidak benar",
	"newpass_same"     	   => "Kata sandi baru sama dengan kata sandi lama",
	"oldpass_wrong"        => "kata sandi lama salah",
	"newandconpass_notsame"   => "New Password and Confirm New Password does not match",
	"wrong_captcha"        => "Kode Captcha Salah",
	"login_fail"     	   => "Masuk gagal!",
	"wallet_error1"        => "Dompet tidak bisa sama!",
	"wallet_error2"        => "Saldo tidak cukup!",
	"decimal"      	       => "Format benar, hanya 2 digit titik desimal diterima!",

	/*
	|--------------------------------------------------------------------------
	| Custom Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| Here you may specify custom validation messages for attributes using the
	| convention "attribute.rule" to name the lines. This makes it quick to
	| specify a specific custom language line for a given attribute rule.
	|
	*/

	'custom' => [
		'attribute-name' => [
			'rule-name' => 'custom-message',
		],	
		'rules' => [
			"required"      => "Silakan membaca dan memahami Deposit Promo Aturan!",
		],
	],

	/*
	|--------------------------------------------------------------------------
	| Custom Validation Attributes
	|--------------------------------------------------------------------------
	|
	| The following language lines are used to swap attribute place-holders
	| with something more reader friendly such as E-Mail Address instead
	| of "email". This simply helps us make messages a little cleaner.
	|
	*/

	'attributes' => [],

];
