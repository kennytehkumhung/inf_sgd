<?php

return [

	"AboutUs"            			 => "Tentang Kami",
	"AccountCreated"            	 => "Nama Pengguna berhasil didaftarkan ! Silahkan lanjut Masuk.",
	"AccountDetails"            	 => "Rincian Nama Pengguna",
	"AccountHolder"            		 => "Nama Pemegang Rekening",
	"AccountName"            		 => "Nama Rekening",
	"AccountNumber"            		 => "Nomor Rekening",
	"AccountUpdateFailed"            => "*Pembaharuan info Nama Pengguna berhasil",
	"Adjustment"            		 => "Penyesuaian",
	"AgreementStatementRules"        => "Pernyataan Aturan Kesepakatan",
	"All"            				 => "Semua",
	"AllMembersOnceADay"             => "Semua anggota diperbolehkan menarik dana 1x per hari",
	"Amount"            		 	 => "Jumlah",
	"AmountCannotBeLessThanBalance"  => "*Dana tidak bisa melebihi saldo",
	"AmountMYR"            			 => "Jumlah (IDR)",
	"AmountRange"            		 => "Silahkan masukkan Jumlah Deposit dengan Maksimum 1,000,000,000 dan Minimum 100,000.",
	"AmountRange2"            		 => "Silahkan masukkan Jumlah Penarikan dengan Maksimum 1,000,000,000 dan Minimum 250,000.",
	"Answer"            			 => "Jawaban",
	"April"            				 => "April",
	"ATMBanking"            		 => "Perbankan ATM",
	"August"            			 => "Agustus",
	"AvailableBalance"            	 => "Saldo Tersedia",
	"Baccarat"            			 => "Baccarat",
	"Back"            				 => "Info Perbankan",
	"BackToLobby"            		 => "Kembali ke Lobi",
	"Balance"            			 => "Saldo",
	"Bank"            				 => "Bank",
	"BankAccount"            		 => "Rekening Bank",
	"BankAccountName"            	 => "Nama Rekening Bank",
	"BankAccountNo"            		 => "Nomor Rekening Bank",
	"BankBranch"            		 => "Cabang Bank",
	"BankDetails"            		 => "Rincian Bank",
	"BankDetailsContent"             => "Silahkan hubungi petugas layanan pelanggan kami jika Anda perlu memperbarui informasi yang bersangkutan.",
	"Banking"            		 	 => "Perbankan",
	"BankingOptions"            	 => "Perbankan",
	"BankMaxExceeded"            	 => "*Melampaui batas kriteria deposit maksimum bank",
	"BankMinDepositCriteria"         => "*Kriteria deposit minimal bank tidak dipenuhi.",
	"BankName"            			 => "Nama Bank",
	"BankTransfer"            		 => "Transfer Bank",
	"BankTransferNote1"            	 => "Minimum Penarikan adalah IDR 60000 dan Maximum Penarikan adalah IDR 150 Juta.",
	"BankTransferNote2"            	 => "Semua anggota diperbolehkan menarik dana 1x per hari",
	"Blackjack"            			 => "Blackjack",
	"Bonus"            				 => "Bonus",
	"BonusContent"            		 => "Silakan mengklaim Bonus Anda dalam waktu 14 hari, jika tidak maka akan batal.",
	"BonusMinimumCriteria"           => "*Kriteria minimal deposit bonus tidak dipenuhi",
	"Cancel"            			 => "Batal",
	"CaribbeanPoker"            	 => "Caribbean Poker",
	"CasinoWallet"            		 => "Dompet Kasino",
	"cfd"            				 => "Investasi CFD",
	"ChangePassword"            	 => "Ganti Kata Laluan",
	"ChangePasswordNote"             => "Catatan: Gunakan hanya huruf dan nomor saja",
	"ChangePasswordSuccess"          => "Ganti Password Berhasil",
	"ClubSuite"            			 => "Club Suite",
	"CockFighting"				     =>	"Cock Fighting",
	"Code"            				 => "Kode",
	"ConfirmNewPassword"             => "Konfirmasi Kata Sandi Baru",
	"ConfirmPassword"            	 => "Konfirmasi Kata Sandi",
	"ContactNumber"            		 => "Nomor Handphone",
	"ContactUs"            		 	 => "Hubungi Kami",
	"Credit"            			 => "Kredit",
	"CreditLimit"            		 => "Batas Kredit",
	"CreditManagement"            	 => "Nama",
	"Currency"            			 => "Mata Uang",
	"Currentbalance"            	 => "Saldo Sekarang",
	"CurrentPassword"            	 => "Kata Sandi Sekarang",
	"4d"            				 => "Togel 4D",
	"DateFrom"            			 => "Dari Tanggal",
	"DateOrTime"            		 => "Tanggal/Waktu",
	"DateRange"            			 => "Rentang Tanggal",
	"DateTime"            			 => "Tanggal/Waktu",
	"DateTimeContent"            	 => "Silahkan berikan Tanggal / Waktu deposit yang telah Anda buat.",
	"Dealer"            			 => "Bandar",
	"Debit"            				 => "Debet",
	"December"            			 => "Desember",
	"Deposit"            		 	 => "Deposit",
	"DepositBank"            		 => "Bank Deposit",
	"DepositChannel"            	 => "Saluran Deposit",
	"DepositDetails"            	 => "Rincian Deposit",
	"DepositFund"            		 => "Deposit <br /> Dana",
	"DepositHelp"            		 => "Bantuan Deposit",
	"DepositMethod"            		 => "Metode Deposit",
	"DepositMethodContent"           => "Silahkan pilih metode deposit yang telah Anda buat.",
	"DepositNow"            		 => "Deposit <br /> Sekarang",
	"DepositPromoRules"            	 => "Aturan Promosi Deposit",
	"DepositPromotion"            	 => "Promosi Deposit",
	"DepositReceipt"            	 => "Bukti Transfer",
	"DepositReceiptPolicy"           => "Ukuran file kurang dari 1 MB dan tipe file .jpg/.jpeg/.bmp/.png saja",
	"DepositSelection"            	 => "Silahkan pilih bank yang Anda buat deposit ke.",
	"DepositSteps1"            		 => "Langkah 1: Silakan melakukan deposit ke salah satu rekening bank berikut:",
    "DepositSteps1b"                 =>	"Langkah 1: Hubungi Customer Service kami untuk mendapatkan informasi bank sebelum melanjutkan dengan deposit. Anda bisa transfer dana ke rekening tersebut setelah diberikan.",
	"DepositSteps2"            		 => "Langkah 2: Silahkan mengisi rincian deposito:",
	"DepositWithdrawal"            	 => "Deposit/Penarikan",
	"DiamondSuite"            		 => "Diamond Suite",
	"DifficultyContact"            	 => "Jika Anda mengalami kesulitan memasuk rincian di atas, silakan Hubungi Kami.",
	"Disagree"            			 => "Tidak Setuju",
	"DOB"            				 => "Tanggal Lahir",
	"DoYouWantToTransfer"            => "Anda masih ada saldo di dompet lain. Apakah Anda ingin mentransfer ke Dompet Utama?",
	"DragonTiger"            		 => "Dragon Tiger",
	"Egames"            			 => "E-Permainan",
	"Email"            				 => "Email",
	"EmailAddress"            		 => "Alamat Email",
	"EmailReminder"            		 => "Harap berikan Alamat Email yang Benar untuk korespondensi pemberitahuan dan pembayaran di masa depan.",
	"EnterContactNumber"             => "Silahkan masukkan Nomor Handphone Anda.",
	"EnterEmailAddress"            	 => "Silahkan masukkan Alamat Email Anda yang benar.",
	"EnterFullName"            		 => "Silahkan masukkan Nama Lengkap Anda.",
	"EnterPassword"            		 => "Masukkan Kata Sandi Anda.",
	"EnterUserName"            		 => "Silahkan masukkan Nama Pengguna yang diinginkan.",
	"ForgotPassword"				 =>	"lupa kata sandi?",
	"FAQ"            				 => "Tanya Jawab",
	"Featured"            			 => "Fitur",
	"February"            			 => "Februari",
	"Fee"            				 => "Biaya",
	"Female"            			 => "Perempuan",
	"FileTooHuge"            		 => "*File Terlalu Besar",
	"FundTransferSuccessful"         => "*Transfer Dana Berhasil",
	"Games"            				 => "Permainan",
	"Gender"            			 => "Jenis Kelamin",
	"GeneralTNC"            		 => "Syarat & Ketentuan Umum",
	"HeartSuite"            		 => "Heart Suite",
	"Help"            				 => "Bantuan",
	"Horse"            				 => "Pacuan Kuda",
	"HowToJoin"            			 => "Bagaimana Mendaftar",
	"HowToLoginDeposit"            	 => "Cara Masuk & Mendeposit",
	"HowToRegister"            		 => "Cara Mendaftar",
	"IAlreadyUnderstand"             => "Saya sudah mengerti",
	"IAlreadyUnderstandRules"        => "Saya mengerti Aturan Promosi Deposit",
	"ID"            				 => "ID",
	"IDoNotAccept"            		 => "Saya tidak menerima.",
	"IDoNotWantPromotion"            => "Tanpa Promo",
	"image_android"            		 => '<img src="/img/mobiledownload/android.png" alt=""/>',
	"image_android_livecasino"       => '<img src="/img/mobiledownload/android_lc_bm.png" alt=""/>',
	"image_android_slot"             => '<img src="/img/mobiledownload/android_sl.png" alt=""/>',
	"image_ios_livecasino"           => '<img src="/img/mobiledownload/ap_lc_bm.png" alt=""/>',
	"image_mobile_ag"            	 => '<img src="/img/mobiledownload/ag-casino-bm.png"/>',
	"image_mobile_click"             => '<img src="/img/mobiledownload/click-bm.png"/>',
	"image_mobile_maxbet"            => '<img src="/img/mobiledownload/maxbet-casino-bm.png"/>',
	"image_mobile_playtech"          => '<img src="/img/mobiledownload/pt-casino-bm.png"/>',
	"image_mobile_w88"            	 => '<img src="/img/mobiledownload/w88-casino-bm.png"/>',
	"image_mobiledownload"           => '<img src="/img/mobiledownload/mobiledownload_bm.png" alt=""/>',
	"img_mobiledownloadbar"          => '/img/mobiledownload/intab-bm.png',
	"Important"            			 => "Penting",
	"ImportantNote1"            	 => "Transaksi Penarikan tidak akan berhasil jika Nama Rekening Bank Anda berbeda dari Nama Lengkap Anda yang didaftarkan di infiniwin.",
	"ImportantNote2"             	 => "Anda diperbolehkan untuk menggunakan satu rekening bank dalam penarikan dana Anda. Perubahan rekening bank akan mengakibatkan transaksi ditolak. Untuk info lanjut, silahkan hubungi Customer Service.",
	"IncentiveRebate"            	 => "Rabat Insentif",
	"InfiBankingInfo"            	 => "Infomasi Perbankan Infiniwin",
	"InfiBankName"            		 => "Nama Rekening Bank Infiniwin",
	"InfiBankNote"            		 => "Catatan: Kami barangkali memperbarui Rekening Pembayaran Bank dari waktu ke waktu. Mohon periksa rincian Perbankan kami sebelum melakukan Deposit apapun untuk menghindari komplikasi dalam proses.",
	"InternetBanking"            	 => "Perbankan Internet",
	"InvalidCode"            		 => "*Kode Salah",
	"InvalidDateRange"            	 => "*Salah rentang tanggal",
	"InvalidEmail"            		 => "*Email Salah",
	"InvalidFileType"            	 => "*Tipe File Salah",
	"InvalidFormat"            		 => "*Salah format",
	"InvalidGameType"            	 => "Tipe Permainan Gagal",
	"InvalidUsername"            	 => "*Nama Pengguna Salah",
	"January"            		     => "Januari",
	"JoinNow"            		     => "Daftar",
	"July"            		 	     => "Juli",
	"June"            		         => "Juni",
	"Last5Trans"            		 => "5 Transaksi Terbaru",
	"LiveCasino"            		 => "Live Kasino",
	"LiveChat"            			 => "24/7 Live Chat",
	"Loading"            			 => "Loading",
	"LocalBankTransfer"            	 => "Transfer Bank Lokal",
	"Login"            		 		 => "Masuk",
	"LoginFailed"            		 => "Masuk Gagal",
	"LoginSomewhereElse"             => "Orang lain login di tempat lain",
	"Logout"            		 	 => "Keluar",
	"LogoutSuccess"            		 => "Sukses Keluar",
	"Maintenance"            		 => "Pemeliharaan",
	"MainWallet"            		 => "Dompet Utama",
	"Male"            		 		 => "Lelaki",
	"March"            		 		 => "Maret",
	"MarsClub"            			 => "Mars Club & Diamond slot",
	"Max"            				 => "Max",
	"MaxDeposit"            		 => "Maksimum Deposit",
	"Maximum"            			 => "Maksimum",
	"May"            		 		 => "Mei",
	"MemberWithdrawal"            	 => "Penarikan Member",
	"Min"            		 		 => "Min",
	"MinDeposit"            		 => "Minimum Deposit",
	"Mini"            		 		 => "Mini",
	"Minimum"            			 => "Minimum",
	"MinMaxLimit"            		 => "Batas Min/Max",
	"mobile_username"            	 => "Silakan gunakan di bawah Nama Pengguna untuk masuk Mobile Applikasi",
	"MobileNumber"            		 => "Nomor Telefon",
	"MobilePhone"            		 => "Telefon",
	"MoreInfo"            			 => "Infomasi Lain",
	"MustBeLessThanEqual"            => "*Harus kurang atau sama dengan saldo Dompet Utama",
	"MustBeTheSameAsAbove"           => "*Harus sama dengan diatas",
	"MyAccount"            		 	 => "Data Pribadi",
	"NameCaution"            		 => "Nama mesti sesuai dengan nama pemilik rekening bank untuk urusan transaksi bank : Deposit/Penarikan Dana.",
	"NameMatchWithrawalBank"         => "*Nama mestilah bersamaan dengan nama Rekening Bank anda.",
	"nav_depositnow"                 => '<img src="/img/nav/ID/depositNow.jpg"/>',
	"nav_login"            		     => '<img id="login_img" alt="infiniwin" src="/img/nav/ID/login_now.png"/>',
	"nav_login_button"            	 => '/img/nav/EN/login_btn.jpg',
	"nav_register"            		 => '<img src="/img/nav/ID/btn_register.jpg"/>',
	"nav_register_button"            => "/img/nav/ID/register.png",
	"nav_spin2"            		 	 => '<img src="/img/left_bottom_banner.gif" style="width: 215px;height: 82px;"/>',
	"nav_tutorial"            		 => '<img src="/img/nav/ID/btn_tutorial.jpg"/>',
	"nav01"            		 		 => '<img src="/img/nav/ID/icon_home.png" />',
	"nav02"            		 		 => '<img src="/img/nav/ID/icon_casino.png"/>',
	"nav03"            		 		 => '<img src="/img/nav/ID/icon_sport.png"/>',
	"nav04"            		 		 => '<img src="/img/nav/ID/icon_slot.png"/>',
	"nav06"            		 		 => '<img src="/img/nav/ID/icon_horse_racing.png" style="margin: 2px 0px 0px -8px;">',
	"nav07"            		 		 => '<img src="/img/nav/ID/icon_promotion.png"/>',
	"nav08"            		 		 => '<img src="/img/nav/EN/icon_affiliate.png" style="width: 60px;margin: 8px 0px 0px -5px;"/>',
	"nav09"            		 		 => '<img src="/img/nav/EN/chickenfight.jpg"/>',
	"navjoinnow"            		 => '<img src="/img/join_now_btn.png" />',
	"navJoinNowbtn"            		 => '<div id="join_now_btn" class="join_now_btn" style="background-image:url("/img/nav/EN/joinnow.png");">',
	"navLivechat"            		 => '<img src="/img/live_chat_button.png" />',
	"navLivechatbanner"            	 => '<div style="position:fixed;z-index:9999;top:145px;right:3px;background-image:url(/img/floating_banner_id.png);width:117px; height:345px;">',
	"navMarsClub"            		 => '<img id="mars_club" src="/img/nav/ID/mars_club.png" />',
	"navPlatoclub"            		 => '<img id="pluto_club" src="/img/nav/ID/pluto_club.png" />',
	"navPlaytechClub"            	 => '<img id="playtech_club" src="/img/nav/ID/playtech_club.png" />',
	"navSaturnClub"            		 => '<img id="saturn_club" src="/img/nav/ID/saturn_club-coming-soon.png" />',
	"navspin"            		 	 => '<a href="#" onclick="window.open("/mem/spin.aspx','spin','width=1330,height=650,toolbar=0,resizable=0,scrollbars=0");"><div style="position:fixed;z-index:9999;top:620px;left:3px;background-image:url(/img/nav/EN/flood_infiniwheel.gif);width:165px; height:160px;"></div></a>',
	"navVenusClub"            		 => '<img id="venus_club" src="/img/nav/ID/venus_club.png" />',
	"NewMember"            			 => "Akun Anda Telah Terdaftar. Silahkan Login.",
	"NewPassword"            		 => "Kata Sandi Baru",
	"NewPasswordNote"            	 => "Kata Sandi harus 6-20 karakter; simbol tidak diterima.",
    "NewPasswordNote2"				 =>	"Masukkan kata sandi baru 6 sampai 15 kata-kata, symbol tak bisa dimasukkan.",
	"No"            		 		 => "Tidak.",
	"NoDataFound"            		 => "Data tidak ditemukan",
	"NoRecord"            		 	 => "Cacatan tidak ditemukan",
	"NoTableIsAvailableNow"          => "Tidak ada meja tersedia sekarang",
	"November"            		 	 => "November",
	"NumbersOnly"            		 => "Nomor saja",
	"October"            		 	 => "Oktober",
	"OldPassword"            		 => "Kata Sandi Lama",
	"OnlyNumbers"            		 => "*Angka sahaja",
	"OpenAccount"            		 => "Membuka Nama Pengguna",
	"Option"            		 	 => "Pilihan",
	"Option_withdraw"            	 => "Metode Pembayaran",
	"Password"            		 	 => "Kata Sandi",
	"PasswordPolicy"            	 => "Antara 6-12 alfanumerik (A-Z, a-z, 0-9) karakter saja.",
	"PasswordPolicyContent"          => "Ketentuan Kata Sandi:- minimal 6 karakter dan maksimum 20",
	"Payout"            		 	 => "Pembayaran",
	"PayoutTable"            		 => '<iframe id="iframe_payoutTable" src="payoutTable-ID.html" width="650px" height="660px"></iframe>',
	"PayoutTable1"            		 => '<iframe id="iframe_Tabel Pembayaran" src="Tabel Pembayaran-ID.html" width="650px" height="660px"></iframe>',
	"Pending"            		     => "Tertunda",
	"PersonalDetail"            	 => "Data Pribadi",
	"Phone"            				 => "Telepon",
	"PlayAndWin"            		 => "Main & Menang",
	"Player"            			 => "Pemain",
	"Players"            		 	 => "Pemain",
	"PlayNow"            			 => "Main Sekarang",
	"PleaseLogin"            		 => "Silahkan Masuk",
	"PleaseReadPromoRules"           => "*Silahkan baca dan fahami Aturan Promosi Deposit",
	"PleaseSelectBank"            	 => "*Silahkan pilih bank",
	"PlutoClub"            		 	 => "Pluto Club & Platinum slot",
	"Poker"            		 		 => "Poker",
	"PrivacyPolicy"            		 => "Kebijakan Privasi",
	"ProcessingTime"            	 => "Waktu Memproses",
	"Profile"            		 	 => "Data Pribadi",
	"Promo1"            		 	 => "Bonus Selamat Datang : Welcome Challenger Pack (Min Deposit hanya IDR 60000)",
	"Promo2"            		 	 => "Bonus Deposit Harian 20% (Min Deposit hanya IDR 60000)",
	"Promo3"            		 	 => "Beginner Pack :  Deposit IDR 120000 FREE IDR 66000 (Min Deposit hanya IDR 120000)",
	"Promo4"            		 	 => "Beginner Pack :  Deposit IDR 60000 FREE IDR 33000 (Min Deposit hanya IDR 60000)",
	"Promotion"            		 	 => "Promosi",
	"Reason"            		 	 => "Alasan",
	"ReceiptOptional"            	 => "Penerimaan (Opsional)",
	"RecordType"            		 => "Data Transaksi",
	"ReenterPassword"            	 => "Konfirmasi Password",
	"ReenterPasswordNote"            => "Silahkan masukkan Kata Sandi Anda lagi.",
	"ReferenceCode"            		 => "Kode Referensi",
	"ReferenceContent"            	 => "Dengan memberikan nomor referensi Anda akan mempercepat proses Deposit.",
	"ReferenceNo"            		 => "Nomor Referensi",
	"Register"            			 => "Daftar",
	"RegisterNow"            		 => "Daftar<br/>Sekarang",
	"Registration"            		 => "Pendaftaran",
	"Remark"            		 	 => "Komentar",
	"RepeatPassword"            	 => "Ulang Kata Sandi",
	"Required"            		 	 => "*Wajib diisi",
	"RequiredField"            		 => "Wajib Diisi",
	"RequiredFields"            	 => "Wajib diisi",
	"Reset"            		 		 => "Reset",
	"ResetPasswordFailed"            => "Reset Password gagal! Silahkan coba lagi nanti",
	"ResponsibleGaming"            	 => "Permainan Bertanggungjawab",
	"Roulette"            		 	 => "Roulette",
	"Save"            		 	 	 => "Simpan",
	"ScannedReceipt"            	 => "Penerimaan Dipindai",
	"ScannedReceiptContent"          => "Dengan memberikan Penerimaan Deposit Anda akan mempercepat proses Deposit.",
	"Search"            		 	 => "Cari",
	"SearchCriteria"            	 => "Kriteria Pencarian",
	"SecurityQuestion"            	 => "Pertanyaan Keamanan",
	"September"            			 => "September",
	"Sicbo"            		 		 => "Sic Bo",
	"SignOut"            		 	 => "Keluar",
	"SinglePlayerPoker"            	 => "Pemain Individu Poker",
	"Sitemap"            		 	 => "Peta Situs",
	"Skip"            		 		 => "Abaikan",
	"Skype"            		 		 => "Skype",
	"SlotGames"            		 	 => "Permainan Slot",
	"Slots"            		 		 => "Permainan Slot",
	"SpadeSuite"            		 => "Spade Suite",
	"SportbookWallet"            	 => "Dompet Sportsbook",
	"Sports"            			 => "Sport",
	"SportsBook"            		 => "Taruhan Olahraga",
	"STATEMENT"            			 => "Pernyataan",
	"Status"            		 	 => "Status",
	"StockWallet"            		 => "Dompet Investasi",
	"Submit"            			 => "Kirim",
	"SystemBusy"            		 => "Sistem Sibuk",
	"Table"            				 => "Meja",
	"TableLimit"            		 => "Batas Meja",
	"TermOfUse"            			 => "Syarat & Ketentuan Penggunaan",
	"TimeOut"            			 => "Waktu Habis",
	"TNC"            				 => "Syarat dan Ketentuan",
	"TNCPromo"            			 => "Syarat & Ketentuan Umum Promosi",
	"TNCPromo01"            		 => "Pelanggan harus berusia 18 tahun ke atas, atau usia yang diperbolehkan secara sah, meskipun di atas usia rata-rata dan mempunyai kapasitas mental yang mampu untuk bertanggung jawab atas dirinya sendiri dan terikat dengan syarat dan ketentuan-ketentuan ini. Kami berhak setiap saat dan kapan pun untuk membatalkan transaksi apabila diketahui melakukan pelanggaran.",
	"TNCTitle"            			 => "Syarat & Ketentuan",
	"To"            		 		 => "Ke",
	"Total"            				 => "Jumlah",
	"TransactionCancelFailed"        => "Transaksi Batal Gagal",
	"TransactionDate"            	 => "Tanggal Transaksi",
	"TransactionHistory"             => "Catatan Transaksi",
	"TRANSACTIONHISTORYContent"      => "Menemukan 50 transaksi terakhir Anda di sini <br> Jenis Transaksi: <br><br>",
	"Transfer"            		 	 => "Pindah Dana",
	"TransferAmount"            	 => "Jumlah Dana dipindahkan",
	"TransferDetails"            	 => "Rincian Transfer",
	"TransferFailed"                 => "*Transfer Gagal",
	"TransferFrom"            		 => "Transfer dari",
	"TransferTo"            		 => "Transfer ke",
    "TransferToMain"                 => "Transfer ke dompet utama",
    "ProceedWithoutAnyOfThePromotionsAbove" => "Akan di proses tanpa mengikuti promo apapun di atas.",
	"TryAgain"            			 => "Silahkan coba lagi.",
	"Tutorial"            			 => "Tutorial",
	"Type"            				 => "Tipe",
	"UnderageGambling"            	 => "Judi Bawah Umur",
	"UpdateBankDetail"            	 => "Memperbarui Rincian Bank",
	"UpdatePersonalDetail"           => "Memperbarui Data Pribadi",
	"Username"            		 	 => "Nama Pengguna",
	"Username2"            		 	 => "Nama Pengguna",
	"UsernameNotAvailable"           => "*Nama Pengguna tidak tersedia",
	"VenusClub"            		 	 => "Venus Club",
	"VerificationCode"            	 => "Kode Verifikasi",
	"Wallet"            		 	 => "Dompet",
	"Wallet1"            		 	 => "Dompet 1",
	"Wallet1Sportsbook"            	 => "<u>Dompet 1</u><br />Sportsbook",
	"Wallet2"            		 	 => "Dompet 2",
	"Wallet2ClubVegas"            	 => "<u>Dompet 2</u><br />Club Vegas Casino<br />3D Slots",
	"Wallet3"            		 	 => "Dompet 3",
	"Wallet4"            		 	 => "Dompet 4",
	"Wallet4D"            		 	 => "Dompet 4D",
	"Wallet5"            		 	 => "Dompet 5",
	"WalletMaintenanceTryLater"      => "*Dompet sedang dalam pemeliharaan . Silahkan coba lagi",
	"WalletTransfer"            	 => "Transfer Dompet",
	"Wechat"            		 	 => "WeChat",
	"Welcome"            		 	 => "Selamat Datang",
	"Withdrawal"            		 => "Penarikan",
	"Withdrawaldetails"            	 => "Rincian Penarikan",
	"WithdrawalHelp"            	 => "Bantuan Penarikan",
	"WithdrawalProfile"            	 => "Profil Penarikan",
	"WithdrawalStep1"            	 => "Langkah 1: Silakan memverifikasi rincian Bank Anda, atau hubungi Customer Service kami jika Anda perlu memperbarui informasi yang bersangkutan.",
	"WithdrawalStep2"            	 => "Langkah 2: Silahkan isi formulir berikut untuk meminta penarikan dana:",
	"WithdrawalSuccess"            	 => "*Permintaan Penarikan sukses. Kami akan memprosesnya segera",
	"WithdrawWinnings"            	 => "Penarikan<br/>Kemenangan",
	"WouldYouLikeMakeDeposit"        => 'Saat ini, Akun Anda memiliki <font color="#FF0000">0.00</font> credit.<br/>Anda mau melakukan deposit?<br/>',
	"WouldYouLikeToTransfer"         => "Apakah anda ingin melakukan transfer dari Dompet Utama anda?",
	"WrongCode"            		 	 => "Kode Salah",
	"YouAreNotBornYet"            	 => "*Anda belum lahir pada tahun tersebut",
	"YouMustBe18"            		 => "*Anda harus berusia 18 tahun atau lebih",
	"Mobile"						 =>	"Mobile",
	"Fund"							 =>	"Dana",
	"min"							 =>	"min",
	"max"							 =>	"max",
	"AccountProfile"				 =>	"Data Pribadi",
	"AccountPassword"				 =>	"Ganti Password",
	"TransactionEnquiry"			 =>	"Penyelidikan Transaksi",
	"Fund"  						 => "Manajemen dana",	
	"Mainwallet"				     =>	"Main Wallet",
	"Refresh"				    	 =>	"Refresh",
	"PendingTransaction"			 =>	"Pending Transaksi",
	"NoRecordFound"			 		 =>	"Tidak ada catatan Transaksi",
	"FullName"						 =>	"Nama Lengkap",
	"Special"						 =>	"Khusus",
	"Jackpot"						 =>	"Jackpot",
    "PleaseUploadDepositSupportDocument"    => "Silakan upload dokumen penyimpanan pendukung.",
	"AllMembersAreAllowedToWithdrawXTime"            	 => "Semua anggota diperbolehkan menarik dana :p1 per hari | Semua anggota diperbolehkan menarik dana :p1 per hari",
    "BetHistory"			         => "Catatan Taruhan",
];
