<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines contain the default error messages used by
	| the validator class. Some of these rules have multiple versions such
	| as the size rules. Feel free to tweak each of these messages here.
	|
	*/

	"accepted"             => ":attribute 必须被接受。",
	"active_url"           => ":attribute 是无效的URL。",
	"after"                => ":attribute 必须是 :date 之后的日期。",
	"alpha"                => ":attribute 只能接受字母（letters）。",
	"alpha_dash"           => ":attribute 只能接受字母，数字，与破折号（letters, numbers, and dashes）。",
	"alpha_num"            => ":attribute 只能接受字母与数字（letters and numbers）。",
	"array"                => ":attribute 必须是排列（array）。",
	"before"               => ":attribute 必须是 :date 之前的日期。",
	"between"              => [
		"numeric" => ":attribute 必须于 :min 与 :max 之间。",
		"file"    => ":attribute 必须于 :min 与 :max 千字节（kilobytes）之间。",
		"string"  => ":attribute 必须于 :min 与 :max 字符（characters）之间。",
		"array"   => ":attribute 项目（items）数量必须于 :min 与 :max 之间。",
	],
	"boolean"              => ":attribute 只能接受 “true” 与 “false”。",
    "captcha"              => "无效的验证码。",
	"confirmed"            => ":attribute 与目标不匹配。",
	"date"                 => ":attribute 日期格式不正确。",
	"date_format"          => ":attribute 格式不匹配：:format",
	"different"            => ":attribute 与 :other 必须不同。",
	"digits"               => ":attribute 数字必须是：:digits。",
	"digits_between"       => ":attribute 数字必须与 :min 和 :max 之间。",
	"email"                => ":attribute 必须是正确的电邮地址。",
	"filled"               => ":attribute 不能留空。",
	"exists"               => "无效的选项：:attribute",
	"image"                => ":attribute 只能接受图像文件。",
	"in"                   => "无效的选项：:attribute",
	"integer"              => ":attribute 只能接受整数。",
	"ip"                   => ":attribute 必须是正确的IP地址。",
	"max"                  => [
		"numeric" => ":attribute 不能大于 :max。",
		"file"    => ":attribute 不能大于 :max 千字节（kilobytes）。",
		"string"  => ":attribute 不能大于 :max 字符（characters）。",
		"array"   => ":attribute 项目（items）数量不能大于 :max 。",
	],
	"mimes"                => ":attribute 只接受此类型的附件：:values",
	"min"                  => [
		"numeric" => ":attribute 不能小于 :min。",
		"file"    => ":attribute 不能小于 :min 千字节（kilobytes）。",
		"string"  => ":attribute 不能小于 :min 字符（characters）。",
		"array"   => ":attribute 项目（items）数量不能小于 :min 。",
	],
	"not_in"               => "无效的选项：:attribute 。",
	"numeric"              => ":attribute 只能接受数字。",
	"regex"                => ":attribute 正则表达式格式不正确。",
	"required"             => ":attribute 不能留空。",
	"required_if"          => "当 :other 是 :value 的时候，:attribute 不能留空。",
	"required_with"        => "当 :values 呈现的时候，:attribute 不能留空。",
	"required_with_all"    => "当 :values 呈现的时候，:attribute 不能留空。",
	"required_without"     => "当 :values 不存在的时候，:attribute 不能留空。",
	"required_without_all" => "当 :values 不存在的时候，:attribute 不能留空。",
	"same"                 => ":attribute 与 :other 必须匹配。",
	"size"                 => [
		"numeric" => ":attribute 必须为 :size。",
		"file"    => ":attribute 必须为 :size 千字节（kilobytes）。",
		"string"  => ":attribute 必须为 :size 字符（characters）。",
		"array"   => ":attribute 项目（items）数量必须为 :size 。",
	],
	"unique"               => ":attribute 已经被使用。",
	"url"                  => ":attribute URL格式不正确。",
	"timezone"             => ":attribute 必须是正确的时区。",
	"reserved_str"         => ":attribute 不被接受。",
	"username_exist"       => ":attribute 已被使用。",
	"email_exist"     	   => ":attribute 已被使用。",
	"phone"     	  	   => "无效的电话号码。",
	"newpass_same"     	   => "新密码不能与旧密码相同。",
	"oldpass_wrong"        => "旧密码不正确。",
	"newandconpass_notsame"   => "新密码与确认新密码不一样。",
	"wrong_captcha"        => "无效的验证码。",
	"login_fail"     	   => "登入失败。",
	"wallet_error1"        => "钱包不能相同。",
	"wallet_error2"        => "余额不足。",
	

	/*
	|--------------------------------------------------------------------------
	| Custom Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| Here you may specify custom validation messages for attributes using the
	| convention "attribute.rule" to name the lines. This makes it quick to
	| specify a specific custom language line for a given attribute rule.
	|
	*/

	'custom' => [
		'attribute-name' => [
			'rule-name' => 'custom-message',
		],	
		'rules' => [
			"required"      => "请阅读并同意存款优惠规则。",
		],
	],

	/*
	|--------------------------------------------------------------------------
	| Custom Validation Attributes
	|--------------------------------------------------------------------------
	|
	| The following language lines are used to swap attribute place-holders
	| with something more reader friendly such as E-Mail Address instead
	| of "email". This simply helps us make messages a little cleaner.
	|
	*/

	'attributes' => [],

];
