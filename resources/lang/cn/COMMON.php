<?php
return [


    'ABOUT'							=> '关于',
    'ACCEPT'							=> '接受',
    'ACCEPTED'							=> '被接受',
    'ACCEPTEDFULL'						=> '被充分接受',
    'ACCEPTEDPARTIAL'					=> '被接受的部份',
    'ACCEPTEDON'						=> '接受在',
    'ACCEPTTIMEOUT'					=> '采纳暂停',
    'ACCESSIBILITY'					=> '可及性',
    'ACCESSURL'						=> '访问URL',
    'ACCOUNT'							=> '帐号',
    'ACCOUNTID' 						=> '帐号ID',
    'ACCOUNTBALANCE'					=> '账户余额',
    'ACCOUNTCODE' 						=> 'Account Code',

    'ACCOUNTBLOCKED'					=> '由于安全防备措施, 帐户已由系统阻拦。疏导它请访问"Blocked Account"模块在安全菜单之下',
    'ACCOUNTBLOCKEDFRAUDCASE'			=> '由于发现欺骗活动, 帐户已被阻拦',
    'ACCOUNTDETAILS'					=> '帐户资料',
    'ACCOUNTENQUIRY'					=> '帐户查询',
    'ACCOUNTINFORMATION'				=> '帐户资料',
    'ACCOUNTPOSTMORTEM'				=> '帐户解剖',
    'ACCOUNTREGISTRATION'				=> '帐户注册',
    'ACCOUNTS'							=> '帐户',
    'ACCOUNTSTATUS'					=> '帐户状态',
    'ACCOUNTSETTING'					=> '帐户设定',


    'ACCOUNTSUSPENDED' 				=> '您的帐户已暂停, 请与管理员联系为帮助! - 误差编码: %0',
    'ACCOUNTEXPIRED' 					=> '您的帐户已到期, 请与管理员联系为帮助! - 误差编码: %0',
    'ACTION'							=> '行动',
    'ACTIVATE'							=> '活跃',
    'ACTIVATESELECTEDROW'				=> '活跃被选择的列',
    'ACTIVE'							=> '有效',
    'ACTIVITYREPORT'					=> '活动报告',

    'ADD'								=> '增加',
	'ADDUSERFUNCTION'					=> '新增用户功能',
    'ADDMEMO'							=> '增加备忘录',
    'ADDNEW'							=> '新增',
    'ADDRESS'							=> '地址',
    'ADDRESS1'							=> '地址1',
    'ADDRESS2'							=> '地址2',
    'ADDRESS3'							=> '地址3',
    'ADJUSTMENT'						=> '调整金额',
    'ADJUSTMENTTYPE'					=> '调整类别',
    'ADJUSTMENTAMT'					=> '调整数额',
    'ADJUSTMENTAMTLOCAL'				=> '本地调整数额',
    'ADJUSTMENTFROM'					=> '调整从',
    'ADJUSTMENTMETHOD'					=> '调整方法',
    'ADJUSTMENTPROFILE'				=> '调整简介',
    'ADJUSTMENTREPORT'					=> '调整报告',
    'ADJUSTMENTTO'						=> '调整到',
    'ADMIN'							=> '管理人',
    'ADMINANNOUNCEMENT'				=> '管理公告',
    'ADMINLOG'							=> '管理员记录',
    'ADMINSUMMARY'						=> '管理摘要',
    'ADMINUSER'						=> '管理员',
    'ADMINUSERGROUP'					=> '管理员',
    'AFFILIATE2'						=> '红股代理',
    'AFFILIATECOMMISION'				=> '代理佣金',

    'AFTER'							=> '之后',
    'AGENCY'							=> '代理商',
    'MASTERAGENT'						=> '总代理',



    'AGENT'							=> '占成代理',
    'AGO'								=> '之前',
    'ALL'								=> '所有',
    'ALLBET'							=> '所有注单',
    'ALLOWIP'							=> '准许IP',
    'AMOUNT'							=> '金额',
    'AMOUNTLOCAL'						=> '本地数额',
    'AMOUNTRETURNED'					=> '支出数额',
    'ANALYSIS'							=> '分析',

    'AND'								=> '与',
    'ANNOUNCEMENT'						=> '消息公告',

    'APPLY'							=> '适用',
    'APPLYTO'							=> '适用于',
    'APPROVE'							=> '批准',
    'APPROVEREJECT'					=> '批准/拒绝',

    'ASIAN'							=> '亚洲',
    'ASSIGN'							=> '分配',
    'ASSIGNED'							=> '已指派',

    'ATLEAST'							=> '请输入至少一个%0',
    'ATLEASTREQUIRED'					=> '至少需要%0 %1',
    'ATTENTION'						=> '注意',

    'AUDITED'							=> '审计日期',
    'AUDITEDBY'						=> '审计用户',

    'AUTO'								=> '自动',
    'AUTO_CANCELLED'					=> '自动取消',
    'AUTO_CONFIRMED'					=> '自动批准',
    'AUTO_PENDING'						=> '审理中',

    'AUDIT'	                        => '审核',
    'AVAILABLE'						=> '可用的',
    'AVERAGEBETSIZE'					=> '平均投注量',


    'ABNORMALSTATUS'	                => '异常状态',
    'ACUMULATIVE'	               	    => '累計',
    'AUDITPERMISSION'	               	=> '審核權限設定',

    'BACK'								=> '返回',
    'BACKUP'							=> '备份',
    'BADLOGINLOG'						=> '企图使用坏注册',
    'BALANCE'							=> '余额',

    'BANK'								=> '银行',
    'BANKACCOUNT'						=> '银行帐户',


    'BANKACCBRANCH'					=> '银行帐户控股分支',
    'BANKACCDESC'						=> '银行帐户描述',
    'BANKACCNAME'						=> '银行帐户名字',

    'BANKACCNO'						=> '银行帐户号码',
    'BANKBRANCHCODE' 					=> '分行帐户代码',
    'BANKCODE'							=> '银行代码',
    'BANKCOUNTRY' 						=> '银行国家',
    'BANKDATA'							=> '银行资料',
    'BANKINFORMATION'					=> '银行信息',
    'BANKNAME'							=> '银行名字',
    'BANKPHONENO_1'						=> '银行电话号码1',
    'BANKPHONENO_2'						=> '银行电话号码2',
    'BANKSORTCODE' 						=> '银行排序代码',
    'BANKSWIFTCODE'						=> '快速代码',
    'BANKTRANSFER'						=> '银行汇款',
    'BANKTRANSFEROBJECT'				=> 'BankTransferObject',
    'BANKFULLNAME'						=> '银行全名',
    'BANKFULLNAMEEN'					=> '银行全名 (英文)',
    'BANKFULLNAMECH'					=> '银行全名 (中文)',
    'BANKSHORTNAME'					=> '银行简称',
    'BANKSHORTNAMEEN'					=> '银行简称 (英文)',
    'BANKSHORTNAMECH'					=> '银行简称 (中文)',
    'BANKTYPE'							=> '银行类别',
    'BANKBRANCH'						=> '支行',

    'BANKHQ'							=> '总行',
    'INTERNETBANK'						=> '开户网点',
    'BANNEDIP'							=> '禁止IP',

    'BBF'								=> '账户结馀向前调动',
    'BBFSHORT'							=> 'BBF',
    'BEGINNING'							=> '开始',
    'BEFORE'							=> '以前',
    'BETBOTHSIDE'						=> '双面打水',
    'BETENQUIRY'						=> '注单查询',
    'BETFILTER'							=> '注单过滤器',
    'BETINTERCEPTOR'					=> '注单拦截',
    'CASINOBETAUDIT'		            => '审核真人视讯注单',




    'SPORTBETAUDIT'		            => '审核体育注单',
    'TRANSAUDIT'		                => '审核交易',
    'BETNUM' 							=> '注单',
    'BETS'								=> '投注',
    'BETSLIP'							=> '注单',
    'BETSLIPOBJECT'						=> 'BetslipObject',
    'BETTERM'							=> '投注期限',
    'BETTING'							=> '投注中',
    'BETTYPE'							=> '投注类型',
    'BETTYPEREPORT'					=> '投注类型报告',
    'BETTYPEREPORT2'					=> '投注类型报告2',
    'BETURL'							=> '投注URL',
    'BLOCKED'							=> '封锁',
    'BLOCKEDACCOUNT'					=> '被阻拦的帐号',
    'BLOCKEDIP'							=> '阻拦的IP',



    'BONUS'							=> '现金优惠',
    'BONUSCAMPAIGN'					=> '现金优惠活动',
    'BOOLEAN'							=> 'Boolean',

    'BOTH'								=> '双方',
    'BRANCHCODE'						=> '分支代码',


    'CACHE'							=> '缓存',
    'CACHEEXPIRY'						=> '缓存期满(秒)',
    'CACULATE'							=> '计算',
    'CALLSTACK'							=> '调用栈',
    'CAMPAIGN'							=> '活动',
    'CANNOTMORETHAN'					=> '%0无法多过%1',
    'CANCEL'							=> '取消',
    'CANCELLED'							=> '被取消',
    'CANCELLEDBET'						=> '被取消的赌注',
    'CANCELLEDBYADMIN'					=> '被管理员取消',
    'CANCELLEDMATCH'					=> '取消',
    'CANNOTCANCELBET'					=> '您无法取消这赌注',

    'CANNOTSELECTMORE'					=> '这个工能您只能选择一个项目',
    'CANNOTSIMILAR'						=> '%0无法是同一样%1',
    'CANNOTSETTLENOTRESULT'			=> '无法解决没有赛果的活动',

    'CASH'								=> '现金',
    'CASHCARD'							=> '现金卡',
    'CASHBALANCE'						=> '现金余额',
    'CASHBALANCE_POSITIVE'				=> '现金余额是正数',
    'CASHLEDGER'						=> '现金总帐',
    'CATEGORY'							=> '类别',








    'CENT'								=> '分',
    'CITY'								=> '城市',
    'CGBETTING'							=> '投注配置',
    'CGLOGIN'							=> '注册配置',
    'CGMARKET'							=> '市场配置',
    'CGPAYMENT'							=> '付款配置',
    'CGSYSTEM'							=> '系统配置',
    'CGTRANSACTION'						=> '交易配置',
    'CHANGED'							=> '已更改',
    'CHANGEPASS'						=> '修改密码',
    'CHANGESETTING'						=> '修改个人设定',


    'CHANNEL'							=> '频道',
    'CHARGEFEEREPORT'					=> '收费报告',
    'CHARGESAMOUNT'					=> '收费数目',

    'CHECK'							=> '检查',
    'CHECK_ALL'						=> '全选',
    'CHECKRESULT'						=> '检查赛果',
    'CHECKSETTLEMENT'					=> '检查注单计算',
    'CHECKSTAKE'						=> '检查注额',
    'CHECKWAGERSEQUENCE'				=> '检查赌注序列',

    'CLASS'								=> '等级',
    'CLASSNAME'						=> '等级名称',
    'CLIENT'							=> '客户',

    'CLOSE'								=> '关闭',
    'CLOSEALL'							=> '关闭所有',
    'CLOSEDATE'							=> '关闭日期',
    'CLOSEDATETIME'						=> '关闭时间',
    'CLOSESELECTEDMATCH'				=> '关闭选择的比赛?',
    'CLOSETICKET'						=> '关闭帖子',
    'CLOSETIME'							=> '关闭时间',
    'CLOSINGBALANCE'					=> '收盘余额',

    'CMS'								=> '内容管理',
    'CMSARTICLE'						=> '文章内容',


    'CMSCAT'							=> '种类内容',
    'CODE'								=> '代码',
    'CODE2'								=> '注册码',
    'COMMENT'							=> '评论',
    'COMMENT_COL'						=> '评论',
    'COMMISSION'						=> '佣金',
    'COMMISSIONWITHLABEL'				=> '佣金(不需要添加%)',
    'COMMISSIONTYPE'					=> '佣金类别',
    'COMPANY'							=> '公司',
    'COMPANY_BEAR'						=> '公司承担',
    'COMPETITION'						=> '活动',
    'COMPETITIONLIST'					=> '活动列表',
    'COMPETITIONREPORT'				=> '活动报表',
    'CONDITIONALITY_REVIEW'			=> '条件审查',


    'CONFGROUP'						=> '配置小组',
    'CONFIG'							=> '系统配置',
    'CONFIRM'							=> '确定',
    'CONFIRM2'							=> '确认',
    'CONFIRMACCEPTANCE'				=> '确定接受?',
    'CONFIRMADD'						=> '确定增加?',
    'CONFIRMAPPROVE'					=> '确定批准?',

    'CONFIRMBATCHCREATE'				=> '确定应用庄家资料建造项目?',
    'CONFIRMEDDATA'					=> '确定资料',

    'CONFIRMDELETION'					=> '确定删除? 确定后, 就无法取消。',
    'CONFIRMEVENTRESET'				=> '确定重新设置比分?',
    'CONFIRMMAKESETTLEMENT'			=> '确定做注单计算?',
    'CONFIRMREJECT'					=> '确定拒绝?',
    'CONFIRMPROCESS'					=> '确定审理?',
    'CONFIRMSUBMIT'					=> '确定提交?',


    'CONFIRMREMOVE'					=> '确定删除? 确定后, 就无法取消。',
    'CONFIRMREMOVEMAPPING'				=> '从%1删除%0? 确定后, 就无法取消。',
    'CONFIRMED'						=> '已确定',
    'CONSOLE'							=> '控制台',

    'CONTACTINFO'						=> '联络资料',
    'CONTENT'							=> '内容',
    'CONTENTTITLE'						=> '标题',
    'CONTINUEINSET'					=> '继续输入',

    'COUNT'							=> '计数',
    'COUNTER'							=> '驳回',
    'COUNTRY'							=> '国家',
    'CREATE'							=> '创造',
    'CREATEDBY'						=> '创造者',
    'CREATEDIP'						=> '创造者IP',
    'CREATEDON'						=> '创造时间',
    'CREDIT'							=> '存入',
    'CREDITBALANCE'					=> '信贷馀额',
    'CREDITLIMIT'						=> '信贷限额',


    'CURCODE'							=> '货币代码',
    'CURRATE'							=> '货币兑换率',
    'CURRATESHORT'						=> '货币兑换率',
    'CURRENCY'							=> '货币',
    'CURRENT'							=> '现时的',
    'CURRENT_CASHBALANCE'				=> '现时现金余额',
    'CURRENT_CONDITIONS'				=> '当前条件',
    'CURRENT_TURNOVERSTAKE'			=> '目前有效投注',
    'CURRENT_WINLOSS'					=> '目前输赢',
    'CURRENTPASSWORD'					=> '现时的密码',
    'CUSTOMER'							=> '客户',
    'CUSTOMERACCSTATEMENT'				=> '客户帐户报表',
    'CUSTOMERACTIVITYREPORT'			=> '客户活动报告',
    'CUSTOMERACTIVITYSUMMARYREPORT'	=> '客户活动综合报告',
    'CUSTOMERREPORT'					=> '客户报告',
    'CUSTOMERRESPOND'					=> '客户反应',
    'CUSTOMERSERVICEOPERATOR'			=> '客户服务操作员',
    'CUSTOMERSERVICE'					=> '客户服务',
    'CUSTOMERSUPPORT'					=> '客户服务',
    'CUSTOMERWINAMOUNT'				=> '客户赢的数额',

    'COVER'							=> '覆盖',
    'COVERNUM'							=> '个号码',


    'DAILY'							=> '每日',
    'DATA'								=> '资料',
    'DATABASE'							=> '数据库',
    'DATABASENAME'						=> '数据库名称',
    'DATATYPE'							=> '资料种类',

    'DATE'								=> '日期',
    'DATEPOSTED'						=> '张贴时间',
    'DATEORTIME'						=> '时间',



    'DAY'								=> '日',
    'DAYS'								=> '日',
    'DEBIT'							=> '支出',
    'DECIMAL' 							=> '小数式',
    'DECLINED'							=> '被拒絕',
    'DECRYPT'							=> '解密',

    'DECRYPTURL'						=> '变数解密',
    'DEFAULTLANGUAGE'					=> '默认语言',
    'DELETE'							=> '删除',
    'DELETEALL'						=> '全删除',
    'DELETEFAIL'						=> '删除失败',
    'DELETEOWN'						=> '删除',
    'DELETESELECTEDFILTER'				=> '删除选择的过滤器?',
    'DEPARTMENT'						=> '部门',
    'DEPOSIT'							=> '存款',
    'DEPOSIT_TOBEPROCESSED'			=> '存款申请待处理',
    'DEPOSITPERCENTAGE'				=> '%存款',
    'DEPOSITREPORT'					=> '存款报告',
    'DEPOSITTO'						=> '存款授予',
    'DEPOSITWITHDRAWAL'				=> '存提',
    'DEPOSITWITHDRAWALTIME'			=> '存提次数',
    'DEPOSITWITHDRAWALAMOUNT'			=> '存提总数',

    'DESC'								=> '描述',
    'DETAILEDVIEW'						=> '详细观察',
    'DETAILS'							=> '详细',

    'DETAILSONLYFORWAGER'				=> '抱歉, 细节只可利用为赌注',
    'DIRECTMEMBER'						=> '直接会员',
    'DISABLE' 							=> '停用',
    'DIDNOT_RECEIVE_DEPOSIT'			=> '未收到款',
    'DIFFERENCES'						=> '差异',


    'DOB'								=> '出生日期',
    'DONE'								=> '完成',
    'DOWNLINE'						 	=> '下线',

    'DRAW'								=> '和局',
    'DONTHAVE'							=> '没有',

    'DTZ_MINBALANCE'					=> '斗地主必备余额总数',
    'DUPLICATEIP'						=> '重复IP',
    'DUPLICATEVALUE'					=> '重复值 "%0"',

    'EDIT'								=> '修改',
    'EDITPASSWORD'						=> '修改密码',
    'EDITALL'							=> '修改所有',
    'EDITSTATUS'						=> '修改状态',
    'EDITOWN'							=> '自已修改',


    'EMAIL'							=> '邮箱',
    'EMAILADD'							=> '邮箱地址',
    'ENABLE'	 						=> '启用',
    'ENCODING'							=> '编码',

    'END'								=> '结束',
    'ENDDATETIME'						=> '结束时间',


    'ENDED'							=> '结束',
    'ERROR'							=> '错误',
    'ERRORCODE'						=> '错误代码',

    'ERROR_ALLOWWITHDRAWAL'			=> '您的提款资金数额超出每日支出资金限额。',
    'ERROR_INVALIDPASSWORD'			=> '您输入了一个无效密码',







    'ERROR_MINDEPOSIT'					=> '您的储蓄数额比最小的储蓄限额低。',
    'ERROR_MAXDEPOSIT'					=> '您的储蓄数额比最大的储蓄限额高。',
    'ERROR_MINWITHDRAWAL'				=> '您的提款数额比最小的提款限额低。',
    'ERROR_NETAMOUNT'					=> '您的提款数额超出您的账户结馀。',
    'EVEN'								=> '双',
    'EVENS'							=> '双',
    'EVENT'							=> '项目',
    'EVENTS'							=> '项目',


    'EXCEEDACCOUNTBALANCE'				=> '抱歉, 转移数额超出了账户余额',
    'EXCLUDEIP'							=> '排除IP',
    'EXCLUDEPROMOCASH'					=> '(排除红利)',
    'EXPIRED'							=> '到期',
    'EXPIRYDATE'						=> '有效期限',
    'EXPORT'							=> '导出',
    'EXPORTBATCHFILE'					=> '输出Batch档案',





    'FAIL'								=> '失败',
    'FAILTOOPENFILE'					=> '档案无法打开',
    'FAX'								=> '传真号码',
    'FEATUREDISABLED' 					=> '这个工能暂时是中止的。',
    'FEE'								=> '费用',
    'TOTALFEE'							=> '总费用',
    'FEEDBACK'							=> '回复',
    'FEEDBACKTO'						=> '回复到%0',
    'FEELOCAL'							=> '本地费用',
    'FEETYPE'							=> '费用类别',
    'FEMALE'							=> '女性',
    'FINANCE'							=> '财务',
    'FINANCESUMMARY'					=> '财务端摘要',


    'FILE'								=> '档案',
    'FILE_SIZE'						=> '档案大小是必须较少于2MB',
    'FILTER'							=> '过滤器',
    'FILTERNAME'						=> '过滤器名字',
    'FINALSTAKE'						=> '总注额',

    'FIRSTNAME'						=> '名字',
    'FIRSTTIMEDEPOSIT'					=> '首次存入',
    'FIXEDAMOUNT'						=> '固定值',
    'FIXEDCHARGES'						=> '固定费用',
    'FLATPOINT'						=> '平点',


    'FLOAT'							=> '浮动',
    'FOR'								=> '为',
    'FORMAT'							=> '格式',
    'FRACTIONAL' 						=> '分数式',
    'FRAUDCASE'						=> '欺骗案件',


    'FRAUDCASEACCOUNT'					=> '请注意一旦帐户被设置为"欺骗案件", 所有交易(储蓄、提款、赌注、支出)将被破解并且状态无法再被更新。',
    'FROM'								=> '从',
    'FULLTIME'							=> '全场',
    'FUNCTION'							=> '功能',
    'FUNDINGMETHOD'					=> '支付方式',
    'GAMESTYPE'						=> '游戏类别',
    'GENDER'							=> '性别',
    'GENERATEREPORT'					=> '产生报告',


    'GO'								=> '下一步',
    'GOTO'								=> '页数',
    'GOTOPREVIOUSPAGE'					=> '到前页',


    'GOVTAXREPORT'						=> '政府徵税报告',
    'HAVE'								=> '有',
    'HAVESAME'							=> '%0 和 %1 相同的',


    'HELP'								=> '帮助',
    'HIDE'								=> '隐藏',
    'HIDECOLUMN'						=> '隱藏栏',
    'HISTORY'							=> '记录',

    'HOMEPHONE'						=> '家庭电话号码',
    'HONGKONG'							=> '香港',


    'HOUR'								=> '小时',
    'HOURS'								=> '小时',
    'HOWDIDUHEARABOUTUS'				=> '您如何发现我们?',

    'ICON'								=> '象征',
    'ID'								=> 'ID',

    'IDNOTPROVIDED'						=> '这名用户不将允许执行支出交易因为没有提供身分认证',
    'IDPASSPORT'						=> '身份证 / 护照',
    'IDPROVIDED'						=> '提供ID',
    'IMPORT'							=> '资料载入',

    'IN'								=> '在',
    'INCENTIVE'						=> '退水优惠',
    'INCENTIVE_BONUS'					=> '退水金',
    'INCENTIVEA'						=> '退水A',
    'INCENTIVEB'						=> '退水B',
    'INCENTIVEC'						=> '退水C',
    'INCENTIVEDURATION'				=> '结算时间',
    'INCENTIVEMAX'						=> '封顶值',
    'INCENTIVEASHORT'					=> '退水A',
    'INCENTIVEBSHORT'					=> '退水B',
    'INCENTIVECSHORT'					=> '退水C',
    'INCENTIVESETTING'					=> '退水设定',
    'INCENTIVESPORTSBOOK'				=> '体育退水',
    'INCENTIVESPORT1'					=> 'AH / OU / OE',
    'INCENTIVESPORT2'					=> '1x2',
    'INCENTIVESPORT3'					=> '其他',

    'INCLUDEDRAW'						=> '包括和局选择',
    'INCOMPLETE'						=> '不完整',
    'INPROGRESS'						=> '进展中',
    'INRUNNING'						=> '走地',
    'INCORRECTINPUT'					=> '%0是不正确的',


    'INDEX'							=> '指数',
    'INDO'								=> '印尼',
    'INFORMATION'						=> '资讯',




    'INFO_RIGHT'						=> 'NB. 关於您的帐户Betworks有权与您联系',
    'INFO_SECURITY1'					=> '如果您与我们联系, 我们将使用以下资料确认您的身分',
    'INFO_SECURITYKEY1'					=> '请输入字符如在左边的图像。',
    'INFO_SECURITYKEY2'					=> '这帮助我们改进网站的安全。',
    'INPUTCONFLICT'						=> '%0和%1不能相同',

    'INPUTRANGE'						=> '%0必须是在%1到%2之间的长度',
    'INSTANT_TRANSACTION'				=> '即时交易',
    'INTEGER'							=> '整数',
    'INTERNALTRANSFER'					=> '内部调动',
    'INTERNET'							=> '互联网',
    'INTERVAL'							=> '间隔',
    'INVALIDCHARACTER'					=> '%0包含无效字符',
    'INVALIDDATAFORMAT'					=> '不正确资料格式',

    'INVALIDDATE'						=> '请填写正确日期',
    'INVALIDFORMAT'						=> '不正确%0格式',
    'INVALIDINPUT'						=> '这%0是不正确',

    'INVALIDLOGIN'						=> '抱歉, 您的注册无效。恳请联络管理员寻求协助 - 误差编码: %0',
    'INVALIDMATCH'						=> '"%0"不匹配"%1"',
    'INVALIDMULTITYPESELECTION' 		=> '复式型选择无效',

    'INVALIDPASSWORD'					=> '%0必须包含字母和数字混合',
    'INVALIDPHONENO'					=> '%0为不正确电话号码',

    'INVALIDRANGE'						=> '无效范围为"%0"和"%1"',
    'INVALIDSHORTCUT'					=> '无效捷径',


    'INVALIDUSERNAME'					=> '%0必须包含字母数字和强调字符及从字母表开始',
    'INVALID_USERNAME'					=> '帐号无效',
    'IPADDRESS'							=> 'IP地址',
    'IPLOOKUP'						=> '检查IP',
    'JUMPCODE'						=> '跳码',
    'KILLTRACKER'						=> '废止在线使用者',
    'LANGUAGE'						=> '语言',
    'LAST7DAYS'						=> '前七日',
    'LASTACTION'						=> '前次行动',
    'LASTBETON'						=> '最後投注时间',
    'LASTLOGINON'						=> '最後登入',

    'LASTNAME'						=> '姓氏',
    'LASTUPDATED'						=> '前更新',
    'LESSTHAN1MIN'					=> '少过一分钟',
    'LIABILITY'						=> '负担额',
    'LIABILITYFILTER'					=> '负担额过滤器',
    'LIABILITYMATCH'					=> '赛事负担额',
    'LIABILITYOUTRIGHT'				=> '夺标负担额',

    'LIMIT'							=> '极限',
    'LIMITED'							=> '限制',


    'LINE'								=> '行',
    'LOSER'								=> '输家',
    'LOCAL'							=> '本地',
    'LOCALBALANCE'						=> '本地余额',

    'LOGIN'							=> '登入',
    'LOGINAS'							=> '登入用户',
    'LOGOUT'							=> '登出',

    'LOSS'								=> '输',
    'LOSSHALF'							=> '输半',
    'LOSSONLY'							=> '输而已',

    'MAIN'								=> '主',
    'MAILINGADDR'						=> '邮寄地址',

    'MALE'								=> '男性',
    'MANDATORYFIELD'					=> '必填项目',

    'MANDATORY'						=> '%0是必填项目',
    'MARKETING'							=> '营销',


    'MATCH'							=> '赛事',
    'MAXAMOUNT'						=> '封顶值',
    'MAXCHARGES'						=> '最高费用',
    'MAXDAILYDEPOSIT'					=> '每日最大值的存款数额',
    'MAXDAILYWITHDRAWAL'				=> '每日最大值的提款数额',
    'MAXWITHDRAWAL'					=> '最高提款',
    'MAXLIMIT'							=> '最大限度',

    'MAXUSERTYPEREACH'					=> '网上的用户已经提款到最大限度, 请%0分钟再登入',
    'MAXMEMBER'						=> '人数上限',
    'MEMBER'							=> '会员',
    'MEMBERACCOUNT'					=> '会员账户',
    'MEMBERCANCELLED'					=> '会员取消',
    'MEMBERMAXINCENTIVE'				=> '会员最高优惠',
    'MEMBERNETACCOUNTID'				=> 'Neteller会员账户ID',
    'MEMBERTRENDS'						=> 'MemberTrends',

    'MEMBERREFNO'						=> '会员参考数字',
    'MEMBERNETSECUREID'				=> 'Neteller会员安全ID',
    'MEMBER_WINLOSS'					=> '会员输赢',


    'MEMO'								=> '备忘录',
    'MENU'								=> '选项',
    'METHOD'							=> '方法',
    'MIGRATION' 						=> '迁移',
    'MINLIQUIDITY'						=> 'Minimum Liquidity',
    'MINLIQUIDITYMSG'					=> 'Minimum liquidity will be set to system default (%0) if checkbox is unchecked',
    'MINUTE'							=> '分',
    'MINUTES'							=> '分',
    'MINCHARGES'						=> '最低费用',
    'MINDEPOSIT'						=> '最低存款数额',
    'MINWITHDRAWAL'					=> '最低提款数额',
    'MOBILEPHONE'						=> '流动电话号码',

    'MODE'								=> '方式',
    'MODIFICATION'						=> '修改',
    'MODIFIEDBY'						=> '修改者',
    'MODIFIEDON'						=> '修改时间',
    'MODULE'							=> '模块',
    'MONITORING'						=> '监测',

    'MONTH'							=> '月份',
    'MONTHLY'							=> '每月',
    'MONTHTODATE'						=> '本月至今',
    'MULTIPLE'							=> '复数',
    'MUSTBEMORETHAN'					=> '%0必须多过于%1',
    'MUSTHAVESAME'						=> '%0和%1必须有相同的%2',
    'MUSTGREATERTHAN'					=> '%0必须大于%1',
    'MOREINFO'							=> '更多消息',

    'NAME'								=> '名字',
    'NAMECARD'							=> '名片',
    'NETTDEPOSIT'						=> '净存款',
    'NETTWITHDRAWAL'					=> '净提款',
    'NEUTRAL'							=> '中立场',

    'NEVER'								=> '从未',
    'NEVEREXPIRE'						=> '永不过期',

    'NEW'								=> '新增',
    'NEWACCOUNT'						=> '新帐号',
    'NEWMEMBER'						=> '新会员',
    'NEWMEMO'							=> '新备忘录',
    'NEWPASSWORD'						=> '新密码',
    'NEWS'								=> '新闻',
    'NEWSIGNUP'							=> '新加入',
    'NEWSIGNUPDEPOSITS'					=> '新加入储蓄',
    'NEWSLETETR'						=> '电子报',
    'NEWTRANSACTION'					=> '新交易',





    'NEXT'								=> '新增',
    'NEXTLOGINTEMPBLOCK'				=> '为了安全考量, 请在%0秒以后才登入',
    'NO'								=> '否',
    'NUM'								=> '序',
    'NOACCESSRIGHT'						=> '抱歉, 您没有足够权利执行这功能',
    'NOCHANGEMADETO'					=> '%0没有变动',





    'NOEVENTRIGHT'						=> '抱歉, 您所属的用户群没有权利处理这项目',
    'NOINPUT'							=> '请输入%0',
    'NOMEMO'							=> '此纪录没有备忘录',
    'NOMULTIPLEBET' 					=> '这些选择不含多重投注',
    'NONE'								=> '无',
    'NORECORDADDED'					=> '没有增加纪录',
    'NORMAL'							=> '正常',

    'NOT_EXIST'						=> ' 不存在或记录不存在。',
    'NOTAPPLICABLE'					=> '不适用',
    'NOTAVAILABLE'						=> '不可用',
    'NOTAVAILABLESHORT'				=> 'N/A',
    'NOTHINGCHANGE'					=> '没什么更新',

    'NOTTALLY'							=> '%0与%1不吻合。',
    'NUMBER'							=> '号码',
    'NUMBERSCORESET'					=> '比分数量',
    'NOABNORMALSTATUS'					=> '无异常状态',

    'ODD'								=> '赔率',
    'ODDSTYPE'							=> '赔率型别',

    'OFF'								=> '关掉',
    'OFFLINE'							=> '离线',

    'OK'								=> '确定',
    'OLDMEMBER'						=> '旧会员',

    'ON'								=> '开启',
    'ONLINE'							=> '在线',
    'ONLINEMEMBER'						=> '在线会员',

    'OPEN'								=> '打开',
    'OPENALL'							=> '全部打开',
    'OPENINGBALANCE'					=> '打开余额',
    'OPENSELECTEDMATCH'				=> '打开选择的赛事?',
    'OPENCLOSEPAUSESELECTED'			=> '%0选择%1?',
    'OPERATOR'							=> '操作员',
    'OPERATORPASSWORD'					=> '操作员密码',



    'OR'								=> '或',
    'ORDER'							=> '排序',
    'OTHER'							=> '其它',
    'OTHERDATA'						=> '其它资料',


    'OU'								=> '大/小盘',
    'OUTOFRANGE'						=> '%0是在控制范围之外。',
    'OUTPUT'							=> '产量',

    'OVER'								=> '大',
    'OVER_500_LINE'					=> '文件是已超过500行',

    'OVER_FILE_VALID'					=> '文件是确实的, 及已成功被上载了。',
    'OVER_UPLOAD_ATTACK'				=> '文件可能上载时被攻击!',




    'OVER'								=> '超过',
    'PAGE'								=> '页',
    'PANEL'							=> '盘区',
    'PARAM'							=> '参量',
    'PARENT'							=> '父母',
    'PASSLASTCHANGEON'					=> '密码最后变更在',
    'PASSWORD'							=> '密码',
    'PASSWORDRESETED'					=> '重新设置密码已完成了',


    'PASSWORDEXPIRED'					=> '您的密码已经过期了! 请先更新您的密码后再重新登入。',
    'PAUSE'							=> '暂停',
    'PAUSEALL'							=> '暂停所有',
    'PAUSESELECTEDMATCH'				=> '暂停选择的赛事?',

    'PAYEE'							=> '收款人',
    'PAYEENAME'						=> '收款人名字',
    'PAYMENTMETHOD'					=> '付款方法',
    'PAYOUT'							=> '支出',
    'PAYOUTLOCAL'						=> '本地支出',
    'PAYOUTLOCATION'					=> '出款出处',

    'PEN'								=> '笔',
    'PENDING'							=> '等待',
    'PENDING_LEDGER' 					=> '未确认帐目',
    'PENDINGBET' 						=> '未确认',
    'PENDINGWITHDRAWAL' 				=> '待批提款',
    'PENDINGDEPOSIT' 					=> '待批存款',
    'PENDINGTRANSACTION' 				=> '待批',
    'PERCENTAGE'						=> '百分比',
    'PERMISSION'						=> '权限',
    'PERSONALINFO'						=> '个人信息',


    'PHONE'							=> '电话号码',
    'PLACE'							=> '地方',
    'PLEASELOGIN'						=> '请登入',
    'PLEASESELECT'						=> '请选择一个%0',
    'PLEASESELECTANITEM'				=> '请选择至少一个项目',
    'PLEASESELECTANRECORD'				=> '请选择至少一个记录',
    'PLEASESELECTATLEAST'				=> '请选择至少一个%0',
    'PLEASESELECT_REJECTREASON'		=> '请选择拒绝原因',
    'PLEASE_INPUT'						=> '请输入 ',
    'PLEASE_SELECT'						=> '请选择',
    'PLEASE_SELECT_CSV_FILE'			=> '请选择csv文件',

    'POINT'							=> '点',
    'POSSIBLEPAYOUT' 					=> '可赢额',
    'POSTCODE'							=> '邮政区号',

    'PRICE'							=> '价钱',
    'PRIORITY'							=> '优先权',
    'PRIORITYMINOR'					=> '较小',
    'PRIORITYNORMAL'					=> '普通',
    'PRIORITYURGENT'					=> '急迫',
    'PROCESSED'						=> '审理中',
    'PROCESSING'						=> '处理中',
    'PROCESSTIME'						=> '处理时间',
    'PRODUCTCATEGORY'					=> '产品类别',
    'PRODUCT'							=> '产品',
    'PROFILE'							=> '简介',
    'PROFIT'							=> '赢',
    'PROFITLOSS'						=> '赢/输',
    'NETWINLOSS'						=> '净赢/输',
    'PROFITLOSSLOCAL'					=> '本地赢/输',

    'PROFITONLY'						=> '盈利而已',
    'PROMOCAMPAIGN'					=> '红利活动',
    'PROMOCASH'						=> '红利',
    'PROMOCASHAMT'						=> '红利数额',
    'PROMOCASHAMTLOCAL'				=> '本地红利数额',
    'PROMOCASHFROM'					=> '红利从',
    'PROMOCASHREPORT'					=> '红利报告',
    'PROMOCASHTO'						=> '红利授予',
    'PROVIDER'							=> '供应商',
    'PUBLISHER'						=> '发行者',
    'PUBLISHEROVERWRITEMESSAGE'		=> 'By checking this box, "Article Name", "Summary", and "Description Body" will be copied to other language edition.',
    'PUNTERPROFILE'					=> '投注者简介',
    'QUICKJUMP'						=> '快速跳动',


    'RANGE'							=> '范围',
    'RANGEERROR'						=> '范围到的数额必须大于范围从的数额',
    'RANGEFROM'						=> '范围从',
    'RANGETO'							=> '范围到',


    'RANK'							=> '等级',
    'REAPPLYRISKPROFILE'				=> "所有未完成的项目和他们的市场风险设置使用这外形将是根据现在的数额更新。这将重写的\\n既使市场现在使用风险设置。\\n您希望进行?",
    'REASON'							=> '原因',
    'RECORD'							=> '笔记录',

    'RECORD2'							=> '记录',
    'RECORDADDED'						=> '%0个记录增加',
    'RECORDING'						=> '语音纪录',
    'RECOVER'							=> '复原',
    'RECOVERSELECTEDBET'				=> '恢复选择的赌注?',
    'RECACULATE'						=> '重新结算',
    'RECACULATEVALIDSTAKE'				=> '重新结算有效注资',
    'RECACULATEVALIDSTAKEZERO'			=> '重新结算有效注资为零',



    'REFCODE'							=> '参考代码',
    'REFNO'							=> '参考代码',
    'REFPRICE'							=> '参考价钱',
    'REFRESH'							=> '刷新',
    'REFUND'							=> '退款',
    'REGERROR_NOINPUT'					=> '您必须输入您的%0',

    'REGERROR_USEDINPUT'				=> '这%0已被使用了。请再输入另外一个%0',
    'REGION'							=> '地区',
    'REGISTER'						=> '注册',
    'REGISTEREDON'						=> '注册时间',
    'REGISTERIP'					=> '注册IP',
    'REGISTERCHANNEL'					=> '注册管道',
    'REGISTERVALIDITY'					=> '报名截止',
    'REGISTERMEMBER'					=> '报名人数',
    'REGULATORACCESS'					=> '调整者访问',
    'REJECT'							=> '拒绝',
    'REJECTREASON'						=> '拒绝原因',
    'REJECTREASONMANAGE'				=> '拒绝原因管理',
    'REJECTED'							=> '被拒绝',
    'RELEASE'							=> '释放',
    'REMARK'							=> '备注',
	'REMARKCODE'							=> '备注码',
    'REMARKMANAGE'						=> '备注管理',
    'REMOVE'							=> '除去',
    'REMOVEALL'						=> '除去所有',
    'REOPEN'							=> '再开',

    'REPLY'							=> '回复',
    'REPORT'							=> '报表',
    'REPORTEDBY'						=> '张贴',

    'RESET'							=> '重新设置',
    'RESETPASSWORD'					=> '重新设置密码',
    'RESIDENTIALADDR'					=> '住宅地址',
    'RESOLVED'							=> '已解决',
    'RESPOND'							=> '回应',
    'RESPONDEDON'						=> '回应在',
    'RESPONDTIMEOUT'					=> '暂停回应',
    'RESPONSETIME'						=> '回应时间',
    'RESULT'							=> '赛果',
    'RESULTED'							=> '被赛果',
    'RETURNED'							=> '返回',
    'RETYPEPASSWORD'					=> '重新输入密码',
    'RECEIVEAMOUNT'					=> '接收数额',
    'REVERT'							=> '还原',



    'RISK'								=> '风险',
    'ROBOT'								=> '自动化',
    'ROLE'								=> '系统角色',
    'RUNNING'							=> '走地',
    'SAMEASABOVE'						=> '同上述一样',

    'SAMEPASS'							=> '同样密码',
    'SAMEIP'							=> '同样IP',
    'SAMEBETTABLE'						=> '同局人数',
    'FIRSTTHEREESAMEIP'				=> '前三组IP一样',
    'SAMEBETIP'						=> '同局游戏IP一样',
    'SAMEFIRSTTHEREEBETIP'				=> '同局游戏前三组IP一样',
    'SAMEDEPOSITNAME'					=> '同样存户名字',
    'SAMERES'							=> '同样区域',
    'SHORTCUTSAMEPASS'					=> '密码',
    'SHORTCUTSAMEIP'					=> 'IP',
    'SHORTCUTFIRSTTHEREESAMEIP'		=> '前三组IP',
    'SHORTCUTSAMEBETIP'				=> '游戏IP',
    'SHORTCUTSAMEFIRSTTHEREEBETIP'		=> '游戏前三组IP',
    'SHORTCUTSAMEDEPOSITNAME'			=> '户名',
    'SHORTCUTSAMERES'					=> '区域',


    'SAVE'								=> '储存',
    'SAVINGACC'						=> '储蓄户口',
    'SCHEDULE'							=> '进度表',

    'SCORE'								=> '比数',
    'SEARCH'							=> '查询',
    'SEARCH_FOR'						=> '搜寻',
    'SECOND'							=> '秒',
    'SECONDS'							=> '秒',
    'SECRETANSWER1'						=> '秘密答复1',
    'SECRETQUESTION' 						=> '私人问题',
    'SECRETQUESTION1' 						=> '您母亲的姓名?',
    'SECRETQUESTION2' 						=> '您最喜爱的书?',
    'SECRETQUESTION3' 						=> '您最喜爱的队伍?',
    'SECRETQUESTION4' 						=> '您最喜爱的电影?',
    'SECURITY'							=> '保安系统',
    'SECURITYINFO'						=> '保安资料',
    'SELECT'							=> '请选择',
    'SELECTALL'							=> '选择所有',
    'SELECTION'							=> '市场选择',
    'SELECTION_REMARK'					=> '选择评论',
    'SELECTLANGUAGE'					=> '选择语言',
    'SEQUENCENUMBER'					=> '顺序',
    'SERVER'							=> '服务器',

    'SESSIONEXPIRED'					=> '为了安全考量, 您的登入期限已过。请重新登入',
    'SETTING'							=> '设定',
    'SETTLED'							=> '已结算',
    'SETTLEMENT'						=> '注单计算',
    'SETTLEMENTDATE'					=> '注单计算日期',

    'SETTLEMENTOUTRIGHTERROR'			=> '这个夺标市场是未赛, 所有市场必须赛果在注单之前。',
    'BIGSHAREHOLDER'					=> '大股东',
    'SHAREHOLDER'						=> '股东',
    'SHAREPERCENTAGE'					=> '占成数',
    'SHORTAVERAGELAY'					=> '平均Lay',
    'SHORTBETSLIP'						=> '注单',
    'SHORTBETSLIPHOLD'					=> 'Slip/Hold',
    'SHORTMARKETPERCENTAGE'				=> 'Mkt %',
    'SHORTMULTIPLETOTALHOLD'			=> '持有(M)',
    'SHORTNAME'							=> '简名',
    'SHORTMULTIPLEPAYOUT'				=> '支付(M)',
    'SHORTPROFITLOSS'					=> '赢/输',

    'SHOW'								=> '显示',
    'SHOWCOLUMN'						=> '显示栏',
    'SHOWCOMPETITION'					=> '显示比赛',
    'SHOWHIDEMESSAGE'					=> '%0已选择项目?',
    'SHOWLEAGUE'						=> '显示联盟',
    'SHOWSPORT'							=> '显示体育',
    'SHOWTESTACCT'						=> '显示测试账号',
    'SHOWUNTIL'							=> '显示到',

    'SIGNINGIN'							=> '登入中...请稍等。',
    'SINCEREGISTERED'					=> '从已注册',
    'SINGLE'							=> '单',
    'SINGLEBET'							=> '下单注',
    'SLIPHOLD'							=> 'Slip/Hold',


    'SLOW'								=> '慢',
    'SORT'								=> '排序',
    'SORTBY'							=> '排序',
    'SOUNDALERT'						=> '音效提示',

    'SPORT'							=> '体育',
    'SPORTCODE'						=> '体育代码',
    'SPORTS'							=> '体育',
    'SPORT_PROFILE'					=> '体育简介',
    'SQLCONSOLE'						=> 'SQL控制台',
    'SQLERROR'							=> '数据库检测',
    'SQLMODE'							=> 'SQL方式',
    'SQLSTATEMENT'						=> 'SQL声明',

    'STAKE'							=> '注额',
    'STAKEMORETHAN'					=> '注额大于',
    'STAKELOCAL'						=> '本地注额',
    'STAKEPERBET' 						=> '单注注额',
    'STARTDATETIME'					=> '开赛时间',

    'STATE'							=> '直辖市或省',
    'STATUS'							=> '状态',
    'STRING'							=> '串',

    'STOP'								=> '停止',
    'SUBMIT' 							=> '提交',
    'SUBMITNEWTICKET' 					=> '提交新帖子',
    'SUBAGENT'							=> '副代理',
    'SUCESSFUL'						=> '成功',
    'SUPPORTTICKET'					=> '帖子支持',
    'SUPPORTTOOLS'						=> '支撑工能',
    'SUPPOSEDLY'						=> '假想',
    'SUMMARY'							=> '总结',
    'SUMMARYVIEW'						=> '概略看法',
    'SURVEY'							=> '市调问卷',
    'SUSPEND'							=> '停用',
    'SUSPENDED'						=> '停用',
    'SUSPENDSELECTEDROW'				=> '停用已选择纪录?',
    'SUSPICIOUSIP'						=> '可疑IP',
    'SWAPPRICE'						=> '交换价钱',
    'SWAPPRICESHORT'					=> 'SW',

    'SYSLOG'							=> '系统记录',
    'SYSTEM'							=> '系统',
    'SYSTEMCALCULATION'				=> '系统计算',

    'SYSTEMEXPIRED'					=> '为了安全考量, 您的系统已经过期了。',
    'SYSTEMMULTIMAXPRICEBREACHED' 		=> '抱歉, 您投注不成功。',
    'SYSTEMNAME'						=> '1Bet2u系統',
    'SYSTEMTIME'						=> '系统时间',
    'SYSTEMVERS'						=> '0.10',
    'SYSTEMLOG'						=> '系统记录',
    'SYSTEM_NOT_MAINTENANCES'			=> '系统不在于维修状态!',


    'SYSVAR'							=> '系统制变',
    'TAG'								=> '记号',
    'TAGMANAGE'						=> '记号管理',
    'TAGNAME'							=> '记号名称',
    'TAGCOLOR'							=> '记号颜色',
    'TELEPHONENO'						=> '电话号码',



    'TEMPBLOCK'						=> '登入暂停了%0分钟',
    'TERMS'							=> '期限',
    'TEST'							=> '测试',
    'THISMONTH'					=> '这个月',

    'TIME'							=> '时间',
    'TIMEREMAINING'					=> '剩余时间',
    'TIMEZONE'						=> 'GMT时区',






    'TITLE'							=> '称号',
    'TO'								=> '到',
    'TODAY'							=> '今日',
    'TOOL'								=> '工具',
    'TOTAL'							=> '共计',
    'TOT'								=> '总',
    'TOTALBETMEMBER'					=> '下注人数',
    'TOTALDEPOSIT'						=> '总存款',
    'TOPDEPOSIT'						=> '最高存款排名',
    'TOPWITHDRAWAL'						=> '最高提款排名',
    'TOPLOSER'							=> '最高输家排名',
    'TOPWINNER'							=> '最高赢家排名',
    'TOTALHOLD'						=> '总持有',
    'TOTALMARKETPERCENTAGE'			=> '总Mkt%',
    'TOTALPAYOUT'						=> '总支出',
    'TOTALSTAKE'						=> '总注额',
    'TOTALWITHDRAWAL'					=> '总提款',
    'TOTALBALANCE'						=> '总余额',
    'TOURNAMENT'						=> '锦标赛',
    'TRACKERLIVE'						=> '在线使用者',
    'TRACKERLOG'						=> '登入记录',
    'TRANSACTION'						=> '交易',
    'TRANSACTIONID'					=> '交易码',
    'TRANSACTIONACCEPTED'				=> '您的交易已成功被接受了',
    'TRANSACTIONAL'					=> '交易',
    'TRANSACTIONAMT'					=> '金额',
    'TRANSACTIONDATA'					=> '交易资料',
    'TRANSACTIONDATE'					=> '交易日期',
    'TRANSACTIONDETAILS'				=> '交易记录',
    'TRANSACTIONENQUIRY'				=> '交易记录',


    'TRANSACTIONSUCCESS'				=> '您的交易将会尽快被处理',
    'TRANSACTIONSUCCESSSAVEBANKINFO'	=> '您的交易将会尽快被处理。您想更新您的银行资料吗?',
    'TRANSACTIONTYPE'					=> '交易类型',
    'TRANSFERAMT'						=> '数额',
    'TRANSFERAMTLOCAL'					=> '本地数额',
    'TRANSFERDATE'						=> '调动日期',

    'TRANSFERSUCCESS'					=> '调动成功',
    'TURNOVER'							=> '洗码量',



    'TXNFEE'							=> '交易费',
    'TYPE'								=> '类别',
    'TIMES'							=> '倍',
    'UNBLOCK'							=> '解除封锁',
    'UNCHECK_ALL'						=> '全清除',
    'UNDEFINED'						=> '未定义',
    'UNDOSETTLEMENT'					=> '撤消注单',
    'UNREGISTERED' 					=> '未登记',
    'UNRESOLVED'						=> '未解決',
    'UNRESULTED'						=> '没有赛果',

    'UNSETTLEBEFOREUPDATE'				=> '注单只可在比分更新前撤消。',
    'UNSETTLED'						=> '未结算',
    'UNSETTLEDBETS'					=> '没有投注单',

    'UP'								=> '上',
    'UPDATE'							=> '更新',
    'UPDATED'							=> '已更新',
    'UPDATEEVENT'						=> '更新项目',
    'UPDATEFAIL'						=> '更新失败',


    'UPTO'								=> '上至',
    'URLPARAM'							=> 'URL参数',
    'USECUSTOM'						=> '使用自定?',

    'USER'								=> '用户',
    'USER_BEAR'						=> '用戶承担',
    'USERGROUP'						=> '用户小组',
    'USERNAME'							=> '用户名',
    'USERONLINE'						=> '网上用户',
    'USERTYPE'							=> '用户类别',

    'VALID'							=> '有效',
    'VALIDATED'						=> '确认',
    'VALIDATIONDATE'					=> '检验日期',
    'VALIDITYPERIOD'					=> '有效期限',
    'VALIDSTAKE'						=> '有效注额',
    'VALIDTOTALSTAKE'					=> '有效总注额',


    'VALUE'							=> '价值',
    'VENUE'							=> '地点',
    'VERSUS'							=> 'VS',

    'VIEW'								=> '查看',
    'VIEWMEMBER'						=> '观看会员',
    'VIEWMEMO'							=> '观看备忘录',
    'VOLUME' 							=> '数量',

    'WAGER'							=> '投注',
    'WAGERREPORT'						=> '注单报告',

    'WEB'								=> '网站',
    'WEBSITE'							=> '网站',

    'WEBSITEDESC'						=> '网站介绍',
    'WEEKLY'							=> '每周(星期三至星期二)',
    'WEEK'								=> '每周',


    'WILDCARD'							=> '(最少3个字)',
    'WIN'								=> '赢',
    'WINHALF'							=> '赢半',
    'WINNER'							=> '胜利者',
    'WITHDRAW'							=> '提取',


    'WITHDRAW_ALERT1'					=> '这个帐号提款总数超过或等于现时现金金额的一半(50%)!',
    'WITHDRAW_ALERT2'					=> '原本现金现时信用与余额',
    'WITHDRAWAL'						=> '提款',
    'WITHDRAWALCODE'					=> '提款验证码',
    'WITHDRAWAL_TOBEPROCESSED'			=> '提款申请待处理',
    'WITHDRAWALCONDITION1'				=> '下注总款 >（存款+现金优惠）X ',
    'WITHDRAWALCONDITION2'				=> '下注总款 > 余额 X 1',
    'WITHDRAWALCONDITION3'				=> '下注总款 > 最新存款',
    'WITHDRAWALCONDITION4'				=> '提款 < （存款 X 10）',
    'WITHDRAWALCONDITION5'				=> '提款时的IP与其他用户相同',
    'WITHDRAWALCONDITION6'				=> '提款 < （余额 X 2）',
    'WITHDRAWALRESTRICTION' 			=> '提款限制',
    'WITHDRAWAMT'						=> '提款数额',
    'WITHDRAWALCHARGES'					=> '提款费用',

    'WITHDRAWBALANCE'					=> '在提款以后的余额',
    'WITHDRAWBALANCELOCAL' 			=> '在提款以后的余额(AUD)',
    'WITHDRAWREPORT'					=> '提款报告',


    'WORKPHONE'						=> '工作电话号码',
    'WRONGFIELDNAME'					=> '文件专栏有无效字段名, 也许是字段名包括间距在各个字符之间! 请检查文件。戒备: "文件必须是ANSI和DOS代码!"',
    'WRONGONLY'							=> '错误而已',
    'WINLOSEMORETHAN'					=> '输赢大于',

    'YES'								=> '是',
    'YESTERDAY'						=> '昨日',

    'YEAR'								=> '年',
    'YEARTODATE'						=> '迄今年份',
    'YOURANSWER'						=> '您的答复',
    'YOURDETAILS'						=> '您的细节',

    'NEWCURRATE'						=> '新的汇率',
    'EFFECTDATE'						=> '生效日期',
    'WEBSITEURL'							=> '网站网址',
    'WEBSITENAME'							=> '网站名称',
    'COMPANYNAME'								=> '公司名称',
    'BANKBRANCHNAME' 					=> '分行公司名称',
    'BANKADDRESS' 					=> '银行地址',
    'PROFITRANGESTART' 					=> '利润范围从',
    'PROFITRANGEEND' 					=> '利润范围至',
    'ACTIVEMEMBERRANGESTART' 					=> '活跃会员范围从',
    'ACTIVEMEMBERRANGEEND' 					=> '活跃会员范围至',
    'PERCENTAGERATE' 					=> '比率',






    'NOPROFITRANGE'						=> '请提供一个有效的利润范围',
    'NOPROFITRANGEEND'						=> '利润范围从必须大于利润范围至或0',
    'INVALIDPROFITRANGE' 				=> '利润范围经已存在。请提供另一个有效的利润范围',
    'NOMEMBERRANGE'						=> '请提供一个有效的活跃会员范围',
    'NOMEMBERRANGEEND'						=> '活跃会员范围从必须大于活跃会员范围至或0',
    'SETTLECURR'						=> '结算货币',
    'NICKNAME' 					=> '昵称',
    'TELEPHONENO1'						=> '联络号码1',
    'TELEPHONENO2'						=> '联络号码2',
    'LOCATION'						=> '地址',
    'VIPSTATUS'						=> 'VIP状态',
    'COMMSCHEMECODE'						=> '佣金方案代码',
    'GAINAMTROLLOVER'						=> '累计输赢',
    'COMMROLLOVER'						=> '累计佣金',
    'COMMFORECAST'						=> '预测佣金',

    'COMMREPORT'						=> '佣金报告',
    'AFFILIATECODE'						=> '代理联盟代码',
    'GROSSWINLOSS'						=> '总输赢',
    'SUBCOST'						=> '副成本',
    'TURNOVERSTAKE'							=> '有效投注額',
    'TURNOVERSTAKE2'							=> '有效投注',
    'NETTWINLOSS'						=> '	净输赢',
    'ACTIVEMEMBERS'						=> '活跃会员',



    'COMMRATE'						    => '佣金比率',
    'COMMRATE2'						=> '佣金比例',
    'TOTALCOMM'						=> '总佣金',
    'LASTMONTH'						=> '上个月',
    'CURRENTMONTH'						=> '这个月',
    'PROCESSCOMMISSION'						=> '处理佣金',
    'PROCESSEDCOMMISSION'						=> '佣金已经处理',
    'TIMEFRAME'						=> '时限',
    'MONTHSELECTION'						=> '选择月份',
    'YEARSELECTION'						=> '选择年份',
    'COMMTRANSBREAKDOWN'						=> '交易故障',
    'Q1RESULTED'						=> '首刻',
    'HTRESULTED'						=> '半场',
    'Q3RESULTED'						=> '第三刻',
    'FTRESULTED'						=> '全场',
    'WRONGSEQUENCE'					=> '次序错误',
    'CREDITED'							=> '已经进账',
    'PRICEMIN'							=> '最低价格',
    'RESULTMIN'						=> '最低赛果',
    'MAXDEPOSIT'						=> '最高存款数额',
    'OTHERS'							=> '其它',
    'TRADEADJUSTMENT'					=> '交易调整',
    'ADJUSTMENTSEARCHCAT'				=> 'Search Categories',
    'ADJUSTMENTMETHOD'					=> '调整方式',
    'ADJUSTMENTREIMDEPOSIT'			=> '补还存款',
    'ADJUSTMENTCASHOFFER'				=> 'Cash Offer',
    'ADJUSTMENTOFRATE'					=> '调整金额',
    'TRANSACTIONCODE'					=> '交易码',
    'ADJREIMBURSEMENTDEPOSIT'			=> '交易 - 补还存款',
    'ADJCASHOFFER'						=> '交易 - Cash Offer',

    'ADJRATE'							=> '交易 - 调整金额',
    'ADJTRANSCODE'						=> 'Reimburse Deposit Transaction Code',
    'NOTE'								=> 'Note',
    'PAYMENT'							=> 'Payment',
    'ADJUSTMENTERROR'					=> 'The transaction you requested does not exist',
    'MANPENDING'						=> 'Manual Pending',
    'MANCONFIRMED'						=> '批准',
    'MANCANCELLED'						=> '手动取消',
    'ADJUSTMENTREIMBURSEERROR'			=> 'Transaction reimbursement has been done before',
    'ACCOUNTDETAILACTIVE'				=> '正常',
    'ACCOUNTDETAILSUSPENDED'			=> '会员账号已被停用',
    'BANKHOLDINGBRANCH' 				=> '银行网点',
    'SORTCODE' 						=> 'Sort Code',
    'BANKPROVINCE'						=> 'Bank Province',
    'BANKCITY' 						=> 'Bank City',
    'BANKSAVINGACCNO'					=> 'Saving Account',
    'DEFAULT'							=> '默认',
    'ADJUSTMENTACCNA'					=> '无此会员账号',
    'NORESULT'							=> 'No result',


    'ZHANG'							=> '张',

//JP begin
    'MARKETING' => '营销',
    'INSERT' => '载入',

    'GAMEPERIOD' => '游戏期限',
    'REWARD' => '回酬',

    'REPORTTOTALNEW' => '报表 （新）',
    'AFFILIATE' => '代理',
    'GAMECOUNT' => '游戏统计',
    'TAG' => '记号',
    'MARKETINGTASK' => '营销任务表',
    'CUSTOMERSEARCH' => '客户搜索',
    'POTENTIALCUSTOMERINQUIRY' => '潜在客户查询',
    'EXISTINGCUSTOMERINQUIRY' => '现有客户查询',
    'CUSTOMERFILTER' => '客户过滤器',
    'POTENTIALCUSTOMER' => '潜在客户过滤器',
    'CONDITION' => '条件',
    'SOURCE' => '源头',
    'BIRTHYEAR' => '出生年份',
    'SAVINGAMOUNT' => '储蓄数目',
    'TRANSACTIONAMOUNT' => '交易数目',
    'GAME' => '游戏',
    'PROMOTION' => '推广',
    'ACCOUNTDETAILS' => '帐户资料',
    'TRANSACTIONVIEW' => '交易栏',
    'GAMEVIEW' => '游戏栏',
    'REWARDVIEW' => '回酬栏',
    'EXPIREON' => '有效期至',
    'UPLOADCUSTOMER' => '上载客户资料',
    'POTENTIALID' => '潜在客户ID',
    'ASSIGNTO' => '指派至',
    'SECRETANSWER2' => '秘密答复2',
    'UPLOADON' => '上载于',
    'MEMBERID' => '会员ID',
    'MEMBERNAME' => '姓名',
    'DATEFROM' => '日期从',
    'TILL' => '至',
    'TODAY' => '今日',
    'YESTERDAY' => '昨日',
    'LAST7DAYS' => '前7日',
    'THISMONTH' => '本月',
    'KEYWORD' => '关键字',
    'EXACT' => '准确',
    'QQCONTACT' => 'QQ',
    'CONTACTDETAIL' => '联络资料',
    'SEARCHRESULT' => '搜索结果',
    'INTERESTED' => '有兴趣',
    'NOTINTERESTED' => '没有兴趣',
    'CONSIDERING' => '考虑',
    'CUSTOMERCOMMENT' => '客户意见',
//JP end
//


    'RECONCILATION'					=> '核对',
    'RECONCILE'						=> '核对',
    'RECONCILEINCENTIVE'				=> '核对退水优惠',
    'RECONCILEBONUS'					=> '核对现金优惠',
    'RECONCILE_NOTTALLY_AMT'			=> '数额不符',
    'RECONCILE_NOTTALLY_FEE'			=> '费用不符',
    'RECONCILE_NOTTALLY_BOTH'			=> '数额与费用不符',
    'RECONCILE_TALLY'					=> '已核对',
    'RECONCILE_NOTEXIST'				=> '交易不存在',

    'RECONCILE_STATUS'					=> '核对状况说明与摘要',
    'RECONCILATIONSUMM'				=> '核对摘要',
    'FILETOTAL'						=> '文件总数',
    'DBTOTAL'							=> '数据库资料总量',
    'TALLYTOTAL'						=> '相符总数',
    'NOTTALLYTOTAL'					=> '不相符总数',


    'REPORTCHART'						=> 'ReportChart',
    'REPORTLIST'						=> 'ReportList',
    'MEMBER_INCENTIVE'					=> 'MemIncentive',
    'POSTREPORT'						=> 'PostReport',
    'AGENT_COMMISSION'					=> 'AgentCommission',
    'AGENTCOMMISSION'					=> 'AGCommission',
    'TOTALREPORTLIST'					=> 'TotalReport',
    'SEARCH_MEM_REPORTlIST' 			=> 'SearchMem',
    'WITHDRAWAL_LIST'		            => 'WithdrawalList',
    'TRANX_AUDIT'						=> 'TranxAudit',
    'OTHERSET'							=> 'Other Set',
    'CHAT'								=> 'Chat',
    'AFFILIATELIST'					=> '代理列表',

    'ORGLIST'							=> '代理组织',
    'BALANCE_NOTICE'					=> '负数通知',
    'ACCBALANCE_BFW_FAIL'				=> '帐户余额结转失败',


    'ACCBALANCE_BFW'					=> '帐户余额结转',

    'BETTYPE1X2'						=> '独赢',
    'BETTYPEASIAHANDICAP'				=> '让分',
    'BETTYPEOVERUNDER'					=> '大小',
    'BETTYPEODDEVEN'					=> '单双',
    'BETTYPECORRECTSCORE'				=> '波胆',
    'BETTYPETOTALGOAL'					=> '总入球',
    'BETTYPEASIAHANDICAPRB'			=> '走地让分',
    'BETTYPEOVERUNDERRB'				=> '走地大小',
    'BETTYPEODDEVENRB'					=> '走地单双',
    'BETTYPEPARLAY'					=> '过关',
    'BETTYPEPARLAYHANDICAP'			=> '过关让分',
    'BETTYPEHALFTIMEFULLTIME'			=> '半全场',


    'SELECTIONHOME'					=> '主场',
    'SELECTIONAWAY'					=> '客场',
    'SELECTIONDRAW'					=> '和',
    'SELECTIONOVER'					=> '大',
    'SELECTIONUNDER'					=> '小',
    'SELECTIONODD'					 	=> '单',
    'SELECTIONEVEN'					=> '双',
    'SELECTIONH0C0'					=> '0:0',
    'SELECTIONH1C1'					=> '1:1',
    'SELECTIONH2C2'					=> '2:2',
    'SELECTIONH3C3'					=> '3:3',
    'SELECTIONH4C4'					=> '4:4',
    'SELECTIONH1C0'					=> '1:0',
    'SELECTIONH2C0'					=> '2:0',
    'SELECTIONH2C1'					=> '2:1',
    'SELECTIONH3C0'					=> '3:0',
    'SELECTIONH3C1'					=> '3:1',
    'SELECTIONH3C2'					=> '3:2',
    'SELECTIONH4C0'					=> '4:0',
    'SELECTIONH4C1'					=> '4:1',
    'SELECTIONH4C2'					=> '4:2',
    'SELECTIONH4C3'					=> '4:3',
    'SELECTIONOVH'						=> '5:0 UP',
    'SELECTIONH0C1'					=> '1:0',
    'SELECTIONH0C2'					=> '2:0',
    'SELECTIONH1C2'					=> '2:1',
    'SELECTIONH0C3'					=> '3:0',
    'SELECTIONH1C3'					=> '3:1',
    'SELECTIONH2C3'					=> '3:2',
    'SELECTIONH0C4'					=> '4:0',
    'SELECTIONH1C4'					=> '4:1',
    'SELECTIONH2C4'					=> '4:2',
    'SELECTIONH3C4'					=> '4:3',
    'SELECTIONOVC'						=> '0:5 UP',
    'SELECTION0~1'						=> '0~1',
    'SELECTION2~3'						=> '2~3',
    'SELECTION4~6'						=> '4~6',
    'SELECTION7UP'						=> '7UP',
    'SELECTIONFHH'						=> '主/主',
    'SELECTIONFHD'						=> '主/和',
    'SELECTIONFHC'						=> '主/客',
    'SELECTIONFDH'						=> '和/主',
    'SELECTIONFDD'						=> '和/和',
    'SELECTIONFDC'						=> '和/客',
    'SELECTIONFCH'						=> '客/主',
    'SELECTIONFCD'						=> '客/和',
    'SELECTIONFCC'						=> '客/客',
































































































































































































































































































































































































































































    'FIRSTHALF'						=> '上半场',

    'CANCELBETREASON1' 				=> '球头错误',
    'CANCELBETREASON2' 				=> '赔率错误',
    'CANCELBETREASON3' 				=> '走地球头错误',
    'CANCELBETREASON4' 				=> '走地赔率错误',
    'CANCELBETREASON5' 				=> '走地分数错误',
    'CANCELBETREASON6' 				=> '赛程错误',
    'CANCELBETREASON7' 				=> '红卡错误',
    'CANCELBETREASON8' 				=> '赛事取消',
    'CANCELBETREASON9' 				=> '系统错误',
    'CANCELBETREASON10' 				=> '走地进球销单',
    'CANCELBETREASON11'				=> '系统注销单',

    'DAYOFWEEK1'						=> '星期一',
    'DAYOFWEEK2'						=> '星期二',
    'DAYOFWEEK3'						=> '星期三',
    'DAYOFWEEK4'						=> '星期四',
    'DAYOFWEEK5'						=> '星期五',
    'DAYOFWEEK6'						=> '星期六',
    'DAYOFWEEK7'						=> '星期日',

    'REGERROR_NAME_SHORT'				=> 'Name too short',
    'REGERROR_NAME_EMPTY'				=> 'Name is required',
    'REGERROR_ACCOUNT_SHORT'			=> 'Account code too short',
    'REGERROR_ACCOUNT_EMPTY'			=> 'Account code is required',
    'REGERROR_ACCOUNT_TAKEN'			=> 'Your account code is already in used.',
    'REGERROR_PASSWORD_SHORT'			=> 'Password too short',
    'REGERROR_PASSWORD_EMPTY'			=> 'Password is required',
    'REGERROR_CONFIRM_EMPTY'			=> 'Confirm password is required',
    'REGERROR_CONFIRM_NOMATCH'			=> 'Confirm password does not match',
    'REGERROR_WITHDRAWAL_SHORT'		=> 'Withdrawal code is too short',
    'REGERROR_WITHDRAWAL_ALNUM'		=> 'Withdrawal code must be alphanumeric',
    'REGERROR_WITHDRAWAL_EMPTY'		=> 'Withdrawal code is required',
    'REGERROR_PHONE_EMPTY'				=> 'Phone number is required',
    'REGERROR_PHONE_TAKEN'			 	=> 'Your phone number is already in used.',
    'REGERROR_EMAIL_EMPTY'				=> '邮箱是必填项目',
    'REGERROR_EMAIL_TAKEN'			 	=> '邮箱已被注册',
    'REGERROR_ANSWER_EMPTY'			=> 'Answer is required',
    'REGERROR_CAPTCHA_SHORT'			=> 'Captcha code is too short',
    'REGERROR_CAPTCHA_ALNUM'			=> 'Captcha code must be alphanumeric',
    'REGERROR_CAPTCHA_EMPTY'			=> 'Captcha code is required',

    'ADJERROR_DEPOSITNOTFOUND'			=> '无法查询此交易',
    'ADJERROR_DEPOSITPENDING'			=> '存款尚未确认，不能补还',
    'ADJERROR_DEPOSITCONFIRMED'		=> '存款已经确认，不能补还',
    'ADJERROR_DEPOSITADJUSTED'			=> '存款已经补还',

    'INVALIDFILEFORMAT'				=> '档案格式不对',

    'CTABNAME' 						=> '名称',
    'CTABNAMENOTE' 					=> '所有提款都使用这个名称',
    'CTABPASSWORD' 					=> '设定密码',
    'CTABCHANGEPASSWORD' 				=> '更改登入密码',
    'CTABCONFIRMPASSWORD' 				=> '确认密码',
    'CTABPASSWORDNOTE' 				=> '长度在6至12个字符，必须英文字与数字混合(AZ，az，0-9)',
    'CTABWITHDRAWALCODE' 				=> '提款验证码',
    'CTABCHANGEWITHDRAWALCODE' 		=> '更改提款验证码',
    'CTABCURRENCY' 					=> '货币',
    'CTABBIRTHDAY' 					=> '生日',
    'CTABBIRTHDAYNOTE' 				=> '年- 月- 日',
    'CTABGENDER' 						=> '性别',
    'CTABCONTACTINFO' 					=> '联络资料',
    'CTABCOUNTRY' 						=> '国家',
    'CTABADDRESS' 						=> '地址',
    'CTABCITY' 						=> '城市',
    'CTABSTATE'						=> '状态',
    'CTABPOSTCODE' 					=> '邮区号',
    'CTABPOSTCODENOTE' 				=> '五个或以上数字',
    'CTABPHONENUMBER' 					=> '电话号码',
    'CTABPHONENUMBERNOTE' 				=> '例如: 012-3456789',
    'CTABEMAIL' 						=> '邮箱',
    'CTABREMARK' 						=> '备注',
    'CTABREQUIRED' 					=> '必填项目',
    'CTABOTHER' 						=> '其它',
    'CTABCASHBALANCE' 					=> '现金结余',
    'CTABSTATUS' 						=> '状况',
    'CTABSECURITYQUESTION' 			=> '私人问题',
    'CTABSECURITYANSWER' 				=> '私人答案',
    'CTABAGENT' 						=> '代理',
    'CTABTOTALDEPOSIT' 				=> '会员总存款',
    'CTABTOTALWITHDRAWAL' 				=> '会员总提款',
    'CTABLASTLOGINTIME' 				=> '最后一次登入时间',
    'CTABLASTLOGINIP' 					=> '最后一次登入IP',
    'CTABREGISTERIP' 					=> '注册IP',
    'CTABWITHDRAWAL' 					=> '提款',
    'CTABWITHDRAWALLIMIT' 				=> '每日最高提款次',
    'CTABWITHDRAWALMAX' 				=> '每日最高提款',
    'CTABDEPOSITMAX' 					=> '每日最高存款',
    'CTABBANKSETTING' 					=> '银行设定',
    'CTABNO' 							=> '序',
    'CTABCODE' 						=> '代码',
    'CTABBANK' 						=> '銀行',
    'CTABBANKACCOUNT' 					=> '银行账号',
    'CTABTAG' 							=> '记号',
    'TRANSACTIONSEARCH' 				=> '会员交易搜索',

    'SPORTSBOOK' 						=> '体育竞猜',
    'MINBET' 							=> '单注最低',
    'MAXBET' 							=> '单注最高',
    'MAXMATCH' 						=> '单场最高',
    'BONUSLIMIT' 						=> '现金优惠限制',
    'NOOFTIMES'						=> '次数',

    'MEMBERINCENTIVE'					=> '会员退水',
    'AFFILIATEINCENTIVE'				=> '代理退水',
    'TOTALINCENTIVE'					=> '总退水',
    'FINALINCENTIVE'					=> '最终退水',
    'UPLOAD'							=> '上载',
    'CALLOUT'							=> '电访',
    'CALLOUTENQUIRY'					=> '电访查询',
    'CALLOUTWORKSHEET'					=> '电访工作表',
    'UPLOADFILE'						=> '上载档案',
    'UPLOADCALLOUT'					=> '上载名单',
    'NOACTION'							=> '没有动作',
    'PAUSEGAMING'						=> '暂停游戏',
    'RELEASEPAUSE'						=> '解除暂停',
    'MANAGEBANKACCOUNT'				=> '帐户管理',
    'LEVEL'							=> '层级',
    'REPORTSIMPLE'                     => '报表总结',
    'REPORTTOTAL'                      => '报表',
    'TAGMEMBER' 						=> '标记帐户',
    'ENQUIRY' 							=> '查询',
    'PROCESS' 							=> '处理',
    'WAGERNUM'							=> '注单数量',
    'PAYMENTACCOUNT'					=> '存款账户',
    'INCLUDEFEE'						=> '纳入费用',
    'PLEASESELECTPAYMENTACCOUNT'		=> '请选择出款帐户',

    'BANKER'    						=> '庄',
    'PLAYER'    						=> '闲',
    'TIE'     							=> '和',
    'PBANKER'    						=> '庄对',
    'PPLAYER'    						=> '闲对',
    'BIG'     							=> '大',
    'SMALL'    						=> '小',
    'BACCARAT'    						=> '百家乐',
    'ROLLETE'    						=> '轮盘',
    'SHAIBAO'    						=> '骰宝',
    'LONGHU'    						=> '龙虎',
    'FANTAN'    						=> '番摊',
    'DRAGON'    						=> '龙',
    'TIGER'    						=> '虎',
    'FAN'     							=> '番',
    'JIAO'    							=> '角',
    'SANMEN'    						=> '三门',
    'NIAN'    							=> '念',
    'TONG'    							=> '通',
    'ODD'     							=> '单',
    'EVEN'   			 				=> '双',
    'QUANWEI'    						=> '全围',
    'SANJUN'    						=> '三军',
    'DUANPAI'    						=> '短牌',
    'CHANGPAI'    						=> '长牌',
    'WEISE'    						=> '围骰',
    'DIANSHU'    						=> '点数',
    'ZHIZHU'    						=> '直注',
    'FENZHU'    						=> '分注',
    'JIEZHU'    						=> '街注',
    'JIAOZHU'    						=> '角注',
    'XIANZHU'    						=> '线注',
    'SANSHU'    						=> '三数',
    'HANGZHU1'    						=> '行注一',
    'HANGZHU2'    						=> '行注二',
    'HANGZHU3'    						=> '行注三',
    '4NUMBER'    						=> '四个号码',
    'DAZHU1'    						=> '打注一',
    'DAZHU2'   						=> '打注二',
    'DAZHU3'    						=> '打注三',
    'HONGSE'    						=> '红色',
    'HEISE'    						=> '黑色',
    'TABLENUM'							=> '桌号',
    'SHOENUM'							=> '靴号',
    'GAMENUM'							=> '局号',

    'CHECKSAMENAMEACCOUNT'				=> '检查会员名称',
    'CHECKCURRENTBALANCE'				=> '检查现时余额',
    'CHECKALLBALANCE'					=> '检查会员余额',

    'REPORTALL'						=> '总帐',
    'REPORTCATEGORY'					=> '分类帐',
    'REPORTDETAIL'						=> '明细帐',
    'MEMBERPNL'						=> '会员赢/输',
    'COMPANY'							=> '公司',
    'MARKETSHARE'						=> '市占率',
    'BATCH'							=> '批量',
    'FEEREPORT'						=> '费用统计',
    'INCENTIVEREPORT'					=> '退水优惠表',
    'TRANSUMMARY'						=> '交易总结',
    'TRANADJUST'						=> '交易管理',
    'BONUSSETTING'						=> '优惠设定',
    'INCENTIVESETTING'					=> '退水设定',
    'CAllOUTSETTING'					=> '电访设定',
    'EXPORTTRANSACTION'				=> '导出交易记录',
    'EXPORTACCOUNT'					=> '导出账号资料',
    'USERMANAGE'						=> '用戶管理',
    'AFFILIATEDETAIL'					=> '代理资料',
    'AFFILIATEENQUIRY'					=> '代理查询',
    'AFFILIATEREPORT'					=> '红股代理报表',
    'AFFILIATECOMMISSION'				=> '代理佣金',
    'AFFILIATETRANSACTION'				=> '代理交易',
    'MOVEMEMBER'						=> '会员转移',
    'BANKCARD'							=> '银行卡',
    'MINSTAKE'							=> '下限',
    'MAXSTAKE'							=> '上限',
    'CALCULATEINCENTIVE'				=> '计算退水',
    'INCENTIVECAMPAIGN'				=> '退水活动',
    'OPENSAMENAME'						=> '开放同姓名',
    'OVERMAXIMUMAMOUNT'				=> '金额超过封顶值 %0',
    'INTERNETTRANSFER'					=> '网上转账',
    'ATMTRANSFER'						=> 'ATM转帐',
    'CASHTRANSFER'						=> '现金存款',
    'MAINTENANCE'						=> '维护中',
    'OTHERBANK'						=> '跨行',
    'ASSIGNDATETIME'					=> '分配时间',
    'REMINDER'							=> '提醒',
    'COMPLETED'						=> '完成',
    'BLACKLISTED'						=> '黑名单',
    'MAINWALLET' 						=> '主要钱包',
    'SPORTWALLET' 						=> '体育',
    'CASINOWALLET' 					=> '真人视讯',
    'KENOWALLET' 						=> '快乐彩',
    'DAILYDEPOSIT' 					=> '每日存款',
    'DEPOSITREBATE' 					=> '存款回馈',
    'TRIALACCOUNT' 					=> '测试账号',

    'PAYMENTGATEWAY'					=> '支付平台',
    'PAYMENTGATEWAY1'					=> '支付宝',
    'PAYMENTGATEWAY2'					=> '财付通',
    'PAYMENTGATEWAY3'					=> '网银在线',
    'PAYMENTGATEWAY4'					=> '快钱',
    'PAYMENTGATEWAY5'					=> '易宝',
    'PAYMENTGATEWAY6'					=> '环迅',

    'COMMISSIONTYPE_1'					=> '注册',
    'COMMISSIONTYPE_2'					=> '分成',
    'COMMISSIONTYPE_3'					=> '退水',

    'LUCKYDRAW'						=> '抽奖',
    'LUCKYBll'							=> '抽奖球',
    'LUCKYDRAWID'						=> '期数',
    'LUCKYDRAWNO'						=> '抽奖期号',
    'LUCKYDRAWSETTING'					=> '抽奖设定',
    'LUCKYDRAWRESULTSETTING'			=> '开奖设定',
    'LUCKYEND'							=> '結束',
    'LUCKYPENDING'						=> '等待',
    'LUCKYPRIZE'						=> '奖金',
    'LUCKYPRIZETEXT'					=> '奖金信息',
    'LUCKYPRIZESETTING'				=> '奖金设定',
    'LUCKYRESULT'						=> '結果',
    'LUCKYSTART'						=> '開始',
    'LUCKYTIMENO'						=> '次数',
    'LUCKYANNOUNCEMENT'				=> '公告',
    'LUCKYTOTALNUMBER'					=> '縂數',
    'LUCKY_YES'						=> '是',
    'LUCKY_NO'							=> '否',


    'SAMEIPCHECK'						=> '查询相同IP',
    'SAMEIP'							=> '相同IP',
    'MAINTENANCES'						=> '维修中',

    'TOTALNUM'							=> '总数',
    'CHANGES'							=> '变化',
    'CASINO'							=> '真人',
    'KENO'								=> '快乐彩',
    'AVERAGE'							=> '平均',
    'AVERAGEAMOUNT'					=> '平均金额',
    'AVERAGETRANS'						=> '平均笔数',
    'REGISTRATION'						=> '注册',
    'EVENTREVIEW'						=> '活动分析',
    'BEFOREEVENT'						=> '活动前期',
    'PREEVENT'							=> '活动宣传',
    'ONEVENT'							=> '活动期间',
    'POSTEVENT'						=> '活动后期',
    'EVENTTURNOVER'					=> '打码',
    'EVENTACTIVATE'					=> '激活',
    'TRACEROUTE'						=> '路由追踪',
    'TRANSACTINFORM'                   => '交易通知',
    'TRANSFERINFORM'                   => '转帐通知',
    'OLDMEM_CALLOUT'                   => '旧会员电访',
    'DEPOSITREPORT'                    => '存款报表',
    'DAILYSUMMARY'                     => '每日总结',
    'FLAGMEMREPORT'                    => '插旗会员报表',
    'AUDITREPORT'                      => '审核报表',
    'AGENTREG'                         => '代理申请',
    'AGENTAD'                          => '代理广告',
    'IPAUDIT'                          => 'IP审核',
    'CMS'                              => '文章管理',
    'RECONCILE_TRANS'                  => '核对交易',
    'KINGACCOUNT_ENQUIRY'              => '赌王帐号查询',
    'SURVEY_ANALYSIS'                  => '问卷分析',
    'ADVISE_ANALYSIS'                  => '建议分析',
    'SURVEY_LUCKDRAW'                  => '问卷抽奖',
    'LUCKYDRAWLIST'                    => '抽奖名单',
    'LUCKYDRAWLIST_ENQUIRY'            => '抽奖名单查询',
    'EVENTREVIEW2'						=> '活动分析2',
    'ISSUENO'						=> '问题编号',
    'ACTIVATEALL'						=> '激活全部',
    'SUSPENDALL'						=> '停用全部',
    'INFORMATION'	    			=> '资料',
    'REGFEE'	    				=> '报名收费',
    'PACKAGE'	    				=> '配套',
    'CHIP'	    					=> '筹码',
    'TIME_MINUTES'	    			=> '时间（分钟）',
    'EVENT_TITLE'					=> '活动',
    'EVENT_REPORT'					=> '活动报表',

//game name
    'GAMESPONGEBOB'					=> '海绵宝宝',
    'GAMEMARIO'						=> '小玛莉',
    'GAMESANGUO'						=> '三国无双',
    'GAMEMONSTER'						=> '怪兽拉霸',
    'GAMEEGYPT'						=> '埃及拉霸',
    'GAMEWOMEN'						=> '女人我最大',
    'GAMEAFRICA'						=> '非洲乐园',
    'GAMEARMY'							=> '军火之王',
    'GAMEHIGHWAY'						=> '快车大盗',
    'GAMESNOW'							=> '冰雪奇迹',
    'GAMEEBACCARAT'					=> '电子百家乐',

    'SEDIE'    						=> '色碟',
    'SEDIE4RED'    					=> '4红',
    'SEDIE4WHITE'    					=> '4白',
    'SEDIE1RED3WHITE'    				=> '1红3白',
    'SEDIE2RED2WHITE'    				=> '2红2白',
    'SEDIE3RED1WHITE'    				=> '3红1白',
    'AFFFIRSTDEPOSIT' 					=> '代理首存',
    'TOTALPLACE' 						=> '名額',
    'INVALID' 							=> '无效',
    'NOTVERIFIED' 						=> '未确认',
    'OPERATEREMARK'                    => '操作记录',
    'REMOVESTATUS'						=> '移除',
    'REQUESTFEEDBACK'                  => '要求回复',
    'REPLIED'                  	    => '已回复',
    'CLOSED'                  		    => '已关闭',
    'REG_AMOUNT'                       => '注册人数',
    'BETSIZE'                          => '投注量',
    'REGISTERED'                       => '注册会员',
    'EFFECTIVEMEM'                     => '有效会员',
    'TOTALBETS'						=> '总投注',
    'SPONSORPRIZE'						=> '推荐奖金',
    'LASTCUMULATIVE'					=> '上月累计',
    'FIRSTDEPOSIT'				    	=> '首存',
    'CONTDEPOSIT'				    	=> '续存',

    'RISKPROFILE'						=> '风控',
    'HIGHEST'							=> '最高',
    'HIGH'								=> '高',
    'MEDIUM'							=> '中',
    'LOW'								=> '低',
    'LOWEST'							=> '最低',

    'SUCCESS'							=> '成功',
    'FAILED'							=> '失败',
    'ERROR'							=> '错误',
    'BETMEMBER'                        => '投注会员',
    'FIRSTDEPMEMBER'					=> '首存会员',
    'CONTDEPMEMBER'				    => '续存会员',
    'PAYOUTCOMMISSION'				    => '派出佣金',
    'DISTRIBUTE'				        => '派发',
    'AGREE'				            => '同意',
    'OPINION'				            => '意见',
    'ACCUMULATE_WINLOST'		        => '累积输赢',
    'MONTHLYWINLOST'				    => '本月输赢',
    'LASTWINLOST'			    	    => '上月输赢',
    'MONTHLYFEE'				        => '本月费用',
    'LASTFEE'  				        => '上月费用',
    'ACCUMULATE_FEE'				    => '累积费用',
    'CLICKS'			    	        => '点击数',

    'WHEEL'			    	        => '轮盘抽奖',
    'WHEELPRIZE'			    	    => '轮盘奖品',
    'WHEELTOKEN'			    	    => '轮盘名单',
    'WHEELREPORT'			    	    => '轮盘报表',
    'PRIZE'			    	   	    => '奖品',

	'PRODUCTSPB'			    	    => '体育',
	'PRODUCTLVC'			    	    => '真人赌场',
	'PRODUCTNUM'			    	    => '彩票',
	'PRODUCTFIN'			    	    => 'Finances',
	'PRODUCTRNG'			    	    => '老虎机',
	'PRODUCTLOTTERY'			      	=> 'Lottery',
	'PRODUCTP2P'			    	    => 'P2P',

    'MAINTENANCEMSG'			    	=> 'Maintenance Message',
    'TEMPLATE'			    	    => 'Template',
    'PREFIX'			    	 	  	=> 'Prefix',
    'WITHDRAWALLIMIT'					=> 'Withdrawal Limit',
    'LASTDEPOSIT'						=> '最后存款时间',
    'MEMBERENQUIRY'					=> '会员查询',
    'MAXIMUM'							=> 'Maximum',
    'MINIMUM'							=> 'Minimum',
    'MAXPAYOUT'						=> 'Max Payout',
    'MINPAYOUT'						=> 'Min Payout',
    'CODENOTMATCH'					=> 'Verification code not match',
    'TRANSFERRED'						=> 'Transferred',
    'DISPLAY'							=> 'Display',
    'PERCENTAGEAMOUNT'				=> 'Percentage / Amount',
    'OPENBETS'							=> 'Open Bets',
    'PREREQUISITE'						=> 'Prerequisite',
    'SUBTOTAL'							=> 'Sub Total',
    'REGISTRATIONSHORT'					=> 'Reg.',
    'APPLIED'							=> 'Applied',
    'APPROVED'							=> 'Approved',
    'GROSSBALANCE'						=> 'Gross Balance',
    'BANKSUMMARY'						=> '银行总结',
    'TRANSFER'							=> 'Transfer',
    'BANKTRANSACTION'					=> 'Bank Transaction',
    'MEMBERREPORT'						=> '会员报表',
    'CHANNELREPORT'						=> '渠道报表',
    'BONUSREPORT'						=> '奖金报表',
    'REFERRALREPORT'					=> '介绍报表',
    'MEMBERWALLETREPORT'				=> '会员钱包报表',
    'AGENTREPORT'						=> '占成代理报表',
    'BANNERMANAGE'						=> '网页广告',
    'APILOG'							=> '接口日记',
    'ERRORLOG'							=> '错误日记',
    'INSUFFICIENTBALANCE'				=> 'Insufficient balance',
    'RATE'								=> 'Rate',
    'INCENTIVEAPPROVED'					=> 'Rebate Approved',
    'EFFECTIVE'							=> 'Effective',
    'CONFIRMATION'						=> 'Confirmation',
    'MARGINPERCENTAGE'					=> 'Margin %',
    'SYSTEMBALANCE'						=> 'System Balance',
    'WALLET'							=> 'Wallet',
    'WALLETBALANCE'						=> 'Wallet Balance',
    'ARTICLE'							=> 'Article',
    'GENERAL'							=> 'General',
    'EXCERPT'							=> 'Excerpt',
    'IMAGE'								=> 'Image',
    'THUMBNAIL'							=> 'Thumbnail',
    'LOGO'								=> 'Logo',
    'BETDETAILS'						=> 'Bet Details',
    'REFNOSHORT'						=> 'Ref. No',
    'TIMELAPSED'						=> 'Time Lapsed',
    'PRODUCTMAINTENANCE'				=> '产品维修',
    'NETPROFITLOSS'						=> 'Net PNL',
    'AFFILIATEREPORT2'					=> 'Affiliate Report (2)',
    'WINLOSE'							=> '输/赢',
    'UPLINE'							=> '上线',
    'AGENTLINK'							=> '代理链接',
    'AFFILIATELINK'						=> '代理链接',
    'ALLOW'								=> '允许',
    'DISALLOW'							=> '不允许',
    'GAMETYPESUMMARY'					=> '游戏类型总结',
    'PRODUCTSUMMARYBYPROVIDER'			=> '产品总结',
    'PLEASERESETPASSWORD'				=> '请设置您的密码',
    'SMSLOGREPORT'						=> 'SMS 日志',
    'SMS'								=> 'SMS',
    'VIEWWINLOSSREPORT'					=> '查看输赢报表',
    'DEPOSITSUCESSFULREQUEST'			=> '成功提交存款申请，稍后片刻等候款项审核入帐',
    'WITHDRAWALSUCESSFULREQUEST'		=> '成功提交提款申请，稍后片刻等候款项审核入帐',
	'WITHDRAWALCARDSUCESSFULREQUEST'	=> '在交易记录中获取您的提款代码',
	'QRCODE'							=> 'QR图',
	'NOTRECOMMENDUSEWECHATTOSCAN'		=> '为了更好的体验，请勿使用微信扫描。',
    'PAYMENTGATEWAYSETTING'				=> '支付平台设定',
	'DISTRICT'							=> '区',
	'ALIPAYTRANSFER'					=> '支付宝',
	'GAMETYPE'						    => '游戏类型',
	'EXPORT'						    => '导出',
	'MANUAL'						    => '手动',
    'PRIVATEPAYMENTGATEWAY'				=> '私人支付平台',
    'MANUALPAYMENTGATEWAY'				=> '手动支付平台',
    'PRIVATEPAYMENTGATEWAYSETTING'		=> '私人支付平台设定',
    'AUTOPAYMENTGATEWAYSETTING'		=> '自动支付设定',
    'MANUALPAYMENTGATEWAYSETTING'		=> '手动支付设定',
    'VENDOR'							=> '供应商',
	'DAILYREBATE'						=> '每日退水',
	'DAILYREBATEREPORT'					=> '每日退水报表',
    "NEWUSERNOTICE"                     => "有新的账户名 :name已注册",
]

?>
