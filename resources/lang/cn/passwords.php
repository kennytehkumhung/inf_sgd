<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Password Reminder Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are the default lines which match reasons
	| that are given by the password broker for a password update attempt
	| has failed, such as for an invalid token or invalid new password.
	|
	*/

	"password" => "密码必须至少六个字符，并且匹配确认的密码。",
	"user" => "找不到该用户。",
	"token" => "无效的密码重置令牌（password reset token）。",
	"sent" => "密码重置链接已发送到您的电邮信箱。",
	"reset" => "密码重置成功。",
	"failed" => "账号未激活。",

];
