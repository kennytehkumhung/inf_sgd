<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Password Reminder Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are the default lines which match reasons
	| that are given by the password broker for a password update attempt
	| has failed, such as for an invalid token or invalid new password.
	|
	*/

	"password" => "密碼必須至少六個字符，並且匹配確認的密碼。",
	"user" => "找不到該用戶。",
	"token" => "無效的密碼重置令牌（password reset token）。",
	"sent" => "密碼重置鏈接已發送到您的電郵信箱。",
	"reset" => "密碼重置成功。",
	"failed" => "賬號未激活。",

];
