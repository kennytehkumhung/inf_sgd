<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines contain the default error messages used by
	| the validator class. Some of these rules have multiple versions such
	| as the size rules. Feel free to tweak each of these messages here.
	|
	*/

	"accepted"             => ":attribute 必須被接受。",
	"active_url"           => ":attribute 是無效的URL。",
	"after"                => ":attribute 必須是 :date 之後的日期。",
	"alpha"                => ":attribute 只能接受字母（letters）。",
	"alpha_dash"           => ":attribute 只能接受字母，數字，與破折號（letters, numbers, and dashes）。",
	"alpha_num"            => ":attribute 只能接受字母與數字（letters and numbers）。",
	"array"                => ":attribute 必須是排列（array）。",
	"before"               => ":attribute 必須是 :date 之前的日期。",
	"between"              => [
		"numeric" => ":attribute 必須於 :min 與 :max 之間。",
		"file"    => ":attribute 必須於 :min 與 :max 千字節（kilobytes）之間。",
		"string"  => ":attribute 必須於 :min 與 :max 字符（characters）之間。",
		"array"   => ":attribute 項目（items）數量必須於 :min 與 :max 之間。",
	],
	"boolean"              => ":attribute 只能接受 “true” 與 “false”。",
    "captcha"              => "無效的驗證碼。",
	"confirmed"            => ":attribute 與目標不匹配。",
	"date"                 => ":attribute 日期格式不正確。",
	"date_format"          => ":attribute 格式不匹配：:format",
	"different"            => ":attribute 與 :other 必須不同。",
	"digits"               => ":attribute 數字必須是：:digits。",
	"digits_between"       => ":attribute 數字必須與 :min 和 :max 之間。",
	"email"                => ":attribute 必須是正確的電郵地址。",
	"filled"               => ":attribute 不能留空。",
	"exists"               => "無效的選項：:attribute",
	"image"                => ":attribute 只能接受圖像文件。",
	"in"                   => "無效的選項：:attribute",
	"integer"              => ":attribute 只能接受整數。",
	"ip"                   => ":attribute 必須是正確的IP地址。",
	"max"                  => [
		"numeric" => ":attribute 不能大於 :max。",
		"file"    => ":attribute 不能大於 :max 千字節（kilobytes）。",
		"string"  => ":attribute 不能大於 :max 字符（characters）。",
		"array"   => ":attribute 項目（items）數量不能大於 :max 。",
	],
	"mimes"                => ":attribute 只接受此類型的附件：:values",
	"min"                  => [
		"numeric" => ":attribute 不能小於 :min。",
		"file"    => ":attribute 不能小於 :min 千字節（kilobytes）。",
		"string"  => ":attribute 不能小於 :min 字符（characters）。",
		"array"   => ":attribute 項目（items）數量不能小於 :min 。",
	],
	"not_in"               => "無效的選項：:attribute 。",
	"numeric"              => ":attribute 只能接受數字。",
	"regex"                => ":attribute 正則表達式格式不正確。",
	"required"             => ":attribute 不能留空。",
	"required_if"          => "當 :other 是 :value 的時候，:attribute 不能留空。",
	"required_with"        => "當 :values 呈現的時候，:attribute 不能留空。",
	"required_with_all"    => "當 :values 呈現的時候，:attribute 不能留空。",
	"required_without"     => "當 :values 不存在的時候，:attribute 不能留空。",
	"required_without_all" => "當 :values 不存在的時候，:attribute 不能留空。",
	"same"                 => ":attribute 與 :other 必須匹配。",
	"size"                 => [
		"numeric" => ":attribute 必須為 :size。",
		"file"    => ":attribute 必須為 :size 千字節（kilobytes）。",
		"string"  => ":attribute 必須為 :size 字符（characters）。",
		"array"   => ":attribute 項目（items）數量必須為 :size 。",
	],
	"unique"               => ":attribute 已經被使用。",
	"url"                  => ":attribute URL格式不正確。",
	"timezone"             => ":attribute 必須是正確的時區。",
	"reserved_str"         => ":attribute 不被接受。",
	"username_exist"       => ":attribute 已被使用。",
	"email_exist"     	   => ":attribute 已被使用。",
	"phone"     	  	   => "無效的手機號碼。",
	"newpass_same"     	   => "新密碼不能與舊密碼相同。",
	"oldpass_wrong"        => "舊密碼不正確。",
	"newandconpass_notsame"   => "新密碼與確認新密碼不一樣。",
	"wrong_captcha"        => "無效的驗證碼。",
	"login_fail"     	   => "登入失敗。",
	"wallet_error1"        => "錢包不能相同。",
	"wallet_error2"        => "餘點不足。",
	

	/*
	|--------------------------------------------------------------------------
	| Custom Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| Here you may specify custom validation messages for attributes using the
	| convention "attribute.rule" to name the lines. This makes it quick to
	| specify a specific custom language line for a given attribute rule.
	|
	*/

	'custom' => [
		'attribute-name' => [
			'rule-name' => 'custom-message',
		],	
		'rules' => [
			"required"      => "請閱讀並同意存款優惠規則。",
		],
	],

	/*
	|--------------------------------------------------------------------------
	| Custom Validation Attributes
	|--------------------------------------------------------------------------
	|
	| The following language lines are used to swap attribute place-holders
	| with something more reader friendly such as E-Mail Address instead
	| of "email". This simply helps us make messages a little cleaner.
	|
	*/

	'attributes' => [],

];
