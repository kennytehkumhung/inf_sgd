<?php

return [
    "AboutUs" 									=> "關於我們",
    "Account" 									=> "賬戶",
    "AccountCreated" 							=> "賬戶已創建！請登入。",
    "AccountDetails" 							=> "賬戶資料",
    "AccountHolder" 							=> "賬戶持有人姓名",
    "AccountName" 								=> "帳戶姓名",
    "AccountNumber" 							=> "帳戶號碼",
    "AccountUpdateFailed" 						=> "*更新資料失敗",
    "AccountUpdateSuccess" 						=> "*更新資料成功",
    "Adjustment" 								=> "*調整",
    "Agree" 									=> "同意",
    "AgreementStatementRules" 					=> "協議聲明規則",
    "All" 										=> "全部",
    "AllMembersAreAllowedToWithdrawXTimePerDay" => "會員一天只能提點 :p1 次(每次提點將會扣除5%當作手續費)|會員一天只能提點 :p1 次",
    "AllMembersOnceADay" 						=> "*會員一天只能提點一次",
    "Amount" 									=> "點數",
    "AmountCannotBeLessThanBalance" 			=> "*點數不能超出餘點",
    "AmountMYR" 								=> "點數（馬幣 MYR）",
    "AmountRange" 								=> "請輸入最高: 1,000,000,000/最低: 100,000的儲值點數。",
    "AmountRange2" 								=> "請輸入最高: 1,000,000,000/最低: 250,000的提點點數。",
    "Answer" 									=> "回答",
    "April" 									=> "四月",
    "AtLeast18AndRead" 							=> "我已年滿18歲，且已閱讀、接受並同意本公司條款與規則、條例、隱私政策、瀏覽政策及有關年齡與身份驗證的政策。",
    "ATMBanking" 								=> "ATM轉點",
    "August" 									=> "八月",
    "AvailableBalance" 							=> "現有餘點",
    "Baccarat" 									=> "百家樂",
    "Back" 										=> "返回",
    "BackToLobby" 								=> "返回大廳",
    "Balance" 									=> "餘點",
    "Bank" 										=> "銀行",
    "BankAccount" 								=> "銀行賬戶",
    "BankAccountName" 							=> "銀行戶口名字",
    "BankAccountNo" 							=> "銀行戶口號碼",
    "BankBranch" 								=> "銀行分支",
    "BankDetails" 								=> "銀行資料",
    "BankDetailsContent" 						=> "如需更新相關信息，請聯繫我們的客服人員。",
    "Banking" 									=> "銀行信息",
    "BankingInformation" 						=> "銀行信息",
    "BankingInformation2" 						=> "銀行信息",
    "BankingOptions" 							=> "銀行選項",
    "BankMaxExceeded" 							=> "*已超出銀行的儲值上限。",
    "BankMinDepositCriteria" 					=> "*不符合銀行的最低儲值要求。",
    "BankName" 									=> "銀行",
    "BankTransfer" 								=> "銀行轉點",
    "BankTransferNote1" 						=> "最低提點：MYR50.00 / 最高提點：MYR50,000.00",
    "BankTransferNote2" 						=> "所有會員可以每日提點 <b>1</b>次。",
    "Banner01" 									=> '<img src="/img/homebanner/CN/1.jpg">',
    "Banner02" 									=> '<img src="/img/homebanner/CN/2.jpg">',
    "Banner03" 									=> '<img src="/img/homebanner/CN/3.jpg">',
    "Banner04" 									=> '<img src="/img/homebanner/CN/4.jpg">',
    "Banner05" 									=> '<img src="/img/homebanner/CN/5.jpg">',
    "Banner06" 									=> '<img src="/img/homebanner/CN/6.jpg">',
    "Banner07" 									=> '<img src="/img/homebanner/CN/7.jpg">',
    "Banner08" 									=> '<img src="/img/homebanner/CN/8.jpg">',
    "Banner09" 									=> '<img src="/img/homebanner/CN/9.jpg">',
    "Banner10" 									=> '<img src="/img/homebanner/CN/10.jpg">',
    "Blackjack" 								=> "21點",
    "Bonus" 									=> "紅利",
    "BonusContent" 								=> "請於14天內領取您的紅利，否則將作廢。",
    "BonusMinimumCriteria" 						=> "*不符合紅利的最低儲值標準",
    "Cancel" 									=> "取消",
    "CaribbeanPoker" 							=> "加勒比海撲克",
    "casinolobbyBackground" 					=> '<div style="width:980px;height:242px;position:relative; background:url(/img/casinobanner_cn.png);"></div>',
    "CasinoWallet" 								=> "賭場點包",
    "cfd" 										=> "CFD金融",
    "ChangePassword" 							=> '更換密碼',
    "ChangePasswordNote" 						=> "注意：請只輸入字母和數字",
    "ChangePasswordSuccess" 					=> "重置密碼的郵件已發送！請檢查您的電子郵件。",
    "ClubSuite" 								=> "梅花娛樂城",
    "Code" 										=> "驗證碼",
    "ConfirmNewPassword" 						=> "確認新密碼",
    "ConfirmPassword" 							=> "確認密碼",
    "ConfirmWithdrawalCode" 					=> "確認提点密码",
    "ContactNumber" 							=> "手機號碼",
    "ContactNumberContent" 						=> "請提供一個有效的手機號碼（僅限數字）以得到最新促銷與特別通知。",
    "ContactUs" 								=> "聯絡我們",
    "ContactNo" 								=> "手機號碼",
    "Credit" 									=> "款額",
    "CreditAndDebitRecords"						=> "款額和增入記錄",
    "CreditRecords"								=> "款額記錄",
    "CreditLimit" 								=> "款額限度",
    "CreditManagement" 							=> "款額管理",
    "Currency" 									=> "貨幣",
    "Currentbalance" 							=> "現有餘點",
    "CurrentPassword" 							=> "目前密碼",
    "4D" 										=> "4D萬字",
    "Date" 										=> "日期",
    "DateFrom" 									=> "日期從",
    "DateTo" 									=> "日期至",
    "DateOrTime" 								=> "日期 / 時間",
    "DateRange" 								=> "日期範圍",
    "DateTime" 									=> "日期/時間",
    "DateTimeContent" 							=> "請提供您儲值的日期/時間。",
    "Dealer" 									=> "荷官",
    "Debit" 									=> "增入",
    "DebitRecords"								=> "增入記錄",
    "December" 									=> "十二月",
    "Deposit" 									=> "儲值",
    "DepositAmount" 							=> "儲值點數",
    "DepositBank" 								=> "儲值銀行",
    "DepositChannel" 							=> "儲值方式",
    "DepositDetailNote" 						=> "注意：我們不接受支票儲值。任何存入我們戶口的支票儲值將受處罰，恕不另行通知。",
    "DepositDetails" 							=> "儲值資料",
    "DepositFund" 								=> "儲值 <br/>點數",
    "DepositMethod" 							=> "儲值方式",
    "DepositMethodContent" 						=> "請選擇您所使用的儲值方式。",
    "DepositNow" 								=> "立即 <br /> 儲值",
    "DepositPromoRules" 						=> "儲值優惠活動規則",
    "DepositPromotion" 							=> "儲值優惠活動",
    "DepositReceipt" 							=> "儲值收據",
    "DepositReceiptPolicy" 						=> "請選擇少於1 MB 的.jpg/.jpeg/.bmp/.png文件",
    "DepositReceiptReferenceNo"					=> "儲值收據號碼",
    "DepositSelection" 							=> "請選擇您儲值的銀行。",
    "DepositSteps1" 							=> "步驟 1：請儲值到以下任何一個銀行賬戶：",
    "DepositSteps1b" 							=> "步驟 1：在您儲值之前，請聯繫我們的客服以獲取銀行信息。您可以在儲值之後再填寫以下表格。",
    "DepositSteps2" 							=> "步驟 2：請填寫儲值的詳細資料：",
    "DepositWithdrawal" 						=> "儲值 / 提點",
    "DiamondSuite" 								=> "鑽石娛樂城 ",
    "DifficultyContact" 						=> "如果您遇到困難提供上述資料，請與我們聯繫。",
    "Disagree" 									=> "不同意",
    "DOB" 										=> "出生日期",
    "Download"									=> "下載",
    "DoYouWantToTransfer" 						=> "您的其他點包尚有餘點，您是否要轉移到中心點包？",
    "DragonTiger" 								=> "龍虎",
    "Egames" 									=> "E-遊戲",
    "Email" 									=> "電郵",
    "EmailAddress" 								=> "電郵地址",
    "EmailReminder" 							=> "請提供您有效的電郵地址以得到支付相關與特別事項的通知。",
    "EnterContactNumber" 						=> "請輸入您的手機號碼",
    "EnterEmailAddress" 						=> "請輸入您有效的電郵地址",
    "EnterFullName" 							=> "輸入您的全名",
    "EnterPassword" 							=> "提供一個密碼",
    "EnterUserName" 							=> "輸入您的用戶名",
    "FAQ" 										=> "常見問題",
    "Female" 									=> "女",
    "FileTooHuge" 								=> "*文件太大",
    "FooterCopyRight" 							=> '© <span style="color:#67b3e6">無極限娛樂網</span>版權所有',
    "ForgotLoginDetails" 						=> "忘記登錄資料?",
    "ForgotPassword" 							=> "忘記密碼?",
    "ForgotPasswordContent" 					=> "請輸入您的用戶名和電郵地址，我們會重置密碼並電郵給您。",
    "ForgotPasswordContent2" 					=> "我們可以幫助您重置密碼和安全信息。首先，請輸入您的註冊信息，我們將發送一個賬戶回复鏈接到您的電子郵件。",
    "From" 										=> "從",
    "FullName" 									=> "全名",
    "FundTransferSuccessful" 					=> "*款額轉點成功",
    "Gamefowl" 									=> "鬥雞",
    "Games" 									=> "遊戲",
    "Gender" 									=> "性別",
    "GeneralTNC" 								=> "一般條款與規則",
    "HeartSuite" 								=> "紅心娛樂城",
    "Help" 										=> "幫助",
    "HelpDeposit" 								=> "儲值 > 幫助",
    "helpWithdrawal" 							=> "提點 > 幫助",
    "History" 									=> "記錄",
    "Home" 										=> "首頁",
    "Horse" 									=> "賽馬",
    "HowToJoin" 								=> "如何加入",
    "HowToLoginDeposit" 						=> "如何登錄及儲值",
    "HowToRegister" 							=> "如何註冊",
    "IfAny"										=> "如有",
    "IAlreadyUnderstand" 						=> "我已了解。",
    "IAlreadyUnderstandRules" 					=> "我已了解並同意儲值及優惠活動條款與規則",
    "ID" 										=> "ID",
    "IDoNotAccept" 								=> "我不接受。",
    "IDoNotWantPromotion" 						=> "我不享有以上優惠活動",
    "image_android" 							=> '<img src="/img/mobiledownload/android.png" alt=""/>',
    "image_android_livecasino" 					=> '<img src="/img/mobiledownload/android_lc_cn.png" lt=""/>',
    "image_android_slot" 						=> '<img src="/img/mobiledownload/android_sl_cn.png"alt=""/>',
    "image_ios_livecasino" 						=> '<img src="/img/mobiledownload/ap_lc_cn.png" alt=""/>',
    "image_mobile_ag" 							=> '<img src="/img/mobiledownload/ag-casino-cn.png"/>',
    "image_mobile_click" 						=> '<img src="/img/mobiledownload/click-cn.png"/>',
    "image_mobile_maxbet" 						=> '<img src="/img/mobiledownload/maxbet-casino-cn.png"/>',
    "image_mobile_playtech" 					=> '<img src="/img/mobiledownload/pt-casino-cn.png"/>',
    "image_mobile_w88" 							=> '<img src="/img/mobiledownload/w88-casino-cn.png"/>',
    "image_mobiledownload" 						=> '<img src="/img/mobiledownload/mobiledownload_cn.png" alt=""/>',
    "img_mobiledownloadbar" 					=> '/img/mobiledownload/intab-cn.png',
    "Important" 								=> "重要",
    "ImportantNote1" 							=> "如果您的無極限娛樂網註冊姓名與銀行帳戶名稱不相同，您的提點申請將不被批准。",
    "ImportantNote2" 							=> "您只允許使用一個銀行賬戶作提點用途。任何銀行賬戶變更可能導致您的交易遭到拒絕。如有疑問，請於客服聯繫。",
    "IncentiveRebate" 							=> "獎勵回扣",
    "IncorrectWithdrawalCode"					=> "提點密碼不正確。",
    "InfiBankingInfo" 							=> "無極限娛樂網銀行信息",
    "InfiBankName" 								=> "無極限娛樂網銀行戶口名字",
    "InfiBankNo" 								=> "無極限娛樂網銀行戶口號碼",
    "InfiBankNote" 								=> "注意：我們不時更新銀行匯款戶口。請用戶們轉點前查看我們的銀行資料以避免發生任何付款問題。",
    "InternetBanking" 							=> "網上銀行轉點",
    "InvalidCode" 								=> "*驗證碼無效",
    "InvalidDateRange" 							=> "*日期範圍無效",
    "InvalidEmail" 								=> "*電郵無效",
    "InvalidFileType" 							=> "*文件格式無效",
    "InvalidFormat" 							=> "*格式無效",
    "InvalidGameType" 							=> "遊戲類型無效",
    "InvalidUsername" 							=> "*用戶名無效",
    "January" 									=> "一月",
    "JoinNow" 									=> "立即 加入",
    "July" 										=> "七月",
    "June" 										=> "六月",
    "Last5Trans" 								=> "最近五次交易",
    "LiveCasino" 								=> "真人娛樂城",
    "LiveChat" 									=> "24/7在線客服",
    "LiveChat2" 								=> "在線客服",
    "Loading" 									=> "載入中",
    "LocalBankTransfer" 						=> "本地銀行轉點",
    "Login" 									=> "登入",
    "LoginFailed" 								=> "登入失敗",
    "LoginSomewhereElse" 						=> "此帳戶已有人在別處登錄。",
    "Logout" 									=> "登出",
    "LogoutSuccess" 							=> "登出成功",
    "Maintenance" 								=> "維護中",
    "MainWallet" 								=> "中心點包",
    "Male" 										=> "男",
    "March" 									=> "三月",
    "MarsClub" 									=> "Mars 俱樂部 & Diamond slot",
    "Max" 										=> "最高",
    "MaxDeposit" 								=> "最高儲值",
    "Maximum" 									=> "最高",
    "May" 										=> "五月",
    "MemberWithdrawal" 							=> "會員提點",
    "Min" 										=> "最低",
    "MinDeposit" 								=> "最低儲值",
    "Mini" 										=> "迷你",
    "Minimum" 									=> "最低",
    "MinMaxLimit" 								=> "最低/最高限額",
    "MobileNumber" 								=> "手機號碼",
    "MobilePhone" 								=> "手機號碼",
    "MoreInfo" 									=> "更多信息",
    "MustBeLessThanEqual" 						=> "*必須少過或等於中心點包的餘點",
    "MustBeTheSameAsAbove" 						=> "*必須與上述相同",
    "Multiplayer"								=> "多人遊戲",
    "MyAccount" 								=> "我的帳戶",
    "MyAccountDetail" 							=> "我的帳戶",
    "NameCaution" 								=> "姓名必須與銀行賬戶持有人的姓名相同以批准提點",
    "NameMatchWithrawalBank" 					=> "*姓名必須與銀行戶口姓名相同",
    "nav_depositnow" 							=> '<img src="/img/nav/CN/depositNow.jpg"/>',
    "nav_login" 								=> '<img id="login_img" alt="infiniwin" src="/img/nav/CN/login_now.png"/>',
    "nav_login_button" 							=> '/img/nav/CN/login_btn.png',
    "nav_register" 								=> '<img src="/img/nav/CN/btn_register.png"/>',
    "nav_register_button" 						=> "/img/nav/CN/register.png",
    "nav_spin2" 								=> '<img src="/img/left_bottom_banner.gif" style="width: 215px;height: 82px;"/>',
    "nav_tutorial" 								=> '<img src="/img/nav/CN/btn_tutorial.png"/>',
    "nav01" 									=> '<img src="/img/nav/CN/icon_home.png" />',
    "nav02" 									=> '<img src="/img/nav/CN/icon_casino.png"/>',
    "nav03" 									=> '<img src="/img/nav/CN/icon_sport.png"/>',
    "nav04" 									=> '<img src="/img/nav/CN/icon_slot.png"/>',
    "nav05" 									=> '<img src="/img/nav/CN/icon_4D.png"/>',
    "nav06" 									=> '<img src="/img/nav/CN/icon_horse_racing.png" style="margin-top: 10px;">',
    "nav07" 									=> '<img src="/img/nav/CN/icon_promotion.png"/>',
    "nav08" 									=> '<img src="/img/nav/CN/icon_affiliate.png" style="width: 60px;margin: 8px 0px 0px -5px;"/>',
    "nav09" 									=> '<img src="/img/nav/CN/chickenfight.jpg"/>',
    "navjoinnow" 								=> '<img src="/img/join_now_btn_cn.png" />',
    "navJoinNowbtn" 							=> '<div id="join_now_btn" class="join_now_btn" style="background-image:url("/img/nav/CN/joinnow.png");">',
    "navLivechat" 								=> '<img src="/img/live_chat_button_cn.png" />',
    "navLivechatbanner" 						=> '<div style="position:fixed;z-index:9999;top:145px;right:3px;background-image:url(/img/floating_banner_cn.png);width:117px; height:345px ;">',
    "navMarsClub" 								=> '<img id="mars_club" src="/img/nav/CN/mars_club.png" />',
    "navPlatoclub" 								=> '<img id="pluto_club" src="/img/nav/CN/pluto_club.png" />',
    "navPlaytechClub" 							=> '<img id="playtech_club" src="/img/nav/CN/playtech_club.png" />',
    "navSaturnClub" 							=> '<img id="saturn_club" src="/img/nav/CN/saturn_club-coming-soon.png" />',
    "navVenusClub" 								=> '<img id="venus_club" src="/img/nav/CN/venus_club.png" />',
    "NewMember"						 			=> "成功創建賬戶。謝謝！",
    "NewPassword" 								=> "新密碼",
    "NewPasswordNote" 							=> "密碼必須是6-20個字符，符號除外。",
    "NewPasswordNote2" 							=> "密碼必須是6-15個字符，符號除外。",
    "No" 										=> "編號",
    "NoDataFound" 								=> "無資料顯示",
    "NoRecord" 									=> "暫無記錄",
    "NoTableIsAvailableNow" 					=> "暫無空座",
    "November" 									=> "十一月",
    "NumbersOnly" 								=> "數字而已",
    "October" 									=> "十月",
    "OldPassword" 								=> "舊密碼",
    "OnlyNumbers" 								=> "*輸入數字而已",
    "OpenAccount" 								=> "開設賬戶",
    "Option" 									=> "選項",
    "OverCounter" 								=> "通過櫃檯",
    "Password" 									=> "密碼",
    "PasswordPolicy" 							=> "6至12個字符（字母數字：A-Z, a-z, 0-9)而已。",
    "PasswordPolicyContent" 					=> "密碼指示:<br />- 請輸入6至20個字符",
    "PasswordsMatch"							=> "密碼匹配！",
    "PasswordsDoNotMatch"			 			=> "密碼不匹配！",
    "Payout" 									=> "賠率",
    "PayoutTable" 								=> '<iframe id="iframe_payoutTable" src="payoutTable-CN.html" width="650px" height="660px"></iframe>',
    "Pending" 									=> "待定",
    "PersonalDetail" 							=> "個人資料",
    "Phone" 									=> "手機",
    "PlayAndWin" 								=> "遊戲即贏",
    "Player" 									=> "玩家",
    "Players" 									=> "玩家",
    "PlayNow" 									=> "開始遊戲",
    "PlaytechClub" 								=> "Playtech 俱樂部 & Sapphire 老虎機",
    "PleaseLogin" 								=> "請登入",
    "PleaseReadPromoRules" 						=> "*請仔細閱讀並理解儲值促銷規則",
    "PleaseSelectBank" 							=> "*請選擇銀行",
    "PlutoClub" 								=> "Pluto 俱樂部 & Platinum 老虎機",
    "Poker" 									=> "撲克",
    "popupImage" 								=> '<img ID="popup_img" src="/img/PopUp Image_CN.jpg"/>',
    "PrivacyPolicy" 							=> "隱私政策",
    "ProcessingTime" 							=> "處理時間",
    "Profile" 									=> "基本資料",
    "Promo1" 									=> "挑戰者紅利 : 新會員100% 首存紅利 （最低儲值只需RM30）",
    "Promo2" 									=> "每日 20% 儲值紅利 （最低儲值只需RM30）",
    "Promo3" 									=> "新會員歡迎紅利 : 儲值50免費28令吉（最低儲值只需RM50）",
    "Promo4" 									=> "新會員歡迎紅利 : 儲值30免費18令吉（最低儲值只需RM30）",
    "Promotion" 								=> "優惠活動",
    "Reason" 									=> "原因",
    "ReceiptOptional" 							=> "儲值收據 （自選）",
    "RecordType" 								=> "記錄類型",
    "ReenterPassword" 							=> "重新輸入密碼",
    "ReenterPasswordNote" 						=> "請重新輸入您的密碼",
    "ReferenceCode" 							=> "參考號碼",
    "ReferenceContent" 							=> "提供存交易參考號碼將加快儲值處理速度。",
    "ReferenceNo" 								=> "參考號碼",
    "Register" 									=> "註冊",
    "Register" 									=> "註冊",
    "RegisterNow" 								=> "立即註冊",
    "RegisterNote1" 							=> "創建一個新賬戶。步驟簡單而且免費。",
    "Registration" 								=> "會員註冊",
    "Remark" 									=> "備註",
    "RepeatPassword" 							=> "重複您的密碼",
    "Required" 									=> "* 必填寫項",
    "RequiredField" 							=> "必填寫項",
    "RequiredFields" 							=> "必填寫項",
    "Reset" 									=> "重置",
    "ResetPasswordFailed" 						=> "重設密碼失敗！請稍後再試。",
    "ResponsibleGaming" 						=> "責任博彩",
    "Roulette" 									=> "輪盤",
    "Save" 										=> "儲存",
    "ScannedReceipt" 							=> "收據掃描",
    "ScannedReceiptContent" 					=> "提供儲值銀行收據將加快儲值處理速度。",
    "Search" 									=> "搜索標準",
    "SearchCriteria" 							=> "搜索標準",
    "SecurityQuestion" 							=> "安全問題",
    "September" 								=> "九月",
    "Sicbo" 									=> "骰寶",
    "SignOut" 									=> "登出",
    "SinglePlayerPoker" 						=> "單人撲克",
    "Sitemap" 									=> "網站地圖",
    "Skip" 										=> "跳過",
    "Skype" 									=> "SKYPE",
    "SlotGames" 								=> "老虎機",
    "Slots" 									=> "老虎機",
    "SpadeSuite" 								=> "黑桃娛樂城",
    "SportbookWallet" 							=> "體育點包",
    "Sports" 									=> "體育",
    "SportsBook" 								=> "體育博彩",
    "Statement" 								=> "聲明",
    "STATEMENT" 								=> "聲明",
    "Status" 									=> "狀態",
    "StockWallet" 								=> "金融點包",
    "Submit" 									=> "提交",
    "SystemAccount" 							=> "系統賬號",
    "SystemBusy" 								=> "系統繁忙",
    "Table" 									=> "賭桌",
    "TableGames" 								=> "電子娛樂城",
    "TableLimit" 								=> "賭注限額",
    "TAC" 										=> "條款與規則",
    "TermOfUse" 								=> "使用條款",
    "TimeOut" 									=> "超時",
    "TNC" 										=> "條款與規則",
    "To" 										=> "至",
    "Total" 									=> "總額",
    "TransactionCancelFailed" 					=> "*交易取消失敗",
    "TransactionDate" 							=> "交易日期",
    "TransactionHistory" 						=> "交易記錄",
    "TRANSACTIONHISTORYContent" 				=> "看看您最近的50個交易。<br>交易類型: <br><br>",
    "Transfer" 									=> "轉點",
    "TransferAmount" 							=> "轉點點數",
    "TransferDetails" 							=> "轉點資料",
    "TransferFailed" 							=> "*轉點失敗",
    "TransferFrom" 								=> "轉點從",
    "TransferTo" 								=> "轉點至",
    "TryAgain" 									=> "請再嘗試",
    "Tutorial" 									=> "教程",
    "Type" 										=> "類型",
    "Update"						 			=> "更新",
    "UnderageGambling" 							=> "未成年賭博",
    "UpdateBankDetail" 							=> "更新銀行資料",
    "UpdatePersonalDetail" 						=> "更新個人資料",
    "UploadBankbookPicture" 					=> "上传银行簿图片",
    "UploadDepositReceipt" 						=> "上传儲值收據",
    "Username" 									=> "用戶名",
    "Username2" 								=> "用戶名",
    "UsernameNotAvailable" 						=> "*用戶名已存在",
    "UsernamePolicyContent" 					=> "用戶名指示:<br />- 請輸入6至20個字符",
    "VenusClub" 								=> "Venus 俱樂部",
    "VerificationCode" 							=> "驗證碼",
    "Wallet" 									=> "點包",
    "Wallet1" 									=> "點包 1",
    "Wallet1Sportsbook" 						=> "<u>點包 1</u><br />體育博彩",
    "Wallet2" 									=> "點包 2",
    "Wallet2ClubVegas" 							=> "<u>點包 2</u><br />Club Vegas Casino<br />3D Slots",
    "Wallet3" 									=> "點包 3",
    "Wallet4" 									=> "點包 4",
    "Wallet4D" 									=> "萬字點包",
    "Wallet5" 									=> "點包 5",
    "WalletMaintenanceTryLater" 				=> "*點包正在維修，請稍後再試。",
    "WalletTransfer" 							=> "點包轉點",
    "WalletTransferHistory" 					=> "點包轉點記錄",
    "Wechat" 									=> "微信",
    "Welcome" 									=> "歡迎",
    "Withdrawal" 								=> "提點",
    "WithdrawalCode"							=> "提點密碼",
    "WithdrawalCodeDoNotMatch"					=> "提點密碼不匹配！",
    "WithdrawalCodeMustBe4Digits"				=> "提點密碼必須為4個數字。",
    "Withdrawaldetails" 						=> "提點資料",
    "WithdrawalProfile" 						=> "提點簡介",
    "WithdrawalStep1" 							=> "步驟 1：請驗證您的銀行資料，或聯繫我們的客服以更新相關資料。",
    "WithdrawalStep2" 							=> "步驟 2：請填寫以下表格以申請提點 ：",
    "WithdrawalSuccess" 						=> "*提點申請成功。我們將盡快處理，謝謝。",
    "WithdrawWinnings" 							=> "提點",
    "WouldYouLikeMakeDeposit" 					=> '您的帳戶現有 <font color="#FF0000">0.00</font> 款額。 <br/>請問您要儲值嗎?<br/>',
    "WouldYouLikeToTransfer" 					=> "您想要從中心點包轉點嗎？",
    "WrongCode" 								=> "驗證碼錯誤",
    "YouAreNotBornYet" 							=> "*您還未出生",
    "YouMustBe18" 								=> "*您必需滿18 歲或以上",
    "Mobile" 									=> "手機",
    "MobileVerificationCode" 					=> "手機驗證碼",
    "Fund" 										=> "點數管理",
    "min" 										=> "至少",
    "max" 										=> "最多",
    "AccountProfile" 							=> "賬號管理",
    "AccountPassword" 							=> "賬號密碼",
    "AccountPassword2" 							=> "賬號密碼",
    "TransactionEnquiry" 						=> "交易查詢",
    "Mainwallet" 								=> "中心點包",
    "Refresh" 									=> "更新",
    "NoDataAvailable" 							=> "無可用數據!",
    "NoDataFound" 								=> "沒有找到數據",
    "Attachment" 								=> "附件",
    "Package" 									=> "配套",
    "ImportantInfo" 							=> "重要信息",
    "depositnotice" 							=> "注意：請選擇配套以及存入正確數目",
    "BetHistory" 								=> "遊戲記錄",
    "Wager" 									=> "賭注",
    "Stake" 									=> "注額",
    "WinLose" 									=> "贏 / 輸",
    "RealBet" 									=> "有效的注額",
    "Product" 									=> "產品",
    "MinimumDepositAmountX" 					=> "最低儲值為: :p1",
    "Notice" 									=> "通告",
    "MobileSlot" 								=> "手機版老虎機",
    "YouMayFindYourLastXTransactionsHere" 		=> "您可以在這裡找到您最近的 :p1 筆交易",
    "ProceedWithoutAnyOfThePromotionsAbove" 	=> "我不要上述任何促銷。",
    "MiniGames"									=> "小遊戲",
    "MinimumAmount" 							=> "最低限額",
    "MaximumAmount" 							=> "最高限額",
    "EasternTime" 								=> "美東時間",
    "ResponsibleGaming" 						=> "博彩責任",
    "HowToDeposit" 								=> "如何儲值",
    "ArcadeGames"								=> "街機遊戲",
    "Arcades"									=> "街機遊戲",
    "AreYouSureYouWanToTransferAllGameBalancesToMainWallet" => "您確定要把所有遊戲的餘點轉點到中心點包？",
    "SelectYourGame" 							=> "選擇遊戲",
    "ScanQrCodeAndDownload" 					=> "掃描QR圖以下載遊戲",
    "OrViaDownloadLinksBelow" 					=> "或者通過以下下載鏈接",
    "SameAsLoginPassword" 						=> "與登入此網站的密碼一致。",
    "MaxUploadFileSize" 						=> "文件最大上傳大小: :p1",
    "FirstPrize2" 								=> "首獎",
    "SecondPrize2" 								=> "次獎",
    "ThirdPrize2" 								=> "三獎",
    "Racebook" 									=> "賽馬",
    "MobileApp" 								=> "手機應用",
    "Referral" 									=> "經銷商",
    "PleaseWaitForXSecondsToResendSMS" 			=> "請等候 :p1 秒再重試發送 SMS。",
    "PleaseEnterContactNumberToContinue" 		=> "請輸入手機號碼再繼續。",
    "SendVerificationCodeViaSMSToThisNumber" 	=> "確定發送驗證碼 SMS 給此手機號碼？",
    "ContactNumberIsAlreadyInUse" 				=> "手機號碼已經被使用。",
    "InvalidVerificationCode" 					=> "無效的驗證碼。",
    "InvalidUsernameOrEmail"					=> "用戶名或電郵無效！",
    "SendSMS" 									=> "發送 SMS",
    "Optional" 									=> "可選",
    "ReferralID" 								=> "經銷商賬號",
    "MyReferralID" 								=> "我的經銷商賬號",
    "RegisterDownline" 							=> "註冊下線",
    "InvalidReferralID" 						=> "無效的經銷商賬號",
    "ClickHereToGetVerificationCode" 			=> "送出驗證碼",
    "StatusAlreadyModifiedPleaseReloadPage" 	=> "狀態已被修改，請刷新頁面。",
    "UnsupportedRegion" 						=> "地區不支持。",
    "ResendConfirmationSMS" 					=> "重新發送SMS",
    "SMSSentSuccessfully" 						=> "SMS發送成功。",
    "CannotSendSMSAtTheMoment" 					=> "無法發送SMS。",
    "Share" 									=> "分享",
    "UsernameNote" 								=> "用戶名必須至少6個字符。",
    "MobileDownload" 							=> "手機版下載",
    "GoMobileFriendly" 							=> "我們擁有手機版網頁",
    "VideoPoker"								=> "電子撲克",
    "VisitOurMobileSiteForBetterExperience" 	=> "請試試我們的手機版網頁以獲得更佳的體驗！",
    "NoThanks" 									=> "不，謝謝",
    "Fortune4DTokenReminder" 					=> "親愛的會員，您還有4D遊戲代幣尚未使用。請在4D代幣有效期限內使用完，謝謝！",
    "PremierLeagueTokenReminder" 				=> "親愛的會員，您還有遊戲代幣尚未使用。請在代幣有效期限內使用完，謝謝！",
    "LineId"                					=> "LINE",
    "PleaseEnterLineOrWechatID"                	=> "請輸入LINE或微信賬號。",
    "PleaseUploadBankbookPicture"				=> "請上傳您的銀行簿载图。",
	"Inbox"										=> "收件箱",
	"inboxTitle"								=> "標題",
    "inboxContent"								=> "内容",
    "inboxDate"									=> "日期",
    "24HoursActiveUser"							=> "24小時活躍用戶",
    "24HoursCumulativeAmountOfBets"				=> "24小時累計遊戲點數",
    "24HoursCumulativeDepositWithdrawals"		=> "24小時累計儲值提點",
    "ActualFullName"							=> "真實姓名",
    "ContactNumberMustStartsWithX"				=> "手機號碼必須以 :p1 開始",
    "InvalidDepositMethod"				  		=> "無效的存款方式。",
    "Branch"				 			 		=> "分行",
    "FromBranch"			 			 		=> "從分行",
    "FromBank"			 				 		=> "從銀行",
    "UsernameOrEmailAlreadyExists"				=> "用戶名或電子郵件已經存在。",
    "Dashboard"									=> "儀表板",
    "SaveFailedPleaseRefreshPageAndTryAgain"	=> "保存失败，请刷新页面再重试。",
    "SignInToStartYourSession"					=> "请登入",
    "WalletSummary"								=> "點包摘要",
    "SystemId"									=> "系統 ID",
    "Zone"										=> "時區",
    "BankProfile"								=> "銀行簡介",
    "BankBook"									=> "銀行簿子",
    "PhoneNumber"								=> "聯繫號碼",
    "Send"										=> "發送",
    "SendToAll"									=> "發送給全部",
    "InboxMessages"								=> "收件箱消息",
    "OperatorTag"								=> "操作員標籤",
    "TotalRebate"								=> "總回扣",
    "DailyWithdrawalLimit"						=> "每日提點上限",
    "BankSettings"								=> "銀行設定",
    "CustomerTag"								=> "客戶標籤",
    "LastLoginIp"								=> "最後登入IP",
    "UpdateSuccess"								=> "更新成功！",
    "Success"									=> "成功！",
    "Failed"									=> "失敗！",
    "ConfirmSendSMS"							=> "確定發送SMS？",
    "ConfirmSendInboxMessage"					=> "確定發送消息？",
    "ConfirmSendInboxMessageToAllMember"		=> "確定發送消息給所有會員？",
];