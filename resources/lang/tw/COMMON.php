<?php

return [


'ABOUT' 						=> '關於',
'ACCEPT' 						=> '接受',
'ACCEPTED' 						=> '被接受',
'ACCEPTEDFULL' 					=> '被充分接受',
'ACCEPTEDPARTIAL' 				=> '被接受的部份',
'ACCEPTEDON' 					=> '接受在',
'ACCEPTTIMEOUT' 				=> '採納暫停',
'ACCESSIBILITY' 				=> '可及性',
'ACCESSURL' 					=> '訪問URL',
'ACCOUNT' 						=> '帳號',
'ACCOUNTID' 					=> 'Account ID',
'ACCOUNTBALANCE' 				=> '賬戶餘點',
'ACCOUNTCODE' 					=> 'Account Code',

'ACCOUNTBLOCKED' 				=> '由於安全防備措施, 帳戶已由系統阻攔。疏導它請訪問"Blocked Account"模塊在安全菜單之下',
'ACCOUNTBLOCKEDFRAUDCASE' 		=> '由於發現欺騙活動, 帳戶已被阻攔',
'ACCOUNTDETAILS' 				=> '帳戶資料',
'ACCOUNTENQUIRY' 				=> '帳戶查詢',
'ACCOUNTINFORMATION' 			=> '帳戶資料',
'ACCOUNTPOSTMORTEM' 			=> '帳戶解剖',
'ACCOUNTREGISTRATION' 			=> '帳戶註冊',
'ACCOUNTS' 						=> '帳戶',
'ACCOUNTSTATUS' 				=> '帳戶狀態',
'ACCOUNTSETTING' 				=> '帳戶設定',


'ACCOUNTSUSPENDED' 				=> '您的帳戶已暫停, 請與管理員聯繫為幫助! - 誤差編碼: %0',
'ACCOUNTEXPIRED' 				=> '您的帳戶已到期, 請與管理員聯繫為幫助! - 誤差編碼: %0',
'ACTION' 						=> '行動',
'ACTIVATE' 						=> '活躍',
'ACTIVATESELECTEDROW' 			=> '活躍被選擇的列',
'ACTIVE' 						=> '有效',
'ACTIVITYREPORT' 				=> '活動報告',

'ADD' 							=> '增加',
'ADDMEMO' 						=> '增加備忘錄',
'ADDNEW' 						=> '新增',
'ADDRESS' 						=> '地址',
'ADDRESS1' 						=> '地址1',
'ADDRESS2' 						=> '地址2',
'ADDRESS3' 						=> '地址3',
'ADJUSTMENT' 					=> '調整點數',
'ADJUSTMENTTYPE' 				=> '調整類別',
'ADJUSTMENTAMT' 				=> '調整數額',
'ADJUSTMENTAMTLOCAL' 			=> '本地調整數額',
'ADJUSTMENTFROM' 				=> '調整從',
'ADJUSTMENTMETHOD' 				=> '調整方法',
'ADJUSTMENTPROFILE' 			=> '調整簡介',
'ADJUSTMENTREPORT' 				=> '調整報告',
'ADJUSTMENTTO' 					=> '調整到',
'ADMIN' 						=> 'Admin',
'ADMINANNOUNCEMENT' 			=> '管理公告',
'ADMINLOG' 						=> '管理員記錄',
'ADMINSUMMARY' 					=> '管理摘要',
'ADMINUSER' 					=> '管理員',
'ADMINUSERGROUP' 				=> '管理員',
'AFFILIATE' 					=> '代理',
'AFFILIATECOMMISION' 			=> '代理佣金',

'AFTER' 						=> '之後',
'AGENCY' 						=> '代理商',
'MASTERAGENT' 					=> '總代理',



'AGENT' 						=> '代理',
'AGO' 							=> '之前',
'ALL' 							=> '所有',
'ALLBET' 						=> '所有註單',
'ALLOWIP' 						=> '准許IP',
'AMOUNT' 						=> '點數',
'AMOUNTLOCAL' 					=> '本地數額',
'AMOUNTRETURNED' 				=> '支出數額',
'ANALYSIS' 						=> '分析',

'AND' 							=> '與',
'ANNOUNCEMENT' 					=> '消息公告',

'APPLY' 						=> '適用',
'APPLYTO' 						=> '適用於',
'APPROVE' 						=> '批准',
'APPROVEREJECT' 				=> '批准/拒絕',

'ASIAN' 						=> '亞洲',
'ASSIGN' 						=> '分配',
'ASSIGNED' 						=> '已指派',

'ATLEAST' 						=> '請輸入至少一個%0',
'ATLEASTREQUIRED' 				=> '至少需要%0 %1',
'ATTENTION' 					=> '注意',

'AUDITED' 						=> '審計日期',
'AUDITEDBY' 					=> '審計用戶',

'AUTO' 							=> '自動',
'AUTO_CANCELLED' 				=> '自動取消',
'AUTO_CONFIRMED' 				=> '自動批准',
'AUTO_PENDING' 					=> '審理中',

'AUDIT' 						=> '審核',
'AVAILABLE' 					=> '可用的',
'AVERAGEBETSIZE' 				=> '平均遊戲量',


'ABNORMALSTATUS' 				=> '異常狀態',
'ACUMULATIVE' 					=> '累計',
'AUDITPERMISSION' 				=> '審核權限設定',

'BACK' 							=> '返回',
'BACKUP' 						=> '備份',
'BADLOGINLOG' 					=> '企圖使用壞註冊',
'BALANCE' 						=> '餘點',

'BANK' 							=> '銀行',
'BANKACCOUNT' 					=> '銀行帳戶',


'BANKACCBRANCH' 				=> '銀行帳戶控股分支',
'BANKACCDESC' 					=> '銀行帳戶描述',
'BANKACCNAME' 					=> '銀行帳戶名字',

'BANKACCNO' 					=> '銀行帳戶號碼',
'BANKBRANCHCODE' 				=> '分行帳戶代碼',
'BANKCODE' 						=> '銀行代碼',
'BANKCOUNTRY' 					=> '銀行國家',
'BANKDATA' 						=> '銀行資料',
'BANKINFORMATION' 				=> '銀行信息',
'BANKNAME' 						=> '銀行名字',
'BANKPHONENO_1' 				=> '銀行電話號碼1',
'BANKPHONENO_2' 				=> '銀行電話號碼2',
'BANKSORTCODE' 					=> '銀行排序代碼',
'BANKSWIFTCODE' 				=> '快速代碼',
'BANKTRANSFER' 					=> '銀行匯款',
'BANKTRANSFEROBJECT' 			=> 'BankTransferObject',
'BANKFULLNAME' 					=> '銀行全名',
'BANKFULLNAMEEN' 				=> '銀行全名 (英文)',
'BANKFULLNAMECH' 				=> '銀行全名 (中文)',
'BANKSHORTNAME' 				=> '銀行簡稱',
'BANKSHORTNAMEEN' 				=> '銀行簡稱 (英文)',
'BANKSHORTNAMECH' 				=> '銀行簡稱 (中文)',
'BANKTYPE' 						=> '銀行類別',
'BANKBRANCH' 					=> '支行',

'BANKHQ' 						=> '總行',
'INTERNETBANK' 					=> '開戶網點',
'BANNEDIP' 						=> '禁止IP',

'BBF' 							=> '賬戶結馀向前調動',
'BBFSHORT' 						=> 'BBF',
'BEGINNING' 					=> '開始',
'BEFORE' 						=> '以前',
'BETBOTHSIDE' 					=> '雙面打水',
'BETENQUIRY' 					=> '注單查詢',
'BETFILTER' 					=> '注單過濾器',
'BETINTERCEPTOR' 				=> '注單攔截',
'CASINOBETAUDIT' 				=> '審核真人視訊注單',




'SPORTBETAUDIT' 				=> '審核體育注單',
'TRANSAUDIT' 					=> '審核交易',
'BETNUM' 						=> '注單',
'BETS' 							=> '遊戲',
'BETSLIP' 						=> '注單',
'BETSLIPOBJECT' 				=> 'BetslipObject',
'BETTERM' 						=> '遊戲期限',
'BETTING' 						=> '遊戲中',
'BETTYPE' 						=> '遊戲類型',
'BETTYPEREPORT' 				=> '遊戲類型報告',
'BETTYPEREPORT2' 				=> '遊戲類型報告2',
'BETURL' 						=> '遊戲URL',
'BLOCKED' 						=> '封鎖',
'BLOCKEDACCOUNT' 				=> '被阻攔的帳號',
'BLOCKEDIP' 					=> '阻攔的IP',



'BONUS' 						=> '現金優惠活動',
'BONUSCAMPAIGN' 				=> '現金優惠活動',
'BOOLEAN' 						=> 'Boolean',

'BOTH' 							=> '雙方',
'BRANCHCODE' 					=> '分支代碼',


'CACHE' 						=> '緩存',
'CACHEEXPIRY' 					=> '緩存期滿(秒)',
'CACULATE' 						=> '計算',
'CALLSTACK' 					=> '調用棧',
'CAMPAIGN' 						=> '活動',
'CANNOTMORETHAN' 				=> '%0無法多過%1',
'CANCEL' 						=> '取消',
'CANCELLED' 					=> '被取消',
'CANCELLEDBET' 					=> '被取消的賭注',
'CANCELLEDBYADMIN' 				=> '被管理員取消',
'CANCELLEDMATCH' 				=> '取消',
'CANNOTCANCELBET' 				=> '您無法取消這賭注',

'CANNOTSELECTMORE' 				=> '這個工能您只能選擇一個項目',
'CANNOTSIMILAR' 				=> '%0無法是同一樣%1',
'CANNOTSETTLENOTRESULT' 		=> '無法解決沒有賽果的活動',

'CASH' 							=> '現金',
'CASHCARD'						=> '现金卡',
'CASHBALANCE' 					=> '現金餘點',
'CASHBALANCE_POSITIVE' 			=> '現金餘點是正數',
'CASHLEDGER' 					=> '現金總帳',
'CATEGORY' 						=> '類別',








'CENT' 							=> '分',
'CITY' 							=> '城市',
'CGBETTING' 					=> '遊戲配置',
'CGLOGIN' 						=> '註冊配置',
'CGMARKET' 						=> '市場配置',
'CGPAYMENT' 					=> '付款配置',
'CGSYSTEM' 						=> '系統配置',
'CGTRANSACTION' 				=> '交易配置',
'CHANGED' 						=> '已更改',
'CHANGEPASS' 					=> '修改密碼',
'CHANGESETTING' 				=> '修改個人設定',


'CHANNEL' 						=> '頻道',
'CHARGEFEEREPORT' 				=> '收費報告',
'CHARGESAMOUNT' 				=> '收費數目',

'CHECK' 						=> '檢查',
'CHECK_ALL' 					=> '全選',
'CHECKRESULT' 					=> '檢查賽果',
'CHECKSETTLEMENT' 				=> '檢查注單計算',
'CHECKSTAKE' 					=> '檢查注額',
'CHECKWAGERSEQUENCE' 			=> '檢查賭注序列',

'CLASS' 						=> '等級',
'CLASSNAME' 					=> '等級名稱',
'CLIENT' 						=> '客戶',

'CLOSE' 						=> '關閉',
'CLOSEALL' 						=> '關閉所有',
'CLOSEDATE' 					=> '關閉日期',
'CLOSEDATETIME' 				=> '關閉時間',
'CLOSESELECTEDMATCH' 			=> '關閉選擇的比賽?',
'CLOSETICKET' 					=> '關閉帖子',
'CLOSETIME' 					=> '關閉時間',
'CLOSINGBALANCE' 				=> '收盤餘點',

'CMS' 							=> '內容管理',
'CMSARTICLE' 					=> '文章內容',


'CMSCAT' 						=> '種類內容',
'CODE' 							=> '代碼',
'COMMENT' 						=> '評論',
'COMMENT_COL' 					=> '評論',
'COMMISSION' 					=> '佣金',
'COMMISSIONTYPE' 				=> '佣金類別',
'COMPANY' 						=> '公司',
'COMPANY_BEAR' 					=> '公司承擔',
'COMPETITION' 					=> '活動',
'COMPETITIONLIST' 				=> '活動列表',
'COMPETITIONREPORT' 			=> '活動報表',
'CONDITIONALITY_REVIEW' 		=> '條件審查',


'CONFGROUP' 					=> '配置小組',
'CONFIG' 						=> '系統配置',
'CONFIRM' 						=> '確定',
'CONFIRM2' 						=> '確認',
'CONFIRMACCEPTANCE' 			=> '確定接受?',
'CONFIRMADD' 					=> '確定增加?',
'CONFIRMAPPROVE' 				=> '確定批准?',

'CONFIRMBATCHCREATE' 			=> '確定應用莊家資料建造項目?',
'CONFIRMEDDATA' 				=> '確定資料',

'CONFIRMDELETION' 				=> '確定刪除? 確定後, 就無法取消。 ',
'CONFIRMEVENTRESET' 			=> '確定重新設置比分?',
'CONFIRMMAKESETTLEMENT' 		=> '確定做注單計算?',
'CONFIRMREJECT' 				=> '確定拒絕?',
'CONFIRMPROCESS' 				=> '確定審理?',
'CONFIRMSUBMIT' 				=> '確定提交?',


'CONFIRMREMOVE' 				=> '確定刪除? 確定後, 就無法取消。 ',
'CONFIRMREMOVEMAPPING' 			=> '從%1刪除%0? 確定後, 就無法取消。 ',
'CONFIRMED' 					=> '已確定',
'CONSOLE' 						=> '控制台',

'CONTACTINFO' 					=> '聯絡資料',
'CONTENT' 						=> '內容',
'CONTENTTITLE' 					=> '標題',
'CONTINUEINSET' 				=> '繼續輸入',

'COUNT' 						=> '計數',
'COUNTER' 						=> '駁回',
'COUNTRY' 						=> '國家',
'CREATE' 						=> '創造',
'CREATEDBY' 					=> '創造者',
'CREATEDIP' 					=> '創造者IP',
'CREATEDON' 					=> '創造時間',
'CREDIT' 						=> '存入',
'CREDITBALANCE' 				=> '信貸馀額',
'CREDITLIMIT' 					=> '信貸限額',


'CURCODE' 						=> '貨幣代碼',
'CURRATE' 						=> '貨幣兌換率',
'CURRATESHORT' 					=> '貨幣兌換率',
'CURRENCY' 						=> '貨幣',
'CURRENT' 						=> '現時的',
'CURRENT_CASHBALANCE' 			=> '現時現金餘點',
'CURRENT_CONDITIONS' 			=> '當前條件',
'CURRENT_TURNOVERSTAKE' 		=> '目前有效遊戲',
'CURRENT_WINLOSS' 				=> '目前輸贏',
'CURRENTPASSWORD' 				=> '現時的密碼',
'CUSTOMER' 						=> '客戶',
'CUSTOMERACCSTATEMENT' 			=> '客戶帳戶報表',
'CUSTOMERACTIVITYREPORT' 		=> '客戶活動報告',
'CUSTOMERACTIVITYSUMMARYREPORT' => '客戶活動綜合報告',
'CUSTOMERREPORT' 				=> '客戶報告',
'CUSTOMERRESPOND' 				=> '客戶反應',
'CUSTOMERSERVICEOPERATOR' 		=> '客戶服務操作員',
'CUSTOMERSERVICE' 				=> '客戶服務',
'CUSTOMERSUPPORT' 				=> '客戶服務',
'CUSTOMERWINAMOUNT' 			=> '客戶贏的數額',

'COVER' 						=> '覆蓋',
'COVERNUM' 						=> '個號碼',


'DAILY' 						=> '每日',
'DATA' 							=> '資料',
'DATABASE' 						=> '數據庫',
'DATABASENAME' 					=> '數據庫名稱',
'DATATYPE' 						=> '資料種類',

'DATE' 							=> '日期',
'DATEPOSTED' 					=> '張貼時間',
'DATEORTIME' 					=> '時間',



'DAY' 							=> '日',
'DAYS' 							=> '日',
'DEBIT' 						=> '支出',
'DECIMAL' 						=> '小數式',
'DECLINED' 						=> '被拒絕',
'DECRYPT' 						=> '解密',

'DECRYPTURL' 					=> '變數解密',
'DEFAULTLANGUAGE' 				=> '默認語言',
'DELETE' 						=> '刪除',
'DELETEALL' 					=> '全刪除',
'DELETEFAIL' 					=> '刪除失敗',
'DELETEOWN' 					=> '刪除',
'DELETESELECTEDFILTER' 			=> '刪除選擇的過濾器?',
'DEPARTMENT' 					=> '部門',
'DEPOSIT' 						=> '存點',
'DEPOSIT_TOBEPROCESSED' 		=> '存點申請待處理',
'DEPOSITPERCENTAGE' 			=> '%存點',
'DEPOSITREPORT' 				=> '存點報告',
'DEPOSITTO' 					=> '存點授予',
'DEPOSITWITHDRAWAL' 			=> '存提',
'DEPOSITWITHDRAWALTIME' 		=> '存提次數',
'DEPOSITWITHDRAWALAMOUNT' 		=> '存提總數',

'DESC' 							=> '描述',
'DETAILEDVIEW' 					=> '詳細觀察',
'DETAILS' 						=> '詳細',

'DETAILSONLYFORWAGER' 			=> '抱歉, 細節只可利用為賭注',
'DIRECTMEMBER' 					=> '直接會員',
'DISABLE' 						=> '停用',
'DIDNOT_RECEIVE_DEPOSIT' 		=> '未收到款',
'DIFFERENCES' 					=> '差異',


'DOB' 							=> '出生日期',
'DONE' 							=> '完成',
'DOWNLINE' 						=> '下線',

'DRAW' 							=> '和局',
'DONTHAVE' 						=> '沒有',

'DTZ_MINBALANCE' 				=> '斗地主必備餘點總數',
'DUPLICATEIP' 					=> '重複IP',
'DUPLICATEVALUE' 				=> '重複值 "%0"',

'EDIT' 							=> '修改',
'EDITPASSWORD' 					=> '修改密碼',
'EDITALL' 						=> '修改所有',
'EDITSTATUS' 					=> '修改狀態',
'EDITOWN' 						=> '自已修改',


'EMAIL' 						=> '郵箱',
'EMAILADD' 						=> '郵箱地址',
'ENABLE' 						=> '啟用',
'ENCODING' 						=> '編碼',

'END' 							=> '結束',
'ENDDATETIME' 					=> '結束時間',


'ENDED' 						=> '結束',
'ERROR' 						=> '錯誤',
'ERRORCODE' 					=> '錯誤代碼',

'ERROR_ALLOWWITHDRAWAL' 		=> '您的提點資金數額超出每日支出資金限額。 ',
'ERROR_INVALIDPASSWORD' 		=> '您輸入了一個無效密碼',







'ERROR_MINDEPOSIT' 				=> '您的儲蓄數額比最小的儲蓄限額低。 ',
'ERROR_MAXDEPOSIT' 				=> '您的儲蓄數額比最大的儲蓄限額高。 ',
'ERROR_MINWITHDRAWAL' 			=> '您的提點數額比最小的提點限額低。 ',
'ERROR_NETAMOUNT' 				=> '您的提點數額超出您的賬戶結馀。 ',
'EVEN' 							=> '雙',
'EVENS' 						=> '雙',
'EVENT' 						=> '項目',
'EVENTS' 						=> '項目',


'EXCEEDACCOUNTBALANCE' 			=> '抱歉, 轉移數額超出了賬戶餘點',
'EXCLUDEIP' 					=> '排除IP',
'EXCLUDEPROMOCASH' 				=> '(排除紅利)',
'EXPIRED' 						=> '到期',
'EXPIRYDATE' 					=> '有效期限',
'EXPORT' 						=> '導出',
'EXPORTBATCHFILE' 				=> '輸出Batch檔案',





'FAIL' 							=> '失敗',
'FAILTOOPENFILE' 				=> '檔案無法打開',
'FAX' 							=> '傳真號碼',
'FEATUREDISABLED' 				=> '這個工能暫時是中止的。 ',
'FEE' 							=> '費用',
'TOTALFEE' 						=> '總費用',
'FEEDBACK' 						=> '回复',
'FEEDBACKTO' 					=> '回復到%0',
'FEELOCAL' 						=> '本地費用',
'FEETYPE' 						=> '費用類別',
'FEMALE' 						=> '女性',
'FINANCE' 						=> '財務',
'FINANCESUMMARY' 				=> '財務端摘要',


'FILE' 							=> '檔案',
'FILE_SIZE' 					=> '檔案大小是必須較少於2MB',
'FILTER' 						=> '過濾器',
'FILTERNAME' 					=> '過濾器名字',
'FINALSTAKE' 					=> '總注額',

'FIRSTNAME' 					=> '名字',
'FIRSTTIMEDEPOSIT' 				=> '首次存入',
'FIXEDAMOUNT' 					=> '固定值',
'FIXEDCHARGES' 					=> '固定費用',
'FLATPOINT' 					=> '平點',


'FLOAT' 						=> '浮動',
'FOR' 							=> '為',
'FORMAT' 						=> '格式',
'FRACTIONAL' 					=> '分數式',
'FRAUDCASE' 					=> '欺騙案件',


'FRAUDCASEACCOUNT' 				=> '請注意一旦帳戶被設置為"欺騙案件", 所有交易(儲蓄、提點、賭注、支出)將被破解並且狀態無法再被更新。 ',
'FROM' 							=> '從',
'FULLTIME' 						=> '全場',
'FUNCTION' 						=> '功能',
'FUNDINGMETHOD' 				=> '支付方式',
'GAMESTYPE' 					=> '遊戲類別',
'GENDER' 						=> '性別',
'GENERATEREPORT' 				=> '產生報告',


'GO' 							=> '下一步',
'GOTO' 							=> '頁數',
'GOTOPREVIOUSPAGE' 				=> '到前頁',


'GOVTAXREPORT' 					=> '政府徵稅報告',
'HAVE' 							=> '有',
'HAVESAME' 						=> '%0 和 %1 相同的',


'HELP' 							=> '幫助',
'HIDE' 							=> '隱藏',
'HIDECOLUMN' 					=> '隱藏欄',
'HISTORY' 						=> '記錄',

'HOMEPHONE' 					=> '家庭電話號碼',
'HONGKONG' 						=> '香港',


'HOUR' 							=> '小時',
'HOURS' 						=> '小時',
'HOWDIDUHEARABOUTUS' 			=> '您如何發現我們?',

'ICON' 							=> '象徵',
'ID' 							=> 'ID',

'IDNOTPROVIDED' 				=> '這名用戶不將允許執行支出交易因為沒有提供身分認證',
'IDPASSPORT' 					=> '身份證 / 護照',
'IDPROVIDED' 					=> '提供ID',
'IMPORT' 						=> '資料載入',

'IN' 							=> '在',
'INCENTIVE' 					=> '退水優惠活動',
'INCENTIVE_BONUS' 				=> '退水金',
'INCENTIVEA' 					=> '退水A',
'INCENTIVEB' 					=> '退水B',
'INCENTIVEC' 					=> '退水C',
'INCENTIVEDURATION' 			=> '結算時間',
'INCENTIVEMAX' 					=> '封頂值',
'INCENTIVEASHORT' 				=> '退水A',
'INCENTIVEBSHORT' 				=> '退水B',
'INCENTIVECSHORT' 				=> '退水C',
'INCENTIVESETTING' 				=> '退水設定',
'INCENTIVESPORTSBOOK' 			=> '體育退水',
'INCENTIVESPORT1' 				=> 'AH / OU / OE',
'INCENTIVESPORT2' 				=> '1x2',
'INCENTIVESPORT3' 				=> '其他',

'INCLUDEDRAW' 					=> '包括和局選擇',
'INCOMPLETE' 					=> '不完整',
'INPROGRESS' 					=> '進展中',
'INRUNNING' 					=> '走地',
'INCORRECTINPUT' 				=> '%0是不正確的',


'INDEX' 						=> '指數',
'INDO' 							=> '印尼',
'INFORMATION' 					=> '資訊',




'INFO_RIGHT' 					=> 'NB. 關於您的帳戶Betworks有權與您聯繫',
'INFO_SECURITY1' 				=> '如果您與我們聯繫, 我們將使用以下資料確認您的身分',
'INFO_SECURITYKEY1' 			=> '請輸入字符如在左邊的圖像。 ',
'INFO_SECURITYKEY2' 			=> '這幫助我們改進網站的安全。 ',
'INPUTCONFLICT' 				=> '%0和%1不能相同',

'INPUTRANGE' 					=> '%0必須是在%1到%2之間的長度',
'INSTANT_TRANSACTION' 			=> '即時交易',
'INTEGER' 						=> '整數',
'INTERNALTRANSFER' 				=> '內部調動',
'INTERNET' 						=> '互聯網',
'INTERVAL' 						=> '間隔',
'INVALIDCHARACTER' 				=> '%0包含無效字符',
'INVALIDDATAFORMAT' 			=> '不正確資料格式',

'INVALIDDATE' 					=> '請填寫正確日期',
'INVALIDFORMAT' 				=> '不正確%0格式',
'INVALIDINPUT' 					=> '這%0是不正確',

'INVALIDLOGIN' 					=> '抱歉, 您的註冊無效。懇請聯絡管理員尋求協助 - 誤差編碼: %0',
'INVALIDMATCH' 					=> '"%0"不匹配"%1"',
'INVALIDMULTITYPESELECTION' 	=> '複式型選擇無效',

'INVALIDPASSWORD' 				=> '%0必須包含字母和數字混合',
'INVALIDPHONENO' 				=> '%0為不正確電話號碼',

'INVALIDRANGE' 					=> '無效範圍為"%0"和"%1"',
'INVALIDSHORTCUT' 				=> '無效捷徑',


'INVALIDUSERNAME' 				=> '%0必須包含字母數字和強調字符及從字母表開始',
'INVALID_USERNAME' 				=> '帳號無效',
'IPADDRESS' 					=> 'IP地址',
'IPLOOKUP' 						=> '檢查IP',
'JUMPCODE' 						=> '跳碼',
'KILLTRACKER' 					=> '廢止在線使用者',
'LANGUAGE' 						=> '語言',
'LAST7DAYS' 					=> '前七日',
'LASTACTION' 					=> '前次行動',
'LASTBETON' 					=> '最後遊戲時間',
'LASTLOGINON' 					=> '最後登入',

'LASTNAME' 						=> '姓氏',
'LASTUPDATED' 					=> '前更新',
'LESSTHAN1MIN' 					=> '少過一分鐘',
'LIABILITY' 					=> '負擔額',
'LIABILITYFILTER' 				=> '負擔額過濾器',
'LIABILITYMATCH' 				=> '賽事負擔額',
'LIABILITYOUTRIGHT' 			=> '奪標負擔額',

'LIMIT' 						=> '極限',
'LIMITED' 						=> '限制',


'LINE' 							=> '行',
'LOCAL' 						=> '本地',
'LOCALBALANCE' 					=> '本地餘點',

'LOGIN' 						=> '登入',
'LOGINAS' 						=> '登入用戶',
'LOGOUT' 						=> '登出',

'LOSS' 							=> '輸',
'LOSSHALF' 						=> '輸半',
'LOSSONLY' 						=> '輸而已',

'MAIN' 							=> '主',
'MAILINGADDR' 					=> '郵寄地址',

'MALE' 							=> '男性',
'MANDATORYFIELD' 				=> '必填項目',

'MANDATORY' 					=> '%0是必填項目',
'MARKETING' 					=> '營銷',


'MATCH' 						=> '賽事',
'MAXAMOUNT' 					=> '封頂值',
'MAXCHARGES' 					=> '最高費用',
'MAXDAILYDEPOSIT' 				=> '每日最大值的存點數額',
'MAXDAILYWITHDRAWAL' 			=> '每日最大值的提點數額',
'MAXWITHDRAWAL' 				=> '最高提點',
'MAXLIMIT' 						=> '最大限度',

'MAXUSERTYPEREACH' 				=> '網上的用戶已經提點到最大限度, 請%0分鐘再登入',
'MAXMEMBER' 					=> '人數上限',
'MEMBER' 						=> '會員',
'MEMBERACCOUNT' 				=> '會員賬戶',
'MEMBERCANCELLED' 				=> '會員取消',
'MEMBERMAXINCENTIVE' 			=> '會員最高優惠活動',
'MEMBERNETACCOUNTID' 			=> 'Neteller會員賬戶ID',
'MEMBERTRENDS' 					=> 'MemberTrends',

'MEMBERREFNO' 					=> '會員參考數字',
'MEMBERNETSECUREID' 			=> 'Neteller會員安全ID',
'MEMBER_WINLOSS' 				=> '會員輸贏',


'MEMO' 							=> '備忘錄',
'MENU' 							=> '選項',
'METHOD' 						=> '方法',
'MIGRATION' 					=> '遷移',
'MINLIQUIDITY' 					=> 'Minimum Liquidity',
'MINLIQUIDITYMSG' 				=> 'Minimum liquidity will be set to system default (%0) if checkbox is unchecked',
'MINUTE' 						=> '分',
'MINUTES' 						=> '分',
'MINCHARGES' 					=> '最低費用',
'MINDEPOSIT' 					=> '最低存點數額',
'MINWITHDRAWAL' 				=> '最低提點數額',
'MOBILEPHONE' 					=> '手機電話號碼',

'MODE' 							=> '方式',
'MODIFICATION' 					=> '修改',
'MODIFIEDBY' 					=> '修改者',
'MODIFIEDON' 					=> '修改時間',
'MODULE' 						=> '模塊',
'MONITORING' 					=> '監測',

'MONTH' 						=> '月份',
'MONTHLY' 						=> '每月',
'MONTHTODATE' 					=> '本月至今',
'MULTIPLE' 						=> '複數',
'MUSTBEMORETHAN' 				=> '%0必須多過於%1',
'MUSTHAVESAME' 					=> '%0和%1必須有相同的%2',
'MUSTGREATERTHAN' 				=> '%0必須大於%1',

'NAME' 							=> '名字',
'NAMECARD' 						=> '名片',
'NETTDEPOSIT' 					=> '淨存點',
'NETTWITHDRAWAL' 				=> '淨提點',
'NEUTRAL' 						=> '中立場',

'NEVER' 						=> '從未',
'NEVEREXPIRE' 					=> '永不過期',

'NEW' 							=> '新',
'NEWACCOUNT' 					=> '新帳號',
'NEWMEMBER' 					=> '新會員',
'NEWMEMO' 						=> '新備忘錄',
'NEWPASSWORD' 					=> '新密碼',
'NEWSIGNUP' 					=> '新加入',
'NEWSIGNUPDEPOSITS' 			=> '新加入儲蓄',
'NEWSLETETR' 					=> '電子報',
'NEWTRANSACTION' 				=> '新交易',





'NEXT' 							=> '新增',
'NEXTLOGINTEMPBLOCK' 			=> '為了安全考量, 請在%0秒以後才登入',
'NO' 							=> '否',
'NUM' 							=> '序',
'NOACCESSRIGHT' 				=> '抱歉, 您沒有足夠權利執行這功能',
'NOCHANGEMADETO' 				=> '%0沒有變動',





'NOEVENTRIGHT' 					=> '抱歉, 您所屬的用戶群沒有權利處理這項目',
'NOINPUT' 						=> '請輸入%0',
'NOMEMO' 						=> '此紀錄沒有備忘錄',
'NOMULTIPLEBET' 				=> '這些選擇不含多重遊戲',
'NONE' 							=> '無',
'NORECORDADDED' 				=> '沒有增加紀錄',
'NORMAL' 						=> '正常',

'NOT_EXIST' 					=> ' 不存在或記錄不存在。 ',
'NOTAPPLICABLE' 				=> '不適用',
'NOTAVAILABLE' 					=> '不可用',
'NOTAVAILABLESHORT' 			=> 'N/A',
'NOTHINGCHANGE' 				=> '沒什麼更新',

'NOTTALLY' 						=> '%0與%1不吻合。 ',
'NUMBER' 						=> '號碼',
'NUMBERSCORESET' 				=> '比分數量',
'NOABNORMALSTATUS' 				=> '無異常狀態',

'ODD' 							=> '賠率',
'ODDSTYPE' 						=> '賠率型別',

'OFF' 							=> '關掉',
'OFFLINE' 						=> '離線',

'OK' 							=> '確定',
'OLDMEMBER' 					=> '舊會員',

'ON' 							=> '開啟',
'ONLINE' 						=> '在線',

'OPEN' 							=> '打開',
'OPENALL' 						=> '全部打開',
'OPENINGBALANCE' 				=> '打開餘點',
'OPENSELECTEDMATCH' 			=> '打開選擇的賽事?',
'OPENCLOSEPAUSESELECTED' 		=> '%0選擇%1?',
'OPERATOR' 						=> '操作員',
'OPERATORPASSWORD' 				=> '操作員密碼',



'OR' 							=> '或',
'ORDER' 						=> '排序',
'OTHER' 						=> '其它',
'OTHERDATA' 					=> '其它資料',


'OU' 							=> '大/小盤',
'OUTOFRANGE' 					=> '%0是在控制範圍之外。 ',
'OUTPUT' 						=> '產量',

'OVER' 							=> '大',
'OVER_500_LINE' 				=> '文件是已超過500行',

'OVER_FILE_VALID' 				=> '文件是確實的, 及已成功被上載了。 ',
'OVER_UPLOAD_ATTACK' 			=> '文件可能上載時被攻擊!',




'OVER' 							=> '超過',
'PAGE' 							=> '頁',
'PANEL' 						=> '盤區',
'PARAM' 						=> '參量',
'PARENT' 						=> '父母',
'PASSLASTCHANGEON' 				=> '密碼最後變更在',
'PASSWORD' 						=> '密碼',
'PASSWORDRESETED' 				=> '重新設置密碼已完成了',


'PASSWORDEXPIRED' 				=> '您的密碼已經過期了! 請先更新您的密碼後再重新登入。 ',
'PAUSE' 						=> '暫停',
'PAUSEALL' 						=> '暫停所有',
'PAUSESELECTEDMATCH' 			=> '暫停選擇的賽事?',

'PAYEE' 						=> '收款人',
'PAYEENAME' 					=> '收款人名字',
'PAYMENTMETHOD' 				=> '付款方法',
'PAYOUT' 						=> '支出',
'PAYOUTLOCAL' 					=> '本地支出',
'PAYOUTLOCATION' 				=> '出款出處',

'PEN' 							=> '筆',
'PENDING' 						=> '等待',
'PENDING_LEDGER' 				=> '未確認帳目',
'PENDINGBET' 					=> '未確認',
'PENDINGWITHDRAWAL' 			=> '待批提點',
'PENDINGDEPOSIT' 				=> '待批存點',
'PENDINGTRANSACTION' 			=> '待批',
'PERCENTAGE' 					=> '百分比',
'PERMISSION' 					=> '權限',
'PERSONALINFO' 					=> '個人信息',


'PHONE' 						=> '電話號碼',
'PLACE' 						=> '地方',
'PLEASELOGIN' 					=> '請登入',
'PLEASESELECT' 					=> '請選擇一個%0',
'PLEASESELECTANITEM' 			=> '請選擇至少一個項目',
'PLEASESELECTANRECORD' 			=> '請選擇至少一個記錄',
'PLEASESELECTATLEAST' 			=> '請選擇至少一個%0',
'PLEASESELECT_REJECTREASON' 	=> '請選擇拒絕原因',
'PLEASE_INPUT' 					=> '請輸入 ',
'PLEASE_SELECT' 				=> '請選擇',
'PLEASE_SELECT_CSV_FILE' 		=> '請選擇csv文件',

'POINT' 						=> '點',
'POSSIBLEPAYOUT' 				=> '可贏額',
'POSTCODE' 						=> '郵政區號',

'PRICE' 						=> '價錢',
'PRIORITY' 						=> '優先權',
'PRIORITYMINOR' 				=> '較小',
'PRIORITYNORMAL' 				=> '普通',
'PRIORITYURGENT' 				=> '急迫',
'PROCESSED' 					=> '審理通過',
'PROCESSING' 					=> '處理中',
'PROCESSTIME' 					=> '處理時間',
'PRODUCTCATEGORY' 				=> '產品類別',
'PRODUCT' 						=> '產品',
'PROFILE' 						=> '簡介',
'PROFITLOSS' 					=> '贏/輸',
'NETWINLOSS'					=> '净赢/输',
'PROFITLOSSLOCAL' 				=> '本地贏/輸',

'PROFITONLY' 					=> '盈利而已',
'PROMOCAMPAIGN' 				=> '紅利活動',
'PROMOCASH' 					=> '紅利',
'PROMOCASHAMT' 					=> '紅利數額',
'PROMOCASHAMTLOCAL' 			=> '本地紅利數額',
'PROMOCASHFROM' 				=> '紅利從',
'PROMOCASHREPORT' 				=> '紅利報告',
'PROMOCASHTO' 					=> '紅利授予',
'PROVIDER' 						=> '供應商',
'PUBLISHER' 					=> '發行者',
'PUBLISHEROVERWRITEMESSAGE' 	=> 'By checking this box, "Article Name", "Summary", and "Description Body" will be copied to other language edition.',
'PUNTERPROFILE' 				=> '遊戲者簡介',
'QUICKJUMP' 					=> '快速跳動',


'RANGE' 						=> '範圍',
'RANGEERROR' 					=> '範圍到的數額必須大於範圍從的數額',
'RANGEFROM' 					=> '範圍從',
'RANGETO' 						=> '範圍到',


'RANK' 							=> '等級',
'REAPPLYRISKPROFILE' 			=> "所有未完成的項目和他們的市場風險設置使用這外形將是根據現在的數額更新。這將重寫的\\n既使市場現在使用風險設置。\\n您希望進行?",
'REASON' 						=> '原因',
'RECORD' 						=> '筆記錄',

'RECORD2' 						=> '記錄',
'RECORDADDED' 					=> '%0個記錄增加',
'RECORDING' 					=> '語音紀錄',
'RECOVER' 						=> '復原',
'RECOVERSELECTEDBET' 			=> '恢復選擇的賭注?',
'RECACULATE' 					=> '重新結算',
'RECACULATEVALIDSTAKE' 			=> '重新結算有效注資',
'RECACULATEVALIDSTAKEZERO' 		=> '重新結算有效注資為零',



'REFCODE' 						=> '參考代碼',
'REFNO' 						=> '參考代碼',
'REFPRICE' 						=> '參考價錢',
'REFRESH' 						=> '刷新',
'REFUND' 						=> '退款',
'REGERROR_NOINPUT' 				=> '您必須輸入您的%0',

'REGERROR_USEDINPUT' 			=> '這%0已被使用了。請再輸入另外一個%0',
'REGION' 						=> '地區',
'REGISTEREDON' 					=> '註冊時間',
'REGISTERCHANNEL' 				=> '註冊管道',
'REGISTERVALIDITY' 				=> '報名截止',
'REGISTERMEMBER' 				=> '報名人數',
'REGULATORACCESS' 				=> '調整者訪問',
'REJECT' 						=> '拒絕',
'REJECTREASON' 					=> '拒絕原因',
'REJECTREASONMANAGE' 			=> '拒絕原因管理',
'REJECTED' 						=> '被拒絕',
'RELEASE' 						=> '釋放',
'REMARK' 						=> '備註',
'REMARKMANAGE' 					=> '備註管理',
'REMOVE' 						=> '除去',
'REMOVEALL' 					=> '除去所有',
'REOPEN' 						=> '再開',

'REPLY' 						=> '回复',
'REPORT' 						=> '報表',
'REPORTEDBY' 					=> '張貼',

'RESET' 						=> '重新設置',
'RESETPASSWORD' 				=> '重新設置密碼',
'RESIDENTIALADDR' 				=> '住宅地址',
'RESOLVED' 						=> '已解決',
'RESPOND' 						=> '回應',
'RESPONDEDON' 					=> '回應在',
'RESPONDTIMEOUT' 				=> '暫停回應',
'RESPONSETIME' 					=> '回應時間',
'RESULT' 						=> '賽果',
'RESULTED' 						=> '被賽果',
'RETURNED' 						=> '返回',
'RETYPEPASSWORD' 				=> '重新輸入密碼',
'RECEIVEAMOUNT' 				=> '接收數額',
'REVERT' 						=> '還原',



'RISK' 							=> '風險',
'ROBOT' 						=> '自動化',
'ROLE' 							=> '系統角色',
'RUNNING' 						=> '走地',
'SAMEASABOVE' 					=> '同上述一樣',

'SAMEPASS' 						=> '同樣密碼',
'SAMEIP' 						=> '同樣IP',
'SAMEBETTABLE' 					=> '同局人數',
'FIRSTTHEREESAMEIP' 			=> '前三組IP一樣',
'SAMEBETIP' 					=> '同局遊戲IP一樣',
'SAMEFIRSTTHEREEBETIP' 			=> '同局遊戲前三組IP一樣',
'SAMEDEPOSITNAME' 				=> '同樣存戶名字',
'SAMERES' 						=> '同樣區域',
'SHORTCUTSAMEPASS' 				=> '密碼',
'SHORTCUTSAMEIP' 				=> 'IP',
'SHORTCUTFIRSTTHEREESAMEIP' 	=> '前三組IP',
'SHORTCUTSAMEBETIP' 			=> '遊戲IP',
'SHORTCUTSAMEFIRSTTHEREEBETIP' 	=> '遊戲前三組IP',
'SHORTCUTSAMEDEPOSITNAME' 		=> '戶名',
'SHORTCUTSAMERES' 				=> '區域',


'SAVE' 							=> '儲存',
'SAVINGACC' 					=> '儲蓄戶口',
'SCHEDULE' 						=> '進度表',

'SCORE' 						=> '比數',
'SEARCH' 						=> '查詢',
'SEARCH_FOR' 					=> '搜尋',
'SECOND' 						=> '秒',
'SECONDS' 						=> '秒',
'SECRETANSWER1' 				=> '秘密答复1',
'SECRETQUESTION' 				=> '私人問題',
'SECRETQUESTION1' 				=> '您母親的姓名?',
'SECRETQUESTION2' 				=> '您最喜愛的書?',
'SECRETQUESTION3' 				=> '您最喜愛的隊伍?',
'SECRETQUESTION4' 				=> '您最喜愛的電影?',
'SECURITY' 						=> '保安系統',
'SECURITYINFO' 					=> '保安資料',
'SELECT' 						=> '請選擇',
'SELECTALL' 					=> '選擇所有',
'SELECTION' 					=> '市場選擇',
'SELECTION_REMARK' 				=> '選擇評論',
'SELECTLANGUAGE' 				=> '選擇語言',
'SEQUENCENUMBER' 				=> '順序',
'SERVER' 						=> '服務器',

'SESSIONEXPIRED' 				=> '為了安全考量, 您的登入期限已過。請重新登入',
'SETTING' 						=> '設定',
'SETTLED' 						=> '已結算',
'SETTLEMENT' 					=> '注單計算',
'SETTLEMENTDATE' 				=> '注單計算日期',

'SETTLEMENTOUTRIGHTERROR' 		=> '這個奪標市場是未賽, 所有市場必須賽果在註單之前。 ',
'BIGSHAREHOLDER' 				=> '大股東',
'SHAREHOLDER' 					=> '股東',
'SHAREPERCENTAGE' 				=> '佔成數',
'SHORTAVERAGELAY' 				=> '平均Lay',
'SHORTBETSLIP' 					=> '注單',
'SHORTBETSLIPHOLD' 				=> 'Slip/Hold',
'SHORTMARKETPERCENTAGE' 		=> 'Mkt %',
'SHORTMULTIPLETOTALHOLD' 		=> '持有(M)',
'SHORTNAME' 					=> '簡名',
'SHORTMULTIPLEPAYOUT' 			=> '支付(M)',
'SHORTPROFITLOSS' 				=> '贏/輸',

'SHOW' 							=> '顯示',
'SHOWCOLUMN' 					=> '顯示欄',
'SHOWCOMPETITION' 				=> '顯示比賽',
'SHOWHIDEMESSAGE' 				=> '%0已選擇項目?',
'SHOWLEAGUE' 					=> '顯示聯盟',
'SHOWSPORT' 					=> '顯示體育',
'SHOWTESTACCT' 					=> '顯示測試賬號',
'SHOWUNTIL' 					=> '顯示到',

'SIGNINGIN' 					=> '登入中...請稍等。 ',
'SINCEREGISTERED' 				=> '從已註冊',
'SINGLE' 						=> '單',
'SINGLEBET' 					=> '下單註',
'SLIPHOLD' 						=> 'Slip/Hold',


'SLOW' 							=> '慢',
'SORT' 							=> '排序',
'SORTBY' 						=> '排序',
'SOUNDALERT' 					=> '音效提示',

'SPORT' 						=> '體育',
'SPORTCODE' 					=> '體育代碼',
'SPORTS' 						=> '體育',
'SPORT_PROFILE' 				=> '體育簡介',
'SQLCONSOLE' 					=> 'SQL控制台',
'SQLERROR' 						=> '數據庫檢測',
'SQLMODE' 						=> 'SQL方式',
'SQLSTATEMENT' 					=> 'SQL聲明',

'STAKE' 						=> '注額',
'STAKEMORETHAN' 				=> '注額大於',
'STAKELOCAL' 					=> '本地註額',
'STAKEPERBET' 					=> '單註注額',
'STARTDATETIME' 				=> '開賽時間',

'STATE' 						=> '直轄市或省',
'STATUS' 						=> '狀態',
'STRING' 						=> '串',

'STOP' 							=> '停止',
'SUBMIT' 						=> '提交',
'SUBMITNEWTICKET' 				=> '提交新帖子',
'SUBAGENT' 						=> '副代理',
'SUCESSFUL' 					=> '成功',
'SUPPORTTICKET' 				=> '帖子支持',
'SUPPORTTOOLS' 					=> '支撐工能',
'SUPPOSEDLY' 					=> '假想',
'SUMMARY' 						=> '總結',
'SUMMARYVIEW' 					=> '概略看法',
'SURVEY' 						=> '市調問卷',
'SUSPEND' 						=> '停用',
'SUSPENDED' 					=> '停用',
'SUSPENDSELECTEDROW' 			=> '停用已選擇紀錄?',
'SUSPICIOUSIP' 					=> '可疑IP',
'SWAPPRICE' 					=> '交換價錢',
'SWAPPRICESHORT' 				=> 'SW',

'SYSLOG' 						=> '系統記錄',
'SYSTEM' 						=> '系統',
'SYSTEMCALCULATION' 			=> '系統計算',

'SYSTEMEXPIRED' 				=> '為了安全考量, 您的系統已經過期了。 ',
'SYSTEMMULTIMAXPRICEBREACHED' 	=> '抱歉, 您遊戲不成功。 ',
'SYSTEMNAME' 					=> '1Bet2u系統',
'SYSTEMTIME' 					=> '系統時間',
'SYSTEMVERS' 					=> '0.10',
'SYSTEMLOG' 					=> '系統記錄',
'SYSTEM_NOT_MAINTENANCES' 		=> '系統不在於維修狀態!',


'SYSVAR' 						=> '系統制變',
'TAG' 							=> '記號',
'TAGMANAGE' 					=> '記號管理',
'TAGNAME' 						=> '記號名稱',
'TAGCOLOR' 						=> '記號顏色',
'TELEPHONENO' 					=> '電話號碼',



'TEMPBLOCK' 					=> '登入暫停了%0分鐘',
'TERMS' 						=> '期限',
'TEST' 							=> '測試',
'THISMONTH' 					=> '這個月',

'TIME' 							=> '時間',
'TIMEREMAINING' 				=> '剩餘時間',
'TIMEZONE' 						=> 'GMT時區',






'TITLE' 						=> '稱號',
'TO' 							=> '到',
'TODAY' 						=> '今日',
'TOOL' 							=> '工具',
'TOTAL' 						=> '共計',
'TOT' 							=> '總',
'TOTALBETMEMBER' 				=> '下注人數',
'TOTALDEPOSIT' 					=> '總存點',
'TOTALHOLD' 					=> '總持有',
'TOTALMARKETPERCENTAGE' 		=> '總Mkt%',
'TOTALPAYOUT' 					=> '總支出',
'TOTALSTAKE' 					=> '總注額',
'TOTALWITHDRAWAL' 				=> '總提點',
'TOTALBALANCE' 					=> '總餘點',
'TOURNAMENT' 					=> '錦標賽',
'TRACKERLIVE' 					=> '在線使用者',
'TRACKERLOG' 					=> '登入記錄',
'TRANSACTION' 					=> '交易',
'TRANSACTIONID' 				=> '交易碼',
'TRANSACTIONACCEPTED' 			=> '您的交易已成功被接受了',
'TRANSACTIONAL' 				=> '交易',
'TRANSACTIONAMT' 				=> '點數',
'TRANSACTIONDATA' 				=> '交易資料',
'TRANSACTIONDATE' 				=> '交易日期',
'TRANSACTIONDETAILS' 			=> '交易記錄',
'TRANSACTIONENQUIRY' 			=> '交易記錄',


'TRANSACTIONSUCCESS' 				=> '您的交易將會盡快被處理',
'TRANSACTIONSUCCESSSAVEBANKINFO' 	=> '您的交易將會盡快被處理。您想更新您的銀行資料嗎?',
'TRANSACTIONTYPE' 					=> '交易類型',
'TRANSFERAMT' 						=> '數額',
'TRANSFERAMTLOCAL' 					=> '本地數額',
'TRANSFERDATE' 						=> '調動日期',

'TRANSFERSUCCESS' 					=> '調動成功',
'TURNOVER' 							=> '洗碼量',



'TXNFEE' 						=> '交易費',
'TYPE' 							=> '類別',
'TIMES' 						=> '倍',
'UNBLOCK' 						=> '解除封鎖',
'UNCHECK_ALL' 					=> '全清除',
'UNDEFINED' 					=> '未定義',
'UNDOSETTLEMENT' 				=> '撤消注單',
'UNREGISTERED' 					=> '未登記',
'UNRESOLVED' 					=> '未解決',
'UNRESULTED' 					=> '沒有賽果',

'UNSETTLEBEFOREUPDATE' 			=> '注單只可在比分更新前撤消。 ',
'UNSETTLED' 					=> '未結算',
'UNSETTLEDBETS' 				=> '沒有遊戲單',

'UP' 							=> '上',
'UPDATE' 						=> '更新',
'UPDATED' 						=> '已更新',
'UPDATEEVENT' 					=> '更新項目',
'UPDATEFAIL' 					=> '更新失敗',


'UPTO' 							=> '上至',
'URLPARAM' 						=> 'URL參數',
'USECUSTOM' 					=> '使用自定?',

'USER' 							=> '用戶',
'USER_BEAR' 					=> '用戶承擔',
'USERGROUP' 					=> '用戶小組',
'USERNAME' 						=> '用戶名',
'USERONLINE' 					=> '網上用戶',
'USERTYPE' 						=> '用戶類別',

'VALID' 						=> '有效',
'VALIDATED' 					=> '確認',
'VALIDATIONDATE' 				=> '檢驗日期',
'VALIDITYPERIOD' 				=> '有效期限',
'VALIDSTAKE' 					=> '有效注額',
'VALIDTOTALSTAKE' 				=> '有效總注額',


'VALUE' 						=> '價值',
'VENUE' 						=> '地點',
'VERSUS' 						=> 'VS',

'VIEW' 							=> '查看',
'VIEWMEMBER' 					=> '觀看會員',
'VIEWMEMO' 						=> '觀看備忘錄',
'VOLUME' 						=> '數量',

'WAGER' 						=> '遊戲',
'WAGERREPORT' 					=> '注單報告',

'WEB' 							=> '網站',
'WEBSITE' 						=> '網站',

'WEBSITEDESC' 					=> '網站介紹',
'WEEKLY' 						=> '每週(星期三至星期二)',


'WILDCARD' 						=> '(最少3個字)',
'WIN' 							=> '贏',
'WINHALF' 						=> '贏半',
'WINNER' 						=> '勝利者',
'WITHDRAW' 						=> '提取',


'WITHDRAW_ALERT1' 				=> '這個帳號提點總數超過或等於現時現金點數的一半(50%)!',
'WITHDRAW_ALERT2' 				=> '原本現金現時信用與餘點',
'WITHDRAWAL' 					=> '提點',
'WITHDRAWALCODE' 				=> '提點驗證碼',
'WITHDRAWAL_TOBEPROCESSED' 		=> '提點申請待處理',
'WITHDRAWALCONDITION1' 			=> '下注總款 >（存點+現金優惠活動）X ',
'WITHDRAWALCONDITION2' 			=> '下注總款 > 餘點 X 1',
'WITHDRAWALCONDITION3' 			=> '下注總款 > 最新存點',
'WITHDRAWALCONDITION4' 			=> '提點 < （存點 X 10）',
'WITHDRAWALCONDITION5' 			=> '提點時的IP與其他用戶相同',
'WITHDRAWALCONDITION6' 			=> '提點 < （餘點 X 2）',
'WITHDRAWALRESTRICTION' 		=> '提點限制',
'WITHDRAWAMT' 					=> '提點數額',

'WITHDRAWBALANCE' 				=> '在提點以後的餘點',
'WITHDRAWBALANCELOCAL' 			=> '在提點以後的餘點(AUD)',
'WITHDRAWREPORT' 				=> '提點報告',


'WORKPHONE' 					=> '工作電話號碼',
'WRONGFIELDNAME' 				=> '文件專欄有無效字段名, 也許是字段名包括間距在各個字符之間! 請檢查文件。戒備: "文件必須是ANSI和DOS代碼!"',
'WRONGONLY' 					=> '錯誤而已',
'WINLOSEMORETHAN' 				=> '輸贏大於',

'YES' 							=> '是',
'YESTERDAY' 					=> '昨日',

'YEAR' 							=> '年',
'YEARTODATE' 					=> '迄今年份',
'YOURANSWER' 					=> '您的答复',
'YOURDETAILS' 					=> '您的細節',

'NEWCURRATE' 					=> '新的匯率',
'EFFECTDATE' 					=> '生效日期',
'WEBSITEURL' 					=> '網站網址',
'WEBSITENAME' 					=> '網站名稱',
'COMPANYNAME' 					=> '公司名稱',
'BANKBRANCHNAME' 				=> '分行公司名稱',
'BANKADDRESS' 					=> '銀行地址',
'PROFITRANGESTART' 				=> '利潤範圍從',
'PROFITRANGEEND' 				=> '利潤範圍至',
'ACTIVEMEMBERRANGESTART' 		=> '活躍會員範圍從',
'ACTIVEMEMBERRANGEEND' 			=> '活躍會員範圍至',
'PERCENTAGERATE' 				=> '比率',






'NOPROFITRANGE' 				=> '請提供一個有效的利潤範圍',
'NOPROFITRANGEEND' 				=> '利潤範圍從必須大於利潤範圍至或0',
'INVALIDPROFITRANGE' 			=> '利潤範圍經已存在。請提供另一個有效的利潤範圍',
'NOMEMBERRANGE' 				=> '請提供一個有效的活躍會員範圍',
'NOMEMBERRANGEEND' 				=> '活躍會員範圍從必須大於活躍會員範圍至或0',
'SETTLECURR' 					=> '結算貨幣',
'NICKNAME' 						=> '暱稱',
'TELEPHONENO1' 					=> '聯絡號碼1',
'TELEPHONENO2' 					=> '聯絡號碼2',
'LOCATION' 						=> '地址',
'VIPSTATUS' 					=> 'VIP狀態',
'COMMSCHEMECODE' 				=> '佣金方案代碼',
'GAINAMTROLLOVER' 				=> '累計輸贏',
'COMMROLLOVER' 					=> '累計佣金',
'COMMFORECAST' 					=> '預測佣金',

'COMMREPORT' 					=> '佣金報告',
'AFFILIATECODE' 				=> '代理聯盟代碼',
'GROSSWINLOSS' 					=> '總輸贏',
'SUBCOST' 						=> '副成本',
'TURNOVERSTAKE' 				=> '有效遊戲額',
'TURNOVERSTAKE2' 				=> '有效遊戲',
'NETTWINLOSS' 					=> ' 淨輸贏',
'ACTIVEMEMBERS' 				=> '活躍會員',



'COMMRATE' 						=> '佣金比率',
'COMMRATE2' 					=> '佣金比例',
'TOTALCOMM' 					=> '總佣金',
'LASTMONTH' 					=> '上個月',
'CURRENTMONTH' 					=> '這個月',
'PROCESSCOMMISSION' 			=> '處理佣金',
'PROCESSEDCOMMISSION' 			=> '佣金已經處理',
'TIMEFRAME' 					=> '時限',
'MONTHSELECTION' 				=> '選擇月份',
'YEARSELECTION' 				=> '選擇年份',
'COMMTRANSBREAKDOWN' 			=> '交易故障',
'Q1RESULTED' 					=> '首刻',
'HTRESULTED' 					=> '半場',
'Q3RESULTED' 					=> '第三刻',
'FTRESULTED' 					=> '全場',
'WRONGSEQUENCE' 				=> '次序錯誤',
'CREDITED' 						=> '已經進賬',
'PRICEMIN' 						=> '最低價格',
'RESULTMIN' 					=> '最低賽果',
'MAXDEPOSIT' 					=> '最高存點數額',
'OTHERS' 						=> '其它',
'TRADEADJUSTMENT' 				=> '交易調整',
'ADJUSTMENTSEARCHCAT' 			=> 'Search Categories',
'ADJUSTMENTMETHOD' 				=> '調整方式',
'ADJUSTMENTREIMDEPOSIT' 		=> '補還存點',
'ADJUSTMENTCASHOFFER' 			=> 'Cash Offer',
'ADJUSTMENTOFRATE' 				=> '調整點數',
'TRANSACTIONCODE' 				=> '交易碼',
'ADJREIMBURSEMENTDEPOSIT' 		=> '交易 - 補還存點',
'ADJCASHOFFER' 					=> '交易 - Cash Offer',

'ADJRATE' 						=> '交易 - 調整點數',
'ADJTRANSCODE' 					=> 'Reimburse Deposit Transaction Code',
'NOTE' 							=> 'Note',
'PAYMENT' 						=> 'Payment',
'ADJUSTMENTERROR' 				=> 'The transaction you requested does not exist',
'MANPENDING' 					=> 'Manual Pending',
'MANCONFIRMED' 					=> '批准',
'MANCANCELLED' 					=> '手動取消',
'ADJUSTMENTREIMBURSEERROR' 		=> 'Transaction reimbursement has been done before',
'ACCOUNTDETAILACTIVE' 			=> '正常',
'ACCOUNTDETAILSUSPENDED' 		=> '會員賬號已被停用',
'BANKHOLDINGBRANCH' 			=> 'Bank Holding Branch',
'SORTCODE' 						=> 'Sort Code',
'BANKPROVINCE' 					=> 'Bank Province',
'BANKCITY' 						=> 'Bank City',
'BANKSAVINGACCNO' 				=> 'Saving Account',
'DEFAULT' 						=> '默認',
'ADJUSTMENTACCNA' 				=> '無此會員賬號',
'NORESULT' 						=> 'No result',


'ZHANG' 						=> '張',

//JP begin
'MARKETING' 					=> '營銷',
'INSERT' 						=> '載入',

'GAMEPERIOD' 					=> '遊戲期限',
'REWARD' 						=> '回酬',


'AFFILIATE' 					=> '代理',
'GAMECOUNT' 					=> '遊戲統計',
'TAG' 							=> '記號',
'MARKETINGTASK' 				=> '營銷任務表',
'CUSTOMERSEARCH' 				=> '客戶搜索',
'POTENTIALCUSTOMERINQUIRY' 		=> '潛在客戶查詢',
'EXISTINGCUSTOMERINQUIRY' 		=> '現有客戶查詢',
'CUSTOMERFILTER' 				=> '客戶過濾器',
'POTENTIALCUSTOMER' 			=> '潛在客戶過濾器',
'CONDITION' 					=> '條件',
'SOURCE' 						=> '源頭',
'BIRTHYEAR' 					=> '出生年份',
'SAVINGAMOUNT' 					=> '儲蓄數目',
'TRANSACTIONAMOUNT' 			=> '交易數目',
'GAME' 							=> '遊戲',
'PROMOTION' 					=> '推廣',
'ACCOUNTDETAILS' 				=> '帳戶資料',
'TRANSACTIONVIEW' 				=> '交易欄',
'GAMEVIEW' 						=> '遊戲欄',
'REWARDVIEW' 					=> '回酬欄',
'EXPIREON' 						=> '有效期至',
'UPLOADCUSTOMER' 				=> '上載客戶資料',
'POTENTIALID' 					=> '潛在客戶ID',
'ASSIGNTO' 						=> '指派至',
'SECRETANSWER2' 				=> '秘密答复2',
'UPLOADON' 						=> '上載於',
'MEMBERID' 						=> '會員ID',
'MEMBERNAME' 					=> '姓名',
'DATEFROM' 						=> '日期從',
'TILL' 							=> '至',
'TODAY' 						=> '今日',
'YESTERDAY' 					=> '昨日',
'LAST7DAYS' 					=> '前7日',
'THISMONTH' 					=> '本月',
'KEYWORD' 						=> '關鍵字',
'EXACT' 						=> '準確',
'QQCONTACT' 					=> 'QQ',
'CONTACTDETAIL' 				=> '聯絡資料',
'SEARCHRESULT' 					=> '搜索結果',
'INTERESTED' 					=> '有興趣',
'NOTINTERESTED' 				=> '沒有興趣',
'CONSIDERING' 					=> '考慮',
'CUSTOMERCOMMENT' 				=> '客戶意見',
//JP end
//


'RECONCILATION' 				=> '核對',
'RECONCILE' 					=> '核對',
'RECONCILEINCENTIVE' 			=> '核對退水優惠活動',
'RECONCILEBONU​​S' 				=> '核對現金優惠活動',
'RECONCILE_NOTTALLY_AMT' 		=> '數額不符',
'RECONCILE_NOTTALLY_FEE' 		=> '費用不符',
'RECONCILE_NOTTALLY_BOTH' 		=> '數額與費用不符',
'RECONCILE_TALLY' 				=> '已核對',
'RECONCILE_NOTEXIST' 			=> '交易不存在',

'RECONCILE_STATUS' 				=> '核對狀況說明與摘要',
'RECONCILATIONSUMM' 			=> '核對摘要',
'FILETOTAL' 					=> '文件總數',
'DBTOTAL' 						=> '數據庫資料總量',
'TALLYTOTAL' 					=> '相符總數',
'NOTTALLYTOTAL' 				=> '不相符總數',


'REPORTCHART' 					=> 'ReportChart',
'REPORTLIST' 					=> 'ReportList',
'MEMBER_INCENTIVE' 				=> 'MemIncentive',
'POSTREPORT' 					=> 'PostReport',
'AGENT_COMMISSION' 				=> 'AgentCommission',
'AGENTCOMMISSION' 				=> 'AGCommission',
'TOTALREPORTLIST' 				=> 'TotalReport',
'SEARCH_MEM_REPORTlIST' 		=> 'SearchMem',
'WITHDRAWAL_LIST' 				=> 'WithdrawalList',
'TRANX_AUDIT' 					=> 'TranxAudit',
'OTHERSET' 						=> 'Other Set',
'CHAT' 							=> 'Chat',
'AFFILIATELIST' 				=> '代理列表',

'ORGLIST' 						=> '代理組織',
'BALANCE_NOTICE' 				=> '負數通知',
'ACCBALANCE_BFW_FAIL' 			=> '帳戶餘點結轉失敗',


'ACCBALANCE_BFW' 				=> '帳戶餘點結轉',

'BETTYPE1X2' 					=> '獨贏',
'BETTYPEASIAHANDICAP' 			=> '讓分',
'BETTYPEOVERUNDER' 				=> '大小',
'BETTYPEODDEVEN' 				=> '單雙',
'BETTYPECORRECTSCORE' 			=> '波膽',
'BETTYPETOTALGOAL' 				=> '總入球',
'BETTYPEASIAHANDICAPRB' 		=> '走地讓分',
'BETTYPEOVERUNDERRB' 			=> '走地大小',
'BETTYPEODDEVENRB' 				=> '走地單雙',
'BETTYPEPARLAY' 				=> '過關',
'BETTYPEPARLAYHANDICAP' 		=> '過關讓分',
'BETTYPEHALFTIMEFULLTIME' 		=> '半全場',


'SELECTIONHOME' 				=> '主場',
'SELECTIONAWAY' 				=> '客場',
'SELECTIONDRAW' 				=> '和',
'SELECTIONOVER' 				=> '大',
'SELECTIONUNDER' 				=> '小',
'SELECTIONODD' 					=> '單',
'SELECTIONEVEN' 				=> '雙',
'SELECTIONH0C0' 				=> '0:0',
'SELECTIONH1C1' 				=> '1:1',
'SELECTIONH2C2' 				=> '2:2',
'SELECTIONH3C3' 				=> '3:3',
'SELECTIONH4C4' 				=> '4:4',
'SELECTIONH1C0' 				=> '1:0',
'SELECTIONH2C0' 				=> '2:0',
'SELECTIONH2C1' 				=> '2:1',
'SELECTIONH3C0' 				=> '3:0',
'SELECTIONH3C1' 				=> '3:1',
'SELECTIONH3C2' 				=> '3:2',
'SELECTIONH4C0' 				=> '4:0',
'SELECTIONH4C1' 				=> '4:1',
'SELECTIONH4C2' 				=> '4:2',
'SELECTIONH4C3' 				=> '4:3',
'SELECTIONOVH' 					=> '5:0 UP',
'SELECTIONH0C1' 				=> '1:0',
'SELECTIONH0C2' 				=> '2:0',
'SELECTIONH1C2' 				=> '2:1',
'SELECTIONH0C3' 				=> '3:0',
'SELECTIONH1C3' 				=> '3:1',
'SELECTIONH2C3' 				=> '3:2',
'SELECTIONH0C4' 				=> '4:0',
'SELECTIONH1C4' 				=> '4:1',
'SELECTIONH2C4' 				=> '4:2',
'SELECTIONH3C4' 				=> '4:3',
'SELECTIONOVC' 					=> '0:5 UP',
'SELECTION0~1' 					=> '0~1',
'SELECTION2~3' 					=> '2~3',
'SELECTION4~6' 					=> '4~6',
'SELECTION7UP' 					=> '7UP',
'SELECTIONFHH' 					=> '主/主',
'SELECTIONFHD' 					=> '主/和',
'SELECTIONFHC' 					=> '主/客',
'SELECTIONFDH' 					=> '和/主',
'SELECTIONFDD' 					=> '和/和',
'SELECTIONFDC' 					=> '和/客',
'SELECTIONFCH' 					=> '客/主',
'SELECTIONFCD' 					=> '客/和',
'SELECTIONFCC' 					=> '客/客',
































































































































































































































































































































































































































































'FIRSTHALF' 					=> '上半場',

'CANCELBETREASON1' 				=> '球頭錯誤',
'CANCELBETREASON2' 				=> '賠率錯誤',
'CANCELBETREASON3' 				=> '走地球頭錯誤',
'CANCELBETREASON4' 				=> '走地賠率錯誤',
'CANCELBETREASON5' 				=> '走地分數錯誤',
'CANCELBETREASON6' 				=> '賽程錯誤',
'CANCELBETREASON7' 				=> '紅卡錯誤',
'CANCELBETREASON8' 				=> '賽事取消',
'CANCELBETREASON9' 				=> '系統錯誤',
'CANCELBETREASON10' 			=> '走地進球銷單',
'CANCELBETREASON11' 			=> '系統註銷單',

'DAYOFWEEK1' 					=> '星期一',
'DAYOFWEEK2' 					=> '星期二',
'DAYOFWEEK3' 					=> '星期三',
'DAYOFWEEK4' 					=> '星期四',
'DAYOFWEEK5' 					=> '星期五',
'DAYOFWEEK6' 					=> '星期六',
'DAYOFWEEK7' 					=> '星期日',

'REGERROR_NAME_SHORT' 			=> 'Name too short',
'REGERROR_NAME_EMPTY' 			=> 'Name is required',
'REGERROR_ACCOUNT_SHORT' 		=> 'Account code too short',
'REGERROR_ACCOUNT_EMPTY' 		=> 'Account code is required',
'REGERROR_ACCOUNT_TAKEN' 		=> 'Your account code is already in used.',
'REGERROR_PASSWORD_SHORT' 		=> 'Password too short',
'REGERROR_PASSWORD_EMPTY' 		=> 'Password is required',
'REGERROR_CONFIRM_EMPTY' 		=> 'Confirm password is required',
'REGERROR_CONFIRM_NOMATCH' 		=> 'Confirm password does not match',
'REGERROR_WITHDRAWAL_SHORT' 	=> 'Withdrawal code is too short',
'REGERROR_WITHDRAWAL_ALNUM' 	=> 'Withdrawal code must be alphanumeric',
'REGERROR_WITHDRAWAL_EMPTY' 	=> 'Withdrawal code is required',
'REGERROR_PHONE_EMPTY' 			=> 'Phone number is required',
'REGERROR_PHONE_TAKEN' 			=> 'Your phone number is already in used.',
'REGERROR_EMAIL_EMPTY' 			=> '郵箱是必填項目',
'REGERROR_EMAIL_TAKEN' 			=> '郵箱已被註冊',
'REGERROR_ANSWER_EMPTY' 		=> 'Answer is required',
'REGERROR_CAPTCHA_SHORT' 		=> 'Captcha code is too short',
'REGERROR_CAPTCHA_ALNUM' 		=> 'Captcha code must be alphanumeric',
'REGERROR_CAPTCHA_EMPTY' 		=> 'Captcha code is required',

'ADJERROR_DEPOSITNOTFOUND' 		=> '無法查詢此交易',
'ADJERROR_DEPOSITPENDING' 		=> '存點尚未確認，不能補還',
'ADJERROR_DEPOSITCONFIRMED' 	=> '存點已經確認，不能補還',
'ADJERROR_DEPOSITADJUSTED' 		=> '存點已經補還',

'INVALIDFILEFORMAT' 			=> '檔案格式不對',

'CTABNAME' 						=> '名稱',
'CTABNAMENOTE' 					=> '所有提點都使用這個名稱',
'CTABPASSWORD' 					=> '設定密碼',
'CTABCHANGEPASSWORD' 			=> '更改登入密碼',
'CTABCONFIRMPASSWORD' 			=> '確認密碼',
'CTABPASSWORDNOTE' 				=> '長度在6至12個字符，必須英文字與數字混合(AZ，az，0-9)',
'CTABWITHDRAWALCODE' 			=> '提點驗證碼',
'CTABCHANGEWITHDRAWALCODE' 		=> '更改提點驗證碼',
'CTABCURRENCY' 					=> '貨幣',
'CTABBIRTHDAY' 					=> '生日',
'CTABBIRTHDAYNOTE' 				=> '年- 月- 日',
'CTABGENDER' 					=> '性別',
'CTABCONTACTINFO' 				=> '聯絡資料',
'CTABCOUNTRY' 					=> '國家',
'CTABADDRESS' 					=> '地址',
'CTABCITY' 						=> '城市',
'CTABSTATE' 					=> '狀態',
'CTABPOSTCODE' 					=> '郵區號',
'CTABPOSTCODENOTE' 				=> '五個或以上數字',
'CTABPHONENUMBER' 				=> '電話號碼',
'CTABPHONENUMBERNOTE' 			=> '例如: 012-3456789',
'CTABEMAIL' 					=> '郵箱',
'CTABREMARK' 					=> '備註',
'CTABREQUIRED' 					=> '必填項目',
'CTABOTHER' 					=> '其它',
'CTABCASHBALANCE' 				=> '現金結餘',
'CTABSTATUS' 					=> '狀況',
'CTABSECURITYQUESTION' 			=> '私人問題',
'CTABSECURITYANSWER' 			=> '私人答案',
'CTABAGENT' 					=> '代理',
'CTABTOTALDEPOSIT' 				=> '會員總存點',
'CTABTOTALWITHDRAWAL' 			=> '會員總提點',
'CTABLASTLOGINTIME' 			=> '最後一次登入時間',
'CTABLASTLOGINIP' 				=> '最後一次登入IP',
'CTABREGISTERIP' 				=> '註冊IP',
'CTABWITHDRAWAL' 				=> '提點',
'CTABWITHDRAWALLIMIT' 			=> '每日最高提點次',
'CTABWITHDRAWALMAX' 			=> '每日最高提點',
'CTABDEPOSITMAX' 				=> '每日最高存點',
'CTABBANKSETTING' 				=> '銀行設定',
'CTABNO' 						=> '序',
'CTABCODE' 						=> '代碼',
'CTABBANK' 						=> '銀行',
'CTABBANKACCOUNT' 				=> '銀行賬號',
'CTABTAG' 						=> '記號',
'TRANSACTIONSEARCH' 			=> '會員交易搜索',

'SPORTSBOOK' 					=> '體育競猜',
'MINBET' 						=> '單註最低',
'MAXBET' 						=> '單註最高',
'MAXMATCH' 						=> '單場最高',
'BONUSLIMIT' 					=> '現金優惠活動限制',
'NOOFTIMES' 					=> '次數',

'MEMBERINCENTIVE' 				=> '會員退水',
'AFFILIATEINCENTIVE' 			=> '代理退水',
'TOTALINCENTIVE' 				=> '總退水',
'FINALINCENTIVE' 				=> '最終退水',
'UPLOAD' 						=> '上載',
'CALLOUT' 						=> '電訪',
'CALLOUTENQUIRY' 				=> '電訪查詢',
'CALLOUTWORKSHEET' 				=> '電訪工作表',
'UPLOADFILE' 					=> '上載檔案',
'UPLOADCALLOUT' 				=> '上載名單',
'NOACTION' 						=> '沒有動作',
'PAUSEGAMING' 					=> '暫停遊戲',
'RELEASEPAUSE' 					=> '解除暫停',
'MANAGEBANKACCOUNT' 			=> '帳戶管理',
'LEVEL' 						=> '層級',
'REPORTSIMPLE' 					=> '報表總結',
'REPORTTOTAL' 					=> '報表',
'TAGMEMBER' 					=> '標記帳戶',
'ENQUIRY' 						=> '查詢',
'PROCESS' 						=> '處理',
'WAGERNUM' 						=> '注單數量',
'PAYMENTACCOUNT' 				=> '出款帳戶',
'INCLUDEFEE' 					=> '納入費用',
'PLEASESELECTPAYMENTACCOUNT' 	=> '請選擇出款帳戶',

'BANKER' 						=> '莊',
'PLAYER' 						=> '閒',
'TIE' 							=> '和',
'PBANKER' 						=> '莊對',
'PPLAYER' 						=> '閒對',
'BIG' 							=> '大',
'SMALL' 						=> '小',
'BACCARAT' 						=> '百家樂',
'ROLLETE' 						=> '輪盤',
'SHAIBAO' 						=> '骰寶',
'LONGHU' 						=> '龍虎',
'FANTAN' 						=> '番攤',
'DRAGON' 						=> '龍',
'TIGER' 						=> '虎',
'FAN' 							=> '番',
'JIAO' 							=> '角',
'SANMEN' 						=> '三門',
'NIAN' 							=> '念',
'TONG' 							=> '通',
'ODD' 							=> '單',
'EVEN' 							=> '雙',
'QUANWEI' 						=> '全圍',
'SANJUN' 						=> '三軍',
'DUANPAI' 						=> '短牌',
'CHANGPAI' 						=> '長牌',
'WEISE' 						=> '圍骰',
'DIANSHU' 						=> '點數',
'ZHIZHU' 						=> '直注',
'FENZHU' 						=> '分注',
'JIEZHU' 						=> '街注',
'JIAOZHU' 						=> '角注',
'XIANZHU' 						=> '線注',
'SANSHU' 						=> '三數',
'HANGZHU1' 						=> '行註一',
'HANGZHU2' 						=> '行註二',
'HANGZHU3' 						=> '行註三',
'4NUMBER' 						=> '四個號碼',
'DAZHU1' 						=> '打註一',
'DAZHU2' 						=> '打註二',
'DAZHU3' 						=> '打註三',
'HONGSE' 						=> '紅色',
'HEISE' 						=> '黑色',
'TABLENUM' 						=> '桌號',
'SHOENUM' 						=> '靴號',
'GAMENUM' 						=> '局號',

'CHECKSAMENAMEACCOUNT' 			=> '檢查會員名稱',
'CHECKCURRENTBALANCE' 			=> '檢查現時餘點',
'CHECKALLBALANCE' 				=> '檢查會員餘點',

'REPORTALL' 					=> '總帳',
'REPORTCATEGORY' 				=> '分類帳',
'REPORTDETAIL' 					=> '明細帳',
'MEMBERPNL' 					=> '會員贏/輸',
'COMPANY' 						=> '公司',
'MARKETSHARE' 					=> '市佔率',
'BATCH' 						=> '批量',
'FEEREPORT' 					=> '費用統計',
'INCENTIVEREPORT' 				=> '退水優惠活動表',
'TRANSUMMARY' 					=> '交易總結',
'TRANADJUST' 					=> '交易管理',
'BONUSSETTING' 					=> '優惠活動設定',
'INCENTIVESETTING' 				=> '退水設定',
'CAllOUTSETTING' 				=> '電訪設定',
'EXPORTTRANSACTION' 			=> '導出交易記錄',
'EXPORTACCOUNT' 				=> '導出賬號資料',
'USERMANAGE' 					=> '用戶管理',
'AFFILIATEDETAIL' 				=> '代理資料',
'AFFILIATEENQUIRY' 				=> '代理查詢',
'AFFILIATEREPORT' 				=> '代理報表',
'AFFILIATECOMMISSION' 			=> '代理佣金',
'AFFILIATETRANSACTION' 			=> '代理交易',
'MOVEMEMBER' 					=> '會員轉移',
'BANKCARD' 						=> '銀行卡',
'MINSTAKE' 						=> '下限',
'MAXSTAKE' 						=> '上限',
'CALCULATEINCENTIVE' 			=> '計算退水',
'INCENTIVECAMPAIGN' 			=> '退水活動',
'OPENSAMENAME' 					=> '開放同姓名',
'OVERMAXIMUMAMOUNT' 			=> '點數超過封頂值 %0',
'INTERNETTRANSFER' 				=> '網上轉點',
'ATMTRANSFER' 					=> 'ATM轉點',
'CASHTRANSFER' 					=> '現金存點',
'MAINTENANCE' 					=> '維護中',
'OTHERBANK' 					=> '跨行',
'ASSIGNDATETIME' 				=> '分配時間',
'REMINDER' 						=> '提醒',
'COMPLETED' 					=> '完成',
'BLACKLISTED' 					=> '黑名單',
'MAINWALLET' 					=> '中心點包',
'SPORTWALLET' 					=> '體育',
'CASINOWALLET' 					=> '真人視訊',
'KENOWALLET' 					=> '快樂彩',
'DAILYDEPOSIT' 					=> '每日存點',
'DEPOSITREBATE' 				=> '存點回饋',
'TRIALACCOUNT' 					=> '測試賬號',

'PAYMENTGATEWAY' 				=> '支付平台',
'PAYMENTGATEWAY1' 				=> '支付寶',
'PAYMENTGATEWAY2' 				=> '財付通',
'PAYMENTGATEWAY3' 				=> '網銀在線',
'PAYMENTGATEWAY4' 				=> '快錢',
'PAYMENTGATEWAY5' 				=> '易寶',
'PAYMENTGATEWAY6' 				=> '環迅',

'COMMISSIONTYPE_1' 				=> '註冊',
'COMMISSIONTYPE_2' 				=> '分成',
'COMMISSIONTYPE_3' 				=> '退水',

'LUCKYDRAW' 					=> '抽獎',
'LUCKYBll' 						=> '抽獎球',
'LUCKYDRAWID' 					=> '期數',
'LUCKYDRAWNO' 					=> '抽獎期號',
'LUCKYDRAWSETTING' 				=> '抽獎設定',
'LUCKYDRAWRESULTSETTING' 		=> '開獎設定',
'LUCKYEND' 						=> '結束',
'LUCKYPENDING' 					=> '等待',
'LUCKYPRIZE' 					=> '獎金',
'LUCKYPRIZETEXT' 				=> '獎金信息',
'LUCKYPRIZESETTING' 			=> '獎金設定',
'LUCKYRESULT' 					=> '結果',
'LUCKYSTART' 					=> '開始',
'LUCKYTIMENO' 					=> '次數',
'LUCKYANNOUNCEMENT' 			=> '公告',
'LUCKYTOTALNUMBER' 				=> '縂數',
'LUCKY_YES' 					=> '是',
'LUCKY_NO' 						=> '否',


'SAMEIPCHECK' 					=> '查詢相同IP',
'SAMEIP' 						=> '相同IP',
'MAINTENANCES' 					=> '維修中',

'TOTALNUM' 						=> '總數',
'CHANGES' 						=> '變化',
'CASINO' 						=> '真人',
'KENO' 							=> '快樂彩',
'AVERAGE' 						=> '平均',
'AVERAGEAMOUNT' 				=> '平均點數',
'AVERAGETRANS' 					=> '平均筆數',
'REGISTRATION' 					=> '註冊',
'EVENTREVIEW' 					=> '活動分析',
'BEFOREEVENT' 					=> '活動前期',
'PREEVENT' 						=> '活動宣傳',
'ONEVENT' 						=> '活動期間',
'POSTEVENT' 					=> '活動後期',
'EVENTTURNOVER' 				=> '打碼',
'EVENTACTIVATE' 				=> '激活',
'TRACEROUTE' 					=> '路由追踪',
'TRANSACTINFORM' 				=> '交易通知',
'TRANSFERINFORM' 				=> '轉點通知',
'OLDMEM_CALLOUT' 				=> '舊會員電訪',
'DEPOSITREPORT' 				=> '存點報表',
'DAILYSUMMARY' 					=> '每日總結',
'FLAGMEMREPORT' 				=> '插旗會員報表',
'AUDITREPORT' 					=> '審核報表',
'AGENTREG' 						=> '代理申請',
'AGENTAD' 						=> '代理廣告',
'IPAUDIT' 						=> 'IP審核',
'CMS' 							=> '文章管理',
'RECONCILE_TRANS' 				=> '核對交易',
'KINGACCOUNT_ENQUIRY' 			=> '賭王帳號查詢',
'SURVEY_ANALYSIS' 				=> '問卷分析',
'ADVISE_ANALYSIS' 				=> '建議分析',
'SURVEY_LUCKDRAW' 				=> '問卷抽獎',
'LUCKYDRAWLIST' 				=> '抽獎名單',
'LUCKYDRAWLIST_ENQUIRY' 		=> '抽獎名單查詢',
'EVENTREVIEW2' 					=> '活動分析2',
'ISSUENO' 						=> '問題編號',
'ACTIVATEALL' 					=> '激活全部',
'SUSPENDALL' 					=> '停用全部',
'INFORMATION' 					=> '資料',
'REGFEE' 						=> '報名收費',
'PACKAGE' 						=> '配套',
'CHIP' 							=> '籌碼',
'TIME_MINUTES' 					=> '時間（分鐘）',
'EVENT_TITLE' 					=> '活動',
'EVENT_REPORT' 					=> '活動報表',

//game name
'GAMESPONGEBOB' 				=> '海綿寶寶',
'GAMEMARIO' 					=> '小瑪莉',
'GAMESANGUO' 					=> '三國無雙',
'GAMEMONSTER' 					=> '怪獸拉霸',
'GAMEEGYPT' 					=> '埃及拉霸',
'GAMEWOMEN' 					=> '女人我最大',
'GAMEAFRICA' 					=> '非洲樂園',
'GAMEARMY' 						=> '軍火之王',
'GAMEHIGHWAY' 					=> '快車大盜',
'GAMESNOW' 						=> '冰雪奇蹟',
'GAMEEBACCARAT' 				=> '電子百家樂',

'SEDIE' 						=> '色碟',
'SEDIE4RED' 					=> '4紅',
'SEDIE4WHITE' 					=> '4白',
'SEDIE1RED3WHITE' 				=> '1紅3白',
'SEDIE2RED2WHITE' 				=> '2紅2白',
'SEDIE3RED1WHITE' 				=> '3紅1白',
'AFFFIRSTDEPOSIT' 				=> '代理首存',
'TOTALPLACE' 					=> '名額',
'INVALID' 						=> '無效',
'NOTVERIFIED' 					=> '未確認',
'OPERATEREMARK' 				=> '操作記錄',
'REMOVESTATUS' 					=> '移除',
'REQUESTFEEDBACK' 				=> '要求回复',
'REPLIED' 						=> '已回复',
'CLOSED' 						=> '已關閉',
'REG_AMOUNT' 					=> '註冊人數',
'BETSIZE' 						=> '遊戲量',
'REGISTERED' 					=> '註冊會員',
'EFFECTIVEMEM' 					=> '有效會員',
'TOTALBETS' 					=> '總遊戲',
'SPONSORPRIZE' 					=> '推薦獎金',
'LASTCUMULATIVE' 				=> '上月累計',
'FIRSTDEPOSIT' 					=> '首存',
'CONTDEPOSIT' 					=> '續存',

'RISKPROFILE' 					=> '風控',
'HIGHEST' 						=> '最高',
'HIGH' 							=> '高',
'MEDIUM' 						=> '中',
'LOW' 							=> '低',
'LOWEST' 						=> '最低',

'SUCCESS' 						=> '成功',
'FAILED' 						=> '失敗',
'ERROR' 						=> '錯誤',
'BETMEMBER' 					=> '遊戲會員',
'FIRSTDEPMEMBER' 				=> '首存會員',
'CONTDEPMEMBER' 				=> '續存會員',
'PAYOUTCOMMISSION' 				=> '派出佣金',
'DISTRIBUTE' 					=> '派發',
'AGREE' 						=> '同意',
'OPINION' 						=> '意見',
'ACCUMULATE_WINLOST' 			=> '累積輸贏',
'MONTHLYWINLOST' 				=> '本月輸贏',
'LASTWINLOST' 					=> '上月輸贏',
'MONTHLYFEE' 					=> '本月費用',
'LASTFEE' 						=> '上月費用',
'ACCUMULATE_FEE' 				=> '累積費用',
'CLICKS' 						=> '點擊數',

'WHEEL' 						=> '輪盤抽獎',
'WHEELPRIZE' 					=> '輪盤獎品',
'WHEELTOKEN' 					=> '輪盤名單',
'WHEELREPORT' 					=> '輪盤報表',
'PRIZE' 						=> '獎品',


'MAINTENANCEMSG' 				=> 'Maintenance Message',
'TEMPLATE' 						=> 'Template',
'PREFIX' 						=> 'Prefix',
'WITHDRAWALLIMIT' 				=> 'Withdrawal Limit',
'LASTDEPOSIT' 					=> 'Last Deposit',
'MEMBERENQUIRY' 				=> '會員查詢',
'MAXIMUM' 						=> 'Maximum',
'MINIMUM' 						=> 'Minimum',
'MAXPAYOUT' 					=> 'Max Payout',
'MINPAYOUT' 					=> 'Min Payout',
'CODENOTMATCH' 					=> 'Verification code not match',
'TRANSFERRED' 					=> 'Transferred',
'DISPLAY' 						=> 'Display',
'PERCENTAGEAMOUNT' 				=> 'Percentage / Amount',
'OPENBETS' 						=> 'Open Bets',
'PREREQUISITE' 					=> 'Prerequisite',
'SUBTOTAL' 						=> 'Sub Total',
'REGISTRATIONSHORT' 			=> 'Reg.',
'APPLIED' 						=> 'Applied',
'APPROVED' 						=> 'Approved',
'GROSSBALANCE' 					=> 'Gross Balance',
'BANKSUMMARY' 					=> 'Bank Summary',
'TRANSFER' 						=> 'Transfer',
'BANKTRANSACTION' 				=> 'Bank Transaction',
'MEMBERREPORT' 					=> 'Member Report',
'INSUFFICIENTBALANCE' 			=> 'Insufficient balance',
'RATE' 							=> 'Rate',
'INCENTIVEAPPROVED' 			=> 'Rebate Approved',
'EFFECTIVE' 					=> 'Effective',
'CONFIRMATION' 					=> 'Confirmation',
'MARGINPERCENTAGE' 				=> 'Margin %',
'SYSTEMBALANCE' 				=> 'System Balance',
'WALLET' 						=> 'Wallet',
'WALLETBALANCE' 				=> 'Wallet Balance',
'ARTICLE' 						=> 'Article',
'GENERAL' 						=> 'General',
'EXCERPT' 						=> 'Excerpt',
'IMAGE' 						=> 'Image',
'THUMBNAIL' 					=> 'Thumbnail',
'LOGO' 							=> 'Logo',
'BETDETAILS' 					=> 'Bet Details',
'REFNOSHORT' 					=> 'Ref. No',
'TIMELAPSED' 					=> 'Time Lapsed',
'PRODUCTMAINTENANCE' 			=> 'Product Maintenance',
'NETPROFITLOSS' 				=> 'Net PNL',
'AFFILIATEREPORT2' 				=> 'Affiliate Report (2)',
'WINLOSE' 						=> 'Win/Loss',
'UPLINE' 						=> 'Upline'
]
?>