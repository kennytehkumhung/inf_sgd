﻿<!-- head section start-->
@include('affiliate/include/head')
<!-- head section end-->

<div id="wrapper">
	<!-- Navigation start-->
	@include('affiliate/include/navigation')
	<!-- Navigation end-->
	<div id="page-wrapper">
		<div class="row">
		
		</div>
		<div class="row">
			<div class="col-md-12">					
				@yield('content')
			</div>
			
		</div>
	</div>
</div>
	<!--footer section start-->
	@include('affiliate/include/footer')
	<!--footer section end-->
	



    


