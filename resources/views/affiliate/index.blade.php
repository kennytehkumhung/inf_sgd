﻿@extends('affiliate/master')

@section('title', 'Affiliate Dashboard')

@section('top_js')
    <link rel="stylesheet" href="affiliate/jqwidgets/styles/jqx.base.css" type="text/css" />
	<link rel="stylesheet" href="{{ URL('/') }}/admin/jqwidgets/styles/jqx.metro.css" type="text/css" />   
    <link rel="stylesheet" href="affiliate/jqwidgets/styles/jqx.bootstrap.css" type="text/css" />
    
    <script type="text/javascript" src="affiliate/jqwidgets/jqx-all.js"></script>

    <link rel="stylesheet" href="{{ URL('/') }}/admin/jqwidgets/styles/jqx.base.css" type="text/css" />
    <link rel="stylesheet" href="{{ URL('/') }}/admin/jqwidgets/styles/jqx.metro.css" type="text/css" />   
    <script type="text/javascript" src="{{ URL('/') }}/admin/jqwidgets/jqx-all.js"></script>	

    <script type="text/javascript" src="{{ URL('/') }}/admin/jqwidgets/jqxcore.js"></script>
    <script type="text/javascript" src="{{ URL('/') }}/admin/jqwidgets/jqxdraw.js"></script>
    <script type="text/javascript" src="{{ URL('/') }}/admin/jqwidgets/jqxchart.core.js"></script>
    <script type="text/javascript" src="{{ URL('/') }}/admin/jqwidgets/jqxdata.js"></script>
	<script type="text/javascript" src="{{ URL('/') }}/admin/jqwidgets/jqxtabs.js"></script>
	
<script type="text/javascript">
	$(document).ready(function () {
						var h = $(window).height() - 53;
		// create jqxTabs.
		$('#mastertab').jqxTabs({ height: h, width: '100%',  autoHeight: false, showCloseButtons: true ,  theme:'ui-start' });
		
		$('#mastertab').on('removed', function (event) { 

			tabs[event.args.title] = true;
		}); 
		//$("#jqxNavigationBar").jqxNavigationBar({ width: 200, height: 600, theme:'fresh', expandMode: 'toggle'});
		
	});
	var tabs = new Array(); 
	var tabs_record = new Array(); 
	function addTab(title,url){
		
		for(count = 0; count < 20; count++){
			text = $('#mastertab').jqxTabs('getTitleAt', count); 
			if(text == title)
			{
				$('#mastertab').jqxTabs('removeAt', count); 
			}
        }
		
		if ( tabs[title] == undefined ||  tabs[title] == true ) {
			tabs[title] = false;
			
			 $('#mastertab').jqxTabs({ selectedItem: 1 }); 
			 $('#mastertab').jqxTabs('addLast', title, ' <iframe frameBorder="0" src="'+url+'" style="overflow:hidden;overflow-x:hidden;overflow-y:hidden;height: 90%; width:100%;"></iframe> ');

		}
		
		
        
	
	}	
	function closeTab(){

		$('#mastertab').jqxTabs('removeAt', $('#mastertab').jqxTabs('selectedItem'));


	}
	
	 
</script>
@stop

@section('content')
	
<div id='mastertab' style="float: left; width:100%; height:100%;">
				<ul style="margin: 30px;" id="unorderedList">
					<li>Dashboard</li>
					
				</ul>
	
				<div>
				</div>
			</div>

	
@stop

@section('bottom_js')
   	
@stop