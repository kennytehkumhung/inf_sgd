<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom:0">
	<div class="navbar-header">
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</button>
		{{Lang::get('COMMON.AFFILIATE')}}
	</div>
	<ul class="nav navbar-top-links navbar-right">
	@if( Config::get('setting.front_path') == 'front' )
		<input id="text" type="hidden" value="//{{Session::get('affiliate_username')}}.infiniwin.net"/>
	@else
		<input id="text" type="hidden" value="//{{Config::get('setting.frontend')}}?utm_source=affiliate&utm_medium={{Session::get('affiliate_code')}}&refcode={{Session::get('affiliate_code')}}"/>
	@endif
	<div id="qrcode" style="display:none;width:100px; height:100px; margin-top:15px;"></div>
		<li>
			<a href="javascript:void(0);" id="myImgBTN" onclick="qrPOPUP()">{{Lang::get('COMMON.QRCODE')}}</a>
		</li>
		<li>
		@if( Config::get('setting.front_path') == 'front' )
			<a target="_blank" href="//{{Session::get('affiliate_username')}}.infiniwin.net">{{Lang::get('COMMON.AFFILIATELINK')}}</a>
		@elseif(  Config::get('setting.opcode') == 'GSC')
			<a href="javascript:void(0);" onclick='copyText()'>{{Lang::get('COMMON.AFFILIATELINK')}}</a>
		@else
			<a target="_blank" href="//{{Config::get('setting.frontend')}}?utm_source=affiliate&utm_medium={{Session::get('affiliate_code')}}&refcode={{Session::get('affiliate_code')}}">{{Lang::get('COMMON.AFFILIATELINK')}}</a>
		@endif
		</li>	
		<li>
		
			<a href="{{route( 'languages', [ 'lang'=> 'en'])}}" >
                  <img src="{{url()}}/front/img/en.png">
            </a>
		</li>
		<li>
			<a href="{{route( 'languages', [ 'lang'=> 'cn'])}}"><img src="{{url()}}/front/img/cn.png"></a>
		</li>
		<li class="dropdown">
			<a class="dropdown-toggle" data-toggle="dropdown" href="#">
				<i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
			</a>
			<ul class="dropdown-menu dropdown-user">
				<li><a href="#" onClick="addTab('{{Lang::get('COMMON.CHANGEPASS')}}','password')"><i class="fa fa-user fa-fw"></i> {{Lang::get('COMMON.CHANGEPASS')}}</a></li>
				<li class="divider"></li>
				<li><a href="{{route('affiliate_logout')}}"><i class="fa fa-sign-out fa-fw"></i> {{Lang::get('COMMON.LOGOUT')}}</a></li>
			</ul>
		</li>
	</ul>
	<div class="navbar-default sidebar" role="navigation">
		<div class="sidebar-nav navbar-collapse">
			<ul class="nav" id="side-menu">	
				<li>
					<a href="#"> {{Lang::get('COMMON.MEMBER')}}<span class="fa arrow"></span></a>
					<ul class="nav nav-second-level">
						<li>
							<a href="#" onClick="addTab('{{Lang::get('COMMON.MEMBERENQUIRY')}}','member')">{{Lang::get('COMMON.MEMBERENQUIRY')}}</a>
						</li>
					</ul>
				</li>
				<li>
					<a href="#"> {{Lang::get('COMMON.REPORT')}}<span class="fa arrow"></span></a>
					<ul class="nav nav-second-level">
						
						<li>
							<a href="#" onClick="addTab('{{Lang::get('COMMON.REPORTTOTAL')}}','profitloss?aflid={{Session::get('affiliate_userid')}}')">{{Lang::get('COMMON.REPORTTOTAL')}}</a>
						</li>

						@if (Config::get('setting.opcode') == 'GSC')
						<li>
							<a href="#" onClick="addTab('{{Lang::get('COMMON.REPORTTOTALNEW')}}','profitloss2?aflid={{Session::get('affiliate_userid')}}')"> {{Lang::get('COMMON.REPORTTOTALNEW')}}</a>
						</li>
						@endif

						<li>
							<a href="#" onClick="addTab('{{Lang::get('COMMON.AFFILIATEREPORT')}}','affiliateReport')">{{Lang::get('COMMON.AFFILIATEREPORT')}}</a>
						</li>
					</ul>
				</li>
				@if (Config::get('setting.opcode') == 'LVG')
				<li>
					<a href="#">{{Lang::get('COMMON.PROFILE')}} <span class="fa arrow"></span></a>
					<ul class="nav nav-second-level">
						<li>
							<a href="#" onClick="addTab('{{Lang::get('COMMON.CHANGEPASS')}}','password')">{{Lang::get('COMMON.CHANGEPASS')}}</a>
						</li>
						<li>
							<a href="#" onClick="addTab('Update Purchase Password','purchase-password')">Update Purchase Password</a>
						</li>
					</ul>
				</li>
				@endif
			</ul>
		</div>
	</div>
</nav>

<script type="text/javascript" src="{{url()}}/affiliate/js/qrcode.js"></script>
<script>
function copyText(){
	var x=prompt("{{Lang::get('COMMON.AFFILIATELINK')}}", "//{{Config::get('setting.frontend')}}?utm_source=affiliate&utm_medium={{Session::get('affiliate_code')}}&refcode={{Session::get('affiliate_code')}}");
}
function qrPOPUP(){
	var myImgSrc = document.getElementById("myImg").getAttribute('src');
	var myWindow=window.open("", "", "width=400,height=350");
	myWindow.document.title = 'testing';
	myWindow.document.write("<script type=\"text/javascript\">function prepHref(linkElement) {var myDiv = document.getElementById('Div_contain_image');var myImage = myDiv.children[0];linkElement.href = myImage.src;}<\/script>'");
	myWindow.document.write("<title>QRcode</title><div id=\"Div_contain_image\"><img src="+myImgSrc+" style='margin-left: 60px;'></div><h3 align='center'>({{Lang::get('COMMON.NOTRECOMMENDUSEWECHATTOSCAN')}})</h3><a href=\"#\" onclick=\"prepHref(this)\" download><h3 style='margin-left: 130px;'>下载二维码</h3></a>");
}
var qrcode = new QRCode(document.getElementById("qrcode"), {
	width : 256,
	height : 256
});

function makeCode () {		
	var elText = document.getElementById("text");
	
	if (!elText.value) {
		alert("Input a text");
		elText.focus();
		return;
	}
	
	qrcode.makeCode(elText.value);
}

makeCode();

$("#text").
	on("blur", function () {
		makeCode();
	}).
	on("keydown", function (e) {
		if (e.keyCode == 13) {
			makeCode();
		}
	});
</script>