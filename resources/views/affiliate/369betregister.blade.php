﻿﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Affiliate - Register - {{Config::get('setting.website_name')}}</title>
<style type="text/css">
body {
	background: url(affiliate/images/369bet_img/bg.jpg) top center  #f1f1f1 !important;
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
.style2 {
	font-family: Tahoma;
	font-size: 20px;
	font-weight: bold;
	color: #FF4E00;
}
.style3 {
	color: #FFFFFF;
	font-size: 13px;
	font-weight:100;
}
.style4 {
	color: #FF6600;
	font-weight: bold;
}
.style9 {
	color: #FFFFFF
}
.style10 {
	font-family: Tahoma;
	font-size: 16px;
	font-weight: bold;
	color: #333333;
}
.style11 {
	font-family: Tahoma;
	font-size: 12px;
}
</style>
<script type="text/javascript">
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}
</script>
<link rel="stylesheet" type="text/css" href="{{url()}}/affiliate/css/369bet/style.css"/>
<script type="text/javascript" src="{{url()}}/affiliate/js/369bet/jssor.slider.min.js"></script>
<!-- use jssor.slider.debug.js instead for debug -->
<script>
        jssor_1_slider_init = function() {
            
            var jssor_1_SlideoTransitions = [
              [{b:5500,d:3000,o:-1,r:240,e:{r:2}}],
              [{b:-1,d:1,o:-1,c:{x:51.0,t:-51.0}},{b:0,d:1000,o:1,c:{x:-51.0,t:51.0},e:{o:7,c:{x:7,t:7}}}],
              [{b:-1,d:1,o:-1,sX:9,sY:9},{b:1000,d:1000,o:1,sX:-9,sY:-9,e:{sX:2,sY:2}}],
              [{b:-1,d:1,o:-1,r:-180,sX:9,sY:9},{b:2000,d:1000,o:1,r:180,sX:-9,sY:-9,e:{r:2,sX:2,sY:2}}],
              [{b:-1,d:1,o:-1},{b:3000,d:2000,y:180,o:1,e:{y:16}}],
              [{b:-1,d:1,o:-1,r:-150},{b:7500,d:1600,o:1,r:150,e:{r:3}}],
              [{b:10000,d:2000,x:-379,e:{x:7}}],
              [{b:10000,d:2000,x:-379,e:{x:7}}],
              [{b:-1,d:1,o:-1,r:288,sX:9,sY:9},{b:9100,d:900,x:-1400,y:-660,o:1,r:-288,sX:-9,sY:-9,e:{r:6}},{b:10000,d:1600,x:-200,o:-1,e:{x:16}}]
            ];
            
            var jssor_1_options = {
              $AutoPlay: true,
              $SlideDuration: 800,
              $SlideEasing: $Jease$.$OutQuint,
              $CaptionSliderOptions: {
                $Class: $JssorCaptionSlideo$,
                $Transitions: jssor_1_SlideoTransitions
              },
              $ArrowNavigatorOptions: {
                $Class: $JssorArrowNavigator$
              },
              $BulletNavigatorOptions: {
                $Class: $JssorBulletNavigator$
              }
            };
            
            var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);
            
            //responsive code begin
            //you can remove responsive code if you don't want the slider scales while window resizing
            function ScaleSlider() {
                var refSize = jssor_1_slider.$Elmt.parentNode.clientWidth;
                if (refSize) {
                    refSize = Math.min(refSize, 1920);
                    jssor_1_slider.$ScaleWidth(refSize);
                }
                else {
                    window.setTimeout(ScaleSlider, 30);
                }
            }
            ScaleSlider();
            $Jssor$.$AddEvent(window, "load", ScaleSlider);
            $Jssor$.$AddEvent(window, "resize", ScaleSlider);
            $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
            //responsive code end
        };
</script>
<style>
/* jssor slider bullet navigator skin 05 css */
        /*
        .jssorb05 div           (normal)
        .jssorb05 div:hover     (normal mouseover)
        .jssorb05 .av           (active)
        .jssorb05 .av:hover     (active mouseover)
        .jssorb05 .dn           (mousedown)
        */
        .jssorb05 {
	position: absolute;
}
.jssorb05 div, .jssorb05 div:hover, .jssorb05 .av {
	position: absolute;
	/* size of bullet elment */
            width: 16px;
	height: 16px;
	background: url('affiliate/images/369bet_img/b05.png') no-repeat;
	overflow: hidden;
	cursor: pointer;
}
.jssorb05 div {
	background-position: -7px -7px;
}
.jssorb05 div:hover, .jssorb05 .av:hover {
	background-position: -37px -7px;
}
.jssorb05 .av {
	background-position: -67px -7px;
}
.jssorb05 .dn, .jssorb05 .dn:hover {
	background-position: -97px -7px;
}
/* jssor slider arrow navigator skin 22 css */
        /*
        .jssora22l                  (normal)
        .jssora22r                  (normal)
        .jssora22l:hover            (normal mouseover)
        .jssora22r:hover            (normal mouseover)
        .jssora22l.jssora22ldn      (mousedown)
        .jssora22r.jssora22rdn      (mousedown)
        */
        .jssora22l, .jssora22r {
	display: block;
	position: absolute;
	/* size of arrow element */
            width: 40px;
	height: 58px;
	cursor: pointer;
	background: url('affiliate/images/369bet_img/a22.png') center center no-repeat;
	overflow: hidden;
}
.jssora22l {
	background-position: -10px -31px;
}
.jssora22r {
	background-position: -70px -31px;
}
.jssora22l:hover {
	background-position: -130px -31px;
}
.jssora22r:hover {
	background-position: -190px -31px;
}
.jssora22l.jssora22ldn {
	background-position: -250px -31px;
}
.jssora22r.jssora22rdn {
	background-position: -310px -31px;
}
</style>
<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
<link rel="stylesheet" href="{{url()}}/affiliate/css/369bet/jquery.maximage.css" type="text/css" media="screen" title="CSS" charset="utf-8" />
<script src="{{url()}}/affiliate/js/369bet/jquery.cycle.all.js" type="text/javascript"></script>
<script src="{{url()}}/affiliate/js/369bet/jquery.easing.1.3.js" type="text/javascript"></script>
<script src="{{url()}}/affiliate/js/369bet/jquery.maximage.min.js" type="text/javascript"></script>
<script type="text/javascript" charset="utf-8">
      $(function(){
        // Trigger maximage
        jQuery('#maximage').maximage();
      });
      
      function register(){
                        //var dob = $('#dob_year').val()+'-'+$('#dob_month').val()+'-'+$('#dob_day').val();
			$.ajax({
				type: "POST",
				url: "{{route('register_process')}}",
				data: {
                                        _token: "{{ csrf_token() }}",
                                        surname:		 $('#surname').val(),
                                        username:		 $('#username').val(),
                                        password: 		 $('#password').val(),
                                        confirm:                 $('#confirm').val(),
                                        email:    		 $('#email').val(),
                                        telephone:    		 $('#telephone').val(),
                                        website:                 $('#website').val(),
                                        //website2:                $('#website2').val(),
                                        //website3:                $('#website3').val(),
                                        websitedesc:             $('#websitedesc').val(),
                                        //dob:			 dob,
                                        //agent:                   $('#agent').val(),
                                        //code:                    $('#code').val(),
                                },
			}).done(function( json ) {
					 obj = JSON.parse(json);
					 var str = '';
					 $.each(obj, function(i, item) {
						str += item + '\n';
					})
					if( "Successful\n" == str ){
						alert('ยินดีด้วยครับ คุณสมัครสมาชิกสำเร็จแล้ว');
                                                window.location.href = "http://affiliate.369thai.com/ ";
					}else{
						alert(str);
					}
					
			});
		

	}
</script>
<style type="text/css">
<!--
.style12 {
	font-family: Tahoma;
	font-size: 14px;
	color: #666666;
}
.style13 {
	font-family: Tahoma;
	font-size: 16px;
	color: #333333;
}
.style14 {
	font-weight: bold
}
.style15 {
	font-size: 14px
}
.style16 {
	font-size: 14
}
.style17 {
	font-family: Tahoma;
	font-size: 14px;
	color: #333333;
}
.style18 {font-family: Tahoma; font-size: 18px; }
.style19 {color: #333333}
.style21 {font-size: 13px}
.style22 {color: #000000}
.style24 {
	font-size: 14px;
	font-family: Tahoma;
	font-weight: bold;
}
.style25 {
	font-family: Tahoma;
	font-size: 14px;
}
.style26 {font-family: Tahoma}
.style28 {color: #000000; font-weight: bold; }
.style30 {font-size: 14px; font-weight: bold; color: #FFFFFF; }
-->
</style>
</head>

<body>
@include('affiliate/include/369betheader')

<div style="height:350px; width:1050px; margin:auto;  "> 
  <div align="center"><img src="{{url()}}/affiliate/images/369bet_img/aa.png" /></div>
</div>
<div style="width:1050px;  margin:auto; overflow:hidden; ">
  <div style="width:1036px; margin:auto; padding-top:15px;"></div>
    <div align="center" style="width:1036px; margin:auto; padding:7px 0 0px 0;">
        <form id="cboForm">
            <table width="100%" border="0">
              <tr>
                <td width="35%">&nbsp;</td>
                <td width="65%">&nbsp;</td>
              </tr>
              <tr>
                <td height="42"><div align="right" style="font-family:Tahoma; font-size:12px; color:#333333;">ชื่อ - นามสกุล   :</div></td>
                <td><font color="#000000">
                  <input type="text" id="surname" class="easyui-validatebox" value="" style="height:27px; width:300px; border:solid 1px #b9b9b9; font-size:12px;" />
                </font></td>
              </tr>
              <tr>
                <td height="47">&nbsp;</td>
                <td valign="top"><div align="right" style="font-family:Tahoma; font-size:12px; color:#333333;">
                  <div align="left">ในการถอนเงินทั้งหมดจะทำการถอนด้วยชื่อเดียวกัน โดยต้องเป็นตัวอักษรเท่านั้น <br /> และจะไม่สามารถทำการแก้ไขได้หลังจากทำการลงทะเบียนแล้ว  </div>
                </div></td>
              </tr>
<!--              <tr>
                <td height="36"><div align="right" style="font-family:Tahoma; font-size:12px; color:#333333;">วันเกิด   :</div></td>
                <td>
                    
                    <select id="dob_day"  style="height:27px; width:50px; border:solid 1px #b9b9b9; font-size:12px;">
                                                <option value="01">01</option>
						<option value="02">02</option>
						<option value="03">03</option>
						<option value="04">04</option>
						<option value="05">05</option>
						<option value="06">06</option>
						<option value="07">07</option>
						<option value="08">08</option>
						<option value="09">09</option>
						<option value="10">10</option>
						<option value="11">11</option>
						<option value="12">12</option>
						<option value="13">13</option>
						<option value="14">14</option>
						<option value="15">15</option>
						<option value="16">16</option>
						<option value="17">17</option>
						<option value="18">18</option>
						<option value="19">19</option>
						<option value="20">20</option>
						<option value="21">21</option>
						<option value="22">22</option>
						<option value="23">23</option>
						<option value="24">24</option>
						<option value="25">25</option>
						<option value="26">26</option>
						<option value="27">27</option>
						<option value="28">28</option>
						<option value="29">29</option>
						<option value="30">30</option>
						<option value="31">31</option>
                </select>
                  <select id="dob_month"  style="height:27px; width:100px; border:solid 1px #b9b9b9; font-size:12px;">
                                            <option value="01">January</option>
                                            <option value="02">February</option>
                                            <option value="03">March</option>
                                            <option value="04">April</option>
                                            <option value="05">May</option>
                                            <option value="06">June</option>
                                            <option value="07">July</option>
                                            <option value="08">August</option>
                                            <option value="09">September</option>
                                            <option value="10">October</option>
                                            <option value="11">November</option>
                                            <option value="12">December</option>
                            </select>
                  <select id="dob_year"  style="height:27px; width:70px; border:solid 1px #b9b9b9; font-size:12px;">
                                <option value="2006">2006</option>
                                <option value="2005">2005</option>
                                <option value="2004">2004</option>
                                <option value="2003">2003</option>
                                <option value="2002">2002</option>
                                <option value="2001">2001</option>
                                <option value="2000">2000</option>
                                <option value="1999">1999</option>
                                <option value="1998">1998</option>
                                <option value="1997">1997</option>
                                <option value="1996">1996</option>
                                <option value="1995">1995</option>
                                <option value="1994">1994</option>
                                <option value="1993">1993</option>
                                <option value="1992">1992</option>
                                <option value="1991">1991</option>
                                <option value="1990">1990</option>
                                <option value="1989">1989</option>
                                <option value="1988">1988</option>
                                <option value="1987">1987</option>
                                <option value="1986">1986</option>
                                <option value="1985">1985</option>
                                <option value="1984">1984</option>
                                <option value="1983">1983</option>
                                <option value="1982">1982</option>
                                <option value="1981">1981</option>
                                <option value="1980">1980</option>
                                <option value="1979">1979</option>
                                <option value="1978">1978</option>
                                <option value="1977">1977</option>
                                <option value="1976">1976</option>
                                <option value="1975">1975</option>
                                <option value="1974">1974</option>
                                <option value="1973">1973</option>
                                <option value="1972">1972</option>
                                <option value="1971">1971</option>
                                <option value="1970">1970</option>
                                <option value="1969">1969</option>
                                <option value="1968">1968</option>
                                <option value="1967">1967</option>
                                <option value="1966">1966</option>
                                <option value="1965">1965</option>
                                <option value="1964">1964</option>
                                <option value="1963">1963</option>
                                <option value="1962">1962</option>
                                <option value="1961">1961</option>
                                <option value="1960">1960</option>	
                            </select>
                   
                </td>
              </tr>-->
              <tr>
                <td height="42"><div align="right" style="font-family:Tahoma; font-size:12px; color:#333333;">ชื่อผู้ใช้งาน   :</div></td>
                <td><font color="#000000">
                  <input type="text" id="username" class="easyui-validatebox" value="" style="height:27px; width:300px; border:solid 1px #b9b9b9; font-size:12px;" />
                </font></td>
              </tr>
              <tr>
                <td height="32">&nbsp;</td>
                <td valign="top"><div align="right" style="font-family:Tahoma; font-size:12px; color:#333333;">
                  <div align="left">จะต้องอยู่ในระหว่าง 6 - 15 หลักที่ผสมระหว่าง ตัวเลขและตัวอักษร (A-Z,a-z,0-9)  </div>
                </div></td>
              </tr>
              <tr>
                <td height="42"><div align="right" style="font-family:Tahoma; font-size:12px; color:#333333;">รหัสผ่าน   :</div></td>
                <td><font color="#000000">
                  <input type="password" id="password" class="easyui-validatebox" value="" style="height:27px; width:300px; border:solid 1px #b9b9b9; font-size:12px;" />
                </font></td>
              </tr>
              <tr>
                <td height="27">&nbsp;</td>
                <td valign="top"><div align="right" style="font-family:Tahoma; font-size:12px; color:#333333;">
                  <div align="left">จะต้องอยู่ในระหว่าง 8 - 10 ตัวอักษร  </div>
                </div></td>
              </tr>
              <tr>
                <td height="42"><div align="right" style="font-family:Tahoma; font-size:12px; color:#333333;">ยืนยันรหัสผ่าน   :</div></td>
                <td><font color="#000000">
                  <input type="password" id="confirm" class="easyui-validatebox" value="" style="height:27px; width:300px; border:solid 1px #b9b9b9; font-size:12px;" />
                </font></td>
              </tr>
              <tr>
                <td height="42"><div align="right" style="font-family:Tahoma; font-size:12px; color:#333333;">อีเมล์   :</div></td>
                <td><font color="#000000">
                  <input type="text" id="email" class="easyui-validatebox" value="" style="height:27px; width:300px; border:solid 1px #b9b9b9; font-size:12px;" />
                </font></td>
              </tr>
              <tr>
                <td height="42"><div align="right" style="font-family:Tahoma; font-size:12px; color:#333333;">เบอร์โทรศัพท์   :</div></td>
                <td><font color="#000000">
                  <input type="text" id="telephone" class="easyui-validatebox" value="" style="height:27px; width:300px; border:solid 1px #b9b9b9; font-size:12px;" />
                </font></td>
              </tr>
<!--              <tr>
                <td height="42"><div align="right" style="font-family:Tahoma; font-size:12px; color:#333333;">ประเภทพันธมิตร   :</div></td>
                <td><select id="agent" style="height:27px; width:300px; border:solid 1px #b9b9b9; font-size:12px;">
                        <option>กรุณาเลือกประเภทพันธมิตร</option>
                        <option>มาสเตอร์</option>
                        <option>เอเย่นต์</option>
                        <option>รับเงินเดือน+ค่าคอม</option>
                   </select></td>
              </tr>-->
              <tr>
                <td height="42"><div align="right" style="font-family:Tahoma; font-size:12px; color:#333333;">เว็บไซด์ URL   :</div></td>
                <td><font color="#000000">
                  <input type="text" id="website" class="easyui-validatebox" value="" style="height:27px; width:300px; border:solid 1px #b9b9b9; font-size:12px;" />
                </font></td>
              </tr>
<!--              <tr>
                <td height="42"><div align="right" style="font-family:Tahoma; font-size:12px; color:#333333;">(2)  :</div></td>
                <td><font color="#000000">
                  <input type="text" id="website2" class="easyui-validatebox" value="" style="height:27px; width:300px; border:solid 1px #b9b9b9; font-size:12px;" />
                </font></td>
              </tr>
              <tr>
                <td height="42"><div align="right" style="font-family:Tahoma; font-size:12px; color:#333333;">(3)  :</div></td>
                <td><font color="#000000">
                  <input type="text" id="website3" class="easyui-validatebox" value="" style="height:27px; width:300px; border:solid 1px #b9b9b9; font-size:12px;" />
                </font></td>
              </tr>-->
              <tr>
                <td height="29">&nbsp;</td>
                <td valign="top"><div align="right" style="font-family:Tahoma; font-size:12px; color:#333333;">
                  <div align="left">หากคุณไม่มีเว็บไซต์ โปรดอธิบายการทำตลาดให้กับเรา</div>
                </div></td>
              </tr>
              <tr>
                <td height="42">&nbsp;</td>
                <td><textarea id="websitedesc" cols="45" rows="5" style="height:100px; width:300px; border:solid 1px #b9b9b9; font-size:12px;"></textarea></td>
              </tr>
<!--              <tr>
                <td height="42"><div align="right" style="font-family:Tahoma; font-size:12px; color:#333333;">รหัสตรวจสอบ  :</div></td>
                <td><font color="#000000">
                  <input type="text" id="code" style="height:27px; width:100px; border:solid 1px #b9b9b9; font-size:12px;" />
                </font></td>
              </tr>-->
              <tr>
                <td height="56"><div align="right" style="font-family:Tahoma; font-size:12px; color:#333333;">
                  <input type="checkbox" checked/>
                &nbsp;&nbsp; </div></td>
                <td><div align="right" style="font-family:Tahoma; font-size:12px; color:#333333;">
                  <div align="left">ข้าพเจ้ามีอายุเกิน 18 ปีและได้ทำการอ่าน ยอมรับและสัญญาว่าจะทำตามเงื่อนไขและข้อกำหนด, กฏ, <br />
                    นโยบายความเป็นส่วนตัว, นโยบาย คุ๊กกี้และนโยบายที่เกี่ยวกับการตรวจสอบอายุ </div>
                </div></td>
              </tr>
              <tr>
                <td height="42">&nbsp;</td>
                <td>
                  <input onClick="register()" type="button" value="ร่วมกับเรา" style="background:#006cbf; width:110px; height:30px; color:#fff;  border:solid 1px #b0b0b0;"/>
                </td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
            </table>
        </form>
    </div>

<div style="width:1036px; margin:auto; padding-top:15px;"></div>   


</div><!--end main-->

</body>
</html>