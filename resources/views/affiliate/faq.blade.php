﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "//www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="//www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<title>Affiliate - Home - {{Config::get('setting.website_name')}}</title>
	<link href="{{url()}}/affiliate/css/login.css" type="text/css" media="all" rel="stylesheet" />
	<!--<script type="text/javascript" language="javascript" src="{{url()}}/front/resources/js/jquery.bxslider.min.js"></script>-->
	<script type="text/javascript" src="//gc.kis.scr.kaspersky-labs.com/1B74BD89-2A22-4B93-B451-1C9E1052A0EC/main.js" charset="UTF-8"></script><script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
</head>
	<body>
	<div id="main">
	<style>
	#login_btn
	{
		background:url({{url()}}/affiliate/images/en/login_btn.jpg) no-repeat;
	}
	#reg_btn
	{
		background:url({{url()}}/affiliate/images/en/join_now_btn.jpg) no-repeat;
	}		
	</style>
	
		<div id="header">
			<div class="center_content">
				<div id="logo"><img src="{{url()}}/{{Config::get('setting.affiliate_logo')}}" /></div>
				<div id="form_portion">
					<form method="post" action="login_process" accept-charset="UTF-8" autocomplete="off">
						<a href="register">
							<div id="reg_btn"></div>
						</a>
						<input type="text" name="username" class="username" placeholder="Username" value=""/>						
						<input type="password" name="password" class="pass" placeholder="Password"/>
						
						<div id="code">
							<img src="captcha" width="50" height="22"/>
						</div>
						
						<input type="text" class="code_fill" name="code" placeholder="Code"/>
						<a href="#">
							<input id="login_btn" type="submit" value=""style="cursor:pointer;">
						</a>
						<input type="hidden" value="{{csrf_token()}}" name="_token">
					</form>
				</div>
			</div>
		</div>
		
		<div id="navbar">
			<div class="center_content">
				<ul>
					<li id="nav_home"><a href="{{route('affiliate_login')}}">Home</a></li>
					<div class="nav_divider"></div>
					<li id="nav_product"><a href="#">Our Products</a></li>
					<div class="nav_divider"></div>
					<li id="nav_cp"><a href="{{route('affiliate_commision')}}">Commission Plan</a></li>
					<div class="nav_divider"></div>
					<li id="nav_faq"><a href="{{route('affiliate_faq')}}">FAQ</a></li>
					<div class="nav_divider"></div>
					<li id="nav_contact"><a href="#">CONTACT US</a>
					<div class="contact_dd">
					<table border="0" cellspacing="0" cellpadding="2">
						<tr>
							<td>Email </td>
							<td>:</td>
							<td>affiliate@ {{Config::get('setting.website_name')}}.com</td>
						</tr>
						<tr>
							<td>Skype </td>
							<td>:</td>
							<td>{{Session::get('setting.website_name')}}</td>
						</tr>
						<tr>
							<td>Phone</td>
							<td>:</td>
							<td>6011 2780 7697</td>
						</tr>
						<tr>
							<td>Live Chat</td>
							<td>:</td>
							<td>24x7</td>
						</tr>
					</table>
					<div class="clear"></div>
					</div>
					</li>
				</ul>
			</div>
		</div> 
		
	<div>
	
	
	<div class="center_content" style="background-color:white;padding:30px;">
    
<div>
    <p id="accordion"></p>
    
<h4 class="accordion-toggle">1.Affiliate Programme</h4>

    <p class="accordion-content default">
        <br/>
    </p>
    <p>1.1 What is {{Config::get('setting.website_name')}} Affiliate Program?
        <br/>1.2 How do I sign up with {{Config::get('setting.website_name')}} Affiliate Program?
        <br/>1.3 What if I don't have a website, can I still join?
        <br/>1.4 What if I have more than one website?
        <br/>1.5 Why might I not be accepted as an affiliate?</p>
    <br/>
    <p></p>
    <br/>
     <h4 class="accordion-toggle">1.1 What is {{Config::get('setting.website_name')}} Affiliate Program?</h4>

    <p class="accordion-content default">
        <br/>
    </p>
    <p>Opening your account with www.{{Config::get('setting.website_name')}}.com is simple and will only take a few minutes of your time.</p>
    <p>
        <br/><span style="color: #fff !important;font-weight: bold !important;">STEP 1</span>

        <br/>
    </p>
    <p>On the home page, click on the "Sign up" button in the top right-hand corner of the page. This will take you to the registration page.</p>
    <br/><span style="color: #fff !important;font-weight: bold !important;">STEP 2 </span>

    <br/>
    <p>You will also be asked to read and accept the www.{{Config::get('setting.website_name')}}.com Terms &amp;amp; Conditions and Rules &amp;amp; Regulations, confirm that you are over 18 years of age and enter the required personal and contact information.</p>
    <br/><span style="color: #fff !important;font-weight: bold !important;">STEP 3 </span>

    <br/>
    <p>Once your registration is completed, you may immediately login, deposit and then start to place bets.</p>
    <br/>
    <p></p>
    <br/>
     <h4 class="accordion-toggle">1.2 How do I sign up with {{Config::get('setting.website_name')}} Affiliate Program?</h4>

    <p class="accordion-content default">
        <br/>
    </p>
    <p>The {{Config::get('setting.website_name')}} Affiliate Program is free to join. All you need to do is complete the online registration form and you will be contacted within three (3) business days regarding your acceptance into the program. If your application is accepted, you will receive an email containing your unique affiliate ID, giving access to banners, text links, and other content and marketing tools to add to your website.</p>
    <br/>
    <p></p>
    <br/>
     <h4 class="accordion-toggle">1.3 What if I don't have a website, can I still join?</h4>

    <p class="accordion-content default">
        <br/>
    </p>
    <p>To join the {{Config::get('setting.website_name')}} Affiliate Program, you should have a Website (Live) that has online gambling as its central theme. Should you not have a website, {{Config::get('setting.website_name')}} Affiliate will however review cases on an individual basis. Please email affiliate@{{Config::get('setting.website_name')}}.com for more information.</p>
    <br/>
    <p></p>
    <br/>
     <h4 class="accordion-toggle">1.4 What if I have more than one website?</h4>

    <p class="accordion-content default">
        <br/>
    </p>
    <p>You can have multiple websites, you only need to apply once to {{Config::get('setting.website_name')}} Affiliate and use the same affiliate ID for multiple sites. You can also set up separate tracking codes for each site, so you can compare how well each site performs.</p>
    <br/>
    <p></p>
    <br/>
     <h4 class="accordion-toggle">1.5 Why might I not be accepted as an affiliate?</h4>

    <p class="accordion-content default">
        <br/>
    </p>
    <p>Some customers may experience a display issue where the "Bet Slip" and "Log In Boxes" appear off the edge of your screen. This is most likely to be due to the screen resolution of your PC being set too low (below 1024 by 768). For optimal use of the website, please ensure your screen resolution is set to 1024 x 768 or higher.</p>
    <br/>
    <p></p>
    <br/>
    <p></p>​</div>
	
	</div>
	
		<div id="copyright">
			<div class="center_content">
				<div id="icon">
					<img src="{{url()}}/affiliate/images/en/footer_icon.png" width="525" height="31"/>
				</div>
				<div id="left_copyright">© Copyright<span style="color:#767057"> {{Config::get('setting.website_name')}} Affiliate</span>. All rights reserved.</div>
				<div id="right_tnc">
					<a href="#">Terms and Conditions</a> &nbsp; &nbsp;&nbsp;
					<a href="#">Disclaimer</a> &nbsp;&nbsp;&nbsp;
					<a href="#">Privacy Policy</a> &nbsp;&nbsp;&nbsp;
					<a href="#">FAQ</a>
				</div>
			</div> 
		</div>  
		<iframe id="frameSubmit" name="frameSubmit" src="resources/script/index.html" border="0" marginwidth="0" marginheight="0" hspace="0" vspace="0" frameborder="0" scrolling="auto" style="position:absolute; z-index:0; visibility:hidden"></iframe>
	</body>
</html>
