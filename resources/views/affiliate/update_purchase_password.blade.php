<!DOCTYPE html>
<html lang="en">
<head>
    <title id='Description'>
    </title>
	<head>
	<link rel="stylesheet" href="affiliate/jqwidgets/styles/jqx.base.css" type="text/css" />
    <link rel="stylesheet" href="affiliate/jqwidgets/styles/jqx.bootstrap.css" type="text/css" />
    <script type="text/javascript" src="affiliate/jqwidgets/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="affiliate/jqwidgets/jqx-all.js"></script>
	 <script type="text/javascript">
	 $(document).ready(function () {
		$("#form_submit_btn").jqxButton({ width: '150', template: "success"});
	 });
     
	function submit_form(){

		$.ajax({
			type: "POST",
			url: "purchase-password-edit?"+$('form').serialize(),
			data: {
				_token: 		 "{{ csrf_token() }}",
			},
		}).done(function( json ) {
				 $('.error_msg').html('');
				 obj = JSON.parse(json);
				 var str = '';
				 $.each(obj, function(i, item) {
					if( '{{Lang::get('COMMON.SUCESSFUL')}}' == item ){
						alert('{{Lang::get('COMMON.SUCESSFUL')}}');
					}else{
						$('.'+i+'_error').html(item);
				
					}
				})
				
		});

	}
		
		
		
    </script>
	</head>
	<body class='default'>

<form method="post">
<div id="form_wapper" >
<table>
	
	<tr>
		<td class="formLabelLeft">
			{{Lang::get('COMMON.PASSWORD')}}
		</td>
		<td>
			<input  type="password" name="password" value="">
		</td>
		<td>
			<div class="current_password_error error_msg" style="color:red;"></div>
		</td>
	</tr>	
	
	

</table>

		</div>
		<div class="clear">
            <input style='margin-top: 20px;cursor: pointer;' value="{{Lang::get('COMMON.SUBMIT')}}" id='form_submit_btn' onClick="submit_form()"/>
        </div>
	
</form>

</body>
</html>