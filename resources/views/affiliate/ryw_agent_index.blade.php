<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<title>{{Lang::get('COMMON.PLEASELOGIN')}}</title>

	<!-- Google Fonts -->
	<link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700|Lato:400,100,300,700,900' rel='stylesheet' type='text/css'>

	<link rel="stylesheet" href="{{url('/')}}/affiliate/css/animate.css">
	<!-- Custom Stylesheet -->
	<link rel="stylesheet" href="{{url('/')}}/affiliate/css/style2.css">

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
</head>

<body>
	<div class="container">
		<div class="top">
			<h1 id="title" class="hidden"><span id="logo"><img src="{{url('/')}}/affiliate/images/logo2.png" width="100%"></span></h1>
		</div>
		<div class="login-box animated fadeInUp">
			<div class="box-header">
				<h2>{{Lang::get('COMMON.PLEASELOGIN')}}</h2>
			</div>		
			<form method="post" action="login_process" accept-charset="UTF-8" autocomplete="off">

			<label for="username">{{Lang::get('COMMON.USERNAME')}}</label>
			<br/>
			<input type="text" name="username">
			<br/>
			<label for="password">{{Lang::get('COMMON.PASSWORD')}}</label>
			<br/>
			<input type="password" name="password">
			<br/>
			 <div class="row">
        <div class="col-sm-3">
        <label>{{Lang::get('COMMON.CODE')}}</label>
        </div>
        <div class="col-sm-4"><img src="captcha" width="80" height="30" style="position: relative;bottom: -10px;"/>

        <input type="text" class="code_fill" name="code" style="width: 100px;"/>
        </div>
      
		</div>
        
			<input id="login_btn" type="submit" value="{{Lang::get('COMMON.LOGIN')}}" style="cursor:pointer;">
			</form>
			<br/>
		
		</div>
	</div>
</body>

<script>
	$(document).ready(function () {
    	$('#logo').addClass('animated fadeInDown');
    	$("input:text:visible:first").focus();
	});
	$('#username').focus(function() {
		$('label[for="username"]').addClass('selected');
	});
	$('#username').blur(function() {
		$('label[for="username"]').removeClass('selected');
	});
	$('#password').focus(function() {
		$('label[for="password"]').addClass('selected');
	});
	$('#password').blur(function() {
		$('label[for="password"]').removeClass('selected');
	});
</script>

</html>