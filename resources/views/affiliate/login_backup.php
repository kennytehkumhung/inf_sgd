<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>{{Config::get('setting.website_name')}} - Affiliate</title>

    <!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	<link href="{{url()}}/	/css/sb_bootstrap/admin_style.css" rel="stylesheet">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">

	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</head>
<body>
    <div class="container">
		<div class="row">
			<div class="col-md-12">
				<br>
				<br>
				<br>
				<br>
				<br>
				<br>
			</div>
		</div>
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-primary">
                    <div class="panel-heading">
					<div id="logo">
						<img width="236" height="71" src="images/logo.png">
					</div>
                    </div>
                    <div class="panel-body">
					 <form method="POST" action="login_process" accept-charset="UTF-8" autocomplete="off" class="form-horizontal well">
                        <table width="100%">
							<tr>
								<td>Username</td>
								<td colspan="2"><input type="text" class="form-control" name="username" ></td>
							</tr>
							
							<tr>
								<td>Password</td>
								<td colspan="2"><input type="password" class="form-control" name="password" ></td>
							</tr>
							
							<tr>
								<td>Code</td>
								<td><input type="text" class="form-control" name="code" ></td>
								<td><img width="100%" height="auto" src="captcha" width=""></img></td>
							</tr>
							
							<tr>
								<td>Language</td>
								<td colspan="2">
									<select class="form-control">
										<option>English</option>
										<option>Chinese Simplified</option>									
									</select>
								</td>
							</tr>
							<tr>
								<td></td>
								<td colspan="2" style="height:40px;">
									<h4 style="color:red;">{{ Session::get('message') }}</h4>
								</td>
							</tr>
						</table>
						
					
						<input class="btn btn-sm btn-block btn-primary" type="submit" value="Sign In" style="cursor:pointer;">
						<input type="hidden" value="{{csrf_token()}}" name="_token">
						 </form>
                    </div>
                </div>
            </div>



        </div>
		<div class="row">
			<div class="col-md-12">
				<center>{{Config::get('setting.website_name')}} 2015 Copyright &copy; All rights reserved. </center>
			</div>
		</div>
    </div>
</body>
</html>
