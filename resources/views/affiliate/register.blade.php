﻿
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "//www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="//www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Affiliate - Register - {{Config::get('setting.website_name')}}</title>
<link href="{{url()}}/affiliate/css/login.css" rel="stylesheet" type="text/css" />
<style>
.formLabelLeft{
	width: 200px;
}
</style>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function($) {
			$('#accordion').find('.accordion-toggle').click(function(){
	
				//Expand or collapse this panel
				$(this).next().slideToggle('fast');
	
				//Hide the other panels
				$(".accordion-content").not($(this).next()).slideUp('fast');
	
			});
		});
	
	function register(){
		

			$.ajax({
				type: "POST",
				url: "{{route('register_process')}}",
				data: $( "#cboForm" ).serialize(),
			}).done(function( json ) {
					 obj = JSON.parse(json);
					 var str = '';
					 $.each(obj, function(i, item) {
						str += item + '\n';
					})
					if( "Successful\n" == str ){
						alert('Successful');
					}else{
						alert(str);
					}
					
			});
		

	}
	</script>
</head>

<body>

<div id="main">
<div id="header">
			<div class="center_content">
				<div id="logo"><img src="{{url()}}/{{Config::get('setting.affiliate_logo')}}" /></div>
				<div id="form_portion">
					<form method="post" action="login_process" accept-charset="UTF-8" autocomplete="off">
						<a href="register">
							<div id="reg_btn"></div>
						</a>
						<input type="text" name="username" class="username" placeholder="Username" value=""/>						
						<input type="password" name="password" class="pass" placeholder="Password"/>
						
						<div id="code">
							<img src="captcha" width="50" height="22"/>
						</div>
						
						<input type="text" class="code_fill" name="code" placeholder="Code"/>
						<a href="#">
							<input id="login_btn" type="submit" value=""style="cursor:pointer;">
						</a>
						<input type="hidden" value="{{csrf_token()}}" name="_token">
					</form>
				</div>
			</div>
		</div>
		
		<div id="navbar">
			<div class="center_content">
				<ul>
					<li id="nav_home"><a href="#">Home</a></li>
					<div class="nav_divider"></div>
					<li id="nav_product"><a href="#">Our Products</a></li>
					<div class="nav_divider"></div>
					<li id="nav_cp"><a href="#">Commission Plan</a></li>
					<div class="nav_divider"></div>
					<li id="nav_faq"><a href="#">FAQ</a></li>
					<div class="nav_divider"></div>
					<li id="nav_contact"><a href="#">CONTACT US</a>
					<div class="contact_dd">
					<table border="0" cellspacing="0" cellpadding="2">
						<tr>
							<td>Email </td>
							<td>:</td>
							<td>affiliate@ {{Config::get('setting.website_name')}}.com</td>
						</tr>
						<tr>
							<td>Skype </td>
							<td>:</td>
							<td>{{Session::get('setting.website_name')}}</td>
						</tr>
						<tr>
							<td>Phone</td>
							<td>:</td>
							<td>6011 2780 7697</td>
						</tr>
						<tr>
							<td>Live Chat</td>
							<td>:</td>
							<td>24x7</td>
						</tr>
					</table>
					<div class="clear"></div>
					</div>
					</li>
				</ul>
			</div>
		</div> 

<div class="clear"></div>

</div> 
 </li>
  
  </ul>
  
  </div>

</div><!--end navbar-->

<div id="main_container">
<h2>JOINNOW</h2>

<form id="cboForm">
<table>
<tr>
<td class="formLabelLeft">USERNAME</td>
<td>
<input type="text" name="username" class="easyui-validatebox" value="">
</td>
</tr>
<tr>
<td class="formLabelLeft">PASSWORD</td>
<td>
<input type="password" name="password" class="easyui-validatebox" value="">
</td>
</tr>
<tr>
<td class="formLabelLeft">RETYPEPASSWORD</td>
<td>
<input type="password" name="confirm" class="easyui-validatebox" value="">
</td>
</tr>
<tr>
<td class="formLabelLeft">Full Name</td>
<td>
<input type="text" name="name" class="easyui-validatebox" value="">
</td>
</tr>
<tr>
<td class="formLabelLeft">EMAIL</td>
<td>
<input type="text" name="email" class="easyui-validatebox" value="">
</td>
</tr>
<tr>
<td class="formLabelLeft">Contact Number</td>
<td>
<input type="text" name="telephone" class="easyui-validatebox" value="">
</td>
</tr>
<tr>
<td class="formLabelLeft">Website</td>
<td>
<input type="text" name="website" class="easyui-validatebox" value="">
</td>
</tr>
<tr>
<td class="formLabelLeft">Website Description</td>
<td>
<textarea cols="50" rows="4" name="websitedesc"></textarea>
</td>
</tr>
<tr>
<td colspan="2" style="text-align:right;">
<br><br>

<div onClick="register()" class="btn">Register</div>
</td>
</tr>
</table>

</form>

</div><!--end main container-->



    <div id="copyright">
        <div class="center_content">
          <div id="icon"><img src="{{url()}}/affiliate/images/en/footer_icon.png" width="525" height="31" /></div>
                    <div id="left_copyright">© COPYRIGHT</div>
        
                <div id="right_tnc">
                <a href="#">TNC</a> &nbsp;	&nbsp;&nbsp;
                <a href="#">DISCLAIMER</a> &nbsp;&nbsp;&nbsp;   
                <a href="#">PRIVACYPOLICY</a> &nbsp;&nbsp;&nbsp;	
                <a href="#">FAQ</a>
                </div>
        </div><!--end center_content-->
    </div>	<!--end copyright->

</div><!--end main-->

</body>
</html>
