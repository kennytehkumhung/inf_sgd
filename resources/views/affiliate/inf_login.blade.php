﻿<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Affiliate - Home - Infiniwin</title>
  <link href="{{ asset('/front/affiliate/css/bootstrap.min.css') }}" rel="stylesheet">
  <link href="{{ asset('/front/affiliate/css/animate.min.css') }}" rel="stylesheet">
  <link href="{{ asset('/front/affiliate/css/font-awesome.min.css') }}" rel="stylesheet">
  <link href="{{ asset('/front/affiliate/css/lightbox.css') }}" rel="stylesheet">
  <link href="{{ asset('/front/affiliate/css/main.css') }}" rel="stylesheet">
  <link id="css-preset" href="{{ asset('/front/affiliate/css/presets/preset1.css') }}" rel="stylesheet">
  <link href="{{ asset('/front/affiliate/css/responsive.css') }}" rel="stylesheet">

  <!--[if lt IE 9]>
    <script src="{{ asset('/front/affiliate/js/html5shiv.js') }}"></script>
    <script src="{{ asset('/front/affiliate/js/respond.min.js') }}"></script>
  <![endif]-->
  
  <link href='//fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>
  <link rel="shortcut icon" href="{{ asset('/affiliate/v2/images/favicon.ico') }}">
<script>
	@if (session('message'))
	   alert('{{ session('message') }}');
	@endif
</script>
</head><!--/head-->

<body>
<div id="sky">
  <a href="#blog"><div id="sun"></div></a>
</div>
<!--.login popup-->
<div id="loginBx" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
	<form method="post" action="login_process" accept-charset="UTF-8" autocomplete="off">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Please Login</h4>
      </div>
      <div class="modal-body">
	  
        <div class="row">
        <div class="col-sm-3">
        <label>Username</label>
        </div>
        <div class="col-sm-9">
        <input type="text" name="username">
        </div>

        </div>
        
        <div class="row">
        <div class="col-sm-3">
        <label>Password</label>
        </div>
        <div class="col-sm-9">
        <input type="password" name="password">
        </div>

        </div>
        
        <div class="row">
        <div class="col-sm-3">
        <label>Code</label>
        </div>
        <div class="col-sm-4">
        <input type="text" class="code_fill" name="code"/>
        </div>
        <div class="col-sm-4">
        <img src="captcha" width="50" height="22"/>
        </div>
        </div>
        
        
        
        
        
        
        
      </div>
      <div class="modal-footer">
<input id="login_btn" type="submit" value="Login" style="cursor:pointer;">


        
      </div>
	</form>
    </div>

  </div>
</div>
<!--/.login popup-->

  <!--.preloader-->
  <div class="preloader"> <i class="fa fa-circle-o-notch fa-spin"></i></div>
  <!--/.preloader-->

  <header id="home">
    <div id="home-slider" class="carousel slide carousel-fade" data-ride="carousel">
      <div class="carousel-inner">
        <div class="item active" style="background-image: url(affiliate/v2/images/slider/1.jpg)">
          <div class="caption">
            <h1 class="animated fadeInLeftBig">Welcome to <span>INFINIWIN AFFILIATE</span></h1>
            <p class="animated fadeInRightBig"><span style="font-weight: bold; color: #D4B35D;">4 Simple Steps</span> - Sign Up - Approved - Promote - Start Earning</p>
            <p class="animated fadeInLeftBig" style="margin-top: 30px;"><span style="font-weight: bold; color: #D4B35D;"><a href="#blog">Sign Up</a></span> for free now!</p>
            <a data-scroll class="btn btn-start animated fadeInUpBig" href="#services">Learn More</a>
          </div>
        </div>
        <div class="item" style="background-image: url(affiliate/v2/images/slider/2.jpg)">
          <div class="caption">
            <h1 class="animated fadeInLeftBig">High percentage in <span>Revenue</span></h1>
            <p class="animated fadeInRightBig">Keep up to <span style="font-weight: bold; color: #D4B35D;">45%</span> of all your generated revenue</p>
            <p class="animated fadeInLeftBig" style="margin-top: 30px;"><span style="font-weight: bold; color: #D4B35D;"><a href="#blog">Sign Up</a></span> for free now!</p>
            <a data-scroll class="btn btn-start animated fadeInUpBig" href="#services">Learn More</a>
          </div>
        </div>
        <div class="item" style="background-image: url(affiliate/v2/images/slider/3.jpg)">
          <div class="caption">
            <h1 class="animated fadeInLeftBig">Lucrative <span>Revenue Program</span></h1>
            <p class="animated fadeInRightBig"><span style="font-weight: bold; color: #D4B35D;">Earn Lifetime</span> revenue on referred players</p>
            <p class="animated fadeInLeftBig" style="margin-top: 30px;"><span style="font-weight: bold; color: #D4B35D;"><a href="#blog">Sign Up</a></span> for free now!</p>
            <a data-scroll class="btn btn-start animated fadeInUpBig" href="#services">Learn More</a>
          </div>
        </div>
        
      </div>
      <a class="left-control" href="#home-slider" data-slide="prev"><i class="fa fa-angle-left"></i></a>
      <a class="right-control" href="#home-slider" data-slide="next"><i class="fa fa-angle-right"></i></a>

      <a id="tohash" href="#services"><i class="fa fa-angle-down"></i></a>

    </div><!--/#home-slider-->
    <div class="main-nav">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="{{ url() }}">
            <h1><img class="img-responsive" src="{{ asset('/front/affiliate/images/logo.png') }}" alt="logo"></h1>
          </a>                    
        </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav navbar-right">                 
            <li class="scroll active"><a href="#services">Home</a></li>
            <li class="scroll"><a href="#about-us">Our Products</a></li> 
            <li class="scroll"><a href="#pricing">Commission Plan</a></li>                     
            <li class="scroll"><a href="#features">Faq</a></li>
            <li class="scroll hot"><a href="#blog">Join Now</a></li>
            <li class="scroll"><a data-toggle="modal" data-target="#loginBx" href="#">Login</a></li>
            <li class="scroll"><a href="#footer">Contact Us</a></li>       
          </ul>
        </div>
      </div>
    </div><!--/#main-nav-->
  </header><!--/#home-->
  <section id="services">
    <div class="container">
          
          <div class="heading wow fadeInUp animated" data-wow-duration="1000ms" data-wow-delay="300ms" style="visibility: visible; animation-duration: 1000ms; animation-delay: 300ms; animation-name: fadeInUp;">
        <div class="row">
        <div class="text-center col-sm-8 col-sm-offset-2">
            <h2>4 Easy step , Start Earning Now</h2>
            <p>Our products range from live casino to sportsbook</p>
            
          </div>
          </div> 
      </div>
      
      <div class="text-center our-services">
          <div class="row">
          <div class="col-sm-3 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="300ms">
            <div class="service-icon">
              <i class="fa fa-pencil-square-o"></i>
            </div>
            <div class="service-info">
              <h3>Sign Up</h3>
              <p>As soon as you submit it, you become part of the most rewarding affiliate program around</p>
            </div>
          </div>
          <div class="col-sm-3 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="450ms">
            <div class="service-icon">
              <i class="fa fa-thumbs-up"></i>
            </div>
            <div class="service-info">
              <h3>Approved</h3>
              <p>Approval process is fast and hassle free</p>
            </div>
          </div>
          <div class="col-sm-3 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="550ms">
            <div class="service-icon">
              <i class="fa fa-volume-up"></i>
            </div>
            <div class="service-info">
              <h3>Promote</h3>
              <p>We offers you a vast array of advanced marketing tools to attract and start promoting </p>
            </div>
          </div>
          <div class="col-sm-3 wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="650ms">
            <div class="service-icon">
              <i class="fa fa-money"></i>
            </div>
            <div class="service-info">
              <h3>Start Earning!</h3>
              <p> The more you keep them coming, the higher your revenue.</p>
            </div>
          </div>
          
          
        </div>
      </div>
    </div>
  </section><!--/#services-->
  <section id="about-us" class="parallax">
    <div class="container">
    <div class="heading wow fadeInUp animated" data-wow-duration="1000ms" data-wow-delay="300ms" style="visibility: visible; animation-duration: 1000ms; animation-delay: 300ms; animation-name: fadeInUp;">
        <div class="row">
        <div class="text-center col-sm-8 col-sm-offset-2">
            <h2>Our Products</h2>
            <p>Why would your members bet anywhere else? We have everything you need for your members to play with us.</p>
            
          </div>
          </div> 
      </div>
      <div class="team-members">
        <div class="row">
          <div class="col-sm-3">
            <div class="team-member wow flipInY" data-wow-duration="1000ms" data-wow-delay="300ms">
              <div class="member-image">
                <img class="img-responsive" src="{{ asset('/front/affiliate/images/team/1.jpg') }}" alt="">
              </div>
              <div class="member-info">
                <h3>Sports</h3>
                <p style="color: #fff;padding: 4px;">The most generous rebates on sports you can follow on your favorite match up to the last final minute. </p>
              </div>
              <div class="social-icons">
                <ul>
                  <li><a class="facebook" href="#blog">Join Now</a></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="col-sm-3">
            <div class="team-member wow flipInY" data-wow-duration="1000ms" data-wow-delay="500ms">
              <div class="member-image">
                <img class="img-responsive" src="{{ asset('/front/affiliate/images/team/2.jpg') }}" alt="">
              </div>
              <div class="member-info">
                <h3>Live Casino</h3>
                <p style="color: #fff;padding: 4px;">Offering all customers a fantastic online gaming experience and top notch apps technology</p>
              </div>
              <div class="social-icons">
               <ul>
                  <li><a class="facebook" href="#blog">Join Now</a></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="col-sm-3">
            <div class="team-member wow flipInY" data-wow-duration="1000ms" data-wow-delay="800ms">
              <div class="member-image">
                <img class="img-responsive" src="{{ asset('/front/affiliate/images/team/3.jpg') }}" alt="">
              </div>
              <div class="member-info">
                <h3>Poker</h3>
                 <p style="color: #fff;padding: 4px;">You'll find more games than other Poker sites, with 24/7 support, secure deposits, fast withdraw</p>
              </div>
              <div class="social-icons">
                <ul>
                  <li><a class="facebook" href="#blog">Join Now</a></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="col-sm-3">
            <div class="team-member wow flipInY" data-wow-duration="1000ms" data-wow-delay="1100ms">
              <div class="member-image">
                <img class="img-responsive" src="{{ asset('/front/affiliate/images/team/4.jpg') }}" alt="">
              </div>
              <div class="member-info">
                <h3>Slot Games</h3>
                <p style="color: #fff;padding: 4px;">You will find all the latest and innovative Slots and Casino games such as Slots Machine and many more</p>
              </div>
              <div class="social-icons">
                <ul>
                  <li><a class="facebook" href="#blog">Join Now</a></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="col-sm-3">
            <div class="team-member wow flipInY" data-wow-duration="1000ms" data-wow-delay="1100ms">
              <div class="member-image">
                <img class="img-responsive" src="{{ asset('/front/affiliate/images/team/5.jpg') }}" alt="">
              </div>
              <div class="member-info">
                <h3>Mobile Slot</h3>
                <p style="color: #fff;padding: 4px;">You will find all the latest and innovative Slots and Casino games such as Slots Machine, Blackjack and many more</p>
              </div>
              <div class="social-icons">
                <ul>
                  <li><a class="facebook" href="#blog">Join Now</a></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="col-sm-3">
            <div class="team-member wow flipInY" data-wow-duration="1000ms" data-wow-delay="1100ms">
              <div class="member-image">
                <img class="img-responsive" src="{{ asset('/front/affiliate/images/team/6.jpg') }}" alt="">
              </div>
              <div class="member-info">
                <h3>Racebook</h3>
                <p style="color: #fff;padding: 4px;">You will find all the latest and innovative Slots and Casino games such as Slots Machine, Blackjack and many more</p>
              </div>
              <div class="social-icons">
                <ul>
                  <li><a class="facebook" href="#blog">Join Now</a></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="col-sm-3">
            <div class="team-member wow flipInY" data-wow-duration="1000ms" data-wow-delay="1100ms">
              <div class="member-image">
                <img class="img-responsive" src="{{ asset('/front/affiliate/images/team/7.jpg') }}" alt="">
              </div>
              <div class="member-info">
                <h3>4D</h3>
                <p style="color: #fff;padding: 4px;">Latest results and high payout</p>
              </div>
              <div class="social-icons">
                <ul>
                  <li><a class="facebook" href="#blog">Join Now</a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
      
    </div>
  </section><!--/#about-us-->
  
  <section id="pricing">
    <div class="container">
      <div class="row">
        <div class="heading text-center col-sm-9 col-sm-offset-1 wow fadeInUp" data-wow-duration="1200ms" data-wow-delay="300ms">
          <h2>Welcome to Infiniwin Affiliate Program</h2>
          <p>Infiniwin Affiliate Program is providing our affiliate member the most attractive profit sharing structure. All Infiniwin Affiliate member can enjoy up to 45% of commissions.</p>
        </div>
      </div>
      <hr>
      <div class="row">
        <div class="heading text-center col-sm-9 col-sm-offset-1 wow fadeInUp" data-wow-duration="1200ms" data-wow-delay="300ms">
          <h2>Why Infiniwin Affiliate Program</h2>
          <p>
          <div class="list1">
          <ul>
          <li>We are providing World-Class Gaming Product: Sportsbook, Live Casino, 4D, Slot game,Poker and RNG Slot Games etc..</li>
          <li>Our effective market analysis and marketing strategy which guaranteed high members conversion and retention rate.</li>
          <li>Highest Commission Given in the Field.</li>
          <li>Commission Rate Up to 45%</li>
          <li>We are providing user-friendly software consists of online reporting of your affiliate performance.</li>
          <li>We have a well-trained affiliate team who will full-filled your requirement.</li>
          </ul>
          </div>
          </p>
        </div>
      </div>
      <hr>
      <div class="row">
        <div class="heading text-center col-sm-9 col-sm-offset-1 wow fadeInUp" data-wow-duration="1200ms" data-wow-delay="300ms">
          <h2>Infiniwin Commission Structure <br><span style="font-size: 17px;">( Sole Discretion of Infiniwin )</span></h2>
          <p>You can receive 25% to 45% of your affiliate member's net losses as your affiliate commission in Infiniwin affiliate program.</p>
        </div>
      </div>
      <div class="pricing-table">
        <div class="row">
          <div class="col-sm-4">
            <div class="single-table wow flipInY" data-wow-duration="1000ms" data-wow-delay="300ms">
              <h3>Member Affiliate's Net Losses<br>
<span style="font-size: 12px;padding-bottom: 4px;">( First Day to Last Day of the Month )</span><br>
<span style="font-size: 12px;padding-bottom: 4px;">(MYR or same Currency)</span></h3>
<hr>
              <div class="price">
              <span>1. </span>MYR 30 to MYR 20,000                          
              </div>
              <div class="price">
              <span>2. Next</span> MYR20.001 to MYR 200,000                         
              </div>
              <div class="price">
              <span>3. Next</span> MYR 200,001 and above                        
              </div>
              <hr>
              <a href="#blog" class="btn btn-lg btn-primary">Join Now</a>
              or
              <a href="#" class="btn btn-lg btn-danger" data-toggle="modal" data-target="#loginBx">Login</a>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="single-table wow flipInY" data-wow-duration="1000ms" data-wow-delay="500ms">
              <h3>Monthly Active Member<br>
<span style="font-size: 12px;padding-bottom: 4px;">(At Least 1  Active Wager)</span><br><br>
<hr>
</h3>
              <div class="price">
              <span>>5</span>                                
              </div>
              <div class="price">
              <span>>5</span>                                
              </div>
              <div class="price">
              <span>>5</span>                                
              </div>
              <hr>
              <a href="#blog" class="btn btn-lg btn-primary">Join Now</a>
              or
              <a href="#" class="btn btn-lg btn-danger" data-toggle="modal" data-target="#loginBx">Login</a>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="single-table wow flipInY" data-wow-duration="1000ms" data-wow-delay="800ms">
              <h3 style="padding-bottom: 19px;padding-top: 10px;margin-bottom: 30px;">Commision Rate</h3>
              <hr>
              <div class="price" style="margin-top: 30px;">
              <span>25%</span>                                
              </div>
              <div class="price">
              <span>35%</span>                                
              </div>
              <div class="price">
              <span>45%</span>                                
              </div>
              <hr>
              <a href="#blog" class="btn btn-lg btn-primary">Join Now</a>
              or
              <a href="#" class="btn btn-lg btn-danger" data-toggle="modal" data-target="#loginBx">Login</a>
            </div>
          </div>
          
        </div>
        <div class="smallT">(Minimum Payout = MYR 300.00)</div>
      </div>
      <div class="row">
        <div class="heading col-sm-12 wow fadeInUp" data-wow-duration="1200ms" data-wow-delay="300ms">

          <p style="font-weight: bold;">Above is the Infiniwin Affiliate Program's Commission Rate Table. ( Commission Rate is based on member's net losses. )</p>
          <p>Important: With at least 1 active members (members that have placed at one bet in the month) will entitle you to start earning commission.. Negative revenue of any given month will be carried forward to the following month.</p>
<p>
Example 1: If Company net profit ( Member net losses) is MYR 20,000, affiliate commission rate will be 25%. (shown on the table above)</p>
<p>

Example 2: If Company net profit ( Member net losses) is MYR 60,000, affiliate commission rate will be 25% of the first MYR 20,000 of Company net profit and 35% of the next MYR 40,000 of Company net profit.(shown on the table above)
</p>
<p>
Example 3: If Company net profit ( Member net losses) is MYR 300,000, affiliate commission rate will be 25% of the first MYR 20,000 of Company net profit, 35% of the next MYR 200,000 of Company net profit and 45% of the next MYR 80,000 of company net profit.(shown on the table above)
</p>
<p>
<div class="list1">
<ul>
<li>With a 45% commission rate, 45% of members' net losses is your commission. Increase in members' net losses and active members will enable you to earn more.</li>
<li>With at least 6 active members (members that have placed at one bet in the month) will entitle you to start earning commission.</li>
<li>We monitor our affiliates' performance and may contact you directly to increase your commission rate.</li>
<li>Negative revenue for any given month will be carried forward to the following month.</li>
<li>918 kiss slot and 4D product not included at the profit and loss statement calculation.</li>
<li>All affiliates are subjected to administrative and marketing costs incurred directly for the acquisition of members.</li>
<li>The cost is deducted from the revenue on a monthly basis. These cost may include the following
<ol>
  <li>Payment charges – Deposit and withdrawal charges involving all the affiliate's members.</li>
	<li>Media Fees – Any fees that incurred towards Infiniwin to support or assist affiliate in conjunction with promotions or marketing purposes.</li>
    <li>Promotion bonuses – Monetary bonuses or rebates given to the affiliate's members.</li>
    	</ol>
</li>
</ul>
</div>
 <h2>Payout</h2>
 <p>
 <div class="list1">
<ul>
<li>Commissions will be automatically deposited into your bank account specified upon registration within next 5 working days after the last day of the month. There will only be one payout per each calendar month. While we offer free payout transactions we will not be held responsible if the receiving financial institute impose a fee of any kind at their end.
</li>
<li>There is a minimum amount of MYR 300 or equivalent currency for payout to take effect. Any amount below this threshold will be carried forward to the next month.

</li>
</ul>
</div>
 </p>
 <p>
 Infiniwin reserves the right to alter or amend and/or add any of the terms mentioned above as we deemed fit.
  </p>
  <p>
  Infiniwin reserves the right to cancel the affiliate account at any point of time without prior notice in the event the affiliate's performance does not meet our expected quotas at a given period of time.
  </p>
</p>
        </div>
      </div>
      
      
      
    </div>
  </section><!--/#pricing-->


  <section id="features" class="parallax">
    <div class="container">
    <div class="row">
        <div class="heading text-left col-sm-12 wow fadeInUp" data-wow-duration="1200ms" data-wow-delay="300ms">
          <h2>FAQs</h2>
         
        </div>
      </div>
      <p>
      <div class="row">
        <div class="heading text-left col-sm-12 wow fadeInUp" data-wow-duration="1200ms" data-wow-delay="300ms">
        <h3>Infiniwin Affiliate Program</h3>
          <ol>
          <li><span style="font-weight: bold;color:#D4B35D;">What is the Infiniwin Affiliate Program?</span>
          <p>The  Affiliate Program is a Partnership Program which allows you to receive commission on every player you referred to Infiniwin based on the player's wagering activity over the course of the player's account life.</p>
          </li>
          
          <li><span style="font-weight: bold;color:#D4B35D;">Is there a set up fee?</span>
          <p>To sign up in Infiniwin Affiliate Program is absolutely free!</p>
          </li>
          
          <li><span style="font-weight: bold;color:#D4B35D;">How can I register with the affiliate program?</span>
          <p>To join our Affiliate Program, simply fill out the Registration Form found in our website by accessing this: link and within a few days you will be received an email  by our Affiliate Program Manager regarding your acceptance into the program. For the meantime, please read through our terms and conditions by accessing this link. </p>
          </li>
          
          <li><span style="font-weight: bold;color:#D4B35D;">Can the affiliate himself also be a player?</span>
          <p>Affiliates will not be allowed to place wagers using an Affiliate account. An affiliate needs to register and use his/her player account in able to place wagers.</p>
          </li>
          
          <li><span style="font-weight: bold;color:#D4B35D;">Can an affiliate have more than one account? </span>
          <p>Affiliate shall not have more than one affiliate account, nor shall an Affiliate be allowed to earn commissions on their own or through any related parties.</p>
          </li>
          
          <li><span style="font-weight: bold;color:#D4B35D;">What if i don't have a website, can i still join?</span>
          <p>To participate in the Infiniwin Affiliate Program, you should have a Website that has online gambling as its central theme. Should you not have a website, Infiniwin Affiliate will however review cases on an individual basis. Please email affiliate@infiniwin.net for more information..</p>
          </li>
          
          <li><span style="font-weight: bold;color:#D4B35D;">If I have multiple websites, do i need to create multiple affiliate accounts?</span>
          <p>If you have multiple websites, you only need to apply once to Infiniwin Affiliates and use the same affiliate ID for multiple sites. You can also set up separate tracking codes for each site, so you compare how well each site performs. There is no need to create another account for every website created, since Affiliates are not allowed to have multiple accounts.</p>
          </li>
          
          <li><span style="font-weight: bold;color:#D4B35D;">What is the percentage of profit that I get paid?</span>
          <p>Infiniwin offers one of the best Affiliate Program commissions available in the Internet Gaming industry today. Our affiliates can enjoy up to a 45% net profit. Infiniwin guarantees you one of the best conversion ratios on the web today. </p>
          </li>
          
          <li><span style="font-weight: bold;color:#D4B35D;">Do you carry over negative balances?</span>
          <p>Yes, when you incur a negative balance, it will be carried over to the next month. For example if for the month of January your affiliate account ends with -MYR1000, this amount will be carried to February. If at the end of February your affiliate account earns MYR3000, the total balance will be MYR2000 (MYR3000 deduct -MYR1000). However if your account is still making a loss at the end of February, for example -MYR500, then it will be added to the previous negative and be carried forward to March resulting in your March balance brought forward to be -MYR1500. The commission earned shall then be paid according to the scale.</p>
          </li>
          
          <li><span style="font-weight: bold;color:#D4B35D;">How can I check how much I have earned?</span>
          <p>We provide online stats for you 24 hours a day, 7 days a week and 365 days a year. Login with your user name and password to our secure affiliate stats interface page to see how much you've earned and other relevant stats.</p>
          </li>
          
          <li><span style="font-weight: bold;color:#D4B35D;">What are my responsibilities as an affiliate?</span>
          <p>As an Affiliate, you are responsible for promoting Infiniwin by implementing the advertising, banners and tracking URL's on your websites, e-mails or other communications.</p>
          </li>
          
          </ol>
          </div>
          </div>
          </p>
          
          <p>
      <div class="row">
        <div class="heading text-left col-sm-12 wow fadeInUp" data-wow-duration="1200ms" data-wow-delay="300ms">
        <h3>Payout</h3>
          <ol>
          <li><span style="font-weight: bold;color:#D4B35D;">How and when do I get paid?</span>
          <p>We pay our affiliates on the 15th of every month. Depending on the amount due that month, we will send you the money through bank transfer. </p>
          </li>
          
          <li><span style="font-weight: bold;color:#D4B35D;">What are your payment methods?</span>
          <p>The commissions will be paid by bank transfer as specified by the Affiliate's.</p>
          </li>
          
          <li><span style="font-weight: bold;color:#D4B35D;">What is the conversion rate being used if I use a different currency other than MYR?</span>
          <p>We usually pay our affiliate commission using MYR. For other currencies, respective exchange rate on the payout date will be applied.</p>
          </li>
          
          </ol>
          </div>
          </div>
          </p>
          
          <p>
      <div class="row">
        <div class="heading text-left col-sm-12 wow fadeInUp" data-wow-duration="1200ms" data-wow-delay="300ms">
        <h3>Reporting</h3>
          <ol>
          <li><span style="font-weight: bold;color:#D4B35D;">How can I check my account statistics?</span>
          <p>You can login to this page by entering your username and password to check your account details: affiliate.infiniwin.com</p>
          </li>
          
          <li><span style="font-weight: bold;color:#D4B35D;">What information is displayed on my stats page? </span>
          <p>Infiniwin Affiliate system displays a very detailed statistic analysis for affiliates in real time, anytime. You will be able to view the exact number of Impressions,  Signups of Real accounts, and of course, your share of the profit. </p>
          </li>
          
          <li><span style="font-weight: bold;color:#D4B35D;">I have checked the online stats for a while, but they haven't changed.</span>
          <p>The database serving the online stats interface is updated once every few hours. The date and time it was last updated is displayed at the top of the stats page.</p>
          </li>
          
          </ol>
          </div>
          </div>
          </p>
          
          <p>
      <div class="row">
        <div class="heading text-left col-sm-12 wow fadeInUp" data-wow-duration="1200ms" data-wow-delay="300ms">
        <h3>Account Information</h3>
          <ol>
          <li><span style="font-weight: bold;color:#D4B35D;">Can I edit my account information? How can I edit it?</span>
          <p>You can't edit your username, affiliate ID and email address. We only support alphanumeric characters for usernames and passwords. Usernames must have at least six (6) alphanumeric characters.</p>
          </li>
          
          <li><span style="font-weight: bold;color:#D4B35D;">I do not have a website, what should I put in the website URL?</span>
          <p>If you do not have a website, please put it blank. </p>
          </li>
          
          <li><span style="font-weight: bold;color:#D4B35D;">What to do if I forget my login details? </span>
          <p>Please contact our customer support or please email to affiliate@infiniwin.net</p>
          </li>
          
          </ol>
          </div>
          </div>
          </p>
          
          <p>
      <div class="row">
        <div class="heading text-left col-sm-12 wow fadeInUp" data-wow-duration="1200ms" data-wow-delay="300ms">
        <h3>Marketing</h3>
          <ol>
          <li><span style="font-weight: bold;color:#D4B35D;">How does the tracking link work?</span>
          <p>We use your affiliate ID for your tracking link so we can trace the players joining from your website.</p>
          <p>Example: //infiniwin.com/my?utm_source=affiliate&utm_medium=1234567&refcode=1234567</p>
          </li>
          
          </ol>
          
          </div>
          </div>
          </p>
      
    </div>
  </section><!--/#features-->



  <section id="blog">
    <div class="container">
      <div class="row">
        <div class="heading text-center col-sm-8 col-sm-offset-2 wow fadeInUp" data-wow-duration="1200ms" data-wow-delay="300ms">
          <h2>Join Now</h2>
          <p>Kindly fill up the form below and our Affiliate Program Manager will contact you as soon as possible</p>
        </div>
      </div>
      <div class="contact-form wow fadeIn" data-wow-duration="1000ms" data-wow-delay="600ms">
          <div class="row">
            <div class="col-sm-8 col-sm-offset-2">
              <form id="main-contact-form" name="contact-form" method="post" action="#">
                <div class="row  wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="300ms">
                  <div class="col-sm-12">
                    <div class="form-group">
                      <input type="text" name="username" class="form-control" placeholder="Username" required="required">
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <input type="password" name="password" class="form-control" placeholder="Password" required="required">
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <input type="password" name="confirm" class="form-control" placeholder="Re-type Password" required="required">
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group">
                      <input type="text" name="name" class="form-control" placeholder="Full Name" required="required">
                    </div>
                  </div>
                   <div class="col-sm-6">
                    <div class="form-group">
                      <input type="text" name="email" class="form-control" placeholder="Email" required="required">
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <input type="text" name="telephone" class="form-control" placeholder="Contact Number" required="required">
                    </div>
                  </div>
                  {{--<div class="col-sm-12">--}}
                    {{--<div class="form-group">--}}
                      {{--<input type="" name="website" class="form-control" placeholder="Website" required="required">--}}
                    {{--</div>--}}
                  {{--</div>--}}
                </div>
                {{--<div class="form-group">--}}
                  {{--<textarea name="websitedesc" id="message" class="form-control" rows="4" placeholder="Website description" required="required"></textarea>--}}
                {{--</div>                        --}}
                <div class="form-group">
                  <button id="btn_register" type="submit" class="btn-submit" onClick="register(this)">Register Now</button>
                </div>
              </form>   
            </div>
            
          </div>
        </div>
    </div>
  </section><!--/#blog-->

  <footer id="footer">
    <div class="footer-top wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="300ms">
      <div class="container text-center">
        <div class="col-sm-12">
              <div class="contact-info wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="300ms">
                <ul class="address">
                  <li><i class="fa fa-envelope"></i> <span> Email:</span><a href="mailto:affiliate@Infiniwin.com">affiliate@infiniwin.net</a></li>

                </ul>
              </div>                            
            </div>
        
      </div>
    </div>
    <div class="footer-bottom">
      <div class="container">
        <div class="row">
          <div class="col-sm-6">
            <p>&copy; 2017 infiniwin.com</p>
          </div>
          <div class="col-sm-6">
            <p class="pull-right"> <img class="img-responsive" src="{{ asset('/affiliate/v2/images/footer_icon.png') }}" alt=""></p>
          </div>
        </div>
      </div>
    </div>
  </footer>

  <script type="text/javascript" src="{{ asset('/front/affiliate/js/jquery.js') }}"></script>
  <script type="text/javascript" src="{{ asset('/front/affiliate/js/bootstrap.min.js') }}"></script>
  <script type="text/javascript" src="//maps.google.com/maps/api/js?sensor=true"></script>
  <script type="text/javascript" src="{{ asset('/front/affiliate/js/jquery.inview.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('/front/affiliate/js/wow.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('/front/affiliate/js/mousescroll.js') }}"></script>
  <script type="text/javascript" src="{{ asset('/front/affiliate/js/smoothscroll.js') }}"></script>
  <script type="text/javascript" src="{{ asset('/front/affiliate/js/jquery.countTo.js') }}"></script>
  <script type="text/javascript" src="{{ asset('/front/affiliate/js/lightbox.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('/front/affiliate/js/main.js') }}"></script>
  <script type="text/javascript" src="{{ asset('/front/affiliate/js/jqfloat.min.js') }}"></script>
	<script type="text/javascript">

        $(document).ready(function() {
            //jqfloat.js script
            $('#sun').jqFloat({
                width:20,
                height:80,
                speed:1800
            });
        });
	
	function register(btn){

	    var btnRegObj = $(btn);
	    var btnRegOriText = btnRegObj.text();

	    btnRegObj.prop("readonly", true).prop("disabled", true).text("Loading...");

        var form = $("#main-contact-form");

			$.ajax({
				type: "POST",
				url: "{{route('register_process')}}",
				data: form.serialize()
			}).done(function( json ) {

                btnRegObj.prop("readonly", false).prop("disabled", false).text(btnRegOriText);

					 obj = JSON.parse(json);
					 var str = '';
					 $.each(obj, function(i, item) {
						str += item + '\n';
					});
					if( "Successful\n" == str ){
						alert('Successful Registered. Please check your email for the application status. Thank You!');
                        $("input", form).val("");
					}else{
						alert(str);
					}
					
			});
		

	}
	</script>
</body>
</html>