﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>Affiliate - Home - {{Config::get('setting.website_name')}}</title>
<style type="text/css">
body {
	background: url(affiliate/images/369bet_img/bg.jpg) top center  #f1f1f1 !important;
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
.style2 {
	font-family: Tahoma;
	font-size: 20px;
	font-weight: bold;
	color: #FF4E00;
}
.style3 {
	color: #FFFFFF;
	font-size: 13px;
	font-weight:100;
}
.style4 {
	color: #FF6600;
	font-weight: bold;
}
.style9 {
	color: #FFFFFF
}
.style10 {
	font-family: Tahoma;
	font-size: 16px;
	font-weight: bold;
	color: #333333;
}
.style11 {
	font-family: Tahoma;
	font-size: 12px;
}
</style>
<script type="text/javascript">
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}
</script>
<link rel="stylesheet" type="text/css" href="{{url()}}/affiliate/css/369bet/style.css"/>
<script type="text/javascript" src="{{url()}}/affiliate/js/369bet/jssor.slider.min.js"></script>
<!-- use jssor.slider.debug.js instead for debug -->
<script>
        jssor_1_slider_init = function() {
            
            var jssor_1_SlideoTransitions = [
              [{b:5500,d:3000,o:-1,r:240,e:{r:2}}],
              [{b:-1,d:1,o:-1,c:{x:51.0,t:-51.0}},{b:0,d:1000,o:1,c:{x:-51.0,t:51.0},e:{o:7,c:{x:7,t:7}}}],
              [{b:-1,d:1,o:-1,sX:9,sY:9},{b:1000,d:1000,o:1,sX:-9,sY:-9,e:{sX:2,sY:2}}],
              [{b:-1,d:1,o:-1,r:-180,sX:9,sY:9},{b:2000,d:1000,o:1,r:180,sX:-9,sY:-9,e:{r:2,sX:2,sY:2}}],
              [{b:-1,d:1,o:-1},{b:3000,d:2000,y:180,o:1,e:{y:16}}],
              [{b:-1,d:1,o:-1,r:-150},{b:7500,d:1600,o:1,r:150,e:{r:3}}],
              [{b:10000,d:2000,x:-379,e:{x:7}}],
              [{b:10000,d:2000,x:-379,e:{x:7}}],
              [{b:-1,d:1,o:-1,r:288,sX:9,sY:9},{b:9100,d:900,x:-1400,y:-660,o:1,r:-288,sX:-9,sY:-9,e:{r:6}},{b:10000,d:1600,x:-200,o:-1,e:{x:16}}]
            ];
            
            var jssor_1_options = {
              $AutoPlay: true,
              $SlideDuration: 800,
              $SlideEasing: $Jease$.$OutQuint,
              $CaptionSliderOptions: {
                $Class: $JssorCaptionSlideo$,
                $Transitions: jssor_1_SlideoTransitions
              },
              $ArrowNavigatorOptions: {
                $Class: $JssorArrowNavigator$
              },
              $BulletNavigatorOptions: {
                $Class: $JssorBulletNavigator$
              }
            };
            
            var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);
            
            //responsive code begin
            //you can remove responsive code if you don't want the slider scales while window resizing
            function ScaleSlider() {
                var refSize = jssor_1_slider.$Elmt.parentNode.clientWidth;
                if (refSize) {
                    refSize = Math.min(refSize, 1920);
                    jssor_1_slider.$ScaleWidth(refSize);
                }
                else {
                    window.setTimeout(ScaleSlider, 30);
                }
            }
            ScaleSlider();
            $Jssor$.$AddEvent(window, "load", ScaleSlider);
            $Jssor$.$AddEvent(window, "resize", ScaleSlider);
            $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
            //responsive code end
        };
</script>
<style>
/* jssor slider bullet navigator skin 05 css */
        /*
        .jssorb05 div           (normal)
        .jssorb05 div:hover     (normal mouseover)
        .jssorb05 .av           (active)
        .jssorb05 .av:hover     (active mouseover)
        .jssorb05 .dn           (mousedown)
        */
        .jssorb05 {
	position: absolute;
}
.jssorb05 div, .jssorb05 div:hover, .jssorb05 .av {
	position: absolute;
	/* size of bullet elment */
            width: 16px;
	height: 16px;
	background: url('affiliate/images/369bet_img/b05.png') no-repeat;
	overflow: hidden;
	cursor: pointer;
}
.jssorb05 div {
	background-position: -7px -7px;
}
.jssorb05 div:hover, .jssorb05 .av:hover {
	background-position: -37px -7px;
}
.jssorb05 .av {
	background-position: -67px -7px;
}
.jssorb05 .dn, .jssorb05 .dn:hover {
	background-position: -97px -7px;
}
/* jssor slider arrow navigator skin 22 css */
        /*
        .jssora22l                  (normal)
        .jssora22r                  (normal)
        .jssora22l:hover            (normal mouseover)
        .jssora22r:hover            (normal mouseover)
        .jssora22l.jssora22ldn      (mousedown)
        .jssora22r.jssora22rdn      (mousedown)
        */
        .jssora22l, .jssora22r {
	display: block;
	position: absolute;
	/* size of arrow element */
            width: 40px;
	height: 58px;
	cursor: pointer;
	background: url('affiliate/images/369bet_img/a22.png') center center no-repeat;
	overflow: hidden;
}
.jssora22l {
	background-position: -10px -31px;
}
.jssora22r {
	background-position: -70px -31px;
}
.jssora22l:hover {
	background-position: -130px -31px;
}
.jssora22r:hover {
	background-position: -190px -31px;
}
.jssora22l.jssora22ldn {
	background-position: -250px -31px;
}
.jssora22r.jssora22rdn {
	background-position: -310px -31px;
}
</style>
<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
<link rel="stylesheet" href="{{url()}}/affiliate/css/369bet/jquery.maximage.css" type="text/css" media="screen" title="CSS" charset="utf-8" />
<script src="{{url()}}/affiliate/js/369bet/jquery.cycle.all.js" type="text/javascript"></script>
<script src="{{url()}}/affiliate/js/369bet/jquery.easing.1.3.js" type="text/javascript"></script>
<script src="{{url()}}/affiliate/js/369bet/jquery.maximage.min.js" type="text/javascript"></script>
<script type="text/javascript" charset="utf-8">
      $(function(){
        // Trigger maximage
        jQuery('#maximage').maximage();
      });
</script>
<style type="text/css">
<!--
.style12 {
	font-family: Tahoma;
	font-size: 14px;
	color: #666666;
}
.style13 {
	font-family: Tahoma;
	font-size: 16px;
	color: #333333;
}
.style14 {
	font-weight: bold
}
.style15 {
	font-size: 14px
}
.style16 {
	font-size: 14
}
.style17 {
	font-family: Tahoma;
	font-size: 14px;
	color: #333333;
}
.style18 {font-family: Tahoma; font-size: 18px; }
.style19 {color: #333333}
.style21 {font-size: 13px}
.style22 {color: #000000}
.style24 {
	font-size: 14px;
	font-family: Tahoma;
	font-weight: bold;
}
.style25 {
	font-family: Tahoma;
	font-size: 14px;
}
.style26 {font-family: Tahoma}
.style28 {color: #000000; font-weight: bold; }
.style30 {font-size: 14px; font-weight: bold; color: #FFFFFF; }
.style31 {color: #FFFFFF; font-family: Tahoma; font-size: 14px; font-weight: bold; }
-->
</style>
</head>

<body>
@include('affiliate/include/369betheader')	

<div style="height:350px; width:1050px; margin:auto; background:#FFFFFF;  ">
  <div id="jssor_1" style="position: relative; margin: 0 auto; top: 0px; left: 0px; width: 1036px; height:350px; overflow: hidden; visibility: hidden;">
    <!-- Loading Screen -->
    <div data-u="loading" style="position: absolute; top: 0px; left: 0px;">
      <div style="filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block; top: 0px; left: 0px; width: 100%; height: 100%;"></div>
      <div style="position:absolute;display:block;background:url('affiliate/images/369bet_img/loading.gif') no-repeat center center;top:0px;left:0px;width:100%;height:100%;"></div>
    </div>
    <div data-u="slides" style="cursor: default; position: relative; top: 0px; left: 0px; width: 1036px; height: 350px; overflow: hidden;">
      <div data-p="225.00" style="display: none;"> <img data-u="image" src="{{url()}}/affiliate/images/369bet_img/slide1.jpg" /> </div>
      <div data-p="225.00" style="display: none;"> <img data-u="image" src="{{url()}}/affiliate/images/369bet_img/slide2.jpg" /> </div>
      <div data-p="225.00" style="display: none;"> <img data-u="image" src="{{url()}}/affiliate/images/369bet_img/slide3.jpg" /> </div>
     
    </div>
    <!-- Bullet Navigator -->
    <div data-u="navigator" class="jssorb05" style="bottom:16px;right:16px; " data-autocenter="1">
      <!-- bullet navigator item prototype -->
      <div data-u="prototype" style="width:16px;height:16px;"></div>
    </div>
    <!-- Arrow Navigator -->
    <span data-u="arrowleft" class="jssora22l" style="top:0px;left:12px;width:40px;height:58px;" data-autocenter="2"></span> <span data-u="arrowright" class="jssora22r" style="top:0px;right:12px;width:40px;height:58px;" data-autocenter="2"></span> </div>
<script>
        jssor_1_slider_init();
</script>
</div>
			
<div style="width:1050px;  margin:auto; overflow:hidden; ">
  <div style="width:1036px; margin:auto; padding-top:15px; "><img src="{{url()}}/affiliate/images/369bet_img/step-register.png" /><br /><img src="{{url()}}/affiliate/images/369bet_img/sport1-1.png" /><br /><img src="{{url()}}/affiliate/images/369bet_img/casino1-1.png" /><br /><img src="{{url()}}/affiliate/images/369bet_img/game1.png" /><br /><img src="{{url()}}/affiliate/images/369bet_img/lotto1.png" /></div>
  
  <div style="width:1036px; margin:auto; padding:15px; border:solid 1px #d2caca; background:#fff;">
    <p><font size="5"><strong><font color="#0384ED" size="6" face="Tahoma">เงินเดือน + คอมมิชชั่น </font></strong></font><font color="#000000" size="2" face="Tahoma"><br />
    </font></p>
    <p><strong><font color="#000000" size="3" face="Tahoma">369bet ยินดีต้อนรับสู่แผนค่าคอมมิชชั่นพันธมิตร</font></strong></p>
    <p><font color="#000000" size="3" face="Tahoma"> <font color="#666666">&nbsp;&nbsp;&nbsp;&nbsp; 369bet ตระหนักและมุ่งมั่นในการนำเสนอโปรแกรมที่ดีที่สุดของระบบพันธมิตรให้แก่คุณ โดยโปรแกรมพันธมิตรของ 369bet สามารถรับเงินเดือนสูงถึง 35,000 บาท พร้อมรับค่าคอมมิชชั่นได้สูงถึง 20% ต่อเดือน กับข้อเสนอพิเศษสุดนี้ ที่ๆคุณสามารถขอรับได้ที่นี่ ที่เดียว&nbsp; <br />
      <br />
     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; โปรแกรมพันธมิตรของ 369bet จะทำการจ่ายค่าคอมมิชชั่น 1 ครั้งต่อเดือน การคำนวณค่าคอมมิชชั่นจะทำการคำนวนโดยเริ่มจากวันที่ 1 ถึงวันสุดท้ายของเดือนนั้นๆ จะเป็นการคำนวณค่าคอมมิชชั่น </font><br />
      <br />
      <br />
      <strong>ฐานเงินเดือน และ ฐานคอมมิชชั่น</strong></font></p>
    <p>&nbsp;</p>
    <table width="852" border="0" align="center" cellspacing="1" bgcolor="#CFCFCF">
      <tr>
        <td width="281" bgcolor="#0368BA"><div align="center"><span class="style31">จำนวนสมาชิก</span></div></td>
        <td width="281" height="35" bgcolor="#0368BA"><div align="center"><span class="style31">เงินเดือน (บาท)</span></div></td>
        <td width="280" bgcolor="#0368BA"><div align="center"><span class="style31">ค่าคอมมิชชั่น</span></div></td>
      </tr>
      <tr>
        <td bgcolor="#FFFFFF"><div align="center" class="style25"><font color="#000000">&nbsp;0 - 20</font></div></td>
        <td height="30" bgcolor="#FFFFFF"><div align="center" class="style25"><font color="#000000">0</font></div></td>
        <td bgcolor="#FFFFFF"><div align="center" class="style24"><font color="#000000">&nbsp;5%</font></div></td>
      </tr>
      <tr>
        <td bgcolor="#FFFFFF"><div align="center" class="style25"><font color="#000000">21 - 50</font></div></td>
        <td height="30" bgcolor="#FFFFFF"><div align="center" class="style25"><font color="#000000">&nbsp; 5,000</font></div></td>
        <td bgcolor="#FFFFFF"><div align="center" class="style24"><font color="#000000">&nbsp; 5%</font></div></td>
      </tr>
      <tr>
        <td bgcolor="#FFFFFF"><div align="center" class="style25"><font color="#000000">&nbsp; 51 - 100</font></div></td>
        <td height="30" bgcolor="#FFFFFF"><div align="center" class="style25"><font color="#000000">12,000</font></div></td>
        <td bgcolor="#FFFFFF"><div align="center" class="style24"><font color="#000000">&nbsp;10%</font></div></td>
      </tr>
      <tr>
        <td bgcolor="#FFFFFF"><div align="center" class="style25"><font color="#000000">101 - 150</font></div></td>
        <td height="30" bgcolor="#FFFFFF"><div align="center" class="style25"><font color="#000000">20,000</font></div></td>
        <td bgcolor="#FFFFFF"><div align="center" class="style24"><font color="#000000">15%</font></div></td>
      </tr>
      <tr>
        <td bgcolor="#FFFFFF"><div align="center" class="style25"><font color="#000000">151 - 200</font></div></td>
        <td height="30" bgcolor="#FFFFFF"><div align="center" class="style25"><font color="#000000">28,500</font></div></td>
        <td bgcolor="#FFFFFF"><div align="center" class="style24"><font color="#000000">15%</font></div></td>
      </tr>
      <tr>
        <td bgcolor="#FFFFFF"><div align="center" class="style25"><font color="#000000">200 UP</font></div></td>
        <td height="30" bgcolor="#FFFFFF"><div align="center" class="style25"><font color="#000000">35,000</font></div></td>
        <td bgcolor="#FFFFFF"><div align="center" class="style24"><font color="#000000">20%</font></div></td>
      </tr>
    </table>
    <p style="line-height: 22.72px; margin: 0px 0px 1.35em; color: rgb(0, 0, 0); font-family: Calibri, Arial, Helvetica, sans-serif; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px;"><br />
      <br />
      <strong>369bet มีวิธีการนับจำนวนสมาชิก ดังนี้</strong></p>
    <p style="line-height: 22.72px; margin: 0px 0px 1.35em; color: rgb(0, 0, 0); font-family: Calibri, Arial, Helvetica, sans-serif; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px;"><font color="#666666">1. ถ้าสมัครสมาชิก แต่ไม่มียอดเล่น ทางเราจะถือว่าไม่เป็นสมาชิก<br />
    2.<span class="Apple-converted-space"> </span><span style="line-height: 22.72px;">สมาชิกจะต้องมียอดเล่นสม่ำเสมอตลอดทั้งเดือนเท่านั้น จึงจะนับเป็นสมาชิก</span></font><span style="line-height: 22.72px;"></span></p>
    <p>&nbsp;</p>
    <p><font color="#000000" size="3" face="Tahoma"><strong>กรณียอดบัญชีติดลบ </strong><br />
      <br />
     &nbsp;&nbsp;&nbsp;&nbsp;<font color="#666666">&nbsp; ยอดติดลบของเดือนที่ให้จะถูกยกยอดไป ตัวอย่างเช่น บัญชีพันธมิตรของเดือนมกราคมของคุณ มียอดติดลบที่ -3,480 บาท โดยจำนวนนี้จะถูกยกยอดไปไว้ในเดือนกุมภาพันธ์ และตอนสิ้นเดือนกุมภาพันธ์บัญชีคุณมียอดรายรับเข้ามาที่ +5,800 บาท โดยบัญชีคุณจะมียอดคงเหลือที่ 2,320 บาท (มาจาก +5,800 ลบด้วย -3,480). อย่างไรก็ตาม หากเดือนกุมภาพันธ์คุณยังมียอดติดลบอีก เช่น - 2,500 ทางเราจะนำยอดนี้ไปบวกเพิ่ม ในยอดติดลบก่อนหน้านี้ เพื่อยกยอดไปยังเดือนถัดไปคือ เดือนมีนาคม รวมเป็นยอดติดลบทั้งหมด <span style="font-family: Calibri, Arial, Helvetica, sans-serif; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); display: inline !important; float: none;">-5980</span> บาท แต่ทางคุณยังสามารถรับเงินเดือนได้ตามเรท ปรกติ ที่ทาง 369bet ได้กำหนดไว้</font></font></p>
    <p><font color="#000000" size="3" face="Tahoma"><strong><br />
      การได้รับเงิน </strong><br />
      <br />
      <font color="#666666">1. คุณสามารถแจ้งถอนรายได้คุณโดยใช้สกุลเงิน THB จะขึ้นอยู่กับฝ่ายการจ่ายเงิน<br />
      2. เราจะทำการจ่ายค่าคอมมิชชั่นเพียง 1 ครั้งต่อเดือนเท่านั้น เราจะไม่รับผิดชอบหากสถาบันทางการเงินปลายทางที่ได้โอนค่าคอมมิชชั่นไปกำหนด ในค่าธรรมเนียมขึ้น </font><br />
    </font></p>
    <p><font color="#000000" size="3" face="Tahoma"><strong><br />
    หมายเหตุ :</strong> </font></p>
    <p><font color="#000000" size="3" face="Tahoma"> &nbsp;&nbsp;&nbsp;<font color="#666666">&nbsp; สมาชิกพันธมิตรทุกท่านจะต้องรับผิดชอบค่าใช้จ่ายทางการตลาดที่เกิดขึ้นจาก สมาชิกที่ท่านแนะนำและชักชวนให้เข้ามาเล่นกับ 369bet ค่าใช้จ่ายดังกล่าวจะทำการหักออกจากยอดได้/เสียของสมาชิกในแต่ละเดือน ซึ่งค่าใช้จ่ายดังกล่าว แสดงรายละเอียดดังต่อไปนี้: </font></font></p>
    <p><font color="#666666" size="3" face="Tahoma"> 1. ค่าธรรมเนียมทางการเงิน - ค่าใช้จ่ายในการฝาก-ถอนเงินของสมาชิก </font></p>
    <p><font color="#666666" size="3" face="Tahoma"> 2. ค่าใช้จ่ายของการโฆษณา - ทุกๆค่าใช้จ่ายที่ทาง 369bet ทำการส่งเสริมในการตลาดอาทิ เช่น ค่าใช้จ่ายในโปรโมชั่นหรือการส่งเสริมทางการตลาด </font></p>
    <p><font color="#666666" size="3" face="Tahoma"> 3. โปรโมชั่นโบนัส - เงินโบนัสหรือเงินคืนเดิมพันที่มอบให้สมาชิก <br />
    </font></p>
    <IMG src="{{url()}}/affiliate/images/369bet_img/notice.png" width="100%" />
    <p><font color="#666666" size="3" face="Tahoma"> 1. ห้ามคุณสมัครเอง เพื่อเพิ่มยอดจำนวนสมาชิก เพื่อมาเป็นฐานรับเงินเดือนโดยเด็ดขาด</font></p>
    <p><font color="#666666" size="3" face="Tahoma"> 2. ห้ามสมาชิกของทางคุณ มี ip Address ซ้ำกันโดยเด็ดขาด</font><font color="#000000" size="3" face="Tahoma"><br />
    </font></p>
    <p><font color="#FF0000" size="3" face="Tahoma"> *** ถ้าทาง 369bet ตรวจพบเจอ ตามข้อที่ 1 และ 2 ทาง 369bet ขออนุญาติไม่จ่ายเรทเงินเดือนให้ตามฐานสมาชิก และจะระงับการให้บริการ Affiliate แก่สมาชิกท่านนั้นโดยเด็ดขาด</font></p>
  </div>
  
   <div align="center" style="width:1036px; margin:auto; padding:7px 0 0px 0;">
  <div style="float:left; width:320px; padding:15px; border:solid 1px #d2caca; background:#e4e1e1;">
    <div> 
      <div align="left"><strong>1. ส่งใบสมัคร</strong></div>
    </div>
    <div align="left"><font color="#333333" size="2" face="Tahoma">เริ่มต้นการเข้าร่วมโปรแกรมพันธมิตร ด้วยการกรอกใบสมัครพร้อมระบุข้อมูลของท่าน แล้วทำการยื่นใบสมัคร หลังจากนั้นคุณจะได้ร่วมเป็นส่วนหนึ่งของโปรแกรมพันธมิตรของเรา สมัครเลยซิค่ะ!</font><br />
      <br />
    </div>
    <div> 
      <div align="left"><strong>2. เริ่มทำการประชาสัมพันธ์</strong></div>
    </div>
    <div align="left"><font color="#333333" size="2" face="Tahoma">โปรแกรมพันธมิตรของ 369BET เสนอเครื่องมือทางการตลาดที่ทันสมัยเพื่อให้คุณได้ดึงดูดสมาชิกใหม่ และเริ่มทำการส่งเสริมการขายได้มากขึ้น</font><font color="#333333"><br />
        <br />
    </font></div>
    <div>
      <div align="left"><strong>3. เริ่มสร้างรายได้</strong></div>
    </div>
    <div align="left"><font color="#333333" size="2" face="Tahoma">รายได้หลักของคุณนั้นจะมาจากเงินเดือนประจำตำแหน่ง และ ผู้เล่นทุกคน ที่คุณแนะนำให้เข้าร่วมกับ 369BET ยิ่งคุณสามารถหาผู้เล่นได้มาก คุณก็จะยิ่งได้รับรายได้จากค่าคอมมิชชั่น และฐานเงินเดือนเพิ่มมากขึ้น</font></div>
  </div> 
  <div style="float:left; width:390px; padding:15px; border-right:solid 1px #c3c3c3;">
    <div>
      <div align="left"><strong>ทำไมถึงต้องเข้าร่วมในโปรแกรมพันธมิตรของเรา?</strong></div>
    </div>
    <br />
    <div>
      <div align="left"><strong><font color="#000000" size="2" face="Tahoma">ในการเป็นพันธมิตรกับ 369BET นั้น คุณสามารถ</font></strong><br />
        <br />
      </div>
    </div>
    <div align="left">
      <table width="100%" border="0">
        <tr>
          <td width="8%"><img src="{{url()}}/affiliate/images/369bet_img/bb.png" width="16" height="16" /></td>
          <td width="92%">
            <font color="#333333" size="2" face="Tahoma">รับฐานเงินเดือนสูงสุด 35,000 บาท</font></td>
        </tr>
        <tr>
          <td><img src="{{url()}}/affiliate/images/369bet_img/bb.png" width="16" height="16" /></td>
          <td><font color="#333333" size="2" face="Tahoma">
รับคอมมิชชั่นสูงถึง 20% จากส่วนแบ่งรายได้สุทธิ </font></td>
        </tr>
        <tr>
          <td valign="top"><img src="{{url()}}/affiliate/images/369bet_img/bb.png" width="16" height="16" /></td>
          <td><font color="#333333" size="2" face="Tahoma">รับรายได้สุทธิตลอดไปจากการเดิมพัน ของสมาชิกที่คุณแนะนำ</font></td>
        </tr>
        <tr>
          <td><img src="{{url()}}/affiliate/images/369bet_img/bb.png" width="16" height="16" /></td>
          <td><font color="#333333" size="2" face="Tahoma">รับรายได้เต็มๆจากทุกผลิตภัณฑ์ </font></td>
        </tr>
        <tr>
          <td valign="top"><img src="{{url()}}/affiliate/images/369bet_img/bb.png" width="16" height="16" /></td>
          <td><font color="#333333" size="2" face="Tahoma">ร่วมเป็นส่วนหนึ่ง กับรูปแบบใหม่ของการเดิมพันกีฬา 
การเดิมพันครบวงจร </font></td>
        </tr>
        <tr>
          <td valign="top"><img src="{{url()}}/affiliate/images/369bet_img/bb.png" width="16" height="16" /></td>
          <td><font color="#333333" size="2" face="Tahoma">รับการสนับสนุนอย่างเต็มที่ จากผู้จัดการพันธมิตรโดยตรง</font></td>
        </tr>
      </table>
     </div>
  </div> 
  <div style="float:left; width:300px; padding:15px; border-left:solid 1px #FFFFFF;">
    <div align="left"><strong>ติดต่อเราได้</strong></div>
    <br />
    <table width="100%" border="0">
      <tr>
        <td width="18%"><img src="{{url()}}/affiliate/images/369bet_img/ico_phone.png" width="45" height="45" /></td>
        <td width="82%"><span style="color: rgb(0, 0, 0); font-family: Calibri, Arial, Helvetica, sans-serif; font-size: 16px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); display: inline !important; float: none;">088-369-9991 , 088-369-9992</span></td>
      </tr>
      <tr>
        <td><img src="{{url()}}/affiliate/images/369bet_img/ico_mail.png" width="45" height="45" /></td>
        <td><font color="#333333" size="3" face="Tahoma">Th369bet@outlook.com</font></td>
      </tr>
      <tr>
        <td><img src="{{url()}}/affiliate/images/369bet_img/ico_line.png" width="45" height="45" /></td>
        <td><font color="#333333" size="3" face="Tahoma">@369bet</font></td>
      </tr>
    </table>
  </div>  
  
  </div>
  
<div style="width:1036px; margin:auto; padding-top:15px;"></div>
     
</div>
						
</body>
</html>
