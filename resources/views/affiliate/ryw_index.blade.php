<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Affiliate - Royalewin</title>

    <link href="{{ asset('/royalewin/affiliate/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/royalewin/affiliate/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/royalewin/affiliate/css/prettyPhoto.css') }}" rel="stylesheet">
    <link href="{{ asset('/royalewin/affiliate/css/main.css') }}" rel="stylesheet">

    <!--[if lt IE 9]>
    <script src="{{ asset('/royalewin/affiliate/js/html5shiv.js') }}"></script>
    <script src="{{ asset('/royalewin/affiliate/js/respond.min.js') }}"></script>
    <![endif]-->

    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{ asset('/royalewin/affiliate/images/ico/apple-touch-icon-144-precomposed.png') }}">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ asset('/royalewin/affiliate/images/ico/apple-touch-icon-114-precomposed.png') }}">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ asset('/royalewin/affiliate/images/ico/apple-touch-icon-72-precomposed.png') }}">
    <link rel="apple-touch-icon-precomposed" href="{{ asset('/royalewin/affiliate/images/ico/apple-touch-icon-57-precomposed.png') }}">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700,800" rel="stylesheet">
	<script>
@if (session('message'))
   alert('{{ session('message') }}');
@endif
	</script>
</head><!--/head-->

<body data-spy="scroll" data-target="#navbar" data-offset="0">
<!--.login popup-->
<div id="loginBx" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <form method="post" action="login_process" accept-charset="UTF-8" autocomplete="off">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Please Login</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-3">
                            <label>Username</label>
                        </div>
                        <div class="col-sm-9">
                            <input type="text" name="username">
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-sm-3">
                            <label>Password</label>
                        </div>
                        <div class="col-sm-9">
                            <input type="password" name="password">
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-sm-3">
                            <label>Code</label>
                        </div>
                        <div class="col-sm-4">
                            <input type="text" class="code_fill" name="code"/>
                        </div>
                        <div class="col-sm-4">
                            <img src="captcha" width="50" height="22"/>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-lg btn-primary">Login</button>
                </div>
            </form>
        </div>

    </div>
</div>
<!--/.login popup-->
<header id="header" role="banner">
    <div class="container">
        <div id="navbar" class="navbar navbar-default">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{ url() }}"></a>
            </div>
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="#main-slider"><i class="icon-home"></i></a></li>
                    <li><a href="#pricing">Commission Plan</a></li>
                    <li><a href="#reason">Reason To Join</a></li>
                    <li><a href="#services">How It Works</a></li>
                    <li><a href="#portfolio">Our Products</a></li>

                    <li><a href="#faq">FAQ</a></li>
                    <li class="sp1"><a href="#contact" id="goto_register"><i class="icon-user ab4"></i>Join Now</a></li>
                    <li><a data-toggle="modal" data-target="#loginBx" href="#"><i class="icon-check ab4"></i>Login</a></li>
                </ul>
            </div>
        </div>
    </div>
</header><!--/#header-->

<section id="main-slider" class="carousel">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="txtHead1 tp01">JOIN THE HIGHEST REWARDING</div>
                <div class="txtHead1">AFFILIATE PROGRAM </div>
                <div class="txtHead3">EARN UP TO 50% LIFETIME COMMISSION!</div>
                <p class="tp02">The most profitable way to earn money in the industry</p>
                <p class=""><button id="btn_register" type="button" onclick="$('#goto_register').click();" class="btn btn-danger btn-lg txtHead3">JOIN NOW FOR FREE!</button></p>
            </div>

            <div class="col-md-4 text-center">
                <img src="{{ asset('/royalewin/affiliate/images/offer.png') }}" width="280">
            </div>

        </div>
    </div>

</section><!--/#main-slider-->

<section id="pricing">
    <div class="container">
        <div class="box">
            <div class="center" style="display: none;">
                <h2>Welcome to Royalewin Affiliate Program</h2>
                <p class="lead">Royalewin Affiliate Program is providing our affiliate member the most attractive profit sharing structure. All Royalewin Affiliate member can enjoy up to 45% of commissions.</p>
            </div><!--/.center-->
            <div class="center" style="display: none;">
                <h2 style="margin-top: 0px;">Why Royalewin Affiliate Program</h2>
                <p class="lead">
                <ul>
                    <li>We are providing World-Class Gaming Product: Sportsbook, Live Casino, 4D, Slot game,Poker and RNG Slot Games etc..</li>
                    <li>Our effective market analysis and marketing strategy which guaranteed high members conversion and retention rate.</li>
                    <li>Highest Commission Given in the Field.</li>
                    <li>Commission Rate Up to 45%</li>
                    <li>We are providing user-friendly software consists of online reporting of your affiliate performance.</li>
                    <li>We have a well-trained affiliate team who will full-filled your requirement.</li>

                </ul>

                </p>
            </div><!--/.center-->

            <div id="pricing-table" class="row">
                <div class="center" style="display: none;">
                    <h2>Royalewin Commission Structure <span>( Sole Discretion of Royalewin )</span></h2>
                    <p class="lead">You can receive 25% to 45% of your affiliate member's net losses as your affiliate commission in Royalewin affiliate program.</p>
                </div>
                <div class="gap"></div>
                <div class="col-sm-4">
                    <ul class="plan">
                        <li class="plan-name">Member Affiliate's Net Losses
                            <span>
                                ( First Day to Last Day of the Month )<br>
                                (MYR or same Currency)
                            </span>
                        </li>
                        <li>1. MYR 30 to MYR 20,000 </li>
                        <li>2. Next MYR20.001 to MYR 200,000 </li>
                        <li>3. Next MYR 200,001 and above </li>

                    </ul>
                </div><!--/.col-sm-4-->
                <div class="col-sm-4">
                    <ul class="plan">
                        <li class="plan-name">Monthly Active Member
                            <span class="tp03">(At Least 1 Active Wager)</span>
                        </li>
                        <li>>5</li>
                        <li>>5</li>
                        <li>>5</li>
                    </ul>
                </div><!--/.col-sm-4-->
                <div class="col-sm-4">
                    <ul class="plan">
                        <li class="plan-name tp04">Commision Rate</li>
                        <li>25%</li>
                        <li>35%</li>
                        <li>50%</li>
                    </ul>
                </div><!--/.col-sm-4-->

            </div>
            <div class="row center">
                <p class="lead"><em>(Minimum Payout = MYR 300.00)</em></p>
            </div>
            <div>
                <p class="ab1">Above is the Royalewin Affiliate Program's Commission Rate Table. ( Commission Rate is based on member's net losses. )</p>
                <p>Example 1: If Company net profit ( Member net losses) is MYR 20,000, affiliate commission rate will be 25%. (shown on the table above).</p>
                <p>Example 2: If Company net profit ( Member net losses) is MYR 60,000, affiliate commission rate will be 25% of the first MYR 20,000 of Company net profit and 35% of the next MYR 40,000 of Company net profit.(shown on the table above) </p>

                <p>Example 3: If Company net profit ( Member net losses) is MYR 300,000, affiliate commission rate will be 25% of the first MYR 20,000 of Company net profit, 35% of the next MYR 200,000 of Company net profit and 50% of the next MYR 80,000 of company net profit.(shown on the table above) </p>
                <div class="ab2">
                    <ul>
                        <li>With a 50% commission rate, 50% of members' net losses is your commission. Increase in members' net losses and active members will enable you to earn more.</li>
                        <li>With at least 6 active members (members that have placed at one bet in the month) will entitle you to start earning commission.</li>
                        <li>We monitor our affiliates' performance and may contact you directly to increase your commission rate.</li>
                        <li>Negative revenue for any given month will be carried forward to the following month.</li>
                        <li>918 kiss slot and 4D product not included at the profit and loss statement calculation.</li>
                        <li>All affiliates are subjected to administrative and marketing costs incurred directly for the acquisition of members.</li>
                        <li>The cost is deducted from the revenue on a monthly basis. These cost may include the following
                            <ol>
                                <li>Payment charges – Deposit and withdrawal charges involving all the affiliate's members.</li>
                                <li>Media Fees – Any fees that incurred towards Royalewin to support or assist affiliate in conjunction with promotions or marketing purposes.</li>
                                <li>Promotion bonuses – Monetary bonuses or rebates given to the affiliate's members.</li>
                            </ol>
                        </li>
                    </ul>
                </div>
                <p class="ab3">Payout</p>
                <div class="ab2">
                    <ul>
                        <li>Commissions will be automatically deposited into your bank account specified upon registration within next 5 working days after the last day of the month. There will only be one payout per each calendar month. While we offer free payout transactions we will not be held responsible if the receiving financial institute impose a fee of any kind at their end. </li>
                        <li>There is a minimum amount of MYR 300 or equivalent currency for payout to take effect. Any amount below this threshold will be carried forward to the next month. </li>
                    </ul>
                </div>
                <p>Royalewin reserves the right to alter or amend and/or add any of the terms mentioned above as we deemed fit. </p>
                <p>Royalewin reserves the right to cancel the affiliate account at any point of time without prior notice in the event the affiliate's performance does not meet our expected quotas at a given period of time. </p>
            </div>
        </div>
    </div>
</section>

<section id="reason">
    <div class="container">
        <div class="box">
            <div class="center gap" style="margin-bottom: 40px;">
                <h2>REASONS TO JOIN ROYALEWIN AFFILIATE PROGRAM</h2>
            </div><!--/.center-->
            <div class="row gap1">
                <div class="col-md-3 col-sm-6 reas text-center">
                    <img src="{{ asset('/royalewin/affiliate/images/reason-1.png') }}">
                    <span class="reasH">ANONYMITY</span>
                    <span class="reasP">Remain anonymous, having no need to share personal data to confirm your identiy</span>
                </div>
                <div class="col-md-3 col-sm-6 reas text-center">
                    <img src="{{ asset('/royalewin/affiliate/images/reason-2.png') }}">
                    <span class="reasH">HD GAMES</span>
                    <span class="reasP">Over 1000 HD casino games your player will fall in love with</span>
                </div>
                <div class="col-md-3 col-sm-6 reas text-center">
                    <img src="{{ asset('/royalewin/affiliate/images/reason-3.png') }}">
                    <span class="reasH">LIVE DEALERS</span>
                    <span class="reasP">Real casino environment via 24/7 video-stream with live sexy dealers</span>
                </div>
                <div class="col-md-3 col-sm-6 reas text-center">
                    <img src="{{ asset('/royalewin/affiliate/images/reason-4.png') }}">
                    <span class="reasH">UP TO 50%</span>
                    <span class="reasP">Generous payout structure let you earn up to 50% livetime commission on every player</span>
                </div>

                <div class="gap1"></div>
            </div>


        </div><!--/.box-->
    </div><!--/.container-->
</section>

<section id="earn">
    <div class="container">
        <div class="box">
            <div class="center">
                <h2>EARN LIFETIME BY PROMOTING ROYALEWIN </h2>
            </div>
            <div class="center tp05">
                <img src="{{ asset('/royalewin/affiliate/images/bg-4.png') }}">
                <div class="txtRight">
                    <span class="tp06"><i class="icon-check tp08"></i>Remain anonymous</span>
                    <span class="tp07"><i class="icon-check tp08"></i>IT`S FREE TO PARTICIPATE</span>
                </div>
                <div class="txtLeft">
                    <span class="tp010">Up to 50% Commission per player<i class="icon-check tp09"></i></span>
                    <span class="tp011">Play from any place<i class="icon-check tp09"></i></span>
                </div>
            </div>
        </div>
    </div>
</section>


<section id="services">
    <div class="container">
        <div class="box">
            <div class="center gap" style="margin-bottom: 40px;">
                <h2>HOW IT WORKS?</h2>
            </div>
            <div class="row">
                <div class="col-md-4 col-sm-6">
                    <div class="center">
                        <i class="icon-edit icon-md icon-color1"></i>
                        <h4>Sign Up</h4>
                        <p>It`s free and takes
                            just 30 seconds to
                            register</p>
                    </div>
                </div><!--/.col-md-4-->
                <!--/.col-md-4-->
                <div class="col-md-4 col-sm-6">
                    <div class="center">
                        <i class="icon-bullhorn icon-md icon-color3"></i>
                        <h4>Promote</h4>
                        <p>Put the referral link on your
                            Sms,website, Facebook page, wechat,whatsapp or send it via e-mail</p>
                    </div>
                </div><!--/.col-md-4-->
                <div class="col-md-4 col-sm-6">
                    <div class="center">
                        <i class="icon-money icon-md icon-color4"></i>
                        <h4>Get Paid</h4>
                        <p>Earn Revenue Share from your visitors </p>
                    </div>
                </div><!--/.col-md-4-->

            </div><!--/.row-->
        </div><!--/.box-->
    </div><!--/.container-->
</section><!--/#services-->

<section id="portfolio">
    <div class="container">
        <div class="box">
            <div class="center gap">
                <h2>Our Products</h2>
                <p class="lead">Why would your members bet anywhere else? We have everything you need for your members to play with us.</p>
            </div><!--/.center-->
            <!--/#portfolio-filter-->
            <ul class="portfolio-items col-4">
                <li class="portfolio-item apps">
                    <div class="item-inner">
                        <div class="portfolio-image">
                            <img src="{{ asset('/royalewin/affiliate/images/portfolio/thumb/item1.jpg') }}" alt="">
                            <div class="overlay">
                                <a class="preview btn btn-danger" title="Sports" href="{{ asset('/royalewin/affiliate/images/portfolio/full/item1.jpg') }}"><i class="icon-eye-open"></i></a>
                            </div>
                        </div>
                        <h5>Sports</h5>
                    </div>
                </li><!--/.portfolio-item-->
                <li class="portfolio-item joomla bootstrap">
                    <div class="item-inner">
                        <div class="portfolio-image">
                            <img src="{{ asset('/royalewin/affiliate/images/portfolio/thumb/item2.jpg') }}" alt="">
                            <div class="overlay">
                                <a class="preview btn btn-danger" title="Live Casino" href="{{ asset('/royalewin/affiliate/images/portfolio/full/item2.jpg') }}"><i class="icon-eye-open"></i></a>
                            </div>
                        </div>
                        <h5>Live Casino</h5>
                    </div>
                </li><!--/.portfolio-item-->
                <li class="portfolio-item bootstrap wordpress">
                    <div class="item-inner">
                        <div class="portfolio-image">
                            <img src="{{ asset('/royalewin/affiliate/images/portfolio/thumb/item3.jpg') }}" alt="">
                            <div class="overlay">
                                <a class="preview btn btn-danger" title="Poker" href="{{ asset('/royalewin/affiliate/images/portfolio/full/item3.jpg') }}"><i class="icon-eye-open"></i></a>
                            </div>
                        </div>
                        <h5>Poker</h5>
                    </div>
                </li><!--/.portfolio-item-->
                <li class="portfolio-item joomla wordpress apps">
                    <div class="item-inner">
                        <div class="portfolio-image">
                            <img src="{{ asset('/royalewin/affiliate/images/portfolio/thumb/item4.jpg') }}" alt="">
                            <div class="overlay">
                                <a class="preview btn btn-danger" title="Slot" href="{{ asset('/royalewin/affiliate/images/portfolio/full/item4.jpg') }}"><i class="icon-eye-open"></i></a>
                            </div>
                        </div>
                        <h5>Slot</h5>
                    </div>
                </li><!--/.portfolio-item-->
                <li class="portfolio-item joomla html">
                    <div class="item-inner">
                        <div class="portfolio-image">
                            <img src="{{ asset('/royalewin/affiliate/images/portfolio/thumb/item5.jpg') }}" alt="">
                            <div class="overlay">
                                <a class="preview btn btn-danger" title="Mobile Slot" href="{{ asset('/royalewin/affiliate/images/portfolio/full/item5.jpg') }}"><i class="icon-eye-open"></i></a>
                            </div>
                        </div>
                        <h5>Mobile Slot</h5>
                    </div>
                </li><!--/.portfolio-item-->
                <li class="portfolio-item wordpress html">
                    <div class="item-inner">
                        <div class="portfolio-image">
                            <img src="{{ asset('/royalewin/affiliate/images/portfolio/thumb/item6.jpg') }}" alt="">
                            <div class="overlay">
                                <a class="preview btn btn-danger" title="Racebook" href="{{ asset('/royalewin/affiliate/images/portfolio/full/item6.jpg') }}"><i class="icon-eye-open"></i></a>
                            </div>
                        </div>
                        <h5>Racebook</h5>
                    </div>
                </li><!--/.portfolio-item-->
                <li class="portfolio-item joomla html">
                    <div class="item-inner">
                        <div class="portfolio-image">
                            <img src="{{ asset('/royalewin/affiliate/images/portfolio/thumb/item7.jpg') }}" alt="">
                            <div class="overlay">
                                <a class="preview btn btn-danger" title="4D" href="{{ asset('/royalewin/affiliate/images/portfolio/full/item7.jpg') }}"><i class="icon-eye-open"></i></a>
                            </div>
                        </div>
                        <h5>4D</h5>
                    </div>
                </li><!--/.portfolio-item-->
                <!--/.portfolio-item-->
            </ul>
        </div><!--/.box-->
    </div><!--/.container-->
</section><!--/#portfolio-->


<section id="faq">
    <div class="container">
        <div class="box">
            <div class="gap">
                <h2 class="tit1">FAQ</h2>
            </div><!--/.center-->
            <!--/#portfolio-filter-->
            <p class="ab1 ud">Royalewin Affiliate Program</p>

            <ol>
                <li><span style="font-weight: bold;color:#D4B35D;">What is the Royalewin Affiliate Program?</span>
                    <p>The Affiliate Program is a Partnership Program which allows you to receive commission on every player you referred to Royalewin based on the player's wagering activity over the course of the player's account life.</p>
                </li>
                <li><span style="font-weight: bold;color:#D4B35D;">Is there a set up fee?</span>
                    <p>To sign up in Royalewin Affiliate Program is absolutely free!</p>
                </li>
                <li><span style="font-weight: bold;color:#D4B35D;">How can I register with the affiliate program?</span>
                    <p>To join our Affiliate Program, simply fill out the Registration Form found in our website by accessing this: link and within a few days you will be received an email by our Affiliate Program Manager regarding your acceptance into the program. For the meantime, please read through our terms and conditions by accessing this link. </p>
                </li>
                <li><span style="font-weight: bold;color:#D4B35D;">Can the affiliate himself also be a player?</span>
                    <p>Affiliates will not be allowed to place wagers using an Affiliate account. An affiliate needs to register and use his/her player account in able to place wagers.</p>
                </li>
                <li><span style="font-weight: bold;color:#D4B35D;">Can an affiliate have more than one account? </span>
                    <p>Affiliate shall not have more than one affiliate account, nor shall an Affiliate be allowed to earn commissions on their own or through any related parties.</p>
                </li>
                <li><span style="font-weight: bold;color:#D4B35D;">What if i don't have a website, can i still join?</span>
                    <p>To participate in the Royalewin Affiliate Program, you should have a Website that has online gambling as its central theme. Should you not have a website, Royalewin Affiliate will however review cases on an individual basis. Please email affiliate@royalewin.com for more information..</p>
                </li>
                <li><span style="font-weight: bold;color:#D4B35D;">If I have multiple websites, do i need to create multiple affiliate accounts?</span>
                    <p>If you have multiple websites, you only need to apply once to Royalewin Affiliates and use the same affiliate ID for multiple sites. You can also set up separate tracking codes for each site, so you compare how well each site performs. There is no need to create another account for every website created, since Affiliates are not allowed to have multiple accounts.</p>
                </li>
                <li><span style="font-weight: bold;color:#D4B35D;">What is the percentage of profit that I get paid?</span>
                    <p>Royalewin offers one of the best Affiliate Program commissions available in the Internet Gaming industry today. Our affiliates can enjoy up to a 50% net profit. Royalewin guarantees you one of the best conversion ratios on the web today. </p>
                </li>
                <li><span style="font-weight: bold;color:#D4B35D;">Do you carry over negative balances?</span>
                    <p>Yes, when you incur a negative balance, it will be carried over to the next month. For example if for the month of January your affiliate account ends with -MYR1000, this amount will be carried to February. If at the end of February your affiliate account earns MYR3000, the total balance will be MYR2000 (MYR3000 deduct -MYR1000). However if your account is still making a loss at the end of February, for example -MYR500, then it will be added to the previous negative and be carried forward to March resulting in your March balance brought forward to be -MYR1500. The commission earned shall then be paid according to the scale.</p>
                </li>
                <li><span style="font-weight: bold;color:#D4B35D;">How can I check how much I have earned?</span>
                    <p>We provide online stats for you 24 hours a day, 7 days a week and 365 days a year. Login with your user name and password to our secure affiliate stats interface page to see how much you've earned and other relevant stats.</p>
                </li>
                <li><span style="font-weight: bold;color:#D4B35D;">What are my responsibilities as an affiliate?</span>
                    <p>As an Affiliate, you are responsible for promoting Royalewin by implementing the advertising, banners and tracking URL's on your websites, e-mails or other communications.</p>
                </li>
            </ol>
            <p class="ab1 ud">Payout</p>
            <ol>
                <li><span style="font-weight: bold;color:#D4B35D;">How and when do I get paid?</span>
                    <p>We pay our affiliates on the 15th of every month. Depending on the amount due that month, we will send you the money through bank transfer. </p>
                </li>
                <li><span style="font-weight: bold;color:#D4B35D;">What are your payment methods?</span>
                    <p>The commissions will be paid by bank transfer as specified by the Affiliate's.</p>
                </li>
                <li><span style="font-weight: bold;color:#D4B35D;">What is the conversion rate being used if I use a different currency other than MYR?</span>
                    <p>We usually pay our affiliate commission using MYR. For other currencies, respective exchange rate on the payout date will be applied.</p>
                </li>
            </ol>
            <p class="ab1 ud">Reporting</p>
            <ol>
                <li><span style="font-weight: bold;color:#D4B35D;">How can I check my account statistics?</span>
                    <p>You can login to this page by entering your username and password to check your account details: affiliate.myroyalewin.com</p>
                </li>
                <li><span style="font-weight: bold;color:#D4B35D;">What information is displayed on my stats page? </span>
                    <p>Royalewin Affiliate system displays a very detailed statistic analysis for affiliates in real time, anytime. You will be able to view the exact number of Impressions, Signups of Real accounts, and of course, your share of the profit. </p>
                </li>
                <li><span style="font-weight: bold;color:#D4B35D;">I have checked the online stats for a while, but they haven't changed.</span>
                    <p>The database serving the online stats interface is updated once every few hours. The date and time it was last updated is displayed at the top of the stats page.</p>
                </li>
            </ol>
            <p class="ab1 ud">Account Information</p>
            <ol>
                <li><span style="font-weight: bold;color:#D4B35D;">Can I edit my account information? How can I edit it?</span>
                    <p>You can't edit your username, affiliate ID and email address. We only support alphanumeric characters for usernames and passwords. Usernames must have at least six (6) alphanumeric characters.</p>
                </li>
                <li><span style="font-weight: bold;color:#D4B35D;">I do not have a website, what should I put in the website URL?</span>
                    <p>If you do not have a website, please put it blank. </p>
                </li>
                <li><span style="font-weight: bold;color:#D4B35D;">What to do if I forget my login details? </span>
                    <p>Please contact our customer support or please email to affiliate@royalewin.com</p>
                </li>
            </ol>
            <p class="ab1 ud">Marketing</p>
            <ol>
                <li><span style="font-weight: bold;color:#D4B35D;">How does the tracking link work?</span>
                    <p>We use your affiliate ID for your tracking link so we can trace the players joining from your website.</p>
                    <p>Example: //Royalewin.com/my?utm_source=affiliate&amp;refcode=1234567</p>
                </li>
            </ol>
        </div><!--/.box-->
    </div><!--/.container-->
</section>

<!--/#about-us-->
<section id="contact">
    <div class="container">
        <div class="box last">
            <div class="row">
                <div class="col-sm-12">
                    <h1>Join Now</h1>
                    <p>Kindly fill up the form below and our Affiliate Program Manager will contact you as soon as possible</p>
                    <div class="status alert alert-success" style="display: none"></div>
                    <form id="main-contact-form" class="contact-form" name="contact-form" method="post" action="#" role="form">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="text" name="username" class="form-control" placeholder="Username" required="required">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="text"  id="fullname" name="name" class="form-control" placeholder="Full Name" required="required">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="password" name="password" class="form-control" placeholder="Password" required="required">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="password" name="confirm" class="form-control" placeholder="Re-type Password" required="required">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="text" id="email" name="email" class="form-control" placeholder="Email" required="required">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="text" id="mobile" name="telephone" class="form-control" placeholder="Contact Number" required="required">
                                </div>
                            </div>
                        </div>
                        {{--<div class="row">--}}
                            {{--<div class="col-sm-12">--}}
                                {{--<div class="form-group">--}}
                                    {{--<input type="text" name="website" class="form-control" placeholder="Website" required="required">--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        <div class="row">
                            <div class="col-sm-12">
                                {{--<div class="form-group">--}}
                                    {{--<textarea name="websitedesc" id="message" class="form-control" rows="8" placeholder="Website Description" required="required"></textarea>--}}
                                {{--</div>--}}
                                <div class="form-group">
                                    <button id="btn_register" type="button" onclick="register(this);" class="btn btn-danger btn-lg">Register Now</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="row">
                        <div class="col-sm-12">
                            or Email us at <a href="mailto:affiliate@royalewin.com">affiliate@royalewin.com</a>
                        </div>
                    </div>
                </div><!--/.col-sm-6-->
                <!--/.col-sm-6-->
            </div><!--/.row-->
        </div><!--/.box-->
    </div><!--/.container-->
</section><!--/#contact-->

<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                &copy; 2017 <a target="_blank" href="#" title="affiliate royalewin">Royalewin</a>. All Rights Reserved.
            </div>
            <div class="col-sm-6">
                <img class="pull-right" src="{{ asset('/royalewin/affiliate/images/footer_icon.png') }}" alt="ShapeBootstrap" title="ShapeBootstrap">
            </div>
        </div>
    </div>
</footer><!--/#footer-->

<script src="{{ asset('/royalewin/affiliate/js/jquery.js') }}"></script>
<script src="{{ asset('/royalewin/affiliate/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('/royalewin/affiliate/js/jquery.isotope.min.js') }}"></script>
<script src="{{ asset('/royalewin/affiliate/js/jquery.prettyPhoto.js') }}"></script>
<script src="{{ asset('/royalewin/affiliate/js/main.js') }}"></script>

<script type="text/javascript">
    $(function () {
        var goto = "{{ isset($goto) ? $goto : '' }}";

        if (goto != "") {
            $("#goto_" + goto).click();
        }

        auto_fillin();
    });

    function register(btn){

        var btnRegObj = $(btn);
        var btnRegOriText = btnRegObj.text();

        btnRegObj.prop("readonly", true).prop("disabled", true).text("Loading...");

        var form = $("#main-contact-form");

        $.ajax({
            type: "POST",
            url: "{{route('register_process')}}",
            data: form.serialize()
        }).done(function( json ) {

            btnRegObj.prop("readonly", false).prop("disabled", false).text(btnRegOriText);

            obj = JSON.parse(json);
            var str = '';
            $.each(obj, function(i, item) {
                str += item + '\n';
            });

            if( "Successful\n" == str ){
                alert('Successful Registered. Please check your email for the application status. Thank You!');
                $("input", form).val("");
            }else{
                alert(str);
            }
        });
    }
	
	function auto_fillin(){
		var fullname = getQueryStringValue('fullname');
		var email = getQueryStringValue('email');
		var mobile = getQueryStringValue('mobile');
		$('#fullname').val(fullname);
		$('#email').val(email);
		$('#mobile').val(mobile);

		if (fullname.length > 0) {
		    setTimeout(function () {
                $("#goto_register").click();
            }, 1000);
        }
	}
	function getQueryStringValue (key) {  
	  return decodeURIComponent(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + encodeURIComponent(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));  
	} 
</script>

</body>
</html>