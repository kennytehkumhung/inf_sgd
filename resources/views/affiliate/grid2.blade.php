
<!DOCTYPE html>
<html lang="en">
<head>
    <title id='Description'>This demo illustrates the basic functionality of the Grid plugin. The affiliate/jqwidgets Grid plugin offers rich support for interacting with data, including paging, grouping and sorting. 
    </title>
    <link rel="stylesheet" href="affiliate/jqwidgets/styles/jqx.base.css" type="text/css" />
    <link rel="stylesheet" href="affiliate/jqwidgets/styles/jqx.bootstrap.css" type="text/css" />
    <script type="text/javascript" src="affiliate/jqwidgets/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="affiliate/jqwidgets/jqx-all.js"></script>

    <script type="text/javascript">
		
		function doGridSearch(id,type,value){
			if(type == 'search')
			{
				$("#"+id).val(value);
			}
			else if(type == 'dropdown')
			{
				$("#"+id).jqxDropDownList('selectItem', value ); 
			
			}
		}
		
		function doSearch(){
			search();
		}

		function search(){

			var aflt = new Array();
			var aqfld = new Array();
			var temp = new Array();
			var params = new Array();
			
			
			var paginginformation = $('#{{$grid['id']}}').jqxGrid('getpaginginformation');
		
			params['aflt'] = aflt;
			params['aqfld'] = aqfld;
			params['createdfrom'] = $('#dateFrom').jqxDateTimeInput('getText'); 
			params['createdto']   = $('#dateTo').jqxDateTimeInput('getText'); 
			params['igd']   = paginginformation.pagenum; 
			params['ilt']   = paginginformation.pagesize; 
			params['ist']   = paginginformation.pagescount; 
			
			$('#{{$grid['id']}}').jqxGrid('clear');
			
			@if( isset($grid['filters']))
					@foreach( $grid['filters'] as $filter )
							var temp = $("#{{$filter['name']}}").jqxDropDownList('getSelectedItem');
							if( temp != null ){
								var	selected_dropdown_{{$filter['name']}} = temp.value;
							}else{
								var	selected_dropdown_{{$filter['name']}} = 0;
							}
					@endforeach				
			@endif
	
			var source =
			{
				datatype: "json",
				datafields: [
					@foreach( $grid['columns'] as $column)
							 { name: '{{$column['index']}}' },
					@endforeach	
				],
				url: "{{$grid['action']}}",
				data: {
					@if( isset($grid['options']['params'] ) )
						@foreach( $grid['options']['params'] as $key => $value )
						{{$key}}: '{{$value}}',
						@endforeach	
					@endif
					igd: params['igd'],
					limit: params['ilt'],
					start: params['ist'],
					createdfrom: params['createdfrom'],
					createdto: params['createdto'],
					aflt:
					{
						@if( isset($grid['searches']) )
							@foreach( $grid['searches'] as $search )
								@foreach( $search['options'] as $key => $option )
									@if(!is_array($option))
										{{$key}} : $('#{{$key}}').jqxInput('val'),
									@endif
								@endforeach			
							@endforeach
						@endif
					},
					aqfld:
					{
						@if( isset($grid['filters']))
							@foreach( $grid['filters'] as $filter )
									{{$filter['name']}} : selected_dropdown_{{$filter['name']}}, 
							@endforeach				
						@endif
					},
						
				}
			
				
			};
			
            var dataAdapter = new $.jqx.dataAdapter(source, {
                downloadComplete: function (data, status, xhr) { },
                loadComplete: function (data) { },
                loadError: function (xhr, status, error) { }
            });
			
            $("#{{$grid['id']}}").jqxGrid(
            {
                source: dataAdapter,                
            });

		}
		
        $(document).ready(function () {
            // prepare the data
			var source_initial =
			{
				datatype: "json",
				datafields: [
					@foreach( $grid['columns'] as $column)
							 { name: '{{$column['index']}}' },
					@endforeach	
				],
				url: "{{$grid['action']}}",
				beforeprocessing: function (data) {
                    source_initial.totalrecords = data.total;
                },
				data: {
					@if( isset($grid['options']['params']) )
						@foreach( $grid['options']['params'] as $key => $value )
						{{$key}}: '{{$value}}',
						@endforeach	
					@endif
					limit: 20,
					start: 0,
					aflt:
					{
						@if( isset($grid['searches']) )
							@foreach( $grid['searches'] as $search )
								@foreach( $search['options'] as $key => $option )
									@if(!is_array($option) && isset($search['params']['value']))
											{{$key}} : '{{$search['params']['value']}}',								
									@endif
								@endforeach	
							@endforeach
						@endif
					}
					
				}
			};
	

            var cellsrenderer = function (row, columnfield, value, defaulthtml, columnproperties, rowdata) {
                if (value < 20) {
                    return '<span style="margin: 4px; float: ' + columnproperties.cellsalign + '; color: #ff0000;">' + value + '</span>';
                }
                else {
                    return '<span style="margin: 4px; float: ' + columnproperties.cellsalign + '; color: #008000;">' + value + '</span>';
                }
            }

            var dataAdapter = new $.jqx.dataAdapter(source_initial, {
                downloadComplete: function (data, status, xhr) { },
                loadComplete: function (data) { },
                loadError: function (xhr, status, error) { }
            });

            // initialize jqxGrid
            $("#{{$grid['id']}}").jqxGrid(
            {
                width: 1500,
                source: dataAdapter,                
                pageable: true,
                autoheight: true,
                sortable: true,
				pageable: true,
                altrows: true,
				theme:'bootstrap',
                enabletooltips: true,
                editable: false,
				pagesize: 20,
				pagesizeoptions: ['20', '50', '100', '150', '200'],
                selectionmode: 'multiplecellsadvanced',
				virtualmode: true,
				rendergridrows: function(obj)
				{
					  return obj.data;     
				},
                columns: [
				  @foreach( $grid['columns'] as $column)
						 { text: '{{$column['name']}}', datafield: '{{$column['index']}}' @if( isset($column['options']['align'])), cellsalign: '{{$column['options']['align']}}',align: '{{$column['options']['align']}}'@endif ,  width: {{$column['width']}} },
				  @endforeach	
                ]
            });
			
			$("#dateFrom, #dateTo").jqxDateTimeInput({ formatString: "F", width: '120px' , height: '25px' , formatString: "yyyy-MM-dd" ,openDelay: 0,closeDelay: 0});
			@if( isset($grid['searches']) )
				@foreach( $grid['searches'] as $search )
					@foreach( $search['options'] as $key => $option )
						@if(!is_array($option))
								$("#{{$key}}").jqxInput({placeHolder: "{{$option}}", height: 25, width: 200, minLength: 1 });
						@endif
					@endforeach			
				@endforeach
			@endif
			
	
			@if( isset($grid['filters']) )
				@foreach( $grid['filters'] as $filter )
					var {{$filter['name']}}_data = [];
			
			
					@foreach( $filter['options'] as $key => $value )
							{{$filter['name']}}_data.push({ id: "{{$key}}", title: "{{$value}}" });
							
					@endforeach
					   var source =
						   {
							   localdata: {{$filter['name']}}_data,
							   datatype: "array",
							   datafields: [
								   { name: 'id' },
								   { name: 'title' }
							   ],
						   };
						   
						    var dataAdapter = new $.jqx.dataAdapter(source);
					
					$("#{{$filter['name']}}").jqxDropDownList({ source: dataAdapter,  width: '200', height: '25',filterDelay:0,placeHolder: "Select:",openDelay: 0,closeDelay: 0,valueMember: 'id',displayMember: 'title'});
				@endforeach
			@endif
			@if( isset($grid['buttons'][1]) )
				@foreach( $grid['buttons'][1] as $button )
					$("#{{$button['name']}}").jqxButton({ template: "primary" });
				@endforeach
			@endif

			$("#jqxSubmitButton").jqxButton({ width: '150', template: "success"});
			$("#btn_excel").jqxButton({ width: '80', template: "success"});
			$("#jqxSubmitButton").on('click', function () {
                    $("#events").find('span').remove();
                    $("#events").append('<span>Submit Button Clicked</span>');
             });
			 

			
        });
		
		

		function openParentTab(title, url) {
			window.opener.addTab(title, url);
		} 
		
		@if( $grid['action'] == 'instant-data' )
			setInterval(doSearch, 10000);
		@endif
		
		var popup_created = false;
		
		function doConfirmAction(url,status,id){
			if( popup_created == false){
				$('#ok').attr( 'onClick' , 'doConfirmActionProcess(\''+url+'\',\''+status+'\',\''+id+'\')' );
				createElements();
			}
		     $('#eventWindow').jqxWindow('open');
		}	
		
		function doConfirmActionProcess(url,status,id){
			
			$.ajax({
				 type: "POST",
				 url:  url,
				 data: {
					_token: "{{ csrf_token() }}",
					action: status,
					id: id,
				 },
				 beforeSend: function(){
					
				 },
				 success: function(json){
						obj = JSON.parse(json);
						 var str = '';
						 $.each(obj, function(i, item) {
							
							if( '{{Lang::get('COMMON.SUCESSFUL')}}' == item ){
								alert('{{Lang::get('COMMON.SUCESSFUL')}}');
								doSearch();
							
							}else{
								str += item + '<br>';
							}
						})
						
					
				}
			})
		}
		
        function createElements() {
			popup_created = true;
			$('#eventWindow').show();
            $('#eventWindow').jqxWindow({
                maxHeight: 150, maxWidth: 280, minHeight: 30, minWidth: 250, height: 145, width: 270,
                resizable: false, isModal: true, modalOpacity: 0.1,
                okButton: $('#ok'), cancelButton: $('#cancel'),
                initContent: function () {
                    $('#ok').jqxButton({ width: '65px' });
                    $('#cancel').jqxButton({ width: '65px' });
                    $('#ok').focus();
                }
            });
            $('#events').jqxPanel({ height: '250px', width: '450px' });
            $('#showWindowButton').jqxButton({ width: '100px' });
			
        }
		
		function export_excel(){
			
			$("#{{$grid['id']}}").jqxGrid('exportdata', 'xls', 'jqxGrid');  
		}
		
		

    </script>
	<style>
	.fl{
		float: left;
	}
	.label-margin{
		padding-top:5px;
		margin-left: 10px;
		margin-right: 5px;
	}
	.clear{
		clear: both;
	}
	</style>
</head>
<body class='default'>
    <div id='jqxWidget' style="font-size: 13px; font-family: Verdana; float: left;margin-left:10px;margin-top:10px;">
		
		@if( isset($grid['buttons'][1]) )
			@foreach( $grid['buttons'][1] as $button )
			<div>
				<input style='cursor: pointer;' onClick="parent.addTab('{{$button['value']}}','{{$button['params']['url']}}')" type="button" value="{{$button['name']}}" id="{{$button['name']}}" />
			</div>
			@endforeach
		@endif
			<!--
			<div>
				<input style='cursor: pointer;' onClick="export_excel()" type="button" value="Excel" id="btn_excel" />
			</div>
			-->

		
		@if( isset($grid['searches']) )
			@foreach( $grid['searches'] as $search )
				@foreach( $search['options'] as $key => $option )
					@if(!is_array($option))
							<label class="fl label-margin">{{$option}}</label>
							<input type="text" id="{{$key}}" class="fl" value="@if(isset($search['params']['value'])){{$search['params']['value']}}@endif"/>	
					@endif
				@endforeach			
			@endforeach
			@if( count($grid['searches']) > 0)
				<br>
				<br>
			@endif
		@endif
	
		@if( isset($grid['filters']) )
			@foreach( $grid['filters'] as $filter )
		
				@if( isset($filter['params']['display']) )
					<label class="fl label-margin">{{$filter['params']['display']}}</label>
				@endif 
				
				<div id="{{$filter['name']}}" class="fl"></div>
			@endforeach
			@if( count($grid['filters']) > 0)
				<br>
				<br>
				<br>
			@endif
		@endif
		
	
		<label class="fl label-margin clear" >Date</label>
		<div id='dateFrom' class="fl">
        </div>
		
		<span class="fl label-margin">~</span>
		
		<div id='dateTo' class="fl">
        </div>
	
	
			
		
		
		<div class="clear">
            <input style='margin-top: 20px;cursor: pointer;' type="submit" value="Search" id='jqxSubmitButton' onClick="search()"/>
        </div>
		

		<br>
		<br>
         <div id="{{$grid['id']}}" >
        </div>
     </div>
	 
	 <div id="eventWindow" style="display:none">
           <div>
               <img width="14" height="14" src="affiliate/jqwidgets/styles/images/help.png" alt="" />
               Confirmation</div>
           <div>
               <div>
					Confirm Process?
               </div>
               <div>
               <div style="float: right; margin-top: 15px;">
                   <input type="button" id="ok" value="OK" style="margin-right: 10px" onClick=""/>
                   <input type="button" id="cancel" value="Cancel" />
               </div>
               </div>
           </div>
    </div>
</body>
</html>
