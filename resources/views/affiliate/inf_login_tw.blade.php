<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<title>Infiniwin Taiwan Affiliate</title>

	<!-- Google Fonts -->
	<link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700|Lato:400,100,300,700,900' rel='stylesheet' type='text/css'>

	<link rel="stylesheet" href="{{ asset('/front/affiliate_tw/css/animate.css') }}">
	<!-- Custom Stylesheet -->
	<link rel="stylesheet" href="{{ asset('/front/affiliate_tw/css/style.css') }}">

	<link rel="shortcut icon" href="{{ asset('/affiliate/v2/images/favicon.ico') }}">

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
</head>

<body>
	<div class="container">
		<div class="top">
			<h1 id="title" class="hidden"><span id="logo"><img src="{{ asset('/front/affiliate_tw/images/logo.png') }}"></span></h1>
		</div>
		<div class="login-box animated fadeInUp">
			<form method="post" action="login_process" accept-charset="UTF-8" autocomplete="off">
				<div class="box-header">
					<h2>请登录</h2>
				</div>
				<label for="username">用户名</label>
				<br/>
				<input type="text" id="username" name="username">
				<br/>
				<label for="password">密码</label>
				<br/>
				<input type="password" id="password" name="password">
				<br/>
				<input type="hidden" id="crccode" name="crccode" value="TWD">
				<button type="submit" id="login_btn">签到</button>
				<br/>
			</form>
		</div>
	</div>
</body>

<script>
	$(document).ready(function () {
    	$('#logo').addClass('animated fadeInDown');
    	$("input:text:visible:first").focus();

		@if (session('message'))
        	alert('{{ session('message') }}');
		@endif
	});
	$('#username').focus(function() {
		$('label[for="username"]').addClass('selected');
	});
	$('#username').blur(function() {
		$('label[for="username"]').removeClass('selected');
	});
	$('#password').focus(function() {
		$('label[for="password"]').addClass('selected');
	});
	$('#password').blur(function() {
		$('label[for="password"]').removeClass('selected');
	});
</script>

</html>