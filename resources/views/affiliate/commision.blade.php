﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "//www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="//www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<title>Affiliate - Home - {{Config::get('setting.website_name')}}</title>
	<link href="{{url()}}/affiliate/css/login.css" type="text/css" media="all" rel="stylesheet" />
	<!--<script type="text/javascript" language="javascript" src="{{url()}}/front/resources/js/jquery.bxslider.min.js"></script>-->
	<script type="text/javascript" src="//gc.kis.scr.kaspersky-labs.com/1B74BD89-2A22-4B93-B451-1C9E1052A0EC/main.js" charset="UTF-8"></script><script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
</head>
	<body>
	<div id="main">
	<style>
	#login_btn
	{
		background:url({{url()}}/affiliate/images/en/login_btn.jpg) no-repeat;
	}
	#reg_btn
	{
		background:url({{url()}}/affiliate/images/en/join_now_btn.jpg) no-repeat;
	}		
	</style>
	
		<div id="header">
			<div class="center_content">
				<div id="logo"><img src="{{url()}}/{{Config::get('setting.affiliate_logo')}}" /></div>
				<div id="form_portion">
					<form method="post" action="login_process" accept-charset="UTF-8" autocomplete="off">
						<a href="register">
							<div id="reg_btn"></div>
						</a>
						<input type="text" name="username" class="username" placeholder="Username" value=""/>						
						<input type="password" name="password" class="pass" placeholder="Password"/>
						
						<div id="code">
							<img src="captcha" width="50" height="22"/>
						</div>
						
						<input type="text" class="code_fill" name="code" placeholder="Code"/>
						<a href="#">
							<input id="login_btn" type="submit" value=""style="cursor:pointer;">
						</a>
						<input type="hidden" value="{{csrf_token()}}" name="_token">
					</form>
				</div>
			</div>
		</div>
		
		<div id="navbar">
			<div class="center_content">
				<ul>
					<li id="nav_home"><a href="{{route('affiliate_login')}}">Home</a></li>
					<div class="nav_divider"></div>
					<li id="nav_product"><a href="#">Our Products</a></li>
					<div class="nav_divider"></div>
					<li id="nav_cp"><a href="{{route('affiliate_commision')}}">Commission Plan</a></li>
					<div class="nav_divider"></div>
					<li id="nav_faq"><a href="{{route('affiliate_faq')}}">FAQ</a></li>
					<div class="nav_divider"></div>
					<li id="nav_contact"><a href="#">CONTACT US</a>
					<div class="contact_dd">
					<table border="0" cellspacing="0" cellpadding="2">
						<tr>
							<td>Email </td>
							<td>:</td>
							<td>affiliate@ {{Config::get('setting.website_name')}}.com</td>
						</tr>
						<tr>
							<td>Skype </td>
							<td>:</td>
							<td>{{Session::get('setting.website_name')}}</td>
						</tr>
						<tr>
							<td>Phone</td>
							<td>:</td>
							<td>6011 2780 7697</td>
						</tr>
						<tr>
							<td>Live Chat</td>
							<td>:</td>
							<td>24x7</td>
						</tr>
					</table>
					<div class="clear"></div>
					</div>
					</li>
				</ul>
			</div>
		</div> 
		
	<div>
	
	
	<div class="center_content" style="background-color:white;padding:30px;">
    <h3>Welcome to {{Config::get('setting.website_name')}} Affiliate Program</h3>
{{Config::get('setting.website_name')}} Affiliate Program is providing our affiliate members the most attractive profit sharing structure. All {{Config::get('setting.website_name')}} Affiliate members can enjoy up to 45% commission rate.
    <br/>
    <br/>
    <h3>Why {{Config::get('setting.website_name')}} Affiliate Program</h3>

    <ul>
        <li>We are providing World-Class Gaming Product: Sportsbook, Live Casino, 4D, Poker and etc.</li>
        <li>Our effective market analysis and marketing strategy guarantee high member conversion and retention rate.</li>
        <li>Highest Commission Given in the Field.
            <br/>- Commission Rate Up to 45%
            <br/>- Commission from your direct member and your sub-affiliate’s Members</li>
        <li>We are providing user-friendly software consists of online reporting of your affiliate performance.</li>
        <li>We have a well-trained affiliate team to support your every need.</li>
    </ul>
    
<h3>Double or Triple your commission from your direct members and your sub-affiliates’ members.</h3>
{{Config::get('setting.website_name')}} Commission Structure ( Sole Discretion of {{Config::get('setting.website_name')}} )
    <br/>You can receive 20% to 45% of your affiliate member’s net losses as your affiliate commission in {{Config::get('setting.website_name')}} affiliate program.
    <br/>
    <br/>
    <table bgcolor="#CCC" border="0" cellpadding="5" cellspacing="1" width="500">
        <tbody>
            <tr class="white">
                <td align="center" bgcolor="#4B3E10"><b>Member Affiliate’s Net Losses</b>

                    <br/>( First Day to Last Day of the Month )
                    <br/>({{Config::get('setting.currency')}} or equivalent currency)</td>
                <td align="center" bgcolor="#4B3E10"><b>Monthly Active Member    </b>

                    <br/>(At Least One Active Wager)</td>
                <td align="center" bgcolor="#4B3E10"><b>Commision Rate</b>

                </td>
            </tr>
			@if( Config::get('setting.currency') == "MYR" )
			<tr>
                <td bgcolor="#FFFFFF">1. {{Config::get('setting.currency')}} 0 to {{Config::get('setting.currency')}} 20,000</td>
                <td align="center" bgcolor="#FFFFFF">5</td>
                <td align="center" bgcolor="#FFFFFF">25%</td>
            </tr>
            <tr>
                <td bgcolor="#FFFFFF">2. Next {{Config::get('setting.currency')}} 20,001 to {{Config::get('setting.currency')}} 200,000</td>
                <td align="center" bgcolor="#FFFFFF">5</td>
                <td align="center" bgcolor="#FFFFFF">35%</td>
            </tr>
            <tr>
                <td bgcolor="#FFFFFF">3. Next {{Config::get('setting.currency')}} 200,001 and above</td>
                <td align="center" bgcolor="#FFFFFF">5</td>
                <td align="center" bgcolor="#FFFFFF">45%</td>
            </tr>
			@else
			<tr>
                <td bgcolor="#FFFFFF">1. {{Config::get('setting.currency')}} 1 to {{Config::get('setting.currency')}} 10,000</td>
                <td align="center" bgcolor="#FFFFFF">5</td>
                <td align="center" bgcolor="#FFFFFF">25%</td>
            </tr>
            <tr>
                <td bgcolor="#FFFFFF">2. Next {{Config::get('setting.currency')}} 10,001 to {{Config::get('setting.currency')}} 100,000</td>
                <td align="center" bgcolor="#FFFFFF">5</td>
                <td align="center" bgcolor="#FFFFFF">35%</td>
            </tr>
            <tr>
                <td bgcolor="#FFFFFF">3. Next {{Config::get('setting.currency')}} 100,001 and above</td>
                <td align="center" bgcolor="#FFFFFF">5</td>
                <td align="center" bgcolor="#FFFFFF">45%</td>
            </tr>
			@endif
        </tbody>
    </table>(Minimum Payout = {{Config::get('setting.currency')}} 300.00)
    <br/>
    <br/>Above is the {{Config::get('setting.website_name')}} Affiliate Program’s Commission Rate Table. ( Commission Rate is based on members’ net losses. )
    <br/>Important: With at least 6 active members (members that have placed at least one bet in the month) will entitle you to start earning commission.
    <br/>Negative revenue of any given month will be carried forward to the following month.
    <br/>
    <br/>Example 1: If Company net profit (Member net losses) is {{Config::get('setting.currency')}} 20,000, affiliate commission rate will be 20%. (shown on the table above)
    <br/>
    <br/>Example 2: If Company net profit (Member net losses) is {{Config::get('setting.currency')}} 60,000, affiliate commission rate will be 20% of the first {{Config::get('setting.currency')}} 20,000 of Company net profit and 30% of the next {{Config::get('setting.currency')}} 40,000 of Company net profit.(shown on the table above)
    <br/>
    <br/>Example 3: If Company net profit (Member net losses) is {{Config::get('setting.currency')}} 300,000, affiliate commission rate will be 20% of the first {{Config::get('setting.currency')}} 20,000 of Company net profit, 30% of the next {{Config::get('setting.currency')}} 200,000 of Company net profit and 45% of the next {{Config::get('setting.currency')}} 80,000 of company net profit.(shown on the table above)
    <p>
        <br/>
    </p>
    <p></p>
    <ul>
        <li>With a 45% commission rate, 45% of members’ net losses is your commission. Increase in members’ net losses and active members will enable you to earn more.</li>
        <li>With at least 6 active members (members that have placed at least one bet in the month) will entitle you to start earning commission.</li>
        <li>We monitor our affiliates' performance and may contact you directly to increase your commission rate.</li>
        <li>Negative revenue for any given month will be carried forward to the following month.</li>
        <li>All affiliates are subjected to administrative and marketing costs incurred directly for the acquisition of members.</li>
        <li>The cost is deducted from the revenue on a monthly basis. These cost may include the following:</li>
        <ul>
            <li>Payment charges – Deposit and withdrawal charges involving all the affiliate’s members.</li>
            <li>Media Fees – Any fees that incurred towards {{Config::get('setting.website_name')}} to support or assist affiliate in conjunction with promotions or marketing purposes.</li>
            <li>Promotion bonuses – Monetary bonuses or rebates given to the affiliate’s members.</li>
        </ul>
    </ul>
    <p>
        <br/>
    </p>
    <p>(II) Commission Structure from sub-affiliates’ members:</p>
    <p></p>
    <ul>
        <li>All affiliates will get an extra 10% on sub-affiliates’ commission, thus invite your friends to be part of our {{Config::get('setting.website_name')}} Affiliates and start earning money together. The more sub-affiliates you have, the more you earn. You will also earn from your sub-affiliates’ affiliates.</li>
        <li>Illustration: Tabulation of Multi-Tier Commission&amp;nbsp;</li>
    </ul>
    <p></p>
    <table bgcolor="#CCC" border="0" cellpadding="5" cellspacing="1" width="500">
        <tbody>
            <tr class="white">
                <td align="center" bgcolor="#4B3E10">Line</td>
                <td align="center" bgcolor="#4B3E10">Downline Member</td>
                <td align="center" bgcolor="#4B3E10">Percentage</td>
            </tr>
            <tr>
                <td bgcolor="#FFFFFF">L1
                    <br/>L1
                    <br/>L1</td>
                <td align="center" bgcolor="#FFFFFF">L2
                    <br/>L3
                    <br/>L4</td>
                <td align="center" bgcolor="#FFFFFF">5%
                    <br/>2%
                    <br/>1%</td>
            </tr>
            <tr>
                <td bgcolor="#FFFFFF">L2
                    <br/>L2
                    <br/>L2</td>
                <td align="center" bgcolor="#FFFFFF">L3
                    <br/>L4
                    <br/>L5</td>
                <td align="center" bgcolor="#FFFFFF">5%
                    <br/>2%
                    <br/>1%</td>
            </tr>
            <tr>
                <td bgcolor="#FFFFFF">L3
                    <br/>L3
                    <br/>L3</td>
                <td align="center" bgcolor="#FFFFFF">L4
                    <br/>L5
                    <br/>L6</td>
                <td align="center" bgcolor="#FFFFFF">5%
                    <br/>2%
                    <br/>1%</td>
            </tr>
            <tr>
                <td bgcolor="#FFFFFF">L4
                    <br/>L4
                    <br/>L4</td>
                <td align="center" bgcolor="#FFFFFF">L5
                    <br/>L6
                    <br/>L7</td>
                <td align="center" bgcolor="#FFFFFF">5%
                    <br/>2%
                    <br/>1%</td>
            </tr>
        </tbody>
    </table>
    <br/>
    <p></p>
    <p></p>
    <p></p>
    <p><b>Payout</b>

    </p>
    <p></p>
    <p></p>
    <ul>
        <li>Commissions will be automatically deposited into your bank account specified upon registration within next 5 working days after the last day of the month. There will only be one payout per each calendar month. While we offer free payout transactions we will not be held responsible if the receiving financial institute impose a fee of any kind at their end.</li>
        <li>There is a minimum amount of {{Config::get('setting.currency')}} 300 or equivalent currency for payout to take effect. Any amount below this threshold will be carried forward to the next month.</li>
    </ul>
    <p>
        <br/>
    </p>
    <p></p>
    <p><b>{{Config::get('setting.website_name')}} </b>
reserves the right to alter or amend and/or add any of the terms mentioned above as we deemed fit.</p>
    <p><b>{{Config::get('setting.website_name')}} </b>
reserves the right to cancel the affiliate account at any point of time without prior notice in the event the affiliate’s performance does not meet our expected quotas at a given period of time.</p>
    <p></p>
    <p></p>
    <p></p>​</div>
	
	</div>
	
		<div id="copyright">
			<div class="center_content">
				<div id="icon">
					<img src="{{url()}}/affiliate/images/en/footer_icon.png" width="525" height="31"/>
				</div>
				<div id="left_copyright">© Copyright<span style="color:#767057"> {{Config::get('setting.website_name')}} Affiliate</span>. All rights reserved.</div>
				<div id="right_tnc">
					<a href="#">Terms and Conditions</a> &nbsp; &nbsp;&nbsp;
					<a href="#">Disclaimer</a> &nbsp;&nbsp;&nbsp;
					<a href="#">Privacy Policy</a> &nbsp;&nbsp;&nbsp;
					<a href="#">FAQ</a>
				</div>
			</div> 
		</div>  
		<iframe id="frameSubmit" name="frameSubmit" src="resources/script/index.html" border="0" marginwidth="0" marginheight="0" hspace="0" vspace="0" frameborder="0" scrolling="auto" style="position:absolute; z-index:0; visibility:hidden"></iframe>
	</body>
</html>
