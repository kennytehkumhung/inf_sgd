@extends('front/master')

@section('title', 'Account Password')

@section('top_js')
<link href="{{url()}}/front/resources/css/acct_management.css" rel="stylesheet">
@stop

@section('content')

@include('front/transaction_mange_top', [ 'title' => Lang::get('public.AccountPassword') ] )
<div class="acctContent">
    <div class="depLeft">
		<div class="cont">
			<!--MID SECTION-->
			<?php echo htmlspecialchars_decode($content); ?>
			<!--MID SECTION-->
		</div>
	</div> 
	<div class="depRight">
	<span class="wallet_title">{{Lang::get('public.AccountPassword')}}</span>
	<form id="formChangePassword">
		
		<div class="acctRow">
			<label>{{Lang::get('public.CurrentPassword')}} :</label><input type="password" id="current_password" />
			<div class="clr"></div>
		</div>
		
		<div class="acctRow">
			<label>{{Lang::get('public.NewPassword')}} :</label><input type="password" id="new_password" />
			<div class="clr"></div>
		</div>
		
		<div class="acctRow">
			<label>{{Lang::get('public.ConfirmNewPassword')}} :</label><input type="password" id="confirm_new_password" onkeyup="checkPass(); return false;"/>
			<span id="confirmMessage" class="confirmMessage"></span>
			<div class="clr"></div>
		</div>

		<div class="submitAcct">
			<a href="javascript:void(0)" onclick="update_password()">{{Lang::get('public.Update')}}</a>
		</div>
			
	</div>
	<div class="clr"></div>
	</form>

@stop

@section('bottom_js')
<script src="{{url()}}/front/resources/js/slick.min.js"></script>
<script>
$('.caro').slick({
  infinite: true,
  slidesToShow: 2,
  slidesToScroll: 2,
  autoplay: true,
  variableWidth: true,
  arrows: false
});

function checkPass()
{
    //Store the password field objects into variables ...
    var new_password = document.getElementById('new_password');
    var confirm_new_password = document.getElementById('confirm_new_password');
    //Store the Confimation Message Object ...
    var message = document.getElementById('confirmMessage');
    //Set the colors we will be using ...
    var goodColor = "#66cc66";
    var badColor = "#ff6666";
    //Compare the values in the password field 
    //and the confirmation field
    if(new_password.value == confirm_new_password.value){
        //The passwords match. 
        //Set the color to the good color and inform
        //the user that they have entered the correct password 
        confirm_new_password.style.backgroundColor = goodColor;
        message.style.color = goodColor;
        message.innerHTML = "{{ Lang::get('public.PasswordsMatch') }}"
    }else{
        //The passwords do not match.
        //Set the color to the bad color and
        //notify the user.
        confirm_new_password.style.backgroundColor = badColor;
        message.style.color = badColor;
        message.innerHTML = "{{ Lang::get('public.PasswordsDoNotMatch') }}"
    }
} 

 function update_password(){
	$.ajax({
		type: "POST",
		url: "{{action('User\MemberController@ChangePassword')}}",
		data: {
			_token: "{{ csrf_token() }}",
			current_password:		$('#current_password').val(),
			new_password:    		$('#new_password').val(),
			confirm_new_password:   $('#confirm_new_password').val()

		},
	}).done(function( json ) {
		
		obj = JSON.parse(json);
			 var str = '';
			 $.each(obj, function(i, item) {
				str += item + '\n';
			})
			
			alert(str);
					
	});
}
</script>
@stop