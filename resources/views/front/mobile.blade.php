@extends('front/master')

@if (Session::get('currency') == 'TWD')
    @section('title', '無極限娛樂網')
@elseif (Session::get('currency') == 'MYR')
    @section('title', 'Best Mobile Casino Malaysia - iPhone & Android Casino')
    @section('keywords', 'Best Mobile Casino Malaysia - iPhone & Android Casino')
    @section('description', 'Play at InfiniWin Mobile Online Casino Malaysia - awarded Best Online Casino and enjoy 100% Welcome Deposit Bonus. Join InfiniWin mobile casino online today.')
@endif

@section('top_js')
	<script type="text/javascript" language="javascript">
		$(document).ready(function() {
			$('.box_skitter_large').skitter({
				theme: 'clean',
				numbers_align: 'center',
				progressbar: false, 
				dots: true, 
				preview: false
			});
		});
	</script>
	<style>
	.modal__inner{
		width: 542px;
		height: 350px;
	}
	.mobHead  {
	margin-top: 10px;
}

.coded   {
	position: relative;
	display: block;
}

.mobBox  {
	background: #fff;
	float: left;
	display: block;
	position: relative;
	width: 233px;
	text-align: center;
	margin: 0px 13px 0px 0px;
}

.mobBoxL  {
	background: #fff;
	float: left;
	display: block;
	position: relative;
	width: 233px;
	text-align: center;
	margin: 0px 0px 0px 0px;
	//padding: 7px 0px;
}

.get1  {
	position: absolute;
	display: block;
	bottom: 5px;
	left: 25px;
}

.get2  {
	position: absolute;
	display: block;
	bottom: 5px;
	right: 25px;
}

.get3  {
	position: absolute;
	display: block;
	bottom: 10px;
	left: 15px;
}



.mobGet  {
	padding: 5px 0px;
	height: 68px;
}

.mobGet2  {
	padding: 0px;
	height: 68px;
	background: url(../front/img/ag-mid-img.jpg)no-repeat;
	display: block;
	position: relative;
}

.mobGet3  {
	padding: 0px;
	height: 68px;
	background: url(../front/img/mob-bg-none{{ in_array(Lang::getLocale(), ['tw']) ? '-'.Lang::getLocale() : '' }}.jpg)no-repeat;
	display: block;
	position: relative;
}

.mobGet4  {
	padding: 0px;
	height: 68px;
	background: url(../front/img/mob-bg-none{{ in_array(Lang::getLocale(), ['tw']) ? '-'.Lang::getLocale() : '' }}.jpg)no-repeat;
	display: block;
	position: relative;
        //margin-top: -7px;
}

.mobContTop  {
width: 994px;
display: block;
position: relative;
height: {{ Session::get('currency') == 'TWD' ? '2300px' : 'auto' }};

padding: 15px;
background: #000;
}

.mobWrap  {
	width: 974px;
	height: 1600px;
	display: block;
	position: relative;
	-moz-border-radius-topleft: 15px;
-webkit-border-top-left-radius: 15px;
 border-top-left-radius: 15px;
-moz-border-radius-topright: 15px;
-webkit-border-top-right-radius: 15px;
border-top-right-radius: 15px;
   background: #47483b;
   padding: 10px;
}

.mobBx  {
	width: 180px;
	position: relative;
	display: inline-block;
	height: auto;
	float: left;
	background: #fff;
}

#button1 {position: absolute; top:102px;left:25px;}
#button2 {position: absolute; top:102px;left:140px;}
#button3 {position: absolute; top:102px;left:278px;}
#button4 {position: absolute; top:102px;left:388px;}

#content {
    width:300px; 
    height:500px;
    position:absolute;
    left:0px;
    top:250px;
    font-size:10px;
    text-align:center;
 }

	</style>
	 <link href="{{url()}}/front/resources/css/skitter.styles1.css" type="text/css" media="all" rel="stylesheet" />
	 <script type="text/javascript" src="{{url('/')}}/{{ session('public_folder') }}/resources/js/qrcode.js"></script>

@stop

@section('content')
<!--Mobile-->
<div class="mobCont">
	<img src="{{url()}}/front/img/mob-title{{ in_array(Lang::getLocale(), ['tw']) ? '-'.Lang::getLocale() : '' }}.jpg" width="1024" height="66">
	<div class="mobContTop">
		<div class="mobWrap" style="height: 1850px;">
			<div class="mobHead">
				<div class="mobBox">
					<img src="{{url()}}/front/img/mob-sky.jpg" alt="">
					<div class="mobGet2">

					</div>
					<div class="mobGet">
						<div class="get1">
							<label class="btn" for="modal-9"><img src="{{url()}}/front/img/getNow-orange{{ in_array(Lang::getLocale(), ['tw']) ? '-'.Lang::getLocale() : '' }}.png" width="70" height="60"></label>
							<input class="modal-state" id="modal-9" type="checkbox" />
							<div class="modal">
								<label class="modal__bg" for="modal-9"></label>
								<div class="modal__inner">
									<label class="modal__close" for="modal-9"></label>
									<center>
												<div class="QRcodeIMG" style="padding:20px;">
												<input id="getLink7" type="text" value="{{$android['sky']}}"	hidden>
												<a href="{{$android['sky']}}"><div id="printQR7"></div></a>
												</div>										
												<a style=" display: block; font-size: 24px; color:blue;" href="{{$android['sky']}}" class="button" target="_blank">{{ Lang::get('public.Download') }}</a>
									</center>
									<br>
									<span style="color:black;font-size:30px;margin-left:170px;">
							@if(Session::has('username'))
											{{Config::get(Session::get('currency').'.sky.agid').((new \App\libraries\providers\SKY())->formatUserId(Session::get('userid')))}}
										@else
											Please Login!
										@endif
							</span>

								</div>
							</div>
						</div>
					</div>
					<div class="get2">


						<label class="btn" for="modal-10"><img src="{{url()}}/front/img/getNow-Red{{ in_array(Lang::getLocale(), ['tw']) ? '-'.Lang::getLocale() : '' }}.png" width="70" height="60"></label>
						<input class="modal-state" id="modal-10" type="checkbox" />
						<div class="modal">
							<label class="modal__bg" for="modal-10"></label>
							<div class="modal__inner">
								<label class="modal__close" for="modal-10"></label>
								<center>
												<div class="QRcodeIMG" style="padding:20px;">
												<input id="getLink6" type="text" value="{{$ios['sky']}}"	hidden>
												<a href="{{$ios['sky']}}"><div id="printQR6"></div></a>
												</div>										
												<a style=" display: block; font-size: 24px; color:blue;" href="{{$ios['sky']}}" class="button" target="_blank">{{ Lang::get('public.Download') }}</a>
								</center>
								<br>
								<span style="color:black;font-size:30px;margin-left:170px;">
							@if(Session::has('username'))
										{{Config::get(Session::get('currency').'.sky.agid').((new \App\libraries\providers\SKY())->formatUserId(Session::get('userid')))}}
									@else
										Please Login!
									@endif
							</span>

							</div>
						</div>
					</div>
					<div class="clr"></div>
				</div>

				<div class="mobBox">
					<img src="{{url()}}/front/img/mob-alb.jpg" alt="">
					<div class="mobGet2">
					</div>
					<div class="mobGet">
						<div class="get1">
							<label class="btn" for="modal-11"><img src="{{url()}}/front/img/getNow-orange{{ in_array(Lang::getLocale(), ['tw']) ? '-'.Lang::getLocale() : '' }}.png" width="70" height="60"></label>
							<input class="modal-state" id="modal-11" type="checkbox" />
							<div class="modal">
								<label class="modal__bg" for="modal-11"></label>
								<div class="modal__inner">
									<label class="modal__close" for="modal-11"></label>
									<center>
												<div class="QRcodeIMG" style="padding:20px;">
												<input id="getLink5" type="text" value="{{$android['alb']}}"	hidden>
												<a href="{{$android['alb']}}"><div id="printQR5"></div></a>
												</div>																				<a style=" display: block; font-size: 24px; color:blue;" href="{{$android['alb']}}" class="button" target="_blank">{{ Lang::get('public.Download') }}</a>
									</center>
									<br>
									<span style="color:black;font-size:30px;margin-left:170px;">
							@if(Session::has('username'))
											{{ strtolower(Config::get(Session::get('currency').'.alb.prefix')).'_'.strtolower(Session::get('username')).(Session::get('currency') == 'TWD' ? 'dyn' : 'av2') }}
										@else
											Please Login!
										@endif
							</span>

								</div>
							</div>
						</div>
						<div class="get2">

							<label class="btn" for="modal-12"><img src="{{url()}}/front/img/getNow-Red{{ in_array(Lang::getLocale(), ['tw']) ? '-'.Lang::getLocale() : '' }}.png" width="70" height="60"></label>
							<input class="modal-state" id="modal-12" type="checkbox" />
							<div class="modal">
								<label class="modal__bg" for="modal-12"></label>
								<div class="modal__inner">
									<label class="modal__close" for="modal-12"></label>
									<center>
												<div class="QRcodeIMG" style="padding:20px;">
												<input id="getLink4" type="text" value="{{$ios['alb']}}"	hidden>
												<a href="{{$ios['alb']}}"><div id="printQR4"></div></a>
												</div>										
												<a style=" display: block; font-size: 24px; color:blue;" href="{{$android['alb']}}" class="button" target="_blank">{{ Lang::get('public.Download') }}</a>
									</center>
									<br>
									<span style="color:black;font-size:30px;margin-left:170px;">
							@if(Session::has('username'))
											{{ strtolower(Config::get(Session::get('currency').'.alb.prefix')).'_'.strtolower(Session::get('username')).(Session::get('currency') == 'TWD' ? 'dyn' : 'av2') }}
										@else
											Please Login!
										@endif
							</span>

								</div>
							</div>
						</div>
						<div class="clr"></div>
					</div>
				</div>

				<div class="mobBox">
				<img src="{{url()}}/front/img/mob-mxb.png" alt="">
				<a class="tut-btn" href="javascript:void(0);" data-tut-id="mxa">
					<div class="mobGet3">
						&nbsp;
					</div>
				</a>
				<div class="mobGet">
				<div class="get1">
					
					<label class="btn" for="modal-5"><img src="{{url()}}/front/img/getNow-orange{{ in_array(Lang::getLocale(), ['tw']) ? '-'.Lang::getLocale() : '' }}.png" width="70" height="60"></label>
					<input class="modal-state" id="modal-5" type="checkbox" />
					<div class="modal">
						<label class="modal__bg" for="modal-5"></label>
						<div class="modal__inner">
							<label class="modal__close" for="modal-5"></label>
							<center>
							@if(Session::get('currency')=='MYR')
												<div class="QRcodeIMG" style="padding:20px;">
												<input id="getLink1" type="text" value="{{$android['mxb']}}"	hidden>
												<a href="{{$android['mxb']}}"><div id="printQR1"></div></a>
												</div>
												@endif
								@if (Session::get('currency') == 'TWD')
									<a style=" display: block; font-size: 24px; color:blue;" href="{{ asset('/front/mobile/xpg_twd.apk') }}" class="button" target="_blank">{{ Lang::get('public.Download') }}</a>
								@else
									<a style=" display: block; font-size: 24px; color:blue;" href="{{$android['mxb']}}" class="button" target="_blank">{{ Lang::get('public.Download') }}</a>
								@endif
							</center>
							<br>
							<span style="color:black;font-size:30px;margin-left:170px;">
							@if(Session::has('username'))
								{{strtoupper(Session::get('username'))}}
							@else
								Please Login!
							@endif
							</span>
							
						</div>
					</div>
				</div>
				<div class="get2">
				<label class="btn" for="modal-5"><img src="{{url()}}/front/img/getNow-Red{{ in_array(Lang::getLocale(), ['tw']) ? '-'.Lang::getLocale() : '' }}.png" width="70" height="60"></label>
				</div>
				<div class="clr"></div>
				</div>
				</div>

				<div class="mobBoxL">
					<img src="{{url()}}/front/img/mob-joker.jpg" alt="">
					<div class="mobGet2" style="position: relative;">
						<a class="tut-btn" href="javascript:void(0);" data-tut-id="jka" style="position: absolute; top: 8px; left: 15px; z-index: 2;"><img src="{{url()}}/front/img/mob-and-lc{{ in_array(Lang::getLocale(), ['tw']) ? '-'.Lang::getLocale() : '' }}.png" width="88" height="45"></a>
						<a class="tut-btn" href="javascript:void(0);" data-tut-id="jki" style="position: absolute; top: 8px; left: 130px; z-index: 2;"><img src="{{url()}}/front/img/mob-app-lc{{ in_array(Lang::getLocale(), ['tw']) ? '-'.Lang::getLocale() : '' }}.png" width="88" height="45"></a>
					</div>
					<div class="mobGet">
						<div class="get1">
							<label class="btn" for="modal-7"><img src="{{url()}}/front/img/getNow-orange{{ in_array(Lang::getLocale(), ['tw']) ? '-'.Lang::getLocale() : '' }}.png" width="70" height="60"></label>
							<input class="modal-state" id="modal-7" type="checkbox" />
							<div class="modal">
								<label class="modal__bg" for="modal-7"></label>
								<div class="modal__inner">
									<label class="modal__close" for="modal-7"></label>
									<center>
												<div class="QRcodeIMG" style="padding:20px;">
												<input id="getLink2" type="text" value="{{$android['jok']}}"	hidden>
												<a href="{{$android['jok']}}"><div id="printQR2"></div></a>
												</div>			
												<a style=" display: block; font-size: 24px; color:blue;" href="{{$android['jok']}}" class="button" target="_blank">{{ Lang::get('public.Download') }}</a>
									</center>
									<br>
									<span style="color:black;font-size:30px;margin-left:180px;">
							@if(Session::has('username'))
											{{ Config::get(Session::get('currency').'.jok.appid') }}.{{ Config::get(Session::get('currency').'.jok.prefix') }}_{{strtoupper(Session::get('username'))}}
										@else
											Please Login!
										@endif
							</span>

								</div>
							</div>
						</div>
						<div class="get2">


							<label class="btn" for="modal-8"><img src="{{url()}}/front/img/getNow-Red{{ in_array(Lang::getLocale(), ['tw']) ? '-'.Lang::getLocale() : '' }}.png" width="70" height="60"></label>
							<input class="modal-state" id="modal-8" type="checkbox" />
							<div class="modal">
								<label class="modal__bg" for="modal-8"></label>
								<div class="modal__inner">
									<label class="modal__close" for="modal-8"></label>
									<center>
												<div class="QRcodeIMG" style="padding:20px;">
												<input id="getLink3" type="text" value="{{$ios['jok']}}"	hidden>
												<a href="{{$ios['jok']}}"><div id="printQR3"></div></a>
												</div>											
												<a style=" display: block; font-size: 24px; color:blue;" href="{{$ios['jok']}}" class="button" target="_blank">{{ Lang::get('public.Download') }}</a>
									</center>
									<br>
									<span style="color:black;font-size:30px;margin-left:180px;">
							@if(Session::has('username'))
											{{ Config::get(Session::get('currency').'.jok.appid') }}.{{ Config::get(Session::get('currency').'.jok.prefix') }}_{{strtoupper(Session::get('username'))}}
										@else
											Please Login!
										@endif
							</span>

								</div>
							</div>
						</div>
						<div class="clr"></div>
					</div>
				</div>

				<a id="button1" class="tut-btn" href="javascript:void(0);" data-tut-id="ska"><img src="{{url()}}/front/img/mob-and-lc{{ in_array(Lang::getLocale(), ['tw']) ? '-'.Lang::getLocale() : '' }}.png" width="88" height="45"></a>
				<a id="button2" class="tut-btn" href="javascript:void(0);" data-tut-id="ski"><img src="{{url()}}/front/img/mob-app-lc{{ in_array(Lang::getLocale(), ['tw']) ? '-'.Lang::getLocale() : '' }}.png" width="88" height="45"></a>
				<a id="button3" class="tut-btn" href="javascript:void(0);" data-tut-id="ala"><img src="{{url()}}/front/img/mob-and-lc{{ in_array(Lang::getLocale(), ['tw']) ? '-'.Lang::getLocale() : '' }}.png" width="88" height="45"></a>
				<a id="button4" class="tut-btn" href="javascript:void(0);" data-tut-id="ali"><img src="{{url()}}/front/img/mob-app-lc{{ in_array(Lang::getLocale(), ['tw']) ? '-'.Lang::getLocale() : '' }}.png" width="88" height="45"></a>

				@if (Session::get('currency') != 'TWD')

				<div class="mobBox" style="margin-top: 13px;">
					<img src="{{url()}}/front/img/mob-pt.png" alt="">
					<div class="mobGet2" style="position: relative;">
						<a class="tut-btn" href="javascript:void(0);" data-tut-id="and" style="position: absolute; top: 8px; left: 15px; z-index: 2;"><img src="{{url()}}/front/img/mob-and-lc{{ in_array(Lang::getLocale(), ['tw']) ? '-'.Lang::getLocale() : '' }}.png" width="88" height="45"></a>
						<a class="tut-btn" href="javascript:void(0);" data-tut-id="enj" style="position: absolute; top: 8px; left: 130px; z-index: 2;"><img src="{{url()}}/front/img/mob-and-lc{{ in_array(Lang::getLocale(), ['tw']) ? '-'.Lang::getLocale() : '' }}.png" width="88" height="45"></a>
					</div>
					<div class="mobGet">
						<div class="get1">
							<label class="btn" for="modal-3"><img src="{{url()}}/front/img/getNow-orange{{ in_array(Lang::getLocale(), ['tw']) ? '-'.Lang::getLocale() : '' }}.png" width="70" height="60"></label>
							<input class="modal-state" id="modal-3" type="checkbox" />
							<div class="modal">
								<label class="modal__bg" for="modal-3"></label>
								<div class="modal__inner">
									<label class="modal__close" for="modal-3"></label>
									<center>
												<div class="QRcodeIMG" style="padding:20px;width: 0%;position: absolute;">
												<input id="getLink12" type="text" value="{{$android['pltslot']}}"	hidden>
												<a href="{{$android['pltslot']}}"><div id="printQR12"></div></a>
												</div>				
												<div class="QRcodeIMG" style="padding: 20px;padding-left: 334px;">
												<input id="getLink13" type="text" value="{{$android['pltlive']}}"	hidden>
												<a href="{{$android['pltlive']}}"><div id="printQR13"></div></a>
												</div>										</center>
									<a style="font-size: 24px; color:blue; margin-left:60px;" href="{{$android['pltslot']}}" class="button" target="_blank">{{ Lang::get('public.Download') }}</a><a style="margin-left:220px; font-size: 24px; color:blue;" href="{{$android['pltlive']}}" class="button" target="_blank">{{ Lang::get('public.Download') }}</a>
									<br><br>
									<span style="color:black;font-size:30px;margin-left:170px;">
							@if(Session::has('username'))
											INF_{{Session::get('username')}}
										@else
											Please Login!
										@endif
							</span>

								</div>
							</div>
						</div>
						<div class="get2">


							<label class="btn" for="modal-4"><img src="{{url()}}/front/img/getNow-Red{{ in_array(Lang::getLocale(), ['tw']) ? '-'.Lang::getLocale() : '' }}.png" width="70" height="60"></label>
							<input class="modal-state" id="modal-4" type="checkbox" />
							<div class="modal">
								<label class="modal__bg" for="modal-4"></label>
								<div class="modal__inner">
									<label class="modal__close" for="modal-4"></label>
									<center>

												<div class="QRcodeIMG" style="padding:20px;width: 0%;position: absolute;">
												<input id="getLink11" type="text" value="{{$android['pltslot']}}"	hidden>
												<a href="{{$android['pltslot']}}"><div id="printQR11"></div></a>
												</div>				
												<div class="QRcodeIMG" style="padding: 20px;padding-left: 334px;">
												<input id="getLink10" type="text" value="{{$android['pltlive']}}"	hidden>
												<a href="{{$android['pltlive']}}"><div id="printQR10"></div></a>
												</div>													
									</center>
									<a style="font-size: 24px; color:blue; margin-left:60px;" href="{{$android['pltslot']}}" class="button" target="_blank">{{ Lang::get('public.Download') }}</a><a style="margin-left:220px; font-size: 24px; color:blue;" href="{{$android['pltlive']}}" class="button" target="_blank">{{ Lang::get('public.Download') }}</a>
									<br><br>
									<span style="color:black;font-size:30px;margin-left:170px;">
							@if(Session::has('username'))
											INF_{{Session::get('username')}}
										@else
											Please Login!
										@endif
							</span>

								</div>
							</div>
						</div>
						<div class="clr"></div>
					</div>
				</div>

				<div class="mobBox" style="margin-top: 13px;">
					<img src="{{url()}}/front/img/mob-ag.png" alt="">
					<div class="mobGet2" style="position: relative;">
						<a class="tut-btn" href="javascript:void(0);" data-tut-id="def" style="position: absolute; top: 8px; left: 15px; z-index: 2;"><img src="{{url()}}/front/img/mob-and-lc{{ in_array(Lang::getLocale(), ['tw']) ? '-'.Lang::getLocale() : '' }}.png" width="88" height="45"></a>
						<a class="tut-btn" href="javascript:void(0);" data-tut-id="her" style="position: absolute; top: 8px; left: 130px; z-index: 2;"><img src="{{url()}}/front/img/mob-app-lc{{ in_array(Lang::getLocale(), ['tw']) ? '-'.Lang::getLocale() : '' }}.png" width="88" height="45"></a>
					</div>
					<div class="mobGet">
						<div class="get1">
							<label class="btn" for="modal-1"><img src="{{url()}}/front/img/getNow-orange{{ in_array(Lang::getLocale(), ['tw']) ? '-'.Lang::getLocale() : '' }}.png" width="70" height="60"></label>
							<input class="modal-state" id="modal-1" type="checkbox" />
							<div class="modal">
								<label class="modal__bg" for="modal-1"></label>
								<div class="modal__inner">
									<label class="modal__close" for="modal-1"></label>
									<center>
												<div class="QRcodeIMG" style="padding:20px;">
												<input id="getLink8" type="text" value="{{$android['agg']}}"	hidden>
												<a href="{{$android['agg']}}"><div id="printQR8"></div></a>
												</div>										
												<a style=" display: block; font-size: 24px; color:blue;" href="{{$android['agg']}}" class="button" target="_blank">{{ Lang::get('public.Download') }}</a>
									</center>
									<br>
									<span style="color:black;font-size:30px;margin-left:180px;">
							@if(Session::has('username'))
											AVIW_{{Session::get('username')}}
										@else
											Please Login!
										@endif
							</span>

								</div>
							</div>
						</div>
						<div class="get2">


							<label class="btn" for="modal-2"><img src="{{url()}}/front/img/getNow-Red{{ in_array(Lang::getLocale(), ['tw']) ? '-'.Lang::getLocale() : '' }}.png" width="70" height="60"></label>
							<input class="modal-state" id="modal-2" type="checkbox" />
							<div class="modal">
								<label class="modal__bg" for="modal-2"></label>
								<div class="modal__inner">
									<label class="modal__close" for="modal-2"></label>
									<center>
												<div class="QRcodeIMG" style="padding:20px;">
												<input id="getLink9" type="text" value="{{$ios['agg']}}"	hidden>
												<a href="{{$ios['agg']}}"><div id="printQR9"></div></a>
												</div>										
												<a style=" display: block; font-size: 24px; color:blue;" href="{{$ios['agg']}}" class="button" target="_blank">{{ Lang::get('public.Download') }}</a>
									</center>
									<br>
									<span style="color:black;font-size:30px;margin-left:180px;">
							@if(Session::has('username'))
										AVIW_{{Session::get('username')}}
										@else
											Please Login!
										@endif
							</span>

								</div>
							</div>
						</div>
						<div class="clr"></div>
					</div>
				</div>
				@endif

				<div id="content" style="{{ Session::get('currency') == 'TWD' ? '' : 'top: 482px;' }}">
					@if (Session::get('currency') == 'TWD')
						<div id="ska"><img src="{{url()}}/front/img/tw/step-sky-android.jpg" width="995px"></div>
						<div id="ski"><img src="{{url()}}/front/img/tw/step-sky-ios.jpg" width="995px"></div>
						<div id="ala"><img src="{{url()}}/front/img/tw/step-alb-android.png" width="995px"></div>
						<div id="ali"><img src="{{url()}}/front/img/tw/step-alb-ios.png" width="995px"></div>
						<div id="mxa"><img src="{{url()}}/front/img/tw/step-xpro-android.jpg" width="995px"></div>
						<div id="jka"><img src="{{url()}}/front/img/tw/step-jok-android.png" width="995px"></div>
						<div id="jki"><img src="{{url()}}/front/img/tw/step-jok-ios.jpg" width="995px"></div>
					@else
						<div id="ska"><img src="{{url()}}/front/img/step-sky-android.png" width="995px"></div>
						<div id="ski"><img src="{{url()}}/front/img/step-sky-ios.png" width="995px"></div>
						<div id="ala"><img src="{{url()}}/front/img/step-alb-android.png" width="995px"></div>
						<div id="ali"><img src="{{url()}}/front/img/step-alb-ios.png" width="995px"></div>
					@endif

				 <div id="and"><img src="{{url()}}/front/img/step-pt-android.png" width="995px"></div>
				 <div id="enj"><img src="{{url()}}/front/img/step-pt-android.png" width="995px"></div>
				 <div id="def"><img src="{{url()}}/front/img/step-ag-android.png" width="995px"></div>
				 <div id="faq"><img src="{{url()}}/front/img/step-ag-android.png" width="995px"></div>
				 <div id="her"><img src="{{url()}}/front/img/step-ag-ios.png" width="995px"></div>
				</div>
				<div class="clr"></div>
			</div>
			<div class="clr"></div>
		</div>
	</div>
	<div class="appBg">
	</div>
</div>

<div class="clr"></div>
@if (Session::get('currency') == 'MYR')
<div class="seo_content">
    <h1 style="font-size:13px;">Play Anywhere, Anytime With Infiniwin Mobile Casinos!</h1>
    <p>Mobile gaming is the craze of almost everyone with a mobile phone right now. But why would you spend hours on mobile games when you would not be making any money from it? Enter mobile casinos.</p>
    <p>Mobile casinos is perhaps the best thing that comes to mind when you are thinking about a thing that combines fun and money. You can get mobile casinos from Google Playstore or Apple Store but these apps are not that good. The quality of the games are not well made and the rewards are also not that much. Therefore, you must go to Infiniwin, the best website that has the best mobile casinos in Malaysia.</p>
    <p>Infiniwin does not only have one app for mobile casino download; it has six different apps for six different casino game providers! This is to ensure that you will get the best mobile casino experience! You can choose from Joker123, Sky3888, AllBet, XPro Gaming, PlayTech or Asia Gaming mobile casinos to download into your phones. HOwever, not all of them provide native Android and iOS app; XPro Gaming and PlayTech only has Android apps so iPhone and iPad users will need to download the mobile casinos from other game providers.</p>
    <p>In order to play on these mobile casinos, you still need to have an Infiniwin account. If you already have one, you must have your fund already deposited into that account. Infiniwin is able to receive deposits from four different banks which are Maybank, CIMB, Public Bank and Hong Leong Bank. </p>
    <p>Infiniwin has been the main player in the gambling industry and they are famous for their short processing times. For deposits, you can expect around 3 minutes processing time before the amount is transferred into your Infiniwin account. For withdraws, expect a longer processing time of around 10 minutes. This is understandable because Infiniwin will do some extra steps and validations to ensure that the money went to the right hands.</p>
    <p>No matter what mobile casino games that you play; slots, pokers, blackjacks, sportsbook or even roulettes, Infiniwin is the best choice out there. They provide the best gaming and gambling experience to the players and you will surely love what you are seeing!</p>
</div>
@endif
<!--Mobile-->
@stop

@section('bottom_js')
<script type="text/javascript" src="{{url()}}/front/resources/js/qrcode.js"></script>

<script src="{{url()}}/front/resources/js/slick.min.js"></script>
<script>
	$(function () {
	    $("#content div").hide();
	    $("#content div:eq(0)").show();

		$(".tut-btn").click(function () {
		    var tutId = $(this).data("tut-id");

		    if (typeof tutId == "undefined" || tutId.length < 1) {
		        tutId = $("#content div:eq(0)").attr("id");
			}

			console.log("#content #" + tutId);

            $("#content div").hide();
            $("#content #" + tutId).show();
		});
	});

$('.caro').slick({
  infinite: true,
  slidesToShow: 2,
  slidesToScroll: 2,
  autoplay: true,
  variableWidth: true,
  arrows: false
});
</script>
<script type="text/javascript">
@for ($i=1; $i<=13; $i++)
var qrcode = new QRCode(document.getElementById("printQR{{$i}}"), {
	width : 200,
	height : 200
});

function makeCode{{$i}} () {
	var elText = document.getElementById("getLink{{$i}}");

	if (!elText.value) {
		alert("Input a text");
		elText.focus();
		return;
	}

	qrcode.makeCode(elText.value);
}

makeCode{{$i}}();

$("#getLink{{$i}}").
	on("blur", function () {
		makeCode{{$i}}();
	}).
	on("keydown", function (e) {
		if (e.keyCode == 13) {
			makeCode{{$i}}();
		}
	});
@endfor
</script>
@stop