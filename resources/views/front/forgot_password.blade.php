@extends('front/master')

@if (Session::get('currency') == 'TWD')
    @section('title', '無極限娛樂網')
@elseif (Session::get('currency') == 'MYR')
    @section('title', 'Best Online Live Casino Malaysia - Password')
    @section('keywords', 'Best Online Live Casino Malaysia - Password')
    @section('description', 'Forgot your password? Please provide the username or email address that you used when you signed up for your InfiniWin Online Casino Malaysia account.')
@endif

@section('top_js')
  <script>
  function forgotpassword_submit(){

	$.ajax({
		type: "POST",
		url: "{{route('resetpassword')}}",
		data: {
			_token: "{{ csrf_token() }}",
			username:		 $('#fg_username').val(),
			email:    		 $('#fg_email').val(),

		},
	}).done(function( json ) {
			 $('.acctTextReg').html('');
			 obj = JSON.parse(json);
			 var str = '';
			 $.each(obj, function(i, item) {
				if( '{{Lang::get('COMMON.SUCESSFUL')}}' == item ){
					window.location.href = "{{route('homepage')}}";
				}else{
					$('.'+i+'_acctTextReg').html(item);
					
				}
			})
	
	});
}
  </script>
@stop

@section('content')
<div class="midSect">
   <div class="midSectInner">
     <div class="spacingTop"></div>
    
     <h2> {{Lang::get('public.ForgotPassword')}}</h2> 
	 
     <p>
		  {{Lang::get('public.ForgotPasswordContent2')}}
		  <br><br>
     </p>
	 
     <div class="acctContentReg">
         <P>Please Contact Live Chat! <a href="#" onclick="livechat()" style="color: #f5f015;">Click here.</a></P>
     </div>
   </div>
</div>
@stop

@section('bottom_js')

@stop