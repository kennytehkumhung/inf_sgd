<div class="midCinner2">
    <div class="detWrap bdrBottom">
        <div class="detImg">
            <img src="{{ asset('/front/mobile/img/main-wallet-icon.jpg') }}" alt=""/>
        </div>

        <div class="detList3">
            <span class="tp2">{{ Lang::get('public.MainWallet') }}</span>
        </div>

        <div class="amt1">
            <span class="main_wallet tp2">0.00</span>
        </div>


        <div class="clr"></div>
    </div>

    @foreach( Session::get('products_obj') as $prdid => $object)
        <?php
        $showPrd = true;
        if (Session::get('currency') == 'VND' && ($object->code == 'JOK')) {
            $showPrd = false;
        }
        ?>
        @if ($showPrd)
            <div class="detWrap bdrBottom">
                <div class="detImg">
                    <img class="game_icon" data-game-code="{{$object->code}}" src="" alt=""/>
                </div>

                <div class="detList3">
                    <span class="tp2">{{$object->name}}</span>
                </div>

                <div class="amt1">
                    <span class="{{$object->code}}_balance tp2">0.00</span>
                    <input type="hidden" class="{{$object->code}}_balance transferable_game_balance" data-game-code="{{$object->code}}" value="0">
                </div>

                <div class="clr"></div>
            </div>
        @endif
    @endforeach

    <div class="detWrap">
        <div class="detImg">
            <img src="{{ asset('/front/mobile/img/total-icon.jpg') }}" alt=""/>
        </div>

        <div class="detList3">
            <span class="tp2">{{ Lang::get('public.Total') }}</span>
        </div>

        <div class="amt1">
            <span id="total_balance" class="wallTotal main_wallet tp2">0.00</span>
        </div>

        <div class="clr"></div>
    </div>

    <script type="text/javascript">
        var iconset = {
            IBCX: "{{ asset('/front/mobile/img/sp-icon.jpg') }}",
            PSB: "{{ asset('/front/mobile/img/4d-icon.jpg') }}",
            W88: "{{ asset('/front/mobile/img/pt-icon.jpg') }}",
            PLT: "{{ asset('/front/mobile/img/ptc-icon.jpg') }}",
            AGG: "{{ asset('/front/mobile/img/ag-icon.jpg') }}",
            MXB: "{{ asset('/front/mobile/img/spd-icon.jpg') }}",
            ALB: "{{ asset('/front/mobile/img/allbet-icon.jpg') }}",
            SPG: "{{ asset('/front/mobile/img/spade-icon.jpg') }}",
            CTB: "{{ asset('/front/mobile/img/ctb-icon.png') }}",
            VGS: "{{ asset('/front/mobile/img/ev-icon.png') }}",
            HOG: "{{ asset('/front/mobile/img/ho-icon.png') }}",
            JOK: "{{ asset('/front/mobile/img/jok-icon.png') }}",
            OPU: "{{ asset('/front/mobile/img/ops-icon.png') }}",
            SKY: "{{ asset('/front/mobile/img/sky-icon.png') }}"
        };

        $(".game_icon").each(function () {
            var obj = $(this);
            var code = obj.data("game-code");

            if (iconset.hasOwnProperty(code)) {
                obj.attr("src", iconset[code]);
            } else {
                obj.attr("src", "{{ asset('/front/mobile/img/total-icon.jpg') }}");
            }
        });
    </script>

</div>
