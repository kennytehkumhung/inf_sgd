@section('js')
    @parent

    <script>
        var transferringToMain = false;

        function transferToMain() {
            if (!transferringToMain) {
                if (confirm("{{ Lang::get('public.AreYouSureYouWanToTransferAllGameBalancesToMainWallet') }}")) {
                    transferringToMain = true;

                    $(".transfer_to_main_icon").hide();
                    $(".transfer_to_main_loading").show();

                    var gameBalances = {};
                    var lastGameCode = "";

                    $(".transferable_game_balance").each(function () {
                        try {
                            var gameCode = $(this).data("game-code");
                            var gameBalance = parseFloat($(this).val());

                            if (!isNaN(gameBalance) && gameBalance > 0) {
                                gameBalances[gameCode] = gameBalance;
                                lastGameCode = gameCode;
                            }
                        } catch (e) {
                            // Do nothing.
                        }
                    });

                    if (lastGameCode != "") {
                        for (var key in gameBalances) {
                            if (gameBalances.hasOwnProperty(key)) {
                                submitTransfer(key, gameBalances[key], lastGameCode);
                            }
                        }
                    } else {
                        resetTransferStatus();
                    }
                }
            }
        }

        function submitTransfer(gameCode, gameBalance, lastGameCode) {
            $.ajax({
                type: "POST",
                url: "{{route('transfer-process')}}",
                data: {
                    _token: "{{ csrf_token() }}",
                    amount: gameBalance,
                    from: gameCode,
                    to: "MAIN"
                },
                complete: function (jqXHR, textStatus) {
                    if (gameCode == lastGameCode) {
                        resetTransferStatus();
                    }
                }
            });
        }

        function resetTransferStatus() {
            transferringToMain = false;

            $(".transfer_to_main_loading").hide();
            $(".transfer_to_main_icon").show();

            getBalance(true);
        }
    </script>
@endsection

<div class="detWrapN">
    <div class="detImg" onclick="window.location.href='{{ route('update-profile') }}';">
        <img src="{{ asset('/front/mobile/img/username-icon.jpg') }}" alt=""/>
    </div>
    <div class="detList3" onclick="window.location.href='{{ route('update-profile') }}';">
        <span class="tp1">{{ Session::get('username') }}</span>
    </div>
    <div class="spinner">
        <a href="javascript:void(0);" onclick="getBalance(1);">
            <img src="{{ asset('/front/mobile/img/spinner.png') }}">
        </a>
    </div>

    <div class="clr"></div>
</div>

<div class="fig1">
    <div class="txt1">{{ Lang::get('public.MainWallet') }} <span class="main_wallet">0.00</span></div>

    <div class="txt2">
        <a class="transfer_to_main_icon" href="javascript:void(0);" onclick="transferToMain();">{{ Lang::get('public.TransferToMain') }}</a>
        <img class="transfer_to_main_loading" src="{{ asset('/front/img/ajax-loading.gif') }}" width="28" height="28" style="display: none;" />
    </div>
    <div class="clr"></div>
</div>

<div class="tabH">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td id="thead_transfer" onclick="window.location.href = '{{ route('transfer') }}';" style="cursor: pointer; width: 33%;">
                <img src="{{ asset('/front/mobile/img/trsf-icon.jpg') }}" alt="" style="height: 26px; width: 26px;" />
                <span>{{ strtoupper(Lang::get('public.Transfer')) }}</span>
            </td>
            <td id="thead_deposit" onclick="window.location.href = '{{ route('deposit') }}';" style="cursor: pointer; width: 33%;">
                <img src="{{ asset('/front/mobile/img/dep-icon.jpg') }}" alt="" style="height: 26px; width: 26px;" />
                <span>{{ strtoupper(Lang::get('public.Deposit')) }}</span>
            </td>
            <td id="thead_withdraw" onclick="window.location.href = '{{ route('withdraw') }}';" style="cursor: pointer; width: 33%;">
                <img src="{{ asset('/front/mobile/img/withdraw-icon.jpg') }}" alt="" style="height: 26px; width: 26px;" />
                <span>{{ strtoupper(Lang::get('public.Withdrawal')) }}</span>
            </td>
        </tr>
    </table>
</div>

<div class="detWrap2">
    <?php $prdCount = count(Session::get('products_obj')); ?>
    <?php $runCount = 1; ?>
    <?php $colSplited = false; ?>

    <div class="trsfTblLeft" style="width: 49%; border-right: #615c49 1px solid;">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td width="75" align="center">{{ Lang::get('public.MainWallet') }}</td>
                <td width="50" align="center"><strong><span class="main_wallet">0.00</span></strong></td>
            </tr>
            @foreach ( Session::get('products_obj') as $prdid => $object )
                <?php
                $showPrd = true;
                if (Session::get('currency') == 'VND' && ($object->code == 'JOK')) {
                    $showPrd = false;
                }
                ?>
                @if ($showPrd)
                    <tr>
                        <td width="75" align="center">{{ $object->name }}</td>
                        <td width="50" align="center">
                            <strong><span class="{{$object->code}}_balance">0.00</span></strong>
                            <input type="hidden" class="{{$object->code}}_balance transferable_game_balance" data-game-code="{{$object->code}}" value="0">
                        </td>
                    </tr>
                @endif

                <?php $runCount++; ?>

                @if (!$colSplited && $runCount > $prdCount / 2)
        </table>
    </div>
    <div class="trsfTblRight" style="width: 49%;">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <?php $colSplited = true; ?>
            @endif
            @endforeach
        </table>
    </div>

    <div class="clr"></div>
</div>
