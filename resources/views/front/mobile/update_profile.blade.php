@extends('front/mobile/master')

@section('title', 'Profile')

@section('js')
    @parent

    <script src="{{ asset('/front/resources/js/jquery-ui.js') }}"></script>
    <script>
        $(function () {
            $(".datepicker").datepicker({dateFormat: 'yy-mm-dd', defaultDate: new Date()});

            load_profile();
        });

        // Update password.
        function checkPass() {
            //Store the password field objects into variables ...
            var new_password = document.getElementById('new_password');
            var confirm_new_password = document.getElementById('confirm_new_password');
            //Store the Confimation Message Object ...
            var message = document.getElementById('confirmMessage');
            //Set the colors we will be using ...
            var goodColor = "#66cc66";
            var badColor = "#ff6666";
            //Compare the values in the password field and the confirmation field
            if (new_password.value == confirm_new_password.value) {
                //The passwords match.
                //Set the color to the good color and inform the user that they have entered the correct password
                confirm_new_password.style.backgroundColor = goodColor;
                message.style.color = goodColor;
                message.innerHTML = "Passwords Match!"
            } else {
                //The passwords do not match.
                //Set the color to the bad color and notify the user.
                confirm_new_password.style.backgroundColor = badColor;
                message.style.color = badColor;
                message.innerHTML = "Passwords Do Not Match!"
            }
        }

        function update_password() {
            $.ajax({
                type: "POST",
                url: "{{action('User\MemberController@ChangePassword')}}",
                data: {
                    _token: "{{ csrf_token() }}",
                    current_password: $('#current_password').val(),
                    new_password: $('#new_password').val(),
                    confirm_new_password: $('#confirm_new_password').val()
                },
            }).done(function (json) {
                obj = JSON.parse(json);
                var str = '';
                $.each(obj, function (i, item) {
                    str += item + '\n';
                });
                if (str != '') {
                    alert(str);
                }
            });
        }
        // End update password.

        // Update personal info.
        function load_profile() {
            $.ajax({
                url: "{{ route('update-profile') }}",
                type: "GET",
                dataType: "JSON",
                data: {
                    _token: "{{ csrf_token() }}",
                    as_json: true
                },
                success: function (result) {
                    if (typeof result != undefined && result != null) {
                        $("#profile_email").val(result.email);
                        $("#profile_fullname").val(result.fullname);
                        $("#profile_telmobile").val(result.telmobile);
                        $("#profile_gender").val(result.gender);
                        $("#profile_address").val(result.resaddress);
                        $("#profile_city").val(result.rescity);
                        $("#profile_dob_hdn").val(result.dob);
                        $("#profile_referral_id").val(result.referralid);

                        $("#profile_referral_id_link").attr("href", "{{ route('register') }}?referralid=" + result.referralid);

                        if (result.dob == "0000-00-00") {
                            $("#profile_dob_writeable").show();
                            $("#profile_dob_readonly").hide();
                        } else {
                            $("#profile_dob_writeable").hide();
                            $("#profile_dob_readonly").show();
                            $("#profile_dob").val(result.dob);
                        }
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    setTimeout(function () {
                        load_profile();
                    }, 1000);
                }
            });
        }

        function update_profile() {
            var dob = $("#profile_dob_hdn").val();
            if (typeof $('#dob_year').val() != undefined) {
                dob = $('#dob_year').val() + '-' + $('#dob_month').val() + '-' + $('#dob_day').val();
            }
            $.ajax({
                type: "POST",
                url: "{{action('User\MemberController@UpdateDetail')}}",
                data: {
                    _token: "{{ csrf_token() }}",
                    gender: $('#profile_gender').val(),
                    dob: dob,
                    address: $("#profile_address").val(),
                    city: $("#profile_city").val()
                }
            }).done(function (json) {
                obj = JSON.parse(json);
                var str = '';
                $.each(obj, function (i, item) {
                    str += item + '\n';
                });
                if (str != '') {
                    alert(str);
                }
            });
        }
        // End update personal info.
    </script>
@endsection

@section('content')
    <div class="antitle">
        {{ Lang::get('public.ChangePassword') }}
    </div>

    <div class="midC" style="padding-bottom: 0px;">
        <div class="midCinner2">
            <div class="detWrap">
                <div class="detList1">
                    <input type="password" id="current_password" name="current_password" placeholder="{{ Lang::get('public.CurrentPassword') }}*">
                </div>

                <div class="clr"></div>
            </div>

            <div class="detWrap">
                <div class="detList1">
                    <input type="password" id="new_password" name="new_password" placeholder="{{ Lang::get('public.NewPassword') }}*">
                    <p>{{ Lang::get('public.NewPasswordNote2') }}</p>
                </div>

                <div class="clr"></div>
            </div>

            <div class="detWrap">
                <div class="detList1">
                    <input type="password" id="confirm_new_password" name="confirm_new_password" onkeyup="checkPass(); return false;" placeholder="{{ Lang::get('public.ReenterPassword') }}*">
                    <span id="confirmMessage" class="confirmMessage tp1 clr2"></span>
                </div>

                <div class="clr"></div>
            </div>

            <div class="nd1">
                <div class="submitC">
                    <a href="javascript:void(0);" onclick="update_password();">{{ Lang::get('public.Submit') }}</a>
                </div>

                <div class="clr"></div>
            </div>
        </div>
    </div>

    <hr>

    <div class="antitle">
        {{ Lang::get('COMMON.PERSONALINFO') }}
    </div>

    <div class="midC">
        <div class="midCinner2">

            <div class="detWrap bdrBottom">
                <div class="detImg">
                    <img src="{{ asset('/front/mobile/img/email-2-icon.png') }}" alt=""/>
                </div>

                <div class="detList5">
                    <span class="tp1">{{ Lang::get('public.Email') }}</span>
                    <input type="text" id="profile_email" readonly />
                </div>

                <div class="clr"></div>
            </div>

            <div class="detWrap bdrBottom">
                <div class="detImg">
                    <img src="{{ asset('/front/mobile/img/fullname-2-icon.png') }}" alt=""/>
                </div>

                <div class="detList5">
                    <span class="tp1">{{ Lang::get('public.FullName') }}</span>
                    <input type="text" id="profile_fullname" readonly />
                </div>

                <div class="clr"></div>
            </div>

            <div class="detWrap bdrBottom">
                <div class="detImg">
                    <img src="{{ asset('/front/mobile/img/contactno-2-icon.png') }}" alt=""/>
                </div>

                <div class="detList5">
                    <span class="tp1">{{ Lang::get('public.MobileNumber') }}</span>
                    <input type="text" id="profile_telmobile" readonly />
                </div>

                <div class="clr"></div>
            </div>

            <div class="detWrap bdrBottom">
                <div class="detImg">
                    <img src="{{ asset('/front/mobile/img/edit.png') }}" alt=""/>
                </div>

                <div class="detList5">
                    <span class="tp1">{{ Lang::get('public.Gender') }}</span>
                    <select id="profile_gender" name="gender">
                        <option value="1">{{ Lang::get('public.Male') }}</option>
                        <option value="2">{{ Lang::get('public.Female') }}</option>
                    </select>
                </div>

                <div class="clr"></div>
            </div>

            <div class="detWrap bdrBottom">
                <div class="detImg">
                    <img src="{{ asset('/front/mobile/img/edit.png') }}" alt=""/>
                </div>

                <div class="detList5">
                    <span class="tp1">{{ Lang::get('public.DOB') }}</span>
                    <input type="hidden" id="profile_dob_hdn" />
                    <div id="profile_dob_writeable" class="col-xs-3">
                        <select id="dob_day">
                            @foreach (\App\Models\AccountDetail::getDobDayOptions() as $key => $val)
                                <option value="{{ $key }}">{{ $val }}</option>
                            @endforeach
                        </select>
                        <select id="dob_month">
                            @foreach (\App\Models\AccountDetail::getDobMonthOptions() as $key => $val)
                                <option value="{{ $key }}">{{ $val }}</option>
                            @endforeach
                        </select>
                        <select id="dob_year">
                            @foreach (\App\Models\AccountDetail::getDobYearOptions() as $key => $val)
                                <option value="{{ $key }}">{{ $val }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div id="profile_dob_readonly" class="col-xs-3">
                        <input type="text" id="profile_dob" readonly />
                    </div>
                </div>

                <div class="clr"></div>
            </div>

            <div class="detWrap bdrBottom">
                <div class="detImg">
                    <img src="{{ asset('/front/mobile/img/edit.png') }}" alt=""/>
                </div>

                <div class="detList5">
                    <span class="tp1">{{ Lang::get('COMMON.ADDRESS') }}</span>
                    <input type="text" id="profile_address" />
                </div>

                <div class="clr"></div>
            </div>

            <div class="detWrap bdrBottom">
                <div class="detImg">
                    <img src="{{ asset('/front/mobile/img/edit.png') }}" alt=""/>
                </div>

                <div class="detList5">
                    <span class="tp1">{{ Lang::get('COMMON.CITY') }}</span>
                    <input type="text" id="profile_city" />
                </div>

                <div class="clr"></div>
            </div>

            <div class="nd1">
                <div class="submitC">
                    <a href="javascript:void(0);" onclick="update_profile();">{{ Lang::get('public.Submit') }}</a>
                </div>

                <div class="clr"></div>
            </div>
        </div>
    </div>
@endsection
