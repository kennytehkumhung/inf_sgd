@extends('front/mobile/master')

@section('title', 'Slot')

@section('js')
    @parent

    <script type="text/javascript">

        var game = "{{ request()->type }}";
        var gameUrls = {
            plt: "{{route('plt' , [ 'type' => 'pgames' , 'category' => '1' ] )}}",
            spg: "{{route('spg', [ 'type' => 'slot' , 'category' => 'slot' ] )}}"
        };

        if (game != '' && gameUrls.hasOwnProperty(game)) {
            window.location.href = gameUrls[game];
        }
    </script>
@endsection
