@extends('front/mobile/master')

@if (Session::get('currency') == 'TWD')
    @section('title', '無極限娛樂網')
@elseif (Session::get('currency') == 'MYR')
    @section('title', 'Play Live Fortune 4D Malaysia Online Betting')
    @section('keywords', 'Play Live Fortune 4D Malaysia Online Betting')
    @section('description', 'Play Live 4D at InfiniWin Online Casino Malaysia - awarded Best Online Casino and enjoy 100% Welcome Deposit Bonus. Join InfiniWin casino online today.')
@endif

@section('css')
    @parent

    <style type="text/css">
        .res_table_holder {
            border: #5e5c47 1px solid;
        }

        .res_table {
            width: 100%;
        }

        .res_table tr td {
            background-color: #ffffff;
            border: #5e5c47 1px solid;
            text-align: center;
        }

        .res_title {
            background-color: #8d8a7b !important;
            font-weight: bold;
        }

        .res_prize_name {
            background-color: #b0ad9d !important;
        }

        .res_blank {
            background-color: #999999 !important;
        }
    </style>
@endsection

@section('content')

    <div class="antitle">
        {{ Lang::get('public.d4') }}
    </div>

    <div class="midC" style="padding-bottom: 90px;">
        <a onClick="@if (Auth::user()->check())window.open('{{route('psb')}}', 'pubContent');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif" href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('betbtn','','{{asset('/front/img/bet-btn-hover.png')}}',0)"><img src="{{asset('/front/img/bet-btn.png')}}" alt="" width="222" height="45" id="betbtn"></a>

        <hr>

        <!--magnum-->
        <div class="res_table_holder">
            <table class="res_table" border="0" cellpadding="2" cellspacing="0">
                <tr>
                    <td colspan="3" class="res_title">MAGNUM 4D ({{ $Magnum['date'] }})</td>
                </tr>
                <tr>
                    <td class="res_prize_name">1st</td>
                    <td class="res_prize_name">2nd</td>
                    <td class="res_prize_name">3rd</td>
                </tr>
                <tr>
                    <td><span>{{$Magnum['first']}}</span></td>
                    <td><span>{{$Magnum['second']}}</span></td>
                    <td><span>{{$Magnum['third']}}</span></td>
                </tr>
            </table>
            <table cellpadding="2" cellspacing="0" class="res_table normal_price">
                <tr>
                    <td colspan="3" class="res_prize_name">Special</td>
                </tr>
                <tr>
                    <td><span>{{$Magnum['special'][0]}}</span></td>
                    <td><span>{{$Magnum['special'][1]}}</span></td>
                    <td><span>{{$Magnum['special'][2]}}</span></td>
                </tr>
                <tr>
                    <td><span>{{$Magnum['special'][3]}}</span></td>
                    <td><span>{{$Magnum['special'][4]}}</span></td>
                    <td><span>{{$Magnum['special'][5]}}</span></td>
                </tr>
                <tr>
                    <td><span>{{$Magnum['special'][6]}}</span></td>
                    <td><span>{{$Magnum['special'][7]}}</span></td>
                    <td><span>{{$Magnum['special'][8]}}</span></td>
                </tr>
                <tr>
                    <td class="res_blank">&nbsp;</td>
                    <td><span>{{$Magnum['special'][9]}}</span></td>
                    <td class="res_blank">&nbsp;</td>
                </tr>
            </table>
            <table border="0" cellpadding="2" cellspacing="0" class="res_table normal_price">
                <tr>
                    <td colspan="3" class="res_prize_name">Consolation</td>
                </tr>
                <tr>
                    <td><span>{{$Magnum['consolation'][0]}}</span></td>
                    <td><span>{{$Magnum['consolation'][1]}}</span></td>
                    <td><span>{{$Magnum['consolation'][2]}}</span></td>
                </tr>
                <tr>
                    <td><span>{{$Magnum['consolation'][3]}}</span></td>
                    <td><span>{{$Magnum['consolation'][4]}}</span></td>
                    <td><span>{{$Magnum['consolation'][5]}}</span></td>
                </tr>
                <tr>
                    <td><span>{{$Magnum['consolation'][6]}}</span></td>
                    <td><span>{{$Magnum['consolation'][7]}}</span></td>
                    <td><span>{{$Magnum['consolation'][8]}}</span></td>
                </tr>
                <tr>
                    <td class="res_blank">&nbsp;</td>
                    <td><span>{{$Magnum['consolation'][9]}}</span></td>
                    <td class="res_blank">&nbsp;</td>
                </tr>
            </table>
        </div>
        <!--end magnum-->

        <hr>

        <!--damacai-->
        <div class="res_table_holder">
            <table class="res_table" border="0" cellpadding="2" cellspacing="0">
                <tr>
                    <td colspan="3" class="res_title">DAMACAI 1+3D ({{ $PMP['date'] }})</td>
                </tr>
                <tr>
                    <td class="res_prize_name">1st</td>
                    <td class="res_prize_name">2nd</td>
                    <td class="res_prize_name">3rd</td>
                </tr>
                <tr>
                    <td><span>{{$PMP['first']}}</span></td>
                    <td><span>{{$PMP['second']}}</span></td>
                    <td><span>{{$PMP['third']}}</span></td>
                </tr>
            </table>
            <table cellpadding="2" cellspacing="0" class="res_table normal_price">
                <tr>
                    <td colspan="3" class="res_prize_name">Special</td>
                </tr>
                <tr>
                    <td><span>{{$PMP['special'][0]}}</span></td>
                    <td><span>{{$PMP['special'][1]}}</span></td>
                    <td><span>{{$PMP['special'][2]}}</span></td>
                </tr>
                <tr>
                    <td><span>{{$PMP['special'][3]}}</span></td>
                    <td><span>{{$PMP['special'][4]}}</span></td>
                    <td><span>{{$PMP['special'][5]}}</span></td>
                </tr>
                <tr>
                    <td><span>{{$PMP['special'][6]}}</span></td>
                    <td><span>{{$PMP['special'][7]}}</span></td>
                    <td><span>{{$PMP['special'][8]}}</span></td>
                </tr>
                <tr>
                    <td class="res_blank">&nbsp;</td>
                    <td><span>{{$PMP['special'][9]}}</span></td>
                    <td class="res_blank">&nbsp;</td>
                </tr>
            </table>
            <table border="0" cellpadding="2" cellspacing="0" class="res_table normal_price">
                <tr>
                    <td colspan="3" class="res_prize_name">Consolation</td>
                </tr>
                <tr>
                    <td><span>{{$PMP['consolation'][0]}}</span></td>
                    <td><span>{{$PMP['consolation'][1]}}</span></td>
                    <td><span>{{$PMP['consolation'][2]}}</span></td>
                </tr>
                <tr>
                    <td><span>{{$PMP['consolation'][3]}}</span></td>
                    <td><span>{{$PMP['consolation'][4]}}</span></td>
                    <td><span>{{$PMP['consolation'][5]}}</span></td>
                </tr>
                <tr>
                    <td><span>{{$PMP['consolation'][6]}}</span></td>
                    <td><span>{{$PMP['consolation'][7]}}</span></td>
                    <td><span>{{$PMP['consolation'][8]}}</span></td>
                </tr>
                <tr>
                    <td class="res_blank">&nbsp;</td>
                    <td><span>{{$PMP['consolation'][9]}}</span></td>
                    <td class="res_blank">&nbsp;</td>
                </tr>
            </table>
        </div>
        <!--end damacai-->

        <hr>

        <!--toto-->
        <div class="res_table_holder">
            <table class="res_table" border="0" cellpadding="2" cellspacing="0">
                <tr>
                    <td colspan="3" class="res_title">TOTO 4D ({{ $Toto['date'] }})</td>
                </tr>
                <tr>
                    <td class="res_prize_name">1st</td>
                    <td class="res_prize_name">2nd</td>
                    <td class="res_prize_name">3rd</td>
                </tr>
                <tr>
                    <td><span>{{$Toto['first']}}</span></td>
                    <td><span>{{$Toto['second']}}</span></td>
                    <td><span>{{$Toto['third']}}</span></td>
                </tr>
            </table>
            <table cellpadding="2" cellspacing="0" class="res_table normal_price">
                <tr>
                    <td colspan="3" class="res_prize_name">Special</td>
                </tr>
                <tr>
                    <td><span>{{$Toto['special'][0]}}</span></td>
                    <td><span>{{$Toto['special'][1]}}</span></td>
                    <td><span>{{$Toto['special'][2]}}</span></td>
                </tr>
                <tr>
                    <td><span>{{$Toto['special'][3]}}</span></td>
                    <td><span>{{$Toto['special'][4]}}</span></td>
                    <td><span>{{$Toto['special'][5]}}</span></td>
                </tr>
                <tr>
                    <td><span>{{$Toto['special'][6]}}</span></td>
                    <td><span>{{$Toto['special'][7]}}</span></td>
                    <td><span>{{$Toto['special'][8]}}</span></td>
                </tr>
                <tr>
                    <td class="res_blank">&nbsp;</td>
                    <td><span>{{$Toto['special'][9]}}</span></td>
                    <td class="res_blank">&nbsp;</td>
                </tr>
            </table>
            <table border="0" cellpadding="2" cellspacing="0" class="res_table normal_price">
                <tr>
                    <td colspan="3" class="res_prize_name">Consolation</td>
                </tr>
                <tr>
                    <td><span>{{$Toto['consolation'][0]}}</span></td>
                    <td><span>{{$Toto['consolation'][1]}}</span></td>
                    <td><span>{{$Toto['consolation'][2]}}</span></td>
                </tr>
                <tr>
                    <td><span>{{$Toto['consolation'][3]}}</span></td>
                    <td><span>{{$Toto['consolation'][4]}}</span></td>
                    <td><span>{{$Toto['consolation'][5]}}</span></td>
                </tr>
                <tr>
                    <td><span>{{$Toto['consolation'][6]}}</span></td>
                    <td><span>{{$Toto['consolation'][7]}}</span></td>
                    <td><span>{{$Toto['consolation'][8]}}</span></td>
                </tr>
                <tr>
                    <td class="res_blank">&nbsp;</td>
                    <td><span>{{$Toto['consolation'][9]}}</span></td>
                    <td class="res_blank">&nbsp;</td>
                </tr>
            </table>
        </div>
        <!--end toto-->

        <hr>

        <!--singapore-->
        <div class="res_table_holder">
            <table class="res_table" border="0" cellpadding="2" cellspacing="0">
                <tr>
                    <td colspan="3" class="res_title">SINGAPORE 4D ({{ $Singapore['date'] }})</td>
                </tr>
                <tr>
                    <td class="res_prize_name">1st</td>
                    <td class="res_prize_name">2nd</td>
                    <td class="res_prize_name">3rd</td>
                </tr>
                <tr>
                    <td><span>{{$Singapore['first']}}</span></td>
                    <td><span>{{$Singapore['second']}}</span></td>
                    <td><span>{{$Singapore['third']}}</span></td>
                </tr>
            </table>
            <table cellpadding="2" cellspacing="0" class="res_table normal_price">
                <tr>
                    <td colspan="3" class="res_prize_name">Special</td>
                </tr>
                <tr>
                    <td><span>{{$Singapore['special'][0]}}</span></td>
                    <td><span>{{$Singapore['special'][1]}}</span></td>
                    <td><span>{{$Singapore['special'][2]}}</span></td>
                </tr>
                <tr>
                    <td><span>{{$Singapore['special'][3]}}</span></td>
                    <td><span>{{$Singapore['special'][4]}}</span></td>
                    <td><span>{{$Singapore['special'][5]}}</span></td>
                </tr>
                <tr>
                    <td><span>{{$Singapore['special'][6]}}</span></td>
                    <td><span>{{$Singapore['special'][7]}}</span></td>
                    <td><span>{{$Singapore['special'][8]}}</span></td>
                </tr>
                <tr>
                    <td class="res_blank">&nbsp;</td>
                    <td><span>{{$Singapore['special'][9]}}</span></td>
                    <td class="res_blank">&nbsp;</td>
                </tr>
            </table>
            <table border="0" cellpadding="2" cellspacing="0" class="res_table normal_price">
                <tr>
                    <td colspan="3" class="res_prize_name">Consolation</td>
                </tr>
                <tr>
                    <td><span>{{$Singapore['consolation'][0]}}</span></td>
                    <td><span>{{$Singapore['consolation'][1]}}</span></td>
                    <td><span>{{$Singapore['consolation'][2]}}</span></td>
                </tr>
                <tr>
                    <td><span>{{$Singapore['consolation'][3]}}</span></td>
                    <td><span>{{$Singapore['consolation'][4]}}</span></td>
                    <td><span>{{$Singapore['consolation'][5]}}</span></td>
                </tr>
                <tr>
                    <td><span>{{$Singapore['consolation'][6]}}</span></td>
                    <td><span>{{$Singapore['consolation'][7]}}</span></td>
                    <td><span>{{$Singapore['consolation'][8]}}</span></td>
                </tr>
                <tr>
                    <td class="res_blank">&nbsp;</td>
                    <td><span>{{$Singapore['consolation'][9]}}</span></td>
                    <td class="res_blank">&nbsp;</td>
                </tr>
            </table>
        </div>
        <!--end singapore-->

        <div class="ht1"></div>

        <!--t88-->
        <div class="res_table_holder">
            <table class="res_table" border="0" cellpadding="2" cellspacing="0">
                <tr>
                    <td colspan="3" class="res_title">SABAH 4D ({{ $Sabah['date'] }})</td>
                </tr>
                <tr>
                    <td class="res_prize_name">1st</td>
                    <td class="res_prize_name">2nd</td>
                    <td class="res_prize_name">3rd</td>
                </tr>
                <tr>
                    <td><span>{{$Sabah['first']}}</span></td>
                    <td><span>{{$Sabah['second']}}</span></td>
                    <td><span>{{$Sabah['third']}}</span></td>
                </tr>
            </table>
            <table cellpadding="2" cellspacing="0" class="res_table normal_price">
                <tr>
                    <td colspan="3" class="res_prize_name">Special</td>
                </tr>
                <tr>
                    <td><span>{{$Sabah['special'][0]}}</span></td>
                    <td><span>{{$Sabah['special'][1]}}</span></td>
                    <td><span>{{$Sabah['special'][2]}}</span></td>
                </tr>
                <tr>
                    <td><span>{{$Sabah['special'][3]}}</span></td>
                    <td><span>{{$Sabah['special'][4]}}</span></td>
                    <td><span>{{$Sabah['special'][5]}}</span></td>
                </tr>
                <tr>
                    <td><span>{{$Sabah['special'][6]}}</span></td>
                    <td><span>{{$Sabah['special'][7]}}</span></td>
                    <td><span>{{$Sabah['special'][8]}}</span></td>
                </tr>
                <tr>
                    <td class="res_blank">&nbsp;</td>
                    <td><span>{{$Sabah['special'][9]}}</span></td>
                    <td class="res_blank">&nbsp;</td>
                </tr>
            </table>
            <table border="0" cellpadding="2" cellspacing="0" class="res_table normal_price">
                <tr>
                    <td colspan="3" class="res_prize_name">Consolation</td>
                </tr>
                <tr>
                    <td><span>{{$Sabah['consolation'][0]}}</span></td>
                    <td><span>{{$Sabah['consolation'][1]}}</span></td>
                    <td><span>{{$Sabah['consolation'][2]}}</span></td>
                </tr>
                <tr>
                    <td><span>{{$Sabah['consolation'][3]}}</span></td>
                    <td><span>{{$Sabah['consolation'][4]}}</span></td>
                    <td><span>{{$Sabah['consolation'][5]}}</span></td>
                </tr>
                <tr>
                    <td><span>{{$Sabah['consolation'][6]}}</span></td>
                    <td><span>{{$Sabah['consolation'][7]}}</span></td>
                    <td><span>{{$Sabah['consolation'][8]}}</span></td>
                </tr>
                <tr>
                    <td class="res_blank">&nbsp;</td>
                    <td><span>{{$Sabah['consolation'][9]}}</span></td>
                    <td class="res_blank">&nbsp;</td>
                </tr>
            </table>
        </div>
        <!--End of t88-->

        <hr>

        <!--STC-->
        <div class="res_table_holder">
            <table class="res_table" border="0" cellpadding="2" cellspacing="0">
                <tr>
                    <td colspan="3" class="res_title">SANDAKAN 4D ({{ $Sandakan['date'] }})</td>
                </tr>
                <tr>
                    <td class="res_prize_name">1st</td>
                    <td class="res_prize_name">2nd</td>
                    <td class="res_prize_name">3rd</td>
                </tr>
                <tr>
                    <td><span>{{$Sandakan['first']}}</span></td>
                    <td><span>{{$Sandakan['second']}}</span></td>
                    <td><span>{{$Sandakan['third']}}</span></td>
                </tr>
            </table>
            <table cellpadding="2" cellspacing="0" class="res_table normal_price">
                <tr>
                    <td colspan="3" class="res_prize_name">Special</td>
                </tr>
                <tr>
                    <td><span>{{$Sandakan['special'][0]}}</span></td>
                    <td><span>{{$Sandakan['special'][1]}}</span></td>
                    <td><span>{{$Sandakan['special'][2]}}</span></td>
                </tr>
                <tr>
                    <td><span>{{$Sandakan['special'][3]}}</span></td>
                    <td><span>{{$Sandakan['special'][4]}}</span></td>
                    <td><span>{{$Sandakan['special'][5]}}</span></td>
                </tr>
                <tr>
                    <td><span>{{$Sandakan['special'][6]}}</span></td>
                    <td><span>{{$Sandakan['special'][7]}}</span></td>
                    <td><span>{{$Sandakan['special'][8]}}</span></td>
                </tr>
                <tr>
                    <td class="res_blank">&nbsp;</td>
                    <td><span>{{$Sandakan['special'][9]}}</span></td>
                    <td class="res_blank">&nbsp;</td>
                </tr>
            </table>
            <table border="0" cellpadding="2" cellspacing="0" class="res_table normal_price">
                <tr>
                    <td colspan="3" class="res_prize_name">Consolation</td>
                </tr>
                <tr>
                    <td><span>{{$Sandakan['consolation'][0]}}</span></td>
                    <td><span>{{$Sandakan['consolation'][1]}}</span></td>
                    <td><span>{{$Sandakan['consolation'][2]}}</span></td>
                </tr>
                <tr>
                    <td><span>{{$Sandakan['consolation'][3]}}</span></td>
                    <td><span>{{$Sandakan['consolation'][4]}}</span></td>
                    <td><span>{{$Sandakan['consolation'][5]}}</span></td>
                </tr>
                <tr>
                    <td><span>{{$Sandakan['consolation'][6]}}</span></td>
                    <td><span>{{$Sandakan['consolation'][7]}}</span></td>
                    <td><span>{{$Sandakan['consolation'][8]}}</span></td>
                </tr>
                <tr>
                    <td class="res_blank">&nbsp;</td>
                    <td><span>{{$Sandakan['consolation'][9]}}</span></td>
                    <td class="res_blank">&nbsp;</td>
                </tr>
            </table>
        </div>
        <!--End of STC-->

        <hr>

        <!--Cash-->
        <div class="res_table_holder">
            <table class="res_table" border="0" cellpadding="2" cellspacing="0">
                <tr>
                    <td colspan="3" class="res_title">SPECIAL BIGSWEEP ({{ $Sarawak['date'] }})</td>
                </tr>
                <tr>
                    <td class="res_prize_name">1st</td>
                    <td class="res_prize_name">2nd</td>
                    <td class="res_prize_name">3rd</td>
                </tr>
                <tr>
                    <td><span>{{$Sarawak['first']}}</span></td>
                    <td><span>{{$Sarawak['second']}}</span></td>
                    <td><span>{{$Sarawak['third']}}</span></td>
                </tr>
            </table>
            <table cellpadding="2" cellspacing="0" class="res_table normal_price">
                <tr>
                    <td colspan="3" class="res_prize_name">Special</td>
                </tr>
                <tr>
                    <td><span>{{$Sarawak['special'][0]}}</span></td>
                    <td><span>{{$Sarawak['special'][1]}}</span></td>
                    <td><span>{{$Sarawak['special'][2]}}</span></td>
                </tr>
                <tr>
                    <td><span>{{$Sarawak['special'][3]}}</span></td>
                    <td><span>{{$Sarawak['special'][4]}}</span></td>
                    <td><span>{{$Sarawak['special'][5]}}</span></td>
                </tr>
                <tr>
                    <td><span>{{$Sarawak['special'][6]}}</span></td>
                    <td><span>{{$Sarawak['special'][7]}}</span></td>
                    <td><span>{{$Sarawak['special'][8]}}</span></td>
                </tr>
                <tr>
                    <td class="res_blank">&nbsp;</td>
                    <td><span>{{$Sarawak['special'][9]}}</span></td>
                    <td class="res_blank">&nbsp;</td>
                </tr>
            </table>
            <table border="0" cellpadding="2" cellspacing="0" class="res_table normal_price">
                <tr>
                    <td colspan="3" class="res_prize_name">Consolation</td>
                </tr>
                <tr>
                    <td><span>{{$Sarawak['consolation'][0]}}</span></td>
                    <td><span>{{$Sarawak['consolation'][1]}}</span></td>
                    <td><span>{{$Sarawak['consolation'][2]}}</span></td>
                </tr>
                <tr>
                    <td><span>{{$Sarawak['consolation'][3]}}</span></td>
                    <td><span>{{$Sarawak['consolation'][4]}}</span></td>
                    <td><span>{{$Sarawak['consolation'][5]}}</span></td>
                </tr>
                <tr>
                    <td><span>{{$Sarawak['consolation'][6]}}</span></td>
                    <td><span>{{$Sarawak['consolation'][7]}}</span></td>
                    <td><span>{{$Sarawak['consolation'][8]}}</span></td>
                </tr>
                <tr>
                    <td class="res_blank">&nbsp;</td>
                    <td><span>{{$Sarawak['consolation'][9]}}</span></td>
                    <td class="res_blank">&nbsp;</td>
                </tr>
            </table>
        </div>
        <!--End of Cash-->

    </div>
@endsection
