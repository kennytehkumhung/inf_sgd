@extends('front/mobile/master')

@section('title', 'Wallet Transfer')

@section('css')
    @parent

    <style type="text/css">
        .ic-qr {
            height: 125px;
            width: 125px;
        }
		
		.remodalcus {
			color: #fff600;
			 background-color: #000000;
			
		}
    </style>
@endsection

@section('js')
    @parent

    <script>
        function remodalbox(on_url,games,version,username){
            var downloaddetail=" ";
            var url=" ";
			if(version=='Android'){
				version='<img src="{{url()}}/front/mobile/img/girl1.png" width="100%">';
				versionIcon='<img src="{{url()}}/front/mobile/img/dl_android1.png" width="100%">';
			}else if(version=='ios'){
				version='<img src="{{url()}}/front/mobile/img/apple.png" width="100%">';
				versionIcon='<img src="{{url()}}/front/mobile/img/apple1.png" width="100%">';
			}
            $("#version").html(version);
            $("#versionIcon").html(versionIcon);
            if (username=="NULL"){
                downloaddetail="Please Login!";
                $("#downloaddetail").html(downloaddetail);
            }else{
                if (games=="ag"){
                    username="AVIW_{{Session::get('username')}}";
                }
                else if (games=="pt"){
                    username="INF_{{Session::get('username')}}";
                }
                else if (games=="mxb"){
                    username="{{Session::get('username')}}".toUpperCase();
                }
                else if (games=="joker"){
                    username="{{ Config::get(Session::get('currency').'.jok.appid') }}.{{ Config::get(Session::get('currency').'.jok.prefix') }}_{{strtoupper(Session::get('username'))}}";
                }
                else if (games=="sky"){
                    username="{{Config::get(Session::get('currency').'.sky.agid').((new \App\libraries\providers\SKY())->formatUserId(Session::get('userid')))}}";
                }
                else if (games=="allb"){
                    username="{{ strtolower(Config::get(Session::get('currency').'.alb.prefix')).'_'.strtolower(Session::get('username')).(Session::get('currency') == 'TWD' ? 'dyn' : 'av2') }}";
                }
                url='<a href={{ url('appdownload') }}?url='+encodeURIComponent(on_url)+' target="_blank"><img src="{{url()}}/front/mobile/img/download.png" width="100%"></a>';
                $("#username").html(username);
                $(".modal-footer").html(url);
            }
        }
    </script>
@endsection
<div class="remodal remodalcus" data-remodal-id="downloadbox" role="dialog" aria-labelledby="modal1Title" aria-describedby="modal1Desc">
  <button data-remodal-action="close" class="remodal-close" aria-label="Close"></button>
  <div>
	<div class="modal-body">
		<div id="version"></div><br>
		<div id="downloaddetail">
			<div><img src="{{url()}}/front/mobile/img/step1.png" alt="Placeholder image"></div>
			<div>Click on the Download button.<div id="versionIcon"></div></div>
			<div style="vertical-align:middle"><img src="{{url()}}/front/mobile/img/step2.png" alt="Placeholder image"></div>
			<div>Run apps, and install the file after download.</div>
			<div style="vertical-align:middle;"><img src="{{url()}}/front/mobile/img/step3.png" alt="Placeholder image"></div>
			<div>Please use this username</div><br>
			<p id="username" style="color:white;"></p><br><br>
		</div>
	</div>
	<div class="modal-footer">
	</div>
  </div>
</div>
@section('content')
    <div class="antitle">
        {{ Lang::get('public.MobileDownload') }}
    </div>

    <div class="midC">
        <div class="midCinner2">

            <div id="m_agg" class="mobWrap">
                <div class="mobBxTit">
                    <img src="{{ asset('/front/img/mob-ag.png') }}">
                </div>

                <div class="bx5">
                    <a href="#downloadbox" onclick="remodalbox('{{$android['agg']}}','ag','Android',@if(Session::has('username'))
																										'{{Session::get('username')}}'
																									@else
																										'NULL'
																									@endif)">
                        <img class="ic-qr" src="{{ asset('/front/mobile/img/dl_android.png') }}">
                    </a>
                    <span class="mobIcon and">{{ Lang::get('public.LiveCasino') }}</span>
                </div>

                <div class="bx5">
					<a href="#downloadbox" onclick="remodalbox('{{$ios['agg']}}','ag','ios',@if(Session::has('username'))
																										'{{Session::get('username')}}'
																									@else
																										'NULL'
																									@endif)">
                        <img class="ic-qr" src="{{ asset('/front/mobile/img/dl_apple.png') }}">
                    </a>
                    <span class="mobIcon app">{{ Lang::get('public.LiveCasino') }}</span>
                </div>

                <div class="clr"></div>
            </div>

            <div id="m_plt" class="mobWrap">
                <div class="mobBxTit">
                    <img src="{{ asset('/front/mobile/img/mob-pt.png') }}">
                </div>

                <div class="bx5">
					<a href="#downloadbox" onclick="remodalbox('{{$android['pltslot']}}','pt','Android',@if(Session::has('username'))
																										'{{Session::get('username')}}'
																									@else
																										'NULL'
																									@endif)">
                        <img class="ic-qr" src="{{ asset('/front/mobile/img/dl_android.png') }}">
                    </a>
                    <span class="mobIcon and">{{ Lang::get('public.Slot') }}</span>
                </div>

                <div class="bx5">
					<a href="#downloadbox" onclick="remodalbox('{{$android['pltlive']}}','pt','Android',@if(Session::has('username'))
																										'{{Session::get('username')}}'
																									@else
																										'NULL'
																									@endif)">
                        <img class="ic-qr" src="{{ asset('/front/mobile/img/dl_android.png') }}">
                    </a>
                    <span class="mobIcon and">{{ Lang::get('public.LiveCasino') }}</span>
                </div>

                <div class="clr"></div>
            </div>

            <div id="m_mxb" class="mobWrap">
                <div class="mobBxTit">
                    <img src="{{ asset('/front/mobile/img/mob-mxb.png') }}">
                </div>

                <div class="bx5">
					<a href="#downloadbox" onclick="remodalbox('{{$android['mxb']}}','mxb','Android',@if(Session::has('username'))
																										'{{Session::get('username')}}'
																									@else
																										'NULL'
																									@endif)">
                        <img class="ic-qr" src="{{ asset('/front/mobile/img/dl_android.png') }}">
                    </a>
                    <span class="mobIcon and">{{ Lang::get('public.Slot') }}</span>
                </div>

                <div class="bx5">
					<a href="#downloadbox" onclick="remodalbox('{{$android['mxb']}}','mxb','Android',@if(Session::has('username'))
																										'{{Session::get('username')}}'
																									@else
																										'NULL'
																									@endif)">
                        <img class="ic-qr" src="{{ asset('/front/mobile/img/dl_android.png') }}">
                    </a>
                    <span class="mobIcon and">{{ Lang::get('public.LiveCasino') }}</span>
                </div>

                <div class="clr"></div>
            </div>

            <div id="m_jok" class="mobWrap">
                <div class="mobBxTit">
                    <img src="{{ asset('/front/mobile/img/mob-joker.png') }}">
                </div>

                <div class="bx5">
					<a href="#downloadbox" onclick="remodalbox('{{$android['jok']}}','joker','Android',@if(Session::has('username'))
																										'{{Session::get('username')}}'
																									@else
																										'NULL'
																									@endif)">
                        <img class="ic-qr" src="{{ asset('/front/mobile/img/dl_android.png') }}">
                    </a>
                    <span class="mobIcon and">{{ Lang::get('public.LiveCasino') }}</span>
                </div>

                <div class="bx5">
					<a href="#downloadbox" onclick="remodalbox('{{$ios['jok']}}','joker','ios',@if(Session::has('username'))
																										'{{Session::get('username')}}'
																									@else
																										'NULL'
																									@endif)">
                        <img class="ic-qr" src="{{ asset('/front/mobile/img/dl_apple.png') }}">
                    </a>
                    <span class="mobIcon app">{{ Lang::get('public.LiveCasino') }}</span>
                </div>

                <div class="clr"></div>
            </div>

            <div id="m_sky" class="mobWrap">
                <div class="mobBxTit">
                    <img src="{{ asset('/front/mobile/img/mob-sky.png') }}">
                </div>

                <div class="bx5">
					<a href="#downloadbox" onclick="remodalbox('{{$android['sky']}}','sky','Android',@if(Session::has('username'))
																										'{{Session::get('username')}}'
																									@else
																										'NULL'
																									@endif)">
                        <img class="ic-qr" src="{{ asset('/front/mobile/img/dl_android.png') }}">
                    </a>
                    <span class="mobIcon and">{{ Lang::get('public.LiveCasino') }}</span>
                </div>

                <div class="bx5">
					<a href="#downloadbox" onclick="remodalbox('{{$ios['sky']}}','sky','ios',@if(Session::has('username'))
																										'{{Session::get('username')}}'
																									@else
																										'NULL'
																									@endif)">
                        <img class="ic-qr" src="{{ asset('/front/mobile/img/dl_apple.png') }}">
                    </a>
                    <span class="mobIcon app">{{ Lang::get('public.LiveCasino') }}</span>
                </div>

                <div class="clr"></div>
            </div>

            <div id="m_alb" class="mobWrap">
                <div class="mobBxTit">
                    <img src="{{ asset('/front/mobile/img/mob-allb.png') }}">
                </div>

                <div class="bx5">
					<a href="#downloadbox" onclick="remodalbox('{{$android['alb']}}','allb','Android',@if(Session::has('username'))
																										'{{Session::get('username')}}'
																									@else
																										'NULL'
																									@endif)">
                        <img class="ic-qr" src="{{ asset('/front/mobile/img/dl_android.png') }}">
                    </a>
                    <span class="mobIcon and">{{ Lang::get('public.LiveCasino') }}</span>
                </div>

                <div class="bx5">
					<a href="#downloadbox" onclick="remodalbox('{{$ios['alb']}}','allb','ios',@if(Session::has('username'))
																										'{{Session::get('username')}}'
																									@else
																										'NULL'
																									@endif)">
                        <img class="ic-qr" src="{{ asset('/front/mobile/img/dl_apple.png') }}">
                    </a>
                    <span class="mobIcon app">{{ Lang::get('public.LiveCasino') }}</span>
                </div>

                <div class="clr"></div>
            </div>

        </div>
    </div>
@endsection
