@extends('front/mobile/master')

@if (Session::get('currency') == 'TWD')
    @section('title', '無極限娛樂網')
@elseif (Session::get('currency') == 'MYR')
    @section('title', 'Best Online Live Casino Malaysia - Password')
    @section('keywords', 'Best Online Live Casino Malaysia - Password')
    @section('description', 'Forgot your password? Please provide the username or email address that you used when you signed up for your InfiniWin Online Casino Malaysia account.')
@endif

@section('js')
    @parent

    <script>
        function forgotpassword_submit(){
            $.ajax({
                type: "POST",
                url: "{{route('resetpassword')}}",
                data: {
                    _token: "{{ csrf_token() }}",
                    username: $('#fg_username').val(),
                    email: $('#fg_email').val()
                }
            }).done(function (json) {
                $('.acctTextReg').html('');
                obj = JSON.parse(json);
                var str = '';
                $.each(obj, function (i, item) {
                    if ('{{Lang::get('COMMON.SUCESSFUL')}}' == item) {
                        window.location.href = "{{route('homepage')}}";
                    } else {
                        $('.' + i + '_acctTextReg').html(item);
                    }
                });
            });
        }
    </script>
@endsection

@section('content')
    <div class="antitle">
        {{ Lang::get('public.ForgotPassword') }}
    </div>

    <div class="midC">
        <div class="midCinner2">
            <P>Please Contact Live Chat! <a href="javascript:livechat();" style="color: #f5f015;">Click here.</a></P>
        </div>
    </div>
@endsection
