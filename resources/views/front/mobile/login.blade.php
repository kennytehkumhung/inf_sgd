@extends('front/mobile/master')

@section('title', 'Login')

@section('js')
    @parent

    <script>
        function enterpressalert(e, textarea) {
            var code = (e.keyCode ? e.keyCode : e.which);
            if(code == 13) { //Enter keycode
                login();
            }
        }

        function login() {
            var btnLogin = $("#btnLogin");

            btnLogin.text("{{ Lang::get('public.Loading') }}...");

            $.ajax({
                type: "POST",
                url: "{{route('login')}}",
                data: {
                    _token: "{{ csrf_token() }}",
                    username: $('#username').val(),
                    password: $('#password').val(),
                    code:     $('#code').val(),
                },
            }).done(function(json) {
                obj = JSON.parse(json);
                var str = '';
                $.each(obj, function(i, item) {
                    str += item + '\n';
                });
                if ("{{Lang::get('COMMON.SUCESSFUL')}}\n" == str) {
                    {{--window.location.href = "{{ route('homepage') }}";--}}
                    window.location.href = "{{ route('homepage') }}";
                } else {
                    btnLogin.text("{{ Lang::get('public.Login') }}");
                    alert(str);
                }
            });
        }
    </script>
@endsection

@section('content')
    <div class="antitle">
        {{ Lang::get('public.Login') }}
    </div>

    <div class="midC">
        <div class="midCinner2">

            <div class="detWrap">
                <div class="detList">
                    <input type="text" id="username" name="username" onKeyPress="enterpressalert(event, this);" placeholder="{{ Lang::get('public.Username') }}">
                </div>

                <div class="clr"></div>
            </div>

            <div class="detWrap">
                <div class="detList">
                    <input type="password" id="password" name="password" onKeyPress="enterpressalert(event, this);" placeholder="{{ Lang::get('public.Password') }}">
                    <input type="hidden" id="code" name="code" value="0000" />
                </div>

                <div class="clr"></div>
            </div>

            <div class="detWrap">
                <div class="detList">
                    <div class="submitB">
                        <a id="btnLogin" href="javascript:void(0);" onclick="login();">{{ Lang::get('public.Login') }}</a>
                    </div>
                </div>

                <div class="clr"></div>
            </div>

            <div class="detWrap">
                <div class="lst">
                    <a href="{{ route('forgotpassword') }}">{{ Lang::get('public.ForgotPassword') }}</a>
                </div>

                <div class="clr"></div>
            </div>

            <div class="detWrap">
                <div class="lst">
                    <a href="{{ route('register') }}">{{ Lang::get('public.RegisterNow') }}</a>
                </div>

                <div class="clr"></div>
            </div>
        </div>
    </div>
@endsection
