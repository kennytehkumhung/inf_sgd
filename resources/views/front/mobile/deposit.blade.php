@extends('front/mobile/master')

@section('title', 'Deposit')

@section('css')
    @parent

    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <style type="text/css">
        input[type=radio],
        input[type=checkbox]
        {
            height: 18px;
            width: 18px;
            margin-right: 5px;
        }
    </style>
@endsection

@section('js')
    @parent

    <script src="{{ asset('/front/resources/js/jquery-ui.js') }}"></script>
    <script>
        $(function () {
            $(".datepicker").datepicker({dateFormat: 'yy-mm-dd', defaultDate: new Date()});
            $("#thead_deposit").addClass("selected");

            load_deposit();
            getBalance(1);
        });

        // Deposit.
        function load_deposit() {
            $.ajax({
                url: "{{ route('deposit') }}",
                type: "GET",
                dataType: "JSON",
                data: {
                    _token: "{{ csrf_token() }}",
                    as_json: true
                },
                success: function (result) {
                    if (typeof result != undefined && result != null) {
                        var resultHtml = "";

                        for (var key in result.banks) {
                            if (result.banks.hasOwnProperty(key)) {
                                resultHtml += '<option value="' + result.banks[key].bnkid + '" data-min="' + result.banks[key].min + '" data-max="' + result.banks[key].max + '" data-account-name="' + result.banks[key].bankaccname + '" data-account-number="' + result.banks[key].bankaccno + '">\n\
                                    ' + result.banks[key].name + '\n\
                                    </option>';
                            }
                        }

                        $("#deposit_banklist").html(resultHtml);

                        var selectObj = $("#deposit_banklist[name='bank']");
                        selectObj.val($("option:eq(0)", selectObj).val()).change();

                        resultHtml = "";

                        for (var key in result.promos) {
                            if (result.promos.hasOwnProperty(key)) {
                                resultHtml += '<div class="col-md-12">\n\
                                    <div class="col-xs-10 cut1">\n\
                                        <label><input type="radio" name="promo" value="' + result.promos[key].code + '">' + result.promos[key].name + '</label>\n\
                                    </div>\n\
                                    <div class="promoWrap">\n\
                                        <img src="' + result.promos[key].image + '" class="object-fit_cover" style="height: auto; width: 100%;" />\n\
                                    </div>\n\
                                </div>';
                            }
                        }

                        resultHtml += '<div class="col-md-12">\n\
                            <div class="col-xs-10 cut1">\n\
                                <label><input type="radio" name="promo" value="0">{{ Lang::get('public.ProceedWithoutAnyOfThePromotionsAbove') }}</label>\n\
                            </div>\n\
                        </div>';

                        $("#deposit_promolist").html(resultHtml);
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    setTimeout(function () {
                        load_deposit();
                    }, 1000);
                }
            });
        }

        function depositBankChanged(obj) {
            var rObj = $(obj);

            if (rObj.length) {
                rObj = $("option[value='" + $(obj).val() + "']", $("#deposit_banklist[name='bank']"));

                $("#deposit_account_name").html(rObj.data("account-name"));
                $("#deposit_account_number").html(rObj.data("account-number"));
                $("#deposit_min").html("{{Session::get('currency')}} " + rObj.data("min"));
                $("#deposit_max").html("{{Session::get('currency')}} " + rObj.data("max"));
            }
        }

        function deposit_submit(){
            var file_data = $("#deposit_receipt").prop("files")[0];
            var form_data = new FormData();

            form_data.append("file", file_data);

            var data = $('#deposit_form').serializeArray();
            var obj = {};

            for (var i = 0, l = data.length; i < l; i++) {
                form_data.append(data[i].name, data[i].value);
            }

            form_data.append('_token', '{{csrf_token()}}');

            $.ajax({
                type: "POST",
                url: "{{route('deposit-process')}}?",
                dataType: 'script',
                cache: false,
                contentType: false,
                processData: false,
                data: form_data, // Setting the data attribute of ajax with file_data
                type: 'post',
                beforeSend: function() {
                    $('#deposit_sbumit_btn').attr('onclick', '');
                },
                complete: function(json){
                    obj = JSON.parse(json.responseText);
                    var str = '';
                    $.each(obj, function(i, item) {
                        if ('{{Lang::get('COMMON.SUCESSFUL')}}' == item) {
                            alert('{{Lang::get('COMMON.SUCESSFUL')}}');
                            window.location.href = "{{route('profitloss')}}";
                        } else {
                            $('#deposit_sbumit_btn').attr('onclick', 'deposit_submit()');
                            str += item + '\n';
                        }
                    });
                    if (str != '') {
                        alert(str);
                    }
                }
            });
        }
        // End deposit.
    </script>
@endsection

@section('content')
    <div class="antitle">
        {{ Lang::get('public.Deposit') }}
    </div>

    <div class="midC" style="padding-bottom: 90px;">

        @include('front.mobile.include.transfer_head')

        <div class="detWrap2 bg-wht">
            <div class="detList2">
                <span>{{ Lang::get('public.DepositSteps1b') }}</span>
            </div>

            <div class="clr"></div>
        </div>

        <form id="deposit_form">
            <div class="detWrap2 bg-wht">
                <div class="detList2">
                    <span class="tp1">{{ Lang::get('public.Bank') }}* :</span>
                    <select id="deposit_banklist" name="bank" onchange="depositBankChanged(this);"></select>
                </div>

                <div class="clr"></div>
            </div>

            <div class="detWrap2 bg-wht">
                <div class="detList2">
                    <span>{{ Lang::get('public.BankingInformation2') }}</span>
                </div>

                <div class="clr"></div>
            </div>

            <div class="detWrap2 bg-wht">
                <div class="row">
                    <table border="0">
                        <tr>
                            <td>{{ Lang::get('public.BankAccountName') }}</td>
                            <td>:&nbsp;</td>
                            <td><span id="deposit_account_name"></span></td>
                        </tr>
                        <tr>
                            <td>{{ Lang::get('public.BankAccountNo') }}</td>
                            <td>:&nbsp;</td>
                            <td><span id="deposit_account_number"></span></td>
                        </tr>
                        <tr>
                            <td>{{ Lang::get('public.MinimumAmount') }}</td>
                            <td>:&nbsp;</td>
                            <td><span id="deposit_min"></span></td>
                        </tr>
                        <tr>
                            <td>{{ Lang::get('public.MaximumAmount') }}</td>
                            <td>:&nbsp;</td>
                            <td><span id="deposit_max"></span></td>
                        </tr>
                    </table>
                </div>
            </div>

            <hr>

            <div class="detWrap2 bg-wht">
                <div class="detList2">
                    <span>{{ Lang::get('public.DepositSteps2') }}</span>
                </div>

                <div class="clr"></div>
            </div>

            <div class="detWrap2 bg-wht">
                <div class="detList2">
                    <span class="tp1">{{ Lang::get('public.Amount') }}* :</span>
                    <input type="text" id="deposit_amount" name="amount" placeholder="0.00" />
                </div>

                <div class="clr"></div>
            </div>

            <div class="detWrap2 bg-wht">
                <div class="detList2">
                    <span class="tp1">{{ Lang::get('public.DepositMethod') }}* :</span>
                    <select id="deposit_type" name="type">
                        <option value="0">{{ Lang::get('public.OverCounter') }}</option>
                        <option value="1">{{ Lang::get('public.InternetBanking') }}</option>
                        <option value="2">{{ Lang::get('public.ATMBanking') }}</option>
                    </select>
                </div>

                <div class="clr"></div>
            </div>

            <div class="detWrap2 bg-wht">
                <div class="detList2">
                    <span class="tp1">{{ Lang::get('public.DateTime') }}* :</span>
                    <input type="text" class="datepicker" id="deposit_date" style="cursor:pointer;" name="date" value="{{date('Y-m-d')}}">
                </div>

                <div class="clr"></div>
            </div>

            <div class="detWrap2 bg-wht">
                <div class="detList2">
                    <table border="0" style="width: 100%;">
                        <tr>
                            <td>
                                <select id="deposit_hours" name="hours">
                                    @for ($i = 1; $i <= 12; $i++)
                                        <option value="{{ str_pad($i, 2, '0', STR_PAD_LEFT) }}">{{ str_pad($i, 2, '0', STR_PAD_LEFT) }}</option>
                                    @endfor
                                </select>
                            </td>
                            <td>
                                <select id="deposit_minutes" name="minutes">
                                    @for ($i = 0; $i <= 59; $i++)
                                        <option value="{{ str_pad($i, 2, '0', STR_PAD_LEFT) }}">{{ str_pad($i, 2, '0', STR_PAD_LEFT) }}</option>
                                    @endfor
                                </select>
                            </td>
                            <td>
                                <select id="deposit_range" name="range">
                                    <option value="AM">AM</option>
                                    <option value="PM">PM</option>
                                </select>
                            </td>
                        </tr>
                    </table>
                </div>

                <div class="clr"></div>
            </div>

            <div class="detWrap2 bg-wht">
                <div class="detList2">
                    <span class="tp1">{{ Lang::get('public.ReferenceNo') }}* :</span>
                    <input type="text" id="deposit_refno" name="refno" />
                </div>

                <div class="clr"></div>
            </div>

            <div class="detWrap2 bg-wht">
                <div class="detList2">
                    <span class="tp1">{{ Lang::get('public.DepositReceipt') }}* :</span>
                    <input class="upload" type="file" name="receipt" id="deposit_receipt">
                </div>

                <div class="clr"></div>
            </div>

            <div class="detWrap2 bg-wht">
                <div class="detList2">
                    <div id="deposit_promolist">
                        {{ Lang::get('public.Loading') }}...
                    </div>
                </div>

                <div class="clr"></div>
            </div>

            <div class="detWrap2 bg-wht">
                <div class="detList2">
                    <div class="col-md-12">
                        <label><input type="checkbox" id="deposit_rules" name="rules" > {{ Lang::get('public.IAlreadyUnderstandRules') }}</label>
                    </div>
                </div>

                <div class="clr"></div>
            </div>
        </form>

        <div class="nd1">
            <div class="submitC">
                <a id="btn_submit_transfer" href="javascript:void(0);" onclick="deposit_submit();">{{Lang::get('public.Submit')}}</a>
                <span class="error_validation" style="color:Red;"></span>
            </div>

            <div class="clr"></div>
        </div>

    </div>
@endsection
