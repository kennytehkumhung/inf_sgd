@extends('front/mobile/master')

@if (Session::get('currency') == 'TWD')
    @section('title', '無極限娛樂網')
@elseif (Session::get('currency') == 'MYR')
    @section('title', 'The Best Online Live Casino in Malaysia')
    @section('keywords', 'The Best Online Live Casino in Malaysia')
    @section('description', 'Play at InfiniWin Online Casino Malaysia - The Best Online Casino and enjoy 100% Welcome Deposit Bonus. Join InfiniWin casino online today.')
@endif

@section('js')
    @parent

    <script>
        $(function () {
            $("body").css("background-color", "#ffffff");
        });
    </script>
@endsection

@section('content')
    @if(isset($banners))
        <div class="slider">
            <div class="sliderInner">
                <ul class="bxslider">
                    @foreach( $banners as $key => $banner )
                        <li><a target="_blank" href="@if($banner->url != 'none'){{$banner->url}}@endif"><img src="{{$banner->domain}}/{{$banner->path}}" /></a></li>
                    @endforeach
                </ul>
            </div>
        </div>
    @endif

    <div class="anmnt">
        <marquee>{{ App\Http\Controllers\User\AnnouncementController::index() }}</marquee>
    </div>

    <div class="midC">
        <div class="midCinner">
            @if(in_array('AGG',Session::get('valid_product')))
                <div class="bx1">
                    <a href="javascript:void(0);" onclick="{{ Auth::user()->check() ? 'window.open("'.route('agg').'","casino_ag");' : 'alert("'.Lang::get('COMMON.PLEASELOGIN').'");' }}">
                        <img src="{{ asset('/front/mobile/img/ag-min.png') }}">
                        <span>AG Casino</span>
                    </a>
                </div>
            @endif

            <div class="bx1">
                <a href="{{ route('playtechmobile', ['type' => 'all']) }}">
                    <img src="{{ asset('/front/mobile/img/playtech slot-min.png') }}">
                    <span>Playtech Slot</span>
                </a>
            </div>

            <div class="bx1">
                <a href="{{ route('playtechmobile', ['type' => 'live-casino']) }}">
                    <img src="{{ asset('/front/mobile/img/playtech-min.png') }}">
                    <span>Playtech Live Casino</span>
                </a>
            </div>

            <div class="bx1">
                <a href="{{ route('mobile') }}#m_mxb">
                    <img src="{{ asset('/front/mobile/img/maxbet-min.png') }}">
                    <span>Xprogaming</span>
                </a>
            </div>
			@if(Session::get('currency')=='MYR')
			<div class="bx1">
                <a href="javascript:void(0);"  onclick="{{ Auth::user()->check() ? 'window.open("'.route('wmcframe').'","casino_wmc");' : 'alert("'.Lang::get('COMMON.PLEASELOGIN').'");' }}">
                    <img src="{{ asset('/front/mobile/img/wmcmobile.png') }}">
                    <span>WMC Casino</span>
                </a>
            </div>
			@endif
            @if(in_array('SPG',Session::get('valid_product')))
                <div class="bx1">
                    <a href="{{ route('spg', [ 'type' => 'slot' , 'category' => 'slot' ] ) }}">
                        <img src="{{ asset('/front/mobile/img/spade-min.png') }}">
                        <span>Spade Gaming</span>
                    </a>
                </div>
            @endif

            <div class="bx1">
                <a href="{{ route('mobile') }}#m_sky">
                    <img src="{{ asset('/front/mobile/img/sky-min.png') }}">
                    <span>Sky3888</span>
                </a>
            </div>

            <div class="bx1">
                <a href="{{ route('mobile') }}#m_alb">
                    <img src="{{ asset('/front/mobile/img/allb-min.png') }}">
                    <span>Allbet</span>
                </a>
            </div>

            <div class="bx1">
                <a href="{{ route('mobile') }}#m_jok">
                    <img src="{{ asset('/front/mobile/img/joker-min.png') }}">
                    <span>Joker123</span>
                </a>
            </div>

            <div class="bx1">
                <a {!! Auth::user()->check() ? 'href="'.route('ibc').'" target="_blank"' : 'href="javascript:void(0);" onclick="alert(\''.Lang::get('COMMON.PLEASELOGIN').'\');"' !!}>
                    <img src="{{ asset('/front/mobile/img/isport-min.png') }}">
                    <span>I-Sport</span>
                </a>
            </div>
            <div class="bx1">
                <a {!! Auth::user()->check() ? 'href="'.route('m8b').'" target="_blank"' : 'href="javascript:void(0);" onclick="alert(\''.Lang::get('COMMON.PLEASELOGIN').'\');"' !!}>
                    <img src="{{ asset('/front/mobile/img/sport-icon.png') }}">
                    <span>M-Sport</span>
                </a>
            </div>
            <div class="bx1">
                <a href="{{ route('4d') }}">
                    <img src="{{ asset('/front/mobile/img/4d-min.png') }}">
                    <span>4D</span>
                </a>
            </div>

            @if(in_array('VGS',Session::get('valid_product')))
                <div class="bx1">
                    <a href="javascript:void(0);" onClick="@if (Auth::user()->check())window.open('{{route('vgs')}}', 'casino14');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif">
                        <img src="{{ asset('/front/mobile/img/ev-min.png') }}">
                        <span>Evolution Gaming</span>
                    </a>
                </div>
            @endif

            @if(in_array('HOG',Session::get('valid_product')))
                <div class="bx1">
                    <a href="javascript:void(0);" onclick="{{ Auth::user()->check() ? 'window.open("'.route('hog').'","casino_as");' : 'alert("'.Lang::get('COMMON.PLEASELOGIN').'");' }}">
                        <img src="{{ asset('/front/mobile/img/ho-min.png') }}">
                        <span>HOGaming</span>
                    </a>
                </div>
            @endif

            <div class="bx1">
                <a href="javascript:void(0);" onclick="{{ Auth::user()->check() ? 'window.open("'.route('fortune').'","casino_fortune4d");' : 'alert("'.Lang::get('COMMON.PLEASELOGIN').'");' }}">
                    <img src="{{ asset('/front/mobile/img/fortune4d-min.png') }}">
                    <span>Fortune 4D</span>
                </a>
            </div>

            <div class="bx1">
                <a href="{{ route('promotion') }}">
                    <img src="{{ asset('/front/mobile/img/promotion-min.png') }}">
                    <span>{{ Lang::get('public.Promotion') }}</span>
                </a>
            </div>

            <div class="bx1">
                <a href="https://api.whatsapp.com/send?phone=601136933926" target="_blank">
                    <img src="{{ asset('/front/mobile/img/whasapp-min.png') }}">
                    <span>{{ Lang::get('public.WhatsAppLiveChat') }}</span>
                </a>
            </div>

            <div class="bx1">
                <a href="{{ route('switchwebtype') }}?type=d">
                    <img src="{{ asset('/front/mobile/img/desktop-web.png') }}">
                    <span>{{ Lang::get('public.DesktopSite') }}</span>
                </a>
            </div>
            <div class="clr"></div>
        </div>
    </div>
@endsection
