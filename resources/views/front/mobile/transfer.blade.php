@extends('front/mobile/master')

@section('title', 'Wallet Transfer')

@section('js')
    @parent

    <script>
        $(function () {
            var showWallet = "{{ Request::input('wallet') }}";

            if (showWallet == "y") {
                $("#midCmain").hide();
                $("#midCbalance").show();

                getBalance(1);
            } else {
                $("#midCbalance").hide();
                $("#midCmain").show();

                $("#transfer_to").change();
            }

            $("#thead_transfer").addClass("selected");

            getBalance(1);
        });

        // Transfer.
        function load_transfer_balance(code, type) {
            if(code == 'MAIN') {
                url = '{{route('mainwallet')}}';
            } else {
                url = '{{route('getbalance')}}';
            }
            $.ajax({
                type: "POST",
                url:  url,
                data: {
                    _token:     "{{ csrf_token() }}",
                    product:    code,
                },
                beforeSend: function(balance) {
                    $('#' + type).html('<img style="float:left;position:static;top:0px;margin-left:20px;" src="{{asset('/front/img/ajax-loading.gif')}}" width="20" height="20">');
                },
                success: function(balance) {
                    if (code == 'MAIN') {
                        $('.main_wallet').html(parseFloat(Math.round(balance * 100) / 100).toFixed(2));
                    }
                    $('#' + type).html('{{Session::get('currency')}} ' + parseFloat(Math.round(balance * 100) / 100).toFixed(2));
                }
            });
        }

        function submit_transfer() {
            $.ajax({
                type: "POST",
                url: "{{route('transfer-process')}}",
                data: {
                    _token: 		 "{{ csrf_token() }}",
                    amount:			 $('#transfer_amount').val(),
                    from:     		 $('#transfer_from').val(),
                    to: 			 $('#transfer_to').val(),
                },
                beforeSend: function() {
                    $('#btn_submit_transfer').attr('onClick', '');
                    $('#btn_submit_transfer').html('Loading...');
                },
                success: function(json) {
                    obj = JSON.parse(json);
                    var str = '';
                    $.each(obj, function(i, item) {

                        if ('{{Lang::get('COMMON.SUCESSFUL')}}' == item){

                            //$('.main_wallet').html(obj.main_wallet);

                        }
                        if (i != 'main_wallet') {
                            str += item + '\n';
                        }
                    });

                    load_transfer_balance($('#transfer_from').val(), 'transfer_from_balance');
                    load_transfer_balance($('#transfer_to').val(), 'transfer_to_balance');
                    getBalance(false);
                    $('#btn_submit_transfer').attr('onClick', 'submit_transfer()');
                    $('#btn_submit_transfer').html('{{Lang::get('public.Submit')}}');
                    if (str != '') {
                        alert(str);
                    }
                }
            });
        }
        // End transfer.
    </script>
@endsection

@section('content')

    <div id="midCbalance" style="display: none;">
        <div class="antitle">
            {{ Lang::get('public.Wallet') }}
        </div>

        <div class="midC">
            @include('front.mobile.include.balance')
        </div>
    </div>

    <div id="midCmain" style="display: none;">
        <div class="antitle">
            {{ Lang::get('public.Transfer') }}
        </div>

        <div class="midC" style="padding-bottom: 90px;">

            @include('front.mobile.include.transfer_head')

            <div class="detWrap2 bg-wht">
                <div class="detList2">
                    <span class="tp1">{{ Lang::get('public.From') }}* :</span>
                    <select id="transfer_from" onchange="load_transfer_balance(this.value,'transfer_from_balance')">
                        <option selected="selected" value="MAIN">{{ Lang::get('public.MainWallet') }}</option>
                        @foreach( Session::get('products_obj') as $prdid => $object)
                            <?php
                            $showPrd = true;
                            if (Session::get('currency') == 'VND' && ($object->code == 'JOK')) {
                                $showPrd = false;
                            }
                            ?>
                            @if ($showPrd)
                                <option value="{{$object->code}}">{{$object->name}}</option>
                            @endif
                        @endforeach
                    </select>
                </div>

                <div class="clr"></div>
            </div>

            <div class="detWrap2 bg-wht">
                <div class="detList2">
                    <span class="tp1">{{ Lang::get('public.To') }}* :</span>
                    <select id="transfer_to" onchange="load_transfer_balance(this.value,'transfer_to_balance')">
                        @foreach( Session::get('products_obj') as $prdid => $object)
                            <?php
                            $showPrd = true;
                            if (Session::get('currency') == 'VND' && ($object->code == 'JOK')) {
                                $showPrd = false;
                            }
                            ?>
                            @if ($showPrd)
                                <option value="{{$object->code}}">{{$object->name}}</option>
                            @endif
                        @endforeach
                        <option  value="MAIN">{{Lang::get('public.MainWallet')}}</option>
                    </select>
                </div>

                <div class="clr"></div>
            </div>

            <div class="detWrap2 bg-wht">
                <div class="detList2">
                    <span class="tp1">{{Lang::get('public.Amount')}}* :</span>
                    <input type="text" id="transfer_amount" name="transfer_amount" placeholder="0.00" />
                    <span id="error_deposit" style="color:Red;display:none;">*Your account have 0.00 balance, please make a {{Lang::get('public.Deposit')}}.</span>
                    <span id="error_require" style="color:Red;display:none;">{{Lang::get('public.Required')}}</span>
                </div>

                <div class="clr"></div>
            </div>

            <div class="nd1">
                <div class="submitC">
                    <a id="btn_submit_transfer" href="javascript:void(0);" onclick="submit_transfer();">{{Lang::get('public.Submit')}}</a>
                    <span class="error_validation" style="color:Red;"></span>
                </div>

                <div class="clr"></div>
            </div>

        </div>
    </div>
@endsection
