@extends('front/mobile/master')

@section('title', 'Withdrawal')

@section('js')
    @parent

    <script>
        $(function () {
            $("#thead_withdraw").addClass("selected");

            load_withdrawal();
            getBalance(1);
        });

        // Withdrawal.
        function load_withdrawal() {
            $.ajax({
                url: "{{ route('withdraw') }}",
                type: "GET",
                dataType: "JSON",
                data: {
                    _token: "{{ csrf_token() }}",
                    as_json: true
                },
                success: function (result) {
                    if (typeof result != undefined && result != null) {
                        $("#withdraw_min").html(result.min);
                        $("#withdraw_max").html(result.max);

                        var banklistObj = $("#withdraw_bank");

                        for (var key in result.banklists) {
                            if (result.banklists.hasOwnProperty(key)) {
                                banklistObj.append('<option value="' + result.banklists[key].code + '">' + key + '</option>');
                            }
                        }
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    setTimeout(function () {
                        load_withdrawal();
                    }, 1000);
                }
            });
        }

        function submit_withdraw() {
            $.ajax({
                type: "POST",
                url: "{{route('withdraw-process')}}?" + $('#withdraw_form').serialize(),
                data: {
                    _token: "{{ csrf_token() }}",
                },
                beforeSend: function() {
                },
                success: function(json) {
                    obj = JSON.parse(json);
                    var str = '';
                    $.each(obj, function(i, item) {
                        if ('{{Lang::get('COMMON.SUCESSFUL')}}' == item) {
                            alert('{{Lang::get('COMMON.SUCESSFUL')}}');
                            window.location.href = "{{route('profitloss')}}";
                        } else {
                            str += item + '\n';
                        }
                    });
                    if (str != '') {
                        alert(str);
                    }
                }
            });
        }
        // End withdrawal.
    </script>
@endsection

@section('content')
    <div class="antitle">
        {{ Lang::get('public.Withdrawal') }}
    </div>

    <div class="midC" style="padding-bottom: 90px;">

        @include('front.mobile.include.transfer_head')

        <form id="withdraw_form">
            <div class="detWrap2 bg-wht">
                <div class="detList2">
                    <span class="tp1">{{ Lang::get('public.Amount') }}* :</span>
                    <input type="text" id="withdraw_amount" name="amount" placeholder="0.00" />
                    <table border="0">
                        <tr>
                            <td>{{ Lang::get('public.Minimum') }}&nbsp;{{ Session::get('currency') }}</td>
                            <td><span id="withdraw_min" style="margin: 0;">-</span></td>
                        </tr>
                        <tr>
                            <td>{{ Lang::get('public.Maximum') }}&nbsp;{{ Session::get('currency') }}</td>
                            <td><span id="withdraw_max" style="margin: 0;">-</span></td>
                        </tr>
                    </table>
                </div>

                <div class="clr"></div>
            </div>

            <div class="detWrap2 bg-wht">
                <div class="detList2">
                    <span class="tp1">{{ Lang::get('public.Bank') }}* :</span>
                    <select id="withdraw_bank" name="bank"></select>
                </div>

                <div class="clr"></div>
            </div>

            <div class="detWrap2 bg-wht">
                <div class="detList2">
                    <span class="tp1">{{ Lang::get('public.FullName') }}:</span>
                    <input type="text" value="{{ Session::get('fullname') }}" readonly />
                </div>

                <div class="clr"></div>
            </div>

            <div class="detWrap2 bg-wht">
                <div class="detList2">
                    <span class="tp1">{{ Lang::get('public.BankAccountNo') }} :</span>
                    <input id="withdraw_accountno" name="accountno" type="text" />
                </div>

                <div class="clr"></div>
            </div>
        </form>

        <div class="nd1">
            <div class="submitC">
                <a id="btn_submit_transfer" href="javascript:void(0);" onclick="submit_withdraw();">{{Lang::get('public.Submit')}}</a>
                <span class="error_validation" style="color:Red;"></span>
            </div>

            <div class="clr"></div>
        </div>

    </div>
@endsection
