<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="@yield('keywords')"/>
    <meta name="description" content="@yield('description')"/>

    <title>Infiniwin @yield('title')</title>

    <link href="{{ asset('/front/favicon.ico') }}" rel="shortcut icon" type="image/icon" />
    <link href="{{ asset('/front/mobile/resources/css/style.css') }}" rel="stylesheet">

    @yield('css')

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
    <script src="{{ asset('/front/mobile/resources/js/jquery.bxslider.js') }}"></script>
</head>
<body style="background-color: #c9cac5;">

<div class="header">
    <div class="logo">
        <a href="{{ route('homepage') }}">
            <img src="{{ asset('/front/mobile/img/logo.png') }}">
        </a>
    </div>

    <div class="clr"></div>
</div>

<div class="midC">
    <div class="midCinner2">
        <span class="mnt">Website currently under maintenance.</span><br>
        <span class="mnt1">Come back again soon.</span>
    </div>
</div>

</body>
</html>