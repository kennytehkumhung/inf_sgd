@extends('front/mobile/master')

@section('title', 'Register')

@section('js')
    @parent

    <script>
        function enterpressalert(e, textarea)
        {
            var code = (e.keyCode ? e.keyCode : e.which);
            if(code == 13) { //Enter keycode
                register_submit();
            }
        }

        function register_submit() {
            var dob = '0000-00-00';
            if (typeof $('#dob_year').val() != undefined) {
                dob = $('#dob_year').val() + '-' + $('#dob_month').val() + '-' + $('#dob_day').val();
            }
            $.ajax({
                type: "POST",
                url: "{{route('register_process')}}",
                data: {
                    _token:     "{{ csrf_token() }}",
                    username:	$('#r_username').val(),
                    password:	$('#r_password').val(),
                    repeatpassword: $('#r_repeatpassword').val(),
                    code:		$('#r_code').val(),
                    email:		$('#r_email').val(),
                    mobile:		$('#r_mobile').val(),
                    fullname:	$('#r_fullname').val(),
                    referralid:	$('#r_referralid').val(),
                    dob:		dob
                },
            }).done(function(json) {
                $('.acctTextReg').html('');
                obj = JSON.parse(json);
                var str = '';
                $.each(obj, function(i, item) {
                    if ('{{Lang::get('COMMON.SUCESSFUL')}}' == item) {
                        alert('{{Lang::get('public.NewMember')}}');
                        window.location.href = "{{route('homepage')}}";
                    } else {
                        $('.' + i + '_acctTextReg').html('<font style="color:red">' + item + '</font>');
                    }
                });
            });
        }
    </script>
@endsection

@section('content')
    <div class="antitle">
        {{ Lang::get('public.Register') }}
    </div>

    <div class="midC">
        <div class="midCinner2">

            <div class="detWrap tp001">
                <div class="detList1">
                    <input type="text" id="r_fullname" name="r_fullname" placeholder="{{ Lang::get('public.FullName') }}*">
                    <span>{{ Lang::get('public.NameCaution') }}</span>
                    <span class="fullname_acctTextReg acctTextReg"></span>
                </div>

                <div class="clr"></div>
            </div>

            <div class="detWrap tp001">
                <div class="detList1">
                    <input type="text" id="r_username" name="r_username" placeholder="{{ Lang::get('public.Username') }}*">
                    <span>{{ Lang::get('public.UsernameNote') }}</span>
                    <span class="fullname_acctTextReg acctTextReg"></span>
                </div>

                <div class="clr"></div>
            </div>

            <div class="detWrap tp001">
                <div class="detList1">
                    <input type="password" id="r_password" name="r_password" placeholder="{{ Lang::get('public.Password') }}*">
                    <span>{{ Lang::get('public.NewPasswordNote') }}</span>
                    <span class="password_acctTextReg acctTextReg"></span>
                </div>

                <div class="clr"></div>
            </div>

            <div class="detWrap tp001">
                <div class="detList1">
                    <input type="password" id="r_repeatpassword" name="r_repeatpassword" placeholder="{{ Lang::get('public.ConfirmPassword') }}*">
                </div>

                <div class="clr"></div>
            </div>

            <div class="detWrap tp001">
                <div class="detList1">
                    <input type="text" id="r_email" name="r_email" placeholder="{{ Lang::get('public.Email') }}*">
                    <span class="email_acctTextReg acctTextReg"></span>
                </div>

                <div class="clr"></div>
            </div>

            <div class="detWrap tp001">
                <div class="detList1">
                    <input type="text" id="r_mobile" name="r_mobile" placeholder="{{ Lang::get('public.ContactNumber') }}*">
                    <span class="mobile_acctTextReg acctTextReg"></span>
                </div>

                <div class="clr"></div>
            </div>

            <div class="detWrap tp001">
                <div class="detList1">
                    <input type="text" id="r_code" name="r_code" onKeyPress="enterpressalert(event, this)" placeholder="{{ Lang::get('public.Code') }}*">
                    <span class="code_acctTextReg acctTextReg"></span>
                </div>

                <div class="clr"></div>
            </div>

            <div class="detWrap tp001">
                <div class="detList1">
                    <img src="{{ route('captcha', ['type' => 'register_captcha']) }}" class="coded blacked" style="height: 27px; width: 60px;" />
                </div>

                <div class="clr"></div>
            </div>

            <div class="detWrap tp001">
                <div class="detList1">
                    <p>* {{ Lang::get('public.RequiredField') }}</p>
                    <p>{{ Lang::get('public.DifficultyContact') }}</p>
                </div>

                <div class="clr"></div>
            </div>

            <div class="detWrap">
                <div class="detList">
                    <div class="submitB">
                        <a href="javascript:void(0);" onclick="register_submit();">{{ Lang::get('public.Submit') }}</a>
                    </div>
                </div>

                <div class="clr"></div>
            </div>
        </div>
    </div>
@endsection
