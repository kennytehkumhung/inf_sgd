@extends('front/mobile/master')

@section('title', 'Live Casino')

@section('js')
    @parent

    <script type="text/javascript">
        function callUrl(id,url,limitKey){
            var limitId = $("#" + limitKey).val();

            window.open('{{ route('mxb-casino') }}?g=' + id + '&l=' + limitId + '&c=' + url, 'maxbet');
        }
    </script>
@endsection

@section('content')
    <!-- Page Content -->
    <div class="container">

        <div class="ht1"></div>

        <!--LOBBY MENU-->
        <div class="row">
            <div class="col-md-1 col-xs-6">
                <a class="category-btn" href="{{route('mxb', [ 'type' => 'casino' , 'category' => 'baccarat' ] )}}">
                    <div>Bacarrat</div>
                </a>
            </div>

            <div class="col-md-1 col-xs-6">
                <a class="category-btn" href="{{route('mxb', [ 'type' => 'casino' , 'category' => 'roulette' ] )}}">
                    <div>Roulette</div>
                </a>
            </div>

            <div class="col-md-1 col-xs-6">
                <a class="category-btn" href="{{route('mxb', [ 'type' => 'casino' , 'category' => 'blackjack' ] )}}">
                    <div>Black Jack</div>
                </a>
            </div>

            <div class="col-md-1 col-xs-6">
                <a class="category-btn" href="{{route('mxb', [ 'type' => 'casino' , 'category' => 'single_player_poker' ] )}}">
                    <div>Poker</div>
                </a>
            </div>

            <div class="col-md-1 col-xs-6">
                <a class="category-btn" href="{{route('mxb', [ 'type' => 'casino' , 'category' => 'carribean_poker' ] )}}">
                    <div>Carribean Poker</div>
                </a>
            </div>

            <div class="col-md-1 col-xs-6">
                <a class="category-btn" href="{{route('mxb', [ 'type' => 'casino' , 'category' => 'dragon_tiger' ] )}}">
                    <div>Dragon Tiger</div>
                </a>
            </div>

            <div class="col-md-1 col-xs-6">
                <a class="category-btn" href="{{route('mxb', [ 'type' => 'casino' , 'category' => 'sicbo' ] )}}">
                    <div>Sic Bo</div>
                </a>
            </div>
        </div>
        <!--LOBBY MENU-->

        <div class="ht1"></div>

        <div class="row">
            @foreach( $gamelist as $key => $list)
                <div class="col-lg-6 col-md-12">
                    <div class="roomTitle">{{$list['gameName']}}</div>
                    <div class="roomDetails">
                        <div class="photo">
                            <img src="{{$list['image']}}" style="width: 100%; height: auto">
                        </div>
                        <div class="roomInfo">
                            Dealer: {{$list['dealerName']}}
                            <br>
                            <br>
                            Currency: {{ Session::get('currency', 'MYR') }}
                            <br>
                            <br>
                            Table Limit:
                            <br>
                            <select id="limit_{{$key}}">
                                @foreach( $list['limit'] as $url_key => $value)
                                    <option value="{{$value['limitSetID']}}">{{$value['minBet']}} ~ {{$value['maxBet']}}</option>
                                @endforeach
                            </select>
                            <br>
                            <a class="login_btn" href="#" onClick="callUrl('{{$key}}','{{Crypt::encrypt($list['url'])}}','limit_{{$key}}')">Play Now</a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>

    </div>
@endsection
