@extends('front/mobile/master')

@section('title', 'Slot')

@section('css')
    @parent

    <style type="text/css">
        .tbl_category {
            width: 100%;
        }

        .tbl_category * {
            color: #ffffff;
        }

        .tbl_category tr td {
            text-align: center;
            background-color: #5e5c47;
            width: 33%;
        }

        .tbl_category_blanks {
            background-color: transparent !important;
        }

        .slot_box {
            background-color: #b3ae96;
            margin: 0 5px 25px 5px;
            /*max-width: 180px;*/
            max-width: 45%;
            min-height: 147px;
            border: 1px solid #5e5c47;
            float: left;
            text-align: center;
        }

        .slot_box img {
            color: #5e5c47;
            font-weight: bold;
            width: 100%;
            height: auto;
            max-height: 170px;
        }

        .slot_box span {
            color: #5e5c47;
            font-weight: bold;
            line-height: 28px;
        }
    </style>
@endsection

@section('js')
    @parent

    <script type="text/javascript">
        $(function () {
            resizeSlotBox();

            window.addEventListener("resize", resizeSlotBox);
        });

        function resizeSlotBox() {
            var maxSlotBoxHeight = 0;
            var boxHeight = 0;

            $(".slot_box ").each(function () {
                boxHeight = parseFloat($(this).css("height").replace("px", ""));

                if (boxHeight > maxSlotBoxHeight) {
                    maxSlotBoxHeight = boxHeight;
                }
            });

            $(".slot_box").css("height", maxSlotBoxHeight + "px");
        }
    </script>
@endsection

@section('content')
    <div class="midC" style="padding-bottom: 90px;">
        <!--LOBBY MENU-->
        <table class="tbl_category" border="0" cellspacing="6" cellpadding="3">
            <tr>
                <td>
                    <a class="category-btn" href="{{route('playtechmobile', [ 'type' => 'slot' , 'category' => 'slot' ] )}}">Slots</a>
                </td>
                <td>
                    <a class="category-btn" href="{{route('playtechmobile', [ 'type' => 'slot' , 'category' => 'progressive' ] )}}">Progressive</a>
                </td>
				  <td>
                    <a class="category-btn" href="{{route('playtechmobile', [ 'type' => 'slot' , 'category' => 'live-casino' ] )}}">Live-casino</a>
                </td>
            </tr>
        </table>
        <!--LOBBY MENU-->

        <hr>

        <div class="row" style="text-align: center;">
            @foreach( $lists as $list )
                <div class="slot_box">
                    <a href="javascript:void(0)" onclick="@if (Auth::user()->check())window.open('{{route('playtech-h5-slot' , [ 'gameid' => $list['code'] ] )}}', 'plt_slot');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif" >
						<img src="{{url()}}/front/img/plt/{{$list->code}}.jpg" alt="" onerror="this.onerror=null;this.src='{{url('/')}}/front/img/plt/notimage.png';" />

                        <span>{{$list['name']}}</span>
                    </a>
                </div>
            @endforeach
        </div>

        <div class="clr"></div>
    </div>
@endsection
