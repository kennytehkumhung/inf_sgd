@include('front/mobile/wc/wc_header' )
<style>
.tnc  {}

.tnc table tr  {
	background: url(../images/table-bg.png)repeat;
}

.tnc table tr:last-child  {
	border-bottom: 1px solid #252525;
}

.tnc table th  {
	background: #000;
	color: #FFED82;
	border-top: 1px solid #252525;
	border-left: 1px solid #252525;
	border-right: 1px solid #252525;
}

.tnc table tbody tr td  {
	border-top: 1px solid #252525;
	border-right: 1px solid #252525;
	color: #fff;
}

.tnc > table > thead > tr > th  {
	border-bottom: 0px;
	text-align: center;
}

.tnc2  {}

.tnc2 table tr  {
	background: url(../images/table-bg.png)repeat;
}

.tnc2 table tr:last-child  {
	border-bottom: 1px solid #252525;
}

.tnc2 table th  {
	background: #363636;
	color: #FFED82;
	border-top: 1px solid #252525;
	border-left: 1px solid #252525;
	border-right: 1px solid #252525;
}

.tnc2 table tbody tr td  {
	border-top: 1px solid #252525;
	border-right: 1px solid #252525;
	color: #fff;
	text-align: center;
}

.tnc2 > table > thead > tr > th  {
	border-bottom: 0px;
	text-align: center;
}

.tnc3  {
	font-size: 12px;
	color: #fff;
	padding: 0px 8px;
}
</style>
<!-- User details Section -->
<div class="row">
<div class="col-md-6 col-xs-6">
<div class="userN">Username: <span>user1234</span></div>
</div>
<div class="col-md-6 col-xs-6">
<div class="tokenWrap">Token Use<span>28</span></div>
</div>
</div>
<!-- User details Section -->

<!-- Mainpage table Section -->
<div class="row">
<div class="tnc">
  <table class="table">
  <thead>
    <tr>
      <th scope="col">Cagetories</th>
      <th scope="col" width="30%">Prizes Payout</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>TEAM TO REACH THE FINAL & TEAM TO REACH THE SEMI FINAL & TEAM TO REACH THE QUARTER FINAL</td>
      <td>MYR 200</td>
    </tr>
    <tr>
      <td>GROUP WINNER & HIGHER TEAM GOAL & TOP GOALSCORER</td>
      <td>MYR 400</td>
    </tr>
    <tr>
      <td>WINNER & FINALISTS</td>
      <td>MYR 600</td>
    </tr>
  </tbody>
  </table>
</div>
<div class="tnc2">
  <table class="table">
  <thead>
    <tr>
      <th scope="col">Cagetories</th>
      <th scope="col" width="40%">Token Spending Limit</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Winner</td>
      <td>3</td>
    </tr>
    <tr>
      <td>Finalists</td>
      <td>7</td>
    </tr>
    <tr>
      <td>Team To Reach The Finals</td>
      <td>4</td>
    </tr>
    <tr>
      <td>Team To Reach The Semi Finals</td>
      <td>4</td>
    </tr>
    <tr>
      <td>Team To Reach The Quarter Finals</td>
      <td>4</td>
    </tr>
<tr>
      <td>Higher Team Goal</td>
      <td>6</td>
    </tr>
<tr>
      <td>Top Goal Scorer</td>
      <td>6</td>
    </tr>
    <tr>
      <td>Group Winner</td>
      <td>16</td>
    </tr>

  </tbody>
  </table>
</div>
<div class="tnc3">
<p>1. To redeem winnings from this promotion, there will be ONLY FIVE (5 times) rollover requirement attached to the winning prizes</p>

<p>2. Will be given out to 20 winners from Infiniwin with each Category , Grab the chance to bet before this game kicks off.</p>

<p>3. Maximum World Cup Tournament redeemable are 50chances only.</p>
</div>
</div>
<!-- Mainpage table Section -->

</div>
<!-- /.container -->
@include('front/mobile/wc/wc_footer' )