@include('front/mobile/wc/wc_header' )
</div>

<script type="text/javascript" language="javascript">
    function submit_winner() {
        var list = [];
        $(':checkbox:checked').each(function(i){
          list[i] = $(this).val();
        });
        if(list.length > 6){
            alert("Maximum 6 box can be selected! Thanks!");
        }
        else if(list.length == 0){
            window.location.href = "{{route('wcgroupwinner')}}";   
        }
        else{
            $.ajax({
                type: "POST",
                url: "{{route('wctopscorer-process')}}",
                data: {
                       _token: "{{ csrf_token() }}",
                       username: "{{ Session::get('username') }}",
                       team: list
                },
                beforeSend: function(){
                       $('#next_button').attr('onClick','');
                       $('#next_button').html('Loading...');
                },
                success: function(json){
                    obj = JSON.parse(json);
                        var str = '';
                        $.each(obj, function(i, item) {
                            if(item == '{{Lang::get('COMMON.SUCESSFUL')}}'){
                                alert('We will process to next section.');
                                window.location.href = "{{route('wcgroupwinner')}}";                                
                            }
                            else{
                                alert('[ERROR] Please refer your Token Balance.');
                                $('#next_button').attr('onClick','submit_winner()');
                                $('#next_button').html('Next');
                            }

                        })
                    }
               })
        }
    }
    
    function clear_checkbox(){
        $('input:checkbox').removeAttr('checked');
    }
</script>


<div class="container wrap01">
<!-- User details Section -->
<div class="row">
<div class="col-md-6 col-xs-6">
<div class="userN text-grey">{{ Lang::get('public.Username') }}: {{ Session::get('username') }}</div>
</div>
<div class="col-md-6 col-xs-6">
<div class="tokenWrap">LIMIT TOKEN LEFT<span>{{$token}}</span></div>
</div>
</div>
<!-- User details Section -->
<!-- desc Section -->
<div class="row bg-03">
<div class="col-md-12 col-xs-12">
<div class="wrapDesc">
<div class="descImg"><img src="{{ URL('/') }}/front/mobile/wc/images/desc-icon-7.png"></div>
<div class="descTxt adj01"><span>Pick</span> your team</div>
<div class="clearfix"></div>
</div>
</div>
</div>
<!-- desc Section -->
<div class="row">
<div class="col-md-12 col-xs-12">
<div class="spend">**Spending limit of 6 Tokens</div>
</div>
</div>
<!-- Selection Section -->
<div class="row">
    <div class="col-md-3 col-xs-6">
        <label class="wrapS2">
          <input type="checkbox" name="wc_list[]" id="inlineRadio1" value="1">
          <img class="tt6" src="{{ URL('/') }}/front/mobile/wc/images/argentina-flag.png" width="20"><span class="tt5">Lionel Messi</span></label>
    </div>    
    <div class="col-md-3 col-xs-6">
    <label class="wrapS2">
      <input type="checkbox" name="wc_list[]" id="inlineRadio2" value=2">
      <img class="tt6" src="{{ URL('/') }}/front/mobile/wc/images/argentina-flag.png" width="20"><span class="tt5">Gonzalo Higuain</span></label>
    </div>
    <div class="col-md-3 col-xs-6">
    <label class="wrapS2">
      <input type="checkbox" name="wc_list[]" id="inlineRadio3" value="3">
      <img class="tt6" src="{{ URL('/') }}/front/mobile/wc/images/argentina-flag.png" width="20"><span class="tt5">Paolo Dybala</span></label>
    </div>
    <div class="col-md-3 col-xs-6">
    <label class="wrapS2">
      <input type="checkbox" name="wc_list[]" id="inlineRadio4" value="4">
      <img class="tt6" src="{{ URL('/') }}/front/mobile/wc/images/argentina-flag.png" width="20"><span class="tt5">Sergio Aguero</span></label>
    </div>
    <div class="col-md-3 col-xs-6">
    <label class="wrapS2">
      <input type="checkbox" name="wc_list[]" id="inlineRadio5" value="5">
      <img class="tt6" src="{{ URL('/') }}/front/mobile/wc/images/colombia-flag.png" width="20"><span class="tt5">James Rodriguez</span></label>
    </div>
    <div class="col-md-3 col-xs-6">
    <label class="wrapS2">
      <input type="checkbox" name="wc_list[]" id="inlineRadio6" value="6">
      <img class="tt6" src="{{ URL('/') }}/front/mobile/wc/images/colombia-flag.png" width="20"><span class="tt5">Radamel Falcao</span></label>
    </div>
    <div class="col-md-3 col-xs-6">
    <label class="wrapS2">
      <input type="checkbox" name="wc_list[]" id="inlineRadio7" value="7">
      <img class="tt6" src="{{ URL('/') }}/front/mobile/wc/images/england-flag.png" width="20"><span class="tt5">Jamie Vardy</span></label>
    </div>
    <div class="col-md-3 col-xs-6">
    <label class="wrapS2">
      <input type="checkbox" name="wc_list[]" id="inlineRadio8" value="8">
      <img class="tt6" src="{{ URL('/') }}/front/mobile/wc/images/england-flag.png" width="20"><span class="tt5">Harry Kane</span></label>
    </div>
    <div class="col-md-3 col-xs-6">
    <label class="wrapS2">
      <input type="checkbox" name="wc_list[]" id="inlineRadio9" value="9">
      <img class="tt6" src="{{ URL('/') }}/front/mobile/wc/images/england-flag.png" width="20"><span class="tt5">Raheem sterling</span></label>
    </div>
    <div class="col-md-3 col-xs-6">
    <label class="wrapS2">
      <input type="checkbox" name="wc_list[]" id="inlineRadio10" value="10">
      <img class="tt6" src="{{ URL('/') }}/front/mobile/wc/images/england-flag.png" width="20"><span class="tt5">Marcus Rashford</span></label>
    </div>
    <div class="col-md-3 col-xs-6">
    <label class="wrapS2">
      <input type="checkbox" name="wc_list[]" id="inlineRadio11" value="11">
      <img class="tt6" src="{{ URL('/') }}/front/mobile/wc/images/serbia-flag.png" width="20"><span class="tt5">Mario Mandzukic</span></label>
    </div>
    <div class="col-md-3 col-xs-6">
    <label class="wrapS2">
      <input type="checkbox" name="wc_list[]" id="inlineRadio12" value="12">
      <img class="tt6" src="{{ URL('/') }}/front/mobile/wc/images/switzerland-flag.png" width="20"><span class="tt5">Christian Eriksen</span></label>
    </div>
    <div class="col-md-3 col-xs-6">
    <label class="wrapS2">
      <input type="checkbox" name="wc_list[]" id="inlineRadio13" value="13">
      <img class="tt6" src="{{ URL('/') }}/front/mobile/wc/images/iceland-flag.png" width="20"><span class="tt5">Gylfi Sigurdsson</span></label>
    </div>
    <div class="col-md-3 col-xs-6">
    <label class="wrapS2">
      <input type="checkbox" name="wc_list[]" id="inlineRadio15" value="15">
      <img class="tt6" src="{{ URL('/') }}/front/mobile/wc/images/france-flag.png" width="20"><span class="tt5">Kylian Mbappe</span></label>
    </div>
    <div class="col-md-3 col-xs-6">
    <label class="wrapS2">
      <input type="checkbox" name="wc_list[]" id="inlineRadio16" value="16">
      <img class="tt6" src="{{ URL('/') }}/front/mobile/wc/images/france-flag.png" width="20"><span class="tt5">Antoine Griezmann</span></label>
    </div>
    <div class="col-md-3 col-xs-6">
    <label class="wrapS2">
      <input type="checkbox" name="wc_list[]" id="inlineRadio17" value="17">
      <img class="tt6" src="{{ URL('/') }}/front/mobile/wc/images/france-flag.png" width="20"><span class="tt5">Olivier Giroud</span></label>
    </div>
    <div class="col-md-3 col-xs-6">
    <label class="wrapS2">
      <input type="checkbox" name="wc_list[]" id="inlineRadio14" value="14">
      <img class="tt6" src="{{ URL('/') }}/front/mobile/wc/images/costa-flag.png" width="20"><span class="tt5">Bryan Ruiz</span></label>
    </div>
    <div class="col-md-3 col-xs-6">
    <label class="wrapS2">
      <input type="checkbox" name="wc_list[]" id="inlineRadio18" value="18">
      <img class="tt6" src="{{ URL('/') }}/front/mobile/wc/images/germany-flag.png" width="20"><span class="tt5">Timo Werner</span></label>
    </div>
    <div class="col-md-3 col-xs-6">
    <label class="wrapS2">
      <input type="checkbox" name="wc_list[]" id="inlineRadio19" value="19">
      <img class="tt6" src="{{ URL('/') }}/front/mobile/wc/images/germany-flag.png" width="20"><span class="tt5">Thomas Muller</span></label>
    </div>
    <div class="col-md-3 col-xs-6">
    <label class="wrapS2">
      <input type="checkbox" name="wc_list[]" id="inlineRadio20" value="20">
      <img class="tt6" src="{{ URL('/') }}/front/mobile/wc/images/germany-flag.png" width="20"><span class="tt5">Mario Gomez</span></label>
    </div>
    <div class="col-md-3 col-xs-6">
    <label class="wrapS2">
      <input type="checkbox" name="wc_list[]" id="inlineRadio21" value="21">
      <img class="tt6" src="{{ URL('/') }}/front/mobile/wc/images/brazil-flag.png" width="20"><span class="tt5">Neymar</span></label>
    </div>
    <div class="col-md-3 col-xs-6">
    <label class="wrapS2">
      <input type="checkbox" name="wc_list[]" id="inlineRadio22" value="22">
      <img class="tt6" src="{{ URL('/') }}/front/mobile/wc/images/brazil-flag.png" width="20"><span class="tt5">Roberto Firmino</span></label>
    </div>
    <div class="col-md-3 col-xs-6">
    <label class="wrapS2">
      <input type="checkbox" name="wc_list[]" id="inlineRadio23" value="23">
      <img class="tt6" src="{{ URL('/') }}/front/mobile/wc/images/brazil-flag.png" width="20"><span class="tt5">Gabriel Jesus</span></label>
    </div>
    <div class="col-md-3 col-xs-6">
    <label class="wrapS2">
      <input type="checkbox" name="wc_list[]" id="inlineRadio24" value="24">
      <img class="tt6" src="{{ URL('/') }}/front/mobile/wc/images/brazil-flag.png" width="20"><span class="tt5">Philippe Coutinho</span></label>
    </div>
    <div class="col-md-3 col-xs-6">
    <label class="wrapS2">
      <input type="checkbox" name="wc_list[]" id="inlineRadio25" value="25">
      <img class="tt6" src="{{ URL('/') }}/front/mobile/wc/images/portugal-flag.png" width="20"><span class="tt5">Cristiano Ronaldo</span></label>
    </div>
    <div class="col-md-3 col-xs-6">
    <label class="wrapS2">
      <input type="checkbox" name="wc_list[]" id="inlineRadio26" value="26">
      <img class="tt6" src="{{ URL('/') }}/front/mobile/wc/images/portugal-flag.png" width="20"><span class="tt5">Andre Silva</span></label>
    </div>
    <div class="col-md-3 col-xs-6">
    <label class="wrapS2">
      <input type="checkbox" name="wc_list[]" id="inlineRadio27" value="27">
      <img class="tt6" src="{{ URL('/') }}/front/mobile/wc/images/belgium-flag.png" width="20"><span class="tt5">Eden Hazard</span></label>
    </div>
    <div class="col-md-3 col-xs-6">
    <label class="wrapS2">
      <input type="checkbox" name="wc_list[]" id="inlineRadio28" value="28">
      <img class="tt6" src="{{ URL('/') }}/front/mobile/wc/images/belgium-flag.png" width="20"><span class="tt5">Christian Benteke</span></label>
    </div>
    <div class="col-md-3 col-xs-6">
    <label class="wrapS2">
      <input type="checkbox" name="wc_list[]" id="inlineRadio29" value="29">
      <img class="tt6" src="{{ URL('/') }}/front/mobile/wc/images/belgium-flag.png" width="20"><span class="tt5">Romelo Lukaku</span></label>
    </div>
    <div class="col-md-3 col-xs-6">
    <label class="wrapS2">
      <input type="checkbox" name="wc_list[]" id="inlineRadio30" value="30">
      <img class="tt6" src="{{ URL('/') }}/front/mobile/wc/images/belgium-flag.png" width="20"><span class="tt5">Kevin De Bruyne</span></label>
    </div>
    <div class="col-md-3 col-xs-6">
    <label class="wrapS2">
      <input type="checkbox" name="wc_list[]" id="inlineRadio31" value="31">
      <img class="tt6" src="{{ URL('/') }}/front/mobile/wc/images/australia-flag.png" width="20"><span class="tt5">Tim Cahill</span></label>
    </div>
    <div class="col-md-3 col-xs-6">
    <label class="wrapS2">
      <input type="checkbox" name="wc_list[]" id="inlineRadio32" value="32">
      <img class="tt6" src="{{ URL('/') }}/front/mobile/wc/images/egypt-flag.png" width="20"><span class="tt5">Mohamed Salah</span></label>
    </div>
<div class="col-md-3 col-xs-6">
    <label class="wrapS2">
      <input type="checkbox" name="wc_list[]" id="inlineRadio33" value="33">
      <img class="tt6" src="{{ URL('/') }}/front/mobile/wc/images/korea-flag.png" width="20"><span class="tt5">Son Heung Min</span></label>
</div>
<div class="col-md-3 col-xs-6">
    <label class="wrapS2">
      <input type="checkbox" name="wc_list[]" id="inlineRadio34" value="34">
      <img class="tt6" src="{{ URL('/') }}/front/mobile/wc/images/mexico-flag.png" width="20"><span class="tt5">Javier Hernandez</span></label>
</div>
<div class="col-md-3 col-xs-6">
    <label class="wrapS2">
      <input type="checkbox" name="wc_list[]" id="inlineRadio35" value="35">
      <img class="tt6" src="{{ URL('/') }}/front/mobile/wc/images/poland-flag.png" width="20"><span class="tt5">Lewandowski</span></label>
</div>
<div class="col-md-3 col-xs-6">
    <label class="wrapS2">
      <input type="checkbox" name="wc_list[]" id="inlineRadio36" value="36">
      <img class="tt6" src="{{ URL('/') }}/front/mobile/wc/images/uruguay-flag.png" width="20"><span class="tt5">Luis Suarez</span></label>
</div>
<div class="col-md-3 col-xs-6">
    <label class="wrapS2">
      <input type="checkbox" name="wc_list[]" id="inlineRadio40" value="40">
      <img class="tt6" src="{{ URL('/') }}/front/mobile/wc/images/uruguay-flag.png" width="20"><span class="tt5">Edinson Cavani</span></label>
</div>
<div class="col-md-3 col-xs-6">
    <label class="wrapS2">
      <input type="checkbox" name="wc_list[]" id="inlineRadio37" value="37">
      <img class="tt6" src="{{ URL('/') }}/front/mobile/wc/images/senegal-flag.png" width="20"><span class="tt5">Sadio Mane</span></label>
</div>
    <div class="col-md-3 col-xs-6">
    <label class="wrapS2">
      <input type="checkbox" name="wc_list[]" id="inlineRadio38" value="38">
      <img class="tt6" src="{{ URL('/') }}/front/mobile/wc/images/spain-flag.png" width="20"><span class="tt5">Isco</span></label>
</div>
<div class="col-md-3 col-xs-6">
    <label class="wrapS2">
      <input type="checkbox" name="wc_list[]" id="inlineRadio39" value="39">
      <img class="tt6" src="{{ URL('/') }}/front/mobile/wc/images/spain-flag.png" width="20"><span class="tt5">Diego Costa</span></label>
</div>

</div>
<!-- Selection Section -->
<!-- confirm Section -->
<div class="row">
<div class="col-md-3 col-xs-7">
<div class="totToken">
TOTAL TOKEN <span>{{$total_token}}</span>
</div>
</div>
<div class="col-md-3 col-xs-6">
<div class="submitBtn">
<a href="#" onclick="clear_checkbox()">Reset</a>
</div>
</div>
<div class="col-md-3 col-xs-6">
<div class="submitBtn">
<a id="next_button" href="#" onclick="submit_winner()">Next</a>
</div>
</div>
</div>
<!-- confirm Section -->
</div>

<!-- /.container -->
@include('front/mobile/wc/wc_footer' )