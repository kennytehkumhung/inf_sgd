@include('front/mobile/wc/wc_header' )

<!-- User details Section -->
<div class="row">
<div class="col-md-6 col-xs-6">
<div class="userN">{{ Lang::get('public.Username') }}: <span>{{ Session::get('username') }}</span></div>
</div>
<div class="col-md-6 col-xs-6">
<div class="tokenWrap">Total Token<span>{{$total_token}}</span></div>
</div>
</div>
<!-- User details Section -->

<!-- Mainpage table Section -->
<div class="row">
<div>
  <table class="table table-striped">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Category</th>
      <th scope="col">Selection</th>
      <th scope="col">Status</th>
      <th scope="col">Bet Time</th>
    </tr>
  </thead>
  <tbody>
    <?php $no = 1; ?>
    @foreach($ledgers as $key => $val)
        <tr>
          <th scope="row">{{$no++}}</td>
          <td>{{$val['category']}}</td>
          <td>{{$val['selection']}}</td>
          <td>{{$val['status']}}</td>
          <td>{{$val['date']}}</td>
        </tr>
    @endforeach
  </tbody>
  </table>
</div>
</div>
<!-- Mainpage table Section -->

</div>
<!-- /.container -->
@include('front/mobile/wc/wc_footer' )