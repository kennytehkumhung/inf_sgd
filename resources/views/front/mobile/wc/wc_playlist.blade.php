@include('front/mobile/wc/wc_header' )
</div>
<div class="container wrap01">
<!-- User details Section -->
<div class="row">
<div class="col-md-6 col-xs-6">
<div class="userN text-grey">{{ Lang::get('public.Username') }}: <span>{{ Session::get('username') }}</span></div>
</div>
<div class="col-md-6 col-xs-6">
<div class="tokenWrap">Token Use<span>{{$total_token}}</span></div>
</div>
</div>
<!-- User details Section -->
<!-- Selection Section -->
<div class="row">
<div class="col-md-12 col-xs-12 pl">
<ul>
<li><a href="#" onclick="alert('Bet session closed. Thanks for participant!');">- Winner</a></li>
<li><a href="#" onclick="alert('Bet session closed. Thanks for participant!');">- Finalist</a></li>
<li><a href="#" onclick="alert('Bet session closed. Thanks for participant!');">- Team To Reach The Final</a></li>
<li><a href="#" onclick="alert('Bet session closed. Thanks for participant!');">- Team To Reach The Semi Final</a></li>
<li><a href="#" onclick="alert('Bet session closed. Thanks for participant!');">- Team To Reach The Quarter Final</a></li>
<li><a href="#" onclick="alert('Bet session closed. Thanks for participant!');">- Higher Team Goal</a></li>
<li><a href="#" onclick="alert('Bet session closed. Thanks for participant!');">- Top Goal Scorer</a></li>
<li><a href="#" onclick="alert('Bet session closed. Thanks for participant!');">- Group Winner</a></li>
</ul>
</div>
</div>

<!-- Selection Section -->

</div>

<!-- /.container -->
@include('front/mobile/wc/wc_footer' )