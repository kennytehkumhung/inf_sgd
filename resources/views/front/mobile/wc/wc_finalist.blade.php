@include('front/mobile/wc/wc_header' )
</div>

<script type="text/javascript" language="javascript">
    function submit_winner() {
        var list = [];
        $(':checkbox:checked').each(function(i){
          list[i] = $(this).val();
        });
        if(list.length > 7){
            alert("Maximum 7 box can be selected! Thanks!");
        }
        else if(list.length == 0){
            window.location.href = "{{route('wcfinal')}}";   
        }
        else{
            $.ajax({
                type: "POST",
                url: "{{route('wcfinalists-process')}}",
                data: {
                       _token: "{{ csrf_token() }}",
                       username: "{{ Session::get('username') }}",
                       team: list
                },
                beforeSend: function(){
                       $('#next_button').attr('onClick','');
                       $('#next_button').html('Loading...');
                },
                success: function(json){
                    obj = JSON.parse(json);
                        var str = '';
                        $.each(obj, function(i, item) {
                            if(item == '{{Lang::get('COMMON.SUCESSFUL')}}'){
                                alert('We will process to next section.');
                                window.location.href = "{{route('wcfinal')}}";                                
                            }
                            else{
                                alert('[ERROR] Please refer your Token Balance.');
                                $('#next_button').attr('onClick','submit_winner()');
                                $('#next_button').html('Next');
                            }

                        })
                    }
               })
        }
    }
    
    function clear_checkbox(){
        $('input:checkbox').removeAttr('checked');
    }
</script>

<div class="container wrap01">
<!-- User details Section -->
<div class="row">
<div class="col-md-6 col-xs-6">
<div class="userN text-grey">{{ Lang::get('public.Username') }}: {{ Session::get('username') }}</div>
</div>
<div class="col-md-6 col-xs-6">
<div class="tokenWrap">LIMIT TOKEN LEFT<span>{{$token}}</span></div>
</div>
</div>
<!-- User details Section -->
<!-- desc Section -->
<div class="row bg-03">
<div class="col-md-12 col-xs-12">
<div class="wrapDesc">
<div class="descImg"><img src="{{ URL('/') }}/front/mobile/wc/images/desc-icon-2.png"></div>
<div class="descTxt"><span>Pick</span> your World Cup <br><span><em>Finalists match</em></span></div>
<div class="clearfix"></div>
</div>
</div>
</div>
<!-- desc Section -->
<div class="row">
<div class="col-md-12 col-xs-12">
<div class="spend text-red">**Other ( Except Argentina, Belgium, Brazil, England, France, Germany, Portugal, Spain)</div>
</div>
</div>
<!-- Selection Section -->
<div class="row">
<div class="col-md-3 col-xs-6">
    <label class="wrapS1">
      <input type="checkbox" name="wc_list[]" id="inlineRadio1" value="1">
      <img class="tt3" src="{{ URL('/') }}/front/mobile/wc/images/argentina-flag.png" width="15"><span class="tt4">ARG</span></label>
      <img src="{{ URL('/') }}/front/mobile/wc/images/vs.png" width="15">
    <label class="wrapS1">
      <img class="tt3" src="{{ URL('/') }}/front/mobile/wc/images/belgium-flag.png" width="15"><span class="tt4">BEL</span></label>
</div>
<div class="col-md-3 col-xs-6">
    <label class="wrapS1">
      <input type="checkbox" name="wc_list[]" id="inlineRadio2" value="2">
      <img class="tt3" src="{{ URL('/') }}/front/mobile/wc/images/argentina-flag.png" width="15"><span class="tt4">ARG</span></label>
      <img src="{{ URL('/') }}/front/mobile/wc/images/vs.png" width="15">
    <label class="wrapS1">
      <img class="tt3" src="{{ URL('/') }}/front/mobile/wc/images/brazil-flag.png" width="15"><span class="tt4">BRA</span></label>
</div>
<div class="col-md-3 col-xs-6">
    <label class="wrapS1">
      <input type="checkbox" name="wc_list[]" id="inlineRadio3" value="3">
      <img class="tt3" src="{{ URL('/') }}/front/mobile/wc/images/argentina-flag.png" width="15"><span class="tt4">ARG</span></label>
      <img src="{{ URL('/') }}/front/mobile/wc/images/vs.png" width="15">
    <label class="wrapS1">
      <img class="tt3" src="{{ URL('/') }}/front/mobile/wc/images/england-flag.png" width="15"><span class="tt4">ENG</span></label>
</div>
<div class="col-md-3 col-xs-6">
    <label class="wrapS1">
      <input type="checkbox" name="wc_list[]" id="inlineRadio4" value="4">
      <img class="tt3" src="{{ URL('/') }}/front/mobile/wc/images/argentina-flag.png" width="15"><span class="tt4">ARG</span></label>
      <img src="{{ URL('/') }}/front/mobile/wc/images/vs.png" width="15">
    <label class="wrapS1">
      <img class="tt3" src="{{ URL('/') }}/front/mobile/wc/images/france-flag.png" width="15"><span class="tt4">FRA</span></label>
</div>
<div class="col-md-3 col-xs-6">
    <label class="wrapS1">
      <input type="checkbox" name="wc_list[]" id="inlineRadio5" value="5">
      <img class="tt3" src="{{ URL('/') }}/front/mobile/wc/images/argentina-flag.png" width="15"><span class="tt4">ARG</span></label>
      <img src="{{ URL('/') }}/front/mobile/wc/images/vs.png" width="15">
    <label class="wrapS1">
      <img class="tt3" src="{{ URL('/') }}/front/mobile/wc/images/germany-flag.png" width="15"><span class="tt4">GER</span></label>
</div>
<div class="col-md-3 col-xs-6">
    <label class="wrapS1">
      <input type="checkbox" name="wc_list[]" id="inlineRadio6" value="6">
      <img class="tt3" src="{{ URL('/') }}/front/mobile/wc/images/argentina-flag.png" width="15"><span class="tt4">ARG</span></label>
      <img src="{{ URL('/') }}/front/mobile/wc/images/vs.png" width="15">
    <label class="wrapS1">
      <img class="tt3" src="{{ URL('/') }}/front/mobile/wc/images/portugal-flag.png" width="15"><span class="tt4">PRT</span></label>
</div>
<div class="col-md-3 col-xs-6">
    <label class="wrapS1">
      <input type="checkbox" name="wc_list[]" id="inlineRadio7" value="7">
      <img class="tt3" src="{{ URL('/') }}/front/mobile/wc/images/argentina-flag.png" width="15"><span class="tt4">ARG</span></label>
      <img src="{{ URL('/') }}/front/mobile/wc/images/vs.png" width="15">
    <label class="wrapS1">
      <img class="tt3" src="{{ URL('/') }}/front/mobile/wc/images/spain-flag.png" width="15"><span class="tt4">ESP</span></label>
</div>
<div class="col-md-3 col-xs-6">
    <label class="wrapS1">
      <input type="checkbox" name="wc_list[]" id="inlineRadio8" value="8">
      <img class="tt3" src="{{ URL('/') }}/front/mobile/wc/images/brazil-flag.png" width="15"><span class="tt4">BRA</span></label>
      <img src="{{ URL('/') }}/front/mobile/wc/images/vs.png" width="15">
    <label class="wrapS1">
      <img class="tt3" src="{{ URL('/') }}/front/mobile/wc/images/belgium-flag.png" width="15"><span class="tt4">BEL</span></label>
</div>
<div class="col-md-3 col-xs-6">
    <label class="wrapS1">
      <input type="checkbox" name="wc_list[]" id="inlineRadio9" value="9">
      <img class="tt3" src="{{ URL('/') }}/front/mobile/wc/images/brazil-flag.png" width="15"><span class="tt4">BRA</span></label>
      <img src="{{ URL('/') }}/front/mobile/wc/images/vs.png" width="15">
    <label class="wrapS1">
      <img class="tt3" src="{{ URL('/') }}/front/mobile/wc/images/england-flag.png" width="15"><span class="tt4">ENG</span></label>
</div>
<div class="col-md-3 col-xs-6">
    <label class="wrapS1">
      <input type="checkbox" name="wc_list[]" id="inlineRadio10" value="10">
      <img class="tt3" src="{{ URL('/') }}/front/mobile/wc/images/brazil-flag.png" width="15"><span class="tt4">BRA</span></label>
      <img src="{{ URL('/') }}/front/mobile/wc/images/vs.png" width="15">
    <label class="wrapS1">
      <img class="tt3" src="{{ URL('/') }}/front/mobile/wc/images/france-flag.png" width="15"><span class="tt4">FRA</span></label>
</div>
<div class="col-md-3 col-xs-6">
    <label class="wrapS1">
      <input type="checkbox" name="wc_list[]" id="inlineRadio11" value="11">
      <img class="tt3" src="{{ URL('/') }}/front/mobile/wc/images/brazil-flag.png" width="15"><span class="tt4">BRA</span></label>
      <img src="{{ URL('/') }}/front/mobile/wc/images/vs.png" width="15">
    <label class="wrapS1">
      <img class="tt3" src="{{ URL('/') }}/front/mobile/wc/images/germany-flag.png" width="15"><span class="tt4">GER</span></label>
</div>
<div class="col-md-3 col-xs-6">
    <label class="wrapS1">
      <input type="checkbox" name="wc_list[]" id="inlineRadio12" value="12">
      <img class="tt3" src="{{ URL('/') }}/front/mobile/wc/images/brazil-flag.png" width="15"><span class="tt4">BRA</span></label>
      <img src="{{ URL('/') }}/front/mobile/wc/images/vs.png" width="15">
    <label class="wrapS1">
      <img class="tt3" src="{{ URL('/') }}/front/mobile/wc/images/portugal-flag.png" width="15"><span class="tt4">POR</span></label>
</div>
<div class="col-md-3 col-xs-6">
    <label class="wrapS1">
      <input type="checkbox" name="wc_list[]" id="inlineRadio13" value="13">
      <img class="tt3" src="{{ URL('/') }}/front/mobile/wc/images/brazil-flag.png" width="15"><span class="tt4">BRA</span></label>
      <img src="{{ URL('/') }}/front/mobile/wc/images/vs.png" width="15">
    <label class="wrapS1">
      <img class="tt3" src="{{ URL('/') }}/front/mobile/wc/images/spain-flag.png" width="15"><span class="tt4">ESP</span></label>
</div>
<div class="col-md-3 col-xs-6">
    <label class="wrapS1">
      <input type="checkbox" name="wc_list[]" id="inlineRadio14" value="14">
      <img class="tt3" src="{{ URL('/') }}/front/mobile/wc/images/england-flag.png" width="15"><span class="tt4">ENG</span></label>
      <img src="{{ URL('/') }}/front/mobile/wc/images/vs.png" width="15">
    <label class="wrapS1">
      <img class="tt3" src="{{ URL('/') }}/front/mobile/wc/images/belgium-flag.png" width="15"><span class="tt4">BEL</span></label>
</div>
<div class="col-md-3 col-xs-6">
    <label class="wrapS1">
      <input type="checkbox" name="wc_list[]" id="inlineRadio15" value="15">
      <img class="tt3" src="{{ URL('/') }}/front/mobile/wc/images/england-flag.png" width="15"><span class="tt4">ENG</span></label>
      <img src="{{ URL('/') }}/front/mobile/wc/images/vs.png" width="15">
    <label class="wrapS1">
      <img class="tt3" src="{{ URL('/') }}/front/mobile/wc/images/france-flag.png" width="15"><span class="tt4">FRA</span></label>
</div>
<div class="col-md-3 col-xs-6">
    <label class="wrapS1">
      <input type="checkbox" name="wc_list[]" id="inlineRadio16" value="16">
      <img class="tt3" src="{{ URL('/') }}/front/mobile/wc/images/england-flag.png" width="15"><span class="tt4">ENG</span></label>
      <img src="{{ URL('/') }}/front/mobile/wc/images/vs.png" width="15">
    <label class="wrapS1">
      <img class="tt3" src="{{ URL('/') }}/front/mobile/wc/images/germany-flag.png" width="15"><span class="tt4">GER</span></label>
</div>
<div class="col-md-3 col-xs-6">
    <label class="wrapS1">
      <input type="checkbox" name="wc_list[]" id="inlineRadio17" value="17">
      <img class="tt3" src="{{ URL('/') }}/front/mobile/wc/images/england-flag.png" width="15"><span class="tt4">ENG</span></label>
      <img src="{{ URL('/') }}/front/mobile/wc/images/vs.png" width="15">
    <label class="wrapS1">
      <img class="tt3" src="{{ URL('/') }}/front/mobile/wc/images/portugal-flag.png" width="15"><span class="tt4">POR</span></label>
</div>
<div class="col-md-3 col-xs-6">
    <label class="wrapS1">
      <input type="checkbox" name="wc_list[]" id="inlineRadio18" value="18">
      <img class="tt3" src="{{ URL('/') }}/front/mobile/wc/images/england-flag.png" width="15"><span class="tt4">ENG</span></label>
      <img src="{{ URL('/') }}/front/mobile/wc/images/vs.png" width="15">
    <label class="wrapS1">
      <img class="tt3" src="{{ URL('/') }}/front/mobile/wc/images/spain-flag.png" width="15"><span class="tt4">ESP</span></label>
</div>
<div class="col-md-3 col-xs-6">
    <label class="wrapS1">
      <input type="checkbox" name="wc_list[]" id="inlineRadio19" value="19">
      <img class="tt3" src="{{ URL('/') }}/front/mobile/wc/images/belgium-flag.png" width="15"><span class="tt4">BEL</span></label>
      <img src="{{ URL('/') }}/front/mobile/wc/images/vs.png" width="15">
    <label class="wrapS1">
      <img class="tt3" src="{{ URL('/') }}/front/mobile/wc/images/spain-flag.png" width="15"><span class="tt4">ESP</span></label>
</div>
<div class="col-md-3 col-xs-6">
    <label class="wrapS1">
      <input type="checkbox" name="wc_list[]" id="inlineRadio20" value="20">
      <img class="tt3" src="{{ URL('/') }}/front/mobile/wc/images/france-flag.png" width="15"><span class="tt4">FRA</span></label>
      <img src="{{ URL('/') }}/front/mobile/wc/images/vs.png" width="15">
    <label class="wrapS1">
      <img class="tt3" src="{{ URL('/') }}/front/mobile/wc/images/belgium-flag.png" width="15"><span class="tt4">BEL</span></label>
</div>
<div class="col-md-3 col-xs-6">
    <label class="wrapS1">
      <input type="checkbox" name="wc_list[]" id="inlineRadio21" value="21">
      <img class="tt3" src="{{ URL('/') }}/front/mobile/wc/images/france-flag.png" width="15"><span class="tt4">FRA</span></label>
      <img src="{{ URL('/') }}/front/mobile/wc/images/vs.png" width="15">
    <label class="wrapS1">
      <img class="tt3" src="{{ URL('/') }}/front/mobile/wc/images/germany-flag.png" width="15"><span class="tt4">GER</span></label>
</div> 
<div class="col-md-3 col-xs-6">
    <label class="wrapS1">
      <input type="checkbox" name="wc_list[]" id="inlineRadio22" value="22">
      <img class="tt3" src="{{ URL('/') }}/front/mobile/wc/images/france-flag.png" width="15"><span class="tt4">FRA</span></label>
      <img src="{{ URL('/') }}/front/mobile/wc/images/vs.png" width="15">
    <label class="wrapS1">
      <img class="tt3" src="{{ URL('/') }}/front/mobile/wc/images/portugal-flag.png" width="15"><span class="tt4">POR</span></label>
</div> 
<div class="col-md-3 col-xs-6">
    <label class="wrapS1">
      <input type="checkbox" name="wc_list[]" id="inlineRadio23" value="23">
      <img class="tt3" src="{{ URL('/') }}/front/mobile/wc/images/france-flag.png" width="15"><span class="tt4">FRA</span></label>
      <img src="{{ URL('/') }}/front/mobile/wc/images/vs.png" width="15">
    <label class="wrapS1">
      <img class="tt3" src="{{ URL('/') }}/front/mobile/wc/images/spain-flag.png" width="15"><span class="tt4">ESP</span></label>
</div>
<div class="col-md-3 col-xs-6">
    <label class="wrapS1">
      <input type="checkbox" name="wc_list[]" id="inlineRadio24" value="24">
      <img class="tt3" src="{{ URL('/') }}/front/mobile/wc/images/germany-flag.png" width="15"><span class="tt4">GER</span></label>
      <img src="{{ URL('/') }}/front/mobile/wc/images/vs.png" width="15">
    <label class="wrapS1">
      <img class="tt3" src="{{ URL('/') }}/front/mobile/wc/images/belgium-flag.png" width="15"><span class="tt4">BEL</span></label>
</div>
<div class="col-md-3 col-xs-6">
    <label class="wrapS1">
      <input type="checkbox" name="wc_list[]" id="inlineRadio25" value="25">
      <img class="tt3" src="{{ URL('/') }}/front/mobile/wc/images/germany-flag.png" width="15"><span class="tt4">GER</span></label>
      <img src="{{ URL('/') }}/front/mobile/wc/images/vs.png" width="15">
    <label class="wrapS1">
      <img class="tt3" src="{{ URL('/') }}/front/mobile/wc/images/portugal-flag.png" width="15"><span class="tt4">POR</span></label>
</div>
<div class="col-md-3 col-xs-6">
    <label class="wrapS1">
      <input type="checkbox" name="wc_list[]" id="inlineRadio26" value="26">
      <img class="tt3" src="{{ URL('/') }}/front/mobile/wc/images/germany-flag.png" width="15"><span class="tt4">GER</span></label>
      <img src="{{ URL('/') }}/front/mobile/wc/images/vs.png" width="15">
    <label class="wrapS1">
      <img class="tt3" src="{{ URL('/') }}/front/mobile/wc/images/spain-flag.png" width="15"><span class="tt4">ESP</span></label>
</div> 
<div class="col-md-3 col-xs-6">
    <label class="wrapS1">
      <input type="checkbox" name="wc_list[]" id="inlineRadio27" value="27">
      <img class="tt3" src="{{ URL('/') }}/front/mobile/wc/images/portugal-flag.png" width="15"><span class="tt4">POR</span></label>
      <img src="{{ URL('/') }}/front/mobile/wc/images/vs.png" width="15">
    <label class="wrapS1">
      <img class="tt3" src="{{ URL('/') }}/front/mobile/wc/images/belgium-flag.png" width="15"><span class="tt4">BEL</span></label>
</div> 
<div class="col-md-3 col-xs-6">
    <label class="wrapS1">
      <input type="checkbox" name="wc_list[]" id="inlineRadio28" value="28">
      <img class="tt3" src="{{ URL('/') }}/front/mobile/wc/images/portugal-flag.png" width="15"><span class="tt4">POR</span></label>
      <img src="{{ URL('/') }}/front/mobile/wc/images/vs.png" width="15">
    <label class="wrapS1">
      <img class="tt3" src="{{ URL('/') }}/front/mobile/wc/images/spain-flag.png" width="15"><span class="tt4">ESP</span></label>
</div> 
<div class="col-md-3 col-xs-6">
    <label class="wrapS1">
      <input type="checkbox" name="wc_list[]" id="inlineRadio29" value="29">
      <img class="tt3" src="{{ URL('/') }}/front/mobile/wc/images/portugal-flag.png" width="15"><span class="tt4">POR</span></label>
      <img src="{{ URL('/') }}/front/mobile/wc/images/vs.png" width="15">
    <label class="wrapS1">
      <span class="tt4">OTHERS</span></label>
</div>  
<div class="col-md-3 col-xs-6">
    <label class="wrapS1">
      <input type="checkbox" name="wc_list[]" id="inlineRadio30" value="30">
      <img class="tt3" src="{{ URL('/') }}/front/mobile/wc/images/belgium-flag.png" width="15"><span class="tt4">BEL</span></label>
      <img src="{{ URL('/') }}/front/mobile/wc/images/vs.png" width="15">
    <label class="wrapS1">
      <span class="tt4">OTHERS</span></label>
</div>
<div class="col-md-3 col-xs-6">
    <label class="wrapS1">
      <input type="checkbox" name="wc_list[]" id="inlineRadio31" value="31">
      <img class="tt3" src="{{ URL('/') }}/front/mobile/wc/images/brazil-flag.png" width="15"><span class="tt4">BRA</span></label>
      <img src="{{ URL('/') }}/front/mobile/wc/images/vs.png" width="15">
    <label class="wrapS1">
      <span class="tt4">OTHERS</span></label>
</div>
<div class="col-md-3 col-xs-6">
    <label class="wrapS1">
      <input type="checkbox" name="wc_list[]" id="inlineRadio32" value="32">
      <img class="tt3" src="{{ URL('/') }}/front/mobile/wc/images/england-flag.png" width="15"><span class="tt4">ENG</span></label>
      <img src="{{ URL('/') }}/front/mobile/wc/images/vs.png" width="15">
    <label class="wrapS1">
      <span class="tt4">OTHERS</span></label>
</div>
<div class="col-md-3 col-xs-6">
    <label class="wrapS1">
      <input type="checkbox" name="wc_list[]" id="inlineRadio33" value="33">
      <img class="tt3" src="{{ URL('/') }}/front/mobile/wc/images/france-flag.png" width="15"><span class="tt4">FRA</span></label>
      <img src="{{ URL('/') }}/front/mobile/wc/images/vs.png" width="15">
    <label class="wrapS1">
      <span class="tt4">OTHERS</span></label>
</div>
<div class="col-md-3 col-xs-6">
    <label class="wrapS1">
      <input type="checkbox" name="wc_list[]" id="inlineRadio34" value="34">
      <img class="tt3" src="{{ URL('/') }}/front/mobile/wc/images/germany-flag.png" width="15"><span class="tt4">GER</span></label>
      <img src="{{ URL('/') }}/front/mobile/wc/images/vs.png" width="15">
    <label class="wrapS1">
      <span class="tt4">OTHERS</span></label>
</div>
</div>
<!-- Selection Section -->
<!-- confirm Section -->
<div class="row">
<div class="col-md-3 col-xs-7">
<div class="totToken">
TOTAL TOKEN <span>{{$total_token}}</span>
</div>
</div>
<div class="col-md-3 col-xs-6">
<div class="submitBtn">
<a href="#" onclick="clear_checkbox()">Reset</a>
</div>
</div>
<div class="col-md-3 col-xs-6">
<div class="submitBtn">
<a id="next_button" href="#" onclick="submit_winner()">Next</a>
</div>
</div>
</div>
<!-- confirm Section -->
</div>

<!-- /.container -->
@include('front/mobile/wc/wc_footer' )