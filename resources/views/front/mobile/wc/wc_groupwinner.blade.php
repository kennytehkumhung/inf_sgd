@include('front/mobile/wc/wc_header' )
</div>

<script type="text/javascript" language="javascript">
    $(document).ready(function(){
        var limit = 2;
        $('input.a-checkbox').on('click', function (evt) {
            console.log($('.a-checkbox:checked').length);
            if ($('.a-checkbox:checked').length > limit) {
                this.checked = false;
            }
        });
        $('input.b-checkbox').on('click', function (evt) {
            if ($('.b-checkbox:checked').length > limit) {
                this.checked = false;
            }
        });
        $('input.c-checkbox').on('click', function (evt) {
            if ($('.c-checkbox:checked').length > limit) {
                this.checked = false;
            }
        });
        $('input.d-checkbox').on('click', function (evt) {
            if ($('.d-checkbox:checked').length > limit) {
                this.checked = false;
            }
        });
        $('input.e-checkbox').on('click', function (evt) {
            if ($('.e-checkbox:checked').length > limit) {
                this.checked = false;
            }
        });
        $('input.f-checkbox').on('click', function (evt) {
            if ($('.f-checkbox:checked').length > limit) {
                this.checked = false;
            }
        });
        $('input.g-checkbox').on('click', function (evt) {
            if ($('.g-checkbox:checked').length > limit) {
                this.checked = false;
            }
        });
        $('input.h-checkbox').on('click', function (evt) {
            if ($('.h-checkbox:checked').length > limit) {
                this.checked = false;
            }
        });
    });
    function submit_winner() {
        var list = [];
        $(':checkbox:checked').each(function(i){
          list[i] = $(this).val();
        });
        if(list.length > 18){
            alert("Maximum 18 box can be selected! Thanks!");
        }
        else if(list.length == 0){
            window.location.href = "{{route('wclobby')}}";   
        }
        else{
            $.ajax({
                type: "POST",
                url: "{{route('wcgroupwinner-process')}}",
                data: {
                       _token: "{{ csrf_token() }}",
                       username: "{{ Session::get('username') }}",
                       team: list
                },
                beforeSend: function(){
                       $('#next_button').attr('onClick','');
                       $('#next_button').html('Loading...');
                },
                success: function(json){
                    obj = JSON.parse(json);
                        var str = '';
                        $.each(obj, function(i, item) {
                            if(item == '{{Lang::get('COMMON.SUCESSFUL')}}'){
                                alert('thank you for participating in this event! Good Luck!');
                                window.location.href = "{{route('wclobby')}}";                                
                            }
                            else{
                                alert('[ERROR] Please refer your Token Balance.');
                                $('#next_button').attr('onClick','submit_winner()');
                                $('#next_button').html('Next');
                            }

                        })
                    }
               })
        }
    }
    
    function clear_checkbox(){
        $('input:checkbox').removeAttr('checked');
    }
        
</script>


<div class="container wrap01">
<!-- User details Section -->
<div class="row">
<div class="col-md-6 col-xs-6">
<div class="userN text-grey">{{ Lang::get('public.Username') }}: {{ Session::get('username') }}</div>
</div>
<div class="col-md-6 col-xs-6">
<div class="tokenWrap">LIMIT TOKEN LEFT<span>{{$token}}</span></div>
</div>
</div>
<!-- User details Section -->
<!-- desc Section -->
<div class="row bg-03">
<div class="col-md-12 col-xs-12">
<div class="wrapDesc">
<div class="descImg"><img src="{{ URL('/') }}/front/mobile/wc/images/desc-icon-8.png"></div>
<div class="descTxt adj01"><span>Pick</span> your team</div>
<div class="clearfix"></div>
</div>
</div>
</div>
<!-- desc Section -->
<div class="row">
<div class="col-md-12 col-xs-12">
<div class="spend">**Spending limit of 18 Tokens</div>
</div>
</div>
<!-- Selection Section -->
<div class="row">
<div class="col-md-12 col-xs-12">
<div class="gT">Group A <span><img src="{{ URL('/') }}/front/mobile/wc/images/gT-icon.png"></span></div>
</div>
<div class="col-md-3 col-xs-6">
    <label class="wrapS">
      <input class="a-checkbox" type="checkbox" name="wc_list[]" id="inlineRadio1" value="1">
      <img class="tt1" src="{{ URL('/') }}/front/mobile/wc/images/russia-flag.png" width="25"><span class="tt2">Russia</span></label>
</div>
<div class="col-md-3 col-xs-6">
    <label class="wrapS">
      <input class="a-checkbox" type="checkbox" name="wc_list[]" id="inlineRadio2" value="2">
      <img class="tt1" src="{{ URL('/') }}/front/mobile/wc/images/egypt-flag.png" width="25"><span class="tt2">Egypt</span></label>
</div>
<div class="col-md-3 col-xs-6">
    <label class="wrapS">
      <input class="a-checkbox" type="checkbox" name="wc_list[]" id="inlineRadio3" value="3">
      <img class="tt1" src="{{ URL('/') }}/front/mobile/wc/images/arabia-flag.png" width="25"><span class="tt2">Saudi Arabia</span></label>
</div>
<div class="col-md-3 col-xs-6">
    <label class="wrapS">
      <input class="a-checkbox" type="checkbox" name="wc_list[]" id="inlineRadio4" value="4">
      <img class="tt1" src="{{ URL('/') }}/front/mobile/wc/images/uruguay-flag.png" width="25"><span class="tt2">Uruguay</span></label>
</div>
<div class="col-md-12 col-xs-12">
<div class="line"></div>
</div>
</div>
<div class="row">
<div class="col-md-12 col-xs-12">
<div class="gT">Group B <span><img src="{{ URL('/') }}/front/mobile/wc/images/gT-icon.png"></span></div>
</div>
<div class="col-md-3 col-xs-6">
    <label class="wrapS">
      <input class="b-checkbox" type="checkbox" name="wc_list[]" id="inlineRadio5" value="5">
      <img class="tt1" src="{{ URL('/') }}/front/mobile/wc/images/morocco-flag.png" width="25"><span class="tt2">Morocco</span></label>
</div>
<div class="col-md-3 col-xs-6">
    <label class="wrapS">
      <input class="b-checkbox" type="checkbox" name="wc_list[]" id="inlineRadio6" value="6">
      <img class="tt1" src="{{ URL('/') }}/front/mobile/wc/images/portugal-flag.png" width="25"><span class="tt2">Portugal</span></label>
</div>
<div class="col-md-3 col-xs-6">
    <label class="wrapS">
      <input class="b-checkbox" type="checkbox" name="wc_list[]" id="inlineRadio7" value="7">
      <img class="tt1" src="{{ URL('/') }}/front/mobile/wc/images/spain-flag.png" width="25"><span class="tt2">Spain</span></label>
</div>
<div class="col-md-3 col-xs-6">
    <label class="wrapS">
      <input class="b-checkbox" type="checkbox" name="wc_list[]" id="inlineRadio8" value="8">
      <img class="tt1" src="{{ URL('/') }}/front/mobile/wc/images/iran-flag.png" width="25"><span class="tt2">Iran</span></label>
</div>
<div class="col-md-12 col-xs-12">
<div class="line"></div>
</div>
</div>
<div class="row">
<div class="col-md-12 col-xs-12">
<div class="gT">Group C <span><img src="{{ URL('/') }}/front/mobile/wc/images/gT-icon.png"></span></div>
</div>
<div class="col-md-3 col-xs-6">
    <label class="wrapS">
      <input class="c-checkbox" type="checkbox" name="wc_list[]" id="inlineRadio9" value="9">
      <img class="tt1" src="{{ URL('/') }}/front/mobile/wc/images/france-flag.png" width="25"><span class="tt2">France</span></label>
</div>
<div class="col-md-3 col-xs-6">
    <label class="wrapS">
      <input class="c-checkbox" type="checkbox" name="wc_list[]" id="inlineRadio10" value="10">
      <img class="tt1" src="{{ URL('/') }}/front/mobile/wc/images/australia-flag.png" width="25"><span class="tt2">Australia</span></label>
</div>
<div class="col-md-3 col-xs-6">
    <label class="wrapS">
      <input class="c-checkbox" type="checkbox" name="wc_list[]" id="inlineRadio11" value="11">
      <img class="tt1" src="{{ URL('/') }}/front/mobile/wc/images/peru-flag.png" width="25"><span class="tt2">Peru</span></label>
</div>
<div class="col-md-3 col-xs-6">
    <label class="wrapS">
      <input class="c-checkbox" type="checkbox" name="wc_list[]" id="inlineRadio12" value="12">
      <img class="tt1" src="{{ URL('/') }}/front/mobile/wc/images/denmark-flag.png" width="25"><span class="tt2">Denmark</span></label>
</div>
<div class="col-md-12 col-xs-12">
<div class="line"></div>
</div>
</div>
<div class="row">
<div class="col-md-12 col-xs-12">
<div class="gT">Group D <span><img src="{{ URL('/') }}/front/mobile/wc/images/gT-icon.png"></span></div>
</div>
<div class="col-md-3 col-xs-6">
    <label class="wrapS">
      <input class="d-checkbox" type="checkbox" name="wc_list[]" id="inlineRadio13" value="13">
      <img class="tt1" src="{{ URL('/') }}/front/mobile/wc/images/argentina-flag.png" width="25"><span class="tt2">Argentina</span></label>
</div>
<div class="col-md-3 col-xs-6">
    <label class="wrapS">
      <input class="d-checkbox" type="checkbox" name="wc_list[]" id="inlineRadio14" value="14">
      <img class="tt1" src="{{ URL('/') }}/front/mobile/wc/images/iceland-flag.png" width="25"><span class="tt2">Iceland</span></label>
</div>
<div class="col-md-3 col-xs-6">
    <label class="wrapS">
      <input class="d-checkbox" type="checkbox" name="wc_list[]" id="inlineRadio15" value="15">
      <img class="tt1" src="{{ URL('/') }}/front/mobile/wc/images/croatia-flag.png" width="25"><span class="tt2">Croatia</span></label>
</div>
<div class="col-md-3 col-xs-6">
    <label class="wrapS">
      <input class="d-checkbox" type="checkbox" name="wc_list[]" id="inlineRadio16" value="16">
      <img class="tt1" src="{{ URL('/') }}/front/mobile/wc/images/nigeria-flag.png" width="25"><span class="tt2">Nigeria</span></label>
</div>
<div class="col-md-12 col-xs-12">
<div class="line"></div>
</div>
</div>
<div class="row">
<div class="col-md-12 col-xs-12">
<div class="gT">Group E <span><img src="{{ URL('/') }}/front/mobile/wc/images/gT-icon.png"></span></div>
</div>
<div class="col-md-3 col-xs-6">
    <label class="wrapS">
      <input class="e-checkbox" type="checkbox" name="wc_list[]" id="inlineRadio17" value="17">
      <img class="tt1" src="{{ URL('/') }}/front/mobile/wc/images/brazil-flag.png" width="25"><span class="tt2">Brazil</span></label>
</div>
<div class="col-md-3 col-xs-6">
    <label class="wrapS">
      <input class="e-checkbox" type="checkbox" name="wc_list[]" id="inlineRadio18" value="18">
      <img class="tt1" src="{{ URL('/') }}/front/mobile/wc/images/switzerland-flag.png" width="25"><span class="tt2">Switzerland</span></label>
</div>
<div class="col-md-3 col-xs-6">
    <label class="wrapS">
      <input class="e-checkbox" type="checkbox" name="wc_list[]" id="inlineRadio19" value="19">
      <img class="tt1" src="{{ URL('/') }}/front/mobile/wc/images/costa-flag.png" width="25"><span class="tt2">Costa Rica</span></label>
</div>
<div class="col-md-3 col-xs-6">
    <label class="wrapS">
      <input class="e-checkbox" type="checkbox" name="wc_list[]" id="inlineRadio20" value="20">
      <img class="tt1" src="{{ URL('/') }}/front/mobile/wc/images/serbia-flag.png" width="25"><span class="tt2">Serbia</span></label>
</div>
<div class="col-md-12 col-xs-12">
<div class="line"></div>
</div>
</div>
<div class="row">
<div class="col-md-12 col-xs-12">
<div class="gT">Group F <span><img src="{{ URL('/') }}/front/mobile/wc/images/gT-icon.png"></span></div>
</div>
<div class="col-md-3 col-xs-6">
    <label class="wrapS">
      <input class="f-checkbox" type="checkbox" name="wc_list[]" id="inlineRadio21" value="21">
      <img class="tt1" src="{{ URL('/') }}/front/mobile/wc/images/germany-flag.png" width="25"><span class="tt2">Germany</span></label>
</div>
<div class="col-md-3 col-xs-6">
    <label class="wrapS">
      <input class="f-checkbox" type="checkbox" name="wc_list[]" id="inlineRadio22" value="22">
      <img class="tt1" src="{{ URL('/') }}/front/mobile/wc/images/mexico-flag.png" width="25"><span class="tt2">Mexico</span></label>
</div>
<div class="col-md-3 col-xs-6">
    <label class="wrapS">
      <input class="f-checkbox" type="checkbox" name="wc_list[]" id="inlineRadio23" value="23">
      <img class="tt1" src="{{ URL('/') }}/front/mobile/wc/images/sweden-flag.png" width="25"><span class="tt2">Sweden</span></label>
</div>
<div class="col-md-3 col-xs-6">
    <label class="wrapS">
      <input class="f-checkbox" type="checkbox" name="wc_list[]" id="inlineRadio24" value="24">
      <img class="tt1" src="{{ URL('/') }}/front/mobile/wc/images/korea-flag.png" width="25"><span class="tt2">South Korea</span></label>
</div>
<div class="col-md-12 col-xs-12">
<div class="line"></div>
</div>
</div>
<div class="row">
<div class="col-md-12 col-xs-12">
<div class="gT">Group G <span><img src="{{ URL('/') }}/front/mobile/wc/images/gT-icon.png"></span></div>
</div>
<div class="col-md-3 col-xs-6">
    <label class="wrapS">
      <input class="g-checkbox" type="checkbox" name="wc_list[]" id="inlineRadio25" value="25">
      <img class="tt1" src="{{ URL('/') }}/front/mobile/wc/images/panama-flag.png" width="25"><span class="tt2">Panama</span></label>
</div>
<div class="col-md-3 col-xs-6">
    <label class="wrapS">
      <input class="g-checkbox" type="checkbox" name="wc_list[]" id="inlineRadio26" value="26">
      <img class="tt1" src="{{ URL('/') }}/front/mobile/wc/images/belgium-flag.png" width="25"><span class="tt2">Belgium</span></label>
</div>
<div class="col-md-3 col-xs-6">
    <label class="wrapS">
      <input class="g-checkbox" type="checkbox" name="wc_list[]" id="inlineRadio27" value="27">
      <img class="tt1" src="{{ URL('/') }}/front/mobile/wc/images/tunisia-flag.png" width="25"><span class="tt2">Tunisia</span></label>
</div>
<div class="col-md-3 col-xs-6">
    <label class="wrapS">
      <input class="g-checkbox" type="checkbox" name="wc_list[]" id="inlineRadio28" value="28">
      <img class="tt1" src="{{ URL('/') }}/front/mobile/wc/images/england-flag.png" width="25"><span class="tt2">England</span></label>
</div>
<div class="col-md-12 col-xs-12">
<div class="line"></div>
</div>
</div>
<div class="row">
<div class="col-md-12 col-xs-12">
<div class="gT">Group H <span><img src="{{ URL('/') }}/front/mobile/wc/images/gT-icon.png"></span></div>
</div>
<div class="col-md-3 col-xs-6">
    <label class="wrapS">
      <input class="h-checkbox" type="checkbox" name="wc_list[]" id="inlineRadio29" value="29">
      <img class="tt1" src="{{ URL('/') }}/front/mobile/wc/images/colombia-flag.png" width="25"><span class="tt2">Colombia</span></label>
</div>
<div class="col-md-3 col-xs-6">
    <label class="wrapS">
      <input class="h-checkbox" type="checkbox" name="wc_list[]" id="inlineRadio30" value="30">
      <img class="tt1" src="{{ URL('/') }}/front/mobile/wc/images/poland-flag.png" width="25"><span class="tt2">Poland</span></label>
</div>
<div class="col-md-3 col-xs-6">
    <label class="wrapS">
      <input class="h-checkbox" type="checkbox" name="wc_list[]" id="inlineRadio31" value="31">
      <img class="tt1" src="{{ URL('/') }}/front/mobile/wc/images/japan-flag.png" width="25"><span class="tt2">Japan</span></label>
</div>
<div class="col-md-3 col-xs-6">
    <label class="wrapS">
      <input class="h-checkbox" type="checkbox" name="wc_list[]" id="inlineRadio32" value="32">
      <img class="tt1" src="{{ URL('/') }}/front/mobile/wc/images/senegal-flag.png" width="25"><span class="tt2">Senegal</span></label>
</div>
<div class="col-md-12 col-xs-12">
<div class="line"></div>
</div>
</div>
<!-- Selection Section -->
<!-- confirm Section -->
<div class="row">
<div class="col-md-3 col-xs-7">
<div class="totToken">
TOTAL TOKEN <span>{{$total_token}}</span>
</div>
</div>
<div class="col-md-3 col-xs-6">
<div class="submitBtn">
<a href="#" onclick="clear_checkbox()">Reset</a>
</div>
</div>
<div class="col-md-3 col-xs-6">
<div class="submitBtn">
<a id="next_button" href="#" onclick="submit_winner()">Next</a>
</div>
</div>
</div>
<!-- confirm Section -->
</div>

<!-- /.container -->
@include('front/mobile/wc/wc_footer' )