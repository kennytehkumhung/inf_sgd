<!-- Bottom menu Section -->
<div class="btm">
<div class="flex-container">
    <div>
        <a href="{{route('wclobby')}}">
        <img src="{{ URL('/') }}/front/mobile/wc/images/btm-icon-5.png">
        <div class="btmTxt">History</div>
        </a>
    </div>
    <div>
        <a href="{{route('wcplaylist')}}">
        <img src="{{ URL('/') }}/front/mobile/wc/images/btm-icon-2.png">
        <div class="btmTxt">Playlist</div>
        </a>
    </div> 
    <div>
        <a href="{{route('wctnc')}}">
        <img src="{{ URL('/') }}/front/mobile/wc/images/btm-icon-3.png">
        <div class="btmTxt">T & C</div>
        </a>
    </div>
    <div>
        <a href="{{URL('/')}}/my">
        <img src="{{ URL('/') }}/front/mobile/wc/images/btm-icon-1.png">
        <div class="btmTxt">Back Casino</div>
        </a>
    </div>
</div>
</div>
<!-- Bottom menu Section -->

<!-- Bootstrap Core JavaScript -->
<script src="{{ URL('/') }}/front/mobile/wc/js/bootstrap.min.js"></script>


</body>

</html>