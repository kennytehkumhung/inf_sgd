<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<title>InfiniWin World Cup 2018 Prediction</title>

<!-- Bootstrap Core CSS -->
<link href="{{ URL('/') }}/front/mobile/wc/css/bootstrap.min.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="{{ URL('/') }}/front/mobile/wc/css/modern-business.css" rel="stylesheet">

<!-- Custom Fonts -->
<link href="{{ URL('/') }}/front/mobile/wc/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->

<script src="{{url('/')}}/front/resources/js/jq214.js"></script>
<script src="{{ URL('/') }}/front/mobile/wc/js/jquery.countdown.js"></script>

<script>
// When the user scrolls down 20px from the top of the document, show the button
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        document.getElementById("myBtn").style.display = "block";
    } else {
        document.getElementById("myBtn").style.display = "none";
    }
}

// When the user clicks on the button, scroll to the top of the document
function topFunction() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
}
</script>
@if (Request::input('reminder') == 'y')
    <script type="text/javascript">
        $(function () {
            alert("Dear member, you still have World Cup token yet been bet. Please finish before forfeit. Thank you!");
        });
    </script>
@endif
</head>
<body>
<!-- Navigation -->
    <nav class="navbar navbar-inverse" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html"><img src="{{ URL('/') }}/front/mobile/wc/images/logo.png"></a>
                <div class="logBtn"><a href="{{ route('logout') }}">Logout</a></div>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="#" onclick="alert('Bet session closed. Thanks for participant!');">Winner</a>
                    </li>
                    <li>
                        <a href="#" onclick="alert('Bet session closed. Thanks for participant!');">Finalist</a>
                    </li>
                    <li>
                        <a href="#" onclick="alert('Bet session closed. Thanks for participant!');"">Team To Reach The Finals</a>
                    </li>
                    <li>
                        <a href="#" onclick="alert('Bet session closed. Thanks for participant!');">Team To Reach The Semi-Finals</a>
                    </li>
                    <li>
                        <a href="#" onclick="alert('Bet session closed. Thanks for participant!');">Team To Reach The Quarter-Finals</a>
                    </li>
                    <li>
                        <a href="#" onclick="alert('Bet session closed. Thanks for participant!');">Higher Team Goal</a>
                    </li>
                    <li>
                        <a href="#" onclick="alert('Bet session closed. Thanks for participant!');">Top Goal Scorer</a>
                    </li>
                    <li>
                        <a href="#" onclick="alert('Bet session closed. Thanks for participant!');">Group Winner</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
    

<!-- Content -->
<div class="container" style="padding-bottom: 70px">
<!-- Title and timer Section -->
<div class="row">
<div class="col-lg-12">
<img class="img-responsive mar01" src="{{ URL('/') }}/front/mobile/wc/images/txt01.png">
</div>
<div class="col-lg-12 bg-02">
<!--WC 18 Timer-->
<div class="wc">
   <div class="wcInner">
   <div class="timerWrap">
   <div id="getting-started" class="timerDig"></div>
   </div>
   <script type="text/javascript">
   $("#getting-started")
   .countdown("2018/06/14 23:00:00", function(event) {
    $(this).text(
      event.strftime('%D %H %M %S')
    );
    });
    </script>
   </div>
</div>
<!--WC 18 Timer-->
</div>
</div>
<!-- Title and timer Section -->