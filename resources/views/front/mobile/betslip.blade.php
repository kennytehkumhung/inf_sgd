<!DOCTYPE html>
<html lang="en">
<head>
<title id='Description'>Infiniwin Bet Slip
</title>
<link rel="stylesheet" href="{{ asset('/admin/jqwidgets/styles/jqx.base.css') }}" type="text/css"/>
<link rel="stylesheet" href="{{ asset('/admin/jqwidgets/styles/jqx.metro.css') }}" type="text/css"/>
<script type="text/javascript" src="{{ asset('/admin/jqwidgets/jquery-1.11.1.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/admin/jqwidgets/jqx-all.js') }}"></script>
<script type="text/javascript">
		
		function doGridSearch(id,type,value){

			if(type == 'search')
			{
				$("#"+id).val(value);
			}
			else if(type == 'dropdown')
			{
				$("#"+id).jqxDropDownList('selectItem', value ); 
			
			}
		}
		
		function doSearch(){
			search();
		}

		function search(){

			var aflt = new Array();
			var aqfld = new Array();
			var temp = new Array();
			var params = new Array();
			
			
			var paginginformation = $('#dtGrid').jqxGrid('getpaginginformation');
		
			params['aflt'] = aflt;
			params['aqfld'] = aqfld;
			params['createdfrom'] = $('#dateFrom').jqxDateTimeInput('getText'); 
			params['createdto']   = $('#dateTo').jqxDateTimeInput('getText'); 
			params['igd']   = paginginformation.pagenum; 
			params['ilt']   = paginginformation.pagesize; 
			params['ist']   = paginginformation.pagescount; 
			params['timefrom'] = $( "#time_from" ).val();
			params['timeto']   = $( "#time_to" ).val();
			
			
			$('#dtGrid').jqxGrid('clear');
			
												
				
			var source =
			{
				datatype: "json",
				datafields: [
												 { name: 'date' },
												 { name: 'member' },
												 { name: 'vendor' },
												 { name: 'product' },
												 { name: 'detail' },
												 { name: 'currency' },
												 { name: 'stake' },
												 { name: 'winloss' },
												 { name: 'valid' },
												 { name: 'result' },
						
				],
				url: "betslip-data",
				beforeprocessing: function (data) {
                    source.totalrecords = data.total;
					                },
				data: {
																		prdid: '{{$prdid}}',
												createdfrom: '{{$from}}',
												createdto: '{{$to}}',
											
							
										igd: params['igd'],
					limit: params['ilt'],
					start: params['ist'],
					createdfrom: params['createdfrom'],
					createdto: params['createdto'],
					timefrom : params['timefrom'],
						timeto : params['timeto'],
					aflt:
					{
																								},
					aqfld:
					{
																	
											},
						
				}
			
				
			};
			
            var dataAdapter = new $.jqx.dataAdapter(source, {
                downloadComplete: function (data, status, xhr) { },
                loadComplete: function (data) { 
					$.each( data.footer, function( key, object ) {
						
						$.each( object, function( colunm, value ) {
							$('#total_'+colunm).html(value);
						});
					});
			
									},
                loadError: function (xhr, status, error) { }
            });
			
            $("#dtGrid").jqxGrid(
            {
                source: dataAdapter,                
            });

		}
		
        $(document).ready(function () {
            // prepare the data
			var source_initial =
			{
				datatype: "json",
				datafields: [
												 { name: 'date' },
												 { name: 'member' },
												 { name: 'vendor' },
												 { name: 'product' },
												 { name: 'detail' },
												 { name: 'currency' },
												 { name: 'stake' },
												 { name: 'winloss' },
												 { name: 'valid' },
												 { name: 'result' },
						
				],
				url: "betslip-data",
				beforeprocessing: function (data) {
                    source_initial.totalrecords = data.total;
                },
				data: {
																	prdid: '{{$prdid}}',
												createdfrom: '{{$from}}',
												createdto: '{{$to}}',
								
							
										limit: 20,
					start: 0,
															aflt:
					{
																								}
					
				}
			};
	

            var cellsrenderer = function (row, columnfield, value, defaulthtml, columnproperties, rowdata) {
                if (value < 20) {
                    return '<span style="margin: 4px; float: ' + columnproperties.cellsalign + '; color: #ff0000;">' + value + '</span>';
                }
                else {
                    return '<span style="margin: 4px; float: ' + columnproperties.cellsalign + '; color: #008000;">' + value + '</span>';
                }
            }

            var dataAdapter = new $.jqx.dataAdapter(source_initial, {
                downloadComplete: function (data, status, xhr) { },
                loadComplete: function (data) { 
	
										$.each( data.footer, function( key, object ) {
	
						$.each( object, function( colunm, value ) {
							$('#total_'+colunm).html(value);
						});
					});
					
					
				},
                loadError: function (xhr, status, error) { }
            });

            // initialize jqxGrid
            $("#dtGrid").jqxGrid(
            {
				                width: 1000,
				                source: dataAdapter,                
                pageable:  true ,
                autoheight: true,
				columnsresize: true,
				altrows: true,
                sortable: true,
                altrows: true,
				theme:'metro',
                enabletooltips: true,
				showaggregates: true,
                editable: false,
								showstatusbar: true,
				                statusbarheight: 50,
				pagesize: 20,
				pagesizeoptions: ['20', '50', '100', '150', '200'],
								selectionmode: 'singlecell',	
								virtualmode: true,
				
				rendergridrows: function(obj)
				{
					  return obj.data;     
				},
                columns: [
				  						 { text: 'Time', datafield: 'date' , cellsalign: 'center',align: 'center' ,  width: 150 ,aggregates: ['Time'],aggregatesrenderer: function (aggregatedValue, currentValue) {
							  return '<div id="total_date"></div>';
                          }
						 },
				  						 { text: 'Member', datafield: 'member' , cellsalign: 'left',align: 'left' ,  width: 80 ,aggregates: ['Member'],aggregatesrenderer: function (aggregatedValue, currentValue) {
							  return '<div id="total_member"></div>';
                          }
						 },
				  						 { text: 'Vendor', datafield: 'vendor' , cellsalign: 'center',align: 'center' ,  width: 80 ,aggregates: ['Vendor'],aggregatesrenderer: function (aggregatedValue, currentValue) {
							  return '<div id="total_vendor"></div>';
                          }
						 },
				  						 { text: 'Products', datafield: 'product' , cellsalign: 'left',align: 'left' ,  width: 80 ,aggregates: ['Products'],aggregatesrenderer: function (aggregatedValue, currentValue) {
							  return '<div id="total_product"></div>';
                          }
						 },
				  						 { text: 'Details', datafield: 'detail'  ,  width: 200 ,aggregates: ['Details'],aggregatesrenderer: function (aggregatedValue, currentValue) {
							  return '<div id="total_detail"></div>';
                          }
						 },
				  						 { text: 'Currency', datafield: 'currency' , cellsalign: 'center',align: 'center' ,  width: 60 ,aggregates: ['Currency'],aggregatesrenderer: function (aggregatedValue, currentValue) {
							  return '<div id="total_currency"></div>';
                          }
						 },
				  						 { text: 'Stake', datafield: 'stake' , cellsalign: 'right',align: 'right' ,  width: 70 ,aggregates: ['Stake'],aggregatesrenderer: function (aggregatedValue, currentValue) {
							  return '<div id="total_stake"></div>';
                          }
						 },
				  						 { text: 'Profit/Loss', datafield: 'winloss' , cellsalign: 'right',align: 'right' ,  width: 70 ,aggregates: ['Profit/Loss'],aggregatesrenderer: function (aggregatedValue, currentValue) {
							  return '<div id="total_winloss"></div>';
                          }
						 },
				  						 { text: 'Real Bets', datafield: 'valid' , cellsalign: 'right',align: 'right' ,  width: 70 ,aggregates: ['Real Bets'],aggregatesrenderer: function (aggregatedValue, currentValue) {
							  return '<div id="total_valid"></div>';
                          }
						 },
				  						 { text: 'Result', datafield: 'result' , cellsalign: 'center',align: 'center' ,  width: 70 ,aggregates: ['Result'],aggregatesrenderer: function (aggregatedValue, currentValue) {
							  return '<div id="total_result"></div>';
                          }
						 },
				  	
                ]
            });
			
			$("#dtGrid").on("sort", function (event) {
				 search();
				 
            });
			
			
			
						
		
			
			$("#dateFrom, #dateTo").jqxDateTimeInput({ formatString: "F", width: '120px' , height: '25px' , formatString: "yyyy-MM-dd" ,openDelay: 0,closeDelay: 0});
			
			
				var t = "{{$from}}".split(/[- :]/);
				$('#dateFrom').jqxDateTimeInput('setDate', new Date(t[0], t[1]-1, t[2]));
				
				var t = "{{$to}}".split(/[- :]/);
				$('#dateTo').jqxDateTimeInput('setDate', new Date(t[0], t[1]-1, t[2]));
						
													
	
														
			
			$("#jqxSubmitButton").jqxButton({ width: '150', template: "success"});
			//$("#btn_excel").jqxButton({ width: '80', template: "success"});
			$("#jqxSubmitButton").on('click', function () {
                    $("#events").find('span').remove();
                    $("#events").append('<span>Submit Button Clicked</span>');
             });
			 

			
        });
		
				
		

		function openParentTab(title, url) {
			window.opener.addTab(title, url);
		} 
		
				
		var popup_created = false;
		
		function doConfirmAction(url,status,id){
			
			$('#ok').attr( 'onClick' , 'doConfirmActionProcess(\''+url+'\',\''+status+'\',\''+id+'\')' );
			if( popup_created == false){
				createElements();
			}
		     $('#eventWindow').jqxWindow('open');
		}	
		
		function doConfirmActionProcess(url,status,id){
			
			$.ajax({
				 type: "POST",
				 url:  url,
				 data: {
					_token: "So0aO1tTpDvr6YmTccOkyc7SSjmekciMxYpLmBa7",
					action: status,
					id: id,
				 },
				 beforeSend: function(){
					
				 },
				 success: function(json){
						obj = JSON.parse(json);
						 var str = '';
						 $.each(obj, function(i, item) {
							
							if( 'Successful' == item ){
								alert('Successful');
								doSearch();
							
							}else{
								str += item + '<br>';
							}
						})
						
					
				}
			})
		}
		
        function createElements() {
			popup_created = true;
			$('#eventWindow').show();
            $('#eventWindow').jqxWindow({
                maxHeight: 150, maxWidth: 280, minHeight: 30, minWidth: 250, height: 145, width: 270,
                resizable: false, isModal: true, modalOpacity: 0.1,
                okButton: $('#ok'), cancelButton: $('#cancel'),
                initContent: function () {
                    $('#ok').jqxButton({ width: '65px' });
                    $('#cancel').jqxButton({ width: '65px' });
                    $('#ok').focus();
                }
            });
            $('#events').jqxPanel({ height: '250px', width: '450px' });
            $('#showWindowButton').jqxButton({ width: '100px' });
			
        }
		
		function export_excel(){
			
			$("#dtGrid").jqxGrid('exportdata', 'xls', 'jqxGrid');  
		}
		
		function doChangeDate(dateFrom , dateTo){
			
			var t = dateFrom.split(/[- :]/);
			$('#dateFrom').jqxDateTimeInput('setDate', new Date(t[0], t[1]-1, t[2]));			
			
			var t = dateTo.split(/[- :]/);
			$('#dateTo').jqxDateTimeInput('setDate', new Date(t[0], t[1]-1, t[2]));
		}
		
		function playSound(){
			document.getElementById("sound").innerHTML='<audio autoplay="autoplay"><source src="{{ asset('/admin/audios/alert.mp3') }}" type="http://bo.infiniwin.net/admin/audio/mpeg" /><source src="{{ asset('/admin/audios/alert.ogg') }}" type="audio/ogg" /><embed hidden="true" autostart="true" loop="false" src="{{ asset('/admin/audios/alert.mp3') }}" /></audio>';

		}

		function checkBalance(product){
			
			$.ajax({
				 type: "POST",
				 url:  'product-checkBalance',
				 data: {
					_token: "So0aO1tTpDvr6YmTccOkyc7SSjmekciMxYpLmBa7",
					product: product,
				 },
				 beforeSend: function(){
					$('#'+product).html('<img id="loading" src="{{ asset('/admin/images/ajax-loader.gif') }}"/>');
					
				 },
				 success: function(json){
					$('#'+product).html(json);
				 }
			})
		}
		
		function export_4dlist(){
			window.open('4dpromo-list', '4d list');
		}
	
		function export_excel(){
			
			 $("#dtGrid").jqxGrid('exportdata', 'xls', 'jqxGrid');
		}
    </script>
<style>.fl{float:left;}.label-margin{padding-top:5px;margin-left:10px;margin-right:5px;}.clear{clear:both;}div.triangle{border-bottom:10px solid;border-left:9px solid transparent;border-right:9px solid transparent;bottom:11px;display:inline;font-size:0;height:0;line-height:0;margin:0 auto;position:relative;width:0;}</style>
</head>
<body class='default'>
<div id="sound"></div>
<div id='jqxWidget' style="font-size: 13px; font-family: Verdana; float: left;margin-left:10px;margin-top:10px;">
 
<label class="fl label-margin clear" >Date</label>
		<div id='dateFrom' class="fl">
		
        </div>
					<select id="time_from" name="time_from" style="float:left;margin-left:3px;margin-top:3px;">
				<option value="00:00:00" selected="selected">00:00</option>
				<option value="01:00:00">01:00</option>
				<option value="02:00:00">02:00</option>
				<option value="03:00:00">03:00</option>
				<option value="04:00:00">04:00</option>
				<option value="05:00:00">05:00</option>
				<option value="06:00:00">06:00</option>
				<option value="07:00:00">07:00</option>
				<option value="08:00:00">08:00</option>
				<option value="09:00:00">09:00</option>
				<option value="10:00:00">10:00</option>
				<option value="11:00:00">11:00</option>
				<option value="12:00:00">12:00</option>
				<option value="13:00:00">13:00</option>
				<option value="14:00:00">14:00</option>
				<option value="15:00:00">15:00</option>
				<option value="16:00:00">16:00</option>
				<option value="17:00:00">17:00</option>
				<option value="18:00:00">18:00</option>
				<option value="19:00:00">19:00</option>
				<option value="20:00:00">20:00</option>
				<option value="21:00:00">21:00</option>
				<option value="22:00:00">22:00</option>
				<option value="23:00:00">23:00</option>
			</select>
				<span class="fl label-margin">~</span>
		
		<div id='dateTo' class="fl">
		
        </div>
		
					<select id="time_to" name="time_to" style="float:left;margin-left:3px;margin-top:3px;">
				<option value="00:59:59">00:59</option>
				<option value="01:59:59">01:59</option>
				<option value="02:59:59">02:59</option>
				<option value="03:59:59">03:59</option>
				<option value="04:59:59">04:59</option>
				<option value="05:59:59">05:59</option>
				<option value="06:59:59">06:59</option>
				<option value="07:59:59">07:59</option>
				<option value="08:59:59">08:59</option>
				<option value="09:59:59">09:59</option>
				<option value="10:59:59">10:59</option>
				<option value="11:59:59">11:59</option>
				<option value="12:59:59">12:59</option>
				<option value="13:59:59">13:59</option>
				<option value="14:59:59">14:59</option>
				<option value="15:59:59">15:59</option>
				<option value="16:59:59">16:59</option>
				<option value="17:59:59">17:59</option>
				<option value="18:59:59">18:59</option>
				<option value="19:59:59">19:59</option>
				<option value="20:59:59">20:59</option>
				<option value="21:59:59">21:59</option>
				<option value="22:59:59">22:59</option>
				<option value="23:59:59" selected="selected">23:59</option>
			</select>
				
				<div style="float:left;margin-left:15px;margin-top:5px;">
			<a onclick="doChangeDate('2016-09-29', '2016-09-29');" href="#">+0D</a>
			|
			<a onclick="doChangeDate('2016-09-28', '2016-09-28');" href="#">-1D</a>
			|
			<a onclick="doChangeDate('2016-09-22', '2016-09-29');" href="#">-7D</a>
			|
			<a onclick="doChangeDate('2016-09-01', '2016-09-29');" href="#">MTD</a>
			|
			<a onclick="doChangeDate('2014-01-01', '2016-09-29');" href="#">ALL</a>
		</div>
		
		
		<div class="clear">
            <input style='margin-top: 20px;cursor: pointer;' type="submit" value="Search" id='jqxSubmitButton' onClick="search()"/>
			        </div>
					
<br>
<div id="dtGrid">
</div>
</div>
<div id="eventWindow" style="display:none">
<div>
<img width="14" height="14" src="{{ asset('/admin/jqwidgets/styles/images/help.png') }}" alt=""/>
Confirmation</div>
<div>
<div>
Confirm Process?
</div>
<div>
<div style="float: right; margin-top: 15px;">
<input type="button" id="ok" value="OK" style="margin-right: 10px" onClick=""/>
<input type="button" id="cancel" value="Cancel"/>
</div>
</div>
</div>
</div>
</body>
</html>
