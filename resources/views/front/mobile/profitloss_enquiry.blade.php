@extends('front/mobile/master')

@section('title', 'Bet History')

@section('css')
    @parent

    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

    <style type="text/css">
        #bet_history,
        #bet_history_product
        {
            width: 100%;
        }

        #bet_history th,
        #bet_history_product th
        {
            background-color: #8d8a7b;
            text-align: center;
        }

        #bet_history tbody tr:nth-child(odd),
        #bet_history_product tbody tr:nth-child(odd)
        {
            background-color: #b3ae96;
        }

        #bet_history tbody,
        #bet_history_product tbody
        {
            text-align: right;
        }
    </style>
@endsection

@section('js')
    @parent

    <script src="{{ asset('/front/resources/js/jquery-ui.js') }}"></script>
    <script type="text/javascript">
        $(function () {
            $(".datepicker").datepicker({dateFormat: 'yy-mm-dd', defaultDate: new Date()});

            setTimeout(bet_history, 4000);
        });

        // Bet history.
        function bet_history() {
            $.ajax({
                type: "POST",
                url: "{{action('User\TransactionController@profitlossProcess')}}",
                data: {
                    _token: "{{ csrf_token() }}",
                    date_from: $('#bet_history_date_from').val(),
                    date_to: $('#bet_history_date_to').val(),
                    record_type: $('#bet_history_record_type').val()
                },
            }).done(function (json) {
                //var str;
                var headStr = '';
                var bodyStr = '';

                headStr += '<thead><tr><th>{{ Lang::get('public.Date') }}</th><th>{{ Lang::get('public.Wager') }}</th><th>{{ Lang::get('public.Stake') }}</th><th>{{ Lang::get('public.WinLose') }}</th><th>{{ Lang::get('public.RealBet') }}</th></tr></thead>';
                obj = JSON.parse(json);
                var count = 0;

                $.each(obj, function (i, item) {
                    bodyStr += '<tr><td style="text-decoration: underline;color:blue;">' + item.date + '</td><td>' + item.wager + '</td><td>' + item.stake + '</td><td>' + item.winloss + '</td><td>' + item.validstake + '</td></tr>';
                    count++;
                });

                if (count <= 0) {
                    bodyStr += '<td colspan="6"><h2><center>{{ Lang::get('public.NoDataAvailable') }}</center></h2></td>';
                }

                bodyStr = '<tbody>' + bodyStr + '</tbody>';

                $('#bet_history').html(headStr + bodyStr);
            });
        }

        function transaction_history_product(date_from, product) {
            $.ajax({
                type: "POST",
                url: "{{action('User\TransactionController@profitlossProcess')}}",
                data: {
                    _token: "{{ csrf_token() }}",
                    date_from: date_from,
                    group_by: product,
                },
            }).done(function (json) {
                //var str;
                var headStr = '';
                var bodyStr = '';

                headStr += '<thead><tr><th>{{ Lang::get('public.Product') }}</th><th>{{ Lang::get('public.Wager') }}</th><th>{{ Lang::get('public.Stake') }}</th><th>{{ Lang::get('public.WinLose') }}</th><th>{{ Lang::get('public.RealBet') }}</th></tr></thead>';
                obj = JSON.parse(json);
                var count = 0;

                $.each(obj, function (i, item) {
                    bodyStr += '<tr><td>' + item.product + '</td><td>' + item.wager + '</td><td>' + item.stake + '</td><td>' + item.winloss + '</td><td style="text-decoration: underline;color:blue;">' + item.validstake + '</td></tr>';
                    count++;
                });

                if (count <= 0) {
                    bodyStr += '<td colspan="6"><h2><center>{{ Lang::get('public.NoDataAvailable') }}</center></h2></td>';
                }

                bodyStr = '<tbody>' + bodyStr + '</tbody>';

                $('#bet_history_product').html(headStr + bodyStr);
            });
        }

        setInterval(function() {
            $("#bet_history_button").trigger("click");
        }, 10000);
        // End bet history.
    </script>
@endsection

@section('content')
    <div class="antitle">
        {{ Lang::get('public.BetHistory') }}
    </div>

    <div class="midC">
        <div class="midCinner2">

            <div class="detWrap">
                <span class="betTxt">{{ Lang::get('public.From') }}</span>
                <div class="detList">
                    <input type="text" class="datepicker" id="bet_history_date_from" style="cursor:pointer;" value="{{ date('Y-m-d') }}">
                </div>


                <div class="clr"></div>
            </div>

            <div class="detWrap">
                <span class="betTxt">{{ Lang::get('public.To') }}</span>
                <div class="detList">
                    <input type="text" class="datepicker" id="bet_history_date_to" style="cursor:pointer;" value="{{ date('Y-m-d') }}">
                </div>

                <div class="clr"></div>
            </div>

            <div class="detWrap">
                <div class="detList">
                    <div class="submitB">
                        <a href="javascript:void(0);" onclick="bet_history();">{{ Lang::get('public.Submit') }}</a>
                    </div>
                </div>

                <div class="clr"></div>
            </div>

            <hr>

            <div class="detWrap">
                <div class="table-responsive">
                    <table id="bet_history" class="table" border="0" cellpadding="5" cellspacing="0">
                        <thead>
                        <tr>
                            <th>{{Lang::get('public.Date')}}</th>
                            <th>{{Lang::get('public.Wager')}}</th>
                            <th>{{Lang::get('public.Stake')}} </th>
                            <th>{{Lang::get('public.WinLose')}}</th>
                            <th>{{Lang::get('public.RealBet')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td colspan="6"><h2><center>{{ Lang::get('public.NoDataAvailable') }}</center></h2></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <hr>

            <div class="detWrap">
                <div class="table-responsive">
                    <table id="bet_history_product" class="table" border="0" cellpadding="5" cellspacing="0">
                        <thead>
                        <tr>
                            <th>{{Lang::get('public.Product')}}</th>
                            <th>{{Lang::get('public.Wager')}}</th>
                            <th>{{Lang::get('public.Stake')}} </th>
                            <th>{{Lang::get('public.WinLose')}}</th>
                            <th>{{Lang::get('public.RealBet')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td colspan="6"><h2><center>{{ Lang::get('public.NoDataAvailable') }}</center></h2></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
@endsection
