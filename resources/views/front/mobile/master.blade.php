<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="@yield('keywords')"/>
    <meta name="description" content="@yield('description')"/>

    <title>INFINIWIN: @yield('title')</title>

    <link href="{{ asset('/front/favicon.ico') }}" rel="shortcut icon" type="image/icon" />
    <link href="{{ asset('/front/mobile/resources/css/style.css') }}" rel="stylesheet"><!--position-->
    <link href="{{ asset('/front/mobile/resources/css/reset.css') }}" rel="stylesheet"><!--position-->
    <link href="{{ asset('/front/mobile/resources/css/jquery.bxslider.css') }}" rel="stylesheet"><!--slider-->
    <link href="{{ asset('/front/mobile/resources/css/remodal.css') }}" rel="stylesheet">
    <link href="{{ asset('/front/mobile/resources/css/remodal-default-theme.css') }}" rel="stylesheet">
    <link href="{{url()}}/front/mobile/resources/css/wc.css" rel="stylesheet">
    <style type="text/css">
        .language_btn {
            display: inline;
        }

        .language_btn img {
            height: 26px;
            margin: 6px 8px 0 8px;
        }

        .language_btn a {
            text-decoration: none;
        }

        .setting-bar {
            margin: 0 auto;
        }

        .setting-bar td {
            vertical-align: middle;
        }
    </style>

    @yield('css')

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
    <script src="{{ asset('/front/mobile/resources/js/jquery.bxslider.js') }}"></script>
    <script src="{{ asset('/front/mobile/resources/js/jquery.easing.1.3.js') }}"></script>
    <script src="{{ asset('/front/mobile/resources/js/jquery.fitvids.js') }}"></script>
    <script src="{{ asset('/front/mobile/resources/js/remodal.js') }}"></script>
    <script src="{{url()}}/front/mobile/resources/js/jquery.countdown.js"></script>
</head>
<body style="background-color: #c9cac5;">
<!-- Modal -->
<div class="remodal" data-remodal-id="modal" role="dialog" aria-labelledby="modal1Title" aria-describedby="modal1Desc">
  <button data-remodal-action="close" class="remodal-close" aria-label="Close"></button>
  <div>
    <div class="msgBx">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
		<thead>
					<th></th>
					<th>Title</th>
					<th>Content</th>
				</thead>
				<tbody id="msgTable">
				</tbody>
	</table>
    </div>
  </div>
</div>

<div><div  id="msgINBOX"></div></div>
<!-- Modal -->
  

<!--modal-->
<div class="header">
    <div class="logo">
        <a href="{{ route('homepage') }}">
            <img src="{{ asset('/front/mobile/img/logo.png') }}">
        </a>
    </div>

    @if (!Auth::user()->check())
        <div class="userJoin">
            <a href="{{ route('login') }}"><img src="{{ asset('/front/mobile/img/login-icn.png') }}"></a>
        </div>

        <div class="userLogin">
            <a href="{{ route('register') }}"><img src="{{ asset('/front/mobile/img/join-icn.png') }}"></a>
        </div>
		
    @else
        <div class="userJoin" style="margin-top: 16px;">
            {{--<a href="{{ route('homepage') }}"><img src="{{ asset('/front/mobile/img/home2-btn.png') }}" style="height: 39px;"></a>--}}
            <a href="{{ route('update-profile') }}" style="color: #ffffff;">{{ Session::get('username') }}</a>
        </div>

        <div class="userLogin">
            <a href="{{ route('logout') }}"><img src="{{ asset('/front/mobile/img/logout-w-icon.png') }}"></a>
        </div>
		<div class="message">
            <a href="#modal"  onclick="getAllMessage()"><b id="totalMessage"></b></a>
        </div>
    @endif

    <div class="clr"></div>
</div>

<!--<div class="timerWrap">
    <div class="rowM">
        <div class="col-md-6 col-xs-12">
            <div class="wcWrap">
            <img class="img-responsive" src="{{url()}}/front/mobile/img/wc-img.png">
            </div>
        </div>
        <div class="col-md-6 col-xs-12">
            <div id="getting-started" class="timerDig"></div>
            <script type="text/javascript">
                    $("#getting-started")
                        .countdown("2018/06/14 23:00:00", function(event) {
                        $(this).text(
                        event.strftime('%D Days  %H: %M: %S')
                        );
                    });
            </script>
        </div>
        <div class="clr"></div>
    </div>
 </div>-->

@yield('content')

<div class="bottomBar">
    <div id="language_box" style="background-color: #8d8a7b; text-align: center; display: none;">
        <table class="setting-bar" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td>
                    <b>{{ Lang::get('COMMON.LANGUAGE') }}:&nbsp;</b>
                </td>
                <td>
                    @if( Session::get('currency') == 'MYR' )
                        @if( Lang::getLocale() != 'en' )
                            <div class="language_btn">
                                <a href="{{route( 'language', [ 'lang'=> 'en'])}}">
                                    <img src="{{url()}}/front/img/en.png" />
                                </a>
                            </div>
                        @endif
                        @if( Lang::getLocale() != 'cn' )
                            <div class="language_btn">
                                <a href="{{route( 'language', [ 'lang'=> 'cn'])}}">
                                    <img src="{{url()}}/front/img/cn.png" />
                                </a>
                            </div>
                        @endif
                        @if( Lang::getLocale() != 'vi' )
                            <div class="language_btn">
                                <a href="{{route( 'language', [ 'lang'=> 'vi'])}}">
                                    <img src="{{url()}}/front/img/vi.png" />
                                </a>
                            </div>
                        @endif
                        @if( Lang::getLocale() != 'id' )
                            <div class="language_btn">
                                <a href="{{route( 'language', [ 'lang'=> 'id'])}}">
                                    <img src="{{url()}}/front/img/id.png" />
                                </a>
                            </div>
                        @endif
                    @elseif( Session::get('currency') == 'VND' )
                        @if( Lang::getLocale() != 'vi' )
                            <div class="language_btn">
                                <a href="{{route( 'language', [ 'lang'=> 'vi'])}}">
                                    <img src="{{url()}}/front/img/vi.png" />
                                </a>
                            </div>
                        @endif
                        @if( Lang::getLocale() != 'en' )
                            <div class="language_btn">
                                <a href="{{route( 'language', [ 'lang'=> 'en'])}}">
                                    <img src="{{url()}}/front/img/en.png" />
                                </a>
                            </div>
                        @endif
                        @if( Lang::getLocale() != 'cn' )
                            <div class="language_btn">
                                <a href="{{route( 'language', [ 'lang'=> 'cn'])}}">
                                    <img src="{{url()}}/front/img/cn.png" />
                                </a>
                            </div>
                        @endif

                    @elseif( Session::get('currency') == 'IDR' )
                        @if( Lang::getLocale() != 'id' )
                            <div class="language_btn">
                                <a href="{{route( 'language', [ 'lang'=> 'id'])}}">
                                    <img src="{{url()}}/front/img/id.png" />
                                </a>
                            </div>
                        @endif
                        @if( Lang::getLocale() != 'en' )
                            <div class="language_btn">
                                <a href="{{route( 'language', [ 'lang'=> 'en'])}}">
                                    <img src="{{url()}}/front/img/en.png" />
                                </a>
                            </div>
                        @endif
                    @endif
                </td>
                <td style="padding-left: 50px;">
                    <b>{{ Lang::get('public.DesktopSite') }}:&nbsp;</b>
                </td>
                <td>
                    <a href="{{ route('switchwebtype') }}?type=d">
                        <img src="{{url()}}/front/mobile/img/desktop-site-icon.png" />
                    </a>
                </td>
            </tr>
        </table>
    </div>

    <div class="bottomBarInner">
        <div class="bx2">
            @if (Auth::user()->check())
            <a href="{{ route('transfer') }}?wallet=y">
            @else
            <a href="javascript:void(0);" onclick="alert('{{ Lang::get('COMMON.PLEASELOGIN') }}');">
            @endif
                <img src="{{ asset('/front/mobile/img/wallet-icon.png') }}">
                <span>{{ Lang::get('public.Wallet') }}</span>
            </a>
        </div>

        <div class="bx2">
            @if (Auth::user()->check())
            <a href="{{ route('transfer') }}">
            @else
            <a href="javascript:void(0);" onclick="alert('{{ Lang::get('COMMON.PLEASELOGIN') }}');">
            @endif
                <img src="{{ asset('/front/mobile/img/transfer-icon.png') }}">
                <span>{{ Lang::get('public.Transfer') }}</span>
            </a>
        </div>

        <div class="bx2">
            @if (Auth::user()->check())
            <a href="{{ route('profitloss') }}">
            @else
            <a href="javascript:void(0);" onclick="alert('{{ Lang::get('COMMON.PLEASELOGIN') }}');">
            @endif
                <img src="{{ asset('/front/mobile/img/bet-history-icon.png') }}">
                <span>{{ Lang::get('public.BetHistory') }}</span>
            </a>
        </div>

        <div class="bx2">
            <a href="javascript:livechat();">
                <img src="{{ asset('/front/mobile/img/chat-icon.png') }}">
                <span>{{ Lang::get('public.LiveChat2') }}</span>
            </a>
        </div>

        <div class="bx2">
            <a href="javascript:void(0);" onclick="toggleLanguageBox();">
                <img src="{{ asset('/front/mobile/img/setting-icon.png') }}">
                <span>{{ Lang::get('COMMON.SETTING') }}</span>
            </a>
        </div>

        <div class="clr"></div>
    </div>
</div>

@if ((Session::get('currency') == 'MYR'))
<div id="minigameNotify" style="display: none; position: fixed; bottom: 57px; z-index: 10000;">
    <a href="{{route('minigame')}}" target="_blank"><img src="{{url()}}/front/resources/img/banner-mobile.gif"></a>
</div>
@endif

<script>


    $(document).ready(function(){
		@if (Auth::user()->check())
			getAllMessage();
			load_message();
                        minigame();
		@endif
		
        $('.bxslider').bxSlider();

       // $('#marquee').mouseover(function() {
       //    this.stop();
       // }).mouseout(function() {
       //    this.start();
       // });
    });
    
    function minigame(){
        $.ajax({
                url: "{{ route('minigame-status') }}",
                type: "GET",
                dataType: "text",
                data: {
                },
                success: function (result) {
                        if(result == 1){
                             $("#minigameNotify").slideDown(2000);
                        }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                }
        });
    }

    function toggleLanguageBox() {
        $("#language_box").slideToggle(500);
    }

    function livechat() {
        @if( Session::get('currency') == 'MYR')
            window.open('https://secure.livechatinc.com/licence/3155342/open_chat.cgi?groups=2', 'livechatifw');
        @elseif( Session::get('currency') == 'VND')
            window.open('https://secure.livechatinc.com/licence/3155342/open_chat.cgi?groups=0', 'livechatifw');
        @elseif( Session::get('currency') == 'IDR')
            window.open('https://secure.livechatinc.com/licence/5548521/open_chat.cgi?groups=0', 'livechatifw');
        @endif
    }

    @if (Auth::user()->check())
			function load_message() {
			$.ajax({
				url: "{{ route('showTotalUnseenMessage') }}",
				type: "GET",
				dataType: "text",
				data: {
				},
				success: function (result) {
					if(result>0){
						$("#totalMessage").html("<img src='{{ asset('/front/mobile/img/msg-icon-in.gif') }}'/>");
					}else{
						$("#totalMessage").html("<img src='{{ asset('/front/mobile/img/msg-icon.png') }}'/>");
					}
				},
				error: function (XMLHttpRequest, textStatus, errorThrown) {
				}
			});
		}
		
			 function getAllMessage(){
        $.ajax({
            type: "GET",
            url: "{{route('inbox')}}?type=getData",
            data: {
                _token: "{{ csrf_token() }}"
            },
        }).done(function (data) {
			var trHTML = '';
			var divContent='';
			var obj= data;
			obj = JSON.parse(obj);
			$.each(obj, function(i, item) {
				$.each(item, function(key, val) {
					if(val['status']==0){	
						trHTML+='<tr><td><img src="{{ asset('/front/mobile/img/mark-unread.png') }}"/></td><td>'+val['title']+'</td><td id="valTitle" ><a href="#modalC'+val['id']+'" onClick="updateStatus('+val['id']+','+val['is_all']+')">'+val['content']+'</a></td></tr>';
					}else if(val['status']==1){
						trHTML+='<tr><td><img src="{{ asset('/front/mobile/img/mark-read.png') }}"/></td><td>'+val['title']+'</td><td id="valTitle" ><a href="#modalC'+val['id']+'" >'+val['content']+'</a></td></tr>';	
					}
					
					divContent+='<div class="remodalcustom" data-remodal-id="modalC'+val['id']+'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"><button data-remodal-action="close" class="remodal-close" aria-label="Close"></button><div class="msgBx" id="#modalC'+val['id']+'">'+val['content']+'</div></div>';
					
					
					$("#msgTable").html(trHTML);
					$("#msgINBOX").html(divContent);
					$(".remodalcustom").remodal();
					//$('[data-remodal-id="modalC'+val['id']+'"]').remodal(divContent);
					})
			})
		});
	}
	
		function updateStatus(id,is_all){
			$.ajax({
				type: "POST",
				url: "{{action('User\InboxMessage@viewMessage')}}?id="+id+"&is_all="+is_all,
				data: {
					_token: "{{ csrf_token() }}"
				},
			});
		}
        function update_mainwallet(){
            $.ajax({
                type: "POST",
                url: "{{route( 'mainwallet', [ '_token'=> csrf_token() ])}}",
                beforeSend: function(balance){
                },
                success: function(balance) {
                    var walletObj = $('.main_wallet');

                    if (walletObj.is("input[type='text']")) {
                        $('.main_wallet').val(parseFloat(Math.round(balance * 100) / 100).toFixed(2));
                    } else {
                        $('.main_wallet').html(parseFloat(Math.round(balance * 100) / 100).toFixed(2));
                    }
//						total_balance += parseFloat(balance);
                }
            });
        }

        function getBalance(type) {
            var total_balance = 0;
            var currency = "{{ Session::get('currency') }}";

            $.when(
                $.ajax({
                    type: "POST",
                    url: "{{route('mainwallet', [ '_token'=> csrf_token()])}}",
                    beforeSend: function(balance){
                    },
                    success: function(balance){
                        var walletObj = $('.main_wallet');

                        if (walletObj.is("input[type='text']")) {
                            $('.main_wallet').val(parseFloat(Math.round(balance * 100) / 100).toFixed(2));
                        } else {
                            $('.main_wallet').html(parseFloat(Math.round(balance * 100) / 100).toFixed(2));
                        }
                        total_balance += parseFloat(balance);
                    }
                }),
                @foreach(Session::get('products_obj') as $prdid => $object)
                    $.ajax({
                        type: "POST",
                        url: "{{route( 'getbalance', [ '_token'=> csrf_token(), 'product'=> $object->code ])}}&rd=<?php echo rand(10000, 99999); ?>",
                        beforeSend: function(balance){
                            var balObj = $('.{{$object->code}}_balance');

                            if (balObj.is("input[type='text']")) {
                                var balLoadingObj = $(".{{$object->code}}_balance_loading");

                                if (balLoadingObj.length) {
                                    balLoadingObj.show();
                                }
                            } else {
                                balObj.html('<img style="" src="{{url()}}/front/img/ajax-loading.gif" width="20" height="20">');
                            }
                        },
                        success: function(balance){
                            var balObj = $('.{{$object->code}}_balance');

                            if (balance != '{{Lang::get('Maintenance')}}') {
                                balObj.each(function () {
                                    if ($(this).is("input[type='text']") || $(this).is("input[type='hidden']")) {
                                        $(this).val(parseFloat(Math.round(balance * 100) / 100).toFixed(2));
                                    } else {
                                        $(this).html(parseFloat(Math.round(balance * 100) / 100).toFixed(2));
                                    }
                                });

                                var skipTotal = false;

                                if (!skipTotal) {
                                    total_balance += parseFloat(balance);
                                }
                            } else {
                                balObj.each(function () {
                                    if ($(this).is("input[type='text']") || $(this).is("input[type='hidden']")) {
                                        $(this).val(balance);
                                    } else {
                                        $(this).html(balance);
                                    }
                                });
                            }

                            var balLoadingObj = $(".{{$object->code}}_balance_loading");

                            if (balLoadingObj.length) {
                                balLoadingObj.hide();
                            }
                        }
                    })
                    @if ($prdid != Session::get('last_prdid'))
                    ,
                    @endif
                @endforeach
            ).then(function() {
                var objs = $('#total_balance, #top_total_balance');

                objs.each(function () {
                    var obj = $(this);
                    if (obj.is("input[type='text']") || obj.is("input[type='hidden']")) {
                        obj.val(parseFloat(Math.round(total_balance * 100) / 100).toFixed(2));
                    } else {
                        obj.html(parseFloat(Math.round(total_balance * 100) / 100).toFixed(2));
                    }
                });

                $(".total_balance_loading").hide();
            });
        }
    @endif

    @if( Session::get('currency') == 'MYR')

        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function()
            { (i[r].q=i[r].q||[]).push(arguments)}

            ,i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-43720111-1', 'auto');
        ga('send', 'pageview');

    @elseif( Session::get('currency') == 'TWD')

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-43720111-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
            gtag('config', 'UA-43720111-1');
        </script>

    @elseif( Session::get('currency') == 'VND')

        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function()
            { (i[r].q=i[r].q||[]).push(arguments)}

            ,i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-43720111-1', 'auto');
        ga('send', 'pageview');

    @endif
</script>
<script>
  $(document).on('opening', '.remodal', function () {
    console.log('opening');
  });

  $(document).on('opened', '.remodal', function () {
    console.log('opened');
  });

  $(document).on('closing', '.remodal', function (e) {
    console.log('closing' + (e.reason ? ', reason: ' + e.reason : ''));
  });

  $(document).on('closed', '.remodal', function (e) {
    console.log('closed' + (e.reason ? ', reason: ' + e.reason : ''));
  });

  $(document).on('confirmation', '.remodal', function () {
    console.log('confirmation');
  });

  $(document).on('cancellation', '.remodal', function () {
    console.log('cancellation');
  });

//  Usage:
//  $(function() {
//
//    // In this case the initialization function returns the already created instance
//    var inst = $('[data-remodal-id=modal]').remodal();
//
//    inst.open();
//    inst.close();
//    inst.getState();
//    inst.destroy();
//  });

  //  The second way to initialize:
  $('[data-remodal-id=modal2]').remodal({
    modifier: 'with-red-theme'
  });
</script>

@yield('js')

</body>
</html>