@extends('front/mobile/master')

@section('title', 'Promotions')

@section('css')
    @parent

    <link href="{{url()}}/front/resources/css/jquery.megamenu.css" rel="stylesheet">
    <link href="{{url()}}/front/resources/css/promo.css" rel="stylesheet">
    <style type="text/css">
        .tnc_list li { padding: 3px 0; }
        .my-button_std {
            background: none!important;
            border: 0px solid!important;
            cursor: pointer!important;
            padding: 0px!important;
        }
        .element_to_pop_up {
            background-color: #000;
            border-radius: 15px;
            border: 1px solid #C31818;
            color: #fff;
            display: none;
            padding: 20px;
            width: auto !important;
            min-width: 300px !important;
            min-height: 180px;
            font-size: 12px;
        }
    </style>
@endsection

@section('js')
    @parent

    <script src="{{url()}}/front/resources/js/jquery.fancybox.js"></script>
    <script type="text/javascript">
        $(function () {
            var compabilityClasses = ["my-button", "element_to_pop_up"];

            for (var i = 0; i < compabilityClasses.length; i++) {
                $("." + compabilityClasses[i]).each(function () {
                    var obj = $(this);
                    var id = obj.attr("id");
                    var idIndex = id.replace(compabilityClasses[i], "");

                    if (parseFloat(idIndex) >= 10) {
                        obj.addClass(compabilityClasses[i] + "_std");
                    }
                });
            }
        });

        @foreach ($promo as $key => $value )
        (function($) {

            // DOM Ready
            $(function() {

                // Binding a click event
                // From jQuery v.1.7.0 use .on() instead of .bind()
                $('#my-button{{ $key+1}}').bind('click', function(e) {

                    // Prevents the default action to be triggered.
                    e.preventDefault();

                    // Triggering bPopup when click event is fired
                    $('#element_to_pop_up{{ $key+1}}').bPopup();

                });

            });

            $(function() {

                // Binding a click event
                // From jQuery v.1.7.0 use .on() instead of .bind()
                $('#my-button-tnc').bind('click', function(e) {

                    // Prevents the default action to be triggered.
                    e.preventDefault();

                    // Triggering bPopup when click event is fired
                    $('#element_to_pop_up_tnc').bPopup();

                });

            });

        })(jQuery);
        @endforeach

        /*================================================================================
         * @name: bPopup - if you can't get it up, use bPopup
         * @author: (c)Bjoern Klinggaard (twitter@bklinggaard)
         * @demo: http://dinbror.dk/bpopup
         * @version: 0.9.4.min
         ================================================================================*/
        (function(b){b.fn.bPopup=function(z,F){function K(){a.contentContainer=b(a.contentContainer||c);switch(a.content){case "iframe":var h=b('<iframe class="b-iframe" '+a.iframeAttr+"></iframe>");h.appendTo(a.contentContainer);r=c.outerHeight(!0);s=c.outerWidth(!0);A();h.attr("src",a.loadUrl);k(a.loadCallback);break;case "image":A();b("<img />").load(function(){k(a.loadCallback);G(b(this))}).attr("src",a.loadUrl).hide().appendTo(a.contentContainer);break;default:A(),b('<div class="b-ajax-wrapper"></div>').load(a.loadUrl,a.loadData,function(){k(a.loadCallback);G(b(this))}).hide().appendTo(a.contentContainer)}}function A(){a.modal&&b('<div class="b-modal '+e+'"></div>').css({backgroundColor:a.modalColor,position:"fixed",top:0,right:0,bottom:0,left:0,opacity:0,zIndex:a.zIndex+t}).appendTo(a.appendTo).fadeTo(a.speed,a.opacity);D();c.data("bPopup",a).data("id",e).css({left:"slideIn"==a.transition||"slideBack"==a.transition?"slideBack"==a.transition?g.scrollLeft()+u:-1*(v+s):l(!(!a.follow[0]&&m||f)),position:a.positionStyle||"absolute",top:"slideDown"==a.transition||"slideUp"==a.transition?"slideUp"==a.transition?g.scrollTop()+w:x+-1*r:n(!(!a.follow[1]&&p||f)),"z-index":a.zIndex+t+1}).each(function(){a.appending&&b(this).appendTo(a.appendTo)});H(!0)}function q(){a.modal&&b(".b-modal."+c.data("id")).fadeTo(a.speed,0,function(){b(this).remove()});a.scrollBar||b("html").css("overflow","auto");b(".b-modal."+e).unbind("click");g.unbind("keydown."+e);d.unbind("."+e).data("bPopup",0<d.data("bPopup")-1?d.data("bPopup")-1:null);c.undelegate(".bClose, ."+a.closeClass,"click."+e,q).data("bPopup",null);H();return!1}function G(h){var b=h.width(),e=h.height(),d={};a.contentContainer.css({height:e,width:b});e>=c.height()&&(d.height=c.height());b>=c.width()&&(d.width=c.width());r=c.outerHeight(!0);s=c.outerWidth(!0);D();a.contentContainer.css({height:"auto",width:"auto"});d.left=l(!(!a.follow[0]&&m||f));d.top=n(!(!a.follow[1]&&p||f));c.animate(d,250,function(){h.show();B=E()})}function L(){d.data("bPopup",t);c.delegate(".bClose, ."+a.closeClass,"click."+e,q);a.modalClose&&b(".b-modal."+e).css("cursor","pointer").bind("click",q);M||!a.follow[0]&&!a.follow[1]||d.bind("scroll."+e,function(){B&&c.dequeue().animate({left:a.follow[0]?l(!f):"auto",top:a.follow[1]?n(!f):"auto"},a.followSpeed,a.followEasing)}).bind("resize."+e,function(){w=y.innerHeight||d.height();u=y.innerWidth||d.width();if(B=E())clearTimeout(I),I=setTimeout(function(){D();c.dequeue().each(function(){f?b(this).css({left:v,top:x}):b(this).animate({left:a.follow[0]?l(!0):"auto",top:a.follow[1]?n(!0):"auto"},a.followSpeed,a.followEasing)})},50)});a.escClose&&g.bind("keydown."+e,function(a){27==a.which&&q()})}function H(b){function d(e){c.css({display:"block",opacity:1}).animate(e,a.speed,a.easing,function(){J(b)})}switch(b?a.transition:a.transitionClose||a.transition){case "slideIn":d({left:b?l(!(!a.follow[0]&&m||f)):g.scrollLeft()-(s||c.outerWidth(!0))-C});break;case "slideBack":d({left:b?l(!(!a.follow[0]&&m||f)):g.scrollLeft()+u+C});break;case "slideDown":d({top:b?n(!(!a.follow[1]&&p||f)):g.scrollTop()-(r||c.outerHeight(!0))-C});break;case "slideUp":d({top:b?n(!(!a.follow[1]&&p||f)):g.scrollTop()+w+C});break;default:c.stop().fadeTo(a.speed,b?1:0,function(){J(b)})}}function J(b){b?(L(),k(F),a.autoClose&&setTimeout(q,a.autoClose)):(c.hide(),k(a.onClose),a.loadUrl&&(a.contentContainer.empty(),c.css({height:"auto",width:"auto"})))}function l(a){return a?v+g.scrollLeft():v}function n(a){return a?x+g.scrollTop():x}function k(a){b.isFunction(a)&&a.call(c)}function D(){x=p?a.position[1]:Math.max(0,(w-c.outerHeight(!0))/2-a.amsl);v=m?a.position[0]:(u-c.outerWidth(!0))/2;B=E()}function E(){return w>c.outerHeight(!0)&&u>c.outerWidth(!0)}b.isFunction(z)&&(F=z,z=null);var a=b.extend({},b.fn.bPopup.defaults,z);a.scrollBar||b("html").css("overflow","hidden");var c=this,g=b(document),y=window,d=b(y),w=y.innerHeight||d.height(),u=y.innerWidth||d.width(),M=/OS 6(_\d)+/i.test(navigator.userAgent),C=200,t=0,e,B,p,m,f,x,v,r,s,I;c.close=function(){a=this.data("bPopup");e="__b-popup"+d.data("bPopup")+"__";q()};return c.each(function(){b(this).data("bPopup")||(k(a.onOpen),t=(d.data("bPopup")||0)+1,e="__b-popup"+t+"__",p="auto"!==a.position[1],m="auto"!==a.position[0],f="fixed"===a.positionStyle,r=c.outerHeight(!0),s=c.outerWidth(!0),a.loadUrl?K():A())})};b.fn.bPopup.defaults={amsl:50,appending:!0,appendTo:"body",autoClose:!1,closeClass:"b-close",content:"ajax",contentContainer:!1,easing:"swing",escClose:!0,follow:[!0,!0],followEasing:"swing",followSpeed:500,iframeAttr:'scrolling="no" frameborder="0"',loadCallback:!1,loadData:!1,loadUrl:!1,modal:!0,modalClose:!0,modalColor:"#000",onClose:!1,onOpen:!1,opacity:0.7,position:["auto","auto"],positionStyle:"absolute",scrollBar:!0,speed:250,transition:"fadeIn",transitionClose:!1,zIndex:9997}})(jQuery);
    </script>
@endsection

@section('content')

    <div class="antitle">
        {{ Lang::get('public.Promotion') }}
    </div>

    <div class="midC" style="padding-bottom: 90px;">
        @foreach ($promo as $key => $value )
            @if (isset($value['image']) && strlen($value['image']) > 0)
                <div id="my-button{{$key+1}}" class="promoBx mt04 my-button">
                    <div class="promoTxt" style="z-index: 0;"><span>{{ $value['title'] }}</span></div>
                    <img class="img-responsive" src="{{ $value['image'] }}" data-toggle="modal" data-target="#md_promo_details_{{ $key }}" style="height: auto; width: 100%;">
                </div>
                <div class="element_to_pop_up" id="element_to_pop_up{{$key+1}}">
                    <a class="b-close">x</a>
                        <h3>{{$value['title']}}</h3>
                    <?php echo htmlspecialchars_decode($value['content']); ?>
                </div>
            @endif
        @endforeach

        <div style="width: 100%; text-align: center;">
            <a id="my-button-tnc" href="javascript:void(0);" class="my-button" style="color: #fff;">
                @if (Lang::getLocale() == 'tw')
                    一般促銷條款和條件
                @elseif (Lang::getLocale() == 'cn')
                    一般促销条款和条件
                @else
                    General Terms &amp; Conditions of Promotions
                @endif
            </a>
        </div>

        <div class="element_to_pop_up" id="element_to_pop_up_tnc">
            <a class="b-close">x</a>
            <div>
                @include('front.include.promotion_tnc')
            </div>
        </div>
    </div>
@endsection
