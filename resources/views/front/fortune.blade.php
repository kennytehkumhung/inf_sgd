@extends('front/master')

@if (Session::get('currency') == 'TWD')
    @section('title', '無極限娛樂網')
@elseif (Session::get('currency') == 'MYR')
    @section('title', 'Play Live 4D Malaysia Online Betting')
    @section('keywords', 'Play Live 4D Malaysia Online Betting')
    @section('description', 'Play Live Fortune 4D at InfiniWin Online Casino Malaysia - awarded Best Online Casino and enjoy 100% Welcome Deposit Bonus. Join InfiniWin casino online today.')
@endif

@section('content')
    @if (Request::input('reminder') == 'y')
        <script type="text/javascript">
            $(function () {
                alert("{{ Lang::get('public.Fortune4DTokenReminder') }}");
            });
        </script>
    @endif

<iframe id="fortune" frameborder="0" style="overflow:hidden;overflow-x:hidden;overflow-y:hidden;height: 100%; min-height: 640px; width:100%;" src="{{route('4dpromo')}}"></iframe>

<div class="seo_content">
    <h1 style="font-size:13px;">Easy Money With Infiniwin Fortune 4D</h1>
    <p>Infiniwin is undeniably one of the best online casinos in Malaysia. Not only that it has casino games with great graphics and interesting gameplay, Infiniwin also provides simpler but addictive games for their loyal players. These games are not only for fun, it is also a way for Infiniwin to reward their players with great prizes. Slot games, sportsbook, and fortune 4D are just some of these ‘simpler’ games currently available.</p>
    <p>Fortune 4D is a very interesting game with very interesting prizes. You will use tickets to roll the 4D numbers on the screen. If the number you get is the lucky number, you will be rewarded based on the 4D result from Sports Toto and other 4D game providers. The prizes breakdown are as below:</p>
    <ul style='list-style: none;'>
        <li>1. First prize - RM2,500</li>
        <li>2. Second prize - RM1,000</li>
        <li>3. Third prize - RM500</li>
        <li>4. Special prize - RM200</li>
        <li>5. Consolation prize - RM60</li>
    </ul>
    <p>At first glance, you may think that the prizes are not that high, if you are a high roller. You will be surprised that the prize is actually for every RM1 bet you made. Just imagine the amount of money you will get if you won the first prize with a RM1,000 bet!</p>
    <p>Currently, Infiniwin is having a Fortune 4D deposit promotion until June 30th, 2018. Every deposit worth RM300 will get a free Fortune 4D ticket. However, keep in mind that the Fortune 4D ticket is not transferable and your account will be locked if you violates this rule. Every players in Infiniwin is also not allowed to create a second or third account with Infiniwin. If you want to get money for you and your family’s long-awaited overseas trip fast, then Fortune 4D is where your destiny awaits!</p>
    <p>Aside from Fortune 4D, there are other games that you can play at Infiniwin. If you are a new Infiniwin player, you can get a lot of rewards and promotions special for new players. Make sure that you do not put these rewards to waste! </p>
</div>
@stop

