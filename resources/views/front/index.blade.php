@extends('front/master')

@if (Session::get('currency') == 'TWD')
    @section('title', '無極限娛樂網')
@elseif (Session::get('currency') == 'MYR')
    @section('title', 'The Best of Online Live Betting Casino')
    @section('keywords', 'The Best Online Live Casino in Malaysia')
    @section('description', 'A leader in online casino. Play at live casino, online slot or bet on casino games. Enjoy sports betting, horse racing betting and more. Bet now!')
@endif

@section('top_js')
<script type="text/javascript">



   if($.cookie("css")) {
      $("link").attr("href",$.cookie("css"));
   }
   $(document).ready(function() { 
      $(".lgCont li a").click(function() { 
         $("link").attr("href",$(this).attr('rel'));
         $.cookie("css",$(this).attr('rel'), {expires: 365, path: '/'});
         return false;
      });
   });
</script>

<script>
          $(document).ready(function () {
              @if (Session::get('currency') == 'TWD')
                if ($("body").data("hasTheme") != "Y") {
                  $('body').css('background-image', 'url({{ asset('/front/img/tw/main_bg.jpg') }})');
                }
              @endif

              fakeJackport();
              setInterval(updateJackport, 3000);
          });

          function fakeJackport() {

              var value = Math.floor(Math.random() * 10);

              value = 1356289 + value * 2.496;


              var value2 = value.toFixed(2);

              $('#jp_1').text(addCommas(value2));

          }

          function updateJackport() {
              var val = $("span[id^='jp_1']").text();
              val = removeComma(val);

              if (val > 1416669)
                  val = 1416789;

              var value = Math.floor(Math.random() * 10);
              value = value * 0.027;
              
              var value2 = (parseFloat(val) + value).toFixed(2);

              $('#jp_1').text(addCommas(value2));
          }

          function addCommas(str) {
              var parts = (str + "").split("."),
          main = parts[0],
          len = main.length,
          output = "",
          i = len - 1;

              while (i >= 0) {
                  output = main.charAt(i) + output;
                  if ((len - i) % 3 === 0 && i > 0) {
                      output = "," + output;
                  }
                  --i;
              }
              // put decimal part back
              if (parts.length > 1) {
                  output += "." + parts[1];
              }
              return output;
          }

          function removeComma(str) {
              var parts = (str + "").split(",");

              var output = "";

              for (var i = 0; i < parts.length; i++) {
                  output += parts[i];
              }
              return output;
          }
</script>
@stop

@section('content')

	@if( $popup_banner )
    <div id="masklayer" style="height: 100%;position: fixed;opacity: 0.6;background-color: black;z-index: 10001;left: 0px;right: 0px;top: 0px;display:block;" onclick="closePopup();"></div>
     <img src="{{$popup_banner}}" class="popup_img">
     @endif

	


          <ul class="bxslider">
		  @if(isset($banners))
			@foreach( $banners as $key => $banner )
				<li><a target="_blank" href="@if($banner->url != 'none'){{$banner->url}}@endif"><img src="{{$banner->domain}}/{{$banner->path}}" class="circles" /></a></li>
				
            @endforeach
		  @endif
		
          </ul>


<!--Jackpot-->
<div class="jackP">
	<ul style="padding: 18px 0px 20px;" id="accordion">
        @if (Session::get('currency') == 'TWD')
            <li>
                <a onClick="@if (!Auth::user()->check())alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif" href="@if (Auth::user()->check()){{route('slot', [ 'type' => 'uc8'  ])}}@endif"><img src="{{url()}}/front/img/tw/uc-hpg.png"></a>
            </li>
            <li>
                <a href="javascript:void(0);" onClick="@if (Auth::user()->check())window.open('{{route('alb')}}', 'casino13');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif"><img src="{{url()}}/front/img/tw/allbet-hpg.png"></a>
            </li>
            <li>
                <a href="javascript:void(0);" onclick="@if (Auth::user()->check())window.open('{{route('vgs', [ 'type' => 'casino' ] )}}', 'casino13');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif"><img src="{{url()}}/front/img/tw/ev-hpg.png"></a>
            </li>
            <li>
                <a onClick="@if (!Auth::user()->check())alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif" href="@if (Auth::user()->check()){{route('mxb', [ 'type' => 'casino' , 'category' => 'baccarat' ] )}}@endif"><img src="{{url()}}/front/img/tw/xp-hpg.png"></a>
            </li>
            <li>
                <a href="javascript:void(0);" onClick="@if (Auth::user()->check())window.open('{{route('hog')}}', 'casino15');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif"><img src="{{url()}}/front/img/tw/ho-hpg.png"></a>
            </li>
            <li>
                <a href="javascript:void(0);" onClick="@if (Auth::user()->check())window.open('{{route('jok')}}', 'jok_slot');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif"><img src="{{url()}}/front/img/tw/jk-hpg.png"></a>
            </li>
            <li>
                <a onClick="@if (!Auth::user()->check())alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif" href="@if (Auth::user()->check()){{route('slot', [ 'type' => 'sky'  ])}}@endif"><img src="{{url()}}/front/img/tw/sky-hpg.png"></a>
            </li>
            <li>
                <a onClick="@if (!Auth::user()->check())alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif" href="@if (Auth::user()->check()){{route('ctb')}}@endif"><img src="{{url()}}/front/img/tw/ctb-hpg.png"></a>
            </li>
        @else
            <li>
                <a onClick="@if (!Auth::user()->check())alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif" href="@if (Auth::user()->check()){{route('mxb', [ 'type' => 'casino' , 'category' => 'baccarat' ] )}}@endif"><img src="{{url()}}/front/resources/img/mxb-hpg.png"></a>
            </li>
            <li>
                <a href="javascript:void(0);" onClick="@if (Auth::user()->check())window.open('{{route('pltiframe')}}', 'casino13');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif"><img src="{{url()}}/front/resources/img/pt-hpg.png"></a>
            </li>
            <li>
                <a href="javascript:void(0);" onClick="@if (Auth::user()->check())window.open('{{route('alb')}}', 'casino13');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif"><img src="{{url()}}/front/resources/img/allbet-hpg.png"></a>
            </li>
            <li>
                <a href="javascript:void(0);" onclick="@if (Auth::user()->check())window.open('{{route('agg')}}', 'casino11');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif"><img src="{{url()}}/front/resources/img/ag-hpg.png"></a>
            </li>
            <li>
                <a href="{{ route('ibc') }}"><img src="{{url()}}/front/resources/img/ibc-hpg.png"></a>
            </li>
            {{--<li>--}}
                {{--<a href="javascript:void(0);" onClick="@if (Auth::user()->check())window.open('{{route('opu', [ 'type' => 'casino' , 'category' => 'live'])}}', 'casino16');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif"><img src="{{url()}}/front/resources/img/ops-hpg.png"></a>--}}
            {{--</li>--}}
            <li>
                <a href="javascript:void(0);" onclick="@if (Auth::user()->check())window.open('{{route('vgs', [ 'type' => 'casino' ] )}}', 'casino13');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif"><img src="{{url()}}/front/resources/img/vgs-hpg.png"></a>
            </li>
            <li>
                <a href="javascript:void(0);" onClick="@if (Auth::user()->check())window.open('{{route('hog')}}', 'casino15');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif"><img src="{{url()}}/front/resources/img/ho-hpg.png"></a>
            </li>
        @endif
	</ul>
</div>
<!--Jackpot-->

<div class="line-seperator"></div>


<!--Bottom Section-->
{{--@if (Session::get('currency') == 'TWD')--}}
<div class="bott">
<div class="col1">
<table width="auto" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>
      @if (Session::get('currency') == 'TWD')
        <a href="{{route('promotion')}}"><img src="{{url()}}/front/img/tw/infi-promotion-banner.gif" alt=""/></a>
      @else
        <a href="{{route('fortune')}}"><img src="{{url()}}/front/resources/img/infi-promotion-banner.gif" alt=""/></a>
      @endif
    </td>
  </tr>
</table>
</div>


@if (Session::get('currency') == 'TWD')
    <div class="col3" style="background: url({{url()}}/front/img/tw/jackpot.png) 0 0 no-repeat;">
        <div class="digit">
            {{Session::get('currency')}} <span id="jp_1">88,888.888.00</span>
        </div>
    </div>
@else
    <div class="col3">
        <div class="digit">
            {{Session::get('currency')}} <span id="jp_1">88,888.888.00</span>
        </div>
    </div>
@endif


</div>

<div class="clr"></div>

{{--@if (Session::get('currency') == 'TWD')--}}
<div class="col6">
    <table cellspacing="0" cellpadding="0" border="0">
        <tbody>
        <tr>
            <td>
                @if (Session::get('currency') == 'TWD')
                    <img src="{{url()}}/front/img/tw/live-support-title.png">
                @else
                    <img src="{{url()}}/front/resources/img/live-support-title.png">
                @endif
            </td>
        </tr>
        <tr>
            <td>
                <div class="bx1" style="font-size:11px;">
                    <span>{{Lang::get('public.24HoursActiveUser')}}</span>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div class="bx2" style="font-size:11px;">
                    <span>{{Lang::get('public.24HoursCumulativeAmountOfBets')}}</span>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div class="bx3" style="font-size:11px;">
                    <span>{{Lang::get('public.24HoursCumulativeDepositWithdrawals')}}</span>
                </div>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
        </tbody>
    </table>
</div>
<div class="col1">
    <table width="auto" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td>
                @if (Session::get('currency') == 'TWD')
                    <a href="{{route('sport')}}"><img src="{{url()}}/front/img/tw/main_sportbook.png" width="554" height="228"></a>
                @else
                    <a href="{{route('android')}}"><img src="{{url()}}/front/resources/img/sportbook.gif" width="554" height="228"></a>
                @endif
            </td>
        </tr>
    </table>
</div>


<div class="clr"></div>
@if (Session::get('currency') == 'MYR')
<div class="seo_content">
    <h1 style="font-size:13px;">Best Casino Experience and Reward at Infiniwin</h1>
    <p>Infiniwin is one of the foremost online casino in Malaysia. Focusing on Malaysia allows Infiniwin to have a dedicated customer service team that is able to provide support in multiple languages like Malay and Mandarin. The customer service is also available 24/7 to ensure that your online casino experience will not be interrupted. If you encounter any problems while using Infiniwin platform, the customer service is the place to start.</p>
    <p>If you are new to online casino games or sportsbook, you can try to register a new account at Infiniwin. Registering is easy, where you will need to fill in a user name, password, a valid e-mail address, full name, as well as contact number. The contact number, full name and e-mail address are very important because it will be used for bank withdrawals and deposits. Therefore, double check these items when you are registering.</p>
    <p>Once you have joined Infiniwin, you will notice that Infiniwin is more than just an online casino in Malaysia. It is a place where you can earn a lot of money while having fun. There are hundreds of games to choose from and you will certainly find a game that you will love.</p>
    <p>Since the World Cup 2018 is near, it is worth mentioning that Infiniwin hold special event and promotion to celebrate this grand football tournament. The sportsbook for World Cup 2018 will surely be a hot place for both new and experienced sportsbook enthusiasts. The odds will be better and the stakes will be high as well. If sportsbook is not your thing, there are always other games to play like slot games, 4D, racebook and online casinos.</p>
    <p>Casinos in Malaysia are aplenty but those who are at the same standard as Infiniwin are few. Create an Infiniwin account now to get all those promotions and rewards currently available. If you wait, you might missed them so hurry up!</p>
</div>
@endif
<!--Bottom Section-->


<div id="tryMobileBox" style="display: none; background-color: #393928; border: 3px #bd9545 solid; color: #fff; position: fixed; bottom: 0; right: 0; width: 500px; height: 350px; z-index: 10000;">
    <div style="position: relative;">
        <div style="text-align: right;">
            <img style="padding: 8px; cursor: pointer; width: auto; height: 30px;" onmouseout="this.src='{{ asset('/front/img/closeBtn.png') }}'" onmouseover="this.src='{{ asset('/front/img/closeBtn_hover.png') }}'" src="{{ asset('/front/img/closeBtn.png') }}" onclick="closeTryMobileBox();">
        </div>
        <div style="padding: 8px; text-align: center;">
            <strong style="font-size: 4em;">{{ Lang::get('public.GoMobileFriendly') }}</strong><br><br>
            <span style="font-size: 3em;">{{ Lang::get('public.VisitOurMobileSiteForBetterExperience') }}</span><br><br>
            <table border="0" cellpadding="0" cellspacing="8" style="width: 100%; height: 90px;">
                <tr>
                    <td style="width: 50%;">
                        <button style="font-size: 2em; cursor: pointer; margin: 6px 0; height: 100%; width: 100%; border: none; background-color: #4CAF50; color: #fff; padding: 5px 0;" onclick="window.location.href='{{ route('switchwebtype') }}?type=m';">{{ Lang::get('COMMON.OK') }}</button><br>
                    </td>
                    <td>
                        <button style="font-size: 2em; cursor: pointer; margin: 6px 0; height: 100%; width: 100%; border: none; background-color: #f44336; color: #fff; padding: 5px 0;" onclick="closeTryMobileBox();">{{ Lang::get('public.NoThanks') }}</button><br>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>

@stop

@section('bottom_js')
<script>
	$(document).ready(function(){
		activeItem = $("#accordion li:first");
		$(activeItem).addClass('active');
		$("#accordion li").hover(function(){
		$(activeItem).animate({width: "108px"}, {duration:300, queue:false});
		$(this).animate({width: "260px"}, {duration:300, queue:false});
		activeItem = this;
		});

		@if (isset($is_mobile_agent) && $is_mobile_agent == true)
            $("#tryMobileBox").slideDown(2000);
        @endif
	});

	function closeTryMobileBox() {
        $('#tryMobileBox').hide();
    }
</script>

@stop