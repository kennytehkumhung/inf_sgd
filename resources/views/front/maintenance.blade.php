@extends('front/master')

@section('title', 'Maintenance')

@section('top_js')

@stop

@section('content')
<div class="midSect">
	<div class="midSectInner">
		<div class="spacingTop"></div>
		<div class="maintenance">      
			<span class="maintainMSG">We are upgrading our server and  <br>user interface for a <br>better gaming experience</span>
			<span class="maintainMSGsub">For more details, please contact us at inquire@infiniwin.net</span>
		</div>
	</div>
</div>
@stop

@section('bottom_js')

@stop