<!--Footer-->
<div class="footer">

<div class="line"></div>
<div class="copyright">
<strong>Copyright © INFINIWIN. All Rights Reserved.</strong><br>
<ul>
<li><a href="#">About Us</a></li>
<li><a href="#">Banking Options</a></li>
<li><a href="#">Contact Us</a></li>
<li><a href="#">FAQ</a></li>
<li><a href="#">How To Join</a></li>
<li><a href="#">Terms And Conditions</a></li>
<li style="border: 0px;"><a href="#">Sitemap</a></li>
</ul>
</div>

<div class="social">
<ul>
<li><a href="#"><img src="front/img/twit-icon.png" width="34" height="34" alt=""/></a></li>
<li><a href="#"><img src="front/img/fb-icon.png" width="34" height="34" alt=""/></a></li>
</ul>
</div>
<div class="clr"></div>
</div>
<!--Footer-->

</div>


</div>
<script src="front/resources/js/slick.min.js"></script>
<script>
$('.caro').slick({
  infinite: true,
  slidesToShow: 2,
  slidesToScroll: 2,
  autoplay: true,
  variableWidth: true,
  arrows: false
});
</script>
</body>
</html>