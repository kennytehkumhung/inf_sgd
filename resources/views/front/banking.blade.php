@extends('front/master')

@if (Session::get('currency') == 'TWD')
    @section('title', '無極限娛樂網')
@elseif (Session::get('currency') == 'MYR')
    @section('title', 'Banking for Online Casino Malaysia - Deposit/Withdrawal')
    @section('keywords', 'Banking for Online Casino Malaysia - Deposit/Withdrawal')
    @section('description', 'How to Deposit and Withdraw at InfiniWin Online Casino Malaysia. Get your WELCOME BONUS  NOW!')
@endif

@section('top_js')
<link href="{{url()}}/front/resources/css/otherPg.css" rel="stylesheet">
@stop

@section('content')
<!--MID SECTION-->
<?php echo htmlspecialchars_decode($content); ?>
<!--MID SECTION-->
@stop

@section('bottom_js')

@stop