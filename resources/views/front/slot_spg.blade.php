<html>
<head>
 <link href="{{url()}}/front/resources/css/slot.css" rel="stylesheet">
 <link href="{{url()}}/front/resources/css/style_2.css" rel='stylesheet' type='text/css'>

<!-- Home slider style -->
<link rel="stylesheet" href="{{url()}}/front/resources/css/style_3.css">
</head>
<body>
	<div class="slot_menu">
		<ul>
			<li><a href="{{route('spg', [ 'type' => 'slot' , 'category' => 'slot' ] )}}">Slots</a></li>
                        <li><a href="{{route('spg', [ 'type' => 'slot' , 'category' => 'progressive' ] )}}">Progressive</a></li>
		</ul>
	</div>
	<div id="slot_lobby">
		@foreach( $lists as $list )
		<div class="slot_box">
			<a href="javascript:void(0)" onclick="@if (Auth::user()->check())window.open('{{route('spg-slot' , [ 'gameid' => $list['gamecode'] ] )}}', 'spg_slot');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif" >
				<img src="{{url()}}/front/img/spg/{{$list->gamecode}}.jpg" width="150" height="150" alt=""/>
			</a>
			<span>{{$list['gamename_en']}}</span>
		</div>			 
		@endforeach
	  <div class="clr"></div>
	</div>
    <div class="clr"></div>
<!--Slot-->
</body>
</html>