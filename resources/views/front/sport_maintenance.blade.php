@extends('front/master')

@section('keywords', 'football betting,sports betting')
@section('description', 'Gambling and bet on football at trusted Malaysia online sports betting site, Infiniwin. Win big today!')

@section('content')
@include('front/sport_top')
<div class="midSect">
	<div class="midSectInner">
		<div class="spacingTop"></div>
		<div class="maintenance">      
			<span class="maintainMSG">We are upgrading our server and  <br>user interface for a <br>better gaming experience</span>
			<span class="maintainMSGsub">For more details, please contact us at inquire@infiniwin.net</span>
		</div>
	</div>
</div>
@endsection