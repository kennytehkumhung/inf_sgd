@extends('front/master')

@if (Session::get('currency') == 'TWD')
    @section('title', '無極限娛樂網')
@elseif (Session::get('currency') == 'MYR')
    @section('title', 'Play Live Fortune 4D Malaysia Online Betting')
    @section('keywords', 'Play Live Fortune 4D Malaysia Online Betting')
    @section('description', 'Play Live 4D at InfiniWin Online Casino Malaysia - awarded Best Online Casino and enjoy 100% Welcome Deposit Bonus. Join InfiniWin casino online today.')
@endif

@section('top_js')
<!-- modal css-->
<link href="{{url()}}/front/resources/css/portBox.css" rel="stylesheet">
<link href="{{url()}}/front/resources/css/modal.css" rel="stylesheet">

<!-- modal js-->
<script src="{{url()}}/front/resources/js/jquery-ui-1.10.3.custom.min.js"></script>
<script src="{{url()}}/front/resources/js/portBox.slimscroll.min.js"></script>
@stop

@section('content')
<!--4d-->
<div class="D_container">
<table width="auto" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>
    <div class="dinmenu">
<img src="{{url()}}/front/img/4d-icon.png" width="220" height="180" alt=""/>
 <a onClick="@if (Auth::user()->check())window.open('{{route('psb')}}', 'pubContent');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif" href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('betbtn','','{{url()}}/front/img/bet-btn-hover.png',0)"><img src="{{url()}}/front/img/bet-btn.png" alt="" width="222" height="45" id="betbtn"></a>
 <a href="#" data-display="empty" class="button"><img src="{{url()}}/front/img/pay-btn.png" alt="" width="222" height="45" id="paybtn"></a> </div>
    </td>
    <td><!--magnum-->
        <div id="fD_table">
    	<div id="fD_table_header" class="magnum">
        	<div id="fD_table_img">
            <img src="{{url()}}/front/img/4D_magnum.png" width="80" height="40" />
            </div>
            <div id="fD_table_title">
              <span class="title">MAGNUM 4D</span><br/>
                <span>{{$Magnum['date']}}</span>
            </div>
        </div>
        	<table align="center" width="210" border="1" cellpadding="1" cellspacing="0" style="border-color:#999;">
              <tr>
                <td width="61" align="center" class="magnum fD_top3_description">1st</td>
                <td width="61" align="center" class="magnum fD_top3_description">2nd</td>
                <td align="center" class="magnum fD_top3_description">3rd</td>
              </tr>
              <tr>
                <td align="center" class="fD_top3_no"><span>{{$Magnum['first']}}</span></td>
                <td align="center" class="fD_top3_no"><span>{{$Magnum['second']}}</span></td>
                <td align="center" class="fD_top3_no"><span>{{$Magnum['third']}}</span></td>
              </tr>
            </table>
            <table align="center" width="210" border="1" cellpadding="1" cellspacing="0" class="fD_normalPrice">
              <tr>
                <td colspan="3" align="center" class="magnum">Special</td>
              </tr>
              <tr>
                <td align="center"><span>{{$Magnum['special'][0]}}</span></td>
                <td align="center"><span>{{$Magnum['special'][1]}}</span></td>
                <td align="center"><span>{{$Magnum['special'][2]}}</span></td>
              </tr>
              <tr>
                <td align="center"><span>{{$Magnum['special'][3]}}</span></td>
                <td align="center"><span>{{$Magnum['special'][4]}}</span></td>
                <td align="center"><span>{{$Magnum['special'][5]}}</span></td>
              </tr>
              <tr>
                
                <td align="center"><span>{{$Magnum['special'][6]}}</span></td>
                <td align="center"><span>{{$Magnum['special'][7]}}</span></td>
                <td align="center"><span>{{$Magnum['special'][8]}}</span></td>
                
              </tr>
              <tr>
                <td align="center" bgcolor="#999">&nbsp;</td>
                <td align="center"><span>{{$Magnum['special'][9]}}</span></td>
                <td align="center" bgcolor="#999">&nbsp;</td>
              </tr>
          </table>
            <table align="center" width="210" border="1" cellpadding="1" cellspacing="0" class="fD_normalPrice">
              <tr>
                <td colspan="3" align="center" class="magnum">Consolation</td>
              </tr>
			<tr>
                <td align="center"><span>{{$Magnum['consolation'][0]}}</span></td>
                <td align="center"><span>{{$Magnum['consolation'][1]}}</span></td>
                <td align="center"><span>{{$Magnum['consolation'][2]}}</span></td>
              </tr>
              <tr>
                <td align="center"><span>{{$Magnum['consolation'][3]}}</span></td>
                <td align="center"><span>{{$Magnum['consolation'][4]}}</span></td>
                <td align="center"><span>{{$Magnum['consolation'][5]}}</span></td>
              </tr>
              <tr>
                
                <td align="center"><span>{{$Magnum['consolation'][6]}}</span></td>
                <td align="center"><span>{{$Magnum['consolation'][7]}}</span></td>
                <td align="center"><span>{{$Magnum['consolation'][8]}}</span></td>
                
              </tr>
              <tr>
                <td align="center" bgcolor="#999">&nbsp;</td>
                <td align="center"><span>{{$Magnum['consolation'][9]}}</span></td>
                <td align="center" bgcolor="#999">&nbsp;</td>
              </tr>
          </table>
        </div>
      <!--end magnum--></td>
    <td><!--damacai-->
      <div id="fD_table">
   	  <div id="fD_table_header" class="damacai">
        	<div id="fD_table_img">
            <img src="{{url()}}/front/img/4D_damacai.png" width="80" height="40" />
            </div>
            <div id="fD_table_title">
                <span class="title">DAMACAI 1+3D</span><br/>
                <span>{{$PMP['date']}}</span>
            </div>
        </div>
        <div id="fD_table_result">
        	<table width="210" border="1" cellpadding="1" cellspacing="0" style="border-color:#999;">
              <tr>
                <td width="61" align="center" class="damacai fD_top3_description">1st</td>
                <td width="61" align="center" class="damacai fD_top3_description">2nd</td>
                <td align="center" class="damacai fD_top3_description">3rd</td>
              </tr>
              <tr>
                <td align="center" class="fD_top3_no"><span id="ContentPlaceHolder1_lbl_PMP_1st">{{$PMP['first']}}</span></td>
                <td align="center" class="fD_top3_no"><span id="ContentPlaceHolder1_lbl_PMP_2nd">{{$PMP['second']}}</span></td>
                <td align="center" class="fD_top3_no"><span id="ContentPlaceHolder1_lbl_PMP_3rd">{{$PMP['third']}}</span></td>
              </tr>
            </table>
            <table width="210" border="1" cellpadding="1" cellspacing="0" class="fD_normalPrice">
              <tr>
                <td colspan="3" align="center" class="damacai">Special</td>
              </tr>
              <tr>
                <td align="center"><span>{{$PMP['special'][0]}}</span></td>
                <td align="center"><span>{{$PMP['special'][1]}}</span></td>
                <td align="center"><span>{{$PMP['special'][2]}}</span></td>
              </tr>
              <tr>
                <td align="center"><span>{{$PMP['special'][3]}}</span></td>
                <td align="center"><span>{{$PMP['special'][4]}}</span></td>
                <td align="center"><span>{{$PMP['special'][5]}}</span></td>
              </tr>
              <tr>
                
                <td align="center"><span>{{$PMP['special'][6]}}</span></td>
                <td align="center"><span>{{$PMP['special'][7]}}</span></td>
                <td align="center"><span>{{$PMP['special'][8]}}</span></td>
                
              </tr>
              <tr>
                <td align="center" bgcolor="#999">&nbsp;</td>
                <td align="center"><span>{{$PMP['special'][9]}}</span></td>
                <td align="center" bgcolor="#999">&nbsp;</td>
              </tr>
          </table>
            <table width="210" border="1" cellpadding="1" cellspacing="0" class="fD_normalPrice">
              <tr>
                <td colspan="3" align="center" class="damacai">Consolation</td>
              </tr>
			  <tr>
                <td align="center"><span>{{$PMP['consolation'][0]}}</span></td>
                <td align="center"><span>{{$PMP['consolation'][1]}}</span></td>
                <td align="center"><span>{{$PMP['consolation'][2]}}</span></td>
              </tr>
              <tr>
                <td align="center"><span>{{$PMP['consolation'][3]}}</span></td>
                <td align="center"><span>{{$PMP['consolation'][4]}}</span></td>
                <td align="center"><span>{{$PMP['consolation'][5]}}</span></td>
              </tr>
              <tr>
                
                <td align="center"><span>{{$PMP['consolation'][6]}}</span></td>
                <td align="center"><span>{{$PMP['consolation'][7]}}</span></td>
                <td align="center"><span>{{$PMP['consolation'][8]}}</span></td>
                
              </tr>
              <tr>
                <td align="center" bgcolor="#999">&nbsp;</td>
                <td align="center"><span>{{$PMP['consolation'][9]}}</span></td>
                <td align="center" bgcolor="#999">&nbsp;</td>
              </tr>
          </table>
      </div>
      </div>
      <!--end damacai--></td>
    <td><!--toto-->
      <div id="fD_table">
   	  <div id="fD_table_header" class="toto">
        	<div id="fD_table_img">
            <img src="{{url()}}/front/img/4D_toto.png" width="80" height="40" />
            </div>
            <div id="fD_table_title">
                <span class="title">TOTO 4D</span><br/>
                <span>{{$Toto['date']}}</span>
            </div>
        </div>
        <div id="fD_table_result">
        	<table width="210" border="1" cellpadding="1" cellspacing="0" style="border-color:#999;">
              <tr>
                <td width="61" align="center" class="toto fD_top3_description">1st</td>
                <td width="61" align="center" class="toto fD_top3_description">2nd</td>
                <td align="center" class="toto fD_top3_description">3rd</td>
              </tr>
              <tr>
                <td align="center" class="fD_top3_no"><span>{{$Toto['first']}}</span></td>
                <td align="center" class="fD_top3_no"><span>{{$Toto['second']}}</span></td>
                <td align="center" class="fD_top3_no"><span>{{$Toto['third']}}</span></td>
              </tr>
            </table>
            <table width="210" border="1" cellpadding="1" cellspacing="0" class="fD_normalPrice">
              <tr>
                <td colspan="3" align="center" class="toto">Special</td>
              </tr>
              <tr>
                <td align="center"><span>{{$Toto['special'][0]}}</span></td>
                <td align="center"><span>{{$Toto['special'][1]}}</span></td>
                <td align="center"><span>{{$Toto['special'][2]}}</span></td>
                
              </tr>
              <tr>
                <td align="center"><span>{{$Toto['special'][3]}}</span></td>
                <td align="center"><span>{{$Toto['special'][4]}}</span></td>
                <td align="center"><span>{{$Toto['special'][5]}}</span></td>
              </tr>
              <tr>
                <td align="center"><span>{{$Toto['special'][6]}}</span></td>
                <td align="center"><span>{{$Toto['special'][7]}}</span></td>
                <td align="center"><span>{{$Toto['special'][8]}}</span></td>
              </tr>
              <tr>
                <td align="center" bgcolor="#999">&nbsp;</td>
                <td align="center"><span>{{$Toto['special'][9]}}</span></td>
                <td align="center" bgcolor="#999">&nbsp;</td>
              </tr>
          </table>
            <table width="210" border="1" cellpadding="1" cellspacing="0" class="fD_normalPrice">
              <tr>
                <td colspan="3" align="center" class="toto">Consolation</td>
              </tr>
			<tr>
                <td align="center"><span>{{$Toto['consolation'][0]}}</span></td>
                <td align="center"><span>{{$Toto['consolation'][1]}}</span></td>
                <td align="center"><span>{{$Toto['consolation'][2]}}</span></td>
              </tr>
              <tr>
                <td align="center"><span>{{$Toto['consolation'][3]}}</span></td>
                <td align="center"><span>{{$Toto['consolation'][4]}}</span></td>
                <td align="center"><span>{{$Toto['consolation'][5]}}</span></td>
              </tr>
              <tr>
                
                <td align="center"><span>{{$Toto['consolation'][6]}}</span></td>
                <td align="center"><span>{{$Toto['consolation'][7]}}</span></td>
                <td align="center"><span>{{$Toto['consolation'][8]}}</span></td>
                
              </tr>
              <tr>
                <td align="center" bgcolor="#999">&nbsp;</td>
                <td align="center"><span>{{$Toto['consolation'][9]}}</span></td>
                <td align="center" bgcolor="#999">&nbsp;</td>
              </tr>
          </table>
      </div>
    </div>
      <!--end toto--></td>

  </tr>
  <tr>
    <td><!--Singapore-->
       <div style="margin-left: 28px;" id="fD_table">
   	  <div id="fD_table_header" class="singapore">
        	<div id="fD_table_img">
            <img src="{{url()}}/front/img/4D_singapore.png" width="80" height="40" />
            </div>
            <div id="fD_table_title">
                <span class="title">SINGAPORE 4D</span><br/>
                <span>{{$Singapore['date']}}</span>
            </div>
        </div>
         
        <div id="fD_table_result">
       	  <table width="210" border="1" cellpadding="1" cellspacing="0" style="border-color:#999;">
              <tr>
                <td width="61" align="center" class="singapore fD_top3_description">1st</td>
                <td width="61" align="center" class="singapore fD_top3_description">2nd</td>
                <td align="center" class="singapore fD_top3_description">3rd</td>
              </tr>
              <tr>
                <td align="center" class="fD_top3_no"><span>{{$Singapore['first']}}</span></td>
                <td align="center" class="fD_top3_no"><span>{{$Singapore['second']}}</span></td>
                <td align="center" class="fD_top3_no"><span>{{$Singapore['third']}}</span></td>
              </tr>
            </table>
            <table width="210" border="1" cellpadding="1" cellspacing="0" class="fD_normalPrice">
              <tr>
                <td colspan="3" align="center" class="singapore">Special</td>
              </tr>
			  <tr>
                <td align="center"><span>{{$Singapore['special'][0]}}</span></td>
                <td align="center"><span>{{$Singapore['special'][1]}}</span></td>
                <td align="center"><span>{{$Singapore['special'][2]}}</span></td>
                
              </tr>
              <tr>
                <td align="center"><span>{{$Singapore['special'][3]}}</span></td>
                <td align="center"><span>{{$Singapore['special'][4]}}</span></td>
                <td align="center"><span>{{$Singapore['special'][5]}}</span></td>
              </tr>
              <tr>
                <td align="center"><span>{{$Singapore['special'][6]}}</span></td>
                <td align="center"><span>{{$Singapore['special'][7]}}</span></td>
                <td align="center"><span>{{$Singapore['special'][8]}}</span></td>
              </tr>
              <tr>
                <td align="center" bgcolor="#999">&nbsp;</td>
                <td align="center"><span>{{$Singapore['special'][9]}}</span></td>
                <td align="center" bgcolor="#999">&nbsp;</td>
              </tr>
            </table>
            <table width="210" border="1" cellpadding="1" cellspacing="0" class="fD_normalPrice">
              <tr>
                <td colspan="3" align="center" class="singapore">Consolation</td>
              </tr>
         	<tr>
                <td align="center"><span>{{$Singapore['consolation'][0]}}</span></td>
                <td align="center"><span>{{$Singapore['consolation'][1]}}</span></td>
                <td align="center"><span>{{$Singapore['consolation'][2]}}</span></td>
              </tr>
              <tr>
                <td align="center"><span>{{$Singapore['consolation'][3]}}</span></td>
                <td align="center"><span>{{$Singapore['consolation'][4]}}</span></td>
                <td align="center"><span>{{$Singapore['consolation'][5]}}</span></td>
              </tr>
              <tr>
                
                <td align="center"><span>{{$Singapore['consolation'][6]}}</span></td>
                <td align="center"><span>{{$Singapore['consolation'][7]}}</span></td>
                <td align="center"><span>{{$Singapore['consolation'][8]}}</span></td>
                
              </tr>
              <tr>
                <td align="center" bgcolor="#999">&nbsp;</td>
                <td align="center"><span>{{$Singapore['consolation'][9]}}</span></td>
                <td align="center" bgcolor="#999">&nbsp;</td>
              </tr>
          </table>
      </div>
    </div>
     <!--end singapore--></td>
    <td><!--t88-->
     <div id="fD_table">
   	  <div id="fD_table_header" class="t88">
        	<div id="fD_table_img">
            <img src="{{url()}}/front/img/4D_88.png" width="80" height="40" />
            </div>
            <div id="fD_table_title">
                <span class="title">SABAH 4D</span><br/>
                <span>{{$Sabah['date']}}</span>
            </div>
        </div>

        <div id="fD_table_result">
        	<table width="210" border="1" cellpadding="1" cellspacing="0" style="border-color:#999;">
              <tr>
                <td width="61" align="center" class="t88 fD_top3_description">1st</td>
                <td width="61" align="center" class="t88 fD_top3_description">2nd</td>
                <td align="center" class="t88 fD_top3_description">3rd</td>
              </tr>
              <tr>
                <td align="center" class="fD_top3_no"><span>{{$Sabah['first']}}</span></td>
                <td align="center" class="fD_top3_no"><span>{{$Sabah['second']}}</span></td>
                <td align="center" class="fD_top3_no"><span>{{$Sabah['third']}}</span></td>
              </tr>
            </table>
          <table width="210" border="1" cellpadding="1" cellspacing="0" class="fD_normalPrice">
              <tr>
                <td colspan="3" align="center" class="t88">Special</td>
            </tr>
			<tr>
                <td align="center"><span>{{$Sabah['special'][0]}}</span></td>
                <td align="center"><span>{{$Sabah['special'][1]}}</span></td>
                <td align="center"><span>{{$Sabah['special'][2]}}</span></td>
                
              </tr>
              <tr>
                <td align="center"><span>{{$Sabah['special'][3]}}</span></td>
                <td align="center"><span>{{$Sabah['special'][4]}}</span></td>
                <td align="center"><span>{{$Sabah['special'][5]}}</span></td>
              </tr>
              <tr>
                <td align="center"><span>{{$Sabah['special'][6]}}</span></td>
                <td align="center"><span>{{$Sabah['special'][7]}}</span></td>
                <td align="center"><span>{{$Sabah['special'][8]}}</span></td>
              </tr>
              <tr>
                <td align="center" bgcolor="#999">&nbsp;</td>
                <td align="center"><span>{{$Sabah['special'][9]}}</span></td>
                <td align="center" bgcolor="#999">&nbsp;</td>
              </tr>
            </table>
            <table width="210" border="1" cellpadding="1" cellspacing="0" class="fD_normalPrice">
              <tr>
                <td colspan="3" align="center" class="t88">Consolation</td>
              </tr>
			<tr>
                <td align="center"><span>{{$Sabah['consolation'][0]}}</span></td>
                <td align="center"><span>{{$Sabah['consolation'][1]}}</span></td>
                <td align="center"><span>{{$Sabah['consolation'][2]}}</span></td>
              </tr>
              <tr>
                <td align="center"><span>{{$Sabah['consolation'][3]}}</span></td>
                <td align="center"><span>{{$Sabah['consolation'][4]}}</span></td>
                <td align="center"><span>{{$Sabah['consolation'][5]}}</span></td>
              </tr>
              <tr>
                
                <td align="center"><span>{{$Sabah['consolation'][6]}}</span></td>
                <td align="center"><span>{{$Sabah['consolation'][7]}}</span></td>
                <td align="center"><span>{{$Sabah['consolation'][8]}}</span></td>
                
              </tr>
              <tr>
                <td align="center" bgcolor="#999">&nbsp;</td>
                <td align="center"><span>{{$Sabah['consolation'][9]}}</span></td>
                <td align="center" bgcolor="#999">&nbsp;</td>
              </tr>
          </table>
      </div>
    </div>
    <!--End of t88--></td>
    <td><!--STC-->
    <div id="fD_table">
   	  <div id="fD_table_header" class="stc">
        	<div id="fD_table_img">
            <img src="{{url()}}/front/img/4D_stc.png" width="80" height="40" />
            </div>
            <div id="fD_table_title">
                <span class="title">SANDAKAN 4D</span><br/>
                <span>{{$Sandakan['date']}}</span>
            </div>
        </div>

        <div id="fD_table_result">
       	  <table width="210" border="1" cellpadding="1" cellspacing="0" style="border-color:#999;">
              <tr>
                <td width="61" align="center" class="stc fD_top3_description">1st</td>
                <td width="61" align="center" class="stc fD_top3_description">2nd</td>
                <td align="center" class="stc fD_top3_description">3rd</td>
              </tr>
              <tr>
                <td align="center" class="fD_top3_no"><span>{{$Sandakan['first']}}</span></td>
                <td align="center" class="fD_top3_no"><span>{{$Sandakan['second']}}</span></td>
                <td align="center" class="fD_top3_no"><span>{{$Sandakan['third']}}</span></td>
              </tr>
            </table>
            <table width="210" border="1" cellpadding="1" cellspacing="0" class="fD_normalPrice">
              <tr>
                <td colspan="3" align="center" class="stc">Special</td>
              </tr>
			<tr>
                <td align="center"><span>{{$Sandakan['special'][0]}}</span></td>
                <td align="center"><span>{{$Sandakan['special'][1]}}</span></td>
                <td align="center"><span>{{$Sandakan['special'][2]}}</span></td>
                
              </tr>
              <tr>
                <td align="center"><span>{{$Sandakan['special'][3]}}</span></td>
                <td align="center"><span>{{$Sandakan['special'][4]}}</span></td>
                <td align="center"><span>{{$Sandakan['special'][5]}}</span></td>
              </tr>
              <tr>
                <td align="center"><span>{{$Sandakan['special'][6]}}</span></td>
                <td align="center"><span>{{$Sandakan['special'][7]}}</span></td>
                <td align="center"><span>{{$Sandakan['special'][8]}}</span></td>
              </tr>
              <tr>
                <td align="center" bgcolor="#999">&nbsp;</td>
                <td align="center"><span>{{$Sandakan['special'][9]}}</span></td>
                <td align="center" bgcolor="#999">&nbsp;</td>
              </tr>
            </table>
            <table width="210" border="1" cellpadding="1" cellspacing="0" class="fD_normalPrice">
              <tr>
                <td colspan="3" align="center" class="stc">Consolation</td>
              </tr>
			<tr>
                <td align="center"><span>{{$Sandakan['consolation'][0]}}</span></td>
                <td align="center"><span>{{$Sandakan['consolation'][1]}}</span></td>
                <td align="center"><span>{{$Sandakan['consolation'][2]}}</span></td>
                
              </tr>
              <tr>
                <td align="center"><span>{{$Sandakan['consolation'][3]}}</span></td>
                <td align="center"><span>{{$Sandakan['consolation'][4]}}</span></td>
                <td align="center"><span>{{$Sandakan['consolation'][5]}}</span></td>
              </tr>
              <tr>
                <td align="center"><span>{{$Sandakan['consolation'][6]}}</span></td>
                <td align="center"><span>{{$Sandakan['consolation'][7]}}</span></td>
                <td align="center"><span>{{$Sandakan['consolation'][8]}}</span></td>
              </tr>
              <tr>
                <td align="center" bgcolor="#999">&nbsp;</td>
                <td align="center"><span>{{$Sandakan['consolation'][9]}}</span></td>
                <td align="center" bgcolor="#999">&nbsp;</td>
              </tr>
          </table>
      </div>
    </div>
    <!--End of STC--></td>
    <td><!--Cash-->
    <div id="fD_table">
   	  <div id="fD_table_header" class="cash">
        	<div id="fD_table_img">
            <img src="{{url()}}/front/img/4D_cash.png" width="80" height="40" />
            </div>
            <div id="fD_table_title">
                <span class="title">SPECIAL BIGSWEEP</span><br/>
                <span>{{$Sarawak['date']}}</span>
            </div>
        </div>

        <div id="fD_table_result">
        	<table width="210" border="1" cellpadding="1" cellspacing="0" style="border-color:#999;">
              <tr>
                <td width="61" align="center" class="cash fD_top3_description">1st</td>
                <td width="61" align="center" class="cash fD_top3_description">2nd</td>
                <td align="center" class="cash fD_top3_description">3rd</td>
              </tr>
              <tr>
                <td align="center" class="fD_top3_no"><span>{{$Sarawak['first']}}</span></td>
                <td align="center" class="fD_top3_no"><span>{{$Sarawak['second']}}</span></td>
                <td align="center" class="fD_top3_no"><span>{{$Sarawak['third']}}</span></td>
              </tr>
            </table>
            <table width="210" border="1" cellpadding="1" cellspacing="0" class="fD_normalPrice">
              <tr>
                <td colspan="3" align="center" class="cash">Special</td>
              </tr>
			<tr>
                <td align="center"><span>{{$Sarawak['special'][0]}}</span></td>
                <td align="center"><span>{{$Sarawak['special'][1]}}</span></td>
                <td align="center"><span>{{$Sarawak['special'][2]}}</span></td>
                
              </tr>
              <tr>
                <td align="center"><span>{{$Sarawak['special'][3]}}</span></td>
                <td align="center"><span>{{$Sarawak['special'][4]}}</span></td>
                <td align="center"><span>{{$Sarawak['special'][5]}}</span></td>
              </tr>
              <tr>
                <td align="center"><span>{{$Sarawak['special'][6]}}</span></td>
                <td align="center"><span>{{$Sarawak['special'][7]}}</span></td>
                <td align="center"><span>{{$Sarawak['special'][8]}}</span></td>
              </tr>
              <tr>
                <td align="center" bgcolor="#999">&nbsp;</td>
                <td align="center"><span>{{$Sarawak['special'][9]}}</span></td>
                <td align="center" bgcolor="#999">&nbsp;</td>
              </tr>
          </table>
            <table width="210" border="1" cellpadding="1" cellspacing="0" class="fD_normalPrice">
              <tr>
                <td colspan="3" align="center" class="cash">Consolation</td>
              </tr>
			<tr>
                <td align="center"><span>{{$Sarawak['consolation'][0]}}</span></td>
                <td align="center"><span>{{$Sarawak['consolation'][1]}}</span></td>
                <td align="center"><span>{{$Sarawak['consolation'][2]}}</span></td>
              </tr>
              <tr>
                <td align="center"><span>{{$Sarawak['consolation'][3]}}</span></td>
                <td align="center"><span>{{$Sarawak['consolation'][4]}}</span></td>
                <td align="center"><span>{{$Sarawak['consolation'][5]}}</span></td>
              </tr>
              <tr>
                
                <td align="center"><span>{{$Sarawak['consolation'][6]}}</span></td>
                <td align="center"><span>{{$Sarawak['consolation'][7]}}</span></td>
                <td align="center"><span>{{$Sarawak['consolation'][8]}}</span></td>
                
              </tr>
              <tr>
                <td align="center" bgcolor="#999">&nbsp;</td>
                <td align="center"><span>{{$Sarawak['consolation'][9]}}</span></td>
                <td align="center" bgcolor="#999">&nbsp;</td>
              </tr>
          </table>
      </div>
    </div>
    <!--End of Cash--></td>

  </tr>
</table>
    <div class="seo_content">
        <h1 style="font-size:13px;">Shoot to Riches with Infiniwin’s Live 4D Results</h1>
        <p>4D is legal in Malaysia and you can get lots of money if your number is picked. To cater for the need of the customers, Infiniwin now has live 4D results for players to check the draw results. Since life is all about taking chances, Infiniwin added a game based on the 4D result.</p>
        <p>The game is very simple where you will need to put your bet first. After your bet is placed, you will then need to click on the bet button. By clicking the button, Infiniwin will pick a random 4D number. If this 4D number is inside the list of winners in Toto or Magnum 4D of the current period, you will won some prizes easily.</p>
        <p>The list of winners are not limited to Toto and Magnum 4D only. There are other live 4D results shown inside Infiniwin like SIngapore 4D, Sabah 4D, Sandakan 4D, Special Bigsweep, and Damacai 1+3D. If the 4D number you got is from any of these 4D providers, you are surely a lucky guy.</p>
        <p>There are two different ways to win Infiniwin Live 4D results. The Big Forecast provides the grand prize of RM3,400 for every RM1 you bet while the Small Forecast are higher with RM4,8000 for every RM1 bet. Just imagine if you bet RM100 and you get the grand prize. You can easily become a rich man and retire early to just enjoy life!</p>
        <p>If you do no want to play Infiniwin Live 4D results game, you can also play other types of lottery-based games for 3D, 5D and 6D. The higher number of digit inside a combination, the higher the rewards that you can get. However, the difficulties are also increased dramatically with every digit being added.</p>
        <p>Infiniwin is the best place for you to play this Live 4D results game. You do not need to spend a lot of money but the gains are very huge. Come and sign up at Infiniwin to get the exclusive access to the 4D section and you will be amazed with the grandeur of the website.</p>
    </div>
</div>
  
  
  
<!-- payout modal-->
<div id="empty" class="portBox">
    <h2 style="color:#FFBF00;">{{Lang::get('public.Payout')}} {{Lang::get('public.Table')}}</h2><br>
	<b>Prize money for Big Forecast</b><br><br>
	For this Forecast, as shown in the table below, with every RM1 bet, the 1st Prize pays RM3,400, 2nd Prize RM1,200, 3rd Prize RM600, Special Prize RM250 and Consolation Prize RM80.
	<br><br>
	<div class="table" >	
		<table>
			<tr>
				<td>BIG FORECAST</td>
				<td>PRIZE AMOUNT</td>
			</tr>
			<tr>
				<td>1st Prize</td>
				<td>RM 2,500.00 + RM 900 (36% Extra)</td>
			</tr>
			<tr>
				<td>2nd Prize</td>
				<td>RM 1,000.00 + RM 200 (20% Extra)</td>
			</tr>
			<tr>
				<td>3rd Prize</td>
				<td>RM 500.00 + RM 100 (20% Extra)</td>
			</tr>
			<tr>
				<td>Special Prize</td>
				<td>RM 200.00 + RM 50 (25% Extra)</td>
			</tr>
			<tr>
				<td>Consolation Prize</td>
				<td>RM 60.00 + RM 20 (30% Extra)</td>
			</tr>
		</table>
	</div>
	
	<b>Prize money for Small Forecast</b><br><br>
	For this Forecast, as shown in the table below, with every RM1 bet, the 1st Prize pays RM4,800, 2nd Prize RM2,400, 3rd Prize RM1,200.
	<br><br>
	
	<div class="table" >	
		<table>
			<tr>
				<td>SMALL FORECAST</td>
				<td>PRIZE AMOUNT</td>
			</tr>
			<tr>
				<td>1st Prize</td>
				<td>RM 3,500.00 + RM 1,300 (37% Extra)</td>
			</tr>
			<tr>
				<td>2nd Prize</td>
				<td>RM 2,000.00 + RM 400 (20% Extra)</td>
			</tr>
			<tr>
				<td>3rd Prize</td>
				<td>RM 1,000.00 + RM 200 (20% Extra)</td>
			</tr>
			<tr>
				<td>4A Prize</td>
				<td>RM 6,000 + RM 2,000 (30% Extra)</td>
			</tr>
		</table>
	</div>
	
	<b>Prize money for 3D</b><br><br>
	For this Forecast, as shown in the table below, with every RM1 bet, the 1st Prize pays RM 240, 2nd Prize RM240, and 3rd Prize RM240.<br><br>
	
	<div class="table" >	
		<table>
			<tr>
				<td>3D</td>
				<td>PRIZE AMOUNT</td>
			</tr>
			<tr>
				<td>1st Prize</td>
				<td>RM 215.00 + RM 25 (12% Extra)</td>
			</tr>
			<tr>
				<td>2nd Prize</td>
				<td>RM 215.00 + RM 25 (12% Extra)</td>
			</tr>
			<tr>
				<td>3rd Prize</td>
				<td>RM 215.00 + RM 25 (12% Extra)</td>
			</tr>
			<tr>
				<td>3A Prize</td>
				<td>RM 645 + RM 195 (30% Extra)</td>
			</tr>
		</table>
	</div>
	
	<b>Prize money for 5D/6D</b><br><br>
	For this Forecast, as shown in the table below, with every RM1 bet<br><br>
	
	<div class="table" >	
		<table>
			<tr>
				<td>5D</td>
				<td>PRIZE AMOUNT</td>
				<td>6D</td>
				<td>PRIZE AMOUNT</td>
			</tr>
			<tr>
				<td>1st Prize</td>
				<td>RM 15,000.00</td>
				<td>1st Prize</td>
				<td>RM 100,000.00</td>
			</tr>
			<tr>
				<td>2nd Prize</td>
				<td>RM 5,000.00</td>
				<td>2nd Prize</td>
				<td>RM 3,000.00</td>
			</tr>
			<tr>
				<td>3rd Prize</td>
				<td>RM 3,000.00</td>
				<td>3rd Prize</td>
				<td>RM 300.00</td>
			</tr>
			<tr>
				<td>4th Prize</td>
				<td>RM 500.00</td>
				<td>4th Prize</td>
				<td>RM 30.00</td>
			</tr>
			<tr>
				<td>5th Prize</td>
				<td>RM 20.00</td>
				<td>5th Prize</td>
				<td>RM 4.00</td>
			</tr>
			<tr>
				<td>6th Prize</td>
				<td>RM 5.00</td>
				<td></td>
				<td></td>
			</tr>
		</table>
	</div>
</div>


<!--4d-->
@stop

@section('bottom_js')
<script src="{{url()}}/front/resources/js/slick.min.js"></script>
<script>
$('.caro').slick({
  infinite: true,
  slidesToShow: 2,
  slidesToScroll: 2,
  autoplay: true,
  variableWidth: true,
  arrows: false
});
</script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36251023-1']);
  _gaq.push(['_setDomainName', 'jqueryscript.net']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
@stop