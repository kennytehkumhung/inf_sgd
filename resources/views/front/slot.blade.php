@extends('front/master')

@if (Session::get('currency') == 'TWD')
    @section('title', '無極限娛樂網')
@elseif (Session::get('currency') == 'MYR')
    <?php
    $title = '';
    $desc = '';
    ?>
    @if ($type == 'mxb')
        <?php $title = ' - Maxbet Slot Games'; $desc = 'Maxbet '; ?>
    @elseif ($type == 'plt' || $type == 'pltb')
        <?php $title = ' - PlayTech Slot Games'; $desc = 'PlayTech '; ?>
    @elseif ($type == 'sky')
        <?php $title = ' - Sky3888 and SCR888 Slot Games'; $desc = 'Sky3888 and SCR888 '; ?>
    @elseif ($type == 'spg')
        <?php $title = ' - SpadeGaming Slot Games'; $desc = 'SpadeGaming '; ?>
    @endif

    @section('title', 'Online Slot Malaysia'.$title)
    @section('keywords', 'Online Slot Malaysia'.$title)
    @section('description', 'Play '.$desc.'Online Slots at InfiniWin Online Casino Malaysia and claim a FREE SPIN bonus today!')
@endif


@section('top_js')
 <link href="{{url()}}/front/resources/css/slot.css" rel="stylesheet">
 <link href="{{url()}}/front/resources/css/jquery.bxslider.css" type="text/css" media="all" rel="stylesheet" />
 <script type="text/javascript" language="javascript" src="{{url()}}/front/resources/js/jquery.bxslider.min.js"></script>
   <!-- Base MasterSlider style sheet -->
<link rel="stylesheet" href="{{url()}}/front/resources/css/masterslider.css" />

<!-- Master Slider -->
<script src="{{url()}}/front/resources/js/masterslider.min.js"></script>

<!-- Master Slider Skin -->
<link href="{{url()}}/front/resources/css/style_2.css" rel='stylesheet' type='text/css'>

<!-- Home slider style -->
<link rel="stylesheet" href="{{url()}}/front/resources/css/style_3.css">
 <script>
 $(document).ready(function() {

     @if (Session::get('currency') == 'TWD')
        $('body').css('background-image', 'url({{ asset('/front/img/tw/slot_bg.jpg') }})');
     @else
        $('body').css('background-image', 'url({{ asset('/front/img/bg-blackSlot.jpg') }})');
     @endif

	@if( $type == 'mxb' )
		$('.slot_mxb_image').hide();
	@elseif( $type == 'plt' )
		$('.slot_plt_image').hide();	
	@elseif( $type == 'a1a' )
		$('.slot_a1a_image').hide();	
	@elseif( $type == 'w88' )
		$('.slot_w88_image').hide();
	@elseif( $type == 'jok' )
		$('.slot_jok_image').hide();
	@elseif( $type == 'sky' )
		$('.slot_sky_image').hide();
    @elseif( $type == 'spg' )
        $('.slot_spg_image').hide();
    @elseif( $type == 'uc8' )
        $('.slot_uc8_image').hide();
	@endif
 });
 function open_game(gameid){
	 

 }
   function resizeIframe(obj){
     obj.style.height = 0;
     obj.style.height = obj.contentWindow.document.body.scrollHeight + 'px';
  }
  
  function change_iframe(url,image_link,product){
	  $('#iframe_game').attr('src',url);
	  $('.top').show();
	  $('.slot_'+product+'_image').hide();
  }
 </script>

@stop

@section('content')
<!--Slot-->
@include('front/slot_top')

	<iframe id="iframe_game" frameborder="0" style="overflow:hidden;overflow-x:hidden;overflow-y:hidden;height: 98%; min-height: 2000px; width:100%;" src="
	@if( $type == 'mxb' )
		{{route('mxb' , [ 'type' => 'slot' , 'category' => 'Slots' ] )}}
	@elseif( $type == 'w88' )
		{{route('w88', [ 'type' => 'slot' , 'category' => 'Arcades' ] )}}
	@elseif( $type == 'plt' )
		{{route('plt' , [ 'type' => 'pgames' , 'category' => '1' ] )}}
	@elseif( $type == 'a1a' )
		{{route('a1a')}}
	@elseif( $type == 'jok' )
		{{route('jok')}}
	@elseif( $type == 'sky' )
		{{route('sky', [ 'type' => 'slot', 'category' => 'all' ] )}}
    @elseif( $type == 'spg' )
        {{route('spg', [ 'type' => 'slot' , 'category' => 'slot' ] )}}
    @elseif( $type == 'uc8' )
        {{route('uc8', [ 'type' => 'slot', 'category' => 'all' ] )}}
	@endif
	" onload="resizeIframe(this)"></iframe>
<!--Slot-->

<div class="clr"></div>
@if (Session::get('currency') == 'MYR')
<div class="seo_content">
    @if( $type == 'mxb' )
        <h1 style="font-size:13px;">Why Play MaxBet’s Online Slots Compared to Land-Based Slots?</h1>
        <p>In the beginning, land-based slots are the craze at almost every casinos in the world, from Las Vegas to Monte Carlo. The prospect of pulling the lever and chinks of coins flowing down into payout tray is everyone’s dream. Thanks to technology, you can now play slots game anywhere in the world, without needing to go the US or other countries. Infiniwin is one of those places.</p>
        <p>Infiniwin is the best online slot place in Malaysia. It has been operating for several years and known to provide the best gaming and gambling service to players around Malaysia. There are several online slot game provider in Infiniwin and one of them is MaxBet. Here we provide several reasons why MaxBet online slots are even better compared to land-based slots overseas.</p>
        <p>Firstly, playing MaxBet online slots means that you will not be spending your hard-earned money on other things except for slots alone. If you travel to any land-based casinos, you will need to spend on drinks, food, accommodation as well as transportation. These are unnecessary expenses that are completely eliminated if you play online slots game.</p>
        <p>Secondly, online casinos have virtually unlimited space to host all the slots game available. If you go to land-based casinos, slots games machines are filling up the floor and sooner or later, the old ones that are not played as much will be thrown away. What if those old ones are your favorites? If you go to online casinos, you can play these classic slots game easily and the online slot provider does not need to pay that much upkeep compared to land-based casinos.</p>
        <p>Lastly, online casinos like MaxBet are just a cut above the rest. The quality of MaxBet games combined with Infiniwin’s great customer service leaves no choice for players to come and stay for the fun!</p>
    @elseif( $type == 'spg' )
        <h1 style="font-size:13px;">What is So Good About Spade Gaming Infiniwin Slots?</h1>
        <p>Infiniwin is one of the best online slot website in Malaysia right now. Its sleek and fast website with games that has gorgeous graphic and sounds are just some of the reasons why it is preferred by new and experienced players alike. There are a lot of game providers in Infiniwin and one of them is Spade Gaming.</p>
        <p>Spade Gaming is perhaps the best online slot game providers in the Asia region. Established more than 10 years ago, this once small company from Philippines now has sales offices in almost all countries in South East Asia. This shows how popular and trusted the brand is to all online casino players.</p>
        <p>What is good about Spade Gaming slots games is that there is always something for everyone. Spade Gaming’s specialty is online slots game and there are more than a hundred of them are developed by their developers for the players. You can choose to play the normal slots game which is easy to pick up and play but those who are seeking for more thrill and rewards will want to try the progressive slots.</p>
        <p>Progressive slots are a bit different from normal slots. While normal slots has a fixed jackpot amount, progressive slots has an accumulating jackpot amount, as long as no one wins it. Some of the Spade Gaming progressive slots game are New Big Prosperity, Lucky Tank, Adventure Iceland and Pocket Mon Go. If you do not want to play progressive slots, you can play the normal slots like Fafafa, Master Chef, and New The Song.</p>
        <p>If you want to choose between normal and progressive slots game, what you need to do is choose how much do you hope to win. If you want to win a specific amount based on your wager, then you can play the normal slots game. Normal slots game are also has cheaper bets compared to progressive slots game. If you want to seek some thrills and gain many times more than your wager, then you can play the progressive slots.</p>
        <p>Infiniwin is the best place to play online slots in Malaysia. You can play Infiniwin anywhere, as long as you have a stable Internet connection. Creating an Infiniwin account will only takes about five minutes so what are you waiting for? Come and join us and win!</p>
    @elseif( $type == 'plt' )
        <h1 style="font-size:13px;">Four Tips to Win Big in Infiniwin PlayTech Slots Game</h1>
        <p>PlayTech is one of the slots game providers in Infiniwin. Even though slots game is PlayTech’s expertise, it also providers other types of games to Infiniwin like online casinos, video poker, as well as online scratchcards. If you want to play online slot in Infiniwin, here are the top four tips to win big without breaking the bank.</p>
        <p>Firstly, you need to know that online slots game are not as simple as you think. Yes, playing it is easy - just click a button and the reels will turn. However, winning online slots game is not that easy. You need to first learn to calculate your probability of winning. Just multiply the number of symbols that the slot game has. </p>
        <p>As an example, if a slot game has three reels with five symbols, it worked out as: 5 x 5 x 5 = 125. Then, you will need to divide it by the number of ways to win. If let’s say that slot game has 243 days to win, just divide 125 / 243 x 100 = 51%. This means 51% of your rolls will win something. Of course, different combination will have different payouts but this should give a ballpark figure on your probability of winning.</p>
        <p>Secondly, you need to pick slots games with a small jackpot. This might not be a popular decision to many but this tips is actually very important. This is because, the smaller the jackpot, the easier for you to win in a short time. This means that you will need to spend less compared to trying to win slots game with higher jackpot amount. Not to mention, slots games with high jackpot amount are usually more expensive compared to low jackpot ones.</p>
        <p>The third tips is to play slots with a lot of bonus rounds and wilds. This is because bonus rounds will let you play without needing to pay anything. This increases your chances to win by miles compared to those without bonus rounds. Wilds, on the other hand, dramatically increases your chances of winning because it can become any symbol on the slots game.</p>
        <p>Last but not least, perhaps the most important tips of all, is to play PlayTech slots game in Infiniwin. Infiniwin is the best website that provide online slots in Malaysia and you should utilize all the tips above to win your wager easily.</p>
    @elseif( $type == 'sky' )
        <h1 style="font-size:13px;">Four Tips to Play Infiniwin’s Sky3888 Slots Game</h1>
        <p>No matter what casino you go, either live casinos or online casinos, slots games are the crowd favorite. With some money in your hand and blessings from lady luck, you can win a lot more than you can imagine easily. The odds are the same and everything is decided by the slots machine. However, one question still remains: how can I win at slots game?</p>
        <p>Infiniwin is currently one of the best places to play online slots game in Malaysia. There are a lot of slots game providers in Infiniwin and one of them is Sky3888. Follow the below tips to learn the fundamental tips to win at Sky3888 slots game.</p>
        <ul style='list-style: none;'>
            <li>1. Higher denominations = higher payback percentages</li>
            <li>If you are playing online slots game with higher denominations, it will be faster to get back your wager if you win. This is because lower denominations means lower payouts and you do not want to play 50 rounds just to get back that $5 you spend on slots game.</li>
            <li>2. Play games that fits you</li>
            <li>Different slots game has different graphics and sounds. Some slots game has breathtaking graphics and magnificent sound quality while others are really low-key with minimal differences to old slots machine. It’s up to your preferences but ensure that you pick a slots game that you are able to stay playing for a period of time. This is important for the next tips below.</li>
            <li>3. Start small</li>
            <li>Some slots game has hidden algorithm that will reward players who spend a long time at it. To make sure that you do not waste your chances to win this, you can start playing from the lowest denomination. Increase your bets gradually and let’s hope that the slots game acknowledged your game time and reward you with a jackpot!</li>
            <li>4. Be aware of your money</li>
            <li>Always set a limit to how much you want to bet on online slots game and make sure to stick to it. There are a lot of people who cannot stop themselves from playing rounds after rounds after rounds of slots until their money dry up. Do not let your emotions control you and keep it in check.</li>
        </ul>
        <p>Use the above tips to get that delicious jackpot from Infiniwin’s Sky3888 slot games and you will thank me for it!</p>
    @endif
</div>
@endif
@stop

@section('bottom_js')
<script>
    $('.bxslider').bxSlider({
  pagerCustom: '#bx-pager'
});
</script>
<script src="{{url()}}/front/resources/js/slick.min.js"></script>
<script>
$('.caro').slick({
  infinite: true,
  slidesToShow: 2,
  slidesToScroll: 2,
  autoplay: true,
  variableWidth: true,
  arrows: false
});
</script>
<script type="text/javascript">
         
   var slider = new MasterSlider();
 
    // adds Arrows navigation control to the slider.
    slider.control('arrows');
    slider.control('timebar' , {insertTo:'#masterslider'});
    slider.control('bullets');
 
     slider.setup('masterslider' , {
         width:1400,    // slider standard width
         height:380,   // slider standard height
         space:1,
         layout:'fullwidth',
         loop:true,
         preload:0,
         autoplay:true
    });
 
</script>
@stop