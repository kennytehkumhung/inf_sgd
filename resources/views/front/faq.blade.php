@extends('front/master')

@if (Session::get('currency') == 'TWD')
    @section('title', '無極限娛樂網')
@elseif (Session::get('currency') == 'MYR')
    @section('title', 'Best Online Live Casino Malaysia - FAQ')
    @section('keywords', 'Best Online Live Casino Malaysia - FAQ')
    @section('description', 'Find an answer to any of your queries regarding InfiniWin Online Casino Malaysia.')
@endif

@section('top_js')
<link href="{{url()}}/front/resources/css/otherPg.css" rel="stylesheet">
@stop

@section('content')
<!--MID SECTION-->
<div class="midSect">
   <div class="midSectInner">
<?php echo htmlspecialchars_decode($content); ?>
   </div>
</div>
<!--MID SECTION-->
@stop

@section('bottom_js')

@stop