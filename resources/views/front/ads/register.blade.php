<!DOCTYPE html>
<!--[if lt IE 7]>      <html lang="en" class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html lang="en" class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html lang="en" class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
    <head>
    	<!-- meta charec set -->
        <meta charset="utf-8">
		<!-- Always force latest IE rendering engine or request Chrome Frame -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<!-- Page Title -->
        <title>Infiniwin Ads</title>		
		<!-- Meta Description -->
        <meta name="description" content="Infiniwin Ads">
        <meta name="keywords" content="Infiniwin Ads">
        <meta name="author" content="Infiniwin Ads">
		<!-- Mobile Specific Meta -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
		
		<!-- Google Font -->
		
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>
        
        <link href="https://fonts.googleapis.com/css?family=Bilbo+Swash+Caps" rel="stylesheet"> 

		<!-- CSS
		================================================== -->
		<!-- Fontawesome Icon font -->
        <link rel="stylesheet" href="{{url()}}/front/ads/css/font-awesome.min.css">
		<!-- Twitter Bootstrap css -->
        <link rel="stylesheet" href="{{url()}}/front/ads/css/bootstrap.min.css">
		<!-- jquery.fancybox  -->
        <link rel="stylesheet" href="{{url()}}/front/ads/css/jquery.fancybox.css">
		<!-- animate -->
        <link rel="stylesheet" href="{{url()}}/front/ads/css/animate.css">
		<!-- Main Stylesheet -->
        <link rel="stylesheet" href="{{url()}}/front/ads/css/main.css">
		<!-- media-queries -->
        <link rel="stylesheet" href="{{url()}}/front/ads/css/media-queries.css">

		<!-- Modernizer Script for old Browsers -->
        <script src="{{url()}}/front/ads/js/modernizr-2.6.2.min.js"></script>
<script>
  function register_submit(){
	$.ajax({
		type: "POST",
		url: "{{route('register_process')}}",
		data: {
			_token: "{{ csrf_token() }}",
			username:		 $('#r_username').val(),
			password: 		 $('#r_password').val(),
			repeatpassword:  $('#r_repeatpassword').val(),
			code:    		 $('#r_code').val(),
			email:    		 $('#r_email').val(),
			mobile:    		 $('#r_mobile').val(),
			fullname:        $('#r_fullname').val(),
			dob:			 $('#dob').val(),
			wechat:			 $('#r_wechat').val(),
			line:			 $('#r_line').val(),
            referralid:		 $('#r_referralid').val()
		},
	}).done(function( json ) {
			 $('.acctTextReg').html('');
			 obj = JSON.parse(json);
			 var str = '';
			 $.each(obj, function(i, item) {
				if( '{{Lang::get('COMMON.SUCESSFUL')}}' == item ){
					alert('{{Lang::get('public.NewMember')}}');
					window.location.href = "{{route('homepage')}}";
				}else{
					$('.'+i+'_acctTextReg').html(item);
					//str += item + '\n';
				}
			})
			
			//alert(str);
			
			
	});
}
</script>
    </head>
	
    <body id="body">
	
		<!-- preloader -->
		<div id="preloader">
			<img src="{{url()}}/front/ads/img/30.gif" alt="Preloader">
		</div>
		<!-- end preloader -->		
		
		
        <!--
        Home Slider
        ==================================== -->
		
		<section id="slider" style="display: none;">
			<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
			
				<!-- Indicators bullet -->
				<ol class="carousel-indicators">
					<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
					<li data-target="#carousel-example-generic" data-slide-to="1"></li>
				</ol>
				<!-- End Indicators bullet -->				
				
				<!-- Wrapper for slides -->
				<div class="carousel-inner" role="listbox">
					
					<!-- single slide -->
					<div class="item active" style="background-image: url({{url()}}/front/ads/img/banner.jpg);">
						<div class="carousel-caption">
							<h2 data-wow-duration="700ms" data-wow-delay="500ms" class="wow bounceInDown animated">Meet<span> Brandi</span>!</h2>
							<h3 data-wow-duration="1000ms" class="wow slideInLeft animated"><span class="color">/creative</span> one page template.</h3>
							<p data-wow-duration="1000ms" class="wow slideInRight animated">We are a team of professionals</p>
							
							<ul class="social-links text-center">
								<li><a href=""><i class="fa fa-twitter fa-lg"></i></a></li>
								<li><a href=""><i class="fa fa-facebook fa-lg"></i></a></li>
								<li><a href=""><i class="fa fa-google-plus fa-lg"></i></a></li>
								<li><a href=""><i class="fa fa-dribbble fa-lg"></i></a></li>
							</ul>
						</div>
					</div>
					<!-- end single slide -->

					
				</div>
				<!-- End Wrapper for slides -->
				
			</div>
		</section>
		
        <!--
        End Home SliderEnd
        ==================================== -->
		
        <!--
        Features
        ==================================== -->
		
		<section id="features" class="features">
			<div class="container">
				<div class="row">
					<div class="sec-title text-center mb50 wow bounceInDown animated" data-wow-duration="1000ms">
						<h2><img src="{{url()}}/front/ads/img/logo-white.png"></h2>
					</div>
                    <div class="col-md-12 text-center mb50 wow bounceInUp animated" data-wow-duration="1000ms">
						<h2 class="ab1">Join us and become the next millionaire</h2>
					</div>
					
					<!-- service item -->
					<div style="margin-top: 10px;" class="col-md-offset-3 col-md-6 wow fadeInRight ctr" data-wow-duration="1000ms"  data-wow-delay="900ms">
					<div class="regWrap">
                    <div class="acctContentReg">
                    <div class="input-field">
		            <label>Username :</label><input id="r_username" type="text"><span class="username_acctTextReg acctTextReg"></span>
		            <div class="clr"></div>
                    </div>
                    <div class="input-field">
		            <label>Password :</label><input id="r_password" type="password"><span class="password_acctTextReg acctTextReg"></span>
		            <div class="clr"></div>
                    </div>
                    <div class="input-field">
		            <label>Re-enter password :</label><input id="r_repeatpassword" type="password">
		            <div class="clr"></div>
                    </div>
                    <div class="input-field">
		            <label>Email Address :</label><input id="r_email" type="text"><span class="email_acctTextReg acctTextReg"></span>
		            <div class="clr"></div>
                    </div>
                    <div class="input-field">
		            <label>Contact No :</label><input id="r_mobile" type="text"><span class="mobile_acctTextReg acctTextReg"></span>
		            <div class="clr"></div>
                    </div>

	  		       <div class="input-field">
			       <label>Full Name :</label><input id="r_fullname" type="text"><span class="fullname_acctTextReg acctTextReg"></span>
			       <div class="clr"></div>
		           </div>
		           <div class="input-field">
			  <label>Date of birth :</label>
			  <select style="margin-right: 8px;" id="dob">
				<option value="01">01</option>
				<option value="02">02</option>
				<option value="03">03</option>
				<option value="04">04</option>
				<option value="05">05</option>
				<option value="06">06</option>
				<option value="07">07</option>
				<option value="08">08</option>
				<option value="09">09</option>
				<option value="10">10</option>
				<option value="11">11</option>
				<option value="12">12</option>
				<option value="13">13</option>
				<option value="14">14</option>
				<option value="15">15</option>
				<option value="16">16</option>
				<option value="17">17</option>
				<option value="18">18</option>
				<option value="19">19</option>
				<option value="20">20</option>
				<option value="21">21</option>
				<option value="22">22</option>
				<option value="23">23</option>
				<option value="24">24</option>
				<option value="25">25</option>
				<option value="26">26</option>
				<option value="27">27</option>
				<option value="28">28</option>
				<option value="29">29</option>
				<option value="30">30</option>
				<option value="31">31</option>
			  </select>
			  <select style="margin-right: 8px;">
				<option value="01">01</option>
				<option value="02">02</option>
				<option value="03">03</option>
				<option value="04">04</option>
				<option value="05">05</option>
				<option value="06">06</option>
				<option value="07">07</option>
				<option value="08">08</option>
				<option value="09">09</option>
				<option value="10">10</option>
				<option value="11">11</option>
				<option value="12">12</option>
			  </select>
			  <select>
				<option value="1995">1995</option>
				<option value="1994">1994</option>
				<option value="1993">1993</option>
				<option value="1992">1992</option>
				<option value="1991">1991</option>
				<option value="1990">1990</option>
				<option value="1989">1989</option>
				<option value="1988">1988</option>
				<option value="1987">1987</option>
				<option value="1986">1986</option>
				<option value="1985">1985</option>
				<option value="1984">1984</option>
				<option value="1983">1983</option>
				<option value="1982">1982</option>
				<option value="1981">1981</option>
				<option value="1980">1980</option>
				<option value="1979">1979</option>
				<option value="1978">1978</option>
				<option value="1977">1977</option>
				<option value="1976">1976</option>
				<option value="1975">1975</option>
				<option value="1974">1974</option>
				<option value="1973">1973</option>
				<option value="1972">1972</option>
				<option value="1971">1971</option>
				<option value="1970">1970</option>
				<option value="1969">1969</option>
				<option value="1968">1968</option>
				<option value="1967">1967</option>
				<option value="1966">1966</option>
				<option value="1965">1965</option>
				<option value="1964">1964</option>
				<option value="1963">1963</option>
				<option value="1962">1962</option>
				<option value="1961">1961</option>
				<option value="1960">1960</option>

			  </select>
			  <div class="clr"></div>
		  </div>

		  <div class="input-field">
			  <label style="float: left;"> Code :</label><input id="r_code" type="text"><img style="margin-left:6px;" src="{{route('captcha', ['type' => 'register_captcha'])}}" width="60" height="22"> <span class="code_acctTextReg acctTextReg"></span>
			  <div class="clr"></div>
		  </div>

		  

	  </div>
                    </div>
					</div>
					<!-- end service item -->

                    <div class="row">
                    
                    <div class="col-md-12 text-center wow fadeInRight" data-wow-duration="1800ms"  data-wow-delay="300ms">
					    <div class="joinBtn" onClick="register_submit()"><a href="#">Submit</a></div>
					</div>
                    </div>
						
				</div>
			</div>
		</section>
		
        <!--
        End Features
        ==================================== -->

			
		<footer id="footer" class="footer">
			<div class="container">
				
				<div class="row">
					<div class="col-md-12">
						<p class="copyright text-center">
							Copyright © Infiniwin. All rights reserved
						</p>
					</div>
				</div>
			</div>
		</footer>
		
		<a href="javascript:void(0);" id="back-top"><i class="fa fa-angle-up fa-3x"></i></a>

		<!-- Essential jQuery Plugins
		================================================== -->
		<!-- Main jQuery -->
        <script src="{{url()}}/front/ads/js/jquery-1.11.1.min.js"></script>
		<!-- Single Page Nav -->
        <script src="{{url()}}/front/ads/js/jquery.singlePageNav.min.js"></script>
		<!-- Twitter Bootstrap -->
        <script src="{{url()}}/front/ads/js/bootstrap.min.js"></script>
		<!-- jquery.fancybox.pack -->
        <script src="{{url()}}/front/ads/js/jquery.fancybox.pack.js"></script>
		<!-- jquery.mixitup.min -->
        <script src="{{url()}}/front/ads/js/jquery.mixitup.min.js"></script>
		<!-- jquery.parallax -->
        <script src="{{url()}}/front/ads/js/jquery.parallax-1.1.3.js"></script>
		<!-- jquery.countTo -->
        <script src="{{url()}}/front/ads/js/jquery-countTo.js"></script>
		<!-- jquery.appear -->
        <script src="{{url()}}/front/ads/js/jquery.appear.js"></script>
		<!-- Contact form validation -->
		<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery.form/3.32/jquery.form.js"></script>
		<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.11.1/jquery.validate.min.js"></script>
		<!-- Google Map -->
        <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>
		<!-- jquery easing -->
        <script src="{{url()}}/front/ads/js/jquery.easing.min.js"></script>
		<!-- jquery easing -->
        <script src="{{url()}}/front/ads/js/wow.min.js"></script>
		<script>
			var wow = new WOW ({
				boxClass:     'wow',      // animated element css class (default is wow)
				animateClass: 'animated', // animation css class (default is animated)
				offset:       120,          // distance to the element when triggering the animation (default is 0)
				mobile:       false,       // trigger animations on mobile devices (default is true)
				live:         true        // act on asynchronously loaded content (default is true)
			  }
			);
			wow.init();
		</script> 
		<!-- Custom Functions -->
        <script src="{{url()}}/front/ads/js/custom.js"></script>
		
		<script type="text/javascript">
			$(function(){
				/* ========================================================================= */
				/*	Contact Form
				/* ========================================================================= */
				
				$('#contact-form').validate({
					rules: {
						name: {
							required: true,
							minlength: 2
						},
						email: {
							required: true,
							email: true
						},
						message: {
							required: true
						}
					},
					messages: {
						name: {
							required: "come on, you have a name don't you?",
							minlength: "your name must consist of at least 2 characters"
						},
						email: {
							required: "no email, no message"
						},
						message: {
							required: "um...yea, you have to write something to send this form.",
							minlength: "thats all? really?"
						}
					},
					submitHandler: function(form) {
						$(form).ajaxSubmit({
							type:"POST",
							data: $(form).serialize(),
							url:"process.php",
							success: function() {
								$('#contact-form :input').attr('disabled', 'disabled');
								$('#contact-form').fadeTo( "slow", 0.15, function() {
									$(this).find(':input').attr('disabled', 'disabled');
									$(this).find('label').css('cursor','default');
									$('#success').fadeIn();
								});
							},
							error: function() {
								$('#contact-form').fadeTo( "slow", 0.15, function() {
									$('#error').fadeIn();
								});
							}
						});
					}
				});
			});
		</script>
    </body>
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function()
{ (i[r].q=i[r].q||[]).push(arguments)}

,i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-43720111-1', 'auto');
ga('send', 'pageview');
</script>	
</html>
