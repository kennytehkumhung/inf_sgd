<html>
<head>
 <link href="{{url()}}/front/resources/css/slot.css" rel="stylesheet">
 <link href="{{url()}}/front/resources/css/style_2.css" rel='stylesheet' type='text/css'>

<!-- Home slider style -->
<link rel="stylesheet" href="{{url()}}/front/resources/css/style_3.css">
</head>
<body>
	<div class="slot_menu">
		<ul>
			<li><a href="{{route('plt', [ 'type' => 'pgames' , 'category' => '1' ] )}}">Progressive Games</a></li>
                        <li><a href="{{route('plt', [ 'type' => 'newgames' , 'category' => '1' ] )}}">New Games</a></li>
                        <li><a href="{{route('plt', [ 'type' => 'brand' , 'category' => '1' ] )}}">Branded Games</a></li>
                        <li><a href="{{route('plt', [ 'type' => 'slot' , 'category' => 'slot' ] )}}">Slot (30)</a></li>
                        <li><a href="{{route('plt', [ 'type' => 'slot' , 'category' => 'videopoker' ] )}}">Video Poker (22)</a></li>
                        <li><a href="{{route('plt', [ 'type' => 'slot' , 'category' => 'arcade' ] )}}">Arcade (16)</a></li>
			<li><a href="{{route('plt', [ 'type' => 'slot' , 'category' => 'tablecards' ] )}}">Tablecards (4)</a></li>
			<li><a href="{{route('plt', [ 'type' => 'slot' , 'category' => 'scratchcards' ] )}}">Scratchcards(15)</a></li>
		</ul>
	</div>
	<div id="slot_lobby">		
		<div class="slot_box">
		<a onclick=" @if (Auth::user()->check())window.open('{{route('pltslotiframe' , [ 'gamecode' => 'sfh' ] )}}', 'plt_slot');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif  " href="javascript:void(0)">
		<img width="150" height="150" alt="" src="https://infiniwin.net/front/img/plt/sfh.jpg">
		</a>
		<span>Safari Heat</span>
		</div>	
		<div class="slot_box">
		<a onclick=" @if (Auth::user()->check())window.open('{{route('pltslotiframe' , [ 'gamecode' => 'dnr' ] )}}', 'plt_slot');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif  " href="javascript:void(0)">
		<img width="150" height="150" alt="" src="https://infiniwin.net/front/img/plt/dnr.jpg">
		</a>
		<span>Dolphin Reef</span>
		</div>
		<div class="slot_box">
		<a onclick=" @if (Auth::user()->check())window.open('{{route('pltslotiframe' , [ 'gamecode' => 'gtsdnc' ] )}}', 'plt_slot');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif  " href="javascript:void(0)">
		<img width="150" height="150" alt="" src="https://infiniwin.net/front/img/plt/gtsdnc.jpg">
		</a>
		<span>dolphin cash scratch</span>
		</div>
		@foreach( $lists as $list )
		<div class="slot_box">
			<a href="javascript:void(0)" onclick="@if (Auth::user()->check())window.open('{{route('pltslotiframe' , [ 'gamecode' => $list['code'] ] )}}', 'plt_slot');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif ">
				<img src="{{url()}}/front/img/plt/{{$list->code}}.jpg" width="150" height="150" alt=""/>
			</a>
			<span>{{$list->gameName}}</span>
		</div>				 
		@endforeach
		<div class="clr"></div>
	</div>
    <div class="clr"></div> 
<!--Slot-->
</body>
</html>