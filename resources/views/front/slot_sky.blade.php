<html>
<head>
 <link href="{{url()}}/front/resources/css/slot.css" rel="stylesheet">
 <link href="{{url()}}/front/resources/css/style_2.css" rel='stylesheet' type='text/css'>

<!-- Home slider style -->
<link rel="stylesheet" href="{{url()}}/front/resources/css/style_3.css">
</head>
<body>
	<div class="slot_menu">
		<ul>
            <li><a href="{{route('sky', [ 'type' => 'slot' , 'category' => 'singleplayer_games' ] )}}">{{ Lang::get('public.Slots') }} (85)</a></li>
            <li><a href="{{route('sky', [ 'type' => 'slot' , 'category' => 'online_casino' ] )}}">{{ Lang::get('public.TableGames') }} (4)</a></li>
            <li><a href="{{route('sky', [ 'type' => 'slot' , 'category' => 'multiplayer_p2p' ] )}}">{{ Lang::get('public.Multiplayer') }} (2)</a></li>
            {{--<li><a href="{{route('sky', [ 'type' => 'slot' , 'category' => 'online_games' ] )}}">{{ Lang::get('public.Arcades') }} (1)</a></li>--}}
		</ul>
	</div>
	<div id="slot_lobby">		
		@foreach( $lists as $list )
        <div class="slot_box">
           <a href="javascript:void(0)" onclick="@if (Auth::user()->check())window.open('{{route('sky-slot', array('gamecode' => $list['game_code']))}}','sky_slot')@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif">
               <img src="{{ $list['img_url'] }}" width="150" height="150" alt=""/>
           </a>
           <span>{{ $list['game_name'] }}</span>
        </div>
		@endforeach
		<div class="clr"></div>
	</div>
    <div class="clr"></div> 
<!--Slot-->
</body>
</html>
