@extends('front/master')

@if (Session::get('currency') == 'TWD')
    @section('title', '無極限娛樂網')
@elseif (Session::get('currency') == 'MYR')
    @section('title', 'Best Online Live Casino Malaysia - About Us')
    @section('keywords', 'Best Online Live Casino Malaysia - About Us')
    @section('description', 'Learn about InfiniWin Malaysia\'s Online Casino, a multiple award-winning online casino in Malaysia that gives you all the thrills you could possibly want from gaming online.')
@endif

@section('top_js')
<link href="{{url()}}/front/resources/css/otherPg.css" rel="stylesheet">
@stop

@section('content')
<!--MID SECTION-->
<?php echo htmlspecialchars_decode($content); ?>
<!--MID SECTION-->
@stop

@section('bottom_js')

@stop