
<!-- masterslider -->
<div >
	@if (Lang::getLocale() == 'tw')
		<img src="{{url()}}/front/img/tw/bannerslot.png" style="padding-bottom: 40px;" />
	@else
		<img src="{{url()}}/front/img/banner/slot_head.jpg" style="padding-bottom: 40px;" />
	@endif
</div>
    <!-- end of slide -->
</div>

<div class="slotCont">

	<table width="auto" border="0" cellspacing="0" cellpadding="0" style="margin: 0 auto;">

		<tr>
			@if(in_array('MXB',Session::get('valid_product')))
			<td>

				<div class="lcBox">

					<div id="crossfade">

						<a href="{{route('slot', [ 'type' => 'mxb'  ])}}">

							@if( $name == 'mxb' )

								<img class="bottom" width="190px" src="{{url()}}/front/img/mxb-slot-hover.png" />

							@else

								<img class="bottom" width="190px" src="{{url()}}/front/img/mxb-slot-hover.png" />

								<img class="top slot_mxb_image" width="190px" src="{{url()}}/front/img/mxb-slot.png" />

							@endif

						</a>

					</div>

				</div>

			</td>
			@endif
			{{--@if(in_array('W88',Session::get('valid_product')))--}}
			{{--<td>--}}

				{{--<div class="lcBox">--}}

					{{--<div id="crossfade">--}}

						{{--<a href="javascript:void(0)" onClick="change_iframe('{{route('w88' , [ 'type' => 'slot' , 'category' => 'Arcades' ] )}}','gp-slot-hover.png','w88')">--}}

							{{--@if( $name == 'w88' )--}}

								{{--<img class="bottom" width="190px" src="{{url()}}/front/img/gp-slot-hover.png" />--}}

							{{--@else--}}

								{{--<img class="bottom" width="190px" src="{{url()}}/front/img/gp-slot-hover.png" />--}}

								{{--<img class="top slot_w88_image" width="190px" src="{{url()}}/front/img/gp-slot.png" />--}}

							{{--@endif--}}

						{{--</a>--}}

					{{--</div>--}}

				{{--</div>--}}

			{{--</td>--}}
			{{--@endif--}}
			@if(in_array('SPG',Session::get('valid_product')))
				<td>

					<div class="lcBox">

						<div id="crossfade">

							<a href="{{route('slot', [ 'type' => 'spg'  ])}}">

								@if( $name == 'spg' )

									<img class="bottom" width="190px" src="{{url()}}/front/img/sp-slot-hover.png" />

								@else

									<img class="bottom" width="190px" src="{{url()}}/front/img/sp-slot-hover.png" />

									<img class="top slot_w88_image" width="190px" src="{{url()}}/front/img/sp-slot.png" />

								@endif

							</a>

						</div>

					</div>

				</td>
			@endif
			@if(in_array('PLT',Session::get('valid_product')))
			<td>

				<div class="lcBox">

					<div id="crossfade">

						<a href="{{route('slot', [ 'type' => 'plt'  ])}}">
							@if( $name == 'plt' )

								<img class="bottom" width="190px" src="{{url()}}/front/img/pt-slot-hover.png" />

							@else

								<img class="bottom" width="190px" src="{{url()}}/front/img/pt-slot-hover.png" />

								<img class="top slot_plt_image" width="190px" src="{{url()}}/front/img/pt-slot.png" />

							@endif

						</a>

						</a>

					</div>

				</div>

			</td>
			@endif
			@if(in_array('SPG',Session::get('valid_product')))
			<!--<td>

                    <div class="lcBox">

                        <div id="crossfade">

                            <a href="javascript:void(0)" onClick="change_iframe('{{route('spg', [ 'type' => 'slot' , 'category' => 'slot' ] )}}','sp-slot-hover.png','spg')">
							@if( $name == 'spg' )

								<img class="bottom" width="190px" src="{{url()}}/front/img/sp-slot-hover.png" />

							@else

								<img class="bottom" width="190px" src="{{url()}}/front/img/sp-slot-hover.png" />

								<img class="top slot_plt_image" width="190px" src="{{url()}}/front/img/sp-slot.png" />

							@endif

						</a>

						</a>

					</div>

				</div>

			</td>-->
			@endif
            @if(in_array('JOK',Session::get('valid_product')))
			<td>

				<div class="lcBox">

					<div id="crossfade">

						<a href="javascript:void(0);" onclick="@if (Auth::user()->check())window.open('{{route('jok')}}','jok_slot')@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif">
							@if( $name == 'jok' )

								<img class="bottom" width="190px" src="{{url()}}/front/img/joker-slot-hover.png" />

							@else

								<img class="bottom" width="190px" src="{{url()}}/front/img/joker-slot-hover.png" />

								<img class="top slot_jok_image" width="190px" src="{{url()}}/front/img/joker-slot.png" />

							@endif

						</a>

						</a>

					</div>

				</div>

			</td>
			@endif
            @if(in_array('SKY',Session::get('valid_product')))
			<td>

				<div class="lcBox">

					<div id="crossfade">

						<a href="{{route('slot', [ 'type' => 'sky'  ])}}">
							@if( $name == 'sky' )

								<img class="bottom" width="190px" src="{{url()}}/front/img/scr-slot-hover.png" />

							@else

								<img class="bottom" width="190px" src="{{url()}}/front/img/scr-slot-hover.png" />

								<img class="top slot_sky_image" width="190px" src="{{url()}}/front/img/scr-slot.png" />

							@endif

						</a>

						</a>

					</div>

				</div>

			</td>
			@endif
			@if(in_array('UC8',Session::get('valid_product')))
				<td>

					<div class="lcBox">

						<div id="crossfade">

							<a href="javascript:void(0)" onClick="change_iframe('{{route('uc8', ['type' => 'slot', 'category' => 'all'] )}}','scr-slot-hover.png','uc8')">
								@if( $name == 'uc8' )

									<img class="bottom" width="190px" src="{{url()}}/front/img/uc8-slot-hover.png" />

								@else

									<img class="bottom" width="190px" src="{{url()}}/front/img/uc8-slot-hover.png" />

									<img class="top slot_uc8_image" width="190px" src="{{url()}}/front/img/uc8-slot.png" />

								@endif

							</a>

							</a>

						</div>

					</div>

				</td>
			@endif
		</tr>

	</table>