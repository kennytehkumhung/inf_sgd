@extends('front/master')

@if (Session::get('currency') == 'TWD')
    @section('title', '無極限娛樂網')
@elseif (Session::get('currency') == 'MYR')
    @section('title', 'Best Online Live Casino Games in Malaysia')
    @section('keywords', 'Best Online Live Casino Games in Malaysia')
    @section('description', 'A new live online casino experience from InfiniWin Malaysia, including roulette, blackjack and slots. Sign Up TODAY and get Free Spin Bonus!')
@endif

@section('top_js')
<link href="{{url()}}/front/resources/css/jquery.bxslider.css" type="text/css" media="all" rel="stylesheet" />

<script type="text/javascript" language="javascript" src="{{url()}}/front/resources/js/jquery.bxslider.min.js"></script>
<style>
body  {
	@if (Session::get('currency') == 'TWD')
		background-image: url('{{ asset('/front/img/tw/lc_bg.jpg') }}') !important;
	@else
		background-image: url({{url()}}/front/resources/img/bg-lc.jpg) !important;
	@endif
	background-repeat: no-repeat; background-position:
	top center; background-color: #000000;}
    
    #bx-pager img { height: auto; width: 102px; }
    #bx-pager a { vertical-align: bottom; height: 102px; }
</style>
@stop

@section('content')

<!--Slider-->
<div class="sliderCont">
<ul class="bxslider" style="cursor:pointer;">


  @if(in_array('AGG',Session::get('valid_product')))
		<li>
			<a id="gameroom_agg" onClick="@if (Auth::user()->check())window.open('{{route('agg')}}', 'casino11');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif">
				<img src="{{url()}}/front/resources/img/banner/casino-1.png" />
			</a>
		</li>
  @endif

  
  @if(in_array('PLT',Session::get('valid_product')))
		<li>
			<a id="gameroom_plt" onClick="@if (Auth::user()->check())window.open('{{route('pltiframe')}}', 'casinoplt');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif">
				<img src="{{url()}}/front/resources/img/banner/casino-2.png" />
			</a>
		</li>
  @endif


  @if(in_array('MXB',Session::get('valid_product')))
		<li>
			<a id="gameroom_mxb" onClick="@if (Auth::user()->check())window.location.href='{{route('mxb',[ 'type' => 'casino' , 'category' => 'baccarat' ])}}';@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif">
				@if (Lang::getLocale() == 'tw')
					<img src="{{url()}}/front/img/tw/casino-4.png" />
				@else
					<img src="{{url()}}/front/resources/img/banner/casino-4.png" />
				@endif
			</a>
		</li>
  @endif

  @if(in_array('ALB',Session::get('valid_product')))
		<li>
			<a id="gameroom_alb" onClick="@if (Auth::user()->check())window.open('{{route('alb', [ 'type' => 'casino' ] )}}', 'casino12');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif" >
				@if (Lang::getLocale() == 'tw')
					<img src="{{url()}}/front/img/tw/casino-5.png" />
				@else
					<img src="{{url()}}/front/resources/img/banner/casino-5.png" />
				@endif
			</a>
		</li>
  @endif

  @if(in_array('VGS',Session::get('valid_product')))
		<li>
			<a id="gameroom_vgs" onClick="@if (Auth::user()->check())window.open('{{route('vgs', [ 'type' => 'casino' ] )}}', 'casino13');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif" >
				@if (Lang::getLocale() == 'tw')
					<img src="{{url()}}/front/img/tw/casino-evolution.png" />
				@else
					<img src="{{url()}}/front/resources/img/banner/casino-evolution.png" />
				@endif
			</a>
		</li>
  @endif

  @if(in_array('HOG',Session::get('valid_product')))
	  <li>
		  <a id="gameroom_hog" onClick="@if (Auth::user()->check())window.open('{{route('hog' )}}', 'casino14');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif" >
			  @if (Lang::getLocale() == 'tw')
				  <img src="{{url()}}/front/img/tw/casino-6.png" />
			  @else
				  <img src="{{url()}}/front/resources/img/banner/casino-6.png" />
			  @endif
		  </a>
	  </li>
  @endif
 @if(Session::get('currency')=='MYR')
  @if(in_array('WMC',Session::get('valid_product')))
	  <li>
		  <a id="gameroom_wmc" onClick="@if (Auth::user()->check())window.open('{{route('wmcframe' )}}', '');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif" >
			 
				  <img src="{{url()}}/front/resources/img/banner/casino-8.png" />
			  
		  </a>
	  </li>
  @endif
  @endif
  
  
  {{--@if(in_array('OPU',Session::get('valid_product')))--}}
		{{--<li>--}}
			{{--<a id="gameroom_opu" onClick="@if (Auth::user()->check())window.open('{{route('opu', [ 'type' => 'casino' , 'category' => 'live'] )}}', 'casino15');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif" >--}}
				{{--<img src="{{url()}}/front/resources/img/banner/casino-7.png" />--}}
			{{--</a>--}}
		{{--</li>--}}
  {{--@endif--}}
</ul>

<div id="bx-pager" style="text-align: center;">
  <?php $count = 0; ?> 
  @if(in_array('AGG',Session::get('valid_product')))
	<a onclick="openGameroom('agg');" data-slide-index="{{$count++}}" href=""><img src="{{url()}}/front/resources/img/lc-ag-thumb.png" /></a>
  @endif

  
  @if(in_array('PLT',Session::get('valid_product')))
	<a onclick="openGameroom('plt');" data-slide-index="{{$count++}}" href=""><img src="{{url()}}/front/resources/img/lc-pt-thumb.png" /></a>
  @endif

  @if(in_array('MXB',Session::get('valid_product')))
	<a onclick="openGameroom('mxb');" data-slide-index="{{$count++}}" href=""><img src="{{url()}}/front/resources/img/lc-xpro-thumb.png" /></a>
  @endif
  
  @if(in_array('ALB',Session::get('valid_product')))
	<a onclick="openGameroom('alb');" data-slide-index="{{$count++}}" href=""><img src="{{url()}}/front/resources/img/lc-allbet-thumb.png" /></a>
  @endif
  
  @if(in_array('VGS',Session::get('valid_product')))
	<a onclick="openGameroom('vgs');" data-slide-index="{{$count++}}" href=""><img src="{{url()}}/front/resources/img/lc-el-thumb.png" /></a>
  @endif

  @if(in_array('HOG',Session::get('valid_product')))
	  <a onclick="openGameroom('hog');" data-slide-index="{{$count++}}" href=""><img src="{{url()}}/front/resources/img/lc-ho-thumb.png" /></a>
  @endif
  @if(Session::get('currency')=='MYR')
    @if(in_array('WMC',Session::get('valid_product')))
	  <a onclick="openGameroom('wmc');" data-slide-index="{{$count++}}" href=""><img src="{{url()}}/front/img/lc-wm-thumb2.gif" /></a>
  @endif
 @endif
  {{--@if(in_array('OPU',Session::get('valid_product')))--}}
	{{--<a onclick="openGameroom('opu');" data-slide-index="{{$count++}}" href=""><img src="{{url()}}/front/resources/img/lc-opus-thumb.png" /></a>--}}
  {{--@endif--}}
  
</div>
</div>
<!--Slider-->
<div class="clr"></div>
@if (Session::get('currency') == 'MYR')
<div class="seo_content">
    <h1 style="font-size:13px;">Best Live Casino For Fun and Rewards: Infiniwin</h1>
	<p>Infiniwin is the best Malaysia online casino. Not only that they have a good customer service to help with any problems, Infiniwin also has partnerships with trusted and reputable live casino providers in the region. In total, there are six different live casino providers inside Infiniwin and each of them is good in their own way. To play at Infiniwin’s live casinos, make sure to create an account. Without a login, you will not be able to get an exclusive access to the live casinos.</p>
	<p>With live casinos, you will get almost the same experience as playing at land-based casinos like Genting Highlands or Las Vegas. You will be playing with a real dealer who will deal your cards live in front of a camera. Thanks to the technology we have today, the video and sound quality of online casino in Infiniwin is top-notch, with almost no delay. You will also be able to interact directly with the dealer if you have a microphone in hand. This gives a surreal experience of playing poker and black jack at the comfort of your home or anywhere you like.</p>
	<p>Since there are not many land-based casinos in Malaysia, it is very convenient to play at Infiniwin. The nearest is at Genting Highlands which is still far even if you are travelling from Kuala Lumpur. With Infiniwin’s high quality live casino, you will get the same experience and thrill as if you are at Genting Highlands without needing to waste your money on transportation and accomodation.</p>
	<p>From the six live casino providers in Infiniwin, the one we like the most is Evolution Club. This is because they have real dealers from foreign countries. This is very rare to see even if you go to Genting Highlands. Therefore, if you want to chat and interact with real dealers from the European or Americas region, you can choose Evolution Club. However, this really depends on your preferences because the other casino providers are also good in their own way.</p>
	<p>Since Infiniwin has a lot of casino providers, you can play various types and multiple variations of casino games. Just make sure that you create a valid account at Infiniwin and read the rules carefully. If you somehow violates the rules and regulations, you might have some problems to deposit or withdraw your earnings. Therefore, consult the customer service if you ever got into trouble!</p>
</div>
@endif
@stop

@section('bottom_js')
<script>
    $('.bxslider').bxSlider({
  pagerCustom: '#bx-pager'
});
</script>
<script src="{{url()}}/front/resources/js/slick.min.js"></script>
<script>
	$(function () {
	    $(".gamelink").click(function () {
	        var index = $(this).data("slide-index");

	        if (!isNaN(index)) {
                $(".gameroom:eq(" + index + ")").click();
			}
		});
	});

$('.caro').slick({
  infinite: true,
  slidesToShow: 2,
  slidesToScroll: 2,
  autoplay: true,
  variableWidth: true,
  arrows: false
});

function openGameroom(gameId) {
    $("#gameroom_" + gameId).click();
}
</script>
@stop