<!-- head section-->
@include('front/include/head')
<!-- head section-->
<body onload=display_ct();>
<div class="container">
	<div class="header">
		<!-- navigation header starts-->	
		@include('front/include/navigation')	
		<!-- navigation header ends-->

		<!--MegaMenu Starts-->
		@include('front/include/megamenu')
		<!--MegaMenu Ends-->

		<!--Annoucement-->
		<div class="anmnt">
			<marquee>{{App\Http\Controllers\User\AnnouncementController::index()}}</marquee>
		</div>
		<!--Annoucement-->

		@yield('content')

		<!--Footer-->
		@include('front/include/footer')
		<!--Footer-->
	</div>
</div>

<script type="text/javascript">
    function enterpressalert(e, textarea){

        var code = (e.keyCode ? e.keyCode : e.which);
        if(code == 13) { //Enter keycode
            login();
        }
    }
</script>

@yield('bottom_js')
</body>
</html>