<!-- head section-->
@include('front/include/head')
<!-- head section-->
<style>

.closeBtn2 {
    cursor: pointer;
    position: absolute;
    right: 0;
    z-index: 10000;
}

#minimize1{
position: fixed;
z-index: 9999;
bottom: 110px;
left: 3px;
width: 180px;
height: 180px;
}

block1.b{
display:none;
}
</style>
<body>




	<div class="container">
		<div class="header">
			<!-- navigation header starts-->	
			@include('front/include/navigation')	
			<!-- navigation header ends-->

			<!--MegaMenu Starts-->

			@include('front/include/megamenu')
			<!--MegaMenu Ends-->

			<!--Annoucement-->
			<!--<div class="anmnt">
				<marquee>{{App\Http\Controllers\User\AnnouncementController::index()}}</marquee>
			</div>-->
			<!--Annoucement-->
                        @yield('sport_menu')
			@yield('content')

			<!--Footer-->
			@include('front/include/footer')
			<!--Footer-->
		</div>
	</div>
@yield('bottom_js')
@if( Session::get('currency') == 'MYR')
	<!--<block1 id="minimize1" class="a">
	<div class="closeBtn2" onclick="javascript:hideminimize1()">
	<img onmouseout="this.src='{{url()}}/front/img/closeBtn.png'" onmouseover="this.src='{{url()}}/front/img/closeBtn_hover.png'" src="{{url()}}/front/img/closeBtn.png">
	</div>
	<div id="navspin">
	<a onclick="@if (Auth::user()->check())window.open('{{route('omt')}}','spin','width=1330,height=650,toolbar=0,resizable=0,scrollbars=0');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif" href="#">
	<div style="position:fixed;z-index:9999;bottom:100px;left:3px;background-image:url({{url()}}/front/img/lucky_en.png);background-size:contain;;width:180px; height:180px;"> </div>
	</a>
	</div>
	</block1>-->
@endif

@if (Session::get('currency') == 'MYR')
	<!-- Start of LiveChat (www.livechatinc.com) code -->
	<script type="text/javascript">
        window.__lc = window.__lc || {};
        window.__lc.license = 3155342;
        (function() {
            var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
            lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
        })();
	</script>
	<!-- End of LiveChat code -->
@endif

@if ((Session::get('currency') == 'MYR'))
<div id="minigameNotify" style="display: none; position: fixed; bottom: 0; z-index: 10000;">
    <a href="{{route('minigame')}}" target="_blank"><img src="{{url()}}/front/resources/img/free-token.gif"></a>
</div>
@endif

</body>

<script type="text/javascript">
    $(function () {
@if (Auth::user()->check())
			load_message();
                    minigame();
@endif
        $('marquee').mouseover(function() {
            this.stop();
        }).mouseout(function() {
            this.start();
        });
    });

    function enterpressalert(e, textarea){

        var code = (e.keyCode ? e.keyCode : e.which);
        if(code == 13) { //Enter keycode
            login();
        }
    }

    var sDate = new Date("{{date('c')}}");
    var weekstr = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
    var monthstr = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

    function updateTime() {
        sDate = new Date(sDate.getTime() + 1000);

        var hour = parseFloat(sDate.getHours());
        var min = parseFloat(sDate.getMinutes());
        var sec = parseFloat(sDate.getSeconds());

        if (hour < 10)
            hour = "0" + hour;

        if (min < 10)
            min = "0" + min;

        if (sec < 10)
            sec = "0" + sec;

        $("#sys_date").html(sDate.getDate() + " " + monthstr[sDate.getMonth()] + " " + sDate.getFullYear() + ", " + weekstr[sDate.getDay()] + " " + hour + ":" + min + ":" + sec + " (GMT+8)");
    }
	
	function inbox() {
		window.open('{{route('inbox')	}}', '', 'width=800,height=600');
    }
@if (Auth::user()->check())
	function load_message() {
			$.ajax({
				url: "{{ route('showTotalUnseenMessage') }}",
				type: "GET",
				dataType: "text",
				data: {
				},
				success: function (result) {
					if(result>0){
						$("#totalMessage").html("<b style='color: red'>["+result+"]</b>");
						$("#totalMessage2").html("<b style='color: red'>["+result+"]</b>");
					}else{
						$("#totalMessage").html("["+result+"]");
					}
				},
				error: function (XMLHttpRequest, textStatus, errorThrown) {
				}
			});
		}
            
        function minigame(){
            $.ajax({
                    url: "{{ route('minigame-status') }}",
                    type: "GET",
                    dataType: "text",
                    data: {
                    },
                    success: function (result) {
                        console.log(result);
                            if(result == 1){
                                 $("#minigameNotify").slideDown(2000);
                            }else{
                                $("#minigameNotify").slideUp(2000);
                            }
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                    }
            });
        }
        setInterval(load_message,20000);
        setInterval(minigame,10000);
@endif

    setInterval(updateTime, 1000);
</script>

@if( Session::get('currency') == 'MYR')
<script>
function hideminimize1() {
document.getElementById('minimize1').setAttribute('class', 'b');
}
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function()
{ (i[r].q=i[r].q||[]).push(arguments)}

,i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-43720111-1', 'auto');
ga('send', 'pageview');
</script>
@elseif( Session::get('currency') == 'TWD')
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-43720111-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());
    gtag('config', 'UA-43720111-1');
</script>
@elseif( Session::get('currency') == 'VND')
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function()
{ (i[r].q=i[r].q||[]).push(arguments)}

,i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-43720111-1', 'auto');
ga('send', 'pageview');

</script>
@endif
</html>