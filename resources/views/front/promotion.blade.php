@extends('front/master')

@if (Session::get('currency') == 'TWD')
    @section('title', '無極限娛樂網')
@elseif (Session::get('currency') == 'MYR')
    @section('title', 'Best Online Casino Malaysia Promotions & Bonus')
    @section('keywords', 'Best Online Casino Malaysia Promotions & Bonus')
    @section('description', 'Every month at InfiniWin Live Online Casino Malaysia we have new promotion and bonus for you to enjoy. Don’t wait, Sign Up Now!')
@endif

@section('top_js')
 <style type="text/css">
	.tnc_list li { padding: 3px 0; }
	.my-button_std {
		background: none!important;
		border: 0px solid!important;
		cursor: pointer!important;
		padding: 0px!important;
	}
	.element_to_pop_up_std {
		background-color: #000;
		border-radius: 15px;
		border: 1px solid #C31818;
		color: #fff;
		display: none;
		padding: 20px;
		width: 800px;
		min-width: 400px;
		min-height: 180px;
		font-size: 12px;
	}
 </style>
 <link href="{{url()}}/front/resources/css/promo.css" rel="stylesheet">
 <script src="{{url()}}/front/resources/js/jquery.fancybox.js"></script>
	<script type="text/javascript">
		$(function () {
		    var compabilityClasses = ["my-button", "element_to_pop_up"];

		    for (var i = 0; i < compabilityClasses.length; i++) {
                $("." + compabilityClasses[i]).each(function () {
                    var obj = $(this);
                    var id = obj.attr("id");
                    var idIndex = id.replace(compabilityClasses[i], "");

                    if (parseFloat(idIndex) >= 10) {
                        obj.addClass(compabilityClasses[i] + "_std");
                    }
                });
			}
		});
	</script>
@stop

@section('content')
<!--Promotion-->
<div class="promoCont">
<table width="auto" border="0" cellspacing="0" cellpadding="0">
  	@foreach ($promo as $key => $value )
	@if ( ($key+1) %2 != 0)
	 <tr>
    @endif
	    <td>
			<div class="promoadj">
			<!-- Button that triggers the popup -->
			<button class="my-button" id="my-button{{$key+1}}"><img src="{{$value['image']}}" alt="" height="181" width="500" /></button>
			<table width="auto" border="0" cellspacing="0" cellpadding="0">
			  <tr>
				<td>
					<a href="{{route('register')}}">
						@if (Lang::getLocale() == 'tw')
							<img style="margin-left: 3px;" src="{{url()}}/front/img/tw/promo-join.jpg" width="138" height="29" alt=""/>
						@else
							<img style="margin-left: 3px;" src="{{url()}}/front/img/promo-join.jpg" width="138" height="29" alt=""/>
						@endif
					</a>
				</td>
			  </tr>
			</table>

				<div class="element_to_pop_up" id="element_to_pop_up{{$key+1}}">
					 <a class="b-close">x</a>
					  <h3>{{$value['title']}}</h3>
					 <?php echo htmlspecialchars_decode($value['content']); ?>
				</div>
			</div>
		</td>
	@if ( ($key+1) % 2 == 0)
	 </tr>
    @endif
	@endforeach
</table>
</div>
<!--Promotion-->

<!--TnC-->
<div class="promo" style="color: #fff;padding-top: 10px;">
	@include('front.include.promotion_tnc')
</div>
<!--TnC-->

<div class="clr"></div>
@if (Session::get('currency') == 'MYR')
<div class="seo_content">
    <h1 style="font-size:13px;">Great Rewards and Promotions at Infiniwin</h1>
    <p>Gambling is a great way for you to have fun. You can interact with other players and share stories or you can choose to lay back and play the slot games to have a peace of mind. Thanks to the technology, you can now play these casino games at the comfort of your house. Online casinos are aplenty in Malaysia but the best among all of them would be Infiniwin. Infiniwin has a really beautiful and responsive website as well as hundreds of casino games to play with.</p>
    <p>Another thing to like about Infiniwin is that it is very generous in terms of promotions and rewards. These two items are vital to increase as well as retain the number of players currently playing on their platform. The more players there are, the easier for Infiniwin to profit and thus improve the quality of their games.</p>
    <p>Perhaps the best thing about Infiniwin is the Fortune 4D promotion. Fortune 4D is a game where players will get tickets every week to roll the reels. The number of tickets that every player get is different, depending on their weekly turnover as well as the amount of deposit that they have made. If you are lucky, you will get free money of up to RM2,500!</p>
    <p>There is also Infiniwin’s daily deposit promotion. This promotion is basically the same as the new player deposit promotion that most online casinos have. The only difference is that you can get the extra deposit daily. As an example, if you deposited RM300 today, you will get an extra RM60 inside your deposit. Do take note that the cap for this daily deposit promotion is until the deposit reaches RM238.</p>
    <p>The rebates that Infiniwin provides to their loyal players are also quite high. You will get 0.5% rebate for live casino games and 0.3% rebate for slot games and sportsbook. Infiniwin also has a special weekly promotion rebate of another 0.6% rebate, snowballing the rebate to up to 1.1%. That is considered high in the gambling industry!</p>
</div>
@endif
@stop

@section('bottom_js')
<script>
@foreach ($promo as $key => $value )
    ;(function($) {

         // DOM Ready
        $(function() {

            // Binding a click event
            // From jQuery v.1.7.0 use .on() instead of .bind()
            $('#my-button{{ $key+1}}').bind('click', function(e) {

                // Prevents the default action to be triggered. 
                e.preventDefault();

                // Triggering bPopup when click event is fired
                $('#element_to_pop_up{{ $key+1}}').bPopup();

            });

        });

    })(jQuery);
@endforeach
   


/*================================================================================
 * @name: bPopup - if you can't get it up, use bPopup
 * @author: (c)Bjoern Klinggaard (twitter@bklinggaard)
 * @demo: http://dinbror.dk/bpopup
 * @version: 0.9.4.min
 ================================================================================*/
 (function(b){b.fn.bPopup=function(z,F){function K(){a.contentContainer=b(a.contentContainer||c);switch(a.content){case "iframe":var h=b('<iframe class="b-iframe" '+a.iframeAttr+"></iframe>");h.appendTo(a.contentContainer);r=c.outerHeight(!0);s=c.outerWidth(!0);A();h.attr("src",a.loadUrl);k(a.loadCallback);break;case "image":A();b("<img />").load(function(){k(a.loadCallback);G(b(this))}).attr("src",a.loadUrl).hide().appendTo(a.contentContainer);break;default:A(),b('<div class="b-ajax-wrapper"></div>').load(a.loadUrl,a.loadData,function(){k(a.loadCallback);G(b(this))}).hide().appendTo(a.contentContainer)}}function A(){a.modal&&b('<div class="b-modal '+e+'"></div>').css({backgroundColor:a.modalColor,position:"fixed",top:0,right:0,bottom:0,left:0,opacity:0,zIndex:a.zIndex+t}).appendTo(a.appendTo).fadeTo(a.speed,a.opacity);D();c.data("bPopup",a).data("id",e).css({left:"slideIn"==a.transition||"slideBack"==a.transition?"slideBack"==a.transition?g.scrollLeft()+u:-1*(v+s):l(!(!a.follow[0]&&m||f)),position:a.positionStyle||"absolute",top:"slideDown"==a.transition||"slideUp"==a.transition?"slideUp"==a.transition?g.scrollTop()+w:x+-1*r:n(!(!a.follow[1]&&p||f)),"z-index":a.zIndex+t+1}).each(function(){a.appending&&b(this).appendTo(a.appendTo)});H(!0)}function q(){a.modal&&b(".b-modal."+c.data("id")).fadeTo(a.speed,0,function(){b(this).remove()});a.scrollBar||b("html").css("overflow","auto");b(".b-modal."+e).unbind("click");g.unbind("keydown."+e);d.unbind("."+e).data("bPopup",0<d.data("bPopup")-1?d.data("bPopup")-1:null);c.undelegate(".bClose, ."+a.closeClass,"click."+e,q).data("bPopup",null);H();return!1}function G(h){var b=h.width(),e=h.height(),d={};a.contentContainer.css({height:e,width:b});e>=c.height()&&(d.height=c.height());b>=c.width()&&(d.width=c.width());r=c.outerHeight(!0);s=c.outerWidth(!0);D();a.contentContainer.css({height:"auto",width:"auto"});d.left=l(!(!a.follow[0]&&m||f));d.top=n(!(!a.follow[1]&&p||f));c.animate(d,250,function(){h.show();B=E()})}function L(){d.data("bPopup",t);c.delegate(".bClose, ."+a.closeClass,"click."+e,q);a.modalClose&&b(".b-modal."+e).css("cursor","pointer").bind("click",q);M||!a.follow[0]&&!a.follow[1]||d.bind("scroll."+e,function(){B&&c.dequeue().animate({left:a.follow[0]?l(!f):"auto",top:a.follow[1]?n(!f):"auto"},a.followSpeed,a.followEasing)}).bind("resize."+e,function(){w=y.innerHeight||d.height();u=y.innerWidth||d.width();if(B=E())clearTimeout(I),I=setTimeout(function(){D();c.dequeue().each(function(){f?b(this).css({left:v,top:x}):b(this).animate({left:a.follow[0]?l(!0):"auto",top:a.follow[1]?n(!0):"auto"},a.followSpeed,a.followEasing)})},50)});a.escClose&&g.bind("keydown."+e,function(a){27==a.which&&q()})}function H(b){function d(e){c.css({display:"block",opacity:1}).animate(e,a.speed,a.easing,function(){J(b)})}switch(b?a.transition:a.transitionClose||a.transition){case "slideIn":d({left:b?l(!(!a.follow[0]&&m||f)):g.scrollLeft()-(s||c.outerWidth(!0))-C});break;case "slideBack":d({left:b?l(!(!a.follow[0]&&m||f)):g.scrollLeft()+u+C});break;case "slideDown":d({top:b?n(!(!a.follow[1]&&p||f)):g.scrollTop()-(r||c.outerHeight(!0))-C});break;case "slideUp":d({top:b?n(!(!a.follow[1]&&p||f)):g.scrollTop()+w+C});break;default:c.stop().fadeTo(a.speed,b?1:0,function(){J(b)})}}function J(b){b?(L(),k(F),a.autoClose&&setTimeout(q,a.autoClose)):(c.hide(),k(a.onClose),a.loadUrl&&(a.contentContainer.empty(),c.css({height:"auto",width:"auto"})))}function l(a){return a?v+g.scrollLeft():v}function n(a){return a?x+g.scrollTop():x}function k(a){b.isFunction(a)&&a.call(c)}function D(){x=p?a.position[1]:Math.max(0,(w-c.outerHeight(!0))/2-a.amsl);v=m?a.position[0]:(u-c.outerWidth(!0))/2;B=E()}function E(){return w>c.outerHeight(!0)&&u>c.outerWidth(!0)}b.isFunction(z)&&(F=z,z=null);var a=b.extend({},b.fn.bPopup.defaults,z);a.scrollBar||b("html").css("overflow","hidden");var c=this,g=b(document),y=window,d=b(y),w=y.innerHeight||d.height(),u=y.innerWidth||d.width(),M=/OS 6(_\d)+/i.test(navigator.userAgent),C=200,t=0,e,B,p,m,f,x,v,r,s,I;c.close=function(){a=this.data("bPopup");e="__b-popup"+d.data("bPopup")+"__";q()};return c.each(function(){b(this).data("bPopup")||(k(a.onOpen),t=(d.data("bPopup")||0)+1,e="__b-popup"+t+"__",p="auto"!==a.position[1],m="auto"!==a.position[0],f="fixed"===a.positionStyle,r=c.outerHeight(!0),s=c.outerWidth(!0),a.loadUrl?K():A())})};b.fn.bPopup.defaults={amsl:50,appending:!0,appendTo:"body",autoClose:!1,closeClass:"b-close",content:"ajax",contentContainer:!1,easing:"swing",escClose:!0,follow:[!0,!0],followEasing:"swing",followSpeed:500,iframeAttr:'scrolling="no" frameborder="0"',loadCallback:!1,loadData:!1,loadUrl:!1,modal:!0,modalClose:!0,modalColor:"#000",onClose:!1,onOpen:!1,opacity:0.7,position:["auto","auto"],positionStyle:"absolute",scrollBar:!0,speed:250,transition:"fadeIn",transitionClose:!1,zIndex:9997}})(jQuery);
</script>
@stop