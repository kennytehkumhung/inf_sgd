<html>
<head>
 <link href="{{url()}}/front/resources/css/slot.css" rel="stylesheet">
 <link href="{{url()}}/front/resources/css/style_2.css" rel='stylesheet' type='text/css'>

<!-- Home slider style -->
<link rel="stylesheet" href="{{url()}}/front/resources/css/style_3.css">
</head>
<body>
	<div class="slot_menu">
		<ul>
            <li><a href="{{route('uc8', [ 'type' => 'slot' , 'category' => 'all' ] )}}">{{ Lang::get('public.Slots') }} (68)</a></li>
		</ul>
	</div>
	<div id="slot_lobby">		
		@foreach( $lists as $list )
        <div class="slot_box">
           <a href="javascript:void(0)" onclick="@if (Auth::user()->check())window.open('{{route('uc8-slot', array('gamecode' => $list['code']))}}','uc8_slot')@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif">
               <img src="{{ $list['image'] }}" width="150" height="150" alt=""/>
           </a>
           <span>{{ $list['name'] }}</span>
        </div>
		@endforeach
		<div class="clr"></div>
	</div>
    <div class="clr"></div> 
<!--Slot-->
</body>
</html>
