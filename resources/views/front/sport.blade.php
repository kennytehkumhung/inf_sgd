@extends('front/master')

@if (Session::get('currency') == 'TWD')
    @section('title', '無極限娛樂網')
@elseif (Session::get('currency') == 'MYR')
    @section('title', 'Best Online Live Casino Malaysia - Terms & Conditions')
    @section('keywords', 'Best Online Live Casino Malaysia - Terms & Conditions')
    @section('description', 'Find our more about InfiniWin Online Live Casino Malaysia Terms and Conditions.')
@endif

@section('top_js')
	<style type="text/css">
		body {
			@if (Session::get('currency') == 'TWD')
				background-image: url('{{ asset('/front/img/tw/sp_bg.jpg') }}') !important;
			@endif
			background-repeat: no-repeat;
			background-position: top center; background-color: #000000;
		}
	</style>
@endsection

@section('content')
<br>

<div style="width: 100%;"><img alt="" src="{{ asset('/front/img/tw/bannersp.png') }}" style="width: 100%;" /></div>

<table border="0" cellpadding="0" cellspacing="20" style="width: 100%;">
	<tr>
		<td style="width: 50%;">
			<a href="{{ route('m8b') }}" target="_blank">
				<img alt="" src="{{ asset('/front/img/tw/mb_s.png') }}" style="width: 100%;" />
			</a>
		</td>
		<td>
			<a href="{{ route('wft') }}" target="_blank">
				<img alt="" src="{{ asset('/front/img/tw/wf_s.png') }}" style="width: 100%;" />
			</a>
		</td>
	</tr>
</table>

@endsection
