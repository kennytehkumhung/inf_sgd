@extends('front/master')

@section('title', 'Transaction Enquiry')

@section('top_js')
  <link href="{{url()}}/front/resources/css/acct_management.css" rel="stylesheet">
  <link href="{{url()}}/front/resources/css/table.css" rel="stylesheet">
  <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
@stop

@section('content')

@include('front/transaction_mange_top', [ 'title' => Lang::get('public.TransactionHistory') ] )
<div class="acctContent">
    <div class="depLeft">
		<div class="cont">
			<!--MID SECTION-->
				<?php echo htmlspecialchars_decode($content); ?>
			<!--MID SECTION-->
		</div>
	</div>
	<div class="depRight">
		<span class="wallet_title">{{Lang::get('public.TransactionHistory')}}</span>
		<div class="acctRow">
			<div class="acctRow">
				<label>{{Lang::get('public.DateFrom')}} :</label>
				<input type="text" class="datepicker" id="date_from" style="cursor:pointer;" value="{{date('Y-m-d')}}"><br>
			</div>
			<div class="acctRow">
				<label>{{Lang::get('public.DateTo')}}:</label>
				<input type="text" class="datepicker" id="date_to" style="cursor:pointer;" value="{{date('Y-m-d')}}">
			</div>

			<div class="acctRow">
				<div class="submitAcct">
					<a id="trans_history_button" href="#" onClick="transaction_history()"> {{Lang::get('public.Submit')}}</a>
					<br><br>
				</div>
			</div>
		</div>
		<div class="clr"></div>
		<div class="table" style="display:block;">
			<table width="100%" id="trans_history">
				<tr>
					<td>{{Lang::get('public.DateOrTime')}}</td>
					<td>{{Lang::get('COMMON.WAGER')}}</td>
					<td>{{Lang::get('COMMON.STAKE')}} </td>
					<td>{{Lang::get('public.WinLoss')}}</td>
					<td>{{Lang::get('COMMON.VALIDSTAKE')}}</td>
				</tr>
				<tr>
					<td colspan="6"><h2><center>No Data Available!</center></h2></td>
				</tr>
			</table>
			<Br><br>
			<table width="100%" id="trans_history_product">
				<tr>
					<td>{{Lang::get('COMMON.PRODUCT')}}</td>
					<td>{{Lang::get('COMMON.WAGER')}}</td>
					<td>{{Lang::get('COMMON.STAKE')}} </td>
					<td>{{Lang::get('public.WinLoss')}}</td>
					<td>{{Lang::get('COMMON.VALIDSTAKE')}}</td>
				</tr>
				<tr>
					<td colspan="6"><h2><center>No Data Available!</center></h2></td>
				</tr>
			</table>	
			<Br><br>
	
		</div>
	</div>
</div>
</div>
</div>

@stop

@section('bottom_js')
<script src="{{url()}}/front/resources/js/slick.min.js"></script>
<script>
transaction_history();
$('.caro').slick({
  infinite: true,
  slidesToShow: 2,
  slidesToScroll: 2,
  autoplay: true,
  variableWidth: true,
  arrows: false
});

$(function() {
	$( ".datepicker" ).datepicker({ dateFormat: 'yy-mm-dd'  , defaultDate: new Date() });
});
  
function transaction_history(){
$.ajax({
	type: "POST",
	url: "{{action('User\TransactionController@profitlossProcess')}}",
	data: {
		_token:   "{{ csrf_token() }}",
		date_from:		$('#date_from').val(),
		date_to:    	$('#date_to').val(),
		record_type:    $('#record_type').val()

	},
}).done(function( json ) {

			//var str;
			 var str = '';
			str += '	<tr ><td>Date</td><td>Wager</td><td>Stake</td><td>Win/Lose</td><td>Real Bet</td></tr>';
			
			 obj = JSON.parse(json);
			//alert(obj);
			 $.each(obj, function(i, item) {
				str +=  '<tr><td>'+item.date+'</td><td>'+item.wager+'</td><td>'+item.stake+'</td><td>'+item.winloss+'</td><td>'+item.validstake+'</td></tr>';
				
			})
				
			//alert(json);
			$('#trans_history').html(str);
			
	});
}

function transaction_history_product(date_from,product){
$.ajax({
	type: "POST",
	url: "{{action('User\TransactionController@profitlossProcess')}}",
	data: {
		_token:   "{{ csrf_token() }}",
		date_from:		date_from,
		group_by:		product,

	},
}).done(function( json ) {

			//var str;
			 var str = '';
			str += '	<tr ><td>Product</td><td>Wager</td><td>Stake</td><td>Win/Lose</td><td>Real Bet</td></tr>';
			
			 obj = JSON.parse(json);
			//alert(obj);
			 $.each(obj, function(i, item) {
				str +=  '<tr><td>'+item.product+'</td><td>'+item.wager+'</td><td>'+item.stake+'</td><td>'+item.winloss+'</td><td>'+item.validstake+'</td></tr>';
				
			})
				
			//alert(json);
			$('#trans_history_product').html(str);
			
	});
}


setInterval(updateTrans, 10000);
setInterval(update_mainwallet, 10000);

function updateTrans(){
	$( "#trans_history_button" ).trigger( "click" );
}

</script>
@stop