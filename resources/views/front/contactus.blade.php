@extends('front/master')

@if (Session::get('currency') == 'TWD')
    @section('title', '無極限娛樂網')
@elseif (Session::get('currency') == 'MYR')
    @section('title', 'Best Online Live Casino Malaysia - Contact Us')
    @section('keywords', 'Best Online Live Casino Malaysia - Contact Us')
    @section('description', 'Contact InfiniWin Online Casino Malaysia\'s customer service team here.')
@endif

@section('top_js')

@stop

@section('content')
<!--MID SECTION-->
<?php echo htmlspecialchars_decode($content); ?>
<!--MID SECTION-->
@stop

@section('bottom_js')

@stop