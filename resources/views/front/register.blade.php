@extends('front/master')

@if (Session::get('currency') == 'TWD')
    @section('title', '無極限娛樂網')
@elseif (Session::get('currency') == 'MYR')
    @section('title', 'FREE Best Online Live Casino Malaysia Registration')
    @section('keywords', 'FREE Best Online Live Casino Malaysia Registration')
    @section('description', 'InfiniWin Online Live Casino Malaysia gives you Free Spins when you sign up NOW. Play the best online casino games – slots, roulette, video poker and more.')
@endif

@section('top_js')

@if (Session::get('currency') == 'TWD')
	<style type="text/css">
		.acctRow label { margin-left: 275px; }
		.acctTextReg { width: 250px; }

		.acctContentReg *::-webkit-input-placeholder { /* WebKit, Blink, Edge */
			color:    #8C8C8C;
		}
		.acctContentReg *:-moz-placeholder { /* Mozilla Firefox 4 to 18 */
			color:    #8C8C8C;
			opacity:  1;
		}
		.acctContentReg *::-moz-placeholder { /* Mozilla Firefox 19+ */
			color:    #8C8C8C;
			opacity:  1;
		}
		.acctContentReg *:-ms-input-placeholder { /* Internet Explorer 10-11 */
			color:    #8C8C8C;
		}
		.acctContentReg *::-ms-input-placeholder { /* Microsoft Edge */
			color:    #8C8C8C;
		}

		.acctContentReg input { color: #000 !important; }

		@if (Session::get('currency') == 'TWD')
            body {
				background-image: url('{{ asset('/front/img/tw/lv_bg.jpg') }}') !important;
		}
		@endif
	</style>
@endif

<script>
  function register_submit(){
	$.ajax({
		type: "POST",
		url: "{{route('register_process')}}",
		data: {
			_token: "{{ csrf_token() }}",
			username:		 $('#r_username').val(),
			password: 		 $('#r_password').val(),
			repeatpassword:  $('#r_repeatpassword').val(),
			code:    		 $('#r_code').val(),
			email:    		 $('#r_email').val(),
			mobile:    		 $('#r_mobile').val(),
			fullname:        $('#r_fullname').val(),
			dob:			 $('#dob').val(),
			wechat:			 $('#r_wechat').val(),
			line:			 $('#r_line').val(),
            referralid:		 $('#r_referralid').val(),
            mobile_vcode:	 $("#r_mobile_vcode").val()
		},
	}).done(function( json ) {
			 $('.acctTextReg').html('');
			 obj = JSON.parse(json);
			 var str = '';
			 $.each(obj, function(i, item) {
				if( '{{Lang::get('COMMON.SUCESSFUL')}}' == item ){
					alert('{{Lang::get('public.NewMember')}}');
					window.location.href = "{{route('homepage')}}";
				}else{
					$('.'+i+'_acctTextReg').html(item);
					//str += item + '\n';
				}
			})
			
			//alert(str);
			
			
	});
}

  var countDownSecond = 0;
  var intervalObj = null;
  var btnObj = $("#btn_sms");

  function sendVerifySms() {
      if (intervalObj != null) {
          alert("{{ Lang::get('public.PleaseWaitForXSecondsToResendSMS') }}".replace(":p1", countDownSecond));
          return false;
      }

      var mobileObj = $("#r_mobile");
      var mobileNum = mobileObj.val();

      if (mobileNum.length < 1) {
          alert("{{ Lang::get('public.PleaseEnterContactNumberToContinue') }}");
          mobileObj.focus();

          return false;
      }

      if (confirm("{{ Lang::get('public.SendVerificationCodeViaSMSToThisNumber') }} " + mobileNum)) {
          countDownSecond = 60;
          smsResendCountDown();

          $.ajax({
              type: "POST",
			  dataType: "json",
              url: "{{url('smsregcode')}}",
              data: {
                  _token:     "{{ csrf_token() }}",
				  language:   "{{ Lang::getLocale() }}",
                  tel_mobile:	mobileNum
              },
              success: function (result) {
                  if (result.code != 0) {
                      alert(result.msg);
				  }

				  if (result.code == 2) {
                      countDownSecond = 10;
				  }
              },
              error: function (XMLHttpRequest, textStatus, errorThrown) {
//                    console.log('ok failed');
              }
          });
      }
  }

  function smsResendCountDown() {
      if (intervalObj == null) {
          intervalObj = setInterval(smsResendCountDown, 1000)
      }

      if (countDownSecond <= 1) {
          btnObj.text("Send SMS");
          clearInterval(intervalObj);
          intervalObj = null;
          return;
      }

      btnObj.text("Resend (" + --countDownSecond + ")");
  }
</script>
@stop

@section('content')
<div class="acctContentReg">
      <span class="wallet_title"> {{Lang::get('public.Registration')}}</span>

	  @if (Session::get('currency') == 'TWD')
		  <div class="acctRow">
		  	<label>{{Lang::get('public.Username')}}* :</label><input type="text" id="r_username" placeholder="不能小於6個英，数组合"><span class="username_acctTextReg acctTextReg"></span>
		  	<div class="clr"></div>
		  </div>
		  <div class="acctRow">
		  	<label>{{Lang::get('public.Password')}}* :</label><input type="password" id="r_password" placeholder="不能小於6個英，数组合"><span class="password_acctTextReg acctTextReg"></span>
		  	<div class="clr"></div>
		  </div>
		  <div class="acctRow">
		  	<label>{{Lang::get('public.RepeatPassword')}}* :</label><input type="password" id="r_repeatpassword">
		  	<div class="clr"></div>
		  </div>
		  <div class="acctRow">
		  	<label>{{Lang::get('public.EmailAddress')}}* :</label><input type="text" id="r_email" placeholder="example@email.com"><span class="email_acctTextReg acctTextReg"></span>
		  	<div class="clr"></div>
		  </div>
		  <div class="acctRow">
		  	<label>{{Lang::get('public.ContactNo')}}* :</label><input type="text" id="r_mobile" placeholder="09xxxxxxxx"><span class="mobile_acctTextReg acctTextReg"></span>
		  	<div class="clr"></div>
		  </div>
		  <div class="acctRow">
			  <label>{{Lang::get('public.MobileVerificationCode')}}* :</label><input type="text" id="r_mobile_vcode">
			  <a href="javascript:void(0);" id="btn_sms" style="line-height: 27px; padding: 9px; margin-left: 5px; background-color: #C59F4D; text-decoration: none;" onclick="sendVerifySms();">{{ Lang::get('public.ClickHereToGetVerificationCode') }}</a>
			  <br>
			  <span class="mobile_vcode_acctTextReg acctTextReg" style="margin-left: 455px;"></span>
			  <div class="clr"></div>
		  </div>
		  <div class="acctRow">
			  <label>* {{Lang::get('public.RequiredFields')}}</label>
			  <div class="clr"></div>
		  </div>

		  <input type="hidden" id="r_referralid" value="">
		  <input type="hidden" id="r_line" value="">
		  <input type="hidden" id="r_wechat" value="">
		  <input type="hidden" id="r_fullname" value="">
		  <input type="hidden" id="dob" value="1987-01-01">
		  <input type="hidden" id="r_code" value="{{ Session::get('register_captcha') }}">

		  <div class="submitAcct" style="margin-left: 580px;">
		  	<a href="#" onClick="register_submit()" > {{Lang::get('public.Submit')}}</a>
		  </div>
	  @else
		  <div class="acctRow">
		  	<label>{{Lang::get('public.Username')}}* :</label><input type="text" id="r_username"><span class="username_acctTextReg acctTextReg"></span>
		  	<div class="clr"></div>
		  </div>
		  <div class="acctRow">
		  	<label>{{Lang::get('public.Password')}}* :</label><input type="password" id="r_password"><span class="password_acctTextReg acctTextReg"></span>
		  	<div class="clr"></div>
		  </div>
		  <div class="acctRow">
		  	<label>{{Lang::get('public.RepeatPassword')}}* :</label><input type="password" id="r_repeatpassword">
		  	<div class="clr"></div>
		  </div>
		  <div class="acctRow">
		  	<label>{{Lang::get('public.EmailAddress')}}* :</label><input type="text" id="r_email"><span class="email_acctTextReg acctTextReg"></span>
		  	<div class="clr"></div>
		  </div>
		  <div class="acctRow">
		  	<label>{{Lang::get('public.ContactNo')}}* :</label><input type="text" id="r_mobile"><span class="mobile_acctTextReg acctTextReg"></span>
		  	<div class="clr"></div>
		  </div>
		  <div class="acctRow">
			  <label>{{Lang::get('public.FullName')}} :</label><input type="text" id="r_fullname"><span class="fullname_acctTextReg acctTextReg"></span>
			  <div class="clr"></div>
		  </div>
		  <div class="acctRow">
			  <label>{{Lang::get('public.DOB')}} :</label>
			  <select style="margin-right: 8px;" id="dob">
				<option value="01">01</option>
				<option value="02">02</option>
				<option value="03">03</option>
				<option value="04">04</option>
				<option value="05">05</option>
				<option value="06">06</option>
				<option value="07">07</option>
				<option value="08">08</option>
				<option value="09">09</option>
				<option value="10">10</option>
				<option value="11">11</option>
				<option value="12">12</option>
				<option value="13">13</option>
				<option value="14">14</option>
				<option value="15">15</option>
				<option value="16">16</option>
				<option value="17">17</option>
				<option value="18">18</option>
				<option value="19">19</option>
				<option value="20">20</option>
				<option value="21">21</option>
				<option value="22">22</option>
				<option value="23">23</option>
				<option value="24">24</option>
				<option value="25">25</option>
				<option value="26">26</option>
				<option value="27">27</option>
				<option value="28">28</option>
				<option value="29">29</option>
				<option value="30">30</option>
				<option value="31">31</option>
			  </select>
			  <select style="margin-right: 8px;">
				<option value="01">01</option>
				<option value="02">02</option>
				<option value="03">03</option>
				<option value="04">04</option>
				<option value="05">05</option>
				<option value="06">06</option>
				<option value="07">07</option>
				<option value="08">08</option>
				<option value="09">09</option>
				<option value="10">10</option>
				<option value="11">11</option>
				<option value="12">12</option>
			  </select>
			  <select>
				<option value="1995">1995</option>
				<option value="1994">1994</option>
				<option value="1993">1993</option>
				<option value="1992">1992</option>
				<option value="1991">1991</option>
				<option value="1990">1990</option>
				<option value="1989">1989</option>
				<option value="1988">1988</option>
				<option value="1987">1987</option>
				<option value="1986">1986</option>
				<option value="1985">1985</option>
				<option value="1984">1984</option>
				<option value="1983">1983</option>
				<option value="1982">1982</option>
				<option value="1981">1981</option>
				<option value="1980">1980</option>
				<option value="1979">1979</option>
				<option value="1978">1978</option>
				<option value="1977">1977</option>
				<option value="1976">1976</option>
				<option value="1975">1975</option>
				<option value="1974">1974</option>
				<option value="1973">1973</option>
				<option value="1972">1972</option>
				<option value="1971">1971</option>
				<option value="1970">1970</option>
				<option value="1969">1969</option>
				<option value="1968">1968</option>
				<option value="1967">1967</option>
				<option value="1966">1966</option>
				<option value="1965">1965</option>
				<option value="1964">1964</option>
				<option value="1963">1963</option>
				<option value="1962">1962</option>
				<option value="1961">1961</option>
				<option value="1960">1960</option>

			  </select>
			  <div class="clr"></div>
		  </div>

		  <div class="acctRow">
			  <label> {{Lang::get('public.Code')}} :</label><input type="text" id="r_code"> <img style="float: left;margin-left:6px;"  src="{{route('captcha', ['type' => 'register_captcha'])}}" width="60" height="27" /><span class="code_acctTextReg acctTextReg"></span>
			  <div class="clr"></div>
		  </div>

		  <input type="hidden" id="r_line" value="">
		  <input type="hidden" id="r_wechat" value="">
		  <input type="hidden" id="r_referralid" value="">
		  <input type="hidden" id="r_mobile_vcode" value="">

		  <div class="submitAcct">
		  	<a href="#" onClick="register_submit()" > {{Lang::get('public.Submit')}}</a>
		  </div>
	  @endif
</div>

<div class="clr"></div>
@if (Session::get('currency') == 'MYR')
<div class="seo_content">
    <h1 style="font-size:13px;">How to Register an Infiniwin Account</h1>
    <p>If you are living in the USA, you are lucky to be there. There are no place like Las Vegas, The Gambling Capital of The World, especially in Malaysia. However, Infiniwin is quickly gaining reputation to be The Online Gambling Capital in Malaysia. Infiniwin does almost everything right, from the huge list of casino games to the efficient and hardworking customer service.</p>
    <p>You cannot start playing in Infiniwin without an account. Therefore, head to the registration page to start the procedure. It is very easy to create an Infiniwin account, you just need to fill in the following details:</p>
    <ul style='list-style: none;'>
        <li>1. Username</li>
        <li>2. Password</li>
        <li>3. E-mail</li>
        <li>4. Contact Number</li>
        <li>5. Full Name</li>
        <li>6. Date of Birth</li>
    </ul>
    <p>After you have filled in those details, ensure that all of them are correct before you key in the authentication code and click the submit button. These details are important in order to verify and validate your deposits and withdrawal. Therefore, if you notice that you filled in wrongly, contact the customer service quickly. They will be able to offer help and guide you to the process.</p>
    <p>If your registration is successful and everything checks out, you will get an email not long after to confirm your username and password. Once you get this, you will then be able to login into Infiniwin with your username and password. You will then be allowed to deposit money in order to play the Infiniwin casino games.</p>
    <p>Since this is a new account, you must take advantage of the 100% welcome bonus. This welcome bonus is only applicable to your first deposit so make sure to deposit as much as you can to maximize the bonus. If you are low on funds, do not worry. There is also the 20% daily deposit bonus that you can utilize. These deposit bonuses are what makes Infiniwin different to other online casinos in Malaysia.</p>
</div>
@endif
@stop

@section('bottom_js')
@stop