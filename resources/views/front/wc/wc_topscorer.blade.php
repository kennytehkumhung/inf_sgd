<!DOCTYPE html>
<html lang="en">
@include('front/wc/wc_header' )

<script type="text/javascript" language="javascript">
    function submit_winner() {
        var list = [];
        $(':checkbox:checked').each(function(i){
          list[i] = $(this).val();
        });
        if(list.length > 6){
            alert("Maximum 6 box can be selected! Thanks!");
        }
        else if(list.length == 0){
            window.location.href = "{{route('wcgroupwinner')}}";   
        }
        else{
            $.ajax({
                type: "POST",
                url: "{{route('wctopscorer-process')}}",
                data: {
                       _token: "{{ csrf_token() }}",
                       username: "{{ Session::get('username') }}",
                       team: list
                },
                beforeSend: function(){
                       $('#next_button').attr('onClick','');
                       $('#next_button').html('Loading...');
                },
                success: function(json){
                    obj = JSON.parse(json);
                        var str = '';
                        $.each(obj, function(i, item) {
                            if(item == '{{Lang::get('COMMON.SUCESSFUL')}}'){
                                alert('We will process to next section.');
                                window.location.href = "{{route('wcgroupwinner')}}";                                
                            }
                            else{
                                alert('[ERROR] Please refer your Token Balance.');
                                $('#next_button').attr('onClick','submit_winner()');
                                $('#next_button').html('Next');
                            }

                        })
                    }
               })
        }
    }
    
    function clear_checkbox(){
        $('input:checkbox').removeAttr('checked');
    }
</script>

<div class="row">
<div class="col-lg-6">
<div class="adj01">
<img src="{{ URL('/') }}/front/wc/img/title-7.png">
<span>**Spending limit of 6 Tokens</span>
</div>
</div>
<div class="col-lg-3"></div>
<div class="col-lg-3">
<div class="tokenWrap">
<img src="{{ URL('/') }}/front/wc/img/token-img.png">
<div class="tokenCont">
<span class="tokTit">LIMIT TOKEN LEFT</span><span class="tokDig">{{$token}}</span>
</div>
<div class="backBtn" style="display: block; position: absolute;right: 50px;top: 70px;"><a href="{{route('wchighgoal')}}"><img src="{{ URL('/') }}/front/wc/img/back-btn.png"></a></div>
</div>
</div>
</div>

<div class="row">
<div class="col-lg-12">
<div class="opt1">
<div class="row">
<div class="col-lg-3 bordRight">
<div class="form-check form-check-inline adj03">
<input class="form-check-input" type="checkbox" name="inlineRadioOptions" id="inlineRadio1" value="1">
<label class="form-check-label" for="inlineRadio1">
<span class="adj02">Lionel Messi</span>
<span><img src="{{ URL('/') }}/front/wc/img/argentina-flag-small.png"></span>
</label>
</div>
<div class="form-check form-check-inline adj03">
<input class="form-check-input" type="checkbox" name="inlineRadioOptions" id="inlineRadio2" value="2">
<label class="form-check-label" for="inlineRadio2">
<span class="adj02">Gonzalo Higuain</span>
<span><img src="{{ URL('/') }}/front/wc/img/argentina-flag-small.png"></span>
</label>
</div>
<div class="form-check form-check-inline adj03">
<input class="form-check-input" type="checkbox" name="inlineRadioOptions" id="inlineRadio3" value="3">
<label class="form-check-label" for="inlineRadio3">
<span class="adj02">Paulo Dybala</span>
<span><img src="{{ URL('/') }}/front/wc/img/argentina-flag-small.png"></span>
</label>
</div>
<div class="form-check form-check-inline adj03">
<input class="form-check-input" type="checkbox" name="inlineRadioOptions" id="inlineRadio4" value="4">
<label class="form-check-label" for="inlineRadio4">
<span class="adj02">Sergio Aguero</span>
<span><img src="{{ URL('/') }}/front/wc/img/argentina-flag-small.png"></span>
</label>
</div>
<div class="form-check form-check-inline adj03">
<input class="form-check-input" type="checkbox" name="inlineRadioOptions" id="inlineRadio5" value="5">
<label class="form-check-label" for="inlineRadio5">
<span class="adj02">James Rodriguez</span>
<span><img src="{{ URL('/') }}/front/wc/img/colombia-flag-small.png"></span>
</label>
</div>
<div class="form-check form-check-inline adj03">
<input class="form-check-input" type="checkbox" name="inlineRadioOptions" id="inlineRadio6" value="6">
<label class="form-check-label" for="inlineRadio6">
<span class="adj02">Radamel Falcao</span>
<span><img src="{{ URL('/') }}/front/wc/img/colombia-flag-small.png"></span>
</label>
</div>
<div class="form-check form-check-inline adj03">
<input class="form-check-input" type="checkbox" name="inlineRadioOptions" id="inlineRadio7" value="7">
<label class="form-check-label" for="inlineRadio7">
<span class="adj02">Jamie Vardy</span>
<span><img src="{{ URL('/') }}/front/wc/img/england-flag-small.png"></span>
</label>
</div>
<div class="form-check form-check-inline adj03">
<input class="form-check-input" type="checkbox" name="inlineRadioOptions" id="inlineRadio8" value="8">
<label class="form-check-label" for="inlineRadio8">
<span class="adj02">Harry Kane</span>
<span><img src="{{ URL('/') }}/front/wc/img/england-flag-small.png"></span>
</label>
</div>
<div class="form-check form-check-inline adj03">
<input class="form-check-input" type="checkbox" name="inlineRadioOptions" id="inlineRadio9" value="9">
<label class="form-check-label" for="inlineRadio9">
<span class="adj02">Raheem Sterling</span>
<span><img src="{{ URL('/') }}/front/wc/img/england-flag-small.png"></span>
</label>
</div>
<div class="form-check form-check-inline adj03">
<input class="form-check-input" type="checkbox" name="inlineRadioOptions" id="inlineRadio10" value="10">
<label class="form-check-label" for="inlineRadio10">
<span class="adj02">Marcus Rashford</span>
<span><img src="{{ URL('/') }}/front/wc/img/england-flag-small.png"></span>
</label>
</div>

</div>
<div class="col-lg-3 bordRight">
<div class="form-check form-check-inline adj03">
<input class="form-check-input" type="checkbox" name="inlineRadioOptions" id="inlineRadio11" value="11">
<label class="form-check-label" for="inlineRadio11">
<span class="adj02">Mario Mandzukic</span>
<span><img src="{{ URL('/') }}/front/wc/img/serbia-flag-small.png"></span>
</label>
</div>
<div class="form-check form-check-inline adj03">
<input class="form-check-input" type="checkbox" name="inlineRadioOptions" id="inlineRadio12" value="12">
<label class="form-check-label" for="inlineRadio12">
<span class="adj02">Christian Eriksen</span>
<span><img src="{{ URL('/') }}/front/wc/img/switzerland-flag-small.png"></span>
</label>
</div>
<div class="form-check form-check-inline adj03">
<input class="form-check-input" type="checkbox" name="inlineRadioOptions" id="inlineRadio13" value="13">
<label class="form-check-label" for="inlineRadio13">
<span class="adj02">Gylfi Sigurdsson</span>
<span><img src="{{ URL('/') }}/front/wc/img/iceland-flag-small.png"></span>
</label>
</div>
<div class="form-check form-check-inline adj03">
<input class="form-check-input" type="checkbox" name="inlineRadioOptions" id="inlineRadio15" value="15">
<label class="form-check-label" for="inlineRadio15">
<span class="adj02">Kylian Mbappe</span>
<span><img src="{{ URL('/') }}/front/wc/img/france-flag-small.png"></span>
</label>
</div>
<div class="form-check form-check-inline adj03">
<input class="form-check-input" type="checkbox" name="inlineRadioOptions" id="inlineRadio16" value="16">
<label class="form-check-label" for="inlineRadio16">
<span class="adj02">Antoine Greizmann</span>
<span><img src="{{ URL('/') }}/front/wc/img/france-flag-small.png"></span>
</label>
</div>
<div class="form-check form-check-inline adj03">
<input class="form-check-input" type="checkbox" name="inlineRadioOptions" id="inlineRadio17" value="17">
<label class="form-check-label" for="inlineRadio17">
<span class="adj02">Olivier Giroud</span>
<span><img src="{{ URL('/') }}/front/wc/img/france-flag-small.png"></span>
</label>
</div>
<div class="form-check form-check-inline adj03">
<input class="form-check-input" type="checkbox" name="inlineRadioOptions" id="inlineRadio14" value="14">
<label class="form-check-label" for="inlineRadio14">
<span class="adj02">Bryan Ruiz</span>
<span><img src="{{ URL('/') }}/front/wc/img/Costa-Rica.png"></span>
</label>
</div>
<div class="form-check form-check-inline adj03">
<input class="form-check-input" type="checkbox" name="inlineRadioOptions" id="inlineRadio18" value="18">
<label class="form-check-label" for="inlineRadio18">
<span class="adj02">Timo Werner</span>
<span><img src="{{ URL('/') }}/front/wc/img/germany-flag-small.png"></span>
</label>
</div>
<div class="form-check form-check-inline adj03">
<input class="form-check-input" type="checkbox" name="inlineRadioOptions" id="inlineRadio19" value="19">
<label class="form-check-label" for="inlineRadio19">
<span class="adj02">Thomas Muller</span>
<span><img src="{{ URL('/') }}/front/wc/img/germany-flag-small.png"></span>
</label>
</div>
<div class="form-check form-check-inline adj03">
<input class="form-check-input" type="checkbox" name="inlineRadioOptions" id="inlineRadio20" value="20">
<label class="form-check-label" for="inlineRadio20">
<span class="adj02">Mario Gomez</span>
<span><img src="{{ URL('/') }}/front/wc/img/germany-flag-small.png"></span>
</label>
</div>
</div>
<div class="col-lg-3 bordRight">
<div class="form-check form-check-inline adj03">
<input class="form-check-input" type="checkbox" name="inlineRadioOptions" id="inlineRadio21" value="21">
<label class="form-check-label" for="inlineRadio21">
<span class="adj02">Neymar</span>
<span><img src="{{ URL('/') }}/front/wc/img/brazil-flag-small.png"></span>
</label>
</div>
<div class="form-check form-check-inline adj03">
<input class="form-check-input" type="checkbox" name="inlineRadioOptions" id="inlineRadio22" value="22">
<label class="form-check-label" for="inlineRadio22">
<span class="adj02">Roberto Firmino</span>
<span><img src="{{ URL('/') }}/front/wc/img/brazil-flag-small.png"></span>
</label>
</div>
<div class="form-check form-check-inline adj03">
<input class="form-check-input" type="checkbox" name="inlineRadioOptions" id="inlineRadio23" value="23">
<label class="form-check-label" for="inlineRadio23">
<span class="adj02">Gabriel Jesus</span>
<span><img src="{{ URL('/') }}/front/wc/img/brazil-flag-small.png"></span>
</label>
</div>
<div class="form-check form-check-inline adj03">
<input class="form-check-input" type="checkbox" name="inlineRadioOptions" id="inlineRadio24" value="24">
<label class="form-check-label" for="inlineRadio24">
<span class="adj02">Philippe Coutinho</span>
<span><img src="{{ URL('/') }}/front/wc/img/brazil-flag-small.png"></span>
</label>
</div>
<div class="form-check form-check-inline adj03">
<input class="form-check-input" type="checkbox" name="inlineRadioOptions" id="inlineRadio25" value="25">
<label class="form-check-label" for="inlineRadio25">
<span class="adj02">Cristiano Ronaldo</span>
<span><img src="{{ URL('/') }}/front/wc/img/portugal-flag-small.png"></span>
</label>
</div>
<div class="form-check form-check-inline adj03">
<input class="form-check-input" type="checkbox" name="inlineRadioOptions" id="inlineRadio26" value="26">
<label class="form-check-label" for="inlineRadio26">
<span class="adj02">Andre Silva</span>
<span><img src="{{ URL('/') }}/front/wc/img/portugal-flag-small.png"></span>
</label>
</div>
<div class="form-check form-check-inline adj03">
<input class="form-check-input" type="checkbox" name="inlineRadioOptions" id="inlineRadio27" value="27">
<label class="form-check-label" for="inlineRadio27">
<span class="adj02">Eden Hazard</span>
<span><img src="{{ URL('/') }}/front/wc/img/belgium-flag-small.png"></span>
</label>
</div>
<div class="form-check form-check-inline adj03">
<input class="form-check-input" type="checkbox" name="inlineRadioOptions" id="inlineRadio28" value="28">
<label class="form-check-label" for="inlineRadio28">
<span class="adj02">Christian Benteke</span>
<span><img src="{{ URL('/') }}/front/wc/img/belgium-flag-small.png"></span>
</label>
</div>
<div class="form-check form-check-inline adj03">
<input class="form-check-input" type="checkbox" name="inlineRadioOptions" id="inlineRadio29" value="29">
<label class="form-check-label" for="inlineRadio29">
<span class="adj02">Romelo Lukaku</span>
<span><img src="{{ URL('/') }}/front/wc/img/belgium-flag-small.png"></span>
</label>
</div>
<div class="form-check form-check-inline adj03">
<input class="form-check-input" type="checkbox" name="inlineRadioOptions" id="inlineRadio30" value="30">
<label class="form-check-label" for="inlineRadio30">
<span class="adj02">Kevin De Bruyne</span>
<span><img src="{{ URL('/') }}/front/wc/img/belgium-flag-small.png"></span>
</label>
</div>
</div>
<div class="col-lg-3">
<div class="form-check form-check-inline adj03">
<input class="form-check-input" type="checkbox" name="inlineRadioOptions" id="inlineRadio31" value="31">
<label class="form-check-label" for="inlineRadio31">
<span class="adj02">Tim Cahill</span>
<span><img src="{{ URL('/') }}/front/wc/img/australia-flag-small.png"></span>
</label>
</div>
<div class="form-check form-check-inline adj03">
<input class="form-check-input" type="checkbox" name="inlineRadioOptions" id="inlineRadio32" value="32">
<label class="form-check-label" for="inlineRadio32">
<span class="adj02">Mohamed Salah</span>
<span><img src="{{ URL('/') }}/front/wc/img/egypt-flag-small.png"></span>
</label>
</div>
<div class="form-check form-check-inline adj03">
<input class="form-check-input" type="checkbox" name="inlineRadioOptions" id="inlineRadio33" value="33">
<label class="form-check-label" for="inlineRadio33">
<span class="adj02">Son Heung Min</span>
<span><img src="{{ URL('/') }}/front/wc/img/korea-flag-small.png"></span>
</label>
</div>
<div class="form-check form-check-inline adj03">
<input class="form-check-input" type="checkbox" name="inlineRadioOptions" id="inlineRadio34" value="34">
<label class="form-check-label" for="inlineRadio34">
<span class="adj02">Javier Hernandez</span>
<span><img src="{{ URL('/') }}/front/wc/img/mexico-flag-small.png"></span>
</label>
</div>
<div class="form-check form-check-inline adj03">
<input class="form-check-input" type="checkbox" name="inlineRadioOptions" id="inlineRadio35" value="35">
<label class="form-check-label" for="inlineRadio35">
<span class="adj02">Lewandowski</span>
<span><img src="{{ URL('/') }}/front/wc/img/poland-flag-small.png"></span>
</label>
</div>
<div class="form-check form-check-inline adj03">
<input class="form-check-input" type="checkbox" name="inlineRadioOptions" id="inlineRadio36" value="36">
<label class="form-check-label" for="inlineRadio36">
<span class="adj02">Luis Suarez</span>
<span><img src="{{ URL('/') }}/front/wc/img/uruguay-flag-small.png"></span>
</label>
</div>
<div class="form-check form-check-inline adj03">
<input class="form-check-input" type="checkbox" name="inlineRadioOptions" id="inlineRadio40" value="40">
<label class="form-check-label" for="inlineRadio40">
<span class="adj02">Edinson Cavani</span>
<span><img src="{{ URL('/') }}/front/wc/img/uruguay-flag-small.png"></span>
</label>
</div>
<div class="form-check form-check-inline adj03">
<input class="form-check-input" type="checkbox" name="inlineRadioOptions" id="inlineRadio37" value="37">
<label class="form-check-label" for="inlineRadio37">
<span class="adj02">Sadio Mane</span>
<span><img src="{{ URL('/') }}/front/wc/img/senegal-flag-small.png"></span>
</label>
</div>
<div class="form-check form-check-inline adj03">
<input class="form-check-input" type="checkbox" name="inlineRadioOptions" id="inlineRadio38" value="38">
<label class="form-check-label" for="inlineRadio38">
<span class="adj02">Isco</span>
<span><img src="{{ URL('/') }}/front/wc/img/spain-flag-small.png"></span>
</label>
</div>
<div class="form-check form-check-inline adj03">
<input class="form-check-input" type="checkbox" name="inlineRadioOptions" id="inlineRadio39" value="39">
<label class="form-check-label" for="inlineRadio39">
<span class="adj02">Diego Costa</span>
<span><img src="{{ URL('/') }}/front/wc/img/spain-flag-small.png"></span>
</label>
</div>
</div>
</div>


</div>
</div>

</div>

<div class="row">
<div class="col-lg-12">
<div class="btmWrap">
<div class="tokenCont1">
<span class="tokTit">Total Token</span><span class="tokDig">{{$total_token}}</span>
</div>
<div class="submitBtn btm"><a href="#" onclick="The event is ended. Thanks for participant!">Next</a></div>
<div class="submitBtn btm"><a href="#" onclick="clear_checkbox()">Reset</a></div>
</div>
</div>

</div>

<div class="row">
<div class="col-lg-3">
<div class="footerMenu1">
<ul>
<li><a class="sep2" href="#modal">Terms and Conditions</a></li>
<li><a href="{{route('wclobby')}}">Back home</a></li>
</ul>
</div>
</div>
<div class="col-lg-9">
<div class="footerTM text-right text-black">
Copyright infiniwin © 2018 - All rights reserved 
</div>
</div>
</div>

</div>
</div>

<div class="remodal" data-remodal-id="modal" role="dialog" aria-labelledby="modal1Title" aria-describedby="modal1Desc">
    <button data-remodal-action="close" class="remodal-close" aria-label="Close"></button>
    <div>
        <img src="{{ URL('/') }}/front/wc/img/TNC.png">
    </div>
</div>  

<script>
  $(document).on('opening', '.remodal', function () {
    console.log('opening');
  });

  $(document).on('opened', '.remodal', function () {
    console.log('opened');
  });

  $(document).on('closing', '.remodal', function (e) {
    console.log('closing' + (e.reason ? ', reason: ' + e.reason : ''));
  });

  $(document).on('closed', '.remodal', function (e) {
    console.log('closed' + (e.reason ? ', reason: ' + e.reason : ''));
  });

  $(document).on('confirmation', '.remodal', function () {
    console.log('confirmation');
  });

  $(document).on('cancellation', '.remodal', function () {
    console.log('cancellation');
  });

//  Usage:
//  $(function() {
//
//    // In this case the initialization function returns the already created instance
//    var inst = $('[data-remodal-id=modal]').remodal();
//
//    inst.open();
//    inst.close();
//    inst.getState();
//    inst.destroy();
//  });

  //  The second way to initialize:
  $('[data-remodal-id=modal2]').remodal({
    modifier: 'with-red-theme'
  });
</script>

   

  </body>

</html>
