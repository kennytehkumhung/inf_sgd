<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">
<title>Infiniwin World Cup 2018 Predictions</title>
<!-- Bootstrap core CSS -->
<link href="{{ URL('/') }}/front/wc/resources/css/bootstrap.css" rel="stylesheet">
<!-- Custom CSS -->
<link href="{{ URL('/') }}/front/wc/resources/css/custom.css" rel="stylesheet">
<link href="{{ URL('/') }}/front/wc/resources/css/remodal.css" rel="stylesheet">
<link href="{{ URL('/') }}/front/wc/resources/css/remodal-default-theme.css" rel="stylesheet">
<!-- Responsive CSS -->
<link href="{{ URL('/') }}/front/wc/resources/css/responsive.css" rel="stylesheet">
<!-- Bootstrap core JavaScript -->
<script src="{{ URL('/') }}/front/wc/resources/js/jquery.min.js"></script>
<script src="{{ URL('/') }}/front/wc/resources/js/bootstrap.bundle.min.js"></script>
<!-- Remodal -->
<script src="{{ URL('/') }}/front/wc/resources/js/remodal.js"></script>
<!-- Countdown -->
<script src="{{ URL('/') }}/front/wc/resources/js/jquery.countdown.js"></script>
</head>
<body>
<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark">
<div class="container">
<a class="navbar-brand" href="{{ URL('/') }}"><img src="{{ URL('/') }}/front/wc/img/logo-left.png"></a>
<div class="logoRight float-right"><img src="{{ URL('/') }}/front/wc/img/logo-right.png"></div>
</div>
</nav>
<!-- Navigation -->
<!-- Page Content -->
<div class="container">
<div class="content">
<div class="row">
<div class="col-lg-4 text-center">
<div class="tnm"><img src="{{ URL('/') }}/front/wc/img/logo-sm.png"><span>Tournament starts in:</span></div>
</div>
<div class="col-lg-4 text-center">
<!--WC 18 Timer-->
<div class="wcInner">
<div id="getting-started" class="timerDig"></div>
<script type="text/javascript">
   $("#getting-started")
   .countdown("2018/06/14 23:00:00", function(event) {
    $(this).text(
      event.strftime('%D %H %M %S')
    );
    });
</script>
    @if (Request::input('reminder') == 'y')
        <script type="text/javascript">
            $(function () {
                alert("Dear member, you still have World Cup token yet been bet. Please finish before forfeit. Thank you!");
            });
        </script>
    @endif
</div>
<!--WC 18 Timer-->
</div>
<div class="col-lg-4 text-center">
<div class="wrap">
<div class="userN float-left">{{ Lang::get('public.Username') }}: {{ Session::get('username') }}</div>
</div>
</div>
</div>
<div class="row">
<div class="col-lg-3 text-center">
<img src="{{ URL('/') }}/front/wc/img/img-1.png">
</div>
<div class="col-lg-6"></div>
<div class="col-lg-3">
<div class="tokenWrap">
<img src="{{ URL('/') }}/front/wc/img/token-img.png">
<div class="tokenCont">
<span class="tokTit">Total Token</span><span class="tokDig">{{$total_token}}</span>
</div>
</div>
</div>
</div>

<div class="row">
<div class="col-lg-3">
<div class="sideNav">
<ul>
<li><a href="#" onclick="alert('Bet session closed. Thanks for participant!');">Winner</a></li>
<li><a href="#" onclick="alert('Bet session closed. Thanks for participant!');">Finalists</a></li>
<li><a href="#" onclick="alert('Bet session closed. Thanks for participant!');">Team to reach the Finals</a></li>
<li><a href="#" onclick="alert('Bet session closed. Thanks for participant!');">Team to reach the Semi Finals</a></li>
<li><a href="#" onclick="alert('Bet session closed. Thanks for participant!');">Team to reach the Quarter Finals</a></li>
<li><a href="#" onclick="alert('Bet session closed. Thanks for participant!');">Higher team goal</a></li>
<li><a href="#" onclick="alert('Bet session closed. Thanks for participant!');">Top goal scorer</a></li>
<li><a href="#" onclick="alert('Bet session closed. Thanks for participant!');">Group winner</a></li>
</ul>
</div>
</div>
<div class="col-lg-9">
<div class="mainTable" style="height: 330px; overflow: auto;">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <th>#</th>
      <th>Category</th>
      <th>Selection</th>
      <th>Status</th>
      <th>Bet Time</th>
    </tr>
    <?php $no = 1; ?>
    @foreach($ledgers as $key => $val)
        <tr>
          <td>{{$no++}}</td>
          <td>{{$val['category']}}</td>
          <td>{{$val['selection']}}</td>
          <td>{{$val['status']}}</td>
          <td>{{$val['date']}}</td>
        </tr>
    @endforeach
</table>
</div>
</div>

</div>

<div class="row">
<div class="col-lg-3">
<div class="footerMenu">
<ul>
<li><a class="sep2" href="#modal">Terms and Conditions</a></li>
</ul>
</div>
</div>
<div class="col-lg-9">
<div class="footerTM text-right">
Copyright infiniwin © 2018 - All rights reserved 
</div>
</div>
</div>

</div>
</div>

<div class="remodal" data-remodal-id="modal" role="dialog" aria-labelledby="modal1Title" aria-describedby="modal1Desc">
    <button data-remodal-action="close" class="remodal-close" aria-label="Close"></button>
    <div>
        <img src="{{ URL('/') }}/front/wc/img/TNC.png">
    </div>
</div>  

<script>
  $(document).on('opening', '.remodal', function () {
    console.log('opening');
  });

  $(document).on('opened', '.remodal', function () {
    console.log('opened');
  });

  $(document).on('closing', '.remodal', function (e) {
    console.log('closing' + (e.reason ? ', reason: ' + e.reason : ''));
  });

  $(document).on('closed', '.remodal', function (e) {
    console.log('closed' + (e.reason ? ', reason: ' + e.reason : ''));
  });

  $(document).on('confirmation', '.remodal', function () {
    console.log('confirmation');
  });

  $(document).on('cancellation', '.remodal', function () {
    console.log('cancellation');
  });

//  Usage:
//  $(function() {
//
//    // In this case the initialization function returns the already created instance
//    var inst = $('[data-remodal-id=modal]').remodal();
//
//    inst.open();
//    inst.close();
//    inst.getState();
//    inst.destroy();
//  });

  //  The second way to initialize:
  $('[data-remodal-id=modal2]').remodal({
    modifier: 'with-red-theme'
  });
</script>
   

  </body>

</html>
