<!DOCTYPE html>
<html lang="en">
@include('front/wc/wc_header' )

<script type="text/javascript" language="javascript">
    $(document).ready(function(){
    });
    function submit_winner() {
        var list = [];
        $(':checkbox:checked').each(function(i){
          list[i] = $(this).val();
        });
        if(list.length > 3){
            alert("Maximum 3 box can be selected! Thanks!");
        }
        else if(list.length == 0){
            window.location.href = "{{route('wcfinalists')}}";   
        }
        else{
            $.ajax({
                type: "POST",
                url: "{{route('wcwinner-process')}}",
                data: {
                       _token: "{{ csrf_token() }}",
                       username: "{{ Session::get('username') }}",
                       team: list
                },
                beforeSend: function(){
                       $('#next_button').attr('onClick','');
                       $('#next_button').html('Loading...');
                },
                success: function(json){
                    obj = JSON.parse(json);
                        var str = '';
                        $.each(obj, function(i, item) {
                            if(item == '{{Lang::get('COMMON.SUCESSFUL')}}'){
                                alert('We will process to next section.');
                                window.location.href = "{{route('wcfinalists')}}";                                
                            }
                            else{
                                alert('[ERROR] Please refer your Token Balance.');
                                $('#next_button').attr('onClick','submit_winner()');
                                $('#next_button').html('Next');
                            }

                        })
                    }
               })
        }
    }
    
    function clear_checkbox(){
        $('input:checkbox').removeAttr('checked');
    }
        
</script>

<div class="row">
<div class="col-lg-6">
<div class="adj01">
<img src="{{ URL('/') }}/front/wc/img/title-1.png">
**Spending limit of 3 Tokens
</div>
</div>
<div class="col-lg-3"></div>
<div class="col-lg-3">
<div class="tokenWrap">
<img src="{{ URL('/') }}/front/wc/img/token-img.png">
<div class="tokenCont">
<span class="tokTit">LIMIT TOKEN LEFT</span><span class="tokDig">{{$token}}</span>
</div>
<div class="backBtn" style="display: block; position: absolute;right: 50px;top: 70px;"><a href="{{route('wclobby')}}"><img src="{{ URL('/') }}/front/wc/img/back-btn.png"></a></div>
</div>
</div>
</div>

<div class="row">
<div class="col-lg-12">
<div class="opt1">
<div class="form-check form-check-inline">
  <input class="form-check-input" type="checkbox" name="wc_list[]" id="inlineRadio1" value="1">
  <label class="form-check-label" for="inlineRadio1"><span><img src="{{ URL('/') }}/front/wc/img/russia-flag.png"></span>Russia</label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" type="checkbox" name="wc_list[]" id="inlineRadio2" value="2">
  <label class="form-check-label" for="inlineRadio2"><span><img src="{{ URL('/') }}/front/wc/img/egypt-flag.png"></span>Egypt</label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" type="checkbox" name="wc_list[]" id="inlineRadio3" value="3">
  <label class="form-check-label" for="inlineRadio3"><span><img src="{{ URL('/') }}/front/wc/img/arabia-flag.png"></span>Saudi Arabia</label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" type="checkbox" name="wc_list[]" id="inlineRadio4" value="4">
  <label class="form-check-label" for="inlineRadio4"><span><img src="{{ URL('/') }}/front/wc/img/uruguay-flag.png"></span>Uruguay</label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" type="checkbox" name="wc_list[]" id="inlineRadio5" value="5">
  <label class="form-check-label" for="inlineRadio5"><span><img src="{{ URL('/') }}/front/wc/img/morocco-flag.png"></span>Morocco</label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" type="checkbox" name="wc_list[]" id="inlineRadio6" value="6">
  <label class="form-check-label" for="inlineRadio6"><span><img src="{{ URL('/') }}/front/wc/img/portugal-flag.png"></span>Portugal</label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" type="checkbox" name="wc_list[]" id="inlineRadio7" value="7">
  <label class="form-check-label" for="inlineRadio7"><span><img src="{{ URL('/') }}/front/wc/img/iran-flag.png"></span>Iran</label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" type="checkbox" name="wc_list[]" id="inlineRadio8" value="8">
  <label class="form-check-label" for="inlineRadio8"><span><img src="{{ URL('/') }}/front/wc/img/france-flag.png"></span>France</label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" type="checkbox" name="wc_list[]" id="inlineRadio9" value="9">
  <label class="form-check-label" for="inlineRadio9"><span><img src="{{ URL('/') }}/front/wc/img/argentina-flag.png"></span>Argentina</label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" type="checkbox" name="wc_list[]" id="inlineRadio10" value="10">
  <label class="form-check-label" for="inlineRadio10"><span><img src="{{ URL('/') }}/front/wc/img/iceland-flag.png"></span>Iceland</label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" type="checkbox" name="wc_list[]" id="inlineRadio11" value="11">
  <label class="form-check-label" for="inlineRadio11"><span><img src="{{ URL('/') }}/front/wc/img/peru-flag.png"></span>Peru</label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" type="checkbox" name="wc_list[]" id="inlineRadio12" value="12">
  <label class="form-check-label" for="inlineRadio12"><span><img src="{{ URL('/') }}/front/wc/img/croatia-flag.png"></span>Croatia</label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" type="checkbox" name="wc_list[]" id="inlineRadio13" value="13">
  <label class="form-check-label" for="inlineRadio13"><span><img src="{{ URL('/') }}/front/wc/img/denmark-flag.png"></span>Denmark</label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" type="checkbox" name="wc_list[]" id="inlineRadio14" value="14">
  <label class="form-check-label" for="inlineRadio14"><span><img src="{{ URL('/') }}/front/wc/img/nigeria-flag.png"></span>Nigeria</label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" type="checkbox" name="wc_list[]" id="inlineRadio15" value="15">
  <label class="form-check-label" for="inlineRadio15"><span><img src="{{ URL('/') }}/front/wc/img/costa-flag.png"></span>Costa Rica</label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" type="checkbox" name="wc_list[]" id="inlineRadio16" value="16">
  <label class="form-check-label" for="inlineRadio16"><span><img src="{{ URL('/') }}/front/wc/img/germany-flag.png"></span>Germany</label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" type="checkbox" name="wc_list[]" id="inlineRadio17" value="17">
  <label class="form-check-label" for="inlineRadio17"><span><img src="{{ URL('/') }}/front/wc/img/serbia-flag.png"></span>Serbia</label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" type="checkbox" name="wc_list[]" id="inlineRadio18" value="18">
  <label class="form-check-label" for="inlineRadio18"><span><img src="{{ URL('/') }}/front/wc/img/mexico-flag.png"></span>Mexico</label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" type="checkbox" name="wc_list[]" id="inlineRadio19" value="19">
  <label class="form-check-label" for="inlineRadio19"><span><img src="{{ URL('/') }}/front/wc/img/brazil-flag.png"></span>Brazil</label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" type="checkbox" name="wc_list[]" id="inlineRadio20" value="20">
  <label class="form-check-label" for="inlineRadio20"><span><img src="{{ URL('/') }}/front/wc/img/sweden-flag.png"></span>Sweden</label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" type="checkbox" name="wc_list[]" id="inlineRadio21" value="21">
  <label class="form-check-label" for="inlineRadio21"><span><img src="{{ URL('/') }}/front/wc/img/switzerland-flag.png"></span>Switzerland</label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" type="checkbox" name="wc_list[]" id="inlineRadio22" value="22">
  <label class="form-check-label" for="inlineRadio22"><span><img src="{{ URL('/') }}/front/wc/img/korea-flag.png"></span>South Korea</label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" type="checkbox" name="wc_list[]" id="inlineRadio23" value="23">
  <label class="form-check-label" for="inlineRadio23"><span><img src="{{ URL('/') }}/front/wc/img/belgium-flag.png"></span>Belgium</label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" type="checkbox" name="wc_list[]" id="inlineRadio24" value="24">
  <label class="form-check-label" for="inlineRadio24"><span><img src="{{ URL('/') }}/front/wc/img/tunisia-flag.png"></span>Tunisia</label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" type="checkbox" name="wc_list[]" id="inlineRadio25" value="25">
  <label class="form-check-label" for="inlineRadio25"><span><img src="{{ URL('/') }}/front/wc/img/panama-flag.png"></span>Panama</label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" type="checkbox" name="wc_list[]" id="inlineRadio26" value="26">
  <label class="form-check-label" for="inlineRadio26"><span><img src="{{ URL('/') }}/front/wc/img/england-flag.png"></span>England</label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" type="checkbox" name="wc_list[]" id="inlineRadio27" value="27">
  <label class="form-check-label" for="inlineRadio27"><span><img src="{{ URL('/') }}/front/wc/img/colombia-flag.png"></span>Colombia</label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" type="checkbox" name="wc_list[]" id="inlineRadio28" value="28">
  <label class="form-check-label" for="inlineRadio28"><span><img src="{{ URL('/') }}/front/wc/img/poland-flag.png"></span>Poland</label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" type="checkbox" name="wc_list[]" id="inlineRadio29" value="29">
  <label class="form-check-label" for="inlineRadio29"><span><img src="{{ URL('/') }}/front/wc/img/japan-flag.png"></span>Japan</label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" type="checkbox" name="wc_list[]" id="inlineRadio30" value="30">
  <label class="form-check-label" for="inlineRadio30"><span><img src="{{ URL('/') }}/front/wc/img/senegal-flag.png"></span>Senegal</label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" type="checkbox" name="wc_list[]" id="inlineRadio31" value="31">
  <label class="form-check-label" for="inlineRadio31"><span><img src="{{ URL('/') }}/front/wc/img/spain-flag.png"></span>Spain</label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" type="checkbox" name="wc_list[]" id="inlineRadio32" value="32">
  <label class="form-check-label" for="inlineRadio32"><span><img src="{{ URL('/') }}/front/wc/img/nz-flag.png"></span>Australia</label>
</div>
</div>
</div>

</div>

<div class="row">
<div class="col-lg-12">
<div class="btmWrap">
<div class="tokenCont1">
<span class="tokTit">Total Token</span><span class="tokDig">{{$total_token}}</span>
</div>
    <div class="submitBtn btm"><a id="next_button" href="#" onclick="The event is ended. Thanks for participant!">Next</a></div>
    <div class="submitBtn btm"><a href="#" onclick="clear_checkbox()">Reset</a></div>
</div>
</div>

</div>

<div class="row">
<div class="col-lg-3">
<div class="footerMenu1">
<ul>
<li><a class="sep2" href="#modal">Terms and Conditions</a></li>
<li><a href="{{route('wclobby')}}">Back home</a></li>
</ul>
</div>
</div>
<div class="col-lg-9">
<div class="footerTM text-right text-black">
Copyright infiniwin © 2018 - All rights reserved 
</div>
</div>
</div>

</div>
</div>

<div class="remodal" data-remodal-id="modal" role="dialog" aria-labelledby="modal1Title" aria-describedby="modal1Desc">
    <button data-remodal-action="close" class="remodal-close" aria-label="Close"></button>
    <div>
        <img src="{{ URL('/') }}/front/wc/img/TNC.png">
    </div>
</div>  

<script>
  $(document).on('opening', '.remodal', function () {
    console.log('opening');
  });

  $(document).on('opened', '.remodal', function () {
    console.log('opened');
  });

  $(document).on('closing', '.remodal', function (e) {
    console.log('closing' + (e.reason ? ', reason: ' + e.reason : ''));
  });

  $(document).on('closed', '.remodal', function (e) {
    console.log('closed' + (e.reason ? ', reason: ' + e.reason : ''));
  });

  $(document).on('confirmation', '.remodal', function () {
    console.log('confirmation');
  });

  $(document).on('cancellation', '.remodal', function () {
    console.log('cancellation');
  });

//  Usage:
//  $(function() {
//
//    // In this case the initialization function returns the already created instance
//    var inst = $('[data-remodal-id=modal]').remodal();
//
//    inst.open();
//    inst.close();
//    inst.getState();
//    inst.destroy();
//  });

  //  The second way to initialize:
  $('[data-remodal-id=modal2]').remodal({
    modifier: 'with-red-theme'
  });
</script>
   

  </body>

</html>
