@extends('front/master')

@section('title', 'Deposit')

@section('top_js')
 <link href="{{url()}}/front/resources/css/acct_management.css" rel="stylesheet">
 <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <script>
	setInterval(update_mainwallet, 10000);
  function deposit_submit(){

      @if (Session::get('currency') == 'TWD')
		  @if (strlen($fullname) < 1)
			  var fullname = $("#fullname").val();

			  if (fullname.length < 1) {
				  alert("请输入真实姓名。");
				  return;
			  } else if (!confirm("您的真實姓名為：" + fullname + " , 姓名錯誤將無法提點，請您再次確認。")) {
				  return;
			  }
	  	  @endif

		  @if ($withdrawalCodeRequired)
			  if ($("#withdrawalcode").val() != $("#confirmwithdrawalcode").val()) {
			      alert("{{ Lang::get('public.WithdrawalCodeDoNotMatch') }}");
			      return;
			  }
		  @endif
	  @endif

	var file_data = $("#receipt").prop("files")[0];
	var form_data = new FormData();
	form_data.append("file", file_data);

	if ($("#bankbook").length) {
	    file_data = $("#bankbook").prop("files")[0];
        form_data.append("file_bankbook", file_data);
	}
	
	var data = $('#deposit_form').serializeArray();
	var obj = {};
	for (var i = 0, l = data.length; i < l; i++) {
		form_data.append(data[i].name, data[i].value);
	}
	
	form_data.append('_token', '{{csrf_token()}}');

	$.ajax({
		type: "POST",
		url: "{{route('deposit-process')}}?",
		        dataType: 'script',
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,                         // Setting the data attribute of ajax with file_data
                type: 'post',
				beforeSend: function(){
					$('#deposit_sbumit_btn').attr('onclick','');
				},
				complete: function(json){
					
					obj = JSON.parse(json.responseText);

                    if (obj.code == "OK") {
                        alert('{{Lang::get('COMMON.SUCESSFUL')}}');

                        if (obj.action == "drawqrcode") {
                            $("#bui_qrcode").qrcode({
                                width: 256,
                                height: 256,
                                text: obj.url
                            });

                            if (obj.hint.length > 0) {
                                $("#bui_qrlabel").html(obj.hint).show();
                            } else {
                                $("#bui_qrlabel").hide();
                            }

                            showModal = true;
                        } else {
//                            if (obj.hint.length > 0) {
//                                $("#bui_qrlabel").html(obj.hint).show();
//                                showModal = true;
//                            }

                            if (obj.url.length > 0) {
                                window.location.href = obj.url;
//                                $("#bui_qrcode").html('<a href="' + obj.url + '" target="_blank" style="line-height: 250px;">请点这里前往支付网关</a>').show();
//                                showModal = true;
                            }
                        }

                        {{--if (showModal) {--}}
                            {{--$.blockUI({--}}
                                {{--css: { width: "350px", height: "400px", top: "20%", left: "40%" },--}}
                                {{--message: $("#bui_drawqr")--}}
                            {{--});--}}

                            {{--$("#bui_btn_close").click(function () {--}}
                                {{--$.unblockUI();--}}
                                {{--window.location.href = "{{route('transaction')}}";--}}
                            {{--});--}}
                        {{--}--}}
                    } else {
                        var str = '';
                        $.each(obj, function(i, item) {

                            if( '{{Lang::get('COMMON.SUCESSFUL')}}' == item ){
                                alert('{{Lang::get('COMMON.SUCESSFUL')}}');
                                window.location.href = "{{route('transaction')}}";
                            }else{
                                //$('.'+i+'_acctTextReg').html(item);
                                $('#deposit_sbumit_btn').attr('onclick','deposit_submit()');
//							str += item + '<br>';
                                str += item + '\n';
                            }
                        });

                        if (str.length > 0) {
//                        $('.failed_message').html(str);
                            alert(str);
                        }
					}
				}
	});
}
  </script>
  <style>
  .acctRowR table th {
    background-color: #c59f4d;
    border-bottom: 1px solid #d6d6d6 !important;
    color: #000;
    font-weight: bold;
    text-align: center;
    vertical-align: middle;
}
  </style>
@stop

@section('content')

@include('front/transaction_mange_top', [ 'title' => Lang::get('public.Deposit') ] )
<div class="acctContent">
	<div class="depLeft">
		<div class="cont">
			<!--MID SECTION-->
			<?php echo htmlspecialchars_decode($content); ?>
			<!--MID SECTION-->
		</div>
	</div>
<form id="deposit_form">
	<div class="depRight">
		<span class="wallet_title"> {{Lang::get('public.Deposit')}}</span>
			@if (Session::get('currency') == 'TWD')
				<div class="acctRow">
					<label> {{Lang::get('public.DepositMethod')}}* :</label>
					<select id="deposit_pay_channel" name="deposit_pay_channel" style="min-width: 180px;">
						@foreach ($paymentGatewaySetting as $key => $values)
							<optgroup label="{{ $key }}">
								@foreach ($values as $v)
									<option value="{{ $v['code'] }}" {{ isset($v['disabled']) ? $v['disabled'] : '' }}>{{ $v['name'] }}</option>
								@endforeach
							</optgroup>
						@endforeach
					</select>
					<div class="clr"></div>
				</div>

				<div class="acctRow">
					<span>请联络客服以提供储值銀行號碼。</span>
					<div class="clr"></div>
				</div>

				@if (strlen($fullname) < 1)
					<div class="acctRow">
						<label> {{Lang::get('public.ActualFullName')}}* :</label><input type="text" id="fullname" name="fullname" value="{{ $fullname }}" />
						<div class="clr"></div>
					</div>
					<div class="acctRow">
						<span>注意：請輸入您的真實姓名，姓名錯誤將無法提點。</span>
						<div class="clr"></div>
					</div>
				@else
					<div class="acctRow">
						<label> {{Lang::get('public.ActualFullName')}} :</label><span>{{ $fullname }}</span>
						<div class="clr"></div>
					</div>
				@endif

				@if ($withdrawalCodeRequired)
					<div class="acctRow">
						<span>請設定提點密碼以保障您的提點安全。提點密碼必須為4個數字</span>
						<div class="clr"></div>
					</div>
					<div class="acctRow">
						<label> {{Lang::get('public.WithdrawalCode')}}* :</label><input type="password" id="withdrawalcode" name="withdrawalcode" placeholder="必須為4個數字" />
						<div class="clr"></div>
					</div>
					<div class="acctRow">
						<label> {{Lang::get('public.ConfirmWithdrawalCode')}}* :</label><input type="password" id="confirmwithdrawalcode" name="confirmwithdrawalcode" />
						<div class="clr"></div>
					</div>
					<br>
				@endif
			@else
				<input type="hidden" name="fullname" value="{{ $fullname }}">
			@endif

			<div class="acctRow">
				<label> {{Lang::get('public.DepositAmount')}}* :</label><input type="text" name="amount" />
				<div class="clr"></div>
			</div>
			@if (Session::get('currency') == 'TWD')
				<div id="deposit_pay_channel_PWL" class="acctRow deposit_pay_channel_holder" style="display:none; height:100%;">
					<label> {{Lang::get('public.DepositBank')}}* :</label>
					<span id="bankLoading" style="display: none;">{{ Lang::get('public.Loading') }}...</span>
					<select id="deposit_pay_channel_option" name="deposit_pay_channel_option">
					</select>
					<div class="clr"></div>

					<div id="bankInfoHolder" style="display: none; padding-left: 180px; padding-bottom: 12px;"></div>
					<div class="clr"></div>
				</div>

				<div id="deposit_pay_channel_BNK" class="acctRow deposit_pay_channel_holder" style="display:none; height:100%;">
					<label> {{Lang::get('public.DepositBank')}}* :</label>
					<table cellspacing="0" rules="all" border="1" id="ctl00_ctl00_ContentPlaceHolder1_ChildContentMain_deposit_table" style="width:100%;border-collapse:collapse;border: 1px solid #D6D6D6 !important;">
						<tr align="left" style="height:30px;">
							<th scope="col">&nbsp;</th><th scope="col">{{Lang::get('public.Bank')}}</th><th scope="col">{{Lang::get('public.AccountName')}}</th><th scope="col">{{Lang::get('public.AccountNumber')}}</th><th scope="col">{{Lang::get('public.Minimum')}}</th><th scope="col">{{Lang::get('public.Maximum')}}</th><th scope="col">{{Lang::get('public.ProcessingTime')}}</th>
						</tr>
						@foreach( $banks as $key => $bank )
							<tr>
								<td>
									<input name="bank" class="radiobtn" type="radio" value="{{$bank['bnkid']}}">
								</td>
								<td>
									<img src="{{$bank['image']}}">
								</td>
								<td>
									{{$bank['bankaccname']}}
								</td>
								<td>
									{{$bank['bankaccno']}}
								</td>
								<td>
									{{$bank['min']}}
								</td>
								<td>
									{{$bank['max']}}
								</td>
								<td>
									5 min
								</td>
							</tr>
						@endforeach
					</table>
					<br>
					<div class="clr"></div>
				</div>
			@else
				<div class="acctRow" style="display:block; height:100%;">
					<label> {{Lang::get('public.DepositBank')}}* :</label>
					<table cellspacing="0" rules="all" border="1" id="ctl00_ctl00_ContentPlaceHolder1_ChildContentMain_deposit_table" style="width:100%;border-collapse:collapse;border: 1px solid #D6D6D6 !important;">
						<tr align="left" style="height:30px;">
							<th scope="col">&nbsp;</th><th scope="col">{{Lang::get('public.Bank')}}</th><th scope="col">{{Lang::get('public.AccountName')}}</th><th scope="col">{{Lang::get('public.AccountNumber')}}</th><th scope="col">{{Lang::get('public.Minimum')}}</th><th scope="col">{{Lang::get('public.Maximum')}}</th><th scope="col">{{Lang::get('public.ProcessingTime')}}</th>
						</tr>
						@foreach( $banks as $key => $bank )
							<tr>
								<td>
									<input name="bank" class="radiobtn" type="radio" value="{{$bank['bnkid']}}">
								</td>
								<td>
									<img src="{{$bank['image']}}">
								</td>
								<td>
									{{$bank['bankaccname']}}
								</td>
								<td>
									{{$bank['bankaccno']}}
								</td>
								<td>
									{{$bank['min']}}
								</td>
								<td>
									{{$bank['max']}}
								</td>
								<td>
									5 min
								</td>
							</tr>
						@endforeach
					</table>
					<div class="clr"></div>
				</div>
			@endif

			<div id="depositmethodholder0">
				<div class="acctRow" id="refnoholder">
					<label>{{Lang::get('public.DepositReceiptReferenceNo')}} :</label><input name="refno" type="text" />
					<div class="clr"></div>
				</div>
				<div class="acctRow">
					<label>{{Lang::get('public.DepositMethod')}}* :</label>
						<select name="type">
							<option value="0">{{Lang::get('public.OverCounter')}}</option>
							<option value="1">{{Lang::get('public.InternetBanking')}}</option>
							<option value="2">{{Lang::get('public.ATMBanking')}}</option>
						</select>
					<div class="clr"></div>
				</div>
				<div class="acctRow">
					<label>{{Lang::get('public.DateTime')}}* :</label>
					<select name="hours">
						<option value="01">01</option>
						<option value="02">02</option>
						<option value="03">03</option>
						<option value="04">04</option>
						<option value="05">05</option>
						<option value="06">06</option>
						<option value="07">07</option>
						<option value="08">08</option>
						<option value="09">09</option>
						<option value="10">10</option>
						<option value="11">11</option>
						<option value="12">12</option>
					</select>
					<select name="minutes" >
						<option value="00">00</option>
						<option value="01">01</option>
						<option value="02">02</option>
						<option value="03">03</option>
						<option value="04">04</option>
						<option value="05">05</option>
						<option value="06">06</option>
						<option value="07">07</option>
						<option value="08">08</option>
						<option value="09">09</option>
						<option value="10">10</option>
						<option value="11">11</option>
						<option value="12">12</option>
						<option value="13">13</option>
						<option value="14">14</option>
						<option value="15">15</option>
						<option value="16">16</option>
						<option value="17">17</option>
						<option value="18">18</option>
						<option value="19">19</option>
						<option value="20">20</option>
						<option value="21">21</option>
						<option value="22">22</option>
						<option value="23">23</option>
						<option value="24">24</option>
						<option value="25">25</option>
						<option value="26">26</option>
						<option value="27">27</option>
						<option value="28">28</option>
						<option value="29">29</option>
						<option value="30">30</option>
						<option value="31">31</option>
						<option value="32">32</option>
						<option value="33">33</option>
						<option value="34">34</option>
						<option value="35">35</option>
						<option value="36">36</option>
						<option value="37">37</option>
						<option value="38">38</option>
						<option value="39">39</option>
						<option value="40">40</option>
						<option value="41">41</option>
						<option value="42">42</option>
						<option value="43">43</option>
						<option value="44">44</option>
						<option value="45">45</option>
						<option value="46">46</option>
						<option value="47">47</option>
						<option value="48">48</option>
						<option value="49">49</option>
						<option value="50">50</option>
						<option value="51">51</option>
						<option value="52">52</option>
						<option value="53">53</option>
						<option value="54">54</option>
						<option value="55">55</option>
						<option value="56">56</option>
						<option value="57">57</option>
						<option value="58">58</option>
						<option value="59">59</option>
					</select>
					<select name="range">
						<option value="AM">AM</option>
						<option value="PM">PM</option>
					</select>
					<input type="text" class="datepicker" id="deposit_date" style="cursor:pointer;" name="date" value="{{date('Y-m-d')}}"><br>
					<div class="clr"></div>
				</div>

				<div id="receiptholder" class="acctRow">
					<label> {{Lang::get('public.UploadDepositReceipt')}}* :</label><input class="upload" type="file" name="receipt" id="receipt">
					<div class="clr"></div>
				</div>
			</div>

			@if($bankbookRequired)
				<div id="bankbookholder">
					<div class="acctRow">
						<label> {{Lang::get('public.UploadBankbookPicture')}}* :</label><input class="upload" type="file" name="bankbook" id="bankbook">
						<div class="clr"></div>
					</div>
					<div class="acctRow">
						<span>首次存款請上傳您的銀行簿载图。</span>
						<div class="clr"></div>
					</div>
				</div>
			@endif

			<p>* {{Lang::get('public.RequiredFields')}}</p>
			<p>* {{Lang::get('public.MaxUploadFileSize', ['p1' => '1MB'])}}</p>
			<br>
			<div class="line"></div>
			<span class="wallet_title">{{Lang::get('public.Deposit')}} {{Lang::get('public.Promotion')}}</span>
			<div class="promoWrap">
		@foreach( $promos as $key => $promo )
			
				<input name="promo" class="radiobtn" type="radio" value="{{$promo['code']}}">
				{{$promo['name']}}
				<br>
				<img src="{{$promo['image']}}">
				<br>
		@endforeach
				<br>
				<input name="promo" class="radiobtn" type="radio" value="0">  {{Lang::get('public.IDoNotWantPromotion')}}
			</div>
			{{--<p> {{Lang::get('public.RequiredFields')}}</p>--}}
			<p><input class="radiobtn" type="radio" name="rules" >{{Lang::get('public.IAlreadyUnderstandRules')}}</p>
			<span class="failed_message acctTextReg" style="display:block;float:left;height:100%;"></span>

			<div class="submitAcct">
				<a id="deposit_sbumit_btn" href="javascript:void(0)" onclick="deposit_submit()">{{Lang::get('public.Submit')}}</a>
			</div>
	</div>
	</form>
    <div class="clr"></div>
      </div>

@stop

@section('bottom_js')
<script src="{{url()}}/front/resources/js/slick.min.js"></script>
<script>

$(function() {
    $( ".datepicker" ).datepicker({ dateFormat: 'yy-mm-dd' , defaultDate: new Date()});

    $("#deposit_pay_channel_option").change(function () {
        var data = $("option:selected", $(this)).data("description");

        if (data != undefined && data.length > 0) {
			$("#bankInfoHolder").html(data.replace(/[\|]+/g, "<br>")).show();
		} else {
            $("#bankInfoHolder").hide();
		}
	});

    $("#deposit_pay_channel").change(function () {
        var payMethod = $(this).val();

        var holder = $(".deposit_pay_channel_holder");
        holder.hide();
//        $("input, select, textarea", holder).prop("disabled", true);

        holder = $("#deposit_pay_channel_" + payMethod);
//        $("input, select, textarea", holder).prop("disabled", false);
        holder.show();

        // Always enable this input as it use to upload receipt.
//        $("#file").prop("disabled", false);

        if (payMethod != 'BNK') {
            loadPgBankOptions(payMethod);
		}
    }).change();
});

function loadPgBankOptions(pgCode) {
    var payChannelOptionObj = $("#deposit_pay_channel_option");
    var bankSelectLoadingObj = $("#bankLoading");

    payChannelOptionObj.hide();
    bankSelectLoadingObj.show();

    $.ajax({
        url: "{{ route('pg-bank-details') }}",
        type: "GET",
        dataType: "JSON",
        data: {
            _token: "{{ csrf_token() }}",
            deposit_pay_channel: pgCode,
			type: "deposit"
        },
        success: function (result) {
            if (typeof result != undefined && result != null) {
                var resultHtml = "";

                for (var key in result.code) {
                    if (result.code.hasOwnProperty(key)) {
                        var desc = '{{ Lang::get('public.BankAccountNo') }}: ' + result.code[key].bankaccno + "|" +
                            '{{ Lang::get('public.BankAccountName') }}: ' + result.code[key].bankaccname + "|";

                        resultHtml += '<option value="' + key + '" data-description="' + desc + '">' + result.code[key].bankname + '</option>';
                    }
                }

                payChannelOptionObj.html(resultHtml).change();
            }
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            // Do nothing.
        },
		complete: function(json){
            payChannelOptionObj.show();
            bankSelectLoadingObj.hide();
        }
    });
}

$('.caro').slick({
  infinite: true,
  slidesToShow: 2,
  slidesToScroll: 2,
  autoplay: true,
  variableWidth: true,
  arrows: false
});
</script>
@stop