@extends('front/master')

@section('title', 'Withdraw')

@section('top_js')
 <link href="{{url()}}/front/resources/css/acct_management.css" rel="stylesheet">
<script>
	setInterval(update_mainwallet, 10000);
	function submit_transfer(){
	$.ajax({
			 type: "POST",
			 url: "{{route('withdraw-process')}}?"+$('#withdraw_form').serialize(),
			 data: {
				_token: 		 "{{ csrf_token() }}",
			 },
			 beforeSend: function(){
				$('#withdraw_btn_submit').attr('onClick','');
				$('#withdraw_btn_submit').html('Loading...');
			 },
			 success: function(json){
					obj = JSON.parse(json);
					 var str = '';
					 $.each(obj, function(i, item) {
						
						if( '{{Lang::get('COMMON.SUCESSFUL')}}' == item ){
							alert('{{Lang::get('COMMON.SUCESSFUL')}}');
							window.location.href = "{{route('transaction')}}";
						}else{
//							str += item + '<br>';
							str += item + '\n';
						}
					});

					 if (str.length > 0) {
//                         $('.failed_message').html(str);
                         alert(str);
					 }

					$('#withdraw_btn_submit').attr('onClick','submit_transfer()');
					$('#withdraw_btn_submit').html('{{Lang::get('public.Submit')}}');
				
			}
		});
	} 
</script>
@stop

@section('content')

@include('front/transaction_mange_top', [ 'title' => Lang::get('public.Withdrawal') ] )
<div class="acctContent">
	<div class="depLeft">
		<div class="cont">
			<!--MID SECTION-->
			<?php echo htmlspecialchars_decode($content); ?>
			<!--MID SECTION-->
		</div>
	</div>
<form id="withdraw_form">
	<div class="depRight">
		<span class="wallet_title">{{Lang::get('public.Withdrawal')}}</span>
			<div class="acctRow">
				<label>{{Lang::get('public.Balance')}} ({{ Session::get('user_currency') }}) :</label> {{App\Http\Controllers\User\WalletController::mainwallet()}}
				<div class="clr"></div>
			</div>
			<div class="acctRow">
				<label>{{Lang::get('public.Amount')}} ({{ Session::get('user_currency') }}) * :</label><input type="text" name="amount"/>
				<div class="clr"></div>
			</div>
			<div class="acctRow">
				<label>{{Lang::get('public.BankName')}}* :</label>
					<select name="bank">
						@foreach( $banklists as $bank => $detail  )
						<option value="{{$detail['code']}}">{{$bank}}</option>
						@endforeach
					</select>
				<div class="clr"></div>
			</div>
			<div class="acctRow">
				<label>{{Lang::get('public.FullName')}} * :</label><span>{{Session::get('fullname')}}</span>
				<div class="clr"></div>
			</div>
			<div class="acctRow">
				<label>{{Lang::get('public.BankAccountNo')}} :</label><input type="text" name="accountno" />
				<div class="clr"></div>
			</div>

			@if (Session::get('currency') == 'TWD')
				<div class="acctRow">
					<label> {{Lang::get('public.WithdrawalCode')}}* :</label><input type="password" id="withdrawalcode" name="withdrawalcode" />
					<div class="clr"></div>
				</div>
			@endif
		<br>
		
		<div class="submitAcct">
			<a id="withdraw_btn_submit" href="javascript:void(0)" onClick="submit_transfer()">{{Lang::get('public.Submit')}}</a>
		</div>
		<span class="failed_message acctTextReg" style="display:block;float:left;height:100%;"></span>
	</div>
</form>
</div>
@stop

@section('bottom_js')
<script src="{{url()}}/front/resources/js/slick.min.js"></script>
<script>
$('.caro').slick({
  infinite: true,
  slidesToShow: 2,
  slidesToScroll: 2,
  autoplay: true,
  variableWidth: true,
  arrows: false
});
</script>
@stop