@extends('../front/master')

@if (Session::get('currency') == 'TWD')
	@section('title', '無極限娛樂網')
@else
	@section('title', Lang::get('public.SportsBook'))
	@section('keywords', 'football betting,sports betting')
	@section('description', 'Gambling and bet on football at trusted Malaysia online sports betting site, Infiniwin. Win big today!')
@endif

@section('content')
	<iframe name="sportFrame" id="" width="1024px" height="855px" frameborder="0" src="{{$login_url}}"></iframe>
@stop

