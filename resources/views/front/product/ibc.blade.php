@extends('../front/master')

@if (Session::get('currency') == 'TWD')
	@section('title', '無極限娛樂網')
@else
	@section('title', Lang::get('public.SportsBook'))
	@section('keywords', 'football betting,sports betting')
	@section('description', 'Gambling and bet on football at trusted Malaysia online sports betting site, Infiniwin. Win big today!')
@endif

@section('content')
@include('front/sport_top')
@if (!Auth::user()->check())
	<iframe id="ContentPlaceHolder1_iframe_game" width="1024px" height="655px" frameborder="0" src="//mkt.avxsport.com/NewIndex?lang={{$lang}}"></iframe>
@else
	<?php
		$useragent=$_SERVER['HTTP_USER_AGENT'];
		if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4)))
		{
			header('Location: http://ismart.'.$website.'/deposit_ProcessLogin.aspx?lang='.$lang.'&st='.$token);
		}
		else
		{
			//echo '<iframe id="ContentPlaceHolder1_iframe_game" width="1024px" height="655px" frameborder="0" src="http://mkt.'.$website.'/Deposit_ProcessLogin.aspx?lang='.$lang.'"></iframe>';
			echo '<iframe id="ContentPlaceHolder1_iframe_game" width="1024px" height="655px" frameborder="0" src="//www.avxsport.com/ibc?token='.$token.'&lang='.$lang.'"></iframe>';
		}
		
	?>
@endif

<div class="clr"></div>
@if (Session::get('currency') == 'MYR')
<div class="seo_content">
    <h1 style="font-size:13px;">Learn These 3 Simple Sportsbook Tips That Will Change Your World</h1>
    <p>It is actually not that difficult to win a few wagers on sports betting. If you bet on all the possible outcomes, you are surely going to hit some of them right. However, this is not the best way for you to win big and get rich doing sports betting. These three simple sportsbook tips will surely help you achieve that target.</p>
    <p>Firstly, you must manage your expectations. The reality is, there are lots and lots of people are losing money from sports betting. Some people believe that they know everything about the sports and that’s the reason to win a lot from their sport bets. They do not consider the variables that might affect the outcome of the match, and the variables are virtually endless. You must place your bets by keeping in mind that you might lose it. This way, you will be more mindful, you will put more research before putting your bets, and you will be more observant on the amount of money you are putting on the line.</p>
    <p>Secondly, you must be selective. These days, you can bet on almost everything and the bet conditions are even more so. You must be focused on what sports that you want to bet on. It does not necessarily mean that you can only bet on one sport; it’s just that you must focus on the sports that you have the most knowledge on. After that, you will then choose the best bet conditions that will have the best chance to win. Do not spread all your money to multiple bets. You will surely lose more than you gain.</p>
    <p>Lastly, you must play the IBC Sport sportsbook in Infiniwin. This is because Infiniwin is the best, secured and most trusted sportsbook website in Malaysia. Do not place your bets on just any sportsbook websites. They might run away with your hard earned money and disappear without trace. It is also best to pick a sportsbook website with an active customer service. Winning FT’s sportsbook in Infiniwin fulfill these conditions and they are more than able to provide the best sportsbook experience to their customers base.</p>
</div>
@endif

@stop

