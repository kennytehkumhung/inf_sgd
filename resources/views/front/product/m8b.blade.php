@extends('../front/master')

@if (Session::get('currency') == 'TWD')
    @section('title', '無極限娛樂網')
@else
    @section('title', Lang::get('public.SportsBook'))
    @section('keywords', 'football betting,sports betting')
    @section('description', 'Gambling and bet on football at trusted Malaysia online sports betting site, Infiniwin. Win big today!')
@endif

@section('content')
@if (Session::get('currency') == 'MYR')
@include('front/sport_top')
@endif
	@if (Auth::user()->check())

		<iframe id="" width="1040px" height="855px" frameborder="0" src="{{$login_url}}"></iframe>
	@else
        <?php
        $lang = collect(array(
            'en' => 'EN-US',
            'cn' => 'ZH-CN',
            'id' => 'ID-ID',
            'th' => 'TH-TH',
        ));
        ?>
		<iframe name="sportFrame" id="" width="1040px" height="855px" frameborder="0" src="//odds.mywinday.com/Main.aspx"></iframe>

	@endif

@stop
