@extends('front/master')

@section('title', 'Live Casino')

@section('top_js')

 <link href="{{url()}}/front/resources/css/otherPg.css" rel="stylesheet">
 <script type="text/javascript" language="javascript" src="{{url()}}/front/resources/js/jquery.bxslider.min.js"></script>
 <SCRIPT>
 function callUrl(id,url,limitKey){
     var limitId = $("#" + limitKey).val();

     window.open('{{ route('mxb-casino') }}?g=' + id + '&l=' + limitId + '&c=' + url, 'maxbet');
 }
 </script>
@stop

@section('content')
<div class="acctContainer" style="padding-left:0px;">
      <!--LOBBY MENU-->
      <div class="lobbyMenu">
        <ul>
	      <li><a href="{{route('mxb', [ 'type' => 'casino' , 'category' => 'baccarat' ] )}}">Bacarrat</a></li>
          <li><a href="{{route('mxb', [ 'type' => 'casino' , 'category' => 'roulette' ] )}}">Roulette</a></li>
          <li><a href="{{route('mxb', [ 'type' => 'casino' , 'category' => 'blackjack' ] )}}">Black Jack</a></li>
          <li><a href="{{route('mxb', [ 'type' => 'casino' , 'category' => 'single_player_poker' ] )}}">Poker</a></li>
          <li><a href="{{route('mxb', [ 'type' => 'casino' , 'category' => 'carribean_poker' ] )}}">Carribean Poker</a></li>
          <li><a href="{{route('mxb', [ 'type' => 'casino' , 'category' => 'dragon_tiger' ] )}}">Dragon Tiger</a></li>
          <li><a href="{{route('mxb', [ 'type' => 'casino' , 'category' => 'sicbo' ] )}}">Sic Bo</a></li>
          <li><a href="{{route('mxb', [ 'type' => 'casino' , 'category' => 'wheel_of_fortune' ] )}}">Wheel of Fortune</a></li>
        </ul>
      </div>
      <!--LOBBY MENU-->

     <!--LOBBY-->
		<div class="lobby">	
			<div id="casino_table_wrapper">				
				@foreach( $gamelist as $key => $list)		
				<div class="tableInfo">			
					<div class="roomTitle">{{$list['gameName']}}</div>
						<div class="roomDetails">
						<div class="photo">
							<img width="138" height="182" src="{{$list['image']}}">
						</div>
						<div class="roomInfo">
							Dealer: {{$list['dealerName']}}
							<br>
							<br>
							Currency: {{Session::get('currency')}}
							<br>
							<br>
						
							Table Limit:
							<br>
							<select id="limit_{{$key}}">
							@foreach( $list['limit'] as $url_key => $value)
								<option value="{{$value['limitSetID']}}">{{$value['minBet']}} ~ {{$value['maxBet']}}</option>							
							@endforeach
							</select>
							<br>
							<a class="login_btn" href="#" onClick="callUrl('{{$key}}','{{Crypt::encrypt($list['url'])}}','limit_{{$key}}')">Play Now</a>
						</div>
						</div>						
					</div>								
				@endforeach			
			</div>				
		</div>	
		<!--LOBBY-->   
      
      </div>
@stop

@section('bottom_js')
<script>
    $('.bxslider').bxSlider({
  pagerCustom: '#bx-pager'
});
</script>
<script src="{{url()}}/front/resources/js/slick.min.js"></script>
<script>
$('.caro').slick({
  infinite: true,
  slidesToShow: 2,
  slidesToScroll: 2,
  autoplay: true,
  variableWidth: true,
  arrows: false
});
</script>
@stop