@extends('front/master')

@if (Session::get('currency') == 'TWD')
    @section('title', '無極限娛樂網')
@elseif (Session::get('currency') == 'MYR')
    @section('title', 'Best Online Live Casino Malaysia - How To Join')
    @section('keywords', 'Best Online Live Casino Malaysia - How To Join')
    @section('description', 'Press the \'join now\' button on the site which will bring you to InfiniWin Online Casino registration form. Simply enter your details and then click \'submit\'.')
@endif

@section('top_js')
<link href="{{url()}}/front/resources/css/otherPg.css" rel="stylesheet">
@stop

@section('content')
<!--MID SECTION-->
<?php echo htmlspecialchars_decode($content); ?>
<!--MID SECTION-->
@stop

@section('bottom_js')

@stop