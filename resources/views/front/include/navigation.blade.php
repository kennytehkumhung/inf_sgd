<div class="headerMain">
	@if (Lang::locale() == 'tw')
		<div class="headerLeft"  style="background-image: url({{url()}}/front/img/tw/logo.png);"></div>
	@else
            <a href="{{ route('homepage') }}"><div class="headerLeft"  style="background-image: url({{url()}}/front/img/logo-white.png);"></div></a>

		<a href="{{ route('switchwebtype') }}?type=m"><div style="background-image: url('{{url()}}/front/img/mobile_web.png');
					background-size: 53px 70px;
					display: inline-block;
					float: left;
					height: 70px !important;
					padding-top: 1px;
					position: relative;
					width: 53px !important;
					margin-left:10px;
					">

			</div></a>
	@endif
	
	<div class="headerRight">
		<!-- Date Time Chat -->
		<div class="headerDTT">
			<table width="auto" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td>
						<div class="lchat">
							@if (Lang::getLocale() == 'tw')
								<a href="#" onClick="livechat()" ><img src="{{url()}}/front/img/tw/lchat-icon-hover-{{Lang::getLocale()}}.png" alt="" width="121" height="29" id="lchat"></a>
							@else
								<a href="#" onClick="livechat()" ><img src="{{url()}}/front/img/lchat-icon-hover.png" alt="" width="121" height="29" id="lchat"></a>
							@endif
						</div>
					</td>
					<td id="sys_date">
						<!--30 Mar 2015, Mon, 15:03:12 (GMT +8)-->
						<?php echo date("d M Y");?>,
						<?php echo substr(date("l"),0,3);?>,
						<?php echo date(" H:i:s", time());?> (GMT +8)
					</td>
				</tr>
			</table>
		</div>
		<!-- Date Time Chat -->
		<div class="headerTop">
			<table width="auto" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td>
						<ul>
							<li style="display: {{ Session::get('currency') == 'TWD' ? 'none' : 'block' }};">
							
								<div class="lgCont">
									<a href="{{route( 'language', [ 'lang'=> Lang::getLocale()])}}">
										<img src="{{url()}}/front/img/{{Lang::getLocale()}}.png" width="22px"/> 
									</a>
								</div>	
								<center>
								<ul>		
								@if( Session::get('currency') == 'MYR' )
									@if( Lang::getLocale() != 'en' )
									<li>
										<a href="{{route( 'language', [ 'lang'=> 'en'])}}">
											<img src="{{url()}}/front/img/en.png" width="22px"/>		
										</a>
									</li>	
									@endif
									@if( Lang::getLocale() != 'cn' )
									<li>
										<a href="{{route( 'language', [ 'lang'=> 'cn'])}}">
											<img src="{{url()}}/front/img/cn.png" width="22px"/>
										</a>
									</li>
									@endif
									@if( Lang::getLocale() != 'vi' )
									<li>
										<a href="{{route( 'language', [ 'lang'=> 'vi'])}}">
											<img src="{{url()}}/front/img/vi.png" width="22px"/>		
										</a>
									</li>	
									@endif
									@if( Lang::getLocale() != 'id' )
									<li>
										<a href="{{route( 'language', [ 'lang'=> 'id'])}}">
											<img src="{{url()}}/front/img/id.png" width="22px"/>
										</a>
									</li>	
									@endif
								@elseif( Session::get('currency') == 'VND' )
									@if( Lang::getLocale() != 'vi' )
									<li>
										<a href="{{route( 'language', [ 'lang'=> 'vi'])}}">
											<img src="{{url()}}/front/img/vi.png" width="22px"/>		
										</a>
									</li>	
									@endif
									@if( Lang::getLocale() != 'en' )
									<li>
										<a href="{{route( 'language', [ 'lang'=> 'en'])}}">
											<img src="{{url()}}/front/img/en.png" width="22px"/>		
										</a>
									</li>	
									@endif
									@if( Lang::getLocale() != 'cn' )
									<li>
										<a href="{{route( 'language', [ 'lang'=> 'cn'])}}">
											<img src="{{url()}}/front/img/cn.png" width="22px"/>
										</a>
									</li>
									@endif
									
								@elseif( Session::get('currency') == 'IDR' )
									@if( Lang::getLocale() != 'id' )
									<li>
										<a href="{{route( 'language', [ 'lang'=> 'id'])}}">
											<img src="{{url()}}/front/img/id.png" width="22px"/>		
										</a>
									</li>	
									@endif
									@if( Lang::getLocale() != 'en' )
									<li>
										<a href="{{route( 'language', [ 'lang'=> 'en'])}}">
											<img src="{{url()}}/front/img/en.png" width="22px"/>		
										</a>
									</li>	
									@endif
								@endif
								</ul>	
								</center>
							</li>								
						</ul>
					</td>
					@if (!Auth::user()->check())
					<td>
						<div class="usrPw">
							<input type="text" placeholder="{{Lang::get('public.Username')}}" id="username" onKeyPress="enterpressalert(event, this)">
						</div>
					</td>
					<td>
						<div class="usrPw1">
							<input type="password" placeholder="{{Lang::get('public.Password')}}" id="password" onKeyPress="enterpressalert(event, this)">
						</div>
					</td>
					<input hidden type="text" id="code" value="aaaa">
					<!--<td>
						<div class="usrPw2">
							
						</div>
					</td>
					<td>
						<div class="usrPw2" style="margin-left:5px;">
							<img style="float: left;"  src="{{route('captcha', ['type' => 'login_captcha'])}}" width="60" height="24" />
						</div>
					</td>-->
					<td>
						<div class="fP">
							<a style="color:white;text-decoration: none;" href="{{route('forgotpassword')}}">{{Lang::get('public.ForgotPassword')}}</a>
						</div>
					</td>
					<td>
						<div class="submitHeader" id="login">
							<a onclick="login()" id="go" href="#">{{Lang::get('public.Submit')}}</a>
						</div>
						<div class="submitHeader" id="loading" style="display:none;">
							<a id="go" href="#">{{ Lang::get('public.Loading') }}</a>
						</div>
					</td>
					<td><div class="joinHeader"><a href="{{route('register')}}">{{Lang::get('public.JoinNow')}}</a></div></td>
					@else
					<td>
						<span class="spacing1">
							<i class="fa fa-user"></i>{{Lang::get('public.Welcome')}}! {{Session::get('username')}} 
						</span>
					</td>
				<!-- clickable navigation menu-->
					<td>
						<div class="click-nav3">
							<ul class="no-js">
								<li>
									<a class="clicker3">{{Lang::get('public.Profile')}}&nbsp;<b id="totalMessage2"></b><i class="fa fa-caret-down"></i></a>
									<ul>
										<li><a href="{{route('update-profile')}}">{{Lang::get('public.MyAccount')}}</a></li>
										<li><a href="javascript:void(0);" onclick="inbox();">{{Lang::get('public.Inbox')}}&nbsp;<b id="totalMessage"></b></a></li>
										<li><a href="{{route('update-password')}}">{{Lang::get('public.ChangePassword')}}</a></li>
										<li><a href="{{route('profitloss')}}">{{Lang::get('public.BetHistory')}}</a></li>
									</ul>
								</li>
							</ul>
						</div>
					</td>
					<td>
						<div class="click-nav2">
							<ul class="no-js">
								<li>
									<a class="clicker2">{{Lang::get('public.Balance')}}<i class="fa fa-caret-down"></i></a>
									<div class="wlt">
										<table width="auto" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td align="center" style="color: yellow;font-size: 14px;">
													<strong>{{Lang::get('public.Wallet')}}:</strong>
												</td>
												<td>&nbsp;</td>
												<td align="center" style="color: yellow;font-size: 14px;">
													<strong>{{Lang::get('public.Balance')}}:</strong>
												</td>
											</tr>
											<tr>
												<td align="center">{{Lang::get('public.MainWallet')}}</td>
												<td>&nbsp;</td>
												<td align="center" class="main_wallet">0.00</td>
											</tr>
											@foreach( Session::get('products_obj') as $prdid => $object)
                                                <?php
                                                $showPrd = true;
                                                if (Session::get('currency') == 'VND' && ($object->code == 'JOK')) {
                                                    $showPrd = false;
                                                }
                                                ?>
                                                @if ($showPrd)
                                                    <tr>
														@if (Session::get('currency') == 'TWD')
                                                        	<td align="center">{!! str_replace(' (', '<br> (', $object->getNameWithMultiLang('tw')) !!}</td>
														@else
                                                        	<td align="center">{{$object->name}}</td>
														@endif
                                                        <td>&nbsp;</td>
                                                        <td align="center" class="{{$object->code}}_balance">0.00</td>
                                                    </tr>
                                                @endif
											@endforeach
											<tr>
												<td align="center" style="color: yellow;font-size: 14px;"><strong>{{Lang::get('public.Balance')}}:</strong></td>
												<td>&nbsp;</td>
												<td align="center" style="color: red;font-size: 14px; border-bottom: 1px solid red;border-top: 1px solid red;"><strong id="total_balance">00.00</strong></td>
											</tr>
										</table>
										<div class="clr"></div>
									</div>
								</li>
							</ul>
						</div>
					</td>
					<td>
						<div class="click-nav1">
							<ul class="no-js">
								<li>
									<a class="clicker1">{{Lang::get('public.Fund')}}<i class="fa fa-caret-down"></i></a>
									<ul>
										<li><a href="{{route('deposit')}}">{{Lang::get('public.Deposit')}}</a></li>
										<li><a href="{{route('withdraw')}}">{{Lang::get('public.Withdrawal')}}</a></li>
										<li><a href="{{route('transaction')}}">{{Lang::get('public.TransactionHistory')}}</a></li>
										<li><a href="{{route('transfer')}}">{{Lang::get('public.Transfer')}}</a></li>
									</ul>
								</li>
							</ul>
						</div>
					</td>
					<td>
						<div class="submitHeader">
							<a href="{{route('logout')}}">{{Lang::get('public.Logout')}}</a>
						</div>
					</td>
				@endif
				</tr>
			</table>
		</div>
	</div>
</div>