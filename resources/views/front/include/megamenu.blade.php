<?php $gameStatusIconPath = (Session::get('currency') == 'TWD' ? url().'/front/img/tw/' : url().'/front/img/') ?>

<ul class="megamenu">
	<li>
		<a href="{{route('homepage')}}">{{Lang::get('public.Home')}}</a>
		<div style="width: 500px;" class="dropPost">         
			<table border="0" cellpadding="0" cellspacing="0" id="tabular-content">
				<tr>
					<td>
						@if (Session::get('currency') == 'TWD')
							<a href="{{route('banking')}}" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('banking','','{{url()}}/front/img/tw/home-banking-icon-hover.png',0)"><img src="{{url()}}/front/img/tw/home-banking-icon.png" alt="" width="93" height="111" id="banking"></a>
						@else
							<a href="{{route('banking')}}" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('banking','','{{url()}}/front/img/home-banking-icon-hover.png',0)"><img src="{{url()}}/front/img/home-banking-icon.png" alt="" width="93" height="111" id="banking"></a>
						@endif
					</td>
					<td>
						@if (Session::get('currency') == 'TWD')
							<a href="{{route('contactus')}}" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contact','','{{url()}}/front/img/tw/home-contact-icon-hover.png',0)"><img src="{{url()}}/front/img/tw/home-contact-icon.png" alt="" width="93" height="111" id="contact"></a>
						@else
							<a href="{{route('contactus')}}" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contact','','{{url()}}/front/img/home-contact-icon-hover.png',0)"><img src="{{url()}}/front/img/home-contact-icon.png" alt="" width="93" height="111" id="contact"></a>
						@endif
					</td>
					<td>
						@if (Session::get('currency') == 'TWD')
							<a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('chat','','{{url()}}/front/img/tw/home-chat-icon-hover.png',0)" onClick="livechat()"><img src="{{url()}}/front/img/tw/home-chat-icon.png" alt="" width="93" height="111" id="chat"></a>
						@else
							<a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('chat','','{{url()}}/front/img/home-chat-icon-hover.png',0)" onClick="livechat()"><img src="{{url()}}/front/img/home-chat-icon.png" alt="" width="93" height="111" id="chat"></a>
						@endif
					</td>
					@if ( !Auth::user()->check() )
					<td>
						@if (Session::get('currency') == 'TWD')
							<a href="{{route('register')}}" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('register','','{{url()}}/front/img/tw/home-register-icon-hover.png',0)"><img src="{{url()}}/front/img/tw/home-register-icon.png" alt="" width="93" height="111" id="register"></a>
						@else
							<a href="{{route('register')}}" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('register','','{{url()}}/front/img/home-register-icon-hover.png',0)"><img src="{{url()}}/front/img/home-register-icon.png" alt="" width="93" height="111" id="register"></a>
						@endif
					</td>
					@endif
				</tr>
			</table>
		</div>
	</li>
	<li>
		<a href="{{route('livecasino')}}">{{Lang::get('public.LiveCasino')}}</a>
		<div style="width: 500px;" class="dropPost2">
			<table border="0" cellpadding="0" cellspacing="0" id="tabular-content" style="margin-left: 50px;">
				<tr style="cursor:pointer;" class="lc_gameroom">
					@if (Session::get('currency') == 'TWD')
						@if(in_array('ALB',Session::get('valid_product')))
							<td>
								<a href="" onClick="@if (Auth::user()->check())window.open('{{route('alb')}}', 'casino13');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('coming','','{{url()}}/front/img/lc-coming-hover.png',0)"><img src="{{url()}}/front/img/lc-coming-.png" width="93" height="111" alt="" id="coming"/></a>
							</td>
						@endif
						@if(in_array('VGS',Session::get('valid_product')))
							<td>
								<a href="" onClick="@if (Auth::user()->check())window.open('{{route('vgs')}}', 'casino14');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('vgs','','{{url()}}/front/img/lc-el-icon-hover.png',0)"><img src="{{url()}}/front/img/lc-el-icon.png" width="93" height="111" alt="" id="vgs"/></a>
							</td>
						@endif
						@if(in_array('MXB',Session::get('valid_product')))
							<td>
								<a onClick="@if (!Auth::user()->check())alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif" href="@if (Auth::user()->check()){{route('mxb', [ 'type' => 'casino' , 'category' => 'baccarat' ] )}}@endif" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('xp','','{{url()}}/front/img/lc-xp-icon-hover.png',0)"><img src="{{url()}}/front/img/lc-xp-icon.png" alt="" width="93" height="111" id="xp"></a>
							</td><br>
						@endif
						@if(in_array('HOG',Session::get('valid_product')))
							<td>
								<a href="" onClick="@if (Auth::user()->check())window.open('{{route('hog')}}', 'casino15');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('hog','','{{url()}}/front/img/lc-hg-icon-hover.png',0)"><img src="{{url()}}/front/img/lc-hg-icon.png" width="93" height="111" alt="" id="hog"/></a>
							</td>
						@endif
					@else
						@if(in_array('AGG',Session::get('valid_product')))
						<td>
							<a onclick="@if (Auth::user()->check())window.open('{{route('agg')}}', 'casino11');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('ag','','{{url()}}/front/img/lc-ag-icon-hover.png',0)"><img src="{{url()}}/front/img/lc-ag-icon.png" alt="" width="93" height="111" id="ag"></a>
						</td>
						@endif
						@if(in_array('PLT',Session::get('valid_product')))
						<td>
							<a onClick="@if (Auth::user()->check())window.open('{{route('pltiframe')}}', 'casino13');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('pt','','{{url()}}/front/img/lc-pt-icon-hover.png',0)"><img src="{{url()}}/front/img/lc-pt-icon.png" alt="" width="93" height="111" id="pt"></a>
						</td>
						@endif
						@if(in_array('MXB',Session::get('valid_product')))
						<td>
							<a onClick="@if (!Auth::user()->check())alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif" href="@if (Auth::user()->check()){{route('mxb', [ 'type' => 'casino' , 'category' => 'baccarat' ] )}}@endif" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('xp','','{{url()}}/front/img/lc-xp-icon-hover.png',0)"><img src="{{url()}}/front/img/lc-xp-icon.png" alt="" width="93" height="111" id="xp"></a>
						</td><br>
						@endif
						@if(in_array('ALB',Session::get('valid_product')))
						<td>
							<a href="" onClick="@if (Auth::user()->check())window.open('{{route('alb')}}', 'casino13');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('coming','','{{url()}}/front/img/lc-coming-hover.png',0)"><img src="{{url()}}/front/img/lc-coming-.png" width="93" height="111" alt="" id="coming"/></a>
						</td>
						@endif
						@if(in_array('VGS',Session::get('valid_product')))
						<td>
							<a href="" onClick="@if (Auth::user()->check())window.open('{{route('vgs')}}', 'casino14');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('vgs','','{{url()}}/front/img/lc-el-icon-hover.png',0)"><img src="{{url()}}/front/img/lc-el-icon.png" width="93" height="111" alt="" id="vgs"/></a>
						</td>
						@endif
						@if(in_array('HOG',Session::get('valid_product')))
						<td>
							<a href="" onClick="@if (Auth::user()->check())window.open('{{route('hog')}}', 'casino15');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('hog','','{{url()}}/front/img/lc-hg-icon-hover.png',0)"><img src="{{url()}}/front/img/lc-hg-icon.png" width="93" height="111" alt="" id="hog"/></a>
						</td>
						@endif
						@if(Session::get('currency')=='MYR')
						@if(in_array('WMC',Session::get('valid_product')))
						<td>
							<a href="" onClick="@if (Auth::user()->check())window.open('{{route('wmcframe')}}', 'casino15');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('WMC','','{{url()}}/front/img/lc-wm-icon-hover.png',0)"><img src="{{url()}}/front/img/lc-wm-icon2.png" width="93" height="111" alt="" id="wmc"/></a>
						</td>
						@endif
						 @endif
						{{--@if(in_array('OPU',Session::get('valid_product')))--}}
						{{--<td>--}}
							{{--<a href="" onClick="@if (Auth::user()->check())window.open('{{route('opu', [ 'type' => 'casino' , 'category' => 'live'])}}', 'casino16');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('opu','','{{url()}}/front/img/lc-op-icon-hover.png',0)"><img src="{{url()}}/front/img/lc-op-icon.png" width="93" height="111" alt="" id="opu"/></a>--}}
						{{--</td>--}}
						{{--@endif--}}
					@endif
				</tr>
			</table>
		</div>
	</li>
	<li>
		@if (Session::get('currency') == 'TWD')
			<a href="{{route('sport')}}">{{Lang::get('public.SportsBook')}}</a>
		@else
                        <img src="{{$gameStatusIconPath}}new.png" style="position:absolute;margin-top:-20px;margin-left:34px;">
			<a href="{{route('ibc')}}">{{Lang::get('public.SportsBook')}}</a>
                        <div style="width: 500px;" class="dropPost3">
                            <table border="0" cellpadding="0" cellspacing="0" id="tabular-content">
                                <tr>
                                    <td>
                                        <a href="{{route('ibc')}}"><img src="{{url()}}/front/img/isport_top.png" alt="" id="ibc"></a>
                                    </td>
                                    <td>
                                        <a href="{{route('m8b')}}"><img src="{{url()}}/front/img/msport_top.png" alt="" id="m8b"></a>
                                    </td>
                                </tr>
                            </table>
                        </div>
		@endif
	</li>
	<li>
		<a href="{{route('slot', [ 'type' => 'mxb'  ])}}">{{Lang::get('public.Slots')}}</a>
		<div style="width: 500px;" class="dropPost3">
			<table border="0" cellpadding="0" cellspacing="0" id="tabular-content">
				<tr>
					@if(in_array('MXB',Session::get('valid_product')))
					<td>
						<a href="{{route('slot', [ 'type' => 'mxb'  ])}}" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('bt','','{{url()}}/front/img/slt-bt-icon-hover.png',0)"><img src="{{url()}}/front/img/slt-bt-icon.png" alt="" width="161" height="110" id="bt"></a>
					</td>
					@endif
					@if(in_array('SPG',Session::get('valid_product')))
						<td>
							<a href="{{route('slot', [ 'type' => 'spg'  ])}}" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('spgslot','','{{url()}}/front/img/slt-sp-icon-hover.png',0)"><img src="{{url()}}/front/img/slt-sp-icon.png" alt="" width="161" height="110" id="spgslot"></a>
						</td>
					@endif
					@if(in_array('PLT',Session::get('valid_product')))
					<td>
						<a href="{{route('slot', [ 'type' => 'plt'  ])}}" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('pltslot','','{{url()}}/front/img/slt-pt-icon-hover.png',0)"><img src="{{url()}}/front/img/slt-pt-icon.png" alt="" width="161" height="110" id="pltslot"></a>
					</td>
					@endif
					@if(in_array('JOK',Session::get('valid_product')))
					<td>
						<a href="javascript:void(0);" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('jokslot','','{{url()}}/front/img/slt-joker-icon-hover.png',0)"><img src="{{url()}}/front/img/slt-joker-icon.png" alt="" width="161" height="110" id="jokslot" onclick="@if (Auth::user()->check())window.open('{{route('jok')}}','jok_slot')@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif"></a>
					</td>
					@endif
					@if(in_array('SKY',Session::get('valid_product')))
					<td>
						<a href="{{route('slot', [ 'type' => 'sky'  ])}}" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('skyslot','','{{url()}}/front/img/slt-scr-icon-hover.png',0)"><img src="{{url()}}/front/img/slt-scr-icon.png" alt="" width="161" height="110" id="skyslot"></a>
					</td>
					@endif
					@if(in_array('UC8',Session::get('valid_product')))
					<td>
						<a href="{{route('slot', [ 'type' => 'uc8'  ])}}" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('uc8slot','','{{url()}}/front/img/slt-uc-icon-hover.png',0)"><img src="{{url()}}/front/img/slt-uc-icon.png" alt="" width="161" height="110" id="uc8slot"></a>
					</td>
					@endif
				</tr>
			</table>
		</div>
	</li>
	@if( Session::get('currency') == "MYR" )
	<li >
		<img src="{{$gameStatusIconPath}}hot.png" style="position:absolute;margin-top:-20px;margin-left:34px;">
		<a href="{{route('fortune')}}">Fortune 4D</a>
	</li>
	<li>
		<a href="" onClick="@if (Auth::user()->check())window.open('{{route('ctb')}}', 'ctb');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif">{{ Lang::get('public.Racebook') }}</a>
	</li>
	<li>
		<a href="{{route('4d')}}">4D</a>
	</li>
	@elseif( Session::get('currency') == "TWD" )
		<li>
			<img src="{{$gameStatusIconPath}}new.png" style="position:absolute;margin-top:-20px;margin-left:34px;">
			<a href="" onClick="@if (Auth::user()->check())window.open('{{route('ctb')}}', 'ctb');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif">{{ Lang::get('public.Racebook') }}</a>
		</li>
	@endif
	<li>
		<a href="{{route('mobile')}}">{{ Lang::get('public.MobileDownload') }}</a>
	</li>
	<li style="margin-right: 0px;">
		<a style="margin-right: 0px;" href="{{route('promotion')}}">{{Lang::get('public.Promotion')}}</a>
	</li>	
	
</ul>
<!--WC 18 Timer-->
<!--<div class="wcInner">
    <div id="getting-started" class="timerDig"></div>
    <script type="text/javascript">
        $("#getting-started")
            .countdown("2018/06/14 23:00:00", function(event) {
            $(this).text(
              event.strftime('%D %H %M %S')
            );
        });
    </script>
</div>-->
<!--WC 18 Timer-->
<div class="anmnt" style="cursor: pointer;" onclick="window.open('{{route('announcement')}}?lang={{Lang::getLocale()}}', 'Notice', 'width=800,height=600');">
		<marquee>{{App\Http\Controllers\User\AnnouncementController::index()}}</marquee>
</div>