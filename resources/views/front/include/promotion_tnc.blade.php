<h3 style="color:#FFDD84;font-size:14px;padding: 5px 0;">
    @if (Lang::getLocale() == 'tw')
        一般促銷條款和條件
    @elseif (Lang::getLocale() == 'cn')
        一般促销条款和条件
    @else
        General Terms &amp; Conditions of Promotions
    @endif
</h3>
<div class="tnc_list" style="height: 160px; overflow-x: auto;">
    @if (Session::get('currency') == 'TWD')
        <ol style="list-style-type:decimal; padding-left:30px;">
            <li>您必须年满18岁以上或在您被司法管辖范围内属成年年龄; 拥有足够心理承受能力为自己的行为负责， 并接受这些条款与条件的约束。無極限娛樂網保留于任何或所有时间取消任何涉及未成年人的交易的权利。</li>
            <li>所有無極限娛樂網提供予客戶的獎金/紅利僅供娛樂或消遣用途。被發現或懷疑為非消遣性玩家（職業賭徒/只為尋求紅利的玩家）或與其他方協作/串通/試圖欺詐的用戶將無資格參與我們的優惠活動。該失去資格的賬戶所獲得的任何紅利與獎金將能在無極限娛樂網的全權決定下被撤銷。</li>
            <li>您只被允許在網站擁有一個活動帳戶。 無極限娛樂網保留監控任何有意建立多個帳戶的權利。在發現你已經註冊了多個帳戶的情況下，所有帳戶將被關閉，恕不另行通知。 無極限娛樂網有權拒絕任何人訪問的權利。所有優惠僅限於每人，每家庭，每家庭住址，每電子郵件地址，每電話號碼，每銀行賬號和每IP地址的一個帳戶。</li>
            <li>所有的優惠活動將嚴格禁止多個賬戶登記與濫用。 無極限娛樂網擁有全權自行決定某行為構成了多個帳戶濫用的權利。</li>
            <li>濫用獎勵優惠. 所有的獎金優惠只限於娛樂性投注。我們有權定義其為職業玩家或者普通玩家，玩家以任何方式濫用獎金優惠將會被撤銷獎金及進一步處理。濫用獎金優惠活動可以定義為（但不僅限於）客戶提款後用於再次存款，或轉入自己的新賬戶使用。其處罰手段會以增加投注額或者扣除獎金金額的形式，其他相關聯賬戶亦將包括其中。如有必要我們有權開設特別的獎金優惠活動。其包括（但不僅限於）設置地域性限制規則以防止濫用優惠獎勵。</li>
            <li>凡在優惠或促銷如不符合任何條款，未達到資格，違反條規，濫用或被發現有一系列由同一個客戶或一客戶群的下注活動（於存款紅利或任何其他優惠導致保證客戶利潤，不論結果如何及無論個別或集體的下注）的任何證據來源的賬戶持有人，無極限娛樂網保留取消，取消或收回紅利惠獎金加上所有的彩金的權利。此外，無極限娛樂網保留向客戶徵收高達存款紅利價值的費用以支付行政費用的權利。</li>
            <li>
                在達到下注交易額（rollover）要求之前，存款金額加上紅利及有關獎金不可被提取。
                <ol type="a" style="padding-left: 20px;">
                    <li>
                        如果用戶已申請取得優惠獎金，但獎金匯入該用戶賬戶之前：
                        <ol type="i" style="padding-left: 20px;">
                            <li>他的賬戶餘額被視為不足以完成獎金的下注交易額（turnover）要求；無極限娛樂網有權要求該用戶補充賬戶餘額或於獎金匯入完成之前做出同樣數額的新存款。</li>
                            <li>他做了提款申請；無極限娛樂網有權拒絕該提款申請，因該情況下會員將無法履行與完成優惠的下注交易額要求。一旦下注交易額要求已達到，用戶可再次提交提款申請。</li>
                        </ol>
                    </li>
                    <li>
                        同樣的， 當優惠獎金匯入了用戶賬戶後，該用戶隨後做的一次或以上的下注將受到優惠下注交易額的約束。無極限娛樂網將不會批准用戶為了進行提款而希望取消或提取獎金的要求。用戶必須首先滿足該優惠下注交易額的要求才能提款。
                    </li>
                </ol>
            </li>
            <li>只有產生勝/輸結果的賭注將計算在下注交易額內以達到下注交易額要求（rollover requirement）。兩方下注，取消/作廢和平局的賭注將不計算在下注交易額內。體育博彩中的數字遊戲（Number Games）賭注亦不計算在下注交易額內。</li>
            <li>低於以下的投注（十進制投注賠率1.5; 香港盤0.5; 馬來盤0.5; 印尼盤-2.0; 美國盤-200）和無效的，平局，取消的投注，或將取得相同結果的雙方下注將不被計算在下注交易額內。</li>
            <li>根據上文第7項條款，一旦獎金已經匯入會員賬戶，無極限娛樂網將有全部權利決定批准或拒絕該成員欲更改優惠的要求或於還未達到下注交易額要求之前的提款申請。任何此情況下給予的批准，該用戶所擁有的存款和提取的獎金/紅利數額將嚴格受到罰款；並且於提出此提款要求前所贏得/累積的任何彩金將被取消。</li>
        </ol>
    @elseif (Lang::getLocale() == 'cn')
        <ol style="list-style-type:decimal; padding-left:30px;">
            <li>您必须年满18岁以上或在您被司法管辖范围内属成年年龄; 拥有足够心理承受能力为自己的行为负责， 并接受这些条款与条件的约束。Infiniwin保留于任何或所有时间取消任何涉及未成年人的交易的权利。</li>
            <li>所有Infiniwin提供予客户的奖金/红利仅供娱乐或消遣用途。被发现或怀疑为非消遣性玩家（职业赌徒/只为寻求红利的玩家）或与其他方协作/串通/试图欺诈的用户将无资格参与我们的优惠活动。该失去资格的账户所获得的任何红利与奖金将能在Infiniwin的全权决定下被撤销。</li>
            <li>您只被允许在网站拥有一个活动帐户。Infiniwin保留监控任何有意建立多个帐户的权利。在发现你已经注册了多个帐户的情况下，所有帐户将被关闭，恕不另行通知。Infiniwin有权拒绝任何人访问的权利。所有优惠仅限于每人，每家庭，每家庭住址，每电子邮件地址，每电话号码，每银行账号和每IP地址的一个帐户。</li>
            <li>所有的优惠活动将严格禁止多个账户登记与滥用。 Infiniwin拥有全权自行决定某行为构成了多个帐户滥用的权利。</li>
            <li>滥用奖励优惠. 所有的奖金优惠只限于娱乐性投注。我们有权定义其为职业玩家或者普通玩家，玩家以任何方式滥用奖金优惠将会被撤销奖金及进一步处理。滥用奖金优惠活动可以定义为（但不仅限于）客户提款后用于再次存款，或转入自己的新账户使用。其处罚手段会以增加投注额或者扣除奖金金额的形式，其他相关联账户亦将包括其中。如有必要我们有权开设特别的奖金优惠活动。其包括（但不仅限于）设置地域性限制规则以防止滥用优惠奖励。</li>
            <li>凡在优惠或促销如不符合任何条款，未达到资格，违反条规，滥用或被发现有一系列由同一个客户或一客户群的下注活动（于存款红利或任何其他优惠导致保证客户利润，不论结果如何及无论个别或集体的下注）的任何证据来源的账户持有人，， Infiniwin 保留取消，取消或收回红利惠奖金加上所有的彩金的权利。此外，Infiniwin 保留向客户征收高达存款红利价值的费用以支付行政费用的权利。</li>
            <li>
                在达到下注交易额（rollover）要求之前，存款金额加上红利及有关奖金不可被提取。
                <ol type="a" style="padding-left: 20px;">
                    <li>
                        如果用户已申请取得优惠奖金，但奖金汇入该用户账户之前：
                        <ol type="i" style="padding-left: 20px;">
                            <li>他的账户余额被视为不足以完成奖金的下注交易额（turnover）要求；Infiniwin有权要求该用户补充账户余额或于奖金汇入完成之前做出同样数额的新存款。</li>
                            <li>他做了提款申请；Infiniwin有权拒绝该提款申请，因该情况下会员将无法履行与完成优惠的下注交易额要求。一旦下注交易额要求已达到，用户可再次提交提款申请。</li>
                        </ol>
                    </li>
                    <li>
                        同样的， 当优惠奖金汇入了用户账户后，该用户随后做的一次或以上的下注将受到优惠下注交易额的约束。Infiniwin将不会批准用户为了进行提款而希望取消或提取奖金的要求。用户必须首先满足该优惠下注交易额的要求才能提款。
                    </li>
                </ol>
            </li>
            <li>只有产生胜/输结果的赌注将计算在下注交易额内以达到下注交易额要求（rollover requirement）。两方下注，取消/作废和平局的赌注将不计算在下注交易额内。体育博彩中的数字游戏（Number Games）赌注亦不计算在下注交易额内。</li>
            <li>低于以下的投注（十进制投注赔率1.5; 香港盘0.5; 马来盘0.5; 印尼盘 -2.0; 美国盘 -200）和无效的，平局，取消的投注，或将取得相同结果的双方下注将不被计算在下注交易额内。</li>
            <li>根据上文第7项条款，一旦奖金已经汇入会员账户，Infiniwin将有全部权利决定批准或拒绝该成员欲更改优惠的要求或于还未达到下注交易额要求之前的提款申请。任何此情况下给予的批准，该用户所拥有的存款和提取的奖金/红利数额将严格受到罚款；并且于提出此提款要求前所赢得/累积的任何彩金将被取消。</li>
        </ol>
    @else
        <ol style="list-style-type:decimal; padding-left:30px;">
            <li>Customer is over the age of 18, or the age of consent in the customer's home jurisdiction, whichever is higher and has the mental capacity to take responsibility for the Customer's own actions and be bound by these terms and conditions. www.infiniwin.net shall reserves the right at any and all times to void any transactions involving minors.</li>
            <li>All bonuses offered are intended for recreation customers only. Accounts identified or suspected as non-recreational play type or wager style or collaboration between parties is not eligible for this promotion and are subject to have any awarded bonuses plus winnings to be revoked at the sole discretion of www.infiniwin.net.</li>
            <li>You are only permitted to have one active account on the Site. www.infiniwin.net reserves the right to monitor any effort to establish multiple accounts. In the event it is discovered that you have opened more than one account, all accounts will be closed without notice. www.infiniwin.net reserves the right to deny access to anyone. All offers is restricted to only one account per individual, family, household address, email address, telephone number, bank account and IP address.</li>
            <li>In all promotions, strict rules will be enforced on multiple accounts abuse. www.infiniwin.net shall in its sole discretion decide on what activity constitutes multiple account abuse.</li>
            <li>Abuse of Bonus Programs: Bonus programs are intended for recreational bettors only. Professional players or players considered, in our sole discretion, to be abusing the bonus system by any means may have bonuses revoked and be subject to further sanctions. Bonus abuse may be defined as (but not restricted to) clients cashing out for the purpose of redepositing, or referring new accounts that they are using themselves. Sanctions may be in the form of increased rollover requirements or loss of bonus privileges altogether for the offending Account as well as any linked Accounts. We reserve the right to restrict eligibility for special offers and bonuses when necessary.</li>
            <li>Where any term of the offers or promotions have not been fulfilled by eligible account holders, are breached, abused or there is any evidence of a series of bets placed by a customer or group of customers, which due to a deposit bonus or any other promotional offer resulted in guaranteed customer profits irrespective of the outcome, whether individually or as part of a group, Infiniwin.net reserves the right to withhold, cancel or reclaim the bonus plus all winnings. In addition Infiniwin.net reserves the right to levy an administration charge on the customer up to the value of the deposit bonus to cover administrative costs.</li>
            <li>
                Before the promotional rollover requirements are met, the deposit amount plus the Bonus and any winnings attributable are not allowed for withdrawal.
                <ol type="a" style="padding-left: 20px;">
                    <li>
                        If a member has applied for a promotion bonus, but before the bonus has been credited in his account:
                        <ol type="i" style="padding-left: 20px;">
                            <li>His account balance is deemed insufficient to complete the bonus turnover requirement; www.infiniwin.net reserves the right to ask the member to top up the balance or make a fresh deposit of the same amount before the credit of the bonus is done.</li>
                            <li>He makes a withdrawal request; www.infiniwin.net reserves the right to reject the withdrawal due to non-fulfillment of the turnover condition of the promotion. A member can make another request for withdrawal once the turnover condition has been satisfied.</li>
                        </ol>
                    </li>
                    <li>
                        Likewise, when a promotion bonus is credited in a member account and he subsequently places a bet or more, www.infiniwin.net will not entertain his request to cancel or withdraw the bonus in order to allow him to make a withdrawal, unless he fulfills the required promotion turnover first.
                    </li>
                </ol>
            </li>
            <li>Only wagers that generate a win/loss return will contribute to the promotional rollover requirement. Cancelled or void wagers or draw bets will not be counted.</li>
            <li>Bets made below (Decimal odd 1.5; Hong Kong odd 0.5; Malay Positive Odds 0.5; Indo Odd -2.0; US Odds -200) and void, tie, cancelled, or made on opposite sides with same outcome will not be counted towards the rollover requirements.</li>
            <li>Subject to Clause 5 above, once a bonus has been credited into a member's account, www.infiniwin.net will have the sole discretion to approve or reject the member's request to change promotion; or to cancel the bonus; or to withdraw without completing the required rollover. Any approval given will be subjected strictly to a penalty being charged on the deposit, the bonus amount withdrawn and any winnings accrued prior to such a request will be voided.</li>
        </ol>
    @endif
</div>
