<div class="footer">
    @if (Session::get('currency') == 'MYR')
	
        <div class="line"></div>
    @endif
	<div class="copyright">
        @if (Session::get('currency') == 'TWD')
		    <strong>著作權 © 無極限娛樂網。版權所有。</strong><br>
        @else
            <strong>Copyright © INFINIWIN. All Rights Reserved.</strong><br>
        @endif
		<ul>
			<li><a href="{{route('aboutus')}}">{{Lang::get('public.AboutUs')}}</a></li>
			<li><a href="{{route('banking')}}">{{Lang::get('public.BankingOptions')}}</a></li>
			<li><a href="{{route('contactus')}}">{{Lang::get('public.ContactUs')}}</a></li>
			<li><a href="{{route('faq')}}">{{Lang::get('public.FAQ')}}</a></li>
			<li><a href="{{route('howtojoin')}}">{{Lang::get('public.HowToJoin')}}</a></li>
			<li><a href="{{route('tnc')}}">{{Lang::get('public.TNC')}}</a></li>
            @if (Session::get('currency') == 'TWD')
                <li><a href="javascript:void(0);" onclick="alert('請聯絡客服專員。');">{{Lang::get('COMMON.AFFILIATE')}}</a></li>
            @else
			    <li><a href="http://affiliate.infiniwin.net">{{Lang::get('COMMON.AFFILIATE')}}</a></li>
            @endif
		</ul>
	</div>
	<div class="social">
		<ul>
			<li><a href="#"><img src="{{url()}}/front/resources/img/twit-icon.png" width="34" height="34" alt=""/></a></li>
			<li><a href="#"><img src="{{url()}}/front/resources/img/fb-icon.png" width="34" height="34" alt=""/></a></li>
		</ul>
	</div>
	<div class="clr"></div>
</div>


  <style>
.closeBtn  {
  cursor: pointer;
  margin-right: -5px;
  right: 0;
  position: absolute;
  z-index: 10000;
}
.closeBtn1  {
  cursor: pointer;
  margin-left: 135px;
  left: 0;
  position: fixed;
  z-index: 10000;
}
.minimizeImg {
    width: 140px;
    height: auto;
}
#minimize3 {
  position: fixed;
  z-index: 9999;
  top: 145px;
  right: 3px;
  width: 117px;
  height: 345px;
}
#minimize4 {
  position: fixed;
  z-index: 9999;
  top: 145px;
  left: 3px;
}
#minimize5 {
  position: fixed;
  z-index: 9999;
  top: 345px;
  left: 3px;
}
#minimize6 {
  position: fixed;
  z-index: 9999;
  top: 545px;
  left: 3px;
}
block1.a{
  display:block;
}
block1.b{
  display:none;
}
block2.a{
  display:block;
}
block2.b{
  display:none;
}

 </style>        
<script>
    function hide(id) {
        document.getElementById(id).setAttribute('class', 'b');
    }
</script>
         
<block1 id="minimize3" class="a">
    <div class="closeBtn" onclick="javascript:hide('minimize3')" style="margin-right: 0px;">
        <img src="{{url()}}/front/resources/img/closeBtn.png" onmouseover="this.src='{{url()}}/front/resources/img/closeBtn_hover.png'" onmouseout="this.src='{{url()}}/front/resources/img/closeBtn.png'">
    </div>

    @if (Session::get('currency') == 'TWD')
    <div style="position: fixed; z-index: 9999; top: 145px; right: 3px; background-image: url({{url()}}/front/img/tw/livechat.png); background-repeat: no-repeat; width: 117px; height: 498px;">
        <div style="width:100%;border:0px solid black;position:relative;top:445px;">
            <div style="cursor:pointer;width:114px;height:20px;" ID="livechatlink" runat="server" onclick="javascript:livechat();"> </div>
            <a style="height: 30px; display: block; width: 107px;" href="{{route('register')}}"><div ></div></a>
        </div>
    </div>
    @else
    <div style="position:fixed;z-index:9999;top:145px;right:3px;background-image:url({{url()}}/front/resources/img/floating_banner2.png);width:117px; height:345px;">
        <div style="width:100%;border:0px solid black;position:relative;top:291px;">
            <div style="cursor:pointer;width:100px;height:20px;" ID="livechatlink" runat="server" onclick="javascript:livechat();"> </div>
                <a style="height: 30px; display: block; width: 107px;" href="{{route('register')}}"><div ></div></a>
        </div>
    </div>
    @endif

</block1>
@if( Session::get('currency') == 'MYR')
<block2 id="minimize4" class="a">
    <div class="closeBtn1" onclick="javascript:hide('minimize4')" style="margin-right: 0px;">
        <img src="{{url()}}/front/resources/img/closeBtn.png" onmouseover="this.src='{{url()}}/front/resources/img/closeBtn_hover.png'" onmouseout="this.src='{{url()}}/front/resources/img/closeBtn.png'">
    </div>
    <a href="http://infiniwin.net/android.apk"><img src="{{url()}}/front/android/images/icon.png" class="minimizeImg"></a>
</block2>

<block2 id="minimize5" class="a">
    <div class="closeBtn1" onclick="javascript:hide('minimize5')" style="margin-right: 0px;">
        <img src="{{url()}}/front/resources/img/closeBtn.png" onmouseover="this.src='{{url()}}/front/resources/img/closeBtn_hover.png'" onmouseout="this.src='{{url()}}/front/resources/img/closeBtn.png'">
    </div>
    <a href="#" onclick="popup()"><img src="{{url()}}/front/{{Lang::get('public.popupICONM')}}" class="minimizeImg"></a>
</block2>

@if (date('Y-m-d') >= '2018-02-16' && date('Y-m-d') <= '2018-03-02' || (in_array(strtolower(Session::get('username')), ['pkchan1987', 'mingda', 'test118'])))
<block2 id="minimize6" class="a">
    <div class="closeBtn1" onclick="javascript:hide('minimize6')" style="margin-right: 0px;">
        <img src="{{url()}}/front/resources/img/closeBtn.png" onmouseover="this.src='{{url()}}/front/resources/img/closeBtn_hover.png'" onmouseout="this.src='{{url()}}/front/resources/img/closeBtn.png'">
    </div>
    @if(Auth::user()->check())
        <a href="{{ route('omt') }}" target="_blank"><img src="{{url()}}/front/resources/img/cny_draw.gif" class="minimizeImg"></a>
    @else
        <a href="javascript:void(0);" onclick="alert('{{ Lang::get('COMMON.PLEASELOGIN') }}');"><img src="{{url()}}/front/resources/img/cny_draw.gif" class="minimizeImg"></a>
    @endif
</block2>
@endif
@endif
<div class="as11" style="display: none;">
<div id="masklayer1" style="height: 100%;position: fixed;opacity: 0.6;background-color: black;z-index: 10001;left: 0px;right: 0px;top: 0px;display:block;" onclick="closePopup();"></div>
<img src="{{url()}}/front/{{Lang::get('public.popupICONMimg')}}" class="popup_img1">
</div>
<style>

</style>
<script>
function popup(){
    $(".as11").show();
    $(".popup_img1").fadeIn(100);
    $("#masklayer1").fadeIn(100);
    $(".popup_img1").css({
        'display': 'inline-block',
        'position': 'absolute',
        'z-index': '10001',
        'margin': 'auto auto',
        'top': '-400px',
        'bottom':'0',
        'left':'0',
        'right': '0'});
}
</script>
