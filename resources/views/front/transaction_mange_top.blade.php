<script>
 $(document).ready(function() { 
getBalance(true);
});
</script>
<!--MID SECTION-->
<div class="midSect">
   <div class="midSectInner">
     <div class="spacingTop"></div>
    
     <h2>{{Lang::get('public.Fund')}} > {{$title}}</h2> 
     <div class="mainWallet">
		 <div class="wLeft">
			 <span class="wTitle">{{Lang::get('public.Mainwallet')}}</span>
			 <span class="wCurrency">{{Session::get('currency')}}</span>
		 </div>		 
		 <div class="wRight main_wallet">
			{{App\Http\Controllers\User\WalletController::mainwallet()}}
		 </div>	
		
		 <div class="clr"></div>
     </div>

	   @if (Session::get('currency') == 'MYR')
	<div class="submitAcct" style="position: absolute;margin-left:690px;;margin-top:60px;">
		<a onclick="getBalance()" href="#"> {{lang::get('public.Refresh')}}</a>
	</div>

     <div class="rewardPts">
		 <div class="wLeft1">
			<span class="wRewardTitle">Reward<br>Points</span>
		 </div>
		 <div class="wRight">
			000
		 </div>
		 <div class="clr"></div>
     </div>
     <div class="clr"></div>
	   @endif

     <div class="walletTable">
		 <ul>	 
			 @foreach( Session::get('products_obj') as $prdid => $object)
                <?php
                $showPrd = true;
                if (Session::get('currency') == 'VND' && ($object->code == 'JOK')) {
                    $showPrd = false;
                }
                ?>
                @if ($showPrd)
                    <li>
                       <span style="height:20px;" class="wItemAmt {{$object->code}}_balance">0.00</span>
						@if (Session::get('currency') == 'TWD')
                       		<span class="wItem" >{{$object->getNameWithMultiLang('tw')}}</span>
						@else
                       		<span class="wItem" >{{$object->name}}</span>
						@endif
                    </li>
                @endif
			  @endforeach
		 </ul>
     </div>
     
		<div class="title_bar">
			<ul>
				@if( $title != Lang::get('public.AccountPassword') && $title != Lang::get('public.AccountProfile'))
				<li @if($title == Lang::get('public.Deposit') )class="selected"@endif ><a href="{{route('deposit')}}">{{Lang::get('public.Deposit')}}</a></li>
				<li @if($title == Lang::get('public.Withdrawal') )class="selected"@endif><a href="{{route('withdraw')}}">{{Lang::get('public.Withdrawal')}}</a></li>
				<li @if($title == Lang::get('public.TransactionHistory') )class="selected"@endif><a href="{{route('transaction')}}">{{Lang::get('public.TransactionEnquiry')}}</a></li>
				<li @if($title == Lang::get('public.WalletTransfer') )class="selected"@endif><a href="{{route('transfer')}}">{{Lang::get('public.Transfer')}}</a></li>
				@else
				<li @if($title == Lang::get('public.AccountProfile') )class="selected"@endif ><a href="{{route('update-profile')}}">{{Lang::get('public.AccountProfile')}}</a></li>
				<li @if($title == Lang::get('public.AccountPassword') )class="selected"@endif ><a href="{{route('update-password')}}">{{Lang::get('public.AccountPassword')}}</a></li>
				@endif
			</ul>   
		</div>
      <!--<div class="acctContent">-->
		<!--<div class="depLeft">
			<div class="cont">
				<ul>
					<li>Option:	Bank Transfer</li>
					<li>Mode:	Offline</li>
					<li>Min/Max Limit:	30.00/50,000.00</li>
					<li>Daily Limit:	500,000.00</li>
					<li>Total Allowed:	500,000.00</li>
				</ul>
			</div>
			<div class="cont">
				<p>
					XX are favourite for our speedy crediting of funds to your account and experience. Thus, please use Bank Transfer via your local bank account. We do not accept all kinds of deposit by "Cheque" or "Bank Draft" (Company OR Personal Cheque) as your deposit method.
				</p>
				<p>
					Note: Once you have successfully submitted your deposit form and once your funds is cleared in our account, just leave it to our team to process your transactions as speedy as possible. If more than 10 minutes, let us know by clicking here and our Customer Service support will assist you 24/7 anytime. 
				</p>
			</div>
		</div>-->
      
<!--MID SECTION-->