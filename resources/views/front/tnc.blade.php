@extends('front/master')

@if (Session::get('currency') == 'TWD')
    @section('title', '無極限娛樂網')
@elseif (Session::get('currency') == 'MYR')
    @section('title', 'Best Online Live Casino Malaysia - Terms & Conditions')
    @section('keywords', 'Best Online Live Casino Malaysia - Terms & Conditions')
    @section('description', 'Find our more about InfiniWin Online Live Casino Malaysia Terms and Conditions.')
@endif

@section('top_js')
<link href="{{url()}}/front/resources/css/otherPg.css" rel="stylesheet">
@stop

@section('content')
<!--MID SECTION-->
<?php echo htmlspecialchars_decode($content); ?>
<!--MID SECTION-->
@stop

@section('bottom_js')

@stop