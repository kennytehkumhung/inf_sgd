@extends('lasvegas/master')
@section('title', 'Profile')
@section('content')
<script>
	$(document).ready(function() { 
		getBalance(true);
	});
	
	 function update_profile(){
			if( $('#dob_year').val() != undefined ){
	
				var dob = $('#dob_year').val()+'-'+$('#dob_month').val()+'-'+$('#dob_day').val();	
			}else{
				
				var dob = '{{ $acdObj->dob }}';
			}
                $.ajax({
			type: "POST",
			url: "{{action('User\MemberController@UpdateDetail')}}",
			data: {
				_token: "{{ csrf_token() }}",
				//dob:		$('#dob').val(),
				gender:    	$('#gender').val(),
                dob:			 dob,
				fullname:	$('#fullname').val(),
				email:		$('#email').val(),
				telmobile:	$('#telmobile').val()

			},
		}).done(function( json ) {
			
				 obj = JSON.parse(json);
				 var str = '';
				 $.each(obj, function(i, item) {
					str += item + '\n';
				})
				
				alert(str);
				
				
		});
	}
	
</script>
    <!-- ======== @Region: #content ======== -->
    <div id="content" class="mt02 noBg">
      <div class="container">
      <div class="row">
      <div class="col-md-12 col-xs-12">
            @include('lasvegas/trans_top' )
            </div>
            </div>
            
            <div class="row">
              <div class="col-md-2 col-xs-12">
                <div class="men1">
                <a class="selected" href="{{route('update-profile')}}">{{Lang::get('public.AccountProfile')}}</a>
                </div>
              </div>
              <div class="col-md-2 col-xs-12">
                <div class="men1">
                <a href="{{route('update-pass')}}">{{Lang::get('public.AccountPassword')}}</a>
                </div>
              </div>
            </div>
            
			
			
            <div class="row">
               <div class="col-md-12 col-xs-12">
               <div class="cont">
               <h5 class="text-yellow"><i class="fa fa-user-circle-o mr01" aria-hidden="true"></i>{{Lang::get('public.AccountProfile')}}</h5>
               <div class="row mt03">
                 <div class="col-md-2 col-xs-5">
                     <div class="txtWrap">{{Lang::get('public.Username')}}</div>
                 </div>
                 <div class="col-md-3 col-xs-7">
                     <div class="form-group">
					 @if(empty($acdObj->fullname))
					 <input type="text" class="form-control" id="fullname" value="{{$acdObj->fullname}}">
					 @else
					 <input type="text" class="form-control" id="fullname" value="{{$acdObj->fullname}}" disabled>
					 @endif
                     </div>
                 </div>
               </div>
               
               <div class="row mt03">
                 <div class="col-md-2 col-xs-5">
                     <div class="txtWrap">{{Lang::get('public.EmailAddress')}}</div>
                 </div>
                 <div class="col-md-3 col-xs-7">
                     <div class="form-group">
					 @if(empty($acdObj->email))
						<input type="text" class="form-control" id="email" value="{{$acdObj->email}}">	
					 @else
						<input type="text" class="form-control" id="email" value="{{$acdObj->email}}" disabled>
					 @endif
                     </div>
                 </div>
               </div>
               
               <div class="row mt03">
                 <div class="col-md-2 col-xs-5">
                     <div class="txtWrap">{{Lang::get('public.Currency')}}</div>
                 </div>
                 <div class="col-md-3 col-xs-7">
                     <div class="form-group">
                     <input type="text" class="form-control" id="currency" value="{{ Session::get('currency') }}" disabled>
                     </div>
                 </div>
               </div>
               
               <div class="row mt03">
                 <div class="col-md-2 col-xs-5">
                     <div class="txtWrap">{{Lang::get('public.ContactNo')}}</div>
                 </div>
                 <div class="col-md-3 col-xs-7">
                     <div class="form-group">
					 @if(empty($acdObj->telmobile))
						<input type="text" class="form-control" id="telmobile" value="{{$acdObj->telmobile}}">
					 @else
						<input type="text" class="form-control" id="telmobile" value="{{$acdObj->telmobile}}" disabled>
					 @endif
                     </div>
                 </div>
               </div>
               
               <div class="row mt03">
                 <div class="col-md-2 col-xs-5">
                     <div class="txtWrap">{{Lang::get('public.Gender')}}</div>
                 </div>
                 <div class="col-md-3 col-xs-7">
                     <div class="form-group">
                     <select class="form-control" name="" id="gender">
                       <option value="1">{{Lang::get('public.Male')}}</option>
                       <option value="2">{{Lang::get('public.Female')}}</option>
                     </select>
                     </div>
                 </div>
               </div>
               
               <div class="row mt03">
				 @if( $acdObj->dob == '0000-00-00' )
                 <div class="col-md-2 col-xs-12">
                     <div class="txtWrap">{{Lang::get('public.DOB')}}</div>
                 </div>
                 <div class="col-md-1 col-xs-4">
                     <div class="form-group">
						 <select id="dob_day" class="form-control">
							<option value="01">01</option>
							<option value="02">02</option>
							<option value="03">03</option>
							<option value="04">04</option>
							<option value="05">05</option>
							<option value="06">06</option>
							<option value="07">07</option>
							<option value="08">08</option>
							<option value="09">09</option>
							<option value="10">10</option>
							<option value="11">11</option>
							<option value="12">12</option>
							<option value="13">13</option>
							<option value="14">14</option>
							<option value="15">15</option>
							<option value="16">16</option>
							<option value="17">17</option>
							<option value="18">18</option>
							<option value="19">19</option>
							<option value="20">20</option>
							<option value="21">21</option>
							<option value="22">22</option>
							<option value="23">23</option>
							<option value="24">24</option>
							<option value="25">25</option>
							<option value="26">26</option>
							<option value="27">27</option>
							<option value="28">28</option>
							<option value="29">29</option>
							<option value="30">30</option>
							<option value="31">31</option>
						 </select>
                     </div>
                 </div>
                 <div class="col-md-2 col-xs-4">
                     <div class="form-group">
						<select id="dob_month" class="form-control">
							<option value="01">January</option>
							<option value="02">February</option>
							<option value="03">March</option>
							<option value="04">April</option>
							<option value="05">May</option>
							<option value="06">June</option>
							<option value="07">July</option>
							<option value="08">August</option>
							<option value="09">September</option>
							<option value="10">October</option>
							<option value="11">November</option>
							<option value="12">December</option>
						</select>
                     </div>
                 </div>
                 <div class="col-md-2 col-xs-4">
                     <div class="form-group">
                     <select id="dob_year" class="form-control">
						<option value="2006">2006</option>
                                <option value="2005">2005</option>
                                <option value="2004">2004</option>
                                <option value="2003">2003</option>
                                <option value="2002">2002</option>
                                <option value="2001">2001</option>
                                <option value="2000">2000</option>
                                <option value="1999">1999</option>
                                <option value="1998">1998</option>
                                <option value="1997">1997</option>
                                <option value="1996">1996</option>
                                <option value="1995">1995</option>
                                <option value="1994">1994</option>
                                <option value="1993">1993</option>
                                <option value="1992">1992</option>
                                <option value="1991">1991</option>
                                <option value="1990">1990</option>
                                <option value="1989">1989</option>
                                <option value="1988">1988</option>
                                <option value="1987">1987</option>
                                <option value="1986">1986</option>
                                <option value="1985">1985</option>
                                <option value="1984">1984</option>
                                <option value="1983">1983</option>
                                <option value="1982">1982</option>
                                <option value="1981">1981</option>
                                <option value="1980">1980</option>
                                <option value="1979">1979</option>
                                <option value="1978">1978</option>
                                <option value="1977">1977</option>
                                <option value="1976">1976</option>
                                <option value="1975">1975</option>
                                <option value="1974">1974</option>
                                <option value="1973">1973</option>
                                <option value="1972">1972</option>
                                <option value="1971">1971</option>
                                <option value="1970">1970</option>
                                <option value="1969">1969</option>
                                <option value="1968">1968</option>
                                <option value="1967">1967</option>
                                <option value="1966">1966</option>
                                <option value="1965">1965</option>
                                <option value="1964">1964</option>
                                <option value="1963">1963</option>
                                <option value="1962">1962</option>
                                <option value="1961">1961</option>
                                <option value="1960">1960</option>	
					  </select>
                     </div>
                </div>
				@else
                 <div class="col-md-2 col-xs-5">
                     <div class="txtWrap">{{Lang::get('public.DOB')}} :</div>
                 </div>
                 <div class="col-md-3 col-xs-7">
                     <div class="form-group">
                     <input type="text" class="form-control" id="exampleInputName2" value="{{$acdObj->dob }}" disabled>
                     </div>
                 </div>
				@endif
               </div>
               
                  <div class="row">
                  <div class="col-md-4 col-xs-4">
                  <div class="btnNormal ml03">
                    <a href="#" onClick="update_profile()">{{Lang::get('public.Submit')}}</a>
                  </div>
                  </div>
                  </div>
               
               </div>
               </div>
            </div>
          

      </div>
      </div>
@stop