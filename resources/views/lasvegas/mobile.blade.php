@extends('lasvegas/master')
@section('title', 'Deposit')
@section('content')
<div id="content noBg">
      <div class="mission block-pd-sm block-bg-noise bg-mob">
        <div class="container">
        <div class="row">
        <div class="col-md-12 col-xs-12 text-center specialTxt">
        MOBILE DOWNLOAD
        </div>
        </div>
          <div class="row mb01">
          <div class="col-md-3 col-sm-3 col-xs-12">
             <div class="mobBx">
                <div class="mobBxInner">
                <img src="{{url()}}/lasvegas/img/pt-mob.jpg">
                
                <div class="qrCont">
                   <i class="fa fa-android fa-2x" aria-hidden="true"></i><span class="adj01">Android</span><br><br>
                   <span>
					@if(Session::has('username'))
						{{Lang::get('public.Username')}}:<br>
						MVGS_{{Session::get('username')}}
					@else
						{{Lang::get('public.PleaseLogin')}}!
					@endif
					</span>
					<a href="http://m.ld176988.com/live/download.html" target="blank">
						<img src="{{url()}}/lasvegas/img/plt_liveCasino.png" style="height:100px;width:100px;">
					</a>
                   <span>{{Lang::get('public.LiveCasino')}} {{Lang::get('public.Download')}}</span>
                </div>
                
            
                </div>
             </div>
          </div>
          <div class="col-md-3 col-sm-3 col-xs-12">
             <div class="mobBx">
                <div class="mobBxInner">
                <img src="{{url()}}/lasvegas/img/pt-mob.jpg">
                
                <div class="qrCont">
                   <i class="fa fa-android fa-2x" aria-hidden="true"></i><span class="adj01">Android</span><br><br>
                   <span>
					@if(Session::has('username'))
						{{Lang::get('public.Username')}}:<br>
						MVGS_{{Session::get('username')}}
					@else
						{{Lang::get('public.PleaseLogin')}}!
					@endif
					</span>
					<a href="http://m.ld176988.com/download.html" target="blank">
						<img src="{{url()}}/lasvegas/img/plt_slot.png" style="height:100px;width:100px;">
					</a>
                   <span>{{Lang::get('public.Slots')}} {{Lang::get('public.Download')}}</span>
                </div>
            
                </div>
                
               
             </div>
          </div>
          <div class="col-md-3 col-sm-3 col-xs-12">
             <div class="mobBx">
                <div class="mobBxInner">
                <img src="{{url()}}/lasvegas/img/sky-mob.jpg">
                
                <div class="qrCont">
                   <i class="fa fa-android fa-2x" aria-hidden="true"></i><span class="adj01">Android</span><br><br>
                   <span>
				   @if(Session::has('username'))
					   {{Lang::get('public.Username')}}:<br>
						{{Config::get(Session::get('currency').'.sky.agid').((new \App\libraries\providers\SKY())->formatUserId(Session::get('userid')))}}
					@else
						{{Lang::get('public.PleaseLogin')}}!
					@endif
					</span>
					<a href="https://ed.sky3888a.com/sky1388_android.html" target="blank">
						<img src="{{url()}}/lasvegas/img/sky_qr_android_codes.png" style="height:100px;width:100px;">
					</a>
                   <span>{{Lang::get('public.Slots')}} {{Lang::get('public.Download')}}</span>
                </div>
            
                </div>
                
               
             </div>
          </div>
          <div class="col-md-3 col-sm-3 col-xs-12">
             <div class="mobBx">
                <div class="mobBxInner">
                <img src="{{url()}}/lasvegas/img/sky-mob.jpg">
                
                <div class="qrCont">
                   <i class="fa fa-apple fa-2x" aria-hidden="true"></i><span class="adj01">IOS</span><br><br>
                   <span>
				   @if(Session::has('username'))
					   {{Lang::get('public.Username')}}:<br>
						{{Config::get(Session::get('currency').'.sky.agid').((new \App\libraries\providers\SKY())->formatUserId(Session::get('userid')))}}
					@else
						{{Lang::get('public.PleaseLogin')}}!
					@endif
					</span>
					<a href="https://ed.sky3888a.com/sky1388.html" target="blank">
						<img src="{{url()}}/lasvegas/img/sky_qr_ios_codes.png" style="height:100px;width:100px;">
					</a>
                   <span>{{Lang::get('public.Slots')}} {{Lang::get('public.Download')}}</span>
                </div>
            
                </div>
                
               
             </div>
          </div>
          <div class="col-md-3 col-sm-3 col-xs-12">
             <div class="mobBx">
                <div class="mobBxInner">
                <img src="{{url()}}/lasvegas/img/scr-mobChange.jpg">
                
                <div class="qrCont">
                   <i class="fa fa-android fa-2x" aria-hidden="true"></i><span class="adj01">Android</span><br><br>
                   <span>
				   @if(Session::has('username'))
						{{Lang::get('public.Username')}}:<br>
						{{Session::get('scrid')}}<br>
						Password: <br>
						Lv3&nbsp + your login password
					@else
						{{Lang::get('public.PleaseLogin')}}!
					@endif
					</span>
					<a href="http://m.scr-888.co" target="blank">
						<img src="{{url()}}/lasvegas/img/scr888_qr-code1.jpg" style="height:100px;width:100px;">
					</a>
                   <span>{{Lang::get('public.Slots')}} {{Lang::get('public.Download')}}</span>
                </div>
            
                </div>
                
               
             </div>
          </div>
          <div class="col-md-3 col-sm-3 col-xs-12">
             <div class="mobBx">
                <div class="mobBxInner">
                <img src="{{url()}}/lasvegas/img/scr-mobChange.jpg">
                
                <div class="qrCont">
                   <i class="fa fa-apple fa-2x" aria-hidden="true"></i><span class="adj01">IOS</span><br><br>
                   <span>
				   @if(Session::has('username'))
						{{Lang::get('public.Username')}}:<br>
						{{Session::get('scrid')}}<br>
						Password: <br>
						Lv3&nbsp + your login password
					@else
						{{Lang::get('public.PleaseLogin')}}!
					@endif
					<a href="http://m.scr-888.co" target="blank">
						<img src="{{url()}}/lasvegas/img/scr888_qr-code1.jpg" style="height:100px;width:100px;">
					</a>
                   <span>{{Lang::get('public.Slots')}} {{Lang::get('public.Download')}}</span>
                </div>
            
                </div>
                
               
             </div>
          </div>
		  <div class="col-md-3 col-sm-3 col-xs-12">
             <div class="mobBx">
                <div class="mobBxInner">
                <img src="{{url()}}/lasvegas/img/mxb-mob.jpg">
                
                <div class="qrCont">
                   <i class="fa fa-android fa-2x" aria-hidden="true"></i><span class="adj01">Android</span><br><br>
                   <span>
				   @if(Session::has('username'))
						{{Lang::get('public.Username')}}:<br>
						{{strtoupper(Session::get('username'))}}
						<br>
						<br>
						<br>
					@else
						{{Lang::get('public.PleaseLogin')}}!
					@endif
					</span>
					<a href="https://xprogaming.hs.llnwd.net/mobileapk/967-UB-Las%20Vegas-MYR-5.2.0.apk" target="blank">
						<img src="{{url()}}/lasvegas/img/maxbet_qr-code1.jpg" style="height:100px;width:100px;">
					</a>
                   <span>{{Lang::get('public.LiveCasino')}} {{Lang::get('public.Download')}}</span>
                </div>
            
                </div>
             </div>
          </div>
          </div>
        </div>
      </div>

      
    </div>
@stop