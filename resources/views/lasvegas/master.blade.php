  <!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Las Vegas – @yield('title')</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <meta content="" name="keywords">
    <meta content="" name="description">
	
	@yield('css')
	
    <!-- Fav and touch icons -->
    <link rel="shortcut icon" href="{{url()}}/lasvegas/img/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{url()}}/lasvegas/img/icons/114x114.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{url()}}/lasvegas/img/icons/72x72.png">
    <link rel="apple-touch-icon-precomposed" href="{{url()}}/lasvegas/img/icons/default.png">
  
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900" rel="stylesheet">
    
    <!-- Bootstrap CSS File -->
    <link href="{{url()}}/lasvegas/resources/css/bootstrap.min.css" rel="stylesheet">
  
    <!-- Libraries CSS Files -->
    <link href="{{url()}}/lasvegas/resources/css/font-awesome.min.css" rel="stylesheet">
    <link href="{{url()}}/lasvegas/resources/css/owl.carousel.min.css" rel="stylesheet">
    <link href="{{url()}}/lasvegas/resources/css/owl.theme.min.css" rel="stylesheet">
    <link href="{{url()}}/lasvegas/resources/css/owl.transitions.min.css" rel="stylesheet">
    
    <!-- Main Stylesheet File -->
    <link href="{{url()}}/lasvegas/resources/css/style.css?v2" rel="stylesheet">
    <link href="{{url()}}/lasvegas/resources/css/responsive.css" rel="stylesheet">
    <script src="{{url()}}/lasvegas/resources/js/jquery.min.js"></script>
	@yield('js')
    <script type="text/javascript">
		$(function () {
			@if (Auth::user()->check())
						load_message();
						//checkUpdatePW();
			@endif
			$('marquee').mouseover(function() {
				this.stop();
			}).mouseout(function() {
				this.start();
			});
		});
		
		function enterpressalert(e, textarea)
		{
			var code = (e.keyCode ? e.keyCode : e.which);
            if(code == 13) { //Enter keycode
              login();
            }
		}
		function enterpressalert2(e, textarea){
			var code = (e.keyCode ? e.keyCode : e.which);
			if(code == 13) { //Enter keycode
			   login2();
			 }
		}
        function login()
        {
		document.getElementById("loginBtnAction").style.display = "none";
		document.getElementById("loading").style.display = "block";
			$.ajax({
                type: "POST",
				url: "{{route('login')}}",
				data: {
				_token: "{{ csrf_token() }}",
					username: $('#username').val(),
					password: $('#password').val(),
					code:     $('#code').val(),
				},
			}).done(function(json) {
				obj = JSON.parse(json);
				var str = '';
				$.each(obj, function(i, item) {
					str += item + '\n';
				});
				if ("{{Lang::get('COMMON.SUCESSFUL')}}\n" == str) {
					location.reload();
				} else {
					alert(str);
					document.getElementById("loginBtnAction").style.display = "block";
					document.getElementById("loading").style.display = "none";
				}
			});
        }

        function login2() {
		document.getElementById("loginBtnAction2").style.display = "none";
		document.getElementById("loading2").style.display = "block";
			$.ajax({
                type: "POST",
				url: "{{route('login')}}",
				data: {
                    _token: "{{ csrf_token() }}",
					username: $('#username2').val(),
					password: $('#password2').val(),
					code:     $('#code2').val(),
				},
			}).done(function(json) {
				obj = JSON.parse(json);
				var str = '';
				$.each(obj, function(i, item) {
					str += item + '\n';
				});
				if ("{{Lang::get('COMMON.SUCESSFUL')}}\n" == str){
					location.reload();
				} else {
					alert(str);
					document.getElementById("loginBtnAction2").style.display = "block";
					document.getElementById("loading2").style.display = "none";
				}
			});
        }
		function inbox() {
		window.open('{{route('inbox')	}}', '', 'width=800,height=600');
		}
@if (Auth::user()->check())
		function load_message() {
			$.ajax({
				url: "{{ route('showTotalUnseenMessage') }}",
				type: "GET",
				dataType: "text",
				data: {
				},
				success: function (result) {
					if(result>0){
						$("#totalMessage").html("<b style='color: red'>["+result+"]</b>");
						$("#totalMessage2").html("<b style='color: red'>["+result+"]</b>");
						$("#totalMessage3").html("<b style='color: red'>["+result+"]</b>");
					}else{
						$("#totalMessage").html("["+result+"]");
					}
				},
				error: function (XMLHttpRequest, textStatus, errorThrown) {
				}
			});
		}
        setInterval(load_message,20000);
		
		function update_mainwallet(){
			$.ajax({
				  type: "POST",
				  url: "{{route( 'mainwallet', [ '_token'=> csrf_token() ])}}",
				  beforeSend: function(balance){
				
				  },
				  success: function(balance){
					$('.main_wallet').html(parseFloat(Math.round(balance * 100) / 100).toFixed(2));
					total_balance += parseFloat(balance);
				  }
			 });
				
		}
		function getBalance(type){
			var total_balance = 0;
			$.when(
			
				 $.ajax({
							  type: "POST",
							  url: "{{route('mainwallet', [ '_token'=> csrf_token()])}}",
							  beforeSend: function(balance){
						
							  },
							  success: function(balance){
								$('.main_wallet').html(parseFloat(Math.round(balance * 100) / 100).toFixed(2));
								total_balance += parseFloat(balance);
							  }
				 }),
				@foreach( Session::get('products_obj') as $prdid => $object)
				
			
						 $.ajax({
							  type: "POST",
							  url: "{{route( 'getbalance', [ '_token'=> csrf_token(), 'product'=> $object->code ])}}&rd=<?php echo rand ( 10000, 99999 ); ?>",
							  beforeSend: function(balance){
								$('.{{$object->code}}_balance').html('<img style="" src="{{url()}}/front/img/ajax-loading.gif" width="20" height="10">');
							  },
							  success: function(balance){
								  if( balance != '{{Lang::get('Maintenance')}}')
								  {
									$('.{{$object->code}}_balance').html(parseFloat(Math.round(balance * 100) / 100).toFixed(2));
									total_balance += parseFloat(balance);
								  }
								  else
								  {
									  $('.{{$object->code}}_balance').html(balance);
								  }
							  }
						 })
					  @if($prdid != Session::get('last_prdid')) 
						,
					  @endif
				@endforeach

				
			).then(function() {
				
					$('#total_balance').html(parseFloat(Math.round(total_balance * 100) / 100).toFixed(2));
					$('.total_balance').html(parseFloat(Math.round(total_balance * 100) / 100).toFixed(2));

			});

		}
		function checkUpdatePW(){
			$.ajax({
				type: "GET",
				url: "{{route('checkUpdatePassword')}}",
				data: {},
			}).done(function( json ) {
			obj = JSON.stringify(json);
				 $.each(obj, function(i, item) {
					if(window.location.pathname!="/update-pass"){
						alert(item);
						window.location = "{{route('update-pass')}}";
					}
				});		
			});
		}	
		
@endif
		
	var sDate = new Date("{{date('c')}}");
    var weekstr = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
    var monthstr = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
	
    function updateTime() {
        sDate = new Date(sDate.getTime() + 1000);

        var hour = parseFloat(sDate.getHours());
        var min = parseFloat(sDate.getMinutes());
        var sec = parseFloat(sDate.getSeconds());

        if (hour < 10)
            hour = "0" + hour;

        if (min < 10)
            min = "0" + min;

        if (sec < 10)
            sec = "0" + sec;

        $("#sys_date").html(sDate.getDate() + " " + monthstr[sDate.getMonth()] + " " + sDate.getFullYear() + ", " + weekstr[sDate.getDay()] + ", " + hour + ":" + min + ":" + sec + " (GMT +8)");
    }
	setInterval(updateTime, 1000);
	</script>
  </head>
@if (Auth::user()->check())
<!-- profile menu -->
<div id="profileBx" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Account Profile</h4>
      </div>
      <div class="modal-body bg-black text-white">
        <div class="row">
        <div class="col-md-12 col-xs-12">
        <div class="mMenu">
          <ul>
          <li><a href="#" onclick="inbox();">{{Lang::get('public.Inbox')}}&nbsp;<b id="totalMessage"></b></a></li>
          <li><a href="{{route('update-profile')}}">{{Lang::get('public.MyAccount')}}</a></li>
          <li><a href="{{route('update-pass')}}">{{Lang::get('public.ChangePassword')}}</a></li>
          </ul>
        </div>
        </div>
        </div> 
      </div>
      <div class="modal-footer bg-green">
      </div>
    </div>
  </div>
</div>
<!-- profile menu -->

<!-- balance menu -->
  <div id="walletBx" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">My Wallet</h4>
      </div>
      <div class="modal-body bg-black text-white">
        <div class="row">
        <div class="col-md-12 col-xs-12">
			<div class="table-responsive wTable">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td><strong>{{Lang::get('public.Wallet')}}</strong></td>
							<td><strong>{{Lang::get('public.Balance')}}</strong></td>
						</tr>
						<tr>
							<td>{{Lang::get('public.MainWallet')}}</td>
							<td class="main_wallet">0.0</td>
						</tr>
					@foreach( Session::get('products_obj') as $prdid => $object)
						<tr>
							<td>{{$object->name}}</td>
							<td class="{{$object->code}}_balance" >0.00</td>
						</tr>
					@endforeach
						<tr>
							<td><strong>{{Lang::get('public.Total')}}</strong></td>
							<td><strong id="total_balance">00.00</strong></td>
						</tr>
				</table>
			</div>
        </div>
        </div> 
      </div>
      <div class="modal-footer bg-green">
        <div class="btnNormal pull-right">
        <a href="{{route('deposit')}}">{{Lang::get('public.Deposit')}}</a>
        </div>
        <div class="btnNt pull-left">
        <a href="{{route('withdraw')}}">{{Lang::get('public.Withdrawal')}}</a>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- balance menu -->

<!-- fund menu -->
  <div id="fundBx" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">{{Lang::get('public.Fund')}}</h4>
      </div>
      <div class="modal-body bg-black text-white">
        <div class="row">
        <div class="col-md-12 col-xs-12">
        <div class="mMenu">
          <ul>
          <li><a href="{{route('deposit')}}">{{Lang::get('public.Deposit')}}</a></li>
          <li><a href="{{route('withdraw')}}">{{Lang::get('public.Withdrawal')}}</a></li>
          <li><a href="{{route('transaction')}}">{{Lang::get('public.TransactionHistory')}}</a></li>
          <li><a href="{{route('transfer')}}">{{Lang::get('public.Transfer')}} </a></li>
          </ul>
        </div>
        </div>
        </div> 
      </div>
      <div class="modal-footer bg-green">
        
      </div>
    </div>

  </div>
</div>
<!-- fund menu -->
        <!-- Modal -->
<div id="loginBx" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Logout</h4>
      </div>
      <div class="modal-body">
        <div class="row">
        <div class="col-md-12 col-xs-12">
        <h4>Are you sure you want to logout?</h4>
        </div>

        </div>
        
        
   
        
        
        
      </div>
      <div class="modal-footer">
      <div class="loginBtn pull-right">
      <a href="{{route('logout')}}">{{Lang::get('public.Logout') }}</a>
      </div>
        
      </div>
    </div>

  </div>
</div>
@else
<div id="loginBx" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Please Login</h4>
      </div>
      <div class="modal-body">
        <div class="row">
        <div class="col-md-12 col-xs-12">
        <div class="form-group">
			<input type="text" class="form-control" id="username2" name="username" placeholder="{{ Lang::get('public.Username') }}" onKeyPress="enterpressalert(event, this)">
		</div>
        </div>
        <div class="col-md-12 col-xs-12">
        <div class="form-group">
			<input type="password" class="form-control" id="password2" name="password" placeholder="{{ Lang::get('public.Password') }}" onKeyPress="enterpressalert(event, this)">
		</div>
        </div>

        </div>
        
        <div class="row">
        <div class="col-md-6 col-xs-6">
        <div class="form-group">
			<input class="form-control" id="code2" name="" type="text" onKeyPress="enterpressalert(event, this)" placeholder="{{Lang::get('public.Code')}}">
		</div>
        </div>
        <div class="col-md-4 col-xs-4">
			<img style="width:90px;height:34px;" src="{{route('captcha', ['type' => 'login_captcha'])}}">
        </div>
        <div class="col-md-2 col-xs-2">
        <div class="abc">
        
        </div>
        </div>
        </div>
   
        
        
        
      </div>
      <div class="modal-footer">
      <div class="fp pull-left">
		<a href="{{ route('forgotpassword') }}">{{ Lang::get('public.ForgotPassword') }}</a>
      </div>
      <div class="loginBtn pull-right">
		<a id="loginBtnAction2" href="#" onclick="login2();">{{ Lang::get('public.Login') }}</a>
		<a id="loading2" href="javascript:void(0);" style="display:none;">{{ Lang::get('public.Loading') }}</a>
      </div>
        
      </div>
    </div>

  </div>
</div>
@endif

        <!-- Modal -->
  <body class="page-index has-hero">
    
    <!--Change the background class to alter background image, options are: benches, boots, buildings, city, metro -->
    <div id="background-wrapper" data-stellar-background-ratio="0.1">
      
      <!-- ======== @Region: #navigation ======== -->
      <div id="navigation" class="wrapper">
        
        <!--Header & navbar-branding region-->
        <div class="header">
          <div class="header-inner container">
            <div class="row">
              <div class="col-md-4 col-xs-12">
                <!--navbar-branding/logo - hidden image tag & site name so things like Facebook to pick up, actual logo set via CSS for flexibility -->
                <a class="navbar-brand" href="{{ route('homepage') }}" title="Home">
                    <img class="img-responsive" src="{{url()}}/lasvegas/img/logo.png">
                </a>
                
              </div>
              <!--header rightside-->
              <div class="col-md-8 col-xs-12">
                  <div class="row">
                  <div class="col-md-12 col-xs-12">
                  <div class="logWrap">
                  <div class="logLink">
                  <ul>
                  <li class="logSep"><a href="{{route('banking')}}" style="padding-right:10px">{{Lang::get('public.Banking')}}</a></li>
                  <li class="logSep"><a href="{{route('contactus')}}" style="padding-right:3px">{{Lang::get('public.ContactUs')}}</a></li>
                  <li><a href="{{route( 'language', [ 'lang'=> 'en'])}}"><img src="{{url()}}/lasvegas/img/uk-icon.png"></a></li>
                  <li><a href="{{route( 'language', [ 'lang'=> 'cn'])}}"><img src="{{url()}}/lasvegas/img/cn-icon.png"></a></li>
                  </ul>
                  </div>
                  <div class="dateT logSep1" id="sys_date">
				  <?php date_default_timezone_set("Asia/Kuala_Lumpur") ?>
						<?php echo date("d M Y");?>,
						<?php echo substr(date("l"),0,3);?>,
						<?php echo date(" H:i:s", time());?> (GMT +8)
				  </div>
                  <!--mobile login-->
                  <div class="mobLogin">
					  <a data-toggle="modal" data-target="#loginBx" href="#">
						  <i class="fa fa-lock ab tt1" aria-hidden="true"></i>
						  <div class="txt1">@if( !Auth::user()->check() ) Login @else Logout @endif</div>
						  <div class="clearfix"></div>
					  </a>
                  </div>
                  <!--mobile login-->
				  <!--mobile register-->
                  <div class="mobLogin" style="width:30px">
					  <a href="{{route('register')}}">
						  <i class="fa fa-lock ab tt1" aria-hidden="true"></i>
						  <div class="txt1">@if( !Auth::user()->check() ) Register @else  @endif</div>
						  <div class="clearfix"></div>
					  </a>
                  </div>
                  <!--mobile register-->
                  <!--mobile language selection-->
                  <div class="mobLang">
                  <a href="{{route( 'language', [ 'lang'=> 'cn'])}}"><img src="{{url()}}/lasvegas/img/cn-icon.png"></a>
                  <a href="{{route( 'language', [ 'lang'=> 'en'])}}"><img src="{{url()}}/lasvegas/img/uk-icon.png"></a>
                  <div class="clearfix"></div>
                  </div>
                  <!--mobile language selection-->
				  @if( !Auth::user()->check() )
					  <div class="loginD">
						  <form>
						  <div class="form-group">
							<input type="text" class="form-control" id="username" name="username" placeholder="{{ Lang::get('public.Username') }}" onKeyPress="enterpressalert(event, this)">
						  </div>
						  <div class="form-group">
							<input type="password" class="form-control" id="password" name="password" placeholder="{{ Lang::get('public.Password') }}" onKeyPress="enterpressalert(event, this)">
						  </div>
						  <div class="form-oth">
						  <img style="width:70px;height:26px;" src="{{route('captcha', ['type' => 'login_captcha'])}}">
						  </div>
						  <div class="form-group">
							<input class="form-control" id="code" name="" type="text" onKeyPress="enterpressalert(event, this)" placeholder="{{Lang::get('public.Code')}}">
						  </div>
						  <div class="form-group">
							<div class="loginBtn">
							<a id="loginBtnAction" href="#" onclick="login();">{{ Lang::get('public.Login') }}</a>
							<a id="loading" href="javascript:void(0);" style="display:none;">{{ Lang::get('public.Loading') }}</a>
							</div>
						  </div>
						  <div class="form-group">
							<div class="fp">
								<a href="{{ route('forgotpassword') }}">{{ Lang::get('public.ForgotPassword') }}</a>
							</a>
							</div>
						  </div>
						  <div class="clearfix"></div>
						  </form>
					  </div>
				  @else
				  	<div class="loginD">
						  <div class="welc"><i class="fa fa-user-circle" aria-hidden="true"></i>&nbsp;{{Lang::get('public.Welcome')}} {{Session::get('username')}}</div>
						  <div class="welc"><a data-toggle="modal" data-target="#profileBx" href="#"><i class="fa fa-user" aria-hidden="true"></i> {{Lang::get('public.Profile')}}&nbsp;<b id="totalMessage2"></b></a></div>
						  <div class="welc"><a data-toggle="modal" data-target="#walletBx" href="#"  onClick="getBalance()"><i class="fa fa-money" aria-hidden="true"></i> {{Lang::get('public.Balance')}}</a></div>
						  <div class="welc"><a data-toggle="modal" data-target="#fundBx" href="#"><i class="fa fa-university" aria-hidden="true"></i> {{Lang::get('public.Fund')}}</a></div>
						  <div class="form-group abcd">
							<div class="loginBtn">
							<a href="{{route('logout')}}">{{Lang::get('public.Logout') }}</a>
							</div>
						  </div>
						  
						  <div class="clearfix"></div>
                    </div>
				  @endif
                  <div class="clearfix"></div>
                  </div>
                  
                  <div class="clearfix"></div>
                  </div>
                  
                  <div class="col-md-12 col-xs-12">
                  <div class="anmnt" style="cursor: pointer;" onclick="window.open('{{route('announcement')}}?lang={{Lang::getLocale()}}', 'Notice', 'width=800,height=600');">
                  <marquee>{{ App\Http\Controllers\User\AnnouncementController::index() }}</marquee>
                  </div>
                  </div>
                  
                  </div>
                
              </div>
            </div>
          </div>
        </div>
        <div class="container">
          <div class="navbar navbar-default">
		  <!--mobile collapse menu button-->
		  @if(Auth::user()->check() )
          <div class="acctM pull-right" data-toggle="collapse" aria-expanded="false">
             <div class="smallIC">
               <a class="pull-right" data-toggle="modal" data-target="#fundBx" href="#"><i class="fa fa-university" aria-hidden="true"></i><div>{{Lang::get('public.Fund')}}</div></a>
               <a class="pull-right" data-toggle="modal" data-target="#walletBx" href="#" onClick="getBalance()"><i class="fa fa-money" aria-hidden="true"></i><div>{{Lang::get('public.Balance')}}</div></a>
               <a class="pull-right" data-toggle="modal" data-target="#profileBx" href="#"><i class="fa fa-user" aria-hidden="true"></i><div>{{Lang::get('public.Profile')}}&nbsp;<b id="totalMessage3"></b></div></a>
               
             </div>
          </div>
		  @endif
          <!--mobile collapse menu button-->
            <!--mobile collapse menu button-->
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse" aria-expanded="false"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
            <!--social media icons-->
            
            <!--everything within this div is collapsed on mobile-->
            <div class="navbar-collapse collapse">
            <div class="navbar-text jn pull-right">
			@if( !Auth::user()->check() )
              <!--@todo: replace with company social media details-->
              <a href="{{route('register')}}">{{Lang::get('public.JoinNow')}}</a>
			@endif
            </div>
				<div class="navCont">
				<ul>
					<li>
						<a href="{{route('homepage')}}">{{Lang::get('public.Home')}}</a>
					</li>
					<li>
						<a href="{{route('livecasino')}}">{{Lang::get('public.LiveCasino')}}</a>
							<!-- Dropdown Menu -->
							<ul>
								<li><a href="#" onclick="@if (Auth::user()->check())window.open('{{route('vgs', [ 'type' => 'casino'])}}','','width=720px,height=720px,top=0,left=0');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif">Evolution Gaming</a></li>
								
								<li><a href="#" onclick="@if (Auth::user()->check()) window.location.href='{{route('mxb', [ 'type' => 'casino','category' => 'baccarat' ])}}' @else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif">Maxbet</a></li>
								
								<li><a href="javascript:void(0);" onclick="@if (Auth::user()->check())window.open('{{route('pltiframe')}}','','width=1000,height=750')@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif">Playtech</a></li>
								
								<li><a href="javascript:void(0);" onclick="@if (Auth::user()->check())window.open('{{route('hog')}}','','width=1000,height=750')@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif">HoGaming</a></li>
								
							</ul>
					</li>
                <li>
                  <a href="{{route('slot')}}">{{Lang::get('public.Slots')}}</a>
                  <!-- Dropdown Menu -->
                  <ul>
                    <li><a href="javascript:void(0);" onclick="@if (Auth::user()->check())window.open('{{route('plt' , [ 'type' => 'pgames' , 'category' => '1' ] )}}','','width=1100,height=750')@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif">Playtech</a></li>
                    <li><a href="javascript:void(0);" onclick="@if (Auth::user()->check())window.open('{{route('sky' , [ 'type' => 'slot' , 'category' => 'all' ] )}}','','width=1100,height=750')@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif">Sky3888</a></li>
                    <li><a href="javascript:void(0)" onclick="@if (Auth::user()->check())window.open('{{route('scr')}}','','width=1000,height=750')@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif" onclick="change_iframe('{{route('scr')}}')">918Kiss</a></li>
                  </ul>
                </li>
                <li>
                 <a href="{{route('ibc' )}}">{{Lang::get('public.SportsBook')}}</a>
                </li>
                 <!-- <li>
					<a href="{{route('4d')}}">4D</a>
                </li>  -->
                <li>
					<a href="{{ route('ctb') }}?page=intro">{{Lang::get('public.Horse')}}</a>
                </li>
                <li>
                  <a href="{{route('promotion')}}">{{Lang::get('public.Promotion')}}</a>
                </li>
				<li>
                  <a href="{{route('mobile')}}">{{Lang::get('public.MobileDownload')}}</a>
                </li>
              </ul>
            </div>
            </div>
            <!--/.navbar-collapse -->
			
			
          </div>
        </div>
		
		
      </div>
    </div>
	
@yield('content')

    
    
    <!-- ======== @Region: #footer ======== -->
    <footer id="footer" class="block-bg-grey-dark" data-stellar-background-ratio="0.4">
      <div class="container">
        
        <div class="row" id="contact">
          
          
          
          
          
          <div class="col-md-6">
            <h4>
             Connect With Us
            </h4>
            <!--social media icons-->
            <div class="social-media">
              <!--@todo: replace with company social media details-->
              <a target="_blank" href="https://www.facebook.com/mylasvegas888"><img src="{{url()}}/lasvegas/img/fb-icon.png"></a>
			  <a target="_blank" href="https://twitter.com/search?q=%40mylv888&src=typd"><img src="{{url()}}/lasvegas/img/twit-icon.png"></a>
              <a target="#"><img src="{{url()}}/lasvegas/img/skype-icon.png"></a>
            </div>
          </div>
          
          <div class="col-md-6">
            <h4 class="text-right goLeft">Product Sponsored by</h4>
            <img class="img-responsive" src="{{url()}}/lasvegas/img/pd.png">
          </div>
          
        </div>
        
        <div class="row subfooter">
          <!--@todo: replace with company copyright details-->
          <div class="col-md-6">
            <h4>
             {{Lang::get('COMMON.PAYMENTMETHOD')}}
            </h4>
            <img class="img-responsive" src="{{url()}}/lasvegas/img/bank.png">
          </div>
          
          <div class="col-md-6">
            <h4 class="text-right goLeft">
             Supported Browser
            </h4>
            <!--social media icons-->
            <div class="social-media goLeft amRight">
              <!--@todo: replace with company social media details-->
              <a href="#"><img src="{{url()}}/lasvegas/img/chrome-icon.png"></a>
              <a href="#"><img src="{{url()}}/lasvegas/img/ff-icon.png"></a>
              <a href="#"><img src="{{url()}}/lasvegas/img/ie-icon.png"></a>
            </div>
          </div>
		  <div class="col-md-12 col-xs-12">
			<a href="{{route('aboutus')}}" style="color:#ffc600">{{Lang::get('public.AboutUs')}}</a>&nbsp;|
			<a href="{{route('banking')}}" style="color:#ffc600">{{Lang::get('public.BankingOptions')}}</a>&nbsp;|
			<a href="{{route('contactus')}}" style="color:#ffc600">{{Lang::get('public.ContactUs')}}</a>&nbsp;|
			<a href="{{route('faq')}}" style="color:#ffc600">{{Lang::get('public.FAQ')}}</a>&nbsp;|
			<a href="{{route('howtojoin')}}" style="color:#ffc600">{{Lang::get('public.HowToJoin')}}</a>&nbsp;|
			<a href="{{route('tnc')}}" style="color:#ffc600">{{Lang::get('public.TNC')}}</a>&nbsp;
		  </div>
        </div>
        

        
      </div>
    </footer>
    
    <!-- Required JavaScript Libraries -->
    
    <script src="{{url()}}/lasvegas/resources/js/bootstrap.min.js"></script>
    <script src="{{url()}}/lasvegas/resources/js/owl.carousel.min.js"></script>
    <script src="{{url()}}/lasvegas/resources/js/stellar.min.js"></script>
    <script src="{{url()}}/lasvegas/resources/js/waypoints.min.js"></script>
    <script src="{{url()}}/lasvegas/resources/js/counterup.min.js"></script>
    
    <!-- Template Specisifc Custom Javascript File -->
    <script src="{{url()}}/lasvegas/resources/js/custom.js"></script>
    
    <!--Custom scripts demo background & colour switcher - OPTIONAL -->
    <script src="{{url()}}/lasvegas/resources/js/color-switcher.js"></script>
    
    
  <!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/59f48f9a4854b82732ff86c5/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
  </body>
</html>