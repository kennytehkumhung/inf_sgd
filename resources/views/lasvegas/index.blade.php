@extends('lasvegas/master')
@section('content')
@section('title', 'Home')
<script>
          $(document).ready(function () {
              @if (Session::get('currency') == 'TWD')
                $('body').css('background-image', 'url({{ asset('/front/img/tw/main_bg.jpg') }})');
              @endif

              fakeJackport();
              setInterval(updateJackport, 1000);
          });

          function fakeJackport() {

              var value = Math.floor(Math.random() * 10);

              value = 1356289 + value * 2.496;


              var value2 = value.toFixed(2);

              $('#jp_1').text(addCommas(value2));

          }

          function updateJackport() {
              var val = $("span[id^='jp_1']").text();
              val = removeComma(val);

              if (val > 1358889)
                  val = 1356289;

              var value = Math.floor(Math.random() * 10);
              value = value * 0.027;
              
              var value2 = (parseFloat(val) + value).toFixed(2);

              $('#jp_1').text(addCommas(value2));
          }

          function addCommas(str) {
              var parts = (str + "").split("."),
          main = parts[0],
          len = main.length,
          output = "",
          i = len - 1;

              while (i >= 0) {
                  output = main.charAt(i) + output;
                  if ((len - i) % 3 === 0 && i > 0) {
                      output = "," + output;
                  }
                  --i;
              }
              // put decimal part back
              if (parts.length > 1) {
                  output += "." + parts[1];
              }
              return output;
          }

          function removeComma(str) {
              var parts = (str + "").split(",");

              var output = "";

              for (var i = 0; i < parts.length; i++) {
                  output += parts[i];
              }
              return output;
          }
</script>
<div class="hero" id="highlighted">
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-xs-12">
				 <div id="myCarousel" class="carousel slide" data-ride="carousel">
					<!-- Indicators -->
					<ol class="carousel-indicators">
					<?php $no1=0;?>
					  @if(isset($banners))
                            @foreach( $banners as $key => $banner )
								<li data-target="#myCarousel" data-slide-to="<?php echo $no1?>" <?php if($no1==0){echo "class='active'";}?>></li>	
							<?php $no1++; ?>								
                            @endforeach
                        @endif
					</ol>

					<!-- Wrapper for slides -->
					<a href="{{route('promotion')}}" target="blank">
					<div class="carousel-inner">
					  <?php $no=0;?>
					  @if(isset($banners))
                            @foreach( $banners as $key => $banner )
								<div class="item <?php if($no==0){echo "active";}?>">
									<img src="{{$banner->domain}}/{{$banner->path}}" alt="Los Angeles" style="width:100%;height:auto;" class="img-responsive">
								</div>
								<?php $no++; ?>
                            @endforeach
                        @endif
					</div>
					</a>
                               

					<!-- Left and right controls -->
					<a class="left carousel-control" href="#myCarousel" data-slide="prev">
					  <span class="glyphicon glyphicon-chevron-left"></span>
					  <span class="sr-only">Previous</span>
					</a>
					<a class="right carousel-control" href="#myCarousel" data-slide="next">
					  <span class="glyphicon glyphicon-chevron-right"></span>
					  <span class="sr-only">Next</span>
					</a>
				  </div>
				</div>
				
				<div class="col-md-4 col-xs-12 ab1">
				  <a href="{{route('livecasino')}}"><img class="img-responsive" src="{{url()}}/lasvegas/img/slide-img-right.jpg"></a>
				</div>
			
			</div>
		</div>
    </div>
    
    <!-- ======== @Region: #content ======== -->
    <div id="content">
      <div class="mission text-center block block-pd-sm block-bg-noise">
        <div class="container">
          <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12">
             <img class="img-responsive" src="{{url()}}/lasvegas/img/steps.jpg">
          </div>
          <div class="col-md-6 col-sm-6 col-xs-12 mt01">
             <div class="jpCounter">RM <span id="jp_1">88,888.888.00</span></div>
             <img class="img-responsive" src="{{url()}}/lasvegas/img/jp-bg.jpg">
          </div>
          </div>
        </div>
      </div>
      <div class="mission block-pd-sm block-bg-noise">
        <div class="container">
          <div class="row">
          <div class="col-md-3 col-sm-3 col-xs-6">
             <a href="{{route('ibc')}}"><img class="img-responsive" src="{{url()}}/lasvegas/img/hpg-lc-1.png"></a>
          </div>
          <div class="col-md-3 col-sm-3 col-xs-6">
             <a href="{{route('livecasino')}}"><img class="img-responsive" src="{{url()}}/lasvegas/img/hpg-lc-2.png"></a>
          </div>
          <div class="col-md-3 col-sm-3 col-xs-6">
             <a href="{{route('slot')}}"><img class="img-responsive" src="{{url()}}/lasvegas/img/hpg-lc-3.png"></a>
          </div>
          <div class="col-md-3 col-sm-3 col-xs-6">
             <a href="{{route('4d')}}"><img class="img-responsive" src="{{url()}}/lasvegas/img/hpg-lc-4.png"></a>
          </div>
          </div>
        </div>
      </div>
    </div>
@stop