@extends('lasvegas/master')
@section('title', Lang::get('public.Banking'))
@section('content')
        <!-- ======== @Region: #content ======== -->
    <div id="content noBg">
      <div class="mission block-pd-sm block-bg-noise">
        <div class="container">
        <div class="row">
        <div class="col-md-12 col-xs-12 text-center specialTxt">
        {{Lang::get('public.Banking')}}
        </div>
        </div>
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12 text-white">
					<p><?php echo htmlspecialchars_decode($content); ?></p>
				</div>
			</div>
        </div>
      </div>

      
    </div>
@stop