@extends('lasvegas/master')

@section('title', '4D')



@section('content')
<style>
.table2 table {
    border-collapse: collapse;
    width: 100%;
}

.table2 table th {
    text-align: left;
    padding: 8px;
}

.table2 table td {
    text-align: left;
    padding: 8px;
}

.table2 table tr:nth-child(even){background-color: #f2f2f2}

.table2 table th {
    background-color: #4CAF50;
    color: white;
}
</style>

<div id="dPop" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content bg-black">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Please make selection</h4>
      </div>
      <div class="modal-body">
        <div class="row">
        <div class="col-md-4 col-xs-4">
           <a href="#" onClick="@if (Auth::user()->check())window.open('{{route('psb')}}', 'pubContent', 'width=1000,height=750');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif"><img class="img-responsive" src="{{ asset('/lasvegas/img/4d-icon.png') }}"></a>
        </div>
        <div class="col-md-4 col-xs-4">
           <a href="#" onClick="@if (Auth::user()->check())window.open('{{route('psb5d')}}', 'pubContent', 'width=1000,height=750');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif"><img class="img-responsive" src="{{ asset('/lasvegas/img/5d-icon.png') }}"></a>
        </div>
        <div class="col-md-4 col-xs-4">
           <a href="#" onClick="@if (Auth::user()->check())window.open('{{route('psb4dspc')}}', 'pubContent', 'width=1000,height=750');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif"><img class="img-responsive" src="{{ asset('/lasvegas/img/2d4d-icon.png') }}"></a>
        </div>

        </div>
        
        
   
        
        
        
      </div>
      
    </div>

  </div>
</div>

<div id="content noBg">
      <div class="mission block-pd-sm block-bg-noise bg-4d">
        <div class="container">
        <div class="row ab3">
		<div class="container">
        <a href="#" class="col-md-offset-2 col-md-2 col-sm-3 col-xs-6 text-center ac1" data-toggle="modal" data-target="#myModal">
        <img class="img-responsive ab2" src="{{ asset('/lasvegas/img/PAY-BTN.png') }}">
        </a>
        <div class="col-md-4 col-sm-6 col-xs-12 text-center ac1 hid1">
        <img class="img-responsive" src="{{ asset('/lasvegas/img/4d-tit.png') }}">
        </div>
        <div class="col-md-2 col-sm-3 col-xs-6 text-center ac1">
		<!--<a href="#" onClick="@if (Auth::user()->check())window.open('{{route('psb')}}', 'pubContent', 'width=1000,height=750');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif"><img class="img-responsive ab2" src="{{ asset('/lasvegas/img/bet-btn.png') }}" name="betnow" width="225" height="81" border="0"></a>-->
		<a data-toggle="modal" data-target="#dPop" href="#"><img class="img-responsive ab2" src="{{ asset('/lasvegas/img/bet-btn.png') }}"></a>
        </div>
        </div>
        
        <div class="row mb01">
		<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 ac1">
            <!--magnum-->
            <div id="fD_table">
                <div id="fD_table_header" class="magnum">
                    <div id="fD_table_img">
                        <img src="{{ asset('/lasvegas/img/4D_magnum.png') }}" width="80" height="40" />
                    </div>
			<div id="fD_table_title">
				<span class="title">MAGNUM 4D</span><br/>
					<span>{{ $Magnum['date'] }}</span>
            </div>
          </div>
                <div id="fD_table_result">
                    <table align="center" width="210" border="1" cellpadding="1" cellspacing="0" style="border-color:#999;">
                        <tr>
                            <td width="61" align="center" class="magnum fD_top3_description">1st</td>
                            <td width="61" align="center" class="magnum fD_top3_description">2nd</td>
                            <td align="center" class="magnum fD_top3_description">3rd</td>
                        </tr>
                        <tr>
                            <td align="center" class="fD_top3_no"><span>{{ $Magnum['first'] }}</span></td>
                            <td align="center" class="fD_top3_no"><span>{{ $Magnum['second'] }}</span></td>
                            <td align="center" class="fD_top3_no"><span>{{ $Magnum['third'] }}</span></td>
                        </tr>
                    </table>
                    <table align="center" width="210" border="1" cellpadding="1" cellspacing="0" class="fD_normalPrice">
                        <tr>
                            <td colspan="3" align="center" class="magnum">Special</td>
                        </tr>
                        <tr>
                            <td align="center"><span>{{ $Magnum['special'][0] }}</span></td>
                            <td align="center"><span>{{ $Magnum['special'][1] }}</span></td>
                            <td align="center"><span>{{ $Magnum['special'][2] }}</span></td>
                        </tr>
                        <tr>
                            <td align="center"><span>{{ $Magnum['special'][3] }}</span></td>
                            <td align="center"><span>{{ $Magnum['special'][4] }}</span></td>
                            <td align="center"><span>{{ $Magnum['special'][5] }}</span></td>
                        </tr>
                        <tr>
                            <td align="center"><span>{{ $Magnum['special'][6] }}</span></td>
                            <td align="center"><span>{{ $Magnum['special'][7] }}</span></td>
                            <td align="center"><span>{{ $Magnum['special'][8] }}</span></td>
                        </tr>
                        <tr>
                            <td align="center" bgcolor="#999">&nbsp;</td>
                            <td align="center"><span>{{ $Magnum['special'][9] }}</span></td>
                            <td align="center" bgcolor="#999">&nbsp;</td>
                        </tr>
                    </table>
                    <table align="center" width="210" border="1" cellpadding="1" cellspacing="0" class="fD_normalPrice">
                        <tr>
                            <td colspan="3" align="center" class="magnum">Consolation</td>
                        </tr>
                        <tr>
                            <td align="center"><span>{{ $Magnum['consolation'][0] }}</span></td>
                            <td align="center"><span>{{ $Magnum['consolation'][1] }}</span></td>
                            <td align="center"><span>{{ $Magnum['consolation'][2] }}</span></td>
                        </tr>
                        <tr>
                            <td align="center"><span>{{ $Magnum['consolation'][3] }}</span></td>
                            <td align="center"><span>{{ $Magnum['consolation'][4] }}</span></td>
                            <td align="center"><span>{{ $Magnum['consolation'][5] }}</span></td>
                        </tr>
                        <tr>
                            <td align="center"><span>{{ $Magnum['consolation'][6] }}</span></td>
                            <td align="center"><span>{{ $Magnum['consolation'][7] }}</span></td>
                            <td align="center"><span>{{ $Magnum['consolation'][8] }}</span></td>
                        </tr>
                        <tr>
                            <td align="center" bgcolor="#999">&nbsp;</td>
                            <td align="center"><span>{{ $Magnum['consolation'][9] }}</span></td>
                            <td align="center" bgcolor="#999">&nbsp;</td>
                        </tr>
                    </table>
                </div>
            </div>
            
            <!--end magnum-->
			</div>  
          
          <div class="col-md-3 col-sm-4 col-xs-12 ac1">
			 <!--damacai-->
            <div id="fD_table">
                <div id="fD_table_header" class="damacai">
                    <div id="fD_table_img">
                        <img src="{{ asset('/lasvegas/img/4D_damacai.png') }}" width="80" height="40" />
                    </div>
                    <div id="fD_table_title">
                        <span class="title">DAMACAI 1+3D</span></br>
                        <span>{{ $PMP['date'] }}</span>
                    </div>
                </div>
                <div id="fD_table_result">
                    <table width="210" border="1" cellpadding="1" cellspacing="0" style="border-color:#999;">
                        <tr>
                            <td width="61" align="center" class="damacai fD_top3_description">1st</td>
                            <td width="61" align="center" class="damacai fD_top3_description">2nd</td>
                            <td align="center" class="damacai fD_top3_description">3rd</td>
                        </tr>
                        <tr>
                            <td align="center" class="fD_top3_no"><span id="ContentPlaceHolder1_lbl_PMP_1st">{{ $PMP['first'] }}</span></td>
                            <td align="center" class="fD_top3_no"><span id="ContentPlaceHolder1_lbl_PMP_2nd">{{ $PMP['second'] }}</span></td>
                            <td align="center" class="fD_top3_no"><span id="ContentPlaceHolder1_lbl_PMP_3rd">{{ $PMP['third'] }}</span></td>
                        </tr>
                    </table>
                    <table width="210" border="1" cellpadding="1" cellspacing="0" class="fD_normalPrice">
                        <tr>
                            <td colspan="3" align="center" class="damacai">Special</td>
                        </tr>
                        <tr>
                            <td align="center"><span>{{ $PMP['special'][0] }}</span></td>
                            <td align="center"><span>{{ $PMP['special'][1] }}</span></td>
                            <td align="center"><span>{{ $PMP['special'][2] }}</span></td>
                        </tr>
                        <tr>
                            <td align="center"><span>{{ $PMP['special'][3] }}</span></td>
                            <td align="center"><span>{{ $PMP['special'][4] }}</span></td>
                            <td align="center"><span>{{ $PMP['special'][5] }}</span></td>
                        </tr>
                        <tr>
                            <td align="center"><span>{{ $PMP['special'][6] }}</span></td>
                            <td align="center"><span>{{ $PMP['special'][7] }}</span></td>
                            <td align="center"><span>{{ $PMP['special'][8] }}</span></td>
                        </tr>
                        <tr>
                            <td align="center" bgcolor="#999">&nbsp;</td>
                            <td align="center"><span>{{ $PMP['special'][9] }}</span></td>
                            <td align="center" bgcolor="#999">&nbsp;</td>
                        </tr>
                    </table>
                    <table width="210" border="1" cellpadding="1" cellspacing="0" class="fD_normalPrice">
                        <tr>
                            <td colspan="3" align="center" class="damacai">Consolation</td>
                        </tr>
                        <tr>
                            <td align="center"><span>{{ $PMP['consolation'][0] }}</span></td>
                            <td align="center"><span>{{ $PMP['consolation'][1] }}</span></td>
                            <td align="center"><span>{{ $PMP['consolation'][2] }}</span></td>
                        </tr>
                        <tr>
                            <td align="center"><span>{{ $PMP['consolation'][3] }}</span></td>
                            <td align="center"><span>{{ $PMP['consolation'][4] }}</span></td>
                            <td align="center"><span>{{ $PMP['consolation'][5] }}</span></td>
                        </tr>
                        <tr>
                            <td align="center"><span>{{ $PMP['consolation'][6] }}</span></td>
                            <td align="center"><span>{{ $PMP['consolation'][7] }}</span></td>
                            <td align="center"><span>{{ $PMP['consolation'][8] }}</span></td>
                        </tr>
                        <tr>
                            <td align="center" bgcolor="#999">&nbsp;</td>
                            <td align="center"><span>{{ $PMP['consolation'][9] }}</span></td>
                            <td align="center" bgcolor="#999">&nbsp;</td>
                        </tr>
                    </table>
                </div>
            </div>
            <!--end damacai-->
			 </div>
          
          <div class="col-md-3 col-sm-4 col-xs-12 ac1">
			<!--toto-->
            <div id="fD_table">
                <div id="fD_table_header" class="toto">
                    <div id="fD_table_img">
                        <img src="{{ asset('/lasvegas/img/4D_toto.png') }}" width="80" height="40" />
                    </div>
                    <div id="fD_table_title">
                        <span class="title">TOTO 4D</span></br>
                        <span>{{ $Toto['date'] }}</span>
                    </div>
                </div>
                <div id="fD_table_result">
                    <table width="210" border="1" cellpadding="1" cellspacing="0" style="border-color:#999;">
                        <tr>
                            <td width="61" align="center" class="toto fD_top3_description">1st</td>
                            <td width="61" align="center" class="toto fD_top3_description">2nd</td>
                            <td align="center" class="toto fD_top3_description">3rd</td>
                        </tr>
                        <tr>
                            <td align="center" class="fD_top3_no"><span>{{ $Toto['first'] }}</span></td>
                            <td align="center" class="fD_top3_no"><span>{{ $Toto['second'] }}</span></td>
                            <td align="center" class="fD_top3_no"><span>{{ $Toto['third'] }}</span></td>
                        </tr>
                    </table>
                    <table width="210" border="1" cellpadding="1" cellspacing="0" class="fD_normalPrice">
                        <tr>
                            <td colspan="3" align="center" class="toto">Special</td>
                        </tr>
                        <tr>
                            <td align="center"><span>{{ $Toto['special'][0] }}</span></td>
                            <td align="center"><span>{{ $Toto['special'][1] }}</span></td>
                            <td align="center"><span>{{ $Toto['special'][2] }}</span></td>
                        </tr>
                        <tr>
                            <td align="center"><span>{{ $Toto['special'][3] }}</span></td>
                            <td align="center"><span>{{ $Toto['special'][4] }}</span></td>
                            <td align="center"><span>{{ $Toto['special'][5] }}</span></td>
                        </tr>
                        <tr>
                            <td align="center"><span>{{ $Toto['special'][6] }}</span></td>
                            <td align="center"><span>{{ $Toto['special'][7] }}</span></td>
                            <td align="center"><span>{{ $Toto['special'][8] }}</span></td>
                        </tr>
                        <tr>
                            <td align="center" bgcolor="#999">&nbsp;</td>
                            <td align="center"><span>{{ $Toto['special'][9] }}</span></td>
                            <td align="center" bgcolor="#999">&nbsp;</td>
                        </tr>
                    </table>
                    <table width="210" border="1" cellpadding="1" cellspacing="0" class="fD_normalPrice">
                        <tr>
                            <td colspan="3" align="center" class="toto">Consolation</td>
                        </tr>
                        <tr>
                            <td align="center"><span>{{ $Toto['consolation'][0] }}</span></td>
                            <td align="center"><span>{{ $Toto['consolation'][1] }}</span></td>
                            <td align="center"><span>{{ $Toto['consolation'][2] }}</span></td>
                        </tr>
                        <tr>
                            <td align="center"><span>{{ $Toto['consolation'][3] }}</span></td>
                            <td align="center"><span>{{ $Toto['consolation'][4] }}</span></td>
                            <td align="center"><span>{{ $Toto['consolation'][5] }}</span></td>
                        </tr>
                        <tr>
                            <td align="center"><span>{{ $Toto['consolation'][6] }}</span></td>
                            <td align="center"><span>{{ $Toto['consolation'][7] }}</span></td>
                            <td align="center"><span>{{ $Toto['consolation'][8] }}</span></td>
                        </tr>
                        <tr>
                            <td align="center" bgcolor="#999">&nbsp;</td>
                            <td align="center"><span>{{ $Toto['consolation'][9] }}</span></td>
                            <td align="center" bgcolor="#999">&nbsp;</td>
                        </tr>
                    </table>
                </div>
            </div>
            <!--end toto-->
			 </div>
          
          <div class="col-md-3 col-sm-4 col-xs-12 ac1">
			<!--Singapore-->
            <div id="fD_table2">
                <div id="fD_table_header" class="singapore">
                    <div id="fD_table_img">
                        <img src="{{ asset('/lasvegas/img/4D_singapore.png') }}" width="80" height="40" />
                    </div>
                    <div id="fD_table_title">
                        <span class="title">SINGAPORE 4D</span><br/>
                        <span>{{ $Singapore['date'] }}</span>
                    </div>
                </div>
                <div id="fD_table_result">
                    <table width="210" border="1" cellpadding="1" cellspacing="0" style="border-color:#999;">
                        <tr>
                            <td width="61" align="center" class="singapore fD_top3_description">1st</td>
                            <td width="61" align="center" class="singapore fD_top3_description">2nd</td>
                            <td align="center" class="singapore fD_top3_description">3rd</td>
                        </tr>
                        <tr>
                            <td align="center" class="fD_top3_no"><span>{{ $Singapore['first'] }}</span></td>
                            <td align="center" class="fD_top3_no"><span>{{ $Singapore['second'] }}</span></td>
                            <td align="center" class="fD_top3_no"><span>{{ $Singapore['third'] }}</span></td>
                        </tr>
                    </table>
                    <table width="210" border="1" cellpadding="1" cellspacing="0" class="fD_normalPrice">
                        <tr>
                            <td colspan="3" align="center" class="singapore">Special</td>
                        </tr>
                        <tr>
                            <td align="center"><span>{{ $Singapore['special'][0] }}</span></td>
                            <td align="center"><span>{{ $Singapore['special'][1] }}</span></td>
                            <td align="center"><span>{{ $Singapore['special'][2] }}</span></td>
                        </tr>
                        <tr>
                            <td align="center"><span>{{ $Singapore['special'][3] }}</span></td>
                            <td align="center"><span>{{ $Singapore['special'][4] }}</span></td>
                            <td align="center"><span>{{ $Singapore['special'][5] }}</span></td>
                        </tr>
                        <tr>
                            <td align="center"><span>{{ $Singapore['special'][6] }}</span></td>
                            <td align="center"><span>{{ $Singapore['special'][7] }}</span></td>
                            <td align="center"><span>{{ $Singapore['special'][8] }}</span></td>
                        </tr>
                        <tr>
                            <td align="center" bgcolor="#999">&nbsp;</td>
                            <td align="center"><span>{{ $Singapore['special'][9] }}</span></td>
                            <td align="center" bgcolor="#999">&nbsp;</td>
                        </tr>
                    </table>
                    <table width="210" border="1" cellpadding="1" cellspacing="0" class="fD_normalPrice">
                        <tr>
                            <td colspan="3" align="center" class="singapore">Consolation</td>
                        </tr>
                        <tr>
                            <td align="center"><span>{{ $Singapore['consolation'][0] }}</span></td>
                            <td align="center"><span>{{ $Singapore['consolation'][1] }}</span></td>
                            <td align="center"><span>{{ $Singapore['consolation'][2] }}</span></td>
                        </tr>
                        <tr>
                            <td align="center"><span>{{ $Singapore['consolation'][3] }}</span></td>
                            <td align="center"><span>{{ $Singapore['consolation'][4] }}</span></td>
                            <td align="center"><span>{{ $Singapore['consolation'][5] }}</span></td>
                        </tr>
                        <tr>
                            <td align="center"><span>{{ $Singapore['consolation'][6] }}</span></td>
                            <td align="center"><span>{{ $Singapore['consolation'][7] }}</span></td>
                            <td align="center"><span>{{ $Singapore['consolation'][8] }}</span></td>
                        </tr>
                        <tr>
                            <td align="center" bgcolor="#999">&nbsp;</td>
                            <td align="center"><span>{{ $Singapore['consolation'][9] }}</span></td>
                            <td align="center" bgcolor="#999">&nbsp;</td>
                        </tr>
                    </table>
                </div>
            </div>
            <!--end singapore-->
			 </div>
          
          <div class="col-md-3 col-sm-4 col-xs-12 ac1 ml01">
			 <!--t88-->
            <div id="fD_table1">
                <div id="fD_table_header" class="t88">
                    <div id="fD_table_img">
                        <img src="{{ asset('/lasvegas/img/4D_88.png') }}" width="80" height="40" />
                    </div>
                    <div id="fD_table_title">
                        <span class="title">SABAH 4D</span><br/>
                        <span>{{ $Sabah['date'] }}</span>
                    </div>
                </div>
                <div id="fD_table_result">
                    <table width="210" border="1" cellpadding="1" cellspacing="0" style="border-color:#999;">
                        <tr>
                            <td width="61" align="center" class="t88 fD_top3_description">1st</td>
                            <td width="61" align="center" class="t88 fD_top3_description">2nd</td>
                            <td align="center" class="t88 fD_top3_description">3rd</td>
                        </tr>
                        <tr>
                            <td align="center" class="fD_top3_no"><span>{{ $Sabah['first'] }}</span></td>
                            <td align="center" class="fD_top3_no"><span>{{ $Sabah['second'] }}</span></td>
                            <td align="center" class="fD_top3_no"><span>{{ $Sabah['third'] }}</span></td>
                        </tr>
                    </table>
                    <table width="210" border="1" cellpadding="1" cellspacing="0" class="fD_normalPrice">
                        <tr>
                            <td colspan="3" align="center" class="t88">Special</td>
                        </tr>
                        <tr>
                            <td align="center"><span>{{ $Sabah['special'][0] }}</span></td>
                            <td align="center"><span>{{ $Sabah['special'][1] }}</span></td>
                            <td align="center"><span>{{ $Sabah['special'][2] }}</span></td>
                        </tr>
                        <tr>
                            <td align="center"><span>{{ $Sabah['special'][3] }}</span></td>
                            <td align="center"><span>{{ $Sabah['special'][4] }}</span></td>
                            <td align="center"><span>{{ $Sabah['special'][5] }}</span></td>
                        </tr>
                        <tr>
                            <td align="center"><span>{{ $Sabah['special'][6] }}</span></td>
                            <td align="center"><span>{{ $Sabah['special'][7] }}</span></td>
                            <td align="center"><span>{{ $Sabah['special'][8] }}</span></td>
                        </tr>
                        <tr>
                            <td align="center" bgcolor="#999">&nbsp;</td>
                            <td align="center"><span>{{ $Sabah['special'][9] }}</span></td>
                            <td align="center" bgcolor="#999">&nbsp;</td>
                        </tr>
                    </table>
                    <table width="210" border="1" cellpadding="1" cellspacing="0" class="fD_normalPrice">
                        <tr>
                            <td colspan="3" align="center" class="t88">Consolation</td>
                        </tr>
                        <tr>
                            <td align="center"><span>{{ $Sabah['consolation'][0] }}</span></td>
                            <td align="center"><span>{{ $Sabah['consolation'][1] }}</span></td>
                            <td align="center"><span>{{ $Sabah['consolation'][2] }}</span></td>
                        </tr>
                        <tr>
                            <td align="center"><span>{{ $Sabah['consolation'][3] }}</span></td>
                            <td align="center"><span>{{ $Sabah['consolation'][4] }}</span></td>
                            <td align="center"><span>{{ $Sabah['consolation'][5] }}</span></td>
                        </tr>
                        <tr>
                            <td align="center"><span>{{ $Sabah['consolation'][6] }}</span></td>
                            <td align="center"><span>{{ $Sabah['consolation'][7] }}</span></td>
                            <td align="center"><span>{{ $Sabah['consolation'][8] }}</span></td>
                        </tr>
                        <tr>
                            <td align="center" bgcolor="#999">&nbsp;</td>
                            <td align="center"><span>{{ $Sabah['consolation'][9] }}</span></td>
                            <td align="center" bgcolor="#999">&nbsp;</td>
                        </tr>
                    </table>
                </div>
            </div>
            <!--End of t88-->
			</div>
          
          <div class="col-md-3 col-sm-4 col-xs-12 ac1">
			<!--STC-->
            <div id="fD_table3">
                <div id="fD_table_header" class="stc">
                    <div id="fD_table_img">
                        <img src="{{ asset('/lasvegas/img/4D_stc.png') }}" width="80" height="40" />
                    </div>
                    <div id="fD_table_title">
                        <span class="title">SANDAKAN 4D</span><br/>
                        <span>{{ $Sandakan['date'] }}</span>
                    </div>
                </div>
                <div id="fD_table_result">
                    <table width="210" border="1" cellpadding="1" cellspacing="0" style="border-color:#999;">
                        <tr>
                            <td width="61" align="center" class="stc fD_top3_description">1st</td>
                            <td width="61" align="center" class="stc fD_top3_description">2nd</td>
                            <td align="center" class="stc fD_top3_description">3rd</td>
                        </tr>
                        <tr>
                            <td align="center" class="fD_top3_no"><span>{{ $Sandakan['first'] }}</span></td>
                            <td align="center" class="fD_top3_no"><span>{{ $Sandakan['second'] }}</span></td>
                            <td align="center" class="fD_top3_no"><span>{{ $Sandakan['third'] }}</span></td>
                        </tr>
                    </table>
                    <table width="210" border="1" cellpadding="1" cellspacing="0" class="fD_normalPrice">
                        <tr>
                            <td colspan="3" align="center" class="stc">Special</td>
                        </tr>
                        <tr>
                            <td align="center"><span>{{ $Sandakan['special'][0] }}</span></td>
                            <td align="center"><span>{{ $Sandakan['special'][1] }}</span></td>
                            <td align="center"><span>{{ $Sandakan['special'][2] }}</span></td>
                        </tr>
                        <tr>
                            <td align="center"><span>{{ $Sandakan['special'][3] }}</span></td>
                            <td align="center"><span>{{ $Sandakan['special'][4] }}</span></td>
                            <td align="center"><span>{{ $Sandakan['special'][5] }}</span></td>
                        </tr>
                        <tr>
                            <td align="center"><span>{{ $Sandakan['special'][6] }}</span></td>
                            <td align="center"><span>{{ $Sandakan['special'][7] }}</span></td>
                            <td align="center"><span>{{ $Sandakan['special'][8] }}</span></td>
                        </tr>
                        <tr>
                            <td align="center" bgcolor="#999">&nbsp;</td>
                            <td align="center"><span>{{ $Sandakan['special'][9] }}</span></td>
                            <td align="center" bgcolor="#999">&nbsp;</td>
                        </tr>
                    </table>
                    <table width="210" border="1" cellpadding="1" cellspacing="0" class="fD_normalPrice">
                        <tr>
                            <td colspan="3" align="center" class="stc">Consolation</td>
                        </tr>
                        <tr>
                            <td align="center"><span>{{ $Sandakan['consolation'][0] }}</span></td>
                            <td align="center"><span>{{ $Sandakan['consolation'][1] }}</span></td>
                            <td align="center"><span>{{ $Sandakan['consolation'][2] }}</span></td>
                        </tr>
                        <tr>
                            <td align="center"><span>{{ $Sandakan['consolation'][3] }}</span></td>
                            <td align="center"><span>{{ $Sandakan['consolation'][4] }}</span></td>
                            <td align="center"><span>{{ $Sandakan['consolation'][5] }}</span></td>
                        </tr>
                        <tr>
                            <td align="center"><span>{{ $Sandakan['consolation'][6] }}</span></td>
                            <td align="center"><span>{{ $Sandakan['consolation'][7] }}</span></td>
                            <td align="center"><span>{{ $Sandakan['consolation'][8] }}</span></td>
                        </tr>
                        <tr>
                            <td align="center" bgcolor="#999">&nbsp;</td>
                            <td align="center"><span>{{ $Sandakan['consolation'][9] }}</span></td>
                            <td align="center" bgcolor="#999">&nbsp;</td>
                        </tr>
                    </table>
                </div>
            </div>
            <!--End of STC-->
			 </div>
          
          <div class="col-md-3 col-sm-4 col-xs-12 ac1">
			<!--Cash-->
            <div id="fD_table3">
                <div id="fD_table_header" class="cash">
                    <div id="fD_table_img">
                        <img src="{{ asset('/lasvegas/img/4D_cash.png') }}" width="80" height="40" />
                    </div>
                    <div id="fD_table_title">
                        <span class="title">SPECIAL BIGSWEEP</span><br/>
                        <span>{{ $Sarawak['date'] }}</span>
                    </div>
                </div>
                <div id="fD_table_result">
                    <table width="210" border="1" cellpadding="1" cellspacing="0" style="border-color:#999;">
                        <tr>
                            <td width="61" align="center" class="cash fD_top3_description">1st</td>
                            <td width="61" align="center" class="cash fD_top3_description">2nd</td>
                            <td align="center" class="cash fD_top3_description">3rd</td>
                        </tr>
                        <tr>
                            <td align="center" class="fD_top3_no"><span>{{ $Sarawak['first'] }}</span></td>
                            <td align="center" class="fD_top3_no"><span>{{ $Sarawak['second'] }}</span></td>
                            <td align="center" class="fD_top3_no"><span>{{ $Sarawak['third'] }}</span></td>
                        </tr>
                    </table>
                    <table width="210" border="1" cellpadding="1" cellspacing="0" class="fD_normalPrice">
                        <tr>
                            <td colspan="3" align="center" class="cash">Special</td>
                        </tr>
                        <tr>
                            <td align="center"><span>{{ $Sarawak['special'][0] }}</span></td>
                            <td align="center"><span>{{ $Sarawak['special'][1] }}</span></td>
                            <td align="center"><span>{{ $Sarawak['special'][2] }}</span></td>
                        </tr>
                        <tr>
                            <td align="center"><span>{{ $Sarawak['special'][3] }}</span></td>
                            <td align="center"><span>{{ $Sarawak['special'][4] }}</span></td>
                            <td align="center"><span>{{ $Sarawak['special'][5] }}</span></td>
                        </tr>
                        <tr>
                            <td align="center"><span>{{ $Sarawak['special'][6] }}</span></td>
                            <td align="center"><span>{{ $Sarawak['special'][7] }}</span></td>
                            <td align="center"><span>{{ $Sarawak['special'][8] }}</span></td>
                        </tr>
                        <tr>
                            <td align="center" bgcolor="#999">&nbsp;</td>
                            <td align="center"><span>{{ $Sarawak['special'][9] }}</span></td>
                            <td align="center" bgcolor="#999">&nbsp;</td>
                        </tr>
                    </table>
                    <table width="210" border="1" cellpadding="1" cellspacing="0" class="fD_normalPrice">
                        <tr>
                            <td colspan="3" align="center" class="cash">Consolation</td>
                        </tr>
                        <tr>
                            <td align="center"><span>{{ $Sarawak['consolation'][0] }}</span></td>
                            <td align="center"><span>{{ $Sarawak['consolation'][1] }}</span></td>
                            <td align="center"><span>{{ $Sarawak['consolation'][2] }}</span></td>
                        </tr>
                        <tr>
                            <td align="center"><span>{{ $Sarawak['consolation'][3] }}</span></td>
                            <td align="center"><span>{{ $Sarawak['consolation'][4] }}</span></td>
                            <td align="center"><span>{{ $Sarawak['consolation'][5] }}</span></td>
                        </tr>
                        <tr>
                            <td align="center"><span>{{ $Sarawak['consolation'][6] }}</span></td>
                            <td align="center"><span>{{ $Sarawak['consolation'][7] }}</span></td>
                            <td align="center"><span>{{ $Sarawak['consolation'][8] }}</span></td>
                        </tr>
                        <tr>
                            <td align="center" bgcolor="#999">&nbsp;</td>
                            <td align="center"><span>{{ $Sarawak['consolation'][9] }}</span></td>
                            <td align="center" bgcolor="#999">&nbsp;</td>
                        </tr>
                    </table>
                </div>
            </div>
            <!--End of Cash-->
			 </div>
                  
          </div>
        </div>
      </div>

      
			</div>
        </div>
    </div>
</div>

  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header bg-green" >
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">{{Lang::get('public.Payout')}} {{Lang::get('public.Table')}}</h4>
        </div>
        <div class="modal-body">
        <b>Prize money for Big Forecast</b><br>
        For this Forecast, as shown in the table below, with every RM1 bet, the 1st Prize pays RM3,500, 2nd Prize RM1,200, 3rd Prize RM650, Special Prize RM210 and Consolation Prize RM70.
        <br><br>
        <div class="table2" >	
            <table>
                <tr>
                    <th>BIG FORECAST</th>
                    <th>PRIZE AMOUNT</th>
                </tr>
                <tr>
                    <td>1st Prize</td>
                    <td>RM 3,500.00</td>
                </tr>
                <tr>
                    <td>2nd Prize</td>
                    <td>RM 1,200.00</td>
                </tr>
                <tr>
                    <td>3rd Prize</td>
                    <td>RM 650.00</td>
                </tr>
                <tr>
                    <td>Special Prize</td>
                    <td>RM 210.00</td>
                </tr>
                <tr>
                    <td>Consolation Prize</td>
                    <td>RM 70.00</td>
                </tr>
            </table>
        </div>
		<br><br>
        <b>Prize money for Small Forecast</b><br>
        For this Forecast, as shown in the table below, with every RM1 bet, the 1st Prize pays RM4,800, 2nd Prize RM2,300, 3rd Prize RM1,250.
        <br><br>

        <div class="table2" >	
            <table>
                <tr>
                    <th>SMALL FORECAST</th>
                    <th>PRIZE AMOUNT</th>
                </tr>
                <tr>
                    <td>1st Prize</td>
                    <td>RM 4,800.00</td>
                </tr>
                <tr>
                    <td>2nd Prize</td>
                    <td>RM 2,300.00</td>
                </tr>
                <tr>
                    <td>3rd Prize</td>
                    <td>RM 1,250.00</td>
                </tr>
                
            </table>
        </div>
		<br><br>
        <b>Prize money for 3D</b><br>
        For this Forecast, as shown in the table below, with every RM1 bet, the 1st Prize pays RM 820, 2nd Prize RM820, and 3rd Prize RM820.
        <div class="table2" >	
            <table>
                <tr>
                    <th>3D</th>
                    <th>PRIZE AMOUNT</th>
                </tr>
                <tr>
                    <td>1st Prize</td>
                    <td>RM 820.00</td>
                </tr>
                <tr>
                    <td>2nd Prize</td>
                    <td>RM 820.00</td>
                </tr>
                <tr>
                    <td>3rd Prize</td>
                    <td>RM 820.00</td>
                </tr>
                
            </table>
        </div>
		<br><br>
        <b>Prize money for 5D/6D</b><br>
        For this Forecast, as shown in the table below, with every RM1 bet.
        <div class="table2" >	
            <table>
                <tr>
                    <th>5D</th>
                    <th>PRIZE AMOUNT</th>
                    <th>6D</th>
                    <th>PRIZE AMOUNT</th>
                </tr>
                <tr>
                    <td>1st Prize</td>
                    <td>RM 16,500.00</td>
                    <td>1st Prize</td>
                    <td>RM 100,000.00</td>
                </tr>
                <tr>
                    <td>2nd Prize</td>
                    <td>RM 5,500.00</td>
                    <td>2nd Prize</td>
                    <td>RM 3,000.00</td>
                </tr>
                <tr>
                    <td>3rd Prize</td>
                    <td>RM 3,300.00</td>
                    <td>3rd Prize</td>
                    <td>RM 300.00</td>
                </tr>
                <tr>
                    <td>4th Prize</td>
                    <td>RM 550.00</td>
                    <td>4th Prize</td>
                    <td>RM 30.00</td>
                </tr>
                <tr>
                    <td>5th Prize</td>
                    <td>RM 22.00</td>
                    <td>5th Prize</td>
                    <td>RM 4.00</td>
                </tr>
                <tr>
                    <td>6th Prize</td>
                    <td>RM 5.00</td>
                    <td></td>
                    <td></td>
                </tr>
            </table>
        </div>
        </div>
        <div class="modal-footer" >
			<button type="button" class="btn btn-success" data-dismiss="modal" >Close</button>
        </div>
      </div>
    </div>
  </div>
	<!--end Modal-->
@endsection