@extends('lasvegas/master')
@section('title', 'Forgot Password')
@section('content')
<script>
	
	
	function enterpressalert(e, textarea)
	{
		var code = (e.keyCode ? e.keyCode : e.which);
        if(code == 13) { //Enter keycode
            register_submit();
        }
    }

  /* function register_submit(){
	  
	$.ajax({
		type: "POST",
		url: "{{route('register_process')}}",
		data: {
			_token: "{{ csrf_token() }}",
			username:		 $('#r_username').val(),
			password: 		 $('#r_password').val(),
			repeatpassword:  $('#r_repeatpassword').val(),
			code:    		 $('#r_code').val(),
			email:    		 $('#r_email').val(),
			mobile:    		 $('#r_mobile').val(),
			fullname:        $('#r_fullname').val(),
			dob:			 $('#dob').val(),
			wechat:			 $('#r_wechat').val(),
			line:			 $('#r_line').val(),
            referralid:		 $('#r_referralid').val(),
            mobile_vcode:	 $("#r_mobile_vcode").val()
		},
	}).done(function( json ) {
			 $('.acctTextReg').html('');
			 obj = JSON.parse(json);
			 var str = '';
			 $.each(obj, function(i, item) {
				if( '{{Lang::get('COMMON.SUCESSFUL')}}' == item ){
					alert('{{Lang::get('public.NewMember')}}');
					window.location.href = "{{route('homepage')}}";
				}else{
					//$('.'+i+'_acctTextReg').html(item);
					//str += item + '\n';
					$('.' + i + '_acctTextReg').html('<font style="color:red">' + item + '</font>');
				}
			});
			
			//alert(str);
			
			
	});
} */

  var countDownSecond = 0;
  var intervalObj = null;
  var btnObj = $("#btn_sms");

  function sendVerifySms() {
      /* if (intervalObj != null) {
          alert("{{ Lang::get('public.PleaseWaitForXSecondsToResendSMS') }}".replace(":p1", countDownSecond));
          return false;
      } */

      var mobileObj = $("#r_mobile");
      var mobileNum = mobileObj.val();
	  
	  
      if (mobileNum.length < 1) {
          alert("{{ Lang::get('public.PleaseEnterContactNumberToContinue') }}");
          mobileObj.focus();

          return false;
      }

	  
      if (confirm("{{ Lang::get('public.SendVerificationCodeViaSMSToThisNumber') }} " + mobileNum)) {
          countDownSecond = 60;		  
          smsResendCountDown();

          $.ajax({
              type: "POST",
			  dataType: "json",
              url: "{{url('smsresetpassword')}}",
              data: {
                  _token:     "{{ csrf_token() }}",
                  tel_mobile:	mobileNum,
                  username:	$('#fg_username').val(),
              },
              success: function (result) {
				  window.location.href = "{{route('homepage')}}";
                  if (result.code != 0) {
                      alert(result.msg);
				  }

				  if (result.code == 2) {
                      countDownSecond = 10;
				  }
              },
              error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert('Invalid username or phone number.');//console.log('Invalid username or phone number.');
              }
          });
      }
  }

  function smsResendCountDown() {
      if (intervalObj == null) {
          intervalObj = setInterval(smsResendCountDown, 1000)
      }

      if (countDownSecond <= 1) {
          btnObj.text("Send SMS");
          clearInterval(intervalObj);
          intervalObj = null;
          return;
      }

      
  }
  function getQueryStringValue (key) {  
	  return decodeURIComponent(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + encodeURIComponent(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));  
	} 
</script>
    <!-- ======== @Region: #content ======== -->
    <div id="content" class="mt02 noBg">
      <div class="container">


            
            <div class="row">
               <div class="col-md-12 col-xs-12">
               <div class="cont">
              
               
               <div class="row mt3">
                 
                <div class="col-md-12 col-xs-12">
                     <div class="row">
                        <div class="col-md-12 col-xs-12">
							<h5 class="text-yellow"><i class="fa fa-university mr01" aria-hidden="true"></i> {{Lang::get('public.ForgotPassword')}} :</h5>
							<p style="color:white;">
						  {{Lang::get('public.ForgotPasswordContent3')}}
						  <br><br>
						</p>
                        </div>
                     </div>
                     
                     
                     <div class="row mt03">
                        <div class="col-md-2 col-xs-5">
							<div class="txtWrap">{{Lang::get('COMMON.USERNAME')}} :</div>
                        </div>
                        <div class="col-md-3 col-xs-7">
							<div class="form-group">
								<input class="form-control" id="fg_username" type="text"><span class="acctTextReg"></span>
						   </div>
                       </div>
                     </div>
					 
					  <div class="row mt03">
                        <div class="col-md-2 col-xs-5">
							<div class="txtWrap">{{Lang::get('public.MobilePhone')}} :</div>
                        </div>
                        <div class="col-md-3 col-xs-7">
							<div class="form-group">
								<input class="form-control" id="r_mobile" type="text"><span class="acctTextReg"></span>
						   </div>
                       </div>
                     </div>

					<div class="row">
					  <div class="col-md-4 col-xs-4">
						  <div class="btnNormal ml03">
							<a id="btn_sms" href="javascript:void(0)" onClick="sendVerifySms()" >{{Lang::get('public.Submit')}}</a>
						  </div>
					  </div>
					</div>
                     
                 </div>
               </div>
               
               </div>
               </div>
            </div>
          

      </div>
      </div>
@stop