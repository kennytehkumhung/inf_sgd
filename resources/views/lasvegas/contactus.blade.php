@extends('lasvegas/master')
@section('title', 'Profile')
@section('content')
    <!-- ======== @Region: #content ======== -->
    <div id="content noBg">
      <div class="mission block-pd-sm block-bg-noise bg-contact">
        <div class="container">
        <div class="row">
        <div class="col-md-12 col-xs-12 text-center specialTxt">
        {{Lang::get('public.ContactUs')}}
        </div>
        </div>
        <div class="row">
        <div class="col-md-offset-3 col-md-8 col-sm-12 col-xs-12 text-white">
        <p>
       {!! htmlspecialchars_decode($content) !!}
      <br />
      <div class="ct">
      <ul>
      </ul>
      </div>
      </p>
      </div>
        </div>
        
        
          
        </div>
      </div>

      
    </div>
@stop