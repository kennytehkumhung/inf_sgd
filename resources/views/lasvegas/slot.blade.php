@extends('lasvegas/master')
@section('title','Slots')
@section('content')
<script>
 $(document).ready(function() {



	@if( $type == 'plt' )
		$('.slot_plt_image').hide();
    @elseif( $type == 'spg' )
        $('.slot_spg_image').hide();
    @elseif( $type == 'uc8' )
        $('.slot_uc8_image').hide();
	@endif
 });
 function open_game(gameid){
	 

 }
   function resizeIframe(obj){
     obj.style.height = 0;
     obj.style.height = obj.contentWindow.document.body.scrollHeight + 'px';
  }
  
  function change_iframe(url,image_link,product){
	  //alert(product);
	  $('#iframe_game').attr('src',url);
	  $('.top').show();
	  $('.slot_'+product+'_image').hide();
  }
 </script>
<!-- ======== @Region: #content ======== -->
    <div id="content noBg">
      <div class="mission block-pd-sm block-bg-noise bg-slot">
        <div class="container">
        <div class="row">
        <div class="col-md-12 col-xs-12 text-center">
           <img src="{{url()}}/lasvegas/img/slot-bg.png">
        </div>
        </div>
          <div class="row mb01">
          <div class="col-md-4 col-sm-4 col-xs-12 ac1">
			 <a href="javascript:void(0);" onclick="@if (Auth::user()->check())window.open('{{route('plt' , [ 'type' => 'pgames' , 'category' => '1' ] )}}','','width=1100,height=750')@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif">
			 <img class="img-responsive" src="{{url()}}/lasvegas/img/slot-pt.png"></a>
          </div>
          <div class="col-md-4 col-sm-4 col-xs-12 ac1">
		  <a href="javascript:void(0);" onclick="@if (Auth::user()->check())window.open('{{route('sky' , [ 'type' => 'slot' , 'category' => 'all' ] )}}','','width=1100,height=750')@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif">
			 <img class="img-responsive" src="{{url()}}/lasvegas/img/slot-sky.png"></a>
          </div>
          <div class="col-md-4 col-sm-4 col-xs-12 ac1">
             <a href="javascript:void(0)" onclick="@if (Auth::user()->check())window.open('{{route('scr')}}','','width=1000,height=750')@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif"onclick="change_iframe('{{route('scr')}}')"><img src="{{url()}}/lasvegas/img/slot-scrChange.png"></a>
          </div>
          
          </div>
        </div>
      </div>
      
    </div>
@stop