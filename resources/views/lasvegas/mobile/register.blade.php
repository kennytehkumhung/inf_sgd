@extends('lasvegas/mobile/master')
@section('content')
@section('title', 'Register')

<script>
	$(function () {
        @if ($showSMSField != 1)
            $("#vcode_field").css("visibility", "hidden");
        @elseif (Session::get('currency') != 'MYR')
            $("#vcode_field").css("visibility", "hidden");
        @endif
		
		
    });
	
	auto_fillin();
	
	function auto_fillin(){
		var fullname = getQueryStringValue('fullname');
		var email = getQueryStringValue('email');
		var mobile = getQueryStringValue('mobile');
		$('#r_fullname').val(fullname);
		$('#r_email').val(email);
		$('#r_mobile').val(mobile);

		
	}
	
	function enterpressalert(e, textarea)
	{
		var code = (e.keyCode ? e.keyCode : e.which);
        if(code == 13) { //Enter keycode
            register_submit();
        }
    }

  function register_submit(){
	  
	$.ajax({
		type: "POST",
		url: "{{route('register_process')}}",
		data: {
			_token: "{{ csrf_token() }}",
			username:		 $('#r_username').val(),
			password: 		 $('#r_password').val(),
			repeatpassword:  $('#r_repeatpassword').val(),
			code:    		 $('#r_code').val(),
			email:    		 $('#r_email').val(),
			mobile:    		 $('#r_mobile').val(),
			fullname:        $('#r_fullname').val(),
			dob:			 $('#dob').val(),
			wechat:			 $('#r_wechat').val(),
			line:			 $('#r_line').val(),
            referralid:		 $('#r_referralid').val(),
            mobile_vcode:	 $("#r_mobile_vcode").val()
		},
	}).done(function( json ) {
			 $('.acctTextReg').html('');
			 obj = JSON.parse(json);
			 var str = '';
			 $.each(obj, function(i, item) {
				if( '{{Lang::get('COMMON.SUCESSFUL')}}' == item ){
					alert('{{Lang::get('public.NewMember')}}');
					window.location.href = "{{route('homepage')}}";
				}else{
					//$('.'+i+'_acctTextReg').html(item);
					//str += item + '\n';
					$('.' + i + '_acctTextReg').html('<font style="color:red">' + item + '</font>');
				}
			});
			
			//alert(str);
			
			
	});
}

  var countDownSecond = 0;
  var intervalObj = null;
  var btnObj = $("#btn_sms");

  function sendVerifySms() {
      if (intervalObj != null) {
          alert("{{ Lang::get('public.PleaseWaitForXSecondsToResendSMS') }}".replace(":p1", countDownSecond));
          return false;
      }

      var mobileObj = $("#r_mobile");
      var mobileNum = mobileObj.val();
	  
	  
      if (mobileNum.length < 1) {
          alert("{{ Lang::get('public.PleaseEnterContactNumberToContinue') }}");
          mobileObj.focus();

          return false;
      }

	  
      if (confirm("{{ Lang::get('public.SendVerificationCodeViaSMSToThisNumber') }} " + mobileNum)) {
          countDownSecond = 60;		  
          smsResendCountDown();

          $.ajax({
              type: "POST",
			  dataType: "json",
              url: "{{url('smsregcode')}}",
              data: {
                  _token:     "{{ csrf_token() }}",
                  tel_mobile:	mobileNum
              },
              success: function (result) {
                  if (result.code != 0) {
                      alert(result.msg);
				  }

				  if (result.code == 2) {
                      countDownSecond = 10;
				  }
              },
              error: function (XMLHttpRequest, textStatus, errorThrown) {
//                    console.log('ok failed');
              }
          });
      }
  }

  function smsResendCountDown() {
      if (intervalObj == null) {
          intervalObj = setInterval(smsResendCountDown, 1000)
      }

      if (countDownSecond <= 1) {
          btnObj.text("Send SMS");
          clearInterval(intervalObj);
          intervalObj = null;
          return;
      }

      
  }
  function getQueryStringValue (key) {  
	  return decodeURIComponent(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + encodeURIComponent(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));  
	} 
</script>
<style>
.button {
    background-color: #4CAF50; /* Green */
    border: none;
    color: white;
    padding: 5px 8px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 12px;
    margin: 7px 2px;
    -webkit-transition-duration: 0.4s; /* Safari */
    transition-duration: 0.2s;
    cursor: pointer;
	text-decoration: none;
}

.button1 {
    background-color: green; 
    color: black; 
    border: 2px solid #4CAF50;
	text-decoration: none;
}

.button1:hover {
    background-color: #4CAF50;
    color: white;
	text-decoration: none;
}
</style> 

<div class="row pad01">
<div class="col-md-12 col-xs-12 text-center clr2 hg">
<span class="cornMid"><strong>{{Lang::get('public.Registration')}}</strong></span>
</div>
</div>

<div class="row pad01">
<div class="row">
<div class="col-md-12 col-xs-12 text-black lblmar">
<label>{{Lang::get('public.Username')}}* :</label>
</div>
<div class="col-md-12 col-xs-12 text-black wallI">
<input name="" id="r_username" type="text">
</div>
</div>

<div class="row padTB01">
<div class="col-md-12 col-xs-12 text-black lblmar">
<label>{{Lang::get('public.Password')}}* :</label>
</div>
<div class="col-md-12 col-xs-12 text-black wallI">
<input name="" id="r_password"  type="text">
</div>
</div>

<div class="row padTB01">
<div class="col-md-12 col-xs-12 text-black lblmar">
<label>{{Lang::get('public.RepeatPassword')}}* :</label>
</div>
<div class="col-md-12 col-xs-12 text-black wallI">
<input name="" id="r_repeatpassword" type="text">
</div>
<div class="col-md-12 col-xs-12 text-danger wallI">
{{Lang::get('public.NewPasswordNote')}}
</div>
</div>

<div class="row padTB01">
<div class="col-md-12 col-xs-12 text-black lblmar">
<label>{{Lang::get('public.EmailAddress')}}* :</label>
</div>
<div class="col-md-12 col-xs-12 text-black wallI">
<input name="" id="r_email" type="text">
</div>
</div>

<div class="row padTB01">
<div class="col-md-12 col-xs-12 text-black lblmar">
<label>{{Lang::get('public.ContactNo')}}* :</label>
</div>
<div class="col-md-12 col-xs-12 text-black wallI">
<input name="" id="r_mobile" type="text">
</div>
<div class="col-md-12 col-xs-12 text-black wallI">
	<a href="javascript:void(0);" class="button button1 form-control" id="btn_sms" onclick="sendVerifySms();">{{ Lang::get('public.ClickHereToGetVerificationCode') }}</a>
	<br><span class="mobile_vcode_acctTextReg acctTextReg"></span>
</div>
</div>

<div class="row padTB01">
<div class="col-md-12 col-xs-12 text-black lblmar">
<label>{{ Lang::get('public.VerificationCode') }}* :</label>
</div>
<div class="col-md-12 col-xs-12 text-black wallI">
<input name="" id="r_mobile_vcode" type="text">
</div>
</div>

<div class="row padTB01">
<div class="col-md-12 col-xs-12 text-black lblmar">
<label>{{Lang::get('public.FullName')}}* :</label>
</div>
<div class="col-md-12 col-xs-12 text-black wallI">
<input name="" id="r_fullname" type="text">
</div>
<div class="col-md-12 col-xs-12 text-danger wallI">
{{Lang::get('public.NameCaution')}}
</div>
</div>

<div class="row padTB01">
<div class="col-md-12 col-xs-12 text-black lblmar">
<label>{{Lang::get('public.DOB')}}</label>
</div>
<div class="row nopad">
<div class="col-md-4 col-xs-4 text-black wallI">
<select id="dob">
				<option value="01">01</option>
				<option value="02">02</option>
				<option value="03">03</option>
				<option value="04">04</option>
				<option value="05">05</option>
				<option value="06">06</option>
				<option value="07">07</option>
				<option value="08">08</option>
				<option value="09">09</option>
				<option value="10">10</option>
				<option value="11">11</option>
				<option value="12">12</option>
				<option value="13">13</option>
				<option value="14">14</option>
				<option value="15">15</option>
				<option value="16">16</option>
				<option value="17">17</option>
				<option value="18">18</option>
				<option value="19">19</option>
				<option value="20">20</option>
				<option value="21">21</option>
				<option value="22">22</option>
				<option value="23">23</option>
				<option value="24">24</option>
				<option value="25">25</option>
				<option value="26">26</option>
				<option value="27">27</option>
				<option value="28">28</option>
				<option value="29">29</option>
				<option value="30">30</option>
				<option value="31">31</option>
</select>
</div>
<div class="col-md-4 col-xs-4 text-black wallI">
<select id="dob_month">
				<option value="01">January</option>
				<option value="02">February</option>
				<option value="03">March</option>
				<option value="04">April</option>
				<option value="05">May</option>
				<option value="06">June</option>
				<option value="07">July</option>
				<option value="08">August</option>
				<option value="09">September</option>
				<option value="10">October</option>
				<option value="11">November</option>
				<option value="12">December</option>
</select>
</div>
<div class="col-md-4 col-xs-4 text-black wallI">
<select id="dob_year">
				<option value="2006">2006</option>
				<option value="2005">2005</option>
				<option value="2004">2004</option>
				<option value="2003">2003</option>
				<option value="2002">2002</option>
				<option value="2001">2001</option>
				<option value="2000">2000</option>
				<option value="1999">1999</option>
				<option value="1998">1998</option>
				<option value="1997">1997</option>
				<option value="1996">1996</option>
				<option value="1995">1995</option>
				<option value="1994">1994</option>
				<option value="1993">1993</option>
				<option value="1992">1992</option>
				<option value="1991">1991</option>
				<option value="1990">1990</option>
				<option value="1989">1989</option>
				<option value="1988">1988</option>
				<option value="1987">1987</option>
				<option value="1986">1986</option>
				<option value="1985">1985</option>
				<option value="1984">1984</option>
				<option value="1983">1983</option>
				<option value="1982">1982</option>
				<option value="1981">1981</option>
				<option value="1980">1980</option>
				<option value="1979">1979</option>
				<option value="1978">1978</option>
				<option value="1977">1977</option>
				<option value="1976">1976</option>
				<option value="1975">1975</option>
				<option value="1974">1974</option>
				<option value="1973">1973</option>
				<option value="1972">1972</option>
				<option value="1971">1971</option>
				<option value="1970">1970</option>
				<option value="1969">1969</option>
				<option value="1968">1968</option>
				<option value="1967">1967</option>
				<option value="1966">1966</option>
				<option value="1965">1965</option>
				<option value="1964">1964</option>
				<option value="1963">1963</option>
				<option value="1962">1962</option>
				<option value="1961">1961</option>
				<option value="1960">1960</option>	
</select>
</div>
</div>
</div>

<div class="row pad01 padS">
<div class="col-md-4 col-xs-4 pad01">
<div class="submitBtn">
<a href="#" onClick="register_submit()">{{Lang::get('public.Submit')}}</a>
</div>
</div>
</div>


</div>

<!-- /.container -->

@stop