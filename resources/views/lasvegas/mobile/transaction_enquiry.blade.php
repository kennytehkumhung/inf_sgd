@extends('lasvegas/mobile/master')
@section('title', 'Transaction History')
@section('content')

<script>
$(function () {
	transaction_history();
});
  
function transaction_history(){
$.ajax({
	type: "POST",
	url: "{{action('User\TransactionController@transactionProcess')}}",
	data: {
		_token:   "{{ csrf_token() }}",
		date_from:		$('#date_from').val(),
		date_to:    	$('#date_to').val(),
		record_type:    $('#record_type').val()

	},
}).done(function( json ) {

			//var str;
			 var str = '';
			str += '	<table cellspacing="0" class="table" id="trans_history" style="width:98%;border-collapse:collapse; color:#ffffff; font-size: 13px;overflow-x:scroll"><thead><tr align="left" style="height:30px;"><th>{{Lang::get('public.ReferenceNo')}}</th><th>{{Lang::get('public.DateTime')}}</th><th>{{Lang::get('public.Type')}}</th><th>{{Lang::get('public.Amount')}}({{ Session::get('user_currency') }})</th><th>{{Lang::get('public.Status')}}</th><th>{{Lang::get('public.WithdrawalCode')}}</th><th>{{Lang::get('public.Reason')}}</th></tr></thead></table>';
			
			 obj = JSON.parse(json);
			//alert(obj);
			 $.each(obj, function(i, item) {
				str +=  '<tr><td>'+item.id+'</td><td>'+item.created+'</td><td>'+item.type+'</td><td>'+item.amount+'</td><td>'+item.status+'</td><td style="color:#FF0000;">'+item.cardno+'</td><td>'+item.rejreason+'</td></tr>';
				
			})
				
			//alert(json);
			$('#trans_history').html(str);
			
	});
}

setInterval(updateTrans, 10000);
setInterval(update_mainwallet, 10000);

function updateTrans(){
	$( "#trans_history_button" ).trigger( "click" );
}

</script>
<link href="{{url()}}/lasvegas/resources/css/style.css?v2" rel="stylesheet">

	<link href="{{url()}}/lasvegas/resources/css/font-awesome.min.css" rel="stylesheet">
    <link href="{{url()}}/lasvegas/resources/css/owl.carousel.min.css" rel="stylesheet">
    <link href="{{url()}}/lasvegas/resources/css/owl.theme.min.css" rel="stylesheet">
    <link href="{{url()}}/lasvegas/resources/css/owl.transitions.min.css" rel="stylesheet">
	
<div class="bgB">
<div class="row nopad marginTop01">
<div class="col-md-12 col-xs-12 text-center clr2 hg bg-black">
<span class="cornMid"><strong>{{Lang::get('public.TransactionHistory')}}</strong></span>
</div>
</div>

<div class="row pad01 ">

<div class="col-md-6 col-xs-6">
<div class="col-md-12 col-xs-12 padtp1">
{{Lang::get('public.RecordType')}}:
</div>
</div>
<div class="col-md-6 col-xs-6 wallI">
	<select id="record_type">
		<option value="0" selected>{{Lang::get('public.CreditAndDebitRecords')}}</option>
		<option value="1">{{Lang::get('public.CreditRecords')}}</option>
		<option value="2">{{Lang::get('public.DebitRecords')}}</option>
	</select>
</div>

</div>

<div class="row pad01 ">

<div class="col-md-6 col-xs-6">
<div class="col-md-12 col-xs-12 padtp1">
{{Lang::get('public.DateFrom')}} :
</div>
</div>
<div class="col-md-6 col-xs-6 wallI">
<div class="row nopad">
<div class="col-md-6 col-xs-6">
<input type="" class="datepicker" id="date_from" style="cursor:pointer;" value="{{date('Y-m-d')}}" >
</div>
</div>

</div>

</div>

<div class="row pad01 ">

<div class="col-md-6 col-xs-6">
<div class="col-md-12 col-xs-12 padtp1">
{{Lang::get('public.DateTo')}} :
</div>
</div>
<div class="col-md-6 col-xs-6 wallI">
<div class="row nopad">
<div class="col-md-6 col-xs-6">
<input type="" class="datepicker" id="date_to" style="cursor:pointer;" value="{{date('Y-m-d')}}">
</div>
</div>

</div>

</div>

<div class="row pad01 padB">

<div class="col-md-12 col-xs-12">
<div class="submitBtn pull-right">
<a id="trans_history_button" href="javascript:void(0)" onClick="transaction_history()"> {{Lang::get('public.Submit')}}</a>
</div>
</div>

</div>

</div>
	
	<div class="row marginTop01">
	<div class="table-responsive">
	<table cellspacing="0" class="table" id="trans_history" style="width:98%;border-collapse:collapse; color:#ffffff; font-size: 13px;overflow-x:scroll">
							<thead>
								<tr align="left" style="height:30px;">
									<th scope="col">{{Lang::get('public.ReferenceNo')}}</th>
									<th scope="col">{{Lang::get('public.DateOrTime')}}</th>
									<th scope="col">{{Lang::get('public.Type')}}</th>
									<th scope="col">{{Lang::get('public.Amount')}}</th>
									<th scope="col">{{Lang::get('public.Status')}}</th>
									<th scope="col">{{Lang::get('public.WithdrawalCode')}}</th>
									<th scope="col">{{Lang::get('public.Reason')}}</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<th scope="row">-</th>
									<td>-</td>
									<td>-</td>
									<td>-</td>
									<td>-</td>
									<td>-</td>
								</tr>  
							</tbody>
	</table>
	</div>
	</div>
	

<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script>
    jQuery(document).ready(function($) {
        $('.datepicker').datepicker({
            dateFormat: "yy-mm-dd"
        });
    });
</script>
@endsection