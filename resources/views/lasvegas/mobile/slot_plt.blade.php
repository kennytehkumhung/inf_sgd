
    <link href="{{ asset('/gsc/resources/css/slot2.css') }}" rel="stylesheet" type="text/css" />
<style>
.slot_menu ul li a:link, .slot_menu ul li a:visited  {
	display: block;
	text-decoration: none;
	text-align: center;
	font-weight: 600;
	color: #000;
	height: 35px;
	padding: 0px 11px;
}
</style>
visibility: hidden;
<div class="slotContainerBottom" style="width:360px;">
    <div class="slotMenuHolder" style="width:360px;">
        <div class="slot_menu">
            <ul>
				<!--<li><a href="{{route('plt', [ 'type' => 'pgames' , 'category' => '1' ] )}}">{{ Lang::get('public.ProgressiveGames') }}</a></li>-->
				<li><a href="{{route('plt', [ 'type' => 'brand' , 'category' => '1' ] )}}">{{ Lang::get('public.BrandedGames') }}</a></li>
				<li><a href="{{route('plt', [ 'type' => 'slot' , 'category' => 'slot' ] )}}">{{ Lang::get('public.SlotGames2') }}</a></li>
				<!--<li><a href="{{route('plt', [ 'type' => 'slot' , 'category' => 'arcade' ] )}}">{{ Lang::get('public.Arcades') }}</a></li>-->
				<li><a href="{{route('plt', [ 'type' => 'slot' , 'category' => 'tablecards' ] )}}">{{ Lang::get('public.TableCards') }}</a></li>
            </ul>
        </div>
    </div>


    <div id="slot_lobby" class="col-lg-3 col-md-4 col-xs-3 text-center"  style="width:500px;margin-left:20px;">
        @foreach( $lists as $list )
            <div class="slot_box">
                <a href="javascript:void(0)" onclick="@if (Auth::user()->check())window.open('{{route('pltslotiframe' , [ 'gamecode' => $list['code'] ] )}}', 'plt_slot');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif" >
                    <img src="{{url()}}/front/img/plt/{{$list->code}}.jpg" alt=""/>
                </a>
                <span>{{$list['gameName']}}</span>
            </div>
        <div class="clr"></div>
        @endforeach
    </div>


    <div class="clr"></div>
</div>

