<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Las Vegas - @yield('title')</title>

    <!-- Bootstrap Core CSS -->
	<link href="{{url()}}/lasvegas/mobile/resources/css/bootstrap.min.css" rel="stylesheet">
	
    <!-- Custom CSS -->
    <link href="{{url()}}/lasvegas/mobile/resources/css/modern-business.css" rel="stylesheet">
    <link href="{{url()}}/lasvegas/mobile/resources/css/custom.css" rel="stylesheet">
	<link href="{{url()}}/lasvegas/resources/css/owl.carousel.min.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="{{url()}}/lasvegas/mobile/resources/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<script src="{{url()}}/lasvegas/resources/js/jquery.min.js"></script>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
	
	
<script type="text/javascript">
		$(function () {
			@if (Auth::user()->check())
						load_message();
						//checkUpdatePW();
			@endif
			$('marquee').mouseover(function() {
				this.stop();
			}).mouseout(function() {
				this.start();
			});
		});
		
		function enterpressalert(e, textarea)
		{
			var code = (e.keyCode ? e.keyCode : e.which);
            if(code == 13) { //Enter keycode
              login();
            }
		}
		function enterpressalert2(e, textarea){
			var code = (e.keyCode ? e.keyCode : e.which);
			if(code == 13) { //Enter keycode
			   login2();
			 }
		}
		function livechat() {
        window.open('https://tawk.to/chat/59f48f9a4854b82732ff86c5/default/?$_tawk_popout=true&$_tawk_sk=5a41cfb084673f690be18fbe&$_tawk_tk=c59131c59f286bfef32f487057646444&v=573', '', 'width=1150,height=830');
		}
        function login()
        {
		/* document.getElementById("loginBtnAction").style.display = "none";
		document.getElementById("loading").style.display = "block"; */
			$.ajax({
                type: "POST",
				url: "{{route('login')}}",
				data: {
				_token: "{{ csrf_token() }}",
					username: $('#username').val(),
					password: $('#password').val(),
					code:     $('#code').val(),
				},
			}).done(function(json) {
				obj = JSON.parse(json);
				var str = '';
				$.each(obj, function(i, item) {
					str += item + '\n';
				});
				if ("{{Lang::get('COMMON.SUCESSFUL')}}\n" == str) {
					location.reload();
				} else {
					alert(str);
					document.getElementById("loginBtnAction").style.display = "block";
					document.getElementById("loading").style.display = "none";
				}
			});
        }

        function login2() {
		document.getElementById("loginBtnAction2").style.display = "none";
		document.getElementById("loading2").style.display = "block";
			$.ajax({
                type: "POST",
				url: "{{route('login')}}",
				data: {
                    _token: "{{ csrf_token() }}",
					username: $('#username2').val(),
					password: $('#password2').val(),
					code:     $('#code2').val(),
				},
			}).done(function(json) {
				obj = JSON.parse(json);
				var str = '';
				$.each(obj, function(i, item) {
					str += item + '\n';
				});
				if ("{{Lang::get('COMMON.SUCESSFUL')}}\n" == str){
					location.reload();
				} else {
					alert(str);
					document.getElementById("loginBtnAction2").style.display = "block";
					document.getElementById("loading2").style.display = "none";
				}
			});
        }
		function inbox() {
		window.open('{{route('inbox')	}}', '', 'width=800,height=600');
		}
@if (Auth::user()->check())
		function load_message() {
			$.ajax({
				url: "{{ route('showTotalUnseenMessage') }}",
				type: "GET",
				dataType: "text",
				data: {
				},
				success: function (result) {
					if(result>0){
						$("#totalMessage").html("<b style='color: red'>["+result+"]</b>");
						$("#totalMessage2").html("<b style='color: red'>["+result+"]</b>");
						$("#totalMessage3").html("<b style='color: red'>["+result+"]</b>");
					}else{
						$("#totalMessage").html("["+result+"]");
					}
				},
				error: function (XMLHttpRequest, textStatus, errorThrown) {
				}
			});
		}
        setInterval(load_message,20000);
		
		function update_mainwallet(){
			$.ajax({
				  type: "POST",
				  url: "{{route( 'mainwallet', [ '_token'=> csrf_token() ])}}",
				  beforeSend: function(balance){
				
				  },
				  success: function(balance){
					$('.main_wallet').html(parseFloat(Math.round(balance * 100) / 100).toFixed(2));
					total_balance += parseFloat(balance);
				  }
			 });
				
		}
		function getBalance(type){
			var total_balance = 0;
			$.when(
			
				 $.ajax({
							  type: "POST",
							  url: "{{route('mainwallet', [ '_token'=> csrf_token()])}}",
							  beforeSend: function(balance){
						
							  },
							  success: function(balance){
								$('.main_wallet').html(parseFloat(Math.round(balance * 100) / 100).toFixed(2));
								total_balance += parseFloat(balance);
							  }
				 }),
				@foreach( Session::get('products_obj') as $prdid => $object)
				
			
						 $.ajax({
							  type: "POST",
							  url: "{{route( 'getbalance', [ '_token'=> csrf_token(), 'product'=> $object->code ])}}&rd=<?php echo rand ( 10000, 99999 ); ?>",
							  beforeSend: function(balance){
								$('.{{$object->code}}_balance').html('<img style="" src="{{url()}}/front/img/ajax-loading.gif" width="10" height="10">');
							  },
							  success: function(balance){
								  if( balance != '{{Lang::get('Maintenance')}}')
								  {
									$('.{{$object->code}}_balance').html(parseFloat(Math.round(balance * 100) / 100).toFixed(2));
									total_balance += parseFloat(balance);
								  }
								  else
								  {
									  $('.{{$object->code}}_balance').html(balance);
								  }
							  }
						 })
					  @if($prdid != Session::get('last_prdid')) 
						,
					  @endif
				@endforeach

				
			).then(function() {
				
					$('#total_balance').html(parseFloat(Math.round(total_balance * 100) / 100).toFixed(2));
					$('.total_balance').html(parseFloat(Math.round(total_balance * 100) / 100).toFixed(2));

			});

		}
		function checkUpdatePW(){
			$.ajax({
				type: "GET",
				url: "{{route('checkUpdatePassword')}}",
				data: {},
			}).done(function( json ) {
			obj = JSON.stringify(json);
				 $.each(obj, function(i, item) {
					if(window.location.pathname!="/update-pass"){
						alert(item);
						window.location = "{{route('update-pass')}}";
					}
				});		
			});
		}	
		
@endif
		
	var sDate = new Date("{{date('c')}}");
    var weekstr = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
    var monthstr = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
	
    function updateTime() {
        sDate = new Date(sDate.getTime() + 1000);

        var hour = parseFloat(sDate.getHours());
        var min = parseFloat(sDate.getMinutes());
        var sec = parseFloat(sDate.getSeconds());

        if (hour < 10)
            hour = "0" + hour;

        if (min < 10)
            min = "0" + min;

        if (sec < 10)
            sec = "0" + sec;

        $("#sys_date").html(sDate.getDate() + " " + monthstr[sDate.getMonth()] + " " + sDate.getFullYear() + ", " + weekstr[sDate.getDay()] + ", " + hour + ":" + min + ":" + sec + " (GMT +8)");
    }
	setInterval(updateTime, 1000);
	</script>
</head>

<body>
<div id="loginBx" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">{{Lang::get('public.PleaseLogin')}}</h4>
      </div>
      <div class="modal-body">
        <div class="row">
        <div class="col-md-12 col-xs-12">
        <label>{{Lang::get('public.Username')}}</label>
        </div>
        <div class="col-md-12 col-xs-12 text-black wallI">
        <input type="text" id="username">
        </div>

        </div>
        
        <div class="row padTB01">
        <div class="col-md-12 col-xs-12">
        <label>{{Lang::get('public.Password')}}</label>
        </div>
        <div class="col-md-12 col-xs-12 text-black wallI">
        <input type="password" id="password">
        </div>

        </div>
        
        <div class="row padTB01">
        <div class="col-md-5 col-xs-5 text-black wallI">
        <img src="{{route('captcha', ['type' => 'login_captcha'])}}" width="110px" height="25px"/>
        </div>
        <div class="col-md-7 col-xs-7 text-black wallI">
        <input type="text" id="code">
        </div>

        </div>
        </div>
	<div class="modal-footer">
	<div class="submitBtn bt03 pull-right">
		<a href="#" id="loginBtnAction" onclick="login();">{{Lang::get('public.Submit')}}</a>
	</div>
	
	<div class="bt03 pull-left">
		<a style="color:white" href="{{ route('forgotpassword') }}">{{ Lang::get('public.ForgotPassword') }}</a>
	</div>
        
	</div>
    </div>

	</div>
	</div>

<!-- wrapper -->
<div class="row">
<!-- navigation -->

<!-- navigation -->

<div class="col-md-12 nopad">
@if (Auth::user()->check())
<div class="headerBg">
<div class="row">
<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
</button>

	<div class="logo">
		<a href="{{route('homepage')}}"><img src="{{url()}}/lasvegas/mobile/img/logo.png" height="40"></a>
	</div>

	<div class="regBtn">
		<a href="{{route('logout')}}">
			<img src="{{url()}}/lasvegas/mobile/img/logout-icon.png">
		</a>
	</div>

<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="{{route('homepage')}}"><i class="fa fa-home fa-2x icnCl" aria-hidden="true"></i><span class="post01">{{Lang::get('public.Home')}}</span></a>
                    </li>
                    <li>
                        <a href="{{route('update-profile')}}"><i class="fa fa-info fa-2x icnCl" aria-hidden="true"></i><span class="post01">{{Lang::get('public.MyAccount')}}</span></a>
                    </li>
                    <li>
                         <a href="{{route('promotion')}}"><i class="fa fa-gift fa-2x icnCl" aria-hidden="true"></i><span class="post01">{{Lang::get('public.Promotion')}}</span></a>
                    </li>
                    <li>
                        <a href="{{route('mobile')}}"><i class="fa fa-mobile fa-2x icnCl" aria-hidden="true"></i><span class="post01">{{Lang::get('public.MobileDownload')}}</span></a>
                    </li>
                    <li>
                         <a href="javascript:void(0);" onclick="livechat()"><i class="fa fa-weixin fa-2x icnCl" aria-hidden="true"></i><span class="post01">{{Lang::get('public.LiveChat2')}}</span></a>
                    </li>
                    <li>
                        <a href="{{route('transfer')}}"><i class="fa fa-money fa-2x icnCl" aria-hidden="true"></i><span class="post01">{{Lang::get('public.MyWallet')}}</span></a>
                        
                    </li>
                    <li>
                    <span class="cgLang">{{Lang::get('public.ChangeLanguage')}} <a href="{{route( 'language', [ 'lang'=> 'cn'])}}">{{Lang::get('public.Chinese')}}</a> / <a href="{{route( 'language', [ 'lang'=> 'en'])}}">{{Lang::get('public.English')}}</a></span>
                    </li>
                    
                  
                </ul>
               
                
                
            </div>
</div>


</div>
<div class="row bg-red">
<div class="col-md-4 col-xs-4 nopad">
<div class="accBtn">
<a href="{{route('update-profile')}}">{{Session::get('username')}}</a>
</div>
</div>
<div class="col-md-4 col-xs-4 nopad">
<div class="accBtn">
<a data-toggle="modal" data-target="#myModal" href="#">{{Lang::get('public.MyAccount')}}</a>
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">{{Lang::get('public.AccountProfile')}}</h4>
      </div>
      <div class="modal-body">
        <div class="row">
        <div class="col-md-12 col-xs-12">
        <ul>
        <li><a href="{{route('update-profile')}}">{{Lang::get('public.Profile')}}</a></li>
        <li><a href="{{route('update-pass')}}">{{Lang::get('public.ChangePassword')}}</a></li>
        <li><a href="{{route('deposit')}}">{{Lang::get('public.Deposit')}}</a></li>
        <li><a href="{{route('withdraw')}}">{{Lang::get('public.Withdrawal')}}</a></li>
        <li><a href="{{route('transfer')}}">{{Lang::get('public.Transfer')}}</a></li>
        <li><a href="{{route('transaction')}}">{{Lang::get('public.TransactionHistory')}}</a></li>
        </ul>
        </div>

        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">{{Lang::get('COMMON.CLOSE')}}</button>
      </div>
    </div>

  </div>
</div>
</div>
</div>

<div class="col-md-4 col-xs-4 nopad">
<div class="accBtn">
<a href="{{route('logout')}}">{{Lang::get('public.Logout')}}</a>
</div>
</div>
</div>

<div class="row padTB03 announceBg">
<div class="col-md-2 col-xs-1">
<i class="fa fa-volume-up clr1" aria-hidden="true"></i>
</div>
<div class="col-md-10 col-xs-11">
<marquee>{{ App\Http\Controllers\User\AnnouncementController::index() }}</marquee>
</div>
</div>
@yield('content')
<div class="row pad01 padS">

<div class="row">
<div class="equalHWrap eqWrap">
    <div class="equalHW selected1 eq text-center">
    <a href="{{route('homepage')}}">
    <div class="btmIcn"><img src="{{url()}}/lasvegas/mobile/img/btm-home.png"></div>
    <div class="btmTxt">{{Lang::get('public.Home')}}</div>
    </a>
    </div>
    <div class="equalHW eq text-center">
    <a href="{{route('transfer')}}">
    <div class="btmIcn"><img src="{{url()}}/lasvegas/mobile/img/btm-wallet.png"></div>
    <div class="btmTxt">{{Lang::get('public.MyWallet')}}</div>
    </a>
    </div>
    <div class="equalHW eq text-center">
    <a href="{{route('deposit')}}">
    <div class="btmIcn"><img src="{{url()}}/lasvegas/mobile/img/btm-fund.png"></div>
    <div class="btmTxt">{{Lang::get('public.Deposit')}}</div>
    </a>
    </div>
    <div class="equalHW eq text-center">
    <a href="{{route('withdraw')}}">
    <div class="btmIcn"><img src="{{url()}}/lasvegas/mobile/img/withdrawal.png"></div>
    <div class="btmTxt">{{Lang::get('public.Withdrawal')}}</div>
    </a>
    </div>
    <div class="equalHW eq text-center">
    <a href="javascript:void(0);" onclick="livechat()">
    <div class="btmIcn"><img src="{{url()}}/lasvegas/mobile/img/btm-chat.png"></div>
    <div class="btmTxt">{{Lang::get('public.LiveChat2')}}</div>
    </a>
    </div>
</div>
</div>	
@else
<div class="headerBg">
<div class="row">
<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
</button>

<div class="logo">
<a href="{{route('homepage')}}"><img src="{{url()}}/lasvegas/mobile/img/logo.png" height="40"></a>
</div>


	<div class="logBtn">
		<a data-toggle="modal" data-target="#loginBx" href="#">
			<img src="{{url()}}/lasvegas/mobile/img/login-icon.png">
		</a>
	</div>
   
	<div class="regBtn">
		<a href="{{route('register')}}">
			<img src="{{url()}}/lasvegas/mobile/img/register-icon.png">
		</a>
	</div>

			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="{{route('homepage')}}"><i class="fa fa-home fa-2x icnCl" aria-hidden="true"></i><span class="post01">{{Lang::get('public.Home')}}</span></a>
                    </li>
                    <li>
                        <a href="javascript:void(0)" onclick="alert('{{Lang::get('COMMON.PLEASELOGIN')}}')"><i class="fa fa-info fa-2x icnCl" aria-hidden="true"></i><span class="post01">{{Lang::get('public.MyAccount')}}</span></a>
                    </li>
                    <li>
                         <a href="{{route('promotion')}}"><i class="fa fa-gift fa-2x icnCl" aria-hidden="true"></i><span class="post01">{{Lang::get('public.Promotion')}}</span></a>
                    </li>
                    <li>
                        <a href="{{route('mobile')}}"><i class="fa fa-mobile fa-2x icnCl" aria-hidden="true"></i><span class="post01">{{Lang::get('public.MobileDownload')}}</span></a>
                    </li>
                    <li>
                         <a href="javascript:void(0);" onclick="livechat()"><i class="fa fa-weixin fa-2x icnCl" aria-hidden="true"></i><span class="post01">{{Lang::get('public.LiveChat2')}}</span></a>
                    </li>
                    <li>
                        <a href="javascript:void(0)" onclick="alert('{{Lang::get('COMMON.PLEASELOGIN')}}')"><i class="fa fa-money fa-2x icnCl" aria-hidden="true"></i><span class="post01">{{Lang::get('public.MyWallet')}}</span></a>
                        
                    </li>
                    <li>
                    <span class="cgLang">{{Lang::get('public.ChangeLanguage')}}<a href="{{route( 'language', [ 'lang'=> 'cn'])}}">{{Lang::get('public.Chinese')}}</a> / <a href="{{route( 'language', [ 'lang'=> 'en'])}}">{{Lang::get('public.English')}}</a></span>
                    </li>
                </ul>
			</div>
</div>


</div>

<div class="row padTB03 announceBg">
<div class="col-md-2 col-xs-1">
<i class="fa fa-volume-up clr1" aria-hidden="true"></i>
</div>
<div class="col-md-10 col-xs-11">
<marquee>{{ App\Http\Controllers\User\AnnouncementController::index() }}</marquee>
</div>
</div>
@yield('content')
<div class="row pad01 padS">

<div class="row">
<div class="equalHWrap eqWrap">
    <div class="equalHW selected1 eq text-center">
    <a href="{{route('homepage')}}">
    <div class="btmIcn"><img src="{{url()}}/lasvegas/mobile/img/btm-home.png"></div>
    <div class="btmTxt">{{Lang::get('public.Home')}}</div>
    </a>
    </div>
    <div class="equalHW eq text-center">
    <a href="javascript:void(0)" onclick="alert('{{Lang::get('COMMON.PLEASELOGIN')}}')">
    <div class="btmIcn"><img src="{{url()}}/lasvegas/mobile/img/btm-wallet.png"></div>
    <div class="btmTxt">{{Lang::get('public.MyWallet')}}</div>
    </a>
    </div>
    <div class="equalHW eq text-center">
    <a href="javascript:void(0)" onclick="alert('{{Lang::get('COMMON.PLEASELOGIN')}}')">
    <div class="btmIcn"><img src="{{url()}}/lasvegas/mobile/img/btm-fund.png"></div>
    <div class="btmTxt">{{Lang::get('public.Deposit')}}</div>
    </a>
    </div>
    <div class="equalHW eq text-center">
    <a href="javascript:void(0)" onclick="alert('{{Lang::get('COMMON.PLEASELOGIN')}}')">
    <div class="btmIcn"><img src="{{url()}}/lasvegas/mobile/img/withdrawal.png"></div>
    <div class="btmTxt">{{Lang::get('public.Withdrawal')}}</div>
    </a>
    </div>
    <div class="equalHW eq text-center">
    <a href="javascript:void(0);" onclick="livechat()">
    <div class="btmIcn"><img src="{{url()}}/lasvegas/mobile/img/btm-chat.png"></div>
    <div class="btmTxt">{{Lang::get('public.LiveChat2')}}</div>
    </a>
    </div>
</div>
</div>
@endif
    


    
    <!-- /footer -->
</div>

</div>
<!-- wrapper -->

    <!-- jQuery -->
    <script src="{{url()}}/lasvegas/mobile/resources/js/jquery-1.11.3.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="{{url()}}/lasvegas/mobile/resources/js/bootstrap.min.js"></script>

    <!-- Script to Activate the Carousel -->
    <script src="{{url()}}/lasvegas/mobile/resources/js/jssor.slider-21.1.6.mini.js" type="text/javascript"></script>
    <script type="text/javascript">
        jQuery(document).ready(function ($) {

            var jssor_1_SlideoTransitions = [
              [{b:-1,d:1,o:-1},{b:0,d:1000,o:1}],
              [{b:1900,d:2000,x:-379,e:{x:7}}],
              [{b:1900,d:2000,x:-379,e:{x:7}}],
              [{b:-1,d:1,o:-1,r:288,sX:9,sY:9},{b:1000,d:900,x:-1400,y:-660,o:1,r:-288,sX:-9,sY:-9,e:{r:6}},{b:1900,d:1600,x:-200,o:-1,e:{x:16}}]
            ];

            var jssor_1_options = {
              $AutoPlay: true,
              $SlideDuration: 800,
              $SlideEasing: $Jease$.$OutQuint,
              $CaptionSliderOptions: {
                $Class: $JssorCaptionSlideo$,
                $Transitions: jssor_1_SlideoTransitions
              },
              $ArrowNavigatorOptions: {
                $Class: $JssorArrowNavigator$
              },
              $BulletNavigatorOptions: {
                $Class: $JssorBulletNavigator$
              }
            };

            var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

            /*responsive code begin*/
            /*you can remove responsive code if you don't want the slider scales while window resizing*/
            function ScaleSlider() {
                var refSize = jssor_1_slider.$Elmt.parentNode.clientWidth;
                if (refSize) {
                    refSize = Math.min(refSize, 1920);
                    jssor_1_slider.$ScaleWidth(refSize);
                }
                else {
                    window.setTimeout(ScaleSlider, 30);
                }
            }
            ScaleSlider();
            $(window).bind("load", ScaleSlider);
            $(window).bind("resize", ScaleSlider);
            $(window).bind("orientationchange", ScaleSlider);
            /*responsive code end*/
        });
    </script>
    
    <script>
	// Instantiate the Bootstrap carousel
$('.multi-item-carousel').carousel({
  interval: false
});

// for every slide in carousel, copy the next slide's item in the slide.
// Do the same for the next, next item.
$('.multi-item-carousel .item').each(function(){
  var next = $(this).next();
  if (!next.length) {
    next = $(this).siblings(':first');
  }
  next.children(':first-child').clone().appendTo($(this));
  
  if (next.next().length>0) {
    next.next().children(':first-child').clone().appendTo($(this));
  } else {
  	$(this).siblings(':first').children(':first-child').clone().appendTo($(this));
  }
});
	</script>
    
    

</body>

</html>
