@extends('lasvegas/mobile/master')
@section('content')
@section('title', 'Home')

		<div id="jssor_1" style="position: relative; margin: 0 auto; top: 0px; left: 0px; width: 1069px; height: 382px; overflow: hidden; visibility: hidden;">
        <!-- Loading Screen -->
        <div data-u="loading" style="position: absolute; top: 0px; left: 0px;">
            <div style="filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block; top: 0px; left: 0px; width: 100%; height: 100%;"></div>
            <div style="position:absolute;display:block;background:url('{{url()}}/gsc/mobile/resources/img/loading.gif') no-repeat center center;top:0px;left:0px;width:100%;height:100%;"></div>
        </div>
        <div data-u="slides" style="cursor: default; position: relative; top: 0px; left: 0px; width: 1069px; height: 382px; overflow: hidden;">
	@if(isset($banners))
		@foreach( $banners as $key => $banner )
			<div data-p="225.00">
                <a href="{{route('promotion')}}"><img data-u="image" src="{{$banner->domain}}/{{$banner->path}}" /></a>
            </div>
		@endforeach
	@endif
            
        </div>
        <!-- Bullet Navigator -->
        <div data-u="navigator" class="jssorb05" style="bottom:26px;right:16px;" data-autocenter="1">
            <!-- bullet navigator item prototype -->
            <div data-u="prototype" style="width:16px;height:16px;"></div>
        </div>
        <!-- Arrow Navigator -->
        <span data-u="arrowleft" class="jssora22l" style="top:-50px !important;left:8px;width:40px;height:58px;" data-autocenter="2"></span>
        <span data-u="arrowright" class="jssora22r" style="top:0px;right:8px;width:40px;height:58px;" data-autocenter="2"></span>
</div>
	
<div class="row pad01 padS">
<div class="col-md-12">
<div class="row">



<div class="col-lg-3 col-sm-3 col-xs-3 lyrTop text-center">
<a href="{{ route('mobile') }}">
<img class="img-responsive img-portfolio img-hover" src="{{url()}}/lasvegas/mobile/img/hpg-icon-4.png" alt="">
<div class="gmTxt">Playtech Casino</div>
</a>
</div>

<div class="col-lg-3 col-sm-3 col-xs-3 lyrTop text-center">
<a href="javascript:void(0)" onClick="@if (Auth::user()->check())window.open('{{route('slot', [ 'type' => 'plt' ] )}}', 'plt', 'width=450,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif">
<img class="img-responsive img-portfolio img-hover" src="{{url()}}/lasvegas/mobile/img/hpg-icon-4.png" alt="">
<div class="gmTxt">Playtech Slot</div>
</a>
</div>

<div class="col-lg-3 col-sm-3 col-xs-3 lyrTop text-center">
<a href="{{route('mobile')}}">
<img class="img-responsive img-portfolio img-hover" src="{{url()}}/lasvegas/mobile/img/hpg-icon-5.png" alt="">
<div class="gmTxt">Sky3888 Slot</div>
</a>
</div>

<div class="col-lg-3 col-sm-3 col-xs-3 lyrTop text-center">
<a href="javascript:void(0)" onclick="@if (Auth::user()->check())window.open('{{route('scr')}}','','width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif">
<img class="img-responsive img-portfolio img-hover" src="{{url()}}/lasvegas/mobile/img/hpg-icon-6change.png" alt="">
<div class="gmTxt">918Kiss Slot</div>
</a>
</div>

<div class="col-lg-3 col-sm-3 col-xs-3 lyrTop text-center">
<a href="javascript:void(0)" onclick="@if (Auth::user()->check())window.open('{{route('ibc')}}', 'ibc', 'width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif">
<img class="img-responsive img-portfolio img-hover" src="{{url()}}/lasvegas/mobile/img/hpg-icon-7.png" alt="">
<div class="gmTxt">I-Sport</div>
</a>
</div>

<div class="col-lg-3 col-sm-3 col-xs-3 lyrTop text-center">
<a href="javascript:void(0)" onClick="@if (Auth::user()->check())window.open('{{ route('ctb') }}', '', 'width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif">
<img class="img-responsive img-portfolio img-hover" src="{{url()}}/lasvegas/mobile/img/hpg-icon-8.png" alt="">
<div class="gmTxt">Horse Racing</div>
</a>
</div>

<div class="col-lg-3 col-sm-3 col-xs-3 lyrTop text-center">
<a href="{{route('promotion')}}">
<img class="img-responsive img-portfolio img-hover" src="{{url()}}/lasvegas/mobile/img/hpg-icon-9.png" alt="">
<div class="gmTxt">Promotion</div>
</a>
</div>

<div class="col-lg-3 col-sm-3 col-xs-3 lyrTop text-center">
<a href="{{route('mobile')}}">
<img class="img-responsive img-portfolio img-hover" src="{{url()}}/lasvegas/mobile/img/hpg-icon-10.png" alt="">
<div class="gmTxt">Mobile</div>
</a>
</div>

<div class="col-lg-3 col-sm-3 col-xs-3 lyrTop text-center">
<a href="javascript:void(0);" onClick="@if (Auth::user()->check())window.open('{{route('vgs')}}','','width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif">
<img class="img-responsive img-portfolio img-hover" src="{{url()}}/lasvegas/mobile/img/hpg-icon-1.png" alt="">
<div class="gmTxt">Evolution Casino</div>
</a>
</div>

<div class="col-lg-3 col-sm-3 col-xs-3 lyrTop text-center">
<a href="{{ route('mobile') }}">
<img class="img-responsive img-portfolio img-hover" src="{{url()}}/lasvegas/mobile/img/hpg-icon-2.png" alt="">
<div class="gmTxt">Maxbet Casino</div>
</a>
</div>

<div class="col-lg-3 col-sm-3 col-xs-3 lyrTop text-center">
<a href="javascript:void(0);" onclick="{{ Auth::user()->check() ? 'window.open("'.route('hog').'","casino_gp","width=1150,height=830");' : 'alert("'.Lang::get('COMMON.PLEASELOGIN').'");' }}">
<img class="img-responsive img-portfolio img-hover" src="{{url()}}/lasvegas/mobile/img/hpg-icon-3.png" alt="">
<div class="gmTxt">HoGaming Casino</div>
</a>
</div>




</div>
</div>    

</div>
@endsection