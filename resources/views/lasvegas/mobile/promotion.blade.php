@extends('lasvegas/mobile/master')

@section('title', 'Promotions')

@section('content')
<link href="{{ asset('/royalewin/resources/css/promo.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('/lasvegas/resources/css/style4.css') }}?v2" rel="stylesheet" type="text/css" />
<script type="text/javascript">
    $(function($) {
        $('#accordion').find('.accordion-toggle').click(function(){
            //Expand or collapse this panel
            $(this).next().slideToggle('fast');

            //Hide the other panels
            $(".accordion-content").not($(this).next()).slideUp('fast');
        });
    });
	
	 function modalBox(title,content){
		$("#titleBox").html(title);
        $("#contentBox").html(content);
	 }
</script>

<div id="content noBg">
	<div class="btm70 adj13 prm">
		@foreach ($promo as $key => $value )
			<div class="col-md-12 col-xs-12">
				<a data-toggle="modal" data-target="#promoBx" href="#" onclick="modalBox('{{$value['title']}}','{{$value['content']}}')"><img class="img-responsive" src="{{$value['image']}}"></a>
			</div>
		@endforeach
	</div>
</div>
<div id="promoBx" class="modal fade" role="dialog">
	<div class="modal-dialog bg-green">
		<div class="modal-content">
			<div class="modal-header bg-green">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title" id="titleBox" style="color:white"></h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12 col-xs-12" id="contentBox">
					</div>
				</div>
			</div>
			<div class="modal-footer bg-green">
				<div class="fp pull-left">
				</div>
				<div class="pull-right">
					<a href="{{route('deposit')}}" class="btn btn-success">Join Now</a>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection