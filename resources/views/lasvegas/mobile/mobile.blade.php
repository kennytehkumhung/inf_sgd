@extends('lasvegas/mobile/master')
@section('title', 'Mobile')
@section('content')

<div class="row nopad marginTop01">
<div class="col-md-12 col-xs-12 text-center clr2 hg">
<span class="cornMid"><strong>{{Lang::get('public.MobileDownload')}}</strong></span>
</div>
</div>

<div class="row pad01 marginBottom01 padB">
<div class="col-md-6 col-xs-6">
<img class="img-responsive" src="{{url()}}/lasvegas/mobile/img/918Kiss_Mobile.png">
<img class="img-qr" src="{{url()}}/lasvegas/mobile/img/qr/scr(ios).jpg">

<div class="mobLink">
<a href="http://m.scr-888.co">IOS {{Lang::get('public.Slots')}} {{Lang::get('public.Download')}}</a>
</div>
<div class="mobName">
<a href="#">
					@if(Session::has('username'))
						{{Lang::get('public.Username')}}:  <br>
						<strong>{{Session::get('scrid')}}</strong><br>
						{{Lang::get('public.Password')}}: <br>
						<strong>Lv3&nbsp + {{Lang::get('public.YourLoginPassword')}}</strong>
					@else
						{{Lang::get('public.PleaseLoginToCheckYourUsername')}}!
					@endif
</a>
</div>

<div class="clearfix"></div>
</div>

<div class="col-md-6 col-xs-6">
<img class="img-responsive" src="{{url()}}/lasvegas/mobile/img/918Kiss_Mobile.png">
<img class="img-qr" src="{{url()}}/lasvegas/mobile/img/qr/scr(slot).jpg"> <!--height="150" width="160"-->

<div class="mobLink">
<a href="http://m.scr-888.co">Android {{Lang::get('public.Slots')}} {{Lang::get('public.Download')}}</a>
</div>

<div class="mobName">
<a href="#">
				@if(Session::has('scrid'))
						{{Lang::get('public.Username')}}:<br>
						<strong>{{Session::get('scrid')}}</strong><br>
						{{Lang::get('public.Password')}}: <br>
						<strong>Lv3&nbsp + {{Lang::get('public.YourLoginPassword')}}</strong>
					@else
						{{Lang::get('public.PleaseLoginToCheckYourUsername')}}!
					@endif
</a>
</div>

<div class="clearfix"></div>
</div>

<div class="col-md-6 col-xs-6 mt4">
<img class="img-responsive" src="{{url()}}/lasvegas/mobile/img/mob-dl-pt.png">
<img class="img-qr" src="{{url()}}/lasvegas/mobile/img/qr/plt_slot.png">

<div class="mobLink">
<a href="http://m.ld176988.com/download.html">Android {{Lang::get('public.Slots')}} {{Lang::get('public.Download')}}</a>
</div>

<div class="mobName">
<a href="#">
					@if(Session::has('username'))
						{{Lang::get('public.Username')}}:<br>
						<strong>MVGS_{{Session::get('username')}}</strong>
					@else
						{{Lang::get('public.PleaseLoginToCheckYourUsername')}}!
					@endif
</a>
</div>

<div class="clearfix"></div>
</div>

<div class="col-md-6 col-xs-6 mt4">
<img class="img-responsive" src="{{url()}}/lasvegas/mobile/img/mob-dl-pt.png">
<img class="img-qr" src="{{url()}}/lasvegas/mobile/img/qr/plt(casino).png">
<div class="mobLink">
<a href="http://m.ld176988.com/live/download.html">Android {{Lang::get('public.LiveCasino')}} {{Lang::get('public.Download')}}</a>
</div>

<div class="mobName">
<a href="#">
					@if(Session::has('username'))
						{{Lang::get('public.Username')}}:<br>
						<strong>MVGS_{{Session::get('username')}}</strong>
					@else
						{{Lang::get('public.PleaseLoginToCheckYourUsername')}}!
					@endif
</a>
</div>

<div class="clearfix"></div>
</div>

<div class="col-md-6 col-xs-6 mt4">
<img class="img-responsive" src="{{url()}}/lasvegas/mobile/img/mob-dl-sky.png">
<img class="img-qr" src="{{url()}}/lasvegas/mobile/img/qr/sky(ios).png">
<div class="mobLink">
<a href="https://ed.sky3888a.com/sky1388.html">IOS {{Lang::get('public.Slots')}} {{Lang::get('public.Download')}}</a>
</div>

<div class="mobName">
<a href="#">
					@if(Session::has('username'))
					    {{Lang::get('public.Username')}}:<br>
						<strong>{{Config::get(Session::get('currency').'.sky.agid').((new \App\libraries\providers\SKY())->formatUserId(Session::get('userid')))}}</strong>
					@else
						{{Lang::get('public.PleaseLoginToCheckYourUsername')}}!
					@endif
</a>
</div>

<div class="clearfix"></div>
</div>

<div class="col-md-6 col-xs-6 mt4">
<img class="img-responsive" src="{{url()}}/lasvegas/mobile/img/mob-dl-sky.png">
<img class="img-qr" src="{{url()}}/lasvegas/mobile/img/qr/sky(and).png">
<div class="mobLink">
<a href="https://ed.sky3888a.com/sky1388_android.html">Android {{Lang::get('public.Slots')}} {{Lang::get('public.Download')}}</a>
</div>

<div class="mobName">
<a href="#">
					@if(Session::has('username'))
					   {{Lang::get('public.Username')}}:<br>
						<strong>{{Config::get(Session::get('currency').'.sky.agid').((new \App\libraries\providers\SKY())->formatUserId(Session::get('userid')))}}</strong>
					@else
						{{Lang::get('public.PleaseLoginToCheckYourUsername')}}!
					@endif
</a>
</div>

<div class="clearfix"></div>
</div>

<div class="col-md-6 col-xs-6 mt4">
<img class="img-responsive" src="{{url()}}/lasvegas/mobile/img/mob-dl0mxb.png">
<img class="img-qr" src="{{url()}}/lasvegas/mobile/img/qr/mxb.jpg">
<div class="mobLink">
<a href="https://xprogaming.hs.llnwd.net/mobileapk/967-UB-Las%20Vegas-MYR-5.2.0.apk">Android {{Lang::get('public.LiveCasino')}} {{Lang::get('public.Download')}}</a>
</div>

<div class="mobName">
<a href="#">
					@if(Session::has('username'))
						{{Lang::get('public.Username')}}:<br>
						<strong>{{strtoupper(Session::get('username'))}}</strong>
						
					@else
						{{Lang::get('public.PleaseLoginToCheckYourUsername')}}!
					@endif</a>
</div>

<div class="clearfix"></div>
</div>

</div>
@endsection
