@extends('lasvegas/mobile/master')
@section('content')
@section('title', 'ChangePassword')
<script>
	$(document).ready(function() { 
		getBalance(true);
	});
	
function checkPass()
{
    //Store the password field objects into variables ...
    var new_password = document.getElementById('new_password');
    var confirm_new_password = document.getElementById('confirm_new_password');
    //Store the Confimation Message Object ...
    var message = document.getElementById('confirmMessage');
    //Set the colors we will be using ...
    var goodColor = "#66cc66";
    var badColor = "#ff6666";
    //Compare the values in the password field 
    //and the confirmation field
    if(new_password.value == confirm_new_password.value){
        //The passwords match. 
        //Set the color to the good color and inform
        //the user that they have entered the correct password 
        confirm_new_password.style.backgroundColor = goodColor;
        message.style.color = goodColor;
        message.innerHTML = "Passwords Match!"
    }else{
        //The passwords do not match.
        //Set the color to the bad color and
        //notify the user.
        confirm_new_password.style.backgroundColor = badColor;
        message.style.color = badColor;
        message.innerHTML = "Passwords Do Not Match!"
    }
} 

 function update_password(){
	$.ajax({
		type: "POST",
		url: "{{action('User\MemberController@ChangePassword')}}",
		data: {
			_token: "{{ csrf_token() }}",
			current_password:		$('#current_password').val(),
			new_password:    		$('#new_password').val(),
			confirm_new_password:   $('#confirm_new_password').val(),
			mobile_vcode:		    $('#r_mobile_vcode').val(),
			mobile:                 $('#r_mobile').val()
		},
	}).done(function( json ) {
		
		obj = JSON.parse(json);
			 var str = '';
			 $.each(obj, function(i, item) {
				str += item + '\n';
			})
			if(str=="成功\n"||str=="Successful\n"){
				alert(str);
				 window.location = "{{route('homepage')}}";
				//window.location = "{{route('update-profile')}}"; if dont have acc detail //use machine register
			}else{
				alert(str);
			}	
	});
}

	var countDownSecond = 0;
	var intervalObj = null;
    var btnObj = $("#btn_sms");

function sendVerifySms() {
        if (intervalObj != null) {
            alert("{{ Lang::get('public.PleaseWaitForXSecondsToResendSMS') }}".replace(":p1", countDownSecond));
            return false;
        }

	    var mobileObj = $("#r_mobile");
	    var mobileNum = mobileObj.val();

	    if (mobileNum.length < 1) {
	        alert("{{ Lang::get('public.PleaseEnterContactNumberToContinue') }}");
            mobileObj.focus();

	        return false;
        }

	    if (confirm("{{ Lang::get('public.SendVerificationCodeViaSMSToThisNumber') }} " + mobileNum)) {
            countDownSecond = 60;
            smsResendCountDown();

            $.ajax({
                type: "POST",
                dataType: "json",
                url: "{{url('smschangepasscode')}}",
                data: {
                    _token:     "{{ csrf_token() }}",
                    tel_mobile:	mobileNum
                },
                success: function (result) {
                    if (result.code != 0) {
                        alert(result.msg);
                    }

                    if (result.code == 2) {
                        countDownSecond = 10;
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
//                    console.log('ok failed');
                }
            });
        }
    }

    function smsResendCountDown() {
        if (intervalObj == null) {
            intervalObj = setInterval(smsResendCountDown, 1000)
        }

        if (countDownSecond <= 1) {
            btnObj.text("Send SMS");
            clearInterval(intervalObj);
            intervalObj = null;
            return;
        }

        
    }
	function getQueryStringValue (key) {  
	  return decodeURIComponent(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + encodeURIComponent(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));  
	} 
</script>
<style>
.button {
    background-color: #4CAF50; /* Green */
    border: none;
    color: white;
    padding: 8px 18px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    margin: 7px 2px;
    -webkit-transition-duration: 0.4s; /* Safari */
    transition-duration: 0.2s;
    cursor: pointer;
	text-decoration: none;
}

.button1 {
    background-color: green; 
    color: black; 
    border: 2px solid #4CAF50;
}

.button1:hover {
    background-color: #4CAF50;
    color: white;
	text-decoration: none;
}
</style>
<div class="bgB">
<div class="row nopad marginTop01">
<div class="col-md-12 col-xs-12 text-center clr2 hg bg-black">
<span class="cornMid"><strong>{{Lang::get('public.ChangePassword')}}</strong></span>
</div>
</div>

<div class="row pad01">

<div class="col-md-6 col-xs-6">
<div class="col-md-12 col-xs-12  padtp1">
{{Lang::get('public.CurrentPassword')}}
</div>
</div>
<div class="col-md-6 col-xs-6 wallI">
<input type="password" id="current_password">
</div>

</div>

<div class="row pad01">

<div class="col-md-6 col-xs-6">
<div class="col-md-12 col-xs-12  padtp1">
{{Lang::get('public.NewPassword')}}
</div>
</div>
<div class="col-md-6 col-xs-6 wallI">
<input type="password" id="new_password">
</div>
<div class="col-md-12 col-xs-12 text-danger padTB02">
<span>{{Lang::get('public.NewPasswordNote')}}</span>
</div>
</div>

<div class="row pad01">

<div class="col-md-6 col-xs-6">
<div class="col-md-12 col-xs-12 padtp1">
{{Lang::get('public.ConfirmNewPassword')}}
</div>
</div>
<div class="col-md-6 col-xs-6 wallI">
<input type="password" id="confirm_new_password">
</div>

</div>

<div class="row pad01">

<div class="col-md-6 col-xs-6">
<div class="col-md-12 col-xs-12 padtp1">
{{Lang::get('public.MobileNumber')}}
</div>
</div>
<div class="col-md-6 col-xs-6 wallI">
<input type="text" id="r_mobile" value="{{$acdObj->telmobile}}">
</div>

</div>

<div class="row pad01">

<div class="col-md-6 col-xs-6">
<div class="col-md-12 col-xs-12 padtp1">
{{ Lang::get('public.VerificationCode') }} * :
</div>
</div>
<div class="col-md-6 col-xs-6 wallI">
<input type="text" id="r_mobile_vcode" class="form-control">
</div>
<div class="">
<a href="javascript:void(0);" id="btn_sms" class="button button1" onclick="sendVerifySms();">{{ Lang::get('public.ClickHereToGetVerificationCode') }}</a>
		<br><span class="mobile_vcode_acctTextReg acctTextReg"></span> 
</div>
</div>

<div class="row pad01 padB">

<div class="col-md-12 col-xs-12">
<div class="submitBtn pull-right">
<a href="#" onclick="update_password()">{{Lang::get('public.Submit')}}</a>
</div>
</div>

</div>
</div>
@endsection