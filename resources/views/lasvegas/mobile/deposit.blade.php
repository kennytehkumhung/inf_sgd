@extends('lasvegas/mobile/master')
@section('content')
@section('title', 'Deposit')
<script>
/* $(function() {
	$( ".datepicker" ).datepicker({ dateFormat: 'yy-mm-dd'  , defaultDate: new Date() });
}); */

function cashcardPOST(){
        $.ajax({
            type: "POST",
            url: "{{action('User\CashcardController@depositProcess')}}?" + $("form").serialize(),
            data: {
                _token: "{{ csrf_token() }}"
            },
        }).done(function( json ) {
			 obj = JSON.parse(json);
			 var str = '';
			 $.each(obj, function(i, item) {
				if( '{{Lang::get('COMMON.SUCESSFUL')}}' == item ){
					alert('{{Lang::get('COMMON.SUCESSFUL')}}');
					window.location.href = "{{route('transaction')}}";
				}else{
					alert(item);
				}
			})
	
	});
}

setInterval(update_mainwallet, 10000);
  function deposit_submit(){

	var file_data = $("#receipt").prop("files")[0];  
	var form_data = new FormData();                  
	form_data.append("file", file_data);
	
	var data = $('#deposit_form').serializeArray();
	var obj = {};
	for (var i = 0, l = data.length; i < l; i++) {
		form_data.append(data[i].name, data[i].value);
	}
	
	form_data.append('_token', '{{csrf_token()}}');

	$.ajax({
		type: "POST",
		url: "{{route('deposit-process')}}?",
		        dataType: 'script',
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,                         // Setting the data attribute of ajax with file_data
                type: 'post',
				beforeSend: function(){
					$('#deposit_sbumit_btn').attr('onclick','');
				},
				complete: function(json){
					
					obj = JSON.parse(json.responseText);
					 var str = '';
					 $.each(obj, function(i, item) {
						
						if( '{{Lang::get('COMMON.SUCESSFUL')}}' == item ){
							alert('{{Lang::get('COMMON.DEPOSITSUCESSFULREQUEST')}}');
							window.location.href = "{{route('transaction')}}";
						}else{
							//$('.'+i+'_acctTextReg').html(item);
							$('#deposit_sbumit_btn').attr('onclick','deposit_submit()');
							str += item + '<br>';
						}
					})
					$('.failed_message').html(str);
				
				},
					
	});
	//alert('asd');
}
function cashcard() {
    var x = document.getElementById('cashcard_form');
    var x1 = document.getElementById('deposit_form');
	var x2 = document.getElementById('cashcard_note');
    var x3 = document.getElementById('deposit_note');
	document.getElementById("cashcard").style.backgroundColor = "#FDE35B";
	document.getElementById("depositbank").style.backgroundColor = "";
    if (x.style.display === 'none') {
    	x.style.display = 'block';
        x1.style.display = 'none';
		x2.style.display = 'block';
        x3.style.display = 'none';
    } else {
        x.style.display = 'block';
        x1.style.display = 'none';
		x2.style.display = 'block';
        x3.style.display = 'none';
    }
}
function depositbank() {
    var x = document.getElementById('cashcard_form');
    var x1 = document.getElementById('deposit_form');
	var x2 = document.getElementById('cashcard_note');
    var x3 = document.getElementById('deposit_note');
	document.getElementById("cashcard").style.backgroundColor = "";
	document.getElementById("depositbank").style.backgroundColor = "#FDE35B";
    if (x.style.display === 'block') {
    	x.style.display = 'none';
        x1.style.display = 'block';
		x2.style.display = 'none';
        x3.style.display = 'block';
    } else {
        x.style.display = 'none';
        x1.style.display = 'block';
		x2.style.display = 'none';
        x3.style.display = 'block';
    }
}
  function generateFullNumber()
  {
    document.getElementById('fullNumber').value =
    	document.getElementById('number1').value + 
      	document.getElementById('number2').value + 
        document.getElementById('number3').value + 
        document.getElementById('number4').value + 
        document.getElementById('number5').value ;
  }
</script>
 
 <div class="row nopad marginTop01">
		<div class="col-md-12 col-xs-12 text-center clr2 hg bg-black">
			<span class="cornMid"><strong style="color:white">{{Lang::get('public.MyWallet')}}</strong></span>
		</div>
	</div>
@include('lasvegas/mobile/trans_top' )
 
	<div class="row mt03">
		<div class="col-md-12 col-xs-12">
			<h4 class="text-yellow"><i class="fa fa-university mr01" aria-hidden="true"></i>{{Lang::get('public.DepositMethod')}} </h4>
		</div>
		<div class="col-md-5 col-xs-12">
			<div class="btn-group" role="group">
				<a href="javascript:void(0)" class="btn btn-default" onclick="cashcard()" id="cashcard" style="background-color:#FDE35B;">{{Lang::get('public.CashCard')}}</a>
					<a href="javascript:void(0)" class="btn btn-default" onclick="depositbank()" id="depositbank">{{Lang::get('public.DepositBank')}}</a>
			</div>
		</div>
	</div>

<form id="cashcard_form">
<input type="hidden" name="fullname" value="{{ $fullname }}">
	<div class="row nopad marginTop01">
		<div class="col-md-12 col-xs-12 text-center clr2 hg bg-black">
			<span class="cornMid"><strong>{{Lang::get('public.CashCard')}} </strong></span>
		</div>
	</div>

	<div class="row pad01">

	<div class="col-md-6 col-xs-6">
	<div class="col-md-12 col-xs-12 padtp1">
	{{Lang::get('public.SerialNo')}}.* :
	</div>
	</div>
	<div class="col-md-6 col-xs-6 wallI">
	<input type="text" name="serial" placeholder="{{Lang::get('public.Example')}} 1234567890">
	</div>

	</div>
	
	<div class="row pad01">

	<div class="col-md-6 col-xs-6">
	<div class="col-md-12 col-xs-12 padtp1">
	{{Lang::get('public.SecurityCodeNo')}}.* :
	</div>
	</div>
	<div class="col-md-6 col-xs-6 wallI">
	<input type="text" maxlength="4" id="number1" name="serial" onkeyup="generateFullNumber()" placeholder="{{Lang::get('public.Code')}} 1">
	</div>

	</div>
	
	<div class="row pad01">

	<div class="col-md-6 col-xs-6">
	<div class="col-md-12 col-xs-12 padtp1">
	</div>
	</div>
	<div class="col-md-6 col-xs-6 wallI">
	<input type="text" maxlength="4" id="number2" name="serial" onkeyup="generateFullNumber()" placeholder="{{Lang::get('public.Code')}} 2">
	</div>

	</div>
	
	<div class="row pad01">

	<div class="col-md-6 col-xs-6">
	<div class="col-md-12 col-xs-12 padtp1">
	</div>
	</div>
	<div class="col-md-6 col-xs-6 wallI">
	<input type="text" maxlength="4" id="number3" name="serial" onkeyup="generateFullNumber()" placeholder="{{Lang::get('public.Code')}} 3">
	</div>

	</div>
	
	<div class="row pad01">

	<div class="col-md-6 col-xs-6">
	<div class="col-md-12 col-xs-12 padtp1">
	</div>
	</div>
	<div class="col-md-6 col-xs-6 wallI">
	<input type="text" maxlength="4" id="number4" name="serial" onkeyup="generateFullNumber()" placeholder="{{Lang::get('public.Code')}} 4">
	</div>

	</div>
	
	<div class="row pad01">

	<div class="col-md-6 col-xs-6">
	<div class="col-md-12 col-xs-12 padtp1">
	</div>
	</div>
	<div class="col-md-6 col-xs-6 wallI">
	<input type="text" maxlength="4" id="number5" name="serial" onkeyup="generateFullNumber()" placeholder="{{Lang::get('public.Code')}} 5">
	</div>

	</div>
	
	<div class="row nopad marginTop01">
		<div class="col-md-12 col-xs-12 text-center clr2 hg bg-black">
			<span class="cornMid"><strong>{{Lang::get('public.Deposit')}} {{Lang::get('public.Promotion')}}* :</strong></span>
		</div>
	</div>

<div class="row pad01">
<div class="col-lg-12">
		<table>
       @foreach( $promos as $key => $promo )
        <div class="col-xs-1">
        <input name="promo" type="radio" value=""/>
        </div>
        <div class="col-xs-10 cut1">
        <label> {{$promo['code']}}</label>
        </div>
        
        <div class="col-md-12">
        <div class="promoWrap">
        <img class="object-fit_cover" src="{{$promo['image']}}">
        </div>
        </div>
       @endforeach
        <div class="col-xs-1">
        <input type="radio" name="promo" value="0"/>
        </div>
        <div class="col-xs-11 cut1">
        <label>{{Lang::get('public.IDoNotWantPromotion')}}</label>
        </div>
        </table>

        <div class="clearfix"></div>
             <div class="row abz">
        <div class="col-xs-1">
        <input type="radio" name="rules"/>
        </div>
        <div class="col-xs-10 cut1">
        <label>{{Lang::get('public.IAlreadyUnderstandRules')}}</label>
		<span class="failed_message acctTextReg" style="display:block;float:left;height:100%;"></span>
        </div>
        
        </div>
        
        </div>
        
        </div>
		
		<div class="row pad01">
		<div class="col-md-12 col-xs-12">
		<div class="submitBtn pull-right">
		<a id="deposit_sbumit_btn" href="javascript:void(0)" onclick="cashcardPOST()">{{Lang::get('public.Submit')}}</a>
		</div>
		</div>
		</div>
</form>

<form id="deposit_form" style="display:none;">
<input type="hidden" name="fullname" value="{{ $fullname }}">

	<div class="row pad01 bgB padS">

	<div class="row nopad marginTop01">
	<div class="col-md-12 col-xs-12 text-center clr2 hg bg-black">
	<span class="cornMid"><strong>{{Lang::get('public.Deposit')}}</strong></span>
	</div>
	</div>

	<div class="row pad01">

	<div class="col-md-6 col-xs-6">
	<div class="col-md-12 col-xs-12 padtp1">
	{{Lang::get('public.Amount')}}* :
	</div>
	</div>
	<div class="col-md-6 col-xs-6 wallI">
	<input type="text" name="amount" placeholder="XXX">
	</div>

	</div>

	<div class="row pad01">

	<div class="col-md-6 col-xs-6">
	<div class="col-md-12 col-xs-12 padtp1">
	{{ Lang::get('public.BankingOptions') }}* :
	</div>
	</div>
	
	<div class="col-md-6 col-xs-6 wallI">
	</div>
	</div>
	
	<div class="row marginTop01">
	<div class="table-responsive">
	<table cellspacing="0" class="table" id="ctl00_ctl00_ContentPlaceHolder1_ChildContentMain_deposit_table" style="width:98%;border-collapse:collapse; color:#ffffff; font-size: 13px;overflow-x:scroll">
							<thead>
								<tr align="left" style="height:30px;">
									<th scope="col">&nbsp;</th>
									<th scope="col">{{Lang::get('public.Bank')}}</th>
									<th scope="col">{{Lang::get('public.AccountName')}}</th>
									<th scope="col">{{Lang::get('public.AccountNumber')}}</th>
									<th scope="col">{{Lang::get('public.Minimum')}}</th>
									<th scope="col">{{Lang::get('public.Maximum')}}</th>
									<th scope="col">{{Lang::get('public.ProcessingTime')}}</th>
								</tr>
							</thead>
							<tbody>
							@foreach( $banks as $key => $bank )
								<tr>
									<td><input name="bank" class="radiobtn" type="radio" value="{{$bank['bnkid']}}" style="width:10px; height:10px;"></td>
									<td><img src="{{$bank['image']}}" style="width:130px;height:35px;"></td>
									<td>{{$bank['bankaccname']}}</td>		
									<td>{{$bank['bankaccno']}}</td>	
									<td>{{$bank['min']}}</td>	
									<td>{{$bank['max']}}</td>
									<td>5 min</td>
								</tr>  
							@endforeach
							</tbody>
	</table>
	</div>
	</div>

	<div class="row pad01">

	<div class="col-md-6 col-xs-6">
	<div class="col-md-12 col-xs-12 padtp1">
	{{ Lang::get('public.ReferenceNo') }}* :
	</div>
	</div>
	<div class="col-md-6 col-xs-6 wallI">
	<input type="text" name="refno">
	</div>

	</div>

	<div class="row pad01">

	<div class="col-md-6 col-xs-6">
	<div class="col-md-12 col-xs-12 padtp1">
	{{ Lang::get('public.DepositMethod') }}* :
	</div>
	</div>
	<div class="col-md-6 col-xs-6 wallI">
	<select id="type" name="type">
		<option value="0">{{ Lang::get('public.OverCounter') }}</option>
        <option value="1">{{ Lang::get('public.InternetBanking') }}</option>
        <option value="2">{{ Lang::get('public.ATMBanking') }}</option>
    </select>
	</div>

	</div>

	<div class="row pad01">

	<div class="col-md-6 col-xs-6">
	<div class="col-md-12 col-xs-12 padtp1">
	{{ Lang::get('public.DateTime') }}* :
	</div>
	</div>
	<div class="col-md-6 col-xs-6">
	<div class="col-md-12 col-xs-12 padtp1">
	<select name="hours">
							<option value="01">01</option>
							<option value="02">02</option>
							<option value="03">03</option>
							<option value="04">04</option>
							<option value="05">05</option>
							<option value="06">06</option>
							<option value="07">07</option>
							<option value="08">08</option>
							<option value="09">09</option>
							<option value="10">10</option>
							<option value="11">11</option>
							<option value="12">12</option>
						 </select>
	</div>
	</div>
	</div>
	
	<div class="row pad01">
	<div class="col-md-6 col-xs-6">
	<div class="col-md-12 col-xs-12 padtp1">
	</div>
	</div>
	
	<div class="col-md-6 col-xs-6">
	<div class="col-md-12 col-xs-12 padtp1">
	<select name="minutes">
							    <option value="00">00</option>
								<option value="01">01</option>
								<option value="02">02</option>
								<option value="03">03</option>
								<option value="04">04</option>
								<option value="05">05</option>
								<option value="06">06</option>
								<option value="07">07</option>
								<option value="08">08</option>
								<option value="09">09</option>
								<option value="10">10</option>
								<option value="11">11</option>
								<option value="12">12</option>
								<option value="13">13</option>
								<option value="14">14</option>
								<option value="15">15</option>
								<option value="16">16</option>
								<option value="17">17</option>
								<option value="18">18</option>
								<option value="19">19</option>
								<option value="20">20</option>
								<option value="21">21</option>
								<option value="22">22</option>
								<option value="23">23</option>
								<option value="24">24</option>
								<option value="25">25</option>
								<option value="26">26</option>
								<option value="27">27</option>
								<option value="28">28</option>
								<option value="29">29</option>
								<option value="30">30</option>
								<option value="31">31</option>
								<option value="32">32</option>
								<option value="33">33</option>
								<option value="34">34</option>
								<option value="35">35</option>
								<option value="36">36</option>
								<option value="37">37</option>
								<option value="38">38</option>
								<option value="39">39</option>
								<option value="40">40</option>
								<option value="41">41</option>
								<option value="42">42</option>
								<option value="43">43</option>
								<option value="44">44</option>
								<option value="45">45</option>
								<option value="46">46</option>
								<option value="47">47</option>
								<option value="48">48</option>
								<option value="49">49</option>
								<option value="50">50</option>
								<option value="51">51</option>
								<option value="52">52</option>
								<option value="53">53</option>
								<option value="54">54</option>
								<option value="55">55</option>
								<option value="56">56</option>
								<option value="57">57</option>
								<option value="58">58</option>
								<option value="59">59</option>
							</select>
	</div>
	</div>
	</div>
	
	<div class="row pad01">
	<div class="col-md-6 col-xs-6">
	<div class="col-md-12 col-xs-12 padtp1">
	</div>
	</div>
	
	<div class="col-md-6 col-xs-6">
	<div class="col-md-12 col-xs-12 padtp1">
	<select name="range">
								<option value="AM">AM</option>
								<option value="PM">PM</option>
							 </select>
	</div>
	</div>
	</div>
	
	<div class="row pad01">
	<div class="col-md-6 col-xs-6">
	<div class="col-md-12 col-xs-12 padtp1">
	</div>
	</div>
	
	<div class="col-md-6 col-xs-6">
	<div class="col-md-12 col-xs-12 padtp1">
	<input type="text" id="deposit_date" class="datepicker" style="cursor:pointer;" name="date" value="{{date('Y-m-d')}}">
	</div>
	</div>
	</div>
	
	<div class="row pad01">

	<div class="col-md-6 col-xs-6">
	<div class="col-md-12 col-xs-12 padtp1">
	{{Lang::get('public.DepositReceipt')}}* :
	</div>
	</div>
	<div class="col-md-6 col-xs-6 wallI">
	<input class="upload" type="file" name="receipt" id="receipt" style="color:#fff;">
	</div>

	</div>





	<div class="row nopad marginTop01">
	<div class="col-md-12 col-xs-12 text-center clr2 hg bg-black">
	<span class="cornMid"><strong>{{Lang::get('public.Deposit')}} {{Lang::get('public.Promotion')}}* :</strong></span>
	</div>
	</div>

	<div class="row pad01">
	<div class="col-lg-12">
		<table>
       @foreach( $promos as $key => $promo )
        <div class="col-xs-1">
        <input name="promo" type="radio" value=""/>
        </div>
        <div class="col-xs-10 cut1">
        <label> {{$promo['code']}}</label>
        </div>
        
        <div class="col-md-12">
        <div class="promoWrap">
        <img class="object-fit_cover" src="{{$promo['image']}}">
        </div>
        </div>
       @endforeach
        <div class="col-xs-1">
        <input type="radio" name="promo" value="0"/>
        </div>
        <div class="col-xs-11 cut1">
        <label>{{Lang::get('public.IDoNotWantPromotion')}}</label>
        </div>
        </table>

        <div class="clearfix"></div>
             <div class="row abz">
        <div class="col-xs-1">
        <input type="radio" name="rules"/>
        </div>
        <div class="col-xs-10 cut1">
        <label>{{Lang::get('public.IAlreadyUnderstandRules')}}</label>
		<span class="failed_message acctTextReg" style="display:block;float:left;height:100%;"></span>
        </div>
        
        </div>
        
        </div>
        
        </div>
			
		<div class="row pad01">
		<div class="col-md-12 col-xs-12">
		<div class="submitBtn pull-right">
		<a id="deposit_sbumit_btn" href="javascript:void(0)" onclick="cashcardPOST()">{{Lang::get('public.Submit')}}</a>
		</div>
		</div>
		</div>

		<!-- /footer -->

	</div>

	</div>
</form>

<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script>
    jQuery(document).ready(function($) {
        $('.datepicker').datepicker({
            dateFormat: "yy-mm-dd"
        });
    });
</script>
@endsection