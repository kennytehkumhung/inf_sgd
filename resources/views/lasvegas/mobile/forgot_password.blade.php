@extends('lasvegas/mobile/master')
@section('content')
@section('title', 'Home')
<script>
	
	
	function enterpressalert(e, textarea)
	{
		var code = (e.keyCode ? e.keyCode : e.which);
        if(code == 13) { //Enter keycode
            register_submit();
        }
    }

 /*  function register_submit(){
	  
	$.ajax({
		type: "POST",
		url: "{{route('register_process')}}",
		data: {
			_token: "{{ csrf_token() }}",
			username:		 $('#r_username').val(),
			password: 		 $('#r_password').val(),
			repeatpassword:  $('#r_repeatpassword').val(),
			code:    		 $('#r_code').val(),
			email:    		 $('#r_email').val(),
			mobile:    		 $('#r_mobile').val(),
			fullname:        $('#r_fullname').val(),
			dob:			 $('#dob').val(),
			wechat:			 $('#r_wechat').val(),
			line:			 $('#r_line').val(),
            referralid:		 $('#r_referralid').val(),
            mobile_vcode:	 $("#r_mobile_vcode").val()
		},
	}).done(function( json ) {
			 $('.acctTextReg').html('');
			 obj = JSON.parse(json);
			 var str = '';
			 $.each(obj, function(i, item) {
				if( '{{Lang::get('COMMON.SUCESSFUL')}}' == item ){
					alert('{{Lang::get('public.NewMember')}}');
					window.location.href = "{{route('homepage')}}";
				}else{
					//$('.'+i+'_acctTextReg').html(item);
					//str += item + '\n';
					$('.' + i + '_acctTextReg').html('<font style="color:red">' + item + '</font>');
				}
			});
			
			//alert(str);
			
			
	});
} */

  var countDownSecond = 0;
  var intervalObj = null;
  var btnObj = $("#btn_sms");

  function sendVerifySms() {
      /* if (intervalObj != null) {
          alert("{{ Lang::get('public.PleaseWaitForXSecondsToResendSMS') }}".replace(":p1", countDownSecond));
          return false;
      } */

      var mobileObj = $("#r_mobile");
      var mobileNum = mobileObj.val();
	  
	  
      if (mobileNum.length < 1) {
          alert("{{ Lang::get('public.PleaseEnterContactNumberToContinue') }}");
          mobileObj.focus();

          return false;
      }

	  
      if (confirm("{{ Lang::get('public.SendVerificationCodeViaSMSToThisNumber') }} " + mobileNum)) {
          countDownSecond = 60;		  
          smsResendCountDown();

          $.ajax({
              type: "POST",
			  dataType: "json",
              url: "{{url('smsresetpassword')}}",
              data: {
                  _token:     "{{ csrf_token() }}",
                  tel_mobile:	mobileNum,
                  username:	$('#fg_username').val(),
              },
              success: function (result) {
				  window.location.href = "{{route('homepage')}}";
                  if (result.code != 0) {
                      alert(result.msg);
				  }

				  if (result.code == 2) {
                      countDownSecond = 10;
				  }
              },
              error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert('Invalid username or phone number.');//console.log('Invalid username or phone number.');
              }
          });
      }
  }

  function smsResendCountDown() {
      if (intervalObj == null) {
          intervalObj = setInterval(smsResendCountDown, 1000)
      }

      if (countDownSecond <= 1) {
          btnObj.text("Send SMS");
          clearInterval(intervalObj);
          intervalObj = null;
          return;
      }

      
  }
  function getQueryStringValue (key) {  
	  return decodeURIComponent(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + encodeURIComponent(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));  
	} 
</script>

<div class="row pad01">
<div class="col-md-12 col-xs-12 text-center clr2 hg">
<span class="cornMid"><strong>{{ Lang::get('public.ForgotPassword') }}</strong></span>
</div>
</div>

<div class="row pad01">
<div class="row">
<div class="col-md-12 col-xs-12 text-black lblmar">
<label>{{Lang::get('COMMON.USERNAME')}} :</label>
</div>
<div class="col-md-12 col-xs-12 text-black wallI">
<input id="fg_username" type="text" class="acctTextReg">
</div>
</div>


<div class="row padTB01">
<div class="col-md-12 col-xs-12 text-black lblmar">
<label>{{Lang::get('public.MobilePhone')}} :</label>
</div>
<div class="col-md-12 col-xs-12 text-black wallI">
<input id="r_mobile"  type="text" class="acctTextReg">
</div>
</div>
</div>

<div class="row pad01 padS">
<div class="col-md-4 col-xs-4 pad01">
<div class="submitBtn">
<a id="btn_sms" href="javascript:void(0)" onClick="sendVerifySms()" >{{Lang::get('public.Submit')}}</a>
</div>
</div>
</div>
@endsection