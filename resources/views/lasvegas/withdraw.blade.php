@extends('lasvegas/master')
@section('title', 'Withdrawal')
@section('content')
<script type="text/javascript">
	$(document).ready(function(){
			branchData();
    });
	setInterval(update_mainwallet, 10000);
	function submit_transfer(){
	$.ajax({
			 type: "POST",
			 url: "{{route('withdraw-process')}}?"+$('#withdraw_form').serialize(),
			 data: {
				_token: 		 "{{ csrf_token() }}",
			 },
			 beforeSend: function(){
				$('#withdraw_btn_submit').attr('onClick','');
				$('#withdraw_btn_submit').html('Loading...');
			 },
			 success: function(json){
					obj = JSON.parse(json);
					 var str = '';
					 $.each(obj, function(i, item) {
						
						if( '{{Lang::get('COMMON.SUCESSFUL')}}' == item ){
							alert('{{Lang::get('COMMON.WITHDRAWALSUCESSFULREQUEST')}}');
							window.location.href = "{{route('transaction')}}";
						}else{
							str += item + '<br>';
						}
					})
					$('.failed_message').html(str);
					$('#withdraw_btn_submit').attr('onClick','submit_transfer()');
					$('#withdraw_btn_submit').html('{{Lang::get('public.Submit')}}');
				
			}
		})
	}
function withdrawalcard() {
    var x = document.getElementById('withdrawcard_form');
    var x1 = document.getElementById('withdraw_form');
	var x2 = document.getElementById('withdrawcard_note');
    var x3 = document.getElementById('withdraw_note');
	document.getElementById("withdrawalcard").style.backgroundColor = "#FDE35B";
	document.getElementById("withdrawalbank").style.backgroundColor = "";
    if (x.style.display === 'none') {
    	x.style.display = 'block';
        x1.style.display = 'none';
		x2.style.display = 'block';
        x3.style.display = 'none';
    } else {
        x.style.display = 'block';
        x1.style.display = 'none';
		x2.style.display = 'block';
        x3.style.display = 'none';
    }
}
function withdrawalbank() {
    var x = document.getElementById('withdrawcard_form');
    var x1 = document.getElementById('withdraw_form');
	var x2 = document.getElementById('withdrawcard_note');
    var x3 = document.getElementById('withdraw_note');
	document.getElementById("withdrawalcard").style.backgroundColor = "";
	document.getElementById("withdrawalbank").style.backgroundColor = "#FDE35B";
    if (x.style.display === 'block') {
    	x.style.display = 'none';
        x1.style.display = 'block';
		x2.style.display = 'none';
        x3.style.display = 'block';
    } else {
        x.style.display = 'none';
        x1.style.display = 'block';
		x2.style.display = 'none';
        x3.style.display = 'block';
    }
}
	function branchData(){
        $.ajax({
            type: "GET",
            url: "{{route('getAllBranch')}}",
            data: {
                _token: "{{ csrf_token() }}"
            },
        }).done(function (data) {
			var optionData = '';
			var obj= data;
			obj = JSON.parse(obj);
			$.each(obj, function(i, item) {
				//alert(i+" "+item);
				optionData+="<option value="+i+">"+item+"</option>";
				$("#getOptionData").html(optionData);
			})
		});
	}	
</script>
    <!-- ======== @Region: #content ======== -->
        <div id="content" class="mt02 noBg">
      <div class="container">
      <div class="row">
            <div class="col-md-12 col-xs-12">
            @include('lasvegas/trans_top' )
            </div>
            </div>
            
            <div class="row">
              <div class="col-md-2 col-xs-12">
                <div class="men1">
                <a href="{{route('deposit')}}">{{Lang::get('public.Deposit')}}</a>
                </div>
              </div>
              <div class="col-md-2 col-xs-12">
                <div class="men1">
                <a class="selected" href="{{route('withdraw')}}">{{Lang::get('public.Withdrawal')}}</a>
                </div>
              </div>
              <div class="col-md-2 col-xs-12">
                <div class="men1">
                <a href="{{route('transaction')}}">{{Lang::get('public.TransactionHistory')}}</a>
                </div>
              </div>
              <div class="col-md-2 col-xs-12">
                <div class="men1">
                <a href="{{route('transfer')}}">{{Lang::get('public.Transfer')}}</a>
                </div>
              </div>
            </div>
            
            <div class="row">
               <div class="col-md-12 col-xs-12">
               <div class="cont">
              
               
               <div class="row mt3">
			   <div class="col-md-3 col-xs-12 text-white fs12 bdRight">
                 <div class="rule1"></div>
                 <p><?php echo htmlspecialchars_decode($content); ?></p>
                 </div>
                <div class="col-md-9 col-xs-12">
				 <div class="row mt03">
				 
					<div class="col-md-12 col-xs-12">
						<h4 class="text-yellow"><i class="fa fa-university mr01" aria-hidden="true"></i>{{Lang::get('public.WithdrawalMethod')}} </h4>
					</div>
					<div class="col-md-5 col-xs-12">
						<div class="btn-group" role="group">
							<a href="javascript:void(0)" class="btn btn-default" onclick="withdrawalcard()" id="withdrawalcard" style="background-color:#FDE35B">{{Lang::get('public.FromBranch')}}</a>
							<a href="javascript:void(0)" class="btn btn-default" onclick="withdrawalbank()" id="withdrawalbank">{{Lang::get('public.FromBank')}}</a>
						</div>
					</div>
                 </div>
                <form id="withdrawcard_form"> 
					<div class="row">
                        <div class="col-md-12 col-xs-12">
							<h5 class="text-yellow"><i class="fa fa-university mr01" aria-hidden="true"></i>{{Lang::get('public.Withdrawal')}}</h5>
                        </div>
                    </div>  
					<div class="row mt03">
                        <div class="col-md-2 col-xs-5">
							<div class="txtWrap">{{Lang::get('public.Branch')}} * :</div>
                        </div>
                        <div class="col-md-3 col-xs-7">
							<div class="form-group">
								<select class="form-control" name="parentid" id="getOptionData">
								</select>
							</div>
                        </div>
                     </div>
					 <div class="row mt03">
                        <div class="col-md-2 col-xs-5">
							<div class="txtWrap">{{Lang::get('public.Amount')}} ({{ Session::get('user_currency') }}) * :</div>
                        </div>
                        <div class="col-md-3 col-xs-7">
							<div class="form-group">
								<input type="text" class="form-control" name="withdrawcard_amount">
							</div>
                        </div>
                     </div>
					 <div class="col-md-4 col-xs-4">
						  <div class="btnNormal ml03">
								<a href="javascript:void(0)" onClick="withdrawalcardPOST()">{{Lang::get('public.Submit')}}</a>
						  </div>
					 </div>
				</form>
                <form id="withdraw_form" style="display:none;"> 
                     <div class="row">
                        <div class="col-md-12 col-xs-12">
                        <h5 class="text-yellow"><i class="fa fa-university mr01" aria-hidden="true"></i>{{Lang::get('public.WithdrawalBank')}}</h5>
                        </div>
                        </div>                  
                     
                     <div class="row mt03">
                        <div class="col-md-3 col-xs-6">
                        <div class="txtWrap">{{Lang::get('public.Balance')}} ({{ Session::get('user_currency') }}) :</div>
                        </div>
                        <div class="col-md-3 col-xs-6">
                         <div class="txtWrap">{{App\Http\Controllers\User\WalletController::mainwallet()}}</div>
                        </div>
                     </div>
                     
                     <div class="row mt03">
                        <div class="col-md-3 col-xs-12">
                        <div class="txtWrap">{{Lang::get('public.Amount')}} ({{ Session::get('user_currency') }}) * :</div>
                        </div>
                        <div class="col-md-3 col-xs-12">
                        <div class="form-group">
                        <input type="text" class="form-control" name="amount">
                        </div>
                        </div>
                     </div>
                     
                     <div class="row mt03">
                        <div class="col-md-3 col-xs-12">
                        <div class="txtWrap">{{Lang::get('public.BankName')}}* :</div>
                        </div>
                        <div class="col-md-3 col-xs-12">
							<div class="form-group">
								<select class="form-control" name="bank">
									@foreach( $banklists as $bank => $detail  )
										<option value="{{$detail['code']}}">{{$bank}}</option>
									@endforeach
								</select>
						   </div>
						</div>
                       </div>
                       
                       <div class="row mt03">
                        <div class="col-md-3 col-xs-12">
                        <div class="txtWrap">{{Lang::get('public.FullName')}} * :</div>
                        </div>
                        <div class="col-md-3 col-xs-12">
                         <div class="txtWrap">{{Session::get('fullname')}}</div>
                        </div>
                     </div>
                     
                     <div class="row mt03">
                        <div class="col-md-3 col-xs-12">
							<div class="txtWrap">{{Lang::get('public.BankAccountNo')}} :</div>
                        </div>
                        <div class="col-md-3 col-xs-12">
							<div class="form-group">
								<input type="text" class="form-control" name="accountno">
							</div>
                        </div>
                     </div>
					<p style="color:#fff;">{{Lang::get('public.RequiredFields')}}</p>

                     <div class="row">
                  <div class="col-md-4 col-xs-4">
                  <div class="btnNormal ml03">
                    <a id="withdraw_btn_submit" href="javascript:void(0)" onClick="submit_transfer()">{{Lang::get('public.Submit')}}</a>
                  </div>
				  <span class="failed_message acctTextReg" style="display:block;float:left;height:100%;"></span>
                  </div>
                  </div>
                     
                 </div>
				</div>
				</form>
               
               </div>
               </div>
            </div>
          

      </div>
      </div>
<script>
function withdrawalcardPOST(){
        $.ajax({
            type: "POST",
            url: "{{action('User\WithdrawcardController@withdrawcardProcess')}}?" + $("form").serialize(),
            data: {
                _token: "{{ csrf_token() }}"
            },
        }).done(function( json ) {
			 obj = JSON.parse(json);
			 var str = '';
			 $.each(obj, function(i, item) {
				if( '{{Lang::get('COMMON.SUCESSFUL')}}' == item ){
					alert('{{Lang::get('COMMON.WITHDRAWALCARDSUCESSFULREQUEST')}}');
					window.location.href = "{{route('transaction')}}";
				}else{
					str += item + '<br>';
				}
				$('.failed_message').html(str);
			})
	
	});
}
</script>
@stop