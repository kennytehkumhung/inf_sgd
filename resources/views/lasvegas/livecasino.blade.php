@extends('lasvegas/master')
@section('title','Live Casino')
@section('content')
<!-- ======== @Region: #content ======== -->
    <div id="content noBg">
      <div class="mission block-pd-sm block-bg-noise bg-lc">
        <div class="container">
        <div class="row">
        <div class="col-md-12 col-xs-12 text-center">
           <img src="{{url()}}/lasvegas/img/lc-bg.png">
        </div>
        </div>
          <div class="row mb01">
          <div class="col-md-3 col-sm-3 col-xs-6">
		  
		  <a href="#" onclick="@if (Auth::user()->check())window.open('{{route('vgs', [ 'type' => 'casino'])}}','','width=720px,height=720px,top=0,left=0');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif"><img class="img-responsive" src="{{url()}}/lasvegas/img/lc-evo.png"></a>
          </div>
          <div class="col-md-3 col-sm-3 col-xs-6">
			 <a href="#" onclick="@if (Auth::user()->check()) window.location.href='{{route('mxb', [ 'type' => 'casino','category' => 'baccarat' ])}}' @else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif"><img class="img-responsive" src="{{url()}}/lasvegas/img/lc-mxb.png"></a>
          </div>
          <div class="col-md-3 col-sm-3 col-xs-6">
		  <a href="javascript:void(0);" onclick="@if (Auth::user()->check())window.open('{{route('pltiframe')}}','','width=1000,height=750')@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif"><img class="img-responsive" src="{{url()}}/lasvegas/img/lc-pt.png"></a>
          </div>
          <div class="col-md-3 col-sm-3 col-xs-6">
		  <a href="javascript:void(0);" onclick="@if (Auth::user()->check())window.open('{{route('hog')}}','','width=1000,height=750')@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif"><img class="img-responsive" src="{{url()}}/lasvegas/img/lc-ho.png"></a>
          </div>
          </div>
        </div>
      </div>

      
    </div>

@stop