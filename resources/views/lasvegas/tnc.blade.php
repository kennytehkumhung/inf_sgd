@extends('lasvegas/master')
@section('content')
@section('title', 'Terms And Conditions')
    <!-- ======== @Region: #content ======== -->
    <div id="content noBg">
      <div class="mission block-pd-sm block-bg-noise">
        <div class="container">
        <div class="row">
        <div class="col-md-12 col-xs-12 text-center specialTxt">
        {{Lang::get('public.TNC')}}
        </div>
        </div>
        <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12 text-white">
        <p>
       {!! htmlspecialchars_decode($content) !!}
      <br />
      <div class="ct">
      <ul>
      </ul>
      </div>
      </p>
      </div>
        </div>
        
        
          
        </div>
      </div>

      
    </div>
@stop