@extends('lasvegas/master')
@section('title','SportsBook')
@section('content')

 <!-- ======== @Region: #content ======== -->

    <div id="content noBg">
      <div class="mission block-pd-sm block-bg-noise">
        <div class="container">
        <div class="row ab3">
        <div class="col-md-12 col-sm-12 col-xs-12 text-center ac1">
        <div class="horseTit"> <a href="{{route('ctb')}}">
		<a href="javascript:void(0);" onclick="@if (Auth::user()->check())window.open('{{route('ctb')}}','','width=1000,height=750')@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif">
		<img src="{{ asset('/lasvegas/img/horse-tit.png') }}"></a></div>
        <img class="img-responsive" src="{{url()}}/lasvegas/img/backgrounds/horse-bg.png">
        </div>
      
        </div> 
        </div>
      </div>

      
    </div>

    
@stop