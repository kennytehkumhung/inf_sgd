<script>
$(document).ready(function() { 
	getBalance(true);
});
</script>
	<div class="scrollOut">
            <div class="scrollBar">
				<div class="scrollItem marAdj01Left">
					<i class="fa fa-chevron-left" aria-hidden="true"></i>
				</div>
				
				<div class="walletWrap2">
					<div class="walletTit" style="color:#fff;">
					  {{Lang::get('public.Mainwallet')}}
					</div>
					<div class="walletDig main_wallet" style="color:#fff;">
					  {{App\Http\Controllers\User\WalletController::mainwallet()}}
					</div>

				</div>
				<?php $count = 2 ; ?>
				@foreach( Session::get('products_obj') as $prdid => $object)
				<div class="walletWrap <?php echo $count++; ?>">
					<div class="walletTit">{{$object->name}}</div>
					<div class="walletDig {{$object->code}}_balance">0.00</div>
				</div>
				@endforeach
				

				<div class="walletWrap2">
					<div class="walletTit" style="color:#fff;">{{Lang::get('public.Total')}}</div>
					<div class="walletDig total_balance" style="color:#fff;">0.00</div>
				</div>
            </div>
            
            <div class="scrollItem marAdj01Right">
				<i class="fa fa-chevron-right" aria-hidden="true"></i>
            </div>
            <div class="clearfix"></div>
    </div>