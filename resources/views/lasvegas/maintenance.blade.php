@extends('lasvegas/master')
@section('title', 'Register')
@section('content') 
    
    <!-- ======== @Region: #content ======== -->
    <div id="content noBg">
      <div class="container">
      <div class="row">
         <div class="col-md-12 col-xs-12 mt04">
         <div class="swrap">
           <img src="{{url()}}/lasvegas/img/under-maintenance.png">
            <div class="row">
            <div class="col-md-12 col-xs-12">
               <div class="bgBub">
               Website currently under construction
               <div class="two">COME BACK SOON!</div>
               </div>
            </div>
            
            </div>
          </div>
         </div>
      </div>
      </div>

      
    </div>

@stop