@extends('lasvegas/master')
@section('title', 'Deposit')
@section('content')
<script>
	setInterval(update_mainwallet, 10000);
  function deposit_submit(){

	var file_data = $("#receipt").prop("files")[0];  
	var form_data = new FormData();                  
	form_data.append("file", file_data);
	
	var data = $('#deposit_form').serializeArray();
	var obj = {};
	for (var i = 0, l = data.length; i < l; i++) {
		form_data.append(data[i].name, data[i].value);
	}
	
	form_data.append('_token', '{{csrf_token()}}');

	$.ajax({
		type: "POST",
		url: "{{route('deposit-process')}}?",
		        dataType: 'script',
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,                         // Setting the data attribute of ajax with file_data
                type: 'post',
				beforeSend: function(){
					$('#deposit_sbumit_btn').attr('onclick','');
				},
				complete: function(json){
					
					obj = JSON.parse(json.responseText);
					 var str = '';
					 $.each(obj, function(i, item) {
						
						if( '{{Lang::get('COMMON.SUCESSFUL')}}' == item ){
							alert('{{Lang::get('COMMON.DEPOSITSUCESSFULREQUEST')}}');
							window.location.href = "{{route('transaction')}}";
						}else{
							//$('.'+i+'_acctTextReg').html(item);
							$('#deposit_sbumit_btn').attr('onclick','deposit_submit()');
							str += item + '<br>';
						}
					})
					$('.failed_message').html(str);
				
				},
					
	});
	//alert('asd');
}
function cashcard() {
    var x = document.getElementById('cashcard_form');
    var x1 = document.getElementById('deposit_form');
	var x2 = document.getElementById('cashcard_note');
    var x3 = document.getElementById('deposit_note');
	document.getElementById("cashcard").style.backgroundColor = "#FDE35B";
	document.getElementById("depositbank").style.backgroundColor = "";
    if (x.style.display === 'none') {
    	x.style.display = 'block';
        x1.style.display = 'none';
		x2.style.display = 'block';
        x3.style.display = 'none';
    } else {
        x.style.display = 'block';
        x1.style.display = 'none';
		x2.style.display = 'block';
        x3.style.display = 'none';
    }
}
function depositbank() {
    var x = document.getElementById('cashcard_form');
    var x1 = document.getElementById('deposit_form');
	var x2 = document.getElementById('cashcard_note');
    var x3 = document.getElementById('deposit_note');
	document.getElementById("cashcard").style.backgroundColor = "";
	document.getElementById("depositbank").style.backgroundColor = "#FDE35B";
    if (x.style.display === 'block') {
    	x.style.display = 'none';
        x1.style.display = 'block';
		x2.style.display = 'none';
        x3.style.display = 'block';
    } else {
        x.style.display = 'none';
        x1.style.display = 'block';
		x2.style.display = 'none';
        x3.style.display = 'block';
    }
}
  function generateFullNumber()
  {
    document.getElementById('fullNumber').value =
    	document.getElementById('number1').value + 
      	document.getElementById('number2').value + 
        document.getElementById('number3').value + 
        document.getElementById('number4').value + 
        document.getElementById('number5').value ;
  }
  </script>
<!-- ======== @Region: #content ======== -->
        <div id="content" class="mt02 noBg">
      <div class="container">
      <div class="row">
      <div class="col-md-12 col-xs-12">
            @include('lasvegas/trans_top' )
            </div>
            </div>
            
            <div class="row">
              <div class="col-md-2 col-xs-12">
                <div class="men1">
                <a class="selected" href="{{route('deposit')}}">{{Lang::get('public.Deposit')}}</a>
                </div>
              </div>
              <div class="col-md-2 col-xs-12">
                <div class="men1">
                <a href="{{route('withdraw')}}">{{Lang::get('public.Withdrawal')}}</a>
                </div>
              </div>
              <div class="col-md-2 col-xs-12">
                <div class="men1">
                <a href="{{route('transaction')}}">{{Lang::get('public.TransactionHistory')}}</a>
                </div>
              </div>
              <div class="col-md-2 col-xs-12">
                <div class="men1">
                <a href="{{route('transfer')}}">{{Lang::get('public.Transfer')}}</a>
                </div>
              </div>
            </div>
			
			<div class="row">
               <div class="col-md-12 col-xs-12">
               <div class="cont">
              
               
               <div class="row mt3">
                 <div class="col-md-3 col-xs-12 text-white fs12 bdRight">
                 <div class="rule1"></div>
                 <p><?php echo htmlspecialchars_decode($content); ?></p>
                 </div>
                 <div class="col-md-9 col-xs-12">
				 <div class="row mt03">
                        <div class="col-md-12 col-xs-12">
                        <h4 class="text-yellow"><i class="fa fa-university mr01" aria-hidden="true"></i>{{Lang::get('public.DepositMethod')}} </h4>
                        </div>
                        <div class="col-md-5 col-xs-12">
							<div class="btn-group" role="group">
								<a href="javascript:void(0)" class="btn btn-default" onclick="cashcard()" id="cashcard" style="background-color:#FDE35B;">{{Lang::get('public.CashCard')}}</a>
								<a href="javascript:void(0)" class="btn btn-default" onclick="depositbank()" id="depositbank">{{Lang::get('public.DepositBank')}}</a>
							</div>
                        </div>
                 </div>
				 <form id="cashcard_form">
					<input type="hidden" name="fullname" value="{{ $fullname }}">

				 
                     <div class="row mt03">
						<div class="col-md-12 col-xs-12">
							<h5 class="text-yellow"><i class="fa fa-university mr01" aria-hidden="true"></i>{{Lang::get('public.CashCard')}}</h5>
                        </div>
                        <div class="col-md-12 col-xs-12">
                        <div class="txtWrap">{{Lang::get('public.SerialNo')}}.* :</div>
                        </div>
						<div class="col-md-12">
						</div>
                        <div class="col-md-10 col-xs-8">
                        <div class="form-group">
                        <input type="text" class="form-control" name="serial" placeholder="{{Lang::get('public.Example')}} 1234567890">
                        </div>
                        </div>
                     </div>
					  <div class="row mt03">
                        <div class="col-md-12 col-xs-0">
                        <div class="txtWrap">{{Lang::get('public.SecurityCodeNo')}}.* :</div>
                        </div>
						<div class="col-md-12">
						</div>
								<div class="col-md-2 col-xs-8">
									<div class="form-group">
										<input type="text" class="form-control" maxlength="4"  id="number1" onkeyup="generateFullNumber()" placeholder="{{Lang::get('public.Code')}} 1">
									</div>
								</div>
								<div class="col-md-2 col-xs-8">
									<div class="form-group">
										<input type="text" class="form-control" maxlength="4"  id="number2" onkeyup="generateFullNumber()" placeholder="{{Lang::get('public.Code')}} 2">
									</div>
								</div>
								<div class="col-md-2 col-xs-8">
									<div class="form-group">
										<input type="text" class="form-control" maxlength="4"  id="number3" onkeyup="generateFullNumber()" placeholder="{{Lang::get('public.Code')}} 3">
									</div>
								</div>
								<div class="col-md-2 col-xs-8">
									<div class="form-group">
										<input type="text" class="form-control" maxlength="4"  id="number4" onkeyup="generateFullNumber()" placeholder="{{Lang::get('public.Code')}} 4">
									</div>
								</div>
								<div class="col-md-2 col-xs-8">
									<div class="form-group">
										<input type="text" class="form-control" maxlength="4"  id="number5" onkeyup="generateFullNumber()" placeholder="{{Lang::get('public.Code')}} 5">
									</div>
								</div>
								<input type="hidden" id="fullNumber" name="code"/>
                     </div>
					 
					<div class="row mt03">
						<div class="col-md-3 col-xs-12">
							<div class="txtWrap">{{Lang::get('public.Deposit')}} {{Lang::get('public.Promotion')}}.* :</div>
						</div>
						<div class="col-md-12 col-xs-12">
							<div class="form-group">
								<table>
								@foreach( $promos as $key => $promo )
									<tr>
										<td><input name="promo" class="form-control" type="radio" value="{{$promo['code']}}">&nbsp;&nbsp;</td>
										<td style="padding-left:10px;"><img src="{{$promo['image']}}"></td>
										<td><span style="color:#fff;"></span></td>
									</tr>
									<tr>
										<td>&nbsp;</td>
									</tr>
								@endforeach
									<tr>
										<td><input name="promo" class="radiobtn" type="radio" value="0"></td>
										<td><span style="color:#fff;">&nbsp;&nbsp;{{Lang::get('public.IDoNotWantPromotion')}}</span></td>
										<td></td>
									</tr>
								</table>	
							</div>
							<p style="color:#fff;"> {{Lang::get('public.RequiredFields')}}</p>
							<p style="color:#fff;"><input class="radiobtn" type="radio" name="rules" >&nbsp;&nbsp;{{Lang::get('public.IAlreadyUnderstandRules')}}</p>
							<span class="failed_message acctTextReg" style="display:block;float:left;height:100%;"></span>
						</div>
					</div>
					<div class="row">
					  <div class="col-md-4 col-xs-4">
						  <div class="btnNormal ml03">
								<a id="deposit_sbumit_btn" href="javascript:void(0)" onclick="cashcardPOST()">{{Lang::get('public.Submit')}}</a>
						  </div>
					  </div>
					</div>
				 </form>
				 <form id="deposit_form" style="display:none;">

					<input type="hidden" name="fullname" value="{{ $fullname }}">

				 
                     <div class="row mt03">
						<div class="col-md-12 col-xs-12">
                        <h5 class="text-yellow"><i class="fa fa-university mr01" aria-hidden="true"></i>{{Lang::get('public.Deposit')}} </h5>
                        </div>
                        <div class="col-md-2 col-xs-12">
                        <div class="txtWrap">{{Lang::get('public.Amount')}}* :</div>
                        </div>
                        <div class="col-md-3 col-xs-7">
                        <div class="form-group">
                        <input type="text" class="form-control" name="amount" placeholder="XXX">
                        </div>
                        </div>
                     </div>
                     
                     <div class="row mt03">
                        <div class="col-md-3 col-xs-12">
                        <div class="txtWrap">{{ Lang::get('public.BankingOptions') }}* :</div>
                        </div><br><br>
                        <div class="col-md-12 col-xs-12">
							<table cellspacing="0" id="ctl00_ctl00_ContentPlaceHolder1_ChildContentMain_deposit_table" style="width:98%;border-collapse:collapse; color:#ffffff; font-size: 13px;overflow-x:scroll">
								<tr align="left" style="height:30px;">
									<th scope="col">&nbsp;</th>
									<th scope="col">{{Lang::get('public.Bank')}}</th>
									<th scope="col">{{Lang::get('public.AccountName')}}</th>
									<th scope="col">{{Lang::get('public.AccountNumber')}}</th>
									<th scope="col">{{Lang::get('public.Minimum')}}</th>
									<th scope="col">{{Lang::get('public.Maximum')}}</th>
									<th scope="col">{{Lang::get('public.ProcessingTime')}}</th>
								</tr>
							@foreach( $banks as $key => $bank )
								<tr>
									<td><input name="bank" class="radiobtn" type="radio" value="{{$bank['bnkid']}}"></td>
									<td><img src="{{$bank['image']}}" style="width:130px;height:35px;"></td>
									<td>{{$bank['bankaccname']}}</td>		
									<td>{{$bank['bankaccno']}}</td>	
									<td>{{$bank['min']}}</td>	
									<td>{{$bank['max']}}</td>
									<td>5 min</td>
								</tr>  
							@endforeach
							</table>
                        </div>
                     </div>
                     <br>
                     <div class="row mt03">
                        <div class="col-md-2 col-xs-12">
                        <div class="txtWrap">{{ Lang::get('public.ReferenceNo') }}* :</div>
                        </div>
                        <div class="col-md-3 col-xs-7">
                        <div class="form-group">
                        <input type="text" class="form-control" name="refno">
                        </div>
                        </div>
                     </div>
                     
                     <div class="row mt03">
                        <div class="col-md-2 col-xs-12">
                        <div class="txtWrap">{{ Lang::get('public.DepositMethod') }}* :</div>
                        </div>
                        <div class="col-md-3 col-xs-7">
                        <div class="form-group">
                        <select class="form-control" id="type" name="type">
                           <option value="0">{{ Lang::get('public.OverCounter') }}</option>
                           <option value="1">{{ Lang::get('public.InternetBanking') }}</option>
                           <option value="2">{{ Lang::get('public.ATMBanking') }}</option>
                        </select>
                       </div>
                       </div>
                       </div>
                       
                <div class="row mt03">
					 <div class="col-md-2 col-xs-12">
						 <div class="txtWrap">{{ Lang::get('public.DateTime') }}* :</div>
					 </div>
					 <div class="col-md-2 col-xs-4">
						 <div class="form-group">
						 <select name="hours" class="form-control">
							<option value="01">01</option>
							<option value="02">02</option>
							<option value="03">03</option>
							<option value="04">04</option>
							<option value="05">05</option>
							<option value="06">06</option>
							<option value="07">07</option>
							<option value="08">08</option>
							<option value="09">09</option>
							<option value="10">10</option>
							<option value="11">11</option>
							<option value="12">12</option>
						 </select>
						 </div>
					 </div>
					 <div class="col-md-2 col-xs-4">
						 <div class="form-group">
							<select class="form-control" name="minutes">
							    <option value="00">00</option>
								<option value="01">01</option>
								<option value="02">02</option>
								<option value="03">03</option>
								<option value="04">04</option>
								<option value="05">05</option>
								<option value="06">06</option>
								<option value="07">07</option>
								<option value="08">08</option>
								<option value="09">09</option>
								<option value="10">10</option>
								<option value="11">11</option>
								<option value="12">12</option>
								<option value="13">13</option>
								<option value="14">14</option>
								<option value="15">15</option>
								<option value="16">16</option>
								<option value="17">17</option>
								<option value="18">18</option>
								<option value="19">19</option>
								<option value="20">20</option>
								<option value="21">21</option>
								<option value="22">22</option>
								<option value="23">23</option>
								<option value="24">24</option>
								<option value="25">25</option>
								<option value="26">26</option>
								<option value="27">27</option>
								<option value="28">28</option>
								<option value="29">29</option>
								<option value="30">30</option>
								<option value="31">31</option>
								<option value="32">32</option>
								<option value="33">33</option>
								<option value="34">34</option>
								<option value="35">35</option>
								<option value="36">36</option>
								<option value="37">37</option>
								<option value="38">38</option>
								<option value="39">39</option>
								<option value="40">40</option>
								<option value="41">41</option>
								<option value="42">42</option>
								<option value="43">43</option>
								<option value="44">44</option>
								<option value="45">45</option>
								<option value="46">46</option>
								<option value="47">47</option>
								<option value="48">48</option>
								<option value="49">49</option>
								<option value="50">50</option>
								<option value="51">51</option>
								<option value="52">52</option>
								<option value="53">53</option>
								<option value="54">54</option>
								<option value="55">55</option>
								<option value="56">56</option>
								<option value="57">57</option>
								<option value="58">58</option>
								<option value="59">59</option>
							</select>
						 </div>
					 </div>
					 <div class="col-md-2 col-xs-4">
						 <div class="form-group">
							 <select class="form-control" name="range">
								<option value="AM">AM</option>
								<option value="PM">PM</option>
							 </select>
						 </div>
					 </div>
					 <div class="col-md-2 col-xs-12">
						 <div class="form-group">
							 <input type="text" id="deposit_date" class="datepicker" style="cursor:pointer;" name="date" value="{{date('Y-m-d')}}">
						 </div>
					 </div>
               </div>
               
               <div class="row mt03">
					<div class="col-md-2 col-xs-12">
						<div class="txtWrap"> {{Lang::get('public.DepositReceipt')}}* :</div>
					</div>
					<div class="col-md-2">
						<div class="form-group">
							<input class="upload" type="file" name="receipt" id="receipt" style="color:#fff;">
						</div>
					</div>
               </div>
			   
			   <div class="row mt03">
					<div class="col-md-3 col-xs-12">
						<div class="txtWrap">{{Lang::get('public.Deposit')}} {{Lang::get('public.Promotion')}}* :</div>
					</div>
					<div class="col-md-10 col-xs-12">
						<div class="form-group">
						<table>
						@foreach( $promos as $key => $promo )
							<tr>
								<td><input name="promo" class="form-control" type="radio" value="{{$promo['code']}}">&nbsp;&nbsp;</td>
								<td style="padding-left:10px;"><img src="{{$promo['image']}}"></td>
								<td><span style="color:#fff;"></span></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>
						@endforeach
							<tr>
								<td><input name="promo" class="radiobtn" type="radio" value="0"></td>
								<td style="padding-left:10px;"><span style="color:#fff;">{{Lang::get('public.IDoNotWantPromotion')}}</span></td>
								<td></td>
							</tr>
						</table>		
						</div>
						<p style="color:#fff;"> {{Lang::get('public.RequiredFields')}}</p>
						<p style="color:#fff;"><input class="radiobtn" type="radio" name="rules" >&nbsp;&nbsp;{{Lang::get('public.IAlreadyUnderstandRules')}}</p>
						<span class="failed_message acctTextReg" style="display:block;float:left;height:100%;color:#ff0000;"></span>
					</div>
               </div>
			   
                     
                     
                  <div class="row">
					  <div class="col-md-4 col-xs-4">
						  <div class="btnNormal ml03">
								<a id="deposit_sbumit_btn" href="javascript:void(0)" onclick="deposit_submit()">{{Lang::get('public.Submit')}}</a>
						  </div>
					  </div>
                  </div>
                     
				 </form>
                 </div>
               </div>
               
               </div>
               </div>
            </div>
            
           
          

      </div>
      </div>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script>
$(function() {
	$( ".datepicker" ).datepicker({ dateFormat: 'yy-mm-dd'  , defaultDate: new Date() });
});

function cashcardPOST(){
        $.ajax({
            type: "POST",
            url: "{{action('User\CashcardController@depositProcess')}}?" + $("form").serialize(),
            data: {
                _token: "{{ csrf_token() }}"
            },
        }).done(function( json ) {
			 obj = JSON.parse(json);
			 var str = '';
			 $.each(obj, function(i, item) {
				if( '{{Lang::get('COMMON.SUCESSFUL')}}' == item ){
					alert('{{Lang::get('COMMON.SUCESSFUL')}}');
					window.location.href = "{{route('transaction')}}";
				}else{
					alert(item);
				}
			})
	
	});
}
</script>
@stop