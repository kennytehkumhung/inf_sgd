@extends('lasvegas/master')
@section('title', 'Transaction History')
@section('content')
<script>
$(function () {
	transaction_history();
});
  
function transaction_history(){
$.ajax({
	type: "POST",
	url: "{{action('User\TransactionController@transactionProcess')}}",
	data: {
		_token:   "{{ csrf_token() }}",
		date_from:		$('#date_from').val(),
		date_to:    	$('#date_to').val(),
		record_type:    $('#record_type').val()

	},
}).done(function( json ) {

			//var str;
			 var str = '';
			str += '	<tr ><td>{{Lang::get('public.ReferenceNo')}}</td><td>{{Lang::get('public.DateTime')}}</td><td>{{Lang::get('public.Type')}}</td><td>{{Lang::get('public.Amount')}}({{ Session::get('user_currency') }})</td><td>{{Lang::get('public.Status')}}</td><td>{{Lang::get('public.WithdrawalCode')}}</td><td>{{Lang::get('public.Reason')}}</td></tr>';
			
			 obj = JSON.parse(json);
			//alert(obj);
			 $.each(obj, function(i, item) {
				str +=  '<tr><td>'+item.id+'</td><td>'+item.created+'</td><td>'+item.type+'</td><td>'+item.amount+'</td><td>'+item.status+'</td><td style="color:#FF0000;">'+item.cardno+'</td><td>'+item.rejreason+'</td></tr>';
				
			})
				
			//alert(json);
			$('#trans_history').html(str);
			
	});
}

setInterval(updateTrans, 10000);
setInterval(update_mainwallet, 10000);

function updateTrans(){
	$( "#trans_history_button" ).trigger( "click" );
}

</script>
    <!-- ======== @Region: #content ======== -->
    <div id="content" class="mt02 noBg">
      <div class="container">
      <div class="row">
			<div class="col-md-12 col-xs-12">
            @include('lasvegas/trans_top' )
            </div>
            </div>
            
            <div class="row">
              <div class="col-md-2 col-xs-12">
                <div class="men1">
                <a href="{{route('deposit')}}">{{Lang::get('public.Deposit')}}</a>
                </div>
              </div>
              <div class="col-md-2 col-xs-12">
                <div class="men1">
                <a href="{{route('withdraw')}}">{{Lang::get('public.Withdrawal')}}</a>
                </div>
              </div>
              <div class="col-md-2 col-xs-12">
                <div class="men1">
                <a class="selected" href="{{route('transaction')}}">{{Lang::get('public.TransactionHistory')}}</a>
                </div>
              </div>
              <div class="col-md-2 col-xs-12">
                <div class="men1">
                <a href="{{route('transfer')}}">{{Lang::get('public.Transfer')}}</a>
                </div>
              </div>
            </div>
            
            <div class="row">
               <div class="col-md-12 col-xs-12">
               <div class="cont">
              
               
               <div class="row mt3">
                 
                 <div class="col-md-12 col-xs-12">
                     
                     <div class="row">
                        <div class="col-md-12 col-xs-12">
                        <h5 class="text-yellow"><i class="fa fa-university mr01" aria-hidden="true"></i>{{Lang::get('public.TransactionHistory')}}</h5>
                        </div>
                        </div>

                     
                     <div class="row mt03">
                        <div class="col-md-12 col-xs-12">
							 <div class="col-md-2 col-xs-4">
								 <label class="txtWrap">{{Lang::get('public.DateFrom')}} :</label>
							 </div>
							 <div class="col-md-2 col-xs-4">
								 <input type="text" class="datepicker" id="date_from" style="cursor:pointer;" value="{{date('Y-m-d')}}">
							 </div>
						</div>
                        <div class="col-md-12 col-xs-12">
							 <div class="col-md-2 col-xs-4">
								 <label class="txtWrap">{{Lang::get('public.DateTo')}} :</label>
							 </div>
							 <div class="col-md-2 col-xs-4">
								 <input type="text" class="datepicker" id="date_to" style="cursor:pointer;" value="{{date('Y-m-d')}}">
							 </div>
						</div>
					</div>
                     
                     <div class="row mt03">
                        <div class="col-md-2 col-xs-5">
                        <div class="txtWrap">{{Lang::get('public.RecordType')}}:</div>
                        </div>
                        <div class="col-md-3 col-xs-7">
                        <div class="form-group">
                        <select class="form-control" id="record_type">
							<option value="0" selected>{{Lang::get('public.CreditAndDebitRecords')}}</option>
							<option value="1">{{Lang::get('public.CreditRecords')}}</option>
							<option value="2">{{Lang::get('public.DebitRecords')}}</option>
						</select>
                       </div>
                       </div>
                       </div>

                     <div class="row">
                  <div class="col-md-4 col-xs-4">
                  <div class="btnNormal ml03">
                    <a id="trans_history_button" href="javascript:void(0)" onClick="transaction_history()"> {{Lang::get('public.Submit')}}</a>
                  </div>
                  </div>
                  </div>
                     <hr>
					 <div class="row">
                        <div class="col-md-12 col-xs-12">
							<h5 class="text-yellow"><i class="fa fa-university mr01" aria-hidden="true"></i></h5>
                        </div>
                     </div>
                     <div class="row mt03">
                        <div class="col-md-12 col-xs-12">
							<div class="table-responsive">
							<table width="100%" id="trans_history" style="color:#ffffff;">
								<tr>
									<td>{{Lang::get('public.ReferenceNo')}}</td>
									<td>{{Lang::get('public.DateOrTime')}}</td>
									<td>{{Lang::get('public.Type')}}</td>
									<td>{{Lang::get('public.Amount')}} </td>
									<td>{{Lang::get('public.Status')}}</td>
									<td>{{Lang::get('public.WithdrawalCode')}}</td>
									<td>{{Lang::get('public.Reason')}}</td>
								</tr>
								<tr>
									<td colspan="6"><h2><center>No Data Available!</center></h2></td>
								</tr>
							</table>
							</div>
                        </div>
                     </div>
                 </div>
               </div>
               
               </div>
               </div>
            </div>
          

      </div>
      </div>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script>
$(function() {
	$( ".datepicker" ).datepicker({ dateFormat: 'yy-mm-dd'  , defaultDate: new Date() });
});
</script>
@stop