<html>
<head>
	<style type="text/css">
		* {
			color: #fff;
			font-family: Arial, Helvetica, sans-serif, "KaiTi", "楷体", STKaiti, "华文楷体";
		}

		table, table td {
			background-color: #fff;
			border: 1px #ebc675 solid;
			color: #000;
			text-align: justify;
			vertical-align: top;
		}

		table thead td {
			background-color: #ebc675;
			color: #000;
			font-weight: bold;
			text-align: center;
			vertical-align: middle;
		}
	</style>
</head>
<body bgcolor="black">
	<table border="0" cellpadding="10" cellspacing="0" style="width: 100%;">
		<thead>
			<tr>
				<td colspan="2">{{ Lang::get('public.Notice') }}</td>
			</tr>
		</thead>
		<tbody>
		<?php $index = 1; ?>
		@foreach ($data['announcement'] as $r)
			<tr>
				<td style="width: 10px;">
					{{ $index++ }}
				</td>
				<td>
					{{ $r }}
				</td>
			</tr>
		@endforeach
		</tbody>
	</table>
</body>
</html>