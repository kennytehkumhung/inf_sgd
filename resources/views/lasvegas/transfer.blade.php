@extends('lasvegas/master')
@section('title', 'Transfer')
@section('content')
<script>
function submit_transfer(){
	$.ajax({
			 type: "POST",
			 url: "{{route('transfer-process')}}",
			 data: {
				_token: 		 "{{ csrf_token() }}",
				amount:			 $('#transfer_amount').val(),
				from:     		 $('#transfer_from').val(),
				to: 			 $('#transfer_to').val(),
			 },
			 beforeSend: function(){
				$('#btn_submit_transfer').attr('onClick','');
				$('#btn_submit_transfer').html('Loading...');
			 },
			 success: function(json){
			    obj = JSON.parse(json);
				 
				var str = '';
				 $.each(obj, function(i, item) {
					
					if( '{{Lang::get('COMMON.SUCESSFUL')}}' == item ){
						
						//$('.main_wallet').html(obj.main_wallet);
						
					}
					if( i  != 'main_wallet'){
						str += item + '\n';
					}
				})
				
				load_balance($('#transfer_from').val(),'from_balance');
				load_balance($('#transfer_to').val(),'to_balance');
				getBalance(false);
				$('#btn_submit_transfer').attr('onClick','submit_transfer()');
				$('#btn_submit_transfer').html('{{Lang::get('public.Submit')}}');
				alert(str);
			}
		})
 } 
 function load_balance($code, $type){
    if($code == 'MAIN'){
		url = '{{route('mainwallet')}}';
	}else{
		url = '{{route('getbalance')}}';
	}
	$.ajax({
		  type: "POST",
		  url:  url,
		  data: {
				_token: 		 "{{ csrf_token() }}",
				product:		 $code,
			 },
		  beforeSend: function(balance){
			$('#'+$type).html('<img style="float:left;position:static;top:0px;margin-left:20px;" src="{{url()}}/front/img/ajax-loading.gif" width="20" height="20">');
		  },
		  success: function(balance){
			 if($code == 'MAIN'){
				$('.main_wallet').html(parseFloat(Math.round(balance * 100) / 100).toFixed(2));
			 }
			$('#'+$type).html('{{Session::get('currency')}} '+parseFloat(Math.round(balance * 100) / 100).toFixed(2));
			total_balance += parseFloat(balance);
		  }
	})
 }
</script>
    <!-- ======== @Region: #content ======== -->
    <div id="content" class="mt02 noBg">
      <div class="container">
      <div class="row">
			<div class="col-md-12 col-xs-12">
            @include('lasvegas/trans_top' )
            </div>
            </div>
            
            <div class="row">
              <div class="col-md-2 col-xs-12">
                <div class="men1">
                <a href="{{route('deposit')}}">{{Lang::get('public.Deposit')}}</a>
                </div>
              </div>
              <div class="col-md-2 col-xs-12">
                <div class="men1">
                <a href="{{route('withdraw')}}">{{Lang::get('public.Withdrawal')}}</a>
                </div>
              </div>
              <div class="col-md-2 col-xs-12">
                <div class="men1">
                <a href="{{route('transaction')}}">{{Lang::get('public.TransactionHistory')}}</a>
                </div>
              </div>
              <div class="col-md-2 col-xs-12">
                <div class="men1">
                <a class="selected" href="{{route('transfer')}}">{{Lang::get('public.Transfer')}}</a>
                </div>
              </div>
            </div>
            
            <div class="row">
               <div class="col-md-12 col-xs-12">
               <div class="cont">
              
               
               <div class="row mt3">
                 
                <div class="col-md-12 col-xs-12">
                     <div class="row">
                        <div class="col-md-12 col-xs-12">
							<h5 class="text-yellow"><i class="fa fa-university mr01" aria-hidden="true"></i> {{Lang::get('public.Transfer')}}</h5>
                        </div>
                     </div>
                     
                     <div class="row mt03">
						 <div class="col-md-2 ">
							 <div class="txtWrap">{{Lang::get('public.TransferFrom')}} :</div>
						 </div>
						 <div class="col-md-2 ">
							 <div class="form-group">
								<select class="form-control" id="transfer_from" onchange="load_balance(this.value,'from_balance')" style="width:160px;">
								<option selected="selected" value="MAIN">{{Lang::get('public.MainWallet')}}</option>
								@foreach( Session::get('products_obj') as $prdid => $object)
									<?php
									$showPrd = true;
									if (Session::get('currency') == 'VND' && ($object->code == 'JOK')) {
										$showPrd = false;
									}
									?>
									@if ($showPrd)
										<option value="{{$object->code}}">{{$object->name}}</option>
									@endif
								@endforeach
								</select>
							   </div>
						 </div>
						 <div class="col-md-2">
							 <div class="txtWrap">{{Lang::get('public.TransferTo')}} :</div>
						 </div>
						 <div class="col-md-2 ">
							 <div class="form-group">
								<select class="form-control" id="transfer_to" onchange="load_balance(this.value,'to_balance')" style="width:160px;">
									@foreach( Session::get('products_obj') as $prdid => $object)
									<?php
									$showPrd = true;
										if (Session::get('currency') == 'VND' && ($object->code == 'JOK')) {
											$showPrd = false;
										}
									?>
										@if ($showPrd)
											<option value="{{$object->code}}">{{$object->name}}</option>
										@endif
									@endforeach
									<option  value="MAIN">{{Lang::get('public.MainWallet')}}</option>
								</select>
							   </div>
						 </div>
					 </div>
               
				   <div class="row mt03">
						 <div class="col-md-2 col-xs-4">
							<div class="txtWrap">{{Lang::get('public.Balance')}} :</div>
						 </div>
						 <div class="col-md-2 col-xs-3 text-center">
							  <div class="txtWrap" id="from_balance">{{Session::get('currency')}} {{App\Http\Controllers\User\WalletController::mainwallet()}}</div>
						 </div>
						 <div class="col-md-2 col-xs-1 text-center">

						 </div>
						 <div class="col-md-2 col-xs-3 text-center">
							 <div class="txtWrap" id="to_balance">{{Session::get('currency')}} 0.00</div>
						 </div>
				   </div>
				   <div class="row mt03">
				   <br>
				   </div>
                     
                     <div class="row mt03">
                        <div class="col-md-2 col-xs-5">
							<div class="txtWrap">{{Lang::get('public.Amount')}} :</div>
                        </div>
                        <div class="col-md-3 col-xs-7">
							<div class="form-group">
								<input class="form-control" id="transfer_amount" value="0.00" type="text">
								<span id="error_deposit" style="color:Red;display:none;">*Your account have 0.00 balance, please make a {{Lang::get('public.Deposit')}}.</span>
								<span id="error_require" style="color:Red;display:none;">{{Lang::get('public.Required')}}</span>
						   </div>
                       </div>
                     </div>

					<div class="row">
					  <div class="col-md-4 col-xs-4">
						  <div class="btnNormal ml03">
							<a id="btn_submit_transfer" href="javascript:void(0)" onClick="submit_transfer()" >{{Lang::get('public.Submit')}}</a>
						  </div>
					  </div>
					</div>
                     
                 </div>
               </div>
               
               </div>
               </div>
            </div>
          

      </div>
      </div>
@stop