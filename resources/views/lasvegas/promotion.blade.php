@extends('lasvegas/master')

@section('title', 'Promotions')

@section('css')
@parent

<link href="{{ asset('/royalewin/resources/css/promo.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('/lasvegas/resources/css/style4.css') }}?v2" rel="stylesheet" type="text/css" />

@endsection

@section('js')
@parent

<script type="text/javascript">
    $(function($) {
        $('#accordion').find('.accordion-toggle').click(function(){
            //Expand or collapse this panel
            $(this).next().slideToggle('fast');

            //Hide the other panels
            $(".accordion-content").not($(this).next()).slideUp('fast');
        });
    });
	
	 function modalBox(title,content){
		$("#titleBox").html(title);
        $("#contentBox").html(content);
	 }
</script>
@endsection

@section('content')
<div id="content noBg">
	<div class="mission block-pd-sm block-bg-noise">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-xs-12 text-center specialTxt">
				PROMOTION
				</div>
			</div>
			<div class="row">
			@foreach ($promo as $key => $value )
				<div class="col-md-3 col-sm-3 col-xs-12 text-center">
					<a data-toggle="modal" data-target="#promoBx" href="#" onclick="modalBox('{{$value['title']}}','{{$value['content']}}')"><img class="img-responsive" src="{{$value['image']}}"></a>
				</div>
			@endforeach
			</div>
		</div>
	</div>
</div>
<div id="promoBx" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title" id="titleBox"></h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12 col-xs-12" id="contentBox">
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<div class="fp pull-left">
				</div>
				<div class="pull-right">
					@if (Auth::user()->check())
						<a href="{{route('deposit')}}" class="btn btn-success">Join Now</a>
					@else
						<a href="{{route('register')}}" class="btn btn-success">Join Now</a>
					@endif
				</div>
			</div>
		</div>
	</div>
</div>
@endsection