<html>
<head>
 <link href="{{url()}}/front/resources/css/slot.css" rel="stylesheet">
 <link href="{{url()}}/front/resources/css/style_2.css" rel='stylesheet' type='text/css'>

<!-- Home slider style -->
<link rel="stylesheet" href="{{url()}}/front/resources/css/style_3.css">
</head>
<body>
	<div class="slot_menu">
		<ul>
			<li><a href="{{route('pltb', [ 'type' => 'brand' , 'category' => '1' ] )}}">Branded Games (21)</a></li>
			<li><a href="{{route('pltb', [ 'type' => 'slot' , 'category' => 'slot' ] )}}">Slot (152)</a></li>
			<li><a href="{{route('pltb', [ 'type' => 'slot' , 'category' => 'arcade' ] )}}">Arcade (4)</a></li>
			<li><a href="{{route('pltb', [ 'type' => 'slot' , 'category' => 'tablecards' ] )}}">Tablecards (14)</a></li>
		</ul>
	</div>
	<div id="slot_lobby">
		@if (!$isMobile)
			<div class="slot_box">
			<a onclick=" @if (Auth::user()->check())window.open('{{route('pltbslotiframe' , [ 'gamecode' => 'sfh' ] )}}', 'pltb_slot');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif  " href="javascript:void(0)">
			<img width="150" height="150" alt="" src="{{ asset('/front/img/plt/sfh.jpg') }}">
			</a>
			<span>Safari Heat</span>
			</div>
		@endif

		<div class="slot_box">
		<a onclick=" @if (Auth::user()->check())window.open('{{route('pltbslotiframe' , [ 'gamecode' => 'dnr' ] )}}', 'pltb_slot');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif  " href="javascript:void(0)">
		<img width="150" height="150" alt="" src="{{ asset('/front/img/plt/dnr.jpg') }}">
		</a>
		<span>Dolphin Reef</span>
		</div>

		@foreach( $lists as $list )
		<div class="slot_box">
			<a href="javascript:void(0)" onclick="@if (Auth::user()->check())window.open('{{route('pltbslotiframe' , [ 'gamecode' => ($list->html5Code != '' ? $list->html5Code : $list->code) ] )}}', 'pltb_slot');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif ">
				<img src="{{asset('/front/img/plt/'.($list->code != '' ? $list->code : $list->html5Code).'.jpg')}}" width="150" height="150" alt=""/>
			</a>

			@if (Lang::getLocale() == 'cn')
				<span>{{$list->gameNameCn}}</span>
			@else
				<span>{{$list->gameName}}</span>
			@endif
		</div>				 
		@endforeach
		<div class="clr"></div>
	</div>
    <div class="clr"></div> 
<!--Slot-->
</body>
</html>
