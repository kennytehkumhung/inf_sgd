<html>
<head>
<link href="{{url()}}/ampm/resources/css/style.css" rel="stylesheet" type="text/css" />
</head>
<body>	

	
				   <!--Slot Menu-->
				   <div class="slotMenuHolder">
				   <div class="slot_menu">
				   <ul>
						<li class="tab @if($category == 1 && $type == 'pgames') st_selected @endif"><a @if($category == 1 && $type == 'pgames') style="color:black;" @endif href="{{route('plt', [ 'type' => 'pgames' , 'category' => '1' ] )}}">Progressive Games</a></li>
						<li class="tab @if($category == 1 && $type == 'newgames') st_selected @endif"><a @if($category == 1 && $type == 'newgames') style="color:black;" @endif href="{{route('plt', [ 'type' => 'newgames' , 'category' => '1' ] )}}">New Games</a></li>
						<li class="tab @if($category == 1 && $type == 'brand') st_selected @endif"><a @if($category == 1 && $type == 'brand') style="color:black;" @endif href="{{route('plt', [ 'type' => 'brand' , 'category' => '1' ] )}}">Branded Games</a></li>
						<li class="tab @if($category == 'slot' && $type == 'slot') st_selected @endif"><a @if($category == 'slot' && $type == 'slot') style="color:black;" @endif href="{{route('plt', [ 'type' => 'slot' , 'category' => 'slot' ] )}}">Slot</a></li>
						<li class="tab @if($category == 'videopoker' && $type == 'slot') st_selected @endif"><a @if($category == 'videopoker' && $type == 'slot') style="color:black;" @endif href="{{route('plt', [ 'type' => 'slot' , 'category' => 'videopoker' ] )}}">Video Poker</a></li>
						<li class="tab @if($category == 'arcade' && $type == 'slot') st_selected @endif"><a @if($category == 'arcade' && $type == 'slot') style="color:black;" @endif href="{{route('plt', [ 'type' => 'slot' , 'category' => 'arcade' ] )}}">Arcade</a></li>
						<li class="tab @if($category == 'tablecards' && $type == 'slot') st_selected @endif"><a @if($category == 'tablecards' && $type == 'slot') style="color:black;" @endif href="{{route('plt', [ 'type' => 'slot' , 'category' => 'tablecards' ] )}}">Tablecards</a></li>
						<li class="tab @if($category == 'scratchcards' && $type == 'slot') st_selected @endif"><a @if($category == 'scratchcards' && $type == 'slot') style="color:black;" @endif href="{{route('plt', [ 'type' => 'slot' , 'category' => 'scratchcards' ] )}}">Scratchcards</a></li>
				   </ul>
				   </div>
				   </div>
				   <!--Slot Menu-->
				   <!--Slot Lobby-->
				  <div class="slot_lobby">
				    @foreach( $lists as $list )
						
						<div class="slot_box" onclick="@if (Auth::user()->check())window.open('{{route('pltslotiframe' , [ 'gamecode' => $list['code'] ] )}}', 'plt_slot', 'width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif">
							<img src="{{url()}}/front/img/plt/{{$list->code}}.jpg" width="140" height="140" alt=""/>
							<span>{{$list->gamename_en}}</span>
						</div>									
					@endforeach
		
				  
				  <div class="clr"></div>
				  </div>
				   <!--Slot Lobby-->
			   </div>
			   
<!--Slot-->
</body>
</html>