@extends('369bet/master')

@section('title', 'Profile')

@section('content')
  <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="{{url()}}/369bet/resources/js/jquery-ui.js"></script>
   <link href="{{url()}}/369bet/resources/css/table.css" rel="stylesheet">
<script>
	$(document).ready(function() { 
		getBalance(true);
	});
	
$(function() {
	$( ".datepicker" ).datepicker({ dateFormat: 'dd-mm-yy'  , defaultDate: new Date() });
});
  
function transaction_history(){
    var str = $('#date_from').val();
    var res = str.split("-");
    var datefrom = res[2]+'-'+res[1]+'-'+res[0];
    
    var strto = $('#date_to').val();
    var resto = strto.split("-");
    var dateto = resto[2]+'-'+resto[1]+'-'+resto[0];
    
$.ajax({
	type: "POST",
	url: "{{action('User\TransactionController@transactionProcess')}}",
	data: {
		_token:   "{{ csrf_token() }}",
		date_from:                  datefrom,
		date_to:                    dateto,
		record_type:                $('#record_type').val()

	},
}).done(function( json ) {

			//var str;
			 var str = '';
			str += '	<tr ><td>{{Lang::get('public.ReferenceNo')}}</td><td>{{Lang::get('public.DateOrTime')}}</td><td>{{Lang::get('public.type')}}</td><td>{{Lang::get('public.Amount')}}(THB)</td><td>{{Lang::get('public.Status')}}</td><td>{{Lang::get('public.reason')}}</td></tr>';
			
			 obj = JSON.parse(json);
			//alert(obj);
			 $.each(obj, function(i, item) {
                             var datetime = item.created;
                             var sdatetime = datetime.split(" ");
                             var date = sdatetime[0].split("-");
                             var rdatetime = date[2]+'-'+date[1]+'-'+date[0]+' '+sdatetime[1];
                             
				str +=  '<tr><td>'+item.id+'</td><td>'+rdatetime+'</td><td>'+item.type+'</td><td>'+item.amount+'</td><td>'+item.status+'</td><td>'+item.rejreason+'</td></tr>';
				
			})
				
			//alert(json);
			$('#trans_history').html(str);
			
	});
}

setInterval(updateTrans, 10000);
setInterval(update_mainwallet, 10000);

function updateTrans(){
	$( "#trans_history_button" ).trigger( "click" );
}

</script>

<div class="midSectCont">
<br>
@include('369bet/trans_top' )

<!--ACCOUNT MANAGAMENT MENU-->
      <div class="inlineAccMenu">
      <ul>
    	  <li><a href="{{route('deposit')}}">{{Lang::get('public.Deposit')}}</a></li>
		  <li><a href="{{route('withdraw')}}">{{Lang::get('public.Withdrawal')}}</a></li>
		  <li><a href="{{route('transaction')}}">{{Lang::get('public.TransactionHistory')}}</a></li>
		  <li><a href="{{route('transfer')}}">{{Lang::get('public.Transfer')}}</a></li>
      </ul>
      </div>
      <!--ACCOUNT MANAGAMENT MENU-->
   <!--ACCOUNT TITLE-->
      <div class="title_bar">
      <span>{{Lang::get('public.TransactionHistory')}}</span>
      </div>
      <!--ACCOUNT TITLE-->
     <!--ACCOUNT CONTENT-->
      <div class="acctContent">

<div class="clr"></div>
</div>
<span class="wallet_title"><i class="fa fa-file-o"></i>{{Lang::get('public.TransactionHistory')}}</span>
<div class="acctRow">
			<div class="acctRow">
				<label>{{Lang::get('public.DateFrom')}} :</label>
				<input type="text" class="datepicker" id="date_from" style="cursor:pointer;" value="{{date('d-m-Y')}}"><br>
			</div>
			<div class="acctRow">
				<label>{{Lang::get('public.DateTo')}}:</label>
				<input type="text" class="datepicker" id="date_to" style="cursor:pointer;" value="{{date('d-m-Y')}}">
			</div>
			<div class="acctRow">
				<label>{{Lang::get('public.RecordType')}}:</label>
				<select id="record_type" style="width:200px;">
					<option value="0" selected>{{Lang::get('public.CreditAndDebitRecords')}}</option>
					<option value="1">{{Lang::get('public.CreditRecords')}}</option>
					<option value="2">{{Lang::get('public.DebitRecords')}}</option>
				</select>
			</div>
			<div class="acctRow">
				<div class="submitAcct">
					<a id="trans_history_button" href="#" onClick="transaction_history()"> {{Lang::get('public.Submit')}}</a>
					<br><br>
				</div>
			</div>
		</div>
		<div class="clr"></div>
		<div class="table" style="display:block;">
			<table width="100%" id="trans_history">
				<tr>
					<td>{{Lang::get('public.ReferenceNo')}}</td>
					<td>{{Lang::get('public.DateOrTime')}}</td>
					<td>{{Lang::get('public.type')}}</td>
					<td>{{Lang::get('public.Amount')}}(THB) </td>
					<td>{{Lang::get('public.Status')}}</td>
					<td>{{Lang::get('public.reason')}}</td>
				</tr>
				<tr>
					<td colspan="6"><h2><center>ไม่พบข้อมูล</center></h2></td>
				</tr>
			</table>
		</div>
      
      </div>
      <!--ACCOUNT CONTENT-->



<div class="clr"></div>

</div>

@stop