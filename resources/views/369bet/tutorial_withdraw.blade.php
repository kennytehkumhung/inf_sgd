@extends('369bet/master')

@section('title', 'Register Tutorial')

@section('content')
<div style="width:1050px; background: #e2e3e4; margin:auto; overflow:hidden; border:solid 1px #DADBDE;">
<div align="center" style="width:1036px; margin:auto; padding:7px 0 0px 0;"></div>
  <div style="width:1036px; margin:auto; padding-top:15px;">
  
  	<div align="center" style="margin-bottom:-10px;">
  	  <table id="Table_01" width="992" height="85" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td><a href="{{route('deposit_tutorial')}}" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image4','','{{url()}}/369bet/img/tutorial/howto2_01.png',1)"><img src="{{url()}}/369bet/img/tutorial/howto_01.png" name="Image4" width="250" height="85" border="0" id="Image4" /></a></td>
          <td><a href="{{route('withdraw_tutorial')}}"><img src="{{url()}}/369bet/img/tutorial/howto2_02.png" width="248" height="85" alt="" /></a></td>
          <td><a href="{{route('transfer_tutorial')}}" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image5','','{{url()}}/369bet/img/tutorial/howto2_03.png',1)"><img src="{{url()}}/369bet/img/tutorial/howto_03.png" name="Image5" width="248" height="85" border="0" id="Image5" /></a></td>
          <td><a href="{{route('register_tutorial')}}" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image6','','{{url()}}/369bet/img/tutorial/howto2_04.png',1)"><img src="{{url()}}/369bet/img/tutorial/howto_04.png" name="Image6" width="246" height="85" border="0" id="Image6" /></a></td>
        </tr>
      </table>
  	</div>
    <div style=" margin:auto; width:950px; border:solid 1px #adadad; padding:20px;  ">
      <div align="center" style="font-family:Tahoma; font-size:28px; color:#0066CC; font-weight:bold; padding-bottom:10px; border-bottom:3px solid #0066cc">วิธีการถอนเงิน</div>
      <br /><br /><div align="center" style="width:900px; margin:auto; height:478px; background:url({{url()}}/369bet/img/tutorial/bg-howto.png);">
      	<div style="width:540px; height:306px; float:right; margin-right:30px; margin-top:24px;"><iframe width="540" height="306" src="https://www.youtube.com/embed/m9vSiXg6JG4" frameborder="0" allowfullscreen></iframe></div>
      </div>
      <br />
      <h4><font color="#333333" size="3"><strong><font face="Tahoma">ถอนขั้นต่ำ 1,000 บาท / โอนเงิน ภายใน 20 นาที / 1 วัน ถอนได้ 2 ครั้ง</font></strong></font></h4>
      <p><font color="#333333" size="3" face="Tahoma">วิธีการในการถอนเงินจากบัญชีผู้เล่น <strong>369Bet</strong></font></p>
      <ul>
        <li><font color="#333333" size="3" face="Tahoma">สมาชิกที่เปิดบัญชีในประเทศไทยและใช้เงินสกุล THB ( ไทยบาท ) เท่านั้น<br />
          ในการถอนเงินของคุณจากชื่อผู้ใช้ ในการเล่นเกมส์ของคุณ โปรดทำตามขั้นตอนด้านล่างนี้ <br />
          ขั้นตอนที่ 1. เข้าสู่ระบบ โดยการป้อนชื่อผู้ใช้ และ รหัสผ่าน <br />
          ขั้นตอนที่ 2. คลิกที่การถอน <br />
          ขั้นตอนที่ 3. กดยอดเงินที่ผู้เล่นต้องการถอนจากบัญชี <br />
          <br />
          </font>
          <div>
            <table width="905" cellspacing="1" bgcolor="#999999">
              <thead>
                <tr>
                  <td width="253" rowspan="2" bgcolor="#FFFFFF"><div align="center"><font color="#333333" size="3" face="Tahoma">
                    <strong>ตัวเลือกธนาคาร</strong></font></div></td>
                  <td height="36" colspan="2" bgcolor="#FFFFFF"><div align="center"><font color="#333333" size="3" face="Tahoma"><strong>จำกัด การทำธุรกรรม (THB)</strong></font></div></td>
                  <td width="269" rowspan="2" bgcolor="#FFFFFF"><div align="center"><font color="#333333" size="3" face="Tahoma">
                    <strong>ระยะเวลาการทำธุรกรรม</strong></font></div></td>
                </tr>
                <tr>
                  <td width="152" height="37" bgcolor="#FFFFFF"><div align="center"><font color="#333333" size="3" face="Tahoma"><strong>ขั้นต่ำ</strong></font></div></td>
                  <td width="211" bgcolor="#FFFFFF"><div align="center"><font color="#333333" size="3" face="Tahoma"><strong>สูงสุด</strong></font></div></td>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td height="34" colspan="4" bgcolor="#0066CC"><div align="center"><font color="#FFFFFF" size="3" face="Tahoma"><strong>ถอน</strong></font></div></td>
                </tr>
                <tr>
                  <td height="35" align="center" bgcolor="#FFFFFF"><font color="#333333" size="3" face="Tahoma">ถอนด่วน</font></td>
                  <td align="center" bgcolor="#FFFFFF"><font color="#333333" size="3" face="Tahoma">1,000</font></td>
                  <td align="center" bgcolor="#FFFFFF"><font color="#333333" size="3" face="Tahoma">100,000</font></td>
                  <td align="center" bgcolor="#FFFFFF"><font color="#333333" size="3" face="Tahoma"><span lang="th" xml:lang="th">20 นาที</span></font></td>
                </tr>
                <tr>
                  <td height="35" align="center" bgcolor="#FFFFFF"><font color="#333333" size="3" face="Tahoma">โอนเงินผ่านธนาคาร</font></td>
                  <td align="center" bgcolor="#FFFFFF"><font color="#333333" size="3" face="Tahoma">100,001</font></td>
                  <td align="center" bgcolor="#FFFFFF"><font color="#333333" size="3" face="Tahoma">300,000</font></td>
                  <td align="center" bgcolor="#FFFFFF"><font color="#333333" size="3" face="Tahoma">1ชั่วโมง</font></td>
                </tr>
                <tr>
                  <td height="35" align="center" bgcolor="#FFFFFF"><font color="#333333" size="3" face="Tahoma">ถอนเงินจำนวนมาก</font></td>
                  <td colspan="2" align="center" bgcolor="#FFFFFF"><font color="#333333" size="3" face="Tahoma">300,000 ขึ้นไป</font></td>
                  <td align="center" bgcolor="#FFFFFF"><font color="#333333" size="3" face="Tahoma">1 วัน</font></td>
                </tr>
              </tbody>
            </table>
          </div>
          <font color="#333333" size="3" face="Tahoma"><br />
          ขั้นตอนที่ 4. ตรวจสอบข้อมูลที่เกียวกับธนาคารให้ถูกต้อง เมื่อถูกต้องแล้ว คลิกส่ง เพื่อส่งคำขอการถอนเงิน <br />
          ขั้นตอนที่ 5. เมื่อตรวจสอบคำขอถอนเงิน เราจะดำเนินการโอนเงินผ่านธนาคารโดยตรงกับบัญชีธนาคารของคุณ <br />
          ขั้นตอนที่ 6. สมาชิกจะได้รับ SMS แจ้งเตือนเมื่อมีการถอนเงิน เสร็จสิ้น <br />
          ขั้นตอนที่ 7. เมื่อธุรกรรมเสร็จสิ้น สมาชิกสามารถตรวจสอบบันทึกการถอนในหน้าสถานะการทำธุรกรรมได้</font></li>
      </ul>
      <font color="#333333" size="3" face="Tahoma"><br />
      <strong>หมายเหตุ :</strong> </font>
      <ul>
        <li><font color="#333333" size="3" face="Tahoma">จำนวนเงินในการถอนจำนวนมาก อาจจะใช้เวลานานกว่าปรกติในการได้รับเงิน<br />
            <strong>ช่องทางการถอนที่สามารถดำเนินการได้</strong><br />
          <br />
          ในขณะนี้ ทางบริษัทสามารถดำเนินการตามคำขอถอนเงิน ผ่านการโอนเงินจากธนาคารโดยตรงเท่านั้น ซึ่งหมายถึง<br />
          การโอนโดยตรงเข้าบัญชีธนาคารของสมาชิกผ่านบัญชีธนาคารของ 369Bet ดังนั้น สมาชิกจึงควรให้รายละเอียดบัญชีธนาคารที่ถูกต้อง <br />
          ในกรณีที่เกิดข้อผิดพลาดเกี่ยวกับข้อมูลบัญชีธนาคารสมาชิกควรติดต่อเจ้าหน้าที่บริการลูกค้าทันที</font></li>
      </ul>
      <font color="#333333" size="3" face="Tahoma"><br />
      <strong>วิธีการฝากเงินมีรูปแบบใดบ้าง ?</strong> </font>
      <p><font color="#333333" size="3" face="Tahoma">ขณะนี้เรามีการรับฝากเงินโดยผ่านทางการโอนเงินทางธนาคารโดยตรงเท่านั้น คุณสามารถฝากเงินโดยตรงจากบัญชีธนาคารส่วนบุคคลของคุณ ไปยังบัญชีธนาคารของ 369Bet รวมถึงการโอนเงินทางโทรศัพท์ , อินเตอร์เน็ตแบงค์กิ้ง , เคาน์เตอร์ฝากเงิน หรือ ตู้เอทีเอ็ม</font></p>
      <font color="#333333" size="3" face="Tahoma"><br />
      <strong>ธนาคารที่เราให้บริการ มีอะไรบ้าง</strong> </font>
      <ul>
        <li><font color="#333333" size="3" face="Tahoma">กสิกรไทย</font></li>
        <li><font color="#333333" size="3" face="Tahoma">กรุงไทย</font></li>
        <li><font color="#333333" size="3" face="Tahoma">ไทยพาณิชย์</font></li>
        <li><font color="#333333" size="3" face="Tahoma">กรุงศรี</font></li>
        <li><font color="#333333" size="3" face="Tahoma">กรุงเทพ</font></li>
        <li><font color="#333333" size="3" face="Tahoma">ทีเอ็มบี</font></li>
      </ul>
    </div>
  </div>
  
  <br />

@stop

@section('bottom_js')

@stop