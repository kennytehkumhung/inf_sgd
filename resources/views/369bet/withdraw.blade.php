@extends('369bet/master')

@section('title', 'Profile')

@section('content')

<script>
	$(document).ready(function() { 
		getBalance(true);
	});
	setInterval(update_mainwallet, 10000);
	function submit_transfer(){
	$.ajax({
			 type: "POST",
			 url: "{{route('withdraw-process')}}?"+$('#withdraw_form').serialize(),
			 data: {
				_token: 		 "{{ csrf_token() }}",
			 },
			 beforeSend: function(){
				
			 },
			 success: function(json){
					obj = JSON.parse(json);
					 var str = '';
					 $.each(obj, function(i, item) {
                                             if(item == "The amount field is required."){
                                                 item = "โปรดระบุยอดเงินที่ต้องการถอนเงิน";
                                             }
						
						if( '{{Lang::get('COMMON.SUCESSFUL')}}' == item ){
							alert('{{Lang::get('COMMON.SUCESSFUL')}}');
							window.location.href = "{{route('transaction')}}";
						}else{
							str += item + '<br>';
						}
					})
					$('.failed_message').html(str);
				
			}
		})
	} 
</script>

<div class="midSectCont">
<br>
@include('369bet/trans_top' )

<!--ACCOUNT MANAGAMENT MENU-->
      <div class="inlineAccMenu">
      <ul>
    	  <li><a href="{{route('deposit')}}">{{Lang::get('public.Deposit')}}</a></li>
		  <li><a href="{{route('withdraw')}}">{{Lang::get('public.Withdrawal')}}</a></li>
		  <li><a href="{{route('transaction')}}">{{Lang::get('public.TransactionHistory')}}</a></li>
		  <li><a href="{{route('transfer')}}">{{Lang::get('public.Transfer')}}</a></li>
      </ul>
      </div>
      <!--ACCOUNT MANAGAMENT MENU-->
   <!--ACCOUNT TITLE-->
      <div class="title_bar">
      <span>{{Lang::get('public.Withdrawal')}}</span>
      </div>
      <!--ACCOUNT TITLE-->
     <!--ACCOUNT CONTENT-->
	 
      <div class="acctContent">
      <span class="wallet_title"><i class="fa fa-money"></i>{{Lang::get('public.Withdrawal')}}<font color="red" style="margin-left:10px;font-family:Angsana New; font-size: 16px;"> ( ถ้าท่านสมาชิก แจ้งถอนเงินเข้ามาหลัง 22.00 น. ทาง 369bet จะทำการโอนเงินให้เวลา 11.00 น. ในเช้าวันถัดไป )</font></span>
	  <form id="withdraw_form">
     <div class="acctRow">
				<label>{{Lang::get('public.Balance')}} (THB) :</label>
                                <span class="acctText">{{App\Http\Controllers\User\WalletController::mainwallet()}}</span> บาท
				<div class="clr"></div>
			</div>
			<div class="acctRow">
				<label>{{Lang::get('public.AmountIwithdraw')}} (THB) * :</label><input type="text" name="amount"/> บาท    <font color="red" style="font-family:Angsana New; font-size: 16px;">*** ถอนเงินข้ำต่ำ  1,000 บาท ( 1 วันถอนได้ 2 ครั้งต่อ 1 ยูสเซอร์ )</font>
				<div class="clr"></div>
			</div>
			<div class="acctRow">
				<label>{{Lang::get('public.BankName')}} :</label>
					@if($accbank != '')
                                            <span class="acctText">{{$accbank->bankname}}</span>
                                            <input type="hidden" id="accountnopass" name="bank"  value= "{{$accbank->bankcode}}"/>
                                        @endif
				<div class="clr"></div>
			</div>
			<div class="acctRow">
				<label>{{Lang::get('public.FullName')}} :</label>
                                <span class="acctText">{{Session::get('fullname')}}</span>
				<div class="clr"></div>
			</div>
			<div class="acctRow">
				<label>{{Lang::get('public.BankAccountNo')}} :</label>
				<span class="acctText">{{$accbank->bankaccno}}</span> 
                                <input type="hidden" id="accountnopass" name="accountno" value= "{{$accbank->bankaccno}}"/>
				<div class="clr"></div>
			</div>     
		<br>
		

		
		<div class="submitAcct">
			<a href="javascript:void(0)" onClick="submit_transfer()">{{Lang::get('public.Submit')}}</a>
		</div>
		<br><br>
		<span class="failed_message acctTextReg" style="display:block;float:left;height:100%;color:red;"></span>

      
      </div>
      <!--ACCOUNT CONTENT-->
	</form>


<div class="clr"></div>

</div>

@stop