@extends('369bet/master')

@section('title', 'Sportsbook, Live Casino, Slot Games and High 4D Payout')

@section('banner')
@if( $popup_banner )
    <p style="display: none;"><a class="popup_banner" href="{!! $popup_banner !!}"></a></p>

    <script type="text/javascript">
        $(function () {
            $(".popup_banner").colorbox({open: true});
        });
    </script>
@endif
<div class="sliderCont">
<div class="sliderContInner">
<ul class="bxslider">
		  @if(isset($banners))
			@foreach( $banners as $key => $banner )
				<li><a href="@if($banner->url != 'none'){{$banner->url}}@endif"><img src="{{$banner->domain}}/{{$banner->path}}" class="circles" /></a></li>
				
            @endforeach
		  @endif
		
          </ul>
</div>
</div>
@stop

@section('content')



<div class="midSectCont">

<div class="midIcon1">
  <a href="{{route('banking')}}"><img src="{{url()}}/369bet/img/banner-agent.jpg" width="510" height="302">
</div>
<div class="midIcon2">
<iframe width="100%" height="302" src="https://www.youtube.com/embed/PbSwupK-kCM" frameborder="0" allowfullscreen></iframe>	
</div>
<div class="clr"></div>

<div class="bnr01">
  <a href="{{route('livefootball')}}"><img src="{{url()}}/369bet/img/banner-dooball_home.jpg" width="1004" height="250"></a>
</div>

<div class="bnr02">
  <a href="{{route('register')}}"><img src="{{url()}}/369bet/img/register1.jpg" width="261" height="108"></a>
  <img src="{{url()}}/369bet/img/register2.jpg" width="460" height="108">
  <a href="{{route('register_tutorial')}}"><img src="{{url()}}/369bet/img/contactimages.gif" width="283" height="108"></a>
  <div class="clr"></div>
</div>

<div class="bnr03">
  <a href="{{route('deposit_tutorial')}}"><img src="{{url()}}/369bet/img/brt-1.jpg" width="332" height="140"> </a>
  <a href="{{route('withdraw_tutorial')}}"><img src="{{url()}}/369bet/img/brt-2.jpg" width="332" height="140"> </a>
  <a href="{{route('transfer_tutorial')}}"><img src="{{url()}}/369bet/img/brt-3.jpg" width="331" height="140"> </a>
  <div class="clr"></div>
</div>

<div class="bnr04">
<a href="http://www.369zean.com/" target="_blank"><img src="{{url()}}/369bet/img/banner3-3.jpg" width="500" height="250"></a>
<a href="{{route('sbo_tutorial')}}"><img src="{{url()}}/369bet/img/banner3-4.jpg" width="500" height="250"></a>
<div class="clr"></div>
</div>

<div class="bnr05">
<a href="{{route('sport')}}"><img src="{{url()}}/369bet/img/product_sport.jpg" width="1004" height="250"></a>
<div class="clr"></div>
</div>

<div class="bnr06">
<a href="{{route('livecasino')}}"><img src="{{url()}}/369bet/img/product_casino.jpg" width="1004" height="250"></a>
<div class="clr"></div>
</div>

<div class="bnr07">
<a href="{{route('slot', [ 'type' => 'plt' ] )}}"><img src="{{url()}}/369bet/img/product_game.jpg" width="500" height="250"></a>
<a href="{{route('lotto')}}"><img src="{{url()}}/369bet/img/product_lotto.jpg" width="500" height="250"></a>
<div class="clr"></div>
</div>

<div class="bnr08">
<span>เกี่ยวกับ 369bet.com</span>

เราภูมิใจนำเสนอรูปแบบความบันเทิงในการเล่นคาสิโนที่พิเศษที่สุด. 369Bet มอบประสบการณ์การเล่นเกมส์แบบใหม่ที่คุณไม่เคยคิดฝัน. ไม่ว่าจะ หน้ากีฬา คาสิโนสด เกมส์สล๊อต หวย เรามีให้บริการคุณทั้งหมด! เราการันตีว่าคุณจะได้รับความรู้สึกตื่นเต้นในการเล่นคาสิโนด้วยสภาพแวดล้อมที่ความปลอดภัยสูงสุด โดยคุณไม่จำเป็นจะต้องกังวลใดๆทั้งสิ้น.

</div>

</div>
	  
@stop