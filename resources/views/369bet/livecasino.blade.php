@extends('369bet/master')

@section('title', 'Sportsbook, Live Casino, Slot Games and High 4D Payout')

@section('banner')
<div class="sliderCont">
<div class="sliderContInner tp04">
<img src="{{url()}}/369bet/img/tab-casino.jpg" width="1024" height="300">
</div>
</div>
@stop

@section('content')


<div class="midSectCont">

<div class="bnr09">
  @if(in_array('PLTB',Session::get('valid_product')))
	  <a href="#" onClick="@if (Auth::user()->check())window.open('{{route('pltbiframe')}}', 'casinoplt', 'width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif">
			<img src="{{url()}}/369bet/img/casino1.png" width="250" height="302">
	  </a>
  @endif
  @if(in_array('AGG',Session::get('valid_product')))
	  <a href="#" onClick="@if (Auth::user()->check())window.open('{{route('agg')}}', 'casino11', 'width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif">
		<img src="{{url()}}/369bet/img/casino2.png" width="250" height="302">
	  </a>
  @endif
  @if(in_array('CT8',Session::get('valid_product')))
	  <a href="#" onClick="@if (Auth::user()->check())window.open('{{route('ct8')}}', 'casino11', 'width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif">
		<img src="{{url()}}/369bet/img/casino3.png" width="250" height="302">
	  </a>
  @endif
   @if(in_array('W88',Session::get('valid_product')))
	  <a href="#" onClick="@if (Auth::user()->check())window.open('{{route('w88', [ 'type' => 'casino' , 'category' => 'live' ] )}}', 'casino12', 'width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif">
		<img src="{{url()}}/369bet/img/casino4.png" width="250" height="302">
	  </a>
  @endif
   @if(in_array('ALB',Session::get('valid_product')))
	  <a href="#" onClick="@if (Auth::user()->check())window.open('{{route('alb')}}', 'casino11', 'width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif">
		<img src="{{url()}}/369bet/img/casino5.png" width="250" height="302">
	  </a>
  @endif
  @if(in_array('MXB',Session::get('valid_product')))
	  <a onClick="@if (!Auth::user()->check())alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif" href="@if (Auth::user()->check()){{route('mxb',[ 'type' => 'casino' , 'category' => 'baccarat' ])}}@endif">
		<img src="{{url()}}/369bet/img/casino6.png" width="250" height="302">
	  </a>
  @endif
  @if(in_array('GDX',Session::get('valid_product')))
	  <a href="#" onClick="@if (Auth::user()->check())window.open('{{route('gdx')}}', 'casino11', 'width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif">
		<img src="{{url()}}/369bet/img/casino7.png" width="250" height="302">
	  </a>
  @endif  
  @if(in_array('IBCX',Session::get('valid_product')))
	  <a href="#" onClick="@if (Auth::user()->check())window.open('{{route('ibc_casino')}}', 'casino11', 'width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif">
		<img src="{{url()}}/369bet/img/casino8.png" width="250" height="302">
	  </a>
  @endif
  

  <div class="clr"></div>
</div>

<div class="bnr08">
<span>เกี่ยวกับ 369bet.com</span>

เราภูมิใจนำเสนอรูปแบบความบันเทิงในการเล่นคาสิโนที่พิเศษที่สุด. 369Bet มอบประสบการณ์การเล่นเกมส์แบบใหม่ที่คุณไม่เคยคิดฝัน. ไม่ว่าจะ หน้ากีฬา คาสิโนสด เกมส์สล๊อต หวย เรามีให้บริการคุณทั้งหมด! เราการันตีว่าคุณจะได้รับความรู้สึกตื่นเต้นในการเล่นคาสิโนด้วยสภาพแวดล้อมที่ความปลอดภัยสูงสุด โดยคุณไม่จำเป็นจะต้องกังวลใดๆทั้งสิ้น.

</div>

</div>
	  
@stop