@extends('369bet/master')

@section('title', 'Contact Us')

@section('content')
<div class="midSectCont">
<br>
<div class="midSect">

<div style="width:1036px; margin:auto; padding-top:15px;">
    <div style=" margin:auto; width:950px; border:solid 1px #C3C2C2; padding:20px;  ">
      <div align="center" style="font-family:Tahoma; font-size:28px; color:#0066CC; font-weight:bold; padding-bottom:10px; border-bottom:3px solid #0066cc">เกี่ยวกับเรา</div>
      
      <br />
      <p><strong><strong><font color="#333333" size="3" face="Tahoma">ยินดีต้อนรับสู่ 369bet (International Betting Group)</font></strong></strong><font color="#333333" size="3" face="Tahoma"><strong><br />
      </strong></font></p>
      <p><font color="#333333" size="3" face="Tahoma"><strong>เรา</strong> เป็นผู้ดำเนินการธุรกิจเดิมพันออนไลน์ ภายใต้การควบคุมของ International Betting Group (IBG) ซึ่งเป็นบริษัทที่จดทะเบียนโดยถูกต้องตามกฎหมายของประเทศกัมพูชา เราเป็นสื่อกลางให้ท่านได้ร่วมสนุกกับกีฬาออนไลน์มากมายรวมถึงคาสิโน และหวยออนไลน์ผ่านทางเว็บไซด์ชั้นนำ ที่มีการอัพเดทข้อมูลตลอด 24 ชั่วโมง  <br />
      </font></p>
      <p><font color="#333333" size="3" face="Tahoma"><strong>วันนี้</strong> 369bet ตัวแทนเว็บเดิมพันรูปแบบใหม่ ซึ่งมีความทันสมัยและล้ำหน้ากว่าใคร พร้อมเป็นทางเลือกหนึ่งให้กับทุกท่าน ที่รักความเป็นเลิศทางด้านบริการและความรวดเร็ว เรายินดีส่งตรงความสะดวกสบายไปสู่คุณ แค่เพียงสัมผัสปลายนิ้ว ด้วยเว็บไซต์คุณภาพที่เราคัดสรรมาอย่างดี มีความเป็นสากล มาตรฐานระดับโลก พร้อมทีมงานชั้นนำที่คอยมอบบริการให้คุณ ท่านจึงสามารถเล่นคาสิโนออนไลน์ เพื่อผ่อนคลายในยามว่าง โดยไม่ต้องเสียเวลาเดินทางไกล ประกอบกับความสะดวกสบายที่ทำให้ท่านเสมือนอยู่ในสถานที่จริง รวมทั้งสามารถเลือกเดิมพันได้ในกีฬาทุกประเภท ตลอดทั้งเกมส์การแข่งขันทุกชนิด ไม่ว่าจะเป็นการแข่งขันทีมฟุตบอล  ดังระดับโลก บาสเกตบอล NBA , กอล์ฟ , สนุ๊กเกอร์ , ฮอกกี้ , วอลเล่ย์บอล , แบตมินตัน , เบสบอล หรือแม้แต่ Motor Sport , ม้าแข่ง หรือเกมส์อื่นๆ</font><font color="#333333" size="3" face="Tahoma"><br />
      </font></p>
      <p><font color="#333333" size="3" face="Tahoma"><strong>นอกจากนี้</strong> 369bet ยังมอบผลตอบแทนสูงสุดในรูปแบบของราคาต่อรายการ ที่มีความหลากหลายมากที่สุด หรือแม้กระทั่งในรูปแบบของ commission สูงสุด เท่าที่เคยมีในบรรดาเว็บเดิมพันในประเทศไทย การฝาก - ถอนเงิน พร้อมบริการให้คำแนะนำและการแก้ไขปัญหาตลอด การอัพยอดเครดิตที่เร็วสุดเพียง 15 นาที การโอนเงินเต็มจำนวน ไม่หักค่าธรรมเนียม โดยโอนเงินกลับเร็วที่สุดเพียง 20 นาที ซึ่งท่านจะได้รับเงินครบทุกจำนวนที่ทำรายการชนะตามจริงอย่างแน่นอน และเพื่อความเป็นหนึ่งในผู้ให้บริการ 369bet ยังเปิดให้โอกาสกับผู้ที่สนใจร่วมลงทุนธุรกิจกับเรา ด้วยบริการ Super  Agency สิทธิพิเศษเหนือใครที่คุณจะได้รับจากเราที่แรกที่เดียว   </font><font color="#333333" size="3" face="Tahoma"><br />
      </font></p>
      <p><font color="#333333" size="3" face="Tahoma">ด้วยความเป็นเลิศทางด้าน การบริการและเทคโนโลยีที่ก้าวหน้า ท่านจึงสามารถมั่นใจได้ว่าจะได้รับบริการและผลตอบแทนอันล้ำค่า ที่ดีที่สุดจากเราที่เดียว</font></p>
       
      <p><font color="#333333" size="3" face="Tahoma"><strong><br />
        รับประกันการโอนเงินเต็มจำนวน 100% เพราะเราเป็นสายตรงจาก International Betting Group (IBG)  <br /> 
        <br />
      </strong>ที่มีความมั่นคงทางการเงิน และมีทีมงานมืออาชีพดูแลท่านตลอด</font><font color="#333333" size="3" face="Tahoma"><br />
      </font></p>
      <p><font color="#333333" size="3" face="Tahoma">369bet มีทีมงานออนไลน์ที่จะช่วยคุณ ในเวลาทำการ 10.00 น. ถึง ตี 2 ตลอด 7 วัน ต่อสัปดาห์</font></p>
      <p><font color="#333333" size="3" face="Tahoma">เบอร์โทร. 088-369-9991 , 088-369-9992</font></p>
      <p><font size="3" face="Tahoma"><br />
      </font></p>
      <table border="0" cellpadding="1" cellspacing="1" width="100%">
        <tbody>
          <tr>
            <td><font size="3" face="Tahoma"><strong>ฝาก-ถอนได้ถึง 6 ธนาคาร</strong> <br />
              <br />
              1. ธนาคารกสิกรไทย<br />
              2. ธนาคารกรุงเทพ<br />
              3. ธนาคารไทยพาณิชย์<br />
              4. ธนาคารกรุงไทย</font></td>
          </tr>
        </tbody>
      </table>
      <font size="3" face="Tahoma">5. ธนาคารกรุงศรี<br />
      6. ธนาคารทีเอ็มบี</font>
      <div></div>
      <div></div>
      <font color="#333333" size="3" face="Tahoma"><strong><br />
    </strong></font></div>
  </div>
  
  <br />
  </div>
<div class="midSectInner">




</div>

</div>
</div>
@stop

@section('bottom_js')

@stop