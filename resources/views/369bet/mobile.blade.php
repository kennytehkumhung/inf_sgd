@extends('369bet/master')

@section('title', 'About Us')

<style>
     /* The Modal (background) */
.modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

/* Modal Content/Box */
.modal-content {
    background-color: #fefefe;
    margin: 15% auto; /* 15% from the top and centered */
    //padding: 20px;
    //border: 1px solid #888;
    width: 400px; /* Could be more or less, depending on screen size */
    //align-content: center;
}

/* The Close Button */
.close {
    color: #aaa;
    float: right;
    font-size: 28px;
    font-weight: bold;
}

.close:hover,
.close:focus {
    color: black;
    text-decoration: none;
    cursor: pointer;
}
.close2 {
    color: #aaa;
    float: right;
    font-size: 28px;
    font-weight: bold;
}

.close2:hover,
.close2:focus {
    color: black;
    text-decoration: none;
    cursor: pointer;
}
.close3 {
    color: #aaa;
    float: right;
    font-size: 28px;
    font-weight: bold;
}

.close3:hover,
.close3:focus {
    color: black;
    text-decoration: none;
    cursor: pointer;
}
.close4 {
    color: #aaa;
    float: right;
    font-size: 28px;
    font-weight: bold;
}

.close4:hover,
.close4:focus {
    color: black;
    text-decoration: none;
    cursor: pointer;
}
.close6 {
    color: #aaa;
    float: right;
    font-size: 28px;
    font-weight: bold;
}

.close6:hover,
.close6:focus {
    color: black;
    text-decoration: none;
    cursor: pointer;
}
</style>

@section('content')
<div class="midSectCont">
 <img src="{{url()}}/369bet/img/tab-mobile.jpg" width="1025" height="300">
<div class="bnr09 ht02" align="center">
    <button id="myBtn1" style="cursor: pointer">
        <img src="{{url()}}/369bet/img/mobile1.png" width="475">
    </button>
    <div id="myModal1" class="modal">
        <div class="modal-content" id="myModal1">
            <span class="close" id="myModal1">x</span>
                <img src="{{url()}}/369bet/img/playtech_2qrcode.png" width="370" height="370" alt=""/>
                <span style="color:black;font-size:30px;margin-right:95px; margin-left:95px;">(Android)</span>
        </div>
    </div>
    
    <button id="myBtn2" style="cursor: pointer">
        <img src="{{url()}}/369bet/img/mobile2.png" width="475">
    </button>
    <div id="myModal2" class="modal">
        <div class="modal-content" id="myModal2">
            <span class="close2" id="myModal2">x</span>
                <img src="{{url()}}/369bet/img/gameplay_qr.jpg" width="370" height="370" alt=""/>
                <span style="color:black;font-size:30px;margin-right:140px; margin-left:140px;">(Android)</span>
        </div>
    </div>
    
    <button id="myBtn3" style="cursor: pointer">
      <img src="{{url()}}/369bet/img/mobile3.png" width="475">
    </button>
    <div id="myModal3" class="modal">
        <div class="modal-content" id="myModal3">
            <span class="close3" id="myModal3">x</span>
                <img src="{{url()}}/369bet/img/betsoft_qr.jpg" width="370" height="370" alt=""/>
                <span style="color:black;font-size:30px;margin-right:140px; margin-left:140px;">(Android)</span>
        </div>
    </div>
    
    <button id="myBtn4" style="cursor: pointer">
      <img src="{{url()}}/369bet/img/mobile4.png" width="475">
    </button>
    <div id="myModal4" class="modal">
        <div class="modal-content" id="myModal4">
            <span class="close4" id="myModal4">x</span>
                <img src="{{url()}}/369bet/img/ag_qr.jpg" width="370" height="370" alt=""/>
                <span style="color:black;font-size:30px;margin-right:100px; margin-left:100px;">(Android/ios)</span>
        </div>
    </div>
    
    <button id="myBtn5" style="cursor: pointer">
      <img src="{{url()}}/369bet/img/mobile5.png" width="475">
    </button>
<!--    <div id="myModal5" class="modal">
        <div class="modal-content" id="myModal5">
            <span class="close5" id="myModal5">x</span>
                <img src="{{url()}}/369bet/img/ag_qr.jpg" width="370" height="370" alt=""/>
        </div>
    </div>-->
    
    <button id="myBtn6" style="cursor: pointer">
      <img src="{{url()}}/369bet/img/mobile6.png" width="475">
    </button>
    <div id="myModal6" class="modal">
        <div class="modal-content" id="myModal6">
            <span class="close6" id="myModal6">x</span>
                <img src="{{url()}}/369bet/img/allbet_qr.jpg" width="370" height="370" alt=""/>
                <span style="color:black;font-size:30px;margin-right:140px; margin-left:140px;">(Android)</span>
        </div>
    </div>
    
  <div class="clr"></div>
</div>

<div class="bnr08">
<span>เกี่ยวกับ 369bet.com</span>

เราภูมิใจนำเสนอรูปแบบความบันเทิงในการเล่นคาสิโนที่พิเศษที่สุด. 369Bet มอบประสบการณ์การเล่นเกมส์แบบใหม่ที่คุณไม่เคยคิดฝัน. ไม่ว่าจะ หน้ากีฬา คาสิโนสด เกมส์สล๊อต หวย เรามีให้บริการคุณทั้งหมด! เราการันตีว่าคุณจะได้รับความรู้สึกตื่นเต้นในการเล่นคาสิโนด้วยสภาพแวดล้อมที่ความปลอดภัยสูงสุด โดยคุณไม่จำเป็นจะต้องกังวลใดๆทั้งสิ้น.

</div>

</div>

<script>
    // Get the modal
var modal = document.getElementById('myModal1');
var modal2 = document.getElementById('myModal2');
var modal3 = document.getElementById('myModal3');
var modal4 = document.getElementById('myModal4');
var modal6 = document.getElementById('myModal6');
// Get the button that opens the modal
var btn = document.getElementById("myBtn1");
var btn2 = document.getElementById("myBtn2");
var btn3 = document.getElementById("myBtn3");
var btn4 = document.getElementById("myBtn4");
var btn6 = document.getElementById("myBtn6");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];
var span2 = document.getElementsByClassName("close2")[0];
var span3 = document.getElementsByClassName("close3")[0];
var span4 = document.getElementsByClassName("close4")[0];
var span6 = document.getElementsByClassName("close6")[0];
// When the user clicks on the button, open the modal
btn.onclick = function() {
    modal.style.display = "block";
}
btn2.onclick = function() {
    modal2.style.display = "block";
}
btn3.onclick = function() {
    modal3.style.display = "block";
}
btn4.onclick = function() {
    modal4.style.display = "block";
}
btn6.onclick = function() {
    modal6.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal.style.display = "none";
}
span2.onclick = function() {
    modal2.style.display = "none";
}
span3.onclick = function() {
    modal3.style.display = "none";
}
span4.onclick = function() {
    modal4.style.display = "none";
}
span6.onclick = function() {
    modal6.style.display = "none";
}
// When the user clicks anywhere outside of the modal, close it
//window.onclick = function(event) {
//    if (event.target == modal) {
//        modal.style.display = "none";
//    }
//}
</script>
@stop

@section('bottom_js')

@stop