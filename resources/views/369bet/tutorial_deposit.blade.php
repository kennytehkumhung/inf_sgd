@extends('369bet/master')

@section('title', 'Register Tutorial')

@section('content')

<div style="width:1050px; background: #e2e3e4; margin:auto; overflow:hidden; border:solid 1px #DADBDE;">
  <div align="center" style="width:1036px; margin:auto; padding:7px 0 0px 0;"></div>
  <div style="width:1036px; margin:auto; padding-top:15px;">
  
  	<div align="center" style="margin-bottom:-10px;">
  	  <table id="Table_01" width="992" height="85" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td><a href="{{route('deposit_tutorial')}}"><img src="{{url()}}/369bet/img/tutorial/howto2_01.png" width="250" height="85" /><a/></td>
          <td><a href="{{route('withdraw_tutorial')}}" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image7','','{{url()}}/369bet/img/tutorial/howto2_02.png',1)"><img src="{{url()}}/369bet/img/tutorial/howto_02.png" name="Image7" width="248" height="85" border="0" id="Image7" /></a></td>
          <td><a href="{{route('transfer_tutorial')}}" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image5','','{{url()}}/369bet/img/tutorial/howto2_02.png',1)"><img src="{{url()}}/369bet/img/tutorial/howto_02.png" name="Image5" width="248" height="85" border="0" id="Image5" /></a></td>
          <td><a href="{{route('register_tutorial')}}" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image6','','{{url()}}/369bet/img/tutorial/howto2_04.png',1)"><img src="{{url()}}/369bet/img/tutorial/howto_04.png" name="Image6" width="246" height="85" border="0" id="Image6" /></a></td>
        </tr>
      </table>
  	</div>
    <div style=" margin:auto; width:950px; border:solid 1px #adadad; padding:20px;  ">
      <div align="center" style="font-family:Tahoma; font-size:28px; color:#0066CC; font-weight:bold; padding-bottom:10px; border-bottom:3px solid #0066cc">วิธีการฝากเงิน</div>
      <br /><br /><div align="center" style="width:900px; margin:auto; height:478px; background:url({{url()}}/369bet/img/tutorial/bg-howto.png);">
      	<div style="width:540px; height:306px; float:right; margin-right:30px; margin-top:24px;"><iframe width="540" height="306" src="https://www.youtube.com/embed/N42XYJNT0Og" frameborder="0" allowfullscreen></iframe></div>
      </div>
      <br />
      <h4><strong><font color="#333333" size="3" face="Tahoma">ฝากถอนขั้นต่ำ 1,000 บาท / อัพยอดภายใน 15 นาที / 1 วัน ฝากกี่ครั้งก็ได้</font></strong></h4>
      <p><font color="#333333" size="3" face="Tahoma"><strong>วิธีการฝากเงินผ่านธนาคารชั้นนำภายในประเทศ</strong></font></p>
      <ul>
        <li><font color="#333333" size="3" face="Tahoma">สมาชิกที่เปิดบัญชีในประเทศไทยและใช้เงินสกุล THB ( ไทยบาท ) เท่านั้น</font></li>
        <li><font color="#333333" size="3" face="Tahoma">ติดต่อแผนกลูกค้าสัมพันธ์ Call Center เพื่อสอบถามรายละเอียดการโอนเงินเข้าบัญชีธนาคารของเรา</font></li>
        <li><font color="#333333" size="3" face="Tahoma">ทำการฝากเงินของท่าน เข้าสู่บัญชีธนาคารที่ได้สอบถาม <br />
          <strong>หลังจากโอนเงินเข้า บัญชีของทาง 369Bet แล้ว ให้ปฏิบัติตามนี้</strong><br />
          ขั้นตอนที่ 1. เข้าสู่ระบบ โดยการป้อนชื่อผู้ใช้ และ รหัสผ่าน <br />
          ขั้นตอนที่ 2. คลิกที่ปุ่มฝากเงิน <br />
          ขั้นตอนที่ 3. ในช่องที่จัดให้เลือกวิธี และ ธนาคารที่ต้องการของคุณ ตามด้วยการป้อนจำนวนเงิน วันที่ เวลา และคลิกยืนยัน ( โปรดแนบสลิปการโอนเงินธนาคารของคุณหากมี ) <br />
          <br />
          </font>
          <div>
            <table width="905" cellspacing="1" bgcolor="#666666">
              <thead>
                <tr>
                  <td rowspan="2" bgcolor="#FFFFFF"><div align="center"><font color="#333333" size="3" face="Tahoma"><br />
                        <strong>ตัวเลือกธนาคาร</strong></font></div></td>
                  <td height="36" colspan="2" bgcolor="#FFFFFF"><div align="center"><font color="#333333" size="3" face="Tahoma"><strong>จำกัด การทำธุรกรรม (THB)</strong></font></div></td>
                  <td rowspan="2" bgcolor="#FFFFFF"><div align="center"><font color="#333333" size="3" face="Tahoma"><br />
                        <strong>ระยะเวลาการทำธุรกรรม</strong></font></div></td>
                </tr>
                <tr>
                  <td height="40" bgcolor="#FFFFFF"><div align="center"><font color="#333333" size="3" face="Tahoma"><strong>ขั้นต่ำ</strong></font></div></td>
                  <td bgcolor="#FFFFFF"><div align="center"><font color="#333333" size="3" face="Tahoma"><strong>สูงสุด</strong></font></div></td>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td height="33" colspan="4" bgcolor="#0066CC"><div align="center"><font color="#FFFFFF" size="3" face="Tahoma">ฝาก</font></div></td>
                </tr>
                <tr>
                  <td height="35" bgcolor="#FFFFFF"><div align="center"><font color="#333333" size="3" face="Tahoma">เอทีเอ็ม/ซีดีเอ็ม</font></div></td>
                  <td rowspan="3" bgcolor="#FFFFFF"><div align="center"><font color="#333333" size="3" face="Tahoma">
                      
                    1,000<br /></font></div></td>
                  <td rowspan="3" bgcolor="#FFFFFF"><div align="center"><font color="#333333" size="3" face="Tahoma">
                      
                    300,000<br /></font></div></td>
                  <td rowspan="3" bgcolor="#FFFFFF"><div align="center"><font color="#333333" size="3" face="Tahoma">
                      
                    15 นาที<br /></font></div></td>
                </tr>
                <tr>
                  <td height="35" bgcolor="#FFFFFF"><div align="center"><font color="#333333" size="3" face="Tahoma">โอนออนไลน์</font></div></td>
                </tr>
                <tr>
                  <td height="35" bgcolor="#FFFFFF"><div align="center"><font color="#333333" size="3" face="Tahoma">โอนเงินผ่านธนาคาร</font></div></td>
                </tr>
              </tbody>
            </table>
          </div>
          <font color="#333333" size="3" face="Tahoma"><br />
          ขั้นตอนที่ 4. โปรดติดต่อฝ่ายบิรการลูกค้า Call Center ของเราเมื่อทำการฝากเงินสำเร็จ <br />
          ขั้นตอนที่ 5. เมื่อเสร็จสมบูรณ์แล้ว คุณสามารถตรวจสอบบัญชีเงินฝากที่หน้าการทำธุรกรรมของคุณ</font></li>
      </ul>
      <font color="#333333" size="3" face="Tahoma">หมายเหตุ :      </font>
      <ul>
        <li><font color="#333333" size="3" face="Tahoma">จำนวนเงินในการฝากจำนวนมาก อาจจะใช้เวลานานกว่าปรกติในการได้รับการอัพยอดเงิน</font></li>
        <li><font color="#333333" size="3" face="Tahoma">โปรดเก็บหมายเลขธนาคาร หรือได้รับการอ้างอิงการทำธุรกรรมที่เป็นหลักฐาน ที่มีการทำธุรกรรม</font></li>
        <li><font color="#333333" size="3" face="Tahoma">หากมีปัญหา คุณสามารถเรียกร้องเงินฝากของคุณภายใน 3 วัน ก่อนที่จะถูกริบคืน</font></li>
      </ul>
      <font color="#333333" size="3" face="Tahoma"><br />
      <strong>วิธีการฝากเงินมีรูปแบบใดบ้าง ?</strong>
      <p>ขณะนี้เรามีการรับฝากเงินโดยผ่านทางการโอนเงินทางธนาคารโดยตรงเท่านั้น คุณสามารถฝากเงินโดยตรงจากบัญชีธนาคารส่วนบุคคลของคุณ ไปยังบัญชีธนาคารของ 369Bet รวมถึงการโอนเงินทางโทรศัพท์ , อินเตอร์เน็ตแบงค์กิ้ง , เคาน์เตอร์ฝากเงิน หรือ ตู้เอทีเอ็ม</p>
      <br />
      <strong>ธนาคารที่เราให้บริการ มีอะไรบ้าง</strong> </font>
      <ul>
        <li><font color="#333333" size="3" face="Tahoma">กสิกรไทย</font></li>
        <li><font color="#333333" size="3" face="Tahoma">กรุงไทย</font></li>
        <li><font color="#333333" size="3" face="Tahoma">ไทยพาณิชย์</font></li>
        <li><font color="#333333" size="3" face="Tahoma">กรุงศรี</font></li>
        <li><font color="#333333" size="3" face="Tahoma">กรุงเทพ</font></li>
        <li><font color="#333333" size="3" face="Tahoma">ทีเอ็มบี</font></li>
      </ul>
    </div>
  </div>
  
  <br />
  </div>
@stop

@section('bottom_js')

@stop