
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<script src="{{url()}}/369bet/resources/js/jquery-2.1.3.min.js"></script>
  <script>
  function forgotpassword_submit(){

	$.ajax({
		type: "POST",
		url: "{{route('resetpassword')}}",
		data: {
			_token: "{{ csrf_token() }}",
			username:		 $('#fg_username').val(),
			email:    		 $('#fg_email').val(),

		},
	}).done(function( json ) {
			 obj = JSON.parse(json);
			 var str = '';
			 $.each(obj, function(i, item) {
				str += item + '\n';
			})
			if( "{{Lang::get('COMMON.SUCESSFUL')}}\n" == str ){
				alert('{{Lang::get('COMMON.SUCESSFUL')}}');
			}else{
				alert(str);
			}
	
	});
}
  </script>
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
	<div style="width:550px; height:350px; margin:auto;">
	  <table width="529" border="0" align="center">
        <tr>
              <td height="103"><div align="center"><img src="{{url()}}/369bet/img/login/logo.png" width="261" height="70" /></div></td>
        </tr>
            <tr>
              <td><table width="400" border="0" align="center" cellspacing="0">
                <tr>
                  <td width="101"><div align="right"><img src="{{url()}}/369bet/img/login/brt-user.png" width="35" height="35" /></div></td>
                  <td width="295"><input type="text" name="textfield" id="fg_username" style="width:250px; height:30px;" /></td>
                </tr>
              </table></td>
            </tr> 
			<tr>
              <td><table width="400" border="0" align="center" cellspacing="0">
                <tr>
                  <td width="101"><div align="right"><img src="{{url()}}/369bet/img/login/brt-email.png" width="35" height="35" /></div></td>
                  <td width="295"><input type="text" name="textfield" id="fg_email" style="width:250px; height:30px;" /></td>
                </tr>
              </table></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td height="31" valign="bottom"><div align="center">
                <input href="javascript:void(0)" onClick="forgotpassword_submit()" type="button" value="ส่ง" style="background:#006cbf; width:180px; height:35px; font-size:14px; color:#FFFFFF; border:solid 1px #006cbf;" />
              </div></td>
        </tr>
            <tr>
              <td> &nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            </tr>
            <tr>
              <td height="32" valign="bottom"><font color="#666666" size="3" face="Tahoma">* ถ้าคุณไม่ได้รับอีเมลรีเซ็ตรหัสผ่านของคุณภายใน 10 นาที โปรดติดต่อ เราทันที. <br />
              * ในการรีเซ็ตรหัสผ่านของคุณโปรดกรอกข้อมูลในช่องที่จำเป็นดังกล่าวข้างต้น. <br />
              กรุณาอย่าลังเลที่จะติดต่อเราสำหรับข้อมูลเพิ่มเติมใด ๆ บริการลูกค้าสอบถาม.</font></td>
        </tr>
            <tr>
              <td>&nbsp;</td>
        </tr>
          </table>
	</div>
</body>
</html>
