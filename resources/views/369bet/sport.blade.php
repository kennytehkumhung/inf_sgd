@extends('369bet/master')

@section('title', 'Sportsbook, Live Casino, Slot Games and High 4D Payout')

@section('banner')
<div class="sliderCont">
	<div class="sliderContInner tp04">
		<img width="1024" height="300" src="{{url()}}/369bet/img/tab-sport.jpg">
	</div>
</div>
@stop

@section('content')


<div class="midSectCont">

	<div class="midIcon1">
	   <a href="{{route('wft')}}"><img src="{{url()}}/369bet/img/sport1.png" width="480" height="302"></a>
	  </div>
	<div class="midIcon2">
	  <a href="{{route('ibc')}}"><img src="{{url()}}/369bet/img/sport2.png" width="480" height="302"></a>
	</div>
	<div class="clr"></div>

	<div class="midIcon1 tp05">
	  <a href="{{route('tbs')}}"><img src="{{url()}}/369bet/img/sport3.png" width="480" height="302"></a>
	</div>
	<div class="midIcon2 tp05">
	  <a href="{{route('m8b')}}"><img src="{{url()}}/369bet/img/sport4.png" width="480" height="302"></a>
	</div>
	<div class="clr"></div>
        
        <div class="midIcon1 tp05">
	  <a href="#"><img src="{{url()}}/369bet/img/sport6.png" width="480" height="302"></a>
	</div>
	<div class="midIcon2 tp05">
	  <a href="{{route('sbof')}}"><img src="{{url()}}/369bet/img/sport5.png" width="480" height="302"></a>
	</div>
	<div class="clr"></div>

	<div class="bnr08">
	<span>เกี่ยวกับ 369bet.com</span>

	เราภูมิใจนำเสนอรูปแบบความบันเทิงในการเล่นคาสิโนที่พิเศษที่สุด. 369Bet มอบประสบการณ์การเล่นเกมส์แบบใหม่ที่คุณไม่เคยคิดฝัน. ไม่ว่าจะ หน้ากีฬา คาสิโนสด เกมส์สล๊อต หวย เรามีให้บริการคุณทั้งหมด! เราการันตีว่าคุณจะได้รับความรู้สึกตื่นเต้นในการเล่นคาสิโนด้วยสภาพแวดล้อมที่ความปลอดภัยสูงสุด โดยคุณไม่จำเป็นจะต้องกังวลใดๆทั้งสิ้น.

	</div>

</div>
	  
@stop