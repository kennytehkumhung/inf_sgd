@extends('369bet/master')

@section('title', 'Profile')

@section('content')
<script>
	$(document).ready(function() { 
		getBalance(true);
	});
	
function checkPass()
{
    //Store the password field objects into variables ...
    var new_password = document.getElementById('new_password');
    var confirm_new_password = document.getElementById('confirm_new_password');
    //Store the Confimation Message Object ...
    var message = document.getElementById('confirmMessage');
    //Set the colors we will be using ...
    var goodColor = "#66cc66";
    var badColor = "#ff6666";
    //Compare the values in the password field 
    //and the confirmation field
    if(new_password.value == confirm_new_password.value){
        //The passwords match. 
        //Set the color to the good color and inform
        //the user that they have entered the correct password 
        confirm_new_password.style.backgroundColor = goodColor;
        message.style.color = goodColor;
        message.innerHTML = "Passwords Match!"
    }else{
        //The passwords do not match.
        //Set the color to the bad color and
        //notify the user.
        confirm_new_password.style.backgroundColor = badColor;
        message.style.color = badColor;
        message.innerHTML = "Passwords Do Not Match!"
    }
} 

 function update_password(){
	$.ajax({
		type: "POST",
		url: "{{action('User\MemberController@ChangePassword')}}",
		data: {
			_token: "{{ csrf_token() }}",
			current_password:		$('#current_password').val(),
			new_password:    		$('#new_password').val(),
			confirm_new_password:   $('#confirm_new_password').val()

		},
	}).done(function( json ) {
		
		obj = JSON.parse(json);
			 var str = '';
			 $.each(obj, function(i, item) {
				str += item + '\n';
			})
			
			alert(str);
					
	});
}
</script>

<div class="midSectCont">
	<br>
	
	@include('369bet/trans_top' )

	<!--ACCOUNT MANAGAMENT MENU-->
		 <div class="inlineAccMenu">
     <ul>
			<li><a href="{{route('update-profile')}}">{{Lang::get('public.AccountProfile')}}</a></li>
		  <li><a href="{{route('update-password')}}">{{Lang::get('public.AccountPassword')}}</a></li>
      </ul>
      </div>
      <!--ACCOUNT MANAGAMENT MENU-->
      <!--ACCOUNT TITLE-->
      <div class="title_bar">
      <span>{{Lang::get('public.AccountPassword')}}</span>
      </div>
      <!--ACCOUNT TITLE-->
       <!--ACCOUNT CONTENT-->
      <div class="acctContent">
      <span class="wallet_title"><i class="fa fa-pencil"></i>{{Lang::get('public.ChangePassword')}}</span>
      <div class="acctRow">
      <label>{{Lang::get('public.CurrentPassword')}} :</label><input type="password" id="current_password"  style="color:black;" />
      <div class="clr"></div>
      </div>
      <div class="acctRow">
      <label>{{Lang::get('public.NewPassword')}} :</label><input type="password" id="new_password" style="color:black;" /> <font color="red">รหัสผ่านต้องประกอบด้วยอักขระอย่างน้อย 8 ตัว ประกอบด้วยตัวอักษร (ตัวอักษรพิมพ์ใหญ่ หรือตัวพิมพ์เล็ก) และ ตัวเลข</font>
      <div class="clr"></div>
      </div>
      <div class="acctRow">
      <label>{{Lang::get('public.ConfirmNewPassword')}} :</label><input  type="password" id="confirm_new_password" onkeyup="checkPass(); return false;" style="color:black;" />
      <div class="clr"></div>
      </div>
      <div class="submitAcct">
      <a href="javascript:void(0)" onclick="update_password()">บันทึก</a>
      </div>
      <div class="clr"></div>
      </div>
		  <!--ACCOUNT CONTENT-->

	<div class="clr"></div>

</div>
  
@stop