<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
<link href="" rel="icon" type="image/icon" />
<link href="" rel="shortcut icon" type="image/icon" />
<title>Online Betting From 369bet – @yield('title')</title>
<!--[if IE 7]>
	<link rel="stylesheet" type="text/css" href="{{url()}}/369bet/resources/css/iefix.css" />
<![endif]-->
<!--CSS -->


<link href="{{url()}}/369bet/resources/css/style.css" rel="stylesheet" type="text/css" />
<link href="{{url()}}/369bet/resources/css/jquery.bxslider.css" rel="stylesheet" type="text/css" />
<link href="{{url()}}/369bet/resources/css/timetabs.css" rel="stylesheet" type="text/css" />
<link href="{{url()}}/369bet/resources/css/leanmodal.css" rel="stylesheet" type="text/css" />
<link href="{{url()}}/369bet/resources/css/acct_management.css" rel="stylesheet" type="text/css" />
<link href="{{url()}}/369bet/resources/css/font-awesome.css" rel="stylesheet" type="text/css" />
<link href="{{url()}}/369bet/resources/css/jquery.dropdown.css" rel="stylesheet" type="text/css" />
<link href="{{url()}}/369bet/resources/js/jquery.colorbox/colorbox.css" rel="stylesheet" type="text/css" />

<link href="{{url()}}/369bet/resources/css/ddmenu.css" rel="stylesheet" type="text/css" />
<!--SCRIPT-->
<script src="{{url()}}/369bet/resources/js/jquery-2.1.3.min.js"></script>
<script src="{{url()}}/369bet/resources/js/jquery.bxslider.min.js"></script>
<script src="{{url()}}/369bet/resources/js/jquery.timetabs.min.js"></script>
<script src="{{url()}}/369bet/resources/js/jquery.leanModal.min.js"></script>
<script src="{{url()}}/369bet/resources/js/jquery.dropdown.min.js"></script>
<script src="{{url()}}/369bet/resources/js/jquery.colorbox/jquery.colorbox-min.js"></script>

<script src="{{url()}}/369bet/resources/js/ddmenu.js" type="text/javascript"></script>
    
<script type="text/javascript">
jQuery(document).ready(function(){
	
	jQuery('dl#tabs4').addClass('enabled').timetabs({
		defaultIndex: 0,
		interval: 7000,
		continueOnMouseLeave: true,
		animated: 'fade',
		animationSpeed: 500
	});

	// animation preview
	jQuery('input[name=animation]').click(function() {
		$this = jQuery(this);
		jQuery.fn.timetabs.switchanimation($this.val());
	});
	
	$(".iframe").colorbox({iframe:true, width:"650px", height:"450px"});
	
	 $("#masklayer").click(function () {
            $(".popup_img").fadeOut(100);
            $("#masklayer").fadeOut(100);
       });
		
});
function enterpressalert(e, textarea){

	var code = (e.keyCode ? e.keyCode : e.which);
	 if(code == 13) { //Enter keycode
	   login();
	 }
}
function enterpressalert2(e, textarea){

	var code = (e.keyCode ? e.keyCode : e.which);
	 if(code == 13) { //Enter keycode
	   login2();
	 }
}
function login()
{
	$.ajax({
		type: "POST",
		url: "{{route('login')}}",
		data: {
			_token: "{{ csrf_token() }}",
			username: $('#username').val(),
			password: $('#password').val(),
			code:     $('#code').val(),
		},
	}).done(function( json ) {
			 obj = JSON.parse(json);
			 var str = '';
			 $.each(obj, function(i, item) {
				str += item + '\n';
			})
			if( "{{Lang::get('COMMON.SUCESSFUL')}}\n" == str ){
				location.reload(); 
			}else{
				alert(str);
			}
			
	});
}

function login2()
{
	$.ajax({
		type: "POST",
		url: "{{route('login')}}",
		data: {
			_token: "{{ csrf_token() }}",
			username: $('#username2').val(),
			password: $('#password2').val(),
			code:     $('#code2').val(),
		},
	}).done(function( json ) {
			 obj = JSON.parse(json);
			 var str = '';
			 $.each(obj, function(i, item) {
				str += item + '\n';
			})
			if( "{{Lang::get('COMMON.SUCESSFUL')}}\n" == str ){
				location.reload(); 
			}else{
				alert(str);
			}
			
	});
}

function register_popup(){
	
	$('#register').show();
}	
function register_close(){
	
	$('#register').hide();
}	

function register_submit(){
	$.ajax({
		type: "POST",
		url: "{{route('register_process')}}?"+$('#register_form').serialize(),
	}).done(function( json ) {
			 $('.acctTextRegister').html('');
			 obj = JSON.parse(json);
			 var str = '';
			 $.each(obj, function(i, item) {
				if( '{{Lang::get('COMMON.SUCESSFUL')}}' == item ){
					alert('{{Lang::get('public.NewMember')}}');
					window.location.href = "{{route('homepage')}}";
				}else{
					$('.'+i+'_acctTextReg').html(item);
				}
			})
			
	});
}

@if (Auth::user()->check())
function update_mainwallet(){
	$.ajax({
		  type: "POST",
		  url: "{{route( 'mainwallet', [ '_token'=> csrf_token() ])}}",
		  beforeSend: function(balance){
		
		  },
		  success: function(balance){
			$('.main_wallet').html(parseFloat(Math.round(balance * 100) / 100).toFixed(2));
			total_balance += parseFloat(balance);
		  }
	 });
		
}
function getBalance(type){
	var total_balance = 0;
	$.when(
	
		 $.ajax({
					  type: "POST",
					  url: "{{route('mainwallet', [ '_token'=> csrf_token()])}}",
					  beforeSend: function(balance){
				
					  },
					  success: function(balance){
						$('.main_wallet').html(parseFloat(Math.round(balance * 100) / 100).toFixed(2));
						total_balance += parseFloat(balance);
					  }
		 }),
		@foreach( Session::get('products_obj') as $prdid => $object)
		
	
				 $.ajax({
					  type: "POST",
					  url: "{{route( 'getbalance', [ '_token'=> csrf_token(), 'product'=> $object->code ])}}&rd=<?php echo rand ( 10000, 99999 ); ?>",
					  beforeSend: function(balance){
						$('.{{$object->code}}_balance').html('<img style="" src="{{url()}}/front/img/ajax-loading.gif" width="20" height="10">');
					  },
					  success: function(balance){
						  if( balance != '{{Lang::get('Maintenance')}}')
						  {
							$('.{{$object->code}}_balance').html(parseFloat(Math.round(balance * 100) / 100).toFixed(2));
							total_balance += parseFloat(balance);
						  }
						  else
						  {
							  $('.{{$object->code}}_balance').html(balance);
						  }
					  }
				 })
			  @if($prdid != Session::get('last_prdid')) 
				,
			  @endif
		@endforeach

		
	).then(function() {
		
			$('#total_balance').html(parseFloat(Math.round(total_balance * 100) / 100).toFixed(2));
			$('.total_balance').html(parseFloat(Math.round(total_balance * 100) / 100).toFixed(2));

	});

}
@endif
</script>
<style type="text/css">
#annouce:hover{color:black;}
.popup_img { display: inline-block; position: absolute; z-index: 10001; margin: auto auto; top: -2000px; bottom:0; left:0; right: 0; }
</style>
</head>

<body>
<!--Header-->
<div class="header">
<div class="headerInner">
<div class="logo">
<img src="{{url()}}/369bet/img/logo.png">
</div>
<div class="headerRight">

	@if( !Auth::user()->check() )
		<div class="loginBtn"><a class="iframe" id="go" name="signup" href="{{route('login')}}" style="background:#006cbf; width:110px; height:30px; color:#FFFFFF; border:solid 1px #006cbf;">{{Lang::get('public.Login')}}</a></div>
		<div class="joinBtn"><a id="go" rel="leanModal" name="signup" href="{{route('register')}}">{{Lang::get('public.Register')}}</a></div>
		<div class="clr"></div>

		<div class="headerRightTable">
		<table width="auto" border="0" cellspacing="0" cellpadding="0">
		  <tr>
			<td valign="middle"> 
                            <div align="center" style="width:155px; margin-top:2px; margin-right:0px; background:url({{url()}}/369bet/img/bg-time.png) no-repeat top; float:right; padding:7px; color:#fff; font-family:Tahoma; font-size:12px;"><?php date_default_timezone_set("Asia/Bangkok");?>
            <?php echo substr(date("l"),0,3);?>,
			<?php echo date("M d Y");?>,
            <?php echo date(" H:i", time());?> </div></td>
			
			<td valign="middle"><a href="{{route( 'language', [ 'lang'=> 'th'])}}"><img src="{{url()}}/369bet/img/th-icon.png" alt=""/></a></td>
			<td valign="middle"><a href="{{route( 'language', [ 'lang'=> 'en'])}}"><img src="{{url()}}/369bet/img/en.png" alt=""/></a></td>
		  </tr>
		</table>
		</div>
	@else
		<div class="headerRightTable1">
			<table width="auto" border="0" cellspacing="0" cellpadding="0">
			  <tr>
				
				<td valign="middle"><div class="welc"><img width="30" height="29" style="margin-bottom:-5px;" src="{{url()}}/369bet/img/icon-1.png"> {{Lang::get('public.Welcome')}} {{Session::get('username')}}! &emsp; |</div></td>
				<td valign="middle"><div class="welc1"><a href="#" data-dropdown="#dropdown-0"><img width="30" height="29" style="margin-bottom:-5px;" src="{{url()}}/369bet/img/icon-2.png"> {{Lang::get('public.Profile')}}</a> &emsp; | </div>
				<div id="dropdown-0" class="dropdown dropdown-tip " >
					 <ul class="dropdown-menu" style="margin: 0px;min-width:150px;">
						<li><a href="{{route('update-profile')}}">{{Lang::get('public.MyAccount')}}</a></li>
						<li><a href="{{route('update-password')}}">{{Lang::get('public.ChangePassword')}}</a></li>
						<li><a href="{{route('memberbank')}}">{{Lang::get('COMMON.CTABBANKSETTING')}}</a></li>
						<!--<li><a href="{{route('update-profile')}}">Statement</a></li>-->
					 </ul>
					 </div></td>
				<td valign="middle"><div class="welc1"><a href="#" data-dropdown="#dropdown-1" onClick="getBalance()"><img width="30" height="29" style="margin-bottom:-5px;" src="{{url()}}/369bet/img/icon-3.png">{{Lang::get('public.Balance')}}</a> | </div>
				<div id="dropdown-1" class="dropdown dropdown-tip " >
					<div class="dropdown-menu wallcc">
			
				<div class="wallCont">
						<div class="wallIt head">{{Lang::get('public.Wallet')}}</div>
						<div class="wallIt head">{{Lang::get('public.Balance')}}</div>
						<div class="wallIt">{{Lang::get('public.MainWallet')}}</div>
						<div class="wallIt main_wallet">0.00</div>
					@foreach( Session::get('products_obj') as $prdid => $object)
						<div class="wallIt">{{$object->name}}</div>
						<div class="wallIt {{$object->code}}_balance" >0.00</div>
					@endforeach
						<div class="wallIt total" style="margin-top:4px;height:15px;"><strong id="">{{Lang::get('public.Total')}}</strong></span></div>
						<div class="wallIt total" style="margin-top:4px;height:15px;"><strong id="total_balance">00.00</strong></span></div>
				</div>
				
				<div class="clr"></div>
				
			
		
				 
				 </div>
					 
					 </div>
					 
					 
					 </td>
				<td valign="middle">
			   <div class="welc1"> <a href="#" data-dropdown="#dropdown-2"><img width="30" height="29" style="margin-bottom:-5px;" src="{{url()}}/369bet/img/icon-4.png">{{Lang::get('public.Fund')}}</a></div>
				<div id="dropdown-2" class="dropdown dropdown-tip " >
					 <ul class="dropdown-menu" style="margin: 0px;;min-width:150px;">
						<li><a href="{{route('deposit')}}">{{Lang::get('public.Deposit')}}</a></li>
						<li><a href="{{route('withdraw')}}">{{Lang::get('public.Withdrawal')}}</a></li>
						<li><a href="{{route('transaction')}}">{{Lang::get('public.TransactionHistory')}}</a></li>
						<li><a href="{{route('transfer')}}">{{Lang::get('public.Transfer')}}</a></li>
					 </ul>
					 </div></td>

			   
			  </tr>
			  
			</table>



			</div>
			<div class="loginBtn1"><a href="{{route('logout')}}">{{Lang::get('public.Logout')}}</a></div>
			<div class="headerRightTable">
			<table width="auto" border="0" cellspacing="0" cellpadding="0">
			  <tr>
				<td valign="middle">
			
					<div align="center" style="width:155px; margin-top:2px; margin-right:0px; background:url({{url()}}/369bet/img/bg-time.png) no-repeat top; float:right; padding:7px; color:#fff; font-family:Tahoma; font-size:12px;"><?php date_default_timezone_set("Asia/Bangkok");?>
            <?php echo substr(date("l"),0,3);?>,
			<?php echo date("M d Y");?>,
            <?php echo date(" H:i", time());?></div></td>
				
				<td valign="middle"><a href="{{route( 'language', [ 'lang'=> 'th'])}}"><img src="{{url()}}/369bet/img/th-icon.png" alt=""/></a></td>
			<td valign="middle"><a href="{{route( 'language', [ 'lang'=> 'en'])}}"><img src="{{url()}}/369bet/img/en.png" alt=""/></a></td>
			  </tr>
			</table>
			</div>
	@endif


</div>

<div class="lineLink">
  <img src="{{url()}}/369bet/img/brt-line.png" width="195" height="34">
</div>

<div class="fbLink">
  <a href="https://www.facebook.com/369bet-683307905143517/?hc_ref=SEARCH" target="_blank"><img src="{{url()}}/369bet/img/brt-fb.png" width="195" height="34"></a>
</div>

</div>
</div>
<!--Header-->
<!--Navigation-->
<!--
<nav id="ddmenu">
    
    <ul>
        <li>
            <span class="homeIcon"><a href="{{route('homepage')}}"><img src="{{url()}}/369bet/img/home.png"></a></span>
        </li>
        <li>
            <span class="mobIcon"><a href="{{route('mobile')}}"><img src="{{url()}}/369bet/img/mobile.png"></a></span>
        </li>
        <li><a class="top-heading" href="{{route('sport')}}">{{Lang::get('public.Sports')}}</a>
            <div class="dropdown offset300">
                <div class="dd-inner">
                    <ul class="column mayHide">
                        <li><br /><a href="#" onClick="@if (Auth::user()->check())window.open('{{route('w88', [ 'type' => 'casino' , 'category' => 'live' ] )}}', 'casino12', 'width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif"><img src="{{url()}}/369bet/img/tabmenu/sport1.png" /></a></li>
                    </ul>
                    <ul class="column mayHide">
                        <li><br /><a href="#" onClick="@if (Auth::user()->check())window.open('{{route('alb')}}', 'casino11', 'width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif"><img src="{{url()}}/369bet/img/tabmenu/sport2.png" /></a></li>
                    </ul>
                    <ul class="column mayHide">
                        <li><br /><a onClick="@if (!Auth::user()->check())alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif" href="@if (Auth::user()->check()){{route('mxb',[ 'type' => 'casino' , 'category' => 'baccarat' ])}}@endif"><img src="{{url()}}/369bet/img/tabmenu/sport3.png" /></a></li>
                    </ul>
                    <ul class="column mayHide">
                        <li><br /><a href="#" onClick="@if (Auth::user()->check())window.open('{{route('gdx')}}', 'casino11', 'width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif"><img src="{{url()}}/369bet/img/tabmenu/sport4.png" /></a></li>
                    </ul>
                    <ul class="column mayHide">
                        <li><br /><a href="#" onClick="@if (Auth::user()->check())window.open('{{route('ibc_casino')}}', 'casino11', 'width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif"><img src="{{url()}}/369bet/img/tabmenu/sport5.png" /></a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li><a class="top-heading" href="{{route('livecasino')}}">{{Lang::get('public.LiveCasino')}}</a>
            <div class="dropdown offset300">
                <div class="dd-inner">
                    <ul class="column mayHide">
                        <li><br /><a href="#" onClick="@if (Auth::user()->check())window.open('{{route('ct8')}}', 'casino11', 'width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif"><img src="{{url()}}/369bet/img/tabmenu/livecasino1.png" /></a></li>
                    </ul>
                    <ul class="column mayHide">
                        <li><br /><a href="#" onClick="@if (Auth::user()->check())window.open('{{route('agg')}}', 'casino11', 'width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif"><img src="{{url()}}/369bet/img/tabmenu/livecasino2.png" /></a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li><a class="top-heading" href="{{route('slot', [ 'type' => 'plt' ] )}}">{{Lang::get('public.Slot')}}</a>
            <div class="dropdown offset300">
                <div class="dd-inner">
                    <ul class="column mayHide">
                        <li><br /><a href="#" onClick="@if (Auth::user()->check())window.open('{{route('pltiframe')}}', 'casinoplt', 'width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif"><img src="{{url()}}/369bet/img/tabmenu/slot1.png" /></a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li><a class="top-heading" href="{{route('lotto')}}">{{Lang::get('public.Lottery')}}</a>
            <div class="dropdown offset300">
                <div class="dd-inner">
                    <ul class="column mayHide">
                        <li><br /><a href="#" onClick="@if (Auth::user()->check())window.open('{{route('clolotto')}}', 'cloloto', 'width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif" ><img src="{{url()}}/369bet/img/tabmenu/lottery1.png" /></a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="no-sub"><a class="top-heading" href="{{route('livefootball')}}">{{Lang::get('public.LiveSport')}}</a></li>
        <li class="no-sub"><a class="top-heading" href="{{route('promotion')}}">{{Lang::get('public.Promotion')}}</a></li>
        <li class="no-sub"><a class="top-heading" href="{{route('banking')}}">{{Lang::get('public.Bank')}}</a></li>
        <li class="no-sub"><a class="top-heading" href="{{route('contactus')}}">{{Lang::get('public.AboutUs')}}</a></li>
    </ul>
</nav>
-->

<div class="nav">
  <div class="nav-center">
      <ul style="margin-left:30px;">
                    <li class="a00"><a href="{{route('homepage')}}"><img style="margin-top:14px;" src="{{url()}}/369bet/img/index.gif"></a>
                    </li>
                    <li class="a00"><a href="{{route('mobile')}}"><img style="margin-top:13px;" src="{{url()}}/369bet/img/shouji.gif"></a>
                    </li>
                    <li class="a00"><a href="{{route('sport')}}">{{Lang::get('public.Sports')}}</a>
                        <div class="nav-hover-3" id="spp">
                            <ul>
                                    <li class="nav-sportsbook">
                                        <a href="{{route('wft')}}">
                                            <div class="nav-sportsbook-1">
                                                <img src="{{url()}}/369bet/img/1-1.png">
                                            </div>
                                        </a>
                                    </li>
                                    <li class="nav-sportsbook">
                                        <a href="{{route('ibc')}}">
                                            <div class="nav-sportsbook-1">
                                                <img src="{{url()}}/369bet/img/2-2.png">
                                            </div>
                                        </a>
                                    </li>
                                    <li class="nav-sportsbook">
                                        <a href="{{route('tbs')}}">
                                            <div class="nav-sportsbook-1">
                                                <img src="{{url()}}/369bet/img/3-3.png">
                                            </div>
                                        </a>
                                    </li>
                                    <li class="nav-sportsbook">
                                        <a href="{{route('m8b')}}">
                                            <div class="nav-sportsbook-1">
                                                <img src="{{url()}}/369bet/img/4-4.png">
                                            </div>
                                        </a>
                                    </li>
                                    <li class="nav-sportsbook">
                                        <a href="{{route('sbof')}}">
                                            <div class="nav-sportsbook-1">
                                                <img src="{{url()}}/369bet/img/5-5.png">
                                            </div>
                                        </a>
                                    </li>
                                                            </ul>
                        </div>
                    </li>
                    
                    <li class="a00"><a href="{{route('livecasino')}}">{{Lang::get('public.LiveCasino')}}</a>
                        <div class="nav-hover-3" id="spp">
                            <ul>
                                    <li class="nav-sportsbook">
                                        <a href="#" onClick="@if (Auth::user()->check())window.open('{{route('pltbiframe')}}', 'casinoplt', 'width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif">
                                            <div class="nav-sportsbook-1">
                                                <img src="{{url()}}/369bet/img/c1-1.png">
                                            </div>
                                        </a>
                                    </li>
                                    <li class="nav-sportsbook">
                                        <a href="#" onClick="@if (Auth::user()->check())window.open('{{route('agg')}}', 'casino11', 'width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif">
                                            <div class="nav-sportsbook-1">
                                                <img src="{{url()}}/369bet/img/c2-2.png">
                                            </div>
                                        </a>
                                    </li>
                                    <li class="nav-sportsbook">
                                        <a href="#" onClick="@if (Auth::user()->check())window.open('{{route('ct8')}}', 'casino11', 'width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif">
                                            <div class="nav-sportsbook-1">
                                                <img src="{{url()}}/369bet/img/c3-3.png">
                                            </div>
                                        </a>
                                    </li>
                                    <li class="nav-sportsbook">
                                        <a href="#" onClick="@if (Auth::user()->check())window.open('{{route('w88', [ 'type' => 'casino' , 'category' => 'live' ] )}}', 'casino12', 'width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif">
                                            <div class="nav-sportsbook-1">
                                                <img src="{{url()}}/369bet/img/c4-4.png">
                                            </div>
                                        </a>
                                    </li>
                                    <li class="nav-sportsbook">
                                        <a href="#" onClick="@if (Auth::user()->check())window.open('{{route('alb')}}', 'casino11', 'width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif">
                                            <div class="nav-sportsbook-1">
                                                <img src="{{url()}}/369bet/img/c5-5.png">
                                            </div>
                                        </a>
                                    </li>
                                    <li class="nav-sportsbook">
                                        <a onClick="@if (!Auth::user()->check())alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif" href="@if (Auth::user()->check()){{route('mxb',[ 'type' => 'casino' , 'category' => 'baccarat' ])}}@endif">
                                            <div class="nav-sportsbook-1">
                                                <img src="{{url()}}/369bet/img/c6-6.png">
                                            </div>
                                        </a>
                                    </li>
                                    <li class="nav-sportsbook">
                                        <a href="#" onClick="@if (Auth::user()->check())window.open('{{route('gdx')}}', 'casino11', 'width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif">
                                            <div class="nav-sportsbook-1">
                                                <img src="{{url()}}/369bet/img/c7-7.png">
                                            </div>
                                        </a>
                                    </li>
                                    <li class="nav-sportsbook">
                                        <a href="#" onClick="@if (Auth::user()->check())window.open('{{route('ibc_casino')}}', 'casino11', 'width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif">
                                            <div class="nav-sportsbook-1">
                                                <img src="{{url()}}/369bet/img/c8-8.png">
                                            </div>
                                        </a>
                                    </li>
                                                            </ul>
                        </div>
                    </li>
                    
                    <li class="a00"><a href="{{route('slot', [ 'type' => 'pltb' ] )}}">{{Lang::get('public.Slot')}}</a>
                        <div class="nav-hover-3" id="spp">
                            <ul>
                                    <li class="nav-sportsbook">
                                        <a href="{{route('slot', [ 'type' => 'pltb' ] )}}">
                                            <div class="nav-sportsbook-1">
                                                <img src="{{url()}}/369bet/img/g1-1.png">
                                            </div>
                                        </a>
                                    </li>
                                    <li class="nav-sportsbook">
                                        <a href="{{route('slot' , [ 'type' => 'w88' ] )}}">
                                            <div class="nav-sportsbook-1">
                                                <img src="{{url()}}/369bet/img/g2-2.png">
                                            </div>
                                        </a>
                                    </li>
                                    <li class="nav-sportsbook">
                                        <a href="javascript:void(0)" onClick="@if (Auth::user()->check())window.open('{{Session::get('mxb_slot')}}', 'casino11', 'width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif">
                                            <div class="nav-sportsbook-1">
                                                <img src="{{url()}}/369bet/img/g3-3.png">
                                            </div>
                                        </a>
                                    </li>
                                    
                                    
                             </ul>
                        </div>
                    </li>
                    
                    
                    <li class="a00"><a href="{{route('lotto')}}">{{Lang::get('public.Lottery')}}</a>

                        <div class="nav-hover-3" id="lottos">
                            <ul>
                                   <li class="e-game">
                                        <a href="{{route('lotto')}}">
                                            <div>
                                                <img src="{{url()}}/369bet/img/h1-1.png">
                                            </div>
                                        </a>
                                    </li>
                                
                            </ul>
                        </div>
                    </li>
                   
                    <!--<li class="a00"><a href="/index/about/35" target="_blank">OTHER</a></li>-->
                     <li class="join a00 end"><a href="{{route('live-football')}}">{{Lang::get('public.LiveSport')}}<img src="{{url()}}/369bet/img/hot_icon.gif" width="30" height="20"></a>                    </li>
                     
                    <li class="a00"><a href="{{route('promotion')}}">{{Lang::get('public.Promotion')}}</a></li>
                    <li class="a00"><a href="{{route('banking')}}">{{Lang::get('public.Bank')}}</a></li>
                    <li class="a00"><a href="{{route('contactus')}}">{{Lang::get('public.AboutUs')}}</a></li>
                </ul>
            </div>
        </div>
<!--<div class="navCont">
<div class="navContInner">

<ul>
<li class="homeIcon" style=" float:left;margin-left: 27px;width:55px;">
<a href="{{route('homepage')}}"><img src="{{url()}}/369bet/img/home.png" width="25" height="24"></a></li>
<li class="mobIcon" style=" float:left;margin-left: 17px; width:55px;"><a href="{{route('mobile')}}"><img src="{{url()}}/369bet/img/mobile.png" width="25" height="24"></a></li>

<li style="float:left;margin-left: 37px;width:58px;"><a href="{{route('sport')}}">{{Lang::get('public.Sports')}}</a></li>
<li style="float:left;margin-left: 31px;width:110px;"><a href="{{route('livecasino')}}">{{Lang::get('public.LiveCasino')}}</a></li>
<li style="float:left;margin-left: 1px;width:92px;"><a href="{{route('slot', [ 'type' => 'plt' ] )}}">{{Lang::get('public.Slot')}}</a></li>
<li style="float:left;margin-left: 1px;width:92px;"><a href="{{route('lotto')}}">{{Lang::get('public.Lottery')}}</a></li>
<li style="float:left;margin-left: 1px;width:132px;"><a href="{{route('livefootball')}}">{{Lang::get('public.LiveSport')}}</a><img class="hot" src="{{url()}}/369bet/img/hot_icon.gif" width="30"></li>
<li style="float:left;margin-left: 1px;width:132px;"><a href="{{route('promotion')}}">{{Lang::get('public.Promotion')}}</a></li>
<li style="float:left;margin-left: 1px;width:112px;"><a href="{{route('banking')}}">{{Lang::get('public.Bank')}}</a></li>
<li style="float:left;margin-left: 1px;width:112px;"><a href="{{route('contactus')}}">{{Lang::get('public.AboutUs')}}</a></li>
</ul>
</div>
</div>-->

<!--Navigation-->
<!--Mid Sect-->
<div class="midSect" style="">
<div class="anmnt">
<div class="anmntInner">
<span style=""><img src="{{url()}}/369bet/img/vol.png" width="27" height="20"></span>
<marquee id="annouce" scrollamount="4" onmouseover="stop()" onmouseout="start()">{{App\Http\Controllers\User\AnnouncementController::index()}}</marquee>
</div>
</div>



@yield('banner')

<div class="midSectInner">


@yield('content')


</div>

</div>
<!--Foooter-->
<div class="footer">
<div class="footerInner">
<div class="footerLeft">
<ul>
<li class="nM"><a href="{{route('aboutus')}}">About 369bet</a></li>
<li><a href="{{route('banking')}}">{{Lang::get('public.Banking')}}</a></li>
<li><a href="#">{{Lang::get('public.ResponsibleGaming')}}</a></li>
<li><a href="#">{{Lang::get('public.FAQ')}}</a></li>
<li ><a href="#">{{Lang::get('public.TNC')}}</a></li>
<li class="nM1"><a href="http://affiliate.369thai.com/">พันธมิตร</a></li>
</ul>
</div>
<div class="footerRight">
<span>©2016 369bet. All rights reserved | 18+</span>
</div>


<div class="clr"></div>

<div class="footerLeft1">
<span>Partner</span>
<ul>
<li class="nM"><img src="{{url()}}/369bet/img/ag-footer.png" width="61" height="40"></li>
<li><img src="{{url()}}/369bet/img/mxb-footer.png" width="61" height="40"></li>
<li><img src="{{url()}}/369bet/img/allbet-footer.png" width="61" height="40"></li>
<li><img src="{{url()}}/369bet/img/bb-footer.png" width="61" height="40"></li>
<li><img src="{{url()}}/369bet/img/pt-footer.png" width="84" height="40"></li>
<li><img src="{{url()}}/369bet/img/gp-footer.png" width="84" height="40"></li>
</ul>
</div>
<div class="footerRight1">
<span>{{Lang::get('COMMON.PAYMENTMETHOD')}}</span>
<ul>
<li class="nM"><img src="{{url()}}/369bet/img/mbb-footer.png" width="84" height="40"></li>
<li><img src="{{url()}}/369bet/img/cimb-footer.png" width="84" height="40"></li>
<li><img src="{{url()}}/369bet/img/pbb-footer.png" width="84" height="40"></li>
<li><img src="{{url()}}/369bet/img/hlb-footer.png" width="84" height="40"></li>
</ul>
<div class="clr"></div>
</div>


<div class="clr"></div>

</div>

</div>
<!--Login Box-->
<div id="signup">
						<div id="signup-ct">
							<div id="signup-header">
								<h2><img src="{{url()}}/369bet/img/logo.png" width="320px"></h2>
								<p>If you encounter any issues while logging in, please contact our Customer Service for further assistance. </p>
								<a class="modal_close" href="#"></a>
							</div>
							
							<form action="">
				 
							  <div class="txt-fld">
								<label for="">{{Lang::get('public.Username')}}</label>
								<input id="username" class="good_input" name="" type="text"  />

							  </div>
							  <div class="txt-fld">
								<label for="">{{Lang::get('public.Password')}}</label>
								<input id="password" name="" type="password"  />
							  </div>
							  <div class="txt-fld">
								<label for="">{{Lang::get('public.Code')}}</label><img src="{{route('captcha', ['type' => 'login_captcha'])}}">
								<input style="float: left; width: 100px !important; margin-left: 10px;" id="code" name="" type="text" onKeyPress="enterpressalert(event, this)" />
								<div class="clr"></div>
							  </div>
							  <div class="txt-fld">
								<a href="#">{{Lang::get('public.ForgotPassword')}}</a>
								<div class="clr"></div>
							  </div>
							  <div class="login01Btn" onClick="login()" >
									<a href="#">{{Lang::get('public.Submit')}}</a>
							 </div>
							 </form>
						</div>
		</div>
<!--Login Box-->
<!--Register Box-->
<div id="register">
			<div id="register-ct">
				<div id="register-header">
					<h2>{{Lang::get('public.RegisterNow')}}</h2>
					<a class="modal_close" href="#"></a>
				</div>
				
				<form id="register_form">
     
				 <div class="txt-fld">
				    <label for="">{{Lang::get('public.EmailAddress')}}</label>
				    <input id="" name="email" type="text" />
					<br>
					<span class="email_acctTextReg acctTextRegister" style="color: Red ! important; font-size: small; display: inline;"></span>
				  </div>
				  <div class="txt-fld">
				    <label for="">{{Lang::get('public.ContactNo')}}.*</label>
				    <span style="float: left;color: #000;margin-top: 8px;margin-left: 30px;">+60 -</span><input style="float: left; width: 100px !important;" id="" name="mobile" type="text" />
					<br>
					<span class="mobile_acctTextReg acctTextRegister" style="color: Red ! important; font-size: small; display: inline;"></span>
                    <div class="clr"></div>
				  </div>
                  <div class="txt-fld">
				    <label for="">{{Lang::get('public.Currency')}}*</label>
				    <span style="float: left;color: #000;margin-top: 8px;margin-left: 30px;">+IDR</span>
                    <div class="clr"></div>
				  </div>
                  <div class="txt-fld">
				    <label for="">{{Lang::get('public.FullName')}}*</label>
				    <input id="" name="fullname" type="text" />
 	

*Name must be matched with withdrawal bank account 
						<br>
					<span class="fullname_acctTextReg acctTextRegister" style="color: Red ! important; font-size: small; display: inline;"></span>
				  </div>
                  <!--
                 <div class="txt-fld">
				    <label for="">Gender</label>
				    
                    <select class="good_input"></select>
 	
				  </div>
                  -->
				  
                  <div class="txt-fld">
					 <label>{{Lang::get('public.DOB')}} :</label>
					  <select style="margin-right: 8px;" name="day">
						<option value="01">01</option>
						<option value="02">02</option>
						<option value="03">03</option>
						<option value="04">04</option>
						<option value="05">05</option>
						<option value="06">06</option>
						<option value="07">07</option>
						<option value="08">08</option>
						<option value="09">09</option>
						<option value="10">10</option>
						<option value="11">11</option>
						<option value="12">12</option>
						<option value="13">13</option>
						<option value="14">14</option>
						<option value="15">15</option>
						<option value="16">16</option>
						<option value="17">17</option>
						<option value="18">18</option>
						<option value="19">19</option>
						<option value="20">20</option>
						<option value="21">21</option>
						<option value="22">22</option>
						<option value="23">23</option>
						<option value="24">24</option>
						<option value="25">25</option>
						<option value="26">26</option>
						<option value="27">27</option>
						<option value="28">28</option>
						<option value="29">29</option>
						<option value="30">30</option>
						<option value="31">31</option>
					  </select>
					  <select style="margin-right: 8px;" name="month">
						<option value="01">01</option>
						<option value="02">02</option>
						<option value="03">03</option>
						<option value="04">04</option>
						<option value="05">05</option>
						<option value="06">06</option>
						<option value="07">07</option>
						<option value="08">08</option>
						<option value="09">09</option>
						<option value="10">10</option>
						<option value="11">11</option>
						<option value="12">12</option>
					  </select>
					  <select name="year">
						<option value="1995">1995</option>
						<option value="1994">1994</option>
						<option value="1993">1993</option>
						<option value="1992">1992</option>
						<option value="1991">1991</option>
						<option value="1990">1990</option>
						<option value="1989">1989</option>
						<option value="1988">1988</option>
						<option value="1987">1987</option>
						<option value="1986">1986</option>
						<option value="1985">1985</option>
						<option value="1984">1984</option>
						<option value="1983">1983</option>
						<option value="1982">1982</option>
						<option value="1981">1981</option>
						<option value="1980">1980</option>
						<option value="1979">1979</option>
						<option value="1978">1978</option>
						<option value="1977">1977</option>
						<option value="1976">1976</option>
						<option value="1975">1975</option>
						<option value="1974">1974</option>
						<option value="1973">1973</option>
						<option value="1972">1972</option>
						<option value="1971">1971</option>
						<option value="1970">1970</option>
						<option value="1969">1969</option>
						<option value="1968">1968</option>
						<option value="1967">1967</option>
						<option value="1966">1966</option>
						<option value="1965">1965</option>
						<option value="1964">1964</option>
						<option value="1963">1963</option>
						<option value="1962">1962</option>
						<option value="1961">1961</option>
						<option value="1960">1960</option>
						
					  </select>
		  
					<br>
					<span class="dob_acctTextReg acctTextRegister" style="color: Red ! important; font-size: small; display: inline;"></span>
				  </div>
                  
                  <div class="txt-fld">
				    <label for="">{{Lang::get('public.Username')}}*</label>
				    <input id="" class="good_input" name="username" type="text" />
					<br>
					<span class="username_acctTextReg acctTextRegister" style="color: Red ! important; font-size: small; display: inline;"></span>
				  </div>
                  
                  <div class="txt-fld">
				    <label for="">{{Lang::get('public.Password')}}*</label>
				    <input id="" class="good_input" name="password" type="password" />
					<br>
					<span class="password_acctTextReg acctTextRegister" style="color: Red ! important; font-size: small; display: inline;"></span>
				  </div>
                  
                  <div class="txt-fld">
				    <label for="">{{Lang::get('public.ConfirmPassword')}}*</label>
				    <input id="" class="good_input" name="repeatpassword" type="password" />
							
					 <span style="font-size: 11px;">
					{{Lang::get('public.PasswordPolicyContent')}} </span>
					<span class="repeatpassword_acctTextReg acctTextRegister" style="color: Red ! important; font-size: small; display: inline;"></span>
				  </div>
				  <div class="txt-fld">
				    <label for="">Code</label><img style="float: left;margin-left:6px;"  src="{{route('captcha', ['type' => 'register_captcha'])}}" width="60" height="27" />
				    <input style="float: left; width: 100px !important;" id="" name="code" type="text" />
					<span class="code_acctTextReg acctTextRegister" style="color: Red ! important; font-size: small; display: inline;"></span>
                    <div class="clr"></div>*{{Lang::get('public.RequiredFields')}} 
				  </div>
                  
                  <div class="txt-fld">
				    <span style="float: right;font-size: 12px;"><input style="float: left;width: 15px;font-size: 12px;" type="checkbox">{{Lang::get('public.AtLeast18AndRead')}} </span>
				    
                    
                    <div class="clr"></div>
				  </div>
                

				  <div class="login02Btn">
						<a href="#" onClick="register_submit()"  >{{Lang::get('public.Submit')}}</a>
				  </div>

				 </form>
			</div>
		</div>
<!--Register Box-->

<div id="slideout">
    <img src="{{url()}}/369bet/img/slide/callCenter.png" alt="Feedback" />
    <div id="slideout_inner">
       
    </div>
</div>


<script type="text/javascript">
			$(function() {
    			$('a[rel*=leanModal]').leanModal({ top : 200, closeButton: ".modal_close" });		
			});
		</script>
<script>
function fixDiv() {
    var $div = $(".navCont");
    if ($(window).scrollTop() > $div.data("top")) { 
        $('.navCont').css({'position': 'fixed', 'top': '0', 'width': '100%'}); 
    }
    else {
        $('.navCont').css({'position': 'static', 'top': 'auto', 'width': '100%'});
    }
}

$(".navCont").data("top", $(".navCont").offset().top); // set original position on load
$(window).scroll(fixDiv);

</script>        
        
        

<script type="text/javascript">
$('.bxslider').bxSlider({
  auto: true,
  autoControls: true,
});
</script>

<!-- Start of LiveChat (www.livechatinc.com) code -->
<script type="text/javascript">
window.__lc = window.__lc || {};
window.__lc.license = 8474864;
(function() {
  var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
  lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
})();
</script>
<!-- End of LiveChat code -->
</body>
</html>
