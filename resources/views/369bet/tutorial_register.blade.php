@extends('369bet/master')

@section('title', 'Register Tutorial')

@section('content')

<div style="width:1050px; background: #e2e3e4; margin:auto; overflow:hidden; border:solid 1px #DADBDE;">
  <div align="center" style="width:1036px; margin:auto; padding:7px 0 0px 0;"></div>
  <div style="width:1036px; margin:auto; padding-top:15px;">
  
  	<div align="center" style="margin-bottom:-10px;">
  	  <table id="Table_01" width="992" height="85" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td><a href="{{route('deposit_tutorial')}}" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image4','','{{url()}}/369bet/img/tutorial/howto2_01.png',1)"><img src="{{url()}}/369bet/img/tutorial/howto_01.png" name="Image4" width="250" height="85" border="0" id="Image4" /></a></td>
          <td><a href="{{route('withdraw_tutorial')}}" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image5','','{{url()}}/369bet/img/tutorial/howto2_02.png',1)"><img src="{{url()}}/369bet/img/tutorial/howto_02.png" name="Image5" width="248" height="85" border="0" id="Image5" /></a></td>
          <td><a href="{{route('transfer_tutorial')}}" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image6','','{{url()}}/369bet/img/tutorial/howto2_03.png',1)"><img src="{{url()}}/369bet/img/tutorial/howto_03.png" name="Image6" width="248" height="85" border="0" id="Image6" /></a></td>
          <td><a href="{{route('register_tutorial')}}"><img src="{{url()}}/369bet/img/tutorial/howto2_04.png" width="246" height="85" /></a></td>
        </tr>
      </table>
  	</div>
    <div style=" margin:auto; width:950px; border:solid 1px #adadad; padding:20px;  ">
      <div align="center" style="font-family:Tahoma; font-size:28px; color:#0066CC; font-weight:bold; padding-bottom:10px; border-bottom:3px solid #0066cc">วิธีการสมัครสมาชิก</div>
      <br /><br /><div align="center" style="width:900px; margin:auto; height:478px; background:url({{url()}}/369bet/img/tutorial/bg-howto.png);">
      	<div style="width:540px; height:306px; float:right; margin-right:30px; margin-top:24px;"><iframe width="540" height="306" src="https://www.youtube.com/embed/PbSwupK-kCM" frameborder="0" allowfullscreen></iframe></div>
      </div>
      <br />
      <h4 align="center"><strong><font color="#333333" size="3" face="Tahoma"><br />
        <a href="{{route('register')}}"><input type="button" value="สมัครสมาชิก" style="background:#006cbf; width:110px; height:40px; font-size:14px; color:#FFFFFF; border:solid 1px #006cbf;" /></a>
      </font></strong></h4>
    </div>
  </div>
  
  <br />
  </div>
@stop

@section('bottom_js')

@stop