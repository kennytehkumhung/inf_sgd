@extends('369bet/master')

@section('title', 'Register Tutorial')

@section('banner')
<div class="sliderCont">
<div class="sliderContInner">
<ul class="bxslider">
      <li><img src="{{url()}}/369bet/img/tutorial/slide1.jpg"></li>
      <li><img src="{{url()}}/369bet/img/tutorial/slide1-2.jpg"></li>
</ul>
</div>
</div>
@stop


@section('content')
<style>
.style2 {
    color: #ff4e00;
    font-family: Tahoma;
    font-size: 20px;
    font-weight: bold;
}
.style3 {
    color: #ffffff;
    font-size: 13px;
    font-weight: 100;
}
.style4 {
    color: #ff6600;
    font-weight: bold;
}
.style9 {
    color: #ffffff;
}
.style11 {
    font-family: Tahoma;
    font-size: 12px;
}

.style12 {
    color: #666666;
    font-family: Tahoma;
    font-size: 14px;
}
.style13 {
    color: #333333;
    font-family: Tahoma;
    font-size: 16px;
}
.style14 {
    font-weight: bold;
}
.style15 {
    font-size: 14px;
}
.style16 {
}
.style22 {
    color: #000000;
}
.style24 {
    font-family: Tahoma;
    font-size: 14px;
    font-weight: bold;
}
.style25 {
    font-family: Tahoma;
    font-size: 14px;
}
.style26 {
    font-family: Tahoma;
}
.style28 {
    color: #000000;
    font-weight: bold;
}
.style30 {
    color: #ffffff;
    font-size: 14px;
    font-weight: bold;
}
</style>


<div style="width:1024px; background: #e2e3e4; margin:auto; overflow:hidden; border:solid 1px #DADBDE;margin-top:-50px;color: black;">
  <div style="width:1036px; margin:auto; padding-top:15px;"> <img src="{{url()}}/369bet/img/tutorial/slide2.jpg" />&nbsp;&nbsp; <img src="{{url()}}/369bet/img/tutorial/slide3.jpg" /><br />
  </div>
  <div style="width:1036px; margin:auto; padding:7px 0 0px 0;"> <img src="{{url()}}/369bet/img/tutorial/slide4.png" /> </div>
  <div style="width:1036px; margin:auto; padding:0px 0 7px 0;"> <img src="{{url()}}/369bet/img/tutorial/slide5.png" /> </div>
  <div style="width:1036px; margin:auto; padding:0px 0 7px 0;"> <img src="{{url()}}/369bet/img/tutorial/slide6-1.jpg" /> <span class="style1"><span class="style2"><br />
    <br />
    โบนัส เงินฝากรับโปรโมชั่น SBOBET By 369bet</span> <br />
    <span class="style3 style22"><br />
    <span class="style24">เงื่อนไขก่อนรับสิทธิ์</span></span><br />
    <br />
    <span class="style25">- ลูกค้า แจ้งทำรายการฝาก ช่วงเวลา 10.00 - 17.00 น. ของวันจันทร์ - อาทิตย์ <br />
    - ได้รับโบนัส วันละครั้งเท่านั้น <br />
    - โบนัสสูงสุดของ 3% คือ 300 บาท ต่อวัน / User <br />
    - โบนัสสูงสุดของ 5% คือ 500 บาท ต่อวัน / User <br />
    - ต้องมียอดเล่นหมุนเวียน Trun Over 8 เท่า</span><br />
    <br />
    <strong>ตัวอย่าง </strong><br />
    <br />
    <span class="style25">- ฝาก 1,000 บาทขึ้นไป ได้รับเพิ่มอีก 30 บาท ยอดเงินหมุนเวียน = ( 1,000 + 30 ) x 8 = 8,240 บาท <br />
    *** สรุป : ต้องมียอดเล่น 8,240 บาท ลูกค้าถึงจะทำการถอนเงินพร้อมโบนัส ออกได้ <br />
    <br />
    - การเดิมพันทั้งหมดจะเป็นโมฆะ ในกรณีที่มีการวางเดิมพันทั้ง 2 ฝั่ง ในคู่เดียวกัน <br />
    - กรณีที่สมาชิกใช้หลาย User โดยมีการเล่นทั้ง 2 ฝั่งสวนทางกัน ทางทีมงานจะตัดสิทธิทันทีแล้วทำการยึดเงินทั้งหมดของท่าน <br />
    - สงวนสิทธ์คุณลูกค้าเลือกโปรโมชั่นอย่างใดอย่างหนึ่งเท่านั้น <br />
    - 369bet ขอสงวนสิทธิ์ในการแก้ไขเงื่อนไข และข้อตกลงของโปรโมชั่นนี้ได้ทุกเมื่อโดยไม่ต้องแจ้งล่วงหน้า</span></span><span class="style25"><br />
    </span><br />
  </div>
  <div style="width:1036px; margin:auto; padding:0px 0 7px 0;"> <img src="{{url()}}/369bet/img/tutorial/slide6-2.jpg" /> <span class="style1"><span class="style2"><br />
    <br />
    โบนัส คอมมิชชั่นสูงสุด x10 SBOBET By 369bet</span> <span class="style26"><br />
    <br />
    <span class="style3"><span class="style4">ในกรณีที่ลูกค้าไม่ต้องการติด Turn Over ฝาก ถอน เมื่อไหร่ก็ได้ จะได้รับค่าคอมมิชั่นสูงสุด x10</span><br />
    <span class="style28"><br />
    <span class="style15">เงื่อนไขการรับสิทธ์ </span></span></span><br />
    <br />
    </span></span>
    <p class="style1 style26 style15">- ยอด Turn Over จะเริ่มนับ ตั้งแต่วันที่ 1 ของทุกเดือน ถึง วันสิ้นเดือน ของทุกเดือน</p>
    <p class="style1 style26 style15">- เฉพาะเว็บบอล SBOBET เท่านั้นในรอบ 1 เดือน ถ้าคุณลูกค้ามียอด Turn Over ตามที่ทางเราได้กำหนดไว้ ทางเว็บ 369bet มีโบนัสพิเศษให้ ตามนี้</p>
    <table width="568" border="0" align="center" cellspacing="1">
      <tr>
        <td width="281" height="35" bgcolor="#0368BA"><div align="center" class="style30"><span class="style26">ยอด Turn Over (บาท)</span></div></td>
        <td width="280" bgcolor="#0368BA"><div align="center" class="style30"><span class="style26">คอมมิชชั่น</span></div></td>
      </tr>
      <tr>
        <td height="30" bgcolor="#FFFFFF"><div align="center" class="style1">1,000,000 - 1,500,000</div></td>
        <td bgcolor="#FFFFFF"><div align="center" class="style12">&nbsp;x 3</div></td>
      </tr>
      <tr>
        <td height="30" bgcolor="#FFFFFF"><div align="center" class="style1">1,500,001 - 3,500,000</div></td>
        <td bgcolor="#FFFFFF"><div align="center" class="style12">&nbsp;x 4</div></td>
      </tr>
      <tr>
        <td height="30" bgcolor="#FFFFFF"><div align="center" class="style1">3,500,001 - 7,000,000</div></td>
        <td bgcolor="#FFFFFF"><div align="center" class="style12">&nbsp;x 5</div></td>
      </tr>
      <tr>
        <td height="30" bgcolor="#FFFFFF"><div align="center" class="style1">7 ล้านขึ้นไป</div></td>
        <td bgcolor="#FFFFFF"><div align="center" class="style12">x 10</div></td>
      </tr>
    </table>
    <span class="style1"><br />
    </span>
    <p class="style1 style26 style15">- ทาง 369bet จะมอบโบนัสพิเศษให้กับลูกค้า ภายวันที่ 5 ของทุกเดือน</p>
    <p class="style1 style26 style15">- การเดิมพันทั้งหมดจะเป็นโมฆะ ในกรณีที่มีการวางเดิมพันทั้ง 2 ฝั่ง ในคู่เดียวกัน</p>
    <p class="style1 style26 style15">- 369bet ขอสงวนสิทธิ์ในการแก้ไขเงื่อนไข และข้อตกลงของโปรโมชั่นนี้ได้ทุกเมื่อโดยไม่ต้องแจ้งล่วงหน้า</p>
  </div>
  <div style="width:1036px; margin:auto; padding:0px 0 7px 0;"><img src="{{url()}}/369bet/img/tutorial/tab-sbobet.png" />
    <table width="100%" border="0">
      <tr>
        <td width="41%"><img src="{{url()}}/369bet/img/tutorial/pic-sbobet.png" width="425" height="345" /></td>
        <td width="59%"><p class="style13"><span class="style14">แทงบอล SBOBET By 369BET</span> <br />
            <br />
            &nbsp;&nbsp;&nbsp; เว็บไซต์อันดับหนึ่งของประเทศไทยที่ให้บริการ พนันออนไลน์ ความสนุกที่คัดสรรมา เพื่อคุณโดยเฉพาะ คุณสามารถเล่น<em> SBOBET </em>ได้ผ่านหลากหลายช่องทางที่เราเตรียมไว้ให้คุณ ไม่ว่าจะเป็น <strong><span class="style15">ทางเข้า SBOBET มือถือ</span> </strong>และผ่านเว็บไซต์&nbsp; <br />
          </p>
          <p class="style13">&nbsp;          &nbsp; เพื่อความสะดวกรวดเร็วในการเข้าเล่น SBOBET เราได้เตรียม Link มากมาย หมดปัญหา <span class="style16">ทางเข้า sbobet เข้าไม่ได้</span> คุณสามารถเลือก Link Login ได้ทันที </p></td>
      </tr>
    </table>
  </div>
  <div style="width:1036px; margin:auto; padding:0px 0 7px 0;"><img src="{{url()}}/369bet/img/tutorial/tab-link.png" /><br />
    <p>ทางเข้า SBO กรณีที่ <em>SBOBET เข้าไม่ได้</em> หรือ SBOBET เข้ายาก ท่านสามารถเข้า SBOBET ล่าสุด ผ่านทาง desktop, มือถือ และ tablet ได้ทางเว็บไซต์นี้ เรามีทีมงานคอยอัพเดท link ทางเข้า SBOBET ล่าสุดให้ตลอด ไม่ว่าจะเป็นทางเข้า <em>SBO222</em>, <em>SBO333</em>, <em>SBO168, SBO128</em> สามารถ login ได้ทันทีผ่านทาง PC, mobile(Android, iOS(iPhone)) ได้ทันที<span id="more-12"> </span></p>
    <h3><span class="style26"><strong>ทางเข้า SBOBET อัพเดทล่าสุด</strong> (08 April 2016)</span></h3>
    <div>
      <div>
        <table align="center" cellspacing="1">
          <thead>
            <tr>
              <th width="217" height="35" bgcolor="#0368BA" class="style11 style9 style15">ทางเข้า SBOBET</th>
              <th width="451" bgcolor="#0368BA" class="style11 style9 style15">URL</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td height="30" bgcolor="#FFFFFF"><span class="style1 style26 style15">&nbsp;&nbsp; ทางเข้า SBOBET</span></td>
              <td bgcolor="#FFFFFF"><span class="style1"><a target="_blank" rel="nofollow" href="http://www.e16811.com/">http://www.e16811.com/</a> <img src="{{url()}}/369bet/img/tutorial/link-sbobet-update.gif" alt="ทางเข้า SBOBET ใหม่ล่าสุด" width="42" height="12" /></span></td>
            </tr>
            <tr>
              <td height="30" bgcolor="#FFFFFF"><span class="style1 style26 style15">&nbsp;&nbsp; Link SBO Update</span></td>
              <td bgcolor="#FFFFFF"><span class="style1"><a target="_blank" rel="nofollow" href="http://www.longmirror.com/">http://www.longmirror.com/</a> <img src="{{url()}}/369bet/img/tutorial/link-sbobet-update.gif" alt="ทางเข้า SBOBET ใหม่ล่าสุด" width="42" height="12" /></span></td>
            </tr>
            <tr>
              <td height="30" bgcolor="#FFFFFF"><span class="style1 style26 style15">&nbsp;&nbsp; ทางเข้า SBO</span></td>
              <td bgcolor="#FFFFFF"><span class="style1"><a target="_blank" rel="nofollow" href="http://www.currybread.com/">http://www.currybread.com/</a> <img src="{{url()}}/369bet/img/tutorial/link-sbobet-update.gif" alt="ทางเข้า SBOBET ใหม่ล่าสุด" width="42" height="12" /></span></td>
            </tr>
            <tr>
              <td height="30" bgcolor="#FFFFFF"><span class="style1 style26 style15">&nbsp;&nbsp; ทางเข้าสโบ</span></td>
              <td bgcolor="#FFFFFF"><span class="style1"><a target="_blank" rel="nofollow" href="http://www.beer000.com/">http://www.beer000.com/</a><img src="{{url()}}/369bet/img/tutorial/link-sbobet-update.gif" alt="ทางเข้า SBOBET ใหม่ล่าสุด" width="42" height="12" /></span></td>
            </tr>
            <tr>
              <td height="30" bgcolor="#FFFFFF"><span class="style1 style26 style15">&nbsp;&nbsp; ทางเข้าสโบเบท</span></td>
              <td bgcolor="#FFFFFF"><span class="style1"><a target="_blank" rel="nofollow" href="http://www.beer777.com/">http://www.beer777.com/</a><img src="{{url()}}/369bet/img/tutorial/link-sbobet-update.gif" alt="ทางเข้า SBOBET ใหม่ล่าสุด" width="42" height="12" /><img src="{{url()}}/369bet/img/tutorial/sbobet-link-th.gif" alt="SBOBET Link Thai" width="16" height="11" data-wp-pid="2050" /></span></td>
            </tr>
            <tr>
              <td height="30" bgcolor="#FFFFFF"><span class="style1 style26 style15">&nbsp;&nbsp; ทางเข้า SBO TH</span></td>
              <td bgcolor="#FFFFFF"><span class="style1"><a target="_blank" rel="nofollow" href="http://www.e16811.com/">http://www.e16811.com/</a><img src="{{url()}}/369bet/img/tutorial/link-sbobet-update.gif" alt="ทางเข้า SBOBET ใหม่ล่าสุด" width="42" height="12" /><img src="{{url()}}/369bet/img/tutorial/sbobet-link-th.gif" alt="SBOBET Link Thai" width="16" height="11" data-wp-pid="2050" /></span></td>
            </tr>
            <tr>
              <td height="30" bgcolor="#FFFFFF"><span class="style1 style26 style15">&nbsp;&nbsp; ทางเข้า SBOBET Thai</span></td>
              <td bgcolor="#FFFFFF"><span class="style1"><a target="_blank" rel="nofollow" href="http://www.560bet.com/">http://www.560bet.com/</a><img src="{{url()}}/369bet/img/tutorial/link-sbobet-update.gif" alt="ทางเข้า SBOBET ใหม่ล่าสุด" width="42" height="12" /><img src="{{url()}}/369bet/img/tutorial/sbobet-link-th.gif" alt="SBOBET Link Thai" width="16" height="11" data-wp-pid="2050" /></span></td>
            </tr>
            <tr>
              <td height="30" bgcolor="#FFFFFF"><span class="style1 style26 style15">&nbsp;&nbsp; ทางเข้าสโบ</span></td>
              <td bgcolor="#FFFFFF"><a href="http://www.beer789.com/" target="_blank" class="style1" rel="nofollow">http://www.beer789.com/</a></td>
            </tr>
            <tr>
              <td height="30" bgcolor="#FFFFFF"><span class="style1 style26 style15">&nbsp;&nbsp; ทางเข้าเอสบีโอ</span></td>
              <td bgcolor="#FFFFFF"><a href="http://www.tomato333.com/" target="_blank" class="style1" rel="nofollow">http://www.tomato333.com/</a></td>
            </tr>
            <tr>
              <td height="30" bgcolor="#FFFFFF"><span class="style1 style26 style15">&nbsp;&nbsp; ทางเข้า SBO</span></td>
              <td bgcolor="#FFFFFF"><a href="http://www.u16888.com/" target="_blank" class="style1" rel="nofollow">http://www.u16888.com/</a></td>
            </tr>
          </tbody>
        </table>
      </div>
      <h3><span class="style26"><strong>ทางเข้า SBO สำหรับ mobile + tablet อัพเดทล่าสุด</strong> (08 April 2016)</span></h3>
      <div>
        <div>
          <table width="684" align="center" cellspacing="1">
            <thead>
              <tr>
                <th height="35" bgcolor="#0368BA" class="style11"><span class="style9 style15">ทางเข้า SBOBET</span></th>
                <th bgcolor="#0368BA" class="style11"><span class="style9 style15">URL</span></th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td height="30" bgcolor="#FFFFFF"><span class="style1 style26 style15">&nbsp;&nbsp; ทางเข้าสโบเบ็ต</span></td>
                <td bgcolor="#FFFFFF"><span class="style1"><a target="_blank" rel="nofollow" href="http://www.colourhim.com/">http://www.colourhim.com/</a> <img src="{{url()}}/369bet/img/tutorial/link-sbobet-update.gif" alt="ทางเข้า SBOBET ใหม่ล่าสุด" width="42" height="12" /><img src="{{url()}}/369bet/img/tutorial/sbobet-link-th.gif" alt="SBOBET Link Thai" width="16" height="11" data-wp-pid="2050" /></span></td>
              </tr>
              <tr>
                <td height="30" bgcolor="#FFFFFF"><span class="style1 style26 style15">&nbsp;&nbsp; ทางเข้าสโมเบท</span></td>
                <td bgcolor="#FFFFFF"><span class="style1"><a target="_blank" rel="nofollow" href="https://m.beer777.com/">https://m.beer777.com/</a> <img src="{{url()}}/369bet/img/tutorial/link-sbobet-update.gif" alt="ทางเข้า SBOBET ใหม่ล่าสุด" width="42" height="12" /></span></td>
              </tr>
              <tr>
                <td height="30" bgcolor="#FFFFFF"><span class="style1 style26 style15">&nbsp;&nbsp; ทางเข้าเอสบีโอ</span></td>
                <td bgcolor="#FFFFFF"><span class="style1"><a target="_blank" rel="nofollow" href="http://m.u16888.com/">http://m.u16888.com/</a><img src="{{url()}}/369bet/img/tutorial/link-sbobet-update.gif" alt="ทางเข้า SBOBET ใหม่ล่าสุด" width="42" height="12" /></span></td>
              </tr>
              <tr>
                <td height="30" bgcolor="#FFFFFF"><span class="style1 style26 style15">&nbsp;&nbsp; ทางเข้า SBO</span></td>
                <td bgcolor="#FFFFFF"><a href="https://m.sbobet.tv/" target="_blank" class="style1" rel="nofollow">https://m.sbobet.tv/</a></td>
              </tr>
              <tr>
                <td height="30" bgcolor="#FFFFFF"><span class="style1 style26 style15">&nbsp;&nbsp; ทางเข้าสโบ</span></td>
                <td bgcolor="#FFFFFF"><a href="https://m.e16833.com/" target="_blank" class="style1" rel="nofollow">https://m.e16833.com/</a></td>
              </tr>
              <tr>
                <td height="30" bgcolor="#FFFFFF"><span class="style1 style26 style15">&nbsp;&nbsp; SBOBET Mobile</span></td>
                <td bgcolor="#FFFFFF"><a href="http://www.sbobet4mobile.com/" target="_blank" class="style1" rel="nofollow">http://www.sbobet4mobile.com/</a></td>
              </tr>
            </tbody>
          </table>
          <br />
          <br />
          <div>
            <div></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@stop

@section('bottom_js')

@stop