@extends('369bet/master')

@section('title', 'Sportsbook, Live Casino, Slot Games and High 4D Payout')

@section('content')
<script>
 function change_iframe(url,image_link,product){
         $('#iframe_game').attr('src',url);
         $('.top').show();
         $('.slot_'+product+'_image').hide();
 }
</script>

@section('banner')
	<div class="sliderCont">
		<div class="sliderContInner tp04">
			<img width="1024" height="300" src="{{url()}}/369bet/img/tab-game.jpg">
		</div>
	</div>
@stop




<div class="midSectCont">

<div class="bnr09a">
  <a href="javascript:void(0)" onClick="change_iframe('{{route('pltb' , [ 'type' => 'pgames' , 'category' => '1' ] )}}','pt-slot-hover.png','plt')">
	<img src="{{url()}}/369bet/img/game1.png" width="320">
  </a>
  <a href="javascript:void(0)" onClick="change_iframe('{{route('w88' , [ 'type' => 'slot' , 'category' => 'Arcades' ] )}}','gp-slot-hover.png','w88')">
	<img src="{{url()}}/369bet/img/game2.png" width="320">
  </a>
  <a href="javascript:void(0)" onClick="@if (Auth::user()->check())window.open('{{$mxb_slot}}', 'casino11', 'width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif">
	<img src="{{url()}}/369bet/img/game3.png" width="320">
  </a>
  <div class="clr"></div>
</div>

<div class="slotContainerBottom">
		<iframe id="iframe_game" frameborder="0" style="overflow:hidden;overflow-x:hidden;overflow-y:hidden;height: 970px; width:1024px; margin-left: 0px;" src="{{route('pltb' , [ 'type' => 'pgames' , 'category' => '1' ] )}}"></iframe>
			</div>


<div class="bnr08">
<span>เกี่ยวกับ 369bet.com</span>

เราภูมิใจนำเสนอรูปแบบความบันเทิงในการเล่นคาสิโนที่พิเศษที่สุด. 369Bet มอบประสบการณ์การเล่นเกมส์แบบใหม่ที่คุณไม่เคยคิดฝัน. ไม่ว่าจะ หน้ากีฬา คาสิโนสด เกมส์สล๊อต หวย เรามีให้บริการคุณทั้งหมด! เราการันตีว่าคุณจะได้รับความรู้สึกตื่นเต้นในการเล่นคาสิโนด้วยสภาพแวดล้อมที่ความปลอดภัยสูงสุด โดยคุณไม่จำเป็นจะต้องกังวลใดๆทั้งสิ้น.

</div>

</div>
  
@stop