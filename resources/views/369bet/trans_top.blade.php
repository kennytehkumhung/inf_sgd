	<div class="walletTop">
	

		<div class="wallet1">
			<div class="main-set-title">{{Lang::get('public.Mainwallet')}}</div>
			<div class="main-setPrice main_wallet">{{App\Http\Controllers\User\WalletController::mainwallet()}}</div>
		</div>

		<?php $count = 2 ; ?>
		 @foreach( Session::get('products_obj') as $prdid => $object)
					  <div class="wallet<?php echo $count++; ?>">
						<div class="main-set-title">{{$object->name}}</div><div class="main-setPrice {{$object->code}}_balance">0.00</div>
					  </div>
		 @endforeach

		 <div class="wallet<?php echo $count++; ?>" style='background: rgba(0, 0, 0, 0) url("{{url()}}/369bet/img/walletBg-1.jpg") repeat-x scroll 0 0'>
			<div class="main-set-title">{{Lang::get('public.Total')}}</div>
			<div class="main-setPrice total_balance">0.00</div>
		 </div>

	</div>