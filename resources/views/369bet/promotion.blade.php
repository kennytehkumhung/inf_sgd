@extends('369bet/master')

@section('title', 'Promotion')
<!--<style>
.box    {
    border:3px solid #006CBF;
}
</style>-->
@section('content')

   <script type="text/javascript">
		$(document).ready(function($) {
			$('#accordion').find('.accordion-toggle').click(function(){
	
				//Expand or collapse this panel
				$(this).next().slideToggle('fast');
	
				//Hide the other panels
				$(".accordion-content").not($(this).next()).slideUp('fast');
	
			});
		});
	</script> 

<div class="midSectCont">

    <div id="promo">
    
        <div id="accordion">
	
			@foreach ($promo as $key => $value )
		
			  		  
					   <h4 class="accordion-toggle">
                                               
						   <img src="{{$value['image']}}">
                                                   
						 </h4>
						<div class="accordion-content default">
								<?php echo htmlspecialchars_decode($value['content']); ?>
						</div>
				
			@endforeach

				


</div>
          
          
      </div>
    
   
  


<div class="bnr08">
<span>เกี่ยวกับ 369bet.com</span>

เราภูมิใจนำเสนอรูปแบบความบันเทิงในการเล่นคาสิโนที่พิเศษที่สุด. 369Bet มอบประสบการณ์การเล่นเกมส์แบบใหม่ที่คุณไม่เคยคิดฝัน. ไม่ว่าจะ หน้ากีฬา คาสิโนสด เกมส์สล๊อต หวย เรามีให้บริการคุณทั้งหมด! เราการันตีว่าคุณจะได้รับความรู้สึกตื่นเต้นในการเล่นคาสิโนด้วยสภาพแวดล้อมที่ความปลอดภัยสูงสุด โดยคุณไม่จำเป็นจะต้องกังวลใดๆทั้งสิ้น.

</div>

</div>
  
@stop