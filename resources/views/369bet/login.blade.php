
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<script src="{{url()}}/369bet/resources/js/jquery-2.1.3.min.js"></script>
<script>
function login()
{
	$.ajax({
		type: "POST",
		url: "{{route('login')}}",
		data: {
			_token: "{{ csrf_token() }}",
			username: $('#username').val(),
			password: $('#password').val(),
			code:     $('#code').val(),
		},
	}).done(function( json ) {
			 obj = JSON.parse(json);
			 var str = '';
			 $.each(obj, function(i, item) {
				str += item + '\n';
			})
			if( "{{Lang::get('COMMON.SUCESSFUL')}}\n" == str ){
				parent.location.reload(); 
			}else{
				alert(str);
                                location.reload(); 
			}
			$('#code').html('<img style="position:absolute;margin-top:5px;margin-left:5px;" src="{{route('captcha', ['type' => 'login_captcha'])}}" width="60" height="27" />');
			
	});
}
function enterpressalert(e, textarea){

			var code = (e.keyCode ? e.keyCode : e.which);
			 if(code == 13) { //Enter keycode
			   login();
			 }
		}
</script>
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
	<div style="width:550px; height:350px; margin:auto; background:url({{url()}}/369bet/img/login/Gicon.jpg) bottom right no-repeat;">
	  <table width="448" border="0" align="center">
        <tr>
              <td height="103"><div align="center"><img src="{{url()}}/369bet/img/login/logo.png" width="261" height="70" /></div></td>
        </tr>
            <tr>
              <td><table width="400" border="0" cellspacing="0">
                <tr>
                  <td width="101"><div align="right"><img src="{{url()}}/369bet/img/login/brt-user.png" width="35" height="35" /></div></td>
                  <td width="295"><input type="text" name="textfield" id="username" style="width:250px; height:30px;" /></td>
                </tr>
              </table></td>
            </tr>
            <tr>
              <td><table width="400" border="0" cellspacing="0">
                <tr>
                  <td width="101"><div align="right"><img src="{{url()}}/369bet/img/login/brt-password.png" width="35" height="35" /></div></td>
                  <td width="295"><input type="password" name="textfield2" id="password" style="width:250px; height:30px;" /></td>
                </tr>
              </table></td>
            </tr>
            <tr>
              <td height="31" valign="bottom"><strong><font color="#666666" size="3" face="Tahoma">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; รหัสตรวจสอบ</font></strong></td>
        </tr>
            <tr>
              <td> &nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
				
				<input type="text" name="textfield3" id="code" style="width:120px; height:30px;" onKeyPress="enterpressalert(event, this)"/>
				<img style="position:absolute;margin-top:5px;margin-left:5px;" src="{{route('captcha', ['type' => 'login_captcha'])}}" width="60" height="27" />
				</td>
            </tr>
            <tr>
              <td height="32" valign="bottom"><div align="center"><a data-toggle="modal" data-target="#forgot" href="{{route('forgotpassword')}}"><font size="3" face="Tahoma">ลืมรหัสผ่าน? เรียกใหม่ที่นี่!</font></a>&nbsp;&nbsp;&nbsp; |&nbsp;<font size="3" face="Tahoma">&nbsp;&nbsp; <a href="{{route('register')}}" target="_blank">เข้าร่วมกับ 369bet ตอนนี้!!</a></font></div></td>
        </tr>
            <tr>
              <td>&nbsp;</td>
        </tr>
            <tr>
              <td><div align="center">
                <input onclick="login();" type="button" value="เข้าสู่ระบบ" style="background:#006cbf; width:180px; height:35px; font-size:14px; color:#FFFFFF; border:solid 1px #006cbf;cursor:pointer;" />
              </div></td>
            </tr>
          </table>
	</div>
</body>
</html>
