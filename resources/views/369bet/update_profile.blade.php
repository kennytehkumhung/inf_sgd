@extends('369bet/master')

@section('title', 'Profile')

@section('content')
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="{{url()}}/369bet/resources/js/jquery-ui.js"></script>
<script>
	$(document).ready(function() { 
		getBalance(true);
	});
	$(function() {
		$( ".datepicker" ).datepicker({ dateFormat: 'yy-mm-dd'  , defaultDate: new Date() });
	});
	 function update_profile(){
			if( $('#dob_year').val() != undefined ){
	
				var dob = $('#dob_year').val()+'-'+$('#dob_month').val()+'-'+$('#dob_day').val();	
			}else{
				
				var dob = '{{ $acdObj->dob }}';
			}
                $.ajax({
			type: "POST",
			url: "{{action('User\MemberController@UpdateDetail')}}",
			data: {
				_token: "{{ csrf_token() }}",
				//dob:		$('#dob').val(),
				gender:    	$('#gender').val(),
                                dob:			 dob

			},
		}).done(function( json ) {
			
				 obj = JSON.parse(json);
				 var str = '';
				 $.each(obj, function(i, item) {
					str += item + '\n';
				})
				
				alert(str);
				
				
		});
	}
</script>

<div class="midSectCont">
	<br>
	
	@include('369bet/trans_top' )

	<!--ACCOUNT MANAGAMENT MENU-->
		  <div class="inlineAccMenu">
		 <ul>
		 <li><a href="{{route('update-profile')}}">{{Lang::get('public.AccountProfile')}}</a></li>
		  <li><a href="{{route('update-password')}}">{{Lang::get('public.AccountPassword')}}</a></li>
		  <!--<li><a href="statement.html">Statement</a></li>-->
		  </ul>
		  </div>
		  <!--ACCOUNT MANAGAMENT MENU-->
		  <!--ACCOUNT TITLE-->
		  <div class="title_bar">
		  <span>{{Lang::get('public.AccountProfile')}}</span>
		  </div>
		  <!--ACCOUNT TITLE-->
		  <!--ACCOUNT CONTENT-->
			 <div class="acctContent">
				<span class="wallet_title"><i class="fa fa-pencil"></i>{{Lang::get('public.AccountProfile')}}</span>
				<div class="acctRow">
					<label>{{Lang::get('public.Username')}} :</label><span class="acctText">{{$acdObj->fullname}}</span>
					<div class="clr"></div>
				</div>
				<div class="acctRow">
					<label>{{Lang::get('public.EmailAddress')}} :</label><span class="acctText">{{$acdObj->email}}</span>
					<div class="clr"></div>
				</div>
				<div class="acctRow">
					<label>{{Lang::get('public.Currency')}} :</label><span class="acctText">บาท</span>
					<div class="clr"></div>
				</div>
				<div class="acctRow">
					<label>{{Lang::get('public.ContactNo')}} :</label><span class="acctText">{{$acdObj->telmobile}}</span>
					<div class="clr"></div>
				</div>
				<div class="acctRow">
					<label>{{Lang::get('public.Gender')}} :</label>
					<select id="gender">
				<option value="1">{{Lang::get('public.Male')}}</option> 
				<option value="2">{{Lang::get('public.Female')}}</option>
					</select>
					<div class="clr"></div>
				</div>
				<div class="acctRow">
					<label>{{Lang::get('public.DOB')}} :</label>
					@if( $acdObj->dob == '0000-00-00' )
					<select id="dob_day">
                                            <option value="01">01</option>
						<option value="02">02</option>
						<option value="03">03</option>
						<option value="04">04</option>
						<option value="05">05</option>
						<option value="06">06</option>
						<option value="07">07</option>
						<option value="08">08</option>
						<option value="09">09</option>
						<option value="10">10</option>
						<option value="11">11</option>
						<option value="12">12</option>
						<option value="13">13</option>
						<option value="14">14</option>
						<option value="15">15</option>
						<option value="16">16</option>
						<option value="17">17</option>
						<option value="18">18</option>
						<option value="19">19</option>
						<option value="20">20</option>
						<option value="21">21</option>
						<option value="22">22</option>
						<option value="23">23</option>
						<option value="24">24</option>
						<option value="25">25</option>
						<option value="26">26</option>
						<option value="27">27</option>
						<option value="28">28</option>
						<option value="29">29</option>
						<option value="30">30</option>
						<option value="31">31</option>
                                        </select>
                                        <select id="dob_month" >
                                            <option value="01">January</option>
                                            <option value="02">February</option>
                                            <option value="03">March</option>
                                            <option value="04">April</option>
                                            <option value="05">May</option>
                                            <option value="06">June</option>
                                            <option value="07">July</option>
                                            <option value="08">August</option>
                                            <option value="09">September</option>
                                            <option value="10">October</option>
                                            <option value="11">November</option>
                                            <option value="12">December</option>
                                        </select>
                                        <select id="dob_year">
						<option value="2006">2006</option>
                                <option value="2005">2005</option>
                                <option value="2004">2004</option>
                                <option value="2003">2003</option>
                                <option value="2002">2002</option>
                                <option value="2001">2001</option>
                                <option value="2000">2000</option>
                                <option value="1999">1999</option>
                                <option value="1998">1998</option>
                                <option value="1997">1997</option>
                                <option value="1996">1996</option>
                                <option value="1995">1995</option>
                                <option value="1994">1994</option>
                                <option value="1993">1993</option>
                                <option value="1992">1992</option>
                                <option value="1991">1991</option>
                                <option value="1990">1990</option>
                                <option value="1989">1989</option>
                                <option value="1988">1988</option>
                                <option value="1987">1987</option>
                                <option value="1986">1986</option>
                                <option value="1985">1985</option>
                                <option value="1984">1984</option>
                                <option value="1983">1983</option>
                                <option value="1982">1982</option>
                                <option value="1981">1981</option>
                                <option value="1980">1980</option>
                                <option value="1979">1979</option>
                                <option value="1978">1978</option>
                                <option value="1977">1977</option>
                                <option value="1976">1976</option>
                                <option value="1975">1975</option>
                                <option value="1974">1974</option>
                                <option value="1973">1973</option>
                                <option value="1972">1972</option>
                                <option value="1971">1971</option>
                                <option value="1970">1970</option>
                                <option value="1969">1969</option>
                                <option value="1968">1968</option>
                                <option value="1967">1967</option>
                                <option value="1966">1966</option>
                                <option value="1965">1965</option>
                                <option value="1964">1964</option>
                                <option value="1963">1963</option>
                                <option value="1962">1962</option>
                                <option value="1961">1961</option>
                                <option value="1960">1960</option>	
					  </select>
					  @else
					  {{ $acdObj->dob }}
					  @endif
                                        <!--<br>
					<input type="text" class="datepicker" id="deposit_date" style="cursor:pointer;" name="date" value="{{$acdObj->dob}}"><br>-->
					<div class="clr"></div>
				</div>
				<div class="submitAcct">
				<a href="#" onClick="update_profile()" >{{Lang::get('public.Submit')}}</a>
				</div>
			</div>
		  <!--ACCOUNT CONTENT-->

	<div class="clr"></div>

</div>
  
@stop