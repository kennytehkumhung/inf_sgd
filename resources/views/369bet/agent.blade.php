@extends('369bet/master')

@section('title', 'Banking')



@section('content')


<div class="midSectCont">
<br>
<div class="bk01">
<div align="center" style="font-family:Tahoma; font-size:28px; color:#0066CC; font-weight:bold; padding-bottom:10px; border-bottom:3px solid #0066cc">หุ้นส่วนธุรกิจ</div>
      
      <br />
      <font color="#333333" size="3" face="Tahoma"><br />
      </font>
      <p><font color="#333333" size="3" face="Tahoma">บริการพิเศษที่ตอบสนองทุกรูปแบบการใช้ชีวิตของคุณตลอด 24 ชั่วโมง</font></p>
      <p><font color="#333333" size="3" face="Tahoma">วันนี้...บริษัท <strong>369BET</strong> ขอนำเสนอ บริการหุ้นส่วนทางธุรกิจที่ดีที่สุด ที่แรกและที่เดียวในประเทศไทย <br />
      </font><font color="#333333" size="3" face="Tahoma"><br />
      </font></p>
      <p><font color="#333333" size="3" face="Tahoma">สำหรับผู้ที่ต้องการประสบความสำเร็จทางธุรกิจ</font></p>
      <p><font color="#333333" size="3" face="Tahoma">บริษัท <strong>369BET</strong> ยินดีต้อนรับทุกท่านเข้าร่วมเป็นตัวแทนหุ้นส่วนธุรกิจ กับทางเรา ( รับจำนวนจำกัด )<br />
      </font></p>
      <font color="#333333" size="3" face="Tahoma">โดยที่ท่านนั้นสามารถเลือกเป็นได้ทั้ง (Agent หรือ Master) และผลตอบแทนแบบลำดับขั้น 
      ทาง <strong>369BET</strong> เรามีทีมงานที่สามารถให้คำปรึกษา แนะนำ หรือแก้ไขปัญหา และวางแผนการตลาดให้ท่านตลอดเวลา 
      โดยท่านสามารถไว้วางใจกับทางเราได้ ทั้งในด้านการเงิน และการบริการ 
      จากความมั่นคงของผู้ร่วมธุรกิจ และลูกค้าจำนวนมากมายที่ได้ไว้วางใจ <em>369BET</em>      ท่านสามารถมั่นใจได้เลยว่า ท่านจะได้รับค่าตอบแทนอันคุ้มค่ากับการลงทุนอย่างแน่นอน </font>
      <br />
      <br />
      <div style=" width:950px; height:230px; ov">
      	<div style="float:left; width:450px;">
        <table width="412" border="0" align="right" cellspacing="1" bgcolor="#B6B6B6">
          <tr>
            <td height="50" colspan="2" bgcolor="#0066CC"><div align="center"><strong><font color="#FFFFFF" face="Tahoma">MASTER การวางเงินประกัน</font></strong></div></td>
            </tr>
          <tr>
            <td width="186" height="40" bgcolor="#999999"><div align="center"><font color="#FFFFFF" size="3" face="Tahoma">ทุนประกัน</font></div></td>
            <td width="213" bgcolor="#999999"><div align="center"><font color="#FFFFFF" size="3" face="Tahoma">จำนวน % ถือหุ้น</font></div></td>
          </tr>
          <tr>
            <td height="40" bgcolor="#FFFFFF"><div align="center"><font color="#333333" size="3" face="Tahoma">350,000</font></div></td>
            <td bgcolor="#FFFFFF"><div align="center"><font color="#333333" size="3" face="Tahoma">50%</font></div></td>
          </tr>
          <tr>
            <td height="40" bgcolor="#FFFFFF"><div align="center"><font color="#333333" size="3" face="Tahoma">500,000</font></div></td>
            <td bgcolor="#FFFFFF"><div align="center"><font color="#333333" size="3" face="Tahoma">60%</font></div></td>
          </tr>
          <tr>
            <td height="40" bgcolor="#FFFFFF"><div align="center"><font color="#333333" size="3" face="Tahoma">600,000</font></div></td>
            <td bgcolor="#FFFFFF"><div align="center"><font color="#333333" size="3" face="Tahoma">65%</font></div></td>
          </tr>
        </table>
      	</div>
        
        <div style="float:right; width:450px;">
        <table width="412" border="0" align="left" cellspacing="1" bgcolor="#B6B6B6">
          <tr>
            <td height="50" colspan="2" bgcolor="#0066CC"><div align="center"><strong><font color="#FFFFFF" face="Tahoma">AGENT การวางเงินประกัน</font></strong></div></td>
            </tr>
          <tr>
            <td width="186" height="40" bgcolor="#999999"><div align="center"><font color="#FFFFFF" size="3" face="Tahoma">ทุนประกัน</font></div></td>
            <td width="213" bgcolor="#999999"><div align="center"><font color="#FFFFFF" size="3" face="Tahoma">จำนวน % ถือหุ้น</font></div></td>
          </tr>
          <tr>
            <td height="40" bgcolor="#FFFFFF"><div align="center"><font color="#333333" size="3" face="Tahoma">150,000</font></div></td>
            <td bgcolor="#FFFFFF"><div align="center"><font color="#333333" size="3" face="Tahoma">30%</font></div></td>
          </tr>
          <tr>
            <td height="40" bgcolor="#FFFFFF"><div align="center"><font color="#333333" size="3" face="Tahoma">200,000</font></div></td>
            <td bgcolor="#FFFFFF"><div align="center"><font color="#333333" size="3" face="Tahoma">35%</font></div></td>
          </tr>
          <tr>
            <td height="40" bgcolor="#FFFFFF"><div align="center"><font color="#333333" size="3" face="Tahoma">300,000</font></div></td>
            <td bgcolor="#FFFFFF"><div align="center"><font color="#333333" size="3" face="Tahoma">40%</font></div></td>
          </tr>
        </table>
      	</div>
        
      </div>
      <div></div>
      <div></div>
      <font size="3" face="Tahoma"><strong><font color="#333333"><br />
      </font></strong></font>
      <table width="412" border="0" align="center" cellspacing="1" bgcolor="#B6B6B6">
        <tr>
          <td width="399" height="50" bgcolor="#0066CC"><div align="center"><font color="#FFFFFF" size="3" face="Tahoma"><strong>รับสมัครพันธมิตร โดยไม่ถือ % สู้ </strong></font></div></td>
        </tr>

        <tr>
          <td height="40" bgcolor="#FFFFFF"><div align="center">กรุณาติดต่อสอบถามรายละเอียดได้ที่ Call Center</div></td>
        </tr>
      </table>
      <p><font size="3" face="Tahoma"><strong><font color="#333333"><br />
              <br />
        ทำไมถึงต้องเลือกเป็น Partner Agent กับทาง 368BET</font></strong><font color="#333333"><br />
          <br />
          1. เราเป็นเว็บเดิมพันครบวงจร ที่จะทำให้ทุกการ Betting ของคุณพบกับความบันเทิงเต็มทุกรูปแบบ<br />
          2. เรามีความมั่นคงทางการเงินสูง และมีบุคคลากรคุณภาพที่จะคอยให้ความรู้ และแนะนำธุรกิจให้กับท่าน<br />
          <br />
          </font></font></p>
      <p><font color="#333333" size="3" face="Tahoma"><strong>เงื่อนไขการเข้าร่วมทำธุรกิจ</strong></font></p>
      <font color="#333333" size="3" face="Tahoma">1. ร่วมเป็น มาสเตอร์ , เอเย่นในการสร้างฐานลูกค้า<br />
      2. ต้องมีเงินมัดจำ จำนวนนึงเพื่อแลกกับยอดเงินเครดิตสำหรับเติมให้ลูกค้าของท่าน<br />
      3. ต้องมีฐานรายได้ที่มั่นคงเพื่อรองรับลูกค้าที่จะเข้ามา<br />
      4. เครียร์เงินทุกวันอังคาร-ศุกร์ ก่อน 5 โมงเย็น</font><br />
    </div>
<div class="bnr08">
<span>เกี่ยวกับ 369bet.com</span>

เราภูมิใจนำเสนอรูปแบบความบันเทิงในการเล่นคาสิโนที่พิเศษที่สุด. 369Bet มอบประสบการณ์การเล่นเกมส์แบบใหม่ที่คุณไม่เคยคิดฝัน. ไม่ว่าจะ หน้ากีฬา คาสิโนสด เกมส์สล๊อต หวย เรามีให้บริการคุณทั้งหมด! เราการันตีว่าคุณจะได้รับความรู้สึกตื่นเต้นในการเล่นคาสิโนด้วยสภาพแวดล้อมที่ความปลอดภัยสูงสุด โดยคุณไม่จำเป็นจะต้องกังวลใดๆทั้งสิ้น.

</div>
  </div>
  
 

@stop