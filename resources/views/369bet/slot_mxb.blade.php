<html>
<head>
<link href="{{url()}}/ampm/resources/css/style.css" rel="stylesheet" type="text/css" />
<Script>

</script>
</head>
<body>	

	
				   <!--Slot Menu-->
				   <div class="slotMenuHolder">
				   <div class="slot_menu">
				   <ul>
				    <li class="tab @if($category == 'Slots') st_selected @endif"		><a  @if($category == 'Slots') style="color:black;" @endif href="{{route('mxb', [ 'type' => 'slot' , 'category' => 'Slots' ] )}}">Slots</a></li>
					<li class="tab @if($category == 'Table') st_selected @endif"	><a @if($category == 'Table') style="color:black;" @endif href="{{route('mxb', [ 'type' => 'slot' , 'category' => 'Table' ] )}}">Table</a></li>
					<li class="tab @if($category == 'Soft Games') st_selected @endif"		><a @if($category == 'Soft Games') style="color:black;" @endif href="{{route('mxb', [ 'type' => 'slot' , 'category' => 'Soft Games' ] )}}">Soft Games</a></li>
					<li class="tab @if($category == 'Video Poker') st_selected @endif"	><a @if($category == 'Video Poker') style="color:black;" @endif href="{{route('mxb', [ 'type' => 'slot' , 'category' => 'Video Poker' ] )}}">Video Poker</a></li>
				   </ul>
				   </div>
				   </div>
				   <!--Slot Menu-->
				   <!--Slot Lobby-->
				  <div class="slot_lobby">
			
					
					@foreach( $lists as $list )
					<div class="slot_box">
						<a href="javascript:void(0)" onclick="@if (Auth::user()->check())window.open('{{route('mxb-slot' , [ 'gameid' => $list['gameID'] ] )}}', 'mxb_slot', 'width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif" >
							<img src="{{$list['imageUrl']}}" width="150" height="150" alt=""/>
						</a>
						<span>{{$list['gameName']}}</span>
					</div>			 
					@endforeach
		
				  
				  <div class="clr"></div>
				  </div>
				   <!--Slot Lobby-->
			   </div>
			   
<!--Slot-->
</body>
</html>