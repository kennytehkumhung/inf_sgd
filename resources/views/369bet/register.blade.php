@extends('369bet/master')

@section('title', 'Register')

@section('content')

<script>
  function register_submit(){
	$.ajax({
		type: "POST",
		url: "{{route('register_process')}}",
		data: {
			_token: "{{ csrf_token() }}",
			username:		 $('#r_username').val(),
			password: 		 $('#r_password').val(),
			repeatpassword:  $('#r_repeatpassword').val(),
			code:    		 $('#r_code').val(),
			email:    		 $('#r_email').val(),
			mobile:    		 $('#r_mobile').val(),
			fullname:        $('#r_fullname').val(),
			dob:			 '0000-00-00'
		},
	}).done(function( json ) {
			 $('.acctTextReg').html('');
			 obj = JSON.parse(json);
			 var str = '';
			 $.each(obj, function(i, item) {
				if( '{{Lang::get('COMMON.SUCESSFUL')}}' == item ){
					alert('{{Lang::get('public.NewMember')}}');
					window.location.href = "{{route('homepage')}}";
				}else{
					$('.'+i+'_acctTextReg').html('<font style="color:red">'+item+'</font>');
					//str += item + '\n';
//                                $('#r_code').remove();                               
				}
			});
                         $('#r_code_img').attr('src', "{{route('captcha', ['type' => 'register_captcha'])}}&ref=" + Math.floor((Math.random() * 10) + 1));
	});
}

function enterpressalert(e, textarea){

			var code = (e.keyCode ? e.keyCode : e.which);
			 if(code == 13) { //Enter keycode
			   login();
			 }
		}
</script>

<div style="width:1020px; background: #e2e3e4;  border:solid 1px #DADBDE;margin:auto;">

  <div style="width:1036px; margin:auto; padding-top:15px;">
    <div style=" margin:auto; width:950px;   ">
      <div align="center" style="font-family:Tahoma; font-size:28px; color:#0066CC; font-weight:bold; padding-bottom:10px; border-bottom:3px solid #0066cc">{{Lang::get('public.Register')}}</div>
      
      <br />
      <table width="950" border="0" style="color:#333333; font:Tahoma; font-size:14px">
        <tr>
          <td width="303" height="50"><div align="right" class="style26"><span class="style27">&nbsp;<span style="color:#FF0000">*</span> {{Lang::get('public.Username')}} :</div></td>
          <td width="637">
            <input type="text" name="textfield" id="r_username" style="width:250px; height:30px; border:solid 1px #999999;" />
			<div style="float:right; width:360px;"><span style="color: #666666;" class="username_acctTextReg">โปรดระบุชื่อผู้ใช้ที่ไม่ซ้ำกัน (ตัวอักษร (A-Z,a-z,0-9) เท่านั้น). </span></div></td>
        </tr>   
		
		<tr>
          <td width="303" height="50"><div align="right" class="style26"><span class="style27">&nbsp;<span style="color:#FF0000">*</span> {{Lang::get('public.FullName')}} :</div></td>
          <td width="637">
            <input type="text" name="textfield" id="r_fullname" style="width:250px; height:30px; border:solid 1px #999999;" />
			<div style="float:right; width:360px;"><span style="color: #666666;" class="fullname_acctTextReg" ></div> </span></td>
        </tr>
		
		
		<tr>
          <td width="303" height="50"><div align="right" class="style26"><span class="style27">&nbsp;<span style="color:#FF0000">*</span> {{Lang::get('public.Password')}} :</div></td>
          <td width="637">
            <input type="password" name="textfield" id="r_password" style="width:250px; height:30px; border:solid 1px #999999;" />
			<div style="float:right; width:360px;"><span style="color: #666666;" class="password_acctTextReg" >ระบุจำนวน 6-12 ตัวอักษรและมีเพียง(AZ, az, 0-9) เท่านั้น. </span></div></td>
        </tr>   
		
		<tr>
          <td width="303" height="50"><div align="right" class="style26"><span class="style27">&nbsp;<span style="color:#FF0000">*</span> {{Lang::get('public.RepeatPassword')}} :</div></td>
          <td width="637">
            <input type="password" name="textfield" id="r_repeatpassword" style="width:250px; height:30px; border:solid 1px #999999;" />
			<div style="float:right; width:360px;"><span style="color: #666666;" class=""> </div></span></td>
        </tr>	
		
		<tr>
          <td width="303" height="50"><div align="right" class="style26"><span class="style27">&nbsp;<span style="color:#FF0000">*</span> {{Lang::get('public.Email')}} :</div></td>
          <td width="637">
            <input type="text" name="textfield" id="r_email" style="width:250px; height:30px; border:solid 1px #999999;" />
			<div style="float:right; width:360px;"><span style="color: #666666;" class="email_acctTextReg" > โปรดระบุอีเมล์ที่ใช้งานอยู่เพื่อความสะดวกในการชำระเงินใน ครั้งหน้าและการตอบกลับของจดหมาย</span></div></td>
        </tr>	
		
		<tr>
          <td width="303" height="50"><div align="right" class="style26"><span class="style27">&nbsp;<span style="color:#FF0000">*</span> {{Lang::get('public.ContactNo')}} :</div></td>
          <td width="637">
            <input type="text" name="textfield" id="r_mobile" style="width:250px; height:30px; border:solid 1px #999999;" />
			<div style="float:right; width:360px;"><span style="color: #666666;" class="mobile_acctTextReg"  > 
กรุณาใส่หมายเลขผู้ติดต่อที่ถูกต้อง (ตัวเลขเท่านั้น) สำหรับโปรโมชั่นในอนาคตและจดหมายการชําระเงิน.. </span></div></td>
        </tr>
		
	
		
		
		<tr>
          <td width="303" height="50"><div align="right" class="style26"><span class="style27">&nbsp;<span style="color:#FF0000">*</span> {{Lang::get('public.SecurityQuestion')}} :</div></td>
          <td width="637">
		
           <select id="select" style="width:252px; height:35px; font-size:14px; border:solid 1px #999999;" name="r_question">
			<option>สีโปรดของคุณคือสีอะไร?</option>
			<option>นามสกุลของแม่คุณก่อนแต่งงานชื่ออะไร?</option>
			<option>ชื่อหนังสือเล่มโปรดของคุณ?</option>
			<option>ยี่ห้อและรุ่นของรถคันแรกของคุณ?</option>
			<option>โรงเรียนมัธยมปลายที่คุณจบการศึกษาชื่ออะไร?</option>
			</select>
			
			<div style="float:right; width:360px;"><span style="color: #666666;" class="question_acctTextReg"> </span></div></td>
        </tr>	
		
		<tr>
          <td width="303" height="50"><div align="right" class="style26"><span class="style27">&nbsp;<span style="color:#FF0000">*</span> {{Lang::get('public.Answer')}} :</div></td>
          <td width="637">
		
            <input type="text" name="textfield" id="r_answer" style="width:250px; height:30px; border:solid 1px #999999;" />
			
			<div style="float:right; width:360px;"><span style="color: #666666;" class="answer_acctTextReg"> </span></div></td>
        </tr>
		
		<tr>
          <td width="303" height="50"><div align="right" class="style26"><span class="style27">&nbsp;<span style="color:#FF0000">*</span> {{Lang::get('public.VerificationCode')}} :</div></td>
          <td width="637">
		
            <input type="text" name="textfield" id="r_code" style="width:100px; height:30px; border:solid 1px #999999;" onKeyPress="enterpressalert(event, this)"/>
			  <img id="r_code_img" style="float: left;margin-left:6px;"  src="{{route('captcha', ['type' => 'register_captcha'])}}" width="60" height="30" />
			<div style="float:right; width:360px;"><span style="color: #666666;" class="code_acctTextReg"> </span></div></td>
        </tr>
		<tr>
          <td height="66">&nbsp;</td>
          <td><font><font><input  onClick="register_submit()" type="button" value="{{Lang::get('public.Register')}}" style="background:#006cbf; width:110px; height:40px; font-size:14px; color:#FFFFFF; border:solid 1px #006cbf;cursor:pointer;"></font></font></td>
        </tr>
       
        <tr>
          <td height="30">&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
      </table>
    </div>
    
 
  </div>
  
  <br />
  </div>
@stop

@section('bottom_js')

@stop