@extends('369bet/master')

@section('title', 'Register Tutorial')

@section('content')
<div style="width:1050px; background: #e2e3e4; margin:auto; overflow:hidden; border:solid 1px #DADBDE;">
  <div align="center" style="width:1036px; margin:auto; padding:7px 0 0px 0;"></div>
  <div style="width:1036px; margin:auto; padding-top:15px;">
  
  	<div align="center" style="margin-bottom:-10px;">
  	  <table id="Table_01" width="992" height="85" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td><a href="{{route('deposit_tutorial')}}" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image4','','{{url()}}/369bet/img/tutorial/howto2_01.png',1)"><img src="{{url()}}/369bet/img/tutorial/howto_01.png" name="Image4" width="250" height="85" border="0" id="Image4" /></a></td>
          <td><a href="{{route('withdraw_tutorial')}}" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image5','','{{url()}}/369bet/img/tutorial/howto2_02.png',1)"><img src="{{url()}}/369bet/img/tutorial/howto_02.png" name="Image5" width="248" height="85" border="0" id="Image5" /></a></td>
          <td><a href="{{route('transfer_tutorial')}}"><img src="{{url()}}/369bet/img/tutorial/howto2_03.png" width="248" height="85" /></a></td>
          <td><a href="{{route('register_tutorial')}}" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('Image6','','{{url()}}/369bet/img/tutorial/howto2_04.png',1)"><img src="{{url()}}/369bet/img/tutorial/howto_04.png" name="Image6" width="246" height="85" border="0" id="Image6" /></a></td>
        </tr>
      </table>
  	</div>
    <div style=" margin:auto; width:950px; border:solid 1px #adadad; padding:20px;  ">
      <div align="center" style="font-family:Tahoma; font-size:28px; color:#0066CC; font-weight:bold; padding-bottom:10px; border-bottom:3px solid #0066cc">วิธีการโอนเงิน</div>
      <br /><br /><div align="center" style="width:900px; margin:auto; height:478px; background:url({{url()}}/369bet/img/tutorial/bg-howto.png);">
      	<div style="width:540px; height:306px; float:right; margin-right:30px; margin-top:24px;"><iframe width="540" height="306" src="https://www.youtube.com/embed/14yvZZVW-Kg" frameborder="0" allowfullscreen></iframe></div>
      </div>
      <br />
      <h4><strong><font color="#333333" size="3" face="Tahoma"><br />
      วิธีการโอนเงิน หรือ การย้ายยอดเงิน</font></strong></h4>
      <font color="#333333" size="3" face="Tahoma"><br />
      ขั้นตอนที่ 1. เข้าสู่ระบบ โดยการป้อนชื่อผู้ใช้ และ รหัสผ่าน <br />
      ขั้นตอนที่ 2. คลิกที่ โอน <br />
      ขั้นตอนที่ 3. ตรวจสอบยอดเงินทุนว่ามีเพียงพอสำหรับ การโอนหรือไม่ <br />
      ขั้นตอนที่ 4. เลือกช่องทางการโอน <br />
      ขั้นตอนที่ 5. ยืนยันการโอน <br />
      ขั้นตอนที่ 6. ทาง 369Bet จะดำเนินการโอนเครดิตทันที ที่คำขอโอนได้รับการตรวจสอบ <br />
      ขั้นตอนที่ 7. เมื่อการโอนเสร็จสิ้น สมาชิกสามารถตรวจสอบบันทึกการโอนในหน้าสถานะการทำธุรกรรม <br />
      <br />
      <strong>ความหมายของกระเป๋าเงินประเภทต่างๆ ของ 369BET ?</strong><br />
      <br />
      กระเป๋าเงินจะแบ่งออกเป็น 2 ประเภท คือ ประเป๋าหลัก และ กระเป๋าของเกมส์ประเภทต่างๆ<br />
      <br />
      </font>
      <ul>
        <li><font color="#333333" size="3" face="Tahoma">กระเป๋าหลักของ 369Bet จะแสดงยอดเงินทั้งหมดที่ท่านสามารถทำการถอนได้โดยวิธีการทำธุรกรรม ทางธนาคารในรูปแบบต่างๆ หรือจะโอนย้ายไปที่เกมส์ที่ท่านต้องการจะเดิมพัน กระเป๋าหลักนี้จะแสดงยอดเงินที่ท่านโอนย้ายเข้ามาด้วยเช่นกัน</font></li>
        <li><font color="#333333" size="3" face="Tahoma">กระเป๋าของแต่ละเกมส์ จะแสดงยอดเงินที่คุณสามารถใช้เดิมพันได้</font></li>
      </ul>
      <font color="#333333" size="3" face="Tahoma"><br />
      <p>A. กีฬา : สำหรับหน้ากีฬา และ การแข่งม้า<br />
        B. คาสิโน : รวมสุยดอความทันสมัย ของ 369Bet คาสิโนสด<br />
        C. สล็อตเกมส์ และเกมส์ประเภทต่างๆ<br />
        D. ล็อตเตอร์รี่ : สำหรับการเดิมพันสลากกินแบ่งรัฐบาล<br />
      </p>
      <br />
      </font>
      <p><font color="#333333" size="3" face="Tahoma">ท่านสามารถโอนเงินจากกระเป๋าหลัก ไปยังกระเป๋าของเกมส์ต่างๆที่ท่านต้องการ หรือ จะระหว่างกระเป๋าเกมส์ ไปกระเป๋าหลัก ก้อได้ด้วยเช่นกัน</font></p>
    </div>
  </div>
  
  <br />
  </div>

@stop

@section('bottom_js')

@stop