@extends('../369bet/master')

@section('title', Lang::get('public.SportsBook'))
@section('keywords', 'football betting,sports betting')
@section('description', 'Gambling and bet on football at trusted Malaysia online sports betting site, Infiniwin. Win big today!')

@section('content')
<div class="midSectCont">
@if (Auth::user()->check())
	<iframe id="" width="1050px" height="855px" frameborder="0" src="{{$login_url}}"></iframe>
@else
	<iframe name="sportFrame" id="" width="1024px" height="855px" frameborder="0" src="http://odds.mywinday.com"></iframe>
@endif
</div>
@stop

