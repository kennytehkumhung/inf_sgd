<html>
<head>
 <link href="{{url()}}/front/resources/css/slot.css" rel="stylesheet">
 <link href="{{url()}}/front/resources/css/style_2.css" rel='stylesheet' type='text/css'>

<!-- Home slider style -->
<link rel="stylesheet" href="{{url()}}/front/resources/css/style_3.css">
</head>
<body>
	<div class="slot_menu">
		<ul>
			<li><a href="">All (18)</a></li>
		</ul>
	</div>
	<div id="slot_lobby">
		@foreach( $lists as $list )
			@foreach( $list as $key => $value )
				<div class="slot_box">
					<a href="javascript:void(0)" onclick="@if (Auth::user()->check())window.open('{{route('a1a-slot' , [ 'gameid' => $value->id ] )}}', 'slot_a1a', 'width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif" >
						<img src="{{$value->img}}" width="150" height="150" alt=""/>
					</a>
					<span></span>
				</div>			
			@endforeach
		@endforeach
	  <div class="clr"></div>
	</div>
    <div class="clr"></div>
<!--Slot-->
</body>
</html>