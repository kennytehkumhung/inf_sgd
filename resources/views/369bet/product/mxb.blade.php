@extends('369bet/master')

@section('title', 'Live Casino')



@section('content')
 <link href="{{url()}}/369bet/resources/css/otherPg.css" rel="stylesheet">
  <SCRIPT>
 function callUrl($id,$url){
	var str = $url;
	var url = str.replace("{4}", $('#limit_'+$id).val() ); 
	window.open(url, 'maxbet', 'width=1150,height=830');
 }
 </script>
<div class="acctContainer" style="padding-left:0px; background-color: #FEBF52;">
      <!--LOBBY MENU-->
      <div class="lobbyMenu">
        <ul>
	      <li><a href="{{route('mxb', [ 'type' => 'casino' , 'category' => 'baccarat' ] )}}">Bacarrat</a></li>
          <li><a href="{{route('mxb', [ 'type' => 'casino' , 'category' => 'roulette' ] )}}">Roulette</a></li>
          <li><a href="{{route('mxb', [ 'type' => 'casino' , 'category' => 'blackjack' ] )}}">Black Jack</a></li>
          <li><a href="{{route('mxb', [ 'type' => 'casino' , 'category' => 'single_player_poker' ] )}}">Poker</a></li>
          <li><a href="{{route('mxb', [ 'type' => 'casino' , 'category' => 'carribean_poker' ] )}}">Carribean Poker</a></li>
          <li><a href="{{route('mxb', [ 'type' => 'casino' , 'category' => 'dragon_tiger' ] )}}">Dragon Tiger</a></li>
          <li><a href="{{route('mxb', [ 'type' => 'casino' , 'category' => 'sicbo' ] )}}">Sic Bo</a></li>
        </ul>
      </div>
      <!--LOBBY MENU-->

     <!--LOBBY-->
		<div class="lobby">	
			<div id="casino_table_wrapper">				
				@foreach( $gamelist as $key => $list)		
				<div class="tableInfo">			
					<div class="roomTitle">{{$list['gameName']}}</div>
						<div class="roomDetails" style="background-color: black;">
						<div class="photo">
							<img width="138" height="182" src="{{$list['image']}}">
						</div>
						<div class="roomInfo">
							Dealer: {{$list['dealerName']}}
							<br>
							<br>
							Currency: THB
							<br>
							<br>
						
							Table Limit:
							<br>
							<select id="limit_{{$key}}">
							@foreach( $list['limit'] as $url_key => $value)
								<option value="{{$value['limitSetID']}}">{{$value['minBet']}} ~ {{$value['maxBet']}}</option>							
							@endforeach
							</select>
							<br>
                                                        <br>
                                                        <br>
							<a class="login_btn" href="#" onClick="callUrl('{{$key}}','{{$list['url']}}')">Play Now</a>
						</div>
						</div>						
					</div>								
				@endforeach			
			</div>				
		</div>	
		<!--LOBBY-->   
      
      </div>
@stop

@section('bottom_js')
<script>
    $('.bxslider').bxSlider({
  pagerCustom: '#bx-pager'
});
</script>
<script src="{{url()}}/front/resources/js/slick.min.js"></script>
<script>
$('.caro').slick({
  infinite: true,
  slidesToShow: 2,
  slidesToScroll: 2,
  autoplay: true,
  variableWidth: true,
  arrows: false
});
</script>
@stop