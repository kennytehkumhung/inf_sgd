@extends('gsc/master')

@section('title', Lang::get('public.TNC'))

@section('content')
<div class="promoTop">
   <div class="promoTopinner">
    <div class="slotTitle">{{Lang::get('public.AgreementStatementRules')}}</div>
    <div class="promoBlock" id="accordion">
		<?php echo htmlspecialchars_decode($content); ?>
    </div>
   </div>
</div>
@stop