<!doctype html>
<html>
<head>
    <link href="{{ asset('/gsc/resources/css/slot2.css') }}" rel="stylesheet" type="text/css" />
</head>
<body style="background-color: transparent; overflow: hidden;">
<div class="slotContainerBottom">
    <div class="slotMenuHolder">
        <div class="slot_menu">
            <ul>
				<li><a href="{{route('plt', [ 'type' => 'pgames' , 'category' => '1' ] )}}">{{ Lang::get('public.ProgressiveGames') }}</a></li>
				<li><a href="{{route('plt', [ 'type' => 'brand' , 'category' => '1' ] )}}">{{ Lang::get('public.BrandedGames') }}</a></li>
				<li><a href="{{route('plt', [ 'type' => 'slot' , 'category' => 'slot' ] )}}">{{ Lang::get('public.SlotGames2') }}</a></li>
				<li><a href="{{route('plt', [ 'type' => 'slot' , 'category' => 'arcade' ] )}}">{{ Lang::get('public.Arcades') }}</a></li>
				<li><a href="{{route('plt', [ 'type' => 'slot' , 'category' => 'tablecards' ] )}}">{{ Lang::get('public.TableCards') }}</a></li>
            </ul>
        </div>
    </div>
	
    <div id="slot_lobby">
        @foreach( $lists as $list )
            <div class="slot_box">
                <a href="javascript:void(0)" onclick="@if (Auth::user()->check())window.open('{{route('pltslotiframe' , [ 'gamecode' => $list['code'] ] )}}', 'plt_slot');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif ">
                    <img width="150" height="150" alt="" src="{{url()}}/front/img/plt/{{$list->code}}.jpg">
                </a>
                <span>{{$list['gameNameCn']}}</span>
            </div>
        @endforeach

        <div class="clr"></div>
    </div>

    <div class="clr"></div>
</div>
</body>
</html>
	
	