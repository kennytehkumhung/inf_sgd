@extends('gsc/master')

@section('title', Lang::get('public.TransactionHistory'))

@section('content')
<link href="{{url()}}/gsc/resources/css/acctMngmt.css" rel="stylesheet" type="text/css" />
<style type="text/css">
	#trans_history {
		border-collapse: collapse;
	}

	#trans_history td {
		border: 1px solid #d7d7d7;
		padding: 3px;
	}

	#trans_history .tbltitle td {
		background-color: #d7d7d7;
		font-weight: bold;
		text-align: center;
	}
</style>

<script>
$(function () {
	transaction_history();
});
  
function transaction_history(){
$.ajax({
	type: "POST",
	url: "{{action('User\TransactionController@transactionProcess')}}",
	data: {
		_token:   "{{ csrf_token() }}",
		date_from:		$('#date_from').val(),
		date_to:    	$('#date_to').val(),
		record_type:    $('#record_type').val()

	},
}).done(function( json ) {

			//var str;
			 var str = '';
			str += '	<tr><th>{{Lang::get('public.ReferenceNo')}}</th><th>{{Lang::get('public.DateTime')}}</th><th>{{Lang::get('public.Type')}}</th><th>{{Lang::get('public.Amount')}}({{ Session::get('user_currency') }})</th><th>{{Lang::get('public.Status')}}</th><th>{{Lang::get('public.Withdrawal')}}</th><th>{{Lang::get('public.Reason')}}</th></tr>';
			
			 obj = JSON.parse(json);
			//alert(obj);
			 $.each(obj, function(i, item) {
				str +=  '<tr><td>'+item.id+'</td><td>'+item.created+'</td><td>'+item.type+'</td><td>'+item.amount+'</td><td>'+item.status+'</td><td>'+item.cardno+'</td><td>'+item.rejreason+'</td></tr>';
				
			})
				
			//alert(json);
			$('#trans_history').html(str);
			
	});
}

setInterval(updateTrans, 10000);
setInterval(update_mainwallet, 10000);

function updateTrans(){
	$( "#trans_history_button" ).trigger( "click" );
}

</script>
<div class="brwn adj04">
 @include('gsc/trans_top' )
</div>
<div class="btm">
<div class="btmInner">

<!--ACCOUNT MANAGAMENT MENU-->
      <div class="inlineAccMenu">
		@include('gsc/accountmenu' )
      </div>
<!--ACCOUNT MANAGAMENT MENU-->
<!--ACCOUNT TITLE-->
      <div class="title_bar">
		<span>{{Lang::get('public.TransactionHistory')}}</span>
      </div>
<!--ACCOUNT TITLE-->
<!--ACCOUNT CONTENT-->
		<div class="acctContent">     
		<span class="wallet_title"><i class="fa fa-file-o"></i>{{Lang::get('public.TransactionHistory')}}</span>
		<div class="acctRow">
			<label>{{Lang::get('public.DateFrom')}} :</label>
			<div class="cstInput1">
				 <input type="text" class="datepicker" id="date_from" style="cursor:pointer;" value="{{date('Y-m-d')}}">
			</div><br>
			<label>{{Lang::get('public.DateTo')}} :</label>
			<div class="cstInput1">
				  <input type="text" class="datepicker" id="date_to" style="cursor:pointer;" value="{{date('Y-m-d')}}">
			</div>
			<div class="clr"></div>
		</div><br><br>
		<div class="acctRow">
			<label>{{Lang::get('public.RecordType')}} :</label>
				<select id="record_type">
					<option value="0" selected>{{Lang::get('public.CreditAndDebitRecords')}}</option>
					<option value="1">{{Lang::get('public.CreditRecords')}}</option>
					<option value="2">{{Lang::get('public.DebitRecords')}}</option>
				</select>
			<div class="clr"></div>
		</div>
		<div class="acctRow">
			<div class="joinBtn adj05">
				<a id="trans_history_button" href="javascript:void(0)" onClick="transaction_history()"> {{Lang::get('public.Submit')}}</a>
			</div>
		</div>
		<br><br>
		<div class="clr"></div>
		<span class="wallet_title"><i class="fa fa-undo"></i>{{Lang::get('public.TransactionHistory')}}</span> 
		<div class="mtTable">
				<table width="100%" id="trans_history" style="">
					<tbody>
						<tr class="tbltitle">
							<th>{{Lang::get('public.ReferenceNo')}}</th>
							<th>{{Lang::get('public.DateOrTime')}}</th>
							<th>{{Lang::get('public.Type')}}</th>
							<th>{{Lang::get('public.Amount')}} </th>
							<th>{{Lang::get('public.Status')}}</th>
							<th>{{Lang::get('public.Withdrawal')}}</th>
							<th>{{Lang::get('public.Reason')}}</th>
						</tr>
						<tr>
							<td colspan="7"><h2><center>{{ Lang::get('public.NoDataAvailable') }}</center></h2></td>
						</tr>
					</tbody>
				</table>
			<div class="clr"></div>
		</div>
      
      
      </div>
<!--ACCOUNT CONTENT-->
</div>
</div>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script>
$(function() {
	$( ".datepicker" ).datepicker({ dateFormat: 'yy-mm-dd'  , defaultDate: new Date() });
});
</script>
@stop