<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
<link href="{{url()}}/gsc/img/favicon.ico" rel="shortcut icon" type="image/icon" />
<link href="" rel="icon" type="image/icon" />
<title>在线投注从GSC  - 体育博彩，现场赌场，老虎机游戏和高4D支付 @yield('title')</title>
<!--[if IE 7]>
	<link rel="stylesheet" type="text/css" href="resources/css/iefix.css" />
<![endif]-->
<!--CSS -->


<style>
.tab {
    overflow: hidden;
    border: 1px solid #000;
    background-color: #000;
}
/* Style the buttons inside the tab */
.tab button {
	width:50%;
    background-color: inherit;
    float: left;
    border: none;
    outline: none;
    cursor: pointer;
    padding: 14px 90px;
    transition: 0.3s;
    font-size: 20px;
	color:#fff;
}

/* Change background color of buttons on hover */
.tab button:hover {
    background-color: #bc0000;
	
}

/* Create an active/current tablink class */
.active {
	color:#bc0000;
}
</style>
<link href="{{url()}}/gsc/resources/css/style.css" rel="stylesheet" type="text/css" />
<link href="{{url()}}/gsc/resources/css/jquery.bxslider.css" rel="stylesheet" type="text/css" />
<link href="{{url()}}/gsc/resources/css/timetabs.css" rel="stylesheet" type="text/css" />
<link href="{{url()}}/gsc/resources/css/remodal.css" rel="stylesheet" type="text/css" />
<link href="{{url()}}/gsc/resources/css/remodal-default-theme.css" rel="stylesheet" type="text/css" />
<link href="{{url()}}/gsc/resources/css/timetabs.css" rel="stylesheet" type="text/css" />
<link href="{{url()}}/gsc/resources/css/skitter.styles.css" rel="stylesheet" type="text/css" />
<link href="{{url()}}/gsc/resources/css/jquery.lineProgressbar.css" rel="stylesheet" type="text/css" />
<link href="{{url()}}/gsc/resources/css/jquery.dropdown.css" rel="stylesheet" type="text/css" />
<link href="{{url()}}/gsc/resources/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<!--SCRIPT-->
<script src="{{url()}}/gsc/resources/js/jquery-2.1.3.min.js"></script>
<script src="{{url()}}/gsc/resources/js/jquery.bxslider.min.js"></script>
<script src="{{url()}}/gsc/resources/js/jquery.timetabs.min.js"></script>
<script src="{{url()}}/gsc/resources/js/remodal.js"></script>
<script src="{{url()}}/gsc/resources/js/jquery.easing.1.3.js"></script>
<script src="{{url()}}/gsc/resources/js/jquery.skitter.min.js"></script>
<script src="{{url()}}/gsc/resources/js/jquery.lineProgressbar.js"></script>
<script src="{{url()}}/gsc/resources/js/jquery.dropdown.min.js"></script>
<script type="text/javascript">
		$(function () {
			@if (Auth::user()->check())
						load_message();
			@endif
			$('marquee').mouseover(function() {
				this.stop();
			}).mouseout(function() {
				this.start();
			});
			
			$('.combine').on('change', function(){
				var date = $('#dob_year').val() + '-' + $('#dob_month').val() + '-' + $('#dob_day').val();
				$('#dob').val(date);
			});
			
			$("#normal").change(function(){
				$("#normalRegister").show();
				$("#fastRegister").hide();
			}).change();
		});
		function downloadBrowser(){
			if(confirm("寰宇浏览器是一款操作简单、\n运行效能高的优质浏览器，\n更是您在网路国度里的专属电脑管家，\n无论走到哪里都像在使用个人电脑一样安心、自在。")){
				window.open("https://www.ub66.com/zh-cn/", "_blank");
			}
		}
		function downloadDNS(){
			if(confirm("一键修复DNS")){
				window.open("https://s1.xf0371.com/troubleshooter/DNS一键优化.exe", "_blank");
			}
		}
		function registerButton(button){
			if(button=='nor'){
				$("#normalRegister").show();
				$("#fastRegister").hide();
				$(".normalClass").css("color", "red");
				$(".fastClass").css("color", "white");
			}else if(button=='fas'){
				$("#fastRegister").show();
				$("#normalRegister").hide();
				$(".fastClass").css("color", "red");
				$(".normalClass").css("color", "white");
			}
		}
		
		function enterpressalert(e, textarea)
		{
			var code = (e.keyCode ? e.keyCode : e.which);
            if(code == 13) { //Enter keycode
              login();
            }
		}
		function enterpressalert2(e, textarea){
			var code = (e.keyCode ? e.keyCode : e.which);
			if(code == 13) { //Enter keycode
			   login2();
			 }
		}
        function login()
        {
			$.ajax({
                type: "POST",
				url: "{{route('login')}}",
				data: {
				_token: "{{ csrf_token() }}",
					username: $('#username').val(),
					password: $('#password').val(),
					code:     $('#code').val(),
				},
			}).done(function(json) {
				obj = JSON.parse(json);
				var str = '';
				$.each(obj, function(i, item) {
					str += item + '\n';
				});
				if ("{{Lang::get('COMMON.SUCESSFUL')}}\n" == str) {
					location.reload();
				} else {
					if(str=="{{Lang::get('validation.wrong_captcha')}}\n"){
						alert(str);
						location.reload();
					}else{
						alert(str);
					}
					$("#captcha").attr("src", "{{route('captcha', ['type' => 'login_captcha'])}}");
				}
			});
        }
		
		function register_submit(){
			$.ajax({
				type: "POST",
				url: "{{route('register_process')}}",
				data: {
					_token: "{{ csrf_token() }}",
					username:		 $('#r_username').val(),
					password: 		 $('#r_password').val(),
					repeatpassword:  $('#r_repeatpassword').val(),
					code:    		 $('#r_code').val(),
					email:    		 $('#r_email').val(),
					mobile:    		 $('#r_mobile').val(),
					fullname:        $('#r_fullname').val(),
					dob:			 $('#dob').val(),
					wechat:			 $('#r_wechat').val(),
					line:			 $('#r_line').val(),
					referralid:		 $('#r_referralid').val(),
                    referralcode:	 $('#r_referralcode').val(),
					mobile_vcode:	 $("#r_mobile_vcode").val()
				},
			}).done(function( json ) {
					 $('.acctTextReg').html('');
					 obj = JSON.parse(json);
					 var str = '';
					 $.each(obj, function(i, item) {
						if( '{{Lang::get('COMMON.SUCESSFUL')}}' == item ){
							alert('{{Lang::get('public.NewMember')}}');
							window.location.href = "{{route('homepage')}}";
						}else{
							//$('.'+i+'_acctTextReg').html(item);
							str += item + '\n';
						}
					})
					if(str !== ''){
						alert(str);
					}
					
					
			});
		}
	
		function register_submit2(){
			$.ajax({
				type: "POST",
				url: "{{route('register_process')}}",
				data: {
					_token: "{{ csrf_token() }}",
					typer:		"fast",
					username:		 $('#r_username2').val(),
					password: 		 $('#r_password2').val(),
					repeatpassword:  $('#r_repeatpassword2').val(),
					mobile:    		 $('#r_mobile2').val(),
					referralcode:	 $('#r_referralcode2').val(),
				},
			}).done(function( json ) {
					 $('.acctTextReg').html('');
					 obj = JSON.parse(json);
					 var str = '';
					 $.each(obj, function(i, item) {
						if( '{{Lang::get('COMMON.SUCESSFUL')}}' == item ){
							alert('{{Lang::get('public.NewMember')}}');
							window.location.href = "{{route('homepage')}}";
						}else{
							//$('.'+i+'_acctTextReg').html(item);
							str += item + '\n';
						}
					})
					if(str !== ''){
						alert(str);
					}
					
					
			});
		}

		function inbox() {
		window.open('{{route('inbox')	}}', '', 'width=800,height=600');
		}
		// function reloadCaptcha()
		// {
		// var d=new Date();
		// $("#captcha").attr("src", "{{route('captcha', ['type' => 'login_captcha'])}}");
		// document.getElementById("code").value="";
		// }
		
@if (Auth::user()->check())
		function load_message() {
			$.ajax({
				url: "{{ route('showTotalUnseenMessage') }}",
				type: "GET",
				dataType: "text",
				data: {
				},
				success: function (result) {
					if(result>0){
						$("#totalMessage").html("<b style='color: red'>["+result+"]</b>");
						$("#totalMessage2").html("<b style='color: red'>["+result+"]</b>");
						$("#totalMessage3").html("<b style='color: red'>["+result+"]</b>");
					}else{
						$("#totalMessage").html("["+result+"]");
					}
				},
				error: function (XMLHttpRequest, textStatus, errorThrown) {
				}
			});
		}
        setInterval(load_message,20000);
		
		function update_mainwallet(){
			$.ajax({
				  type: "POST",
				  url: "{{route( 'mainwallet', [ '_token'=> csrf_token() ])}}",
				  beforeSend: function(balance){
				
				  },
				  success: function(balance){
					$('.main_wallet').html(parseFloat(Math.round(balance * 100) / 100).toFixed(2));
					total_balance += parseFloat(balance);
				  }
			 });
				
		}
		
		function getBalance(type){
			var total_balance = 0;
			$.when(
			
				 $.ajax({
							  type: "POST",
							  url: "{{route('mainwallet', [ '_token'=> csrf_token()])}}",
							  beforeSend: function(balance){
						
							  },
							  success: function(balance){
								$('.main_wallet').html(parseFloat(Math.round(balance * 100) / 100).toFixed(2));
								total_balance += parseFloat(balance);
							  }
				 }),
				@foreach( Session::get('products_obj') as $prdid => $object)
				
			
						 $.ajax({
							  type: "POST",
							  url: "{{route( 'getbalance', [ '_token'=> csrf_token(), 'product'=> $object->code ])}}&rd=<?php echo rand ( 10000, 99999 ); ?>",
							  beforeSend: function(balance){
								$('.{{$object->code}}_balance').html('<img style="" src="{{url()}}/front/img/ajax-loading.gif" width="20" height="10">');
							  },
							  success: function(balance){
								  if( balance != '{{Lang::get('Maintenance')}}')
								  {
									$('.{{$object->code}}_balance').html(parseFloat(Math.round(balance * 100) / 100).toFixed(2));
									$('.{{$object->code}}_balance2').html('<input type="text" class="{{$object->code}}_balance transferable_game_balance"  data-game-code="{{$object->code}}" value="'+parseFloat(Math.round(balance * 100) / 100).toFixed(2)+'">');
									total_balance += parseFloat(balance);
								  }
								  else
								  {
									  $('.{{$object->code}}_balance').html("{{ Lang::get('COMMON.MAINTENANCE') }}");
								  }
							  }
						 })
					  @if($prdid != Session::get('last_prdid')) 
						,
					  @endif
				@endforeach

				
			).then(function() {
				
					$('#total_balance').html(parseFloat(Math.round(total_balance * 100) / 100).toFixed(2));
					$('.total_balance').html(parseFloat(Math.round(total_balance * 100) / 100).toFixed(2));

			});

		}
@endif
@if (!empty(session('checklogin')))
	alert("{{ session('checklogin') }}");
@endif
		
	var sDate = new Date("{{date('c')}}");
    var weekstr = ["星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"];
    var monthstr = ["1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月", "11月", "12月"];
	
    function updateTime() {
        sDate = new Date(sDate.getTime() + 1000);

        var hour = parseFloat(sDate.getHours());
        var min = parseFloat(sDate.getMinutes());
        var sec = parseFloat(sDate.getSeconds());

        if (hour < 10)
            hour = "0" + hour;

        if (min < 10)
            min = "0" + min;

        if (sec < 10)
            sec = "0" + sec;

       // $("#sys_date").html(sDate.getDate()+"日" + " " + monthstr[sDate.getMonth()] + " " + sDate.getFullYear()+"年" +  ", " + weekstr[sDate.getDay()] + ", " + hour + ":" + min + ":" + sec + " (GMT +8)");
        $("#sys_date").html(sDate.getFullYear()+"年"+monthstr[sDate.getMonth()]+sDate.getDate()+"日"+" ("+weekstr[sDate.getDay()]+") "+hour+":"+min+":"+sec+" (GMT +8)");
    }
	setInterval(updateTime, 1000);
</script>
    
</head>

<body>
<!--Header-->
<style type="text/css">
*{margin:0;padding:0;list-style-type:none;}
/* leftsead */
#leftsead{width:131px;height:143px;position:fixed;top:258px;left:0px;}
*html 
#leftsead{
	margin-top:258px;
	position:absolute;
	top:expression(eval(document.documentElement.scrollTop));
}
#leftsead li{width:131px;height:64px;}
#leftsead li img{float:left;border-bottom-right-radius:5px;border-top-right-radius:5px;}
#leftsead li a{height:85px;float:left;display:block;min-width:47px;max-width:131px;}
#leftsead li a .shows{display:block;}
#leftsead li a .hides{margin-left:-166px;cursor:pointer;cursor:hand;}
#leftsead li a.youhui .hides{display:none;position:absolute;left:216px;top:2px;}
</style>
<div id="leftsead" style="z-index:9999;">
	<ul>
		<li>
			<a href="javascript:void(0)" onClick="downloadBrowser()">
				<img src="{{url()}}/gsc/img/downloadBrowser.png" style="width:80px;height:80px;"/>
			</a>
		</li>

		<li>
			<a href="javascript:void(0)" onClick="downloadDNS()">
				<img src="{{url()}}/gsc/img/downloadDNS.png" style="width:80px;height:80px;"/>
			</a>
		</li>
	</ul>
</div>

<div class="header">
  <div class="headerInner">
    <div class="topMenu">
       <div class="dateT" id="sys_date">{{date('Y年m月d日')}} {{date('H:i:s')}} (GMT +8)</div>
       <div class="langT">
		   <a href="{{route( 'language', [ 'lang'=> 'cn'])}}"><img src="{{url()}}/gsc/img/cn-icon.png" alt=""/></a>
		   <a href="{{route( 'language', [ 'lang'=> 'en'])}}"><img src="{{url()}}/gsc/img/eng-icon.png" alt=""/></a>
	   </div>
    </div>
    <div class="logo"><a href="{{route('homepage')}}"><img src="{{url()}}/gsc/img/logo.png"></a></div>
@if (Auth::user()->check())
<div class="headerLeftIn">
<div class="adj2">
	<table width="auto" border="0" cellspacing="0" cellpadding="0">
	  <tr>
		<td valign="middle">
			<div class="welc">
				<em class="fa fa-user"></em>{{Lang::get('public.Welcome')}} {{Session::get('username')}}<em style="margin-left:15px;" class="fa fa-ellipsis-v fa-1x"></em>
			</div>
		</td>
		<td valign="middle">
			<div class="welc1">
				<a href="#" data-dropdown="#dropdown-0"><i class="fa fa-caret-square-o-right"></i>{{Lang::get('public.Profile')}}&nbsp;<b id="totalMessage2"></b></a>
			</div>
			<div id="dropdown-0" class="dropdown dropdown-tip " >
				 <ul class="dropdown-menu">
					<li><a href="#" onclick="inbox();">{{Lang::get('public.Inbox')}}&nbsp;<b id="totalMessage"></b></a></li>
					<li><a href="{{route('update-profile')}}">{{Lang::get('public.MyAccountDetail')}}</a></li>
					<li><a href="{{route('update-password')}}">{{Lang::get('public.ChangePassword')}}</a></li>
				 </ul>
			</div>
		</td>
		<td valign="middle">
			<div class="welc1">
				<a href="#" data-dropdown="#dropdown-1" onClick="getBalance()"><i class="fa fa-money"></i>{{Lang::get('public.Balance')}}</a>
			</div>
			<div id="dropdown-1" class="dropdown dropdown-tip " >
				 <ul class="dropdown-menu">
					<li class="noAttr">
						<div class="bal">
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<th><strong>{{Lang::get('public.Wallet')}}</strong></th>
										<th><strong>{{Lang::get('public.Balance')}}</strong></th>
									</tr>
									<tr>
										<td>{{Lang::get('public.MainWallet')}}</td>
										<td class="main_wallet">0.0</td>
									</tr>
								@foreach( Session::get('products_obj') as $prdid => $object)
									<tr>
										<td>{{$object->getNameWithMultiLang(Lang::getLocale(), false)}}</td>
										<td class="{{$object->code}}_balance" >0.00</td>
									</tr>
								@endforeach
									<tr>
										<th><strong>{{Lang::get('public.Total')}}</strong></th>
										<th><strong id="total_balance">00.00</strong></th>
									</tr>
							</table>
						</div>
					<div class="clr"></div>
					</li>
				 </ul>
			</div>
		</td>
		<td valign="middle">
			<div class="welc1"> <a href="#" data-dropdown="#dropdown-2"><i class="fa fa-calculator"></i>{{Lang::get('public.Fund')}}</a></div>
			<div id="dropdown-2" class="dropdown dropdown-tip " >
				<ul class="dropdown-menu">
					<li><a href="{{route('deposit')}}">{{Lang::get('public.Deposit')}}</a></li>
					<li><a href="{{route('withdraw')}}">{{Lang::get('public.Withdrawal')}}</a></li>
					<li><a href="{{route('transaction')}}">{{Lang::get('public.TransactionHistory')}}</a></li>
					<li><a href="{{route('transfer')}}">{{Lang::get('public.Transfer2')}} </a></li>
					<li><a href="{{route('transfertomain')}}">{{Lang::get('public.TransferToMain')}} </a></li>
				</ul>
			</div>
		</td>
			 <td>
				<div class="welc1"> <a href="{{route('logout')}}"><i class="fa fa-sign-out"></i>{{Lang::get('public.Logout') }}</a></div>
			 </td>
	  </tr>
	</table>
</div>
<div class="loginBtn adj11"><a href="{{route('deposit')}}">{{Lang::get('public.DepositNow') }}</a></div> 
<div class="clr"></div>

</div>
@else
    <div class="headerLeft">
	<form>
      <div class="logRow inp">
      <input type="text" placeholder="{{ Lang::get('public.Username') }}" id="username" name="username" onKeyPress="enterpressalert(event, this)">
	  <input class="adj08" type="password" placeholder="{{ Lang::get('public.Password') }}" id="password" name="password" onKeyPress="enterpressalert(event, this)">
	  <a href="{{route('forgotpassword')}}" style="text-decoration:none;color:white;font-size:10px">{{ Lang::get('public.ForgotPassword') }}</a>
      <div class="clr"></div>
      </div>
      <div class="logRow">
      <div class="codeW">
        <div class="txt02">{{ Lang::get('public.Code') }}</div>
        <div class="input01"><input type="text" placeholder="" maxlength="4" id="code" onKeyPress="enterpressalert(event, this)">
		<div class="clr"></div>
		</div>
      </div>
		<img class="smll" style="height:29px;width:73px" src="{{route('captcha', ['type' => 'login_captcha'])}}">
      <div class="btnW adj08">
           <div class="loginBtn" style="margin-left:6px;">
				<a href="#" onclick="login();">{{ Lang::get('public.Login') }}</a>
		   </div> 
	</form>
           <div class="joinBtn">
				<a href="#register">{{ Lang::get('public.Register') }}</a>
		   </div> 
        </div>
      <div class="clr"></div>
      </div>
    </div>
@endif
      
    <div class="clr"></div>


<div class="navCont">
	<div class="navContInner">
		<ul>
			<li><a href="{{route('homepage')}}">{{Lang::get('public.Home')}}</a></li>
			<li><a href="{{route('mnt')}}">{{Lang::get('public.FishingGames')}}</a></li>
			<li><a href="{{route('slot', [ 'type' => 'plt' ] )}}">{{Lang::get('public.SlotGames2')}}</a></li>
			<li><a href="{{route('livecasino')}}">{{Lang::get('public.LiveCasino2')}}</a></li>
			<li><a href="{{route('shishicai')}}">{{Lang::get('public.Lottery')}}</a></li>
			<li><a href="{{route('ibc')}}">{{Lang::get('public.SportsBook')}}</a></li>
			<li><a href="{{route('promotion')}}">{{Lang::get('public.Promotion')}}</a></li>
			<li><a href="{{route('mobile')}}">{{Lang::get('public.MobileDownload2')}}</a></li>
			<li><a href="{{route('contactus')}}">{{Lang::get('public.ContactUs')}}</a></li>
		</ul>
	</div>
</div>
</div>

</div>

<div class="anmnt">
<div class="anmntInner" onclick="window.open('{{route('announcement')}}?lang={{Lang::getLocale()}}', 'Notice', 'width=800,height=600');">
<span><img src="{{url()}}/gsc/img/speaker.png"></span>
<marquee>{{ App\Http\Controllers\User\AnnouncementController::index() }}</marquee>
</div>
</div>

@yield('content')

<div class="logos">
   <div class="logosInner">
      <div class="logosLeft">
         <h3>游戏合作伙伴</h3>
         <img src="{{url()}}/gsc/img/pt-btm.png" width="170">
         <img src="{{url()}}/gsc/img/allb-btm.png" width="170">
         <img src="{{url()}}/gsc/img/bb-btm.png" width="170">
         <img src="{{url()}}/gsc/img/ag-btm.png" width="170">
      </div>
     
   </div>
</div>

<!-- Live800在线客服图标:Default icons[浮动图标]开始-->
<div style='display:none;'><a href='https://www.live800.com'>客服系统</a></div><script language="javascript" src="https://chat.livechatvalue.com/chat/chatClient/floatButton.js?jid=6705510837&companyID=905849&configID=54544&codeType=custom&ss=1"></script><div style='display:none;'><a href='https://en.live800.com'>live chat</a></div>
<!-- 在线客服图标:Default icons结束-->

<!--Foooter-->
<div class="footer">
<div class="footerInner">
<div class="footerLeft">
<h3>支付平台</h3>
<ul>
<li class="nM"><img src="{{url()}}/gsc/img/icon-alipay.png"></li>
<li><img src="{{url()}}/gsc/img/payment-2.png"></li>
<li><img src="{{url()}}/gsc/img/payment-3.png"></li>
</ul>
</div>
<div class="footerRight">
<h3>支持的浏览器</h3>
<ul>
<li class="nM"><img src="{{url()}}/gsc/img/icon-ie360.png"></li>
<li><img src="{{url()}}/gsc/img/icon-chrome.png"></li>
</ul>
</div>


<div class="clr"></div>


</div>

<div class="footerBtm">
<div class="footerBtmInner">
<div class="ftrL">版权 @ 金沙会. 版权所有</div>

<div class="footerMenu">
   <ul>
	   <li><a href="{{route('aboutus')}}">{{Lang::get('public.AboutUs')}}</a></li>
	   <li><a href="{{route('responsible')}}">{{Lang::get('public.ResponsibleGaming')}}</a></li>
	   <li><a href="{{route('tnc')}}">{{Lang::get('public.AgreementStatementRules')}}</a></li>
	   <li><a href="{{route('faq')}}">{{Lang::get('public.FAQ')}}</a></li>
   </ul>
</div>

<div class="clr"></div>
</div>
</div>

</div>
@if (!Auth::user()->check())  
<div class="remodal theme1" data-remodal-id="register" role="dialog" aria-labelledby="modal1Title" aria-describedby="modal1Desc">
  <button data-remodal-action="close" class="remodal-close" aria-label="Close"></button>
  <div style="border-bottom:2px;">
	<div class="tab">
		<button class="tablinks" id="normal" onClick="registerButton('nor')"><h2 id="modal1Title" class="normalClass" style="color:red;">{{Lang::get('public.Registration')}}</h2></button>
		<button class="tablinks" id="fast" onClick="registerButton('fas')"><h2 id="modal1Title"  class="fastClass">快速注册</h2></button>
	</div>
  </div>
  <div id="normalRegister">
    <h2 id="modal1Title">{{Lang::get('public.Registration')}}</h2>
    <p id="modal1Desc">
      {{Lang::get('public.RegisterNote1')}}
    </p>
    <p>
    <div class="form">
    <div class="rowed1">
       <label style="color:#FF0000;">{{Lang::get('public.EmailAddress')}}*:</label>
      <input  class="form-control" id="r_email" type="email"><span class="email_acctTextReg acctTextReg"></span>
    </div>
	<div class="note" style="color:#FF0000;">
      *{{Lang::get('public.UseToFindPasswordByEmail')}}
    </div>
    <div class="rowed1">
       <label style="color:#FF0000;">{{Lang::get('public.ContactNo')}}*:</label>
       <input  class="form-control" id="r_mobile" type="tel"><span class="mobile_acctTextReg acctTextReg"></span>
    </div>
    <div class="rowed1">
       <label>{{Lang::get('public.Currency')}}:</label>
       <span class="formTxt">CNY</span>
    </div>
    <div class="rowed1">
       <label style="color:#FF0000;">{{Lang::get('public.FullName')}}*:</label>
        <input  class="form-control" id="r_fullname" type="text"><span class="fullname_acctTextReg acctTextReg"></span>
    </div>
    <div class="rowed1">
    <div class="note" style="color:#FF0000;">
      *{{Lang::get('public.NameMatchWithrawalBank')}}
    </div>
    </div>
	  <br>
    <div class="rowed1">
       <label>{{Lang::get('public.DOB')}}</label>
	    <select id="dob_year" class="short combine">
			<option value="2006">2006</option>
			<option value="2005">2005</option>
			<option value="2004">2004</option>
			<option value="2003">2003</option>
			<option value="2002">2002</option>
			<option value="2001">2001</option>
			<option value="2000">2000</option>
			<option value="1999">1999</option>
			<option value="1998">1998</option>
			<option value="1997">1997</option>
			<option value="1996">1996</option>
			<option value="1995">1995</option>
			<option value="1994">1994</option>
			<option value="1993">1993</option>
			<option value="1992">1992</option>
			<option value="1991">1991</option>
			<option value="1990">1990</option>
			<option value="1989">1989</option>
			<option value="1988">1988</option>
			<option value="1987">1987</option>
			<option value="1986">1986</option>
			<option value="1985">1985</option>
			<option value="1984">1984</option>
			<option value="1983">1983</option>
			<option value="1982">1982</option>
			<option value="1981">1981</option>
			<option value="1980">1980</option>
			<option value="1979">1979</option>
			<option value="1978">1978</option>
			<option value="1977">1977</option>
			<option value="1976">1976</option>
			<option value="1975">1975</option>
			<option value="1974">1974</option>
			<option value="1973">1973</option>
			<option value="1972">1972</option>
			<option value="1971">1971</option>
			<option value="1970">1970</option>
			<option value="1969">1969</option>
			<option value="1968">1968</option>
			<option value="1967">1967</option>
			<option value="1966">1966</option>
			<option value="1965">1965</option>
			<option value="1964">1964</option>
			<option value="1963">1963</option>
			<option value="1962">1962</option>
			<option value="1961">1961</option>
			<option value="1960">1960</option>	
		</select>
		<select id="dob_month" class="short combine">
			<option value="01">{{Lang::get('public.January')}}</option>
			<option value="02">{{Lang::get('public.February')}}</option>
			<option value="03">{{Lang::get('public.March')}}</option>
			<option value="04">{{Lang::get('public.April')}}</option>
			<option value="05">{{Lang::get('public.May')}}</option>
			<option value="06">{{Lang::get('public.June')}}</option>
			<option value="07">{{Lang::get('public.July')}}</option>
			<option value="08">{{Lang::get('public.August')}}</option>
			<option value="09">{{Lang::get('public.September')}}</option>
			<option value="10">{{Lang::get('public.October')}}</option>
			<option value="11">{{Lang::get('public.November')}}</option>
			<option value="12">{{Lang::get('public.December')}}</option>
		</select>
		<select id="dob_day" class="short combine">
			<option value="01">01</option>
			<option value="02">02</option>
			<option value="03">03</option>
			<option value="04">04</option>
			<option value="05">05</option>
			<option value="06">06</option>
			<option value="07">07</option>
			<option value="08">08</option>
			<option value="09">09</option>
			<option value="10">10</option>
			<option value="11">11</option>
			<option value="12">12</option>
			<option value="13">13</option>
			<option value="14">14</option>
			<option value="15">15</option>
			<option value="16">16</option>
			<option value="17">17</option>
			<option value="18">18</option>
			<option value="19">19</option>
			<option value="20">20</option>
			<option value="21">21</option>
			<option value="22">22</option>
			<option value="23">23</option>
			<option value="24">24</option>
			<option value="25">25</option>
			<option value="26">26</option>
			<option value="27">27</option>
			<option value="28">28</option>
			<option value="29">29</option>
			<option value="30">30</option>
			<option value="31">31</option>
		</select>
    </div>
	<input type="hidden" id="dob">
    <div class="rowed1">
       <label style="color:#FF0000;">{{Lang::get('public.Username')}}*:</label>
       <input  class="form-control" id="r_username" type="text"><span class="username_acctTextReg acctTextReg"></span>
    </div>
    <div class="rowed1">
    <div class="note">
      *{{Lang::get('public.UsernamePolicyContent')}}
    </div>
    </div>
	<br>
    <div class="rowed1">
       <label style="color:#FF0000;">{{Lang::get('public.Password')}}*:</label>
       <input  class="form-control" id="r_password" type="password"><span class="password_acctTextReg acctTextReg"></span>
    </div>
    <div class="rowed1">
    </div>
    <div class="rowed1">
       <label style="color:#FF0000;">{{Lang::get('public.RepeatPassword')}}:</label>
       <input  class="form-control" id="r_repeatpassword" type="password"><span class="acctTextReg"></span>
    </div>
    <div class="note">
      *{{Lang::get('public.PasswordPolicyContent')}}
    </div><br>
	<div class="rowed1">
		<label>{{Lang::get('public.RegisterCode')}}*:</label>
		@if (Session::has('user_refcode'))
			<input class="form-control" type="text" value="{{Session::get('user_refcode')}}" disabled>
			<input id="r_referralcode" type="hidden" value="{{Session::get('user_refcode')}}">
		@else
			<input class="form-control" id="r_referralcode" type="text" value="">
		@endif
		<span class="referralcode_acctTextReg acctTextReg"></span>
	</div>
    </div>
	<br>
    </p>
  <br>
  <button data-remodal-action="confirm" class="remodal-confirm" onClick="register_submit()" > {{Lang::get('public.Submit')}}</button>
  </div>
  <div id="fastRegister" style="height:700px;">
  <h2 id="modal1Title">{{Lang::get('public.Registration')}}</h2>
    <p id="modal1Desc">
      {{Lang::get('public.RegisterNote1')}}
    </p>
    <p>
    <div class="form">
    <div class="rowed1">
       <label style="color:#FF0000;">{{Lang::get('public.Username')}}*:</label>
      <input  class="form-control" id="r_username2" type="text"><span class="email_acctTextReg acctTextReg"></span>
    </div><br>
    <div class="rowed1">
       <label style="color:#FF0000;">{{Lang::get('public.ContactNo')}}*:</label>
       <input  class="form-control" id="r_mobile2" type="tel"><span class="mobile_acctTextReg acctTextReg"></span>
    </div><br>
	<div class="rowed1">
       <label style="color:#FF0000;">{{Lang::get('public.Password')}}*:</label>
       <input  class="form-control" id="r_password2" type="password"><span class="password_acctTextReg acctTextReg"></span>
    </div><br>
	<div class="rowed1">
       <label style="color:#FF0000;">{{Lang::get('public.RepeatPassword')}}:</label>
       <input  class="form-control" id="r_repeatpassword2" type="password"><span class="acctTextReg"></span>
    </div><br>
	<div class="rowed1">
		<label>{{Lang::get('public.RegisterCode')}}:</label>
		@if (Session::has('user_refcode'))
			<input class="form-control" type="text" value="{{Session::get('user_refcode')}}" disabled>
			<input id="r_referralcode2" type="hidden" value="{{Session::get('user_refcode')}}">
		@else
			<input class="form-control" id="r_referralcode2" type="text" value="">
		@endif
		<span class="referralcode_acctTextReg acctTextReg"></span>
    </div>
  </div>
  <div style="margin-top:100px;">
	<button data-remodal-action="confirm" class="remodal-confirm" onClick="register_submit2()" > {{Lang::get('public.Submit')}}</button>
  </div>
  </div>
</div> 
@endif
<script>
$('#demoprogressbar14').LineProgressbar({
  percentage: 55,
  fillBackgroundColor: '#5b3703',
  height: '20px',
  radius: '10px'
});
</script>
<script>
$('#demoprogressbar15').LineProgressbar({
  percentage: 35,
  fillBackgroundColor: '#5b3703',
  height: '20px',
  radius: '10px'
});
</script>
<script>
$('#demoprogressbar16').LineProgressbar({
  percentage: 75,
  fillBackgroundColor: '#5b3703',
  height: '20px',
  radius: '10px'
});
</script>

<script type="text/javascript">
$('.bxslider').bxSlider({
  auto: true,
  autoControls: true,
});
  $(document).on('opening', '.remodal', function () {
    console.log('opening');
  });

  $(document).on('opened', '.remodal', function () {
    console.log('opened');
  });

  $(document).on('closing', '.remodal', function (e) {
    console.log('closing' + (e.reason ? ', reason: ' + e.reason : ''));
  });

  $(document).on('closed', '.remodal', function (e) {
    console.log('closed' + (e.reason ? ', reason: ' + e.reason : ''));
  });

  $(document).on('confirmation', '.remodal', function () {
    console.log('confirmation');
  });

  $(document).on('cancellation', '.remodal', function () {
    console.log('cancellation');
  });

//  Usage:
//  $(function() {
//
//    // In this case the initialization function returns the already created instance
//    var inst = $('[data-remodal-id=modal]').remodal();
//
//    inst.open();
//    inst.close();
//    inst.getState();
//    inst.destroy();
//  });

  //  The second way to initialize:
  $('[data-remodal-id=modal2]').remodal({
    modifier: 'with-red-theme'
  });
</script>

@yield('modal')

</body>
</html>
