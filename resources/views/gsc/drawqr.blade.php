<html>
<head>
    <title></title>

    <style type="text/css">
        body * {
            font-family: Arial, Helvetica, sans-serif, "Microsoft YaHei", "微软雅黑", STXihei, "华文细黑", serif;
            font-size: 1.2em;
        }
    </style>
</head>
<body>
<div style="text-align: center;">
    <div id="qrcode"></div>
    <br>
    <span id="qrlabel" style="display: none; padding-top: 15px;"></span>
</div>
</body>

<script type="text/javascript" src="{{ asset('/front/resources/js/jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/front/resources/js/jquery.qrcode.min.js') }}"></script>
<script type="text/javascript">
    $(function () {
        $("#qrcode").qrcode({
            width: 256,
            height: 256,
            text: "{{ $url }}"
        });

        var labelStr = "{{ $label }}";

        if (labelStr.length > 0) {
            var labelObj = $("#qrlabel");

            labelObj.html(labelStr);
            labelObj.show();
        }
    });
</script>

</html>
