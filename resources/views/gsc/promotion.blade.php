@extends('gsc/master')

@section('title', Lang::get('public.Promotion'))

@section('content')
   <script type="text/javascript">
		$(document).ready(function($) {
			$('#accordion').find('.accordion-toggle').click(function(){
	
				//Expand or collapse this panel
				$(this).next().slideToggle('fast');
	
				//Hide the other panels
				$(".accordion-content").not($(this).next()).slideUp('fast');
	
			});
		});
	</script> 
	
<div class="promoTop">
   <div class="promoTopinner">
    <div class="slotTitle">{{Lang::get('public.Promotion')}}</div>
    <div class="promoBlock" id="accordion">
	@foreach ($promo as $key => $value )
		<h4 class="accordion-toggle">
			<img src="{{$value['image']}}">
		</h4>
		<div class="accordion-content default">
			<p>
			<?php echo htmlspecialchars_decode($value['content']); ?>
			</p>
		</div>
	@endforeach
    </div>
   </div>
</div>
@stop