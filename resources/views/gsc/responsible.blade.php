@extends('gsc/master')

@section('title', Lang::get('public.ResponsibleGaming'))

@section('content')
<div class="promoTop">
   <div class="promoTopinner">
    <div class="slotTitle">{{Lang::get('public.ResponsibleGaming')}}</div>
    <div class="promoBlock" id="accordion">
		<?php echo htmlspecialchars_decode($content); ?>
    </div>
   </div>
</div>
@stop