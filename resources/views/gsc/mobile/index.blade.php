@extends('gsc/mobile/master')
@section('content')
    <script src="{{url()}}/gsc/mobile/resources/js/jssor.slider-21.1.6.mini.js" type="text/javascript"></script>
    <script type="text/javascript">
        jQuery(document).ready(function ($) {

            var jssor_1_SlideoTransitions = [
              [{b:-1,d:1,o:-1},{b:0,d:1000,o:1}],
              [{b:1900,d:2000,x:-379,e:{x:7}}],
              [{b:1900,d:2000,x:-379,e:{x:7}}],
              [{b:-1,d:1,o:-1,r:288,sX:9,sY:9},{b:1000,d:900,x:-1400,y:-660,o:1,r:-288,sX:-9,sY:-9,e:{r:6}},{b:1900,d:1600,x:-200,o:-1,e:{x:16}}]
            ];

            var jssor_1_options = {
              $AutoPlay: true,
              $SlideDuration: 800,
              $SlideEasing: $Jease$.$OutQuint,
              $CaptionSliderOptions: {
                $Class: $JssorCaptionSlideo$,
                $Transitions: jssor_1_SlideoTransitions
              },
              $ArrowNavigatorOptions: {
                $Class: $JssorArrowNavigator$
              },
              $BulletNavigatorOptions: {
                $Class: $JssorBulletNavigator$
              }
            };

            var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

            /*responsive code begin*/
            /*you can remove responsive code if you don't want the slider scales while window resizing*/
            function ScaleSlider() {
                var refSize = jssor_1_slider.$Elmt.parentNode.clientWidth;
                if (refSize) {
                    refSize = Math.min(refSize, 1920);
                    jssor_1_slider.$ScaleWidth(refSize);
                }
                else {
                    window.setTimeout(ScaleSlider, 30);
                }
            }
            ScaleSlider();
            $(window).bind("load", ScaleSlider);
            $(window).bind("resize", ScaleSlider);
            $(window).bind("orientationchange", ScaleSlider);
            /*responsive code end*/
        });
    </script>
    
    <script>
	// Instantiate the Bootstrap carousel
$('.multi-item-carousel').carousel({
  interval: false
});

// for every slide in carousel, copy the next slide's item in the slide.
// Do the same for the next, next item.
$('.multi-item-carousel .item').each(function(){
  var next = $(this).next();
  if (!next.length) {
    next = $(this).siblings(':first');
  }
  next.children(':first-child').clone().appendTo($(this));
  
  if (next.next().length>0) {
    next.next().children(':first-child').clone().appendTo($(this));
  } else {
  	$(this).siblings(':first').children(':first-child').clone().appendTo($(this));
  }
});
	</script>
    
    <script>
	// Instantiate the Bootstrap carousel
$('.multi-item-carousel1').carousel1({
  interval: false
});

// for every slide in carousel, copy the next slide's item in the slide.
// Do the same for the next, next item.
$('.multi-item-carousel1 .item').each(function(){
  var next = $(this).next();
  if (!next.length) {
    next = $(this).siblings(':first');
  }
  next.children(':first-child').clone().appendTo($(this));
  
  if (next.next().length>0) {
    next.next().children(':first-child').clone().appendTo($(this));
  } else {
  	$(this).siblings(':first').children(':first-child').clone().appendTo($(this));
  }
});
	</script>
<div id="jssor_1" style="position: relative; margin: 0 auto; top: 0px; left: 0px; width: 1069px; height: 382px; overflow: hidden; visibility: hidden;">
        <!-- Loading Screen -->
        <div data-u="loading" style="position: absolute; top: 0px; left: 0px;">
            <div style="filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block; top: 0px; left: 0px; width: 100%; height: 100%;"></div>
            <div style="position:absolute;display:block;background:url('{{url()}}/gsc/mobile/resources/img/loading.gif') no-repeat center center;top:0px;left:0px;width:100%;height:100%;"></div>
        </div>
        <div data-u="slides" style="cursor: default; position: relative; top: 0px; left: 0px; width: 1069px; height: 382px; overflow: hidden;">
	@if(isset($banners))
		@foreach( $banners as $key => $banner )
			<div data-p="225.00">
                <a href="{{route('promotion')}}"><img data-u="image" src="{{$banner->domain}}/{{$banner->path}}" /></a>
            </div>
		@endforeach
	@endif
            
        </div>
        <!-- Bullet Navigator -->
        <div data-u="navigator" class="jssorb05" style="bottom:26px;right:16px;" data-autocenter="1">
            <!-- bullet navigator item prototype -->
            <div data-u="prototype" style="width:16px;height:16px;"></div>
        </div>
        <!-- Arrow Navigator -->
        <span data-u="arrowleft" class="jssora22l" style="top:-50px !important;left:8px;width:40px;height:58px;" data-autocenter="2"></span>
        <span data-u="arrowright" class="jssora22r" style="top:0px;right:8px;width:40px;height:58px;" data-autocenter="2"></span>
    </div>
<div class="row pad01 bgB padS">
	<div class="col-md-12">
		<div class="row">
			<div class="col-lg-3 col-sm-3 col-xs-3 lyrTop text-center">
				<a href="#" onClick="@if (Auth::user()->check())window.open('{{route('agg')}}', 'casinoagg', 'width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif">
					<img class="img-responsive img-portfolio img-hover" src="{{url()}}/gsc/mobile/resources/img/hpg-thumb-9.png" alt="">
					<div class="gmTxt">亚游(AG)</div>
				</a>
			</div>
			
			<div class="col-lg-3 col-sm-3 col-xs-3 lyrTop text-center">
				<a href="#" onClick="@if (Auth::user()->check())window.open('{{route('bbn')}}', 'casinobbn', 'width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif">
					<img class="img-responsive img-portfolio img-hover" src="{{url()}}/gsc/mobile/resources/img/hpg-thumb-8.png" alt="">
					<div class="gmTxt">宝盈(BBIN)</div>
				</a>
			</div>
			
			<!--
			
			<div class="col-lg-3 col-sm-3 col-xs-3 lyrTop text-center">
				<a href="#" onClick="@if (Auth::user()->check())window.open('{{route('pltiframe')}}', 'casinoplt', 'width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif">
					<img class="img-responsive img-portfolio img-hover" src="{{url()}}/gsc/mobile/resources/img/hpg-thumb-5.png" alt="">
					<div class="gmTxt">PT</div>
				</a>
			</div>
			
			
			<div class="col-lg-3 col-sm-3 col-xs-3 lyrTop text-center">
				<a href="#" onClick="@if (Auth::user()->check())window.open('{{route('bbn_slot')}}', 'casinobbn', 'width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif">
					<img class="img-responsive img-portfolio img-hover" src="{{url()}}/gsc/mobile/resources/img/hpg-thumb-8.png" alt="">
					<div class="gmTxt">宝盈(BBIN)<br>角子机</div>
				</a>
			</div>
			
			<div class="col-lg-3 col-sm-3 col-xs-3 lyrTop text-center">
				<a href="#" onClick="@if (Auth::user()->check())window.open('{{route('bbn_lottery')}}', 'casinobbn', 'width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif">
					<img class="img-responsive img-portfolio img-hover" src="{{url()}}/gsc/mobile/resources/img/hpg-thumb-8.png" alt="">
					<div class="gmTxt">宝盈(BBIN)<br>时时彩</div>
				</a>
			</div>
			-->
			<div class="col-lg-3 col-sm-3 col-xs-3 lyrTop text-center">
				<a href="#" onClick="@if (Auth::user()->check())window.open('{{route('alb')}}', 'casinoalb', 'width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif">
					<img class="img-responsive img-portfolio img-hover" src="{{url()}}/gsc/mobile/resources/img/hpg-thumb-7.png" alt="">
					<div class="gmTxt">欧博(ALB)</div>
				</a>
			</div>

			<div class="col-lg-3 col-sm-3 col-xs-3 lyrTop text-center">
				<a href="#" onClick="@if (Auth::user()->check())window.open('{{route('mig_mobile')}}', 'MGslot', 'width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif">
					<img class="img-responsive img-portfolio img-hover" src="{{url()}}/gsc/mobile/resources/img/hpg-thumb-6.png" alt="">
					<div class="gmTxt">MG角子机</div>
				</a>
			</div>
			

			
			<div class="col-lg-3 col-sm-3 col-xs-3 lyrTop text-center">
				<a href="#" onClick="@if (Auth::user()->check())window.open('{{ route('spg', [ 'type' => 'slot' , 'category' => 'slot' ] ) }}', 'SPGslot', 'width=450,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif">
					<img class="img-responsive img-portfolio img-hover" src="{{url()}}/gsc/mobile/resources/img/hpg-thumb-spec.png" alt="">
					<div class="gmTxt">新霸<br>角子机</div>
				</a>
			</div>
	
			<!--
			
			<div class="col-lg-3 col-sm-3 col-xs-3 lyrTop text-center">
				<a href="#" onClick="@if (Auth::user()->check())window.open('{{route('slot', [ 'type' => 'plt' ] )}}', 'plt', 'width=450,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif">
					<img class="img-responsive img-portfolio img-hover" src="{{url()}}/gsc/mobile/resources/img/hpg-thumb-5.png" alt="">
					<div class="gmTxt">PT角子机</div>
				</a>
			</div>
			<div class="col-lg-3 col-sm-3 col-xs-3 lyrTop text-center">
				<a href="#" onclick="location.href ='{{route('mnt')}}'">
					<img class="img-responsive img-portfolio img-hover" src="{{url()}}/gsc/mobile/resources/img/hpg-thumb-1.png" alt="">
					<div class="gmTxt">{{Lang::get('public.FishingGames')}}</div>
				</a>
			</div>
			-->

			<div class="col-lg-3 col-sm-3 col-xs-3 lyrTop text-center">
				<a href="#" onclick="@if (Auth::user()->check())window.open('{{route('ibc')}}', 'ibc', 'width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif">
					<img class="img-responsive img-portfolio img-hover" src="{{url()}}/gsc/mobile/resources/img/hpg-thumb-11.png" alt="">
					<div class="gmTxt">{{Lang::get('public.SportsBook')}}</div>
				</a>
			</div>
			
			<div class="col-lg-3 col-sm-3 col-xs-3 lyrTop text-center">
				<a href="javascript:void(0)" onClick="@if (Auth::user()->check())window.open('{{route('ilt')}}', 'Lottery', 'width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif">
					<img class="img-responsive img-portfolio img-hover" src="{{url()}}/gsc/mobile/resources/img/hpg-thumb-10.png" alt="">
					<div class="gmTxt">{{Lang::get('public.Lottery')}}</div>
				</a>
			</div>
			
			<div class="col-lg-3 col-sm-3 col-xs-3 lyrTop text-center">
				<a href="{{route('promotion')}}">
					<img class="img-responsive img-portfolio img-hover" src="{{url()}}/gsc/mobile/resources/img/hpg-thumb-12.png" alt="">
					<div class="gmTxt">{{Lang::get('public.Promotion')}}</div>
				</a>
			</div>
		</div>
	</div>
</div>

@stop