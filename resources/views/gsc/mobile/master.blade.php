<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>GSC</title>

    <!-- Bootstrap Core CSS -->
    <link href="{{url()}}/gsc/mobile/resources/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="{{url()}}/gsc/mobile/resources/css/modern-business.css" rel="stylesheet">
    <link href="{{url()}}/gsc/mobile/resources/css/custom.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="{{url()}}/gsc/mobile/resources/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!-- jQuery -->
    <script src="{{url()}}/gsc/mobile/resources/js/jquery-1.11.3.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="{{url()}}/gsc/mobile/resources/js/bootstrap.min.js"></script>

    <!-- Script to Activate the Carousel -->
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
<script type="text/javascript">
		$(function () {
			@if (Auth::user()->check())
						load_message();
			@endif
			
			$('.combine').on('change', function(){
				var date = $('#dob_year').val() + '-' + $('#dob_month').val() + '-' + $('#dob_day').val();
				$('#dob').val(date);
			});
			
			$("#normal").change(function(){
				$("#normalRegister").show();
				$("#fastRegister").hide();
			}).change();
		});

		function registerButton(button){
			if(button=='nor'){
				$("#normalRegister").show();
				$("#fastRegister").hide();
				$(".normalClass").css("color", "red");
				$(".fastClass").css("color", "white");
			}else if(button=='fas'){
				$("#fastRegister").show();
				$("#normalRegister").hide();
				$(".fastClass").css("color", "red");
				$(".normalClass").css("color", "white");
			}
		}

		
		function enterpressalert2(e, textarea){
			var code = (e.keyCode ? e.keyCode : e.which);
			if(code == 13) { //Enter keycode
			   login2();
			 }
		}

		
		function login2() {
			document.getElementById("loginBtnAction2").style.display = "none";
			document.getElementById("loading2").style.display = "block";
				$.ajax({
					type: "POST",
					url: "{{route('login')}}",
					data: {
						_token: "{{ csrf_token() }}",
						username: $('#username2').val(),
						password: $('#password2').val(),
						code:     $('#code2').val(),
					},
				}).done(function(json) {
					obj = JSON.parse(json);
					var str = '';
					$.each(obj, function(i, item) {
						str += item + '\n';
					});
					if ("{{Lang::get('COMMON.SUCESSFUL')}}\n" == str){
						location.reload();
					} else {
						if(str=="{{Lang::get('validation.wrong_captcha')}}\n"){
							alert(str);
							location.reload();
						}else{
							alert(str);
						}
						document.getElementById("loginBtnAction2").style.display = "block";
						document.getElementById("loading2").style.display = "none";
					}
				});
        }
		
		function register_submit(){
			$.ajax({
				type: "POST",
				url: "{{route('register_process')}}",
				data: {
					_token: "{{ csrf_token() }}",
					username:		 $('#r_username').val(),
					password: 		 $('#r_password').val(),
					repeatpassword:  $('#r_repeatpassword').val(),
					code:    		 $('#r_code').val(),
					email:    		 $('#r_email').val(),
					mobile:    		 $('#r_mobile').val(),
					fullname:        $('#r_fullname').val(),
					dob:			 $('#dob').val(),
					wechat:			 $('#r_wechat').val(),
					line:			 $('#r_line').val(),
					referralid:		 $('#r_referralid').val(),
                    referralcode:	 $('#r_referralcode').val(),
					mobile_vcode:	 $("#r_mobile_vcode").val()
				},
			}).done(function( json ) {
					 $('.acctTextReg').html('');
					 obj = JSON.parse(json);
					 var str = '';
					 $.each(obj, function(i, item) {
						if( '{{Lang::get('COMMON.SUCESSFUL')}}' == item ){
							alert('{{Lang::get('public.NewMember')}}');
							window.location.href = "{{route('homepage')}}";
						}else{
							//$('.'+i+'_acctTextReg').html(item);
							str += item + '\n';
						}
					})
					if(str !== ''){
						alert(str);
					}
					
					
			});
		}
	
		function register_submit2(){
			$.ajax({
				type: "POST",
				url: "{{route('register_process')}}",
				data: {
					_token: "{{ csrf_token() }}",
					typer:		"fast",
					username:		 $('#r_username2').val(),
					password: 		 $('#r_password2').val(),
					repeatpassword:  $('#r_repeatpassword2').val(),
					mobile:    		 $('#r_mobile2').val(),
					referralcode:	 $('#r_referralcode2').val(),
				},
			}).done(function( json ) {
					 $('.acctTextReg').html('');
					 obj = JSON.parse(json);
					 var str = '';
					 $.each(obj, function(i, item) {
						if( '{{Lang::get('COMMON.SUCESSFUL')}}' == item ){
							alert('{{Lang::get('public.NewMember')}}');
							window.location.href = "{{route('homepage')}}";
						}else{
							//$('.'+i+'_acctTextReg').html(item);
							str += item + '\n';
						}
					})
					if(str !== ''){
						alert(str);
					}
					
					
			});
		}

		function inbox() {
		window.open('{{route('inbox')	}}', '', 'width=800,height=600');
		}
		// function reloadCaptcha()
		// {
		// var d=new Date();
		// $("#captcha").attr("src", "{{route('captcha', ['type' => 'login_captcha'])}}");
		// document.getElementById("code").value="";
		// }
		
@if (Auth::user()->check())
		function load_message() {
			$.ajax({
				url: "{{ route('showTotalUnseenMessage') }}",
				type: "GET",
				dataType: "text",
				data: {
				},
				success: function (result) {
					if(result>0){
						$("#totalMessage").html("<b style='color: red'>["+result+"]</b>");
						$("#totalMessage2").html("<b style='color: red'>["+result+"]</b>");
						$("#totalMessage3").html("<b style='color: red'>["+result+"]</b>");
					}else{
						$("#totalMessage").html("["+result+"]");
					}
				},
				error: function (XMLHttpRequest, textStatus, errorThrown) {
				}
			});
		}
        setInterval(load_message,20000);
		
		function update_mainwallet(){
			$.ajax({
				  type: "POST",
				  url: "{{route( 'mainwallet', [ '_token'=> csrf_token() ])}}",
				  beforeSend: function(balance){
				
				  },
				  success: function(balance){
					$('.main_wallet').html(parseFloat(Math.round(balance * 100) / 100).toFixed(2));
					total_balance += parseFloat(balance);
				  }
			 });
				
		}
		
		function getBalance(type){
			var total_balance = 0;
			$.when(
			
				 $.ajax({
							  type: "POST",
							  url: "{{route('mainwallet', [ '_token'=> csrf_token()])}}",
							  beforeSend: function(balance){
						
							  },
							  success: function(balance){
								$('.main_wallet').html(parseFloat(Math.round(balance * 100) / 100).toFixed(2));
								total_balance += parseFloat(balance);
							  }
				 }),
				@foreach( Session::get('products_obj') as $prdid => $object)
				
			
						 $.ajax({
							  type: "POST",
							  url: "{{route( 'getbalance', [ '_token'=> csrf_token(), 'product'=> $object->code ])}}&rd=<?php echo rand ( 10000, 99999 ); ?>",
							  beforeSend: function(balance){
								$('.{{$object->code}}_balance').html('<img style="" src="{{url()}}/front/img/ajax-loading.gif" width="20" height="10">');
								$('.{{$object->code}}_balance2').html('<img style="" src="{{url()}}/front/img/ajax-loading.gif" width="20" height="10">');
							  },
							  success: function(balance){
								  if( balance != '{{Lang::get('Maintenance')}}')
								  {
									$('.{{$object->code}}_balance').html(parseFloat(Math.round(balance * 100) / 100).toFixed(2));
									$('.{{$object->code}}_balance2').html('<input type="text" class="{{$object->code}}_balance transferable_game_balance"  data-game-code="{{$object->code}}" value="'+parseFloat(Math.round(balance * 100) / 100).toFixed(2)+'" readonly>');
									total_balance += parseFloat(balance);
								  }
								  else
								  {
									  $('.{{$object->code}}_balance').html("{{ Lang::get('COMMON.MAINTENANCE') }}");
								  }
							  }
						 })
					  @if($prdid != Session::get('last_prdid')) 
						,
					  @endif
				@endforeach

				
			).then(function() {
				
					$('#total_balance').html(parseFloat(Math.round(total_balance * 100) / 100).toFixed(2));
					$('.total_balance').val(parseFloat(Math.round(total_balance * 100) / 100).toFixed(2));

			});

		}
@endif
@if (session('checklogin'))
	alert("{{ session('checklogin') }}");
@endif
		
	var sDate = new Date("{{date('c')}}");
    var weekstr = ["星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"];
    var monthstr = ["1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月", "11月", "12月"];
	
    function updateTime() {
        sDate = new Date(sDate.getTime() + 1000);

        var hour = parseFloat(sDate.getHours());
        var min = parseFloat(sDate.getMinutes());
        var sec = parseFloat(sDate.getSeconds());

        if (hour < 10)
            hour = "0" + hour;

        if (min < 10)
            min = "0" + min;

        if (sec < 10)
            sec = "0" + sec;

       // $("#sys_date").html(sDate.getDate()+"日" + " " + monthstr[sDate.getMonth()] + " " + sDate.getFullYear()+"年" +  ", " + weekstr[sDate.getDay()] + ", " + hour + ":" + min + ":" + sec + " (GMT +8)");
        $("#sys_date").html(sDate.getFullYear()+"年"+monthstr[sDate.getMonth()]+sDate.getDate()+"日"+" ("+weekstr[sDate.getDay()]+") "+hour+":"+min+":"+sec+" (GMT +8)");
    }
	setInterval(updateTime, 1000);
</script>
</head>

<body>
<div id="loginBx" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
	<form>
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">{{ Lang::get('public.PleaseLogin') }}</h4>
      </div>
	  
      <div class="modal-body">
        <div class="row">
			<div class="col-md-12 col-xs-12">
			<label>{{ Lang::get('public.Username') }}</label>
			</div>
			<div class="col-md-12 col-xs-12 text-black wallI">
				<input type="text" class="form-control" id="username2" name="username" onKeyPress="enterpressalert(event, this)">
			</div>
        </div>
      
        <div class="row padTB01">
			<div class="col-md-12 col-xs-12">
				<label>{{ Lang::get('public.Password') }}</label>
			</div>
			<div class="col-md-12 col-xs-12 text-black wallI">
				<input type="password" class="form-control" id="password2" name="password" onKeyPress="enterpressalert(event, this)">
			</div>
        </div>
		
        <div class="row padTB01">
			<div class="col-md-5 col-xs-4 text-black wallI">
				<img style="width:120px;height:34px;" src="{{route('captcha', ['type' => 'login_captcha'])}}">
			</div>
			<div class="col-md-1 col-xs-1 text-black wallI">
			</div>
			<div class="col-md-7 col-xs-7 text-black wallI">
				<input class="form-control" id="code2" name="" type="text" maxlength="4" onKeyPress="enterpressalert(event, this)" placeholder="{{Lang::get('public.Code')}}">
			</div>
        </div>
		
		<div class="row padTB01">
			<div class="col-md-8 col-xs-8">
			</div>
			<div class="col-md-4 col-xs-4">
				<a href="{{route('forgotpassword')}}" style="border-style: none;"><label>{{ Lang::get('public.ForgotPassword') }}</label></a>
			</div>

        </div>
      </div>
	  
      <div class="modal-footer">
		  <div class="submitBtn bt03 pull-right">
				<a id="loginBtnAction2" href="javascript:void(0);" onclick="login2();">{{ Lang::get('public.Login') }}</a>
				<a id="loading2" href="javascript:void(0);" style="display:none;">{{ Lang::get('public.Loading') }}</a>
		  </div>
      </div>
	</form>
    </div>
  </div>
</div>
<div id="myAccMng" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">{{ Lang::get('public.Welcome') }} {{Session::get('username')}}</h4>
      </div>
      <div class="modal-body">
        <div class="row">
        <div class="col-md-12 col-xs-12">
        <ul>
        <li><a href="{{route('update-profile')}}">{{ Lang::get('public.AccountProfile') }}</a></li>
        <li><a href="{{route('deposit')}}">{{ Lang::get('public.Deposit') }}</a></li>
        <li><a href="{{route('withdraw')}}">{{ Lang::get('public.Withdrawal') }}</a></li>
        <li><a href="{{route('transfer')}}">{{ Lang::get('public.Transfer') }}</a></li>
        <li><a href="{{route('transaction')}}">{{ Lang::get('public.TransactionHistory') }}</a></li>
        <li><a href="{{route('memberbank')}}">{{ Lang::get('COMMON.CTABBANKSETTING') }}</a></li>
		<li><a href="{{route('transfertomain')}}">{{ Lang::get('public.TransferToMain') }}</a></li><br>
		<li><a href="{{route('logout')}}">{{ Lang::get('public.Logout') }}</a></li>
        </ul>
        </div>

        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<!-- wrapper -->
<div class="row">
<!-- navigation -->

<!-- navigation -->

<div class="col-md-12 nopad">
<div class="headerBg">
<div class="row">
<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
<div class="col-md-9 col-xs-6 padTB01">
<a href="{{route('homepage')}}">
<img src="{{url()}}/gsc/mobile/resources/img/logo.png">
</a>
</div>

@if (Auth::user()->check())
<div class="col-md-1 col-xs-4 padTB01">
   <div class="logBtn"><a data-toggle="modal" data-target="#myAccMng" href="#">{{substr(Session::get('username'), 0, 10)}}</a></div>
</div>
@else
<div class="col-md-1 col-xs-2 padTB01">
</div>
<div class="col-md-1 col-xs-2 padTB01">
   <div class="logBtn"><a data-toggle="modal" data-target="#loginBx" href="#">{{ Lang::get('public.Login') }}</a></div>
</div>
@endif

<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="{{route('homepage')}}"><i class="fa fa-home fa-2x icnCl" aria-hidden="true"></i><span class="post01">{{ Lang::get('public.Home') }}</span></a>
                    </li>
                    <li>
                        <a href="{{route('aboutus')}}"><i class="fa fa-info fa-2x icnCl" aria-hidden="true"></i><span class="post01">{{ Lang::get('public.AboutUs') }}</span></a>
                    </li>
                    <li>
                         <a href="{{route('promotion')}}"><i class="fa fa-gift fa-2x icnCl" aria-hidden="true"></i><span class="post01">{{ Lang::get('public.Promotion') }}</span></a>
                    </li>
                    <li>
                        <a href="{{route('mobile')}}"><i class="fa fa-mobile fa-2x icnCl" aria-hidden="true"></i><span class="post01">{{ Lang::get('public.MobileDownload2') }}</span></a>
                    </li>
                    <li>
                        <a href="{{route('contactus')}}"><i class="fa fa-phone fa-2x icnCl" aria-hidden="true"></i><span class="post01">{{ Lang::get('public.ContactUs') }}</span></a>
                    </li>
                    <li>
                         <a href="javascript:void(0);" onclick="window.open('https://chat.livechatvalue.com/chat/chatClient/chatbox.jsp?companyID=905849&configID=54544&jid=6705510837&s=1')"><i class="fa fa-weixin fa-2x icnCl" aria-hidden="true"></i><span class="post01">{{ Lang::get('public.LiveChat2') }}</span></a>
                    </li>
					<li>
                    <span class="cgLang">{{ Lang::get('public.ChangeLanguage') }}: <a href="{{route( 'language', [ 'lang'=> 'cn'])}}">{{ Lang::get('public.Chinese') }}</a> / <a href="{{route( 'language', [ 'lang'=> 'en'])}}">{{ Lang::get('public.English') }}</a></span>
                    </li>
                                 
                </ul>
               
                
                
            </div>
</div>

<div class="row padTB03 announceBg">
		<div class="col-md-2 col-xs-1">
			<i class="fa fa-volume-up clr1" aria-hidden="true"></i>
		</div>
		<div class="col-md-10 col-xs-11">
			<marquee>{{ App\Http\Controllers\User\AnnouncementController::index() }}</marquee>
		</div>
</div>
</div>

@yield('content')

<div class="row">
<div class="equalHWrap eqWrap">
	<div class="equalHW selected1 eq text-center">
	<a href="#" onclick="location.href = '{{route('homepage')}}'">
    <div class="btmIcn"><img src="{{url()}}/gsc/mobile/resources/img/btm-hpg-6.png"></div>
    <div class="btmTxt">{{ Lang::get('public.Home') }}</div>
    </a>
    </div>
@if (Auth::user()->check())	
	<div class="equalHW selected1 eq text-center">
	<a href="#" onclick="@if (Auth::user()->check()) location.href = '{{route('deposit')}}' @else alert('{{Lang::get('COMMON.PLEASELOGIN')}}'); @endif">
    <div class="btmIcn"><img src="{{url()}}/gsc/mobile/resources/img/btm-hpg-1.png"></div>
    <div class="btmTxt">{{ Lang::get('public.Deposit') }}</div>
    </a>
    </div>
    <div class="equalHW eq text-center">
    <a href="#" onclick="@if (Auth::user()->check()) location.href = '{{route('transfer')}}' @else alert('{{Lang::get('COMMON.PLEASELOGIN')}}'); @endif">
    <div class="btmIcn"><img src="{{url()}}/gsc/mobile/resources/img/btm-hpg-2.png"></div>
    <div class="btmTxt">{{ Lang::get('public.MyWallet') }}</div>
    </a>
    </div>
    <div class="equalHW eq text-center">
    <a href="#" onclick="@if (Auth::user()->check()) location.href = '{{route('withdraw')}}' @else alert('{{Lang::get('COMMON.PLEASELOGIN')}}'); @endif">
    <div class="btmIcn"><img src="{{url()}}/gsc/mobile/resources/img/btm-hpg-3.png"></div>
    <div class="btmTxt">{{ Lang::get('public.Withdrawal') }}</div>
    </a>
    </div>	
@else
    <div class="equalHW selected1 eq text-center">
	<a href="#" onclick="location.href = '{{route('promotion')}}'">
    <div class="btmIcn"><img src="{{url()}}/gsc/mobile/resources/img/btm-hpg-8.png"></div>
    <div class="btmTxt">{{ Lang::get('public.Promotion') }}</div>
    </a>
    </div>
    <div class="equalHW eq text-center">
    <a href="#" onclick="location.href = '{{route('register')}}'">
    <div class="btmIcn"><img src="{{url()}}/gsc/mobile/resources/img/btm-hpg-7.png"></div>
    <div class="btmTxt">{{ Lang::get('public.RegisterNow') }}</div>
    </a>
    </div>
    <div class="equalHW eq text-center">
    <a href="#" onclick="location.href = '{{route('mobile')}}'">
    <div class="btmIcn"><img src="{{url()}}/gsc/mobile/resources/img/btm-hpg-9.png"></div>
    <div class="btmTxt">{{ Lang::get('public.MobileApp') }}</div>
    </a>
    </div>	
@endif	
    <div class="equalHW eq text-center">
    <a href="javascript:void(0);" onclick="window.open('https://chat.livechatvalue.com/chat/chatClient/chatbox.jsp?companyID=905849&configID=54544&jid=6705510837&s=1')">
    <div class="btmIcn"><img src="{{url()}}/gsc/mobile/resources/img/btm-hpg-4.png"></div>
    <div class="btmTxt">{{ Lang::get('public.LiveChat2') }}</div>
    </a>
    </div>
</div>
</div>

    
    <!-- /footer -->



</div>

</div>
<!-- wrapper -->

    
    

</body>

</html>
