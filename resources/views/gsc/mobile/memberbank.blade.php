@extends('gsc/mobile/master')
@section('content')
<script>
	$(document).ready(function() { 
		getBalance(true);
		bankposition();
		// update_binded_bank();
	});
	
function update_bank(){

	 
	$.ajax({
		type: "POST",
		url: "{{route('updatebank')}}?"+$('#bank_form').serialize(),
	
	}).done(function( result ) {
		if(result == 1){
			
			alert('{{Lang::get('COMMON.SUCESSFUL')}}');
			window.location="{{route('withdraw')}}";
		}
		else if(result == 2){
			
			alert('只允许绑定不超过3个提款户口！');

		}else{
				alert('This bank account is duplicated. Please insert another bank account.');
                                location.reload(); 
		}

					
	});
} 
function bankposition(){
	if(($("#bnkid").val() == 'WEIXIN')||($("#bnkid").val() == 'ZHIFUBAO')){
		if($("#bnkid").val() == 'WEIXIN'){
			$("#fullname").html("{{Lang::get('public.WechatPayName')}}* :");
			$("#bankAccountNo").html("{{Lang::get('public.WechatAccountNo')}}* :");
		}else if($("#bnkid").val() == 'ZHIFUBAO'){
			$("#fullname").html("{{Lang::get('public.AliPayName')}}* :");
			$("#bankAccountNo").html("{{Lang::get('public.AliPayAccountNo')}}* :");
		}	
		$("#distpicker").hide();
		$("#BankBranch3").hide();
	}else{
		$("#fullname").html("{{Lang::get('public.FullName')}}* :");
		$("#bankAccountNo").html("{{Lang::get('public.BankAccountNo')}}* :");
		$("#distpicker").show();
		$("#BankBranch3").show();
	}
}

function update_binded_bank(id){
	bankposition();
	
	$.ajax({
		type: "POST",
		url: "{{route('loadbank')}}",
		data: {
			id:  id.value
		},
	}).done(function( result ) {
		if(result != 'null')
		{
			obj = JSON.parse(result);

			$('#province1').val(obj['bankprovince']).trigger('change');
			$('#province1').attr('disabled','disabled');
			$('#city1').val(obj['bankcity']).trigger('change');
			$('#city1').attr('disabled','disabled');
			$('#district1').val(obj['bankdistrict']).trigger('change');
			$('#district1').attr('disabled','disabled');
			$('#bankbranch').val(obj['bankholdingbranch']);
			$('#bankbranch').attr('disabled','disabled');
			$('#bankaccno').val(obj['bankaccno']);
			$('#bankaccno').attr('disabled','disabled');
			$('#fullname').val(obj['fullname']);
			$('#fullname').attr('disabled','disabled');
		}else{
			
			$('#province1').prop("disabled", false);
			$('#city1').prop("disabled", false);
			$('#district1').prop("disabled", false);
			$('#bankbranch').val('');
			$('#bankbranch').prop("disabled", false);
			$('#bankaccno').val('');
			$('#bankaccno').prop("disabled", false);
			
		}
	});
}
</script>
<form id="bank_form">
	<div class="row nopad marginTop01">
		<div class="col-md-12 col-xs-12 text-center clr2 hg">
			<span class="cornMid">{{Lang::get('COMMON.CTABBANKSETTING')}}</span>
		</div>
	</div>
	<div class="row pad01">
		<div class="col-md-12 col-xs-12">
			<font color="red" style="margin-left:30px;">（新会员请先设定提款户口,只允许绑定不超过3个提款户口！）</font>
		</div>
	</div>
	<div class="row pad01">
		<div class="col-md-6 col-xs-6">
			<div class="col-md-12 col-xs-12 padtp1">
			{{Lang::get('public.BankName')}}* :
			</div>
		</div>
		<div class="col-md-6 col-xs-6 wallI">
			<select class="form-control" name="bnkid" id="bnkid" style="width:202px;" onChange="update_binded_bank(this)">
				@foreach( $banklists as $bank => $detail)
						<option value="{{$detail['code']}}">{{$bank}}</option>
				@endforeach
			</select>
		</div>
	</div>
	<div data-toggle="distpicker" id="distpicker">
		<div class="row pad01">
			<div class="col-md-6 col-xs-6">
				<div class="col-md-12 col-xs-12 padtp1">
				{{Lang::get('public.Province')}}* :
				</div>
			</div>
			<div class="col-md-6 col-xs-6 wallI">
				<select class="form-control" id="province1" name="province" style="width:202px;"></select>
			</div>
		</div>
		<div class="row pad01">
			<div class="col-md-6 col-xs-6">
				<div class="col-md-12 col-xs-12 padtp1">
				{{Lang::get('public.City')}}* :
				</div>
			</div>
			<div class="col-md-6 col-xs-6 wallI">
				<select class="form-control" id="city1" name="city" style="width:202px;"></select>
			</div>
		</div>
		<div class="row pad01">
			<div class="col-md-6 col-xs-6">
				<div class="col-md-12 col-xs-12 padtp1">
				{{Lang::get('public.District')}}* :
				</div>
			</div>
			<div class="col-md-6 col-xs-6 wallI">
				<select class="form-control" id="district1" name="district" style="width:202px;"></select>
			</div>
		</div>
	</div>
		<div class="row pad01" id="BankBranch3">
			<div class="col-md-6 col-xs-6">
				<div class="col-md-12 col-xs-12 padtp1">
				{{Lang::get('public.BankBranch3')}}* :
				</div>
			</div>
			<div class="col-md-6 col-xs-6 wallI">
				<input type="text" class="form-control" id="bankbranch" name="bankbranch">
			</div>
		</div>
		<div class="row pad01">
			<div class="col-md-6 col-xs-6">
				<div class="col-md-12 col-xs-12 padtp1" id="fullname">
				{{Lang::get('public.FullName')}}* :
				</div>
			</div>
			<div class="col-md-6 col-xs-6 wallI">
				@if(!empty($fullname))
					<span class="acctText">{{$fullname}}</span>
				@else
					<input type="text" class="form-control" id="fullname" name="fullname">
			  @endif
			</div>
		</div>
		<div class="row pad01">
			<div class="col-md-6 col-xs-6">
				<div class="col-md-12 col-xs-12 padtp1" id="bankAccountNo">
				{{Lang::get('public.BankAccountNo')}}* :
				</div>
			</div>
			<div class="col-md-6 col-xs-6 wallI">
				<input type="text" id="bankaccno" class="form-control" name="bnkaccno">
			</div>
		</div>
		<div class="row pad01 padB">
			<div class="col-md-12 col-xs-12">
				<div class="submitBtn pull-right">
					<a href="javascript:void(0)" onClick="update_bank()">{{Lang::get('public.Submit')}}</a>
				</div>
			</div>
		</div>
</form>
<script src="{{url()}}/gsc/resources/js/distpicker.data.js"></script>
<script src="{{url()}}/gsc/resources/js/distpicker.js"></script>
<script src="{{url()}}/gsc/resources/js/main.js"></script>
@stop