@extends('gsc/mobile/master')
@section('content')
	<script type="text/javascript">
	TransactionCount();
	setInterval(update_mainwallet, 10000);
	function submit_transfer(){
		var myform = $('#withdraw_form');
		var disabled = myform.find(':input:disabled').removeAttr('disabled');
		
		$.ajax({
			 type: "POST",
			 url: "{{route('withdraw-process')}}?"+myform.serialize(),
			 data: {
				_token: 		 "{{ csrf_token() }}",
			 },
			 beforeSend: function(){
				$('#withdraw_btn_submit').attr('onClick','');
				$('#withdraw_btn_submit').html('Loading...');
			 },
			 success: function(json){
					disabled.attr('disabled','disabled');
					obj = JSON.parse(json);
					 var str = '';
					 $.each(obj, function(i, item) {
						
						if( '{{Lang::get('COMMON.SUCESSFUL')}}' == item ){
							alert('{{Lang::get('COMMON.SUCESSFUL')}}');
							window.location.href = "{{route('transaction')}}";
						}else{
							alert(item);
						}
					})
					// $('.failed_message').html(str);
					$('#withdraw_btn_submit').attr('onClick','submit_transfer()');
					$('#withdraw_btn_submit').html('{{Lang::get('public.Submit')}}');
				
			}
		})
		
		disabled.attr('disabled','disabled');
	}
	
function TransactionCount(){
	$.ajax({
		type: "GET",
		url: "{{route('TransactionCount')}}",
		data: {},
	}).done(function( json ) {
	obj = JSON.parse(json);
		if(obj==1){
			$('#bankcol').hide();
		}	
	});
}	
	
function update_binded_bank(id){
		
	$.ajax({
		type: "POST",
		url: "{{route('loadbank')}}",
		data: {
			id:  id.value
		},
	}).done(function( result ) {
		obj = JSON.parse(result);

		$('#province1').val(obj['bankprovince']);
		$('#city1').val(obj['bankcity']);
		$('#district1').val(obj['bankdistrict']);
		$('#bankbranch').val(obj['bankholdingbranch']);
		$('#bankaccno').val(obj['bankaccno']);
	});
}
	
</script>
<form id="withdraw_form">
	<div style="height:800px;">
	<div class="row nopad marginTop01">
	<div class="col-md-12 col-xs-12 text-center clr2 hg">
	<span class="cornMid"><strong>{{Lang::get('public.Withdrawal')}}</strong></span>
	</div>
	</div>

	<div class="row pad01">
		<div class="col-md-6 col-xs-6">
			<div class="col-md-12 col-xs-12 padtp1">
			{{Lang::get('public.Balance')}} ({{ Session::get('user_currency') }}) :
			</div>
		</div>
		<div class="col-md-6 col-xs-6 wallI">
			{{App\Http\Controllers\User\WalletController::mainwallet()}}
		</div>
	</div>

	<div class="row pad01">
		<div class="col-md-6 col-xs-6">
			<div class="col-md-12 col-xs-12 padtp1">
			{{Lang::get('public.Amount')}} ({{ Session::get('user_currency') }}) * :
			</div>
		</div>
		<div class="col-md-6 col-xs-6 wallI">
			<input type="text" class="form-control" name="amount">
		</div>
	</div>

	<div class="row pad01">
		<div class="col-md-6 col-xs-6">
			<div class="col-md-12 col-xs-12 padtp1">
			己绑定银行卡信息 :
			</div>
		</div>
		<div class="col-md-6 col-xs-6 wallI">
			<select class="form-control" name="bank" style="width:202px;float:left;" onChange="update_binded_bank(this)">
				@foreach( $binded_banks as $binded_bank  )
					<option value="{{$banklists[$binded_bank->bankname]['code']}}">{{$binded_bank->bankname}}</option>
				@endforeach
			</select>
		</div>
		<div class="col-md-6 col-xs-6">
		</div>
		<div class="col-md-6 col-xs-6">
		<a href="{{route('memberbank')}}" style="width:100px;color:red;">新增银行户口</a>
		</div>
	</div>
	<div id="bankcol">
		<div class="row pad01">
			<div class="col-md-6 col-xs-6">
				<div class="col-md-12 col-xs-12 padtp1">
				{{Lang::get('public.Province')}}* :
				</div>
			</div>
			<div class="col-md-6 col-xs-6 wallI">
				<input class="form-control" id="province1" name="province" style="width:195px;" value="{{$binded_banks[0]->bankprovince}}" disabled>
			</div>
		</div>

		<div class="row pad01">
			<div class="col-md-6 col-xs-6">
				<div class="col-md-12 col-xs-12 padtp1">
				{{Lang::get('public.City')}}* :
				</div>
			</div>
			<div class="col-md-6 col-xs-6 wallI">
				<input class="form-control" id="city1" name="city" style="width:195px;" value="{{$binded_banks[0]->bankcity}}" disabled>
			</div>
		</div>

		<div class="row pad01">
			<div class="col-md-6 col-xs-6">
				<div class="col-md-12 col-xs-12 padtp1">
				{{Lang::get('public.District')}}* :
				</div>
			</div>
			<div class="col-md-6 col-xs-6 wallI">
				 <input class="form-control" id="district1" name="district" style="width:195px;" value="{{$binded_banks[0]->bankdistrict}}" disabled>
			</div>
		</div>

		<div class="row pad01">
			<div class="col-md-6 col-xs-6">
				<div class="col-md-12 col-xs-12 padtp1">
				{{Lang::get('public.BankBranch3')}}* :
				</div>
			</div>
			<div class="col-md-6 col-xs-6 wallI">
				 <input type="text" class="form-control" id="bankbranch" name="bankbranch" value="{{$binded_banks[0]->bankholdingbranch}}" disabled>
			</div>
		</div>
	</div>
	<div class="row pad01">
		<div class="col-md-6 col-xs-6">
			<div class="col-md-12 col-xs-12 padtp1">
			{{Lang::get('public.FullName')}}* :
			</div>
		</div>
		<div class="col-md-6 col-xs-6 wallI">
			 {{$fullname}}
		</div>
	</div>

	<div class="row pad01">
		<div class="col-md-6 col-xs-6">
			<div class="col-md-12 col-xs-12 padtp1">
			{{Lang::get('public.BankAccountNo')}}* :
			</div>
		</div>
		<div class="col-md-6 col-xs-6 wallI">
			 <input type="text" class="form-control" id="bankaccno" name="accountno" value="{{$binded_banks[0]->bankaccno}}" disabled>
		</div>
		<div class="col-md-12 col-xs-12 wallI">&nbsp;</div>
		<div class="col-md-12 col-xs-12 wallI">{{Lang::get('public.Notice2')}}。{{Lang::get('public.WithdrawDetailNote')}}
		</div>
	</div>
	<div class="row pad01 padB">
		<div class="col-md-12 col-xs-12">
			<div class="submitBtn pull-right">
				<a id="withdraw_btn_submit" href="javascript:void(0)" onClick="submit_transfer()">{{Lang::get('public.Submit')}}</a>
			</div>
		</div>
	</div>
	</div>
</form>
@stop