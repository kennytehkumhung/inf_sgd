@extends('gsc/mobile/master')
@section('content')
<script>
$(function () {
	transaction_history();
});
  
function transaction_history(){
$.ajax({
	type: "POST",
	url: "{{action('User\TransactionController@transactionProcess')}}",
	data: {
		_token:   "{{ csrf_token() }}",
		date_from:		$('#date_from').val(),
		date_to:    	$('#date_to').val(),
		record_type:    $('#record_type').val()

	},
}).done(function( json ) {

			//var str;
			 var str = '';
			str += '	<thead><tr class="tbltitle"><th>{{Lang::get('public.ReferenceNo')}}</th><th>{{Lang::get('public.DateTime')}}</th><th>{{Lang::get('public.Type')}}</th><th>{{Lang::get('public.Amount')}}({{ Session::get('user_currency') }})</th><th>{{Lang::get('public.Status')}}</th><th>{{Lang::get('public.Withdrawal')}}</th><th>{{Lang::get('public.Reason')}}</th></tr></thead>';
			
			 obj = JSON.parse(json);
			//alert(obj);
			 $.each(obj, function(i, item) {
				str +=  '<tr><td>'+item.id+'</td><td>'+item.created+'</td><td>'+item.type+'</td><td>'+item.amount+'</td><td>'+item.status+'</td><td>'+item.cardno+'</td><td>'+item.rejreason+'</td></tr>';
				
			})
				
			//alert(json);
			$('#trans_history').html(str);
			
	});
}

setInterval(updateTrans, 10000);
setInterval(update_mainwallet, 10000);

function updateTrans(){
	$( "#trans_history_button" ).trigger( "click" );
}

</script>
<div class="row nopad marginTop01">
<div class="col-md-12 col-xs-12 text-center clr2 hg">
<span class="cornMid"><strong>{{Lang::get('public.TransactionHistory')}}</strong></span>
</div>
</div>

<div class="row pad01 ">

<div class="col-md-6 col-xs-6">
<div class="col-md-12 col-xs-12 padtp1">
{{Lang::get('public.RecordType')}} :
</div>
</div>
<div class="col-md-6 col-xs-6 wallI">
<select id="record_type">
	<option value="0" selected>{{Lang::get('public.CreditAndDebitRecords')}}</option>
	<option value="1">{{Lang::get('public.CreditRecords')}}</option>
	<option value="2">{{Lang::get('public.DebitRecords')}}</option>
</select>
</div>

</div>

<div class="row pad01">
	<div class="col-md-6 col-xs-6">
		<div class="col-md-12 col-xs-12 padtp1">
		{{Lang::get('public.DateFrom')}} :
		</div>
	</div>
	<div class="col-md-6 col-xs-6 wallI">
		<input type="text" class="datepicker" id="date_from" style="cursor:pointer;" value="{{date('Y-m-d')}}">
	</div>
</div>

<div class="row pad01">
	<div class="col-md-6 col-xs-6">
		<div class="col-md-12 col-xs-12 padtp1">
		{{Lang::get('public.DateTo')}} :
		</div>
	</div>
	<div class="col-md-6 col-xs-6 wallI">
		<input type="text" class="datepicker" id="date_to" style="cursor:pointer;" value="{{date('Y-m-d')}}">
	</div>
</div>

<div class="row marginTop01">
        <div class="table-responsive">
        <table class="table" id="trans_history">
			<thead>
				<tr class="tbltitle">
					<th>{{Lang::get('public.ReferenceNo')}}</th>
					<th>{{Lang::get('public.DateOrTime')}}</th>
					<th>{{Lang::get('public.Type')}}</th>
					<th>{{Lang::get('public.Amount')}} </th>
					<th>{{Lang::get('public.Status')}}</th>
					<th>{{Lang::get('public.Withdrawal')}}</th>
					<th>{{Lang::get('public.Reason')}}</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td colspan="7"><h2><center>{{ Lang::get('public.NoDataAvailable') }}</center></h2></td>
				</tr>
			</tbody>
        </table>
        </div>  
        </div>

<div class="row pad01 padB">

<div class="col-md-12 col-xs-12">
<div class="submitBtn pull-right">
<a id="trans_history_button" href="javascript:void(0)" onClick="transaction_history()"> {{Lang::get('public.Submit')}}</a>
</div>
</div>


</div>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script>
$(function() {
	$( ".datepicker" ).datepicker({ dateFormat: 'yy-mm-dd'  , defaultDate: new Date() });
});
</script>
@stop