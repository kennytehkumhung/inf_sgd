@extends('gsc/mobile/master')

@section('title', Lang::get('public.Maintenance'))

@section('content')
<img src="{{url()}}/gsc/img/under-maintenance-cn.png" alt="" style="display:block;margin-left:auto;margin-right: auto; width:100%;">
@stop