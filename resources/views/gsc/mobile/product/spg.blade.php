@extends('gsc/mobile/master')
@section('content')
<!doctype html>
<html>
<head>
    <link href="{{ asset('/gsc/resources/css/slot2.css') }}" rel="stylesheet" type="text/css" />
	@include('gsc/product/detectdevice')
</head>
<body style="background-color: black;">
<div class="slotContainerBottom" style="width:360px;">
    <div class="slotMenuHolder" style="width:360px;">
        <div class="slot_menu">
            <ul>
                <li><a href="{{route('spg', [ 'type' => 'slot' , 'category' => 'slot' ] )}}">{{ Lang::get('public.SlotGames2') }}</a></li>
                <li><a href="{{route('spg', [ 'type' => 'slot' , 'category' => 'progressive' ] )}}">{{ Lang::get('public.ProgressiveGames') }}</a></li>
            </ul>
        </div>
    </div>

    <div id="slot_lobby" class="col-lg-3 col-md-4 col-xs-6 text-center" style="width:350px;margin-left:20px;">

        @foreach( $lists as $list )
            <div class="slot_box">
                <a href="javascript:void(0)" onclick="@if (Auth::user()->check())window.open('{{route('spg-slot' , [ 'gameid' => $list['gamecode'] ] )}}', 'spg_slot', 'width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif" >
                    <img src="{{url()}}/gsc/img/spg/{{$list->gamecode}}.jpg" alt=""/>
                </a>
                <span>{{$list['gamename_cn']}}</span>
            </div>
        @endforeach

        <div class="clr"></div>
    </div>

    <div class="clr"></div>
</div>
</body>
</html>
@endsection
