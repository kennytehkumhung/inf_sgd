@extends('gsc/mobile/master')
@section('content')
<script type="text/javascript">
	function modalBox(title,content){
		$("#titleBox").html(title);
        $("#contentBox").html(content);
	 }
</script>
<div class="row nopad marginTop01">
<div class="col-md-12 col-xs-12 text-center clr2 hg">
<span class="cornMid"><strong>{{Lang::get('public.Promotion')}}</strong></span>
</div>
</div>

<div class="row nopad padB">
	<div class="container minH">
		
		<div class="row nopad">
		@foreach ($promo as $key => $value )
			<div class="col-md-12 col-xs-12 promoC">
			<a data-toggle="modal" data-target="#promoBx" href="#" onclick="modalBox('{{$value['title']}}','{{$value['content']}}')"><img src="{{$value['image']}}" data-toggle="modal" data-target="#myModal1"></a>
			</div>
		@endforeach
		</div>
	</div>
</div>
<div id="promoBx" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title" id="titleBox"></h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12 col-xs-12" id="contentBox">
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<div class="fp pull-left">
				</div>
				<div class="pull-right">
					@if (Auth::user()->check())
						<a href="{{route('deposit')}}" class="btn btn-success">{{Lang::get('public.JoinNow')}}</a>
					@else
						<a href="{{route('register')}}" class="btn btn-success">{{Lang::get('public.JoinNow')}}</a>
					@endif
				</div>
			</div>
		</div>
	</div>
</div>
@stop

