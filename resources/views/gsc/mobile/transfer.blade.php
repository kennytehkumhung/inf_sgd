@extends('gsc/mobile/master')
@section('content')
<script>

var amount = Math.floor("{{floor(App\Http\Controllers\User\WalletController::mainwallet())}}");

function submit_transfer(transfer_all){

	if(transfer_all != true){
		amount = $('#transfer_amount').val();
	}
	
	if( ($('#transfer_to').val() == 'BBN' || $('#transfer_to').val() == 'MNT')&& amount % 1 != 0 )
	{
		alert('请输入整数！');
	}else{
		
		$.ajax({
				 type: "POST",
				 url: "{{route('transfer-process')}}",
				 data: {
					_token: 		 "{{ csrf_token() }}",
					amount:			 amount,
					from:     		 $('#transfer_from').val(),
					to: 			 $('#transfer_to').val(),
				 },
				 beforeSend: function(){
					$('#btn_submit_transfer').attr('onClick','');
					$('#btn_submit_transfer').html('Loading...');
				 },
				 success: function(json){
					obj = JSON.parse(json);
					 
					var str = '';
					 $.each(obj, function(i, item) {
						
						if( '{{Lang::get('COMMON.SUCESSFUL')}}' == item ){
							
							//$('.main_wallet').html(obj.main_wallet);
							
						}
						if( i  != 'main_wallet'){
							str += item + '\n';
						}
					})
					
					load_balance($('#transfer_from').val(),'from_balance');
					load_balance($('#transfer_to').val(),'to_balance');
					getBalance(false);
					$('#btn_submit_transfer').attr('onClick','submit_transfer()');
					$('#btn_submit_transfer').html('{{Lang::get('public.Submit')}}');
					alert(str);
				}
			})
		
	}
 } 
 function load_balance($code, $type){
    if($code == 'MAIN'){
		url = '{{route('mainwallet')}}';
	}else{
		url = '{{route('getbalance')}}';
	}
	$.ajax({
		  type: "POST",
		  url:  url,
		  data: {
				_token: 		 "{{ csrf_token() }}",
				product:		 $code,
			 },
		  beforeSend: function(balance){
			$('#'+$type).html('<img style="float:left;position:static;top:0px;margin-left:20px;" src="{{url()}}/front/img/ajax-loading.gif" width="20" height="20">');
		  },
		  success: function(balance){
			  
			  if($type == 'from_balance'){
				  amount = balance;
			  }
			  
			 if($code == 'MAIN'){
				$('.main_wallet').html(parseFloat(Math.round(balance * 100) / 100).toFixed(2));
			 }
			$('#'+$type).html('{{Session::get('currency')}} '+parseFloat(Math.round(balance * 100) / 100).toFixed(2));
			total_balance += parseFloat(balance);
		  }
	})
 }
 
 function enterpresstransfer(e, textarea)
{
	var code = (e.keyCode ? e.keyCode : e.which);
    if(code == 13) { //Enter keycode
      submit_transfer();
    }
} 

 
$(document).ready(function () {
	 $('#transfer_from').change(function() {

	}).trigger('change');
	 $('#transfer_to').change(function() {

	}).trigger('change');
});
</script>
<div class="row nopad marginTop01">
	<div class="col-md-12 col-xs-12 text-center clr2 hg">
		<span class="cornMid">{{Lang::get('public.Wallet')}}</span>
	</div>
</div>
@include('gsc/mobile/trans_top' )
<div style="height:800px;">
<div class="row nopad marginTop01">
<div class="col-md-12 col-xs-12 text-center clr2 hg">
<span class="cornMid"><strong>{{Lang::get('public.Transfer')}}</strong></span>
</div>
</div>

<div class="row pad01">
	<div class="col-md-6 col-xs-6">
		<div class="col-md-12 col-xs-12 padtp1">
		{{Lang::get('public.TransferFrom')}} :
		</div>
	</div>
	<div class="col-md-6 col-xs-6 wallI">
		{{Lang::get('public.TransferTo')}} :
	</div>
</div>

<div class="row pad01">
	<div class="col-md-6 col-xs-6">
		<div class="col-md-12 col-xs-12 padtp1">
			<select id="transfer_from" onchange="load_balance(this.value,'from_balance')" style="width:160px;">
					<option selected="selected" value="MAIN">{{Lang::get('public.MainWallet')}}</option>
					@foreach( Session::get('products_obj') as $prdid => $object)
						<?php
						$showPrd = true;
						if (Session::get('currency') == 'VND' && ($object->code == 'JOK')) {
							$showPrd = false;
						}
						?>
						@if ($showPrd)
							<option value="{{$object->code}}">{{$object->getNameWithMultiLang(Lang::getLocale(), false)}}</option>
						@endif
					@endforeach
			</select>
		</div>
	</div>
	<div class="col-md-6 col-xs-6 wallI">
		<select id="transfer_to" onchange="load_balance(this.value,'to_balance')" style="width:160px;">
					@foreach( Session::get('products_obj') as $prdid => $object)
					<?php
					$showPrd = true;
						if (Session::get('currency') == 'VND' && ($object->code == 'JOK')) {
							$showPrd = false;
						}
					?>
						@if ($showPrd)
							<option value="{{$object->code}}">{{$object->getNameWithMultiLang(Lang::getLocale(), false)}}</option>
						@endif
					@endforeach
					<option  value="MAIN">{{Lang::get('public.MainWallet')}}</option>
				</select>
	</div>
</div>

<div class="row pad01">
	<div class="col-md-6 col-xs-6">
		<div class="col-md-12 col-xs-12 padtp1">
		<span id="from_balance" style="color:white;">{{Session::get('currency')}} {{App\Http\Controllers\User\WalletController::mainwallet()}}</span>	
		</div>
	</div>
	<div class="col-md-6 col-xs-6 wallI">
		<span id="to_balance" style="color:white;">{{Session::get('currency')}} 0.00</span>	
	</div>
</div>

<div class="row pad01">
	<div class="col-md-6 col-xs-6">
		<div class="col-md-12 col-xs-12 padtp1">
		{{Lang::get('public.Amount')}} :
		</div>
	</div>
	<div class="col-md-6 col-xs-6 wallI">
		<input onKeyPress="enterpresstransfer(event, this)" class="form-control" id="transfer_amount" value="0.00" type="text">
	</div>
</div>

<div class="row pad01">
	<div class="col-md-12 col-xs-12">
		<div class="submitBtn pull-right">
			<a style="margin-left:10px;" href="javascript:void(0)" onClick="submit_transfer(true)" >转入全部</a>
		</div>
		<div class="submitBtn pull-right">
			<a id="btn_submit_transfer" href="javascript:void(0)" onClick="submit_transfer(false)" >{{Lang::get('public.Submit')}}</a>
		</div>
	</div>
</div>

<div class="row pad01">
	<div class="col-md-12 col-xs-12">
		<div class="col-md-12 col-xs-12 padtp1">
			<span id="error_deposit" style="color:Red;display:none;">*Your account have 0.00 balance, please make a {{Lang::get('public.Deposit')}}.</span>
		</div>
	</div>
	<div class="col-md-12 col-xs-12">
		<div class="col-md-12 col-xs-12 padtp1">
			<span id="error_require" style="color:Red;display:none;">{{Lang::get('public.Required')}}</span>
		</div>
	</div>
</div>



@stop