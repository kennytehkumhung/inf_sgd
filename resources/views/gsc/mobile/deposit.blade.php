@extends('gsc/mobile/master')
@section('content')
<style>
ul, li {

	padding: 0;
	border: 0;
	font-size: 100%;
	font: inherit;
	vertical-align: baseline;
	margin-top: 10px;
}
ol, ul {
	list-style: none;
}
a {
text-decoration: none;
}
</style>
	<link href="{{url()}}/gsc/resources/css/table.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="{{url()}}/gsc/resources/css/popupstyle.css">
	<script src="{{ asset('/front/resources/js/jquery.qrcode.min.js')}}"></script>
	<script src="{{ asset('/gsc/resources/js/modernizr.js') }}"></script>
	<script type="text/javascript" src="{{ asset('/gsc/resources/js/jquery.form.min.js') }}"></script>
<script type="text/javascript">
    setInterval(update_mainwallet, 10000);
	// var qrcode = new QRCode(document.getElementById("qrcode"), {
		// width : 400,
		// height : 400
	// });
    function deposit_submit(checkPayChannel) {

        var data = $("#deposit_form").serializeArray();
        var form_data = {};

        for (var i = 0; i < data.length; i++) {
            form_data[data[i].name] = data[i].value;
        }

        var submitBtnOriText = $('#deposit_submit_btn').text();
		
		var depositPayHolder = $("#deposit_pay_channel_" + form_data["deposit_pay_channel"]);

        var options = {
            beforeSubmit: function (formData, jqForm, options) {
                $('#deposit_submit_btn').text('{{Lang::get('public.Loading')}}').attr('onclick', '');
            },
            complete: function (json, status) {
                obj = JSON.parse(json.responseText);

                if (obj.code == "OK") {
                    if (obj.action == "drawqrcode") {
                        $("#bui_qrcode").html("").qrcode({
                            width: 256,
                            height: 256,
                            text: obj.url,
                        });

						$("#wechatTEXT").html(obj.hint);
						$('#weixinReturnURL').addClass('is-visible');

						$("#deposit_submit_btn").hide();
						// var weixinURL=obj.url;
						// makeCode(weixinURL);
						// alert(weixinURL);
						// makeCode();
                        if (obj.hint.length > 0) {
                            $("#bui_qrlabel").html(obj.hint).show();
                        } else {
                            $("#bui_qrlabel").hide();
                        }
                    } else {
                        var showModal = false;

                        if (obj.hint_mobile.length > 0) {
                            $("#bui_qrlabel").html(obj.hint_mobile).show();
                            showModal = true;
                        }

                        if (obj.url.length > 0) {
                            $("#bui_qrcode").html('<a href="' + obj.url + '" target="_blank" style="line-height: 250px; color: blue; text-decoration: underline; font-size: 24px;">请点这里前往支付网关</a>').show();
                            showModal = true;
                        }

                        if (showModal) {
                            $("#afterhidden").hide();
                            $('#weixinReturnURL').addClass('is-visible');
						}
                    }
                } else {
					
                    var str = '';
                    $.each(obj, function (i, item) {
                        if ('{{Lang::get('COMMON.SUCESSFUL')}}' == item) {
							if($('#deposit_pay_channel').val()=="BNK"){
								alert('{{Lang::get('COMMON.SUCESSFUL')}}');
								window.location.href = "{{route('transaction')}}";
							}else{
								$('#popupQRcode', depositPayHolder).addClass('is-visible');
								$("#afterhidden").hide();
							}
                            return;
                        } else {
                            //$('.'+i+'_acctTextReg').html(item);
//                            str += item + '<br>';
                            str += item + '\n';
                        }
                    });

                    if (str != '') {
//                    		$('.failed_message').html(str);
                        alert(str);
                    }
                }

                $('#deposit_submit_btn').text(submitBtnOriText).attr('onclick', 'deposit_submit(true)');
            }
        };

        $("#deposit_form").ajaxSubmit(options);

        // !!! Important !!!
        // always return false to prevent standard browser submit and page navigation
        return false;
    }
	function makeid() {
		var text = "";
		var possible = "0123456789";

		for (var i = 0; i < 5; i++){
			text += possible.charAt(Math.floor(Math.random() * possible.length));
		}
		return text;
	}
	
	// function makeCode (weixinURL) {		
		// var elText = weixinURL;
		// qrcode.makeCode(elText);	
	// }
	
	function displaycode(){
	  $("[name='remarkcode']").val(makeid());
	}
    $(function () {
		displaycode();

        $("#deposit_pay_channel").change(function () {
            var payMethod = $(this).val();

            var holder = $(".deposit_pay_channel_holder");
            holder.hide();
            $("input, select, textarea", holder).prop("disabled", true);

            holder = $("#deposit_pay_channel_" + payMethod);
            $("input, select, textarea", holder).prop("disabled", false);
            holder.show();

            // Always enable this input as it use to upload receipt.
            $("#file").prop("disabled", false);
        }).change();
    });
</script>


	<div class="row nopad marginTop01">
		<div class="col-md-12 col-xs-12 text-center clr2 hg">
			<span class="cornMid"><strong>{{Lang::get('public.Deposit')}}</strong></span>
		</div>
	</div>

	<form id="deposit_form" action="{{route('deposit-process')}}" method="post" enctype="multipart/form-data">

		<input type="hidden" id="_token" name="_token" value="{{csrf_token()}}">

		<div class="row pad01">
			<div class="col-md-6 col-xs-6">
				{{Lang::get('public.Amount')}} :
			</div>
			<div class="col-md-6 col-xs-6 wallI">
				<input type="text" class="form-control" name="amount">
			</div>
		</div>

		<div class="row pad01">
			<div class="col-md-6 col-xs-6">
				{{Lang::get('public.PaymentChannel')}} :
			</div>
			<div class="col-md-6 col-xs-6 wallI">
                <?php
                // Just for display.
                if (!isset($paymentGatewaySetting['快捷通道'])) {
                    $paymentGatewaySetting['快捷通道'] = array();
                }

                $v = $paymentGatewaySetting['快捷通道'];
                $v[] = array('code' => 'SNZ', 'name' => '刷脸支付（开发中）', 'disabled' => 'disabled');
                $paymentGatewaySetting['快捷通道'] = $v;
                ?>

				<select id="deposit_pay_channel" name="deposit_pay_channel">
					@foreach ($paymentGatewaySetting as $key => $values)
						<optgroup label="{{ $key }}">
							@foreach ($values as $v)
								<option value="{{ $v['code'] }}" {{ isset($v['disabled']) ? $v['disabled'] : '' }}>{{ $v['name'] }}</option>
							@endforeach
						</optgroup>
					@endforeach
				</select>
			</div>
		</div>
		@foreach ($paymentGatewaySetting as $key => $values)
			@foreach ($values as $v)
			@if (isset($v['is_private']) && $v['is_private'] == 1 && strlen($v['image_url']) > 0)
					<div id="deposit_pay_channel_{{ $v['code'] }}" class="deposit_pay_channel_holder" style="display: none;">
				
						<div id="popupQRcode" class="cd-popup" role="alert">
							<div class="cd-popup-container" style="height:80%;margin: 1em auto;">
								<div class="" style="text-align: center;">
									{{--或扫描二维码支付：<br>--}}
									请扫描二维码支付：<br>
									<img src="{{$v['image_url']}}" style="width:220px; height:220px;">
									<br><br>
								</div>
								<div class="">
									<div class="col-md-6 col-xs-6 wallI" style="color: #000;">
										{{Lang::get('public.RemarkCode')}} :
									</div>
									<div class="col-md-6 col-xs-6 wallI">
										<input type="text" id='remarkcode' name='remarkcode' readonly>
									</div>
								</div>
								<div class="">
									<ul style="margin-left: 0; padding-left: 20px;">
										<li><span style="color: #ff0000;">充值提款中遇到任何问题请联系我们的24小时PC端在线客服</span></li>
										<li><span style="color: #ff0000;">请把此备注码拷贝并填入相关支付通道的备注中</span></li>
										<!--<li><span style="color: #ff0000;">支付完成后请一定要提交此表格，没有提交而造成的损失本网一概不负责</span></li>-->
										<li><span style="color: #000;">{{ Lang::get('COMMON.MINDEPOSIT') }}: {{ number_format($v['deposit_min'], 2) }}</span></li>
										<li><span style="color: #000;">{{ Lang::get('COMMON.MAXDEPOSIT') }}: {{ number_format($v['deposit_max'], 2) }}</span></li>
									</ul>
								</div>
								
								<ul class="cd-buttons">
									<li style="margin-top: 18px;"><a href="{{route('transaction')}}">{{Lang::get('public.Submit')}}</a></li>
								</ul>
							</div> <!-- cd-popup-container -->
						</div> 
						
						<input type="hidden" id="deposit_pay_channel_option" name="deposit_pay_channel_option" value="{{ $v['code'] }}" data-isprivate="{{ $v['is_private'] }}">



						{{--<div class="row pad01">--}}
							{{--<div class="col-md-12">--}}
								{{--<button type="button" class="btn btn-warning btn-block" onclick="window.open('{{ $v['url'] }}', '_blank')">点这里前往支付平台</button>--}}
							{{--</div>--}}
						{{--</div>--}}

						

					</div>
				@endif
			@endforeach
		@endforeach

		<div id="deposit_pay_channel_QYF" class="deposit_pay_channel_holder" style="display: none;">
			<div class="col-md-6 col-xs-3 wallI">
				{{Lang::get('public.PaymentChannelOption')}} :
			</div>
			<div class="col-md-6 col-xs-9 wallI">
				<select id="deposit_pay_channel_option" name="deposit_pay_channel_option">
					<option value="WX">微信支付</option>
					{{--<option value="WX_WAP">微信支付 (手机版)</option>--}}
					<option value="ZFB">支付宝</option>
					{{--<option value="ZFB_WAP">支付宝 (手机版)</option>--}}
					<option value="QQ">QQ钱包</option>
					{{--<option value="QQ_WAP">QQ钱包 (手机版)</option>--}}
				</select>
			</div>
		</div>
		<div id="deposit_pay_channel_DDB" class="deposit_pay_channel_holder" style="display: none;">
			<div class="col-md-6 col-xs-3 wallI">
				{{Lang::get('public.PaymentChannelOption')}} :
			</div>
			<div class="col-md-6 col-xs-9 wallI">
				<select id="deposit_pay_channel_option" name="deposit_pay_channel_option">
					<option value="weixin_scan">微信支付</option>
					<option value="alipay_scan">支付宝</option>
					<option value="qq_scan">QQ钱包</option>
					{{--<option value="jd_scan">京东扫码支付</option>--}}
					<option value="ylpay_scan">银联扫码支付</option>
				</select>
			</div>
		</div>
		<div id="deposit_pay_channel_ETP" class="deposit_pay_channel_holder" style="display: none;">
			<div class="col-md-6 col-xs-3 wallI">
				{{Lang::get('public.PaymentChannelOption')}} :
			</div>
			<div class="col-md-6 col-xs-9 wallI">
				<select id="deposit_pay_channel_option" name="deposit_pay_channel_option">
					<option value="bnk">银联支付</option>
					{{--<option value="wxMicro">微信支付</option>--}}
					{{--<option value="alipayMicro">支付宝</option>--}}
				</select>
			</div>
		</div>

		<div id="deposit_pay_channel_BNK" class="deposit_pay_channel_holder" style="display: none;">
			<div class="row pad01">
				<div class="col-md-12 col-xs-12 wallI">
					{{Lang::get('public.Deposit')}}{{Lang::get('public.Promotion')}} :
				</div>
				<div class="col-md-12 col-xs-12 wallI">
					<table class="w3-table-all" style="font-size: 15px;width:99%;">
						<thead>
						<tr class="w3-custom">
							<th align="center" style="width:2px;">&nbsp;</th>
							<th style="width:10px;">{{Lang::get('public.Bank')}}</th>
							<th>{{Lang::get('public.AccountName')}} / {{Lang::get('public.AccountNumber')}}</th>
						</tr>
						</thead>
						@if($checkBankAccountNumber>=1)
							@foreach( $banks as $key => $bank )
								<tr style="color:black;">
									<td style="width: 24px;"><input name="bank" class="radiobtn" type="radio" value="{{$bank['bnkid']}}"></td>
									<td><img style="height:45px;width:120px;" src="{{$bank['image']}}"></td>
									<td>
										{{$bank['bankaccname']}}<br>
										{{$bank['bankaccno']}}<br>
										{{Lang::get('public.Minimum')}} {{$bank['min']}}<br>
										{{Lang::get('public.Maximum')}}{{$bank['max']}}
									</td>
								</tr>
							@endforeach
						@else<a href="javascript:void(0);" onclick="window.open('https://chat.livechatvalue.com/chat/chatClient/chatbox.jsp?companyID=905849&configID=54544&jid=6705510837&s=1','mywin','width=800,height=700')">{{Lang::get('public.ClickHereForGetBankAccountNumber')}}</a></td>
						@foreach( $banks as $key => $bank )
							<tr style="color:black;">
								<td><input name="bank" class="radiobtn" type="radio" value="{{$bank['bnkid']}}"></td>
								<td><img style="height:45px;width:120px;" src="{{$bank['image']}}"></td>
								<td>
									{{substr($bank['bankaccname'], 0, 5)}}*******<br>
									{{substr($bank['bankaccno'], 0, 12)}}*******<br>
									{{Lang::get('public.Minimum')}} {{$bank['min']}}<br>
									{{Lang::get('public.Maximum')}}{{$bank['max']}}
								</td>
							</tr>
						@endforeach
						@endif
					</table>
				</div>
			</div>

			<div class="row pad01">
				<div class="col-md-6 col-xs-3">
					{{Lang::get('public.DepositMethod')}} :
				</div>
				<div class="col-md-6 col-xs-9 wallI">
					<select class="form-control" name="type">
						<option value="0">{{ Lang::get('public.OverCounter') }}</option>
						<option value="1">{{ Lang::get('public.InternetBanking') }}</option>
						<option value="2">{{ Lang::get('public.ATMBanking') }}</option>
					</select>
				</div>
			</div>
			<div class="row pad01">
				<div class="col-md-6 col-xs-3">
					<div class="col-md-12 col-xs-12  padtp1">
						{{Lang::get('public.DateTime')}} :
					</div>
				</div>
				<div class="col-md-6 col-xs-3 wallI">
					<select name="hours" class="form-control">
						<option value="01">01</option>
						<option value="02">02</option>
						<option value="03">03</option>
						<option value="04">04</option>
						<option value="05">05</option>
						<option value="06">06</option>
						<option value="07">07</option>
						<option value="08">08</option>
						<option value="09">09</option>
						<option value="10">10</option>
						<option value="11">11</option>
						<option value="12">12</option>
					</select>
				</div>
				<div class="col-md-6 col-xs-3 wallI">
					<select class="form-control" name="minutes">
						<option value="00">00</option>
						<option value="01">01</option>
						<option value="02">02</option>
						<option value="03">03</option>
						<option value="04">04</option>
						<option value="05">05</option>
						<option value="06">06</option>
						<option value="07">07</option>
						<option value="08">08</option>
						<option value="09">09</option>
						<option value="10">10</option>
						<option value="11">11</option>
						<option value="12">12</option>
						<option value="13">13</option>
						<option value="14">14</option>
						<option value="15">15</option>
						<option value="16">16</option>
						<option value="17">17</option>
						<option value="18">18</option>
						<option value="19">19</option>
						<option value="20">20</option>
						<option value="21">21</option>
						<option value="22">22</option>
						<option value="23">23</option>
						<option value="24">24</option>
						<option value="25">25</option>
						<option value="26">26</option>
						<option value="27">27</option>
						<option value="28">28</option>
						<option value="29">29</option>
						<option value="30">30</option>
						<option value="31">31</option>
						<option value="32">32</option>
						<option value="33">33</option>
						<option value="34">34</option>
						<option value="35">35</option>
						<option value="36">36</option>
						<option value="37">37</option>
						<option value="38">38</option>
						<option value="39">39</option>
						<option value="40">40</option>
						<option value="41">41</option>
						<option value="42">42</option>
						<option value="43">43</option>
						<option value="44">44</option>
						<option value="45">45</option>
						<option value="46">46</option>
						<option value="47">47</option>
						<option value="48">48</option>
						<option value="49">49</option>
						<option value="50">50</option>
						<option value="51">51</option>
						<option value="52">52</option>
						<option value="53">53</option>
						<option value="54">54</option>
						<option value="55">55</option>
						<option value="56">56</option>
						<option value="57">57</option>
						<option value="58">58</option>
						<option value="59">59</option>
					</select>
				</div>
				<div class="col-md-6 col-xs-3 wallI">
					<select class="form-control" name="range">
						<option value="AM">AM</option>
						<option value="PM">PM</option>
					</select>
				</div>
				<div class="col-md-6 col-xs-3 wallI">
				</div>
				<div class="col-md-6 col-xs-9 wallI">
					<input type="text" id="deposit_date" class="datepicker" style="cursor:pointer;" name="date" value="{{date('Y-m-d')}}">
				</div>
			</div>
			<div class="row pad01">
				<div class="col-md-6 col-xs-3 wallI">
					{{Lang::get('public.RemarkCode')}} :
				</div>
				<div class="col-md-6 col-xs-9 wallI">
					<input type="text" id='remarkcode' name='remarkcode' readonly>
					<label>*请把拷贝此码并填入网银备注码</label>
				</div>
			</div>
			<div class="row pad01">
				<div class="col-md-6 col-xs-3 wallI">
					{{Lang::get('public.DepositReceipt')}} :
				</div>
				<div class="col-md-6 col-xs-9 wallI">
					<input class="upload" type="file" name="file" id="file">
				</div>
			</div>

		</div>
<div id="afterhidden">
		@if(!empty($promos))
			<div class="row pad01">
				<div class="col-md-12 col-xs-12 wallI">
					{{Lang::get('public.Deposit')}}{{Lang::get('public.Promotion')}} :
				</div>
				<div class="col-md-12 col-xs-12 wallI">
					<table width="auto" border="0" cellspacing="0" cellpadding="0">
						@foreach( $promos as $key => $promo )
							<tr>
								<td style="width:12%"><input name="promo" class="form-control" type="radio" value="{{$promo['code']}}" style="margin-top:10px" ></td>
								<td><p>{{$promo['name']}}</p><img src="{{$promo['image']}}" style="height:auto;width:92%;margin-top:-10px;"></td>
							</tr>
						@endforeach
						<tr>
							<td><input name="promo" class="radiobtn" type="radio" value="0" style="" ></td>
							<td><p style="color:#fff">{{Lang::get('public.IDoNotWantPromotion')}}</p></td>
						</tr>
					</table>
				</div>
			</div>
		@endif

		<div class="row pad01">
			<div class="col-md-6 col-xs-9 wallI">
				<input class="radiobtn" type="radio" name="rules" style="width:12%;">
				<span>{{Lang::get('public.IAlreadyUnderstandRules')}}</span>
			</div>
		</div>

		<div class="row pad01 padB">
			<div class="col-md-12 col-xs-12">
				<div class="submitBtn pull-right">
					<a id="deposit_submit_btn" href="javascript:void(0)" onclick="deposit_submit(true)">{{Lang::get('public.Submit')}}</a>
				</div>
			</div>
		</div>
</div>
		<div id="weixinReturnURL" class="cd-popup" role="alert">
			<div class="cd-popup-container"><br><br>
				<div id="bui_qrcode"></div>
				<p style="color:black;" id="wechatTEXT"></p>
				<ul class="cd-buttons">
					<li><a href="{{route('transaction')}}">{{Lang::get('public.Submit')}}</a></li>
				</ul>
			</div> <!-- cd-popup-container -->
		</div> 
	</form>
	<script src="{{ asset('/gsc/resources/js/popupmain.js') }}"></script>
@stop