<script>
	$(document).ready(function() { 
		getBalance(true);
	});
</script>
<div style="height:310px;">
	<div class="row pad01">

	<div class="col-md-6 col-xs-6">
	<div class="row">
	<div class="col-md-12 col-xs-12 text-center">
	  <strong>{{Lang::get('public.Mainwallet')}}</strong>
	  </div>
	<div class="col-md-12 col-xs-12 text-center  wallI">
	<input type="text" value="{{App\Http\Controllers\User\WalletController::mainwallet()}}" readonly>
	</div>
	</div>
	</div>

	<div class="col-md-6 col-xs-6">
	<div class="row">
	<div class="col-md-12 col-xs-12 text-center">
	<strong>{{Lang::get('public.Total')}}</strong>
	</div>
	<div class="col-md-12 col-xs-12 text-center  wallI">
	<input type="text" class="total_balance" value="0.00" readonly>
	</div>
	</div>
	</div>

	</div>

	<div class="row pad01">
	@foreach(Session::get('products_obj') as $prdid => $object)
	<?php $count = 2 ; ?>
		<div class="col-md-6 col-xs-6">
			<div class="row wallet<?php echo $count++; ?>">
				<div class="col-md-12 col-xs-12 text-center">
					<strong>{{$object->getNameWithMultiLang(Lang::getLocale(), false)}}</strong>
				</div>
				<div class="col-md-12 col-xs-12 text-center  wallI {{$object->code}}_balance2">
					<input type="text" class="total_balance" value="0.00" readonly>
				</div>
			</div>
		</div>
	@endforeach
	</div>
</div>
<a onclick="getBalance()" id="getbal" href="#" style="text-decoration:none;color:black"><span class="main-set-title2" hidden>{{lang::get('public.RefreshWallet')}}</span></a> 