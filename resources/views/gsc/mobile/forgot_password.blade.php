@extends('gsc/mobile/master')
@section('content')
<script>
  function forgotpassword_submit(){

	$.ajax({
		type: "POST",
		url: "{{route('resetpassword')}}",
		data: {
			_token: "{{ csrf_token() }}",
			username:		 $('#fg_username').val(),
			email:    		 $('#fg_email').val(),

		},
	}).done(function( json ) {
			 $('.acctTextReg').html('');
			 obj = JSON.parse(json);
			 var str = '';
			 $.each(obj, function(i, item) {
				if( '{{Lang::get('COMMON.SUCESSFUL')}}' == item ){
					window.location.href = "{{route('homepage')}}";
				}else{
					$('.'+i+'_acctTextReg').html(item);
					
				}
			})
	
	});
}
</script>
<div class="row nopad marginTop01">
<div class="col-md-12 col-xs-12 text-center clr2 hg">
<span class="cornMid"><strong>{{Lang::get('public.ForgotPassword')}}</strong></span>
</div>
</div>

	<div class="row pad01">
		<div class="row">
			<div class="col-md-12 col-xs-12 text-black lblmar">
				<label>{{Lang::get('public.ForgotPasswordContent2')}}</label>
			</div>

		</div>
		
		<div class="row">
			<div class="col-md-12 col-xs-12 text-black lblmar">
				<label>{{Lang::get('public.Username')}}*:</label>
			</div>
			<div class="col-md-12 col-xs-12 text-black wallI">
				<input class="form-control" id="fg_username" type="text"><span class="acctTextReg"></span>
			</div>

		</div>

		<div class="row padTB01">
			<div class="col-md-12 col-xs-12 text-black lblmar">
				<label>{{Lang::get('public.EmailAddress')}}*:</label>
			</div>
			<div class="col-md-12 col-xs-12 text-black wallI">
				 <input class="form-control" id="fg_email" type="text"><span class="acctTextReg"></span>
			</div>
		</div>
		
		<div class="row pad01 padB">
			<div class="col-md-8 col-xs-8 pad01">
			</div>
			<div class="col-md-4 col-xs-4 pad01">
				<div class="submitBtn">
				<a id="btn_submit_transfer" href="javascript:void(0)" onClick="forgotpassword_submit()" >{{Lang::get('public.Submit')}}</a>
				</div>
			</div>
		</div>
	</div>

@stop