@extends('gsc/mobile/master')
@section('content')
<script>
    var transferringToMain = false;
    
    function transferToMain() {
        if (!transferringToMain) {
            if (confirm("{{ Lang::get('public.AreYouSureYouWanToTransferAllGameBalancesToMainWallet') }}")) {
                transferringToMain = true;
                
//                $(".transfer_to_main_icon").hide();
//                $(".transfer_to_main_loading").show();
                
                var gameBalances = {};
                var lastGameCode = "";
                
                $(".transferable_game_balance").each(function () {
                    try {
                        var gameCode = $(this).data("game-code");
                        var gameBalance = parseFloat($(this).val());

                        if (!isNaN(gameBalance) && gameBalance > 0) {
                            gameBalances[gameCode] = gameBalance;
                            lastGameCode = gameCode;
                        }
                    } catch (e) {
                        // Do nothing.
                    }
                });
                
                if (lastGameCode != "") {
                    for (var key in gameBalances) {
                        if (gameBalances.hasOwnProperty(key)) {
                            submitTransfer(key, gameBalances[key], lastGameCode);
                        }
                    }
                } else {
                    resetTransferStatus();
                }
            }
        }
    }
    
    function submitTransfer(gameCode, gameBalance, lastGameCode) {
		
		if( gameCode == 'BBN' || gameCode == 'MNT' )
		{
			gameBalance = Math.floor(gameBalance);
		}
		
        $.ajax({
            type: "POST",
            url: "{{route('transfer-process')}}",
            data: {
				_token: "{{ csrf_token() }}",
                amount: gameBalance,
                from: gameCode,
                to: "MAIN"
            },
            complete: function (jqXHR, textStatus) {
                if (gameCode == lastGameCode) {
                    resetTransferStatus();
                }
            }
        });
    }
    
    function resetTransferStatus() {
        transferringToMain = false;

        $(".transfer_to_main_loading").hide();
        $(".transfer_to_main_icon").show();

        getBalance(true);
    }
</script>
<div class="row nopad marginTop01">
	<div class="col-md-12 col-xs-12 text-center clr2 hg">
		<span class="cornMid">{{Lang::get('public.Wallet')}}</span>
	</div>
</div>
@include('gsc/mobile/trans_top' )
<div class="row pad01">
	<div class="col-md-6 col-xs-6">
		<div class="submitBtn">
			<a href="javascript:void(0);" onClick="getBalance()">{{Lang::get('public.Refresh')}}</a>
		</div>
	</div>
	<div class="col-md-6 col-xs-6">
		<div class="submitBtn">
			<a href="javascript:void(0);" onClick="transferToMain()">{{Lang::get('public.TransferToMain')}}</a>
		</div>
	</div>
</div>
		<table cellspacing="0" border="0" style="border-collapse:collapse;display:none;">
				<tbody>
					<tr>
						<th align="center" scope="col">{{Lang::get('public.Wallet')}}</th>
						<th align="center" scope="col">{{Lang::get('public.Balance')}}</th>
						<th align="center" scope="col"></th>
					</tr>
					<tr>
						<td align="center">{{Lang::get('public.MainWallet')}}</td>
						<td align="center" class="main_wallet">0.0</td>
						<td align="center"></td>
					</tr>
				@foreach( Session::get('products_obj') as $prdid => $object)
					<tr>
						<td align="center">{{$object->getNameWithMultiLang(Lang::getLocale(), false)}}</td>
						<td align="center" class="{{$object->code}}_balance">0.00</td>
						<td align="center" style="display:none;" class="{{$object->code}}_balance2"></td>
					</tr>
				@endforeach
					</tr>
				</tbody>
			</table>
@stop