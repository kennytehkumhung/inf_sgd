@extends('gsc/mobile/master')
@section('content')
<div style="height:500px;">
	<div class="row pad01">
		<div class="row">
			<div class="col-md-12 col-xs-12 text-black lblmar">
				 <img src="{{url()}}/gsc/img/fish-bg.png" style="width:100%;">
			</div>
		</div>
	</div>
	<div class="col-md-6 col-xs-6">
        <img class="img-responsive" src="{{url()}}/gsc/mobile/resources/img/MetroUI_OS_Android.png" style="width:220px;height:220px;margin:auto;">

        <div class="mobLink">
            <a href="https://fir.im/u4z8" target="_blank" style="width:100%;height:45px;font-size:20px;">安卓 APP 下载</a>
        </div>
        <div class="mobLink" style="visibility: hidden;">
            <a href="#"></a>
        </div>
        <div class="clearfix"></div>
    </div>
	
	<div class="col-md-6 col-xs-6">
        <img class="img-responsive" src="{{url()}}/gsc/mobile/resources/img/MetroUI_OS_Apple.png" style="width:220px;height:220px;margin:auto;">

        <div class="mobLink">
            <a href="https://fir.im/u4z8" target="_blank" style="width:100%;height:45px;font-size:20px;">IOS APP 下载</a>
        </div>
        <div class="mobLink" style="visibility: hidden;">
            <a href="#"></a>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
	
@stop