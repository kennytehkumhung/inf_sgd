@extends('gsc/mobile/master')
@section('content')
<div class="row nopad marginTop01">
<div class="col-md-12 col-xs-12 text-center clr2 hg">
<span class="cornMid"><strong>{{Lang::get('public.AboutUs')}}</strong></span>
</div>
</div>

<div class="row" style="height:900px;">
<div class="col-md-12 col-xs-12">
<p>
<?php echo htmlspecialchars_decode($content); ?>
</p>
</div>
</div>

@stop