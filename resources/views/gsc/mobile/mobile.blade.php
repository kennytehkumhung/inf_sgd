@extends('gsc/mobile/master')
@section('content')
<div class="row pad01 marginBottom01 padB">

    <div class="col-md-6 col-xs-6">
        <img class="img-responsive" src="{{url()}}/gsc/mobile/resources/img/mob-dl-ag.png">

        <div class="mobLink">
            <a href="http://agmbet.com/">安卓 APP 下载</a>
        </div>
        <div class="mobLink" style="visibility: hidden;">
            <a href="#"></a>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="col-md-6 col-xs-6">
        <img class="img-responsive" src="{{url()}}/gsc/mobile/resources/img/mob-dl-allb.png">

        <div class="mobLink">
            <a href="https://www.abgapp.net/">IOS APP 下载</a>
        </div>
        <div class="mobLink">
            <a href="https://www.abgapp.net/">安卓 APP 下载</a>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="col-md-6 col-xs-6">
        <img class="img-responsive" src="{{url()}}/gsc/mobile/resources/img/mob-dl-pt.png">

        <div class="mobLink">
            <a href="http://m.gm175788.com/live/download.html">安卓 APP 真人视频</a>
        </div>
        <div class="mobLink">
            <a href="http://m.gm175788.com/download.html">安卓 APP 角子机</a>
        </div>
        <div class="clearfix"></div>
    </div>
	
	<div class="col-md-6 col-xs-6">
        <img class="img-responsive" src="{{url()}}/gsc/mobile/resources/img/mob-dl-fish.png">

        <div class="mobLink">
            <a href="https://fir.im/u4z8" target="_blank" >IOS APP 下载</a>
        </div>
        <div class="mobLink">
            <a href="https://fir.im/u4z8" target="_blank" >安卓 APP 下载</a>
        </div>
        <div class="clearfix"></div>
    </div>

</div>
@stop