@extends('gsc/mobile/master')
@section('content')
<div class="row nopad marginTop01">
	<div class="col-md-6 col-xs-6 text-center clr2 hg">
		<span class="cornMid"><a style="color:#ffd478;font-weight: bold;" onClick="registerButton('nor')">{{Lang::get('public.Registration')}}</a></span>
	</div>
	<div class="col-md-6 col-xs-6 text-center clr2 hg">
		<span class="cornMid"><a style="color:#ffd478;font-weight: bold;" onClick="registerButton('fas')">快速注册</a></span>
	</div>
</div>
<div id="normalRegister">
	<div class="row pad01">
		<div class="row">
			<div class="col-md-12 col-xs-12 text-black lblmar">
				<label style="color:#FF0000;">{{Lang::get('public.EmailAddress')}}*:</label>
			</div>
			<div class="col-md-12 col-xs-12 text-black wallI">
				<input  class="form-control" id="r_email" type="email"><span class="email_acctTextReg acctTextReg"></span>
			</div>
			<div class="col-md-12 col-xs-12 text-danger wallI" style="color:#ffd478;">
			*{{Lang::get('public.UseToFindPasswordByEmail')}}
			</div>
		</div>

		<div class="row padTB01">
			<div class="col-md-12 col-xs-12 text-black lblmar">
				<label style="color:#FF0000;">{{Lang::get('public.ContactNo')}}*:</label>
			</div>
			<div class="col-md-12 col-xs-12 text-black wallI">
				 <input  class="form-control" id="r_mobile" type="tel"><span class="mobile_acctTextReg acctTextReg"></span>
			</div>
		</div>

		<div class="row padTB01">
			<div class="col-md-12 col-xs-12 text-black lblmar">
				<label style="color:#FF0000;">{{Lang::get('public.FullName')}}*:</label>
			</div>
			<div class="col-md-12 col-xs-12 text-black wallI">
				<input  class="form-control" id="r_fullname" type="text"><span class="fullname_acctTextReg acctTextReg"></span>
			</div>
			<div class="col-md-12 col-xs-12 text-danger wallI" style="color:#ffd478;">
			*{{Lang::get('public.NameMatchWithrawalBank')}}
			</div>
		</div>

		<div class="row padTB01">
			<div class="col-md-12 col-xs-12 text-black lblmar">
				<label>{{Lang::get('public.DOB')}}</label>
			</div>
			<div class="col-md-12 col-xs-12 text-black wallI">
				<div class="col-md-4 col-xs-4 text-black wallI">
					<select id="dob_year" class="short combine">
						<option value="2006">2006</option>
						<option value="2005">2005</option>
						<option value="2004">2004</option>
						<option value="2003">2003</option>
						<option value="2002">2002</option>
						<option value="2001">2001</option>
						<option value="2000">2000</option>
						<option value="1999">1999</option>
						<option value="1998">1998</option>
						<option value="1997">1997</option>
						<option value="1996">1996</option>
						<option value="1995">1995</option>
						<option value="1994">1994</option>
						<option value="1993">1993</option>
						<option value="1992">1992</option>
						<option value="1991">1991</option>
						<option value="1990">1990</option>
						<option value="1989">1989</option>
						<option value="1988">1988</option>
						<option value="1987">1987</option>
						<option value="1986">1986</option>
						<option value="1985">1985</option>
						<option value="1984">1984</option>
						<option value="1983">1983</option>
						<option value="1982">1982</option>
						<option value="1981">1981</option>
						<option value="1980">1980</option>
						<option value="1979">1979</option>
						<option value="1978">1978</option>
						<option value="1977">1977</option>
						<option value="1976">1976</option>
						<option value="1975">1975</option>
						<option value="1974">1974</option>
						<option value="1973">1973</option>
						<option value="1972">1972</option>
						<option value="1971">1971</option>
						<option value="1970">1970</option>
						<option value="1969">1969</option>
						<option value="1968">1968</option>
						<option value="1967">1967</option>
						<option value="1966">1966</option>
						<option value="1965">1965</option>
						<option value="1964">1964</option>
						<option value="1963">1963</option>
						<option value="1962">1962</option>
						<option value="1961">1961</option>
						<option value="1960">1960</option>	
					</select>
				</div>
				<div class="col-md-4 col-xs-4 text-black wallI">
					<select id="dob_month" class="short combine">
						<option value="01">{{Lang::get('public.January')}}</option>
						<option value="02">{{Lang::get('public.February')}}</option>
						<option value="03">{{Lang::get('public.March')}}</option>
						<option value="04">{{Lang::get('public.April')}}</option>
						<option value="05">{{Lang::get('public.May')}}</option>
						<option value="06">{{Lang::get('public.June')}}</option>
						<option value="07">{{Lang::get('public.July')}}</option>
						<option value="08">{{Lang::get('public.August')}}</option>
						<option value="09">{{Lang::get('public.September')}}</option>
						<option value="10">{{Lang::get('public.October')}}</option>
						<option value="11">{{Lang::get('public.November')}}</option>
						<option value="12">{{Lang::get('public.December')}}</option>
					</select>
				</div>
				<div class="col-md-4 col-xs-4 text-black wallI">
					<select id="dob_day" class="short combine">
						<option value="01">01</option>
						<option value="02">02</option>
						<option value="03">03</option>
						<option value="04">04</option>
						<option value="05">05</option>
						<option value="06">06</option>
						<option value="07">07</option>
						<option value="08">08</option>
						<option value="09">09</option>
						<option value="10">10</option>
						<option value="11">11</option>
						<option value="12">12</option>
						<option value="13">13</option>
						<option value="14">14</option>
						<option value="15">15</option>
						<option value="16">16</option>
						<option value="17">17</option>
						<option value="18">18</option>
						<option value="19">19</option>
						<option value="20">20</option>
						<option value="21">21</option>
						<option value="22">22</option>
						<option value="23">23</option>
						<option value="24">24</option>
						<option value="25">25</option>
						<option value="26">26</option>
						<option value="27">27</option>
						<option value="28">28</option>
						<option value="29">29</option>
						<option value="30">30</option>
						<option value="31">31</option>
					</select>
				</div>
			</div>
			<input type="hidden" id="dob">
		</div>

		<div class="row padTB01">
			<div class="col-md-12 col-xs-12 text-black lblmar">
				<label style="color:#FF0000;">{{Lang::get('public.Username')}}*:</label>
			</div>
			<div class="col-md-12 col-xs-12 text-black wallI">
				<input  class="form-control" id="r_username" type="text"><span class="username_acctTextReg acctTextReg"></span>
			</div>
			<div class="col-md-12 col-xs-12 text-danger wallI" style="color:#ffd478;">
			*{{Lang::get('public.UsernamePolicyContent')}}
			</div>
		</div>

		<div class="row padTB01">
			<div class="col-md-12 col-xs-12 text-black lblmar">
				<label style="color:#FF0000;">{{Lang::get('public.Password')}}*:</label>
			</div>
			<div class="col-md-12 col-xs-12 text-black wallI">
				 <input  class="form-control" id="r_password" type="password"><span class="password_acctTextReg acctTextReg"></span>
			</div>
		</div>



		<div class="row padTB01">
			<div class="col-md-12 col-xs-12 text-black lblmar">
				<label style="color:#FF0000;">{{Lang::get('public.RepeatPassword')}}:</label>
			</div>
			<div class="col-md-12 col-xs-12 text-black lblmar">
				<input  class="form-control" id="r_repeatpassword" type="password"><span class="acctTextReg"></span>
			</div>
			<div class="col-md-12 col-xs-12 text-danger wallI" style="color:#ffd478;">
			*{{Lang::get('public.PasswordPolicyContent')}}
			</div>
		</div>
		
		<div class="row padTB01">
			<div class="col-md-12 col-xs-12 text-black lblmar">
				<label>{{Lang::get('public.RegisterCode')}}*:</label>
			</div>
			<div class="col-md-12 col-xs-12 text-black lblmar">
				@if (Session::has('user_refcode'))
					<input class="form-control" type="text" value="{{Session::get('user_refcode')}}" disabled>
					<input id="r_referralcode" type="hidden" value="{{Session::get('user_refcode')}}">
				@else
					<input class="form-control" id="r_referralcode" type="text" value="">
				@endif
				<span class="referralcode_acctTextReg acctTextReg"></span>
			</div>
		</div>

		<div class="row pad01 padB">
			<div class="col-md-4 col-xs-4 pad01">
				<div class="submitBtn">
				<a href="#" onClick="register_submit()" > {{Lang::get('public.Submit')}}</a>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="fastRegister" style="display:none;">
	<div class="row pad01">
		<div class="row">
			<div class="col-md-12 col-xs-12 text-black lblmar">
				<label style="color:#FF0000;">{{Lang::get('public.Username')}}*:</label>
			</div>
			<div class="col-md-12 col-xs-12 text-black wallI">
				<input  class="form-control" id="r_username2" type="text"><span class="email_acctTextReg acctTextReg"></span>
			</div>
		</div>
		
		<div class="row padTB01">
			<div class="col-md-12 col-xs-12 text-black lblmar">
				<label style="color:#FF0000;">{{Lang::get('public.ContactNo')}}*:</label>
			</div>
			<div class="col-md-12 col-xs-12 text-black wallI">
				<input  class="form-control" id="r_mobile2" type="tel"><span class="mobile_acctTextReg acctTextReg"></span>
			</div>
		</div>
		
		<div class="row padTB01">
			<div class="col-md-12 col-xs-12 text-black lblmar">
				<label style="color:#FF0000;">{{Lang::get('public.Password')}}*:</label>
			</div>
			<div class="col-md-12 col-xs-12 text-black wallI">
				<input  class="form-control" id="r_password2" type="password"><span class="password_acctTextReg acctTextReg"></span>
			</div>
		</div>
		
		<div class="row padTB01">
			<div class="col-md-12 col-xs-12 text-black lblmar">
				<label style="color:#FF0000;">{{Lang::get('public.RepeatPassword')}}*:</label>
			</div>
			<div class="col-md-12 col-xs-12 text-black wallI">
				<input  class="form-control" id="r_repeatpassword2" type="password"><span class="acctTextReg"></span>
			</div>
		</div>
		
		<div class="row padTB01">
			<div class="col-md-12 col-xs-12 text-black lblmar">
				<label>{{Lang::get('public.RegisterCode')}}*:</label>
			</div>
			<div class="col-md-12 col-xs-12 text-black wallI">
				@if (Session::has('user_refcode'))
					<input class="form-control" type="text" value="{{Session::get('user_refcode')}}" disabled>
					<input id="r_referralcode2" type="hidden" value="{{Session::get('user_refcode')}}">
				@else
					<input  class="form-control" id="r_referralcode2" type="text" value="">
				@endif
				<span class="referralcode_acctTextReg acctTextReg"></span>
			</div>
		</div>
		
		<div class="row pad01 padB">
			<div class="col-md-4 col-xs-4 pad01">
				<div class="submitBtn">
				<a href="#" onClick="register_submit2()" > {{Lang::get('public.Submit')}}</a>
				</div>
			</div>
		</div>
	</div>
</div>
@stop