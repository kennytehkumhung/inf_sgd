@extends('gsc/mobile/master')
@section('content')
<div class="row pad01 bgB padS">
	<div class="col-xs-12">
		<iframe id="iframe_game" frameborder="0" style="overflow:hidden;overflow-x:hidden;overflow-y:hidden;height: 98%; min-height: 2000px; width:100%;" src="
		@if( $type == 'plt' )
			{{route('plt' , [ 'type' => 'pgames' , 'category' => '1' ] )}}
		@elseif( $type == 'spg' )
			{{route('spg', [ 'type' => 'slot' , 'category' => 'slot' ] )}}
		@elseif( $type == 'mig' )
			{{route('mig')}}
		@endif
		" onload="resizeIframe(this)"></iframe>
	</div>
</div>
@stop
