@extends('gsc/master')

@section('title', Lang::get('public.LiveCasino'))

@section('content')
<div class="casinoTop">
   <div class="casinoTopinner">
      <div class="slotTitle">真人娱乐场</div>
      <div class="casinoBx space20">
         <a href="#" onClick="@if (Auth::user()->check())window.open('{{route('agg')}}', 'casinoagg', 'width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif" class="hvr-pulse-grow"><img src="{{url()}}/gsc/img/casino-thumb-4.png"></a>
      </div>
      <div class="casinoBx space20">
         <a href="#" onClick="@if (Auth::user()->check())window.open('{{route('alb')}}', 'casinoalb', 'width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif" class="hvr-pulse-grow"><img src="{{url()}}/gsc/img/casino-thumb-2.png"></a>
      </div>
      <div class="casinoBx space20">
         <a href="#" onClick="@if (Auth::user()->check())window.open('{{route('bbn')}}', 'casinobbn', 'width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif" class="hvr-pulse-grow"><img src="{{url()}}/gsc/img/casino-thumb-3.png"></a>
      </div>
      <div class="casinoBx">
         <a href="#" onClick="@if (Auth::user()->check())window.open('{{route('pltiframe')}}', 'casinoplt', 'width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif" class="hvr-pulse-grow"><img src="{{url()}}/gsc/img/pt-cs-thumb.png"></a>
      </div>
      <div class="clr"></div>
   </div>
</div>
@stop