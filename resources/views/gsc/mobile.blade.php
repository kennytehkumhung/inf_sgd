@extends('gsc/master')
@section('title', Lang::get('public.MobileDownload'))
@section('content')
<div class="mobileTop">
   <div class="mobileTopinner">
      <div class="slotTitle">{{Lang::get('public.MobileDownload')}}</div>
     
      <div class="mobBx ag right20">
      <div class="qrBx" style="right:100px;">
         <div class="mobT1">真人娱乐场</div>
         <img src="{{url()}}/gsc/img/ag_iphone_qr.jpg" style="height:100px;width:100px;">
         <div class="mobT1">
         <div class="icon">
         <img src="{{url()}}/gsc/img/android-icon.jpg">
         </div>
         <div class="clr"></div>
         </div>
      </div>
	  <div class="qrBx02">
	  <div class="iconTxt"><br><br>
	  @if(Session::has('username'))
			您的用户名：<br>
			AVGS_<span style="text-transform: uppercase;">{{Session::get('username')}}</span>
		 @else
			请登入获取用户名
		 @endif
	  </div>
      </div>
      </div>
      
      <div class="mobBx allb">
      <div class="qrBx">
         <div class="mobT1">真人娱乐场</div>
         <img src="{{url()}}/gsc/img/allbet_qrcode.png" style="height:100px;width:100px;">
         <div class="mobT1">
         <div class="icon">
         <img src="{{url()}}/gsc/img/android-icon.jpg">
         </div>
         <div class="clr"></div>
         </div>
      </div>
      <div class="qrBx01">
         <div class="mobT1">真人娱乐场</div>
         <img src="{{url()}}/gsc/img/allbet_qrcode.png" style="height:100px;width:100px;">
         <div class="mobT1">
         <div class="icon">
         <img src="{{url()}}/gsc/img/ios-icon.jpg">
         </div>
         <div class="clr"></div>
         </div>
      </div>
	  <div class="qrBx02">
	  <div class="iconTxt"><br><br>
	  @if(Session::has('username'))
			您的用户名：<br>
			GS_<span style="text-transform: uppercase;">{{Session::get('username')}}</span>AV1
		 @else
			请登入获取用户名
		 @endif
	  </div>
      </div>
      </div>
	  
	  <div class="mobBx bb right20">
      <div class="qrBx">
         <div class="mobT1">真人娱乐场</div>
         <img src="{{url()}}/gsc/img/bbn_android_qrcode.jpg" style="height:100px;width:100px;">
         <div class="mobT1">
         <div class="icon">
         <img src="{{url()}}/gsc/img/android-icon.jpg">
         </div>
         <div class="clr"></div>
         </div>
      </div>
      <div class="qrBx01">
         <div class="mobT1">真人娱乐场</div>
         <img src="{{url()}}/gsc/img/bbn_apple_qrcode.jpg" style="height:100px;width:100px;">
         <div class="mobT1">
         <div class="icon">
         <img src="{{url()}}/gsc/img/ios-icon.jpg">
         </div>
         <div class="clr"></div>
         </div>
      </div>
	  <div class="qrBx02">
	  <div class="iconTxt"><br><br>
	  @if(Session::has('username'))
			您的用户名：<br>
			GSC<span style="text-transform: uppercase;">{{Session::get('username')}}</span>@BB9
		 @else
			请登入获取用户名
		 @endif
	  </div>
      </div>
      </div>
	  
	  <div class="mobBx pt">
      <div class="qrBx">
         <div class="mobT1">老虎机游戏</div>
         <img src="{{url()}}/gsc/img/plt_slot.png" style="height:100px;width:100px;">
         <div class="mobT1">
         <div class="icon">
         <img src="{{url()}}/gsc/img/android-icon.jpg">
         </div>

         <div class="clr"></div>
         </div>
      </div>
      <div class="qrBx01">
         <div class="mobT1">真人娱乐场</div>
         <img src="{{url()}}/gsc/img/plt_liveCasino.png" style="height:100px;width:100px;">
         <div class="mobT1">
         <div class="icon">
         <img src="{{url()}}/gsc/img/android-icon.jpg">
         </div>
         <div class="clr"></div>
         </div>
      </div>
	  <div class="qrBx02">
	  <div class="iconTxt">
	   <a href="http://cdn.betlink.info/setupglx.exe" target="blank"><img src="{{url()}}/gsc/img/windows-icon.png"></a><br>
	  @if(Session::has('username'))
			您的用户名：<br>
			CGSC_<span style="text-transform: uppercase;">{{Session::get('username')}}</span>
		 @else
			请登入获取用户名
		 @endif
	  </div>
      </div>	
      </div>
      
      
      <div class="clr"></div>
   </div>
</div>
@stop