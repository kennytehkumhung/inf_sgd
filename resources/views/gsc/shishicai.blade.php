@extends('gsc/master')

@section('title', (Lang::getLocale() == 'en' ? 'iLotto' : '时时彩'))

@section('content')
<div class="dTop">
   <div class="dTopinner">
      <div class="slotTitle">{{Lang::get('public.Lottery')}}</div>
      <div class="dBx">
         <a href="javascript:void(0)" onClick="@if (Auth::user()->check())window.open('{{route('ilt')}}', 'casinoplt', 'width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif" class="hvr-pulse-grow"><img src="{{url()}}/gsc/img/4d-big.png"></a>
      </div>
   </div>
</div>
<div class="lotBtm">
    <div class="lotBtminner">
       <div class="lotBx right10">
       <a href="javascript:void(0)" class="hvr-pulse-grow"><img src="{{url()}}/gsc/img/lottery-thumb-1.png" ></a>
       </div>
       <div class="lotBx right10">
       <a href="javascript:void(0)" class="hvr-pulse-grow"><img src="{{url()}}/gsc/img/lottery-thumb-2.png"></a>
       </div>
       <div class="lotBx right10">
       <a href="javascript:void(0)" class="hvr-pulse-grow"><img src="{{url()}}/gsc/img/lottery-thumb-3.png"></a>
       </div>
       <div class="lotBx">
       <a href="javascript:void(0)" class="hvr-pulse-grow"><img src="{{url()}}/gsc/img/lottery-thumb-4.png"></a>
       </div>
       <div class="lotBx">
       <a href="javascript:void(0)" class="hvr-pulse-grow"><img src="{{url()}}/gsc/img/lottery-thumb-5.png"></a>
       </div>
    <div class="clr"></div>
    </div>
</div>
@stop