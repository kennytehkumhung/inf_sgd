@extends('gsc/master')

@section('title', Lang::get('public.ContactUs'))

@section('content')
<div class="contactTop">
   <div class="contactTopinner">
      <div class="slotTitle">{{Lang::get('public.ContactUs')}}</div>
         <div class="contactImg">
            <div class="ctBox">
            {!! htmlspecialchars_decode($content) !!}
            </div>
         </div>
      <div class="clr"></div>
   </div>
</div>
@stop