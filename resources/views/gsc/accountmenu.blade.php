      <ul>
		<li><a href="{{route('deposit')}}">{{Lang::get('public.Deposit')}}</a></li>
		<li><a href="{{route('withdraw')}}">{{Lang::get('public.Withdrawal')}}</a></li>
		<li><a href="{{route('transaction')}}">{{Lang::get('public.TransactionHistory')}}</a></li>
		<li><a href="{{route('transfer')}}">{{Lang::get('public.Transfer2')}}</a></li>
		<li><a href="{{route('transfertomain')}}">{{Lang::get('public.TransferToMain')}}</a></li>
      </ul>