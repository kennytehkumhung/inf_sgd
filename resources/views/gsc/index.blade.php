@extends('gsc/master')
@section('content')
	<script type="text/javascript" language="javascript">
		$(document).ready(function() {
			$('.box_skitter_large').skitter({
				theme: 'clean',
				numbers_align: 'center',
				progressbar: false,
				dots: true,
				preview: false
			});
		});
	</script>
	<script>
          $(document).ready(function () {
			@if (Auth::user()->check())
				@if($popup_banner)
					$("#popupbanner").click();
				@endif
			@endif
            fakeJackport();
            setInterval(updateJackport, 1000);
          });

          function fakeJackport() {

              var value = Math.floor(Math.random() * 10);

              value = 1356289 + value * 2.496;


              var value2 = value.toFixed(2);

              $('#jp_1').text(addCommas(value2));

          }

          function updateJackport() {
              var val = $("span[id^='jp_1']").text();
              val = removeComma(val);

              if (val > 1358889)
                  val = 1356289;

              var value = Math.floor(Math.random() * 10);
              value = value * 0.027;

              var value2 = (parseFloat(val) + value).toFixed(2);

              $('#jp_1').text(addCommas(value2));
          }

          function addCommas(str) {
              var parts = (str + "").split("."),
          main = parts[0],
          len = main.length,
          output = "",
          i = len - 1;

              while (i >= 0) {
                  output = main.charAt(i) + output;
                  if ((len - i) % 3 === 0 && i > 0) {
                      output = "," + output;
                  }
                  --i;
              }
              // put decimal part back
              if (parts.length > 1) {
                  output += "." + parts[1];
              }
              return output;
          }

          function removeComma(str) {
              var parts = (str + "").split(",");

              var output = "";

              for (var i = 0; i < parts.length; i++) {
                  output += parts[i];
              }
              return output;
          }
</script>
@if (Auth::user()->check())
	@if($popup_banner)
	<a data-remodal-target="modalInformation" id="popupbanner">
	<div class="remodal theme1" data-remodal-id="modalInformation" role="dialog" aria-labelledby="modal1Title" aria-describedby="modal1Desc" style="max-width:870px;">
	  <button data-remodal-action="close" class="remodal-close"></button><br><br>
		<img src="{!! $popup_banner !!}" style="margin-top:-30px;width:100%;"/>
	</div>
	@endif
@endif
<!--Mid Sect-->
<div class="midSect">


<!--Slider-->
<div class="sliderCont">
<div class="sliderContInner">
	<div class="box_skitter box_skitter_large">
		<ul>
	@if(isset($banners))
		@foreach( $banners as $key => $banner )
			<li><a href="{{route('promotion')}}"><img src="{{$banner->domain}}/{{$banner->path}}" style="height:392px;width:1084px;" class="directionLeft" /></a></li>
		@endforeach
	@endif
		</ul>
	</div>
</div>
</div>
<!--Slider-->
<div class="midSectInner">

<div class="sect1 adj09">
   <div class="mirrorBx adj10">
	   <a href="#register"  style="text-decoration: none;">
		  <div class="stp1"><img src="{{ static_asset('/gsc/img/stp-1.png') }}"></div>
		  <div class="stpTxt">{{ Lang::get('public.SignUpFree') }}</div>
		  <div class="clr"></div>
		</a>
   </div>
   <div class="mirrorBx adj10">
	   <a href="#" style="text-decoration: none;" onclick="@if (Auth::user()->check()) window.location.href='{{route('deposit')}}' @else alert('{{Lang::get('COMMON.PLEASELOGIN')}}') @endif">
		  <div class="stp1"><img src="{{ static_asset('/gsc/img/stp-2.png') }}"></div>
		  <div class="stpTxt">{{ Lang::get('public.Deposit') }}</div>
		  <div class="clr"></div>
		</a>
   </div>
   <div class="mirrorBx adj10">
      <div class="stp1"><img src="{{ static_asset('/gsc/img/stp-4.png') }}"></div>
      <div class="stpTxt">领取红利</div>
      <div class="clr"></div>
   </div>
   <div class="mirrorBx ">
	   <a href="#" style="text-decoration: none;" onclick="@if (Auth::user()->check()) window.location.href='{{route('withdraw')}}' @else alert('{{Lang::get('COMMON.PLEASELOGIN')}}') @endif">
		  <div class="stp1"><img src="{{ static_asset('/gsc/img/stp-3.png') }}"></div>
		  <div class="stpTxt">{{ Lang::get('public.Withdrawal') }}</div>
		  <div class="clr"></div>
		</a>
   </div>
   <div class="clr"></div>
</div>

<div class="sect2 adj09">
  <div id="big-image">
  	<div class="box_skitter box_skitter_large">
		<ul>
			<li><a href="javascript:void(0);" onClick="@if (Auth::user()->check())window.open('{{route('pltiframe')}}', 'casinoplt', 'width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif">
				<img src="{{ static_asset('/gsc/img/pt-hpg.png') }}" style="height:392px;width:1084px;" class="directionLeft" /></a>
			</li>
			<li><a href="javascript:void(0);" onClick="@if (Auth::user()->check())window.open('{{route('alb')}}', 'casinoplt', 'width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif">
				<img src="{{ static_asset('/gsc/img/allb-hpg.png') }}" style="height:392px;width:1084px;" class="directionLeft" /></a>
			</li>
			<li><a href="javascript:void(0);" onClick="@if (Auth::user()->check())window.open('{{route('bbn')}}', 'casinoplt', 'width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif">
				<img src="{{ static_asset('/gsc/img/bbin-hpg.png') }}" style="height:392px;width:1084px;" class="directionLeft" /></a>
			</li>
			<li><a href="javascript:void(0);" onClick="@if (Auth::user()->check())window.open('{{route('pltiframe')}}', 'casinoplt', 'width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif">
				<img src="{{ static_asset('/gsc/img/ag-hpg.png') }}" style="height:392px;width:1084px;" class="directionLeft" /></a>
			</li>
		</ul>
	</div>
</div>

<div class="small-images">
	<a href="javascript:void(0);"  onClick="@if (Auth::user()->check())window.open('{{route('pltiframe')}}', 'casinoplt', 'width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif" >
		<img src="{{ static_asset('/gsc/img/pt-hpg-small.jpg') }}">
	</a>
	<a href="javascript:void(0);" onClick="@if (Auth::user()->check())window.open('{{route('alb')}}', 'casinoalb', 'width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif">
		<img src="{{ static_asset('/gsc/img/allb-hpg-small.jpg') }}">
	</a>
	<a href="javascript:void(0);" onClick="@if (Auth::user()->check())window.open('{{route('bbn')}}', 'casinobbn', 'width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif">
		<img src="{{ static_asset('/gsc/img/bbin-hpg-small.jpg') }}">
	</a>
	<a href="javascript:void(0);" onClick="@if (Auth::user()->check())window.open('{{route('agg')}}', 'casinoagg', 'width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif">
		<img src="{{ static_asset('/gsc/img/ag-hpg-small.jpg') }}">
	</a>
</div>
<script>
$(function(){
    $("#big-image img:eq(0)").nextAll().hide();
    $(".small-images img").click(function(e){
        var index = $(this).index();
        $("#big-image img").eq(index).show().siblings().hide();
    });
});
</script>
</div>

<div class="btm">
<div class="btmInner">
   <div class="btmBx right20">
      <h4>优越服务</h4>
      <div class="bar">
      <div class="barLeft">25秒</div>
      <div class="barRight">存款到帐<br>
平均时间</div>
      <div class="clr"></div>
      </div>
      <div class="bar">
      <div class="barLeft">2'57分</div>
      <div class="barRight">取款到帐<br>
平均时间</div>
      <div class="clr"></div>
      </div>
      <div class="bar">
      <div class="barLeft">34家</div>
      <div class="barRight">目前支持<br>
已有银行</div>
      <div class="clr"></div>
      </div>
      </div>

      <div class="btmBx">
         <div class="jp">
			<div class="jpCounter">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;RMB&nbsp;<span id="jp_1">88,888.888.00</span></div>
         </div>
      </div>

      <div class="btmBx right20">
      <h4 class="borderBtm">产品优势</h4>

      <div class="rowed">
         <div class="tit">电子游艺</div>
         <span>8000多款老虎机、电动扑克、大型电玩、桌上游戏、
以丰富的视觉、声光效果提供您一级的娱乐</span>
      </div>

      <div class="rowed">
         <div class="tit">真人娱乐</div>
         <span>8提供真人娱乐、百家乐、骰宝、二八杠、龙虎斗、轮盘、金臂......精彩内容面向全玩家。</span>
      </div>

      <div class="rowed">
         <div class="tit">彩票游戏</div>
         <span>彩票游戏丰富，提供最高赔率，所有赛果依据官方开奖结果。彩票游戏给您一夜致富的机会及空前的游戏体验！</span>
      </div>

      </div>

      <div class="clr"></div>

</div>
</div>
</div>
@stop