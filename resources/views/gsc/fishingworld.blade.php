@extends('gsc/master')

@section('title', Lang::get('public.Slots'))

@section('content')
<div class="fishTop">
   <div class="fishTopinner">
      <img src="{{url()}}/gsc/img/fish-bg.png">
   </div>
</div>

<div class="fishBtm">
    <div class="fishBtminner">
      <div class="fishBxdl right20">
      <img src="{{url()}}/gsc/img/dl-android.png">
       <a href="#"><img src="{{url()}}/gsc/img/mtfishing_qr.jpg" style="height:160px;width:160px;"></a>
       <div class="dlBtn">
          <a href="https://fir.im/u4z8" target="_blank">{{ Lang::get('public.DownloadNow') }}</a>
       </div>
       </div>
	          <div class="fishBxdl right20">
      <img src="{{url()}}/gsc/img/sl-ios.png">
       <a href="#"><img src="{{url()}}/gsc/img/mtfishing_qr.jpg" style="height:160px;width:160px;"></a>
       <div class="dlBtn">
          <a href="https://fir.im/u4z8" target="_blank">{{ Lang::get('public.DownloadNow') }}</a>
       </div>
       </div>
       <div class="fishBxdl right20">
      <img src="{{url()}}/gsc/img/pc-version2.png">
       <a href="#"><img src="{{url()}}/gsc/img/pc-dl.png"></a>
       <div class="dlBtn">
          <a href="{{url()}}/gsc/mobile/mtgame-pc_1024.exe" download>{{ Lang::get('public.DownloadNow') }}</a>
       </div>
       </div>
	          <div class="fishBxdl">
      <img src="{{url()}}/gsc/img/haiwang3icon.png" style="height:60px;width:160px;">
       <a href="#"><img src="{{url()}}/gsc/img/haiwang3.jpg" style="height:160px;width:160px;"></a>
       <div class="dlBtn">
          <a href="http://47.88.217.136/update/download.html" target="blank">{{ Lang::get('public.DownloadNow') }}</a>
       </div>
       </div>
	  <div class="fishBxd2">
				<h3>
				@if (Auth::user()->check())
				   MT专用账户:<br> {{ strtoupper(Config::get(Session::get('currency').'.mnt.prefix') . Session::get('username')) }}
			   @else
				   {{ Lang::get('public.PleaseLoginToCheckYourUsername') }}
			   @endif
		   </h3>
       </div>   
	  <div class="fishBxd3">
				<h3>
				@if (Auth::user()->check())
				   下载app后请使用这个用户名
			   @else
				   
			   @endif
		   </h3>
       </div>  	   
    <div class="clr"></div>
    </div>
</div>
@stop