@extends('gsc/master')

@section('title', Lang::get('public.Deposit'))

@section('content')
<link href="{{url()}}/gsc/resources/css/acctMngmt.css" rel="stylesheet" type="text/css" />
<link href="{{url()}}/gsc/resources/css/table.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="{{ asset('/gsc/resources/js/jquery.form.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/front/resources/js/jquery.qrcode.min.js') }}"></script>
<script type="text/javascript" src="{{asset('/gsc/resources/js/jquery.blockUI.js')}}"></script>
<script type="text/javascript">
    setInterval(update_mainwallet, 10000);
    function deposit_submit(checkPayChannel) {

        var data = $("#deposit_form").serializeArray();
        var form_data = {};

        for (var i = 0; i < data.length; i++) {
            form_data[data[i].name] = data[i].value;
        }

        var submitBtnOriText = $('#deposit_submit_btn').text();

        if (checkPayChannel && ("deposit_pay_channel_option" in form_data)) {
            if ($("#deposit_pay_channel_option[value='" + form_data["deposit_pay_channel"] + "']").data("isprivate") == 1) {
                if (form_data["amount"] == null || form_data["amount"] <= 0) {
                    alert("{{ Lang::get('validation.required', array('attribute' => Lang::get('COMMON.AMOUNT'))) }}");
                    return false;
                } else if (form_data["promo"] == null) {
                    alert("{{ Lang::get('public.PleaseSelectAPromotion') }}");
                    return false;
				} else if (form_data["rules"] == null) {
                    alert("{{ Lang::get('validation.accepted', array('attribute' => Lang::get('public.TNC'))) }}");
                    return false;
				}

                var depositPayHolder = $("#deposit_pay_channel_" + form_data["deposit_pay_channel"]);
                var buiObj = $("#bui_privatepg");
				var privateType = $("#deposit_pay_private_type", depositPayHolder).val();
				var qrUrl = $("#deposit_pay_qr_url", depositPayHolder).val();
				var qrHint = "请使用相关APP扫描二维码";

				if (privateType == "WXP") {
                    qrHint = "请使用微信扫描二维码";
				} else if (privateType == "ZFB") {
                    qrHint = "请使用支付宝扫描二维码";
				}

                $("#bui_privatepg_qrcode", buiObj).attr("src", qrUrl);
                $("#bui_privatepg_qrlabel", buiObj).text(qrHint);
                $("#bui_privatepg_paymentchanned", buiObj).text($("option:selected", $("#deposit_pay_channel")).text());
                $("#bui_privatepg_btn_done", buiObj).val("{{ Lang::get('COMMON.SUBMIT') }}");

                $("#bui_privatepg_amount", buiObj).val($("#amount").val());
                $("#bui_privatepg_remarkcode", buiObj).val($("#remarkcode", depositPayHolder).val());
                $("#bui_privatepg_notes", buiObj).html($("#deposit_pay_notes", depositPayHolder).html());
                $("#bui_privatepg_upload", buiObj).click(function () {
                    $("#file").click();
				});

                $.blockUI({
                    css: { width: "850px", height: "440px", top: "10%", left: "10%" },
                    message: buiObj
                });

				resizeObject(buiObj);

                $("#bui_privatepg_btn_cancel", buiObj).click(function () {
                    $.unblockUI();
                });

                $("#bui_privatepg_btn_done", buiObj).click(function () {
                    $("#amount").val($("#bui_privatepg_amount", buiObj).val());
                    $("#refno", depositPayHolder).val($("#bui_privatepg_refno", buiObj).val());

                    $("#bui_privatepg_upload", buiObj).off("click");
                    $("#bui_privatepg_btn_cancel", buiObj).prop("disabled", true);
                    $("#bui_privatepg_btn_done", buiObj).val("{{ Lang::get('public.Loading') }}");
                    $("#bui_privatepg_btn_done", buiObj).off("click");
                    deposit_submit(false);
                });

                // Await input from BlockUI.
                return false;
			}
		}

        var options = {
            beforeSubmit: function (formData, jqForm, options) {
                $('#deposit_submit_btn').text('{{Lang::get('public.Loading')}}').attr('onclick', '');
            },
            complete: function (json, status) {
                var showModal = false;

                obj = JSON.parse(json.responseText);

                if (obj.code == "OK") {
                    if (obj.action == "drawqrcode") {
                        $("#bui_qrcode").qrcode({
                            width: 256,
                            height: 256,
                            text: obj.url
                        });

                        if (obj.hint.length > 0) {
                            $("#bui_qrlabel").html(obj.hint).show();
                        } else {
                            $("#bui_qrlabel").hide();
                        }

                        showModal = true;
                    } else {
                        if (obj.hint.length > 0) {
                            $("#bui_qrlabel").html(obj.hint).show();
                            showModal = true;
						}

                        if (obj.url.length > 0) {
                            $("#bui_qrcode").html('<a href="' + obj.url + '" target="_blank" style="line-height: 250px;">请点这里前往支付网关</a>').show();
                            showModal = true;
                        }
                    }

                    if (showModal) {
                        $.blockUI({
                            css: { width: "350px", height: "400px", top: "20%", left: "40%" },
                            message: $("#bui_drawqr")
                        });

                        $("#bui_btn_close").click(function () {
                            $.unblockUI();
                            window.location.href = "{{route('transaction')}}";
                        });
					}
                } else {
                    var str = '';
                    $.each(obj, function (i, item) {
                        if ('{{Lang::get('COMMON.SUCESSFUL')}}' == item) {
                            alert('{{Lang::get('COMMON.SUCESSFUL')}}');
                            window.location.href = "{{route('transaction')}}";
                            return;
                        } else {
                            //$('.'+i+'_acctTextReg').html(item);
//                            str += item + '<br>';
                            str += item + '\n';
                        }
                    });

                    if (str != '') {
//                    		$('.failed_message').html(str);
                        alert(str);
                    }
                }

                $('#deposit_submit_btn').text(submitBtnOriText).attr('onclick', 'deposit_submit(true)');
            }
        };

        $("#deposit_form").ajaxSubmit(options);

        // !!! Important !!!
        // always return false to prevent standard browser submit and page navigation
        return false;
    }
	function makeid() {
		var text = "";
		var possible = "0123456789";

		for (var i = 0; i < 5; i++){
			text += possible.charAt(Math.floor(Math.random() * possible.length));
		}
		return text;
	}
	function displaycode(){
	  $("[name='remarkcode']").val(makeid());
	}
    $(function () {
		displaycode();

        $("#deposit_pay_channel").change(function () {
            var payMethod = $(this).val();

            var holder = $(".deposit_pay_channel_holder");
            holder.hide();
            $("input, select, textarea", holder).prop("disabled", true);

            holder = $("#deposit_pay_channel_" + payMethod);
            $("input, select, textarea", holder).prop("disabled", false);
            holder.show();

            // Always enable this input as it use to upload receipt.
            $("#file").prop("disabled", false);
        }).change();
    });
</script>
<div class="brwn adj04">
 @include('gsc/deposit_trans_top' )
</div>
<div class="btm">
<div class="btmInner">

<!--ACCOUNT MANAGAMENT MENU-->
      <div class="inlineAccMenu">
		  @include('gsc/accountmenu' )
      </div>
<!--ACCOUNT MANAGAMENT MENU-->
<!--ACCOUNT TITLE-->
       <div class="title_bar">
      <span>{{Lang::get('public.Deposit')}}</span>
      </div>
<!--ACCOUNT TITLE-->
<!--ACCOUNT CONTENT-->
      <div class="DepContent">
      <div class="depLeft">
         <p><?php echo htmlspecialchars_decode($content); ?></p>
      </div>

      <div class="depRight">
		  <form id="deposit_form" action="{{route('deposit-process')}}" method="post" enctype="multipart/form-data">

			  <input type="hidden" id="_token" name="_token" value="{{csrf_token()}}">

			<span class="wallet_title"><i class="fa fa-money"></i>{{Lang::get('public.DepositBank')}}</span>
			<div class="acctRow">
				  <label>{{Lang::get('public.Amount')}} :</label>
				   <input type="text" class="form-control" id="amount" name="amount">
				  <div class="clr"></div>
			</div>
			  <div class="acctRow">
				  <?php
				  // Just for display.
				  if (!isset($paymentGatewaySetting['快捷通道'])) {
                      $paymentGatewaySetting['快捷通道'] = array();
				  }

                  $v = $paymentGatewaySetting['快捷通道'];
				  $v[] = array('code' => 'SNZ', 'name' => '刷脸支付（开发中）', 'disabled' => 'disabled');
                  $paymentGatewaySetting['快捷通道'] = $v;
				  ?>

				  <label>{{Lang::get('public.PaymentChannel')}} :</label>
				  <select id="deposit_pay_channel" name="deposit_pay_channel" style="min-width: 180px;">
					  @foreach ($paymentGatewaySetting as $key => $values)
						  <optgroup label="{{ $key }}">
							  @foreach ($values as $v)
								  <option value="{{ $v['code'] }}" {{ isset($v['disabled']) ? $v['disabled'] : '' }}>{{ $v['name'] }}</option>
							  @endforeach
						  </optgroup>
					  @endforeach
				  </select>
				  <div class="clr"></div>
			  </div>

			  @foreach ($paymentGatewaySetting as $key => $values)
				  @foreach ($values as $v)
					  @if (isset($v['is_private']) && $v['is_private'] == 1 && strlen($v['image_url']) > 0)
						  <div id="deposit_pay_channel_{{ $v['code'] }}" class="deposit_pay_channel_holder" style="display: none;">

							  <input type="hidden" id="deposit_pay_channel_option" name="deposit_pay_channel_option" value="{{ $v['code'] }}" data-isprivate="{{ $v['is_private'] }}">
							  <input type="hidden" id="deposit_pay_private_type" value="{{ $v['private_type'] }}">
							  <input type="hidden" id="deposit_pay_qr_url" value="{{ $v['image_url'] }}">

							  <div style="display: none;">
								  <input type="text" id='remarkcode' name='remarkcode' readonly>
								  <div id="deposit_pay_notes" style="display: none;">
									  <span style="color: #ff0000;">* 充值提款中遇到任何问题请联系我们的24小时PC端在线客服</span><br>
									  <span style="color: #ff0000;">* 请把此备注码拷贝并填入相关支付通道的备注中</span><br>
									  * {{ Lang::get('COMMON.MINDEPOSIT') }}: {{ number_format($v['deposit_min'], 2) }}<br>
									  * {{ Lang::get('COMMON.MAXDEPOSIT') }}: {{ number_format($v['deposit_max'], 2) }}<br>
								  </div>

								  <input type="text" class="form-control" name="refno" id="refno">
							  </div>
						  </div>
					  @endif
				  @endforeach
			  @endforeach

			  <div id="deposit_pay_channel_QYF" class="deposit_pay_channel_holder" style="display: none;">
				  <div class="acctRow">
					  <label>{{Lang::get('public.PaymentChannelOption')}} :</label>
					  <select id="deposit_pay_channel_option" name="deposit_pay_channel_option" style="min-width: 180px;">
						  <option value="WX">微信支付</option>
						  {{--<option value="WX_WAP">微信支付 (手机版)</option>--}}
						  <option value="ZFB">支付宝</option>
						  {{--<option value="ZFB_WAP">支付宝 (手机版)</option>--}}
						  <option value="QQ">QQ钱包</option>
						  {{--<option value="QQ_WAP">QQ钱包 (手机版)</option>--}}
					  </select>
					  <div class="clr"></div>
				  </div>
			  </div>
			  <div id="deposit_pay_channel_DDB" class="deposit_pay_channel_holder" style="display: none;">
				  <div class="acctRow">
					  <label>{{Lang::get('public.PaymentChannelOption')}} :</label>
					  <select id="deposit_pay_channel_option" name="deposit_pay_channel_option" style="min-width: 180px;">
						  <option value="weixin_scan">微信支付</option>
						  <option value="alipay_scan">支付宝</option>
						  <option value="qq_scan">QQ钱包</option>
						  {{--<option value="jd_scan">京东扫码支付</option>--}}
						  <option value="ylpay_scan">银联扫码支付</option>
					  </select>
					  <div class="clr"></div>
				  </div>
			  </div>
			  <div id="deposit_pay_channel_ETP" class="deposit_pay_channel_holder" style="display: none;">
				  <div class="acctRow">
					  <label>{{Lang::get('public.PaymentChannelOption')}} :</label>
					  <select id="deposit_pay_channel_option" name="deposit_pay_channel_option" style="min-width: 180px;">
						  <option value="bnk">银联支付</option>
						  {{--<option value="wxMicro">微信支付</option>--}}
						  {{--<option value="alipayMicro">支付宝</option>--}}
					  </select>
					  <div class="clr"></div>
				  </div>
			  </div>
			  <div id="deposit_pay_channel_BNK" class="deposit_pay_channel_holder" style="display: none;">
				  <div class="acctRow">
					  <label>{{Lang::get('public.Deposit')}}{{Lang::get('public.Promotion')}} :</label><br><br>
				  </div>
				  <table class="w3-table-all" style="font-size: 15px;">
					  <thead>
					  <tr class="w3-custom">
						  <th align="center" style="width:2px;">&nbsp;</th>
						  <th style="width:10px;">{{Lang::get('public.Bank')}}</th>
						  <th>{{Lang::get('public.AccountName')}} / {{Lang::get('public.AccountNumber')}}</th>
						  <th style="width:10px;">{{Lang::get('public.Minimum')}}</th>
						  <th style="width:10px;">{{Lang::get('public.Maximum')}}</th>
						  <th style="width:86px;">{{Lang::get('public.ProcessingTime')}}</th>
					  </tr>
					  </thead>
				@if($checkBankAccountNumber>=1)
					  @foreach( $banks as $key => $bank )
						  <tr style="color:black;">
							  <td><input name="bank" class="radiobtn" type="radio" value="{{$bank['bnkid']}}"></td>
							  <td><img style="height:45px;width:120px;" src="{{$bank['image']}}"></td>
							  <td>
								  {{$bank['bankaccname']}}<br>
								  {{$bank['bankaccno']}}
							  </td>
							  <td>{{$bank['min']}}</td>
							  <td>{{$bank['max']}}</td>
							  <td>1分钟</td>
						  </tr>
					  @endforeach
				@else<a href="javascript:void(0);" onclick="window.open('https://chat.livechatvalue.com/chat/chatClient/chatbox.jsp?companyID=905849&configID=54544&jid=6705510837&s=1','mywin','width=800,height=700')">{{Lang::get('public.ClickHereForGetBankAccountNumber')}}</a></td>
					@foreach( $banks as $key => $bank )
						  <tr style="color:black;">
							  <td><input name="bank" class="radiobtn" type="radio" value="{{$bank['bnkid']}}"></td>
							  <td><img style="height:45px;width:120px;" src="{{$bank['image']}}"></td>
							  <td>
								  {{substr($bank['bankaccname'], 0, 5)}}*******<br>
								  {{substr($bank['bankaccno'], 0, 12)}}*******
							  </td>
							  <td>{{$bank['min']}}</td>
							  <td>{{$bank['max']}}</td>
							  <td>1分钟</td>
						  </tr>
					  @endforeach
				@endif
				  </table>
				  <div class="clr"></div>
				  <br><br>
				  <div class="acctRow">
					  <label>{{Lang::get('public.DepositMethod')}} :</label>
					  <select class="form-control" id="type" name="type" style="min-width: 180px;">
						  <option value="0">{{ Lang::get('public.OverCounter') }}</option>
						  <option value="1">{{ Lang::get('public.InternetBanking') }}</option>
						  <option value="2">{{ Lang::get('public.ATMBanking') }}</option>
					  </select>
					  <div class="clr"></div>
				  </div><br>
				  <div class="acctRow">
					  <label>{{Lang::get('public.DateTime')}} :</label>
					  <select name="hours" class="form-control">
						  <option value="01">01</option>
						  <option value="02">02</option>
						  <option value="03">03</option>
						  <option value="04">04</option>
						  <option value="05">05</option>
						  <option value="06">06</option>
						  <option value="07">07</option>
						  <option value="08">08</option>
						  <option value="09">09</option>
						  <option value="10">10</option>
						  <option value="11">11</option>
						  <option value="12">12</option>
					  </select>
					  <select class="form-control" name="minutes">
						  <option value="00">00</option>
						  <option value="01">01</option>
						  <option value="02">02</option>
						  <option value="03">03</option>
						  <option value="04">04</option>
						  <option value="05">05</option>
						  <option value="06">06</option>
						  <option value="07">07</option>
						  <option value="08">08</option>
						  <option value="09">09</option>
						  <option value="10">10</option>
						  <option value="11">11</option>
						  <option value="12">12</option>
						  <option value="13">13</option>
						  <option value="14">14</option>
						  <option value="15">15</option>
						  <option value="16">16</option>
						  <option value="17">17</option>
						  <option value="18">18</option>
						  <option value="19">19</option>
						  <option value="20">20</option>
						  <option value="21">21</option>
						  <option value="22">22</option>
						  <option value="23">23</option>
						  <option value="24">24</option>
						  <option value="25">25</option>
						  <option value="26">26</option>
						  <option value="27">27</option>
						  <option value="28">28</option>
						  <option value="29">29</option>
						  <option value="30">30</option>
						  <option value="31">31</option>
						  <option value="32">32</option>
						  <option value="33">33</option>
						  <option value="34">34</option>
						  <option value="35">35</option>
						  <option value="36">36</option>
						  <option value="37">37</option>
						  <option value="38">38</option>
						  <option value="39">39</option>
						  <option value="40">40</option>
						  <option value="41">41</option>
						  <option value="42">42</option>
						  <option value="43">43</option>
						  <option value="44">44</option>
						  <option value="45">45</option>
						  <option value="46">46</option>
						  <option value="47">47</option>
						  <option value="48">48</option>
						  <option value="49">49</option>
						  <option value="50">50</option>
						  <option value="51">51</option>
						  <option value="52">52</option>
						  <option value="53">53</option>
						  <option value="54">54</option>
						  <option value="55">55</option>
						  <option value="56">56</option>
						  <option value="57">57</option>
						  <option value="58">58</option>
						  <option value="59">59</option>
					  </select>
					  <select class="form-control" name="range">
						  <option value="AM">AM</option>
						  <option value="PM">PM</option>
					  </select>
					  <input type="text" id="deposit_date" class="datepicker" style="cursor:pointer;" name="date" value="{{date('Y-m-d')}}">
					  <div class="clr"></div>
				  </div><br>
					<div class="acctRow" style="height: 48px;">
						<label>{{Lang::get('public.RemarkCode')}} :</label>
						<input type="text" id='remarkcode' name='remarkcode' readonly>
						<br>
						<p style="color: #ffffff; margin-left: 250px; color: #ff0000;">*请拷贝此码并填入网银备注码</p>
					</div><br>
				  <div class="acctRow">
					  <label>{{Lang::get('public.DepositReceipt')}} :</label>
					  <input class="upload" type="file" name="file" id="file">
					  <div class="clr"></div>
				  </div>
			  </div>
			<br>
			@if(!empty($promos))
			<div class="acctRow2">
				<label>{{Lang::get('public.Deposit')}}{{Lang::get('public.Promotion')}} :</label>
				  <table width="auto" border="0" cellspacing="0" cellpadding="0">
@foreach( $promos as $key => $promo )
					  <tr>
							<td style="width:5%"><input style="" name="promo" class="form-control" type="radio" value="{{$promo['code']}}"></td>
							<td><p>{{$promo['name']}}</p><img src="{{$promo['image']}}" style="height:auto;width:400px;;"></td>
					  </tr>
@endforeach
					  <tr>
							<td><input name="promo" class="radiobtn" type="radio" value="0">  </td>
							<td><p style="color:grey">{{Lang::get('public.IDoNotWantPromotion')}}</p></td>
					  </tr>
				  </table>
				  <div class="clr"></div>
			</div>
			@else
				  <input name="promo" type="hidden" value="0">
			@endif

			<div class="acctRow2">
				<label> {{Lang::get('public.RequiredFields')}}</label>
				<label><input class="radiobtn" type="radio" name="rules" >{{Lang::get('public.IAlreadyUnderstandRules')}}</label>
			</div><br>
				<div class="acctRow">
				  <span class="failed_message" style="display:block;float:left;height:100%;"></span>
				</div>
				<div class="joinBtn adj05">
					<a id="deposit_submit_btn" href="javascript:void(0)" onclick="deposit_submit(true)">{{Lang::get('public.Submit')}}</a>
				</div>
		  </form>
      </div>
      <div class="clr"></div>

      </div>
<!--ACCOUNT CONTENT-->
</div>
</div>

@section('modal')
<!-- Modal -->
<div id="bui_drawqr" style="display: none; text-align: center; cursor: default;">
	<div id="bui_qrcode" style="padding-top: 15px;"></div>
	<br>
	<span id="bui_qrlabel" style="display: none;"></span>
	<br><br>
	<input type="button" id="bui_btn_close" value="{{ Lang::get('COMMON.DONE') }}" />
</div>

<div id="bui_privatepg" style="display: none; cursor: default;">
	<table border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td style="text-align: center; width: 328px;">
				<img id="bui_privatepg_qrcode" src="" style="max-height: 360px; max-width: 324px;">
				<br><br>
				<span id="bui_privatepg_qrlabel"></span>
			</td>
			<td style="text-align: left;">
				<table border="0" cellpadding="5" cellspacing="5">
					<tr>
						<td style="{{ Lang::getLocale() == 'cn' ? 'width: 100px;' : 'width: 180px;' }}">
							<label>{{Lang::get('public.PaymentChannel')}} :</label>
						</td>
						<td>
							<span id="bui_privatepg_paymentchanned"></span>
						</td>
					</tr>
					<tr>
						<td style="width: 100px;">
							<label>{{Lang::get('public.Amount')}} :</label>
						</td>
						<td>
							<input type="text" id="bui_privatepg_amount" readonly>
						</td>
					</tr>
					<tr>
						<td>
							<label>{{Lang::get('public.RemarkCode')}} :</label>
						</td>
						<td>
							<input type="text" id="bui_privatepg_remarkcode" readonly>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<p id="bui_privatepg_notes" style="color: #000000;">
							</p>
						</td>
					</tr>
					<tr>
						<td>
							<label>{{Lang::get('public.DepositReceipt')}} :</label>
						</td>
						<td>
							<button type="button" id="bui_privatepg_upload" style="width: 100px;">{{ Lang::get("COMMON.UPLOADFILE") }}</button>
						</td>
					</tr>
					<tr>
						<td colspan="2" style="text-align: center;">
							<br>
							<input type="button" id="bui_privatepg_btn_cancel" value="{{ Lang::get('COMMON.CANCEL') }}" style="width: 100px;" />
							<input type="button" id="bui_privatepg_btn_done" value="{{ Lang::get('COMMON.SUBMIT') }}" style="margin-left: 30px; width: 100px;" />
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</div>
@endsection

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script>
$(function() {
	$( ".datepicker" ).datepicker({ dateFormat: 'yy-mm-dd'  , defaultDate: new Date() });

	$(window).resize(function () {
	    resizeObject($("#bui_drawqr"));
	    resizeObject($("#bui_privatepg"));
	});

//	$("#deposit_form").leanModal();
});

function resizeObject(obj) {
    obj = $(obj);

    if (obj.is(":visible")) {
        var windowWidth = $(window).width();
        var blockWidth = obj.width();

        obj.parent().css("left", ((windowWidth / 2) - (blockWidth / 2)));
    }
}

function cashcardPOST(){
        $.ajax({
            type: "POST",
            url: "{{action('User\CashcardController@depositProcess')}}?" + $("form").serialize(),
            data: {
                _token: "{{ csrf_token() }}"
            },
        }).done(function( json ) {
			 obj = JSON.parse(json);
			 var str = '';
			 $.each(obj, function(i, item) {
				if( '{{Lang::get('COMMON.SUCESSFUL')}}' == item ){
					alert('{{Lang::get('COMMON.SUCESSFUL')}}');
					window.location.href = "{{route('transaction')}}";
				}else{
					alert(item);
				}
			})

	});
}
</script>
@stop