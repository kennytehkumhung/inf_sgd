@extends('gsc/master')

@section('title', 'Forgot Password')

@section('top_js')
  <script>
  function forgotpassword_submit(){

	$.ajax({
		type: "POST",
		url: "{{route('resetpassword')}}",
		data: {
			_token: "{{ csrf_token() }}",
			username:		 $('#fg_username').val(),
			email:    		 $('#fg_email').val(),

		},
	}).done(function( json ) {
			 $('.acctTextReg').html('');
			 obj = JSON.parse(json);
			 var str = '';
			 $.each(obj, function(i, item) {
				if( '{{Lang::get('COMMON.SUCESSFUL')}}' == item ){
					window.location.href = "{{route('homepage')}}";
				}else{
					$('.'+i+'_acctTextReg').html(item);
					
				}
			})
	
	});
}
  </script>
@stop

@section('content')

<div class="contactTop">
   <div class="contactTopinnerForgotpass">
      <div class="slotTitle">{{Lang::get('public.ForgotPassword')}}</div>
         <div class="contactImgForgotpass">
            <div class="ctBox" style="text-align:left">
            {{Lang::get('public.ForgotPasswordContent2')}}
           <br>
           <br>
           <br>
			<div class="acctRow">
         <label> {{Lang::get('COMMON.USERNAME')}} :</label><input type="text" id="fg_username" ><span class="username_acctTextReg acctTextReg"></span>
      <div class="clr"></div>
    </div>
      <div class="acctRow">
      <label>{{Lang::get('public.EmailAddress')}} :</label><input type="email" id="fg_email"><span class="email_acctTextReg acctTextReg"></span>
      <div class="clr"></div>
      </div>
	  <div class="joinBtn adj05">
      <a href="javascript:void(0)" onClick="forgotpassword_submit()">{{Lang::get('public.Submit')}}</a>
      </div>
	   </div>
         </div>
      <div class="clr"></div>
   </div>
</div>
@stop

@section('bottom_js')

@stop

