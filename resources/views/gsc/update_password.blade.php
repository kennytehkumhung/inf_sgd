@extends('gsc/master')

@section('title', Lang::get('COMMON.PROFILE'))

@section('content')
<link href="{{url()}}/gsc/resources/css/acctMngmt.css" rel="stylesheet" type="text/css" />
<script>
	$(document).ready(function() { 
		getBalance(true);
	});
	
function checkPass()
{
    //Store the password field objects into variables ...
    var new_password = document.getElementById('new_password');
    var confirm_new_password = document.getElementById('confirm_new_password');
    //Store the Confimation Message Object ...
    var message = document.getElementById('confirmMessage');
    //Set the colors we will be using ...
    var goodColor = "#66cc66";
    var badColor = "#ff6666";
    //Compare the values in the password field 
    //and the confirmation field
    if(new_password.value == confirm_new_password.value){
        //The passwords match. 
        //Set the color to the good color and inform
        //the user that they have entered the correct password 
        confirm_new_password.style.backgroundColor = goodColor;
        message.style.color = goodColor;
        message.innerHTML = "{{ Lang::get('public.PasswordsMatch') }}"
    }else{
        //The passwords do not match.
        //Set the color to the bad color and
        //notify the user.
        confirm_new_password.style.backgroundColor = badColor;
        message.style.color = badColor;
        message.innerHTML = "{{ Lang::get('public.PasswordsDoNotMatch') }}"
    }
} 

 function update_password(){
	$.ajax({
		type: "POST",
		url: "{{action('User\MemberController@ChangePassword')}}",
		data: {
			_token: "{{ csrf_token() }}",
			current_password:		$('#current_password').val(),
			new_password:    		$('#new_password').val(),
			confirm_new_password:   $('#confirm_new_password').val()

		},
	}).done(function( json ) {
		
		obj = JSON.parse(json);
			 var str = '';
			 $.each(obj, function(i, item) {
				str += item + '\n';
			})
			
			alert(str);
					
	});
}
</script>
<div class="brwn adj04">
 @include('gsc/trans_top' )
</div>
<div class="btm">
<div class="btmInner">

<!--ACCOUNT MANAGAMENT MENU-->
      <div class="inlineAccMenu">
		<ul>
			<li><a href="{{route('update-profile')}}">{{Lang::get('public.AccountProfile')}}</a></li>
			<li><a href="{{route('update-password')}}">{{Lang::get('public.AccountPassword')}}</a></li>
			<li><a href="{{route('memberbank')}}">{{Lang::get('COMMON.CTABBANKSETTING')}}</a></li>
		</ul>
      </div>
<!--ACCOUNT MANAGAMENT MENU-->
<!--ACCOUNT TITLE-->
      <div class="title_bar">
      <span>{{Lang::get('public.AccountPassword')}}</span>
      </div>
<!--ACCOUNT TITLE-->
<!--ACCOUNT CONTENT-->
     <div class="acctContent">
      <span class="wallet_title"><i class="fa fa-pencil"></i>{{Lang::get('public.AccountPassword')}}</span>
      <div class="acctRow">
		  <label>{{Lang::get('public.CurrentPassword')}} :</label>
		  <input type="password" id="current_password"/>
		  <div class="clr"></div>
      </div>
      <div class="acctRow">
		  <label>{{Lang::get('public.NewPassword')}} :</label>
		  <input type="password" id="new_password"/>
		  <div class="clr"></div>
      </div>
      <div class="acctRow">
		  <label>{{Lang::get('public.ConfirmNewPassword')}} :</label>
		  <input type="password"  id="confirm_new_password" onkeyup="checkPass(); return false;"/>
		  <div class="clr"></div>
	  </div>
      <div class="joinBtn adj05">
			<a href="#" onclick="update_password()">{{Lang::get('public.Submit')}}</a>
      </div>
      <div class="clr"></div>
      </div>
<!--ACCOUNT CONTENT-->
</div>
</div>
@stop