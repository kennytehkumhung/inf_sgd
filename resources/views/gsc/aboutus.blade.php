@extends('gsc/master')

@section('title', Lang::get('public.AboutUs'))

@section('content')
<div class="promoTop">
   <div class="promoTopinner">
    <div class="slotTitle">{{Lang::get('public.AboutUs')}}</div>
    <div class="promoBlock" id="accordion">
		<?php echo htmlspecialchars_decode($content); ?>
    </div>
   </div>
</div>
@stop