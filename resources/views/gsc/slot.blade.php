@extends('gsc/master')

@section('title', Lang::get('public.Slots'))

@section('content')
 <script>
 $(document).ready(function() {



	@if( $type == 'plt' )
		$('.slot_plt_image').hide();
    @elseif( $type == 'spg' )
        $('.slot_spg_image').hide();
    @elseif( $type == 'uc8' )
        $('.slot_uc8_image').hide();
	@endif
 });
 function open_game(gameid){
	 

 }
   function resizeIframe(obj){
     obj.style.height = 0;
     obj.style.height = obj.contentWindow.document.body.scrollHeight + 'px';
  }
  
  function change_iframe(url,image_link,product){
	  //alert(product);
	  $('#iframe_game').attr('src',url);
	  $('.top').show();
	  $('.slot_'+product+'_image').hide();
  }
 </script>
<div class="slotTop">
   <div class="slotTopinner">
      <div class="slotTitle">{{Lang::get('public.SlotGames2')}}</div>
      <div class="slotLeft">
		<a href="javascript:void(0)" onClick="change_iframe('{{route('plt' , [ 'type' => 'pgames' , 'category' => '1' ] )}}','slot-pt.png','plt')">
			 <img class="hvr-pulse-grow" src="{{url()}}/gsc/img/slot-pt.png">
		</a>
		<a href="javascript:void(0)" onClick="@if (Auth::user()->check())window.open('{{route('bbn_slot')}}', '', 'width=1150,height=830')@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif">
			 <img class="hvr-pulse-grow" src="{{url()}}/gsc/img/bbin.png">
		</a>
      </div>
      <div class="slotRight">
		<a href="javascript:void(0)" onclick="@if(Auth::user()->check())change_iframe('{{route('mig')}}')@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif">
			<img class="hvr-pulse-grow" src="{{url()}}/gsc/img/slot-mg.png">
		</a>
		<a href="javascript:void(0)" onclick="change_iframe('{{route('spg', [ 'type' => 'slot' , 'category' => 'slot' ] )}}',null,'spg');">
			<img class="hvr-pulse-grow" src="{{url()}}/gsc/img/spead.png">
		</a>
      </div>
   </div>
</div>

   	<iframe id="iframe_game" frameborder="0" style="overflow:hidden;overflow-x:hidden;overflow-y:hidden;height: 98%; min-height: 2000px; width:100%;" src="
	@if( $type == 'plt' )
		{{route('plt' , [ 'type' => 'pgames' , 'category' => '1' ] )}}
	@elseif( $type == 'spg' )
		{{route('spg', [ 'type' => 'slot' , 'category' => 'slot' ] )}}
	@elseif( $type == 'mig' )
		{{route('mig')}}
	@endif
	" onload="resizeIframe(this)"></iframe>
@stop