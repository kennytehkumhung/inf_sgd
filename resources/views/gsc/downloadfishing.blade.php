@extends('gsc/master')
@section('title', 'Mobile Download')
@section('content')
<div class="fishTop">
   <div class="fishTopinner">
   <div class="slotTitle"><img src="{{url()}}/gsc/img/fish-dl-title.png"></div>
      <div class="fishBxdl right20">
      <img src="{{url()}}/gsc/img/dl-android.png">
       <a href="#"><img src="{{url()}}/gsc/img/dl-qr.jpg"></a>
       <div class="dlBtn">
          <a href="https://fir.im/2sxg" target="_blank">{{ Lang::get('public.DownloadNow') }}</a>
       </div>
       </div>
       <div class="fishBxdl right20">
      <img src="{{url()}}/gsc/img/sl-ios.png">
       <a href="#"><img src="{{url()}}/gsc/img/dl-qr.jpg"></a>
       <div class="dlBtn">
          <a href="https://fir.im/2sxg" target="_blank">{{ Lang::get('public.DownloadNow') }}</a>
       </div>
       </div>
       <div class="fishBxdl">
      <img src="{{url()}}/gsc/img/pc-version.png">
       <a href="#"><img src="{{url()}}/gsc/img/pc-dl.png"></a>
       <div class="dlBtn">
          <a href="{{url()}}/gsc/mobile/mtgame-pc_1024.exe" download>{{ Lang::get('public.DownloadNow') }}</a>
       </div>
       </div>
    <div class="clr"></div>
       <p style="text-align: center; padding: 40px 0;">
           @if (Auth::user()->check())
               {{ Lang::get('public.Username') }}: {{ strtoupper(Config::get(Session::get('currency').'.mnt.prefix') . Session::get('username')) }}
           @else
               {{ Lang::get('public.PleaseLoginToCheckYourUsername') }}
           @endif
       </p>
   </div>
</div>
@stop