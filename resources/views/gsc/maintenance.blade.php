@extends('gsc/master')

@section('title', Lang::get('public.Maintenance'))

@section('content')
<div class="spTop">
	<div class="noneTopinner">
		<div class="outer">
			<div class="msgBox1">
				<div class="excl">
					<img src="{{url()}}/gsc/img/under-maintenance-cn.png" alt=""/>
				</div>
				<div class="clr"></div>
			</div>
		</div>
	</div>
	<div class="clr"></div>
</div>
@stop