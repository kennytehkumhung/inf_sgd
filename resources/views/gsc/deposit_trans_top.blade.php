<script>
$(document).ready(function() { 
	getBalance(true);
});
</script>
<div class="brwnInner">
<div class="walletTop">
	<div class="walletdeposit ">
	<b><a onclick="getBalance()" href="#" style="text-decoration:none;color:black"><span class="main-set-title2">{{lang::get('public.RefreshWallet')}}</span></a></b>
	</div>
		<div class="wallet1">
			<div class="main-set-title">{{Lang::get('public.Mainwallet')}}</div>
			<div class="main-setPrice main_wallet">{{App\Http\Controllers\User\WalletController::mainwallet()}}</div>
		</div>
		<?php $count = 2 ; ?>
		@foreach( Session::get('products_obj') as $prdid => $object)
		<div class="wallet<?php echo $count++; ?>">
			<div class="main-set-title">{{$object->getNameWithMultiLang(Lang::getLocale(), false)}}</div>
			<div class="main-setPrice {{$object->code}}_balance">0.00</div>
		</div>
		@endforeach
		<div class="wallet11">
			<div class="main-set-title">{{Lang::get('public.Total')}}</div>
			<div class="main-setPrice total_balance">0.00</div>
		</div>
		<div class="clr"></div>
	</div>
</div>