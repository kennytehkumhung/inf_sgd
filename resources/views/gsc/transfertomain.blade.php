@extends('gsc/master')
@section('title', 'Transfer')
@section('content')
<link href="{{url()}}/gsc/resources/css/acctMngmt.css" rel="stylesheet" type="text/css" />
<script>
    var transferringToMain = false;
    
    function transferToMain() {
        if (!transferringToMain) {
            if (confirm("{{ Lang::get('public.AreYouSureYouWanToTransferAllGameBalancesToMainWallet') }}")) {
                transferringToMain = true;
                
//                $(".transfer_to_main_icon").hide();
//                $(".transfer_to_main_loading").show();
                
                var gameBalances = {};
                var lastGameCode = "";
                
                $(".transferable_game_balance").each(function () {
                    try {
                        var gameCode = $(this).data("game-code");
                        var gameBalance = parseFloat($(this).val());

                        if (!isNaN(gameBalance) && gameBalance > 0) {
                            gameBalances[gameCode] = gameBalance;
                            lastGameCode = gameCode;
                        }
                    } catch (e) {
                        // Do nothing.
                    }
                });
                
                if (lastGameCode != "") {
                    for (var key in gameBalances) {
                        if (gameBalances.hasOwnProperty(key)) {
                            submitTransfer(key, gameBalances[key], lastGameCode);
                        }
                    }
                } else {
                    resetTransferStatus();
                }
            }
        }
    }
    
    function submitTransfer(gameCode, gameBalance, lastGameCode) {
		
		if( gameCode == 'BBN' || gameCode == 'MNT' )
		{
			gameBalance = Math.floor(gameBalance);
		}
		
        $.ajax({
            type: "POST",
            url: "{{route('transfer-process')}}",
            data: {
				_token: "{{ csrf_token() }}",
                amount: gameBalance,
                from: gameCode,
                to: "MAIN"
            },
            complete: function (jqXHR, textStatus) {
                if (gameCode == lastGameCode) {
                    resetTransferStatus();
                }
            }
        });
    }
    
    function resetTransferStatus() {
        transferringToMain = false;

        $(".transfer_to_main_loading").hide();
        $(".transfer_to_main_icon").show();

        getBalance(true);
    }
</script>
<div class="brwn adj04">
 @include('gsc/trans_top' )
</div>
<div class="btm">
<div class="btmInner">

<!--ACCOUNT MANAGAMENT MENU-->
      <div class="inlineAccMenu">
		@include('gsc/accountmenu' )
      </div>
<!--ACCOUNT MANAGAMENT MENU-->
<!--ACCOUNT TITLE-->
      <div class="title_bar">
      <span>{{Lang::get('public.TransferToMain')}}</span>
      </div>
<!--ACCOUNT TITLE-->
<!--ACCOUNT CONTENT-->
    <div class="acctContent">
		<div class="mtTable">
			<table cellspacing="0" border="0" style="border-collapse:collapse;">
				<tbody>
					<tr>
						<th align="center" scope="col">{{Lang::get('public.Wallet')}}</th>
						<th align="center" scope="col">{{Lang::get('public.Balance')}}</th>
						<th align="center" scope="col"></th>
					</tr>
					<tr>
						<td align="center">{{Lang::get('public.MainWallet')}}</td>
						<td align="center" class="main_wallet">0.0</td>
						<td align="center"></td>
					</tr>
				@foreach( Session::get('products_obj') as $prdid => $object)
					<tr>
						<td align="center">{{$object->getNameWithMultiLang(Lang::getLocale(), false)}}</td>
						<td align="center" class="{{$object->code}}_balance">0.00</td>
						<td align="center" style="display:none;" class="{{$object->code}}_balance2"></td>
					</tr>
				@endforeach
					</tr>
				</tbody>
			</table>
		</div>
		<br>
<div class="joinBtn adj05">
	<a href="javascript:void(0);" onClick="getBalance()">{{Lang::get('public.Refresh')}}</a>
</div>
<div class="joinBtn adj05" style="margin-left:10px;">
	<a href="javascript:void(0);" onClick="transferToMain()" style="width:130px;">{{Lang::get('public.TransferToMain')}}</a>
</div>
	</div>
<!--ACCOUNT CONTENT-->
</div>
</div>
@stop