<html>
<head>



<link rel="stylesheet" href="https://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.css">
<script src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
<script src="https://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.js"></script>
	<style type="text/css">
		{
			color: #000000;
			font-family: Arial, Helvetica, sans-serif, "KaiTi", "楷体", STKaiti, "华文楷体";
		}

		table, table td {
			background-color: #fff;
			border: 1px #ebc675 solid;
			color: #000;
			text-align: justify;
			vertical-align: top;
		}

		table thead td {
			background-color: #ebc675;
			color: #000;
			font-weight: bold;
			text-align: center;
			vertical-align: middle;
		}
	</style>

</head>
<body bgcolor="black">
	<table border="0" cellpadding="10" cellspacing="0" style="width: 100%;">
		<thead>
			<tr>
				<td style="width: 10%;" colspan='2'>{{ Lang::get('public.inboxTitle') }}</td>
				<td style="width: 15%;">{{ Lang::get('public.inboxDate') }}</td>
			</tr>
		</thead>
		<tbody>
		<?php $no = 1; ?>
		@foreach($inboxMessage as $key => $val)
			<tr>
				<td style="width: 10%;">{{$no++}}</td>
				<td>
					@if($val['status']==0)
						<b style="color:red;">new</b>
					@endif
					<a href="#myPopup{{$val['id']}}" data-rel="popup"  onclick="updateStatus({{$val['id']}},{{$val['is_all']}})" data-position-to="window">{{$val['title']}}</a>
				</td>
				<td>{{$val['date']}}</td>
			</tr>
		@endforeach
		</tbody>
	</table>
	
	<div data-role="main" class="ui-content">
		@foreach($inboxMessage as $key => $val)
		<div data-role="popup" id="myPopup{{$val['id']}}" class="ui-content" data-dismissible="false" style="max-width:400px;">
		  <a href="#" data-rel="back" class="ui-btn ui-corner-all ui-shadow ui-btn ui-icon-delete ui-btn-icon-notext ui-btn-right">{{ Lang::get('COMMON.CLOSE') }}</a>
			<p>
				{{$val['content']}}
			</p>
		</div>
		@endforeach
	</div>
	

</body>
<script>

</script>
<script>
function updateStatus(id,is_all){
        $.ajax({
            type: "POST",
            url: "{{action('User\InboxMessage@viewMessage')}}?id="+id+"&is_all="+is_all,
            data: {
                _token: "{{ csrf_token() }}"
            },
        });
}
</script>
</html>