@extends('gsc/master')

@section('title', Lang::get('public.Withdrawal'))

@section('content')
<link href="{{url()}}/gsc/resources/css/acctMngmt.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
	TransactionCount();
	setInterval(update_mainwallet, 10000);
	function submit_transfer(){
		var myform = $('#withdraw_form');
		var disabled = myform.find(':input:disabled').removeAttr('disabled');
		
		$.ajax({
			 type: "POST",
			 url: "{{route('withdraw-process')}}?"+myform.serialize(),
			 data: {
				_token: 		 "{{ csrf_token() }}",
			 },
			 beforeSend: function(){
				$('#withdraw_btn_submit').attr('onClick','');
				$('#withdraw_btn_submit').html('Loading...');
			 },
			 success: function(json){
					disabled.attr('disabled','disabled');
					obj = JSON.parse(json);
					 var str = '';
					 $.each(obj, function(i, item) {
						
						if( '{{Lang::get('COMMON.SUCESSFUL')}}' == item ){
							alert('{{Lang::get('COMMON.SUCESSFUL')}}');
							window.location.href = "{{route('transaction')}}";
						}else{
							str += item + '<br>';
						}
					})
					$('.failed_message').html(str);
					$('#withdraw_btn_submit').attr('onClick','submit_transfer()');
					$('#withdraw_btn_submit').html('{{Lang::get('public.Submit')}}');
				
			}
		})
		
		disabled.attr('disabled','disabled');
	}
function TransactionCount(){
	$.ajax({
		type: "GET",
		url: "{{route('TransactionCount')}}",
		data: {},
	}).done(function( json ) {
	obj = JSON.parse(json);
		if(obj==1){
			if(($("#bank").val() == 'WEIXIN')||($("#bank").val() == 'ZHIFUBAO')){
				if($("#bank").val() == 'WEIXIN'){
					$("#accNoLabel").html("{{Lang::get('public.WechatAccountNo')}}* :");
				}else if($("#bank").val() == 'ZHIFUBAO'){
					$("#accNoLabel").html("{{Lang::get('public.AliPayAccountNo')}}* :");
				}	
			}else{
				$("#accNoLabel").html("{{Lang::get('public.BankAccountNo')}}* :");
			}
			$('#bankcol').hide();
		}else if(obj!=1){
			if(($("#bank").val() == 'WEIXIN')||($("#bank").val() == 'ZHIFUBAO')){
				if($("#bank").val() == 'WEIXIN'){
					$("#accNoLabel").html("{{Lang::get('public.WechatAccountNo')}}* :");
				}else if($("#bank").val() == 'ZHIFUBAO'){
					$("#accNoLabel").html("{{Lang::get('public.AliPayAccountNo')}}* :");
				}	
				$("#bankcol").hide();
			}else{
				$("#accNoLabel").html("{{Lang::get('public.BankAccountNo')}}* :");
				$("#bankcol").show();
			}

		}	
	});
}
	
function update_binded_bank(id){
	TransactionCount();
	
	$.ajax({
		type: "POST",
		url: "{{route('loadbank')}}",
		data: {
			id:  id.value
		},
	}).done(function( result ) {
		obj = JSON.parse(result);

		$('#province1').val(obj['bankprovince']);
		$('#city1').val(obj['bankcity']);
		$('#district1').val(obj['bankdistrict']);
		$('#bankbranch').val(obj['bankholdingbranch']);
		$('#bankaccno').val(obj['bankaccno']);
	});
}
	
</script>
<div class="brwn adj04">
 @include('gsc/trans_top' )
</div>
<div class="btm">
<div class="btmInner">

<!--ACCOUNT MANAGAMENT MENU-->
      <div class="inlineAccMenu">
      <ul>
			@include('gsc/accountmenu' )
      </ul>
      </div>
<!--ACCOUNT MANAGAMENT MENU-->
<!--ACCOUNT TITLE-->
      <div class="title_bar">
      <span>{{Lang::get('public.Withdrawal')}}</span>
      </div>
<!--ACCOUNT TITLE-->
<!--ACCOUNT CONTENT-->
      <div class="acctContent">
		  <form id="withdraw_form"> 
		  
				<span class="wallet_title"><i class="fa fa-money"></i>{{Lang::get('public.WithdrawalBank')}}</span>
			  <div class="acctRow">
				  <label>{{Lang::get('public.Balance')}} ({{ Session::get('user_currency') }}) :</label><span class="acctText">{{App\Http\Controllers\User\WalletController::mainwallet()}}</span>
				  <div class="clr"></div>
			  </div>
			  <div class="acctRow">
				  <label>{{Lang::get('public.Amount')}} ({{ Session::get('user_currency') }}) * :</label>
				   <input type="text" class="form-control" name="amount">
				  <div class="clr"></div>
			  </div>
			  <div class="acctRow">
				  <label>己绑定银行卡信息:</label>
					<select class="form-control" id="bank" name="bank" style="width:202px;float:left;" onChange="update_binded_bank(this)">
						@foreach( $binded_banks as $binded_bank  )
							<option value="{{$banklists[$binded_bank->bankname]['code']}}">{{$binded_bank->bankname}}</option>
						@endforeach
					</select>
					 <div class="joinBtn adj05" style="float:left;margin-left:10px;">
						<a href="{{route('memberbank')}}" style="width:100px;">新增银行户口</a>
					  </div>
				  <div class="clr"></div>
			  </div>	
<div id="bankcol">			  
			  <div class="acctRow bnkInfoHolder">
				  <label>{{Lang::get('public.Province')}}* :</label>
						<input class="form-control" id="province1" name="province" style="width:195px;" value="{{$binded_banks[0]->bankprovince}}" disabled>
				  <div class="clr"></div>
			  </div>
			  <div class="acctRow bnkInfoHolder">
				  <label>{{Lang::get('public.City')}}* :</label>
					  <input class="form-control" id="city1" name="city" style="width:195px;" value="{{$binded_banks[0]->bankcity}}" disabled>
				  <div class="clr"></div>
			  </div>
			  <div class="acctRow bnkInfoHolder">
				  <label>{{Lang::get('public.District')}}* :</label>
					  <input class="form-control" id="district1" name="district" style="width:195px;" value="{{$binded_banks[0]->bankdistrict}}" disabled>
				  <div class="clr"></div>
			  </div>
			  <div class="acctRow bnkInfoHolder">
				  <label>{{Lang::get('public.BankBranch3')}}* :</label>
				   <input type="text" class="form-control" id="bankbranch" name="bankbranch" value="{{$binded_banks[0]->bankholdingbranch}}" disabled>
				  <div class="clr"></div>
			  </div>
</div>
			  <div class="acctRow">
				  <label>{{Lang::get('public.FullName')}} * :</label>
				  <span class="acctText">{{$fullname}}</span>
				  <div class="clr"></div>
			  </div>
			  <div class="acctRow">
				  <label id="accNoLabel">{{Lang::get('public.BankAccountNo')}} * :</label>
				  <input type="text" class="form-control" id="bankaccno" name="accountno" value="{{$binded_banks[0]->bankaccno}}" disabled>
				  <div class="clr"></div>
			  </div>
			  <i class="fa fa-exclamation-circle"></i>{{Lang::get('public.Notice2')}}。{{Lang::get('public.WithdrawDetailNote')}}
			  <div class="acctRow">
				  <span class="failed_message" style="color:red;font-size:16px;display:block;float:left;height:100%;"></span>
			  </div>
			  <br><br>
			  <div class="joinBtn adj05">
				<a id="withdraw_btn_submit" href="javascript:void(0)" onClick="submit_transfer()">{{Lang::get('public.Submit')}}</a>
			  </div>
			  <br>
		  </form>
      </div>
<!--ACCOUNT CONTENT-->
</div>
</div>
<script>
function withdrawalcardPOST(){
        $.ajax({
            type: "POST",
            url: "{{action('User\WithdrawcardController@withdrawcardProcess')}}?" + $("form").serialize(),
            data: {
                _token: "{{ csrf_token() }}"
            },
        }).done(function( json ) {
			 obj = JSON.parse(json);
			 var str = '';
			 $.each(obj, function(i, item) {
				if( '{{Lang::get('COMMON.SUCESSFUL')}}' == item ){
					alert('{{Lang::get('COMMON.SUCESSFUL')}}');
					window.location.href = "{{route('transaction')}}";
				}else{
					str += item + '<br>';
				}
				$('.failed_message').html(str);
			})
	
	});
}
</script>
	<script src="{{url()}}/gsc/resources/js/distpicker.data.js"></script>
	  <script src="{{url()}}/gsc/resources/js/distpicker.js"></script>
	  <script src="{{url()}}/gsc/resources/js/main.js"></script>
@stop