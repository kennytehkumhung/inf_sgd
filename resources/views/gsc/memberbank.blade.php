@extends('gsc/master')

@section('title', Lang::get('public.Withdrawal'))

@section('content')
<link href="{{url()}}/gsc/resources/css/acctMngmt.css" rel="stylesheet" type="text/css" />
<script>
	$(document).ready(function() { 
		getBalance(true);
		bankposition();
	});
	
function update_bank(){

	 
	$.ajax({
		type: "POST",
		url: "{{route('updatebank')}}?"+$('#bank_form').serialize(),
	
	}).done(function( result ) {
		if(result == 1){
			
			alert('{{Lang::get('COMMON.SUCESSFUL')}}');
			window.location="{{route('withdraw')}}";
		}
		else if(result == 2){
			
			alert('只允许绑定不超过3个提款户口！');

		}else{
				alert('This bank account is duplicated. Please insert another bank account.');
                                location.reload(); 
		}

					
	});
} 

function bankposition(){
	if(($("#bnkid").val() == 'WEIXIN')||($("#bnkid").val() == 'ZHIFUBAO')){
		if($("#bnkid").val() == 'WEIXIN'){
			$("#fullname").html("{{Lang::get('public.WechatPayName')}}* :");
			$("#accNoLabel").html("{{Lang::get('public.WechatAccountNo')}}* :");
		}else if($("#bnkid").val() == 'ZHIFUBAO'){
			$("#fullname").html("{{Lang::get('public.AliPayName')}}* :");
			$("#accNoLabel").html("{{Lang::get('public.AliPayAccountNo')}}* :");
		}	
		$("#distpicker").hide();
		$("#BankBranch3").hide();
	}else{
		$("#fullname").html("{{Lang::get('public.FullName')}}* :");
		$("#accNoLabel").html("{{Lang::get('public.BankAccountNo')}}* :");
		$("#distpicker").show();
		$("#BankBranch3").show();
	}
}

function update_binded_bank(id){
	bankposition();
		
	$.ajax({
		type: "POST",
		url: "{{route('loadbank')}}",
		data: {
			id:  id.value
		},
	}).done(function( result ) {

		if(result != 'null' && result != '')
		{
			obj = JSON.parse(result);

			$('#province1').val(obj['bankprovince']).trigger('change');
			$('#province1').prop('disabled',true);
			$('#city1').val(obj['bankcity']).trigger('change');
			$('#city1').prop('disabled',true);
			$('#district1').val(obj['bankdistrict']).trigger('change');
			$('#district1').prop('disabled',true);
			$('#bankbranch').val(obj['bankholdingbranch']);
			$('#bankbranch').prop('disabled',true);
			$('#bankaccno').val(obj['bankaccno']);
			$('#bankaccno').prop('disabled',true);
			$('#fullname').val(obj['fullname']);
			$('#fullname').prop('disabled',true);
		}else{
			
			$('#province1').prop("disabled", false);
			$('#city1').prop("disabled", false);
			$('#district1').prop("disabled", false);
			$('#bankbranch').val('');
			$('#bankbranch').prop("disabled", false);
			$('#bankaccno').val('');
			$('#bankaccno').prop("disabled", false);
			
		}
	});
}
</script>
<div class="brwn adj04">
 @include('gsc/trans_top' )
</div>

<div class="btm">
<div class="btmInner">

<!--ACCOUNT MANAGAMENT MENU-->
      <div class="inlineAccMenu">
		<ul>
			  <li><a href="{{route('update-profile')}}">{{Lang::get('public.AccountProfile')}}</a></li>
			  <li><a href="{{route('update-password')}}">{{Lang::get('public.AccountPassword')}}</a></li>
			  <li><a href="{{route('memberbank')}}">{{Lang::get('COMMON.CTABBANKSETTING')}}</a></li>
		</ul>
      </div>
<!--ACCOUNT MANAGAMENT MENU-->
<!--ACCOUNT TITLE-->
      <div class="title_bar">
      <span>{{Lang::get('COMMON.CTABBANKSETTING')}}</span>
      </div>
<!--ACCOUNT TITLE-->
<!--ACCOUNT CONTENT-->

<form id="bank_form">
      <div class="acctContent">
      <span class="wallet_title"><i class="fa fa-pencil"></i>{{Lang::get('COMMON.CTABBANKSETTING')}}<font color="red" style="margin-left:170px;">（新会员请先设定提款户口,只允许绑定不超过3个提款户口！）</font></span>
	  		  <div class="acctRow">
				  <label>{{Lang::get('public.BankName')}}* :</label>
					<select class="form-control" id="bnkid" name="bnkid" style="width:202px;" onChange="update_binded_bank(this)">
						@foreach( $banklists as $bank => $detail  )
							<option value="{{$detail['code']}}">{{$bank}}</option>
						@endforeach
					</select>
				  <div class="clr"></div>
			  </div>
		<div data-toggle="distpicker" id="distpicker">
			  <div class="acctRow bnkInfoHolder">
				  <label>{{Lang::get('public.Province')}}* :</label>
						<select class="form-control" id="province1" name="province" style="width:202px;"></select>
				  <div class="clr"></div>
			  </div>
			  <div class="acctRow bnkInfoHolder">
				  <label>{{Lang::get('public.City')}}* :</label>
					  <select class="form-control" id="city1" name="city" style="width:202px;"></select>
				  <div class="clr"></div>
			  </div>
			  <div class="acctRow bnkInfoHolder">
				  <label>{{Lang::get('public.District')}}* :</label>
					  <select class="form-control" id="district1" name="district" style="width:202px;"></select>
				  <div class="clr"></div>
			  </div>
		</div>

			  <div class="acctRow bnkInfoHolder" id="BankBranch3">
				  <label>{{Lang::get('public.BankBranch3')}}* :</label>
				   <input type="text" class="form-control" id="bankbranch" name="bankbranch">
				  <div class="clr"></div>
			  </div>

			  <div class="acctRow">
				  <label id='fullname'>{{Lang::get('public.FullName')}} * :</label>
				  @if(!empty($fullname))
						<span class="acctText">{{$fullname}}</span>
				  @else
						<input type="text" id="fullname" name="fullname">
				  @endif
				  <div class="clr"></div>
			  </div>
			  <div class="acctRow">
				  <label id="accNoLabel">{{Lang::get('public.BankAccountNo')}} * :</label>
				  <input type="text" id="bankaccno" class="form-control" name="bnkaccno">
				  <div class="clr"></div>
			  </div>
			  <div class="acctRow pgInfoHolder" style="display: none;">
				  <label id="accNoLabel">{{Lang::get('public.UploadQRProof')}} * :</label>
				  <input type="file" class="form-control" name="uploadattachment">
				  <div class="clr"></div>
			  </div>
    
      <div class="joinBtn adj05">
      <a href="javascript:void(0)" onClick="update_bank()">{{Lang::get('public.Submit')}}</a>
      </div>
      </div>
</form>
<!--ACCOUNT CONTENT-->
</div>
</div>
	<script src="{{url()}}/gsc/resources/js/distpicker.data.js"></script>
	<script src="{{url()}}/gsc/resources/js/distpicker.js"></script>
	<script src="{{url()}}/gsc/resources/js/main.js"></script>
@stop