<html>
	<head>
		<style>
table {
    border-collapse: collapse;
    width: 100%;
}

th, td {
    text-align: left;
    padding: 8px;
}

tr:nth-child(even){background-color: #f2f2f2}

th {
    background-color: #4CAF50;
    color: white;
}
</style>
	</head>
	<body>
		<table class="table table-hover table-bordered">
			<tr>
				<th>NO</th>
				<th>{{ Lang::get('COMMON.USERNAME') }}</th>
			</tr>
			<?php $no=1;?>
			@foreach($onlinememberlist as $key => $val)
			<tr>
				<td><?php echo $no;?></td>
				<td>{{$val->username}}</td>
			</tr>
			<?php $no++;?>
			@endforeach
		</table>
	</body>
</html>