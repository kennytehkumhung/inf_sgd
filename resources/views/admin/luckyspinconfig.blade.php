<!DOCTYPE html>
<html lang="en">
<head>
    <title id='Description'>
    </title>
	<head>
	<link rel="stylesheet" href="{{url()}}/admin/jqwidgets/styles/jqx.base.css" type="text/css" />
    <link rel="stylesheet" href="{{url()}}/admin/jqwidgets/styles/jqx.bootstrap.css" type="text/css" />
    <script type="text/javascript" src="{{url()}}/admin/jqwidgets/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="{{url()}}/admin/jqwidgets/jqx-all.js"></script>
	 <script type="text/javascript">
        $(document).ready(function () {

			$("#form_submit_btn").jqxButton({ width: '150', template: "success"});
			$("#form_submit_btn").on('click', function () {
                   $("#events").find('span').remove();
                   $("#events").append('<span>Submit Button Clicked</span>');
             });

			$("#to", $("#freeTokenForm")).change(function () {
			    var memObj = $("#member_username", $("#freeTokenForm"));

			    if ($(this).val() == "only") {
                    memObj.prop("readonly", false);
                    memObj.focus();
				} else {
                    memObj.val("").prop("readonly", true);
				}
			}).change();
        });

        function submitPrizeSettingsForm(btn) {
            if (confirm("Confirm save?")) {
                var btnObj = $(btn);
                var form = $("#prizeSettingsForm");
                var inputs = {};

                btnObj.prop("readonly", true).prop("disabled", true).val("Loading...");

                $(".config_input").each(function () {
                    var input = $(this);
                    var id = input.attr("id").replace("input_", "");

                    inputs[id] = {
                        "value": input.val(),
                        "win_probability1": $("#config_input1_" + id).val(),
                        "win_probability_free_token1": $("#config_input1_ft_" + id).val(),
                        "win_probability2": $("#config_input2_" + id).val(),
                        "win_probability_free_token2": $("#config_input2_ft_" + id).val(),
                        "win_probability3": $("#config_input3_" + id).val(),
                        "win_probability_free_token3": $("#config_input3_ft_" + id).val(),
                        "win_probability4": $("#config_input4_" + id).val(),
                        "win_probability_free_token4": $("#config_input4_ft_" + id).val()
                    };
                });

                $.ajax({
                    type: form.attr("method"),
                    url: form.attr("action"),
                    dataType: "json",
                    data: {
                        currency: "{{ $currency }}",
                        inputs: inputs
                    },
                    success: function (result) {
                        alert(result);
                    },
                    complete: function (jqXHR, textStatus) {
                        $("#events").find('span').remove();
                        btnObj.prop("readonly", false).prop("disabled", false).val("Submit");
                    }
                });
			}
		}

		function submitFreeTokenForm(btn) {
            if (confirm("Confirm submit?")) {
                var btnObj = $(btn);
                var form = $("#freeTokenForm");

                btnObj.prop("readonly", true).prop("disabled", true).val("Loading...");

                $.ajax({
                    type: form.attr("method"),
                    url: form.attr("action"),
                    dataType: "json",
                    data: {
                        currency: "{{ $currency }}",
                        to: $("#to", form).val(),
                        sys_ref: $("#sys_ref", form).val(),
                        acccode: $("#member_username", form).val(),
                        amount: $("#token_amount", form).val(),
						remark: $("#free_token_remark", form).val()
                    },
                    success: function (result) {
                        alert(result);
                    },
                    complete: function (jqXHR, textStatus) {
                        btnObj.prop("readonly", false).prop("disabled", false).val("Submit");
                    }
                });
			}
		}

		function submitTokenSettingsForm(btn) {
			if (confirm("Confirm submit?")) {
			    var btnObj = $(btn);
			    var form = $("#tokenSettingsForm");

                btnObj.prop("readonly", true).prop("disabled", true).val("Loading...");

                $.ajax({
                    type: form.attr("method"),
                    url: form.attr("action"),
                    dataType: "json",
                    data: {
                        currency: "{{ $currency }}",
                        daily_login_free_token_amount: $("#daily_login_free_token_amount", form).val(),
                        weekly_login_free_token_amount: $("#weekly_login_free_token_amount", form).val(),
                        monthly_login_free_token_amount: $("#monthly_login_free_token_amount", form).val(),
                        free_token_validity_days: $("#free_token_validity_days", form).val(),
                        normal_token_validity_days: $("#normal_token_validity_days", form).val()
                    },
                    success: function (result) {
                        alert(result);
                    },
                    complete: function (jqXHR, textStatus) {
                        btnObj.prop("readonly", false).prop("disabled", false).val("Submit");
                    }
                });
			}
        }

        function submitWinnerListForm(btn) {
            if (confirm("Confirm submit?")) {
                var btnObj = $(btn);
                var form = $("#winnerListForm");
                var winnerList = {};

                btnObj.prop("readonly", true).prop("disabled", true).val("Loading...");

                $(".winner-list-name").each(function () {
                    var nameObj = $(this);
                    var index = nameObj.data("index");

                    winnerList[index] = {"n": $("#winner_list_n_" + index).val(), "p": $("#winner_list_p_" + index).val()};
				});

                $.ajax({
                    type: form.attr("method"),
                    url: form.attr("action"),
                    dataType: "json",
                    data: {
                        currency: "{{ $currency }}",
                        winner_list: JSON.stringify(winnerList)
                    },
                    success: function (result) {
                        alert(result);
                    },
                    complete: function (jqXHR, textStatus) {
                        btnObj.prop("readonly", false).prop("disabled", false).val("Submit");
                    }
                });
            }
        }
    </script>
	</head>
	<body class='default'>
<h2 style="margin: 18px 0 5px 0;">Free Token</h2>
<form id="freeTokenForm" method="post" action="{{ action('Admin\LuckySpinController@doEditFreeToken') }}">
<table>
	<tr>
		<td style="width: 120px;">Currency Code</td>
		<td>
			<select id="crccode">
				<option value="{{ $currency }}">{{ $currency }}</option>
			</select>
		</td>
	</tr>
	<tr>
		<td>Free Token For</td>
		<td>
			<select id="to">
				<option value="only">Only For (Username)</option>
				<option value="all">Everyone</option>
			</select>
		</td>
	</tr>
	<tr>
		<td>Token Type</td>
		<td>
			<select id="sys_ref">
				@foreach(\App\Models\FortuneWheelLedger::getSysRefOptions() as $key => $val)
					@if (str_contains($key, ['FT', 'NT']))
						<option value="{{ $key }}">{{ $val }}</option>
					@endif
				@endforeach
			</select>
		</td>
	</tr>
	<tr>
		<td>Member Username</td>
		<td><input type="text" id="member_username"></td>
	</tr>
	<tr>
		<td>Token Amount</td>
		<td><input type="text" id="token_amount"></td>
	</tr>
	<tr>
		<td>Remark</td>
		<td><input type="text" id="free_token_remark"></td>
	</tr>
</table>
<input class="jqx-rc-all jqx-button jqx-widget jqx-fill-state-hover jqx-success" style="margin-top: 10px;cursor: pointer;width: 150px;" value="Submit" type="button" onclick="submitFreeTokenForm(this);" />
</form>
<br>
<hr>
<h2 style="margin: 32px 0 5px 0;">Token Settings</h2>
<form id="tokenSettingsForm" method="post" action="{{ action('Admin\LuckySpinController@doEditTokenSettings') }}">
	<table>
		<tr>
			<td style="width: 300px;">Daily Login Free Token Amount</td>
			<td><input type="text" id="daily_login_free_token_amount" value="{{ $dailyLoginFreeTokenAmount['d'] }}"></td>
		</tr>
		<tr>
			<td>7 Days Login Free Token Amount</td>
			<td><input type="text" id="weekly_login_free_token_amount" value="{{ $dailyLoginFreeTokenAmount['w'] }}"></td>
		</tr>
		<tr>
			<td>30 Days Login Free Token Amount</td>
			<td><input type="text" id="monthly_login_free_token_amount" value="{{ $dailyLoginFreeTokenAmount['m'] }}"></td>
		</tr>
		<tr>
			<td>Free Token Forfeited After # Days</td>
			<td><input type="text" id="free_token_validity_days" value="{{ $tokenValidityDays['f'] }}"></td>
		</tr>
		<tr>
			<td>Normal Token Forfeited After # Days</td>
			<td><input type="text" id="normal_token_validity_days" value="{{ $tokenValidityDays['n'] }}"></td>
		</tr>
	</table>
	<input class="jqx-rc-all jqx-button jqx-widget jqx-fill-state-hover jqx-success" style="margin-top: 10px;cursor: pointer;width: 150px;" value="Submit" type="button" onclick="submitTokenSettingsForm(this);" />
</form>
<br>
<hr>
<h2 style="margin: 32px 0 5px 0;">Winner List</h2>
<form id="winnerListForm" method="post" action="{{ action('Admin\LuckySpinController@doEditWinnerList') }}" >
	<table>
		<thead style="font-weight: bold;">
		<tr>
			<td>&nbsp;</td>
			<td style="width: 120px;">Name</td>
			<td style="width: 100px;">Prize</td>
		</tr>
		</thead>
		<tbody>
		@for ($i = 0; $i < 5; $i++)
			<tr>
				<td>{{ $i + 1 }}.</td>
				<td><input type="text" id="winner_list_n_{{ $i }}" data-index="{{ $i }}" class="winner-list-name" value="{{ isset($winnerList[$i]['n']) ? $winnerList[$i]['n'] : '' }}"></td>
				<td><input type="text" id="winner_list_p_{{ $i }}" value="{{ isset($winnerList[$i]['n']) ? $winnerList[$i]['p'] : '' }}"></td>
			</tr>
		@endfor
		</tbody>
	</table>
	<input class="jqx-rc-all jqx-button jqx-widget jqx-fill-state-hover jqx-success" style="margin-top: 10px;cursor: pointer;width: 150px;" value="Submit" type="button" onclick="submitWinnerListForm(this);" />
</form>
<br>
<hr>
<h2 style="margin: 32px 0 5px 0;">Prize Settings</h2>
<form id="prizeSettingsForm" method="post" action="{{ action('Admin\LuckySpinController@doEditConfig') }}" >
<div id="form_wapper" >
<table>
	<thead style="font-weight: bold;">
	<tr>
		<td colspan="3"></td>
		<td colspan="2" style="text-align: center; background-color: #ccc;">Set 1</td>
		<td colspan="2" style="text-align: center; background-color: #ccc;">Set 2</td>
		<td colspan="2" style="text-align: center; background-color: #ccc;">Set 3</td>
		<td colspan="2" style="text-align: center; background-color: #ccc;">Set 4</td>
		<td></td>
	</tr>
	<tr>
		<td style="width: 120px;">Name</td>
		<td style="width: 60px;">Type</td>
		<td style="width: 100px;">Value</td>
		<td style="background-color: #ccc; width: 105px;">Normal Win Rate<br>(0-100)</td>
		<td style="background-color: #ccc; width: 105px;">Free Win Rate<br>(0-100)</td>
		<td style="background-color: #ccc; width: 105px;">Normal Win Rate<br>(0-100)</td>
		<td style="background-color: #ccc; width: 105px;">Free Win Rate<br>(0-100)</td>
		<td style="background-color: #ccc; width: 105px;">Normal Win Rate<br>(0-100)</td>
		<td style="background-color: #ccc; width: 105px;">Free Win Rate<br>(0-100)</td>
		<td style="background-color: #ccc; width: 105px;">Normal Win Rate<br>(0-100)</td>
		<td style="background-color: #ccc; width: 105px;">Free Win Rate<br>(0-100)</td>
		<td style="width: 120px;"></td>
	</tr>
	</thead>
	<tbody>
<?php $ntype = ''; ?>
@foreach( $configObj as $r )

	@if ($ntype != $r->position_type)
        <?php $ntype = $r->position_type; ?>
		<tr>
			<td colspan="8">&nbsp;</td>
		</tr>
	@endif

	<tr>
		<td class="formLabelLeft">{{ $r->prize_name }}</td>
		<td>
			{{ ($r->position_type == 2 ? 'Prize' : ($r->position_type == 3 ? 'Bonus' : '')) }}
		</td>
		<td>
			<input id="input_{{ $r->id }}" value="{{ $r->prize_value }}" class="config_input" />
		</td>
		<td>
			<input id="config_input1_{{ $r->id }}" value="{{ $r->win_probability1 }}" {{ $r->position_type == 0 ? 'disabled' : '' }} />
		</td>
		<td>
			<input id="config_input1_ft_{{ $r->id }}" value="{{ $r->win_probability_free_token1 }}" {{ $r->position_type == 0 ? 'disabled' : '' }} />
		</td>
		<td>
			<input id="config_input2_{{ $r->id }}" value="{{ $r->win_probability2 }}" {{ $r->position_type == 0 ? 'disabled' : '' }} />
		</td>
		<td>
			<input id="config_input2_ft_{{ $r->id }}" value="{{ $r->win_probability_free_token2 }}" {{ $r->position_type == 0 ? 'disabled' : '' }} />
		</td>
		<td>
			<input id="config_input3_{{ $r->id }}" value="{{ $r->win_probability3 }}" {{ $r->position_type == 0 ? 'disabled' : '' }} />
		</td>
		<td>
			<input id="config_input3_ft_{{ $r->id }}" value="{{ $r->win_probability_free_token3 }}" {{ $r->position_type == 0 ? 'disabled' : '' }} />
		</td>
		<td>
			<input id="config_input4_{{ $r->id }}" value="{{ $r->win_probability4 }}" {{ $r->position_type == 0 ? 'disabled' : '' }} />
		</td>
		<td>
			<input id="config_input4_ft_{{ $r->id }}" value="{{ $r->win_probability_free_token4 }}" {{ $r->position_type == 0 ? 'disabled' : '' }} />
		</td>
		<td>
			<div class="input_{{ $r->id }}_error error_msg" style="color:red;"></div>
		</td>
	</tr>
@endforeach
	</tbody>
</table>
		<input type="hidden" name="_token" value="{{csrf_token()}}">
		</div>
		<h4 style="color:red;">
		
		{{ Session::get('message') }}
		@foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
        @endforeach
		</h4>
		<div class="clear">
            <input style='cursor: pointer;' id="form_submit_btn" value="Submit" type="button" onclick="submitPrizeSettingsForm(this);" />
        </div>
		
</form>

</body>
</html>