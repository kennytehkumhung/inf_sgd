<!DOCTYPE html>
<html lang="en">
<head>
    <title id='Description'></title>
    <link rel="stylesheet" href="{{url()}}/admin/jqwidgets/styles/jqx.base.css" type="text/css" />
    <link rel="stylesheet" href="{{url()}}/admin/jqwidgets/styles/jqx.bootstrap.css" type="text/css" />
    <script type="text/javascript" src="{{url()}}/admin/jqwidgets/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="{{url()}}/admin/jqwidgets/jqx-all.js"></script>
    <style>
        .fl{
                float: left;
        }
        .label-margin{
                padding-top:5px;
                margin-left: 10px;
                margin-right: 5px;
        }
        .clear{
                clear: both;
        }
        .formLabelLeft{
                width: 150px;
                padding-right: 10px;
                vertical-align: top;
                font-weight: bold;
        }
    </style>
</head>
<body class='default'>  
<form>
	 <table width="100%">
		<tbody>
			<tr>
				<td width="50%" valign="top">
                    <div id='left_box'>
						<div>Deposit</div>
						<div style="padding:10px 0 10px 10px">
							<table class="formTable">
								<tr>
									<td class="formLabelLeft">{{Lang::get('COMMON.ID')}}</td>
									<td>{{$clgObj->getTransactionId()}}</td>
								</tr>
								<tr>
									<td class="formLabelLeft">{{Lang::get('COMMON.ACCOUNT')}}</td>
									<td>{{$accObj->nickname}}</td>
								</tr>
								<tr>
									<td class="formLabelLeft">{{Lang::get('COMMON.NAME')}}</td>
									<td>{{$clgObj->accname}}</td>
								</tr>
								<tr>
									<td class="formLabelLeft">{{Lang::get('COMMON.IPADDRESS')}}</td>
									<td>{{$ipurl}} [{{$location['country']}}] - {{$location['city']}}</td>
								</tr>
								<tr>
									<td class="formLabelLeft">{{Lang::get('COMMON.TIME')}}</td>
									<td>{{$clgObj->created}}</td>
								</tr>
								<tr>
									<td class="formLabelLeft">{{Lang::get('COMMON.AMOUNT')}}</td>
									<td><span style="font-weight:bold;color:navy;">{{$amount}}</span></td>
								</tr>
								<tr>
									<td class="formLabelLeft">{{Lang::get('COMMON.PAYMENTMETHOD')}}</td>
									<td><span style="font-weight:bold;">{{Lang::get('COMMON.ADJUSTMENT')}}</span></td>
								</tr>
								<tr>
									<td class="formLabelLeft">{{Lang::get('COMMON.TYPE')}}</td>
									<td><span style="font-weight:bold;">{{$type}}</span></td>
								</tr>
								<tr>
									<td class="formLabelLeft">{{Lang::get('COMMON.REMARK')}}</td>
									<td><span style="font-weight:bold;">{{$remark}}</span></td>
								</tr>	
								<tr>
									<td class="formLabelLeft">{{Lang::get('COMMON.STATUS')}}</td>
									<td><span style="font-weight:bold;">{{$clgObj->getStatusText()}}</span></td>
								</tr>
				
							</table>
						</div>
					</div>	
				</td>
				<td width="50%" valign="top">
					<div id='right_box'>
						<div>Other Data</div>
						<div style="padding:10px 0 10px 10px">
							<table class="formTable">
								<tr>
									<th>{{Lang::get('COMMON.FEE')}}</th>
									<td><input type="text" name="editfee" value="@if(isset($fees)){{$fees}}@endif" class="easyui-validatebox" @if(isset($disablefee)) disabled @endif >&nbsp;
									</td>
								</tr>
								<tr>
									<th>{{Lang::get('COMMON.COMMENT')}}</th>
									<td><textarea name="editcomment" class="easyui-validatebox" style="resize:none;" rows="4">@if(isset($comments)){{$comments}}@endif</textarea></td>
								</tr>
								<tr>
									<th>{{Lang::get('COMMON.REJECTREASON')}}</th>
									<td>
										<select name="reason">
											@foreach($reasons as $key => $value)
												<option value="{{$value}}" @if($clgObj->rejreason == $value ) selected @endif>{{$value}}</option>
											@endforeach
										</select>
									</td>
								</tr>
								<tr>
									<th>{{Lang::get('COMMON.REMARK')}}</th>
									<td><textarea name="editremark" class="easyui-validatebox" style="resize:none;" rows="4">{{$clgObj->remarks}}</textarea></td>
								</tr>
			@if( !empty($modifiedby) )
								<tr>
									<th>{{Lang::get('COMMON.CONFIRM2')}} {{Lang::get('COMMON.USER')}}</th>
									<td>{{$modifiedby}}</td>
								</tr>
								<tr>
									<th>{{Lang::get('COMMON.CONFIRM2')}} {{Lang::get('COMMON.DATE')}}</th>
									<td>{{$modified}}</td>
								</tr>
			@endif
								<tr>
							@if (Config::get('setting.opcode') == 'GSC')
								@if($curStatus =='pending')
									<td></td>
									<td>
										<input id="btn_approve" style='cursor: pointer;' onClick="submit_form('approve')" type="button" value="Approve" />
										<input id="btn_reject" style='cursor: pointer;' onClick="submit_form('reject')" type="button" value="Reject" />
									</td>
								@endif
							@else
								<td></td>
									<td>
										<input id="btn_approve" style='cursor: pointer;' onClick="submit_form('approve')" type="button" value="Approve" />
										<input id="btn_reject" style='cursor: pointer;' onClick="submit_form('reject')" type="button" value="Reject" />
									</td>
							@endif
								</tr>
							</table>
				
						</div>
					</div>				
				</td>
			</tr>

		</tbody>
	</table>
</form>
<script type="text/javascript">
    $(document).ready(function () {
        // Create jqxExpander
        $("#left_box").jqxExpander({ width: '100%', theme:'bootstrap', toggleMode: 'dblclick'});
        $("#right_box").jqxExpander({ width: '100%', theme:'bootstrap', toggleMode: 'dblclick'});
		$("#btn_approve").jqxButton({ width: '80', template: "success"});
		$("#btn_reject").jqxButton({ width: '80', template: "danger"});
			
    });
	
	function submit_form(type)
	{

			  	$.ajax({
					type: "POST",
					url: "adjustment-edit",
					data: $( "form" ).serialize()+'&_token={{ csrf_token() }}&actionref='+type+'&id={{$sid}}',
				}).done(function( json ) {
						 obj = JSON.parse(json);
						 $('.error_msg').html('');
						 $.each(obj, function(i, item) {
							if( '{{Lang::get('COMMON.SUCESSFUL')}}' == item ){
								alert('{{Lang::get('COMMON.SUCESSFUL')}}');
								window.parent.closeTab();
							}else{
								$('.'+i+'_error').html(item);
							}
						})
				});

	}
</script>
</body>
</html>
