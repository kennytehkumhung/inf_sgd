<!DOCTYPE html>
<html lang="en">
<head>
    <title id='Description'>
    </title>
	<head>
	<link rel="stylesheet" href="{{url()}}/admin/jqwidgets/styles/jqx.base.css" type="text/css" />
    <link rel="stylesheet" href="{{url()}}/admin/jqwidgets/styles/jqx.bootstrap.css" type="text/css" />
    <script type="text/javascript" src="{{url()}}/admin/jqwidgets/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="{{url()}}/admin/jqwidgets/jqx-all.js"></script>
    <script type="text/javascript" src="{{url()}}/admin/js/general.js"></script>
    <script type="text/javascript" src="{{url()}}/admin/js/checkallbox.inc.js"></script>
	<script type="text/javascript">
	
	 $(document).ready(function () {
		$("#form_submit_btn").jqxButton({ width: '150', template: "success"});
	 });
	function doShowHide(id) {
		var el = document.getElementById(id);
		if(el.style.display == 'none') {
			el.style.display = 'block';
		} else {
			el.style.display = 'none';
		}
	}
	
	function submit_form(){
			
				var form_data = new FormData();                  
				//var data = $('form').serializeArray();
				var data = $('input:checkbox').map(function() {
				   return { name: this.name, value: this.checked ? this.value : "false" };
				 });
				 
				var obj = {};
				for (var i = 0, l = data.length; i < l; i++) {
					form_data.append(data[i].name, data[i].value);
				}
				@if(isset($roleObj))
				form_data.append('sid', {{$roleObj->id}});
				@else
				form_data.append('name',  $('#name').val() );	
				@endif
				
				form_data.append('desc',  $('#desc').val() );	
				form_data.append('status', $('input[name=status]:checked').val());	
				
				//console.log($('form').serializeArray());
				
				form_data.append('_token', '{{csrf_token()}}');

				$.ajax({
					type: "POST",
					url: "adminrole-do-add-edit?",
							dataType: 'script',
							cache: false,
							contentType: false,
							processData: false,
							data: form_data,         
							type: 'post',
							complete: function(json){

								 obj = JSON.parse(json.responseText);
								 $('.error_msg').html('');
								 $.each(obj, function(i, item) {
									if( '{{Lang::get('COMMON.SUCESSFUL')}}' == item ){
										alert('{{Lang::get('COMMON.SUCESSFUL')}}');
									}else{
										$('.'+i+'_error').html(item);
									}
								})
							
							},
								
				});

	}
    </script>
	<style>
	
ul {
    list-style: outside none none;
    margin: 0;
    padding: 0;
}
	</style>
	</head>
	<body class='default'>

<form id="cboForm" method="post" action="index.php" target="frameSubmit">
<div class="easyui-panel" title="Edit Role" style="width:auto">
<div style="padding:10px 0 10px 10px">
<table>
<tr>
<td class="formLabelLeft">{{ Lang::get('COMMON.NAME') }}</td>
@if(isset($roleObj))
<td>{{$roleObj->name}}</td>
@else
<td>
	<input type="text" id="name" name="name" />
</td>
@endif
</tr>
<tr>
<td class="formLabelLeft">Description</td>
<td><textarea name="desc" class="easyui-validatebox" rows="5" cols="30" id="desc">@if(isset($roleObj)){{$roleObj->desc}}@endif</textarea></td>
</tr>
<tr>
<td class="formLabelLeft">{{ Lang::get('COMMON.PERMISSION') }}</td>
<td>
	<ul id="permission">
		@foreach($modules as $catid => $category)
			<li>{{ $category['name'] }}
				@foreach($category['module'] as $mdlid => $module)
					<ul style="padding:3px 0">
						<li>
							<input type="checkbox" name="amodule[{{$catid}}][{{$mdlid}}]" value="1" id="module_{{$catid}}_{{$mdlid}}" onclick="checkAll('module_{{$catid}}_{{$mdlid}}', 'input', this);" @if(isset($permissions[$catid][$mdlid])) checked @endif />
							<span style="padding:3px;">
								<a href="javascript:doShowHide('list_{{$catid}}_{{$mdlid}}')">{{Lang::get('COMMON.'.$module['name'])}}</a>
							</span>
							<ul style="padding:3px 0 3px 15px;display:none;" id="list_{{$catid}}_{{$mdlid}}">
								@foreach($module['task'] as $taskid => $task)
									<li><input type="checkbox" name="apermission[{{$catid}}][{{$mdlid}}][{{$taskid}}]" value="1" class="module_{{$catid}}_{{$mdlid}}" onclick="doCheckParent(this, 'module_{{$catid}}_{{$mdlid}}')" @if(isset($permissions[$catid][$mdlid][$taskid])) checked @endif /><span style="padding:3px;">{{Lang::get('COMMON.'.$task['name'])}}</span></li>
								@endforeach
							</ul>
						</li>
					</ul>
				@endforeach
			</li>
		@endforeach
	</ul>
	
</td>
</tr>
<tr>
<td class="formLabelLeft">Status</td>
<td><label><input type="radio" name="status" value="1" @if(isset($roleObj) && $roleObj->status == 1)checked="checked"@endif />{{ Lang::get('COMMON.ACTIVE') }}</label>&nbsp;
<label><input type="radio" name="status" value="0" @if(isset($roleObj) && $roleObj->status == 0)checked="checked"@endif />{{ Lang::get('COMMON.SUSPENDED') }}</label>&nbsp;</td>
</tr>
</table>
</div>
</div>
<input type="hidden" name="sid" value="2"><input type="hidden" name="mdl" value="adminrole"><input type="hidden" name="action" value="doadd">


<div class="clear">
    <input style='margin-top: 20px;cursor: pointer;' value="Submit" id='form_submit_btn' onClick="submit_form()"/>
 </div>
		
</form>
</body>
</html>