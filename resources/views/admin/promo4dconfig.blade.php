<!DOCTYPE html>
<html lang="en">
<head>
    <title id='Description'>
    </title>
	<head>
	<link rel="stylesheet" href="{{url()}}/admin/jqwidgets/styles/jqx.base.css" type="text/css" />
    <link rel="stylesheet" href="{{url()}}/admin/jqwidgets/styles/jqx.bootstrap.css" type="text/css" />
	<style type="text/css">
		.config_input { width: 100%; }
	</style>
    <script type="text/javascript" src="{{url()}}/admin/jqwidgets/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="{{url()}}/admin/jqwidgets/jqx-all.js"></script>
	 <script type="text/javascript">
        $(document).ready(function () {
			$("#form_submit_btn").jqxButton({ width: '150', template: "success"});
			$("#form_submit_btn").on('click', function () {
                   $("#events").find('span').remove();
                   $("#events").append('<span>Submit Button Clicked</span>');
             });
        });

        function submitWinnerListForm(btn) {
            if (confirm("Confirm save?")) {
                var btnObj = $(btn);
                var form = $("#winnerListForm");
                var p200Inputs = "";
                var p60Inputs = "";

                btnObj.prop("readonly", true).prop("disabled", true);

                $(".input_P200").each(function () {
                    var input = $.trim($(this).val());

                    if (input.length > 0) {
                        p200Inputs += input + ",";
					}
                });

                $(".input_P60").each(function () {
                    var input = $.trim($(this).val());

                    if (input.length > 0) {
                        p60Inputs += input + ",";
					}
                });

                $.ajax({
                    type: form.attr("method"),
                    url: form.attr("action"),
                    dataType: "json",
                    data: {
                        'date': $("#input_date").val(),
                        'P2500': $("#input_P2500").val(),
                        'P1000': $("#input_P1000").val(),
                        'P500': $("#input_P500").val(),
                        'P200': p200Inputs,
                        'P60': p60Inputs
					},
                    success: function (result) {
                        alert(result);
                    },
                    complete: function (jqXHR, textStatus) {
                        $("#events").find('span').remove();
                        btnObj.prop("readonly", false).prop("disabled", false);
                    }
                });
			}
		}
    </script>
	</head>
	<body class='default'>
<h2 style="margin: 32px 0 5px 0;">Winner List</h2>
<form id="winnerListForm" method="post" action="{{ action('Admin\Promo4dController@doEditConfig') }}" >
<div id="form_wapper" >
<table>
	<tbody>
	<tr>
		<td style="width: 120px;">
			Date
		</td>
		<td style="width: 360px;">
			<input id="input_date" value="{{ $date }}" class="config_input" placeholder="YYYY-MM-DD" />
		</td>
		<td style="width: 120px;">
			<div class="input_date_error error_msg" style="color:red;"></div>
		</td>
	</tr>
	</tbody>
</table>
<br>
<table>
	<thead style="font-weight: bold;">
	<tr>
		<td style="width: 120px;">Prize Name</td>
		<td style="width: 360px;">Value</td>
		<td style="width: 120px;"></td>
	</tr>
	</thead>
	<tbody>
	<tr>
		<td class="formLabelLeft">
			1st Prize
		</td>
		<td>
			<input id="input_P2500" value="{{ $winnerList['P2500'] }}" class="config_input" />
		</td>
		<td>
			<div class="input_P2500_error error_msg" style="color:red;"></div>
		</td>
	</tr>
	<tr>
		<td class="formLabelLeft">
			2nd Prize
		</td>
		<td>
			<input id="input_P1000" value="{{ $winnerList['P1000'] }}" class="config_input" />
		</td>
		<td>
			<div class="input_P1000_error error_msg" style="color:red;"></div>
		</td>
	</tr>
	<tr>
		<td class="formLabelLeft">
			3rd Prize
		</td>
		<td>
			<input id="input_P500" value="{{ $winnerList['P500'] }}" class="config_input" />
		</td>
		<td>
			<div class="input_P500_error error_msg" style="color:red;"></div>
		</td>
	</tr>
	<tr>
		<td colspan="2" class="formLabelLeft">
			Special Prize
		</td>
		<td>
			<div class="input_P200_error error_msg" style="color:red;"></div>
		</td>
	</tr>
<?php $count = 0; ?>
@foreach( $winnerList['P200'] as $val )
	<tr>
		<td>&nbsp;</td>
		<td><input class="input_P200 config_input" value="{{ $val }}" /></td>
		<td>&nbsp;</td>
	</tr>
<?php $count++; ?>
@endforeach

	<tr>
		<td class="formLabelLeft">
			Consolation Prize
		</td>
		<td>
			<div class="input_P60_error error_msg" style="color:red;"></div>
		</td>
	</tr>
<?php $count = 0; ?>
@foreach( $winnerList['P60'] as $val )
	<tr>
		<td>&nbsp;</td>
		<td><input class="input_P60 config_input" value="{{ $val }}" /></td>
		<td>&nbsp;</td>
	</tr>
	<?php $count++; ?>
@endforeach
	</tbody>
</table>
		<input type="hidden" name="_token" value="{{csrf_token()}}">
		</div>
		<h4 style="color:red;">

		{{ Session::get('message') }}
		@foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
        @endforeach
		</h4>
		<div class="clear">
            <input style='cursor: pointer;' id="form_submit_btn" value="Submit" type="button" onclick="submitWinnerListForm(this);" />
        </div>

</form>

</body>
</html>