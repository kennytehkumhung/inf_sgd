@extends('admin/master_lte')

@section('title', 'Admin Dashboard')

@section('top_js')
    <link rel="stylesheet" href="{{ URL('/') }}/admin/jqwidgets/styles/jqx.base.css" type="text/css" />
    <link rel="stylesheet" href="{{ URL('/') }}/admin/jqwidgets/styles/jqx.metro.css" type="text/css" />   
    <script type="text/javascript" src="{{ URL('/') }}/admin/jqwidgets/jqx-all.js"></script>	

    <script type="text/javascript" src="{{ URL('/') }}/admin/jqwidgets/jqxcore.js"></script>
    <script type="text/javascript" src="{{ URL('/') }}/admin/jqwidgets/jqxdraw.js"></script>
    <script type="text/javascript" src="{{ URL('/') }}/admin/jqwidgets/jqxchart.core.js"></script>
    <script type="text/javascript" src="{{ URL('/') }}/admin/jqwidgets/jqxdata.js"></script>
	<script type="text/javascript" src="{{ URL('/') }}/admin/jqwidgets/jqxtabs.js"></script>
	
<script type="text/javascript">
	$(document).ready(function () {
						var h = $(window).height() - 53;
		// create jqxTabs.
		$('#mastertab').jqxTabs({ height: h, width: '100%',  autoHeight: false, showCloseButtons: true ,  theme:'ui-start' });
		
		$('#mastertab').on('removed', function (event) { 

			tabs[event.args.title] = true;
		}); 
		//$("#jqxNavigationBar").jqxNavigationBar({ width: 200, height: 600, theme:'fresh', expandMode: 'toggle'});
		
	});
	var tabs = new Array(); 
	var tabs_record = new Array(); 
	function addTab(title,url){
		
		for(count = 0; count < 20; count++){
			text = $('#mastertab').jqxTabs('getTitleAt', count); 
			if(text == title)
			{
				$('#mastertab').jqxTabs('removeAt', count); 
			}
        }
		
		if ( tabs[title] == undefined ||  tabs[title] == true ) {
			tabs[title] = false;
			
			 $('#mastertab').jqxTabs({ selectedItem: 1 }); 
			 $('#mastertab').jqxTabs('addLast', title, ' <iframe frameBorder="0" src="'+url+'" style="overflow:hidden;overflow-x:hidden;overflow-y:hidden;height: 90%; width:100%;"></iframe> ');

		}
		
		
        
	
	}	
	function closeTab(){

		$('#mastertab').jqxTabs('removeAt', $('#mastertab').jqxTabs('selectedItem'));


	}
	
	 
</script>

<!--PL Report Grid chart-->
<script type="text/javascript">
	$(document).ready(function () {
			var sampleData = [
	    @foreach( $currencies as $code => $currency)
		@foreach( $products as $prdid => $product)
			{ Vendor: '{{$product}}', PL: {{$profitloss[$prdid][$currency]['winloss']}}},
		@endforeach
		@endforeach
		]; 
	   /* var sampleData = [
			{ Vendor: 'WFT', PL: 1000},
			{ Vendor: 'IBCX', PL: -2500},
			{ Vendor: 'PSB', PL: 3000},
			{ Vendor: 'W88', PL: -4000},
			{ Vendor: 'PLT', PL: 4500},
			{ Vendor: 'CT8', PL: 3000},
			{ Vendor: 'AGG', PL: 2000},
			{ Vendor: 'MXB', PL: 2000},
						
		]; */
		settings = {
			//title: "Profit & Loss Report",
			//description: '(source: Bureau of Economic Analysis)',
			borderLineWidth: 1,
			showBorderLine: true,
			enableAnimations: true,
			showLegend: false,
			//padding: { left: 5, top: 5, right: 10, bottom: 5 },
			//titlePadding: { left: 0, top: 0, right: 0, bottom: 10 },
			source: sampleData,
			xAxis:
			{
				//description: 'Year and quarter',
				dataField: 'Vendor',
				unitInterval: 1,
				textRotationAngle: -75,
				formatFunction: function (value, itemIndex, serie, group) {
					return value;
				},
				valuesOnTicks: false
			},
			colorScheme: 'scheme05',
			seriesGroups:
			[
				{
					type: 'column',
					valueAxis:
					{
						description: 'Member Profit & Loss',
						formatFunction: function (value) {
							return value;//+ '(MYR)'
						},
						maxValue: 10000,
						minValue: -10000,
						unitInterval: 2000
					},
					series:
						[
							{
								dataField: 'PL',
								displayText: 'Change',
								toolTipFormatFunction: function(value, itemIndex, serie, group, categoryValue, categoryAxis) {
									return '<DIV style="text-align:left";><b>Vendor: </b>' + categoryValue
											 + '<br /><b>P&L:</b> MYR ' + value + '</DIV>'
								},
								colorFunction: function (value, itemIndex, serie, group) {
									return (value < 0) ? '#CC1133' : '#55CC55';
								}
							}
						]
				}
			]
		};
		$("#chart1").jqxChart(settings);
	});
</script>

<!--Transaction Grid chart-->
<script type="text/javascript">
	$(document).ready(function () {
		 var sampleData = [
			@foreach( $currencies as $code => $currency)
			@foreach( $cashledgers[$currency] as $id => $cashledger)
			{ Type: '{{$cashledger['name']}}', PL: <?php echo htmlspecialchars_decode($cashledger['amount']) ?>},
			@endforeach			
			@endforeach	
		]; 
	   /* var sampleData = [
			{ Type: 'Deposit', PL: 1000},
			{ Type: 'Withdrawal', PL: 2500},
			{ Type: 'Bonus', PL: 3000},
			{ Type: 'Adjustment', PL: 4000},
			{ Type: 'Rebate', PL: 4500}
		]; */
		settings = {
			//title: "Transaction Report",
			//description: '(source: Bureau of Economic Analysis)',
			borderLineWidth: 1,
			showBorderLine: true,
			enableAnimations: true,
			showLegend: false,
			//padding: { left: 5, top: 5, right: 10, bottom: 5 },
			//titlePadding: { left: 0, top: 0, right: 0, bottom: 10 },
			source: sampleData,
			xAxis:
			{
				//description: 'Year and quarter',
				dataField: 'Type',
				unitInterval: 1,
				textRotationAngle: -75,
				formatFunction: function (value, itemIndex, serie, group) {
					return value;
				},
				valuesOnTicks: false
			},
			colorScheme: 'scheme05',
			seriesGroups:
			[
				{
					type: 'column',
					valueAxis:
					{
						description: 'Transaction Amount (MYR)',
						formatFunction: function (value) {
							return value;//+ '(MYR)'
						},
						maxValue: 10000,
						minValue: 0,
						unitInterval: 2000
					},
					series:
						[
							{
								dataField: 'PL',
								displayText: 'Change',
								toolTipFormatFunction: function(value, itemIndex, serie, group, categoryValue, categoryAxis) {
									return '<DIV style="text-align:left";><b>Type: </b>' + categoryValue
											 + '<br /><b>Amount:</b> MYR ' + value + '</DIV>'
								},
								// Modify this function to return the desired colors.
								// jqxChart will call the function for each data point.
								// Sequential points that have the same color will be
								// grouped automatically in a line segment
								colorFunction: function (value, itemIndex, serie, group) {
									return (value < 0) ? '#CC1133' : '#55CC55';
								}
							}
						]
				}
			]
		};
		$("#chart2").jqxChart(settings);
	});
</script>

<!--Promotion Grid chart-->
<script type="text/javascript">
	$(document).ready(function () {
		 var sampleData = [
			@foreach( $bonuses as $id => $bonus)
			{ Bonus: '<?php echo htmlspecialchars_decode($bonus['name']) ?>', Amount: '{{$bonus['amount']}}'},		
			@endforeach	
		]; 
	   /* var sampleData = [
			{ Bonus: 'Beginner Pack IDR 120000', Amount: 1000},
			{ Bonus: 'Deposit MYR30 Beginner Pack', Amount: 2500},
			{ Bonus: 'Deposit MYR30 Beginner Pack', Amount: 2500},
			{ Bonus: 'Deposit MYR30 Beginner Pack', Amount: 2500},
			{ Bonus: 'Deposit MYR30 Beginner Pack', Amount: 2500},
			
		]; */
		settings = {
			//title: "Transaction Report",
			//description: '(source: Bureau of Economic Analysis)',
			borderLineWidth: 1,
			showBorderLine: true,
			enableAnimations: true,
			showLegend: false,
			//padding: { left: 5, top: 5, right: 10, bottom: 5 },
			//titlePadding: { left: 0, top: 0, right: 0, bottom: 10 },
			source: sampleData,
			xAxis:
			{
				//description: 'Year and quarter',
				dataField: 'Bonus',
				unitInterval: 1,
				textRotationAngle: -75,
				formatFunction: function (value, itemIndex, serie, group) {
					return value;
				},
				valuesOnTicks: false
			},
			colorScheme: 'scheme05',
			seriesGroups:
			[
				{
					type: 'column',
					valueAxis:
					{
						description: 'Promotion Amount (MYR)',
						formatFunction: function (value) {
							return value;//+ '(MYR)'
						},
						maxValue: 10000,
						minValue: 0,
						unitInterval: 2000
					},
					series:
						[
							{
								dataField: 'Amount',
								displayText: 'Change',
								toolTipFormatFunction: function(value, itemIndex, serie, group, categoryValue, categoryAxis) {
									return '<DIV style="text-align:left";><b>Type: </b>' + categoryValue
											 + '<br /><b>Amount:</b> MYR ' + value + '</DIV>'
								},
								colorFunction: function (value, itemIndex, serie, group) {
									return (value < 0) ? '#CC1133' : '#55CC55';
								}
							}
						]
				}
			]
		};
		$("#chart3").jqxChart(settings);
	});
</script>

<script type="text/javascript">
	$(document).ready(function () {
		$('#jqxTabs').jqxTabs({ width: '100%',  selectionTracker: true, animationType: 'fade' });
	});
</script>
@stop
@section('content')	
<div id='mastertab' style="float: left; width:100%; height:100%;">
	<ul style="margin: 30px;" id="unorderedList">
		<li>{{ Lang::get('public.Dashboard') }}</li>
	</ul>
	<div>
		<div class="row">
			<div class="col-md-12">				
				<div id='jqxTabs' class='jqx-rc-all'>
					<ul id="unorderedList">
						<li>
							<a href="home?currency={{Session::get('admin_crccode')}}">{{Session::get('admin_crccode')}}</a>
						</li>
					</ul>
					<div class="content-container">
						<div class="row">	
							<div class="col-md-12">
								<br>
								<div class="panel panel-primary" style="margin-left: 20px;margin-right: 20px; ">
									<div class="panel-heading">Wallet Summary</div>
									<div class="panel-body">
										<table class="table table-hover table-bordered">
											<thead>
												<tr>
													<!--<th>Currency</th>-->
													<th>{{Lang::get('COMMON.MEMBER')}}</th>
													<th>{{Lang::get('COMMON.REGISTRATION')}}</th>
													<th>{{Lang::get('COMMON.MAINWALLET')}}</th>
													@foreach( $products as $key => $product)
													<th>{{$product}}</th>
													@endforeach
													<th>Balance ({{Session::get('admin_crccode')}})</th>
												</tr>
											</thead>
											<tbody>
											@foreach( $currencies as $code => $currency)
												<tr>
													<!--<td field="currency">{{$currency}}</td>-->
													<td>{{$members[$code]}}</td>
													<td>{{$registers[$code]}}</td>
													<td>{{$balances[$code][0]}}</td>
											@foreach( $products as $prdid => $product)
													<td>{{$balances[$code][$prdid]}}</td>
											@endforeach
													<td>{{$balances[$code]['total']}}</td>
												</tr>
											@endforeach	
											</tbody>
										</table>
									</div>	
								</div>	
								<div class="row">								
									<div class="col-md-12">
										<div class="panel panel-primary" style="margin-left: 20px;margin-right: 20px; ">
											<div class="panel-heading">{{Lang::get('COMMON.PNLREPORT')}}</div>
											<div class="panel-body">
												<table class="table table-hover table-bordered">
													<thead>
														<tr>
															<th>{{Lang::get('COMMON.VENDOR')}}</th>
															<th>{{Lang::get('COMMON.CURRENCY')}}</th>
															<th>{{Lang::get('COMMON.MEMBER')}}</th>
															<th>{{Lang::get('COMMON.WAGERNUM')}}</th>
															<th>{{Lang::get('COMMON.TURNOVER')}}</th>
															<th>{{Lang::get('COMMON.VALIDSTAKE')}}</th>
															<th>{{Lang::get('COMMON.MEMBERPNL')}}</th>
														</tr>						
													</thead>					
													<tbody>
												@foreach( $currencies as $code => $currency)
												@foreach( $products as $prdid => $product)
														<tr>
															<td field="product">{{$product}}</td>
															<td field="currency">{{$currency}}</td>
															<td field="member">{{$profitloss[$prdid][$currency]['member']}}</td>
															<td field="wager">{{$profitloss[$prdid][$currency]['wager']}}</td>
															<td field="turnover">{{$profitloss[$prdid][$currency]['totalstake']}}</td>
															<td field="valid">{{$profitloss[$prdid][$currency]['validstake']}}</td>
															<td field="winloss">{{$profitloss[$prdid][$currency]['winloss']}}</td>
														</tr>		
												@endforeach
														<tr>
															<td field="product"><b>{{Lang::get('COMMON.TOTAL')}}</b></td>
															<td field="currency">{{$currency}}</td>
															<td field="member">{{$pnltotal[$currency]['member']}}</td>
															<td field="wager">{{$pnltotal[$currency]['wager']}}</td>
															<td field="turnover">{{$pnltotal[$currency]['totalstake']}}</td>
															<td field="valid">{{$pnltotal[$currency]['validstake']}}</td>
															<td field="winloss">{{$pnltotal[$currency]['winloss']}}</td>
														</tr>
												@endforeach				
													</tbody>					
												</table>
												<br>
												<div id='chart1' style="width:100%; height:500px">
												</div>				
											</div>								
										</div>
									</div>
								</div>	
								<div class="row">
									<div class="col-md-12">
									<div class="panel panel-primary" style="margin-left: 20px;margin-right: 20px; ">
										<div class="panel-heading">Transaction Report</div>
										<div class="panel-body">
											<table class="table table-hover table-bordered">
												<thead>
													<tr>
														<th>{{Lang::get('COMMON.CURRENCY')}}</th>
														<th>{{Lang::get('COMMON.TYPE')}}</th>
														<th align="right">{{Lang::get('COMMON.TRANSACTION')}}</th>
														<th align="right">{{Lang::get('COMMON.AMOUNT')}}</th>
														<th align="right">{{Lang::get('COMMON.FEE')}}</th>
													</tr>
												</thead>
												<tbody>
													@foreach( $currencies as $code => $currency)
													@foreach( $cashledgers[$currency] as $id => $cashledger)
													<tr>
														<td>{{$currency}}</td>
														<td>{{$cashledger['name']}}</td>
														<td>{{$cashledger['total']}}</td>
														<td><?php echo htmlspecialchars_decode($cashledger['amount']) ?></td>
														<td>{{$cashledger['fee']}}</td>
													</tr>			
													@endforeach			
													@endforeach			
												</tbody>
											</table>
											<div id='chart2' style=" width: 100%; height: 400px;">
											</div>	
										</div>								
										</div>		
									</div>		
								</div>		
								<div class="row">
									<div class="col-md-12">
										<div class="panel panel-primary" style="margin-left: 20px;margin-right: 20px; ">
											<div class="panel-heading">Promotion Report</div>
											<div class="panel-body">
												<table class="table table-hover table-bordered">
													<thead>
														<tr>
															<th align="left">{{Lang::get('COMMON.BONUS')}}</th>
															<!--<th field="currency" width="65" align="center">{{Lang::get('COMMON.CURRENCY')}}</th>-->
															<th align="right">{{Lang::get('COMMON.APPLIED')}}</th>
															<th align="right">{{Lang::get('COMMON.APPROVED')}}</th>
															<th align="right">{{Lang::get('COMMON.CONTDEPOSIT')}}</th>
															<th align="right">{{Lang::get('COMMON.AMOUNT')}}</th>
														</tr>
													</thead>
													<tbody>
														@foreach( $bonuses as $id => $bonus)
														<tr>
															<td field="bonus"><?php echo htmlspecialchars_decode($bonus['name']) ?></td>
															<!--<td field="currency">{{$bonus['currency']}}</td>-->
															<td>{{$bonus['apply']}}</td>
															<td>{{$bonus['apply']}}</td>
															<td>{{$bonus['continuedep']}}</td>
															<td>{{$bonus['amount']}}</td>
														</tr>			
														@endforeach
													</tbody>
												</table>
												<br>
												<div id='chart3' style="width:100%; height:400px">
												</div>	
											</div>
										</div>	
									</div>	
								</div>										
							</div>							
						</div>				
					</div>	
				</div>
			</div>
		</div>		
	</div>	
</div>
@stop
@section('bottom_js')
@stop