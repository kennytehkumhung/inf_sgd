<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "//www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="//www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title></title>
		
		<!--css-->
		<link href="css/style.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open Sans">
		<link rel="stylesheet" type="text/css" href="js/jeasyui/themes/default/easyui.css">
		<link rel="stylesheet" type="text/css" href="js/jeasyui/themes/icon.css">
		<link rel="stylesheet" type="text/css" href="js/editor/jquery-te-1.4.0.css">
		
		<!--script-->
		<link rel="stylesheet" type="text/css" href="css/custom.css">
		<script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
		<script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
		<script type="text/javascript" src="js/jeasyui/jquery.easyui.min.js"></script>
		<script type="text/javascript" src="js/jeasyui/datagrid-detailview.js"></script>
		<script type="text/javascript" src="js/editor/jquery-te-1.4.0.min.js"></script>
		<script type="text/javascript" src="js/general.js?v2"></script>
	</head>
<body>

<div class="horline_wt"></div>
	<div style="margin:10px">
		<table id="{{$grid['id']}}">
		</table>
		
			<div id="{{$grid['id']}}_tb" style="padding:5px;height:auto">
			@if(isset($grid['buttons'][1]))
			<div style="margin-bottom:5px;">
			@foreach( $grid['buttons'][1] as $button )
				<a href="#" onclick="parent.addTab('{{$button['value']}} {{$grid['title']}}', '{{$button['params']['url']}}')" class="easyui-linkbutton" iconCls="icon-{{$button['params']['icon']}} plain="true">{{$button['value']}}</a>
			@endforeach			
			</div>
			@endif

			@if( isset($grid['filters']) || isset($grid['searches']) )
			<div style="padding:3px">
			@if( isset($grid['filters']) )
				@foreach( $grid['filters'] as $filter )
					@if( isset($filter['params']['display']) )
						{{$filter['params']['display']}}
					@endif 
					<select id="{{$filter['name']}}" class="easyui-combobox" panelHeight="auto" style="width:@if( isset($filter['params']['width']) ){{$filter['params']['width']}}@else 120px;@endif" name="aflt[{{$filter['name']}}]" @if( isset($filter['params']['multiple']) && $filter['params']['multiple'] ) multiple="true" @endif>
						@if( !isset($filter['params']['select']) || (isset($filter['params']['select']) && $filter['params']['select']))
						<option value="">{{ Lang::get('COMMON.SELECT') }}</option>
						@endif
						@foreach( $filter['options'] as $key => $value )
						<option value="{{$key}}" @if( isset($filter['params']['selected']) && ($key == $filter['params']['selected'])) selected @endif>{{$value}}</option>
						@endforeach           
					</select>
				@endforeach
			@endif
			
			@if( isset($grid['searches']) )
			@foreach( $grid['searches'] as $search )
				@if (isset($search['params']['hidden']) && $search['params']['hidden'] == true)
					@foreach( $search['options'] as $key => $option )
						@if(!is_array($option))
						<input type="hidden" id="{{ $key }}" value="{{ isset($search['params']['value']) ? $search['params']['value'] : '' }}" />
						@endif
					@endforeach
				@else
					<input name="{{$search['name']}}" id="{{$search['name']}}" class="easyui-searchbox" data-options="prompt:'Please Input Value',menu:'#mm',searcher:doSearch" style="width:250px;" value="@if( isset($search['params']['value'])){{$search['params']['value']}}@endif"></input>
					<div id="mm" style="width:120px">
						@foreach( $search['options'] as $key => $option )
							@if(!is_array($option))
							<div data-options="name:'{{$key}}'">{{$option}}</div>
							@endif
						@endforeach			
					</div>
				@endif
			@endforeach
			@endif
			</div>
			@endif
			
			@if( isset($grid['ranges']))
			<div style="padding:3px">
				@foreach( $grid['ranges'] as $range )
					@if( isset($range['params']['display']))
						{{$range['params']['display']}}
					@endif 
					<input class="easyui-date<?php if( isset($range['params']['time']) && $range['params']['time'])echo 'time';?>box" style="width: @if( isset($range['params']['time']) && $range['params']['time'])140px @else 90px @endif " 
					id="{{$grid['id']}}_{{$range['name']}}From" data-options="formatter:formatDate<?php if( isset($range['params']['time']) && $range['params']['time'])echo 'Time';?>,parser:parseDate<?php if( isset($range['params']['time']) && $range['params']['time']) echo 'Time'; ?>" 
					value="@if( isset($range['params']['values']['from'])){{$range['params']['values']['from']}}@else{{$grid['dates']['todayfrom']}}@endif"> ~ 
					<input class="easyui-date<?php if( isset($range['params']['time']) && $range['params']['time']) echo 'time';?>box" style="width:@if( isset($range['params']['time']) && $range['params']['time'])140px @else 90px @endif" 
					id="{{$grid['id']}}_{{$range['name']}}To" data-options="formatter:formatDate<?php if( isset($range['params']['time']) && $range['params']['time']) echo 'Time';?>,parser:parseDate<?php if( isset($range['params']['time']) && $range['params']['time']) echo 'Time';?>" 
					value="@if(isset($range['params']['values']['to'])){{$range['params']['values']['to']}}@else{{$grid['dates']['todayto']}}@endif">
					
					<a href="#" onclick="doChangeDate('{{$grid['id']}}_{{$range['name']}}', '{{$grid['dates']['todayfrom']}}', '{{$grid['dates']['todayto']}}');">+0D</a> | <a href="#" onclick="doChangeDate('{{$grid['id']}}_{{$range['name']}}', '{{$grid['dates']['yesterdayfrom']}}', '{{$grid['dates']['yesterdayto']}}');">-1D</a> | <a href="#" onclick="doChangeDate('{{$grid['id']}}_{{$range['name']}}', '{{$grid['dates']['sevendayfrom']}}', '{{$grid['dates']['todayto']}}');">-7D</a> | <a href="#" onclick="doChangeDate('{{$grid['id']}}_{{$range['name']}}', '{{$grid['dates']['monthstart']}}', '{{$grid['dates']['todayto']}}');">MTD</a>
				@endforeach			
			</div>
			@endif

			<div style="padding:3px">
			    <a href="#" class="easyui-linkbutton" iconCls="icon-search" onclick="doSearch();">{{ Lang::get('COMMON.SEARCH') }}</a>
				@if( isset($grid['buttons'][2]) )
					@foreach( $grid['buttons'][2] as $button )
						<a href="#" class="easyui-linkbutton" iconCls="icon-{{$button['params']['icon']}}" onclick="{{$button['params']['url']}}">{{$button['value']}}</a>
					@endforeach				
				@endif
				@if( isset($grid['buttons'][4]) )
				    @foreach( $grid['buttons'][4] as $button )
						<a href="javascript:void(0)" id="button" class="easyui-linkbutton" iconCls="icon-{{$button['params']['icon']}}" disabled="{{$button['params']['hide']}}" onclick="doBack();">{{$button['value']}}</a>
				    @endforeach				
				@endif
			</div>
		</div>
		<div id="{{$grid['id'] }}_tt">
			@if( isset($grid['buttons'][3]) )
				@foreach( $grid['buttons'][3] as $button )
				<a href="javascript:void(0)" class="icon-{{$button['params']['icon']}}" onclick="doExport()"></a>
				@endforeach
			@endif
		</div>
	</div>
		<div id="dd" class="easyui-dialog" closed="true"></div>

<script type="text/javascript">
var subgrid_id = 0;
var tempValue = [];
var groupbyDate = 0;

$(function(){
	$('#{{$grid['id']}}').datagrid({
		title: '{{$grid['title']}}',
		iconCls: 'icon-save',
		width: 'auto',
		height: 'auto',
		singleSelect: @if( isset($grid['options']['multiple']) && $grid['options']['multiple'])false @else true @endif,
		tools:'#{{$grid['id']}}_tt',
		toolbar:'#{{$grid['id']}}_tb',
		rownumbers:true,
@if( $grid['limit'] > 0)
		pagination:true,
		pageSize:{{$grid['limit']}},
		pageList: [10,20,50,100,150,200],
@endif
		loader: loadData,
		showFooter: @if( $grid['footer']) true @else false @endif,
		columns:[[
@foreach( $grid['columns'] as $column)

	@if( isset($column['options']['group']))
		{
			title:'{{$column['name']}}',
			@if( isset($column['options']['group']))
				colspan:{{$column['options']['colspan']}},
			@endif
		},
	@else
		{
			field:'{{$column['index']}}',
			title:'{{$column['name']}}',
			width:{{$column['width']}},
			align:
			@if( isset($column['options']['align']))
				'{{$column['options']['align']}}',
			@else
				'left',
			@endif 
			
			@if( isset($column['options']['checkbox']) && $column['options']['checkbox'])
				checkbox: true,
			@endif 
			
			@if( isset($column['options']['sort']) && $column['options']['sort'])
				sortable:true,
			@endif 
			
			@if( isset($column['options']['hidden']) && $column['options']['hidden'])
				hidden:true,
			@endif 
			
			@if( isset($grid['rowspan']))
				rowspan:2 ,
			@endif
		},
	@endif
@endforeach	
]

@if( isset($grid['rowspan']))
,[
@foreach( $grid['subcolumns'] as $column)
{field:'{{$column['index']}}',title:'{{$column['name']}}',width:{{$column['width']}},align:@if( isset($column['options']['align']))'{{$column['options']['align']}}'@else'left'@endif},
@endforeach
]
@endif
		],

@if( isset($grid['options']['detail']) && $grid['options']['detail'])
		view: detailview,
		detailFormatter:function(index,row){
			return '<div style="padding:2px"><table class="ddv"></table></div>';
		},
		onExpandRow: function(index,row){
			var ddv = $(this).datagrid('getRowDetail',index).find('table.ddv');
			subgrid_id = row.id;
			ddv.datagrid({
				loader: loadSubData,
				fitColumns:true,
				singleSelect:true,
				rownumbers:true,
				loadMsg:'',
				height:'auto',
				columns:[[
@foreach( $details['columns'] as $column)
	{
		field:'{{$column['index']}}',
		title:'{{$column['name']}}',
		width:{{$column['width']}},
		align:
			@if( isset($column['options']['align']))
				'{{$column['options']['align']}}',
			@else
				'left',
			@endif
		@if( isset($column['options']['checkbox']) && $column['options']['checkbox'])
			checkbox: true,
		@endif
		@if( isset($column['options']['sort']) && $column['options']['sort'])
			sortable:true,
		@endif
		@if( isset($column['options']['hidden']) && $column['options']['hidden'])
			hidden:true,
		@endif
	}
@endforeach				]],
				onResize:function(){
					$('#dg').datagrid('fixDetailRowHeight',index);
				},
				onLoadSuccess:function(){
					setTimeout(function(){
						$('#dg').datagrid('fixDetailRowHeight',index);
					},0);
				}
			});
			$('#dg').datagrid('fixDetailRowHeight',index);
		},
@endif
		onHeaderContextMenu: function(e, field){
			e.preventDefault();
			if (!cmenu){
				createColumnMenu();
			}
			cmenu.menu('show', {
				left:e.pageX,
				top:e.pageY
			});
		}
	});
@if( isset($grid['filters']))
	@foreach( $grid['filters'] as $filter )
		@if( isset($filter['params']['multiple']) && $filter['params']['multiple'])
			$('#{{$filter['name']}}').combobox('setValues', {{$filter['params']['values']}});
		@endif
	@endforeach
@endif
});

function loadData(param, success, error) {
	var opt = $(this).datagrid('options');
	var limit = parseInt(param.rows);
	var start = (parseInt(param.page) - 1) * limit;
	if(param.sort != 'undefined') var sort = param.sort;
	if(param.order != 'undefined') var order = param.order;
	if(isNaN(limit)) limit = 0;
	if(isNaN(start)) start = 0;

	$.ajax({
		url: '{{$grid['action']}}',
		type: 'POST',
		dataType: 'json',
		jsonp: false,
		data: {
@if( isset($grid['searches']))
			aqfld: {
	@foreach( $grid['searches'] as $search )
					{{$search['name']}}: $('#{{$search['name']}}').searchbox('getName'),
	@endforeach			},
			aqtxt: {
@if( isset($grid['options']['params']['aqtxt']))
	@foreach( $grid['options']['params']['aqtxt'] as $key => $param)
				{{$key}}: '{{$param}}',
	@endforeach
	@endif
	@foreach( $grid['searches'] as $search )
					{{$search['name']}}: $('#{{$search['name']}}').searchbox('getValue'),
	@endforeach			
	},
@endif
@if( isset($grid['filters']))
			aflt: {
@if( isset($grid['options']['params']['aflt']))
@foreach( $grid['options']['params']['aflt'] as $key => $param)
			{{$key}}: '{{$param}}',
@endforeach
@endif
@foreach( $grid['filters'] as $filter )
				{{$filter['name']}}: $('#{{$filter['name']}}').combobox('getValue<?php if( isset($filter['params']['multiple']) && $filter['params']['multiple']) echo 's';?>'),
@endforeach			},
@endif
@if( isset($grid['ranges']))
			aqrng: {
@foreach( $grid['ranges'] as $range )
				{{$range['name']}}from: $('#{{$grid['id']}}_{{$range['name']}}From').datebox('getValue'),
				{{$range['name']}}to: $('#{{$grid['id']}}_{{$range['name']}}To').datebox('getValue'),
@endforeach			},
@endif
			igd: groupbyDate,
			start: start,
			limit: limit,
			sfld: sort,
			sodr: order,
@if( isset($grid['options']['params']))
@foreach( $grid['options']['params'] as $key => $param )
@if( !is_array($param) )
			{{$key}}: '{{$param}}',
@endif
@endforeach
@endif
@if( isset($grid['options']['iprt']))
			iprt: {{$grid['options']['iprt']}},
@endif
@if( isset($sid))
			sid: {{$sid}},
@endif
			mdl:'{{$grid['module']}}',
			action:'{{$grid['action']}}',
			_token: '{{ csrf_token() }}'
		},
		success: function(data){
			success(data);
		},
		error: function(){
			error.apply(this, arguments);
		}
	}).done(function() {
@if( isset($traffic))
	    loadTraffic();
@endif 	    
	});
}


function doChangeDate(field, fromDate, toDate){
	var fieldFrom = '#'+field+'From';
	var fieldTo = '#'+field+'To';
	$(fieldFrom).datebox('setValue', fromDate);
	$(fieldTo).datebox('setValue', toDate);
}
	
function doSearch() {
	$('#{{$grid['id']}}').datagrid('load');
	
@if( isset($back))
	var fieldFrom = '#{{$grid['id']}}_createdFrom';
	var fieldTo = '#{{$grid['id']}}_createdTo';
	
	currentFrom = $(fieldFrom).datebox('getValue');
	currentTo = $(fieldTo).datebox('getValue');
	
	tempValue[0] = 	[{id:"imode",type:"combo",value:"",name:""},
		{id:"iprdid",type:"combo",value:"",name:""},
		{id:"icategory",type:"combo",value:"",name:""},
		{id:"search1",type:"search",value:"",name:"code"},
		{id:"ilevel",type:"combo",value:"",name:""},
		{id:"createdFrom",type:"date",value:currentFrom,name:""},
		{id:"createdTo",type:"date",value:currentTo,name:""},
		{id:"groupbydate",value:0}];
@endif 
}

function doGridSearch(id, type, value, name) {
	if(type == 'search') {
		$('#'+id).searchbox('selectName', name);
		$('#'+id).searchbox('setValue', value);
	} else if(type == 'combo') {
		$('#'+id).combobox('setValue', value);
	} else if(type == 'date'){
	    @if( isset($grid['ranges']))
		    $('#{{$grid['id']}}_'+id).datebox('setValue',value);
	    @endif
	}
	
}

function doGetTempData(imode,iprdid,icategory,search1,ilevel,createdFrom,createdTo,groupbydate){
    	var tempObject = [
	{id:"imode",type:"combo",value:imode,name:""},
	{id:"iprdid",type:"combo",value:iprdid,name:""},
	{id:"icategory",type:"combo",value:icategory,name:""},
	{id:"search1",type:"search",value:search1,name:"code"},
	{id:"ilevel",type:"combo",value:ilevel,name:""},
	{id:"createdFrom",type:"date",value:createdFrom,name:""},
	{id:"createdTo",type:"date",value:createdTo,name:""},
	{id:"groupbydate",value:groupbydate}	    
	];
	tempValue.push(tempObject);
	
}

var cmenu;
function createColumnMenu(){
	cmenu = $('<div/>').appendTo('body');
	cmenu.menu({
		onClick: function(item){
			if (item.iconCls == 'icon-ok'){
				$('#{{$grid['id']}}').datagrid('hideColumn', item.name);
				cmenu.menu('setIcon', {
					target: item.target,
					iconCls: 'icon-empty'
				});
			} else {
				$('#{{$grid['id']}}').datagrid('showColumn', item.name);
				cmenu.menu('setIcon', {
					target: item.target,
					iconCls: 'icon-ok'
				});
			}
		}
	});
	var fields = $('#{{$grid['id']}}').datagrid('getColumnFields');
	for(var i=0; i<fields.length; i++){
		var field = fields[i];
		var col = $('#{{$grid['id']}}').datagrid('getColumnOption', field);
		cmenu.menu('appendItem', {
			text: col.title,
			name: field,
			iconCls: 'icon-ok'
		});
	}
}

function doConfirmAction(mdl, action, sid){
	$.messager.confirm('{{Lang::get('COMMON.CONFIRMATION')}}', '{{Lang::get('COMMON.CONFIRMPROCESS')}}', function(r){
		if (r){
			$.ajax({
			  type: "POST",
			  url: "index.php",
			  data: {mdl:mdl, action:action, sid:sid},
			  success: function(result){
				obj = JSON.parse(result);
				if(obj.success == true) {
					alert('{{Lang::get('COMMON.UPDATED')}}');
					doSearch();
				} else
					alert('{{Lang::get('COMMON.UPDATEFAIL')}}');
			  }
			});
		} else {
			$('#{{$grid['id']}}').datagrid('uncheckAll', i);
		}
	});
}

@if( isset($grid['options']['detail']) && $grid['options']['detail'])
function loadSubData(param, success, error) {
	$.ajax({
		url: 'index.php',
		type: 'POST',
		dataType: 'json',
		jsonp: false,
		data: {
			sid: subgrid_id,
			mdl:'{{$details['mdl']}}',
			action:'{{$details['action']}}'
		},
		success: function(data){
			success(data);
		},
		error: function(){
			error.apply(this, arguments);
		}
	});
}
@endif

</script>
<script>
	function submitForm() {
		$('#cboForm').submit();
	}
</script>
</body>
</html>