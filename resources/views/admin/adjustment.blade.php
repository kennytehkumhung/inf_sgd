<!DOCTYPE html>
<html lang="en">
<head>
    <title id='Description'></title>
	<head>
	<link rel="stylesheet" href="{{url()}}/admin/jqwidgets/styles/jqx.base.css" type="text/css" />
    <link rel="stylesheet" href="{{url()}}/admin/jqwidgets/styles/jqx.bootstrap.css" type="text/css" />
    <script type="text/javascript" src="{{url()}}/admin/jqwidgets/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="{{url()}}/admin/jqwidgets/jqx-all.js"></script>
	 <script type="text/javascript">
        $(document).ready(function () {

			$("#form_submit_btn").jqxButton({ width: '150', template: "success"});
			$("#form_submit_btn").on('click', function () {
                   $("#events").find('span').remove();
                   $("#events").append('<span>Submit Button Clicked</span>');
             });
			//$("#form_wapper").jqxExpander({ width: '100%', theme:'bootstrap', toggleMode: 'dblclick'});
			@foreach( $form['inputs'] as $input )
				@if($input['type'] == 'date')	
					$("#{{$input['name']}}").jqxDateTimeInput({ width: '120px' , height: '25px' , formatString: "yyyy-MM-dd" ,openDelay: 0,closeDelay: 0});
				@endif
				@if($input['type'] == 'datetime')
					$("#{{$input['name']}}").jqxDateTimeInput({ width: '220px' , height: '25px' , formatString: "yyyy-MM-dd h:mm:ss" ,openDelay: 0,closeDelay: 0,showTimeButton: true});
				@endif
			@endforeach	
        });
		
	
		
		
		
    </script>
	</head>
	<body class='default'>

<form id="cboForm" method="get" action="{{$module}}" >
<div id="form_wapper" >
<table>
@foreach( $form['inputs'] as $input )
	<tr @if($input['type'] == 'hidden')style="display:none;"@endif>
		<td class="formLabelLeft">{{$input['title']}}</td>
		<td>
			@if($input['type'] == 'text')
				<input id="{{$input['name']}}" type="text" name="{{$input['name']}}" value="{{$input['value']}}" @if( isset($input['params']['disabled']) && $input['params']['disabled'])disabled @endif>
			@elseif($input['type'] == 'password')
				<input type="password" name="{{$input['name']}}">
				@elseif($input['type'] == 'textarea')
				<textarea rows="5" cols="30" name="{{$input['name']}}">{{$input['value']}}</textarea>
			@elseif($input['type'] == 'checkbox_single')
				<input type="checkbox" name="{{$input['name']}}" value="1" @if($input['value'])checked @endif>{{$value}}
			@elseif($input['type'] == 'checkbox')	
				@foreach( $input['params']['options'] as $key => $value )
						<input type="checkbox" name="{{$input['name']}}[]" value="{{$key}}" @if(in_array($key,$input['params']['values']))checked @endif>{{$value}}
				@endforeach
				
			@elseif($input['type'] == 'radio')	
				@foreach( $input['params']['options'] as $key => $value )
					@if( $key == $input['value'])
						<input name="{{$input['name']}}" type="radio" value="{{$key}}" checked >{{$value}}
					@else
						<input name="{{$input['name']}}" type="radio" value="{{$key}}" >{{$value}}
					@endif
				@endforeach
			@elseif($input['type'] == 'select')	
				<select name="{{$input['name']}}">
					@foreach( $input['params']['options'] as $key => $value )
						@if($key == 0 && $value == Lang::get('COMMON.SELECT'))	
							<option  value="" @if($input['value'] == $key) selected @endif>{{$value}}</option>
						@else
							<option  value="{{$key}}" @if($input['value'] == $key) selected @endif>{{$value}}</option>
						@endif
					@endforeach
				</select>
			@elseif($input['type'] == 'date')	
				<div id="{{$input['name']}}"></div>
			@elseif($input['type'] == 'datetime')	
				<select name="day" class="depRight4" style="margin-right: 8px;">
				    @for ($i = 1; $i <= 31; $i++)
				    <option value="{{ str_pad($i, 2, '0', STR_PAD_LEFT) }}">{{ str_pad($i, 2, '0', STR_PAD_LEFT) }}</option>
				    @endfor
				</select>
				<select name="month" class="depRight4" style="margin-right: 8px;">
				    @for ($i = 1; $i <= 12; $i++)
				    <option value="{{ str_pad($i, 2, '0', STR_PAD_LEFT) }}">{{ str_pad($i, 2, '0', STR_PAD_LEFT) }}</option>
				    @endfor
				</select>
				<select name="year" class="depRight4">
				    <option value="{{ (new DateTime())->format('Y') }}">{{ (new DateTime())->format('Y') }}</option>
				    <option value="{{ (new DateTime('-1 year'))->format('Y') }}">{{ (new DateTime('-1 year'))->format('Y') }}</option>
				</select>
				<select name="hours" style="background: #fff none repeat scroll 0 0 !important;
    border: 1px solid #6b6b6b !important;
    width: 80px !important;">
				<option value="01">01</option>
				<option value="02">02</option>
				<option value="03">03</option>
				<option value="04">04</option>
				<option value="05">05</option>
				<option value="06">06</option>
				<option value="07">07</option>
				<option value="08">08</option>
				<option value="09">09</option>
				<option value="10">10</option>
				<option value="11">11</option>
				<option value="12">12</option>
				</select>
				<select name="minutes" style="background: #fff none repeat scroll 0 0 !important;
    border: 1px solid #6b6b6b !important;
    width: 80px !important;">
				<option value="00">00</option>
				<option value="01">01</option>
				<option value="02">02</option>
				<option value="03">03</option>
				<option value="04">04</option>
				<option value="05">05</option>
				<option value="06">06</option>
				<option value="07">07</option>
				<option value="08">08</option>
				<option value="09">09</option>
				<option value="10">10</option>
				<option value="11">11</option>
				<option value="12">12</option>
				<option value="13">13</option>
				<option value="14">14</option>
				<option value="15">15</option>
				<option value="16">16</option>
				<option value="17">17</option>
				<option value="18">18</option>
				<option value="19">19</option>
				<option value="20">20</option>
				<option value="21">21</option>
				<option value="22">22</option>
				<option value="23">23</option>
				<option value="24">24</option>
				<option value="25">25</option>
				<option value="26">26</option>
				<option value="27">27</option>
				<option value="28">28</option>
				<option value="29">29</option>
				<option value="30">30</option>
				<option value="31">31</option>
				<option value="32">32</option>
				<option value="33">33</option>
				<option value="34">34</option>
				<option value="35">35</option>
				<option value="36">36</option>
				<option value="37">37</option>
				<option value="38">38</option>
				<option value="39">39</option>
				<option value="40">40</option>
				<option value="41">41</option>
				<option value="42">42</option>
				<option value="43">43</option>
				<option value="44">44</option>
				<option value="45">45</option>
				<option value="46">46</option>
				<option value="47">47</option>
				<option value="48">48</option>
				<option value="49">49</option>
				<option value="50">50</option>
				<option value="51">51</option>
				<option value="52">52</option>
				<option value="53">53</option>
				<option value="54">54</option>
				<option value="55">55</option>
				<option value="56">56</option>
				<option value="57">57</option>
				<option value="58">58</option>
				<option value="59">59</option>
				</select>
				<select name="range" style="background: #fff none repeat scroll 0 0 !important;
    border: 1px solid #6b6b6b !important;
    width:80px !important;">
				<option value="AM">AM</option>
				<option value="PM">PM</option>
				</select>
				<input type="hidden" id="date" name="date" />
			@elseif($input['type'] == 'daterange')	
			
			@elseif($input['type'] == 'showonly')	
				{{$input['value']}}
			@elseif($input['type'] == 'file')		
			
			@elseif($input['type'] == 'hidden')
				<input type="hidden" name="{{$input['name']}}" value="{{$input['value']}}">
			@endif
		</td>
		<td>
			<div class="{{$input['name']}}_error error_msg" style="color:red;"></div>
		</td>
	</tr>
@endforeach		
</table>
		<input type="hidden" name="_token" value="{{csrf_token()}}">
		</div>
		<h4 style="color:red;">
		
		{{ Session::get('message') }}
		@foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
        @endforeach
		</h4>
		<div class="clear">
            <input style='margin-top: 20px;cursor: pointer;' value="Submit" id='form_submit_btn' type="submit"/>
        </div>
		
</form>

</body>
</html>