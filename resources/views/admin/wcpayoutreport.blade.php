<link rel="stylesheet" href="{{ URL('/') }}/admin/lte/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ URL('/') }}/admin/lte/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{ URL('/') }}/admin/lte/dist/css/skins/_all-skins.min.css">
	<link rel="stylesheet" href="{{url()}}/admin/jqwidgets/styles/jqx.base.css" type="text/css" />
    <link rel="stylesheet" href="{{url()}}/admin/jqwidgets/styles/jqx.bootstrap.css" type="text/css" />
    <script type="text/javascript" src="{{url()}}/admin/jqwidgets/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="{{url()}}/admin/jqwidgets/jqx-all.js"></script>
    <script type="text/javascript" src="{{url()}}/admin/js/general.js"></script>
		<script src="{{url()}}/gsc/resources/js/distpicker.data.js"></script>
	  <script src="{{url()}}/gsc/resources/js/distpicker.js"></script>
	  <script src="{{url()}}/gsc/resources/js/main.js"></script>

@if(!empty($selectOptionCategorys))
			<script>
			function submitToken(){
				if (confirm('Confirm payout')) {
					$.ajax({
						type: "POST",
						url: "{{action('Admin\WorldCupController@addToken')}}",
						data: {
                            username:$('#username').val(),
                            token:$('#Token').val(),
						}
                    }).done(function( json ) {
                        obj = JSON.parse(json);
                        var str = '';
                        $.each(obj, function(i, item) {
                            str += item + '\n';
                        });
                            alert(str);
                    });
				}
			}
			function submitPayout(){
                if (confirm('Confirm payout')) {
                    $.ajax({
                        type: "POST",
                        url: "{{action('Admin\WorldCupController@showWorldCupPayoutReportdoGetData')}}",
                        data: {
                            category:$('#WCCategory').val(),
                            team:$('#WCTeamName').val(),
                            group:$('#WCGroup').val(),

                        }
                    }).done(function (result) {
                        if (result == 1) {
                            alert('{{ Lang::get('public.Success') }}');
                        } else {
                            alert('{{ Lang::get('public.Failed') }}');
                        }

                    });
                }
			}
				function changeTeamValue(){
					@foreach($selectOptionCategorys as $selectOptionCategory => $value)
                        if($('#WCCategory').val()=='{{$selectOptionCategory}}'){
							$('#WCTeamName')
								.empty()
								@foreach($selectOptionTeams as $selectOptionTeam => $keys)
									@foreach($keys as $key =>$value)
										@if($selectOptionTeam==$selectOptionCategory)
											.append('<option value="{{$key}}">{{$value}}</option>')
										@endif
									@endforeach
								@endforeach
								.find('option:first')
							;
                   		}
					@endforeach
                    if($('#WCCategory').val()=='8') {
                        $('#WCTeamNameDIV').hide();
                        $('#WCGroupDIV').show();
                        $('#WCGroup')
                            .empty()
                            .append('<option disabled selected value> -- select an option -- </option>')
                            @foreach($selectOptionGroups as $selectOptionGroup =>$keys)
								.append('<option value="{{$selectOptionGroup}}">{{$selectOptionGroup}}</option>')
                        	@endforeach
							.find('option:first')
                        ;
                    }else{
                        $('#WCTeamNameDIV').show();
                        $('#WCGroupDIV').hide();
                    }
				}
                function changeGroupValue(){
					@foreach($selectOptionGroups as $selectOptionGroup =>$keys)
                    if($('#WCGroup').val()=='{{$selectOptionGroup}}'){
                        $('#WCTeamNameDIV').show();
                        $('#WCTeamName')
                            .empty()
                            @foreach($keys as $key =>$value)
                                @if($selectOptionGroup==$selectOptionGroup)
									.append('<option value="{{$key}}">{{$value}}</option>')
								@endif
							@endforeach
							.find('option:first')
                        ;
                    }
					@endforeach
                }
			</script>
@endif
<body style="background-color:#ecf0f5;">
<br>
<div class="col-md-6">
	<!-- Horizontal Form -->
	<div class="box box-info">
		<div class="box-header with-border">
			<h3 class="box-title">World-Cup Payout</h3>
		</div>
		<!-- /.box-header -->
		<!-- form start -->
		<form class="form-horizontal">
			<div class="box-body">
				<div class="form-group">
					<label for="inputEmail3" class="col-sm-2 control-label">Category</label>
					<div class="col-sm-10">
						<select class="form-control" id="WCCategory" onchange="changeTeamValue()">
							<option disabled selected value> -- select an option -- </option>
							@foreach($selectOptionCategorys as $selectOptionCategory => $value)
								<option value="{{$selectOptionCategory}}">{{$value}}</option>
							@endforeach
						</select>
					</div>
				</div>
				<div class="form-group" id='WCGroupDIV' hidden>
					<label for="inputEmail3" class="col-sm-2 control-label">Group</label>
					<div class="col-sm-10">
						<select class="form-control" id="WCGroup" onchange="changeGroupValue()">
							<option disabled selected value> -- select an option -- </option>
						</select>
					</div>
				</div>
				<div class="form-group" id='WCTeamNameDIV'>
					<label for="inputEmail3" class="col-sm-2 control-label">Team</label>
					<div class="col-sm-10">
						<select class="form-control" id="WCTeamName">
							<option disabled selected value> -- select an option -- </option>
						</select>
					</div>
				</div>
			</div>
		</form>
			<div class="box-footer">
				<button type="button" class="btn btn-info pull-right" id='form_submit_btn' onClick="submitPayout()" >{{ Lang::get('COMMON.SUBMIT') }}</button>
			</div>
	</div>
</div>

<div class="col-md-6">
	<!-- Horizontal Form -->
	<div class="box box-info">
		<div class="box-header with-border">
			<h3 class="box-title">World-Cup Add Token</h3>
		</div>
		<!-- /.box-header -->
		<!-- form start -->
		<form class="form-horizontal">
			<div class="box-body">
				<div class="form-group">
					<label for="inputEmail3" class="col-sm-2 control-label">{{ Lang::get('COMMON.ACCOUNT') }}</label>

					<div class="col-sm-10">
						<input type="email" class="form-control" id="username" placeholder="{{ Lang::get('COMMON.NAME') }}">
					</div>
				</div>
				<div class="form-group">
					<label for="inputPassword3" class="col-sm-2 control-label">Token</label>

					<div class="col-sm-10">
						<input type="text" class="form-control" id="Token">
					</div>
				</div>
			</div>
		</form>
		<div class="box-footer">
			<button type="button" class="btn btn-info pull-right" id='form_submit_btn' onClick="submitToken()" >{{ Lang::get('COMMON.SUBMIT') }}</button>
		</div>
	</div>
</div>
</body>