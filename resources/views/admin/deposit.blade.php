<!DOCTYPE html>
<html lang="en">
<head>
    <title id='Description'></title>
    <link rel="stylesheet" href="{{url()}}/admin/jqwidgets/styles/jqx.base.css" type="text/css" />
    <link rel="stylesheet" href="{{url()}}/admin/jqwidgets/styles/jqx.bootstrap.css" type="text/css" />
    <script type="text/javascript" src="{{url()}}/admin/jqwidgets/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="{{url()}}/admin/jqwidgets/jqx-all.js"></script>
    <style>
        .fl{
                float: left;
        }
        .label-margin{
                padding-top:5px;
                margin-left: 10px;
                margin-right: 5px;
        }
        .clear{
                clear: both;
        }
        .formLabelLeft{
                width: 150px;
                padding-right: 10px;
                vertical-align: top;
                font-weight: bold;
        }
    </style>
</head>
<body class='default'>  
<form>
	 <table width="100%">
		<tbody>
			<tr>
				<td width="50%" valign="top">
                    <div id='left_box'>
						<div>Deposit</div>
						<div style="padding:10px 0 10px 10px">
							<table class="formTable">
								<tr>
									<td class="formLabelLeft">{{Lang::get('COMMON.ID')}}</td>
									<td>{{$clgObj->getTransactionId()}}</td>
								</tr>
								<tr>
									<td class="formLabelLeft">{{Lang::get('COMMON.ACCOUNT')}}</td>
									<td>{{$accObj->nickname}}</td>
								</tr>
								<tr>
									<td class="formLabelLeft">{{Lang::get('COMMON.NAME')}}</td>
									<td>{{$clgObj->accname}}</td>
								</tr>
								<tr>
									<td class="formLabelLeft">{{Lang::get('COMMON.IPADDRESS')}}</td>
									<td>{{$ipurl}} [{{$location['country']}}] - {{$location['city']}}</td>
								</tr>
								<tr>
									<td class="formLabelLeft">{{Lang::get('COMMON.TIME')}}</td>
									<td>{{$clgObj->created}}</td>
								</tr>
								<tr>
									<td class="formLabelLeft">{{Lang::get('COMMON.AMOUNT')}}</td>
									<td><span style="font-weight:bold;color:navy;">{{$amount}}</span></td>
								</tr>
								@if(!$isbonus)
								<tr>
									<td class="formLabelLeft">{{Lang::get('COMMON.PAYMENTMETHOD')}}</td>
									<td><span style="font-weight:bold;"><?php echo htmlspecialchars_decode($paymentMethod); ?></span></td>
								</tr>
								<tr>
									<td class="formLabelLeft">{{Lang::get('COMMON.REFNO')}}</td>
									<td><span style="font-weight:bold;">{{$refno}}</span></td>
								</tr>
								<tr>
									<td class="formLabelLeft">{{Lang::get('COMMON.CHANNEL')}}</td>
									<td><span style="font-weight:bold;"><?php echo htmlspecialchars_decode($info)?></span></td>
								</tr>
								@endif
								<tr>
									<td class="formLabelLeft">{{Lang::get('COMMON.STATUS')}}</td>
									<td><span style="font-weight:bold;">{{$clgObj->getStatusText()}}</span></td>
								</tr>
								<tr>
									<td class="formLabelLeft">{{Lang::get('COMMON.REMARKCODE')}}</td>
									<td><span style="font-weight:bold;">{{$clgObj->remarkcode}}</span></td>
								</tr>
								<tr>
									<td></td>
									<td>@if( $image )<a href="{{$image}}" target="_blank">Bank Slip</a>@endif</td>
								</tr>
								@if($promoObj != '')
								<tr>
									<td class="formLabelLeft">{{Lang::get('COMMON.CAMPAIGN')}}</td>
									<td>{{$promoObj->name}}</td>
								</tr>		
								<tr>
									<td class="formLabelLeft">{{Lang::get('COMMON.BONUS')}}</td>
									<td>{{$promoObj->getPromoDetail()}}</td>
								</tr>	
								<tr>
									<td class="formLabelLeft">{{Lang::get('COMMON.MINDEPOSIT')}}</td>
									<td>{{$promoObj->mindeposit}}</td>
								</tr>
								<tr>
									<td class="formLabelLeft">{{Lang::get('COMMON.MAXAMOUNT')}}</td>
									<td>{{$promoObj->maxamount}}</td>
								</tr>	
								<tr>
									<td class="formLabelLeft">{{Lang::get('COMMON.DEPOSIT')}} {{Lang::get('COMMON.TRANSACTIONID')}}</td>
									<td>{{$depositid}}</td>
								</tr>	
								<tr>
									<td class="formLabelLeft">{{Lang::get('COMMON.DEPOSIT')}}</td>
									<td>{{$depositamt}}</td>
								</tr>
								@endif
							</table>
						</div>
					</div>	
				</td>
				<td width="50%" valign="top">
					<div id='right_box'>
						<div>Other Data</div>
						<div style="padding:10px 0 10px 10px">
							<table class="formTable">
								<tr>
									<th>{{Lang::get('COMMON.FEE')}}</th>
									<td><input type="text" name="editfee" value="@if(isset($fees)){{$fees}}@endif" class="easyui-validatebox" @if(isset($disablefee)) disabled @endif >&nbsp;
									</td>
								</tr>
								<tr>
									<th>{{Lang::get('COMMON.COMMENT')}}</th>
									<td><textarea name="editcomment" class="easyui-validatebox" style="resize:none;" rows="4">@if(isset($comments)){{$comments}}@endif</textarea></td>
								</tr>
			@if( !empty($bankAccounts) )					
								<tr>
									<th>{{Lang::get('COMMON.PAYMENTACCOUNT')}}</th>
									<td>
										<select name="bacid">
											@foreach($bankAccounts as $key => $value)
												<option value="{{$key}}" @if($clgObj->bacid == $key )selected @endif >{{$value}}</option>
											@endforeach
										</select>
									</td>
								</tr>
			@endif
								<tr>
									<th>{{Lang::get('COMMON.REJECTREASON')}}</th>
									<td>
										<select name="reason">
											@foreach($reasons as $key => $value)
												<option value="{{$value}}" @if($clgObj->rejreason == $value ) selected @endif>{{$value}}</option>
											@endforeach
										</select>
									</td>
								</tr>
								<tr>
									<th>{{Lang::get('COMMON.REMARK')}}</th>
									<td><textarea name="editremark" class="easyui-validatebox" style="resize:none;" rows="4">{{$clgObj->remarks}}</textarea></td>
								</tr>
			@if( $modifiedby != '' )
								<tr>
									<th>{{Lang::get('COMMON.CONFIRM2')}} {{Lang::get('COMMON.USER')}}</th>
									<td>{{$modifiedby}}</td>
								</tr>
								<tr>
									<th>{{Lang::get('COMMON.CONFIRM2')}} {{Lang::get('COMMON.DATE')}}</th>
									<td>{{$modified}}</td>
								</tr>
			@endif
								<tr>
									<td></td>
									@if( Config::get('setting.front_path') == 'ampm' || $curStatus =='pending')
									<td>
										<input id="btn_processed" style='cursor: pointer; {{ $curStatus =='processed' ? 'display: none;' : '' }}' onClick="submit_form('processed')" type="button" value="Processed" />
										<input id="btn_approve" style='cursor: pointer;' onClick="submit_form('approve')" type="button" value="Approve" />
										<input id="btn_reject" style='cursor: pointer;' onClick="submit_form('reject')" type="button" value="Reject" />
										<br>
										<br>
										<span class="code_error" style="color: #ff0000; font-weight: bold; display: none;"></span>
									</td>
									@else
										<td>
											<a href="javascript:void(0);" onclick="resendSMS(this);">{{ Lang::get('public.ResendConfirmationSMS') }}</a>
										</td>
									@endif
								</tr>
								
							</table>
				
						</div>
					</div>				
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<div id="audit"></div>
				</td>
			</tr>
		</tbody>
	</table>
</form>
<script type="text/javascript">
    $(document).ready(function () {
        // Create jqxExpander
        $("#left_box").jqxExpander({ width: '100%', theme:'bootstrap', toggleMode: 'dblclick'});
        $("#right_box").jqxExpander({ width: '100%', theme:'bootstrap', toggleMode: 'dblclick'});
		@if($curStatus =='pending')
		$("#btn_approve").jqxButton({ width: '80', template: "success"});
		$("#btn_reject").jqxButton({ width: '80', template: "danger"});
		$("#btn_processed").jqxButton({ width: '80', template: "primary"});
		@endif
		var source =
		{
			datatype: "json",
			datafields: [
							 { name: 'date' },
							 { name: 'type' },
							 { name: 'transid' },
							 { name: 'status' },
							 { name: 'credit' },
							 { name: 'debit' },
							 { name: 'limit' },
							 { name: 'acclimit' },
							 { name: 'turnover' },
							 { name: 'diff' },
							 { name: 'accdiff' },
							 { name: 'balance' }
					
			],
			url: "audit-data?id={{$sid}}",
				data: {
					limit: 25,
					start: 0
				}
		};
		
		var dataAdapter = new $.jqx.dataAdapter(source, {
                downloadComplete: function (data, status, xhr) { },
                loadComplete: function (data) { },
                loadError: function (xhr, status, error) { }
        });
		
		
		$("#audit").jqxGrid(
		{
			width: '100%',
			source: dataAdapter,                
			pageable: true,
			autoheight: true,
			altrows: true,
			enabletooltips: true,
			theme: 'bootstrap',
			columns: [
			  { text: 'Date',  		 	  datafield: 'date' , 	 	cellsalign: 'center',align: 'center' ,  width: 210 },
			  { text: 'Type',  		 	  datafield: 'type' ,	 	cellsalign: 'center',align: 'center' ,  width: 110 },
			  { text: 'ID',    		 	  datafield: 'transid' , 	 	cellsalign: 'center',align: 'center' ,  width: 110 },
			  { text: 'Status',		 	  datafield: 'status' ,  	cellsalign: 'center',align: 'center' ,  width: 110 },
			  { text: 'Credit',		 	  datafield: 'credit' ,  	cellsalign: 'center',align: 'center' ,  width: 110 },
			  { text: 'Debit', 		 	  datafield: 'debit' ,   	cellsalign: 'center',align: 'center' ,  width: 110 },
			  { text: 'Limit', 		 	  datafield: 'limit' ,   	cellsalign: 'center',align: 'center' ,  width: 110 },
			  { text: 'Acc. Limit',  	  datafield: 'acclimit' ,  cellsalign: 'center',align: 'center' ,  width: 110 },
			  { text: 'Turnover',    	  datafield: 'turnover' ,   cellsalign: 'center',align: 'center' ,  width: 110 },
			  { text: 'Differences', 	  datafield: 'diff' , 		cellsalign: 'center',align: 'center' ,  width: 110 },
			  { text: 'Acc. Differences', datafield: 'accdiff' ,   cellsalign: 'center',align: 'center' ,  width: 110 },
			  { text: 'Balance', 		  datafield: 'balance' ,    cellsalign: 'center',align: 'center' ,  width: 110 }
			]
		}); 
		
    });
	
	function submit_form(type)
	{
		var r = confirm("{{Lang::get('COMMON.CONFIRM')}}?");
		if (r == true) {
			  	$.ajax({
					type: "POST",
					url: "deposit-edit",
					data: $( "form" ).serialize()+'&_token={{ csrf_token() }}&actionref='+type+'&id={{$sid}}',
				}).done(function( json ) {
						 obj = JSON.parse(json);
						 $('.error_msg').html('');
						 $.each(obj, function(i, item) {
							if( '{{Lang::get('COMMON.SUCESSFUL')}}' == item ){
								alert('{{Lang::get('COMMON.SUCESSFUL')}}');
								window.parent.closeTab();
							}else if( item == 'processed'){
								
								alert('Status has been updated!');
                                $("#btn_processed").hide();
							}else{
								$('.'+i+'_error').html(item);

								if (i == "code") {
								    $(".code_error").show();
								}
							}
						})
				});
		}
	}

	function resendSMS(obj) {

        $(obj).text("{{ Lang::get('public.Loading') }}...");

        $.ajax({
            type: "GET",
            url: "deposit-withdraw-resendsms",
            dataType: "json",
            data: {
                id: "{{ $clgObj->id }}"
			}
        }).done(function( json ) {
            alert(json.code);

            $(obj).text("{{ Lang::get('public.ResendConfirmationSMS') }}");
        });
	}
</script>
</body>
</html>
