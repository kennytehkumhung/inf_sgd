<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>{{Config::get('setting.website_name')}} | Admin</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Open Sans">
<link rel="stylesheet" type="text/css" href="js/jeasyui/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="js/jeasyui/themes/icon.css">
<link rel="stylesheet" type="text/css" href="css/custom.css">
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script type="text/javascript" src="js/jeasyui/jquery.easyui.min.js"></script>
<script type="text/javascript" src="js/general.js"></script>
<script> 
function toogleClick(target,s,c){
	$(".subMenu").hide(500);
    $("#"+target).slideToggle(500);
	$("#left li:nth-child("+s+") a").toggleClass(c);
}
function toogleSelect(target,c){
	$("#"+target).toggleClass(c);
}
function dateSelect(target,c){
	for(var i=1;i<=3;i++){
		$("#date li:nth-child("+i+") a").removeClass(c);
	}
	$("#date li:nth-child("+target+") a").addClass(c);
}

</script>
</head>

<body>
<div id="container">
	<div id="header">
    	<div id="header_top">
        	<div id="logo"><img src="http://files.1bet2u.com/logo.png" width="259" height="52" /></div>
        	<div id="login">
                <ul>
					<li><a href="index.php?slang=en" style="padding:0px;"><img src="images/en.png"></a></li>
					<li><a href="index.php?slang=cn" style="padding:0px;"><img src="images/cn.png"></a></li>
                    <li>|</li>
                    <li>Acc: <span class="blue">cs208</span></li>
                    <li>|</li>
                    <li><a href="#">Change Password</a></li>
                    <li>|</li>
                    <li><a href="index.php?action=logout&mdl=login">Sign Out</a></li>
                </ul>
            </div><!-- END LOGIN -->
        </div>
    	<div id="top_bar">
            <div id="timezone">
            	LOCAL: <span id="localClock" class="consoleTimer">2015-06-16 14:28</span> (GMT 8) / SYSTEM: <span id="systemClock" class="consoleTimer">2015-06-16 14:28</span> (GMT 8)
            </div>
            <div id="announcement">
            	...
            </div>
        </div><!-- END TOP BAR -->
    </div><!-- END HEADER -->
    <div id="content">
   	  <div id="left">
        	<ul>
				<li><a onclick="toogleClick('m_Transaction','1','selected');">Transaction<div class="arrow"></div></a>
					<ul class="subMenu" id="m_Transaction">
						<li><a href="#"><a href="#" onclick="addTab('Instant Transaction', 'http://localhost/royalewin/public/admin/enquiry')" id="sc1_1">Instant Transaction</a></a></li>
						<li><a href="#"><a href="#" onclick="addTab('Transaction Enquiry', 'http://localhost/royalewin/public/admin/enquiry')" id="sc1_2">Transaction Enquiry</a></a></li>
						<li><a href="#"><a href="#" onclick="addTab('Adjustment', 'index.php?action=show&mdl=adjustment')" id="sc1_3">Adjustment</a></a></li>
					</ul>
				</li>
				<li><a onclick="toogleClick('m_Member','1','selected');">Member<div class="arrow"></div></a>
					<ul class="subMenu" id="m_Member">
						<li><a href="#"><a href="#" onclick="addTab('Member Enquiry', 'http://localhost/royalewin/public/admin/member')" id="sc2_1">Member Enquiry</a></a></li>
					</ul>
				</li>
				<li><a onclick="toogleClick('m_Report','1','selected');">Report<div class="arrow"></div></a>
					<ul class="subMenu" id="m_Report">
						<li><a href="#"><a href="#" onclick="addTab('Summary', 'index.php?action=show&mdl=reporttotal')" id="sc3_1">Summary</a></a></li>
						<li><a href="#"><a href="#" onclick="addTab('Profit & Loss', 'http://localhost/royalewin/public/admin/profitloss')" id="sc3_2">Profit & Loss</a></a></li>
						<li><a href="#"><a href="#" onclick="addTab('Daily Summary', 'index.php?action=showdaily&mdl=report')" id="sc3_3">Daily Summary</a></a></li>
						<li><a href="#"><a href="#" onclick="addTab('Bank Summary', 'index.php?action=showsummary&mdl=bank')" id="sc3_4">Bank Summary</a></a></li>
						<li><a href="#"><a href="#" onclick="addTab('Deposit Report', 'index.php?action=deposit&mdl=report')" id="sc3_5">Deposit Report</a></a></li>
						<li><a href="#"><a href="#" onclick="addTab('Rebate Report', 'index.php?action=show&mdl=incentive')" id="sc3_6">Rebate Report</a></a></li>
						<li><a href="#"><a href="#" onclick="addTab('Member Report', 'index.php?action=membermonth&mdl=report')" id="sc3_7">Member Report</a></a></li>
						<li><a href="#"><a href="#" onclick="addTab('Promo Campaign', 'index.php?action=showreport&mdl=promocampaign')" id="sc3_8">Promo Campaign</a></a></li>
					</ul>
				</li>
							<li><a onclick="toogleClick('m_Agent','1','selected');">Agent<div class="arrow"></div></a>
					<ul class="subMenu" id="m_Agent">
																<li><a href="#"><a href="#" onclick="addTab('Agent List', 'index.php?action=show&mdl=affiliate')" id="sc4_1">Agent List</a></a></li>
															</ul>
				</li>
					<li><a onclick="toogleClick('m_Tool','1','selected');">Tool<div class="arrow"></div></a>
					<ul class="subMenu" id="m_Tool">
					<li><a href="#"><a href="#" onclick="addTab('Announcement', 'index.php?action=show&mdl=announcement')" id="sc5_1">Announcement</a></a></li>
					<li><a href="#"><a href="#" onclick="addTab('IP Look Up', 'index.php?action=showlog&mdl=tracker')" id="sc5_2">IP Look Up</a></a></li>
					<li><a href="#"><a href="#" onclick="addTab('Robot', 'index.php?action=show&mdl=robot')" id="sc5_3">Robot</a></a></li>
					<li><a href="#"><a href="#" onclick="addTab('Product Maintenance', 'http://localhost/royalewin/public/admin/product')" id="sc5_4">Product Maintenance</a></a></li>
					<li><a href="#"><a href="#" onclick="addTab('Content Management System', 'http://localhost/royalewin/public/admin/cms')" id="sc5_5">Content Management System</a></a></li>
															</ul>
				</li>
							<li><a onclick="toogleClick('m_Setting','1','selected');">Setting<div class="arrow"></div></a>
					<ul class="subMenu" id="m_Setting">
																<li><a href="#"><a href="#" onclick="addTab('Bank', 'index.php?action=show&mdl=bank')" id="sc6_1">Bank</a></a></li>
																					<li><a href="#"><a href="#" onclick="addTab('Bank Account', 'index.php?action=showacc&mdl=bank')" id="sc6_2">Bank Account</a></a></li>
																					<li><a href="#"><a href="#" onclick="addTab('Bonus', 'index.php?action=show&mdl=promocampaign')" id="sc6_3">Bonus</a></a></li>
																					<li><a href="#"><a href="#" onclick="addTab('Rebate', 'index.php?action=setting&mdl=incentive')" id="sc6_4">Rebate</a></a></li>
																					<li><a href="#"><a href="#" onclick="addTab('Tag', 'index.php?action=show&mdl=tag')" id="sc6_5">Tag</a></a></li>
																					<li><a href="#"><a href="#" onclick="addTab('Remark', 'index.php?action=show&mdl=remark')" id="sc6_6">Remark</a></a></li>
																					<li><a href="#"><a href="#" onclick="addTab('Reject Reason', 'index.php?action=show&mdl=rejectreason')" id="sc6_7">Reject Reason</a></a></li>
															</ul>
				</li>
							<li><a onclick="toogleClick('m_Admin','1','selected');">Admin<div class="arrow"></div></a>
					<ul class="subMenu" id="m_Admin">
																<li><a href="#"><a href="#" onclick="addTab('User', 'index.php?action=show&mdl=adminuser')" id="sc7_1">User</a></a></li>
																					<li><a href="#"><a href="#" onclick="addTab('Change Password', 'index.php?action=changepass&mdl=adminuser')" id="sc7_2">Change Password</a></a></li>
															</ul>
				</li>
							<li><a onclick="toogleClick('m_System','1','selected');">System<div class="arrow"></div></a>
					<ul class="subMenu" id="m_System">
																<li><a href="#"><a href="#" onclick="addTab('Configuration', 'index.php?action=show&mdl=config')" id="sc8_1">Configuration</a></a></li>
																					<li><a href="#"><a href="#" onclick="addTab('Role', 'index.php?action=show&mdl=adminrole')" id="sc8_2">Role</a></a></li>
																					<li><a href="#"><a href="#" onclick="addTab('Admin Log', 'index.php?action=show&mdl=adminlog')" id="sc8_3">Admin Log</a></a></li>
																					<li><a href="#"><a href="#" onclick="addTab('SQL Error', 'index.php?action=showerror&mdl=adminlog')" id="sc8_4">SQL Error</a></a></li>
																					<li><a href="#"><a href="#" onclick="addTab('Allow IP', 'index.php?action=show&mdl=allowip')" id="sc8_5">Allow IP</a></a></li>
															</ul>
				</li>
			            </ul>
        </div>
      <div id="right" class="easyui-tabs" style="height:auto;">
        </div><!-- END RIGHT -->
    </div><!-- END CONTENT -->
    <div id="footer">
    	© 2013 Analytics Home | Terms of Service | Privacy Policy | Contact us | Send Feedback
    </div><!-- END FOOTER -->
</div><!-- END CONTAINER -->

<script>
//set content height on windows load or resize
$(window).load(function() {
  var height = $(document).height() - $('#header').height() - $('#footer').height();
  $('#right').tabs({
	height: height
  });
});

$( window ).resize(function() {
  var height = $(document).height() - $('#header').height() - $('#footer').height();
  $('#right').tabs({
	height: height
  });
});

function addTab(title, url){
    if ($('#right').tabs('exists', title)){
        $('#right').tabs('select', title);
		var tab = $('#right').tabs('getSelected');
		tab.panel('refresh');
    } else {
        var content = '<iframe scrolling="auto" frameborder="0"  src="'+url+'" style="width:100%;height:100%;"></iframe>';
        $('#right').tabs('add',{
            title:title,
            content:content,
            closable:true
        });
    }
	window.focus();
}
function closeTab(){
	var tab = $('#right').tabs('getSelected');
	var index = $('#right').tabs('getTabIndex',tab);
	$('#right').tabs('close', index);
}

function popOut(URL, id) {
	day = new Date();
	eval("page" + id + " = window.open(URL, '" + id + "', 'toolbar=0,scrollbars=0,location=0,statusbar=0,menubar=0,resizable=1,width=900,height=700,left = 128,top = 128');");
}
</script>
</body>
</html>

