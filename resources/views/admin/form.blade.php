
	<div title="{{$form.title}}" style="padding:10px">
		@if( $mssg)
		<div class="info" style="margin-bottom:10px">
			<div class="tip icon-tip">&nbsp;</div>
			<div>{{$mssg}}</div>
		</div>
		@endif
		<form id="cboForm" method="post" action="index.php" target="frameSubmit" @if( $form.multipart)enctype="multipart/form-data"@endif>
			<div class="easyui-panel" title="{{$form.title}}" style="width:auto">
				<div style="padding:10px 0 10px 10px">
					<table>
{{foreach from=$form.inputs item=input name=input}}

@if( $input.type != 'hidden')
{{assign var=disabled value=''}}
@if( isset($input.params.disabled) && $input.params.disabled)
{{assign var=disabled value='disabled'}}
@endif

{{assign var=style value=''}}
@if( isset($input.params.style) && $input.params.style)
{{assign var='style' value="style='`$input.params.style`'"}}
@endif

						<tr>
							<td class="formLabelLeft">{{$input.title}}</td>
							<td>
@if( $input.type == 'text'}}
								<input type="text" name="{{$input.name}}" class="easyui-validatebox" value="{{$input.value}}" @if( isset($input.params.disabled) && $input.params.disabled}}disabled@endif {{$style}}>
{{elseif $input.type == 'password'}}
								<input type="password" name="{{$input.name}}" class="easyui-validatebox">
{{elseif $input.type == 'textarea'}}
								<textarea class="easyui-validatebox" rows="5" cols="30" name="{{$input.name}}">{{$input.value}}</textarea>
{{elseif $input.type == 'checkbox_single'}}
								<input type="checkbox" name="{{$input.name}}" class="easyui-validatebox" value="1" @if( $input.value}}checked@endif>
{{elseif $input.type == 'checkbox'}}
								{{html_checkboxes name=$input.name options=$input.params.options selected=$input.params.values separator='&nbsp;'}}
{{elseif $input.type == 'radio'}}
								{{html_radios name=$input.name options=$input.params.options selected=$input.value separator='&nbsp;'}}
{{elseif $input.type == 'select'}}
@if( isset($input.params.onchange)}}
{{assign var=onchange value="`$input.params.onchange`"}}
{{else}}
{{assign var=onchange value=''}}
@endif
								{{html_options name=$input.name options=$input.params.options selected=$input.value class="easyui-combobox" disabled=$disabled strict=true}}
{{elseif $input.type == 'date'}}
								<input type="text" name="{{$input.name}}" class="easyui-datebox" value="{{$input.value}}" data-options="formatter:formatDate,parser:parseDate">
{{elseif $input.type == 'datetime'}}
								<input type="text" name="{{$input.name}}" class="easyui-datetimebox" value="{{$input.value}}" data-options="formatter:formatDateTime,parser:parseDateTime">
{{elseif $input.type == 'daterange'}}
	@if( isset($input.params.time) && $input.params.time}}
								<input type="text" name="{{$input.name}}['sfrom']" class="easyui-datetimebox" value="{{$input.params.from}}" data-options="formatter:formatDateTime,parser:parseDateTime"> ~ <input type="text" name="{{$input.name}}['sto']" class="easyui-datetimebox" value="{{$input.params.to}}" data-options="formatter:formatDateTime,parser:parseDateTime">
	{{else}}
								<input type="text" name="{{$input.name}}['sfrom']" class="easyui-datebox" value="{{$input.params.from}}" data-options="formatter:formatDate,parser:parseDate"> ~ <input type="text" name="{{$input.name}}['sto']" class="easyui-datebox" value="{{$input.params.to}}" data-options="formatter:formatDate,parser:parseDate">
	@endif
{{elseif $input.type == 'showonly'}}
								{{$input.value}}
{{elseif $input.type == 'file'}}
								<input type="file" name="{{$input.name}}" @if( isset($input.params.accept)}}accept="{{$input.params.accept}}"@endif> @if( isset($input.params.view) && $input.params.view && $input.value}}[ <a href="{{$input.value}}" target="CBO_VIEW">{{$spbLang.CBO_GUI_VIEW}}</a> ]@endif
{{elseif $input.type == 'html'}}
								<textarea name="{{$input.name}}" class="html_editor">{{$input.value}}</textarea>
{{elseif $input.type == 'swapbox'}}
{{assign var=name value=$input.name}} 
{{assign var=selection_id value=1000000|rand:5999999}} 
{{assign var=selected_id value=6000000|rand:9999999}} 
								<table cellpadding="1" cellspacing="0" border="0">
								<tr><td valign="top">&nbsp;&nbsp;Available<br>
								{{html_options size="7" name="cbo_$selection_id" options=$input.params.options style="width:228px" ondblclick="swpAddSwapValue(this, 'cbo_$selected_id', '$name', 0)" id="cbo_$selection_id"}}
								</td>
								<td valign="middle"><div onclick="swpAddSwapValue(document.getElementById('cbo_{{$selection_id}}'), 'cbo_{{$selected_id}}', '{{$name}}', 0)" title="Add" style="position: relative"><img src="rabbit/images/right.png"></div><br>
								<div onclick="swpRemoveSwapValue(document.getElementById('cbo_{{$selected_id}}'), 'cbo_{{$selected_id}}', '{{$name}}', 0)" title="Remove" style="position: relative"><img src="rabbit/images/left.png"></div><div style="position: relative"><img alt="" src="rabbit/images/spacer.gif" border=0></div></td><td valign="top">&nbsp;&nbsp;Current<br>
								{{html_options size="7" name="cbo_$selected_id" options=$input.params.values style="width:228px" ondblclick="swpRemoveSwapValue(this, 'cbo_$selected_id', '$name',0)" id="cbo_$selected_id"}}
								</td>
								<td valign="middle"></td>
							</tr>
							</table>
							<input type="hidden" name="{{$input.name}}" value="{{$input.value}}" id="{{$input.name}}">
{{elseif $input.type == 'color'}}
							<select name="{{$input.name}}" id="{{$input.name}}" style="background-color:{{$input.value}}" onChange="setTagcolor()" >
							{{foreach from=$input.params.options item=tcolor}}
							<option style="background-color:{{$tcolor}}" value="{{$tcolor}}" @if( $tcolor == $input.value}} selected="selected" @endif>&nbsp;&nbsp;&nbsp;&nbsp;</option>
							{{/foreach}}
							</select>
@endif				
							</td>
						</tr>
@endif	
{{/foreach}}
					</table>
				</div>
			</div>
{{foreach from=$form.inputs item=input name=input}}
@if( $input.type == 'hidden'}}	
		<input type="hidden" name="{{$input.name}}" value="{{$input.value}}">
@endif	
{{/foreach}}
		@if( $form.hiddenParams}}
		{{$form.hiddenParams}}
		@endif
		</form>
		
		<div style="text-align:center;padding:5px">
			<a href="javascript:void(0)" class="easyui-linkbutton" onclick="submitForm()">{{$spbLang.CBO_GUI_SUBMIT}}</a>
			<a href="javascript:void(0)" class="easyui-linkbutton" onclick="parent.closeTab();">{{$spbLang.CBO_GUI_CLOSE}}</a>
		</div>
	</div>

<script>
	function setTagcolor() {
		var clr = document.getElementById('scolor');
		clr.style.backgroundColor = clr.value;
	}
	$('.html_editor').jqte();
</script>
<iframe id="frameSubmit" name="frameSubmit" src="rabbit/js/index.html" border="0" marginwidth="0" marginheight="0" hspace="0" vspace="0" frameborder="0" scrolling="yes" class="appframe" style="width: 100%; height:0px; overflow:auto; position:relative; z-index:0; visibility:visible; filter:alpha(opacity=50); -moz-opacity:.50; opacity:.50;"></iframe>
