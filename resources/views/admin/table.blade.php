
<!DOCTYPE html>
<html lang="en">
<head>
    <title id='Description'>{{Config::get('setting.website_name')}} | Admin
    </title>
    
    <script type="text/javascript" src="{{url()}}/admin/jqwidgets/jquery-1.11.1.min.js"></script>
	

  <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
   <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">


<script type="text/javascript">
	$(document).ready(function(){
		$('#table_id').dataTable();
			branchData();
    });
/* 	function branchData(){
        $.ajax({
            type: "GET",
            url: "agentReport-data?filterscount=0&groupscount=0&pagenum=0&pagesize=20&recordstartindex=0&recordendindex=20&createdfrom=2017-09-20&createdto=2017-09-20&limit=20&start=0&aflt[sid]=0&_=1505877250230",
        }).done(function (data) {
			var rowsData = '';
			var obj= data;
			obj = JSON.parse(obj);
			$.each(obj.rows, function(i, item) {
				//alert("<td>"+item.affiliate+"</td>");
			rowsData+="<tr><td>"+item.affiliate+"</td><td>"+item.currency+"</td><td>"+item.deposit+"</td><td>"+item.withdrawal+"</td><td>"+item.bonus+"</td><td>"+item.incentive+"</td><td>"+item.turnover+"</td><td>"+item.profitloss+"</td><td bgcolor='red'>"+item.awinloss+"</td><td bgcolor='red'>"+item.apromo+"</td><td bgcolor='red'>"+item.atotal+"</td><td>"+item.bwinloss+"</td><td>"+item.bpromo+"</td><td>"+item.btotal+"</td><td bgcolor='red'>"+item.cwinloss+"</td><td bgcolor='red'>"+item.cpromo+"</td><td bgcolor='red'>"+item.ctotal+"</td></tr>";
				$("#getRowsDataData").html(rowsData);
			})
		});
	} */	
</script>
	<style>

	</style>
</head>
<body class='default'>
	<div class="col-sm-6">
		<table id="table_id" class="table-striped" style="font-size:12px;">
			<thead>
				<tr>
					<td colspan='8' bgcolor="yellow">{{$username}}</td>
					<td colspan='3' bgcolor="red">{{Lang::get('COMMON.DOWNLINE')}}</td>
					<td colspan='3' bgcolor="yellow">{{Lang::get('COMMON.DOWNLINE')}}</td>
					<td colspan='3' bgcolor="red">{{Lang::get('COMMON.UPLINE')}}</td>
				</tr>
				<tr>
					<td width='100' align='center'>{{Lang::get('COMMON.AFFILIATE')}}</td>
					<td width='60' align='center'>{{Lang::get('COMMON.CURRENCY')}}</td>
					<td width='90' align='center'>{{Lang::get('COMMON.DEPOSIT')}}</td>
					<td width='90' align='center'>{{Lang::get('COMMON.WITHDRAWAL')}}</td>
					<td width='90' align='center'>{{Lang::get('COMMON.BONUS')}}</td>
					<td width='90' align='center'>{{Lang::get('COMMON.INCENTIVE')}}</td>
					<td width='60' align='center'>{{Lang::get('COMMON.TURNOVER')}}</td>
					<td width='90' align='center'>{{Lang::get('COMMON.PROFITLOSS')}}</td>
					
					<td width='90' align='center' bgcolor="red">{{Lang::get('COMMON.WINLOSE')}}</td>
					<td width='90' align='center' bgcolor="red">{{Lang::get('COMMON.PROMOTION')}}</td>
					<td width='90' align='center' bgcolor="red">{{Lang::get('COMMON.TOTAL')}}</td>
					
					<td width='90' align='center' bgcolor="yellow">{{Lang::get('COMMON.WINLOSE')}}</td>
					<td width='90' align='center' bgcolor="yellow">{{Lang::get('COMMON.PROMOTION')}}</td>
					<td width='90' align='center' bgcolor="yellow">{{Lang::get('COMMON.TOTAL')}}</td>
					
					<td width='90' align='center' bgcolor="red">{{Lang::get('COMMON.WINLOSE')}}</td>
					<td width='90' align='center' bgcolor="red">{{Lang::get('COMMON.PROMOTION')}}</td>
					<td width='90' align='center' bgcolor="red">{{Lang::get('COMMON.TOTAL')}}</td>
				</tr>
			</thead>
			<tbody id="getRowsDataData">
				
			</tbody>
			<tfoot id="">
				<tr>
					<td>{{Lang::get('COMMON.TOTAL')}}</td>
					<td></td>
					<td>0.00</td>
					<td>0.00</td>
					<td>0.00</td>
					<td>0.00</td>
					<td>0.00</td>
					<td>0.00</td>
					<td>0.00</td>
					<td>0.00</td>
					<td>0.00</td>
					<td>0.00</td>
					<td>0.00</td>
					<td>0.00</td>
					<td>0.00</td>
					<td>0.00</td>
			</tfoot>
		</table>   
	</div>
</body>
</html>
