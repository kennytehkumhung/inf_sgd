<!DOCTYPE html>
<html lang="en">
<head>
    <title id='Description'></title>
    <link rel="stylesheet" href="{{url()}}/admin/jqwidgets/styles/jqx.base.css" type="text/css" />
    <link rel="stylesheet" href="{{url()}}/admin/jqwidgets/styles/jqx.bootstrap.css" type="text/css" />
    <script type="text/javascript" src="{{url()}}/admin/jqwidgets/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="{{url()}}/admin/jqwidgets/jqx-all.js"></script>
	<link rel="stylesheet" href="{{url()}}/admin/css/bootstrap.min.css">
	<script src="{{url()}}/admin/js/bootstrap.min.js"></script>
    <style>
        .fl{
                float: left;
        }
        .label-margin{
                padding-top:5px;
                margin-left: 10px;
                margin-right: 5px;
        }
        .clear{
                clear: both;
        }
        .formLabelLeft{
                width: 150px;
                padding-right: 10px;
                vertical-align: top;
                font-weight: bold;
        }
		div.triangle {
			border-bottom: 10px solid;
			border-left: 9px solid transparent;
			border-right: 9px solid transparent;
			bottom: 11px;
			display: inline;
			font-size: 0;
			height: 0;
			line-height: 0;
			margin: 0 auto;
			position: relative;
			width: 0;
		}
		div.square {
			bottom: 5px;
			display: inline;
			font-size: 0;
			height: 0px;
			line-height: 0;
			margin: 0 auto;
			position: relative;
			width: 0px;
		}
    </style>
</head>
<body class='default'>  
<form>
  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">{{ Lang::get('COMMON.EDIT') }}</h4>
        </div>
        <div class="modal-body" style="height:170px;">
			<div class="container col-md-12">
				<div class="row">
					<div class="col-md-6">{{ Lang::get('public.Bank') }}:</div>
					<div class="col-md-6">
						<select name="bankList" id="bankList" style="width:100%;">
						</select>
					</div>
				</div><br>
				<div class="row">	
					<div class="col-md-6">{{ Lang::get('public.BankAccount') }}:</div>
					<div class="col-md-6">
						<input type="text" name="bankAccount" id="bankAccount" style="width:100%;">
					</div>
				</div>
				<input type="text" name="bId" id="bId" hidden>
			</div>
        </div>
        <div class="modal-footer">
			<a href="javascript:void(0)" onClick="updateBankInfo()" class="btn btn-success" data-dismiss="modal" ondragstart="return false;" ondrop="return false;">{{ Lang::get('public.Submit') }}</a>
        </div>
      </div>
      
    </div>
  </div>
	 <table width="100%">
		<tbody>
			<tr>
				<td width="50%" valign="top">
                                        <div id='left_box1'>
                                                <div>{{ Lang::get('COMMON.ACCOUNTINFORMATION') }} - {{$acccode}}</div>
                                                <div style="padding:10px 0 10px 10px">
                                                    <table>
                                                        <tr>
                                                                <td class="formLabelLeft">{{ Lang::get('COMMON.NAME') }}</td>
																<td><input name="sfullname" id="sfullname" value="{{$fullname}}"></td>
                                                        </tr>
                                                        <tr>
                                                                <td class="formLabelLeft">{{ Lang::get('COMMON.IDPASSPORT') }}</td>
                                                                <td><input name="sidpassport" id="sidpassport"></td>
                                                        </tr> 
														<tr>
                                                                <td class="formLabelLeft">{{ Lang::get('public.SystemId') }}</td>
                                                                <td><input name="sysid" id="sysid" value="{{$id}}" disabled></td>
                                                        </tr>
                                                        @if (isset($scrid))
														<tr>
                                                                <td class="formLabelLeft">SCR ID</td>
                                                                <td><input name="scrid" id="scrid" value="{{$scrid}}" disabled></td>
                                                        </tr>
                                                        @endif
                                                        <tr>
                                                                <td class="formLabelLeft">{{ Lang::get('COMMON.CURRENCY') }}</td>
                                                                <td><div id='currency'></div></td>
                                                        </tr>
                                                        <tr>
                                                                <td class="formLabelLeft">{{ Lang::get('COMMON.DOB') }}</td>
                                                                <td><div id="dob"></div></td>
                                                        </tr>
                                                        <tr>
                                                                <td class="formLabelLeft">{{ Lang::get('COMMON.GENDER') }}</td>
                                                                    <td>
                                                                            <div id='male'>{{ Lang::get('COMMON.MALE') }}</div>
                                                                            <div id='female'>{{ Lang::get('COMMON.FEMALE') }}</div>
                                                                            <input type="button" id='jqxButton' value="{{$gender}}" hidden/>
                                                                    </td>
                                                        </tr>
                                                        <tr>
                                                                <td class="formLabelLeft">{{ Lang::get('COMMON.STATUS') }}</td>
                                                                <td>{{htmlspecialchars_decode($status)}}</td>
                                                        </tr>
                                                        <tr>
                                                                <td class="formLabelLeft">{{ Lang::get('COMMON.PASSWORD') }}</td>
                                                                <td><input name="spassword" id="spassword" type="password" value="********" disabled></td>
                                                        </tr>
                                                        <tr>
                                                                <td class="formLabelLeft">{{ Lang::get('COMMON.RETYPEPASSWORD') }}</td>
                                                                <td><input name="sconfirm" id="sconfirm" type="password" value="********" disabled></td>
                                                        </tr> 
														<tr>
                                                                <td class="formLabelLeft">{{ Lang::get('COMMON.CHANNEL') }}</td>
                                                                <td>{{$channel}}</td>
                                                        </tr>
                                                        <tr>
                                                                <td class="formLabelLeft">{{ Lang::get('COMMON.AGENT') }}</td>
                                                                <td>{{$agtcode}}</td>
                                                        </tr>  
														<tr>
                                                                <td class="formLabelLeft">{{ Lang::get('public.Zone') }}</td>
                                                                <td>{{$utm_medium}}</td>
                                                        </tr>   
														@if( Config::get('setting.front_path') == 'ampm' || Config::get('setting.front_path') == 'dadu')
														<tr>
                                                                <td class="formLabelLeft">{{ Lang::get('') }}Bank Profile</td>
                                                                <td>
																	<select name="bhdid">
																	@foreach( $bankprofileOptions as $key => $value )
																		@if( $key == $bhdid)
																			<option value="{{$key}}" selected>{{$value}}</option>
																		@else
																			<option value="{{$key}}">{{$value}}</option>
																		@endif
																	@endforeach
																	</select>
																
																</td>
                                                        </tr>
														@endif
														
														@if( $crccode == "TWD" )
														<tr>
                                                                <td class="formLabelLeft">{{ Lang::get('public.BankBook') }}</td>
																@if(isset($media->domain))
																	<td><a target="_blank" href="{{$media->domain}}/{{$media->path}}">{{ Lang::get('public.BankBook') }}</a></td>
																@endif
                                                        </tr> 
														@endif
                                                    </table>
                                                </div>
                                        </div>
                                        <br />
					@if ($showcontactinfo === true)
                    <div id='left_box2'>
						<div>{{ Lang::get('COMMON.CONTACTINFO') }}</div>
                                                <div style="padding:10px 0 10px 10px">
                                                        <table>
                                                                <tr>
                                                                        <td class="formLabelLeft">{{ Lang::get('COMMON.COUNTRY') }}</td>
                                                                        <td><div id='country'></div></td>
                                                                </tr>
                                                                <tr>
                                                                        <td class="formLabelLeft">{{ Lang::get('public.PhoneNumber') }}</td>
                                                                        <td>
                                                                            <input name="sphone" id="sphone" value="{{$telmobile}}">
                                                                        </td>
                                                                </tr>
                                                                <tr>
                                                                        <td class="formLabelLeft">{{ Lang::get('COMMON.EMAIL') }}</td>
                                                                        <td><input name="semail" id="semail" value="{{$email}}"></td>
                                                                </tr>
                                                        </table>
                                                </div>
					</div>
                                        <br />
                    @endif
					<div id='left_box3'>
						<div>{{ Lang::get('COMMON.OPERATEREMARK') }}</div>
                                                <div style="padding:5px">
                                                    <div id="opremark"></div>
                                                </div>
					</div>
                                        <br />
					<div id='left_box4'>
						<div>{{ Lang::get('COMMON.REMARK') }}</div>
                                                <div style="padding:10px 0 10px 10px">
                                                        <table style="width:100%">
                                                                <tr>
                                                                        <td class="formLabelLeft" style="width:100%"><textarea name="cremark" id="sremark" rows="4" style="width:100%;"></textarea></td>
                                                                </tr>
                                                        </table>
                                                </div>
					</div>                     
					<br />
					<div id='left_box5'>
						<div>{{ Lang::get('COMMON.SMS') }}</div>
                          <div style="padding:10px 0 10px 10px">
                                  <table style="width:100%">
                                          <tr>
                                                  <td class="formLabelLeft" style="width:100%"><textarea name="sms" id="sms" rows="4" style="width:100%;"></textarea></td>
                                          </tr>
                                  </table>

                              <div id="sms_submit" onClick="sms()" style="float:right; margin-right: 5px;">{{ Lang::get('COMMON.SUBMIT') }}</div>
                          </div>
					</div>
                    @if ($showcontactinfo === true && !empty($bo_whatsapp_telnum))
					<div id='left_box9'>
						<div>Whatsapp</div>
                          <div style="padding:10px 0 10px 10px">
                                  <table style="width:100%">
                                          <tr>
                                                  <td class="formLabelLeft" style="width:100%"><textarea name="bo_whatsapp" id="bo_whatsapp" rows="4" style="width:100%;"></textarea></td>
                                          </tr>
                                  </table>

                              <div id="bo_whatsapp_submit" onClick="boWhatsapp()" style="float:right; margin-right: 5px;">{{ Lang::get('COMMON.SUBMIT') }}</div>
                          </div>
					</div>
                    <br />
					@endif

					<div id='left_box6'>
						<div>{{ Lang::get('public.Inbox') }}</div>
                          <div style="padding:10px 0 10px 10px">
                                  <table style="width:100%">
                                          <tr>
										 
                                                  <td class="formLabelLeft" style="width:100%"><input type="text" name="title" placeholder="Title"></td>
                                          </tr>
										  <tr>
                                                  <td class="formLabelLeft" style="width:100%">
													<select name="inbox_crccode">
														@foreach( $currencyOptions as $currencyOption )
															<option @if($currencyOption == Session::get('admin_crccode')) selected @endif >{{$currencyOption}}</option>
														@endforeach
													</select>
												  </td>
                                          </tr> 
										  <tr>
                                                  <td class="formLabelLeft" style="width:100%"><textarea name="content" id="inbox" rows="4" style="width:100%;" 
												  placeholder="Content"></textarea></td>
                                          </tr>
                                  </table>
								<div id="inbox_submit" onClick="inbox()" style="float:right; margin-right: 5px;">{{ Lang::get('public.Send') }}</div>
								<div id="inboxToAll_submit" onClick="inboxToAll()" style="float:right; margin-right: 5px;">{{ Lang::get('public.SendToAll') }}</div>
                          </div>
					</div>
                    <br />
					<div id='left_box7'>
						<div>{{ Lang::get('public.InboxMessages') }}</div>
                                                <div style="padding:5px">
                                                    <div id="messages"></div>
                                                </div>
					</div>

                    <div id='left_box8'>
                        <div>{{ Lang::get('public.OperatorTag') }}</div>
                        <div style="padding:10px 0 10px 10px">
                            <table>

                                @foreach( $categoryTags as $tag )
                                    <tr>
                                        <td>&nbsp;<input type="checkbox" name="aflag[]" value="{{$tag['id']}}" {{$tag['checked']}}> </td>
                                        <td><div class="square" style="border: 6px solid {{$tag['color']}};"></div>&nbsp;<span>{{$tag['name']}}</span>
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
	
				</td>
				<td width="50%" valign="top">
					<div id='right_box1'>
						<div>{{ Lang::get('COMMON.ENQUIRY') }}</div>
                                                <div style="padding:10px 0 10px 10px">
                                                        <table style="width:100%;">
                                                                <tr>
                                                                        <td>{{ Lang::get('COMMON.OPERATORPASSWORD') }}</td>
                                                                        <td><input name="scspassword" id="scspassword" value="" type="password"></td>
                                                                        <td><div id="opsearch" onClick="verufy_password()" >{{ Lang::get('public.Submit') }}</div></td>
                                                                </tr>
                                                        </table>
                                                </div>
					</div>
                                        <br />
					<div id='right_box2'>
						<div>{{ Lang::get('COMMON.FINANCESUMMARY') }}</div>
                                                <div  style="padding:10px 0 10px 10px">
                                                        <table>
                                                                <tr>
                                                                        <td class="formLabelLeft" >{{ Lang::get('COMMON.MAINWALLET') }}</td>
                                                                        <td id="mainwallet"><span style="font-size:13px; font-weight:bold;">{{$crccode}} <img id="loading" src="{{url()}}/admin/images/ajax-loader.gif"/></span></td>
																		<td class="formLabelLeft" >{{ Lang::get('COMMON.AVAILABLE') }}</td>
																		<td class="formLabelLeft" >{{ Lang::get('COMMON.SUSPENDED') }}</td>
                                                                </tr>
@foreach( $products as $key => $product )
                                                                <tr>
                                                                        
                                                                        <td class="formLabelLeft" >{{$product}}</td>
                                                                        <td id="{{$key}}"><span style="font-size:13px; font-weight:bold;">{{$crccode}} <img id="loading" src="{{url()}}/admin/images/ajax-loader.gif"/></span></td>
																		<td class="formLabelLeft" ><input type="checkbox" {{ ($avaiable_products[$key] == true ? 'checked' : '') }} name="avaiable_products[]" value="{{$key}}"></td>
                                                                        <td class="formLabelLeft" ><input type="checkbox" {{ isset($suspended_products[$key]) && $suspended_products[$key] == true ? 'checked' : '' }} name="suspended_products[]" value="{{$key}}"></td>
                                                                </tr>
@endforeach                                                            
                                                                <tr>
                                                                        <td class="formLabelLeft" >{{ Lang::get('COMMON.TOTALBALANCE') }}</td>
                                                                        <td id='totalbalance'><span style="font-size:13px; font-weight:bold;">{{$crccode}} - </span></td>
                                                                </tr>
                                                            @if ($showcashledgerinfo === true)
                                                                <tr>
                                                                        <td><br /><b>{{ Lang::get('COMMON.DEPOSIT') }} / {{ Lang::get('public.Withdrawal') }}</b></td>
                                                                        <td></td>
                                                                </tr>
                                                                <tr>
                                                                        <td class="formLabelLeft">{{ Lang::get('COMMON.TOTALDEPOSIT') }}</td>
                                                                        <td>{{$crccode}} {{$totaldepositamount}}</td>
                                                                </tr>
                                                                <tr>
                                                                        <td class="formLabelLeft">{{ Lang::get('COMMON.TOTALWITHDRAWAL') }}</td>
                                                                        <td>{{$crccode}} {{$totalwithdrawalamount}}</td>
                                                                </tr>
                                                                <tr>
                                                                        <td class="formLabelLeft">{{ Lang::get('public.TotalBonus') }}</td>
                                                                        <td>{{$crccode}} {{$totalbonusamount}}</td>
                                                                </tr>
                                                                <tr>
                                                                        <td class="formLabelLeft">{{ Lang::get('public.TotalRebate') }}</td>
                                                                        <td>{{$crccode}} {{$totalrebateamount}}<br /><br /></td>
                                                                </tr>
                                                            @else
                                                                <tr>
                                                                    <td class="formLabelLeft">&nbsp;</td>
                                                                    <td><br /><br /></td>
                                                                </tr>
                                                            @endif
                                                                <tr>
                                                                        <td>{{ Lang::get('public.DailyWithdrawalLimit') }}</td>
                                                                        <td><div id='withdrawlimit'></div></td>
																		
                                                                </tr>
												 @if( Config::get('setting.front_path') != 'ampm' && Config::get('setting.front_path') != 'dadu') 
                                                                <tr>
                                                                        <td>{{ Lang::get('COMMON.MAXDAILYWITHDRAWAL') }}</td>
                                                                        <td><input name="imaxwithdrawamt" id="imaxwithdrawamt" value="{{$maxwithdrawamt}}"></td>
                                                                </tr>
                                                                <tr>
                                                                        <td>{{ Lang::get('COMMON.MAXDAILYDEPOSIT') }}</td>
                                                                        <td><input name="imaxdepositamt" id="imaxdepositamt" value="{{$maxdepositamt}}"></td>
                                                                </tr>
                                                @endif                
                                                        </table>
                                                </div>
					</div>
                                        <br />
					<div id='right_box3'>
						<div>{{ Lang::get('public.BankSettings') }}</div>
                                                <div style="padding:5px">
                                                    <div id="banksetting"></div>
                                                </div>
					</div>
                                        <br />
					<div id='right_box4'>
						<div>{{ Lang::get('COMMON.SECURITY') }}</div>
                                                <div style="padding:10px 0 10px 10px">
                                                        <table>
                                                                <tr>
                                                                        <td class="formLabelLeft">{{ Lang::get('COMMON.REGISTEREDON') }}</td>
                                                                        <td>{{$created}}</td>
                                                                </tr>
                                                                <tr>
                                                                        <td class="formLabelLeft">{{ Lang::get('COMMON.REGISTEREDIP') }}</td>
                                                                        <td>{{$createdip}}</td>
                                                                </tr>
                                                                <tr>
                                                                        <td class="formLabelLeft">{{ Lang::get('COMMON.LASTLOGINON') }}</td>
                                                                        <td>{{$lastlogin}}</td>
                                                                </tr>
                                                                <tr>
                                                                        <td class="formLabelLeft">{{ Lang::get('public.LastLoginIp') }}</td>
                                                                        <td>{{$lastloginip}}</td>
                                                                </tr>
                                                        </table>
                                                </div>
					</div>
                                        <br />
					<div id='right_box5'>
						<div>{{ Lang::get('public.CustomerTag') }}</div>
                                                <div style="padding:10px 0 10px 10px">
                                                        <table>
                                                    
																@foreach( $flags as $flag )
																		<tr>
																			<td>&nbsp;<input type="checkbox" name="aflag[]" value="{{$flag['id']}}" {{$flag['checked']}}> </td>
																			<td><div class="triangle" style="border-bottom-color:{{$flag['color']}};"></div>&nbsp; {{$flag['name']}}
																			</td>
																		</tr>
															    @endforeach
                                                        </table>
                                                </div>
					</div>
				</td>
			</tr>
		</tbody>
	</table>
	<input type="hidden" id="schangepassword" name="schangepassword" value="false">
	<input type="hidden" name="sid" value="{{$id}}">
</form>
        <br />
        <div style="text-align:center; padding:5px">
                <button id="submit" onClick="alert('Please key in your password to access!')">{{ Lang::get('public.Submit') }}</button>
        </div>
<script type="text/javascript">
$(document).ready(function () {
    // Create jqxExpander
bankLists();
    $("#left_box1").jqxExpander({ width: '100%', theme:'bootstrap', toggleMode: 'dblclick'});
	@if ($showcontactinfo === true)
		$("#left_box2").jqxExpander({ width: '100%', theme:'bootstrap', toggleMode: 'dblclick'});

		@if (!empty($bo_whatsapp_telnum))
            $("#left_box9").jqxExpander({ width: '100%', theme:'bootstrap', toggleMode: 'dblclick'});
        @endif
	@endif
    $("#left_box3").jqxExpander({ width: '100%', theme:'bootstrap', toggleMode: 'dblclick'});
    $("#left_box4").jqxExpander({ width: '100%', theme:'bootstrap', toggleMode: 'dblclick'});
    $("#left_box5").jqxExpander({ width: '100%', theme:'bootstrap', toggleMode: 'dblclick'});
    $("#left_box6").jqxExpander({ width: '100%', theme:'bootstrap', toggleMode: 'dblclick'});
    $("#left_box7").jqxExpander({ width: '100%', theme:'bootstrap', toggleMode: 'dblclick'});
    $("#left_box8").jqxExpander({ width: '100%', theme:'bootstrap', toggleMode: 'dblclick'});
    $("#right_box1").jqxExpander({ width: '100%', theme:'bootstrap', toggleMode: 'dblclick'});
    $("#right_box2").jqxExpander({ width: '100%', theme:'bootstrap', toggleMode: 'dblclick'});
    $("#right_box3").jqxExpander({ width: '100%', theme:'bootstrap', toggleMode: 'dblclick'});
    $("#right_box4").jqxExpander({ width: '100%', theme:'bootstrap', toggleMode: 'dblclick'});
    $("#right_box5").jqxExpander({ width: '100%', theme:'bootstrap', toggleMode: 'dblclick'});

    // Create jqxButton widgets.
    $("#opsearch").jqxButton({ template: "info" });
    $("#submit").jqxButton({ template: "success"});
    @if ($showcontactinfo === true)
        $("#sms_submit").jqxButton({ template: "success"});

        @if (!empty($bo_whatsapp_telnum))
            $("#bo_whatsapp_submit").jqxButton({ template: "success"});
        @endif
    @endif
    $("#inboxToAll_submit").jqxButton({ template: "success"});
    $("#inbox_submit").jqxButton({ template: "success"});

    // Create a jqxDropDownList
    var dropdownlist = ['0','1','2','3','4','5','6','7','8'];
    var crccode = ['{{$crccode}}'];
	
		$("#withdrawlimit").jqxDropDownList({ source: dropdownlist, selectedIndex: {{$withdrawlimit}}, width: '80', height: '21'});
	
    $("#currency").jqxDropDownList({ source: crccode, selectedIndex: 0, width: 'auto', height: '21'});
	@if ($showcontactinfo === true)
		$("#country").jqxDropDownList({ source: '', selectedIndex: 0, width: 'auto', height: '21'});
	@endif

    // create jqxRadioButton.
    var gender = $("#jqxButton").val();
    $("#male").jqxRadioButton({ groupName: 'genderGroup'});
    $("#female").jqxRadioButton({ groupName: 'genderGroup'});

    $("#"+gender).val(true);

    // create jqxDateTimeInput
    var date = '{{$dob}}';
    $("#dob").jqxDateTimeInput({ width: '120px' , height: '25px' , formatString: "yyyy-MM-dd" ,openDelay: 0,closeDelay: 0});
    $('#dob').jqxDateTimeInput('setDate', new Date(date));
	@if( $dob != '0000-00-00' )
		var t = "{{$dob}}".split(/[- :]/);
		$('#dob').jqxDateTimeInput('setDate', new Date(t[0], t[1]-1, t[2]));
	@endif
	
	var source_initial =
	{
		datatype: "json",
		datafields: [
										 { name: 'admin' },
										 { name: 'remark' },
										 { name: 'date' },
						
				
		],
		url: "customer-memo",
		beforeprocessing: function (data) {
            source_initial.totalrecords = data.total;
        },
		data: {
			id: {{$id}},
			limit: 20,
			start: 0
		}
	};

    var dataAdapter = new $.jqx.dataAdapter(source_initial, {
        downloadComplete: function (data, status, xhr) { },
        loadComplete: function (data) { },
        loadError: function (xhr, status, error) { }
    });

	$("#opremark").jqxGrid(
    {
        width: '100%',      
		source: dataAdapter,    		
        pageable: true,
        autoheight: true,
        sortable: true,
		pageable: true,
        altrows: true,
		theme:'bootstrap',
        enabletooltips: true,
        editable: false,
		pagesize: 5,
        columnsresize: true,
		pagesizeoptions: ['5', '10', '20', '50', '100'],
        selectionmode: 'multiplecellsadvanced',
		virtualmode: true,
		rendergridrows: function(obj)
		{
			  return obj.data;     
		},
        columns: [
		  	 { text: 'Admin', datafield: 'admin', width: '80px'  },
		  	 { text: 'Remark', datafield: 'remark'  },
		  	 { text: 'Date', datafield: 'date', width: '130px'},
        ]
    });
	
	var source_initial2 =
	{
		datatype: "json",
		datafields: [
										 { name: 'title' },
										 { name: 'content' },
										 { name: 'date' },
						
				
		],
		url: "inboxMessage",
		beforeprocessing: function (data) {
            source_initial2.totalrecords = data.total;
        },
		data: {
			id: {{$id}},
			limit: 20,
			start: 0
		}
	};

    var dataAdapter2 = new $.jqx.dataAdapter(source_initial2, {
        downloadComplete: function (data, status, xhr) { },
        loadComplete: function (data) { },
        loadError: function (xhr, status, error) { }
    });
	
	$("#messages").jqxGrid(
    {
        width: '100%',      
		source: dataAdapter2,    		
        pageable: true,
        autoheight: true,
        sortable: true,
		pageable: true,
        altrows: true,
		theme:'bootstrap',
        enabletooltips: true,
        editable: false,
		pagesize: 5,
        columnsresize: true,
		pagesizeoptions: ['5', '10', '20', '50', '100'],
        selectionmode: 'multiplecellsadvanced',
		virtualmode: true,
		rendergridrows: function(obj)
		{
			  return obj.data;     
		},
        columns: [
		  	 { text: 'Title'	, datafield: 'title', width: '80px'  },
		  	 { text: 'Content'	, datafield: 'content'  },
		  	 { text: 'Date'		, datafield: 'date', width: '130px'},
        ]
    });
			
			
			
			
			
	var source_initial =
	{
		datatype: "json",
		datafields: [
										 { name: 'bank' },
										 { name: 'account' },
										 { name: 'id' },
										 { name: 'bankid' },

							
						
				
		],
		url: "customer-bank",
		// updaterow: function (rowid, rowdata, commit) {

                    // $.ajax({
             
                        // url: 'customer-bank-update',
                        // data: {
							// bank: rowdata.bank,
							// account: rowdata.account,
							// sid: rowdata.id,
						// },
                        // success: function (data, status, xhr) {
                           // if(data == 1){
							   
							   // alert('{{ Lang::get('public.UpdateSuccess') }}')
						   // }
                        // }
                    
                    // });
        // },
		beforeprocessing: function (data) {
            source_initial.totalrecords = data.total;
        },
		data: {
			id: {{$id}},
			limit: 20,
			start: 0
		}
	};

    var dataAdapter = new $.jqx.dataAdapter(source_initial, {
        downloadComplete: function (data, status, xhr) { },
        loadComplete: function (data) { },
        loadError: function (xhr, status, error) { }
    });

	$("#banksetting").jqxGrid(
    {
        width: '100%',      
		source: dataAdapter,    		
        pageable: true,
        autoheight: true,
        sortable: true,
		pageable: true,
        altrows: true,
		theme:'bootstrap',
        enabletooltips: true,
        // editable: true,
		pagesize: 20,
		pagesizeoptions: ['20', '50', '100', '150', '200'],
        selectionmode: 'multiplecellsadvanced',
		virtualmode: true,
		rendergridrows: function(obj)
		{
			  return obj.data;     
		},
        columns: [
		  	 { text: '{{ Lang::get('public.Bank') }}', datafield: 'bank'  },
		  	 { text: '{{ Lang::get('public.Account') }}', datafield: 'account'  },
			 { text: '', dataField: 'button', width: '8%', columntype:'button', cellsrenderer: function () {
		                     return '{{ Lang::get('COMMON.EDIT') }}';
		                 }, buttonclick: function (row, event) {

		                     var button = $(event.currentTarget);
		                     var grid = button.parents("[id^=banksetting]");
		                     
		                     var rowData = grid.jqxGrid('getrowdata', row);
							 //console.log(rowData);
							 // alert(rowData.bank);
							 $('#bankList').val(rowData.bankid);
							 $('#bankAccount').val(rowData.account);
							 $('#bId').val(rowData.id);
							 
		                     $('#myModal').modal('show'); 
		                 }
		              }

        ]
    });


			
			
			
			

	


    //load wallet balance

@foreach( $products as $key => $product ) 
    loadWallet({{$key}});
@endforeach
});
var total = parseFloat('{{$mainwallet}}'.replace(',',''));
function loadWallet(product_id){
      $.ajax({
      type: "POST",
      url: "balance/"+product_id+"?rd=<?php echo rand ( 10000, 99999 ); ?>",
      data: {
          _token: "{{ csrf_token() }}",
	  sid: {{$id}}
      }
     }).done(function(data) {
	    var obj = JSON.parse(data);
	    var mainwallet='';
	    var str ='';
	    var totals = '';
	    if(obj['status'] == '{{Lang::get('COMMON.ACTIVE')}}'){
		$.each(obj['wallet'], function(key,value){
			
			var mainwalletamount = parseFloat(obj['mainwallet']);
			var value = parseFloat(value);
		    mainwallet += '<span style="font-size:13px; font-weight:bold;">{{$crccode}} '+addThousandsSeparator(mainwalletamount.toFixed(4))+'</span>';
		    $('#mainwallet').html(mainwallet);
			total += value;
		    str += '<span style="font-size:13px; font-weight:bold;">{{$crccode}} '+addThousandsSeparator(value.toFixed(2))+'</span>';
		    $('#'+obj['prdid']).html(str);

		
		    
			
	

		    totals += '<span style="font-size:13px; font-weight:bold;">{{$crccode}} '+addThousandsSeparator(total.toFixed(2))+'</span>';
		    $('#totalbalance').html(totals);
		});
	    }
	    else{
		str += '<span style="font-size:13px; color:red; font-weight:bold;">'+obj['status']+'</span>';
		$('#'+obj['prdid']).html(str);	    
	    }
     });
}

function addThousandsSeparator(input) {
    var output = input;
    if (parseFloat(input)) {
        input = new String(input); // so you can perform string operations
        var parts = input.split("."); // remove the decimal part
        parts[0] = parts[0].split("").reverse().join("").replace(/(\d{3})(?!$)/g, "$1,").split("").reverse().join("");
        output = parts.join(".");
    }

    return output;
}

function update_profile(){
	
	$.ajax({
		type: "POST",
		url: "{{action('Admin\CustomertabController@updateProfile')}}?"+$( "form" ).serialize(),
		data: {
			_token:   "{{ csrf_token() }}",
			
				iwithdrawlimit:   $("#withdrawlimit").jqxDropDownList('getSelectedIndex'),

            @if ($showcontactinfo === true)
    			rescountry:   $("#country").jqxDropDownList('getSelectedIndex'),
            @endif
			dob:   $('#dob').jqxDateTimeInput('getText')
		},
	}).done(function( json ) {
		alert('{{Lang::get('COMMON.SUCESSFUL')}}');
		location.reload();
	});
	
}

function verufy_password(){
	
	$.ajax({
		type: "POST",
		url: "{{action('Admin\CustomertabController@verifyPassword')}}?"+$( "form" ).serialize(),
		data: {
			_token:   "{{ csrf_token() }}"
		},
	}).done(function( result ) {
		if(result == 1){
			alert('Password match!');
			$('#spassword').prop("disabled", false); 
			$('#sconfirm').prop("disabled", false); 
			$('#schangepassword').val('true'); 
			$('#sconfirm').val(''); 
			$('#spassword').val(''); 
			$('#submit').attr( 'onClick' , 'update_profile()' );
		}else{
			alert('Password does not match!');
		}

	});
	
}

function sms(){
    if (confirm('{{ Lang::get('public.ConfirmSendSMS') }}')) {
        $.ajax({
            type: "POST",
            url: "{{action('Admin\CustomertabController@sms')}}?" + $("form").serialize(),
            data: {
                _token: "{{ csrf_token() }}"
            },
        }).done(function (result) {
            if (result == 1) {
                alert('{{ Lang::get('public.Success') }}');
            } else {
                alert('{{ Lang::get('public.Failed') }}');
            }

        });
    }
	
}

function boWhatsapp() {
    var memtelnum = "{{ (new \App\libraries\sms\SMS88())->formatTel($crccode, $telmobile) }}";
    var content = $("#bo_whatsapp").val();

    var win = window.open("https://api.whatsapp.com/send?phone="+memtelnum+"&text="+encodeURIComponent(content));
    win.focus();
}

function inbox(){
    if (confirm('{{ Lang::get('public.ConfirmSendInboxMessage') }}')) {
        $.ajax({
            type: "POST",
            url: "{{action('Admin\CustomertabController@inbox')}}?" + $("form").serialize(),
            data: {
                _token: "{{ csrf_token() }}"
            },
        }).done(function (result) {
            if (result == 1) {
                alert('{{ Lang::get('public.Success') }}');
            } else {
                alert('{{ Lang::get('public.Failed') }}');
            }

        });
    }	
}

function inboxToAll(){
    if (confirm('{{ Lang::get('public.ConfirmSendInboxMessageToAllMember') }}')) {
        $.ajax({
            type: "POST",
            url: "{{action('Admin\CustomertabController@sendToAll')}}?" + $("form").serialize(),
            data: {
                _token: "{{ csrf_token() }}"
            },
        }).done(function (result) {
            if (result == 1) {
                alert('{{ Lang::get('public.Success') }}');
            } else {
                alert('{{ Lang::get('public.Failed') }}');
            }

        });
    }
	
}
function bankLists(){
        $.ajax({
            type: "GET",
            url: "{{action('Admin\CustomertabController@showBankList')}}",
            data: {
                _token: "{{ csrf_token() }}"
            },
        }).done(function (data) {
			var optionData = '';
			var obj= data;
			obj = JSON.parse(obj);
			$.each(obj, function(i, item) {
				// alert(i+" "+item);
				$('#bankList').append($('<option>', {
					value: i,
					text : item
				}));
			})
		});
	}	
	
function updateBankInfo(){
    if (confirm('{{ Lang::get('public.ConfirmEditBankInfo') }}')) {
        $.ajax({
            type: "POST",
            url: "{{action('Admin\CustomertabController@updateBankInfo')}}?" + $("form").serialize(),
            data: {
				// bankList: bankList,
				// bankAccount: bankAccount,
				// bId: bId,
                _token: "{{ csrf_token() }}"
            },
        }).done(function (result) {
			// alert(result);
            if (result == 1) {
                alert('{{ Lang::get('public.Success') }}');
				$("#banksetting").jqxGrid('updatebounddata');
            } else {
                alert('{{ Lang::get('public.Failed') }}');
            }

        });
    }
	
}
</script>
</body>
</html>
