<!DOCTYPE html>
<html lang="en">
<head>
    <title id='Description'>
    </title>
	<head>
    <link rel="stylesheet" href="{{ URL('/') }}/admin/lte/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ URL('/') }}/admin/lte/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{ URL('/') }}/admin/lte/dist/css/skins/_all-skins.min.css">
	<link rel="stylesheet" href="{{url()}}/admin/jqwidgets/styles/jqx.base.css" type="text/css" />
    <link rel="stylesheet" href="{{url()}}/admin/jqwidgets/styles/jqx.bootstrap.css" type="text/css" />
    <script type="text/javascript" src="{{url()}}/admin/jqwidgets/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="{{url()}}/admin/jqwidgets/jqx-all.js"></script>
    <script type="text/javascript" src="{{url()}}/admin/js/general.js"></script>
		<script src="{{url()}}/gsc/resources/js/distpicker.data.js"></script>
	  <script src="{{url()}}/gsc/resources/js/distpicker.js"></script>
	  <script src="{{url()}}/gsc/resources/js/main.js"></script>
	 <script type="text/javascript">
        $(document).ready(function () {
			
			Start_Time = "21:30:00"; // Initial value retrieved from the grid.
			sTime = ConvertToDate(Start_Time); 

			$("#form_submit_btn").jqxButton({ width: '150', template: "success"});
			$("#form_submit_btn").on('click', function () {
                   $("#events").find('span').remove();
                   $("#events").append('<span>Submit Button Clicked</span>');
             });
			//$("#form_wapper").jqxExpander({ width: '100%', theme:'bootstrap', toggleMode: 'dblclick'});
			@foreach( $form['inputs'] as $input )
				@if($input['type'] == 'date')	
					var t = "{{$input['value']}}".split(/[- :]/);
					$("#{{$input['name']}}").jqxDateTimeInput({ width: '120px' , height: '25px' , formatString: "yyyy-MM-dd" ,openDelay: 0,closeDelay: 0});
					$('#{{$input['name']}}').jqxDateTimeInput('setDate', new Date(t[0], t[1]-1, t[2]));
				@endif
				@if($input['type'] == 'datetime')
					var t = "{{$input['value']}}".split(/[- :]/);
					$("#{{$input['name']}}").jqxDateTimeInput({  width: '220px' , height: '25px' , formatString: "yyyy-MM-dd HH:mm:ss" ,openDelay: 0,closeDelay: 0,showTimeButton: true});
					 $('#{{$input['name']}}').jqxDateTimeInput('setDate', new Date(t[0], t[1]-1, t[2], t[3], t[4], t[5]));
				@endif
				@if($input['type'] == 'html')
					$('#{{$input['name']}}').jqxEditor({
						height: "300px",
						width: '1000px'
					});
				@endif
			@endforeach	
			
			//@if(Session::has('affiliate_province') && Session::get('affiliate_province') != '' )
				//$('#province1').val('{{Session::get('affiliate_province')}}').trigger('change');
				//$('#province1').prop('disabled',true);
			//@endif
			

        });
		
		
		
		
		
		
		function ConvertToDate(stringTime)
		{
		var date = new Date();
		hours = stringTime.substr(0,2);
		minutes = stringTime.substr(3,2);
		date.setHours(parseInt(hours));
		date.setMinutes(parseInt(minutes));

		return date;
		}

		function submit_form(){
			
				var form_data = new FormData();                  
				var data = $('form').serializeArray();
				var obj = {};
				for (var i = 0, l = data.length; i < l; i++) {
					form_data.append(data[i].name, data[i].value);
				}
				@foreach( $form['inputs'] as $input )
					@if($input['type'] == 'datetime' || $input['type'] == 'date')
						form_data.append('{{$input['name']}}', $('#{{$input['name']}}').jqxDateTimeInput('getText'));
					@elseif($input['type'] == 'file')
						var file_data = $("#{{$input['name']}}").prop("files")[0];  
						form_data.append("{{$input['name']}}", file_data);
					@endif
				@endforeach	
				
				form_data.append('_token', '{{csrf_token()}}');

				$.ajax({
					type: "POST",
					url: "{{$module}}-do-add-edit?",
							dataType: 'script',
							cache: false,
							contentType: false,
							processData: false,
							data: form_data,         
							type: 'post',
							complete: function(json){

								 obj = JSON.parse(json.responseText);
								 $('.error_msg').html('');
								 $.each(obj, function(i, item) {
									if( '{{Lang::get('COMMON.SUCESSFUL')}}' == item ){
										alert('{{Lang::get('COMMON.SUCESSFUL')}}');
										window.parent.closeTab();
						
									}else{
										$('.'+i+'_error').html(item);
									}
								})
							
							},
								
				});

		}
		
		
		
		
		
    </script>
	
	</head>
	<body class='default'>
<br>
<form id="cboForm" method="post" action="" target="frameSubmit" @if($form['multipart'])enctype="multipart/form-data"@endif>
<div id="form_wapper" >

	<div class="col-md-10">
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">{{$form['title']}}</h3>
            </div>
            <form class="form-horizontal">
              <div class="box-body">

				@if(Config::get('setting.opcode') == 'GSC')
					@if(isset($form['province']))
						<div id="distpicker5">
					@endif
				@endif
					@foreach( $form['inputs'] as $input )
						
					    
					    @if($input['type'] != 'hidden')
							<div class="form-group" style="margin-bottom:4px;">
							  <label for="inputEmail3" class="col-sm-3 control-label">{{$input['title']}}</label>

							  <div class="col-sm-5">
									@if($input['type'] == 'text')
										<input class="form-control" id="{{$input['name']}}" type="text" name="{{$input['name']}}" value="{{$input['value']}}" @if( isset($input['params']['disabled']) && $input['params']['disabled'])disabled @endif>
									@elseif($input['type'] == 'password')
										<input type="password" name="{{$input['name']}}">
										@elseif($input['type'] == 'textarea')
										<textarea rows="5" cols="30" name="{{$input['name']}}">{{$input['value']}}</textarea>
									@elseif($input['type'] == 'checkbox_single')
										<input type="checkbox" name="{{$input['name']}}" value="1" @if($input['value'])checked @endif>
									@elseif($input['type'] == 'checkbox')	
										@foreach( $input['params']['options'] as $key => $value )
												<input type="checkbox" name="{{$input['name']}}[]" value="{{$key}}" @if(in_array($key,$input['params']['values']))checked @endif>{{$value}}
										@endforeach
										
									@elseif($input['type'] == 'radio')	
										@foreach( $input['params']['options'] as $key => $value )
											@if( isset($input['value']) && $input['value'] == $key)
												<input name="{{$input['name']}}" type="radio" value="{{$key}}" checked >{{$value}}
											@else
												<input name="{{$input['name']}}" type="radio" value="{{$key}}" >{{$value}}
											@endif
										@endforeach
									@elseif($input['type'] == 'select')	
										<select name="{{$input['name']}}">
											@foreach( $input['params']['options'] as $key => $value )
												<option  value="{{$key}}" <?php if($input['value'] == $key)echo 'selected'?>>{{$value}}</option>
											@endforeach
										</select>
                                                                        @elseif($input['type'] == 'color')
                                                                        	<select name="{{$input['name']}}">
											@foreach( $input['params']['options'] as $key => $value )
												<option  style="background-color:{{$value}}" value="{{$value}}" <?php if($input['value'] == $value)echo 'selected'?>>&nbsp;&nbsp;&nbsp;&nbsp;</option>
											@endforeach
										</select>
									@elseif($input['type'] == 'selectchina1')
									
										@if(!Session::has('affiliate_username'))
											<input name="province_select" value="province" type="radio" style="float:left;">
										@endif
										
										<select class="form-control" id="province1" name="province" style="float:left;width:202px;" > 
										
										</select>
										{{$input['value']}}
									@elseif($input['type'] == 'selectchina2')	
										<input name="province_select" value="city" type="radio" style="float:left;">
										<select class="form-control" id="city1" name="city" style="float:left;width:202px;" >
										</select>
										{{$input['value']}}
									@elseif($input['type'] == 'selectchina3')	
										<input name="province_select" value="district" type="radio" style="float:left;">
										<select class="form-control" id="district1" name="district" style="float:left;width:202px;" >
										</select>
										{{$input['value']}}	
									@elseif($input['type'] == 'date')	
										<div id="{{$input['name']}}"></div>
									@elseif($input['type'] == 'datetime')	
										<div id="{{$input['name']}}"></div>
									@elseif($input['type'] == 'daterange')	
									
									@elseif($input['type'] == 'showonly')	
										{{$input['value']}} <input type="hidden" name="{{$input['name']}}" value="{{$input['value']}}">
									@elseif($input['type'] == 'file')		
										<input class="upload" type="file" name="{{$input['name']}}" id="{{$input['name']}}">  [<a target="_blank" href="{{$input['value']}}">View</a>] 
									@elseif($input['type'] == 'hidden')
										<input type="hidden" name="{{$input['name']}}" value="{{$input['value']}}">
									@elseif($input['type'] == 'html')
										<textarea id="{{$input['name']}}" name="{{$input['name']}}" class="html_editor">{{$input['value']}}</textarea>
									@elseif($input['type'] == 'swapbox')
										<table cellpadding="1" cellspacing="0" border="0">
											<?php 
												$selection_id =  rand ( 1000000 , 5999999 );
												$selected_id  =  rand ( 6000000 , 9999999 );
											?>
											<tr>
												<td valign="top">&nbsp;&nbsp;Available<br>
													<select name="cbo_{{$selection_id}}" style="width:328px" ondblclick="swpAddSwapValue(this, 'cbo_{{$selected_id}}', '{{$input['name']}}', 0)" id="cbo_{{$selection_id}}" size="7"> 
														@foreach( $input['params']['options'] as $key => $value )
															<option id="cbo_{{$selection_id}}-{{$key}}" value="{{$key}}">{{$value}}</option>
														@endforeach
													</select>
												</td>
												<td valign="middle">
													<div onclick="swpAddSwapValue(document.getElementById('cbo_{{$selection_id}}'), 'cbo_{{$selected_id}}', '{{$input['name']}}', 0)" title="Add" style="position: relative"><img src="{{url()}}/admin/images/right.png">
													</div>
														<br>
													<div onclick="swpRemoveSwapValue(document.getElementById('cbo_{{$selected_id}}'), 'cbo_{{$selected_id}}', '{{$input['name']}}', 0)" title="Remove" style="position: relative"><img src="{{url()}}/admin/images/left.png"></div><div style="position: relative"><img alt="" src="{{url()}}/admin/images/spacer.gif" border=0>
													</div>
												</td>
												<td valign="top">&nbsp;&nbsp;Current<br>
													<select name="cbo_{{$selected_id}}" style="width:328px" ondblclick="swpRemoveSwapValue(this, 'cbo_{{$selected_id}}', '{{$input['name']}}', 0)" id="cbo_{{$selected_id}}" size="7"> 
														@if( is_array($input['params']['values']) )
															@foreach( $input['params']['values'] as $key => $value )
																<option id="cbo_{{$selected_id}}-{{$key}}" value="{{$key}}">{{$value}}</option>
															@endforeach
														@endif
													</select>
												</td>
												<td valign="middle"></td>
											</tr>
										</table>
										<input type="hidden" name="{{$input['name']}}" value="@if( is_array($input['params']['values']) ) @foreach( $input['params']['values'] as $key => $value ){{$key}},@endforeach @endif" id="{{$input['name']}}">
									<!--//////-->
									@elseif($input['type'] == 'psb_game')	
										<select name="psb" id="psb_game">
											<option value="" selected></option>
											<option value="mag">mag</option>
											<option value="pmp">pmp</option>
											<option value="tot">tot</option>
											<option value="sin">sin</option>
											<option value="sab">sab</option>
											<option value="san">san</option>
											<option value="sar">sar</option>
										</select>
									@elseif($input['type'] == 'psb_type')
										<select name="psb" id="psb_type">
											<option value="" selected></option>
											<option value="big">big</option>
											<option value="small">small</option>
											<option value="4a">4a</option>
											<option value="3a">3a</option>
											<option value="3c">3c</option>
											<option value="4sb">4sb</option>
											<option value="4sc">4sc</option>
											<option value="4sd">4sd</option>
											<option value="4se">4se</option>
											<option value="4c">4c</option>
											<option value="2a">2a</option>
											<option value="2c">2c</option>
										</select>
									
								
									@elseif($input['type'] == 'psb_bet')
										<input type="text" id="psb_bet" name="psb">
									<!--//////	-->
									{{$input['value']}}	
									@endif
							  </div>
					
							  <div class="col-sm-3 {{$input['name']}}_error error_msg" style="color:red;"></div>
							</div>
							<div style="clear:both;margin-bottom:10px;"></div>
							
						@else
							<input type="hidden" name="{{$input['name']}}" value="{{$input['value']}}">
						@endif
					@endforeach	
				@if(Config::get('setting.opcode') == 'GSC')
					@if(isset($form['province']))
						</div>
					@endif
				@endif
              </div>
          
              <div class="box-footer">
               <button class="btn btn-primary" type="button" id='form_submit_btn' onClick="submit_form()" >{{ Lang::get('COMMON.SUBMIT') }}</button>
              </div>
     
            </form>
          </div>
    </div>

</form>

</body>

</html>