
<!DOCTYPE html>
<html lang="en">
<head>
    <title id='Description'>{{Config::get('setting.website_name')}} | {{ Lang::get('COMMON.ADMIN') }}
    </title>
    <link rel="stylesheet" href="{{url()}}/admin/jqwidgets/styles/jqx.base.css" type="text/css" />
    <link rel="stylesheet" href="{{url()}}/admin/jqwidgets/styles/jqx.metro.css" type="text/css" />
    <script type="text/javascript" src="{{url()}}/admin/jqwidgets/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="{{url()}}/admin/jqwidgets/jqx-all.js"></script>
	<style>     
        .leftline {
     
			border-left: 2px solid #525252;
        }
        .leftline:not(.jqx-grid-cell-hover):not(.jqx-grid-cell-selected), .jqx-widget .green:not(.jqx-grid-cell-hover):not(.jqx-grid-cell-selected) {
   
			border-left: 2px solid #525252;
        }
   
    </style>
    <script type="text/javascript">
	
		var cellclass = function (row, columnfield, value) {
              
               return 'green';
            }

		
		function doGridSearch(id,type,value){

			if(type == 'search')
			{
				$("#"+id).val(value);
			}
			else if(type == 'dropdown')
			{
				$("#"+id).jqxDropDownList('selectItem', value ); 
			
			}
		}
		
		function doSearch(){
			search();
		}

		var gridHistory = [];
		var gridCurrent = null;

		function doSetSearchValues(dataObj) {
            for (var key in dataObj) {
                if (dataObj.hasOwnProperty(key)) {
                    var val = dataObj[key];

                    if (Array.isArray(val) || typeof val == "object") {
                        doSetSearchValues(val);
                    } else if (key == "createdfrom" || key == "createdto") {
                        // For date from & date to.
                        var t = dataObj[key].split(/[- :]/);
                        var tid = { "createdfrom": "dateFrom", "createdto": "dateTo" };

                        $("#" + tid[key]).jqxDateTimeInput('setDate', new Date(t[0], t[1]-1, t[2]));
                    } else {
                        var obj = $("#" + key);

                        if (obj.is("[role='combobox']")) {
                            // For combo box.
                            var ans = obj.jqxDropDownList("val", dataObj[key]);

                            if (ans != dataObj[key]) {
                                obj.jqxDropDownList("selectedIndex", -1);
							}
						} else {
                            obj.val(dataObj[key]);
                        }
                    }
                }
            }
		}

		function doBack() {
		    var source = gridHistory.pop();

            if (typeof source != "undefined") {
                gridCurrent = source;

                if (gridHistory.length <= 0) {
                    gridHistory.push(source);
                }

                source = JSON.parse(source);

                doSetSearchValues(source["data"]);

                // Refer to search().
                var dataAdapter = new $.jqx.dataAdapter(source, {
                    downloadComplete: function (data, status, xhr) { },
                    loadComplete: function (data) {
                        return onDataAdapterLoadCompleted(data);
                    },
                    loadError: function (xhr, status, error) { }
                });

                $("#{{$grid['id']}}").jqxGrid({
					source: dataAdapter,
				});
			}
		}

		function search(){

			var aflt = new Array();
			var aqfld = new Array();
			var temp = new Array();
			var params = new Array();
			
			
			var paginginformation = $('#{{$grid['id']}}').jqxGrid('getpaginginformation');
		
			params['aflt'] = aflt;
			params['aqfld'] = aqfld;
			params['createdfrom'] = $('#dateFrom').jqxDateTimeInput('getText'); 
			params['createdto']   = $('#dateTo').jqxDateTimeInput('getText'); 
			params['igd']   = paginginformation.pagenum; 
			params['ilt']   = paginginformation.pagesize; 
			params['ist']   = paginginformation.pagescount; 
			
			@if( $grid['action'] == 'showdetail-data' || $grid['action'] == 'affiliateEnquiry-data')
				params['timefrom'] = $( "#time_from" ).val();
				params['timeto']   = $( "#time_to" ).val();
			@endif
			
			$('#{{$grid['id']}}').jqxGrid('clear');
			
			@if( isset($grid['filters']))
					@foreach( $grid['filters'] as $filter )
							var temp = $("#{{$filter['name']}}").jqxDropDownList('getSelectedItem');
							if( temp != null ){
								var	selected_dropdown_{{$filter['name']}} = temp.value;
							}else{
								var	selected_dropdown_{{$filter['name']}} = '';
							}
					@endforeach				
			@endif
	
			var source =
			{
				datatype: "json",
				datafields: [
					@foreach( $grid['columns'] as $column)
							 { name: '{{$column['index']}}' },
					@endforeach	
				],
				url: "{{$grid['action']}}",
				beforeprocessing: function (data) {
                    source.totalrecords = data.total;
					@if( $grid['action'] == 'instant-data' )
						alert = false;
						$.each(data.rows, function(i, item) {
							console.log(item.no_status);
								if( item.no_status == 0 ){
									alert = true;
								}
						})
						
						if( alert ){
								playSound();
						}
					@endif
                },
				data: {
					@if( isset($grid['options']['params'] ) )
						@foreach( $grid['options']['params'] as $key => $value )
						{{$key}}: '{{$value}}',
						@endforeach	
					@endif
					igd: params['igd'],
					limit: params['ilt'],
					start: params['ist'],
					createdfrom: params['createdfrom'],
					createdto: params['createdto'],
					@if( $grid['action'] == 'showdetail-data' || $grid['action'] == 'affiliateEnquiry-data')
						timefrom : params['timefrom'],
						timeto : params['timeto'],
					@endif
					aflt:
					{
						@if( isset($grid['searches']) )
							@foreach( $grid['searches'] as $search )
								@foreach( $search['options'] as $key => $option )
									@if(!is_array($option))
										{{$key}} : $('#{{$key}}').jqxInput('val'),
									@endif
								@endforeach			
							@endforeach
						@endif
					},
					aqfld:
					{
						@if( isset($grid['filters']))
							@foreach( $grid['filters'] as $filter )
									{{$filter['name']}} : selected_dropdown_{{$filter['name']}}, 
							@endforeach				
						@endif
					},
					btn_click: $('#button_type_click').val(),
						
				}
			
				
			};

            // Keep search history.
			if (typeof gridCurrent != "undefined" && gridCurrent != null) {
                gridHistory.push(gridCurrent);
			}
            gridCurrent = JSON.stringify(source);

            var dataAdapter = new $.jqx.dataAdapter(source, {
                downloadComplete: function (data, status, xhr) { },
                loadComplete: function (data) {
                    return onDataAdapterLoadCompleted(data);
				},
                loadError: function (xhr, status, error) { }
            });

            $("#{{$grid['id']}}").jqxGrid(
            {
                source: dataAdapter,                
            });

		}
		
        $(document).ready(function () {
            // prepare the data
			var source_initial =
			{
				datatype: "json",
				datafields: [
					@foreach( $grid['columns'] as $column)
							 { name: '{{$column['index']}}' },
					@endforeach	
				],
				url: "{{$grid['action']}}",
				beforeprocessing: function (data) {
                    source_initial.totalrecords = data.total;
                },
				data: {
					@if( isset($grid['options']['params']) )
						@foreach( $grid['options']['params'] as $key => $value )
						{{$key}}: '{{$value}}',
						@endforeach	
					@endif
					limit: 20,
					start: 0,
					@if(!isset($grid['options']['params']['createdfrom']))
					createdfrom: '{{date('Y-m-d')}}',
					@endif
					@if(!isset($grid['options']['params']['createdto']))
					createdto: '{{date('Y-m-d')}}',
					@endif
					aflt:
					{
						@if( isset($grid['searches']) )
							@foreach( $grid['searches'] as $search )
								@foreach( $search['options'] as $key => $option )
									@if(!is_array($option) && isset($search['params']['value']))
											{{$key}} : '{{$search['params']['value']}}',								
									@endif
								@endforeach	
							@endforeach
						@endif
					}
					
				}
			};

			// Keep search history.
            gridHistory.push(JSON.stringify(source_initial));

            var cellsrenderer = function (row, columnfield, value, defaulthtml, columnproperties, rowdata) {
                if (value < 20) {
                    return '<span style="margin: 4px; float: ' + columnproperties.cellsalign + '; color: #ff0000;">' + value + '</span>';
                }
                else {
                    return '<span style="margin: 4px; float: ' + columnproperties.cellsalign + '; color: #008000;">' + value + '</span>';
                }
            }

            var dataAdapter = new $.jqx.dataAdapter(source_initial, {
                downloadComplete: function (data, status, xhr) { },
                loadComplete: function (data) {
                    return onDataAdapterLoadCompleted(data);
				},
                loadError: function (xhr, status, error) { }
            });

            // initialize jqxGrid
            $("#{{$grid['id']}}").jqxGrid(
            {
				@if( $grid['action'] == 'showdetail-data' || $grid['action'] == 'affiliateEnquiry-data')
                width: 1600,
				@else
				 width: 1600,	
				@endif
                source: dataAdapter,                
                pageable: @if(isset($paging))false @else true @endif,
                autoheight: true,
				columnsresize: true,
				altrows: true,
                sortable: true,
                altrows: true,
				theme:'metro',
                enabletooltips: true,
				showaggregates: true,
                editable: false,
				@if( isset($grid['options']['footer']) && $grid['options']['footer'] == true )
				showstatusbar: true,
				@endif
                statusbarheight: 50,
				pagesize: 20,
				pagesizeoptions: ['20', '50', '100', '150', '200'],
				@if( isset($grid['options']['checkbox']) && $grid['options']['checkbox'] == true )
                selectionmode: 'checkbox',
				@else
				selectionmode: 'singlecell',	
				@endif
				virtualmode: true,
				
				rendergridrows: function(obj)
				{
					  return obj.data;     
				},
                columns: [
				  @foreach( $grid['columns'] as $column)
						 { @if( isset($column['options']['cellclass']))cellclassname: '{{$column['options']['cellclass']}}', @endif text: '{{$column['name']}}', datafield: '{{$column['index']}}' @if( isset($column['options']['align'])), cellsalign: '{{$column['options']['align']}}',align: '{{$column['options']['align']}}'@endif ,  width: {{$column['width']}} ,aggregates: ['{{$column['name']}}'],aggregatesrenderer: function (aggregatedValue, currentValue) {
							  return '<div id="total_{{$column['index']}}"></div>';
                          }
						 },
				  @endforeach	
                ]
            });
			
			$("#{{$grid['id']}}").on("sort", function (event) {
				 search();
				 
            });
			
			
			
			@if( isset($grid['options']['checkbox']) && $grid['options']['checkbox'] == true )
				$("#{{$grid['id']}}").bind('cellendedit', function (event) {
					if (event.args.value) {
						$("#{{$grid['id']}}").jqxGrid('selectrow', event.args.rowindex);
					}
					else {
						$("#{{$grid['id']}}").jqxGrid('unselectrow', event.args.rowindex);
					}
				});
			@endif
			
		
			
			$("#dateFrom, #dateTo").jqxDateTimeInput({ formatString: "F", width: '120px' , height: '25px' , formatString: "yyyy-MM-dd" ,openDelay: 0,closeDelay: 0});
			
			
			@if(isset($grid['options']['params']['createdfrom']))
				var t = "{{$grid['options']['params']['createdfrom']}}".split(/[- :]/);
				$('#dateFrom').jqxDateTimeInput('setDate', new Date(t[0], t[1]-1, t[2]));
			@endif	
			
			@if(isset($grid['options']['params']['createdto']))
				var t = "{{$grid['options']['params']['createdto']}}".split(/[- :]/);
				$('#dateTo').jqxDateTimeInput('setDate', new Date(t[0], t[1]-1, t[2]));
			@endif
			
			@if( isset($grid['searches']) )
				@foreach( $grid['searches'] as $search )
					@foreach( $search['options'] as $key => $option )
						@if(!is_array($option))
								$("#{{$key}}").jqxInput({placeHolder: "{{$option}}", height: 25, width: 200, minLength: 1 });
						@endif
					@endforeach			
				@endforeach
			@endif
	
			@if( isset($grid['filters']) )
				@foreach( $grid['filters'] as $filter )
					var {{$filter['name']}}_data = [];
			
			
					@foreach( $filter['options'] as $key => $value )
							{{$filter['name']}}_data.push({ id: "{{$key}}", title: "{{$value}}" });
							
					@endforeach
					   var source =
						   {
							   localdata: {{$filter['name']}}_data,
							   datatype: "array",
							   datafields: [
								   { name: 'id' },
								   { name: 'title' }
							   ],
						   };
						   
						    var dataAdapter = new $.jqx.dataAdapter(source);
					
					$("#{{$filter['name']}}").jqxDropDownList({ source: dataAdapter,  width: '200', height: '25',filterDelay:0,placeHolder: "Select:",openDelay: 0,closeDelay: 0,valueMember: 'id',displayMember: 'title'});
				@endforeach
			@endif
			@if( isset($grid['buttons'][1]) )
				@foreach( $grid['buttons'][1] as $button )
					$("#{{$button['name']}}").jqxButton({ template: "primary" });
				@endforeach
			@endif	
			@if( isset($grid['buttons'][2]) )
				@foreach( $grid['buttons'][2] as $button )
					$("#btn_{{$button['name']}}").jqxButton({ template: "primary" });
				@endforeach
			@endif

			$("#jqxSubmitButton").jqxButton({ width: '150', template: "success"});
			//$("#btn_excel").jqxButton({ width: '80', template: "success"});
			$("#jqxSubmitButton").on('click', function () {
                    $("#events").find('span').remove();
                    $("#events").append('<span>Submit Button Clicked</span>');
             });
			 

			
        });
		
		@if( isset($grid['options']['checkbox']) && $grid['options']['checkbox'] == true )
		function approve_batch(type){
			
			   // get all selected records.
         
                var rows = $("#{{$grid['id']}}").jqxGrid('selectedrowindexes');
				
                var selectedRecords = new Array();
                for (var m = 0; m < rows.length; m++) {
                    var row = $("#{{$grid['id']}}").jqxGrid('getrowdata', rows[m]);
					if(typeof(row) != "undefined" && row !== null) {
						  selectedRecords[selectedRecords.length] = row['id'];
					}
                }
				
				$.ajax({
						 type: "POST",
						 url:  '@if( isset($grid['options']['checkbox_url']) && $grid['options']['checkbox_url'] == true ){{$grid['options']['checkbox_url']}}@endif',
						 data: {
							_token: "{{ csrf_token() }}",
							rows: selectedRecords,
							type: type
						 },
						 beforeSend: function(){
							
						 },
						 success: function(json){
							obj = JSON.parse(json);
							$.each(obj, function(i, item) {
								if( '{{Lang::get('COMMON.SUCESSFUL')}}' == item ){
									alert('{{Lang::get('COMMON.SUCESSFUL')}}');
									doSearch();
								}else{
									alert('{{Lang::get('COMMON.FAILED')}}');
								}
							})
							
						}
				})
			
           
		}
		@endif
		
		

		function openParentTab(title, url) {
			window.opener.addTab(title, url);
		} 
		
		@if( $grid['action'] == 'instant-data' )
			setInterval(doSearch, 20000);
		@endif
		
		var popup_created = false;
		
		function doConfirmAction(url,status,id){
			
			$('#ok').attr( 'onClick' , 'doConfirmActionProcess(\''+url+'\',\''+status+'\',\''+id+'\')' );
			if( popup_created == false){
				createElements();
			}
		     $('#eventWindow').jqxWindow('open');
		}	
		
		function doConfirmActionProcess(url,status,id){
			
			$.ajax({
				 type: "POST",
				 url:  url,
				 data: {
					_token: "{{ csrf_token() }}",
					action: status,
					id: id,
				 },
				 beforeSend: function(){
					
				 },
				 success: function(json){
						obj = JSON.parse(json);
						 var str = '';
						 $.each(obj, function(i, item) {
							
							if( '{{Lang::get('COMMON.SUCESSFUL')}}' == item ) {
                                alert('{{Lang::get('COMMON.SUCESSFUL')}}');
                                doSearch();
                            }else if( '{{Lang::get('COMMON.FAILED')}}' == item ) {
                                alert('{{Lang::get('COMMON.FAILED')}}');
                                doSearch();
							}else{
								str += item + '<br>';
							}
						})
						
					
				}
			})
		}
		
        function createElements() {
			popup_created = true;
			$('#eventWindow').show();
            $('#eventWindow').jqxWindow({
                maxHeight: 150, maxWidth: 280, minHeight: 30, minWidth: 250, height: 145, width: 270,
                resizable: false, isModal: true, modalOpacity: 0.1,
                okButton: $('#ok'), cancelButton: $('#cancel'),
                initContent: function () {
                    $('#ok').jqxButton({ width: '65px' });
                    $('#cancel').jqxButton({ width: '65px' });
                    $('#ok').focus();
                }
            });
            $('#events').jqxPanel({ height: '250px', width: '450px' });
            $('#showWindowButton').jqxButton({ width: '100px' });
			
        }
		
		function doChangeDate(type){

		    var dtFromObj = $('#dateFrom');
		    var dtToObj = $('#dateTo');
		    var today = new Date('{{date('Y')}}', parseFloat('{{date('m')}}') - 1, '{{date('d')}}');
		    var fromDate = dtFromObj.jqxDateTimeInput('getDate');
		    var toDate = dtToObj.jqxDateTimeInput('getDate');

		    if (type == 'now') {
                fromDate = today;
                toDate = today;
            } else if (type == 'lswk') {
                fromDate = new Date(today.getFullYear(), today.getMonth(), today.getDate() - 7);
                toDate = today;
            } else if (type == 'lsmt') {
                fromDate = new Date(today.getFullYear(), today.getMonth() - 1, 1);
                toDate = new Date(today.getFullYear(), today.getMonth(), 0);
            } else if (type == 'mtd') {
                fromDate = new Date(today.getFullYear(), today.getMonth(), 1);
                toDate = today;
            } else if (type == 'all') {
                fromDate = new Date(2014, 0, 1);
                toDate = new Date(today.getFullYear(), today.getMonth(), '{{date('t')}}');
			} else {
                var mth = 0;
                var day = 0;

                if (type == 'm1mt')
                    mth = -1;
                else if (type == 'p1mt')
                    mth = 1;

                if (type == 'm1dy')
                    day = -1;
                else if (type == 'p1dy')
                    day = 1;
                else if (type == 'm1wk')
                    day = -7;
                else if (type == 'p1wk')
                    day = 7;

                fromDate = new Date(fromDate.getFullYear(), fromDate.getMonth() + mth, fromDate.getDate() + day);
                toDate = new Date(toDate.getFullYear(), toDate.getMonth() + mth, toDate.getDate() + day);
			}

            dtFromObj.jqxDateTimeInput('setDate', fromDate);
            dtToObj.jqxDateTimeInput('setDate', toDate);

            search();
		}
		
		function playSound(){
			
			@if( Config::get('setting.front_path') == 'front')
				document.getElementById("sound").innerHTML='<audio autoplay="autoplay"><source src="{{url()}}/admin/audios/IW.mp3" type="{{url()}}/admin/audio/mpeg" /><source src="{{url()}}/admin/audios/IW.ogg" type="audio/ogg" /><embed hidden="true" autostart="true" loop="false" src="{{url()}}/admin/audios/IW.mp3" /></audio>';
			@elseif( Config::get('setting.front_path') == 'royalewin')
				document.getElementById("sound").innerHTML='<audio autoplay="autoplay"><source src="{{url()}}/admin/audios/RW.mp3" type="{{url()}}/admin/audio/mpeg" /><source src="{{url()}}/admin/audios/RW.ogg" type="audio/ogg" /><embed hidden="true" autostart="true" loop="false" src="{{url()}}/admin/audios/RW.mp3" /></audio>';
			@else
				document.getElementById("sound").innerHTML='<audio autoplay="autoplay"><source src="{{url()}}/admin/audios/alert.mp3" type="{{url()}}/admin/audio/mpeg" /><source src="{{url()}}/admin/audios/alert.ogg" type="audio/ogg" /><embed hidden="true" autostart="true" loop="false" src="{{url()}}/admin/audios/alert.mp3" /></audio>';
			@endif

		}

		function checkBalance(product){
			
			$.ajax({
				 type: "POST",
				 url:  'product-checkBalance?&rd=<?php echo rand ( 10000, 99999 ); ?>',
				 data: {
					_token: "{{ csrf_token() }}",
					product: product,
				 },
				 beforeSend: function(){
					$('#'+product).html('<img id="loading" src="{{url()}}/admin/images/ajax-loader.gif"/>');
					
				 },
				 success: function(json){
					$('#'+product).html(json);
				 }
			})
		}

		function onDataAdapterLoadCompleted(data) {

            @if(isset($products))
                @foreach( $products as $product)
					checkBalance('{{$product}}');
				@endforeach
			@endif

			$.each(data.footer, function (key, object) {
                $.each(object, function (colunm, value) {
                    $('#total_' + colunm).html(value);
                });
            });

            $("[data-autoload]", $("#{{$grid['id']}}")).each(function (i, v) {
                var obj = $(v);

                if (obj.attr("data-autoload") == "1") {
                    autoloadBinding(obj);
                }
            });
        }

		function autoloadBinding(obj) {

		    // Must follow standard data-? format.
			var jobj = $(obj);
			var href = jobj.data("href"); // Required.
			var hasValue = (jobj.data("hasvalue") == "y"); // Include val in href GET call.
			var loadingImg = (jobj.data("loadingimg")); // a: append, p: prepend, r: replace, n: none
			var outVal = (jobj.data("outval")); // r: replace, v: input.val(), n: none
			var reload = (jobj.data("reload")); // y: yes, n: no

			if (hasValue) {
			    href += (href.indexOf("?") > -1 ? "&" : "?") + "val=" + jobj.val();
			}

			if (typeof href != 'undefined' && href.length > 5) {
			    var loadingImgDom = $('<img class="loading_img" src="{{url()}}/admin/images/ajax-loader.gif"/>');

			    if ($.inArray(loadingImg, ["a", "p", "n"]) < 0) {
                    loadingImg = "r";
				}

			    if ($.inArray(outVal, ["v", "n"]) < 0) {
                    outVal = "r";
				}

                $.ajax({
                    type: "GET",
                    url: href,
                    beforeSend: function(){
                        if (loadingImg == "r") {
	                        jobj.html(loadingImgDom);
						} else if (loadingImg == "a") {
                            jobj.parent().append(loadingImgDom);
						} else if (loadingImg == "p") {
                            jobj.parent().prepend(loadingImgDom);
						}
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert("{{ Lang::get('public.SaveFailedPleaseRefreshPageAndTryAgain') }}");
                    },
                    success: function(json){
                        if (outVal == "r") {
                            jobj.html(json);
						} else if (outVal == "v") {
                            jobj.val(outVal);
						}

						if (reload == "y") {
                            search();
						}
                    },
                    complete: function (jqXHR, textStatus) {
                        loadingImgDom.remove();
                    }
                });
			}
		}

		function export_4dlist(){
			window.open('4dpromo-list', '4d list', 'width=1150,height=830');
		}

		function getSearchJsonData() {
			// Copy from search().
			var aflt = new Array();
			var aqfld = new Array();
			var temp = new Array();
			var params = new Array();
			var paginginformation = $('#{{$grid['id']}}').jqxGrid('getpaginginformation');

			params['aflt'] = aflt;
			params['aqfld'] = aqfld;
			params['createdfrom'] = $('#dateFrom').jqxDateTimeInput('getText');
			params['createdto']   = $('#dateTo').jqxDateTimeInput('getText');
			params['igd']   = paginginformation.pagenum;
			params['ilt']   = paginginformation.pagesize;
			params['ist']   = paginginformation.pagescount;

			@if( $grid['action'] == 'showdetail-data' || $grid['action'] == 'affiliateEnquiry-data')
				params['timefrom'] = $( "#time_from" ).val();
				params['timeto']   = $( "#time_to" ).val();
			@endif


			@if( isset($grid['filters']))
				@foreach( $grid['filters'] as $filter )
					var temp = $("#{{$filter['name']}}").jqxDropDownList('getSelectedItem');
					if( temp != null ){
						var	selected_dropdown_{{$filter['name']}} = temp.value;
					}else{
						var	selected_dropdown_{{$filter['name']}} = '';
					}
				@endforeach
			@endif

			var data = {
				@if( isset($grid['options']['params'] ) )
					@foreach( $grid['options']['params'] as $key => $value )
						{{$key}}: '{{$value}}',
					@endforeach
				@endif
				igd: params['igd'],
				limit: params['ilt'],
				pagesize: params['ilt'],
				start: params['ist'],
				recordstartindex: params['igd'] * params['ilt'],
				createdfrom: params['createdfrom'],
				createdto: params['createdto'],
				@if( $grid['action'] == 'showdetail-data' || $grid['action'] == 'affiliateEnquiry-data')
					timefrom : params['timefrom'],
					timeto : params['timeto'],
				@endif
				aflt:
				{
					@if( isset($grid['searches']) )
						@foreach( $grid['searches'] as $search )
							@foreach( $search['options'] as $key => $option )
								@if(!is_array($option))
									{{$key}} : $('#{{$key}}').jqxInput('val'),
								@endif
							@endforeach
						@endforeach
					@endif
				},
				aqfld:
				{
					@if( isset($grid['filters']))
						@foreach( $grid['filters'] as $filter )
							{{$filter['name']}} : selected_dropdown_{{$filter['name']}},
						@endforeach
					@endif
				}
			};

			return data;
		}

		function export_excel(){

			var url = "{{$grid['action']}}";

			if (url.indexOf("?") > -1) {
				url += "&export=excel";
			} else {
				url += "?export=excel";
			}

			var jsonData = getSearchJsonData();

			for (var key in jsonData) {
				if (!jsonData.hasOwnProperty(key)) {
					continue;
				}

				if (typeof jsonData[key] == "object") {
					var obj = jsonData[key];

					for (var obkey in obj) {
						if (!obj.hasOwnProperty(obkey)) {
							continue;
						}

						url += "&" + key + "%5B" + obkey + "%5D=" + obj[obkey];
					}
				} else {
					url += "&" + key + "=" + jsonData[key];
				}
			}

			window.location.href = url;
		}
		
		function doSearchByButton(type){
				
			$('#button_type_click').val(type);
			search();
      
		}
    </script>
	<style>
	.fl{
		float: left;
	}
	.label-margin{
		padding-top:5px;
		margin-left: 10px;
		margin-right: 5px;
	}
	.clear{
		clear: both;
	}
	div.triangle {
		border-bottom: 10px solid;
		border-left: 9px solid transparent;
		border-right: 9px solid transparent;
		bottom: 11px;
		display: inline;
		font-size: 0;
		height: 0;
		line-height: 0;
		margin: 0 auto;
		position: relative;
		width: 0;
	}
	div.square {
		bottom: 5px;
		display: inline;
		font-size: 0;
		height: 0px;
		line-height: 0;
		margin: 0 auto;
		position: relative;
		width: 0px;
	}
	</style>
</head>
<body class='default'>

	<div id="sound"></div>

    <div id='jqxWidget' style="font-size: 13px; font-family: Verdana; float: left;margin-left:10px;margin-top:10px;">
		
		@if( isset($grid['buttons'][1]) )
			@foreach( $grid['buttons'][1] as $button )
			<div style="float:left;margin-right:10px;">
				<input style='cursor: pointer;' onClick="parent.addTab('{{$button['value']}}','{{$button['params']['url']}}')" type="button" value="{{$button['name']}}" id="{{$button['name']}}" />
			</div>
			@endforeach
			
			@if( count($grid['buttons'][1]) > 0)
				<div style="clear:both;"></div>
				<br>
			@endif
		@endif
			<!--
			<div>
				<input style='cursor: pointer;' onClick="export_excel()" type="button" value="Excel" id="btn_excel" />
			</div>
			-->
		
		@if( isset($grid['searches']) )
			@foreach( $grid['searches'] as $search )
				@foreach( $search['options'] as $key => $option )
					@if(!is_array($option))
						@if (isset($search['params']['hidden']) && $search['params']['hidden'] == true)
							<input type="hidden" id="{{ $key }}" value="{{ isset($search['params']['value']) ? $search['params']['value'] : '' }}" />
						@else
							<label class="fl label-margin">{{$option}}</label>
							<input type="text" id="{{$key}}" class="fl" value="@if(isset($search['params']['value'])){{$search['params']['value']}}@endif"/>	
						@endif
					@endif
				@endforeach
			@endforeach
			@if( count($grid['searches']) > 0)
				<br>
				<br>
			@endif
		@endif
	
		@if( isset($grid['filters']) )
			@foreach( $grid['filters'] as $filter )
		
				@if( isset($filter['params']['display']) )
					<label class="fl label-margin">{{$filter['params']['display']}}</label>
				@endif 
				
				<div id="{{$filter['name']}}" class="fl"></div>
			@endforeach
			@if( count($grid['filters']) > 0)
				<br>
				<br>
			@endif
		@endif
		
	
		<label class="fl label-margin clear" >Date</label>
		<div id='dateFrom' class="fl">
		
        </div>
		@if( $grid['action'] == 'showdetail-data' || $grid['action'] == 'affiliateEnquiry-data' )
			<select id="time_from" name="time_from" style="float:left;margin-left:3px;margin-top:3px;">
				<option value="00:00:00" selected="selected">00:00</option>
				<option value="01:00:00">01:00</option>
				<option value="02:00:00">02:00</option>
				<option value="03:00:00">03:00</option>
				<option value="04:00:00">04:00</option>
				<option value="05:00:00">05:00</option>
				<option value="06:00:00">06:00</option>
				<option value="07:00:00">07:00</option>
				<option value="08:00:00">08:00</option>
				<option value="09:00:00">09:00</option>
				<option value="10:00:00">10:00</option>
				<option value="11:00:00">11:00</option>
				<option value="12:00:00">12:00</option>
				<option value="13:00:00">13:00</option>
				<option value="14:00:00">14:00</option>
				<option value="15:00:00">15:00</option>
				<option value="16:00:00">16:00</option>
				<option value="17:00:00">17:00</option>
				<option value="18:00:00">18:00</option>
				<option value="19:00:00">19:00</option>
				<option value="20:00:00">20:00</option>
				<option value="21:00:00">21:00</option>
				<option value="22:00:00">22:00</option>
				<option value="23:00:00">23:00</option>
			</select>
		@endif
		<span class="fl label-margin">~</span>
		
		<div id='dateTo' class="fl">
		
        </div>
		
		@if( $grid['action'] == 'showdetail-data' || $grid['action'] == 'affiliateEnquiry-data')
			<select id="time_to" name="time_to" style="float:left;margin-left:3px;margin-top:3px;">
				<option value="00:59:59">00:59</option>
				<option value="01:59:59">01:59</option>
				<option value="02:59:59">02:59</option>
				<option value="03:59:59">03:59</option>
				<option value="04:59:59">04:59</option>
				<option value="05:59:59">05:59</option>
				<option value="06:59:59">06:59</option>
				<option value="07:59:59">07:59</option>
				<option value="08:59:59">08:59</option>
				<option value="09:59:59">09:59</option>
				<option value="10:59:59">10:59</option>
				<option value="11:59:59">11:59</option>
				<option value="12:59:59">12:59</option>
				<option value="13:59:59">13:59</option>
				<option value="14:59:59">14:59</option>
				<option value="15:59:59">15:59</option>
				<option value="16:59:59">16:59</option>
				<option value="17:59:59">17:59</option>
				<option value="18:59:59">18:59</option>
				<option value="19:59:59">19:59</option>
				<option value="20:59:59">20:59</option>
				<option value="21:59:59">21:59</option>
				<option value="22:59:59">22:59</option>
				<option value="23:59:59" selected="selected">23:59</option>
			</select>
		@endif
		
		<?php
			$yesterday = date('d') - 1;
			$lastweek  = date('d') - 7;
		?>
		<div style="float:left;margin-left:15px;margin-top:5px;">
			@if(isset($customDateOptions))
				{!! $customDateOptions !!}
			@else
			<a onclick="doChangeDate('now');" href="javascript:void(0);">+0D</a>
			|
			<a onclick="doChangeDate('m1dy');" href="javascript:void(0);">-1D</a>
			|
			<a onclick="doChangeDate('p1dy');" href="javascript:void(0);">+1D</a>
			|
			<a onclick="doChangeDate('lswk');" href="javascript:void(0);">-7D</a>
			|
			<a onclick="doChangeDate('lsmt');" href="javascript:void(0);">-1M</a>
			|
			<a onclick="doChangeDate('mtd');" href="javascript:void(0);">MTD</a>
			|
			<a onclick="doChangeDate('all');" href="javascript:void(0);">ALL</a>
			@endif
		</div>
		
		
		<div class="clear">
            <input style='margin-top: 20px;cursor: pointer;' type="submit" value="{{Lang::get('COMMON.SEARCH')}}" id='jqxSubmitButton' onClick="search()"/>
			@if( isset($grid['buttons'][2]) )
				@foreach( $grid['buttons'][2] as $button )
					<input style='margin-top: 20px;cursor: pointer;' type="button" value="{{$button['name']}}" id="btn_{{$button['name']}}" onClick="{{$button['params']['url']}}" />
				@endforeach
			@endif
        </div>
		<input  id="button_type_click" value="" type="hidden" />

		<br>
		@if(!empty($username))
		<table>
			@if(Config::get('setting.opcode')=='IFW')
				<td width='757' bgcolor='#c9c9c9'></td>
			@else
				<td width='667' bgcolor='#c9c9c9' align='center'>{{Lang::get('COMMON.MEMBER')}}</td>
			@endif
				<td width='266' bgcolor='#c9c9c9' align='center'>{{Lang::get('COMMON.DOWNLINE')}}</td>
				<td width='266' bgcolor='#c9c9c9' align='center'>{{$grid['options']['username']}}</td>
				<td width='266' bgcolor='#c9c9c9' align='center'>{{Lang::get('COMMON.UPLINE')}}</td>
			</tr>
		</table>
		@endif
         <div id="{{$grid['id']}}" >
        </div>
     </div>
	 
	 <div id="eventWindow" style="display:none">
           <div>
               <img width="14" height="14" src="{{url()}}/admin/jqwidgets/styles/images/help.png" alt="" />
               {{ Lang::get('COMMON.CONFIRMATION') }}</div>
           <div>
               <div>
					{{ Lang::get('COMMON.CONFIRMPROCESS') }}
               </div>
               <div>
               <div style="float: right; margin-top: 15px;">
                   <input type="button" id="ok" value="OK" style="margin-right: 10px" onClick=""/>
                   <input type="button" id="cancel" value="Cancel" />
               </div>
               </div>
           </div>
    </div>
</body>
</html>
