@extends('admin/master_lte')

@section('title', 'Admin Dashboard')

@section('top_js')
    <link rel="stylesheet" href="{{ URL('/') }}/admin/jqwidgets/styles/jqx.base.css" type="text/css" />
    <link rel="stylesheet" href="{{ URL('/') }}/admin/jqwidgets/styles/jqx.metro.css" type="text/css" />   
    <script type="text/javascript" src="{{ URL('/') }}/admin/jqwidgets/jqx-all.js"></script>	

    <script type="text/javascript" src="{{ URL('/') }}/admin/jqwidgets/jqxcore.js"></script>
    <script type="text/javascript" src="{{ URL('/') }}/admin/jqwidgets/jqxdraw.js"></script>
    <script type="text/javascript" src="{{ URL('/') }}/admin/jqwidgets/jqxchart.core.js"></script>
    <script type="text/javascript" src="{{ URL('/') }}/admin/jqwidgets/jqxdata.js"></script>
    <script type="text/javascript" src="{{ URL('/') }}/admin/jqwidgets/jqxtabs.js"></script>
    <link href="{{ URL('/') }}/admin/new_design/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="{{ URL('/') }}/admin/new_design/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="{{ URL('/') }}/admin/new_design/ionicons.min.css" rel="stylesheet" type="text/css" />
    <link href="{{ URL('/') }}/admin/new_design/AdminLTE.css" rel="stylesheet" type="text/css" />
    <link href="{{ URL('/') }}/admin/new_design/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
    <link href="{{ URL('/') }}/admin/new_design/datepicker3.css" rel="stylesheet" type="text/css" />
    <link href="{{ URL('/') }}/admin/new_design/bootstrap-timepicker.min.css" rel="stylesheet"/>
    <style>
    canvas {
        -moz-user-select: none;
        -webkit-user-select: none;
        -ms-user-select: none;
    }
    </style>
<script type="text/javascript">
	$(document).ready(function () {
						var h = $(window).height() - 53;
		// create jqxTabs.
		$('#mastertab').jqxTabs({ height: h, width: '100%',  autoHeight: false, showCloseButtons: true ,  theme:'ui-start' });
		
		$('#mastertab').on('removed', function (event) { 

			tabs[event.args.title] = true;
		}); 
		//$("#jqxNavigationBar").jqxNavigationBar({ width: 200, height: 600, theme:'fresh', expandMode: 'toggle'});
		
                $("body").addClass("skin-blue");
	});
	var tabs = new Array(); 
	var tabs_record = new Array(); 
	function addTab(title,url){
		
		for(count = 0; count < 20; count++){
			text = $('#mastertab').jqxTabs('getTitleAt', count); 
			if(text == title)
			{
				$('#mastertab').jqxTabs('removeAt', count); 
			}
        }
		
		if ( tabs[title] == undefined ||  tabs[title] == true ) {
			tabs[title] = false;
			
			 $('#mastertab').jqxTabs({ selectedItem: 1 }); 
			 $('#mastertab').jqxTabs('addLast', title, ' <iframe frameBorder="0" src="'+url+'" style="overflow:hidden;overflow-x:hidden;overflow-y:hidden;height: 90%; width:100%;"></iframe> ');

		}
		
		
        
	
	}	
	function closeTab(){

		$('#mastertab').jqxTabs('removeAt', $('#mastertab').jqxTabs('selectedItem'));


	}
	
	 
</script>



<script type="text/javascript">
	$(document).ready(function () {
		$('#jqxTabs').jqxTabs({ width: '100%',  selectionTracker: true, animationType: 'fade' });
	});
</script>
@stop
@section('content')	
<div id='mastertab' style="float: left; width:100%; height:100%;">
 <ul style="margin: 30px;" id="unorderedList">
  <li>{{ Lang::get('public.Dashboard') }}</li>
 </ul>
 <div>
<div class="wrapper row-offcanvas row-offcanvas-left">


                <!-- Main content -->
                <section class="content">
                
                <!--Date Picker-->
                 <div class="row">
					<div class="col-md-9">
						<div class="form-group">
							<div class="btn-group" role="group">
								<a href="{{route('admin_home')}}?type=today" class="btn btn-default">{{ Lang::get('COMMON.TODAY') }}</a>
								<a href="{{route('admin_home')}}?type=week" class="btn btn-default">{{ Lang::get('COMMON.WEEK') }}</a>
								<a href="{{route('admin_home')}}?type=month" class="btn btn-default">{{ Lang::get('COMMON.MONTH') }}</a>
							</div>
						</div>
					</div>
                </div>
				
                <!--Date Picker Ends-->
                
                <!--Summary widget-->
                <div class="row">
                        <div class="col-lg-2 col-md-6 col-sm-6 col-xs-12">
                            <!-- small box -->
                            <div class="small-box bg-aqua">
                                <div class="inner">
                                    <h3>
										{{number_format($deposit, 2)}}
                                    </h3>
                                    <p>
                                        {{ Lang::get('COMMON.DEPOSIT') }}
                                    </p>
                                </div>
                                <div class="icon">
                                    <i class="glyphicon glyphicon-download-alt"></i>
                                </div>
								
                                <a href="#" class="small-box-footer" onClick="addTab('{{ Lang::get('COMMON.TRANSACTIONENQUIRY') }}','enquiry?createdfrom={{$start}}&createdto={{$end}}&type=1')">
								{{ Lang::get('COMMON.MOREINFO') }}<i class="fa fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div><!-- ./col -->
                        <div class="col-lg-2 col-md-6 col-sm-6 col-xs-12">
                            <!-- small box -->
                            <div class="small-box bg-red">
                                <div class="inner">
                                    <h3 class="text-white">
										{{number_format($withdrawal, 2)}}
                                    </h3>
                                    <p class="text-white">
                                        {{ Lang::get('COMMON.WITHDRAWAL') }}
                                    </p>
                                </div>
                                <div class="icon">
                                    <i class="glyphicon glyphicon-export"></i>
                                </div>
                                <a href="#" class="small-box-footer text-white" onClick="addTab('{{ Lang::get('COMMON.TRANSACTIONENQUIRY') }}','enquiry?createdfrom={{$start}}&createdto={{$end}}&type=2')">
                                    {{ Lang::get('COMMON.MOREINFO') }} <i class="fa fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div><!-- ./col -->
						 <div class="col-lg-2 col-md-6 col-sm-6 col-xs-12">
                            <!-- small box -->
                            <div class="small-box bg-red">
                                <div class="inner">
                                    <h3 class="text-white">
										{{number_format($netloss, 2)}}
                                    </h3>
                                    <p class="text-white">
                                        {{ Lang::get('COMMON.NETTWINLOSS') }}
                                    </p>
                                </div>
                                <div class="icon">
                                    <i class="glyphicon glyphicon-export"></i>
                                </div>
                                <a href="#" class="small-box-footer text-white">
                                    {{ Lang::get('COMMON.MOREINFO') }} <i class="fa fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div><!-- ./col -->
                        <div class="col-lg-2 col-md-6 col-sm-6 col-xs-12">
                            <!-- small box -->
                            <div class="small-box bg-yellow">
                                <div class="inner">
                                    <h3 class="text-blue">
										{{number_format($profitLoss, 2)}}
                                    </h3>
                                    <p>
                                        {{ Lang::get('COMMON.PROFITLOSS') }}
                                    </p>
                                </div>
                                <div class="icon">
                                    <i class="glyphicon glyphicon-usd"></i>
                                </div>
                                <a href="#" class="small-box-footer" onClick="addTab('{{ Lang::get('COMMON.REPORTTOTALNEW') }}','profitloss2?createdfrom={{$start}}&createdto={{$end}}')">
                                    {{ Lang::get('COMMON.MOREINFO') }} <i class="fa fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div><!-- ./col -->
                        <div class="col-lg-2 col-md-6 col-sm-6 col-xs-12">
                            <!-- small box -->
                            <div class="small-box bg-lime">
                                <div class="inner">
                                    <h3>
										{{$register}}
                                    </h3>
                                    <p>
                                        {{ Lang::get('COMMON.REGISTERMEMBER') }}
                                    </p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-person-add"></i>
                                </div>
                                <a href="#" class="small-box-footer" onClick="addTab('{{ Lang::get('COMMON.MEMBERENQUIRY') }}','member?createdfrom={{$start}}&createdto={{$end}}')">
                                    {{ Lang::get('COMMON.MOREINFO') }} <i class="fa fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div><!-- ./col -->
                        <div class="col-lg-2 col-md-6 col-sm-6 col-xs-12">
                            <!-- small box -->
                            <div class="small-box bg-purple">
                                <div class="inner">
                                    <h3 class="text-white">
										{{$onlineMember}}
                                    </h3>
                                    <p class="text-white">
                                        {{ Lang::get('COMMON.ONLINEMEMBER') }}
                                    </p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-person-add"></i>
                                </div>
								
                                <a href="#" class="small-box-footer text-white" onClick="onlineMember()">
                                     {{ Lang::get('COMMON.MOREINFO') }} <i class="fa fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <!--Summary Widget ends-->
                    
                    <div class="row">
                    
                    <div class="col-lg-6 col-xs-12">
                    <div class="box box-info">
                                <div class="box-header" style="cursor: move;">
                                </div>
                                <div class="box-body">
                                <div id="containerC"></div>
                                </div>
                                <div class="box-footer clearfix">
                                   
                                </div>
                            </div>
                            </div>
                            
                            <div class="col-lg-6 col-xs-12">
                    <div class="box box-info">
                                <div class="box-header" style="cursor: move;">
                                </div>
                                <div class="box-body">
                                <div id="containerD"></div>
                                </div>
                                <div class="box-footer clearfix">
                                   
                                </div>
                            </div>
                            </div>
                            
                            <div class="col-lg-6 col-xs-12">
                    <div class="box box-info">
                                <div class="box-header" style="cursor: move;">
                                </div>
                                <div class="box-body">
                                <div id="containerE"></div>
                                </div>
                                <div class="box-footer clearfix">
                                   
                                </div>
                            </div>
                            </div>
                            
                            <div class="col-lg-6 col-xs-12">
                    <div class="box box-info">
                                <div class="box-header" style="cursor: move;">
                                </div>
                                <div class="box-body">
                                <div id="containerF"></div>
                                </div>
                                <div class="box-footer clearfix">
                                   
                                </div>
                            </div>
                            </div>
                            
                            <div class="col-lg-6 col-xs-12">
                    <div class="box box-info">
                                <div class="box-header" style="cursor: move;">
                                </div>
                                <div class="box-body">
                                <div id="containerW"></div>
                                </div>
                                <div class="box-footer clearfix">
                                   
                                </div>
                            </div>
                            </div>
                            
                            <div class="col-lg-6 col-xs-12">
                    <div class="box box-info">
                                <div class="box-header" style="cursor: move;">
                                </div>
                                <div class="box-body">
                                <div id="containerL"></div>
                                </div>
                                <div class="box-footer clearfix">
                                   
                                </div>
                            </div>
                            </div>
                    
                    </div>
                

                </section><!-- /.content -->

        </div><!-- ./wrapper -->
  </div>     
       <!-- jQuery 2.0.2 -->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
        <!-- Moment JS -->
        <script src="{{ URL('/') }}/admin/new_design/moment.js" type="text/javascript"></script>
        <!-- Chart JS -->
        {{--<script src="{{ URL('/') }}/admin/new_design/Chart.bundle.js" type="text/javascript"></script>--}}
        <!-- Util JS -->
        <script src="{{ URL('/') }}/admin/new_design/util.js" type="text/javascript"></script>
        <!-- InputMask -->
        <script src="{{ URL('/') }}/admin/new_design/jquery.inputmask.js" type="text/javascript"></script>
        <script src="{{ URL('/') }}/admin/new_design/jquery.inputmask.date.extensions.js" type="text/javascript"></script>
        <script src="{{ URL('/') }}/admin/new_design/jquery.inputmask.extensions.js" type="text/javascript"></script>
        <!-- date-range-picker -->
        <script src="{{ URL('/') }}/admin/new_design/daterangepicker.js" type="text/javascript"></script>
         <!-- date-range-picker -->
        <script src="{{ URL('/') }}/admin/new_design/bootstrap-datepicker.js" type="text/javascript"></script>
        <!-- bootstrap time picker -->
        <script src="{{ URL('/') }}/admin/new_design/bootstrap-timepicker.min.js" type="text/javascript"></script>
        <!-- highchart -->
        <script src="{{ URL('/') }}/admin/new_design/highcharts.js"></script>
        <script src="{{ URL('/') }}/admin/new_design/exporting.js"></script>
        
<script type="text/javascript">
	function onlineMember() {
		window.open('{{action('Admin\MemberEnquiryController@showonlinememberlist')}}', '', 'width=300,height=500');
	}
// Age categories
var categories = [
					@foreach($productSummaryByProvider as $key => $val)
						"{{$val->code}}",
					@endforeach	
				];

    
$(document).ready(function () {
    
    
    
    Highcharts.chart('containerC', {
        chart: {
            type: 'bar',
			lineColor: 'red',
        },
        title: {
            text: '{{ Lang::get('COMMON.PRODUCTSUMMARYBYPROVIDER') }}'
        },
		
        xAxis: [{
            categories: categories,
            reversed: false,
            labels: {
                step: 1
            }
        }, { // mirror axis on right side
            opposite: true,
            reversed: false,
            categories: categories,
            linkedTo: 0,
            labels: {
                step: 1
            }
        }],
        yAxis: {
            title: {
                text: null
            },
            labels: {
                formatter: function () {
                    return Math.abs(this.value);
                }
            }
        },

        plotOptions: {
            series: {
                stacking: 'normal',
            }
        },

        series: [{
            name: '{{ Lang::get('COMMON.LOSS') }}',
            data: [ 
                @foreach($productSummaryByProvider as $key => $val)
					@if ($val->totalAmount>=0)
						{{$val->totalAmount*-1}},
					@elseif ($val->totalAmount<=0)
						0,
					@endif
				@endforeach	
				
				
            ],
			color: '#FF0000'
        }, {
            name: '{{ Lang::get('COMMON.PROFIT') }}',
            data: [
                
			
				@foreach($productSummaryByProvider as $key => $val)
					@if ($val->totalAmount<=0)
						{{$val->totalAmount*-1}},
					@elseif ($val->totalAmount>=0)
						0,
					@endif
				@endforeach					
				
            ],
			color: '#003471'
        }]
    });
});

		</script>
   
 <script type="text/javascript">
// Age categories
var categories1 = [
					@foreach($category1 as $key => $val)
						"{{$categories[$val->category]}}",
					@endforeach	
					];
$(document).ready(function () {
    Highcharts.chart('containerD', {
        chart: {
            type: 'bar',
			lineColor: 'red',
        },
        title: {
            text: '{{ Lang::get('COMMON.GAMETYPESUMMARY') }}'
        },
		
        xAxis: [{
            categories: categories1,
            reversed: false,
            labels: {
                step: 1
            }
        }, { // mirror axis on right side
            opposite: true,
            reversed: false,
            categories: categories1,
            linkedTo: 0,
            labels: {
                step: 1
            }
        }],
        yAxis: {
            title: {
                text: null
            },
            labels: {
                formatter: function () {
                    return Math.abs(this.value);
                }
            }
        },

        plotOptions: {
            series: {
                stacking: 'normal',
            }
        },

        series: [{
            name: '{{ Lang::get('COMMON.LOSS') }}',
            data: [
					@foreach($category1 as $key => $val)
						@if ($val->amount>=0)
							{{$val->amount*-1}},
						@elseif ($val->amount<=0)
							0,
						@endif
					@endforeach
					],
			color: '#FF0000'
        }, {
            name: '{{ Lang::get('COMMON.PROFIT') }}',
            data: [
						
					@foreach($category1 as $key => $val)
						@if ($val->amount<=0)
							{{$val->amount*-1}},
						@elseif ($val->amount>=0)
							0,
						@endif
					@endforeach	
					],
			color: '#003471'
        }]
    });
});

		</script>   

<script type="text/javascript">
// Age categories
var categories2 = [
					@foreach($topDeposit as $key => $val)
						"{{$val->nickname}}",
					@endforeach	
					];
$(document).ready(function () {
    Highcharts.chart('containerE', {
        chart: {
            type: 'bar',
			lineColor: 'red',
        },
        title: {
            text: '{{ Lang::get('COMMON.TOPDEPOSIT') }}'
        },
		
        xAxis: [{
            categories: categories2,
            reversed: true,
            labels: {
                step: 1
            }
        }],
        yAxis: {
            title: {
                text: null
            },
            labels: {
                formatter: function () {
                    return Math.abs(this.value);
                }
            }
        },

        plotOptions: {
            series: {
                stacking: 'normal',
            }
        },

        series: [{
            name: '{{ Lang::get('COMMON.DEPOSIT') }}',
            data: [
					@foreach($topDeposit as $key => $val)
						{{$val->amount}},
					@endforeach	
					],
			color: '#003471'
        }]
    });
});

		</script> 
        
<script type="text/javascript">
// Age categories
var categories3 = [
					@foreach($topWithdrawal as $key => $val)
						"{{$val->nickname}}",
					@endforeach	
					];
$(document).ready(function () {
    Highcharts.chart('containerF', {
        chart: {
            type: 'bar',
			lineColor: 'red',
        },
        title: {
            text: '{{ Lang::get('COMMON.TOPWITHDRAWAL') }}'
        },
		
        xAxis: [{
            categories: categories3,
            reversed: false,
            labels: {
                step: 1
            }
        }],
        yAxis: {
            title: {
                text: null
            },
            labels: {
                formatter: function () {
                    return Math.abs(this.value);
                }
            }
        },

        plotOptions: {
            series: {
                stacking: 'normal',
            }
        },

        series: [{
            name: '{{ Lang::get('COMMON.WITHDRAWAL') }}',
            data: [
					@foreach($topWithdrawal as $key => $val)
						{{$val->amount}},
					@endforeach	
					],	
			color: '#FF0000'
        }]
    });
});

		</script>   
        
<script type="text/javascript">
// Age categories
var categories4 = [
					@foreach($topLoser as $key => $val)
						@if ($val->amount<=0)
							"{{$val->nickname}}",
						@endif
					@endforeach	
					];
$(document).ready(function () {
    Highcharts.chart('containerW', {
        chart: {
            type: 'bar',
			lineColor: 'red',
        },
        title: {
            text: '{{ Lang::get('COMMON.TOPLOSER') }}'
        },
		
        xAxis: [{
            categories: categories4,
            reversed: true,
            labels: {
                step: 1
            }
        }],
        yAxis: {
            title: {
                text: null
            },
            labels: {
                formatter: function () {
                    return Math.abs(this.value);
                }
            }
        },

        plotOptions: {
            series: {
                stacking: 'normal',
            }
        },

        series: [{
            name: '{{ Lang::get('COMMON.LOSER') }}',
            data: [
					@foreach($topLoser as $key => $val)
						@if ($val->amount<=0)
							{{$val->amount}},
						@endif
					@endforeach	
				],
			color: '#003471'
        }]
    });
});

		</script>
        
<script type="text/javascript">
// Age categories
var categories5 = [
					@foreach($topWinner as $key => $val)
						@if ($val->amount>=0)
							"{{$val->nickname}}",
						@endif
					@endforeach	
				];
$(document).ready(function () {
    Highcharts.chart('containerL', {
        chart: {
            type: 'bar',
			lineColor: 'red',
        },
        title: {
            text: '{{ Lang::get('COMMON.TOPWINNER') }}'
        },
		
        xAxis: [{
            categories: categories5,
            reversed: true,
            labels: {
                step: 1
            }
        }],
        yAxis: {
            title: {
                text: null
            },
            labels: {
                formatter: function () {
                    return Math.abs(this.value);
                }
            }
        },

        plotOptions: {
            series: {
                stacking: 'normal',
            }
        },

        series: [{
            name: '{{ Lang::get('COMMON.WINNER') }}',
            data: [
					@foreach($topWinner as $key => $val)
						@if ($val->amount>=0)
							{{$val->amount}},
						@endif
					@endforeach	
				],
			color: '#FF0000'
        }]
    });
});

		</script>
        
        <script type="text/javascript">
            $(function() {
                //Datemask dd/mm/yyyy
                $("#datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
                //Datemask2 mm/dd/yyyy
                $("#datemask2").inputmask("mm/dd/yyyy", {"placeholder": "mm/dd/yyyy"});
                //Money Euro
                $("[data-mask]").inputmask();

                //Date range picker
                $('#reservation').daterangepicker();
                //Date range picker with time picker
                $('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A'});
                //Date range as a button
                $('#daterange-btn').daterangepicker(
                        {
                            ranges: {
                                'Today': [moment(), moment()],
                                'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
                                'Last 7 Days': [moment().subtract('days', 6), moment()],
                                'Last 30 Days': [moment().subtract('days', 29), moment()],
                                'This Month': [moment().startOf('month'), moment().endOf('month')],
                                'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
                            },
                            startDate: moment().subtract('days', 29),
                            endDate: moment()
                        },
                function(start, end) {
                    $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                }
                );

                //iCheck for checkbox and radio inputs
                $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                    checkboxClass: 'icheckbox_minimal',
                    radioClass: 'iradio_minimal'
                });
                //Red color scheme for iCheck
                $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
                    checkboxClass: 'icheckbox_minimal-red',
                    radioClass: 'iradio_minimal-red'
                });
                //Flat red color scheme for iCheck
                $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                    checkboxClass: 'icheckbox_flat-red',
                    radioClass: 'iradio_flat-red'
                });

                //Colorpicker
                $(".my-colorpicker1").colorpicker();
                //color picker with addon
                $(".my-colorpicker2").colorpicker();

                //Timepicker
                $(".timepicker").timepicker({
                    showInputs: false
                });
				
            });
        </script>
@stop
@section('bottom_js')




@stop