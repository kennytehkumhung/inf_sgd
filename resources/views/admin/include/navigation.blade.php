<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom:0">
	<div class="navbar-header">
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</button>
		<a class="navbar-brand" href="index.html">Admin</a>
	</div>
	
	<!-- /.navbar-header -->
	<ul class="nav navbar-top-links navbar-right">
		<!-- /.dropdown -->
		<li>
			<a href=""><img src="{{url()}}/front/img/en.png"></a>
		</li>
		<li>
			<a href=""><img src="{{url()}}/front/img/cn.png"></a>
		</li>
		<li class="dropdown">
			<a class="dropdown-toggle" data-toggle="dropdown" href="#">
				<i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
			</a>
			<ul class="dropdown-menu dropdown-user">
				<li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a></li>
				<li><a href="#" onClick="addTab('Change Password','password')"><i class="fa fa-gear fa-fw"></i> Change Password</a></li>
				<li class="divider"></li>
				<li><a href="{{route('admin_logout')}}"><i class="fa fa-sign-out fa-fw"></i> Logout</a></li>
			</ul>
			<!-- /.dropdown-user -->
		</li>
		<!-- /.dropdown -->
	</ul>
	<!-- /.navbar-top-links -->

	<div class="navbar-default sidebar" role="navigation">
		<div class="sidebar-nav navbar-collapse">		
			<ul class="nav" id="side-menu">
			@foreach($categorys as $categoryname => $module)
				<li>
					<a href="#">{{Lang::get('COMMON.'.$categoryname)}}<span class="fa arrow"></span></a>
					<ul class="nav nav-second-level">
						@foreach($module as $modulename => $value)
							<li>
								@if( isset($value['popout']) )
									<a href="#" onClick="window.open('{{$value['mdl']}}', '{{Lang::get('COMMON.'.$modulename)}}', 'width=1150,height=830')">Instant Transaction</a>
								@else
									<a href="#" onClick="addTab('{{Lang::get('COMMON.'.$modulename)}}','{{$value['mdl']}}')">{{Lang::get('COMMON.'.$modulename)}}</a>
								@endif
							</li>
						@endforeach		
					</ul>
				</li>	
			@endforeach				
			</ul>
		</div>
		<!-- /.sidebar-collapse -->
	</div>
	<!-- /.navbar-static-side -->
</nav>