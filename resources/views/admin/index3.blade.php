<!DOCTYPE html>
<html lang="en">
<head>
    <title id='Description'></title>
    <link rel="stylesheet" href="jqwidgets/styles/jqx.base.css" type="text/css" />
    <link rel="stylesheet" href="jqwidgets/styles/jqx.fresh.css" type="text/css" />
    <script type="text/javascript" src="jqwidgets/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="jqwidgets/jqx-all.js"></script>
<style>
#jqxNavigationBar{
	cursor: pointer;
}
</style>
</head>
<body>
    <div id='content'>
        <script type="text/javascript">
            $(document).ready(function () {
           
				// create jqxTabs.
				$('#jqxTabs').jqxTabs({ height: 'auto', width: 'auto',  showCloseButtons: true ,  theme:'fresh' });
				
				$('#jqxTabs').on('removed', function (event) { 
	
					tabs[event.args.title] = true;
				}); 
				$("#jqxNavigationBar").jqxNavigationBar({ width: 200, height: 600, theme:'fresh', expandMode: 'toggle'});
				
            });
			var tabs = new Array(); 
			function addTab(title,url){

				if ( tabs[title] == undefined ||  tabs[title] == true ) {
					tabs[title] = false;
					$.get( url, function( data ) {
					 $('#jqxTabs').jqxTabs({ selectedItem: 1 }); 
					 $('#jqxTabs').jqxTabs('addLast', title, data);
					});
				}
				
			}
			
			 
        </script>
        <div id='jqxWidget' >
     
		
			<div id='jqxNavigationBar' style="float: left;">
				<div>Transaction</div>
				<div>
					<ul>
						<li onClick="addTab('Member','<?php echo url();?>/widget/demos/jqxgrid/defaultfunctionality.htm?asdqww')">Instant Transaction</li>
						<li onClick="addTab('Member2','<?php echo url();?>/widget/demos/jqxgrid/defaultfunctionality2.htm?QWe21312')">Transaction Enquiry</li>
						<li>Adjustment</li>
						<li>Failed Transfer</li>
					</ul>
				</div>
				<div>Member</div>
				<div>
					<ul>
						<li onClick="addTab('Member2','member')">Transaction Enquiry</li>
						<li onClick="addTab('Member3','member')">Transaction Enquiry</li>
					</ul>
				</div>
				<div>Report</div>
				<div>
					<ul>
						<li>Profit Loss</li>
					</ul>
				</div>	
				<div>Agent</div>
				<div>
					<ul>
						<li>list</li>
					</ul>
				</div>

			</div>
			
			<div id='jqxTabs' style="float: left;">
				<ul style="margin: 30px;" id="unorderedList">
					<li>Node.js</li>
					
				</ul>
	
				<div>
				</div>
			</div>
        </div>
    </div>
</body>
</html>