<!DOCTYPE html>
<html lang="en">
<head>
    <title id='Description'>{{ Lang::get('COMMON.SUMMARY') }}
    </title>
    <link rel="stylesheet" href="{{url()}}/admin/jqwidgets/styles/jqx.base.css" type="text/css" />
    <link rel="stylesheet" href="{{url()}}/admin/jqwidgets/styles/jqx.bootstrap.css" type="text/css" />
	<script type="text/javascript" src="{{url()}}/admin/jqwidgets/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="{{url()}}/admin/jqwidgets/jqx-all.js"></script>
	
	
	<!-- Bootstrap Core JavaScript -->
	<script src="{{url()}}/admin/css/sb_bootstrap/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

	<!-- Bootstrap Core CSS -->
	<link href="{{url()}}/admin/css/sb_bootstrap/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

	<!-- Custom Fonts -->
	<link href="{{url()}}/admin/css/sb_bootstrap/bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <style>
        .fl{
                float: left;
        }
        .label-margin{
                padding-top:5px;
                margin-left: 10px;
                margin-right: 5px;
        }
        .clear{
                clear: both;
        }
        .formLabelLeft{
                width: 150px;
                padding-right: 10px;
                vertical-align: top;
                font-weight: bold;
        }  
		.figure{
                 text-align: right;
        }
    </style>
	<script>
	 $(document).ready(function () {
		 $("#datefrom").jqxDateTimeInput({  width: '120px' , height: '25px' , formatString: "yyyy-MM-dd" ,openDelay: 0,closeDelay: 0});
		 $("#dateto").jqxDateTimeInput({  width: '120px' , height: '25px' , formatString: "yyyy-MM-dd" ,openDelay: 0,closeDelay: 0});
		 
		 var t = "{{$createdfrom}}".split(/[- :]/);
		 $('#datefrom').jqxDateTimeInput('setDate', new Date(t[0], t[1]-1, t[2]));
		 
		 var t = "{{$createdto}}".split(/[- :]/);
		 $('#dateto').jqxDateTimeInput('setDate', new Date(t[0], t[1]-1, t[2]));
				
		 $("#form_submit_btn").jqxButton({ width: '150', template: "success"});
			$("#form_submit_btn").on('click', function () {
                   $("#events").find('span').remove();
                   $("#events").append('<span>Submit Button Clicked</span>');
             });
	 });
	 function submit_form(){

		 window.location.href = "{{ action('Admin\ReportTotalController@show') }}?datefrom="+$('#datefrom').jqxDateTimeInput('getText')+"&dateto="+$('#dateto').jqxDateTimeInput('getText')+'&crccode='+$('#crccode').val();
	 }
	</script>
</head>
<body class='default'> 
	<div id="tb" style="padding:5px;height:auto;margin-left: 20px;margin-right: 20px; ">	
		<div>
			<table>
				<tr>
					<td>Date: </td>
					<td><div id="datefrom" style="float:left;"></div></td>
					<td>~</td>
					<td><div id="dateto" style="float:left;"></div></td>
				</tr>
				Currency: 
				<select id="crccode">
                                    @foreach( $currencies as $code => $currency)
                                        <option value="{{$currency}}" >{{$currency}}</option>
                                    @endforeach
                                </select>
			</table>
			<div class="clear">
				<br>
				<input type="submit" class="btn btn-sm btn-success" value="Submit" id='form_submit_btn' onClick="submit_form()"/>
				<br>
			</div>		
			
			
		</div>
	</div>

<table width="100%">
	<tr>
		<td colspan="2" valign="top">
			<div>
				<form action="index.php" method="GET" id="cboForm">
					<table width="100%">
						
					</table>
				</form>
			</div>
		</td>
	</tr>
	
	<tr>
		<td colspan="2" valign="top">
			<div class="panel panel-primary" style="margin-left: 20px;margin-right: 20px; ">
				<div class="panel-heading">{{ Lang::get('public.WalletSummary') }}</div>
				<div class="panel-body">
					<table class="table table-hover table-bordered">
						<thead>
							<tr>
								<th field="currency">{{Lang::get('COMMON.CURRENCY')}}</th>
								<th class="figure" field="member">{{Lang::get('COMMON.MEMBER')}}</th>
								<th class="figure" field="register">{{Lang::get('COMMON.REGISTRATION')}}</th>
								<th class="figure" field="main">{{Lang::get('COMMON.MAINWALLET')}}</th>
							@foreach( $products as $key => $product)
								<th class="figure" field="{{$product}}">{{$product}}</th>
							@endforeach
								<th class="figure" field="balance">{{Lang::get('COMMON.BALANCE')}}</th>
							</tr>
						</thead>
						<tbody>
							@foreach( $currencies as $code => $currency)
							<tr>
								<td field="currency">{{$currency}}</td>
								<td class="figure" field="member">{{$members[$code]}}</td>
								<td class="figure" field="register">{{$registers[$code]}}</td>
								<td class="figure" field="main">{{$balances[$code][0]}}</td>
							@foreach( $products as $prdid => $product)
								<td class="figure" field="{{$product}}">{{$balances[$code][$prdid]}}</td>
							@endforeach
								<td class="figure" field="balance">{{$balances[$code]['total']}}</td>
							</tr>
							@endforeach				
						</tbody>
					</table>
				</div>
			</div>
			@if(Config::get('setting.front_path') == 'sbm')
			<div class="panel panel-primary" style="margin-left: 20px;margin-right: 20px;width:500px; ">
				<div class="panel-heading">Current Wallet Balance</div>
				<div class="panel-body">
					<table class="table table-hover table-bordered" width="600px">
						<thead>
							<tr>
								<th class="figure" field="currency">Main Wallet</th>
								<th class="figure" field="member">ASC</th>
								<th class="figure" field="register">M8B</th>
								<th class="figure" field="register">Total</th>
							</tr>
						</thead>
						<tbody>
						
							<tr>
								<td class="figure" field="currency">{{$mainwallet}}</td>
								<td class="figure" field="member">{{$asc}}</td>
								<td class="figure" field="register">{{$m8b}}</td>
								<td class="figure" field="register">{{$currentbalancetotal}}</td>
							</tr>
									
						</tbody>
					</table>
				</div>
			</div>
			@endif

			<div class="panel panel-primary" style="margin-left: 20px;margin-right: 20px; ">
				<div class="panel-heading">Report</div>
				<div class="panel-body">
					<table class="table table-hover table-bordered">
						<thead>
							<tr>
								<th field="product">{{Lang::get('COMMON.VENDOR')}}</th>
								<th field="currency">{{Lang::get('COMMON.CURRENCY')}}</th>
								<th class="figure" field="member">{{Lang::get('COMMON.MEMBER')}}</th>
								<th class="figure" field="wager">{{Lang::get('COMMON.WAGERNUM')}}</th>
								<th class="figure" field="turnover">{{Lang::get('COMMON.TURNOVER')}}</th>
								<th class="figure" field="valid">{{Lang::get('COMMON.VALIDSTAKE')}}</th>
								<th class="figure" field="winloss">{{Lang::get('COMMON.MEMBERPNL')}}</th>
							</tr>
						</thead>
						<tbody>
							@foreach( $currencies as $code => $currency)
							@foreach( $products as $prdid => $product)
							<tr>
								<td field="product">{{$product}}</td>
								<td field="currency">{{$currency}}</td>
								<td class="figure" field="member">{{$profitloss[$prdid][$currency]['member']}}</td>
								<td class="figure" field="wager">{{$profitloss[$prdid][$currency]['wager']}}</td>
								<td class="figure" field="turnover">{{$profitloss[$prdid][$currency]['totalstake']}}</td>
								<td class="figure" field="valid">{{$profitloss[$prdid][$currency]['validstake']}}</td>
								<td class="figure" field="winloss"><?php echo htmlspecialchars_decode($profitloss[$prdid][$currency]['winloss']); ?></td>
							</tr>		
							@endforeach
							<tr>
								<td field="product">{{Lang::get('COMMON.TOTAL')}}</td>
								<td field="currency">{{$currency}}</td>
								<td class="figure" field="member">{{$pnltotal[$currency]['member']}}</td>
								<td class="figure" field="wager">{{$pnltotal[$currency]['wager']}}</td>
								<td class="figure" field="turnover">{{$pnltotal[$currency]['totalstake']}}</td>
								<td class="figure" field="valid">{{$pnltotal[$currency]['validstake']}}</td>
								<td class="figure" field="winloss"><?php echo htmlspecialchars_decode($pnltotal[$currency]['winloss']); ?></td>
							</tr>
							@endforeach				
						</tbody>
					</table>
				</div>
			</div>
		</td>
	</tr>
	
	<tr>
		<td width="50%" valign="top">
			<div class="panel panel-primary" style="margin-left: 20px;margin-right: 20px; ">
				<div class="panel-heading">{{ Lang::get('COMMON.TRANSACTION') }}</div>
				<div class="panel-body">
					<table class="table table-hover table-bordered">
						<thead>
							<tr>
								<th field="currency">{{Lang::get('COMMON.CURRENCY')}}</th>
								<th field="type">{{Lang::get('COMMON.TYPE')}}</th>
								<th class="figure" field="transaction">{{Lang::get('COMMON.TRANSACTION')}}</th>
								<th class="figure" field="amount">{{Lang::get('COMMON.AMOUNT')}}</th>
								<th class="figure" field="fee">{{Lang::get('COMMON.FEE')}}</th>
							</tr>
						</thead>
						<tbody>
						@foreach( $currencies as $code => $currency)
						@foreach( $cashledgers[$currency] as $id => $cashledger)
							<tr>
								<td field="currency">{{$currency}}</td>
								<td field="type">{{$cashledger['name']}}</td>
								<td class="figure" field="transaction">{{$cashledger['total']}}</td>
								<td class="figure" field="amount"><?php echo htmlspecialchars_decode($cashledger['amount']) ?></td>
								<td class="figure" field="fee"><?php echo htmlspecialchars_decode($cashledger['fee']) ?></td>
							</tr>			
						@endforeach			
						@endforeach			
						</tbody>
					</table>
				</div>
			</div>
		</td>
	
		<td  width="50%" valign="top">
			<div class="panel panel-primary" style="margin-left: 20px;margin-right: 20px; ">
				<div class="panel-heading">{{ Lang::get('COMMON.BONUS') }}</div>
				<div class="panel-body">
					<table class="table table-hover table-bordered">
						<thead>
							<tr>
								<th field="bonus">{{Lang::get('COMMON.BONUS')}}</th>
								<th field="currency">{{Lang::get('COMMON.CURRENCY')}}</th>
								<th field="applied">{{Lang::get('COMMON.APPLIED')}}</th>
								<th field="approved">{{Lang::get('COMMON.APPROVED')}}</th>
								<th field="continue">{{Lang::get('COMMON.CONTDEPOSIT')}}</th>
								<th field="amount">{{Lang::get('COMMON.AMOUNT')}}</th>
							</tr>
						</thead>
						<tbody>
						@foreach( $bonuses as $id => $bonus)
							<tr>
								<td field="bonus"><?php echo htmlspecialchars_decode($bonus['name']) ?></td>
								<td field="currency">{{$bonus['currency']}}</td>
								<td class="figure" field="applied">{{$bonus['apply']}}</td>
								<td class="figure" field="approved">{{$bonus['apply']}}</td>
								<td class="figure" field="continue">{{$bonus['continuedep']}}</td>
								<td class="figure" field="amount">{{$bonus['amount']}}</td>
							</tr>			
						@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</td>
	</tr>
</table>
</body>
</html>
