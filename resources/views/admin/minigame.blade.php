<link rel="stylesheet" href="{{ URL('/') }}/admin/lte/bootstrap/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="{{ URL('/') }}/admin/lte/dist/css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
     folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="{{ URL('/') }}/admin/lte/dist/css/skins/_all-skins.min.css">
<link rel="stylesheet" href="{{url()}}/admin/jqwidgets/styles/jqx.base.css" type="text/css" />
<link rel="stylesheet" href="{{url()}}/admin/jqwidgets/styles/jqx.bootstrap.css" type="text/css" />
<script type="text/javascript" src="{{url()}}/admin/jqwidgets/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="{{url()}}/admin/jqwidgets/jqx-all.js"></script>
<script type="text/javascript" src="{{url()}}/admin/js/general.js"></script>
<script src="{{url()}}/gsc/resources/js/distpicker.data.js"></script>
<script src="{{url()}}/gsc/resources/js/distpicker.js"></script>
<script src="{{url()}}/gsc/resources/js/main.js"></script>

    <script>
        $("input").on("change", function() {
            this.setAttribute(
                "data-date",
                moment(this.value, "YYYY-MM-DD")
                    .format( this.getAttribute("data-date-format") )
            )
        }).trigger("change");

        function submitDate(){
            if (confirm('Confirm')) {
                $.ajax({
                    type: "POST",
                    url: "{{action('Admin\ConfigController@minigamedate')}}",
                    data: {
                        method:"post",
                        startdate:$('#startdate').val(),
                        enddate:$('#enddate').val(),
                        eighty:$('#eighty').val(),
                        ten:$('#ten').val(),
                        five:$('#five').val(),
                        three:$('#three').val(),
                        two:$('#two').val(),

                    }
                }).done(function( json ) {
                    obj = JSON.parse(json);
                    var str = '';
                    $.each(obj, function(i, item) {
                        str += item + '\n';
                    });
                    alert(str);
                });
            }
        }
    </script>
<body style="background-color:#ecf0f5;">
<br>
<div class="col-md-6">
    <!-- Horizontal Form -->
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Mini game date</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form class="form-horizontal">
            <div class="box-body">
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Start date</label>
                    <div class="col-sm-10">
                        <input type="datetime-local" class="form-control" id="startdate" value="{{$proValue['startdate']}}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputPassword3" class="col-sm-2 control-label">End date</label>

                    <div class="col-sm-10">
                        <input type="datetime-local" class="form-control" id="enddate" value="{{$proValue['enddate']}}">
                    </div>
                </div>
            </div>
        </form>
        <div class="box-header with-border">
            <h3 class="box-title">Prize Probability</h3>
        </div>
        <form class="form-horizontal">
            <div class="box-body">
                <div class="form-group">
                    <label for="inputPassword3" class="col-sm-2 control-label">80%</label>

                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="eighty" value="{{$proValue['eighty']}}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputPassword3" class="col-sm-2 control-label">10%</label>

                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="ten" value="{{$proValue['ten']}}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputPassword3" class="col-sm-2 control-label">5%</label>

                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="five" value="{{$proValue['five']}}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputPassword3" class="col-sm-2 control-label">3%</label>

                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="three" value="{{$proValue['three']}}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputPassword3" class="col-sm-2 control-label">2%</label>

                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="two" value="{{$proValue['two']}}">
                    </div>
                </div>
            </div>
        </form>

        <div class="box-footer">
            <button type="button" class="btn btn-info pull-right" id='form_submit_btn' onClick="submitDate()" >{{ Lang::get('COMMON.SUBMIT') }}</button>
        </div>
    </div>
</div>
</body>