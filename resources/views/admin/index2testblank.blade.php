@extends('admin/master_lte')

@section('title', 'Admin Dashboard')

@section('top_js')
    <link rel="stylesheet" href="{{ URL('/') }}/admin/jqwidgets/styles/jqx.base.css" type="text/css" />
    <link rel="stylesheet" href="{{ URL('/') }}/admin/jqwidgets/styles/jqx.metro.css" type="text/css" />
    {{--<script type="text/javascript" src="{{ URL('/') }}/admin/jqwidgets/jqx-all.js"></script>--}}

    <script type="text/javascript" src="{{ URL('/') }}/admin/jqwidgets/jqxcore.js"></script>
    <script type="text/javascript" src="{{ URL('/') }}/admin/jqwidgets/jqxdraw.js"></script>
    <script type="text/javascript" src="{{ URL('/') }}/admin/jqwidgets/jqxchart.core.js"></script>
    <script type="text/javascript" src="{{ URL('/') }}/admin/jqwidgets/jqxdata.js"></script>
    <script type="text/javascript" src="{{ URL('/') }}/admin/jqwidgets/jqxtabs.js"></script>
    <link href="{{ URL('/') }}/admin/new_design/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="{{ URL('/') }}/admin/new_design/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="{{ URL('/') }}/admin/new_design/ionicons.min.css" rel="stylesheet" type="text/css" />
    <link href="{{ URL('/') }}/admin/new_design/AdminLTE.css" rel="stylesheet" type="text/css" />
    <link href="{{ URL('/') }}/admin/new_design/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
    <link href="{{ URL('/') }}/admin/new_design/datepicker3.css" rel="stylesheet" type="text/css" />
    <link href="{{ URL('/') }}/admin/new_design/bootstrap-timepicker.min.css" rel="stylesheet"/>
    <style>
        canvas {
            -moz-user-select: none;
            -webkit-user-select: none;
            -ms-user-select: none;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            var h = $(window).height() - 53;
            // create jqxTabs.
            $('#mastertab').jqxTabs({ height: h, width: '100%',  autoHeight: false, showCloseButtons: true ,  theme:'ui-start' });

            $('#mastertab').on('removed', function (event) {

                tabs[event.args.title] = true;
            });
            //$("#jqxNavigationBar").jqxNavigationBar({ width: 200, height: 600, theme:'fresh', expandMode: 'toggle'});

            $("body").addClass("skin-blue");
        });
        var tabs = new Array();
        var tabs_record = new Array();
        function addTab(title,url){

            for(count = 0; count < 20; count++){
                text = $('#mastertab').jqxTabs('getTitleAt', count);
                if(text == title)
                {
                    $('#mastertab').jqxTabs('removeAt', count);
                }
            }

            if ( tabs[title] == undefined ||  tabs[title] == true ) {
                tabs[title] = false;

                $('#mastertab').jqxTabs({ selectedItem: 1 });
                $('#mastertab').jqxTabs('addLast', title, ' <iframe frameBorder="0" src="'+url+'" style="overflow:hidden;overflow-x:hidden;overflow-y:hidden;height: 90%; width:100%;"></iframe> ');

            }




        }
        function closeTab(){

            $('#mastertab').jqxTabs('removeAt', $('#mastertab').jqxTabs('selectedItem'));


        }


    </script>

    <script type="text/javascript">
        $(document).ready(function() {
            load_message();
        });
        function load_message(type) {
            $.ajax({
                url: "{{ action('Admin\NotificationController@showTotalUnseenMessage') }}",
                type: "GET",
                data:{type:type},
                dataType: 'json',
                success: function (result) {
                    if(result == 0){
                        $("#totalMessage").html("");
                    }else if (result > 0){
                        $("#totalMessage").html("<b style='color: white'>"+result+"</b>");
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                }
            });
        }
        setInterval(load_message,180000);
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#jqxTabs').jqxTabs({ width: '100%',  selectionTracker: true, animationType: 'fade' });
        });
    </script>
@stop
@section('content')

    <div id='mastertab' style="float: left; width:100%; height:100%;">
        <ul style="margin: 30px;" id="unorderedList">
            <li>{{ Lang::get('public.Dashboard') }}</li>
        </ul>
        <div>
            <div class="wrapper row-offcanvas row-offcanvas-left">


                <!-- Main content -->


            </div><!-- ./wrapper -->
        </div>
        <!-- jQuery 2.0.2 -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
    <!-- Moment JS -->
    <script src="{{ URL('/') }}/admin/new_design/moment.js" type="text/javascript"></script>
    {{--
    <!-- Chart JS -->
    <script src="{{ URL('/') }}/admin/new_design/Chart.bundle.js" type="text/javascript"></script>
    --}}
    <!-- Util JS -->
        <script src="{{ URL('/') }}/admin/new_design/util.js" type="text/javascript"></script>
    <!-- InputMask -->
        <script src="{{ URL('/') }}/admin/new_design/jquery.inputmask.js" type="text/javascript"></script>
        <script src="{{ URL('/') }}/admin/new_design/jquery.inputmask.date.extensions.js" type="text/javascript"></script>
        <script src="{{ URL('/') }}/admin/new_design/jquery.inputmask.extensions.js" type="text/javascript"></script>
        <!-- date-range-picker -->
        <script src="{{ URL('/') }}/admin/new_design/daterangepicker.js" type="text/javascript"></script>
        <!-- date-range-picker -->
        <script src="{{ URL('/') }}/admin/new_design/bootstrap-datepicker.js" type="text/javascript"></script>
        <!-- bootstrap time picker -->
        <script src="{{ URL('/') }}/admin/new_design/bootstrap-timepicker.min.js" type="text/javascript"></script>
        <!-- highchart -->
        <script src="{{ URL('/') }}/admin/new_design/highcharts.js"></script>
        <script src="{{ URL('/') }}/admin/new_design/exporting.js"></script>


@stop
@section('bottom_js')




@stop