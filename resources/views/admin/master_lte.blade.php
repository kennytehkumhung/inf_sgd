﻿<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{Config::get('setting.website_name')}} | {{ Lang::get('COMMON.ADMIN') }}</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="{{ URL('/') }}/admin/lte/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ URL('/') }}/admin/lte/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{ URL('/') }}/admin/lte/dist/css/skins/_all-skins.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

  </head>

  <body class="hold-transition skin-black">
    <!-- Site wrapper -->
    <div class="wrapper">

      <header class="main-header">
        <!-- Logo -->
        <a href="#" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>{{Config::get('setting.website_name')}}</b></span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>{{Config::get('setting.website_name')}}</b></span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
		  <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <li class="dropdown" id="notifications-header" onClick="load_message(true)">
                <a href="#" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                  <i class="fa fa-bell"></i><span class="badge badge-danger badge-header" style="background-color: red;margin-left: -5px;" id="totalMessage"></span>
                </a>
                <ul class="dropdown-menu">
                  <li class="dropdown-header clearfix"><p class="pull-left">Notifications</p></li>
                  @foreach ($newUser as $key => $values)
                    <li>
                      <a href="#" style="color: #000;">
                        <i class="fa fa-user"></i> <strong>{{ Lang::get('public.NewUser') }} </strong><br>
                        {{ Lang::get('COMMON.NEWUSERNOTICE',['name' => $values['fullname'],'created' => $values['created']]) }}
                      </a>
                    </li>
                  @endforeach
                </ul>
              </li>
            </ul>
            <ul class="nav navbar-nav">
              <!-- Messages: style can be found in dropdown.less-->
              <li class="dropdown">
                <a href="{{route( 'languages', [ 'lang'=> 'en'])}}" class="dropdown-toggle">
                  <img src="{{url()}}/admin/images/en.png">
                </a>
              </li>
              <!-- Notifications: style can be found in dropdown.less -->
              <li class="dropdown">
                <a href="{{route( 'languages', [ 'lang'=> 'th'])}}" class="dropdown-toggle">
                  <img src="{{url()}}/admin/images/th.png">
                </a>
              </li> 
			  <li class="dropdown">
                <a href="{{route( 'languages', [ 'lang'=> 'cn'])}}" class="dropdown-toggle">
                  <img src="{{url()}}/admin/images/cn.png">
                </a>
              </li>

              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="glyphicon glyphicon-user"></i> <span class="hidden-xs">{{ Auth::admin()->user()->username }}</span>
                </a>
                <ul class="dropdown-menu">
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="#" class="btn btn-default btn-flat" onClick="addTab('Change Password','password')">{{ Lang::get('COMMON.CHANGEPASS') }}</a>
                    </div>
                    <div class="pull-right">
                      <a href="{{route('admin_logout')}}" class="btn btn-default btn-flat">{{ Lang::get('COMMON.LOGOUT') }}</a>
                    </div>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </nav>
      </header>

      <!-- =============================================== -->

      <!-- Left side column. contains the sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->

          <!-- sidebar menu: : style can be found in sidebar.less -->

          <ul class="sidebar-menu">
			@foreach($categorys as $categoryname => $module)
				<li class="treeview">
					<a href="#">{{Lang::get('COMMON.'.$categoryname)}}<span class="fa arrow"></span></a>
					<ul class="treeview-menu">
						@foreach($module as $modulename => $value)
							<li>
								@if( isset($value['popout']) )
									<a href="#" onClick="window.open('{{$value['mdl']}}', '{{Lang::get('COMMON.'.$modulename)}}', 'width=1150,height=830')">{{Lang::get('COMMON.INSTANT_TRANSACTION')}}</a>
								@else
									<a href="#" onClick="addTab('{{Lang::get('COMMON.'.$modulename)}}','{{$value['mdl']}}')">{{Lang::get('COMMON.'.$modulename)}}</a>
								@endif
							</li>
						@endforeach
					</ul>
				</li>
			@endforeach

          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>

      <!-- =============================================== -->

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        @yield('content')
      </div><!-- /.content-wrapper -->


      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->

    <!-- jQuery 2.1.4 -->
    <script src="{{ URL('/') }}/admin/lte/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="{{ URL('/') }}/admin/lte/bootstrap/js/bootstrap.min.js"></script>
    <!-- SlimScroll -->
    <script src="{{ URL('/') }}/admin/lte/plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="{{ URL('/') }}/admin/lte/plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="{{ URL('/') }}/admin/lte/dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="{{ URL('/') }}/admin/lte/dist/js/demo.js"></script>
	
	@yield('top_js')
  </body>
</html>
