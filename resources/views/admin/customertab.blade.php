<!DOCTYPE html>
<html lang="en">
<head>
    <title id='Description'></title>
    <link rel="stylesheet" href="{{url()}}/admin/jqwidgets/styles/jqx.base.css" type="text/css" />
    <link rel="stylesheet" href="{{url()}}/admin/jqwidgets/styles/jqx.bootstrap.css" type="text/css" />
    <script type="text/javascript" src="{{url()}}/admin/jqwidgets/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="{{url()}}/admin/jqwidgets/jqx-all.js"></script>

    <script type="text/javascript">
	 $(document).ready(function () {
            // Create jqxTabs.

            $('#jqxTabs').jqxTabs({ width: '100%', height: '700px', position: 'top',theme:'bootstrap'});
            var loadPage = function (url, tabIndex) {
                $.get(url, function (data) {
                    $('#content' + tabIndex).html('<iframe frameBorder="0" src="'+url+'" style="overflow:hidden;overflow-x:hidden;overflow-y:hidden;height: 90%; width:100%;"></iframe>');
                });
            }
            loadPage('customer-tab1?sid={{$sid}}', 1);
            $('#jqxTabs').on('selected', function (event) {
                var pageIndex = event.args.item + 1;
                loadPage('customer-tab' + pageIndex+'?sid={{$sid}}' , pageIndex);
            });
			
        });
    </script>
	<style>
	.fl{
		float: left;
	}
	.label-margin{
		padding-top:5px;
		margin-left: 10px;
		margin-right: 5px;
	}
	.clear{
		clear: both;
	}
	</style>
</head>
<body class='default'>
    <div id='jqxWidget'>
        <div id='jqxTabs'>
            <ul>
                <li style="margin-left: 30px;">{{ Lang::get('COMMON.ACCOUNTDETAILS') }}</li>
                <li>{{ Lang::get('COMMON.TRANSACTION') }}</li>
                <li>{{ Lang::get('COMMON.MEMBERPNL') }}</li>
                <li>{{ Lang::get('COMMON.TOOL') }}</li>
            </ul>
			
            <div id="content1"></div>
            <div id="content2"></div>
            <div id="content3"></div>  
            <div id="content4"></div>
        </div>   
    </div>
</body>
</html>
