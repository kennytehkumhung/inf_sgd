@extends('demo/master')

@section('title', 'Malaysia Online Live Casino | avxdemo')
@section('keywords', 'Malaysia Online Live Casino | avxdemo')
@section('description', 'Enjoy live casino games with live lobby view, beautiful real human dealers. Enjoy live Sic Bo, live baccarat, live slots etc.')

@section('top_js')
<link href="{{url()}}/demo/resources/css/jquery.bxslider.css" type="text/css" media="all" rel="stylesheet" />

<script type="text/javascript" language="javascript" src="{{url()}}/demo/resources/js/jquery.bxslider.min.js"></script>
<style>
body  {
	background-image: url({{url()}}/demo/resources/img/bg-lc.jpg);
	background-repeat: no-repeat; background-position:
	top center; background-color: #000000;}
</style>
@stop

@section('content')

<!--Slider-->
<div class="sliderCont">
<ul class="bxslider" style="cursor:pointer;">
  @if(in_array('AGG',Session::get('valid_product')))
		<li><a onClick="@if (Auth::user()->check())window.open('{{route('agg')}}', 'casino11', 'width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif"><img src="{{url()}}/demo/resources/img/banner/casino-1.png" /></a></li>
  @endif
  
  @if(in_array('PLT',Session::get('valid_product')))
		<li><a onClick="@if (Auth::user()->check())window.open('{{route('pltiframe')}}', 'casinoplt', 'width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif"><img src="{{url()}}/demo/resources/img/banner/casino-2.png" /></a></li>
  @endif
  
  @if(in_array('W88',Session::get('valid_product')))
		<li><a onClick="@if (Auth::user()->check())window.open('{{route('w88', [ 'type' => 'casino' , 'category' => 'live' ] )}}', 'casino12', 'width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif"><img src="{{url()}}/demo/resources/img/banner/casino-3.png" /></a></li>
  @endif
  
  @if(in_array('MXB',Session::get('valid_product')))
		<li><a onClick="@if (!Auth::user()->check())alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif" href="@if (Auth::user()->check()){{route('mxb',[ 'type' => 'casino' , 'category' => 'baccarat' ])}}@endif"><img src="{{url()}}/demo/resources/img/banner/casino-4.png" /></a></li>
  @endif
  
  @if(in_array('ALB',Session::get('valid_product')))
		<li><a onClick="@if (Auth::user()->check())window.open('{{route('alb', [ 'type' => 'casino' ] )}}', 'casino12', 'width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif" ><img src="{{url()}}/demo/resources/img/banner/casino-5.png" /></a></li>
  @endif
</ul>

<div id="bx-pager">
  <?php $count = 0; ?> 
  @if(in_array('AGG',Session::get('valid_product')))
	<a data-slide-index="{{$count++}}" href=""><img src="{{url()}}/demo/resources/img/lc-ag-thumb.png" /></a>
  @endif
  
  @if(in_array('PLT',Session::get('valid_product')))
	<a data-slide-index="{{$count++}}" href=""><img src="{{url()}}/demo/resources/img/lc-pt-thumb.png" /></a>
  @endif
  
  @if(in_array('W88',Session::get('valid_product')))
	<a data-slide-index="{{$count++}}" href=""><img src="{{url()}}/demo/resources/img/lc-gp-thumb.png" /></a>
  @endif
  
  @if(in_array('MXB',Session::get('valid_product')))
	<a data-slide-index="{{$count++}}" href=""><img src="{{url()}}/demo/resources/img/lc-xpro-thumb.png" /></a>
  @endif
  
  @if(in_array('ALB',Session::get('valid_product')))
	<a data-slide-index="{{$count++}}" href=""><img src="{{url()}}/demo/resources/img/lc-allbet-thumb.png" /></a>
  @endif
  
</div>
</div>
<!--Slider-->

@stop

@section('bottom_js')
<script>
    $('.bxslider').bxSlider({
  pagerCustom: '#bx-pager'
});
</script>
<script src="{{url()}}/demo/resources/js/slick.min.js"></script>
<script>
$('.caro').slick({
  infinite: true,
  slidesToShow: 2,
  slidesToScroll: 2,
  autoplay: true,
  variableWidth: true,
  arrows: false
});
</script>
@stop