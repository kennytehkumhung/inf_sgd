@extends('demo/master')

@section('title', 'Mobile')

@section('top_js')
	<script type="text/javascript" language="javascript">
		$(document).ready(function() {
			$('.box_skitter_large').skitter({
				theme: 'clean',
				numbers_align: 'center',
				progressbar: false, 
				dots: true, 
				preview: false
			});
		});
	</script>
	<style>
	.modal__inner{
		width: 542px;
		height: 307px;
	}
	.mobHead  {
	margin-top: 10px;
}

.coded   {
	position: relative;
	display: block;
}

.mobBox  {
	background: #fff;
	float: left;
	display: block;
	position: relative;
	width: 233px;
	text-align: center;
	margin: 0px 13px 0px 0px;
}

.mobBoxL  {
	background: #fff;
	float: left;
	display: block;
	position: relative;
	width: 233px;
	text-align: center;
	margin: 0px 0px 0px 0px;
	padding: 7px 0px;
}

.get1  {
	position: absolute;
	display: block;
	bottom: 5px;
	left: 25px;
}

.get2  {
	position: absolute;
	display: block;
	bottom: 5px;
	right: 25px;
}

.get3  {
	position: absolute;
	display: block;
	bottom: 10px;
	left: 15px;
}



.mobGet  {
	padding: 5px 0px;
	height: 68px;
}

.mobGet2  {
	padding: 0px;
	height: 68px;
	background: url(../demo/img/ag-mid-img.jpg)no-repeat;
	display: block;
	position: relative;
}

.mobGet3  {
	padding: 0px;
	height: 68px;
	background: url(../demo/img/mob-bg-none.jpg)no-repeat;
	display: block;
	position: relative;
}

.mobContTop  {
width: 994px;
display: block;
position: relative;
height: auto;

padding: 15px;
background: #000;
}

.mobWrap  {
	width: 974px;
	height: 1600px;
	display: block;
	position: relative;
	-moz-border-radius-topleft: 15px;
-webkit-border-top-left-radius: 15px;
 border-top-left-radius: 15px;
-moz-border-radius-topright: 15px;
-webkit-border-top-right-radius: 15px;
border-top-right-radius: 15px;
   background: #47483b;
   padding: 10px;
}

.mobBx  {
	width: 180px;
	position: relative;
	display: inline-block;
	height: auto;
	float: left;
	background: #fff;
}

#button1 {position: absolute; top:102px;left:25px;}
#button2 {position: absolute; top:102px;left:140px;}
#button3 {position: absolute; top:102px;left:278px;}
#button4 {position: absolute; top:102px;left:388px;}

#button1:focus~#content #def,
#button2:focus~#content #def,
#button3:focus~#content #def,
#button4:focus~#content #def {display:none;}

#button1:focus~#content div:nth-child(2),
#button2:focus~#content div:nth-child(3),
#button3:focus~#content div:nth-child(4),
#button4:focus~#content div:nth-child(5) {display:block;}

#content {
    width:300px; 
    height:500px;
    position:absolute;
    left:0px;
    top:250px;
    font-size:10px;
    text-align:center;
 }

#faq,#her,#and,#enj {display:none;}
	</style>
	 <link href="{{url()}}/demo/resources/css/skitter.styles1.css" type="text/css" media="all" rel="stylesheet" />
@stop

@section('content')
<!--Mobile-->
<div class="mobCont">
	<img src="{{url()}}/demo/img/mob-title.jpg" width="1024" height="66">
	<div class="mobContTop">
		<div class="mobWrap">
			<div class="mobHead">
				<div class="mobBox">
				<img src="{{url()}}/demo/img/mob-ag.png" alt="">
				<div class="mobGet2">
				</div>
				<div class="mobGet">
				<div class="get1">
					<label class="btn" for="modal-1"><img src="{{url()}}/demo/img/getNow-orange.png" width="70" height="60"></label>
					<input class="modal-state" id="modal-1" type="checkbox" />
					<div class="modal">
						<label class="modal__bg" for="modal-1"></label>
						<div class="modal__inner">
							<label class="modal__close" for="modal-1"></label>
							<center><img src="{{url()}}/demo/img/ag_android_qr.jpg" width="50%" height="auto" alt=""/></center>
							
							<span style="color:black;font-size:30px;margin-left:170px;">
							@if(Session::has('username'))
								AVIW_{{Session::get('username')}}
							@else
								Please Login!
							@endif
							</span>
							
						</div>
					</div>
				</div>
				<div class="get2">
					
					<label class="btn" for="modal-2"><img src="{{url()}}/demo/img/getNow-Red.png" width="70" height="60"></label>
					<input class="modal-state" id="modal-2" type="checkbox" />
					<div class="modal">
						<label class="modal__bg" for="modal-2"></label>
						<div class="modal__inner">
							<label class="modal__close" for="modal-2"></label>
							<center><img src="{{url()}}/demo/img/ag_iphone_qr.jpg" width="50%" height="auto" alt=""/></center>
							
							<span style="color:black;font-size:30px;margin-left:170px;">
							@if(Session::has('username'))
								AVIW_{{Session::get('username')}}
							@else
								Please Login!
							@endif
							</span>
							
						</div>
					</div>
				</div>
				<div class="clr"></div>
				</div>
				</div>

				<div class="mobBox">
				<img src="{{url()}}/demo/img/mob-pt.png" alt="">
				<div class="mobGet2">

				</div>
				<div class="mobGet">
				<div class="get1">
					<label class="btn" for="modal-3"><img src="{{url()}}/demo/img/getNow-orange.png" width="70" height="60"></label>
					<input class="modal-state" id="modal-3" type="checkbox" />
					<div class="modal">
						<label class="modal__bg" for="modal-3"></label>
						<div class="modal__inner">
							<label class="modal__close" for="modal-3"></label>
							<center><img src="{{url()}}/demo/img/plt_qr_codes.png" width="100%" height="auto" alt=""/></center>
							
							<span style="color:black;font-size:30px;margin-left:170px;">
							@if(Session::has('username'))
								IFW@ {{Session::get('username')}}
							@else
								Please Login!
							@endif
							</span>
							
						</div>
					</div>
				</div>
				<div class="get2">
				
				
					<label class="btn" for="modal-4"><img src="{{url()}}/demo/img/getNow-Red.png" width="70" height="60"></label>
					<input class="modal-state" id="modal-4" type="checkbox" />
					<div class="modal">
						<label class="modal__bg" for="modal-4"></label>
						<div class="modal__inner">
							<label class="modal__close" for="modal-4"></label>
							<center><img src="{{url()}}/demo/img/plt_qr_codes.png" width="100%" height="auto" alt=""/></center>
							
							<span style="color:black;font-size:30px;margin-left:170px;">
							@if(Session::has('username'))
								IFW@ {{Session::get('username')}}
							@else
								Please Login!
							@endif
							</span>
							
						</div>
					</div>
				</div>
				<div class="clr"></div>
				</div>
				</div>

				<div class="mobBox">
				<img src="{{url()}}/demo/img/mob-mxb.png" alt="">
				<div class="mobGet3">

				</div>
				<div class="mobGet">
				<div class="get1">
					
					<label class="btn" for="modal-5"><img src="{{url()}}/demo/img/getNow-orange.png" width="70" height="60"></label>
					<input class="modal-state" id="modal-5" type="checkbox" />
					<div class="modal">
						<label class="modal__bg" for="modal-5"></label>
						<div class="modal__inner">
							<label class="modal__close" for="modal-5"></label>
							<center><img src="{{url()}}/demo/img/maxbet_qr.jpg" width="50%" height="auto" alt=""/></center>
							
							<span style="color:black;font-size:30px;margin-left:170px;">
							@if(Session::has('username'))
								{{Session::get('username')}}
							@else
								Please Login!
							@endif
							</span>
							
						</div>
					</div>
				</div>
				<div class="get2">
				<label class="btn" for="modal-5"><img src="{{url()}}/demo/img/getNow-Red.png" width="70" height="60"></label>
				</div>
				<div class="clr"></div>
				</div>
				</div>

				<div class="mobBoxL">
				<img src="{{url()}}/demo/img/gp-comingSoon.jpg" alt="">


				</div>

				<a id="button1" href="#" tabindex="1"><img src="{{url()}}/demo/img/mob-and-lc.png" width="88" height="45"></a>

				<a id="button2" href="#" tabindex="2"><img src="{{url()}}/demo/img/mob-app-lc.png" width="88" height="45"></a>


				<a id="button3" href="#" tabindex="3"><img src="{{url()}}/demo/img/mob-and-slot.png" width="88" height="45"></a>
				<a id="button4" href="#" tabindex="4"><img src="{{url()}}/demo/img/mob-and-lc.png" width="88" height="45"></a>

				<div id="content"> 
				 <div id="def"><img src="{{url()}}/demo/img/step-ag-android.png" width="995px"></div>
				 <div id="faq"><img src="{{url()}}/demo/img/step-ag-android.png" width="995px"></div>
				 <div id="her"><img src="{{url()}}/demo/img/step-ag-ios.png" width="995px"></div>
				 <div id="and"><img src="{{url()}}/demo/img/step-pt-android.png" width="995px"></div>
				 <div id="enj"><img src="{{url()}}/demo/img/step-pt-android.png" width="995px"></div>
				</div>
				<div class="clr"></div>
			</div>
			<div class="clr"></div>
		</div>
	</div>
	<div class="appBg">
	</div>
</div>
<!--Mobile-->
@stop

@section('bottom_js')
<script src="{{url()}}/demo/resources/js/slick.min.js"></script>
<script>
$('.caro').slick({
  infinite: true,
  slidesToShow: 2,
  slidesToScroll: 2,
  autoplay: true,
  variableWidth: true,
  arrows: false
});
</script>
@stop