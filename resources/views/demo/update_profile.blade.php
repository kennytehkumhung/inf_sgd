@extends('demo/master')

@section('title', 'Account Profile')

@section('top_js')
<link href="{{url()}}/demo/resources/css/acct_management.css" rel="stylesheet">
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
@stop

@section('content')

@include('demo/transaction_mange_top', [ 'title' => 'Account Profile' ] )
<div class="acctContent">
    <div class="depLeft">
		<div class="cont">
			<!--MID SECTION-->
			<?php echo htmlspecialchars_decode($content); ?>   
			<!--MID SECTION-->

		</div>
	</div>
	<div class="depRight">
		<span class="wallet_title">{{Lang::get('public.AccountProfile')}}</span>  
		<div class="acctRow">
			<label> {{Lang::get('public.Username')}} :</label>
			<span>{{ $acdObj->fullname}}</span>
		</div>
		<div class="acctRow">
			<label>{{Lang::get('public.EmailAddress')}} :</label><span>{{ $acdObj->email}}</span>
			<div class="clr"></div>
		</div>
		
		<div class="acctRow">
			<label>Currency :</label><span>MYR</span>
			<div class="clr"></div>
		</div>
		
		<div class="acctRow">
			<label>{{Lang::get('public.ContactNo')}} :</label>{{ $acdObj->telmobile}}
			<div class="clr"></div>
		</div>
			
		<div class="acctRow">
			<label>{{Lang::get('public.Gender')}} :</label>
				<select id="gender">
					<option value="1">{{Lang::get('public.Male')}}</option> 
					<option value="2">{{Lang::get('public.Female')}}</option>
				</select>
			<div class="clr"></div>
		</div>
		<div class="acctRow">
			<label>{{Lang::get('public.DOB')}} :</label>
				{{ $acdObj->dob}}
			<div class="clr"></div>
		</div>
	
	</div>
	<div class="clr"></div>
	</div>
         
   </div>
</div>
@stop

@section('bottom_js')
<script src="{{url()}}/demo/resources/js/slick.min.js"></script>
<script>
$('.caro').slick({
  infinite: true,
  slidesToShow: 2,
  slidesToScroll: 2,
  autoplay: true,
  variableWidth: true,
  arrows: false
});

$(function() {
    $( ".datepicker" ).datepicker({ dateFormat: 'yy-mm-dd' });
  });

 function update_profile(){
	$.ajax({
		type: "POST",
		url: "{{action('User\MemberController@UpdateDetail')}}",
		data: {
			_token: "{{ csrf_token() }}",
			dob:		$('#dob').val(),
			mobile:    	$('#mobile').val(),
			gender:    	$('#gender').val()

		},
	}).done(function( json ) {
		
			 obj = JSON.parse(json);
			 var str = '';
			 $.each(obj, function(i, item) {
				str += item + '\n';
			})
			
			alert(str);
			
			
	});
}
</script>
@stop