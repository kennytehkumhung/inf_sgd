@extends('demo/master')

@section('title', 'Malaysia Online Casino, Gambling Games | avxdemo')
@section('keywords', 'Malaysia Online Casino, Gambling Games | avxdemo')
@section('description', 'Enjoy gambling (kasino) games at Malaysia most trusted online casino, avxdemo. Play live casino, slots, horse racing and more.')


@section('top_js')
<script type="text/javascript">

	$(document).ready(function() {
			$('.box_skitter_large').skitter({
				theme: 'clean',
				numbers_align: 'center',
				progressbar: false, 
				dots: false, 
				preview: false,
				hideTools: false,
				enable_navigation_keys: false
			});
		});

   if($.cookie("css")) {
      $("link").attr("href",$.cookie("css"));
   }
   $(document).ready(function() { 
      $(".lgCont li a").click(function() { 
         $("link").attr("href",$(this).attr('rel'));
         $.cookie("css",$(this).attr('rel'), {expires: 365, path: '/'});
         return false;
      });
   });
</script>

<script>
          $(document).ready(function () {
              fakeJackport();
              setInterval(updateJackport, 3000);
          });

          function fakeJackport() {

              var value = Math.floor(Math.random() * 10);

              value = 1356289 + value * 2.496;


              var value2 = value.toFixed(2);

              $('#jp_1').text(addCommas(value2));

          }

          function updateJackport() {
              var val = $("span[id^='jp_1']").text();
              val = removeComma(val);

              if (val > 1358889)
                  val = 1356289;

              var value = Math.floor(Math.random() * 10);
              value = value * 0.027;
              
              var value2 = (parseFloat(val) + value).toFixed(2);

              $('#jp_1').text(addCommas(value2));
          }

          function addCommas(str) {
              var parts = (str + "").split("."),
          main = parts[0],
          len = main.length,
          output = "",
          i = len - 1;

              while (i >= 0) {
                  output = main.charAt(i) + output;
                  if ((len - i) % 3 === 0 && i > 0) {
                      output = "," + output;
                  }
                  --i;
              }
              // put decimal part back
              if (parts.length > 1) {
                  output += "." + parts[1];
              }
              return output;
          }

          function removeComma(str) {
              var parts = (str + "").split(",");

              var output = "";

              for (var i = 0; i < parts.length; i++) {
                  output += parts[i];
              }
              return output;
          }
</script>
@stop

@section('content')


<div class="box_skitter box_skitter_large">
          <ul>
			@if(isset($banners))
				@foreach( $banners as $key => $banner )
					<li><a href="#cube"><img src="{{$banner->domain}}/{{$banner->path}}" class="circles" /></a></li>
					
				@endforeach
			@endif
          </ul>
        </div>

<!--Jackpot-->
<div class="jackP">
	<ul style="padding: 18px 0px 20px;" id="accordion">
		<li>
			<a href="#"><img src="{{url()}}/demo/resources/img/mxb-hpg.png"></a>
		</li>
		<li>
			<a href="#"><img src="{{url()}}/demo/resources/img/pt-hpg.png"></a>
		</li>
		<li>
			<a href="#"><img src="{{url()}}/demo/resources/img/gp-hpg.png"></a>
		</li>
		<li>
			<a href="#"><img src="{{url()}}/demo/resources/img/allbet-hpg.png"></a>
		</li>
		<li>
			<a href="#"><img src="{{url()}}/demo/resources/img/ag-hpg.png"></a>
		</li>
		<li>
			<a href="#"><img src="{{url()}}/demo/resources/img/ibc-hpg.png"></a>
		</li>

	</ul>
</div>
<!--Jackpot-->

<div class="line-seperator"></div>


<!--Bottom Section-->
<div class="bott">
<div class="col1">
<table width="auto" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>
      <img src="{{url()}}/demo/resources/img/infi-promotion-banner.gif" alt=""/></td>
  </tr>
</table>

</div>


<div class="col3">

    <div class="digit">MYR <span id="jp_1">88,888.888.00</span></div>
</div>


</div>

<div class="clr"></div>

<div class="col6">
<table cellspacing="0" cellpadding="0" border="0">
  <tbody><tr>
    <td><img src="{{url()}}/demo/resources/img/live-support-title.png"></td>
  </tr>
  <tr>
    <td><div class="bx1" style="font-size:11px;"><span>24 hours active users</span></div></td>
  </tr>
  <tr>
    <td><div class="bx2" style="font-size:11px;"><span>24 hours cumulative amount of bets</span></div></td>
  </tr>
  <tr>
    <td><div class="bx3" style="font-size:11px;"><span>24 hours cumulative deposit withdrawals</span></div></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
</tbody></table>

</div>
<div class="col1">
<table width="auto" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>
      <img src="{{url()}}/demo/resources/img/sportbook.png" width="554" height="228"></td>
  </tr>
</table>

</div>





<div class="clr"></div>

</div>

<!--Bottom Section-->




@stop

@section('bottom_js')
<script>
	$(document).ready(function(){
		activeItem = $("#accordion li:first");
		$(activeItem).addClass('active');
		$("#accordion li").hover(function(){
		$(activeItem).animate({width: "110px"}, {duration:300, queue:false});
		$(this).animate({width: "260px"}, {duration:300, queue:false});
		activeItem = this;
		});
	});

</script>

@stop