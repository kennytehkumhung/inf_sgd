<!-- head section-->
@include('demo/include/head')
<!-- head section-->
<body onload=display_ct();>
<div class="container">
	<div class="header">
		<!-- navigation header starts-->	
		@include('demo/include/navigation')	
		<!-- navigation header ends-->

		<!--MegaMenu Starts-->
		@include('demo/include/megamenu')
		<!--MegaMenu Ends-->

		<!--Annoucement-->
		<div class="anmnt">
			<marquee>{{App\Http\Controllers\User\AnnouncementController::index()}}</marquee>
		</div>
		<!--Annoucement-->

		@yield('content')

		<!--Footer-->
		@include('demo/include/footer')
		<!--Footer-->
	</div>
</div>
@yield('bottom_js')
</body>
</html>