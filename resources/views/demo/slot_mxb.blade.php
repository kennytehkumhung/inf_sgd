<html>
<head>
 <link href="{{url()}}/demo/resources/css/slot.css" rel="stylesheet">
 <link href="{{url()}}/demo/resources/css/style_2.css" rel='stylesheet' type='text/css'>

<!-- Home slider style -->
<link rel="stylesheet" href="{{url()}}/demo/resources/css/style_3.css">
</head>
<body>
	<div class="slot_menu">
		<ul>
			<li><a href="{{route('mxb', [ 'type' => 'slot' , 'category' => 'Slots' ] )}}">Slots (18)</a></li>
			<li><a href="{{route('mxb', [ 'type' => 'slot' , 'category' => 'Table' ] )}}">Table(23)</a></li>
			<li><a href="{{route('mxb', [ 'type' => 'slot' , 'category' => 'Soft Games' ] )}}">Mini (3)</a></li>
			<li><a href="{{route('mxb', [ 'type' => 'slot' , 'category' => 'Video Poker' ] )}}">Video Poker (19)</a></li>
		</ul>
	</div>
	<div id="slot_lobby">
		@foreach( $lists as $list )
		<div class="slot_box">
			<a href="javascript:void(0)" onclick="@if (Auth::user()->check())window.open('{{route('mxb-slot' , [ 'gameid' => $list['gameID'] ] )}}', 'mxb_slot', 'width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif" >
				<img src="{{$list['imageUrl']}}" width="150" height="150" alt=""/>
			</a>
			<span>{{$list['gameName']}}</span>
		</div>			 
		@endforeach
	  <div class="clr"></div>
	</div>
    <div class="clr"></div>
<!--Slot-->
</body>
</html>