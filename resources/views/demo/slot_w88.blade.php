<html>
<head>
 <link href="{{url()}}/demo/resources/css/slot.css" rel="stylesheet">
 <link href="{{url()}}/demo/resources/css/style_2.css" rel='stylesheet' type='text/css'>

<!-- Home slider style -->
<link rel="stylesheet" href="{{url()}}/demo/resources/css/style_3.css">
</head>
<body>
	<div class="slot_menu">
		<ul>
			<li><a href="{{route('w88', [ 'type' => 'slot' , 'category' => 'Arcades' ] )}}">Arcades (16)</a></li>
			<li><a href="{{route('w88', [ 'type' => 'slot' , 'category' => 'Card Games' ] )}}">Card Games(15)</a></li>
			<li><a href="{{route('w88', [ 'type' => 'slot' , 'category' => 'Slots' ] )}}">Slots (30)</a></li>
			<li><a href="{{route('w88', [ 'type' => 'slot' , 'category' => 'Table Games' ] )}}">Table Games (4)</a></li>
			<li><a href="{{route('w88', [ 'type' => 'slot' , 'category' => 'Video Poker' ] )}}">Video Poker (22)</a></li>
		</ul>
	</div>
	
	<div id="slot_lobby">
		@foreach( $lists as $list )
			 <div class="slot_box">
				<a href="javascript:void(0)" onclick="@if (Auth::user()->check())window.open('http://casino.gpiops.com/mini/?op=avxdemo&game_code={{$list->code}}&language=en&playmode=real&ticket={{$token}}','w88_slot','width=1000,height=750')@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif">
					<img src="{{url()}}/demo/img/w88/{{$list->image}}" width="150" height="150" alt=""/>
				</a>
				<span>{{$list->gamename_en}}</span>
			 </div>			 
		@endforeach
		<div class="clr"></div>
	</div>
   <div class="clr"></div>
<!--Slot-->
</body>
</html>