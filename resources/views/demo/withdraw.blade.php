@extends('demo/master')

@section('title', 'Withdraw')

@section('top_js')
 <link href="{{url()}}/demo/resources/css/acct_management.css" rel="stylesheet">
<script>
	setInterval(update_mainwallet, 10000);
	function submit_transfer(){
	$.ajax({
			 type: "POST",
			 url: "{{route('withdraw-process')}}?"+$('#withdraw_form').serialize(),
			 data: {
				_token: 		 "{{ csrf_token() }}",
			 },
			 beforeSend: function(){
				
			 },
			 success: function(json){
					obj = JSON.parse(json);
					 var str = '';
					 $.each(obj, function(i, item) {
						
						if( '{{Lang::get('COMMON.SUCESSFUL')}}' == item ){
							alert('{{Lang::get('COMMON.SUCESSFUL')}}');
							window.location.href = "{{route('transaction')}}";
						}else{
							str += item + '<br>';
						}
					})
					$('.failed_message').html(str);
				
			}
		})
	} 
</script>
@stop

@section('content')

@include('demo/transaction_mange_top', [ 'title' => Lang::get('public.Withdrawal') ] )
<div class="acctContent">
	<div class="depLeft">
		<div class="cont">
			<!--MID SECTION-->
			<?php echo htmlspecialchars_decode($content); ?>
			<!--MID SECTION-->
		</div>
	</div>
<form id="withdraw_form">
	<div class="depRight">
		<span class="wallet_title">{{Lang::get('public.Withdrawal')}}</span>
			<div class="acctRow">
				<label>{{Lang::get('public.Balance')}} (MYR) :</label> {{App\Http\Controllers\User\WalletController::mainwallet()}}
				<div class="clr"></div>
			</div>
			<div class="acctRow">
				<label>{{Lang::get('public.Amount')}} (MYR) * :</label><input type="text" name="amount"/>
				<div class="clr"></div>
			</div>
			<div class="acctRow">
				<label>{{Lang::get('public.BankName')}}* :</label>
					<select name="bank">
						@foreach( $banklists as $bank => $detail  )
						<option value="{{$detail['code']}}">{{$bank}}</option>
						@endforeach
					</select>
				<div class="clr"></div>
			</div>
			<div class="acctRow">
				<label>{{Lang::get('public.FullName')}} * :</label><span>{{Session::get('fullname')}}</span>
				<div class="clr"></div>
			</div>
			<div class="acctRow">
				<label>{{Lang::get('public.BankAccountNo')}} :</label><input type="text" name="accountno" />
				<div class="clr"></div>
			</div>     
		<br>
		<p>{{Lang::get('public.RequiredFields')}}</p>

		<span class="failed_message acctTextReg" style="display:block;float:left;height:100%;"></span>
		<div class="submitAcct">
			<a href="javascript:void(0)" onClick="submit_transfer()">{{Lang::get('public.Submit')}}</a>
		</div>
	</div>
</form>
</div>
@stop

@section('bottom_js')
<script src="{{url()}}/demo/resources/js/slick.min.js"></script>
<script>
$('.caro').slick({
  infinite: true,
  slidesToShow: 2,
  slidesToScroll: 2,
  autoplay: true,
  variableWidth: true,
  arrows: false
});
</script>
@stop