@extends('demo/master')

@section('title', Lang::get('public.ContactUs'))

@section('top_js')

@stop

@section('content')
<!--MID SECTION-->
<?php echo htmlspecialchars_decode($content); ?>
<!--MID SECTION-->
@stop

@section('bottom_js')

@stop