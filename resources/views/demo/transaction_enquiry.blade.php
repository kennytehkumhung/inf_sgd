@extends('demo/master')

@section('title', 'Transacrion Enquiry')

@section('top_js')
  <link href="{{url()}}/demo/resources/css/acct_management.css" rel="stylesheet">
  <link href="{{url()}}/demo/resources/css/table.css" rel="stylesheet">
  <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
@stop

@section('content')

@include('demo/transaction_mange_top', [ 'title' => Lang::get('public.TransactionHistory') ] )
<div class="acctContent">
    <div class="depLeft">
		<div class="cont">
			<!--MID SECTION-->
				<?php echo htmlspecialchars_decode($content); ?>
			<!--MID SECTION-->
		</div>
	</div>
	<div class="depRight">
		<span class="wallet_title">{{Lang::get('public.TransactionHistory')}}</span>
		<div class="acctRow">
			<div class="acctRow">
				<label>{{Lang::get('public.DateFrom')}} :</label>
				<input type="text" class="datepicker" id="date_from" style="cursor:pointer;" value="{{date('Y-m-d')}}"><br>
			</div>
			<div class="acctRow">
				<label>{{Lang::get('public.DateTo')}}:</label>
				<input type="text" class="datepicker" id="date_to" style="cursor:pointer;" value="{{date('Y-m-d')}}">
			</div>
			<div class="acctRow">
				<label>{{Lang::get('public.RecordType')}}:</label>
				<select id="record_type">
					<option value="0" selected>{{Lang::get('public.CreditAndDebitRecords')}}</option>
					<option value="1">{{Lang::get('public.CreditRecords')}}</option>
					<option value="2">{{Lang::get('public.DebitRecords')}}</option>
				</select>
			</div>
			<div class="acctRow">
				<div class="submitAcct">
					<a id="trans_history_button" href="#" onClick="transaction_history()"> {{Lang::get('public.Submit')}}</a>
					<br><br>
				</div>
			</div>
		</div>
		<div class="clr"></div>
		<div class="table" style="display:block;">
			<table width="100%" id="trans_history">
				<tr>
					<td>{{Lang::get('public.ReferenceNo')}}</td>
					<td>{{Lang::get('public.DateOrTime')}}</td>
					<td>{{Lang::get('public.type')}}</td>
					<td>{{Lang::get('public.Amount')}} </td>
					<td>{{Lang::get('public.Status')}}</td>
					<td>{{Lang::get('public.reason')}}</td>
				</tr>
				<tr>
					<td colspan="6"><h2><center>No Data Available!</center></h2></td>
				</tr>
			</table>
		</div>
	</div>
</div>
</div>
</div>

@stop

@section('bottom_js')
<script src="{{url()}}/demo/resources/js/slick.min.js"></script>
<script>
transaction_history();
$('.caro').slick({
  infinite: true,
  slidesToShow: 2,
  slidesToScroll: 2,
  autoplay: true,
  variableWidth: true,
  arrows: false
});

$(function() {
	$( ".datepicker" ).datepicker({ dateFormat: 'yy-mm-dd'  , defaultDate: new Date() });
});
  
function transaction_history(){
$.ajax({
	type: "POST",
	url: "{{action('User\TransactionController@transactionProcess')}}",
	data: {
		_token:   "{{ csrf_token() }}",
		date_from:		$('#date_from').val(),
		date_to:    	$('#date_to').val(),
		record_type:    $('#record_type').val()

	},
}).done(function( json ) {

			//var str;
			 var str = '';
			str += '	<tr ><td>Reference No.</td><td>Date/Time</td><td>Type</td><td>Amount(MYR)</td><td>Status</td><td>Reason</td></tr>';
			
			 obj = JSON.parse(json);
			//alert(obj);
			 $.each(obj, function(i, item) {
				str +=  '<tr><td>'+item.id+'</td><td>'+item.created+'</td><td>'+item.type+'</td><td>'+item.amount+'</td><td>'+item.status+'</td><td>'+item.rejreason+'</td></tr>';
				
			})
				
			//alert(json);
			$('#trans_history').html(str);
			
	});
}

setInterval(updateTrans, 10000);
setInterval(update_mainwallet, 10000);

function updateTrans(){
	$( "#trans_history_button" ).trigger( "click" );
}

</script>
@stop