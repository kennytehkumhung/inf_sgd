@extends('demo/master')

@section('title', 'Deposit')

@section('top_js')
 <link href="{{url()}}/demo/resources/css/acct_management.css" rel="stylesheet">
 <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <script>
	setInterval(update_mainwallet, 10000);
  function deposit_submit(){
	var file_data = $("#receipt").prop("files")[0];  
	var form_data = new FormData();                  
	form_data.append("file", file_data);
	
	var data = $('#deposit_form').serializeArray();
	var obj = {};
	for (var i = 0, l = data.length; i < l; i++) {
		form_data.append(data[i].name, data[i].value);
	}
	
	form_data.append('_token', '{{csrf_token()}}');

	$.ajax({
		type: "POST",
		url: "{{route('deposit-process')}}?",
		        dataType: 'script',
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,                         // Setting the data attribute of ajax with file_data
                type: 'post',
				beforeSend: function(){
					$('#deposit_sbumit_btn').attr('onclick','');
				},
				complete: function(json){
					
					obj = JSON.parse(json.responseText);
					 var str = '';
					 $.each(obj, function(i, item) {
						
						if( '{{Lang::get('COMMON.SUCESSFUL')}}' == item ){
							alert('{{Lang::get('COMMON.SUCESSFUL')}}');
							window.location.href = "{{route('transaction')}}";
						}else{
							//$('.'+i+'_acctTextReg').html(item);
							$('#deposit_sbumit_btn').attr('onclick','deposit_submit()');
							str += item + '<br>';
						}
					})
					$('.failed_message').html(str);
				
				},
					
	});
	//alert('asd');
}
  </script>
  <style>
  .acctRowR table th {
    background-color: #c59f4d;
    border-bottom: 1px solid #d6d6d6 !important;
    color: #000;
    font-weight: bold;
    text-align: center;
    vertical-align: middle;
}
  </style>
@stop

@section('content')

@include('demo/transaction_mange_top', [ 'title' => Lang::get('public.Deposit') ] )
<div class="acctContent">
	<div class="depLeft">
		<div class="cont">
			<!--MID SECTION-->
			<?php echo htmlspecialchars_decode($content); ?>
			<!--MID SECTION-->
		</div>
	</div>
<form id="deposit_form">
	<div class="depRight">
		<span class="wallet_title"> {{Lang::get('public.Deposit')}}</span>
			<div class="acctRow">
				<label> {{Lang::get('public.Amount')}} :</label><input type="text" name="amount" />
				<div class="clr"></div>
			</div>
			<div class="acctRowR" style="display:block; height:100%;">
				<label> {{Lang::get('public.Bank')}} :</label>
					<table cellspacing="0" rules="all" border="1" id="ctl00_ctl00_ContentPlaceHolder1_ChildContentMain_deposit_table" style="width:100%;border-collapse:collapse;border: 1px solid #D6D6D6 !important;">
						<tr align="left" style="height:30px;">
							<th scope="col">&nbsp;</th><th scope="col">{{Lang::get('public.Bank')}}</th><th scope="col">{{Lang::get('public.AccountName')}}</th><th scope="col">{{Lang::get('public.AccountNumber')}}</th><th scope="col">{{Lang::get('public.Minimum')}}</th><th scope="col">{{Lang::get('public.Maximum')}}</th><th scope="col">{{Lang::get('public.ProcessingTime')}}</th>
						</tr>
					@foreach( $banks as $key => $bank )
						<tr>
							<td>
								<input name="bank" class="radiobtn" type="radio" value="{{$bank['bnkid']}}">
							</td>
							<td>
								<img src="{{$bank['image']}}">
								</td>
							<td>
								{{$bank['bankaccname']}}
							</td>		
							<td>
								{{$bank['bankaccno']}}
							</td>	
							<td>
								{{$bank['min']}}
							</td>	
							<td>
								{{$bank['max']}}
							</td>
							<td>
								5 min
							</td>
						</tr>  
					@endforeach
					</table>
				<div class="clr"></div>
			</div>
			<div class="acctRow">
				<label>{{Lang::get('public.ReferenceNo')}} :</label><input name="refno" type="text" />
				<div class="clr"></div>
			</div>
			<div class="acctRow">
				<label>{{Lang::get('public.DepositMethod')}}:</label>
					<select name="type">
						<option value="0">{{Lang::get('public.OverCounter')}}</option>
						<option value="1">{{Lang::get('public.InternetBanking')}}</option>
						<option value="2">{{Lang::get('public.ATMBanking')}}</option>
					</select>
				<div class="clr"></div>
			</div>
			<div class="acctRow">
			<label>{{Lang::get('public.DateTime')}} :</label>
				<select name="hours">
				<option value="01">01</option>
				<option value="02">02</option>
				<option value="03">03</option>
				<option value="04">04</option>
				<option value="05">05</option>
				<option value="06">06</option>
				<option value="07">07</option>
				<option value="08">08</option>
				<option value="09">09</option>
				<option value="10">10</option>
				<option value="11">11</option>
				<option value="12">12</option>
				</select>
				<select name="minutes" >
				<option value="00">00</option>
				<option value="01">01</option>
				<option value="02">02</option>
				<option value="03">03</option>
				<option value="04">04</option>
				<option value="05">05</option>
				<option value="06">06</option>
				<option value="07">07</option>
				<option value="08">08</option>
				<option value="09">09</option>
				<option value="10">10</option>
				<option value="11">11</option>
				<option value="12">12</option>
				<option value="13">13</option>
				<option value="14">14</option>
				<option value="15">15</option>
				<option value="16">16</option>
				<option value="17">17</option>
				<option value="18">18</option>
				<option value="19">19</option>
				<option value="20">20</option>
				<option value="21">21</option>
				<option value="22">22</option>
				<option value="23">23</option>
				<option value="24">24</option>
				<option value="25">25</option>
				<option value="26">26</option>
				<option value="27">27</option>
				<option value="28">28</option>
				<option value="29">29</option>
				<option value="30">30</option>
				<option value="31">31</option>
				<option value="32">32</option>
				<option value="33">33</option>
				<option value="34">34</option>
				<option value="35">35</option>
				<option value="36">36</option>
				<option value="37">37</option>
				<option value="38">38</option>
				<option value="39">39</option>
				<option value="40">40</option>
				<option value="41">41</option>
				<option value="42">42</option>
				<option value="43">43</option>
				<option value="44">44</option>
				<option value="45">45</option>
				<option value="46">46</option>
				<option value="47">47</option>
				<option value="48">48</option>
				<option value="49">49</option>
				<option value="50">50</option>
				<option value="51">51</option>
				<option value="52">52</option>
				<option value="53">53</option>
				<option value="54">54</option>
				<option value="55">55</option>
				<option value="56">56</option>
				<option value="57">57</option>
				<option value="58">58</option>
				<option value="59">59</option>
				</select>
				<select name="range">
				<option value="AM">AM</option>
				<option value="PM">PM</option>
				</select>
				<input type="text" class="datepicker" id="deposit_date" style="cursor:pointer;" name="date" value="{{date('Y-m-d')}}"><br>
				<div class="clr"></div>
			</div>
			<div class="acctRow">
			<label> {{Lang::get('public.DepositReceipt')}} :</label><input class="upload" type="file" name="receipt" id="receipt">
				<div class="clr"></div>
			</div>
			<br>
			<div class="line"></div>
			<span class="wallet_title">{{Lang::get('public.Deposit')}} {{Lang::get('public.Promotion')}}</span>
			<div class="promoWrap">
		@foreach( $promos as $key => $promo )
			
				<input name="promo" class="radiobtn" type="radio" value="{{$promo['code']}}">
				{{$promo['name']}}
				<br>
				<img src="{{$promo['image']}}">
				<br>
		@endforeach
				<br>
				<input name="promo" class="radiobtn" type="radio" value="0">  {{Lang::get('public.IDoNotWantPromotion')}}
			</div>
			<p> {{Lang::get('public.RequiredFields')}}</p>
			<p><input class="radiobtn" type="radio" name="rules" >{{Lang::get('public.IAlreadyUnderstandRules')}}</p>
			<span class="failed_message acctTextReg" style="display:block;float:left;height:100%;"></span>

			<div class="submitAcct">
				<a id="deposit_sbumit_btn" href="javascript:void(0)" onclick="deposit_submit()">{{Lang::get('public.Submit')}}</a>
			</div>
	</div>
	</form>
    <div class="clr"></div>
      </div>

@stop

@section('bottom_js')
<script src="{{url()}}/demo/resources/js/slick.min.js"></script>
<script>

$(function() {
    $( ".datepicker" ).datepicker({ dateFormat: 'yy-mm-dd' , defaultDate: new Date()});
});

$('.caro').slick({
  infinite: true,
  slidesToShow: 2,
  slidesToScroll: 2,
  autoplay: true,
  variableWidth: true,
  arrows: false
});
</script>
@stop