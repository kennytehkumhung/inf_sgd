<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>avxdemo @yield('title')</title>
    <link href="{{url()}}/demo/resources/css/style.css" rel="stylesheet">
	<link rel="alternate stylesheet" type="text/css" href="{{url()}}/demo/resources/css/style-dark.css" title="styles2" media="screen" />
    <link href="{{url()}}/demo/resources/css/jquery.megamenu.css" rel="stylesheet">
    <link href="{{url()}}/demo/resources/css/skitter.styles.css" type="text/css" media="all" rel="stylesheet" />
    <link href="{{url()}}/demo/resources/css/slick.css" rel="stylesheet">
	<link href="{{url()}}/demo/resources/css/clickDrop.css" rel="stylesheet">
    <link href="{{url()}}/demo/resources/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="{{url()}}/demo/resources/js/jq214.js"></script>
    <script src="{{url()}}/demo/resources/js/script.js"></script>
    <script src="{{url()}}/demo/resources/js/jquery.megamenu.js" type="text/javascript"></script>
    <script src="{{url()}}/demo/resources/js/styleswitcher.js" type="text/javascript"></script>
	<script type="text/javascript" language="javascript" src="{{url()}}/demo/resources/js/jquery.easing.1.3.js"></script>
	<script type="text/javascript" language="javascript" src="{{url()}}/demo/resources/js/jquery.skitter.min.js"></script>
    <script type="text/javascript">

	   $(document).ready(function() { 
		  $(".lgCont li a").click(function() { 
			 $("link").attr("href",$(this).attr('rel'));
			 $.cookie("css",$(this).attr('rel'), {expires: 365, path: '/'});
			 return false;
		  });
	
	   });
      jQuery(function(){
        var SelfLocation = window.location.href.split('?');
        switch (SelfLocation[1]) {
          case "justify_right":
            jQuery(".megamenu").megamenu({ 'justify':'right' });
            break;
          case "justify_left":
          default:
            jQuery(".megamenu").megamenu();
        }
      });
    </script>
	@yield('top_js')
    <script type="text/javascript">
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
function login(){
	$.ajax({
		type: "POST",
		url: "{{route('login')}}",
		data: {
			_token: "{{ csrf_token() }}",
			username: $('#username').val(),
			password: $('#password').val(),
			code:     $('#code').val(),
		},
	}).done(function( json ) {
			 obj = JSON.parse(json);
			 var str = '';
			 $.each(obj, function(i, item) {
				str += item + '\n';
			})
			if( "{{Lang::get('COMMON.SUCESSFUL')}}\n" == str ){
				location.reload(); 
			}else{
				alert(str);
			}
			
	});
}
function enterpressalert(e, textarea){

	var code = (e.keyCode ? e.keyCode : e.which);
	 if(code == 13) { //Enter keycode
	   login();
	 }
}
@if (Auth::user()->check())
	function getBalance($load_mainwallet = true){
		var total_balance = {{App\Http\Controllers\User\WalletController::mainwallet()}};
		if($load_mainwallet)
		$('.main_wallet').html(parseFloat(Math.round(total_balance * 100) / 100).toFixed(2));
		$.when(
			
			@foreach( Session::get('products_obj') as $prdid => $object)
			
		
					 $.ajax({
						  type: "POST",
						  url: "{{route( 'getbalance', [ '_token'=> csrf_token(), 'product'=> $object->code ])}}",
						  beforeSend: function(balance){
							$('.{{$object->code}}_balance').html('<img style="float:left;position:static;top:0px;margin-left:20px;" src="{{url()}}/demo/img/ajax-loading.gif" width="20" height="20">');
						  },
						  success: function(balance){
							$('.{{$object->code}}_balance').html(parseFloat(Math.round(balance * 100) / 100).toFixed(2));
							total_balance += parseFloat(balance);
						  }
					 })
				  @if($prdid != Session::get('last_prdid')) 
					,
				  @endif
			@endforeach

			
		).then(function() {

				$('#total_balance').html(parseFloat(Math.round(total_balance * 100) / 100).toFixed(2));

		});
	
	}
@endif
    </script>
	    <script>
		$(function() {
			// Clickable Dropdown
			$('.click-nav > ul').toggleClass('no-js js');
			$('.click-nav .js ul').hide();
			$('.click-nav .js').click(function(e) {
				$('.click-nav .js ul').slideToggle(200);
				$('.clicker').toggleClass('active');
				e.stopPropagation();
			});
			$(document).click(function() {
				if ($('.click-nav .js ul').is(':visible')) {
					$('.click-nav .js ul', this).slideUp();
					$('.clicker').removeClass('active');
				}
			});
		});
		
		$(function() {
			// Clickable Dropdown
			$('.click-nav1 > ul').toggleClass('no-js js');
			$('.click-nav1 .js ul').hide();
			$('.click-nav1 .js').click(function(e) {
				$('.click-nav1 .js ul').slideToggle(200);
				$('.clicker1').toggleClass('active');
				e.stopPropagation();
			});
			$(document).click(function() {
				if ($('.click-nav1 .js ul').is(':visible')) {
					$('.click-nav1 .js ul', this).slideUp();
					$('.clicker1').removeClass('active');
				}
			});
		});
		
		$(function() {
			// Clickable Dropdown
			$('.click-nav2 > ul').toggleClass('no-js js');
			$('.click-nav2 .js .wlt').hide();
			$('.click-nav2 .js').click(function(e) {
				if (!$('.click-nav2 .js .wlt').is(':visible')) {
					getBalance();
				}
				
				$('.click-nav2 .js .wlt').slideToggle(200);
				$('.clicker2').toggleClass('active');
				e.stopPropagation();
			});
			$(document).click(function() {
				
				if ($('.click-nav2 .js .wlt').is(':visible')) {
					$('.click-nav2 .js .wlt', this).slideUp();
					$('.clicker2').removeClass('active');
				}
			});
		});
		
		
		$(function() {
			// Clickable Dropdown
			$('.click-nav3 > ul').toggleClass('no-js js');
			$('.click-nav3 .js ul').hide();
			$('.click-nav3 .js').click(function(e) {
				$('.click-nav3 .js ul').slideToggle(200);
				$('.clicker3').toggleClass('active');
				e.stopPropagation();
			});
			$(document).click(function() {
				if ($('.click-nav3 .js ul').is(':visible')) {
					$('.click-nav3 .js ul', this).slideUp();
					$('.clicker3').removeClass('active');
				}
			});
		});
		function livechat() {
			window.open('https://secure.livechatinc.com/licence/3155342/open_chat.cgi?groups=2', '', 'width=525, height=520, top=150, left=250');
		}
		</script>
		<style>
		.loading_wallet{
			
			float:left;position:static;top:0px;left:0px;
		}
			
		</style>
  </head>
<body>
<div class="container">


<div class="header">
<div class="headerMain">
<div class="headerLeft">

</div>
<div class="headerRight">
<!-- Date Time Chat -->
<div class="headerDTT">
<table width="auto" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>
    <div class="lchat">
    <a href="#" onClick="livechat()" ><img src="{{url()}}/demo/img/lchat-icon-hover.png" alt="" width="121" height="29" id="lchat"></a>
    </div>
    </td>
    <td>30 Mar 2015, Mon, 15:03:12 (GMT +8) </td>
  </tr>
</table>

</div>
<!-- Date Time Chat -->
<div class="headerTop">
<table width="auto" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>
    <ul>
  <li>
  <div class="lgCont">
   <a href="serversideSwitch.html%3Fstyle=style1.html" rel="styles1" class="styleswitch"><img src="{{url()}}/demo/img/black-icon.png"></a>
   </div>
    <ul>
	   <li><a href="serversideSwitch.html%3Fstyle=style2.html" rel="styles2" class="styleswitch"><img src="{{url()}}/demo/img/white-icon.png"></a></li>
    </ul>
  </li>
  
</ul></td>
    <td><ul>
  <li>
  <div class="lgCont1">
   <img src="{{url()}}/demo/img/my-icon.png">
   </div>
    <ul>
      <li><img src="{{url()}}/demo/img/in0icon.png"></li>
    </ul>
  </li>
  
</ul>
</td>
@if (!Auth::user()->check())
    <td><div class="usrPw"><input type="text" placeholder="Username" id="username"></div></td>
    <td><div class="usrPw1"><input type="password" placeholder="Password" id="password"></div></td>
    <td><div class="usrPw2"><input type="text" placeholder="Code" id="code" onKeyPress="enterpressalert(event, this)"></div></td>
    <td><div class="usrPw2" style="margin-left:5px;"><img style="float: left;"  src="{{route('captcha', ['type' => 'login_captcha'])}}" width="60" height="24" />
	</div></td>
    <td><div class="fP"><a style="color:white;text-decoration: none;" href="{{route('forgotpassword')}}"> Forgot Password</a></div></td>
    <td><div class="submitHeader"><a onclick="login()" href="#">Submit</a></div></td>
    <td><div class="joinHeader"><a href="{{route('register')}}">Join Now</a></div></td>
@else
  <td><span class="spacing1"><i class="fa fa-user"></i>Welcome! {{Session::get('username')}} </span></td>
    <td><!-- Clickable Nav -->
		<div class="click-nav3">
			<ul class="no-js">
				<li>
					<a class="clicker3">Profile<i class="fa fa-caret-down"></i></a>
					<ul>
						<li><a href="{{route('update-profile')}}">My Account Details</a></li>
						<li><a href="{{route('update-password')}}">Change Password</a></li>
					</ul>
				</li>
			</ul>
		</div>
		<!-- /Clickable Nav --></td>
    <td><!-- Clickable Nav -->
		<div class="click-nav2">
			<ul class="no-js">
				<li>
					<a class="clicker2">Balance<i class="fa fa-caret-down"></i>
</a>
					<div class="wlt">
						<table width="auto" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center" style="color: yellow;font-size: 14px;"><strong>Wallet:</strong></td>
    <td>&nbsp;</td>
    <td align="center" style="color: yellow;font-size: 14px;"><strong>Balance:</strong></td>
  </tr>
  <tr>
    <td align="center">Main Wallet</td>
    <td>&nbsp;</td>
    <td align="center" class="main_wallet">0.00</td>
  </tr>
  @foreach( Session::get('products_obj') as $prdid => $object)
	   <tr>
		<td align="center">{{$object->name}}</td>
		<td>&nbsp;</td>
		<td align="center" class="{{$object->code}}_balance">0.00</td>
	  </tr>
  @endforeach

  <tr>
    <td align="center" style="color: yellow;font-size: 14px;"><strong>Balance:</strong></td>
    <td>&nbsp;</td>
    <td align="center" style="color: red;font-size: 14px; border-bottom: 1px solid red;border-top: 1px solid red;"><strong id="total_balance">00.00</strong></td>
  </tr>
  
</table>

                    <div class="clr"></div>
					</div>
				</li>
			</ul>
		</div>
		<!-- /Clickable Nav --></td>
    <td>
    <!-- Clickable Nav -->
		<div class="click-nav1">
			<ul class="no-js">
				<li>
					<a class="clicker1">Fund<i class="fa fa-caret-down"></i></a>
					<ul>
						<li><a href="{{route('deposit')}}">Deposit</a></li>
						<li><a href="{{route('withdraw')}}">Withdrawal</a></li>
						<li><a href="{{route('transaction')}}">Transaction History</a></li>
                        <li><a href="{{route('transfer')}}">Transfer</a></li>
					</ul>
				</li>
			</ul>
		</div>
		<!-- /Clickable Nav -->
    </td>
    <td><div class="submitHeader"><a href="{{route('logout')}}">Logout</a></div></td>
@endif
 </tr>
</table>

</div>

</div>



</div>

<!--MegaMenu Starts-->
    <ul class="megamenu">
      <li>
        <a href="{{route('homepage')}}">HOME</a>
        <div style="width: 500px;" class="dropPost">
          
          <table border="0" cellpadding="0" cellspacing="0" id="tabular-content">
            <tr>
              <td><a href="{{route('banking')}}" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('banking','','{{url()}}/demo/img/home-banking-icon-hover.png',0)"><img src="{{url()}}/demo/img/home-banking-icon.png" alt="" width="93" height="111" id="banking"></a></td>
              <td>
              <a href="{{route('contactus')}}" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contact','','{{url()}}/demo/img/home-contact-icon-hover.png',0)"><img src="{{url()}}/demo/img/home-contact-icon.png" alt="" width="93" height="111" id="contact"></a></td>
              <td><a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('chat','','{{url()}}/demo/img/home-chat-icon-hover.png',0)"><img src="{{url()}}/demo/img/home-chat-icon.png" alt="" width="93" height="111" id="chat"></a></td>
              <td><a href="{{route('register')}}" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('register','','{{url()}}/demo/img/home-register-icon-hover.png',0)"><img src="{{url()}}/demo/img/home-register-icon.png" alt="" width="93" height="111" id="register"></a></td>
            </tr>
          </table>
        </div>
      </li>
      <li>
        <a href="{{route('livecasino')}}">LIVE CASINO</a>
        <div style="width: 500px;" class="dropPost2">
          
          <table border="0" cellpadding="0" cellspacing="0" id="tabular-content">
            <tr>
              <td>
              <a onclick="@if (Auth::user()->check())window.open('{{route('agg')}}', 'casino11', 'width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('ag','','{{url()}}/demo/img/lc-ag-icon-hover.png',0)"><img src="{{url()}}/demo/img/lc-ag-icon.png" alt="" width="93" height="111" id="ag"></a></td>
              <td>
              <a onclick="@if (Auth::user()->check())window.open('{{route('w88' , [ 'type' => 'casino' ] )}}', 'casino12', 'width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('gp','','{{url()}}/demo/img/lc-gp-icon-hover.png',0)"><img src="{{url()}}/demo/img/lc-gp-icon.png" alt="" width="93" height="111" id="gp"></a></td>
              <td><a onClick="@if (Auth::user()->check())window.open('{{route('plt', [ 'type' => 'casino' ] )}}', 'casino13', 'width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('pt','','{{url()}}/demo/img/lc-pt-icon-hover.png',0)"><img src="{{url()}}/demo/img/lc-pt-icon.png" alt="" width="93" height="111" id="pt"></a></td>
              <td>
              <a onClick="@if (!Auth::user()->check())alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif" href="@if (Auth::user()->check()){{route('mxb', [ 'type' => 'casino' , 'category' => 'baccarat' ] )}}@endif" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('xp','','{{url()}}/demo/img/lc-xp-icon-hover.png',0)"><img src="{{url()}}/demo/img/lc-xp-icon.png" alt="" width="93" height="111" id="xp"></a></td><br>
<td>

             <a href="" onClick="@if (Auth::user()->check())window.open('{{route('alb')}}', 'casino13', 'width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif"><img src="{{url()}}/demo/img/lc-coming-.png" width="93" height="111" alt=""/></a></td>
            </tr>
          </table>
        </div>
      </li>
      <li>
        <a href="{{route('ibc')}}">SPORTSBOOK</a>
      </li>
      <li>
        <a href="{{route('mxb', [ 'type' => 'slot' , 'category' => 'Slots' ] )}}">SLOT</a>
        <div style="width: 500px;" class="dropPost3">
          
          <table border="0" cellpadding="0" cellspacing="0" id="tabular-content">
            <tr>
              <td>
              <a href="{{route('mxb' , [ 'type' => 'slot' , 'category' => 'Slots' ] )}}" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('bt','','{{url()}}/demo/img/slt-bt-icon-hover.png',0)"><img src="{{url()}}/demo/img/slt-bt-icon.png" alt="" width="161" height="110" id="bt"></a></td>
              <td>
              <a href="{{route('w88', [ 'type' => 'slot' , 'category' => 'Arcades' ] )}}" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('gps','','{{url()}}/demo/img/slt-gp-icon-hover.png',0)"><img src="{{url()}}/demo/img/slt-gp-icon.png" alt="" width="161" height="110" id="gps"></a></td>
              <td>
              <a href="{{route('plt', [ 'type' => 'slot' , 'category' => 'arcade' ] )}}" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('pltslot','','{{url()}}/demo/img/slt-pt-icon-hover.png',0)"><img src="{{url()}}/demo/img/slt-pt-icon.png" alt="" width="161" height="110" id="pltslot"></a></td>
              <td>
              
<td>
			 <a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('sltmicro','','{{url()}}/demo/img/slt-topmicro-icon-hover.png',0)"><img src="{{url()}}/demo/img/slt-topmicro-icon.png" alt="" width="161" height="110" id="sltmicro"></a></td>
            </tr>
          </table>
        </div>
      </li>
      <li>
        <a href="{{route('4d')}}">4D</a>
      </li>
      <li>
        <a href="{{route('mobile')}}">MOBILE</a>
        <div style="width: 500px;" class="dropPost3">
          
          <table border="0" cellpadding="0" cellspacing="0" id="tabular-content">
            <tr>
              <td>
              <a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('bmlapp','','{{url()}}/demo/img/mbl-app-icon-hover.png',0)"><img src="{{url()}}/demo/img/mbl-app-icon.png" alt="" width="450" height="122" id="bmlapp"></a></td>
              <td>
              <a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('mbland','','{{url()}}/demo/img/mbl-and-icon-hover.png',0)"><img src="{{url()}}/demo/img/mbl-and-icon.png" alt="" width="450" height="122" id="mbland"></a></td>
              
            </tr>
          </table>
        </div>
      </li>
      <li style="margin-right: 0px;">
        <a style="margin-right: 0px;" href="{{route('promotion')}}">PROMOTION</a>
      </li>
    </ul>
<!--MegaMenu Ends-->

<!--Annoucement-->
<div class="anmnt">

<marquee>{{App\Http\Controllers\User\AnnouncementController::index()}}</marquee>
</div>
<!--Annoucement-->


@yield('content')


<!--Footer-->
<div class="footer">
<img src="{{url()}}/demo/img/bank-logo.png">
<div class="line"></div>
<div class="copyright">
<strong>Copyright © avxdemo. All Rights Reserved.</strong><br>
<ul>
<li><a href="#">About Us</a></li>
<li><a href="{{route('banking')}}">Banking Options</a></li>
<li><a href="{{route('contactus')}}">Contact Us</a></li>
<li><a href="{{route('faq')}}">FAQ</a></li>
<li><a href="#">How To Join</a></li>
<li><a href="#">Terms And Conditions</a></li>
<li style="border: 0px;"><a href="#">Sitemap</a></li>
</ul>
</div>

<div class="social">
<ul>
<li><a href="#"><img src="{{url()}}/demo/img/twit-icon.png" width="34" height="34" alt=""/></a></li>
<li><a href="#"><img src="{{url()}}/demo/img/fb-icon.png" width="34" height="34" alt=""/></a></li>
</ul>
</div>
<div class="clr"></div>
</div>
<!--Footer-->

</div>


</div>
@yield('bottom_js')
</body>
</html>