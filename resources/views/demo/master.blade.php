<!-- head section-->
@include('demo/include/head')
<!-- head section-->
<style>

.closeBtn2 {
    cursor: pointer;
    position: absolute;
    right: 0;
    z-index: 10000;
}

#minimize1{
position: fixed;
z-index: 9999;
bottom: 110px;
left: 3px;
width: 180px;
height: 180px;
}
From:
Si Yuan
block1.b{
display:none;
}
</style>
<body>




	<div class="container">
		<div class="header">
			<!-- navigation header starts-->	
			@include('demo/include/navigation')	
			<!-- navigation header ends-->

			<!--MegaMenu Starts-->

			@include('demo/include/megamenu')
			<!--MegaMenu Ends-->

			<!--Annoucement-->
			<!--<div class="anmnt">
				<marquee>{{App\Http\Controllers\User\AnnouncementController::index()}}</marquee>
			</div>-->
			<!--Annoucement-->

			@yield('content')

			<!--Footer-->
			@include('demo/include/footer')
			<!--Footer-->
		</div>
	</div>
@yield('bottom_js')
@if( Session::get('currency') == 'MYR')
	<!--<block1 id="minimize1" class="a">
	<div class="closeBtn2" onclick="javascript:hideminimize1()">
	<img onmouseout="this.src='{{url()}}/demo/img/closeBtn.png'" onmouseover="this.src='{{url()}}/demo/img/closeBtn_hover.png'" src="{{url()}}/demo/img/closeBtn.png">
	</div>
	<div id="navspin">
	<a onclick="@if (Auth::user()->check())window.open('{{route('omt')}}','spin','width=1330,height=650,toolbar=0,resizable=0,scrollbars=0');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif" href="#">
	<div style="position:fixed;z-index:9999;bottom:100px;left:3px;background-image:url({{url()}}/demo/img/lucky_en.png);background-size:contain;;width:180px; height:180px;"> </div>
	</a>
	</div>
	</block1>-->
@endif
</body>

@if( Session::get('currency') == 'MYR')
<script>
function hideminimize1() {
document.getElementById('minimize1').setAttribute('class', 'b');
}
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function()
{ (i[r].q=i[r].q||[]).push(arguments)}

,i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-43720111-1', 'auto');
ga('send', 'pageview');
</script>
@elseif( Session::get('currency') == 'VND')
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function()
{ (i[r].q=i[r].q||[]).push(arguments)}

,i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-43720111-1', 'auto');
ga('send', 'pageview');

</script>
@endif
</html>