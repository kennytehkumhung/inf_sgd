@extends('demo/master')

@section('title', 'About Us')

@section('top_js')
<link href="{{url()}}/demo/resources/css/otherPg.css" rel="stylesheet">
@stop

@section('content')
<!--MID SECTION-->
<?php echo htmlspecialchars_decode($content); ?>
<!--MID SECTION-->
@stop

@section('bottom_js')

@stop