@extends('demo/master')

@section('title', 'Malaysia Online Slot Machine Games | avxdemo')
@section('keywords', 'Malaysia Online Slot Machine Games | avxdemo')
@section('description', 'Play slot games (mesin slot) at avxdemo. Wide range of slots available. Register free account.')

@section('top_js')
 <link href="{{url()}}/demo/resources/css/slot.css" rel="stylesheet">
 <link href="{{url()}}/demo/resources/css/jquery.bxslider.css" type="text/css" media="all" rel="stylesheet" />
 <script type="text/javascript" language="javascript" src="{{url()}}/demo/resources/js/jquery.bxslider.min.js"></script>
   <!-- Base MasterSlider style sheet -->
<link rel="stylesheet" href="{{url()}}/demo/resources/css/masterslider.css" />

<!-- Master Slider -->
<script src="{{url()}}/demo/resources/js/masterslider.min.js"></script>

<!-- Master Slider Skin -->
<link href="{{url()}}/demo/resources/css/style_2.css" rel='stylesheet' type='text/css'>

<!-- Home slider style -->
<link rel="stylesheet" href="{{url()}}/demo/resources/css/style_3.css">
 <script>
 $(document).ready(function() { 
	@if( $type == 'mxb' )
		$('.slot_mxb_image').hide();
	@elseif( $type == 'plt' )
		$('.slot_plt_image').hide();	
	@elseif( $type == 'a1a' )
		$('.slot_a1a_image').hide();	
	@elseif( $type == 'w88' )
		$('.slot_w88_image').hide();
	@endif
 });
 function open_game(gameid){
	 

 }
   function resizeIframe(obj){
     obj.style.height = 0;
     obj.style.height = obj.contentWindow.document.body.scrollHeight + 'px';
  }
  
  function change_iframe(url,image_link,product){
	  $('#iframe_game').attr('src',url);
	  $('.top').show();
	  $('.slot_'+product+'_image').hide();
  }
 </script>

@stop

@section('content')
<!--Slot-->
@include('demo/slot_top')

	<iframe id="iframe_game" frameborder="0" style="overflow:hidden;overflow-x:hidden;overflow-y:hidden;height: 98%; min-height: 2000px; width:100%;" src="
	@if( $type == 'mxb' )
		{{route('mxb' , [ 'type' => 'slot' , 'category' => 'Slots' ] )}}
	@elseif( $type == 'w88' )
		{{route('w88', [ 'type' => 'slot' , 'category' => 'Arcades' ] )}}
	@elseif( $type == 'plt' )
		{{route('plt' , [ 'type' => 'pgames' , 'category' => '1' ] )}}
	@elseif( $type == 'a1a' )
		{{route('a1a')}}
	@endif
	" onload="resizeIframe(this)"></iframe>
<!--Slot-->
@stop

@section('bottom_js')
<script>
    $('.bxslider').bxSlider({
  pagerCustom: '#bx-pager'
});
</script>
<script src="{{url()}}/demo/resources/js/slick.min.js"></script>
<script>
$('.caro').slick({
  infinite: true,
  slidesToShow: 2,
  slidesToScroll: 2,
  autoplay: true,
  variableWidth: true,
  arrows: false
});
</script>
<script type="text/javascript">
         
   var slider = new MasterSlider();
 
    // adds Arrows navigation control to the slider.
    slider.control('arrows');
    slider.control('timebar' , {insertTo:'#masterslider'});
    slider.control('bullets');
 
     slider.setup('masterslider' , {
         width:1400,    // slider standard width
         height:380,   // slider standard height
         space:1,
         layout:'fullwidth',
         loop:true,
         preload:0,
         autoplay:true
    });
 
</script>
@stop