
<!-- masterslider -->
<div class="master-slider ms-skin-default" id="masterslider">
  <!-- new slide -->
    <div class="ms-slide slide-3" data-delay="1">
         
        <!-- slide background -->
        <img src="{{url()}}/demo/img/blank.gif" data-src="{{url()}}/demo/img/banner/bg.jpg" alt="Slide3 Background"/>     
		
		 <h3 class="ms-layer hps-title1"
			style="left:500px;top:100px;border: none;"
			data-type="text"
			data-ease="easeOutExpo"
			data-delay="2000"
		 	data-duration="1400"
		 	data-effect="skewleft(30,80)">
	    <img src="{{url()}}/demo/img/banner/txt1.png" width="451" height="79"> </h3>

		<h3 class="ms-layer hps-title4"
			style="left: 422px;top: 157px;border: none;"
			data-type="text"
			data-delay="3100"
		 	data-duration="2800"
		 	data-effect="rotate3dtop(-100,0,0,18,t)"
		 	data-ease="easeOutExpo">
			<img src="{{url()}}/demo/img/banner/txt2.png" width="638" height="79">
		</h3>
    <img src="{{url()}}/demo/img/blank.gif" data-src="{{url()}}/demo/img/banner/part-3.png" alt="Snow particle 3"
		 	  style="left:100px; top:70px;"
		 	  class="ms-layer"
		 	  data-type="image"
		 	  data-ease="easeOutExpo"
		 	  data-duration="7400"
		 	  data-effect="scalefrom(0.3,0.3,180,300,br)"
		 	  
		/>
		
		<img src="{{url()}}/demo/img/blank.gif" data-src="{{url()}}/demo/img/banner/cool-guy.png" alt="A guy with snowboard"
		 	  style="left:200px; top:43px;"
		 	  class="ms-layer"
		 	  data-type="image"
		 	  data-delay="0"
		 	  data-ease="easeOutExpo"
		 	  data-duration="5000"
		 	  data-effect="scalefrom(0.7,0.7,380,450,br,false)"
		/>
        
        <img src="{{url()}}/demo/img/blank.gif" data-src="{{url()}}/demo/img/banner/cool-guy2.png" alt="A guy with snowboard"
		 	  style="right:180px; top:43px;"
		 	  class="ms-layer"
		 	  data-type="image"
		 	  data-delay="0"
		 	  data-ease="easeOutExpo"
		 	  data-duration="8000"
		 	  data-effect="scalefrom(0.3,0.3,180,450,br)"
		/>

</div>
    <!-- end of slide -->
</div>

<div class="slotCont">

	<table width="auto" border="0" cellspacing="0" cellpadding="0">

		<tr>
			@if(in_array('MXB',Session::get('valid_product')))
			<td>

				<div class="lcBox">

					<div id="crossfade">

						<a href="javascript:void(0)" onClick="change_iframe('{{route('mxb' , [ 'type' => 'slot' , 'category' => 'Slots' ] )}}','mxb-slot-hover.png','mxb')">

							@if( $name == 'mxb' )

								<img class="bottom" width="240px" src="{{url()}}/demo/img/mxb-slot-hover.png" />

							@else

								<img class="bottom" width="240px" src="{{url()}}/demo/img/mxb-slot-hover.png" />

								<img class="top slot_mxb_image" width="240px" src="{{url()}}/demo/img/mxb-slot.png" />

							@endif

						</a>

					</div>

				</div>

			</td>
			@endif
			@if(in_array('W88',Session::get('valid_product')))
			<td>

				<div class="lcBox">

					<div id="crossfade">

						<a href="javascript:void(0)" onClick="change_iframe('{{route('w88' , [ 'type' => 'slot' , 'category' => 'Arcades' ] )}}','gp-slot-hover.png','w88')">

							@if( $name == 'w88' )

								<img class="bottom" width="240px" src="{{url()}}/demo/img/gp-slot-hover.png" />

							@else

								<img class="bottom" width="240px" src="{{url()}}/demo/img/gp-slot-hover.png" />

								<img class="top slot_w88_image" width="240px" src="{{url()}}/demo/img/gp-slot.png" />

							@endif

						</a>

					</div>

				</div>

			</td>
			@endif
			@if(in_array('PLT',Session::get('valid_product')))
			<td>

				<div class="lcBox">

					<div id="crossfade">

						<a href="javascript:void(0)" onClick="change_iframe('{{route('plt' , [ 'type' => 'pgames' , 'category' => '1' ] )}}','pt-slot-hover.png','plt')">
							@if( $name == 'plt' )

								<img class="bottom" width="240px" src="{{url()}}/demo/img/pt-slot-hover.png" />

							@else

								<img class="bottom" width="240px" src="{{url()}}/demo/img/pt-slot-hover.png" />

								<img class="top slot_plt_image" width="240px" src="{{url()}}/demo/img/pt-slot.png" />

							@endif

						</a>

						</a>

					</div>

				</div>

			</td>
			@endif
			<!--
			@if(in_array('A1A',Session::get('valid_product')))
			<td>

				<div class="lcBox">

					<div id="crossfade">

						<a href="javascript:void(0)" onClick="change_iframe('{{route('a1a')}}','a1-slot-hover.png','alb')">
							@if( $name == 'a1a' )

								<img class="bottom" width="240px" src="{{url()}}/demo/img/a1-slot-hover.png" />

							@else

								<img class="bottom" width="240px" src="{{url()}}/demo/img/a1-slot-hover.png" />

								<img class="top slot_alb_image" width="240px" src="{{url()}}/demo/img/a1-slot.png" />

							@endif

						</a>

					</div>

				</div>

			</td>
			@endif
			-->
		</tr>

	</table>