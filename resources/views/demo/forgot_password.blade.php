@extends('demo/master')

@section('title', 'Forgot Password')

@section('top_js')
  <script>
  function forgotpassword_submit(){

	$.ajax({
		type: "POST",
		url: "{{route('resetpassword')}}",
		data: {
			_token: "{{ csrf_token() }}",
			username:		 $('#fg_username').val(),
			email:    		 $('#fg_email').val(),

		},
	}).done(function( json ) {
			 $('.acctTextReg').html('');
			 obj = JSON.parse(json);
			 var str = '';
			 $.each(obj, function(i, item) {
				if( '{{Lang::get('COMMON.SUCESSFUL')}}' == item ){
					window.location.href = "{{route('homepage')}}";
				}else{
					$('.'+i+'_acctTextReg').html(item);
					
				}
			})
	
	});
}
  </script>
@stop

@section('content')
<div class="midSect">
   <div class="midSectInner">
     <div class="spacingTop"></div>
    
     <h2> {{Lang::get('public.ForgotPassword')}}</h2> 
	 
     <p>
		  We can help you reset your password and security info. First, enter your registered info and we will
		  send an account restoration link to your email
		  <br><br>
     </p>
	 
     <div class="acctContentReg">
       <div class="acctRow">
         <label> {{Lang::get('COMMON.USERNAME')}} :</label><input type="text" id="fg_username" ><span class="username_acctTextReg acctTextReg"></span>
      <div class="clr"></div>
    </div>
      <div class="acctRow">
      <label>{{Lang::get('public.EmailAddress')}} :</label><input type="text" id="fg_email"><span class="email_acctTextReg acctTextReg"></span>
      <div class="clr"></div>
      </div>
      
      <div class="submitAcct">
      <a href="javascript:void(0)" onClick="forgotpassword_submit()">{{Lang::get('public.Submit')}}</a>
      </div>
     </div>
   </div>
</div>
@stop

@section('bottom_js')

@stop