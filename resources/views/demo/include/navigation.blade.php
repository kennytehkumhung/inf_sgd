<div class="headerMain">
	<?php 
		if( $_SERVER['HTTP_HOST'] == 'rajabet.com' 
		||  $_SERVER['HTTP_HOST'] == 'rajabet.us' 
		||  $_SERVER['HTTP_HOST'] == 'rajabet.biz' 
		||  $_SERVER['HTTP_HOST'] == 'rajabets.com' 
		||  $_SERVER['HTTP_HOST'] == 'rajabet.com' 
		)
	
			echo '<div class="headerLeft"  style="background-image: url('.url().'/demo/img/logoRJ.png);"></div>';
		else
			echo '<div class="headerLeft"  style="background-image: url('.url().'/demo/img/logo.png);"></div>';
	?>
	<div class="headerRight">
		<!-- Date Time Chat -->
		<div class="headerDTT">
			<table width="auto" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td>
						<div class="lchat">
							<a href="#" onClick="livechat()" ><img src="{{url()}}/demo/img/lchat-icon-hover.png" alt="" width="121" height="29" id="lchat"></a>
						</div>
					</td>
					<td>							
						<!--30 Mar 2015, Mon, 15:03:12 (GMT +8)-->
						<?php echo date("d M Y");?>,
						<?php echo substr(date("l"),0,3);?>,
						<?php echo date(" H:i:s", time());?> (GMT +8)
					</td>
				</tr>
			</table>
		</div>
		<!-- Date Time Chat -->
		<div class="headerTop">
			<table width="auto" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td>
						<ul>
							<li>
							
								<div class="lgCont">
									<a href="{{route( 'language', [ 'lang'=> Lang::getLocale()])}}">
										<img src="{{url()}}/demo/img/{{Lang::getLocale()}}.png" width="22px"/> 
									</a>
								</div>	
								<center>
								<ul>		
								@if( Session::get('currency') == 'MYR' )
									@if( Lang::getLocale() != 'en' )
									<li>
										<a href="{{route( 'language', [ 'lang'=> 'en'])}}">
											<img src="{{url()}}/demo/img/en.png" width="22px"/>		
										</a>
									</li>	
									@endif
									@if( Lang::getLocale() != 'cn' )
									<li>
										<a href="{{route( 'language', [ 'lang'=> 'cn'])}}">
											<img src="{{url()}}/demo/img/cn.png" width="22px"/>
										</a>
									</li>
									@endif
									@if( Lang::getLocale() != 'vi' )
									<li>
										<a href="{{route( 'language', [ 'lang'=> 'vi'])}}">
											<img src="{{url()}}/demo/img/vi.png" width="22px"/>		
										</a>
									</li>	
									@endif
									@if( Lang::getLocale() != 'id' )
									<li>
										<a href="{{route( 'language', [ 'lang'=> 'id'])}}">
											<img src="{{url()}}/demo/img/id.png" width="22px"/>
										</a>
									</li>	
									@endif
								@elseif( Session::get('currency') == 'VND' )
									@if( Lang::getLocale() != 'vi' )
									<li>
										<a href="{{route( 'language', [ 'lang'=> 'vi'])}}">
											<img src="{{url()}}/demo/img/vi.png" width="22px"/>		
										</a>
									</li>	
									@endif
									
								@elseif( Session::get('currency') == 'IDR' )
									@if( Lang::getLocale() != 'id' )
									<li>
										<a href="{{route( 'language', [ 'lang'=> 'id'])}}">
											<img src="{{url()}}/demo/img/id.png" width="22px"/>		
										</a>
									</li>	
									@endif
									@if( Lang::getLocale() != 'en' )
									<li>
										<a href="{{route( 'language', [ 'lang'=> 'en'])}}">
											<img src="{{url()}}/demo/img/en.png" width="22px"/>		
										</a>
									</li>	
									@endif
								@endif
								</ul>	
								</center>
							</li>								
						</ul>
					</td>
					@if (!Auth::user()->check())
					<td>
						<div class="usrPw">
							<input type="text" placeholder="{{Lang::get('public.Username')}}" id="username">
						</div>
					</td>
					<td>
						<div class="usrPw1">
							<input type="password" placeholder="{{Lang::get('public.Password')}}" id="password">
						</div>
					</td>
					<td>
						<div class="usrPw2">
							<input type="text" placeholder="Code" id="code" onKeyPress="enterpressalert(event, this)">
						</div>
					</td>
					<td>
						<div class="usrPw2" style="margin-left:5px;">
							<img style="float: left;"  src="{{route('captcha', ['type' => 'login_captcha'])}}" width="60" height="24" />
						</div>
					</td>
					<td>
						<div class="fP">
							<a style="color:white;text-decoration: none;" href="{{route('forgotpassword')}}">{{Lang::get('public.ForgotPassword')}}</a>
						</div>
					</td>
					<td>
						<div class="submitHeader">
							<a onclick="login()" href="#">{{Lang::get('public.Submit')}}</a>
						</div>
					</td>
					<td><div class="joinHeader"><a href="{{route('register')}}">{{Lang::get('public.JoinNow')}}</a></div></td>
					@else
					<td>
						<span class="spacing1">
							<i class="fa fa-user"></i>{{Lang::get('public.Welcome')}}! {{Session::get('username')}} 
						</span>
					</td>
				<!-- clickable navigation menu-->
					<td>
						<div class="click-nav3">
							<ul class="no-js">
								<li>
									<a class="clicker3">{{Lang::get('public.Profile')}}<i class="fa fa-caret-down"></i></a>
									<ul>
										<li><a href="{{route('update-profile')}}">{{Lang::get('public.MyAccount')}}</a></li>
										<li><a href="{{route('update-password')}}">{{Lang::get('public.ChangePassword')}}</a></li>
									</ul>
								</li>
							</ul>
						</div>
					</td>
					<td>
						<div class="click-nav2">
							<ul class="no-js">
								<li>
									<a class="clicker2">{{Lang::get('public.Balance')}}<i class="fa fa-caret-down"></i></a>
									<div class="wlt">
										<table width="auto" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td align="center" style="color: yellow;font-size: 14px;">
													<strong>{{Lang::get('public.Wallet')}}:</strong>
												</td>
												<td>&nbsp;</td>
												<td align="center" style="color: yellow;font-size: 14px;">
													<strong>{{Lang::get('public.Balance')}}:</strong>
												</td>
											</tr>
											<tr>
												<td align="center">{{Lang::get('public.MainWallet')}}</td>
												<td>&nbsp;</td>
												<td align="center" class="main_wallet">0.00</td>
											</tr>
											@foreach( Session::get('products_obj') as $prdid => $object)
											<tr>
												<td align="center">{{$object->name}}</td>
												<td>&nbsp;</td>
												<td align="center" class="{{$object->code}}_balance">0.00</td>
											</tr>
											@endforeach
											<tr>
												<td align="center" style="color: yellow;font-size: 14px;"><strong>{{Lang::get('public.Balance')}}:</strong></td>
												<td>&nbsp;</td>
												<td align="center" style="color: red;font-size: 14px; border-bottom: 1px solid red;border-top: 1px solid red;"><strong id="total_balance">00.00</strong></td>
											</tr>
										</table>
										<div class="clr"></div>
									</div>
								</li>
							</ul>
						</div>
					</td>
					<td>
						<div class="click-nav1">
							<ul class="no-js">
								<li>
									<a class="clicker1">{{Lang::get('public.Fund')}}<i class="fa fa-caret-down"></i></a>
									<ul>
										<li><a href="{{route('deposit')}}">{{Lang::get('public.Deposit')}}</a></li>
										<li><a href="{{route('withdraw')}}">{{Lang::get('public.Withdrawal')}}</a></li>
										<li><a href="{{route('transaction')}}">{{Lang::get('public.TransactionHistory')}}</a></li>
										<li><a href="{{route('transfer')}}">{{Lang::get('public.Transfer')}}</a></li>
									</ul>
								</li>
							</ul>
						</div>
					</td>
					<td>
						<div class="submitHeader">
							<a href="{{route('logout')}}">{{Lang::get('public.Logout')}}</a>
						</div>
					</td>
				@endif
				</tr>
			</table>
		</div>
	</div>
</div>