@extends('idnwin99/master')

@section('title', 'Home')

@section('css')
@parent
<style type="text/css">
    .hpgBox3 li:before { content: '- '; font-weight: normal; }
    .popup_img { display: inline-block; position: absolute; z-index: 10001; margin: auto auto; top: 0px; bottom:0; left:0; right: 0; }
</style>
@endsection

@section('js')
@parent
<script type="text/javascript">
    $(function () {
        $("#masklayer").click(function () {
            $(".popup_img").fadeOut(100);
            $("#masklayer").fadeOut(100);
        });
        
        setInterval(updateJackport, 3000);
        initAutoMenu();
    });
    
    function updateJackport() {
        var jps = ["jp_1"];
        
        for (var i = 0; i < jps.length; i++) {
            var val = $("span[id^='" + jps[i] + "']").text();
            val = removeComma(val);
        
            if (val > 1358889) {
                val = 1356289;
            }

            var value = Math.floor(Math.random() * 10);
            value = value * 0.027;

            var value2 = (parseFloat(val) + value).toFixed(2);

            $("#" + jps[i]).text(addCommas(value2));
        }
    }

    function addCommas(str) {
        var parts = (str + "").split("."),
            main = parts[0],
            len = main.length,
            output = "",
            i = len - 1;

        while (i >= 0) {
            output = main.charAt(i) + output;
            if ((len - i) % 3 === 0 && i > 0) {
                output = "," + output;
            }
            --i;
        }
        // put decimal part back
        if (parts.length > 1) {
            output += "." + parts[1];
        }
        return output;
    }

    function removeComma(str) {
        var parts = (str + "").split(",");
        var output = "";

        for (var i = 0; i < parts.length; i++) {
            output += parts[i];
        }
        return output;
    }
    
    function initAutoMenu() {
        autoMenuAppendLinks("mg_menu_sportsbook", ["auto_menu_sportsbook"]);
        autoMenuAppendLinks("mg_menu_livecasino", ["auto_menu_livecasino", "auto_menu_livecasino2"]);
        autoMenuAppendLinks("mg_menu_slots", ["auto_menu_slots", "auto_menu_slots2"]);
    }
    
    function autoMenuAppendLinks(mgLinkId, destinationIds) {
        var count = 0;
        var hasTwoColumn = (destinationIds.length > 1);
        
        $("#" + mgLinkId + " li").each(function () {
            var link = $(this).clone();
            var shortName = link.children("a:eq(0)").data("name-short");
            
            if (typeof shortName != "undefined" && shortName.length > 0) {
                link.children("a:eq(0)").text(shortName);
            }
            
            if (hasTwoColumn) {
                if (count >= 3) {
                    $("#" + destinationIds[1]).append("<li>" + link.html() + "</li>");
                } else {
                    $("#" + destinationIds[0]).append("<li>" + link.html() + "</li>");
                }
            } else {
                $("#" + destinationIds[0]).append("<li>" + link.html() + "</li>");
            }
            
            count++;
        });
    };
	
    function closePopup() {
        $(".popup_img").fadeOut(100);
        $("#masklayer").fadeOut(100);
    }
</script>
@endsection

@section('content')

    @if( $popup_banner )
        <div id="masklayer" style="height: 100%;position: fixed;opacity: 0.6;background-color: black;z-index: 10001;left: 0px;right: 0px;top: 0px;display:block;" onclick="closePopup();"></div>
        <img src="{{$popup_banner}}" class="popup_img">
    @endif

    <div class="midSect">
        <div class="sliderCont aac3">
            <div class="sliderContInner">
                <div class="box_skitter box_skitter_large">
                    <ul>
                        <li><a href="#cubeRandom"><img src="{{ asset('/idnwin99/img/banner/slide1.png') }}" class="directionLeft" /></a></li>
                        @if(isset($banners))
                            @foreach( $banners as $key => $banner )
                                <li><a target="_blank" href="@if($banner->url != 'none'){{$banner->url}}@endif"><img src="{{$banner->domain}}/{{$banner->path}}" class="directionLeft" /></a></li>
                            @endforeach
                        @endif
                    </ul>
                </div>
                <div class="jp">
                    <div class="jpNum">
                        IDR <span id="jp_1">1,356,300.79</span>
                    </div>
                </div>
                <div class="clr"></div>
            </div>
        </div>

        <div class="anmnt">
            <div class="anmntInner">
                <span><img src="{{ asset('/idnwin99/img/spade.png') }}"></span>
                <marquee>{{ App\Http\Controllers\User\AnnouncementController::index() }}</marquee>
                <div class="clr"></div>
            </div>
        </div>

        <div class="midSectInner">
            <div class="midBG">
                <div class="midBGbx">
                    <div id="cf">
                        <a href="{{ route('sport') }}">
                            <img class="bottom" src="{{ asset('/idnwin99/img/sp-thumb-hover.png') }}" />
                            <img class="top" src="{{ asset('/idnwin99/img/sp-thumb.png') }}" />
                        </a>
                    </div>
                </div>
                
                <div class="midBGbx">
                    <div id="cf">
                        <a href="{{ route('slotgames') }}">
                            <img class="bottom" src="{{ asset('/idnwin99/img/slot-thumb-hover.png') }}" />
                            <img class="top" src="{{ asset('/idnwin99/img/slot-thumb.png') }}" />
                        </a>
                    </div>
                </div>
                
                <div class="midBGbx">
                    <div id="cf">
                        <a href="{{ route('livecasino') }}">
                            <img class="bottom" src="{{ asset('/idnwin99/img/casino-thumb-hover.png') }}" />
                            <img class="top" src="{{ asset('/idnwin99/img/casino-thumb.png') }}" />
                        </a>
                    </div>
                </div>
                
                <div class="midBGbx">
                    <div id="cf">
                        <a href="{{ route('fbl') }}?page=intro">
                            <img class="bottom" src="{{ asset('/idnwin99/img/4d-thumb-hover.png') }}" />
                            <img class="top" src="{{ asset('/idnwin99/img/4d-thumb.png') }}" />
                        </a>
                    </div>
                </div>
                
                <div class="clr"></div>
                
                <div class="cont1">
                    <div class="hpgBox">
                        <div class="boxTit">
                            4 EASY STEP TO WIN
                        </div>
                        <div class="boxReg">
                            <span>1.</span>
                            Join Now
                            <div class="subT">
                                Quick &amp; Easy "Register Now"
                            </div>
                        </div>
                        <div class="boxDep">
                            <span>2.</span>
                            Deposit
                            <div class="subT">
                                To Claim Your Welcome Bonus
                            </div>
                        </div>
                        <div class="boxTrsf">
                            <span>3.</span>
                            Start Playing
                            <div class="subT">
                                Begin your Gaming Adventure
                            </div>
                        </div>
                        <div class="boxWith">
                            <span>4.</span>
                            REGISTER NOW
                        </div>
                    </div>
                    
                    <div class="hpgSep">
                        <img src="{{ asset('/idnwin99/img/hpg-sep.png') }}">
                    </div>
                    
                    <div class="hpgBox2">
                        <div class="boxTit">
                            BANKING
                        </div>
                        <div class="box2in">
                            <div class="inTit">DEPOSIT</div>
                            <p>ATM/CDM, ONLINE TRANSFER, LOCAL BANK TRANSFER</p>
                            <div class="inner1">
                                MIN:IDR25,000
                            </div>
                            <div class="inner2">
                                MAX:IDR100,000,000
                            </div>
                            <div class="inner3">
                                5MIN
                            </div>
                            <div class="clr"></div>
                        </div>
                        <div class="box2in adj05">
                            <div class="inTit">WITHDRAWAL</div>
                            <p>QUICK WITHDRAWAL:</p>
                            <div class="inner1">
                                MIN:IDR25,000
                            </div>
                            <div class="inner2">
                                MAX:IDR100,000,000
                            </div>
                            <div class="inner3">
                                5MIN
                            </div>
                            <div class="clr"></div>
                            <br>
                            <p>LOCAL BANK TRANSFER:</p>
                            <div class="inner1">
                                MIN:IDR25,000
                            </div>
                            <div class="inner2">
                                MAX:IDR100,000,000
                            </div>
                            <div class="inner3">
                                5MIN
                            </div>
                            <div class="clr"></div>
                        </div>
                    </div>
                    
                    <div class="hpgSep">
                        <img src="{{ asset('/idnwin99/img/hpg-sep.png') }}">
                    </div>
                    
                    <div class="hpgBox3">
                        <div class="boxTit1">
                            products
                        </div>
                        <div class="box3in">
                            <div class="inTit1">{{ Lang::get('public.SportsBook') }}</div>
                            <ul id="auto_menu_sportsbook">
                            </ul>
                        </div>
                        <div class="box3in1">
                            <div class="inTit1">{{ Lang::get('public.LiveCasino') }}</div>
                            <div class="left">
                                <ul id="auto_menu_livecasino">
                                </ul>
                            </div>
                            <div class="left1">
                                <ul id="auto_menu_livecasino2">
                                </ul>
                            </div>
                            <div class="clr"></div>
                        </div>
                        <div class="box3in2">
                            <div class="inTit1">{{ Lang::get('public.Slots') }}</div>
                            <div class="left">
                                <ul id="auto_menu_slots">
                                </ul>
                            </div>
                            <div class="left1">
                                <ul id="auto_menu_slots2">
                                </ul>
                            </div>
                            <div class="clr"></div>
                        </div>
                    </div>
                    
                    <div class="clr"></div>
                </div>
                
                <div class="clr"></div>
            </div>
        </div>
    </div>
@endsection