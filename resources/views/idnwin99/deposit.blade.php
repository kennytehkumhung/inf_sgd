@extends('idnwin99/master')

@section('title', 'Deposit')

@section('css')
@parent

<style type="text/css">
    .bkOpt input:focus { width: 25px !important; }
</style>
@endsection

@section('js')
@parent

<script>
    $(document).ready(function() {
		getBalance(true);
		
		// Setup date.
		$('#deposit_year').val('{{ date('Y') }}');
		$('#deposit_month').val('{{ date('m') }}');
		$('#deposit_day').val('{{ date('d') }}');
    });
    setInterval(update_mainwallet, 10000);
    function deposit_submit() {
        if (!checkAmount()) {
            return false;
        }

        $('#date').val($('#deposit_year').val() + '-' + $('#deposit_month').val() + '-' + $('#deposit_day').val());
        var file_data = $("#receipt").prop("files")[0];
        var form_data = new FormData();
        form_data.append("file", file_data);
        var data = $('#deposit_form').serializeArray();
        var obj = {};
        for (var i = 0, l = data.length; i < l; i++) {
            if (data[i].name == "refno"){
                form_data.append(data[i].name, 'none');
            } else {
                form_data.append(data[i].name, data[i].value);
            }
        }

        form_data.append('_token', '{{csrf_token()}}');
        $.ajax({
            type: "POST",
            url: "{{route('deposit-process')}}?",
            dataType: 'script',
            cache: false,
            contentType: false,
            processData: false,
            data: form_data, // Setting the data attribute of ajax with file_data
            type: 'post',
            beforeSend: function() {
                $('#deposit_sbumit_btn').attr('onclick', '').text("{{ Lang::get('public.Loading') }}...");
            },
            complete: function(json) {
                obj = JSON.parse(json.responseText);
                var str = '';
                $.each(obj, function(i, item) {
                    if ('{{Lang::get('COMMON.SUCESSFUL')}}' == item) {
                        alert('{{Lang::get('COMMON.SUCESSFUL')}}');
                        window.location.href = "{{route('transaction')}}";
                    } else {
                        //$('.'+i+'_acctTextReg').html(item);
                        $('#deposit_sbumit_btn').attr('onclick', 'deposit_submit()').text("{{ Lang::get('public.Submit') }}");
                        str += item + '<br>';
                    }
                });
                $('.failed_message').html(str);
            },
        });
    }
	
    function checkAmount() {
        var amount = parseFloat($("#amount").val());
        
        if (isNaN(amount) || amount < 30000) {
            alert("{{ Lang::get('public.MinimumDepositAmountX', ['p1' => '30,000.00']) }}");
            return false;
        }
        
        return true;
    }
    
	function onBkOptChanging(obj) {
		$('.bkOpt .box1').hide(300);

		var rid = $(obj).val();
		$('.bkOpt .box1_' + rid).show(300);
	}
</script>
@endsection

@section('content')
<div class="midSect">
    
    @include('idnwin99.include.notice')
    
    <div class="midSectInner">
        <div class="midSectCont">
            
            @include('idnwin99/include/balance_top')

            @include('idnwin99/include/submenu_top', array('type' => 'fund'))
            
            <!--ACCOUNT TITLE-->
            <div class="title_bar">
                <span>{{ Lang::get('public.Deposit') }}</span>
            </div>
            <!--ACCOUNT TITLE-->
            <!--ACCOUNT CONTENT-->
            <div class="DepContent">
                <div class="depLeft">
                    <div class="cont">
                        <ul>
                            <li>Option:	Bank Transfer</li>
                            <li>Mode:	Offline</li>
                            <li>Min/Max Limit:	30,000.00 / 100,000,000.00</li>
                            <li>Daily Limit:	100,000,000.00</li>
                            <li>Total Allowed:	100,000,000.00</li>
                        </ul>
                    </div>
                    <div class="cont">
                        <p>
                            BCA &amp; Mandiri are favourite for our speedy crediting of funds to your account and experience. Thus, please use Bank Transfer via your local bank account. We do not accept all kinds of deposit by "Cheque" or "Bank Draft" (Company OR Personal Cheque) as your deposit method.
                        </p>
                        <p>
                            Note: Once you have successfully submitted your deposit form and once your funds is cleared in our account, just leave it to our team to process your transactions as speedy as possible. If more than 10 minutes, let us know by clicking here and our Customer Service support will assist you 24/7 anytime. 
                        </p>
                    </div>
                </div>

                <form id="deposit_form">
                    <input type="hidden" id="rules" name="rules" value="1" />
                    
                    <div class="depRight">
                        <span class="wallet_title"><i class="fa fa-money"></i>{{ Lang::get('public.Deposit') }}</span>
                        <div class="acctRow">
                            <label>{{ Lang::get('public.Amount') }}* :</label><input type="text" id="amount" name="amount" class="depRight5" />
                            <div class="clr"></div>
                        </div>
                        <div class="acctRowR">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td rowspan="3" style="vertical-align: top; width: 108px; padding-left: 4px;">
                                        <label>{{ Lang::get('public.BankingOptions') }}* :</label>
                                    </td>
                                    <td>
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="bkOpt">
                                            @foreach($banks as $key => $bank)
                                                <tr>
                                                    <td>
                                                        <label style="padding: 3px 0;">
                                                            <input type="radio" id="bank" name="bank" class="radiobtn" value="{{ $bank['bnkid'] }}" title="{{ $bank['name'] }}" onchange="onBkOptChanging(this);">
                                                            <img src="{{ $bank['image'] }}" style="max-height: 25px; width: auto;">
                                                            <div class="red box1 box1_{{ $bank['bnkid'] }}" style="padding-left: 32px; line-height: 20px; display: none;">
                                                                <p>
                                                                    <strong>{{ Lang::get('public.ImportantInfo') }}</strong><br>
                                                                    {{ $bank['bankaccname'] }}<br>
                                                                    {{ $bank['bankaccno'] }}
                                                                </p>
                                                            </div>
                                                        </label>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <div class="clr"></div>
                        </div>
                        <div class="acctRow">
                            <label>{{ Lang::get('public.ReferenceNo') }}* :</label><input type="text" id="refno" name="refno" class="depRight5" />
                            <div class="clr"></div>
                        </div>
                        <div class="acctRow">
                            <label>{{ Lang::get('public.DepositMethod') }}* :</label>
                            <select id="type" name="type" class="depRight3" style="width: 160px;">
                                <option value="3">{{ Lang::get('public.OverCounter') }}</option>
                                <option value="1">{{ Lang::get('public.InternetBanking') }}</option>
                                <option value="2">{{ Lang::get('public.ATMBanking') }}</option>
                            </select>
                            <div class="clr"></div>
                        </div>
                        <div class="acctRow">
                            <label>{{ Lang::get('public.DateTime') }} :</label>
                            <select id="deposit_day" class="depRight4" style="margin-right: 8px;">
                                @for ($i = 1; $i <= 31; $i++)
                                <option value="{{ str_pad($i, 2, '0', STR_PAD_LEFT) }}">{{ str_pad($i, 2, '0', STR_PAD_LEFT) }}</option>
                                @endfor
                            </select>
                            <select id="deposit_month" class="depRight4" style="margin-right: 8px;">
                                @for ($i = 1; $i <= 12; $i++)
                                <option value="{{ str_pad($i, 2, '0', STR_PAD_LEFT) }}">{{ str_pad($i, 2, '0', STR_PAD_LEFT) }}</option>
                                @endfor
                            </select>
                            <select id="deposit_year" class="depRight4" style="margin-right: 8px;">
                                <option value="{{ (new DateTime())->format('Y') }}">{{ (new DateTime())->format('Y') }}</option>
                                <option value="{{ (new DateTime('-1 year'))->format('Y') }}">{{ (new DateTime('-1 year'))->format('Y') }}</option>
                            </select>
                            <input type="hidden" id="date" name="date" />
                            <div class="clr"></div>
                        </div>
                        <div class="acctRow">
                            <label>&nbsp;</label>
                            <select name="hours" class="depRight4" style="margin-right: 8px;">
                                @for ($i = 1; $i <= 12; $i++)
                                <option value="{{ str_pad($i, 2, '0', STR_PAD_LEFT) }}">{{ str_pad($i, 2, '0', STR_PAD_LEFT) }}</option>
                                @endfor
                            </select>
                            <select name="minutes" class="depRight4" style="margin-right: 8px;">
                                @for ($i = 1; $i <= 59; $i++)
                                <option value="{{ str_pad($i, 2, '0', STR_PAD_LEFT) }}">{{ str_pad($i, 2, '0', STR_PAD_LEFT) }}</option>
                                @endfor
                            </select>
                            <select name="range" class="depRight4" style="margin-right: 8px;">
                                <option value="AM">AM</option>
                                <option value="PM">PM</option>
                            </select>
                            <div class="clr"></div>
                        </div>
                        <div class="acctRowR">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td rowspan="3" style="vertical-align: top; width: 108px; padding-left: 4px;">
                                        <label>{{ Lang::get('public.Promotion') }}* :</label>
                                    </td>
                                    <td>
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="bkOpt">
                                            @foreach($promos as $key => $promo)
                                                <tr>
                                                    <td>
                                                        <label style="padding: 3px 0;">
                                                            <input type="radio" name="promo" class="radiobtn" value="{{ $promo['code'] }}">
                                                            <img src="{{$promo['image']}}" style="max-height: 60px; width: auto;">
                                                        </label>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            
                                            <tr>
                                                <td>
                                                    <label style="padding: 3px 0; width: 100%;">
                                                        <input type="radio" name="promo" class="radiobtn" value="0"> {{Lang::get('public.IDoNotWantPromotion')}}
                                                    </label>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <div class="clr"></div>
                        </div>
                        <div class="acctRow2">
                            <label>{{ Lang::get('public.Attachment') }} :</label>
                            <input class="upload" type="file" id="receipt" name="receipt">
                            <div class="clr"></div>
                        </div>
                        <div class="acctRow2">
                            <label>&nbsp;</label>
                            * {{ Lang::get('public.MaxUploadFileSize', array('p1' => '6MB')) }}
                            <div class="clr"></div>
                        </div>
                        <div class="acctRow2">
                            <label>&nbsp;</label>
                            <label style="width: 300px;">
                                <input class="radiobtn" type="radio" name="rules" style="width: 25px; padding-left: 0; margin-left: 0;">{{Lang::get('public.IAlreadyUnderstandRules')}}
                            </label>
                            <div class="clr"></div>
                        </div>
                        <div class="submitAcct">
                            <a id="deposit_sbumit_btn" href="javascript:void(0);" onclick="deposit_submit()">{{ Lang::get('public.Submit') }}</a>
                        </div>
                        <br><br>
                        <span class="failed_message acctTextReg" style="display:block;float:left;height:100%;color:red;"></span>
                    </div>
                </form>
                <div class="clr"></div>

                <img src="{{ asset('/idnwin99/img/bank-operation-id.png') }}" style="max-width: 998px;" />
            </div>
            <!--ACCOUNT CONTENT-->
            <div class="clr"></div>
        </div>
    </div>
</div>
@endsection