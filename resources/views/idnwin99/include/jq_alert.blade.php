<div id="jAlert" style="display:none; cursor: default">
    <p id="jAlertMessage"></p>
    <input type="button" id="jAlertBtnOk" value="OK" style="min-width: 60px !important;" />
</div>

<script type="text/javascript" src="{{ asset('/idnwin99/resources/js/jquery.blockUI.js') }}"></script>
<script type="text/javascript">
    function jAlert(msg, fn) {
        if (fn === undefined || fn == null) {
            fn = function() {};
        }

        var alertObj = $("#jAlert");

        $("#jAlertMessage", alertObj).html(msg.replace(/(?:\r\n|\r|\n)/g, '<br>'));
        $("#jAlertBtnOk", alertObj).click(function() {
            $.unblockUI();
            return false;
        });

        $.blockUI({
            centerX: true,
            centerY: false,
            message: alertObj,
            css: { 'padding-bottom': '15px', 'z-index': '2101' },
            overlayCSS: { 'z-index': '2100' },
            onUnblock: fn
        });

        $('.blockOverlay').on('click touchstart', $.unblockUI);
    }
</script>
