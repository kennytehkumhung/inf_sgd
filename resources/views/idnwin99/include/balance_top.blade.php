<div class="walletTop">
    <div class="wallet1">
        <div class="main-set-title">{{ Lang::get('public.MainWallet') }}</div>
        <div class="main-setPrice main_wallet">0.00</div>
    </div>
    
    <?php $index = 2; ?>
    @foreach( Session::get('products_obj') as $prdid => $object)
        <div class="wallet{{ $index }}">
            <div class="main-set-title">{{$object->name}}</div>
            <div class="main-setPrice {{$object->code}}_balance">0.00</div>
        </div>
        <?php $index < 9 ? $index++ : ''; ?>
    @endforeach
    
    <div class="wallet10">
        <div class="main-set-title">{{ Lang::get('public.Total') }}</div>
        <div class="main-setPrice top_total_balance"><img class="total_balance_loading" src="{{ asset('/front/img/ajax-loading.gif') }}" width="20" height="10"></div>
    </div>
</div>
