<div class="inlineAccMenu">
    <ul>
        @if (isset($type) && $type == 'fund')
            <li><a href="{{ route('deposit') }}">{{Lang::get('public.Deposit')}}</a></li>
            <li><a href="{{ route('withdraw') }}">{{Lang::get('public.Withdrawal')}}</a></li>
            <li><a href="{{ route('transaction') }}">{{Lang::get('public.TransactionHistory')}}</a></li>
            <li><a href="{{ route('transfer') }}">{{Lang::get('public.Transfer')}}</a></li>
            <!--<li><a href="#4">{{ Lang::get('public.MultipleTransfer') }}</a></li>-->
        @else
            <li><a href="{{ route('update-profile') }}">{{ Lang::get('public.Profile') }}</a></li>
            <li><a href="{{ route('update-password') }}">{{ Lang::get('public.AccountPassword2') }}</a></li>
            <li><a href="{{ route('profitloss') }}">{{ Lang::get('public.Statement') }}</a></li>
        @endif
    </ul>
</div>
