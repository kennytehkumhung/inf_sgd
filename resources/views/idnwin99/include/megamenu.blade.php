<div class="navContInner">
    <ul>
        <li><a href="{{ route('homepage') }}">{{ Lang::get('public.Home') }}</a></li>
        <li>
            <a href="{{ route('sport') }}">{{ Lang::get('public.SportsBook') }}</a>
            <ul id="mg_menu_sportsbook">
                @if(in_array('IBCX',Session::get('valid_product')))
                    <li><a href="{{ route('ibc') }}">I-Sport</a></li>
                @endif
                @if(in_array('WFT',Session::get('valid_product')))
                    <li><a href="{{ route('wft') }}">W-Sport</a></li>
                @endif
            </ul>
        </li>
        <li>
            <a href="{{ route('livecasino') }}">{{ Lang::get('public.LiveCasino') }}</a>
            <ul id="mg_menu_livecasino">
                @if(in_array('PLTB',Session::get('valid_product')))
                    <li><a href="#" onclick="{{ Auth::user()->check() ? 'window.open("'.route('pltbiframe').'","casino_pt","width=1150,height=830");' : 'jAlert("'.Lang::get('COMMON.PLEASELOGIN').'");' }}" data-name-short="Playtech">Playtech Casino</a></li>
                @endif
                @if(in_array('W88',Session::get('valid_product')))
                    <li><a href="#" onclick="{{ Auth::user()->check() ? 'window.open("'.route('w88' , [ 'type' => 'casino' , 'category' => 'live']).'","casino_gp","width=1150,height=830");' : 'jAlert("'.Lang::get('COMMON.PLEASELOGIN').'");' }}" data-name-short="Gameplay">Gameplay Casino</a></li>
                @endif
                @if(in_array('AGG',Session::get('valid_product')))
                    <li><a href="#" onclick="{{ Auth::user()->check() ? 'window.open("'.route('agg').'","casino_ag","width=1150,height=830");' : 'jAlert("'.Lang::get('COMMON.PLEASELOGIN').'");' }}">AG Casino</a></li>
                @endif
            </ul>
        </li>
        <li>
            <a href="{{ route('slotgames') }}">{{ Lang::get('public.Slots') }}</a>
            <ul id="mg_menu_slots">
                @if(in_array('JOK',Session::get('valid_product')))
                    <li><a href="{{ route('slot', ['type' => 'jok']) }}">Joker Slot</a></li>
                @endif
                @if(in_array('AGG',Session::get('valid_product')))
                    <!--<li><a href="#" onclick="{{ Auth::user()->check() ? 'window.open("'.route('agg').'","casino_ag","width=1150,height=830");' : 'jAlert("'.Lang::get('COMMON.PLEASELOGIN').'");' }}">AG Slot</a></li>-->
                    <li><a href="{{ route('slot', ['type' => 'agg']) }}">AG Slot</a></li>
                @endif
                @if(in_array('PLTB',Session::get('valid_product')))
                    <li><a href="{{ route('slot', ['type' => 'pltb']) }}">PT Slot</a></li>
                @endif
            </ul>
        </li>
        <!--<li><a href="poker.html">{{ Lang::get('public.Poker') }}</a></li>-->
        <!--<li><a href="{{ route('4d') }}">{{ Lang::get('public.d4') }}</a></li>-->
        @if(in_array('FBL',Session::get('valid_product')))
            <li><a href="{{ route('fbl') }}?page=intro">{{ Lang::get('public.Fireball') }}</a></li>
        @endif
        <li><a href="{{ route('promotion') }}">{{ Lang::get('public.Promotion') }}</a></li>
        <li><a href="{{ route('mobile') }}">{{ Lang::get('public.Mobile') }}</a></li>
        <li><a href="{{ route('contactus') }}">{{ Lang::get('public.ContactUs') }}</a></li>
    </ul>
</div>
