@extends('idnwin99/master')

@section('title', 'Mobile')

@section('css')
@parent

    <style type="text/css">
        .pickTit * { color: #fff; font-weight: bold; }
    </style>
@endsection

@section('js')
@parent

<script type="text/javascript">
    $(function($) {
        $(".gamedesc").hide();

        $(".picker").click(function(){       
            $(".selected1").removeClass('selected1');
            //Load background string from data of clicked element
            var pickerObj = $(this);
            var bVal = pickerObj.attr('data-bkgnd');
            pickerObj.addClass('selected1');
            //Set display's background to retrieved background string
            if ($("#productDisplay").css('background-image') == '' || $("#productDisplay").css('background-image')) {
                $("#productDisplay").hide().css({'background-image': "url(" + bVal + ")"}).fadeIn(225);
            } else {
                $("#productDisplay").fadeOut(200).css({'background-image': "url(" + bVal + ")"}).fadeIn(225);   
            }

            // Show description.
            $(".gamedesc").hide();
            $("." + pickerObj.data("game") + "_help").show();
        });
    });
</script>
@endsection

@section('content')
<div class="midSect">
    
    @include('idnwin99.include.notice')
    
    <div class="midSectInner">
        <div class="midSectCont">
            <div class="mobileCont">
                <div class="pickBox">
                    <div class="pickTit">
                        1. {{ Lang::get('public.SelectYourGame') }}
                    </div>
                    <div class="picker" data-game="pt" data-bkgnd="{{ asset('/idnwin99/img/qr/PT-QR.png') }}">Playtech</div>
                    <div class="picker" data-game="jk" data-bkgnd="{{ asset('/idnwin99/img/qr/joker-qr.png') }}">Joker</div>
                    <div class="picker" data-game="gp" data-bkgnd="{{ asset('/idnwin99/img/qr/gp-qr.png') }}">Gameplay</div>
                    <div class="picker" data-game="ag" data-bkgnd="{{ asset('/idnwin99/img/qr/ag-qr.png') }}">AG Gaming</div>
                    <div class="clear"></div>
                    <div class="pickTit aac1">
                        2. {{ Lang::get('public.ScanQrCodeAndDownload') }}
                        <div class="gamedesc pt_help">
                            {{ Lang::get('public.OrViaDownloadLinksBelow') }}:
                            <ul>
                                <li><a href="http://m.gm175888.com/download.html" target="_blank">Android Slot</a></li>
                                <li><a href="http://m.gm175888.com/live/download.html" target="_blank">Android Live Casino</a></li>
                                <li><a href="http://cdn.fruitfarm88.com/generic/d/setupglx.exe" target="_blank">Desktop</a></li>
                            </ul>
                        </div>

                        <div class="gamedesc jk_help">
                            {{ Lang::get('public.OrViaDownloadLinksBelow') }}:
                            <ul>
                                <li><a href="http://www.gwc388.net/" target="_blank">iOS / Android</a></li>
                                <li><a href="http://www.gwc388.net/Download/Lobby" target="_blank">Desktop</a></li>
                            </ul>
                        </div>

                        <div class="gamedesc ag_help">
                            {{ Lang::get('public.OrViaDownloadLinksBelow') }}:
                            <ul>
                                <li><a href="http://agmbet.com/" target="_blank">Click to download</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="gamedesc pt_help">
                        <div class="pickTit acc1">
                            @if (Lang::getLocale() == 'cn')
                                3. 请使用您的用户名登入，用户名必须全部大写并在字首部分添加 "IDN_"
                            @else
                                3. Login with your username by adding prefix "IDN_" infront of your username. Username field must be ALL with Capital Letters.
                            @endif
                        </div>
                        <div class="pickTit acc1">
                            @if (Auth::user()->check())
                                {{ Lang::get('public.Username') }}: IDN_{{ strtoupper(Session::get('username')) }}<br>
                                {{ Lang::get('public.Password') }}: {{ Lang::get('public.SameAsLoginPassword') }}
                            @else
                                {{ Lang::get('public.PleaseLogin') }}!
                            @endif
                        </div>
                    </div>
                    <div class="clear"></div>

                    <div class="gamedesc jk_help">
                        <div class="pickTit acc1">
                            @if (Lang::getLocale() == 'cn')
                                3. 请使用您的用户名登入，用户名必须全部大写并在字首部分添加 "{{ Config::get(Session::get('currency') . '.jok.appid') }}.IDN_"
                            @else
                                3. Login with your username by adding prefix "{{ Config::get(Session::get('currency') . '.jok.appid') }}.IDN_" infront of your username. Username field must be ALL with Capital Letters.
                            @endif
                        </div>
                        <div class="pickTit acc1">
                            @if (Auth::user()->check())
                                {{ Lang::get('public.Username') }}: {{ Config::get(Session::get('currency') . '.jok.appid') }}.IDN_{{ strtoupper(Session::get('username')) }}<br>
                                {{ Lang::get('public.Password') }}: {{ Lang::get('public.SameAsLoginPassword') }}
                            @else
                                {{ Lang::get('public.PleaseLogin') }}!
                            @endif
                        </div>
                    </div>
                    <div class="clear"></div>

                    <div class="gamedesc ag_help">
                        <div class="pickTit acc1">
                            @if (Lang::getLocale() == 'cn')
                                3. 请登入<a href="#" onclick="{{ Auth::user()->check() ? 'window.open("'.route('agg').'","casino_ag","width=1150,height=830");' : 'jAlert("'.Lang::get('COMMON.PLEASELOGIN').'");' }}">网页版游戏厅</a>，点击左下角的手机图查看安装与登入教程。
                            @else
                                3. Please login into <a href="#" onclick="{{ Auth::user()->check() ? 'window.open("'.route('agg').'","casino_ag","width=1150,height=830");' : 'jAlert("'.Lang::get('COMMON.PLEASELOGIN').'");' }}">game lobby</a>, click the mobile phone icon at bottom left side, and follow Installation &amp; Login Guide.
                            @endif
                        </div>
                    </div>
                    <div class="clear"></div>

                </div>
                <div id="productDisplay"></div>
            </div>
            <div class="clr"></div>
        </div>
    </div>
</div>
@endsection