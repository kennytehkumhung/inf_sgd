@extends('idnwin99/master')

@section('title', 'Bet History')

@section('css')
@parent

<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<link href="{{ asset('/idnwin99/resources/css/table.css') }}" rel="stylesheet">
@endsection

@section('js')
@parent

<script src="{{ asset('/idnwin99/resources/js/jquery-ui.js') }}"></script>
<script src="{{ asset('/idnwin99/resources/js/slick.min.js') }}"></script>
<script>
    transaction_history();
    $('.caro').slick({
        infinite: true,
        slidesToShow: 2,
        slidesToScroll: 2,
        autoplay: true,
        variableWidth: true,
        arrows: false
    });

    $(function () {
        $(".datepicker").datepicker({dateFormat: 'yy-mm-dd', defaultDate: new Date()});
    });

    function transaction_history() {
        $.ajax({
            type: "POST",
            url: "{{action('User\TransactionController@profitlossProcess')}}",
            data: {
                _token: "{{ csrf_token() }}",
                date_from: $('#date_from').val(),
                date_to: $('#date_to').val(),
                record_type: $('#record_type').val()

            },
        }).done(function (json) {

            //var str;
            var str = '';
            str += '	<tr ><td>{{ Lang::get('public.Date') }}</td><td>{{ Lang::get('public.Wager') }}</td><td>{{ Lang::get('public.Stake') }}</td><td>{{ Lang::get('public.WinLose') }}</td><td>{{ Lang::get('public.RealBet') }}</td></tr>';

            obj = JSON.parse(json);
            //alert(obj);
            $.each(obj, function (i, item) {
                str += '<tr><td style="text-decoration: underline;color:blue;">' + item.date + '</td><td>' + item.wager + '</td><td>' + item.stake + '</td><td>' + item.winloss + '</td><td>' + item.validstake + '</td></tr>';

            })

            //alert(json);
            $('#trans_history').html(str);

        });
    }

    function transaction_history_product(date_from, product) {
        $.ajax({
            type: "POST",
            url: "{{action('User\TransactionController@profitlossProcess')}}",
            data: {
                _token: "{{ csrf_token() }}",
                date_from: date_from,
                group_by: product,
            },
        }).done(function (json) {

            //var str;
            var str = '';
            str += '	<tr ><td>{{ Lang::get('public.Product') }}</td><td>{{ Lang::get('public.Wager') }}</td><td>{{ Lang::get('public.Stake') }}</td><td>{{ Lang::get('public.WinLose') }}</td><td>{{ Lang::get('public.RealBet') }}</td></tr>';

            obj = JSON.parse(json);
            //alert(obj);
            $.each(obj, function (i, item) {
                str += '<tr><td>' + item.product + '</td><td>' + item.wager + '</td><td>' + item.stake + '</td><td>' + item.winloss + '</td><td style="text-decoration: underline;color:blue;">' + item.validstake + '</td></tr>';

            })

            //alert(json);
            $('#trans_history_product').html(str);

        });
    }

    setInterval(updateTrans, 10000);
    setInterval(update_mainwallet, 10000);

    function updateTrans() {
        $("#trans_history_button").trigger("click");
    }
</script>
@endsection

@section('content')
<div class="midSect">
    
    @include('idnwin99.include.notice')
    
    <div class="midSectInner">
        <div class="midSectCont">
            
            @include('idnwin99/include/balance_top')

            @include('idnwin99/include/submenu_top', array('type' => 'profile'))
            
            <!--ACCOUNT MANAGAMENT MENU-->
            <!--ACCOUNT TITLE-->
            <div class="title_bar">
                <span>{{ Lang::get('public.Statement') }}</span>
            </div>
            <!--ACCOUNT TITLE-->
            <!--ACCOUNT CONTENT-->
            <div class="acctContent">
                <span class="wallet_title"><i class="fa fa-pencil"></i>{{ Lang::get('public.BetHistory') }}</span>
                <div class="acctRow">
                    <label>{{ Lang::get('public.DateFrom') }} :</label><div class="cstInput1"><input type="text" class="datepicker" id="date_from" style="cursor:pointer;" value="{{ date('Y-m-d') }}">-<input type="text" class="datepicker" id="date_to" style="cursor:pointer;" value="{{ date('Y-m-d') }}"></div>
                    <div class="clr"></div>
                </div>
                <div class="submitAcct">
                    <a id="trans_history_button" href="javascript:void(0);" onClick="transaction_history()"> {{Lang::get('public.Submit')}}</a>
                </div>
                <div class="clr"></div>
                <br>
                <div class="table" style="display:block;">
                    <table id="trans_history">
                        <tr>
                            <td>{{Lang::get('public.Date')}}</td>
                            <td>{{Lang::get('public.Wager')}}</td>
                            <td>{{Lang::get('public.Stake')}} </td>
                            <td>{{Lang::get('public.WinLose')}}</td>
                            <td>{{Lang::get('public.RealBet')}}</td>
                        </tr>
                        <tr>
                            <td colspan="6"><h2><center>{{ Lang::get('public.NoDataAvailable') }}</center></h2></td>
                        </tr>
                    </table>
                    <br><br>
                    <table id="trans_history_product">
                        <tr>
                            <td>{{Lang::get('public.Product')}}</td>
                            <td>{{Lang::get('public.Wager')}}</td>
                            <td>{{Lang::get('public.Stake')}} </td>
                            <td>{{Lang::get('public.WinLose')}}</td>
                            <td>{{Lang::get('public.RealBet')}}</td>
                        </tr>
                        <tr>
                            <td colspan="6"><h2><center>{{ Lang::get('public.NoDataAvailable') }}</center></h2></td>
                        </tr>
                    </table>	
                    <br><br>
                </div>
            </div>
            <!--ACCOUNT CONTENT-->
            <div class="clr"></div>
        </div>
    </div>
</div>
@endsection
