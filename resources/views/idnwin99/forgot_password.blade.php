@extends('idnwin99/master')

@section('title', 'Forgot Password')

@section('css')
    <style type="text/css">
        .acctTextReg font {
            background-color: yellow;
            padding: 2px;
            margin: 3px;
        }
    </style>
@endsection

@section('js')
@parent

<script type="text/javascript">
        function forgotpassword_submit(){
            $.ajax({
            type: "POST",
            url: "{{route('resetpassword')}}",
            data: {
                _token: "{{ csrf_token() }}",
                username: $('#fg_username').val(),
                email: $('#fg_email').val()
            }
            }).done(function (json) {
                $('.acctTextReg').html('');
                obj = JSON.parse(json);
                var str = '';
                $.each(obj, function (i, item) {
                    if ('{{Lang::get('COMMON.SUCESSFUL')}}' == item) {
                        window.location.href = "{{route('homepage')}}";
                    } else {
                        $('.' + i + '_acctTextReg').html('<font style="color:red">' + item + '</font>');
                    }
                });
            });
        }
</script>
@endsection

@section('content')
<div class="midSect">
    
    @include('idnwin99.include.notice')
    
    <div class="midSectInner">
        <div class="midSectCont">
            <br>
            <div class="midContf wh">
                <h2><i class="fa fa-lock"></i> {{ strtoupper(Lang::get('public.ForgotPassword')) }}</h2> 
                <p>
                    {{ Lang::get('public.ForgotPasswordContent') }}
                    <br><br>
                </p>
                <div class="acctContentReg">
                    <div class="acctRow">
                        <label>{{ Lang::get('COMMON.USERNAME') }}* :</label><input type="text" id="fg_username" ><span class="username_acctTextReg acctTextReg"></span>
                        <div class="clr"></div>
                    </div>
                    <div class="acctRow">
                        <label>{{ Lang::get('public.EmailAddress') }}* :</label><input type="text" id="fg_email"><span class="email_acctTextReg acctTextReg"></span>
                        <div class="clr"></div>
                    </div>
                    <div class="acctRow">
                        <p>* {{ Lang::get('public.RequiredField') }}</p>
                        <div class="clr"></div>
                    </div>
                    <div class="acctRow">
                        <div class="submitAcct">
                            <a href="javascript:void(0)" onclick="forgotpassword_submit()">{{Lang::get('public.Submit')}}</a>
                        </div>
                    </div>
                    <div class="clr"></div>
                </div>
            </div>
            <div class="clr"></div>
        </div>
    </div>
</div>
@endsection