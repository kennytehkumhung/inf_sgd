@extends('idnwin99/master')

@section('title', 'Sportsbook')

@section('content')
<div class="midSect">

    @include('idnwin99.include.notice')

    <div class="midSectInner">
        <div class="midSectCont">
            <div class="sptit">
                W-Sport
            </div>
            <iframe name="sportFrame" id="" width="1024px" height="855px" frameborder="0" src="{{$login_url}}"></iframe>
        </div>
    </div>
</div>
@endsection
