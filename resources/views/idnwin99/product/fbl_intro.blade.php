@extends('idnwin99/master')

@section('title', 'Fireball')

@section('js')
@parent

<script type="text/javascript">
    function MM_swapImgRestore() { //v3.0
        var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
    }
    
    function MM_preloadImages() { //v3.0
        var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
        var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
        if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
    }

    function MM_findObj(n, d) { //v4.01
        var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
            d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
        if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
        for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
        if(!x && d.getElementById) x=d.getElementById(n); return x;
    }

    function MM_swapImage() { //v3.0
        var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
        if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
    }
</script>
@endsection

@section('content')
<div class="midSect">
    
    @include('idnwin99.include.notice')
    
    <div class="midSectInner">
        <div class="midSectCont">
            <div class="fireBcont">
                <div class="fireBtxt">
                    {{ Lang::get('public.Fireball') }}
                </div>
                <div class="lc6">
                    <div class="lcBox3">
                        <div id="crossfade">
                            <a href="javascript:void(0);" onclick="{{ Auth::user()->check() ? 'window.open("'.route('fbl').'","fbl","width=1046,height=830");' : 'jAlert("'.Lang::get('COMMON.PLEASELOGIN').'");' }}">
                                <img class="bottom" src="{{ asset('/idnwin99/img/playNow-hover.png') }}">
                                <img class="top" src="{{ asset('/idnwin99/img/playNow.png') }}">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection