@extends('idnwin99/master')

@section('title', 'Sportsbook')

@section('content')
<div class="midSect">

    @include('idnwin99.include.notice')

    <div class="midSectInner">
        <div class="midSectCont">
            <div class="sptit">
                I-Sport
            </div>
            @if (!Auth::user()->check())
                <iframe id="ContentPlaceHolder1_iframe_game" width="1025px" height="855px" frameborder="0" src="http://mkt.{{$website}}/vender.aspx?lang={{$lang}}"></iframe>
            @else
                <iframe id="ContentPlaceHolder1_iframe_game" width="1025px" height="855px" frameborder="0" src="http://mkt.{{$website}}/Deposit_ProcessLogin.aspx?lang={{$lang}}"></iframe>
            @endif
        </div>
    </div>
</div>
@endsection
