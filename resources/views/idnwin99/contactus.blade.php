@extends('idnwin99/master')

@section('title', 'Contact Us')

@section('content')
<div class="midSect">
    
    @include('idnwin99.include.notice')
    
    <div class="midSectInner">
        <div class="midSectCont">
            <div class="ctCont1">
                    
                {!! htmlspecialchars_decode($content) !!}
                
            </div>
            <div class="clr"></div>
        </div>
    </div>
</div>
@endsection