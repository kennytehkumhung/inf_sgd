@extends('idnwin99/master')

@section('title', 'About Us')

@section('content')
<div class="midSect">
    
    @include('idnwin99.include.notice')

    <div class="midSectInner">
        <div class="midSectCont">
            <div class="ac2 ac2bg">
                {!! htmlspecialchars_decode($content) !!}
            </div>
            
            <div class="clr"></div>
        </div>
    </div>
</div>
@endsection
