@extends('idnwin99/master')

@section('title', 'Profile')

@section('css')
@parent

<style type="text/css">
    .select_fix { margin-right: 5px !important; }
</style>
@endsection

@section('js')
@parent

<script>
	$(function() { 
		getBalance(true);
		
		$('#gender').val('{{ $acdObj->gender }}');
	});
    
    function update_profile() {
        var dob = '{{ $acdObj->dob }}';
        if ($('#dob_year').val() != undefined) {
            dob = $('#dob_year').val() + '-' + $('#dob_month').val() + '-' + $('#dob_day').val();
        }
        $.ajax({
            type: "POST",
            url: "{{action('User\MemberController@UpdateDetail')}}",
            data: {
                _token: "{{ csrf_token() }}",
                //dob:		$('#dob').val(),
                gender: $('#gender').val(),
                dob: dob
            }
        }).done(function (json) {
            obj = JSON.parse(json);
            var str = '';
            $.each(obj, function (i, item) {
                str += item + '\n';
            });
            alert(str);
        });
    }
</script>
@endsection

@section('content')

<div class="midSect">
    
    @include('idnwin99.include.notice')
    
    <div class="midSectInner">
        <div class="midSectCont">
            
            @include('idnwin99/include/balance_top')

            @include('idnwin99/include/submenu_top', array('type' => 'profile'))

            <!--ACCOUNT MANAGAMENT MENU-->
            <!--ACCOUNT TITLE-->
            <div class="title_bar">
                <span>{{ Lang::get('public.Profile') }}</span>
            </div>
            <!--ACCOUNT TITLE-->
            <!--ACCOUNT CONTENT-->
            <div class="acctContent">
                <span class="wallet_title"><i class="fa fa-pencil"></i>{{ Lang::get('public.MyAccountDetail') }}</span>
                <div class="acctRow">
                    <label>{{ Lang::get('public.Username') }} :</label><span class="acctText"> {{ $acdObj->fullname }}</span>
                    <div class="clr"></div>
                </div>
                <div class="acctRow">
                    <label>{{ Lang::get('public.FullName') }} :</label><span class="acctText"> {{ $acdObj->fullname }}</span>
                    <div class="clr"></div>
                </div>
                <div class="acctRow">
                    <label>{{ Lang::get('public.EmailAddress') }} :</label><span class="acctText"> {{ $acdObj->email }}</span>
                    <div class="clr"></div>
                </div>
                <div class="acctRow">
                    <label>{{ Lang::get('public.Currency') }} :</label><span class="acctText"> IDR</span>
                    <div class="clr"></div>
                </div>
                <div class="acctRow">
                    <label>{{ Lang::get('public.ContactNo') }} :</label><span class="acctText"> {{ $acdObj->telmobile }}</span>
                    <div class="clr"></div>
                </div>
                <div class="acctRow">
                    <label>{{ Lang::get('public.Gender') }} :</label>
                    <select id="gender" name="gender" class="select_fix">
                        <option value="1">{{ Lang::get('public.Male') }}</option>
                        <option value="2">{{ Lang::get('public.Female') }}</option>
                    </select>
                    <div class="clr"></div>
                </div>
                <div class="acctRow">
                    <label>{{Lang::get('public.DOB')}} :</label>
                    @if($acdObj->dob == '0000-00-00')
                        <select id="dob_day" class="select_fix">
                            @foreach (\App\Models\AccountDetail::getDobDayOptions() as $key => $val)
                            <option value="{{ $key }}">{{ $val }}</option>
                            @endforeach
                        </select>
                        <select id="dob_month" class="select_fix">
                            @foreach (\App\Models\AccountDetail::getDobMonthOptions() as $key => $val)
                            <option value="{{ $key }}">{{ $val }}</option>
                            @endforeach
                        </select>
                        <select id="dob_year" class="select_fix">
                            @foreach (\App\Models\AccountDetail::getDobYearOptions() as $key => $val)
                            <option value="{{ $key }}">{{ $val }}</option>
                            @endforeach
                        </select>
                    @else
                        {{ $acdObj->dob }}
                    @endif
                    <div class="clr"></div>
                </div>
                <div class="submitAcct">
                    <a href="javascript:void(0);" onclick="update_profile()">{{ Lang::get('public.Submit') }}</a>
                </div>
            </div>
            <!--ACCOUNT CONTENT-->
            <div class="clr"></div>
        </div>
    </div>
</div>
@endsection