@extends('idnwin99/master')

@section('title', 'Maintenance')

@section('content')
<div class="midSect">
    
    @include('idnwin99.include.notice')
    
    <div class="midSectInner">
        <div class="midSectCont">
            <div class="msgBoxM">
                <div class="msgBM">
                    Website currently under maintenance<br>
                    COME BACK SOON
                </div>
            </div>
        </div>
    </div>
</div>
@endsection