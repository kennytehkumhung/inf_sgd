@extends('idnwin99/master')

@section('title', 'Registration')

@section('css')
    <style type="text/css">
        .acctTextReg font {
            background-color: yellow;
            padding: 2px;
            margin: 3px;
        }
    </style>
@endsection

@section('js')
@parent
<script type="text/javascript">
	function enterpressalert(e, textarea)
	{
		var code = (e.keyCode ? e.keyCode : e.which);
        if(code == 13) { //Enter keycode
            register_submit();
        }
    }
    
    function register_submit() {
        var dob = '0000-00-00';
        if (typeof $('#dob_year').val() != undefined) {
            dob = $('#dob_year').val() + '-' + $('#dob_month').val() + '-' + $('#dob_day').val();
        }
        $.ajax({
            type: "POST",
            url: "{{route('register_process')}}",
            data: {
                _token:     "{{ csrf_token() }}",
                username:	$('#r_username').val(),
                password:	$('#r_password').val(),
                repeatpassword: $('#r_repeatpassword').val(),
                code:		$('#r_code').val(),
                email:		$('#r_email').val(),
                mobile:		$('#r_mobile').val(),
                fullname:	$('#r_fullname').val(),
                dob:		dob
            },
        }).done(function(json) {
            $('.acctTextReg').html('');
            obj = JSON.parse(json);
            var str = '';
            $.each(obj, function(i, item) {
                if ('{{Lang::get('COMMON.SUCESSFUL')}}' == item) {
                    alert('{{Lang::get('public.NewMember')}}');
                    window.location.href = "{{route('homepage')}}";
                } else {
                    $('.' + i + '_acctTextReg').html('<font style="color:red">' + item + '</font>');
                }
            });
        });
    }
</script>
@endsection

@section('content')
<div class="midSect">
    
    @include('idnwin99.include.notice')
    
    <div class="midSectInner">
        <div class="midSectCont">
            <div class="otherPg">
                <br>
                <div class="yellow">
                    <p>
                        {{ Lang::get('public.RegisterNote1') }}
                    </p>
                </div>
                <br>
                <div class="acctContentReg">
                    <div class="acctRow">
                        <label>{{ Lang::get('public.EmailAddress') }}* :</label><input type="text" id="r_email" name="r_email"><span class="email_acctTextReg acctTextReg"></span>
                        <div class="clr"></div>
                    </div>
                    <div class="acctRow">
                        <label>{{ Lang::get('public.ContactNumber') }}* :</label><input type="text" id="r_mobile" name="r_mobile"><span class="mobile_acctTextReg acctTextReg"></span>
                        <div class="clr"></div>
                    </div>
                    <div class="acctRow">
                        <label>{{ Lang::get('public.Currency') }} :</label><label>IDR</label>
                        <div class="clr"></div>
                    </div>
                    <div class="acctRow">
                        <label>{{ Lang::get('public.FullName') }}* :</label><input type="text" id="r_fullname" name="r_fullname"><span class="fullname_acctTextReg acctTextReg"></span>
                        <div class="clr"></div>
                    </div>
                    <div class="yellow2">
                        <p>{{ Lang::get('public.NameMatchWithrawalBank') }} </p>
                    </div>
                    <br>
                    <div class="acctRow">
                        <label>{{ Lang::get('public.Gender') }}* :</label>
                        <select id="gender">
                            <option value="1">{{ Lang::get('public.Male') }}</option>
                            <option value="2">{{ Lang::get('public.Female') }}</option>
                        </select>
                        <div class="clr"></div>
                    </div>
                    <div class="acctRow">
                        <label>{{ Lang::get('public.DOB') }}* :</label>
                        <select id="dob_day">
                            @foreach (\App\Models\Accountdetail::getDobDayOptions() as $key => $val)
                            <option value="{{ $key }}">{{ $val }}</option>
                            @endforeach
                        </select>
                        <select id="dob_month" style="margin-left: 5px;">
                            @foreach (\App\Models\Accountdetail::getDobMonthOptions() as $key => $val)
                            <option value="{{ $key }}">{{ $val }}</option>
                            @endforeach
                        </select>
                        <select id="dob_year" style="margin-left: 5px;">
                            @foreach (\App\Models\Accountdetail::getDobYearOptions() as $key => $val)
                            <option value="{{ $key }}">{{ $val }}</option>
                            @endforeach
                        </select>
                        <div class="clr"></div>
                    </div>
                    <div class="acctRow">
                        <label>{{ Lang::get('public.Username') }}* :</label><input type="text" id="r_username" name="r_username" class="sp1"><span class="username_acctTextReg acctTextReg"></span>
                        <div class="clr"></div>
                    </div>
                    <div class="yellow2">
                        <p>*{{ Lang::get('public.UsernamePolicyContent') }}</p>
                    </div>
                    <br>
                    <div class="acctRow">
                        <label>{{ Lang::get('public.Password') }}* :</label><input type="password" id="r_password" name="r_password" class="sp1"><span class="password_acctTextReg acctTextReg"></span>
                        <div class="clr"></div>
                    </div>
                    <div class="yellow2">
                        <p>*{{ Lang::get('public.PasswordPolicyContent') }}</p>
                    </div>
                    <br>
                    <div class="acctRow">
                        <label>{{ Lang::get('public.RepeatPassword') }}* :</label><input type="password" id="r_repeatpassword" name="r_repeatpassword">
                        <div class="clr"></div>
                    </div>
                    <br>
                    <div class="acctRow">
                        <label>{{ Lang::get('public.Code') }}* :</label><input type="text" id="r_code" onKeyPress="enterpressalert(event, this)" class="coded" style="height: 21px; width: 109px;"><img style="float: left;"  src="{{route('captcha', ['type' => 'register_captcha'])}}" width="60" height="27" class="coded blacked" /> <span class="code_acctTextReg acctTextReg"></span>
                        <div class="clr"></div>
                    </div>
                    <br>
                    <div class="acctRow">
                        <p>* {{ Lang::get('public.RequiredField') }}</p>
                        <div class="clr"></div>
                    </div>
                    <div class="submitAcct">
                        <a href="javascript:void(0);" onclick="register_submit();">{{Lang::get('public.Submit')}}</a>
                    </div>
                </div>
            </div>
            <div class="clr"></div>
        </div>
    </div>
</div>
@endsection
