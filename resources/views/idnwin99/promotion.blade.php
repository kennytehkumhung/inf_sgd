@extends('idnwin99/master')

@section('title', 'Promotions')

@section('js')
@parent

<script type="text/javascript">
    $(function () {
        var showBoxesMin = 0;
        var showBoxesMax = 6;
        
        for (var i = showBoxesMin; i <= showBoxesMax; i++) {
            $(".showbox" + i).hover(function() {
                $(this).addClass('transition');
            }, function() {
                $(this).removeClass('transition');
            });
        }
        
        $('#accordion').find('.accordion-toggle').click(function(){
            //Expand or collapse this panel
            $(this).next().slideToggle('fast');

            //Hide the other panels
            $(".accordion-content").not($(this).next()).slideUp('fast');
        });
    });
</script>
@endsection

@section('content')
<div class="midSect">
    
    @include('idnwin99.include.notice')
    
    <div class="midSectInner">
        <div class="midSectCont">
            <div class="promo">
                <div id="accordion">
                    @foreach ($promo as $key => $value )
                        <h4 class="accordion-toggle">
                            <img src="{{ $value['image'] }}" style="max-height: 148px; max-width: 1024px;">
                            <div id="promo_bar"> 
                                <div class="promo_info"><a href="{{ route('register') }}">{{ Lang::get('public.JoinNow') }}</a></div>
                                <label class="promo_more" for="ac-1">{{ Lang::get('public.MoreInfo') }}</label>
                                <div class="clr"></div>
                            </div>
                        </h4>
                        <div class="accordion-content default">
                            <h2 style="color: #fff;">{{$value['title']}}</h2>
                            <p>{!! htmlspecialchars_decode($value['content']) !!}</p>
                        </div>
                    @endforeach
                </div>  
            </div>
            <div class="clr"></div>
        </div>
    </div>
</div>
@endsection
