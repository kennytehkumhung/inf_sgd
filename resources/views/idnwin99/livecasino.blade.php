@extends('idnwin99/master')

@section('title', 'Live Casino')

@section('js')
@parent

<script type="text/javascript">
    function startGame(index) {
        $("a[data-slide-index='" + index + "']").click();
    }
</script>
@endsection

@section('content')
<div class="midSect bglc">
    
    @include('idnwin99.include.notice')
    
    <div class="sliderCont">
        <div class="sliderContInner aac2">
            <ul class="bxslider">
                @if(in_array('PLTB',Session::get('valid_product')))
                    <li>
                        <a href="javascript:void(0);" onclick="startGame(0);"><img src="{{ asset('/idnwin99/img/pt-lc-bg.png') }}" /></a>
                    </li>
                @endif
                @if(in_array('AGG',Session::get('valid_product')))
                    <li>
                        <a href="javascript:void(0);" onclick="startGame(2);"><img src="{{ asset('/idnwin99/img/ag-lc-bg.png') }}" /></a>
                    </li>
                @endif
                @if(in_array('W88',Session::get('valid_product')))
                    <li>
                        <a href="javascript:void(0);" onclick="startGame(1);"><img src="{{ asset('/idnwin99/img/gp-lc-bg.png') }}" /></a>
                    </li>
                @endif
            </ul>
            <div id="bx-pager">
                @if(in_array('PLTB',Session::get('valid_product')))
                    <a data-slide-index="0" href="javascript:void(0);" onclick="{{ Auth::user()->check() ? 'window.open("'.route('pltbiframe').'","casino_pt","width=1150,height=830");' : 'jAlert("'.Lang::get('COMMON.PLEASELOGIN').'");' }}"><img src="{{ asset('/idnwin99/img/pt-thumb.png') }}" /></a>
                @endif
                @if(in_array('AGG',Session::get('valid_product')))
                    <a data-slide-index="2" href="javascript:void(0);" onclick="{{ Auth::user()->check() ? 'window.open("'.route('agg').'","casino_ag","width=1150,height=830");' : 'jAlert("'.Lang::get('COMMON.PLEASELOGIN').'");' }}"><img src="{{ asset('/idnwin99/img/ag-thumb.png') }}" /></a>
                @endif
                @if(in_array('W88',Session::get('valid_product')))
                    <a data-slide-index="1" href="javascript:void(0);" onclick="{{ Auth::user()->check() ? 'window.open("'.route('w88' , [ 'type' => 'casino' , 'category' => 'live']).'","casino_gp","width=1150,height=830");' : 'jAlert("'.Lang::get('COMMON.PLEASELOGIN').'");' }}"><img src="{{ asset('/idnwin99/img/gp-thumb.png') }}" /></a>
                @endif
            </div>
            <div class="clr"></div>
        </div>
    </div>
    <div class="midSectInner">
    </div>
</div>
@endsection