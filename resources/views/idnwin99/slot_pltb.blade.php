<!doctype html>
<html>
    <head>
        <link href="{{ asset('/idnwin99/resources/css/style.css') }}" rel="stylesheet" type="text/css" />
        
        <style type="text/css">
            .slot_box {
                background-color: #000;
                width: 150px; 
                height: 202px;
            }
            
            .slot_box img {
                width: 150px; 
                height: 150px;
            }
        </style>
    </head>
    <body style="background-color: transparent; overflow: hidden;">
        <div id="slot">
            <div class="slot_menu">
                <ul>
                    <li><a href="{{route('pltb', [ 'type' => 'pgames' , 'category' => '1' ] )}}">Progressive Games (56)</a></li>
                    <!--<li><a href="{{route('pltb', [ 'type' => 'newgames' , 'category' => '1' ] )}}">New Games</a></li>-->
                    <li><a href="{{route('pltb', [ 'type' => 'brand' , 'category' => '1' ] )}}">Branded Games (61)</a></li>
                    <li><a href="{{route('pltb', [ 'type' => 'slot' , 'category' => 'slot' ] )}}">Slot (190)</a></li>
                    <li><a href="{{route('pltb', [ 'type' => 'slot' , 'category' => 'videopoker' ] )}}">Video Poker (30)</a></li>
                    <li><a href="{{route('pltb', [ 'type' => 'slot' , 'category' => 'arcade' ] )}}">Arcade (33)</a></li>
                    <li><a href="{{route('pltb', [ 'type' => 'slot' , 'category' => 'tablecards' ] )}}">Tablecards (44)</a></li>
                    <li><a href="{{route('pltb', [ 'type' => 'slot' , 'category' => 'scratchcards' ] )}}">Scratchcards(42)</a></li>
                </ul>
            </div>

            <div id="slot_lobby">
                <div class="slot_box">
                    <a onclick=" @if (Auth::user()->check())window.open('{{route('pltbslotiframe' , [ 'gamecode' => 'sfh' ] )}}', 'pltb_slot', 'width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif  " href="javascript:void(0)">
                        <img width="150" height="150" alt="" src="{{ asset('/royalewin/img/plt/sfh.jpg') }}">
                    </a>
                    <span>Safari Heat</span>
                </div>	
                <div class="slot_box">
                    <a onclick=" @if (Auth::user()->check())window.open('{{route('pltbslotiframe' , [ 'gamecode' => 'dnr' ] )}}', 'pltb_slot', 'width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif  " href="javascript:void(0)">
                        <img width="150" height="150" alt="" src="{{ asset('/royalewin/img/plt/dnr.jpg') }}">
                    </a>
                    <span>Dolphin Reef</span>
                </div>
                <div class="slot_box">
                    <a onclick=" @if (Auth::user()->check())window.open('{{route('pltbslotiframe' , [ 'gamecode' => 'gtsdnc' ] )}}', 'pltb_slot', 'width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif  " href="javascript:void(0)">
                        <img width="150" height="150" alt="" src="{{ asset('/royalewin/img/plt/gtsdnc.jpg') }}">
                    </a>
                    <span>dolphin cash scratch</span>
                </div>
                
                @foreach( $lists as $list )
                    <div class="slot_box">
                        <a href="javascript:void(0)" onclick="@if (Auth::user()->check())window.open('{{route('pltbslotiframe' , [ 'gamecode' => $list['code'] ] )}}', 'pltb_slot', 'width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif ">
                            <img src="{{url()}}/royalewin/img/plt/{{$list->code}}.jpg" alt=""/>
                        </a>
                        <span>{{$list->gameName}}</span>
                    </div>				 
                @endforeach

                <div class="clr"></div>
            </div>

            <div class="clr"></div>
        </div>
    </body>
</html>