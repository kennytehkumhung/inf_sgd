@extends('idnwin99/master')

@section('title', 'Slots')

@section('js')
@parent

<script type="text/javascript">
    $(function () {
        initSlotGameList();
        
        $("#iframe_game").on('load', function () {
            resizeIframe(this)
        });
    });
    
    function initSlotGameList() {
        var game = "{{ request()->type }}";
        
        if (game != '') {
            $("#game_" + game).click();
        } else {
            // Auto select first game.
            $(".slotRight .sltselectable:eq(0) a").click();
        }
    }
    
    function resizeIframe(obj) {
        obj.style.height = 0;
        obj.style.height = obj.contentWindow.document.body.scrollHeight + 'px';
    }

    function change_iframe(url, image_link, product) {
        $('#iframe_game').attr('src', url);
        
        if (product == "jok") {
            $("#left_big_thumb").attr("src", "{{ asset('/idnwin99/img/jkr-slot-bg.png') }}");
        } else if (product == "pltb") {
            $("#left_big_thumb").attr("src", "{{ asset('/idnwin99/img/pt-slot-bg.png') }}");
        } else if (product == "agg") {
            $("#left_big_thumb").attr("src", "{{ asset('/idnwin99/img/ag-slot-bg.png') }}");
        }
    }
    
    function MM_swapImgRestore() { //v3.0
        var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
    }
    
    function MM_preloadImages() { //v3.0
        var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
        var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
        if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
    }

    function MM_findObj(n, d) { //v4.01
        var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
            d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
        if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
        for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
        if(!x && d.getElementById) x=d.getElementById(n); return x;
    }

    function MM_swapImage() { //v3.0
        var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
        if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
    }
</script>
@endsection

@section('content')
<div class="midSect">
    
    @include('idnwin99.include.notice')
    
    <div class="midSectInner">
        <div class="midSectCont">
            <div class="slotLeft">
                <img id="left_big_thumb" src="">
            </div>
            <div class="slotRight">
                @if(in_array('JOK',Session::get('valid_product')))
                    <div class="sltin sltselectable">
                        <a id="game_jok" href="javascript:void(0);" onclick="change_iframe('{{route('jok' , [ 'type' => 'slot' , 'category' => 'Slots' ] )}}',null,'jok');" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('joker slot','','{{ asset('/idnwin99/img/joker-slot-thumb-hover.png') }}',0)"><img src="{{ asset('/idnwin99/img/joker-slot-thumb.png') }}" width="150" height="150" border="0"></a>
                        <!--<a id="game_jok" href="javascript:alert('Product under maintenance.');" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('joker slot','','{{ asset('/idnwin99/img/joker-slot-thumb-hover.png') }}',0)"><img src="{{ asset('/idnwin99/img/joker-slot-thumb.png') }}" width="150" height="150" border="0"></a>-->
                    </div>
                @endif
                @if(in_array('AGG',Session::get('valid_product')))
                    <div class="sltin sltselectable">
                        <!--<a href="javascript:void(0);" onclick="{{ Auth::user()->check() ? 'window.open("'.route('agg').'","casino_ag","width=1150,height=830");' : 'jAlert("'.Lang::get('COMMON.PLEASELOGIN').'");' }}" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('ag slot','','{{ asset('/idnwin99/img/ag-slot-thumb-hover.png') }}',0)"><img src="{{ asset('/idnwin99/img/ag-slot-thumb.png') }}" width="150" height="150" border="0"></a>-->
                        <a id="game_agg" href="javascript:void(0);" onclick="change_iframe('{{route('agg-slot' , [ 'type' => 'slot' , 'category' => '1' ] )}}',null,'agg');" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('ag slot','','{{ asset('/idnwin99/img/ag-slot-thumb-hover.png') }}', 0)"><img src="{{ asset('/idnwin99/img/ag-slot-thumb.png') }}" width="150" height="150" border="0"></a>
                    </div>
                @endif
                @if(in_array('PLTB',Session::get('valid_product')))
                    <div class="sltin sltselectable">
                        <a id="game_pltb" href="javascript:void(0);" onclick="change_iframe('{{route('pltb' , [ 'type' => 'pgames' , 'category' => '1' ] )}}',null,'pltb');" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('pt slot','','{{ asset('/idnwin99/img/pt-slot-thumb-hover.png') }}', 0)"><img src="{{ asset('/idnwin99/img/pt-slot-thumb.png') }}" width="150" height="150" border="0"></a>
                    </div>
                @endif
                <div class="clr"></div>
            </div>
            <div class="clr"></div>
            <div class="midCont">
                <!--CASINO LOBBY SECTION-->
                <iframe id="iframe_game" frameborder="0" style="overflow: hidden; overflow-x: hidden; overflow-y: hidden; height: 98%; min-height: 2000px; width: 100%;" src="
                        @if( request()->type == 'jok' )
                            {{route('jok' , [ 'type' => 'slot' , 'category' => 'Slots' ] )}}
                        @elseif( request()->type == 'plt' || request()->type == 'pltb' )
                            {{route('pltb' , [ 'type' => 'pgames' , 'category' => '1' ] )}}
                        @endif
                    "></iframe>
                <!--CASINO LOBBY SECTION-->
            </div>
        </div>
    </div>
</div>
@endsection