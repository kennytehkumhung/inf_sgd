@extends('idnwin99/master')

@section('title', 'Withdrawal')

@section('js')
@parent

<script>
        $(document).ready(function() {
            getBalance(true);
        });
        setInterval(update_mainwallet, 10000);
        function submit_transfer() {
            $.ajax({
                type: "POST",
                url: "{{route('withdraw-process')}}?" + $('#withdraw_form').serialize(),
                data: {
                    _token: "{{ csrf_token() }}",
                },
                beforeSend: function() {
                },
                success: function(json) {
                    obj = JSON.parse(json);
                    var str = '';
                    $.each(obj, function(i, item) {
                        if ('{{Lang::get('COMMON.SUCESSFUL')}}' == item) {
                            alert('{{Lang::get('COMMON.SUCESSFUL')}}');
                            window.location.href = "{{route('transaction')}}";
                        } else {
                            str += item + '<br>';
                        }
                    });
                    $('.failed_message').html(str);
                }
            });
        }
</script>
@endsection

@section('content')
<div class="midSect">
    
    @include('idnwin99.include.notice')
    <form id="withdraw_form">
    <div class="midSectInner">
        <div class="midSectCont">
            
            @include('idnwin99/include/balance_top')

            @include('idnwin99/include/submenu_top', array('type' => 'fund'))
            
            <!--ACCOUNT MANAGAMENT MENU-->
            <!--ACCOUNT TITLE-->
            <div class="title_bar">
                <span>{{ Lang::get('public.Withdrawal') }}</span>
            </div>
            <!--ACCOUNT TITLE-->
            <!--ACCOUNT CONTENT-->
            <div class="acctContent">
                <span class="wallet_title"><i class="fa fa-money"></i>{{ Lang::get('public.Withdrawal') }}</span>
                <div class="acctRow">
                    <label>{{ Lang::get('public.Option') }}* :</label><span class="acctText">{{ Lang::get('public.BankTransfer') }}</span>
                    <div class="clr"></div>
                </div>
                <div class="acctRow">
                    <label>{{ Lang::get('public.MinMaxLimit') }} :</label><span class="acctText">50,000.00 / 100,000,000.00</span>
                    <div class="clr"></div>
                </div>
                <div class="acctRow">
                    <label>{{ Lang::get('public.ProcessingTime') }} :</label><span class="acctText">15-30 Mins</span>
                    <div class="clr"></div>
                </div>
                <div class="remInfo">
                    <i class="fa fa-exclamation"></i>{{ Lang::choice('public.AllMembersAreAllowedToWithdrawXTime', 1, array('p1' => '1')) }}
                </div>
                <hr style="width: 100%; float: left;">
                <div class="clr"></div>
                <br>
                <div class="acctRow">
                    <label>{{ Lang::get('public.Balance') }} (IDR) :</label><span class="acctText">{{ App\Http\Controllers\User\WalletController::mainwallet() }}</span>
                    <div class="clr"></div>
                </div>
                <div class="acctRow">
                    <label>{{ Lang::get('public.Amount') }} (IDR)* :</label><input type="text" id="amount" name="amount" />
                    <div class="clr"></div>
                </div>
                <div class="acctRow">
                    <label>{{ Lang::get('public.BankName') }}* :</label>
                    <select id="bank" name="bank" style="width: 160px;">
                        @foreach( $banklists as $bank => $detail  )
                            <option value="{{$detail['code']}}">{{$bank}}</option>
                        @endforeach
                    </select>
                    <div class="clr"></div>
                </div>
                <div class="acctRow">
                    <label>{{ Lang::get('public.FullName') }} :</label><span class="acctText">{{ Session::get('fullname') }}</span>
                    <div class="clr"></div>
                </div>
                <div class="acctRow">
                    <label>{{ Lang::get('public.BankAccountNo') }} :</label><input type="text" id="accountno" name="accountno" />
                    <div class="clr"></div>
                </div>
                <div class="remInfo">
                    <i class="fa fa-exclamation-circle"></i>{{ Lang::get('public.RequiredFields') }}
                </div>
                <br><br>
                <div class="submitAcct">
                    <a href="javascript:void(0);" onclick="submit_transfer();">{{ Lang::get('public.Submit') }}</a>
                </div>
                <br><br>
                <span class="failed_message acctTextReg" style="display:block;float:left;height:100%;color:red;"></span>

                <div class="clr"></div>

                <img src="{{ asset('/idnwin99/img/bank-operation-id.png') }}" style="max-width: 998px;" />
            </div>
            <!--ACCOUNT CONTENT-->
            <div class="clr"></div>
        </div>
    </div>
	</form>
</div>
@endsection