@extends('idnwin99/master')

@section('title', 'Transaction History')

@section('css')
@parent

<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<link href="{{ asset('/idnwin99/resources/css/table.css') }}" rel="stylesheet">
@endsection

@section('js')
@parent

<script src="{{ asset('/idnwin99/resources/js/jquery-ui.js') }}"></script>
<script>
    $(document).ready(function () {
        getBalance(true);
    });

    $(function () {
        $(".datepicker").datepicker({dateFormat: 'yy-mm-dd', defaultDate: new Date()});
    });

    function transaction_history() {
        $.ajax({
            type: "POST",
            url: "{{action('User\TransactionController@transactionProcess')}}",
            data: {
                _token: "{{ csrf_token() }}",
                date_from: $('#date_from').val(),
                date_to: $('#date_to').val(),
                record_type: $('#record_type').val()

            },
        }).done(function (json) {

            //var str;
            var str = '';
                    str += '	<tr ><td>{{Lang::get('public.ReferenceNo')}}</td><td>{{Lang::get('public.DateOrTime')}}</td><td>{{Lang::get('public.Type')}}</td><td>{{Lang::get('public.Amount')}}(IDR)</td><td>{{Lang::get('public.Status')}}</td><td>{{Lang::get('public.Reason')}}</td></tr>';
                    obj = JSON.parse(json);
            //alert(obj);
            $.each(obj, function (i, item) {
                str += '<tr><td>' + item.id + '</td><td>' + item.created + '</td><td>' + item.type + '</td><td>' + item.amount + '</td><td>' + item.status + '</td><td>' + item.rejreason + '</td></tr>';

            })

            //alert(json);
            $('#trans_history').html(str);

        });
    }

    setInterval(updateTrans, 10000);
    setInterval(update_mainwallet, 10000);

    function updateTrans() {
        $("#trans_history_button").trigger("click");
    }
</script>
@endsection

@section('content')

<div class="midSect">
    
    @include('idnwin99.include.notice')
    
    <div class="midSectInner">
        <div class="midSectCont">
            
            @include('idnwin99/include/balance_top')

            @include('idnwin99/include/submenu_top', array('type' => 'fund'))
            
            <!--ACCOUNT MANAGAMENT MENU-->
            <!--ACCOUNT TITLE-->
            <div class="title_bar">
                <span>{{ Lang::get('public.TransactionHistory') }}</span>
            </div>
            <!--ACCOUNT TITLE-->
            <!--ACCOUNT CONTENT-->
            <div class="acctContent">
                <span class="wallet_title"><i class="fa fa-undo"></i>{{ Lang::get('public.TransactionHistory') }}</span>
                <div class="acctRow">
                    <label>{{ Lang::get('public.DateFrom') }} :</label><div class="cstInput1"><input type="text" class="datepicker" id="date_from" style="cursor:pointer;" value="{{ date('Y-m-d') }}">-<input type="text" class="datepicker" id="date_to" style="cursor:pointer;" value="{{ date('Y-m-d') }}"></div>
                    <div class="clr"></div>
                </div>
                <div class="acctRow ab1">
                    <label>Record Type :</label>
                    <select id="record_type" style="width:200px;">
                        <option value="0" selected>{{ Lang::get('public.CreditAndDebitRecords') }}</option>
                        <option value="1">{{ Lang::get('public.CreditRecords') }}</option>
                        <option value="2">{{ Lang::get('public.DebitRecords') }}</option>
                    </select>
                    <div class="clr"></div>
                </div>
                <div class="acctRowR">
                    <div class="submitAcct">
                        <a id="trans_history_button" href="javascript:void(0);" onClick="transaction_history();"> {{ Lang::get('public.Submit') }}</a>
                    </div>
                    <div class="clr"></div>
                </div>
                <div class="table" style="display:block;">
                    <table id="trans_history">
                        <tr>
                            <td>{{Lang::get('public.ReferenceNo')}}</td>
                            <td>{{Lang::get('public.DateOrTime')}}</td>
                            <td>{{Lang::get('public.Type')}}</td>
                            <td>{{Lang::get('public.Amount')}}(IDR) </td>
                            <td>{{Lang::get('public.Status')}}</td>
                            <td>{{Lang::get('public.Reason')}}</td>
                        </tr>
                        <tr>
                            <td colspan="6"><h2><center>{{ Lang::get('public.NoDataAvailable') }}</center></h2></td>
                        </tr>
                    </table>
                </div>
            </div>
            <!--ACCOUNT CONTENT-->
            <div class="clr"></div>
        </div>
    </div>
</div>
@endsection