<!doctype html>
<html>
    <head>
        <link href="{{ asset('/idnwin99/resources/css/style.css') }}" rel="stylesheet" type="text/css" />
        
        <style type="text/css">
            .slot_box {
                background-color: #000;
                width: 150px; 
                height: 202px;
            }
            
            .slot_box img {
                width: 150px; 
                height: 150px;
            }
        </style>
    </head>
    <body style="background-color: transparent; overflow: hidden;">
        <div id="slot">
            <div class="slot_menu">
                <ul>
                    <li><a href="{{route('agg-slot', [ 'type' => 'slot' , 'category' => '1' ] )}}">Slot (82)</a></li>
                </ul>
            </div>

            <div id="slot_lobby">
                @foreach( $lists as $list )
                    <div class="slot_box">
                       <a href="javascript:void(0)" onclick="@if (Auth::user()->check())window.open('{{route('agg', array('gametype' => $list['game_code']))}}','ag_slot','width=1000,height=750')@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif">
                           <img src="{{ $list['img_url'] }}" width="150" height="150" alt=""/>
                       </a>
                       <span>{{ $list['game_name'] }}</span>
                    </div>
                @endforeach

                <div class="clr"></div>
            </div>

            <div class="clr"></div>
        </div>
    </body>
</html>
