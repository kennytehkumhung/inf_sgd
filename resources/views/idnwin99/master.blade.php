<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />

        <link href="{{ asset('/idnwin99/img/favicon.ico') }}" rel="shortcut icon" type="image/icon" />
        <link href="{{ asset('/idnwin99/img/favicon.ico') }}" rel="icon" type="image/icon" />
        <title>Online Betting From IDNWIN99 – Sportsbook, Live Casino, Slot Games and High 4D Payout @yield('title')</title>

        <!--CSS-->
        <link href="{{ asset('/idnwin99/resources/css/style.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('/idnwin99/resources/css/skitter.styles.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('/idnwin99/resources/css/timetabs.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('/idnwin99/resources/css/jquery.bxslider.css') }}" type="text/css" media="all" rel="stylesheet" />
        <link href="{{ asset('/idnwin99/resources/css/jquery.dropdown.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('/idnwin99/resources/css/font-awesome.css') }}" rel="stylesheet" type="text/css" />
        @if (Auth::user()->check())
            <link href="{{ asset('/idnwin99/resources/css/acct_management.css') }}" rel="stylesheet" type="text/css" />
        @endif
        
        @yield('css')
        
        <!--SCRIPT-->
        <script src="{{ asset('/idnwin99/resources/js/jquery-2.1.3.min.js') }}"></script>
        <script src="{{ asset('/idnwin99/resources/js/jquery.easing.1.3.js') }}"></script>
        <script src="{{ asset('/idnwin99/resources/js/jquery.skitter.min.js') }}"></script>
        <script src="{{ asset('/idnwin99/resources/js/jquery.timetabs.min.js') }}"></script>
        <script src="{{ asset('/idnwin99/resources/js/jquery.tabSlideOut.v1.3.js') }}"></script>
        <script src="{{ asset('/idnwin99/resources/js/jquery.bxslider.min.js') }}"></script>
        <script src="{{ asset('/idnwin99/resources/js/jquery.dropdown.min.js') }}"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $('.box_skitter_large').skitter({
                    theme: 'clean',
                    numbers_align: 'center',
                    progressbar: false,
                    dots: true,
                    navigation: false,
                    preview: false,
                    interval: 5000
                });
            });
        </script>
        <script type="text/javascript">
            function enterpressalert(e, textarea)
            {
                var code = (e.keyCode ? e.keyCode : e.which);
                if(code == 13) { //Enter keycode
                  login();
                }
            }
            function login()
            {
                $.ajax({
                    type: "POST",
                    url: "{{route('login')}}",
                    data: {
                    _token: "{{ csrf_token() }}",
                        username: $('#username').val(),
                        password: $('#password').val(),
                        code:     $('#code').val(),
                    },
                }).done(function(json) {
                    obj = JSON.parse(json);
                    var str = '';
                    $.each(obj, function(i, item) {
                        str += item + '\n';
                    });
                    if ("{{Lang::get('COMMON.SUCESSFUL')}}\n" == str) {
                        location.reload();
                    } else {
                        alert(str);
                    }
                });
            }

            function login2() {
                $.ajax({
                    type: "POST",
                    url: "{{route('login')}}",
                    data: {
                        _token: "{{ csrf_token() }}",
                        username: $('#username2').val(),
                        password: $('#password2').val(),
                        code:     $('#code2').val(),
                    },
                }).done(function(json) {
                    obj = JSON.parse(json);
                    var str = '';
                    $.each(obj, function(i, item) {
                        str += item + '\n';
                    });
                    if ("{{Lang::get('COMMON.SUCESSFUL')}}\n" == str){
                        location.reload();
                    } else {
                        alert(str);
                    }
                });
            }

            @if (Auth::user()->check())
                function update_mainwallet(){
                    $.ajax({
                        type: "POST",
                        url: "{{route( 'mainwallet', [ '_token'=> csrf_token() ])}}",
                        beforeSend: function(balance){
                        },
                        success: function(balance) {
                            var walletObj = $('.main_wallet');

                            if (walletObj.is("input[type='text']")) {
                                $('.main_wallet').val(parseFloat(Math.round(balance * 100) / 100).toFixed(2));
                            } else {
                                $('.main_wallet').html(parseFloat(Math.round(balance * 100) / 100).toFixed(2));
                            }
    						// total_balance += parseFloat(balance);
                        }
                    });
                }
                function getBalance(type) {
                    var total_balance = 0;
                    var currency = "{{ Session::get('currency') }}";

                    $.when(
                        $.ajax({
                        type: "POST",
                            url: "{{route('mainwallet', [ '_token'=> csrf_token()])}}",
                            beforeSend: function(balance){
                            },
                            success: function(balance){
                                var walletObj = $('.main_wallet');

                                if (walletObj.is("input[type='text']")) {
                                    $('.main_wallet').val(parseFloat(Math.round(balance * 100) / 100).toFixed(2));
                                } else {
                                    $('.main_wallet').html(parseFloat(Math.round(balance * 100) / 100).toFixed(2));
                                }
                                total_balance += parseFloat(balance);
                            }
                        }),
                        @foreach(Session::get('products_obj') as $prdid => $object)
                            $.ajax({
                                type: "POST",
                                url: "{{route( 'getbalance', [ '_token'=> csrf_token(), 'product'=> $object->code ])}}&rd=<?php echo rand(10000, 99999); ?>",
                                beforeSend: function(balance){
                                    var balObj = $('.{{$object->code}}_balance');

                                    if (balObj.is("input[type='text']")) {
                                        var balLoadingObj = $(".{{$object->code}}_balance_loading");

                                        if (balLoadingObj.length) {
                                            balLoadingObj.show();
                                        }
                                    } else {
                                        balObj.html('<img style="" src="{{url()}}/front/img/ajax-loading.gif" width="20" height="10">');
                                    }
                                },
                                success: function(balance){
                                    var balObj = $('.{{$object->code}}_balance');

                                    if (balance != '{{Lang::get('Maintenance')}}') {
                                        if (balObj.is("input[type='text']")) {
                                            balObj.val(parseFloat(Math.round(balance * 100) / 100).toFixed(2));
                                        } else {
                                            balObj.html(parseFloat(Math.round(balance * 100) / 100).toFixed(2));
                                        }

                                        var skipTotal = false;
                                        if (currency == "IDR") {
                                            if ("{{$object->code}}" == "PSB" || "{{$object->code}}" == "OSG") {
                                                skipTotal = true;
                                            }
                                        } else {
                                            if ("{{$object->code}}" == "FBL") {
                                                skipTotal = true;
                                            }
                                        }

                                        if (!skipTotal) {
                                            total_balance += parseFloat(balance);
                                        }
                                    } else {
                                        if (balObj.is("input[type='text']")) {
                                            balObj.val(balance);
                                        } else {
                                            balObj.html(balance);
                                        }
                                    }

                                    var balLoadingObj = $(".{{$object->code}}_balance_loading");

                                    if (balLoadingObj.length) {
                                        balLoadingObj.hide();
                                    }
                                }
                            })
                            @if ($prdid != Session::get('last_prdid'))
                            ,
                            @endif
                        @endforeach
                    ).then(function() {
                        var objs = $('#total_balance, .top_total_balance');

                        objs.each(function () {
                            var obj = $(this);
                            if (obj.is("input[type='text']")) {
                                obj.val(parseFloat(Math.round(total_balance * 100) / 100).toFixed(2));
                            } else {
                                obj.html(parseFloat(Math.round(total_balance * 100) / 100).toFixed(2));
                            }
                        });

                        $(".total_balance_loading").hide();
                    });
                }
            @endif
        </script>
    </head>
    <body>
        <!--Header-->
        <div class="header">
            <div class="headerInner">
                <div class="logo">
                    <a href="{{ route('homepage') }}">
                        <img src="{{ asset('/idnwin99/img/logo.png') }}" height="85">
                    </a>
                </div>
                <div class="headerRight">
                    <div class="headerRightTable">
                        <table width="auto" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <?php date_default_timezone_set("Asia/Jakarta") ?>
                                <td valign="middle">{{ Lang::get('public.DateTime') }}: {{ date('d/m/Y, H:i') }}</td>
                                <td valign="middle">|</td>
                                <td valign="middle"><a href="{{ route( 'language', [ 'lang'=> 'en']) }}"><img src="{{ asset('/idnwin99/img/eng-icon.png') }}" alt=""/></a></td>
                                <td valign="middle">|</td>
                                <td valign="middle"><a href="{{ route( 'language', [ 'lang'=> 'cn']) }}"><img src="{{ asset('/idnwin99/img/cn-icon.png') }}" alt="" width="20px"/></a></td>
                                <td valign="middle">|</td>
                                <td valign="middle"><a href="{{ route( 'language', [ 'lang'=> 'id']) }}"><img src="{{ asset('/idnwin99/img/id-icon.png') }}" alt="" width="20px"/></a></td>
                            </tr>
                        </table>
                    </div>
                    @if (Auth::user()->check())
                        <div class="headerRightTable1">
                            <table width="auto%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td valign="middle"><div class="welc"><em class="fa fa-user"></em> {{ Lang::get('public.Welcome') }} {{ Session::get('username') }}!<em style="margin-left:15px;" class="fa fa-ellipsis-v fa-1x"></em></div></td>
                                    <td valign="middle"><div class="welc1"><a href="#" data-dropdown="#dropdown-0"><i class="fa fa-caret-square-o-right"></i>{{ Lang::get('public.Profile') }}</a></div>
                                        <div id="dropdown-0" class="dropdown dropdown-tip " >
                                            <ul class="dropdown-menu" style="margin: 0px;">
                                                <li><a href="{{ route('update-profile') }}">{{ Lang::get('public.MyAccount') }}</a></li>
                                                <li><a href="{{ route('update-password') }}">{{ Lang::get('public.ChangePassword') }}</a></li>
                                                <li><a href="{{ route('profitloss') }}">{{ Lang::get('public.Statement') }}</a></li>
                                            </ul>
                                        </div>
                                    </td>
                                    <td valign="middle"><div class="welc1"><a href="#" data-dropdown="#dropdown-1" onClick="getBalance()"><i class="fa fa-money"></i>{{ Lang::get('public.Balance') }}</a></div>
                                        <div id="dropdown-1" class="dropdown dropdown-tip" >
                                            <div class="dropdown-menu wallcc">
                                                <div class="wallCont">
                                                    <div class="wallIt head">{{ Lang::get('public.Wallet') }}</div>
                                                    <div class="wallIt head sep">{{ Lang::get('public.Balance') }}</div>
                                                    <div class="wallIt">{{Lang::get('public.MainWallet')}}</div>
                                                    <div class="wallIt main_wallet">0.00</div>
                                                    @foreach( Session::get('products_obj') as $prdid => $object)
                                                        <div class="wallIt">{{$object->name}}</div>
                                                        <div class="wallIt {{$object->code}}_balance" >0.00</div>
                                                    @endforeach
                                                    <div class="wallIt total">{{ Lang::get('public.Total') }} <span id="total_balance" style="margin-right: 0px;">0.00</span></div>
                                                </div>
                                                <div class="clr"></div>
                                            </div>
                                        </div>
                                    </td>
                                    <td valign="middle">
                                        <div class="welc1"> <a href="#" data-dropdown="#dropdown-2"><i class="fa fa-calculator"></i>{{ Lang::get('public.Fund') }}</a></div>
                                        <div id="dropdown-2" class="dropdown dropdown-tip">
                                            <ul class="dropdown-menu" style="margin: 0px;">
                                                <li><a href="{{ route('deposit') }}">{{ Lang::get('public.Deposit') }}</a></li>
                                                <li><a href="{{ route('withdraw') }}">{{ Lang::get('public.Withdrawal') }}</a></li>
                                                <li><a href="{{ route('transaction') }}">{{ Lang::get('public.TransactionHistory') }}</a></li>
                                                <li><a href="{{ route('transfer') }}">{{ Lang::get('public.Transfer') }}</a></li>
                                            </ul>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="loginBtn"><a href="{{ route('logout') }}">{{Lang::get('public.Logout')}}</a></div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    @else
                        <div class="headerRightTable1">
                            <table width="auto%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td><input type="text" id="username" name="username" placeholder="{{ Lang::get('public.Username') }}" onKeyPress="enterpressalert(event, this)"></td>
                                    <td><input type="password" id="password" name="password" placeholder="{{ Lang::get('public.Password') }}" onKeyPress="enterpressalert(event, this)"></td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td><div class="adj01"><input type="text" id="code" name="code" placeholder="{{ Lang::get('public.Code') }}" onKeyPress="enterpressalert(event, this)"></div></td>
                                                <td><div style="color: #fff;text-align: center;width: 58px;"><img src="{{ route('captcha', ['type' => 'login_captcha']) }}" width="40" height="24"></div></td>
                                                <td><div class="loginBtn"><a id="go" href="#" onclick="login();">{{ Lang::get('public.Login') }}</a></div></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <div class="fp"><a href="{{ route('forgotpassword') }}">{{ Lang::get('public.ForgotPassword') }}</a></div>
                        </div>
                        <div class="joinBtn"><a href="{{ route('register') }}"><img src="{{ asset('/idnwin99/img/joinBtn.jpg') }}" height="67"></a></div>
                    @endif
                    <div class="clr"></div>
                </div>
            </div>
        </div>
        
        <!--Navigation-->
        <div class="navCont">
            
            @include('idnwin99.include.megamenu')
            
        </div>
        
        <!--Mid Sect-->
        @yield('content')
        
        <!--Foooter-->
        <div class="footer">
            <div class="footerInner">
                <div class="footerLeft">
                    <ul>
                        <li class="nM"><a href="{{ route('aboutus') }}">{{ Lang::get('COMMON.ABOUT') }} IDNWIN99</a></li>
                        <li><a href="{{ route('banking') }}">{{ Lang::get('public.Banking') }}</a></li>
{{--                        <li><a href="{{ route('tnc') }}">{{ Lang::get('public.ResponsibleGaming') }}</a></li>--}}
{{--                        <li><a href="{{ route('faq') }}">{{ Lang::get('public.FAQ') }}</a></li>--}}
                        <li class="nM1"><a href="{{ route('tnc') }}">{{ Lang::get('public.TermOfUse') }}</a></li>
                    </ul>
                </div>
                <div class="footerRight">
                    <span>©2016 IDNWIN99. All rights reserved | 18+</span>
                </div>
                <div class="clr"></div>
                <div class="footerLeft1">
                    <span>Partner</span>
                    <ul>
                        <li class="nM"><img src="{{ asset('/idnwin99/img/ag-footer.png') }}" width="61" height="40"></li>
                        <li><img src="{{ asset('/idnwin99/img/mxb-footer.png') }}" width="61" height="40"></li>
                        <li><img src="{{ asset('/idnwin99/img/pt-footer.png') }}" width="84" height="40"></li>
                        <li><img src="{{ asset('/idnwin99/img/gp-footer.png') }}" width="84" height="40"></li>
                    </ul>
                </div>
                <div class="footerRight1">
                    <span>Payment method</span>
                    <ul style="margin-top: 5px;">
                        <li class="nM"><img src="{{ asset('/idnwin99/img/bca-footer.PNG') }}" height="22"></li>
                        <li><img src="{{ asset('/idnwin99/img/bni-footer.PNG') }}" height="22"></li>
                        <li><img src="{{ asset('/idnwin99/img/bri-footer.PNG') }}" height="22"></li>
                        <li><img src="{{ asset('/idnwin99/img/mandiri-footer.PNG') }}" height="22"></li>
                    </ul>
                    <div class="clr"></div>
                </div>
                <div class="clr"></div>
            </div>
        </div>
        <div class="hide">
            <div class="slide-out-div">
                <a class="handle" href="http://link-for-non-js-users">Content</a>
                <img src="{{ asset('/idnwin99/img/ct-head.png') }}">
                <div class="ct-bx">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td><img src="{{ asset('/idnwin99/img/ml-icon.png') }}" ></td>
                            <td>EMAIL: win99club.gmail.com</td>
                        </tr>
                        <tr>
                            <td><img src="{{ asset('/idnwin99/img/tl-icon.png') }}"></td>
                            <td>TEL: 018-8888888</td>
                        </tr>
                        <tr>
                            <td><img src="{{ asset('/idnwin99/img/wc-icon.png') }}"></td>
                            <td>WECHAT ID: win_99club</td>
                        </tr>
                    </table>
                    <div class="ct-qr">
                        <img src="{{ asset('/idnwin99/img/qr-1.jpg') }}">
                    </div>
                </div>
            </div>
            <div class="slide-out-div1">
                <a class="handle1" href="http://link-for-non-js-users">Content</a>
                <img src="{{ asset('/idnwin99/img/ct-head.png') }}">
                <div class="ct-bx">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td><img src="{{ asset('/idnwin99/img/ml-icon.png') }}" ></td>
                            <td>EMAIL: win99club.gmail.com</td>
                        </tr>
                        <tr>
                            <td><img src="{{ asset('/idnwin99/img/tl-icon.png') }}"></td>
                            <td>TEL: 018-8888888</td>
                        </tr>
                        <tr>
                            <td><img src="{{ asset('/idnwin99/img/wc-icon.png') }}"></td>
                            <td>WECHAT ID: win_99club</td>
                        </tr>
                    </table>
                    <div class="ct-qr">
                        <img src="{{ asset('/idnwin99/img/qr-1.jpg') }}">
                    </div>
                </div>
            </div>
        </div>

        <script type="text/javascript">
            $(function(){
                $('.slide-out-div1').tabSlideOut1({
                    tabHandle: '.handle1',                              //class of the element that will be your tab
                    pathToTabImage: '{{ asset('/idnwin99/img/ctDl.jpg') }}',          //path to the image for the tab (optionaly can be set using css)
                    imageHeight: '257px',                               //height of tab image
                    imageWidth: '55px',                               //width of tab image    
                    tabLocation: 'left',                               //side of screen where tab lives, top, right, bottom, or left
                    speed: 300,                                        //speed of animation
                    action: 'click',                                   //options: 'click' or 'hover', action to trigger animation
                    topPos: '200px',                                   //position from the top
                    fixedPosition: false,
                    onLoadSlideOut: false                               //options: true makes it stick(fixed position) on scroll
                });
                
                $('.slide-out-div').tabSlideOut({
                    tabHandle: '.handle',                              //class of the element that will be your tab
                    pathToTabImage: '{{ asset('/idnwin99/img/ctTab.jpg') }}',          //path to the image for the tab (optionaly can be set using css)
                    imageHeight: '200px',                               //height of tab image
                    imageWidth: '41px',                               //width of tab image    
                    tabLocation: 'right',                               //side of screen where tab lives, top, right, bottom, or left
                    speed: 300,                                        //speed of animation
                    action: 'click',                                   //options: 'click' or 'hover', action to trigger animation
                    topPos: '200px',                                   //position from the top
                    fixedPosition: false,
                    onLoadSlideOut: false                               //options: true makes it stick(fixed position) on scroll
                });
            });

            $('.bxslider').bxSlider({
              auto: true,
              autoControls: true,
            });

            <!-- LiveChat (www.livechatinc.com) -->
            window.__lc = window.__lc || {};
            window.__lc.license = 8450921;
            (function() {
                var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
                lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
                var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
            })();
        </script>
    
        @yield('js')

        @yield('bottom_js')

    </body>
</html>
