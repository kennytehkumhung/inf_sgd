@extends('idnwin99/master')

@section('title', '4D')

@section('css')
@parent

<link href="{{ asset('/idnwin99/resources/css/leanmodal.css') }}" rel="stylesheet" type="text/css" />
<style type="text/css">
    .lmHolder {
        display: none;
        z-index: 9999 !important;
        position: absolute;
        right: 0;
        bottom: 0;
        left: 0;
        width: 560px;
        margin: auto;
        overflow: auto;
        background: #fff;
        border-radius: 5px;
        padding: 1em 2em;
        max-height: 455px !important;
        font-size: 11px;
    }
    
    .lmHolder table {
        border-collapse: collapse;
        width: 100%;
    }
    
    .lmHolder table, .lmHolder table th, .lmHolder table td {
        border: 1px solid black;
    }
    
    .lmHolder table tr:first-child td {
        font-weight: bold;
    }
    
    .lmHolder table td {
        padding: 6px 0;
        text-align: center;
        vertical-align: middle;
    }
    
    .btnClose {
        font-weight: bold;
        cursor: pointer;
        color: gray;
        font-size: 18px;
    }
</style>
@endsection

@section('js')
@parent

<script src="{{ asset('/idnwin99/resources/js/jquery.leanModal.min.js') }}"></script>
<script type="text/javascript">
    $(function($) {
        $("a[rel*=leanModal]").leanModal({
            overlay : 0.7,
            closeButton: ".btnClose"
        });
    });
    
    function MM_swapImgRestore() { //v3.0
        var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
    }
    
    function MM_preloadImages() { //v3.0
        var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
        var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
        if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
    }

    function MM_findObj(n, d) { //v4.01
        var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
            d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
        if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
        for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
        if(!x && d.getElementById) x=d.getElementById(n); return x;
    }

    function MM_swapImage() { //v3.0
        var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
        if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
    }
</script>
@endsection

@section('content')
<div class="midSect">
    
    @include('idnwin99.include.notice')
    
    <div class="midSectInner">
        <div class="midSectCont">
            <div class="dCont">
                <div class="dCont1">
                    <div class="lc4">
                        <div class="lcBox3">
                            <div id="crossfade">
                                <a href="#lmPayoutTable" rel="leanModal">
                                    <img class="bottom" src="{{ asset('/idnwin99/img/payout-btn-hover.png') }}" />
                                    <img class="top" src="{{ asset('/idnwin99/img/payout-btn.png') }}" />
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="dBnr">
                        <img src="{{ asset('/idnwin99/img/4d-banner.png') }}" width="240" height="182">
                    </div>
                    <div class="lc5">
                        <div class="lcBox3">
                            <div id="crossfade">
                                <a href="javascript:void(0);" onClick="@if (Auth::user()->check())window.open('{{route('psb')}}', 'pubContent', 'width=1050,height=650');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif">
                                    <img class="bottom" src="{{ asset('/idnwin99/img/bet-now-btn-hover.png') }}" />
                                    <img class="top" src="{{ asset('/idnwin99/img/bet-now-btn.png') }}" />
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="dContainerBottom">
                    <div class="dContainerBottomInner">
                        <!--magnum-->
                        <div id="fD_table">
                            <div id="fD_table_header" class="magnum">
                                <div id="fD_table_img">
                                    <img src="{{ asset('/idnwin99/img/4D_magnum.png') }}" width="80" height="40" />
                                </div>
                                <div id="fD_table_title">
                                    <span class="title">MAGNUM 4D</span><br/>
                                    <span>{{ $Magnum['date'] }}</span>
                                </div>
                            </div>
                            <div id="fD_table_result">
                                <table align="center" width="210" border="1" cellpadding="1" cellspacing="0" style="border-color:#999;">
                                    <tr>
                                        <td width="61" align="center" class="magnum fD_top3_description">1st</td>
                                        <td width="61" align="center" class="magnum fD_top3_description">2nd</td>
                                        <td align="center" class="magnum fD_top3_description">3rd</td>
                                    </tr>
                                    <tr>
                                        <td align="center" class="fD_top3_no"><span>{{ $Magnum['first'] }}</span></td>
                                        <td align="center" class="fD_top3_no"><span>{{ $Magnum['second'] }}</span></td>
                                        <td align="center" class="fD_top3_no"><span>{{ $Magnum['third'] }}</span></td>
                                    </tr>
                                </table>
                                <table align="center" width="210" border="1" cellpadding="1" cellspacing="0" class="fD_normalPrice">
                                    <tr>
                                        <td colspan="3" align="center" class="magnum">Special</td>
                                    </tr>
                                    <tr>
                                        <td align="center"><span>{{ $Magnum['special'][0] }}</span></td>
                                        <td align="center"><span>{{ $Magnum['special'][1] }}</span></td>
                                        <td align="center"><span>{{ $Magnum['special'][2] }}</span></td>
                                    </tr>
                                    <tr>
                                        <td align="center"><span>{{ $Magnum['special'][3] }}</span></td>
                                        <td align="center"><span>{{ $Magnum['special'][4] }}</span></td>
                                        <td align="center"><span>{{ $Magnum['special'][5] }}</span></td>
                                    </tr>
                                    <tr>
                                        <td align="center"><span>{{ $Magnum['special'][6] }}</span></td>
                                        <td align="center"><span>{{ $Magnum['special'][7] }}</span></td>
                                        <td align="center"><span>{{ $Magnum['special'][8] }}</span></td>
                                    </tr>
                                    <tr>
                                        <td align="center" bgcolor="#999">&nbsp;</td>
                                        <td align="center"><span>{{ $Magnum['special'][9] }}</span></td>
                                        <td align="center" bgcolor="#999">&nbsp;</td>
                                    </tr>
                                </table>
                                <table align="center" width="210" border="1" cellpadding="1" cellspacing="0" class="fD_normalPrice">
                                    <tr>
                                        <td colspan="3" align="center" class="magnum">Consolation</td>
                                    </tr>
                                    <tr>
                                        <td align="center"><span>{{ $Magnum['consolation'][0] }}</span></td>
                                        <td align="center"><span>{{ $Magnum['consolation'][1] }}</span></td>
                                        <td align="center"><span>{{ $Magnum['consolation'][2] }}</span></td>
                                    </tr>
                                    <tr>
                                        <td align="center"><span>{{ $Magnum['consolation'][3] }}</span></td>
                                        <td align="center"><span>{{ $Magnum['consolation'][4] }}</span></td>
                                        <td align="center"><span>{{ $Magnum['consolation'][5] }}</span></td>
                                    </tr>
                                    <tr>
                                        <td align="center"><span>{{ $Magnum['consolation'][6] }}</span></td>
                                        <td align="center"><span>{{ $Magnum['consolation'][7] }}</span></td>
                                        <td align="center"><span>{{ $Magnum['consolation'][8] }}</span></td>
                                    </tr>
                                    <tr>
                                        <td align="center" bgcolor="#999">&nbsp;</td>
                                        <td align="center"><span>{{ $Magnum['consolation'][9] }}</span></td>
                                        <td align="center" bgcolor="#999">&nbsp;</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <!--end magnum-->
                        <!--damacai-->
                        <div id="fD_table">
                            <div id="fD_table_header" class="damacai">
                                <div id="fD_table_img">
                                    <img src="{{ asset('/idnwin99/img/4D_damacai.png') }}" width="80" height="40" />
                                </div>
                                <div id="fD_table_title">
                                    <span class="title">DAMACAI 1+3D</span><br/>
                                    <span>{{ $PMP['date'] }}</span>
                                </div>
                            </div>
                            <div id="fD_table_result">
                                <table width="210" border="1" cellpadding="1" cellspacing="0" style="border-color:#999;">
                                    <tr>
                                        <td width="61" align="center" class="damacai fD_top3_description">1st</td>
                                        <td width="61" align="center" class="damacai fD_top3_description">2nd</td>
                                        <td align="center" class="damacai fD_top3_description">3rd</td>
                                    </tr>
                                    <tr>
                                        <td align="center" class="fD_top3_no"><span id="ContentPlaceHolder1_lbl_PMP_1st">{{ $PMP['first'] }}</span></td>
                                        <td align="center" class="fD_top3_no"><span id="ContentPlaceHolder1_lbl_PMP_2nd">{{ $PMP['second'] }}</span></td>
                                        <td align="center" class="fD_top3_no"><span id="ContentPlaceHolder1_lbl_PMP_3rd">{{ $PMP['third'] }}</span></td>
                                    </tr>
                                </table>
                                <table width="210" border="1" cellpadding="1" cellspacing="0" class="fD_normalPrice">
                                    <tr>
                                        <td colspan="3" align="center" class="damacai">Special</td>
                                    </tr>
                                    <tr>
                                        <td align="center"><span>{{ $PMP['special'][0] }}</span></td>
                                        <td align="center"><span>{{ $PMP['special'][1] }}</span></td>
                                        <td align="center"><span>{{ $PMP['special'][2] }}</span></td>
                                    </tr>
                                    <tr>
                                        <td align="center"><span>{{ $PMP['special'][3] }}</span></td>
                                        <td align="center"><span>{{ $PMP['special'][4] }}</span></td>
                                        <td align="center"><span>{{ $PMP['special'][5] }}</span></td>
                                    </tr>
                                    <tr>
                                        <td align="center"><span>{{ $PMP['special'][6] }}</span></td>
                                        <td align="center"><span>{{ $PMP['special'][7] }}</span></td>
                                        <td align="center"><span>{{ $PMP['special'][8] }}</span></td>
                                    </tr>
                                    <tr>
                                        <td align="center" bgcolor="#999">&nbsp;</td>
                                        <td align="center"><span>{{ $PMP['special'][9] }}</span></td>
                                        <td align="center" bgcolor="#999">&nbsp;</td>
                                    </tr>
                                </table>
                                <table width="210" border="1" cellpadding="1" cellspacing="0" class="fD_normalPrice">
                                    <tr>
                                        <td colspan="3" align="center" class="damacai">Consolation</td>
                                    </tr>
                                    <tr>
                                        <td align="center"><span>{{ $PMP['consolation'][0] }}</span></td>
                                        <td align="center"><span>{{ $PMP['consolation'][1] }}</span></td>
                                        <td align="center"><span>{{ $PMP['consolation'][2] }}</span></td>
                                    </tr>
                                    <tr>
                                        <td align="center"><span>{{ $PMP['consolation'][3] }}</span></td>
                                        <td align="center"><span>{{ $PMP['consolation'][4] }}</span></td>
                                        <td align="center"><span>{{ $PMP['consolation'][5] }}</span></td>
                                    </tr>
                                    <tr>
                                        <td align="center"><span>{{ $PMP['consolation'][6] }}</span></td>
                                        <td align="center"><span>{{ $PMP['consolation'][7] }}</span></td>
                                        <td align="center"><span>{{ $PMP['consolation'][8] }}</span></td>
                                    </tr>
                                    <tr>
                                        <td align="center" bgcolor="#999">&nbsp;</td>
                                        <td align="center"><span>{{ $PMP['consolation'][9] }}</span></td>
                                        <td align="center" bgcolor="#999">&nbsp;</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <!--end damacai-->
                        <!--toto-->
                        <div id="fD_table">
                            <div id="fD_table_header" class="toto">
                                <div id="fD_table_img">
                                    <img src="{{ asset('/idnwin99/img/4D_toto.png') }}" width="80" height="40" />
                                </div>
                                <div id="fD_table_title">
                                    <span class="title">TOTO 4D</span><br/>
                                    <span>{{ $Toto['date'] }}</span>
                                </div>
                            </div>
                            <div id="fD_table_result">
                                <table width="210" border="1" cellpadding="1" cellspacing="0" style="border-color:#999;">
                                    <tr>
                                        <td width="61" align="center" class="toto fD_top3_description">1st</td>
                                        <td width="61" align="center" class="toto fD_top3_description">2nd</td>
                                        <td align="center" class="toto fD_top3_description">3rd</td>
                                    </tr>
                                    <tr>
                                        <td align="center" class="fD_top3_no"><span>{{ $Toto['first'] }}</span></td>
                                        <td align="center" class="fD_top3_no"><span>{{ $Toto['second'] }}</span></td>
                                        <td align="center" class="fD_top3_no"><span>{{ $Toto['third'] }}</span></td>
                                    </tr>
                                </table>
                                <table width="210" border="1" cellpadding="1" cellspacing="0" class="fD_normalPrice">
                                    <tr>
                                        <td colspan="3" align="center" class="toto">Special</td>
                                    </tr>
                                    <tr>
                                        <td align="center"><span>{{ $Toto['special'][0] }}</span></td>
                                        <td align="center"><span>{{ $Toto['special'][1] }}</span></td>
                                        <td align="center"><span>{{ $Toto['special'][2] }}</span></td>

                                    </tr>
                                    <tr>
                                        <td align="center"><span>{{ $Toto['special'][3] }}</span></td>
                                        <td align="center"><span>{{ $Toto['special'][4] }}</span></td>
                                        <td align="center"><span>{{ $Toto['special'][5] }}</span></td>
                                    </tr>
                                    <tr>
                                        <td align="center"><span>{{ $Toto['special'][6] }}</span></td>
                                        <td align="center"><span>{{ $Toto['special'][7] }}</span></td>
                                        <td align="center"><span>{{ $Toto['special'][8] }}</span></td>
                                    </tr>
                                    <tr>
                                        <td align="center" bgcolor="#999">&nbsp;</td>
                                        <td align="center"><span>{{ $Toto['special'][9] }}</span></td>
                                        <td align="center" bgcolor="#999">&nbsp;</td>
                                    </tr>
                                </table>
                                <table width="210" border="1" cellpadding="1" cellspacing="0" class="fD_normalPrice">
                                    <tr>
                                        <td colspan="3" align="center" class="toto">Consolation</td>
                                    </tr>
                                    <tr>
                                        <td align="center"><span>{{ $Toto['consolation'][0] }}</span></td>
                                        <td align="center"><span>{{ $Toto['consolation'][1] }}</span></td>
                                        <td align="center"><span>{{ $Toto['consolation'][2] }}</span></td>
                                    </tr>
                                    <tr>
                                        <td align="center"><span>{{ $Toto['consolation'][3] }}</span></td>
                                        <td align="center"><span>{{ $Toto['consolation'][4] }}</span></td>
                                        <td align="center"><span>{{ $Toto['consolation'][5] }}</span></td>
                                    </tr>
                                    <tr>
                                        <td align="center"><span>{{ $Toto['consolation'][6] }}</span></td>
                                        <td align="center"><span>{{ $Toto['consolation'][7] }}</span></td>
                                        <td align="center"><span>{{ $Toto['consolation'][8] }}</span></td>
                                    </tr>
                                    <tr>
                                        <td align="center" bgcolor="#999">&nbsp;</td>
                                        <td align="center"><span>{{ $Toto['consolation'][9] }}</span></td>
                                        <td align="center" bgcolor="#999">&nbsp;</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <!--end toto-->
                        <!--Singapore-->
                        <div id="fD_table2">
                            <div id="fD_table_header" class="singapore">
                                <div id="fD_table_img">
                                    <img src="{{ asset('/idnwin99/img/4D_singapore.png') }}" width="80" height="40" />
                                </div>
                                <div id="fD_table_title">
                                    <span class="title">SINGAPORE 4D</span><br/>
                                    <span>{{ $Singapore['date'] }}</span>
                                </div>
                            </div>
                            <div id="fD_table_result">
                                <table width="210" border="1" cellpadding="1" cellspacing="0" style="border-color:#999;">
                                    <tr>
                                        <td width="61" align="center" class="singapore fD_top3_description">1st</td>
                                        <td width="61" align="center" class="singapore fD_top3_description">2nd</td>
                                        <td align="center" class="singapore fD_top3_description">3rd</td>
                                    </tr>
                                    <tr>
                                        <td align="center" class="fD_top3_no"><span>{{ $Singapore['first'] }}</span></td>
                                        <td align="center" class="fD_top3_no"><span>{{ $Singapore['second'] }}</span></td>
                                        <td align="center" class="fD_top3_no"><span>{{ $Singapore['third'] }}</span></td>
                                    </tr>
                                </table>
                                <table width="210" border="1" cellpadding="1" cellspacing="0" class="fD_normalPrice">
                                    <tr>
                                        <td colspan="3" align="center" class="singapore">Special</td>
                                    </tr>
                                    <tr>
                                        <td align="center"><span>{{ $Singapore['special'][0] }}</span></td>
                                        <td align="center"><span>{{ $Singapore['special'][1] }}</span></td>
                                        <td align="center"><span>{{ $Singapore['special'][2] }}</span></td>
                                    </tr>
                                    <tr>
                                        <td align="center"><span>{{ $Singapore['special'][3] }}</span></td>
                                        <td align="center"><span>{{ $Singapore['special'][4] }}</span></td>
                                        <td align="center"><span>{{ $Singapore['special'][5] }}</span></td>
                                    </tr>
                                    <tr>
                                        <td align="center"><span>{{ $Singapore['special'][6] }}</span></td>
                                        <td align="center"><span>{{ $Singapore['special'][7] }}</span></td>
                                        <td align="center"><span>{{ $Singapore['special'][8] }}</span></td>
                                    </tr>
                                    <tr>
                                        <td align="center" bgcolor="#999">&nbsp;</td>
                                        <td align="center"><span>{{ $Singapore['special'][9] }}</span></td>
                                        <td align="center" bgcolor="#999">&nbsp;</td>
                                    </tr>
                                </table>
                                <table width="210" border="1" cellpadding="1" cellspacing="0" class="fD_normalPrice">
                                    <tr>
                                        <td colspan="3" align="center" class="singapore">Consolation</td>
                                    </tr>
                                    <tr>
                                        <td align="center"><span>{{ $Singapore['consolation'][0] }}</span></td>
                                        <td align="center"><span>{{ $Singapore['consolation'][1] }}</span></td>
                                        <td align="center"><span>{{ $Singapore['consolation'][2] }}</span></td>
                                    </tr>
                                    <tr>
                                        <td align="center"><span>{{ $Singapore['consolation'][3] }}</span></td>
                                        <td align="center"><span>{{ $Singapore['consolation'][4] }}</span></td>
                                        <td align="center"><span>{{ $Singapore['consolation'][5] }}</span></td>
                                    </tr>
                                    <tr>
                                        <td align="center"><span>{{ $Singapore['consolation'][6] }}</span></td>
                                        <td align="center"><span>{{ $Singapore['consolation'][7] }}</span></td>
                                        <td align="center"><span>{{ $Singapore['consolation'][8] }}</span></td>
                                    </tr>
                                    <tr>
                                        <td align="center" bgcolor="#999">&nbsp;</td>
                                        <td align="center"><span>{{ $Singapore['consolation'][9] }}</span></td>
                                        <td align="center" bgcolor="#999">&nbsp;</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <!--end singapore-->
                        <!--t88-->
                        <div id="fD_table1">
                            <div id="fD_table_header" class="t88">
                                <div id="fD_table_img">
                                    <img src="{{ asset('/idnwin99/img/4D_88.png') }}" width="80" height="40" />
                                </div>
                                <div id="fD_table_title">
                                    <span class="title">SABAH 4D</span><br/>
                                    <span>{{ $Sabah['date'] }}</span>
                                </div>
                            </div>
                            <div id="fD_table_result">
                                <table width="210" border="1" cellpadding="1" cellspacing="0" style="border-color:#999;">
                                    <tr>
                                        <td width="61" align="center" class="t88 fD_top3_description">1st</td>
                                        <td width="61" align="center" class="t88 fD_top3_description">2nd</td>
                                        <td align="center" class="t88 fD_top3_description">3rd</td>
                                    </tr>
                                    <tr>
                                        <td align="center" class="fD_top3_no"><span>{{ $Sabah['first'] }}</span></td>
                                        <td align="center" class="fD_top3_no"><span>{{ $Sabah['second'] }}</span></td>
                                        <td align="center" class="fD_top3_no"><span>{{ $Sabah['third'] }}</span></td>
                                    </tr>
                                </table>
                                <table width="210" border="1" cellpadding="1" cellspacing="0" class="fD_normalPrice">
                                    <tr>
                                        <td colspan="3" align="center" class="t88">Special</td>
                                    </tr>
                                    <tr>
                                        <td align="center"><span>{{ $Sabah['special'][0] }}</span></td>
                                        <td align="center"><span>{{ $Sabah['special'][1] }}</span></td>
                                        <td align="center"><span>{{ $Sabah['special'][2] }}</span></td>
                                    </tr>
                                    <tr>
                                        <td align="center"><span>{{ $Sabah['special'][3] }}</span></td>
                                        <td align="center"><span>{{ $Sabah['special'][4] }}</span></td>
                                        <td align="center"><span>{{ $Sabah['special'][5] }}</span></td>
                                    </tr>
                                    <tr>
                                        <td align="center"><span>{{ $Sabah['special'][6] }}</span></td>
                                        <td align="center"><span>{{ $Sabah['special'][7] }}</span></td>
                                        <td align="center"><span>{{ $Sabah['special'][8] }}</span></td>
                                    </tr>
                                    <tr>
                                        <td align="center" bgcolor="#999">&nbsp;</td>
                                        <td align="center"><span>{{ $Sabah['special'][9] }}</span></td>
                                        <td align="center" bgcolor="#999">&nbsp;</td>
                                    </tr>
                                </table>
                                <table width="210" border="1" cellpadding="1" cellspacing="0" class="fD_normalPrice">
                                    <tr>
                                        <td colspan="3" align="center" class="t88">Consolation</td>
                                    </tr>
                                    <tr>
                                        <td align="center"><span>{{ $Sabah['consolation'][0] }}</span></td>
                                        <td align="center"><span>{{ $Sabah['consolation'][1] }}</span></td>
                                        <td align="center"><span>{{ $Sabah['consolation'][2] }}</span></td>
                                    </tr>
                                    <tr>
                                        <td align="center"><span>{{ $Sabah['consolation'][3] }}</span></td>
                                        <td align="center"><span>{{ $Sabah['consolation'][4] }}</span></td>
                                        <td align="center"><span>{{ $Sabah['consolation'][5] }}</span></td>
                                    </tr>
                                    <tr>
                                        <td align="center"><span>{{ $Sabah['consolation'][6] }}</span></td>
                                        <td align="center"><span>{{ $Sabah['consolation'][7] }}</span></td>
                                        <td align="center"><span>{{ $Sabah['consolation'][8] }}</span></td>
                                    </tr>
                                    <tr>
                                        <td align="center" bgcolor="#999">&nbsp;</td>
                                        <td align="center"><span>{{ $Sabah['consolation'][9] }}</span></td>
                                        <td align="center" bgcolor="#999">&nbsp;</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <!--End of t88-->
                        <!--STC-->
                        <div id="fD_table3">
                            <div id="fD_table_header" class="stc">
                                <div id="fD_table_img">
                                    <img src="{{ asset('/idnwin99/img/4D_stc.png') }}" width="80" height="40" />
                                </div>
                                <div id="fD_table_title">
                                    <span class="title">SANDAKAN 4D</span><br/>
                                    <span>{{ $Sandakan['date'] }}</span>
                                </div>
                            </div>
                            <div id="fD_table_result">
                                <table width="210" border="1" cellpadding="1" cellspacing="0" style="border-color:#999;">
                                    <tr>
                                        <td width="61" align="center" class="stc fD_top3_description">1st</td>
                                        <td width="61" align="center" class="stc fD_top3_description">2nd</td>
                                        <td align="center" class="stc fD_top3_description">3rd</td>
                                    </tr>
                                    <tr>
                                        <td align="center" class="fD_top3_no"><span>{{ $Sandakan['first'] }}</span></td>
                                        <td align="center" class="fD_top3_no"><span>{{ $Sandakan['second'] }}</span></td>
                                        <td align="center" class="fD_top3_no"><span>{{ $Sandakan['third'] }}</span></td>
                                    </tr>
                                </table>
                                <table width="210" border="1" cellpadding="1" cellspacing="0" class="fD_normalPrice">
                                    <tr>
                                        <td colspan="3" align="center" class="stc">Special</td>
                                    </tr>
                                    <tr>
                                        <td align="center"><span>{{ $Sandakan['special'][0] }}</span></td>
                                        <td align="center"><span>{{ $Sandakan['special'][1] }}</span></td>
                                        <td align="center"><span>{{ $Sandakan['special'][2] }}</span></td>

                                    </tr>
                                    <tr>
                                        <td align="center"><span>{{ $Sandakan['special'][3] }}</span></td>
                                        <td align="center"><span>{{ $Sandakan['special'][4] }}</span></td>
                                        <td align="center"><span>{{ $Sandakan['special'][5] }}</span></td>
                                    </tr>
                                    <tr>

                                        <td align="center"><span>{{ $Sandakan['special'][6] }}</span></td>
                                        <td align="center"><span>{{ $Sandakan['special'][7] }}</span></td>
                                        <td align="center"><span>{{ $Sandakan['special'][8] }}</span></td>

                                    </tr>
                                    <tr>
                                        <td align="center" bgcolor="#999">&nbsp;</td>
                                        <td align="center"><span>{{ $Sandakan['special'][9] }}</span></td>
                                        <td align="center" bgcolor="#999">&nbsp;</td>
                                    </tr>
                                </table>
                                <table width="210" border="1" cellpadding="1" cellspacing="0" class="fD_normalPrice">
                                    <tr>
                                        <td colspan="3" align="center" class="stc">Consolation</td>
                                    </tr>
                                    <tr>
                                        <td align="center"><span>{{ $Sandakan['consolation'][0] }}</span></td>
                                        <td align="center"><span>{{ $Sandakan['consolation'][1] }}</span></td>
                                        <td align="center"><span>{{ $Sandakan['consolation'][2] }}</span></td>
                                    </tr>
                                    <tr>
                                        <td align="center"><span>{{ $Sandakan['consolation'][3] }}</span></td>
                                        <td align="center"><span>{{ $Sandakan['consolation'][4] }}</span></td>
                                        <td align="center"><span>{{ $Sandakan['consolation'][5] }}</span></td>
                                    </tr>
                                    <tr>

                                        <td align="center"><span>{{ $Sandakan['consolation'][6] }}</span></td>
                                        <td align="center"><span>{{ $Sandakan['consolation'][7] }}</span></td>
                                        <td align="center"><span>{{ $Sandakan['consolation'][8] }}</span></td>

                                    </tr>
                                    <tr>
                                        <td align="center" bgcolor="#999">&nbsp;</td>
                                        <td align="center"><span>{{ $Sandakan['consolation'][9] }}</span></td>
                                        <td align="center" bgcolor="#999">&nbsp;</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <!--End of STC-->
                        <!--Cash-->
                        <div id="fD_table3">
                            <div id="fD_table_header" class="cash">
                                <div id="fD_table_img">
                                    <img src="{{ asset('/idnwin99/img/4D_cash.png') }}" width="80" height="40" />
                                </div>
                                <div id="fD_table_title">
                                    <span class="title">SPECIAL BIGSWEEP</span><br/>
                                    <span>{{ $Sarawak['date'] }}</span>
                                </div>
                            </div>
                            <div id="fD_table_result">
                                <table width="210" border="1" cellpadding="1" cellspacing="0" style="border-color:#999;">
                                    <tr>
                                        <td width="61" align="center" class="cash fD_top3_description">1st</td>
                                        <td width="61" align="center" class="cash fD_top3_description">2nd</td>
                                        <td align="center" class="cash fD_top3_description">3rd</td>
                                    </tr>
                                    <tr>
                                        <td align="center" class="fD_top3_no"><span>{{ $Sarawak['first'] }}</span></td>
                                        <td align="center" class="fD_top3_no"><span>{{ $Sarawak['second'] }}</span></td>
                                        <td align="center" class="fD_top3_no"><span>{{ $Sarawak['third'] }}</span></td>
                                    </tr>
                                </table>
                                <table width="210" border="1" cellpadding="1" cellspacing="0" class="fD_normalPrice">
                                    <tr>
                                        <td colspan="3" align="center" class="cash">Special</td>
                                    </tr>
                                    <tr>
                                        <td align="center"><span>{{ $Sarawak['special'][0] }}</span></td>
                                        <td align="center"><span>{{ $Sarawak['special'][1] }}</span></td>
                                        <td align="center"><span>{{ $Sarawak['special'][2] }}</span></td>
                                    </tr>
                                    <tr>
                                        <td align="center"><span>{{ $Sarawak['special'][3] }}</span></td>
                                        <td align="center"><span>{{ $Sarawak['special'][4] }}</span></td>
                                        <td align="center"><span>{{ $Sarawak['special'][5] }}</span></td>
                                    </tr>
                                    <tr>
                                        <td align="center"><span>{{ $Sarawak['special'][6] }}</span></td>
                                        <td align="center"><span>{{ $Sarawak['special'][7] }}</span></td>
                                        <td align="center"><span>{{ $Sarawak['special'][8] }}</span></td>
                                    </tr>
                                    <tr>
                                        <td align="center" bgcolor="#999">&nbsp;</td>
                                        <td align="center"><span>{{ $Sarawak['special'][9] }}</span></td>
                                        <td align="center" bgcolor="#999">&nbsp;</td>
                                    </tr>
                                </table>
                                <table width="210" border="1" cellpadding="1" cellspacing="0" class="fD_normalPrice">
                                    <tr>
                                        <td colspan="3" align="center" class="cash">Consolation</td>
                                    </tr>
                                    <tr>
                                        <td align="center"><span>{{ $Sarawak['consolation'][0] }}</span></td>
                                        <td align="center"><span>{{ $Sarawak['consolation'][1] }}</span></td>
                                        <td align="center"><span>{{ $Sarawak['consolation'][2] }}</span></td>
                                    </tr>
                                    <tr>
                                        <td align="center"><span>{{ $Sarawak['consolation'][3] }}</span></td>
                                        <td align="center"><span>{{ $Sarawak['consolation'][4] }}</span></td>
                                        <td align="center"><span>{{ $Sarawak['consolation'][5] }}</span></td>
                                    </tr>
                                    <tr>
                                        <td align="center"><span>{{ $Sarawak['consolation'][6] }}</span></td>
                                        <td align="center"><span>{{ $Sarawak['consolation'][7] }}</span></td>
                                        <td align="center"><span>{{ $Sarawak['consolation'][8] }}</span></td>
                                    </tr>
                                    <tr>
                                        <td align="center" bgcolor="#999">&nbsp;</td>
                                        <td align="center"><span>{{ $Sarawak['consolation'][9] }}</span></td>
                                        <td align="center" bgcolor="#999">&nbsp;</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <!--End of Cash-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="lmPayoutTable" class="lmHolder">
    <div class="modal-content">
        <div style="text-align: right;"><span class="btnClose">x</span></div>
        
        <h2 style="color:#FFBF00;">{{Lang::get('public.Payout')}} {{Lang::get('public.Table')}}</h2><br>
        <b>Prize money for Big Forecast</b><br><br>
        For this Forecast, as shown in the table below, with every RM1 bet, the 1st Prize pays RM3,400, 2nd Prize RM1,200, 3rd Prize RM600, Special Prize RM250 and Consolation Prize RM80.
        <br><br>
        <div class="table" >	
            <table>
                <tr>
                    <td>BIG FORECAST</td>
                    <td>PRIZE AMOUNT</td>
                </tr>
                <tr>
                    <td>1st Prize</td>
                    <td>RM 3,400.00</td>
                </tr>
                <tr>
                    <td>2nd Prize</td>
                    <td>RM 1,200.00</td>
                </tr>
                <tr>
                    <td>3rd Prize</td>
                    <td>RM 600.00</td>
                </tr>
                <tr>
                    <td>Special Prize</td>
                    <td>RM 250.00</td>
                </tr>
                <tr>
                    <td>Consolation Prize</td>
                    <td>RM 80.00</td>
                </tr>
            </table>
        </div>

        <b>Prize money for Small Forecast</b><br><br>
        For this Forecast, as shown in the table below, with every RM1 bet, the 1st Prize pays RM4,800, 2nd Prize RM2,400, 3rd Prize RM1,200.
        <br><br>

        <div class="table" >	
            <table>
                <tr>
                    <td>SMALL FORECAST</td>
                    <td>PRIZE AMOUNT</td>
                </tr>
                <tr>
                    <td>1st Prize</td>
                    <td>RM 4,800.00</td>
                </tr>
                <tr>
                    <td>2nd Prize</td>
                    <td>RM 2,400.00</td>
                </tr>
                <tr>
                    <td>3rd Prize</td>
                    <td>RM 1,200.00</td>
                </tr>
                <tr>
                    <td>4A Prize</td>
                    <td>RM 8,000.00</td>
                </tr>
            </table>
        </div>

        <b>Prize money for 3D</b><br><br>
        For this Forecast, as shown in the table below, with every RM1 bet, the 1st Prize pays RM 280, 2nd Prize RM280, and 3rd Prize RM280.
        <div class="table" >	
            <table>
                <tr>
                    <td>3D</td>
                    <td>PRIZE AMOUNT</td>
                </tr>
                <tr>
                    <td>1st Prize</td>
                    <td>RM 280.00</td>
                </tr>
                <tr>
                    <td>2nd Prize</td>
                    <td>RM 280.00</td>
                </tr>
                <tr>
                    <td>3rd Prize</td>
                    <td>RM 280.00</td>
                </tr>
                <tr>
                    <td>3A Prize</td>
                    <td>RM 840.00</td>
                </tr>
            </table>
        </div>

        <b>Prize money for 5D/6D</b><br><br>
        For this Forecast, as shown in the table below, with every RM1 bet.
        <div class="table" >	
            <table>
                <tr>
                    <td>5D</td>
                    <td>PRIZE AMOUNT</td>
                    <td>6D</td>
                    <td>PRIZE AMOUNT</td>
                </tr>
                <tr>
                    <td>1st Prize</td>
                    <td>RM 15,000.00</td>
                    <td>1st Prize</td>
                    <td>RM 100,000.00</td>
                </tr>
                <tr>
                    <td>2nd Prize</td>
                    <td>RM 5,000.00</td>
                    <td>2nd Prize</td>
                    <td>RM 3,000.00</td>
                </tr>
                <tr>
                    <td>3rd Prize</td>
                    <td>RM 3,000.00</td>
                    <td>3rd Prize</td>
                    <td>RM 300.00</td>
                </tr>
                <tr>
                    <td>4th Prize</td>
                    <td>RM 500.00</td>
                    <td>4th Prize</td>
                    <td>RM 30.00</td>
                </tr>
                <tr>
                    <td>5th Prize</td>
                    <td>RM 20.00</td>
                    <td>5th Prize</td>
                    <td>RM 4.00</td>
                </tr>
                <tr>
                    <td>6th Prize</td>
                    <td>RM 5.00</td>
                    <td></td>
                    <td></td>
                </tr>
            </table>
        </div>
    </div>
</div>
@endsection
