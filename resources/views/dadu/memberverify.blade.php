@extends('dadu/master')

@section('title','Account Activation')


@section('content')

<script>
 function update_tac(){
	 
	$.ajax({
		type: "POST",
		url: "{{route('updatetac')}}?"+$('#tac_form').serialize(),
	
	}).done(function( result ) {
		if(result == 1){
			alert('{{Lang::get('COMMON.SUCESSFUL')}}');
                        location.reload();
		}else{
                    alert('Invalid TAC CODE!');
                }

					
	});
} 
</script>
@include('dadu/transaction_mange_top', [ 'title' => 'Account Activation' ] )
<div class="midSect">
    <div class="midSectInneracct">
         <!--ACCOUNT MANAGAMENT MENU-->
    <div class="inlineAccMenu">
        <ul>
            <li><a href="{{route('update-profile')}}">{{Lang::get('public.MyAccount')}}</a></li>
            <li><a href="{{route('update-password')}}">{{Lang::get('public.ChangePassword')}}</a></li>
            <li><a href="{{route('memberbank')}}">Bank Setting</a></li>
            <li><a href="{{route('memberverify')}}">Account Activation</a></li>
        </ul>
    </div>
    <!--ACCOUNT MANAGAMENT MENU-->
    <!--ACCOUNT TITLE-->
    <div class="title_bar">
        <span>Account Activation</span>
    </div>
    @if($verify->isemailvalid == 0)
    <form id="tac_form">
          <!--ACCOUNT TITLE-->
          <!--ACCOUNT CONTENT-->
          <div class="acctContent">
          <span class="wallet_title"><i class="fa fa-pencil"></i>Member Account Verify</span>    

          <div class="acctRow">
          <label>TAC CODE :</label>
                    <span class="acctText">
                            <input type="text" name="tac" id="tac">
                    </span>
          <div class="clr"></div>
          </div>  



          <div class="submitAcct" onClick="update_tac()">
          <a href="#">{{Lang::get('public.Submit')}}</a>
          </div>
          </div>
          <!--ACCOUNT CONTENT-->

    </form>   
    @else
    <form id="tac_form">
          <!--ACCOUNT TITLE-->
          <!--ACCOUNT CONTENT-->
          <div class="acctContent">
          <span class="wallet_title"><i class="fa fa-pencil"></i>Member Account Verify</span>    

          <div class="acctRow">
              <center><label>Account Activated!</label></center>
          </div>  

          </div>
          <!--ACCOUNT CONTENT-->

    </form> 
    @endif
    </div>
    <br>
</div>



<div class="clr"></div>






</div>



@stop
