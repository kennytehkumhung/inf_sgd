@extends('dadu/master')

@section('title', 'Indonesia Online Casino, Gambling Games | DADUKEMBAR')
@section('keywords', 'Indonesia Online Casino, Gambling Games | DADUKEMBAR')
@section('description', 'Enjoy gambling (kasino) games at Indonesia most trusted online casino, DADUKEMBAR. Play live casino, slots, horse racing and more.')

@section('top_css')
<link href="{{url()}}/dadu/resources/css/promo.css" rel="stylesheet" type="text/css" />
@stop

@section('top_js')
  <script>
  function forgotpassword_submit(){

	$.ajax({
		type: "POST",
		url: "{{route('resetpassword')}}",
		data: {
			_token: "{{ csrf_token() }}",
			username:		 $('#fg_username').val(),
			email:    		 $('#fg_email').val(),

		},
	}).done(function( json ) {
			 $('.acctTextReg').html('');
			 obj = JSON.parse(json);
			 var str = '';
			 $.each(obj, function(i, item) {
				if( '{{Lang::get('COMMON.SUCESSFUL')}}' == item ){
					window.location.href = "{{route('homepage')}}";
				}else{
					$('.'+i+'_acctTextReg').html(item);
					
				}
			})
	
	});
}
  </script>
@stop

@section('content')
<!--CASINO LOBBY SECTION-->
<div class="lcLobby">
    <div class="lcLobbyInner3">
        <img src="{{url()}}/dadu/img/forgot-tit.png">
    </div>
</div>
<!--CASINO LOBBY SECTION-->

<!--MID SECTION-->
<div class="midSect2">
    <div class="midSectInner">
        <div class="midContf">
            <h2><i class="fa fa-lock"></i> Forgot Password</h2> 
            <br>
            <p style="color:#fff;">
                We can help you reset your password and security info. First, enter your registered info and we will
                send an account restoration link to your email
                <br><br>
            </p>
            <div class="acctContentReg">
                <div class="acctRow">
         <label>{{Lang::get('public.Username')}} :</label><input type="text" id="fg_username" > <span class="username_acctTextReg acctTextReg"></span>
      <div class="clr"></div>
    </div>
      <div class="acctRow">
      <label>{{Lang::get('public.EmailAddress')}} :</label><input type="text" id="fg_email" > <span class="email_acctTextReg acctTextReg"></span>
      <div class="clr"></div>
      </div>
                <div class="loginBtn" style="text-align: center;">
                    <a href="#" onClick="forgotpassword_submit()">Submit</a>
                </div>
            </div>
        </div>
        <div class="clr"></div>
    </div>
</div>
<!--MID SECTION-->
@stop

@section('bottom_js')

@stop