@extends('dadu/master')

@section('title', 'Indonesia Online Casino, Gambling Games | DADUKEMBAR')
@section('keywords', 'Indonesia Online Casino, Gambling Games | DADUKEMBAR')
@section('description', 'Enjoy gambling (kasino) games at Indonesia most trusted online casino, DADUKEMBAR. Play live casino, slots, horse racing and more.')

@section('top_css')
<link href="{{url()}}/dadu/resources/css/lc.css" rel="stylesheet" type="text/css" />
@stop

@section('content')
<!--CASINO LOBBY SECTION-->
<div class="lcLobby">
    <div class="lcLobbyInner">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td width="200" height="200">
                    <div class="lcCont">
                        @if(in_array('PLTB',Session::get('valid_product')))
                        <a onClick="@if (Auth::user()->check())window.open('{{route('pltbiframe')}}', 'casinopltb', 'width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif">
                            <div class="showbox"></div>
                        </a>
                        @endif
                        @if(in_array('W88',Session::get('valid_product')))
                        <a onClick="@if (Auth::user()->check())window.open('{{route('w88', [ 'type' => 'casino' , 'category' => 'live' ] )}}', 'casinogp', 'width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif">
                            <div class="showbox1"></div>
                        </a>
                        @endif
                        @if(in_array('ALB',Session::get('valid_product')))
                        <a onClick="@if (Auth::user()->check())window.open('{{route('alb', [ 'type' => 'casino' ] )}}', 'casino12', 'width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif">
                            <div class="showbox2"></div>
                        </a>
                        @endif
                        @if(in_array('AGG',Session::get('valid_product')))
                        <a onClick="@if (Auth::user()->check())window.open('{{route('agg')}}', 'casino11', 'width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif">
                            <div class="showbox3"></div>
                        </a>
                        @endif
                        <div class="clr"></div>
                    </div>
                </td>
            </tr>
        </table>
        <div class="clr"></div>
    </div>
</div>
<!--CASINO LOBBY SECTION-->
@stop