@extends('dadu/master')

@section('title', 'Indonesia Online Casino, Gambling Games | DADUKEMBAR')
@section('keywords', 'Indonesia Online Casino, Gambling Games | DADUKEMBAR')
@section('description', 'Enjoy gambling (kasino) games at Indonesia most trusted online casino, DADUKEMBAR. Play live casino, slots, horse racing and more.')

@section('top_css')
<link href="{{url()}}/dadu/resources/css/sp.css" rel="stylesheet" type="text/css" />
@stop

@section('content')
<!--CASINO LOBBY SECTION-->
<div class="lcLobby">
    <div class="lcLobbyInnerM">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td width="400" height="300">
                    <div class="lcCont">
                        <a href="{{route('ibc')}}"><div class="showbox"></div></a>
                        <div align="center" class="showbox2"><i class="fa fa-futbol-o" aria-hidden="true"></i>SPORTSBOOK</div>  
                        <a href="{{route('tbs')}}"><div class="showbox1"></div></a>
                        <div class="clr"></div>
                    </div>
                </td>
            </tr>
        </table>
        <div class="clr"></div>
    </div>
</div>
<!--CASINO LOBBY SECTION-->
@stop

