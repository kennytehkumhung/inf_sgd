@extends('dadu/master')

@section('title', 'Indonesia Online Casino, Gambling Games | DADUKEMBAR')
@section('keywords', 'Indonesia Online Casino, Gambling Games | DADUKEMBAR')
@section('description', 'Enjoy gambling (kasino) games at Indonesia most trusted online casino, DADUKEMBAR. Play live casino, slots, horse racing and more.')

@section('top_css')
<link href="{{url()}}/dadu/resources/css/promo.css" rel="stylesheet" type="text/css" />
@stop

@section('content')
<!--CASINO LOBBY SECTION-->
<div class="lcLobby">
    <div class="lcLobbyInner3">
        <img src="{{url()}}/dadu/img/tnc.png" width="706" height="60">
    </div>
</div>
<!--CASINO LOBBY SECTION-->

<!--MID SECTION-->
<div class="midSect2">
    <div class="midSectInner">
        <br>
        @if( Lang::getLocale() == 'en' )
	<h5>TERM AND CONDITION </h5>
        <div class="lol" style="line-height: 20px; text-align: justify;">
	<span>1. General Terms and Conditions</span>
		<p>1.1 By registering to use the services covered by these Terms and Conditions on DADUKEMBAR (hereinafter each separately and all together collectively referred to as "DADUKEMBAR", "DADUKEMBAR.com" or "Site"), you are certifying that you are at least 21 years of age, and that you legally acknowledge to have read, fully understand and agree to be bound by and to comply with these "Terms and Conditions." If you do not wish to accept the following terms and conditions, you must neither register nor open an account and you will be unable to access the software and the gaming services offered in conjunction therewith.</p>
		<p>1.2 All product information, registration on the site and use of any services offered by DADUKEMBAR are subject to these "General Terms and Conditions".</p>
		<p>1.3 DADUKEMBAR reserves the right to amend these General Terms and Conditions at any time without prior notification. Such amendments shall become enforceable with immediate effect immediately upon publication in the "General Terms and Conditions" section of the DADUKEMBAR site. It is the user’s responsibility to review the General Terms and Conditions on a regular basis. Continued use of any services of the Site after a change or update has been made to the General Terms and Conditions will constitute your acknowledgement and acceptance of said change or update.</p>
		<p>1.4 These General Terms and Conditions represent the complete, final and exclusive agreement between the user and DADUKEMBAR. These “General Terms and Conditions” supersede and merge all prior agreements, representations and understandings between the user and DADUKEMBAR. You confirm that, in agreeing to accept these General Terms and Conditions, you have not relied on any representation save insofar as the same has expressly been made a representation in these General Terms and Conditions.</p>
		<p>1.5 In case of any contradiction between a provision of this Agreement and any provision on the DADUKEMBAR website, the provisions as stated in this Agreement shall prevail.</p>
		<p>1.6 We reserve the right, at our own discretion, to request any user to furnish us with a proof of identity and age as a precondition before the user is allowed to participate in or utilize any of DADUKEMBAR services</p>

	<span>2. Account : Conditions Of Use</span>
		<p>2.1 Should there be any dispute concerning a Player’s account, DADUKEMBAR reserves the right to suspend the Player’s account until a resolution is reached. Lodging a report of any dispute must be made in writing and muststate the date, time and details of dispute. To lodge a dispute, it must be sent via email to cs@DADUKEMBAR.com. The Management takes member disputes very seriously and will endeavor to take all necessary and reasonable steps in order to investigate and ultimately resolve all disputes.</p>
		<p>2.2 DADUKEMBAR reserves the right to suspend and/orterminate any user’s account for any reason whatsoever and at any time without prior notice. Any remaining balance of credit in the account at the time of such suspension and/or termination will be refunded to the owner of the account via a payment method that will be determined by DADUKEMBAR. DADUKEMBAR also reserves the right, at its sole discretion, to deem void any winnings and to forfeit any balance of credit or any other winnings or entitlements of any user’s account as well as any user’s use DADUKEMBAR services.</p>
		<p>2.3 Each user is solely responsible for keeping his/her own account number and password secure. Each DADUKEMBAR user is, without exception, expected to strictly disallow any other person or third party including, without limitations, any minor, from using or reusing his/her DADUKEMBAR account, from accessing and/or using any materials or information from the DADUKEMBAR Website as well as from accepting any prize. The useris solely responsible for any purchases and/or losses that are self-incurred or incurred via a third party on his/her DADUKEMBAR account.</p>

	<span>3. Wagering</span>
		<p>3.1 Acceptance and Validation of Wagering: 
		Wager
		Players must wager from funds that have been credited into their respective Player accounts. All wagering will be deemed void, if upon review it is found that the Player has never successfully deposited funds or completed any Funds Transfer into their respective Player accounts. After successfully depositing funds into his/her account, the Player’s total bet must be the equivalent or exceed the deposited amount. Failure to comply will result in failure to apply for any withdrawal, thus it is advisable for Players to make deposits based ondemand.
		Confirmed Wagers
		DADUKEMBAR cannot and will not cancel wagers of any Player account once they have been confirmed, unless the wager is declared void or unless there are circumstances involving incorrect results and settlements for reasons outlined in these Terms & Conditions.
		Accurate Wagering Records
		Despite every effort to ensure total accuracy, DADUKEMBAR does not and will not accept responsibility for any errors or omissions with respect to the information provided on the Website.
		Deduction of Wagers / Unresolved Wagers
		Stakes will be deducted from the respective Player account at the time the Bet is placed, regardless of when the resultsare determined or announced. DADUKEMBAR cannot and will not refund Bets placed on games where Bets have been purchased for future draws that have yet to be resolved or concluded. Such Bets will be resolved upon completion of such games and winnings, if applicable, will be credited to the respective Players’ accounts at that time.
		DADUKEMBAR’s Wagering Decision is Final
		By accepting these General Terms and Conditions, the user agrees that DADUKEMBAR and its records shall be the final authority in determining the terms of any wagers placed as well as the circumstances in which those wagers were made.
		Player’s Sole Responsibility for Winnings
		Any applicable taxes and fees incurred upon any winnings that have been awarded to you as well as all other payments made to the respective Player are considered to be the Player’s sole responsibility. DADUKEMBAR reserves the right to report and withhold any amount from the Player’s winnings in order to comply with any applicable law. Winnings cannot be transferred, substituted or redeemed for any other forms of winnings.</p>

		<p>3.2 Stakes Wagered: 
		Minimum Stakes
		A minimum stake amount applies. The amount is dependent on the particular Game/Product.
		Automatic Game Playing / Manipulation of Software / Tampering / Cheating
		Automatic playing of Games via the use of any third party software and any form of manipulation of any Games or any Player's account data are strictly prohibited and may result in the termination of the offending Player’s membership, deletion of all associated accounts, the cancellation and/or confiscation of any outstanding prizes and deposits,as well as civil/criminal prosecution against the offending Player. By agreeing to these General Terms and Conditions, the Player agrees that DADUKEMBAR possesses the complete and unfettered right, based on its own discretion, to terminate any Player’s account for any reason whatsoever, and without limiting the generality of the foregoing, should it be of the Management’s opinion that the Player’s participation on the Website, or on any games offered therein, is damaging to the principles under which the Website is operated. The Player hereby waives any and all recourse against DADUKEMBAR in the occurrence of any such termination.
		By agreeing to these General Terms and Conditions, the Player acknowledges that full freedom from errors is impossible to achieve with regards to computer software. Should the Player become aware that the software contains any such error, glitch or incompleteness, the Player is legally bound to refrain from taking any advantage of it/them. Moreover, upon becoming aware of such errors, glitches or incompleteness, the Player legally bound to promptly notify DADUKEMBAR in writing. Should you fail to fulfill your undertakings under this clause, DADUKEMBAR is entitled to full compensation for all costs, including costs for rectifying the software that may arise from the Player’s omissions or actions in taking advantage of such errors, glitches or incompleteness.
		By agreeing to these General Terms and Conditions, the Player agrees that DADUKEMBAR cannot be held responsible for any damage, loss, or injury resulting from tampering, or other unauthorized access or use of the Website or Player’s account. Any attempt by any Player to gain unauthorized access to the Website's systems or Player’s account, to interfere with procedures or performance of the Website or games, or to deliberately damage or undermine the Website or Games is subject to civil and/or criminal prosecution and will result in immediate termination of the Player’s account as well as the forfeiture of any and all prizes that the Player has and have been entitled to.</p>

	<span>4. Winnings</span>
		<p>4.1 Prior to releasing or approving any withdrawal, DADUKEMBAR reserves the right to request any Player to furnish the Management with required information such as proof of Personal Identification, front and back copy of credit card/debit card, Passport, Driver’s License, recent bank statement or other appropriate documentation as DADUKEMBAR, at its sole discretion, deems necessary. Should the Player fail to comply with any security request, DADUKEMBAR reserves the right to deem void any winnings in the Player’s account.</p>
		<p>4.2 Crediting Winnings -- Winnings will be automatically added to the respective Player’s account, as deemed appropriate. These updates of the Players’ accounts are not 'proof of win' and/or may refer to funds that have been transferred (debited) from your respective Account. If upon manual review there is evidence of fraud or malpractice, DADUKEMBAR reserves the right to void certain winnings and to take appropriate action againstany Player’s account accordingly.</p>

	<span>5. Bonus Money</span>
		<p>5.1 Bonus Money refers to the virtual money on products that are given to DADUKEMBAR’s users for promotional purposes. Users cannot withdraw such money right after they have been awarded it. Depending on the type of bonus, different processing rules for bonus monies may apply.</p>
		<p>5.2 Bonus money can be converted to real money and then withdrawn after the user has fulfilled the necessary requirements of the particular promotion.</p>

	<span>6. Promotions</span>
		<p>6.1 All promotions, bonuses or special offers are subjected to these Terms and Conditions and any promotion-specific terms and conditions that DADUKEMBAR.com may introduce from time to time in conjunction therewith. DADUKEMBAR.com reserves the right, to suspend, withdraw or modify such bonuses or promotions and/or the specific terms and conditions governing the same at any time.</p>
		<p>6.2 All offers are restricted to only ONE (1) free open account per individual, family, household address, email address, telephone number, bank account and IP address.</p>
		<p>6.3 Any bets placed on two opposing sides will not be taken into the calculation or count towards any Turnover requirement.</p>
		<p>6.4 In the event that a withdrawal is made prior to the rollover requirement, the bonus and winnings may be forfeited. All drawn or tied bets will not be counted towards the rollover requirement.</p>
		<p>6.5 Unless specifically mentioned otherwise in the terms of a particular promotion, only 25% of total wagers placed on any game of Roulette (all variations) and 50% of total wagers placed on any slots and RNG games will count towards the wagering requirement of any bonus.</p>
		<p>6.6 DADUKEMBAR reserves the right to request sufficient proof of a player’s identity at any given time. uch evidence must legally prove a player’s personal information, payment details and/or any other additional information that may be required to pass relevant security checks.</p>
		<p>6.7 No employee or relative of any employee or representative of DADUKEMBAR may hold a Player account at any given time nor take advantage of any promotional offer on the Website.
		<p>6.8 By participating in any promotion run bythe Website, Players agree to abide by these General Terms & Conditions as well as those listed under each individual promotion. Any Player unwilling to agree to these terms should contact the DADUKEMBAR Support Team for further information.</p>
		<p>6.9 In the event that any terms of the offers or promotions are breached, abused and have not been fulfilled by eligible Player account holders, or if there is any evidence of a series of bets placed by a customer or group of customers, which, due to a deposit bonus or any other promotional offer, resulted in guaranteed customer profits, irrespective of the outcome, whether individually or as part of a group, DADUKEMBAR reserves the right to withhold, cancel or reclaim the bonus as well as all winnings. In addition,to cover administrative costs and processing fees, DADUKEMBAR reserves the right to levy an administration charge on the Player with a value that could reach up to the value of the deposit bonus.</p>

	<span>7. Other Service Provisions</span>
		<p>7.1 The Player shall be solely responsible and agree to pay all fees and/or costs incurred, as well as to report and pay all applicable taxes as required by the applicable governing law while using any services of DADUKEMBAR.	</p>

	<span>8. Miscellaneous</span>
		<p>8.1. In the event that the General Terms and Conditions are translated into another language, the English version will prevail.</p>

	<span>9. Disclaimer</span>
		<p>9.1. All Players shall hold the Management of DADUKEMBAR, its employees, officers, directors, shareholders, licensees, distributors, wholesalers, parents, affiliates, subsidiaries, advertisers, promoters or other agencies, media partners, agents and suppliers harmless and unaccountable immediately on demand and shall fully indemnify any or all of these parties from any and/or all claims, losses, costs, damages, liabilities and expenses whatsoever (including legal fees) that may arise, including those from any third parties.</p>
		<p>9.2. We will not be responsible or liable to any Player for any loss of content or material uploaded or transmitted through the Play.By agreeing to these General Terms & Conditions, the Player confirms that we shall not be liable to the Player or any third party for any modifications to, suspensions of or discontinuance of the Play.</p>
		<p>9.3. DADUKEMBAR pledges to offer both fair and timely dispute resolutions and redress without undue cost or burden, by holding detailed wager and transaction records of all its financial dealings. These records are archived for a period of 5 years (from the date of transaction) and are accessible by the dispute resolution authority upon request.</p>
        </div>


@else
	<h5>TERM AND CONDITION </h5>
        <div class="lol" style="line-height: 20px; text-align: justify;">
	<span>1. Syarat dan Ketentuan Umum</span>
		<p>1.1 Dengan mendaftar untuk menggunakan layanan yang tercakup dalam Syarat dan Ketentuan pada DADUKEMBAR (selanjutnya masing-masing secara terpisah dan bersama-sama secara kolektif disebut sebagai “DADUKEMBAR” atau “DADUKEMBAR.com”), Anda menyatakan bahwa Anda setidaknya berusia 21 tahun, dan bahwa Anda secara hukum mengakui telah membaca, memahami dan setuju untuk terikat oleh dan untuk mematuhi "Syarat dan Ketentuan." Jika Anda tidak ingin menerima syarat dan ketentuan berikut, Anda tidak perlu mendaftar atau membuka rekening dan Anda tidak akan dapat mengakses perangkat lunak dan layanan game yang ditawarkan.</p>
		<p>1.2 Semua informasi produk, pendaftaran di situs dan penggunaan setiap layanan yang ditawarkan oleh DADUKEMBAR dikenakan "Syarat dan Ketentuan Umum" ini.</p>
		<p>1.3 DADUKEMBAR berhak untuk mengubah Syarat dan Ketentuan Umum sewaktu-waktu tanpa pemberitahuan terlebih dahulu. Perubahan tersebut akan diberlakukan setelah di publikasi pada "Syarat dan Ketentuan Umum" bagian dari situs DADUKEMBAR. Ini adalah tanggung jawab pengguna untuk meninjau Syarat dan Ketentuan Umum secara teratur. Terus menggunakan setiap jasa dari Situs setelah perubahan atau pembaruan telah diberlakukan untuk Syarat dan Ketentuan Umum akan merupakan kata pengakuan dan penerimaan perubahan atau memperbarui dari anda.</p>
		<p>1.4 Syarat dan Ketentuan Umum mewakili kesepakatan eksklusif dan akhir antara pengguna dan DADUKEMBAR. “Syarat dan Ketentuan Umum” menggantikan dan menggabungkan semua perjanjian sebelumnya, representasi dan pemahaman antara pengguna dan DADUKEMBAR. Dengan menyetujui Syarat dan Ketentuan Umum, anda mengkonfirmasi bahwa anda menerima semua yang ada di Syarat dan Ketentuan Umum yang kami buat.</p>
		<p>1.5 Jika terjadi pertentangan antara ketentuan dalam perjanjian ini dan ketentuan apapun di situs DADUKEMBAR, sebagaimana yang telah tercantum, perjanjian ini akan berlaku.</p>
		<p>1.6 Kami berhak, atas kebijakan kami sendiri, untuk meminta user untuk memberikan bukti identitas dan usia sebagai prasyarat sebelum pengguna diijinkan untuk berpartisipasi dalam atau memanfaatkan salah satu layanan DADUKEMBAR.</p>

	<span>2. Akun : Syarat Pengguna</span>
		<p>2.1 Jika ada perselisihan mengenai akun Pemain, DADUKEMBAR berhak untuk menangguhkan akun Pemain sampai tercapai sebuah solusi. Mengajukan laporan perselisihan harus dibuat secara tertulis dan jelas mengenai tanggal, waktu dan rincian perselisihan tersebut. Untuk mengajukan laporan, harus dikirim melalui email ke support@DADUKEMBAR.com. Customer service DADUKEMBAR menganggap laporan anggota sangat serius dan akan berusaha untuk mengambil semua langkah yang diperlukan dan wajar untuk menyelidiki dan akhirnya menyelesaikan semua masalah.</p>
		<p>2.2 DADUKEMBAR berhak untuk menangguhkan dan / menghapus akun user untuk alasan apapun dan setiap saat tanpa pemberitahuan sebelumnya. Setiap saldo kredit dalam akun tersebut pada saat suspensi dan / atau penghentian tersebut akan dikembalikan kepada pemilik akun melalui metode pembayaran yang akan ditentukan oleh DADUKEMBAR. DADUKEMBAR juga berhak, atas kebijakannya sendiri, untuk menganggap membatalkan setiap kemenangan dan kehilangan saldo kredit atau kemenangan atau hak dari setiap akun pengguna serta penggunaan setiap pengguna layanan DADUKEMBAR itu.</p>
		<p>2.3 Setiap pengguna bertanggung jawab untuk menjaga akun dan kata sandi nya agar tetap aman. Setiap pengguna DADUKEMBAR adalah, tanpa kecuali, diharapkan benar-benar melarang orang lain atau pihak ketiga, termasuk, tanpa batasan, setiap kecil, menggunakan atau menggunakan kembali akun DADUKEMBAR nya, mengakses dan / atau menggunakan bahan atau informasi dari situs DADUKEMBAR. Pengguna bertanggung jawab untuk setiap pembelian dan / atau kerugian yang timbul sendiri atau dikeluarkan melalui pihak ketiga atau akun DADUKEMBAR nya.</p>

	<span>3. Taruhan</span>
		<p>3.1 Penerimaan dan Validasi Taruhan: 
		Taruhan
		Pemain harus bertaruh dari dana yang telah dikreditkan ke rekening masing-masing pemain. Semua taruhan akan dianggap batal, jika pada tinjauan ditemukan bahwa pemain tidak pernah berhasil mendepositkan dana atau menyelesaikan setiap Transfer Dana ke rekening masing-masing pemain. Setelah berhasil menyetorkan dana ke / akunnya, total taruhan Pemain harus menjadi setara atau melebihi jumlah deposit. Kegagalan untuk mematuhi akan mengakibatkan kegagalan untuk mengajukan penarikan, sehingga disarankan bagi Pemain untuk melakukan deposit.
		Konfirmasi Taruhan
		DADUKEMBAR tidak bisa dan tidak akan membatalkan taruhan dari rekening Pemain sekali mereka telah dikonfirmasi, kecuali taruhan dinyatakan batal atau kecuali ada keadaan yang melibatkan hasil yang salah dan kemungkinan untuk alasan yang diuraikan dalam Syarat dan Ketentuan.
		Catatan Akurat Taruhan
		Meskipun segala upaya untuk memastikan akurasi total, DADUKEMBAR tidak dan tidak akan menerima tanggung jawab atas kesalahan atau kelalaian sehubungan dengan informasi yang diberikan di situs.
		Pengurangan Taruhan/Taruhan Yang Belum Terselesaikan
		Taruhan akan dipotong dari rekening pemain yang bersangkutan pada saat Bet ditempatkan, terlepas dari kapan hasil ditentukan atau diumumkan. DADUKEMBAR tidak dapat dan tidak akan mengembalikan Taruhan yang telah ditempatkan pada permainan di mana taruhan telah dipasang. Taruhan tersebut akan diselesaikan setelah selesainya permainan dan kemenangan tersebut, jika ada, akan dikreditkan ke rekening masing-masing pemain 'pada saat itu.
		Keputusan DADUKEMBAR Dalam Taruhan Adalah Keputusan Akhir
		Dengan menerima Syarat dan Ketentuan Umum, pengguna setuju bahwa DADUKEMBAR dan catatan yang akan menjadi otoritas tertinggi dalam menentukan hal apapun termasuk taruhan ditempatkan serta keadaan di mana taruhan dibuat.
		Tanggung Jawab Pemain Untuk Kemenangan
		Pajak yang berlaku dan biaya yang timbul pada setiap kemenangan yang telah diberikan kepada Anda serta semua pembayaran lain yang dibuat dengan pemain yang bersangkutan dianggap tanggung jawab Pemain. DADUKEMBAR berhak untuk melaporkan dan menahan jumlah berapapun dari kemenangan Pemain untuk mematuhi hukum yang berlaku. Kemenangan tidak dapat ditransfer, diganti atau ditukar dengan bentuk lain dari kemenangan.</p>
		<p>3.2 Stake Wagered: 
		Taruhan Minimun
		Sejumlah taruhan minimum berlaku. Jumlah tersebut tergantung pada ketentuan Permainan / Produk.
		Automatic Pemain / Manipulasi Software / Sabotase / Kecurangan
		Automatic Pemain melalui penggunaan perangkat lunak, pihak ketiga dan segala bentuk manipulasi setiap Games atau akun data Pemain dilarang keras dan dapat mengakibatkan penghentian keanggotaan, penghapusan semua akun yang terkait, pembatalan dan / atau penyitaan hadiah dan deposit apapun yang didapat, serta penuntutan pidana terhadap Pemain yang bersangkutan. Dengan menyetujui Syarat dan Ketentuan Umum, Pemain setuju bahwa DADUKEMBAR memiliki hak dan wewenang untuk mengakhiri akun setiap pemain untuk alasan apapun, dan tanpa pembatasan umum yang telah disebutkan sebelumnya, customer service DADUKEMBAR berkeyakinan semua kecurangan yang dilakukan di website atau setiap game yang ditawarkan didalamnya, merugikan dan menentang asas di mana situs web dioperasikan. Dengan ini pemain menghapus setiap dan semua hal-hal yang dapat menyebabkan terjadinya pemutusan tersebut.
		Dengan menyetujui Syarat dan Ketentuan Umum, Pemain mengakui bahwa kesalahan yang berkaitan dengan perangkat lunak komputer mustahil terjadi. Jika Pemain menyadari perangkat lunak tersebut mengalami kesalahan/error, Pemain terikat secara hukum untuk segera memberitahukan kepada DADUKEMBAR secara tertulis. Jika Pemain mengambil keuntungan dari error/kesalahan tersebut, DADUKEMBAR berhak atas kompensasi penuh untuk semua biaya, termasuk biaya untuk perbaikan perangkat lunak yang mungkin timbul dari kelalaian atau tindakan Pemain dalam mengambil keuntungan dari kesalahan/error tersebut.
		Dengan menyetujui Syarat dan Ketentuan Umum, Pemain setuju bahwa DADUKEMBAR tidak bertanggung jawab atas segala kerusakan, kehilangan, atau cedera akibat dari gangguan, atau akses yang tidak sah atau penggunaan Website atau akun Pemain. Setiap usaha oleh Pemain untuk mendapatkan akses tidak sah ke sistem Website atau akun Pemain, mengganggu prosedur atau kinerja Website atau permainan, atau sengaja merusak atau melemahkan Website atau Permainan ini dikenakan dan / atau penuntutan pidana perdata dan akan mengakibatkan penghentian segera rekening Pemain serta penyitaan setiap dan semua hadiah yang Pemain memiliki dan telah dapatkan.</p>

	<span>4. Kemenangan</span>
		<p>4.1 Sebelum mengeluarkan atau menyetujui penarikan, DADUKEMBAR berhak untuk meminta setiap pemain untuk menghubungi customer service DADUKEMBAR dengan informasi yang diperlukan seperti bukti Identifikasi Pribadi, copy depan dan belakang kartu kredit / kartu debit, Paspor, SIM, laporan bank terbaru atau dokumentasi lain yang sesuai dengan DADUKEMBAR, atas kebijakannya sendiri, dianggap perlu. Jika Pemain gagal memenuhi permintaan keamanan, DADUKEMBAR berhak untuk membatalkan kemenangan Pemain.</p>
		<p>4.2 Kemenangan akan secara otomatis ditambahkan ke akun masing-masing Pemain, yang dianggap sesuai. Update dari akun pemain bukanlah bukti kemenangan 'dan / atau dapat merujuk kepada dana yang telah ditransfer (didebit) dari Rekening Anda masing-masing. Jika pada pemeriksaan manual ada bukti penipuan atau malpraktek, DADUKEMBAR berhak untuk membatalkan kemenangan tertentu dan mengambil tindakan terhadap akun Pemain.</p>

	<span>5. Bonus</span>
		<p>5.1 Bonus Uang mengacu pada uang virtual pada produk yang diberikan kepada pengguna DADUKEMBAR untuk tujuan promosi. Pengguna tidak dapat menarik uang tersebut setelah bonus diberikan. Tergantung pada perarturan yang berlaku pada bonus tersebut.</p>
		<p>5.2 Bonus uang dapat dikonversi ke uang asli dan kemudian ditarik setelah pengguna telah memenuhi persyaratan yang diperlukan dari promosi tertentu.</p>

	<span>6. Promosi</span>
		<p>6.1 Semua promosi, bonus atau penawaran khusus adalah bersubjek dari syarat dan ketentuan berikut ini dan setiap syarat dan ketentuan khusus mengenai promosi yang mungkin diberitahukan oleh DADUKEMBAR.com dalam hubungannya dari waktu ke waktu. DADUKEMBAR.com mempunyai hak untuk menunda, menarik, atau merubah bonus atau promosi dan/atau syarat dan ketentuan khusus yang mengatur hal yang sama kapan saja.</p>
		<p>6.2 Semua promosi dibatasi hanya untuk satu (1) akun per individu, keluarga, alamat rumah, alamat email, nomor telepon, rekening bank dan alamat IP.</p>
		<p>6.3 Setiap taruhan yang ditempatkan pada dua sisi yang berlawanan tidak akan dibawa ke dalam hitungan atau dihitung ke dalam persyaratan Turnover.</p>
		<p>6.4 Jika penarikan dilakukan sebelum persyaratan turnover, bonus dan kemenangan akan dibatalkan. Semua taruhan ditarik atau tidak akan dihitung terhadap persyaratan turnover.
		<p>6.5 Kecuali secara khusus disebutkan sebaliknya dalam hal promosi tertentu, hanya 25% dari total taruhan ditempatkan pada setiap permainan Roulette (semua variasi) dan 50% dari total taruhan ditempatkan pada setiap slot dan permainan RNG akan dihitung terhadap persyaratan Taruhan setiap bonus.</p>
		<p>6.6 DADUKEMBAR berhak untuk meminta bukti identitas pemain pada waktu tertentu. Bukti tersebut harus secara hukum membuktikan informasi pribadi pemain, rincian pembayaran dan / atau informasi tambahan lainnya yang mungkin diperlukan untuk lulus pemeriksaan keamanan yang relevan.</p>
		<p>6.7 Tidak ada karyawan atau kerabat karyawan atau perwakilan untuk DADUKEMBAR dapat memegang rekening pemain pada waktu tertentu atau mengambil keuntungan dari setiap tawaran promosi di situs.</p>
		<p>6.8 Dengan berpartisipasi dalam promosi yang dijalankan DADUKEMBAR.com, Pemain setuju untuk mematuhi Syarat & Ketentuan umum, serta yang tercantum di setiap promosi. Setiap pemain yang menyetujui persyaratan tersebut harus menghubungi Customer Service DADUKEMBAR untuk informasi lebih lanjut.</p>
		<p>6.9 Jika setiap syarat dan ketentuan promosi dilanggar, disalahgunakan dan belum dipenuhi oleh Pemain, atau jika ada bukti dari serangkaian taruhan yang dipasang oleh Pemain atau kelompok yang sama, yang, karena bonus deposit atau promosi lainnya, mengakibatkan keuntungan bagi Pemain, terlepas untuk hasilnya, baik secara individu maupun sebagai bagian untuk kelompok, DADUKEMBAR berhak untuk menahan, membatalkan atau mengambil kembali bonus serta semua kemenangan.</p>

	<span>7. Ketentuan Pelayanan Lain</span>
		<p>7.1 Pemain harus bertanggung jawab dan setuju untuk membayar semua biaya dan / atau biaya yang dikeluarkan, serta melaporkan dan membayar semua pajak yang berlaku seperti yang dipersyaratkan oleh hukum pemerintahan yang berlaku saat menggunakan jasa DADUKEMBAR.</p>
	<span>8. Lain-lain</span>
		<p>8.1. Dalam hal ini bahwa Syarat dan Ketentuan Umum yang diterjemahkan ke dalam bahasa lain, versi bahasa Inggris akan berlaku.</p>

	<span>9. Penolakan</span>
		<p>9.1. Semua Pemain wajib membantu Pengelolah DADUKEMBAR, yang karyawan, petugas, direktur, pemegang saham, pemegang lisensi, distributor, grosir, orang tua, afiliasi, anak perusahaan, pengiklan, promotor atau lembaga lain, media partner, agen dan pemasok jaga dari tindakan yang dapat merugikan dan tidak bertanggung jawab. Akan sepenuhnya mengganti kerugian salah satu atau semua pihak tersebut dari setiap dan / atau semua klaim, kerugian, biaya, kerusakan, kewajiban dan beban apapun (termasuk biaya hukum) yang mungkin Pemain timbulkan, termasuk dari pihak ketiga.</p>
		<p>9.2. Kami tidak akan bertanggung jawab atau berkewajiban untuk setiap pemain atas hilangnya isi atau materi upload yang dikirim melalui permainan menyetujui Syarat & Ketentuan umum ini, Pemain menegaskan bahwa kita tidak bertanggung jawab kepada pemain atau pihak ketiga untuk modifikas, suspensi atau penghentian permainan.</p>
		<p>9.3. DADUKEMBAR berjanji untuk menawarkan resolusi perselisihan yang adil dan tepat waktu dan memperbaiki tanpa biaya yang tidak semestinya atau beban, dengan memegang taruhan dan catatan rincian transaksi semua transaksi keuangan. Catatan ini diarsipkan untuk jangka waktu 5 tahun (dari tanggal transaksi) dan dapat diakses oleh otoritas penyelesaian sengketa atas permintaan.</p>
        </div>


@endif
        <br>
        <div class="clr"></div>
    </div>
</div>
<!--MID SECTION-->
@stop
