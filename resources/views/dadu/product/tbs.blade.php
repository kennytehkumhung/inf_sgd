@extends('../dadu/master')

@section('title', 'Indonesia Online Casino, Gambling Games | DADUKEMBAR')
@section('keywords', 'Indonesia Online Casino, Gambling Games | DADUKEMBAR')
@section('description', 'Enjoy gambling (kasino) games at Indonesia most trusted online casino, DADUKEMBAR. Play live casino, slots, horse racing and more.')

@section('top_css')
<link href="{{url()}}/dadu/resources/css/sp.css" rel="stylesheet" type="text/css" />
<style>
body{
    background-color: #000;
    background-image: url(../dadu/img/tsport-bg.jpg) !important;
    background-repeat: repeat-x;
}
</style>
@stop

@section('content')
<!--CASINO LOBBY SECTION-->
<div class="lcLobby">
    <div class="lcLobbyInner">
    <div class="lcCont">
        <div class="sportsTit">     
            <img src="{{url()}}/dadu/img/tsport-tit.png">
        </div>
    </div> 
    </div>
</div>
<!--CASINO LOBBY SECTION-->

<!--MID SECTION-->
<div class="midSect">
    <div class="midSectInner">
        <iframe id="ContentPlaceHolder1_iframe_game" width="1024px" height="655px" frameborder="0" src="{{$login_url}}"></iframe>
    </div>
</div>
<!--MID SECTION-->
@stop

