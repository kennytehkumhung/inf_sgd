@extends('c59/master')

@section('title', 'Live Casino')

@section('top_js')

 <link href="{{url()}}/c59/resources/css/otherPg.css" rel="stylesheet">
 <script>
 function callUrl($id,$url){
	var str = $url;
	var url = str.replace("{4}", $('#limit_'+$id).val() ); 
	window.open(url, 'maxbet', 'width=1150,height=830');
 }
 </script>
@stop

@section('content')
<div class="midSect bglc">
    <div class="midSectInner">
        <div class="midSectCont">
            <div class="acctContainer" style="padding-left:0px;">
              <!--LOBBY MENU-->
              <div class="lobbyMenu">
                <ul>
                      <li><a href="{{route('mxb', [ 'type' => 'casino' , 'category' => 'baccarat' ] )}}">Bacarrat</a></li>
                  <li><a href="{{route('mxb', [ 'type' => 'casino' , 'category' => 'roulette' ] )}}">Roulette</a></li>
                  <li><a href="{{route('mxb', [ 'type' => 'casino' , 'category' => 'blackjack' ] )}}">Black Jack</a></li>
                  <li><a href="{{route('mxb', [ 'type' => 'casino' , 'category' => 'single_player_poker' ] )}}">Poker</a></li>
                  <li><a href="{{route('mxb', [ 'type' => 'casino' , 'category' => 'carribean_poker' ] )}}">Carribean Poker</a></li>
                  <li><a href="{{route('mxb', [ 'type' => 'casino' , 'category' => 'dragon_tiger' ] )}}">Dragon Tiger</a></li>
                  <li><a href="{{route('mxb', [ 'type' => 'casino' , 'category' => 'sicbo' ] )}}">Sic Bo</a></li>
                </ul>
              </div>
              <!--LOBBY MENU-->

             <!--LOBBY-->
                        <div class="lobby">	
                                <div id="casino_table_wrapper">				
                                        @foreach( $gamelist as $key => $list)		
                                        <div class="tableInfo">			
                                                <div class="roomTitle">{{$list['gameName']}}</div>
                                                        <div class="roomDetails">
                                                        <div class="photo">
                                                                <img width="138" height="182" src="{{$list['image']}}">
                                                        </div>
                                                        <div class="roomInfo">
                                                                Dealer: {{$list['dealerName']}}
                                                                <br>
                                                                <br>
                                                                Currency: THB
                                                                <br>
                                                                <br>

                                                                Table Limit:
                                                                <br>
                                                                <select id="limit_{{$key}}">
                                                                @foreach( $list['limit'] as $url_key => $value)
                                                                        <option value="{{$value['limitSetID']}}">{{$value['minBet']}} ~ {{$value['maxBet']}}</option>							
                                                                @endforeach
                                                                </select>
                                                                <br>
                                                                <a class="login_btn" href="#" onClick="callUrl('{{$key}}','{{$list['url']}}')">Play Now</a>
                                                        </div>
                                                        </div>						
                                                </div>								
                                        @endforeach			
                                </div>				
                        </div>	
                        <!--LOBBY-->   

            </div>
@stop