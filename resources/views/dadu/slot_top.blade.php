<!--CASINO LOBBY SECTION-->
<div class="lcLobby">
    <div class="lcLobbyInner3">
        @if(in_array('W88',Session::get('valid_product')))    
        <div class="gpSlot">
            <a href="javascript:void(0)" onClick="change_iframe('{{route('w88' , [ 'type' => 'slot' , 'category' => 'Arcades' ] )}}','','w88')" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('imggp','','{{url()}}/dadu/img/slot-thumb-1-hover.png',0)">
                @if( $name == 'w88' )
                    <img src="{{url()}}/dadu/img/slot-thumb-1-hover.png" name="imggp" width="230" height="295" border="0">
                @else
                    <img src="{{url()}}/dadu/img/slot-thumb-1.png" name="imggp" width="230" height="295" border="0">
                @endif    
            </a>
        </div>
        @endif
        @if(in_array('PLTB',Session::get('valid_product')))
        <div class="ptSlot">
            <a href="javascript:void(0)" onClick="change_iframe('{{route('pltb' , [ 'type' => 'pgames' , 'category' => '1' ] )}}','','pltb')" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('imgpt','','{{url()}}/dadu/img/slot-thumb-2-hover.png',0)">
                @if( $name == 'pltb' )
                    <img src="{{url()}}/dadu/img/slot-thumb-2-hover.png" name="imggp" width="230" height="295" border="0">
                @else
                    <img src="{{url()}}/dadu/img/slot-thumb-2.png" name="imgpt" width="230" height="295" border="0">
                @endif
            </a>
        </div>
        @endif
    </div>
</div>
<!--CASINO LOBBY SECTION-->