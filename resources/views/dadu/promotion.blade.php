@extends('dadu/master')

@section('title', 'Indonesia Online Casino, Gambling Games | DADUKEMBAR')
@section('keywords', 'Indonesia Online Casino, Gambling Games | DADUKEMBAR')
@section('description', 'Enjoy gambling (kasino) games at Indonesia most trusted online casino, DADUKEMBAR. Play live casino, slots, horse racing and more.')

@section('top_css')
<link href="{{url()}}/dadu/resources/css/promo.css" rel="stylesheet" type="text/css" />
@stop

@section('content')
<!--CASINO LOBBY SECTION-->
<div class="lcLobby">
    <div class="lcLobbyInner3">
        <img src="{{url()}}/dadu/img/promo-title.png" width="706" height="60">
        <div class="floatLeftPromo">
            <img src="{{url()}}/dadu/img/fltLeft.png" width="300" height="242">
        </div>
        <div class="floatRightPromo">
            <img src="{{url()}}/dadu/img/fltRight.png" width="300" height="242">
        </div>
    </div>
</div>
<!--CASINO LOBBY SECTION-->

<!--MID SECTION-->
<div class="midSect2">
    <div class="midSectInner">
        <div id="promo">
            <div id="accordion">
                @foreach ($promo as $key => $value )
                <h4 class="accordion-toggle">
                    <img src="{{$value['image']}}">
                </h4>
                <div class="accordion-content default">
                    <p>
                        <?php echo htmlspecialchars_decode($value['content']); ?>
                    </p>
                </div> 
                @endforeach
            </div>
        </div>
    </div>
</div>
<!--MID SECTION-->
@stop

@section('bottom_js')
<script type="text/javascript">
        $(document).ready(function($) {
                $('#accordion').find('.accordion-toggle').click(function(){

                        //Expand or collapse this panel
                        $(this).next().slideToggle('fast');

                        //Hide the other panels
                        $(".accordion-content").not($(this).next()).slideUp('fast');

                });
        });
</script>
@stop