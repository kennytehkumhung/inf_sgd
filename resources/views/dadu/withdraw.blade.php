@extends('dadu/master')

@section('title', 'Withdraw')


@section('top_js')
<script>

	function submit_transfer(){
	$.ajax({
			 type: "POST",
			 url: "{{route('withdraw-process')}}?"+$('#withdraw_form').serialize(),
			 data: {
				_token: 		 "{{ csrf_token() }}",
			 },
			 beforeSend: function(){
				
			 },
			 success: function(json){
					obj = JSON.parse(json);
					 var str = '';
					 $.each(obj, function(i, item) {
						
						if( '{{Lang::get('COMMON.SUCESSFUL')}}' == item ){
							alert('{{Lang::get('COMMON.SUCESSFUL')}}');
							window.location.href = "{{route('transaction')}}";
						}else{
							str += item + '<br>';
						}
					})
					$('.failed_message').html(str);
				
			}
		})
	} 
        
        function load_bank(id,type){

	

		if( type == 'default' ){

			var id = id;

		}else{

			var id = id.value;

		}

		

		$.ajax({

			type: "POST",

			url: "{{route('loadbank')}}",

			data: {

				id:  id

			},

		}).done(function( result ) {

			  $('#accountno').val(result);

			  $('#accountnopass').val(result);

		});

	}

	@foreach( $banklists as $bank => $detail  )

	load_bank('{{$detail['code']}}','default');

    <?php break; ?>

	@endforeach
</script>
@stop

@section('content')

@include('dadu/transaction_mange_top', [ 'title' => Lang::get('public.Withdrawal') ] )
<div class="midSect">
    <div class="midSectInneracct">
        <!--ACCOUNT MANAGAMENT MENU-->
        <div class="inlineAccMenu">
            <ul>
                <li><a href="{{route('deposit')}}">{{Lang::get('public.Deposit')}}</a></li>
                <li><a href="{{route('withdraw')}}">{{Lang::get('public.Withdrawal')}}</a></li>
                <li><a href="{{route('transaction')}}">{{Lang::get('public.TransactionHistory')}}</a></li>
                <li><a href="{{route('transfer')}}">{{Lang::get('public.Transfer')}}</a></li>
            </ul>
        </div>
        <div class="title_bar">
            <span>{{Lang::get('public.Withdrawal')}}</span>
        </div>
        <!--ACCOUNT TITLE-->
       <!--ACCOUNT CONTENT-->
        <div class="acctContent">
            <span class="wallet_title"><i class="fa fa-money"></i>{{Lang::get('public.Withdrawal')}}</span>
            <div class="acctRow">
                <label>{{Lang::get('public.Option')}} :</label><span class="acctText">{{Lang::get('public.BankTransfer')}}</span>
                <div class="clr"></div>
            </div>
            <div class="acctRow">
                <label>{{Lang::get('public.MinMaxLimit')}} :</label><span class="acctText">{{$min}} / {{$max}}</span>
                <div class="clr"></div>
            </div>
            <div class="acctRow">
                <label>{{Lang::get('public.ProcessingTime')}} :</label><span class="acctText">15-30 Mins</span>
                <div class="clr"></div>
            </div>
            <i class="fa fa-exclamation"></i><span style="font-size:13px;color:#FFD200;">{{ Lang::choice('public.AllMembersAreAllowedToWithdrawXTime', 1, array('p1' => $withdrawlimit)) }}</span>
            <hr style="width: 98%; float: left;">
            <div class="clr"></div>
            <br>
            <form id="withdraw_form">
                <div class="acctRow">
                    <label>{{Lang::get('public.Balance')}} (IDR) :</label><span class="acctText">{{App\Http\Controllers\User\WalletController::mainwallet()}}</span>
                    <div class="clr"></div>
                </div>
                <div class="acctRow">
                    <label>{{Lang::get('public.Amount')}} (IDR) * :</label><input type="text" name="amount"/>
                    <div class="clr"></div>
                </div>
                <div class="acctRow">
                    <label>{{Lang::get('public.BankName')}} * :</label>
                    <select name="bank" onChange="load_bank(this,'none')">
						@foreach( $banklists as $bank => $detail  )
						<option value="{{$detail['code']}}">{{$bank}}</option>
						@endforeach
					</select>
                    <div class="clr"></div>
                </div>
                <div class="acctRow">
                    <label>{{Lang::get('public.FullName')}} * :</label><span class="acctText">{{Session::get('fullname')}}</span>
                    <div class="clr"></div>
                </div>
                <div class="acctRow">
                    <label>{{Lang::get('public.BankAccountNo')}} * :</label><input type="text" id="accountno"  style="color:black" disabled />
                    <input type="hidden" id="accountnopass" name="accountno"  />
                    <div class="clr"></div>
                </div>
                <span style="font-size:13px;color:#FFD200;"><i class="fa fa-exclamation-circle"></i>{{Lang::get('public.RequiredFields')}}</span>
                <br><br>
                <div class="submitAcct">
                    <a href="javascript:void(0)" onClick="submit_transfer()">{{Lang::get('public.Submit')}}</a>
                </div>
                <span class="failed_message acctTextReg" style="display:block;float:left;height:100%;"></span>
            </form>
        </div>
    </div>
</div>
@stop

@section('bottom_js')
<script src="{{url()}}/front/resources/js/slick.min.js"></script>
<script>
$('.caro').slick({
  infinite: true,
  slidesToShow: 2,
  slidesToScroll: 2,
  autoplay: true,
  variableWidth: true,
  arrows: false
});
</script>
@stop