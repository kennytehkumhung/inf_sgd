<html>
<head>
<link href="{{url()}}/dadu/resources/css/slot.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <div class="midSect2">
        <div class="midSectInner">
            <div class="slotContainerBottom">
                <!--Slot Menu-->
                <div class="slotMenuHolder">
                    <div class="slot_menu">
                        <ul>
                            <li class="tab"><a href="{{route('pltb', [ 'type' => 'pgames' , 'category' => '1' ] )}}">Progressive Games</a></li>
                            <li class="tab"><a href="{{route('pltb', [ 'type' => 'newgames' , 'category' => '1' ] )}}">New Games</a></li>
                            <li class="tab"><a href="{{route('pltb', [ 'type' => 'brand' , 'category' => '1' ] )}}">Branded Games</a></li>
                            <li class="tab"><a href="{{route('pltb', [ 'type' => 'slot' , 'category' => 'slot' ] )}}">Slot</a></li>
                            <li class="tab"><a href="{{route('pltb', [ 'type' => 'slot' , 'category' => 'videopoker' ] )}}">Video Poker</a></li>
                            <li class="tab"><a href="{{route('pltb', [ 'type' => 'slot' , 'category' => 'arcade' ] )}}">Arcade</a></li>
                            <li class="tab"><a href="{{route('pltb', [ 'type' => 'slot' , 'category' => 'tablecards' ] )}}">Tablecards</a></li>
                            <li class="tab"><a href="{{route('pltb', [ 'type' => 'slot' , 'category' => 'scratchcards' ] )}}">Scratchcards</a></li>
                        </ul>
                    </div>
                </div>
                <!--Slot Menu-->
                <!--Slot Lobby-->
                <div class="slot_lobby">
                    @foreach( $lists as $list )
                    <div class="slot_box">
                        <span>{{$list->gameName}}</span>
                        <a href="javascript:void(0)" onclick="@if (Auth::user()->check())window.open('{{route('pltbslotiframe' , [ 'gamecode' => $list['code'] ] )}}', 'plt_slot', 'width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif ">
                            <img src="{{url()}}/dadu/img/plt/{{$list->code}}.jpg" width="150" height="150" alt=""/>
                        </a>    
                    </div>
                    @endforeach
                </div>    
                <!--Slot Lobby-->
             </div>
        </div>
    </div>
</body>
</html>