<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Online Casino Indonesia &amp; Live Betting - DADUKEMBAR</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="shortcut icon" href="http://dadukembar.com/img/favicon.ico" type="image/icon">
<link rel="icon" href="http://dadukembar.com/img/favicon.ico" type="image/icon">
<link href="{{url()}}/dadu/home/style.css" rel="stylesheet" type="text/css">
<link href="{{url()}}/dadu/home/reset.css" rel="stylesheet" type="text/css">
<link href='http://fonts.googleapis.com/css?family=Play:400,700' rel='stylesheet' type='text/css'>
<script src="{{url()}}/dadu/home/jquery-2.1.3.min.js"></script>    

<script>
$(document).ready(function(){
    $('.showbox').hover(function() {
        $(".showbox").addClass('transition');
    
    }, function() {
        $(".showbox").removeClass('transition');
    });
});

</script>
</head>

<body>
<!--CONTAINER BG-->
    <!--HEADER-->
    <div class="header">
        <div class="headerInner">
            <div class="logo">
                <img src="{{url()}}/dadu/home/logo.png" alt=""/>
            </div>

            <div class="txtBar">
                <a href="{{route('homepage')}}"><div class="showbox1"></div></a>
                <a href="{{route('register')}}"><div class="showbox2"></div></a>
                <a href="{{route('promotion')}}"><div class="showbox3"></div></a>
            </div>
        </div>
    </div>
    <!--HEADER-->

    <!--MID SECTION-->
    <div class="midSect">
       <div class="midSectInner">
        <div class="ladies">
            <a href="{{route('promotion')}}"><img src="{{url()}}/dadu/home/ladies.png"></a>
        </div>
       </div>
    </div>
    <!--MID SECTION-->
    <div class="footerOuter ft1">
        <div class="footer">
            <div class="ft02">
                <img src="{{url()}}/dadu/home/footer-sponsor.jpg" width="1024" height="70">
            </div>
            <ul class="footerL">
                <li><a href="{{route('aboutus')}}">About Us</a></li>
                <li><a href="{{route('faq')}}">FAQ</a></li>
                <li style="padding-right: 0px !important;"><a href="{{route('tnc')}}">Terms & Conditions</a></li>
            </ul>
            <div class="footerR">©2016 DADUKEMBAR. All rights reserved | 18+</div>
            <div class="clr"></div>
        </div>
    </div>
   <!--FOOTER-->
<!--CONTAINER BG-->   
</body>
</html>