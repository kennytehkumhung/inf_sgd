<script>
 $(document).ready(function() { 
getBalance(true);
});
</script>
<!--SLIDER-->
<div class="slider">
    <div class="sliderInner8">
        <div class="walletTop">
            <div class="wallet1">
                <div class="main-set-title">Main Wallet</div>
                <div class="main-setPrice">{{App\Http\Controllers\User\WalletController::mainwallet()}}</div>
            </div>
            @foreach( Session::get('products_obj') as $prdid => $object)
                <div class="wallet{{$prdid+1}}">
                    <div class="main-set-title">{{$object->name}}</div><div class="main-setPrice {{$object->code}}_balance">0.00</div>
                </div>
            @endforeach
        </div>
    </div>
</div>
<!--SLIDER-->