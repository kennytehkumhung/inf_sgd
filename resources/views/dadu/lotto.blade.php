@extends('c59/master')

@section('title', 'Thailand Lotto Result | Club5599')
@section('keywords', 'Thailand Lotto | Club5599')
@section('description', 'Play Thailand lottery and check results.')

@section('content')
<!--4d-->
<div class="midSect bglc">
    <div class="midSectInner">
        <div class="midSectCont">
            <div class="dTit">{{Lang::get('public.Lottery')}}</div>
            
            <div class="dCont">
                <div class="dMtit">{{$date}}</div>
                
                <div class="dMtit2 d1">{{Lang::get('public.6top')}}</div>
                
                <div class="dMtit3">
                    <input type="text" value='{{$sixtop[0]}}' disabled>
                    <input type="text" value='{{$sixtop[1]}}' disabled>
                    <input type="text" value='{{$sixtop[2]}}' disabled>
                    <input type="text" value='{{$sixtop[3]}}' disabled>
                    <input type="text" value='{{$sixtop[4]}}' disabled>
                    <input class="noM" type="text" value='{{$sixtop[5]}}' disabled>
                    <div class="clr"></div>
                </div>
                
                <div class="dMtit2 d1">{{Lang::get('public.3bottom')}}</div>
                
                <div class="dMtit4">
                    <input type="text" value='{{$threebottom[0]}}' disabled>
                    <input type="text" value='{{$threebottom[1]}}' disabled>
                    <input class="noM1" type="text" value='{{$threebottom[2]}}' disabled>
                    <input type="text" value='{{$threebottom[3]}}' disabled>
                    <input type="text" value='{{$threebottom[4]}}' disabled>
                    <input class="noM1" type="text" value='{{$threebottom[5]}}' disabled>
                    <input type="text" value='{{$threebottom[6]}}' disabled>
                    <input type="text" value='{{$threebottom[7]}}' disabled>
                    <input class="noM1" type="text" value='{{$threebottom[8]}}' disabled>
                    <input type="text" value='{{$threebottom[9]}}' disabled>
                    <input type="text" value='{{$threebottom[10]}}' disabled>
                    <input class="noM" type="text" value='{{$threebottom[11]}}' disabled>
                    <div class="clr"></div>
                </div>
                
                <div class="dMtit2 d1">{{Lang::get('public.2bottom')}}</div>
                <div class="dMtit5">
                    <input type="text" value='{{$twobottom[0]}}' disabled>
                    <input class="noM" type="text" value='{{$twobottom[1]}}' disabled>
                    <div class="clr"></div>
                </div>

                <div class="dSubmit">
                    <a onClick="@if (Auth::user()->check())window.open('{{route('clolotto')}}', 'pubContent', 'width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif" >{{Lang::get('public.BetNow')}}</a>
                </div>
            </div>
<!--4d-->
@stop
