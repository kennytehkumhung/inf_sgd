@extends('dadu/master')

@section('title', 'Indonesia Online Casino, Gambling Games | DADUKEMBAR')
@section('keywords', 'Indonesia Online Casino, Gambling Games | DADUKEMBAR')
@section('description', 'Enjoy gambling (kasino) games at Indonesia most trusted online casino, DADUKEMBAR. Play live casino, slots, horse racing and more.')

@section('top_css')
<link href="{{url()}}/dadu/resources/css/slot.css" rel="stylesheet" type="text/css" />
<style>
body{
    background-color: #000;
    background-image: url(../dadu/img/slot-bg.jpg) !important;
    background-repeat: repeat-x;
}
</style>
@stop

@section('top_js')
<script>
$(document).ready(function() { 
       @if( $type == 'pltb' )
               $('.slot_plt_image').hide();		
       @elseif( $type == 'w88' )
               $('.slot_w88_image').hide();
       @endif
});
function open_game(gameid){
}
function resizeIframe(obj){
  obj.style.height = 0;
  obj.style.height = obj.contentWindow.document.body.scrollHeight + 'px';
}

function change_iframe(url,image_link,product){
       $('#iframe_game').attr('src',url);
       $('.top').show();
       $('.slot_'+product+'_image').hide();
}
 
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
</script>
@stop

@section('content')
@include('dadu/slot_top')
<!--MID SECTION-->
<div class="midSect2">
    <div class="midSectInner">
        <div class="slotContainerBottom">
            <iframe id="iframe_game" frameborder="0" style="overflow:hidden;overflow-x:hidden;overflow-y:hidden;height: 970px; width:1053px; margin-left: -11px;" src="
            @if( $type == 'w88' )
                    {{route('w88', [ 'type' => 'slot' , 'category' => 'Arcades' ] )}}
            @elseif( $type == 'plt' || $type == 'pltb' )
                    {{route('pltb' , [ 'type' => 'pgames' , 'category' => '1' ] )}}
            @endif
            "></iframe>
        </div>
    </div>
</div>
<!--MID SECTION-->
@stop