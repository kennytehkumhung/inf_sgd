<html>
<head>
<link href="{{url()}}/c59/resources/css/slot.css" rel="stylesheet">
</head>
<body>
    <div class="slotTab">
        <div class="tab-container">
            <ul class="etabs">
                <li class="tab"><a href="{{route('mxb', [ 'type' => 'slot' , 'category' => 'Slots' ] )}}">Slots</a></li>
                <li class="tab"><a href="{{route('mxb', [ 'type' => 'slot' , 'category' => 'Table' ] )}}">Table</a></li>
                <li class="tab"><a href="{{route('mxb', [ 'type' => 'slot' , 'category' => 'Soft Games' ] )}}">Mini</a></li>
                <li class="tab"><a href="{{route('mxb', [ 'type' => 'slot' , 'category' => 'Video Poker' ] )}}">Video Poker</a></li>
            </ul>
        </div>
    </div>
    <div id="slot_lobby">
            @foreach( $lists as $list )
            <div class="slot_box">
                    <span>{{$list['gameName']}}</span>
                    <a href="javascript:void(0)" onclick="@if (Auth::user()->check())window.open('{{route('mxb-slot' , [ 'gameid' => $list['gameID'] ] )}}', 'mxb_slot', 'width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif" >
                            <img src="{{$list['imageUrl']}}" width="150" height="150" alt=""/>
                    </a>
            </div>			 
            @endforeach
      <div class="clr"></div>
    </div>
    <div class="clr"></div>
<!--Slot-->
</body>
</html>