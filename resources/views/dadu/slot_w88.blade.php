<html>
<head>
<link href="{{url()}}/dadu/resources/css/slot.css" rel="stylesheet">
</head>
<body>
    <div class="midSect2">
        <div class="midSectInner">
            <div class="slotContainerBottom">
                <!--Slot Menu-->
                <div class="slotMenuHolder">
                    <div class="slot_menu">
                        <ul>
                            <li class="tab"><a href="{{route('w88', [ 'type' => 'slot' , 'category' => 'Arcades' ] )}}">Arcades</a></li>
                            <li class="tab"><a href="{{route('w88', [ 'type' => 'slot' , 'category' => 'Card Games' ] )}}">Card Games</a></li>
                            <li class="tab"><a href="{{route('w88', [ 'type' => 'slot' , 'category' => 'Slots' ] )}}">Slots</a></li>
                            <li class="tab"><a href="{{route('w88', [ 'type' => 'slot' , 'category' => 'Table Games' ] )}}">Table Games</a></li>
                            <li class="tab"><a href="{{route('w88', [ 'type' => 'slot' , 'category' => 'Video Poker' ] )}}">Video Poker</a></li>
                        </ul>
                    </div>
                </div>
                <!--Slot Menu-->
                <!--Slot Lobby-->
                <div class="slot_lobby">
                    @foreach( $lists as $list )
                    <div class="slot_box">
                        <span>{{$list->gamename_en}}</span>
                        <a href="javascript:void(0)" onclick="@if (Auth::user()->check())window.open('http://casino.gpiops.com/mini/?op=BAMAKO04&game_code={{$list->code}}&language=en&playmode=real&ticket={{$token}}','w88_slot','width=1000,height=750')@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif">
                                <img src="{{url()}}/dadu/img/w88/{{$list->image}}" width="150" height="150" alt=""/>
                        </a>    
                    </div>
                    @endforeach
                </div>    
                <!--Slot Lobby-->
             </div>
        </div>
    </div>
</body>
</html>