@extends('dadu/master')

@section('title', 'Transaction Enquiry')

@section('top_js')
  <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
@stop

@section('content')

@include('dadu/transaction_mange_top', [ 'title' => Lang::get('public.TransactionHistory') ] )
<div class="midSect">
    <div class="midSectInneracct">
        <!--ACCOUNT MANAGAMENT MENU-->
        <div class="inlineAccMenu">
            <ul>
                <li><a href="{{route('deposit')}}">{{Lang::get('public.Deposit')}}</a></li>
                <li><a href="{{route('withdraw')}}">{{Lang::get('public.Withdrawal')}}</a></li>
                <li><a href="{{route('transaction')}}">{{Lang::get('public.TransactionHistory')}}</a></li>
                <li><a href="{{route('transfer')}}">{{Lang::get('public.Transfer')}}</a></li>
            </ul>
        </div>
        <div class="title_bar">
        <span>{{Lang::get('public.TransactionHistory')}}</span>
        </div>
        <!--ACCOUNT TITLE-->
        <!--ACCOUNT CONTENT-->
        <div class="acctContent">
            <span class="wallet_title"><i class="fa fa-file-o"></i>{{Lang::get('public.TransactionHistory')}}</span>
            <div class="acctRow">
                <label>{{Lang::get('public.DateFrom')}} :</label><div class="cstInput1"><input type="text" class="datepicker" id="date_from" style="cursor:pointer;" value="{{date('Y-m-d')}}">-<input type="text" class="datepicker" id="date_to" style="cursor:pointer;" value="{{date('Y-m-d')}}"></div>
                <div class="clr"></div>
            </div>
            <div class="acctRow">
                <label>{{Lang::get('public.RecordType')}}:</label>
                <select id="record_type">
                    <option value="0" selected>{{Lang::get('public.CreditAndDebitRecords')}}</option>
                    <option value="1">{{Lang::get('public.CreditRecords')}}</option>
                    <option value="2">{{Lang::get('public.DebitRecords')}}</option>
                </select>
                <div class="clr"></div>
            </div>
            <div class="acctRowR">
                <div class="submitAcct">
                    <a id="trans_history_button" href="#" onClick="transaction_history()"> {{Lang::get('public.Submit')}}</a>
                    <br><br>
                </div>
            </div>
            <div>
                <table border='1' width='98%' id="trans_history" style="color:white;">
                  <tr>
                    <th>{{Lang::get('public.ReferenceNo')}}</th>
                    <th>{{Lang::get('public.DateOrTime')}}</th>
                    <th>{{Lang::get('public.Type')}}</th>
                    <th>{{Lang::get('public.Amount')}} </th>
                    <th>{{Lang::get('public.Status')}}</th>
                    <th>{{Lang::get('public.reason')}}</th>
                </tr>
                <tr>
                    <td colspan="6"><h2><center>{{Lang::get('public.NoRecord')}}</center></h2></td>
                </tr>
            </table>          
            </div>
            <br>
            <br>
        </div>
    </div>
</div>
@stop

@section('bottom_js')
<script>
transaction_history();

$(function() {
	$( ".datepicker" ).datepicker({ dateFormat: 'yy-mm-dd'  , defaultDate: new Date() });
});
  
function transaction_history(){
$.ajax({
	type: "POST",
	url: "{{action('User\TransactionController@transactionProcess')}}",
	data: {
		_token:   "{{ csrf_token() }}",
		date_from:		$('#date_from').val(),
		date_to:    	$('#date_to').val(),
		record_type:    $('#record_type').val()

	},
}).done(function( json ) {

			//var str;
			var str = '';
			str += '	<tr><th>Reference No.</th><th>Date/Time</th><th>Type</th><th>Amount</th><th>Status</th><th>Reason</th></tr>';
			
			obj = JSON.parse(json);
                            $.each(obj, function(i, item) {
                                str +=  '<tr><td>'+item.id+'</td><td>'+item.created+'</td><td>'+item.type+'</td><td>'+item.amount+'</td><td>'+item.status+'</td><td>'+item.rejreason+'</td></tr>';
                            })
			//alert(json);
			$('#trans_history').html(str);
			
	});
}

setInterval(updateTrans, 10000);
setInterval(update_mainwallet, 10000);

function updateTrans(){
	$( "#trans_history_button" ).trigger( "click" );
}

</script>
@stop