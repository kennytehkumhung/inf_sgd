@extends('dadu/master')

@section('title', 'Indonesia Online Casino, Gambling Games | DADUKEMBAR')
@section('keywords', 'Indonesia Online Casino, Gambling Games | DADUKEMBAR')
@section('description', 'Enjoy gambling (kasino) games at Indonesia most trusted online casino, DADUKEMBAR. Play live casino, slots, horse racing and more.')

@section('top_css')
<link href="{{url()}}/dadu/resources/css/promo.css" rel="stylesheet" type="text/css" />
@stop

@section('content')
<!--CASINO LOBBY SECTION-->
<div class="lcLobby">
    <div class="lcLobbyInner3">
        @if( Lang::getLocale() == 'en' )
        <img src="{{url()}}/dadu/img/contact-title.png" width="706" height="60">
        @else
        <img src="{{url()}}/dadu/img/hubungkami.png" width="706" height="60">
        @endif
    </div>
</div>
<!--CASINO LOBBY SECTION-->

<!--MID SECTION-->
<div class="midSect2">
    <div class="midSectInner">
        <div class="textct1">
            @if( Lang::getLocale() == 'en' )
            Our Customer Service Officers are available 24 hours a day, <br>
            7 days a week to serve you better.
            @else
            Costumer Service DADUKEMBAR tersedia setiap hari 24 jam <br>
            untuk melayani pelanggan lebih baik.
            @endif
        </div>

        <div class="ct-girl">
            <img src="{{url()}}/dadu/img/ct-girl.png" width="216" height="390">
        </div>

        <div class="clr"></div>

        <div class="textct1a">
            <table width="auto" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td align="center" valign="middle"><img src="{{url()}}/dadu/img/whatsapp-ct.png" width="46" height="46"></td>
                    <td valign="middle"><span class="valg">+855979548236</span></td>
                </tr>
                <tr>
                    <td align="center" valign="middle"><img src="{{url()}}/dadu/img/BBM-ct.png" width="46" height="46"></td>
                    <td><span class="valg">D3D696E5</span></td>
                </tr>
                <tr>
                    <td align="center" valign="middle"><img src="{{url()}}/dadu/img/wechat-ct.png" width="46" height="46"><img src="{{url()}}/dadu/img/LINE-ct.png" width="46" height="46"></td>
                    <td><span class="valg">daduonline</span></td>
                </tr>
                <tr>
                    <td align="center" valign="middle"><img src="{{url()}}/dadu/img/email-ct.png" width="46" height="46"></td>
                    <td><span class="valg">cs.daduonline@yahoo.com</span></td>
                </tr>
            </table>
        </div>
        <div class="clr"></div>
    </div>
</div>
<!--MID SECTION-->
@stop
