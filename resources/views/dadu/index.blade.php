@extends('dadu/master')

@section('title', 'Indonesia Online Casino, Gambling Games | DADUKEMBAR')
@section('keywords', 'Indonesia Online Casino, Gambling Games | DADUKEMBAR')
@section('description', 'Enjoy gambling (kasino) games at Indonesia most trusted online casino, DADUKEMBAR. Play live casino, slots, horse racing and more.')


@section('top_js')
<script type="text/javascript">
    $(document).ready(function() {
        $('.bxslider').bxSlider({
            auto: true,
            autoControls: false
        });
    });
</script>

<script>
          $(document).ready(function () {
              fakeJackport();
              setInterval(updateJackport, 1000);
          });

          function fakeJackport() {

              var value = Math.floor(Math.random() * 100);
              value = 1356289 + value * 5.496;


              var value2 = value.toFixed(2);

              $('#counter').text(addCommas(value2));

          }

          function updateJackport() {
              var val = $("span[id^='counter']").text();
              val = removeComma(val);

              if (val > 1468889)
                  val = 1356289;

              var value = Math.floor((Math.random() * 1000)+100);
              value = value * 1;
              
              var value2 = (parseFloat(val) + value).toFixed(2);

              $('#counter').text(addCommas(value2));
          }

          function addCommas(str) {
              var parts = (str + "").split("."),
          main = parts[0],
          len = main.length,
          output = "",
          i = len - 1;

              while (i >= 0) {
                  output = main.charAt(i) + output;
                  if ((len - i) % 3 === 0 && i > 0) {
                      output = "," + output;
                  }
                  --i;
              }
              // put decimal part back
              if (parts.length > 1) {
                  output += "." + parts[1];
              }
              return output;
          }

          function removeComma(str) {
              var parts = (str + "").split(",");

              var output = "";

              for (var i = 0; i < parts.length; i++) {
                  output += parts[i];
              }
              return output;
          }
</script>
@stop

@section('content')
<!--SLIDER-->
<div class="slider">
    <div class="sliderInner">
        <ul class="bxslider">
            @foreach( $banners as $key => $banner )
		<li><img src="{{$banner->domain}}/{{$banner->path}}"/></li>	
            @endforeach
        </ul>
        <div class="imgLeft"><img src="{{url()}}/dadu/img/torres-left.png" width="492" height="675" alt=""/></div>
        <div class="imgRight"><img src="{{url()}}/dadu/img/messi-right.png" width="784" height="675" alt=""/></div>
        <div class="clr"></div>
    </div>
</div>
<!--SLIDER-->

<!--MID SECTION-->
<div class="midSect">
    <div class="midSectInner">
        <!--ADD THIS FOR JACKPOT COUNTER-->
        <div class="jpCont">
            <div class="digitCont">
                <!--CODING FOR RUNNING COUNTER HERE-->
                <div class="digitCont">
                    <span id="counter">1,457,097.07</span>
                </div>
            </div>
            <div class="clr"></div>
        </div>
        <!--ADD THIS FOR JACKPOT COUNTER-->
        <div class="thumbContainer">
            <a href="{{route('livecasino')}}"><img title="Live Casino" src="{{url()}}/dadu/img/banner/thumb-1.png" onmouseover="this.src='{{url()}}/dadu/img/banner/thumb-1-hover.png'" onmouseout="this.src='{{url()}}/dadu/img/banner/thumb-1.png'" /></a>
            <a href="{{route('sport')}}"><img title="Sportsbook" src="{{url()}}/dadu/img/banner/thumb-2.png" onmouseover="this.src='{{url()}}/dadu/img/banner/thumb-2-hover.png'" onmouseout="this.src='{{url()}}/dadu/img/banner/thumb-2.png'" /></a>
            <a href="{{route('slot',['type' => 'w88'])}}"><img title="Slot" src="{{url()}}/dadu/img/banner/thumb-3.png" onmouseover="this.src='{{url()}}/dadu/img/banner/thumb-3-hover.png'" onmouseout="this.src='{{url()}}/dadu/img/banner/thumb-3.png'" /></a>
            <a href="4d.html"><img class="noSpace" title="4D" src="{{url()}}/dadu/img/banner/thumb-4.png" onmouseover="this.src='{{url()}}/dadu/img/banner/thumb-4-hover.png'" onmouseout="this.src='{{url()}}/dadu/img/banner/thumb-4.png'" /></a>
        </div>
    </div>
</div>
<!--MID SECTION-->
@stop

@section('bottom_js')
<script>
	$(document).ready(function(){
		activeItem = $("#accordion li:first");
		$(activeItem).addClass('active');
		$("#accordion li").hover(function(){
		$(activeItem).animate({width: "110px"}, {duration:300, queue:false});
		$(this).animate({width: "260px"}, {duration:300, queue:false});
		activeItem = this;
		});
	});
        
        $('.bxslider').bxSlider({
            auto: true,
            autoControls: true
        });
</script>

@stop