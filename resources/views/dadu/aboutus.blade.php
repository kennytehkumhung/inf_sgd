@extends('dadu/master')

@section('title', 'Indonesia Online Casino, Gambling Games | DADUKEMBAR')
@section('keywords', 'Indonesia Online Casino, Gambling Games | DADUKEMBAR')
@section('description', 'Enjoy gambling (kasino) games at Indonesia most trusted online casino, DADUKEMBAR. Play live casino, slots, horse racing and more.')

@section('top_css')
<link href="{{url()}}/dadu/resources/css/promo.css" rel="stylesheet" type="text/css" />
@stop

@section('content')
<!--CASINO LOBBY SECTION-->
<div class="lcLobby">
    <div class="lcLobbyInner3">
        <img src="{{url()}}/dadu/img/aboutus.png" width="706" height="60">
    </div>
</div>
<!--CASINO LOBBY SECTION-->

<!--MID SECTION-->
<div class="midSect2">
    <div class="midSectInner">
        <br>
        <?php echo htmlspecialchars_decode($content); ?>
        <br>
        <div class="clr"></div>
    </div>
</div>
<!--MID SECTION-->
@stop
