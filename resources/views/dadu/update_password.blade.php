@extends('dadu/master')

@section('title', 'Indonesia Online Casino, Gambling Games | DADUKEMBAR')
@section('keywords', 'Indonesia Online Casino, Gambling Games | DADUKEMBAR')
@section('description', 'Enjoy gambling (kasino) games at Indonesia most trusted online casino, DADUKEMBAR. Play live casino, slots, horse racing and more.')

@section('top_css')
<link href="{{url()}}/dadu/resources/css/jquery.dropdown.css" rel="stylesheet" type="text/css" />
<link href="{{url()}}/dadu/resources/css/acct_management.css" rel="stylesheet" type="text/css" />
@stop

@section('content')

@include('dadu/transaction_mange_top', [ 'title' => 'Account Password' ] )
<!--MID SECTION-->
<div class="midSect">
    <div class="midSectInneracct">
        <!--ACCOUNT MANAGAMENT MENU-->
        <div class="inlineAccMenu">
            <ul>
                <li><a href="{{route('update-profile')}}">{{Lang::get('public.MyAccount')}}</a></li>
                <li><a href="{{route('update-password')}}">{{Lang::get('public.ChangePassword')}}</a></li>
                <li><a href="{{route('memberbank')}}">Bank Setting</a></li>
            <li><a href="{{route('memberverify')}}">Account Activation</a></li>
            </ul>
        </div>
        <!--ACCOUNT MANAGAMENT MENU-->
        
        <!--ACCOUNT TITLE-->
        <div class="title_bar">
            <span>{{Lang::get('public.ChangePassword')}}</span>
        </div>
        <!--ACCOUNT TITLE-->
        
         <!--ACCOUNT CONTENT-->
        <div class="acctContent">
            <span class="wallet_title"><i class="fa fa-pencil"></i>{{Lang::get('public.ChangePassword')}}</span>
            <form id="formChangePassword">
                <div class="acctRow">
                    <label>{{Lang::get('public.CurrentPassword')}} :</label><input type="password" id="current_password" />
                    <div class="clr"></div>
                </div>
                <div class="acctRow">
                    <label>{{Lang::get('public.NewPassword')}} :</label><input type="password" id="new_password" />
                    <div class="clr"></div>
                </div>
                <div class="acctRow">
                    <label>{{Lang::get('public.ConfirmNewPassword')}} :</label><input type="password" id="confirm_new_password" onkeyup="checkPass(); return false;"/>
                    <div class="clr"></div>
                </div>
                <div class="submitAcct">
                    <a href="javascript:void(0)" onclick="update_password()">{{Lang::get('public.Update')}}</a>
                </div>
                <div class="clr"></div>
            </form>
        </div>
        <!--ACCOUNT CONTENT-->
        
    </div>
</div>
<!--MID SECTION-->
@stop

@section('bottom_js')
<script>
function checkPass()
{
    //Store the password field objects into variables ...
    var new_password = document.getElementById('new_password');
    var confirm_new_password = document.getElementById('confirm_new_password');
    //Store the Confimation Message Object ...
    var message = document.getElementById('confirmMessage');
    //Set the colors we will be using ...
    var goodColor = "#66cc66";
    var badColor = "#ff6666";
    //Compare the values in the password field 
    //and the confirmation field
    if(new_password.value == confirm_new_password.value){
        //The passwords match. 
        //Set the color to the good color and inform
        //the user that they have entered the correct password 
        confirm_new_password.style.backgroundColor = goodColor;
        message.style.color = goodColor;
        message.innerHTML = "Passwords Match!"
    }else{
        //The passwords do not match.
        //Set the color to the bad color and
        //notify the user.
        confirm_new_password.style.backgroundColor = badColor;
        message.style.color = badColor;
        message.innerHTML = "Passwords Do Not Match!"
    }
} 

 function update_password(){
        if($('#new_password').val() == $('#confirm_new_password').val()){
            $.ajax({
                    type: "POST",
                    url: "{{action('User\MemberController@ChangePassword')}}",
                    data: {
                            _token: "{{ csrf_token() }}",
                            current_password:		$('#current_password').val(),
                            new_password:    		$('#new_password').val(),
                            confirm_new_password:   $('#confirm_new_password').val()

                    },
            }).done(function( json ) {

                    obj = JSON.parse(json);
                             var str = '';
                             $.each(obj, function(i, item) {
                                    str += item + '\n';
                            })

                            alert(str);

            });
        }else{
            alert('Invalid Password!');
        }
}
</script>
@stop