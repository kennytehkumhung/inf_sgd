@extends('dadu/master')

@section('title', 'Indonesia Online Casino, Gambling Games | DADUKEMBAR')
@section('keywords', 'Indonesia Online Casino, Gambling Games | DADUKEMBAR')
@section('description', 'Enjoy gambling (kasino) games at Indonesia most trusted online casino, DADUKEMBAR. Play live casino, slots, horse racing and more.')

@section('top_css')
<link href="{{url()}}/dadu/resources/css/mobile.css" rel="stylesheet" type="text/css" />
@stop

@section('content')
<!--CASINO LOBBY SECTION-->
<div class="lcLobby">
    <div class="lcLobbyInner3">
        <img src="{{url()}}/dadu/img/mob-tit.png">
    </div>
</div>
<!--CASINO LOBBY SECTION-->

<!--MID SECTION-->
<div class="midSect2">
    <div class="midSectInner">
        <div class="box1mob">
            <div class="qr">
                <img src="{{url()}}/dadu/img/allbet_qr.jpg" width="78" height="78">
            </div>

            <div class="androidMb">
                <img src="{{url()}}/dadu/img/ios-dl.png">
            </div>
        </div>

        <div class="box2mob">
            <div class="qr1">
                <img src="{{url()}}/dadu/img/qr_code/playtech_live_qr.jpg" width="68">
                <img src="{{url()}}/dadu/img/android-lc.png">
            </div>

            <div class="qr2">
                <img src="{{url()}}/dadu/img/qr_code/playtech_slot_qr.jpg" width="68">
                <img src="{{url()}}/dadu/img/android-slot.png">
            </div>
        </div>

        <div class="box3mob">
            <div class="qr">
                <img src="{{url()}}/dadu/img/qr_code/gameplay_qr.jpg" width="78" height="78">
            </div>

            <div class="androidMb">
                <img src="{{url()}}/dadu/img/android-mb.png">
            </div>
        </div>

        <div class="box4mob">
            <div class="qr1">
                <img src="{{url()}}/dadu/img/qr_code/ag_qr.jpg" width="68">
                <img src="{{url()}}/dadu/img/android-mb.png">
            </div>

            <div class="qr2">
                <img src="{{url()}}/dadu/img/qr_code/ag_qr.jpg" width="68">
                <img src="{{url()}}/dadu/img/ios-dl.png">
            </div>
        </div>
        
        <div class="clr"></div>
    </div>
</div>
<!--MID SECTION-->
@stop
