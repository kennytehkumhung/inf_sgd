@extends('dadu/master')

@section('title', 'Indonesia Online Casino, Gambling Games | DADUKEMBAR')
@section('keywords', 'Indonesia Online Casino, Gambling Games | DADUKEMBAR')
@section('description', 'Enjoy gambling (kasino) games at Indonesia most trusted online casino, DADUKEMBAR. Play live casino, slots, horse racing and more.')

@section('top_css')
<link href="{{url()}}/dadu/resources/css/promo.css" rel="stylesheet" type="text/css" />
@stop

@section('content')
<!--CASINO LOBBY SECTION-->
<div class="lcLobby">
    <div class="lcLobbyInner3">
        <img src="{{url()}}/dadu/img/faq.png" width="706" height="60">
    </div>
</div>
<!--CASINO LOBBY SECTION-->
<script>
$(document).ready(function($){
	$(".accordion-contentAMPM").not($(this).next()).slideUp('');
		$('#accordionAMPM').find('.accordion-toggleAMPM').click(function(){

                //Expand or collapse this panel
                $(this).next().slideToggle('slow');

                //Hide the other panels
                $(".accordion-contentAMPM").not($(this).next()).slideUp('slow');

        });   
		
});
</script>
<!--MID SECTION-->
<div class="midSect2">
    <div class="midSectInner">
        <br>
	<h5></h5>
        <div id="accordionAMPM" style="cursor:pointer;">	
                <h4 class="accordion-toggleAMPM">1. How do I register a Player's account with DADUKEMBAR?</h4>
                <div class="accordion-contentAMPM default">
                <p>Untuk mendaftar di DADUKEMBAR, Anda harus klik “Register Now” di halaman pertama situs DADUKEMBAR.</p>
                </div>
                <br>
                <h4 class="accordion-toggleAMPM">2. Kapan saya bisa mulai bertaruh di DADUKEMBAR?</h4>
                <div class="accordion-contentAMPM default">
                <p>Anda bisa segera melakukan taruhan setelah mendaftarkan diri menjadi member DADUKEMBAR dan melakukan deposit sesuai dengan keinginan Anda.</p>
                </div>
                <br>
                <h4 class="accordion-toggleAMPM">3. Berapa jumlah minimum dan maksimum deposit?</h4>
                <div class="accordion-contentAMPM default">
                <p>Minimum deposit di DADUKEMBAR adalah 100,000 rupiah dan maksimum deposit tidak terbatas.</p>
                </div>
                <br>
                <h4 class="accordion-toggleAMPM">4. Kapan saya bisa melakukan deposit dan kapan deposit tersebut diproses?</h4>
                <div class="accordion-contentAMPM default">
                <p>Deposit dapat dilakukan kapan saja dan kami akan memproses deposit Anda setelah kami mengetahui uang tersebut sudah masuk ke rekening kami.</p>
                <p>Untuk informasi waktu offline Bank BCA, Senin - Jumat (21:00 – 01:00 WIB), Sabtu – Minggu (00:00 – 07:00).</p>
                </div>
                <br>
                <h4 class="accordion-toggleAMPM">5. Bisakah saya membatalkan taruhan?</h4>
                <div class="accordion-contentAMPM default">
                <p>Taruhan yang sudah dikonfirmasi tidak dapat dibatalkan.</p>
                </div>
                <br>
                <h4 class="accordion-toggleAMPM">6. Bagaimana cara melakukan penarikan dana dan berapa lama waktu prosesnya?</h4>
                <div class="accordion-contentAMPM default">
                <p>Dengan mengisi formulir penarikan di bagian ”manajemen dana”. Waktu untuk penarikan dana adalah setiap hari. Kami akan segera transfer uang tersebut ke Nomor Rekening Anda yang sesuai dengan data yang Anda daftarkan saat melakukan pendaftaran.</p>
                </div>
                <br>
                <h4 class="accordion-toggleAMPM">7. Apakah data pribadi saya aman?</h4>
                <div class="accordion-contentAMPM default">
                <p>Kami tidak akan menyebarluaskan atau memberitahukan data pribadi Anda kepada pihak ketiga manapun.</p>
                </div>
                <br>
                <h4 class="accordion-toggleAMPM">8. Apa yang harus saya lakukan jika saya lupa kata sandi (password)?</h4>
                <div class="accordion-contentAMPM default">
                <p>Silahkan klik “lupa kata sandi?” di bawah tombol “Masuk”.</p>
                </div>
                <br>
                <h4 class="accordion-toggleAMPM">9. Apa yang harus dilakukan jika saya tidak bisa masuk (login)?</h4>
                <div class="accordion-contentAMPM default">
                <p>Silahkan Anda menghubungi Costumer Services DADUKEMBAR.</p>
                </div>
                <br>
        </div>
        <br>
        <div class="clr"></div>
    </div>
</div>
<!--MID SECTION-->
@stop
