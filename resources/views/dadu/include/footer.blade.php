<div class="footerOuter">
    <div class="footer">
        <ul class="footerL">
            <li><a href="{{route('aboutus')}}">About Us</a></li>
            <li><a href="{{route('faq')}}">FAQ</a></li>
            <li style="padding-right: 0px !important;"><a href="{{route('tnc')}}">Terms & Conditions</a></li>
        </ul>
        <div class="footerR">©2016 DADUKEMBAR. All rights reserved | 18+</div>
        <div class="line"></div>
        <div class="line"></div>
        <div class="operatorContainer">
            <table class="operatorIMG" width="100%" border="0" cellspacing="0" cellpadding="0">
                <span class="footerBottom_title">Proud Sponsor Of</span>
                <tr>
                    <td><img title="Oneworks" src="{{url()}}/dadu/img/xp.png"/></td>
                    <td><img title="WinningFT" src="{{url()}}/dadu/img/allbetlogo.png"/></td>
                    <td><img title="Maxbet" src="{{url()}}/dadu/img/mxb-logo.png"/></td>
                    <td><img title="855Crown" src="{{url()}}/dadu/img/855-logo.png"/></td>
                    <td><img title="AG Gaming" src="{{url()}}/dadu/img/ag-logo.png"/></td>
                    <td><img title="Gameplay" src="{{url()}}/dadu/img/gp-logo.png"/></td>
                    <td><img title="PSBet" src="{{url()}}/dadu/img/psb-logo.png"></td>
                </tr>
            </table>
        </div>
        <div class="socialContainer">
            <table class="operatorIMG" width="70px" border="0" cellspacing="0" cellpadding="0">
                <span class="footerBottom_title1">Connect With Us</span><br>
                <tr>
                    <td><a href="https://www.facebook.com/Dadukembar-1737879369801103/" id="name"><img title="fb" src="{{url()}}/dadu/img/fb-logo-hover.png" /></a></td>
                    <td><a href="https://twitter.com/dadukembarindo" id="name"><img title="Twitter" src="{{url()}}/dadu/img/twitter-logo-hover.png"/></a></td>
                </tr>
            </table>
            <div class="clr"></div>
        </div>
        <div class="clr"></div>
        <div class="line"></div>
        <div class="browserContainer">
            <table class="operatorIMG2" border="0" cellspacing="0" cellpadding="0">
                <span class="footerBottom_title">Supported Browser</span>
                <tr>
                    <td><a href="#" id="name"><img title="Chrome" src="{{url()}}/dadu/img/chrome-logo.png" /></a></td>
                    <td><a href="#" id="name"><img title="Firefox" src="{{url()}}/dadu/img/ff-logo.png"/></a></td>
                    <td><a href="#" id="name"><img title="IE" src="{{url()}}/dadu/img/ie-logo.png" /></a></td>
                </tr>
            </table>    
        </div>
        <div class="bankContainer">
            <table class="operatorIMG" width="519px" border="0" cellspacing="0" cellpadding="0">
                <span class="footerBottom1_title">Supported Payment Methods</span>
                <tr>
                    <td><a href="#" id="name"><img title="Bank" src="{{url()}}/dadu/img/logo_all_bank.png"/></a></td>
                </tr>
            </table>
        </div>
        <div class="clr"></div>
    </div>
</div>