<div class="header">
    <div class="headerInner">
        <div class="topRow">
            <div class="timer">
                <?php date_default_timezone_set("Asia/Bangkok");?>
                <?php echo date("d M Y");?>,
                <?php echo substr(date("l"),0,3);?>,
                <?php echo date(" H:i:s", time());?> (GMT +7)
            </div>
            <div class="announcement"><marquee>{{App\Http\Controllers\User\AnnouncementController::index()}}</marquee></div>
            <div class="lang">
                <table width="auto" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td valign="middle"><a href="{{route( 'language', [ 'lang'=> 'en'])}}"><img src="{{url()}}/dadu/img/en.png"></a></td>
                        <td valign="middle"><a href="{{route( 'language', [ 'lang'=> 'id'])}}"><img src="{{url()}}/dadu/img/id.png"></a></td>
                    </tr>
                 </table>
            </div>

            <div class="clr"></div>
        </div>
        <div class="logo">
            <img src="{{url()}}/dadu/img/logo.png" width="145" height="96" alt=""/>
        </div>
        @if (!Auth::user()->check())
        <div class="logD">
            <div class="logDinner">
                <input type="text" placeholder="{{Lang::get('public.Username')}}" id="username">
                <input type="password" placeholder="{{Lang::get('public.Password')}}" id="password">
                <img src="{{route('captcha', ['type' => 'login_captcha'])}}" width="50px" height="27px" style="margin-bottom: -8px;"/>
                <input type="text" style="width:40px !important;"placeholder="Code" id="code" onKeyPress="enterpressalert(event, this)">
            </div>
            <div>
                <ul>
                <li class="submit"> <a onclick="login()" href="#">{{Lang::get('public.Login')}}</a></li>
                <li>&nbsp; <a href="{{route('register')}}"> <img src="{{url()}}/dadu/img/register.gif" alt="" height="27px"/></a></li></ul>
            </div>

            <div class="clr"></div>
        </div>
        <span class="fPass"><a href="{{route('forgotpassword')}}">{{Lang::get('public.ForgotPassword')}}</a></span>
        @else
        <div class="logD1">
            <div class="headerRightTable">
                <div class="adj2">
                    <table width="auto" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                        <td valign="middle"><div class="welc"><em class="fa fa-user"></em> {{Lang::get('public.Welcome')}}, {{Session::get('username')}}!<em style="margin-left:15px;" class="fa fa-ellipsis-v fa-1x"></em></div></td>
                            <td valign="middle">
                                <div class="welc1"><a href="#" data-dropdown="#dropdown-0"><i class="fa fa-caret-square-o-right"></i>{{Lang::get('public.Profile')}}</a></div>
                                <div id="dropdown-0" class="dropdown dropdown-tip " >
                                    <ul class="dropdown-menu" style="margin: 0px;">
                                        <li><a href="{{route('update-profile')}}">{{Lang::get('public.MyAccount')}}</a></li>
                                        <li><a href="{{route('update-password')}}">{{Lang::get('public.ChangePassword')}}</a></li>
                                        <li><a href="{{route('memberbank')}}">{{Lang::get('COMMON.CTABBANKSETTING')}}</a></li>
                                        <li><a href="{{route('memberverify')}}">Account Activation</a></li>
                                    </ul>
                                </div>
                            </td>
                            <td valign="middle"><div class="welc1"><a href="#" data-dropdown="#dropdown-1" onclick="getBalance();"><i class="fa fa-money"></i>{{Lang::get('public.Balance')}}</a></div>
                                <div id="dropdown-1" class="dropdown dropdown-tip " >
                                    <ul class="dropdown-menu" style="min-width:245px;">
                                        <li class="noAttr">
                                            <div class="bal">
                                                <ul style="width:60%;">
                                                    <li style="font-size:13px;">{{Lang::get('public.Wallet')}}</li>
                                                    <table width='100%'>
                                                        <tr>
                                                            <td style="color:white;font-size:13px;" align="center">{{Lang::get('public.MainWallet')}}</td>
                                                        </tr>
                                                        @foreach( Session::get('products_obj') as $prdid => $object)
                                                        <tr>
                                                            <td style="color:white;font-size:13px;" align="center">{{$object->name}}</td>
                                                        </tr>
                                                        @endforeach
                                                    </table>
                                                    <li style="font-size:13px;">{{Lang::get('public.Total')}}</li>
                                                </ul>
                                                <ul style="width:40%;">
                                                    <li style="font-size:13px;">{{Lang::get('public.Balance')}}</li>
                                                    <table width='100%'>
                                                        <tr>
                                                            <td class="main_wallet" style="color:white;font-size:13px;" align="center">0.00</td>
                                                        </tr>
                                                        @foreach( Session::get('products_obj') as $prdid => $object)
                                                        <tr>
                                                            <td class="{{$object->code}}_balance" style="color:white;font-size:13px;" align="center">0.00</td>
                                                        </tr>
                                                        @endforeach
                                                    </table>
                                                    <li id="total_balance" style="font-size:13px;">0.00</li>
                                                </ul>
                                            </div>
                                            <div class="clr"></div>
                                        </li>
                                    </ul>
                                </div>
                            </td>
                            <td valign="middle">
                                <div class="welc1"> <a href="#" data-dropdown="#dropdown-2"><i class="fa fa-calculator"></i>{{Lang::get('public.Fund')}}</a></div>
                                <div id="dropdown-2" class="dropdown dropdown-tip " >
                                    <ul class="dropdown-menu" style="margin: 0px;">
                                        <li><a href="{{route('deposit')}}">{{Lang::get('public.Deposit')}}</a></li>
                                        <li><a href="{{route('withdraw')}}">{{Lang::get('public.Withdrawal')}}</a></li>
                                        <li><a href="{{route('transaction')}}">{{Lang::get('public.TransactionHistory')}}</a></li>
                                        <li><a href="{{route('transfer')}}">{{Lang::get('public.Transfer')}}</a></li>
                                    </ul>
                                </div>
                            </td>
                            <td>
                                <ul>
                                    <li class="submit"><a href="{{route('logout')}}">{{Lang::get('public.Logout')}}</a></li>
                                </ul>
                            </td>
                            <td>
                                <ul>
                                    <li>&nbsp; <a href="{{route('deposit')}}"> <img src="{{url()}}/dadu/img/depositnow.gif" alt="" height="24px"/></a></li>
                                </ul>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="clr"></div>
        </div>
        @endif