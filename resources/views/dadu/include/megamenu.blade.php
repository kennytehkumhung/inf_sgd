        <!--NAVIGATION-->
        <div class="navigation">
            <ul>
                <li><a href="{{route('homepage')}}">Home</a></li>
                <li>
                    <a href="{{route('livecasino')}}">{{Lang::get('public.LiveCasino')}}</a>
                </li>
                <li>
                    <a href="{{route('sport')}}">{{Lang::get('public.SportsBook')}}</a>
                </li>
                <li>
                    <a href="{{route('slot',['type' => 'w88'])}}">{{Lang::get('public.Slots')}}</a>
                </li>
                <li><a href="{{route('promotion')}}">{{Lang::get('public.Promotion')}}</a></li>
                <li><a href="{{route('mobile')}}">{{Lang::get('public.Mobile')}}</a></li>
                <li><a href="{{route('contactus')}}">{{Lang::get('public.ContactUs')}}</a></li>
            </ul>
        </div>
        <!--NAVIGATION-->
    </div>
</div>