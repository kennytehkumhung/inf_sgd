@extends('dadu/master')

@section('title', 'Indonesia Online Casino, Gambling Games | DADUKEMBAR')
@section('keywords', 'Indonesia Online Casino, Gambling Games | DADUKEMBAR')
@section('description', 'Enjoy gambling (kasino) games at Indonesia most trusted online casino, DADUKEMBAR. Play live casino, slots, horse racing and more.')

@section('content')

@include('dadu/transaction_mange_top', [ 'title' => 'Account Profile' ] )
<!--MID SECTION-->
<div class="midSect">
    <div class="midSectInneracct">
    <!--ACCOUNT MANAGAMENT MENU-->
    <div class="inlineAccMenu">
        <ul>
            <li><a href="{{route('update-profile')}}">{{Lang::get('public.MyAccount')}}</a></li>
            <li><a href="{{route('update-password')}}">{{Lang::get('public.ChangePassword')}}</a></li>
            <li><a href="{{route('memberbank')}}">Bank Setting</a></li>
            <li><a href="{{route('memberverify')}}">Account Activation</a></li>
        </ul>
    </div>
    <!--ACCOUNT MANAGAMENT MENU-->
    <!--ACCOUNT TITLE-->
    <div class="title_bar">
        <span>{{Lang::get('public.MyAccount')}}</span>
    </div>
    <!--ACCOUNT TITLE-->
    <!--ACCOUNT CONTENT-->
        <div class="acctContent">
        <span class="wallet_title"><i class="fa fa-pencil"></i>{{Lang::get('public.AccountProfile')}}</span>
        <div class="acctRow">
            <label>{{Lang::get('public.Username')}} :</label><span class="acctText">{{$acdObj->fullname}}</span>
            <div class="clr"></div>
        </div>
        <div class="acctRow">
            <label>{{Lang::get('public.FullName')}} :</label><span class="acctText">{{$acdObj->fullname}}</span>
            <div class="clr"></div>
        </div>
        <div class="acctRow">
            <label>{{Lang::get('public.EmailAddress')}} :</label><span class="acctText">{{$acdObj->email}}</span>
            <div class="clr"></div>
        </div>
        <div class="acctRow">
            <label>Currency :</label><span class="acctText">IDR</span>
            <div class="clr"></div>
        </div>
        <div class="acctRow">
            <label>{{Lang::get('public.ContactNo')}} :</label><span class="acctText">{{$acdObj->telmobile}}</span>
            <div class="clr"></div>
        </div>
        <div class="acctRow">
            <label>{{Lang::get('public.DOB')}} :</label>
            {{$acdObj->dob}}
            <div class="clr"></div>
        </div>
    </div>
    <!--ACCOUNT CONTENT-->
    </div>
</div>
<!--MID SECTION-->
@stop

@section('bottom_js')
<script>
$(function() {
    $( ".datepicker" ).datepicker({ dateFormat: 'yy-mm-dd' });
  });

 function update_profile(){
	$.ajax({
		type: "POST",
		url: "{{action('User\MemberController@UpdateDetail')}}",
		data: {
			_token: "{{ csrf_token() }}",
			dob:		$('#dob').val(),
			mobile:    	$('#mobile').val(),
			gender:    	$('#gender').val()

		},
	}).done(function( json ) {
		
			 obj = JSON.parse(json);
			 var str = '';
			 $.each(obj, function(i, item) {
				str += item + '\n';
			})
			
			alert(str);
			
			
	});
}
</script>
@stop