@include('dadu/include/head')

@section('title', 'Maintenance')

<body>
<div class="slider">
    <div class="sliderInner5">
        <div class="msgBox">
            <div class="excl">
                <img src="{{url()}}/dadu/img/under-maintenance.png" alt=""/>
            </div>
            <div class="clr"></div>
        </div>
    </div>
</div>
    
</body>
            
