@extends('ampm/master')
@section('title', 'Slots')
@section('keywords')
Live Casino, Casino Online, Game Online Casino, Casino Live, Casino Online Terpercaya, Casino Bet, Slot Game
@endsection 
@section('description')
Live Casino, Casino Online, Game Online Casino, Casino Live, Casino Online Terpercaya, Casino Bet, Slot Game
@endsection 
@section('content')
<script>
 function change_iframe(url,image_link,product){
         $('#iframe_game').attr('src',url);
         $('.top').show();
         $('.slot_'+product+'_image').hide();
 }
</script>
	<div class="midBottom">

		<div class="lcCont">
		<img src="{{url()}}/ampm/img/slot-banner.jpg">
		<div class="jackp">
		<div class="jpTit">
		  <img src="{{url()}}/ampm/img/jp-title.png" width="600">
		</div>

		<div class="jpCount">
		<iframe id="ContentPlaceHolder1_iframe_game" width="170px" height="20px" frameborder="0" src="http://tickers.playtech.com/jackpots/new_jackpot.swf?info=1&casino=playtech&font_face=arial&bold=true&currency=IDR&game=glrjj-1&auto=1"></iframe>
		</div>

		</div>

		<div class="tp2">
		<table width="auto" border="0" cellspacing="0" cellpadding="0">
		  <tr>
			<td>
			<div class="lcas1">
		<div class="lcBox2">
		<div id="crossfade">
			<a href="javascript:void(0)" onClick="change_iframe('{{route('w88' , [ 'type' => 'slot' , 'category' => 'Arcades' ] )}}','gp-slot-hover.png','w88')">
				<img class="bottom" src="{{url()}}/ampm/img/slot-1-thumb-hover.png" />
				<img class="top" src="{{url()}}/ampm/img/slot-1-thumb.png" />
			</a>
		</div>
		</div>
		</div>
			</td>
			<td>
			<div class="lcas1 marL">
		<div class="lcBox2">
		<div id="crossfade">
			<a href="javascript:void(0)" onClick="change_iframe('{{route('pltb' , [ 'type' => 'pgames' , 'category' => '1' ] )}}','pt-slot-hover.png','plt')">
				<img class="bottom" src="{{url()}}/ampm/img/slot-2-thumb-hover.png" />
				<img class="top" src="{{url()}}/ampm/img/slot-2-thumb.png" />
			</a>
		</div>
		</div>
		</div>
			</td>
			</tr>
			
		  
		</table>
		<div class="slotContainerBottom">
		<iframe id="iframe_game" frameborder="0" style="overflow:hidden;overflow-x:hidden;overflow-y:hidden;height: 970px; width:1100px; margin-left: -11px;" src="
			@if( $type == 'mxb' )
				{{route('mxb' , [ 'type' => 'slot' , 'category' => 'Slots' ] )}}
			@elseif( $type == 'w88' )
				{{route('w88', [ 'type' => 'slot' , 'category' => 'Arcades' ] )}}
			@elseif( $type == 'plt' || $type == 'pltb' )
				{{route('pltb' , [ 'type' => 'pgames' , 'category' => '1' ] )}}
			@elseif( $type == 'a1a' )
				{{route('a1a')}}
			@endif
			"></iframe>
			</div>
		</div>
		</div>

	  <div class="clr"></div>
	<!-- /container -->
	</div>
	  
@stop