@extends('../ampm/master')

@section('title', Lang::get('public.SportsBook'))
@section('keywords', 'football betting,sports betting')
@section('description', 'Gambling and bet on football at trusted Malaysia online sports betting site, Infiniwin. Win big today!')

@section('content')
<div class="lcCont">
<img src="{{url()}}/ampm/img/sp-title.png">

<div class="stripe5"></div>

<div class="tp3">
<table width="auto" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>
    <div class="lcas1">
<div class="lcBox2">
<a href="{{route('ibc')}}">
<div id="crossfade">
<img class="bottom" src="{{url()}}/ampm/img/sp-1-thumb-hover.jpg" />
<img class="top" src="{{url()}}/ampm/img/sp-1-thumb.jpg" />
</div>
</a>
</div>
</div>
    </td>
    <td>
    <div class="lcas1 marL">
<div class="lcBox2">
<div id="crossfade">
<img class="bottom" src="{{url()}}/ampm/img/sp-2-thumb-hover.jpg" />
<img class="top" src="{{url()}}/ampm/img/sp-2-thumb.jpg" />
</div>
</div>
</div>
    </td>
    </tr>
    
  
</table>

</div>
</div>

@stop

