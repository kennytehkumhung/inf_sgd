@extends('../ampm/master')

@section('title', 'TBS')
@section('keywords', 'football betting,sports betting')


@section('content')
<script>
function changeStyle(style){
	
	window.location = "{{route('tbs')}}?style="+style.value;
}
</script>


<select id="tbs_style" onChange="changeStyle(this)" style="float:right;width:100px;" >
	<option value="SP1" @if($style == 'SP1')selected @endif>Cyan</option>
	<option value="SP2" @if($style == 'SP2')selected @endif>Blue</option>
	<option value="SP3" @if($style == 'SP3')selected @endif>Red</option>
	<option value="SP4" @if($style == 'SP4')selected @endif>Gold</option>
</select>

<iframe id="ContentPlaceHolder1_iframe_game" width="1130px" height="1200px" style="margin-left: -15px;" frameborder="0" src="{{$login_url}}"></iframe>


@stop

