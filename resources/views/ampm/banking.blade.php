@extends('ampm/master')

@section('title', 'Banking')


@section('content')



<div class="midBottom">

<div class="lcCont">
<div class="mobTit">
<img src="{{url()}}/ampm/img/bk-title.png">
</div>

<table width="100%" border="0" cellpadding="0" cellspacing="0" class="bankingtable">
        <tr style="background-color: #000; font-weight: bold;">
          <td width="15%" rowspan="2"><div align="center">Bank Name</div></td>
          <td width="15%" rowspan="2"><div align="center">Status</div></td>
          <td width="15%" rowspan="2"><div align="center">Banking Method</div></td>
          <td colspan="2" align="center"><strong>Transaction Limit (IDR)</strong></td>
          <td width="15%" rowspan="2"><div align="center"><strong>Processing Time</strong></div></td>
        </tr>
        <tr style="background-color:#32CBEB;font-weight:bold;">
          <td width="8%"><div align="center"><strong>Min</strong></div></td>
          <td width="8%"><div align="center"><strong>Max</strong></div></td>
        </tr>
        <tr>
          <td height="20" colspan="6" bgcolor="#fff568" align="center"><span style="font-weight:bold;color:#000">Deposit</span></td>
        </tr>
        <tr>
          <td><div align="center"><img src="{{url()}}/ampm/img/footer-1.png" alt=""/></div></td>
          <td><div align="center">Online</div></td>
          <td><div align="center">ATM/Internet</div></td>
          <td rowspan="4"><div align="center">50,000</div></td>
          <td rowspan="4"><div align="center">300,000,000</div></td>
          <td rowspan="4"><div align="center">5 Minutes</div></td>
        </tr>
        <tr>
          <td><div align="center"><img src="{{url()}}/ampm/img/footer-2.png" alt=""/></div></td>
          <td><div align="center">Online</div></td>
          <td><div align="center">ATM/Internet</div></td>
        </tr>
        <tr>
          <td><div align="center"><img src="{{url()}}/ampm/img/footer-3.png" alt=""/></div></td>
          <td><div align="center">Online</div></td>
          <td><div align="center">ATM/Internet</div></td>
        </tr>   
		<tr>
          <td><div align="center"><img src="{{url()}}/ampm/img/logo mandiri.png" width="85" height="35" alt=""/></div></td>
          <td><div align="center">Online</div></td>
          <td><div align="center">ATM/Internet</div></td>
        </tr>
        <tr>
          <td height="20" colspan="6" bgcolor="#fff568" align="center"><span style="font-weight:bold;color:#000">Withdrawal</span></td>
        </tr>
        <tr>
          <td><div align="center"><img src="{{url()}}/ampm/img/footer-1.png" alt=""/></div></td>
          <td align="center"><div align="center">Online</div></td>
          <td align="center"><div align="center">Local Bank Transfer</div></td>
          <td rowspan="4" align="center"><div align="center">100,000</div></td>
          <td rowspan="4" align="center"><div align="center">300,000,000</div></td>
          <td rowspan="4" align="center"><div align="center">15 Minutes</div></td>
        </tr>
        <tr>
          <td><div align="center"><img src="{{url()}}/ampm/img/footer-2.png" alt=""/></div></td>
          <td align="center"><div align="center">Online</div></td>
          <td align="center"><div align="center">Local Bank Transfer</div></td>
        </tr>
        <tr>
          <td><div align="center"><img src="{{url()}}/ampm/img/footer-3.png" alt=""/></div></td>
          <td align="center"><div align="center">Online</div></td>
          <td align="center"><div align="center">Local Bank Transfer</div></td>
        </tr>
		<tr>
           <td><div align="center"><img src="{{url()}}/ampm/img/logo mandiri.png" width="85" height="35" alt=""/></div></td>
          <td align="center"><div align="center">Online</div></td>
          <td align="center"><div align="center">Local Bank Transfer</div></td>
        </tr>   
    
      </table>
      
      <p style="color: #fff;">
      <strong>Friendly Reminder:</strong>
      <br />
      <ul class="txtwh">
        <li>All Deposit and withdrawal processing time are subject to online banking availability</li>
        <li>The bank account holder name and the registered VWanBet.com account name must be identical to ensure successful deposit of funds</li>
        <li>Due to Anti-Money Laundering provisions, withdrawals are only to be paid to individual bank accounts with the same REGISTERED name. <br>
        Withdrawals to third party bank accounts are not allowed.</li>
        <li>All members are allowed to withdraw ONCE per day</li>
        <li>All Maximum transaction amount is based on per transaction basis.</li>
        <li>Please be inform that large withdrawal amounts might take longer processing time.</li>
        <li>We support other Local Banks as well so if you have any special request please speak to our Customer Service</li>
        <li>Please refer to Company's Terms and Condition for more details</li>
        <li>Remember to turn off the block pop-up function of your browser.</li>
      </ul>
      </p>

</div>








  <div class="clr"></div>
<!-- /container -->
</div>

@stop

