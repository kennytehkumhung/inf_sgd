@extends('ampm/master')
@section('title', 'Live Casino')
@section('keywords')
Live Casino, Casino Online, Game Online Casino, Casino Live, Casino Online Terpercaya, Casino Bet, Slot Game
@endsection 
@section('description')
Live Casino, Casino Online, Game Online Casino, Casino Live, Casino Online Terpercaya, Casino Bet, Slot Game
@endsection 
@section('content')

	<div class="midBottom">

	<div class="lcCont">
	<img src="{{url()}}/ampm/img/lc-banner.png" width="1100" height="210">

	<table width="auto" border="0" cellspacing="0" cellpadding="0">
	  <tr>
		<td>
		 @if(in_array('W88',Session::get('valid_product')))
			<div class="lcas1" onClick="@if (Auth::user()->check())window.open('{{route('w88', [ 'type' => 'casino' , 'category' => 'live' ] )}}', 'casino12', 'width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif">
			<div class="lcBox2">
			<div id="crossfade">
			<img class="bottom" src="{{url()}}/ampm/img/lc-1-thumb-hover.png" />
			<img class="top" src="{{url()}}/ampm/img/lc-1-thumb.png" />
			</div>
			</div>
			</div>
		 @endif
		</td>
		<td>
		 @if(in_array('AGG',Session::get('valid_product')))
			<div class="lcas1 marL" onClick="@if (Auth::user()->check())window.open('{{route('agg')}}', 'casino11', 'width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif">
			<div class="lcBox2">
			<div id="crossfade">
			<img class="bottom" src="{{url()}}/ampm/img/lc-2-thumb-hover.png" />
			<img class="top" src="{{url()}}/ampm/img/lc-2-thumb.png" />
			</div>
			</div>
			</div>
		 @endif
		</td>
		</tr>
		<tr>
		<td>
		@if(in_array('ALB',Session::get('valid_product')))
			<div class="lcas1 tp1" onClick="@if (Auth::user()->check())window.open('{{route('alb', [ 'type' => 'casino' ] )}}', 'casino12', 'width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif">
			<div class="lcBox2">
			<div id="crossfade">
			<img class="bottom" src="{{url()}}/ampm/img/lc-3-thumb-hover.png" />
			<img class="top" src="{{url()}}/ampm/img/lc-3-thumb.png" />
			</div>
			</div>
			</div>
		@endif
		</td>
		<td>
		@if(in_array('PLTB',Session::get('valid_product')))
			<div class="lcas1 marL tp1" onClick="@if (Auth::user()->check())window.open('{{route('pltbiframe')}}', 'casinoplt', 'width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif">
			<div class="lcBox2">
			<div id="crossfade">
			<img class="bottom" src="{{url()}}/ampm/img/lc-4-thumb-hover.png" />
			<img class="top" src="{{url()}}/ampm/img/lc-4-thumb.png" />
			</div>
			</div>
			</div>
		@endif
		</td>
	  </tr>
	  
	</table>
	</div>








	  <div class="clr"></div>
	<!-- /container -->
	</div>
	  
@stop