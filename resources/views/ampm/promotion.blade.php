@extends('ampm/master')

@section('title', 'Promotion')



@section('content')
<link rel="stylesheet" type="text/css" href="{{url()}}/ampm/resources/css/jquery.fancybox.css" media="screen" />
<script type="text/javascript" src="{{url()}}/ampm/resources/js/jquery.fancybox.js"></script>
<script type="text/javascript" src="{{url()}}/ampm/resources/js/fancyb.js"></script>

<div class="midBottom">

<div class="lcCont">
<div class="mobTit">
<img src="{{url()}}/ampm/img/promo-title.png">
</div>
<div class="stripe7"></div>

<table width="auto" border="0" cellspacing="0" cellpadding="0">
  	@foreach ($promo as $key => $value )
	@if ( ($key+1) %2 != 0)
	 <tr>
    @endif
		<td>
			<div class="lcas1">
				<div class="lcBox3">
				<a class="fancybox" href="#inline{{$key+1}}" title="{{$value['title']}}">
				<img class="bottom" src="{{$value['image']}}" /></a>

				</div>
			</div>
		</td>
	@if ( ($key+1) % 2 == 0)
	 </tr>
    @endif
	@endforeach
</table>
</div>

@foreach ($promo as $key => $value )
		  <div id="inline{{$key+1}}" style="display: none;">
			  <h3>{{$value['title']}}</h3>
			  <p>
			 <?php echo htmlspecialchars_decode($value['content']); ?>
			  </p>
	      </div>
@endforeach





  <div class="clr"></div>
<!-- /container -->
</div>
@stop

