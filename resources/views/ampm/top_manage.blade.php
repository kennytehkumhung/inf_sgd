<script>
 $(document).ready(function() { 
getBalance(true);
});
</script>
<div class="midSectCont">
<br>
<div class="walletTop">
<?php $count = 1 ; ?>
 @foreach( Session::get('products_obj') as $prdid => $object)
			  <div class="wallet<?php echo $count++; ?>">
				<div class="main-set-title">{{$object->name}}</div><div class="main-setPrice {{$object->code}}_balance">0.00</div>
			  </div>
 @endforeach

<div class="wallet<?php echo $count++; ?>">
<div class="main-set-title">Total</div><div class="main-setPrice" id="top_total_balance">0.00</div>
</div>
</div>

<!--ACCOUNT MANAGAMENT MENU-->
      <div class="inlineAccMenu">
      <ul>
		  @if( $title != 'Account Password' && $title != 'Account Profile' && $title != 'Bank Setting')
			  <li><a href="{{route('deposit')}}">{{Lang::get('public.Deposit')}}</a></li>
			  <li><a href="{{route('withdraw')}}">{{Lang::get('public.Withdrawal')}}</a></li>
			  <li><a href="{{route('transaction')}}">{{Lang::get('public.TransactionHistory')}}</a></li>
			  <li><a href="{{route('transfer')}}">{{Lang::get('public.Transfer')}}</a></li>
			  <!--<li><a href="multipleTransfer.html">Multiple Transfer</a></li>-->
	      @else
			  <li><a href="{{route('update-profile')}}">{{Lang::get('public.AccountProfile')}}</a></li>
			  <li><a href="{{route('update-password')}}">{{Lang::get('public.AccountPassword')}}</a></li>
			  <li><a href="{{route('memberbank')}}">{{Lang::get('COMMON.CTABBANKSETTING')}}</a></li>
		  @endif
      </ul>
      </div>
      <!--ACCOUNT MANAGAMENT MENU-->
   <!--ACCOUNT TITLE-->
      <div class="title_bar">
      <span>{{$title}}</span>
      </div>