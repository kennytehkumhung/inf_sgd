@extends('ampm/master')

@section('title', 'Register')



@section('content')


<div class="midContf">

<div class="otherPg">


<h2><i class="fa fa-user-plus"></i>REGISTRATION</h2> 
     <p>
      Create a new account. It's simple and free
     </p>
<div class="acctContentReg">
       <div class="acctRow">
         <label>Email Address* :</label><input type="text">
      <div class="clr"></div>
    </div>
      <div class="acctRow">
      <label>Contact Number* :</label><input type="text">
      <div class="clr"></div>
      </div>
      <div class="acctRow">
      <label>Currency :</label><label>MYR</label>
      <div class="clr"></div>
      </div>
      <div class="acctRow">
      <label>Full Name* :</label><input type="text">
      <div class="clr"></div>
      </div>
      <p>*Name must be matched with withdrawal bank account </p>
      <div class="acctRow">
      <label>Gender :</label>
      <select>
        <option value="male">male</option>
        <option value="female">female</option>
      </select>
      <div class="clr"></div>
      </div>
      <div class="acctRow">
      <label>Date of birth :</label>
      <select>
        <option value="male">00</option>
        <option value="female">01</option>
      </select>
      <select style="margin-left: 5px;">
        <option value="male">Jan</option>
        <option value="female">Feb</option>
      </select>
      <select style="margin-left: 5px;">
        <option value="male">2015</option>
        <option value="female">2016</option>
      </select>
      <div class="clr"></div>
      </div>
      <div class="acctRow">
      <label>Username* :</label><input type="text">
      <div class="clr"></div>
      </div>
      <p>Username Policy: length at least 6 characters and maximum of 12 </p>
      <div class="acctRow">
      <label>Password* :</label><input type="text">
      <div class="clr"></div>
      </div>
      <p>*Password Policy: length at least 6 characters and maximum of 20 </p>
      <div class="acctRow">
      <label>Re-enter Password* :</label><input type="text">
      <div class="clr"></div>
      </div>
      
      
      <div class="submitAcct">
      <a href="#">Register</a>
      </div>
     </div>

     


</div>
</div>
@stop


