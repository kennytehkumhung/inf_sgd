@extends('ampm/master')

@section('title', 'Mobile Download')


@section('content')
<div class="midBottom">

<div class="lcCont">
<div class="mobTit">
<img src="{{url()}}/ampm/img/mob-title.png">
</div>


<div class="mobCont">
<div class="mobR">
<div class="mobRtd">
<table border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center" valign="middle"><img src="{{url()}}/ampm/img/mobile/ag_qr.jpg" width="78" height="78" alt=""/></td>
    <td width="50" valign="bottom"><img src="{{url()}}/ampm/img/andr-icon.png" width="40" height="60"></td>
    <td valign="bottom">{{Lang::get('public.LiveCasino')}}</td>
    <td width="30" valign="bottom"></td>
    <td align="center" valign="middle"><img src="{{url()}}/ampm/img/mobile/ag_qr.jpg" width="78" height="78" alt=""/></td>
    <td width="50" valign="bottom"><img src="{{url()}}/ampm/img/appl-icon.png" width="40" height="60"></td>
    <td valign="bottom">{{Lang::get('public.LiveCasino')}}</td>
    
  </tr>
  <tr>
    <td>
    

    </td>
    <td width="50" align="center" valign="middle"></td>
    <td align="center" valign="middle">

    </td>
  </tr>
</table>

</div>

<div class="clr"></div>
</div>

<div class="mobR1">
<div class="mobRtd ct">
<table1 border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center" valign="middle"><img src="{{url()}}/ampm/img/mobile/playtech_live_qr.jpg" width="78" height="78" alt=""/></td>
    <td width="50" valign="bottom"><img src="{{url()}}/ampm/img/andr-icon.png" width="40" height="60"></td>
    <td valign="bottom">{{Lang::get('public.LiveCasino')}}</td>
    <td width="30" valign="bottom"></td>
    <td align="center" valign="middle"><img src="{{url()}}/ampm/img/mobile/playtech_slot_qr.jpg" width="78" height="78" alt="" style="padding-left:28px"/></td>
    <td width="50" valign="bottom"><img src="{{url()}}/ampm/img/andr-icon.png" width="40" height="60"></td>
    <td valign="bottom">{{Lang::get('public.Slot')}}</td>
    
  </tr>
  <tr>
    <td>
    

    </td>
    <td width="50" align="center" valign="middle"></td>
    <td align="center" valign="middle">

    </td>
  </tr>
</table1>

</div>
<div class="clr"></div>
</div>

<div class="mobR2">
<div class="mobRtd ct">
<table2 border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center" valign="middle"><img src="{{url()}}/ampm/img/mobile/gameplay_qr.jpg" width="78" height="78" alt=""/></td>
    <td width="50" valign="bottom"><img src="{{url()}}/ampm/img/andr-icon.png" width="40" height="60"></td>
    <td valign="bottom">{{Lang::get('public.LiveCasino')}}</td>
    
    
  </tr>
  <tr>
    <td>
    

    </td>
    <td width="50" align="center" valign="middle"></td>
    <td align="center" valign="middle">

    </td>
  </tr>
</table2>

</div>
<div class="clr"></div>
</div>

<div class="mobR3">
<div class="mobRtd ct">
<table3 border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center" valign="middle"><img src="{{url()}}/ampm/img/mobile/allbet_qr.jpg" width="78" height="78" alt=""/></td>
    <td width="50" valign="bottom"><img src="{{url()}}/ampm/img/andr-icon.png" width="40" height="60"></td>
    <td valign="bottom">{{Lang::get('public.LiveCasino')}}</td>
    
    
  </tr>
  <tr>
    <td>
    

    </td>
    <td width="50" align="center" valign="middle"></td>
    <td align="center" valign="middle">

    </td>
  </tr>
</table3>

</div>
<div class="clr"></div>
</div>



</div>
</div>








  <div class="clr"></div>
<!-- /container -->
</div>

<div class="clr"></div>
</div>

</div>
@stop

