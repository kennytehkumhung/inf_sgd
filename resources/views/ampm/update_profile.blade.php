@extends('ampm/master')

@section('title', Lang::get('public.AccountProfile'))


@section('content')


@include('ampm/top_manage', [ 'title' => 'Account Profile' ] )

      <!--ACCOUNT TITLE-->
      <!--ACCOUNT CONTENT-->
      <div class="acctContent">
      <span class="wallet_title"><i class="fa fa-pencil"></i>{{Lang::get('public.AccountProfile')}}</span>
      <div class="acctRow">
      <label>{{Lang::get('public.Username')}} :</label><span class="acctText"> {{Session::get('username')}}</span>
      <div class="clr"></div>
      </div>
      <div class="acctRow">
      <label>{{Lang::get('public.FullName')}} :</label><span class="acctText"> {{ $acdObj->fullname}}</span>
      <div class="clr"></div>
      </div>
      <div class="acctRow">
      <label>{{Lang::get('public.EmailAddress')}} :</label><span class="acctText"> {{ $acdObj->email}}</span>
      <div class="clr"></div>
      </div>
      <div class="acctRow">
      <label>{{Lang::get('public.Currency')}} :</label><span class="acctText"> {{Session::get('currency')}}</span>
      <div class="clr"></div>
      </div>
      <div class="acctRow">
      <label>{{Lang::get('public.ContactNo')}} :</label><span class="acctText"> {{ $acdObj->telmobile}}</span>
      <div class="clr"></div>
      </div>
      <div class="acctRow">
      <label>{{Lang::get('public.Gender')}} :</label>
				<select id="gender">
					<option value="1">{{Lang::get('public.Male')}}</option> 
					<option value="2">{{Lang::get('public.Female')}}</option>
				</select>
      <div class="clr"></div>
      </div>
      <div class="acctRow">
      <label>{{Lang::get('public.DOB')}} :</label>
      {{ $acdObj->dob}}
      <div class="clr"></div>
      </div>
      <div class="submitAcct">
      <a href="#">{{Lang::get('public.Submit')}}</a>
      </div>
      </div>
      <!--ACCOUNT CONTENT-->






<div class="clr"></div>






</div>



@stop
