@extends('ampm/master')

@section('title', 'Contact Us')



@section('content')


<div class="midBottom">

<div class="lcCont">
<div class="mobTit">
<img src="{{url()}}/ampm/img/ct-title.png">
</div>

<div class="ctBg">
<div class="ctBox">
    
@if( Lang::getLocale() == 'en' )

<h3 style="margin-left: 20px;">Forgot Password?</h3>
<div class="ct1">
<table width="auto" border="0" cellspacing="0" cellpadding="0" style="font-size:14px;">
  <tr>
    <td>Please contact our Customer Service for more information.</td>
  </tr>
</table>
</div>
<div class="ct2">
<table width="auto" border="0" cellspacing="0" cellpadding="0" style="font-size:14px;">
  <tr>
    <td>SMS Center</td>
    <td>:</td>
    <td>+62 819 0888 24 24</td>
  </tr>
  
    <tr>
    <td>WhatsApp</td>
    <td>:</td>
    <td>+62 819 0888 24 24</td>
  </tr> 
  
  <tr>
    <td>Skype</td>
    <td>:</td>
    <td>ampmplay</td>
  </tr>

  <tr>
    <td>Email</td>
    <td>:</td>
    <td>support@ampmplay.com</td>
  </tr>

  <tr>
    <td>WeChat</td>
    <td>:</td>
    <td>ampmoffcial</td>
  </tr>
  
  <tr>
    <td>Line id</td>
    <td>:</td>
    <td>ampmofficial</td>
  </tr> 
  
  <tr>
    <td>BBM</td>
    <td>:</td>
    <td>7AB9B0FD</td>
  </tr>  
  
  <tr>
    <td>YM id</td>
    <td>:</td>
    <td>ampmofficial</td>
  </tr>
</table>

</div>

@else

<h3 style="margin-left: 20px;">Lupa Kata Sandi?</h3>
<div class="ct1">
<table width="auto" border="0" cellspacing="0" cellpadding="0" style="font-size:14px;">
  <tr>
    <td>Silahkan hubungi layanan pelanggan kami untuk mendapatkan Informasi lebih lanjut</td>
  </tr>
</table>
</div>
<div class="ct2">
<table width="auto" border="0" cellspacing="0" cellpadding="0" style="font-size:14px;">
  <tr>
    <td>Layanan SMS</td>
    <td>:</td>
    <td>+62 819 0888 24 24</td>
  </tr>
  
    <tr>
    <td>WhatsApp</td>
    <td>:</td>
    <td>+62 819 0888 24 24</td>
  </tr> 
  
  <tr>
    <td> Skype</td>
    <td>:</td>
    <td>ampmplay</td>
  </tr>

  <tr>
    <td>Email</td>
    <td>:</td>
    <td>support@ampmplay.com</td>
  </tr>

  <tr>
    <td>WeChat</td>
    <td>:</td>
    <td>ampmoffcial</td>
  </tr>
  
  <tr>
    <td>Line id</td>
    <td>:</td>
    <td>ampmofficial</td>
  </tr> 
  
  <tr>
    <td>BBM</td>
    <td>:</td>
    <td>7AB9B0FD</td>
  </tr>  
  
  <tr>
    <td>YM id</td>
    <td>:</td>
    <td>ampmofficial</td>
  </tr>
</table>

</div>

@endif

</div>

<div class="clr"></div>
</div>

</div>








  <div class="clr"></div>
<!-- /container -->
</div>

@stop


