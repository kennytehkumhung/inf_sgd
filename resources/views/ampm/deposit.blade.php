@extends('ampm/master')

@section('title', Lang::get('public.Deposit'))



@section('content')
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script>
 function deposit_submit(){
	var file_data = $("#receipt").prop("files")[0];  
	var form_data = new FormData();                  
	form_data.append("file", file_data);
	
	var data = $('#deposit_form').serializeArray();
	var obj = {};
	for (var i = 0, l = data.length; i < l; i++) {
		form_data.append(data[i].name, data[i].value);
	}
	
	form_data.append('_token', '{{csrf_token()}}');

	$.ajax({
		type: "POST",
		url: "{{route('deposit-process')}}?",
		        dataType: 'script',
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,                         // Setting the data attribute of ajax with file_data
                type: 'post',
				beforeSend: function(){
					$('#deposit_sbumit_btn').attr('onclick','');
				},
				complete: function(json){
					
					obj = JSON.parse(json.responseText);
					 var str = '';
					 $.each(obj, function(i, item) {
						
						if( '{{Lang::get('COMMON.SUCESSFUL')}}' == item ){
							alert('{{Lang::get('COMMON.SUCESSFUL')}}');
							window.location.href = "{{route('transaction')}}";
						}else{
							//$('.'+i+'_acctTextReg').html(item);
							$('#deposit_sbumit_btn').attr('onclick','deposit_submit()');
							str += item + '<br>';
						}
					})
					$('.failed_message').html(str);
				
				},
					
	});
	//alert('asd');
}
$(function() {
    $( ".datepicker" ).datepicker({ dateFormat: 'yy-mm-dd' , defaultDate: new Date()});
});
</script>

@include('ampm/top_manage', [ 'title' => Lang::get('public.Deposit') ] )

      <!--ACCOUNT TITLE-->
     <!--ACCOUNT CONTENT-->
      <div class="DepContent">
	  <form id="deposit_form">
      
      <div class="depRight">
      <span class="wallet_title"><i class="fa fa-money"></i>{{Lang::get('public.Deposit')}}</span>
	  
	  <div class="acctRow">
		  <label>{{Lang::get('public.Bank')}}* :</label>
		  <select name="bank" style="width:300px;">
			@foreach( $banks as $key => $bank )
			<option value="{{$bank['bnkid']}}">{{$bank['name']}} - {{$bank['bankaccno']}} - {{$bank['bankaccname']}}</option>
			@endforeach
		  </select>
		<div class="clr"></div>
      </div>
	  
	  
      <div class="acctRow">
      <label>{{Lang::get('public.Amount')}} :</label><input type="text" name="amount" style="color:black;width:272px;" />
      <div class="clr"></div>
      </div>
	  <br>
	  <br>
	
	  <div class="acctRow">
      <label>{{Lang::get('public.DepositMethod')}}* :</label><select name="type" style="width:300px;">
        <option value="0">{{Lang::get('public.OverCounter')}}</option>
		<option value="1">{{Lang::get('public.InternetBanking')}}</option>
		<option value="2">{{Lang::get('public.ATMBanking')}}</option>
      </select>
      <div class="clr"></div>
      </div>
	  
	  
     
      <div class="acctRow">
		<label>{{Lang::get('public.DateTime')}} :</label>
		<input type="text" class="datepicker" id="deposit_date" style="cursor:pointer;width:70px;color:black;height:23px;" name="date" value="{{date('Y-m-d')}}">
				<select name="hours" style="width:70px;margin-left:3px;" >
				<option value="01">01</option>
				<option value="02">02</option>
				<option value="03">03</option>
				<option value="04">04</option>
				<option value="05">05</option>
				<option value="06">06</option>
				<option value="07">07</option>
				<option value="08">08</option>
				<option value="09">09</option>
				<option value="10">10</option>
				<option value="11">11</option>
				<option value="12">12</option>
				</select>
				<select name="minutes" style="width:70px;margin-left:3px;">
				<option value="00">00</option>
				<option value="01">01</option>
				<option value="02">02</option>
				<option value="03">03</option>
				<option value="04">04</option>
				<option value="05">05</option>
				<option value="06">06</option>
				<option value="07">07</option>
				<option value="08">08</option>
				<option value="09">09</option>
				<option value="10">10</option>
				<option value="11">11</option>
				<option value="12">12</option>
				<option value="13">13</option>
				<option value="14">14</option>
				<option value="15">15</option>
				<option value="16">16</option>
				<option value="17">17</option>
				<option value="18">18</option>
				<option value="19">19</option>
				<option value="20">20</option>
				<option value="21">21</option>
				<option value="22">22</option>
				<option value="23">23</option>
				<option value="24">24</option>
				<option value="25">25</option>
				<option value="26">26</option>
				<option value="27">27</option>
				<option value="28">28</option>
				<option value="29">29</option>
				<option value="30">30</option>
				<option value="31">31</option>
				<option value="32">32</option>
				<option value="33">33</option>
				<option value="34">34</option>
				<option value="35">35</option>
				<option value="36">36</option>
				<option value="37">37</option>
				<option value="38">38</option>
				<option value="39">39</option>
				<option value="40">40</option>
				<option value="41">41</option>
				<option value="42">42</option>
				<option value="43">43</option>
				<option value="44">44</option>
				<option value="45">45</option>
				<option value="46">46</option>
				<option value="47">47</option>
				<option value="48">48</option>
				<option value="49">49</option>
				<option value="50">50</option>
				<option value="51">51</option>
				<option value="52">52</option>
				<option value="53">53</option>
				<option value="54">54</option>
				<option value="55">55</option>
				<option value="56">56</option>
				<option value="57">57</option>
				<option value="58">58</option>
				<option value="59">59</option>
				</select>
				<select name="range" style="width:70px;margin-left:3px;">
				<option value="AM">AM</option>
				<option value="PM">PM</option>
				</select>
				
				
				<div class="clr"></div>
      </div>
	  <br>
	  <br>

	  <div class="acctRow">
      <label>{{Lang::get('public.ReferenceNo')}} :</label><input name="refno" type="text" style="color:black;width:272px;" />
      <div class="clr"></div>
      </div>
  
  
    <div class="acctRow">
		<label>{{Lang::get('public.DepositReceipt')}}  :</label><input class="upload" type="file" name="receipt" id="receipt">
		<div class="clr"></div>
    </div>
	  <br>
	  <br>

	<div class="acctRow">
		  <label>{{Lang::get('public.Promotion')}}* :</label>
		  <select name="promo" style="width:300px;">
		 
		    <option value="0"> {{Lang::get('public.IDoNotWantPromotion')}}</option>
			@foreach( $promos as $key => $promo )
			<option value="{{$promo['code']}}">{{$promo['name']}}</option>
			@endforeach
		  </select>
		  <div class="clr"></div>
    </div>
	
	  <input name="rules" value="1" type="hidden">
		 <br>
	    
      <div class="submitAcct" style="margin-left:185px;">
      <a id="deposit_sbumit_btn" href="javascript:void(0)" onclick="deposit_submit()">{{Lang::get('public.Submit')}}</a>
      </div>
      </div>
      <div class="clr"></div>
	  		 <br>
      <span class="failed_message acctTextReg" style="display:block;float:left;height:100%;margin-left:200px;"></span>
      </div>
      <!--ACCOUNT CONTENT-->


</form>



<div class="clr"></div>






</div>


@stop


