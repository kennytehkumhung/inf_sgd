@extends('ampm/master')

@section('title', 'Transaction Enquiry')


@section('content')
  <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
  <script>
	$(function() {
		$( ".datepicker" ).datepicker({ dateFormat: 'yy-mm-dd'  , defaultDate: new Date() });
		transaction_history();
	});
	  
	function transaction_history(){
	$.ajax({
		type: "POST",
		url: "{{action('User\TransactionController@transactionProcess')}}",
		data: {
			_token:   "{{ csrf_token() }}",
			date_from:		$('#date_from').val(),
			date_to:    	$('#date_to').val(),
			record_type:    $('#record_type').val()

		},
	}).done(function( json ) {

				//var str;
				 var str = '';
				str += '	<tr ><td>Reference No.</td><td>Date/Time</td><td>Type</td><td>Amount(IDR)</td><td>Status</td><td>Reason</td></tr>';
				
				 obj = JSON.parse(json);
				//alert(obj);
				 $.each(obj, function(i, item) {
					str +=  '<tr><td>'+item.id+'</td><td>'+item.created+'</td><td>'+item.type+'</td><td>'+item.amount+'</td><td>'+item.status+'</td><td>'+item.rejreason+'</td></tr>';
					
				})
					
				//alert(json);
				$('#trans_history').html(str);
				
		});
	}


	function updateTrans(){
		$( "#trans_history_button" ).trigger( "click" );
	}

	</script>
@include('ampm/top_manage', [ 'title' => Lang::get('public.TransactionHistory') ] )


       <!--ACCOUNT TITLE-->
     <!--ACCOUNT CONTENT-->
      <div class="acctContent">
      <span class="wallet_title"><i class="fa fa-undo"></i>{{Lang::get('public.PendingTransaction')}}</span>      <div class="acctRowR">
<div>
<table cellspacing="0" border="0">
<tbody><tr>
<td colspan="6">
<div class="noRecord">
{{Lang::get('public.NoRecordFound')}}
</div>
</td>
</tr>
</tbody></table>
</div>
<div class="clr"></div>
</div>
<span class="wallet_title"><i class="fa fa-file-o"></i>{{Lang::get('public.TransactionHistory')}}</span>
<div class="acctRow">
      <label>{{Lang::get('public.DateFrom')}} :</label><div class="cstInput1">
	  <input type="text" class="datepicker" id="date_from" style="cursor:pointer;color:black;" value="{{date('Y-m-d')}}">-
	  <input type="text" class="datepicker" id="date_to" style="cursor:pointer;color:black;" value="{{date('Y-m-d')}}">
	  </div>
      <div class="clr"></div>
      </div>
      <div class="acctRow ab1">
      <label>{{Lang::get('public.RecordType')}} :</label>
				<select id="record_type">
					<option value="0" selected>{{Lang::get('public.CreditAndDebitRecords')}}</option>
					<option value="1">{{Lang::get('public.CreditRecords')}}</option>
					<option value="2">{{Lang::get('public.DebitRecords')}}</option>
				</select>
      <div class="clr"></div>
      </div>
      <div class="acctRowR">
<div class="submitAcct">
<a id="trans_history_button" href="#" onClick="transaction_history()">{{Lang::get('public.Submit')}}</a>

</div>
      </div>
      
		<div class="clr"></div>
		<div >
			<table border='1' width='98%' id="trans_history" style="color:white;">
				<tr>
					<td>{{Lang::get('public.ReferenceNo')}}</td>
					<td>{{Lang::get('public.DateOrTime')}}</td>
					<td>{{Lang::get('public.type')}}</td>
					<td>{{Lang::get('public.Amount')}} </td>
					<td>{{Lang::get('public.Status')}}</td>
					<td>{{Lang::get('public.reason')}}</td>
				</tr>
				<tr>
					<td colspan="6"><h2><center>No Data Available!</center></h2></td>
				</tr>
			</table>
		</div>
		
      </div>
      <!--ACCOUNT CONTENT-->








<div class="clr"></div>






</div>



@stop
