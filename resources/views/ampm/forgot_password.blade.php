@extends('ampm/master')

@section('title', 'Forgot Password')



@section('content')

<script>
  function forgotpassword_submit(){

	$.ajax({
		type: "POST",
		url: "{{route('resetpassword')}}",
		data: {
			_token: "{{ csrf_token() }}",
			username:		 $('#fg_username').val(),
			email:    		 $('#fg_email').val(),

		},
	}).done(function( json ) {
			 $('.acctTextReg').html('');
			 obj = JSON.parse(json);
			 var str = '';
			 $.each(obj, function(i, item) {
				if( '{{Lang::get('COMMON.SUCESSFUL')}}' == item ){
					alert('{{Lang::get('COMMON.SUCESSFUL')}}');
					window.location.href = "{{route('homepage')}}";
				}else{
					$('.'+i+'_acctTextReg').html(item);
					
				}
			})
	
	});
}
  </script>

<div class="midContf">
<h2><i class="fa fa-lock"></i> {{Lang::get('public.ForgotPassword')}}</h2> 
     <p style="color:#fff;">
      We can help you reset your password and security info. First, enter your registered info and we will
send an account restoration link to your email
<br><br>
     </p>
<div class="acctContentReg">
       <div class="acctRow">
         <label>{{Lang::get('public.Username')}} :</label><input type="text" id="fg_username" > <span class="username_acctTextReg acctTextReg"></span>
      <div class="clr"></div>
    </div>
      <div class="acctRow">
      <label>{{Lang::get('public.EmailAddress')}} :</label><input type="text" id="fg_email" > <span class="email_acctTextReg acctTextReg"></span>
      <div class="clr"></div>
      </div>
      
      <div class="submitBtn" style="text-align: center;">
      <a href="#" onClick="forgotpassword_submit()" >&nbsp;</a>
      </div>
     </div>
</div>


@stop


