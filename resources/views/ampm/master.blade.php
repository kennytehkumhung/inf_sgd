<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
<link rel="shortcut icon" href="{{url()}}/ampm/img/favicon.ico" type="image/x-icon">
<link rel="icon" href="{{url()}}/ampm/img/favicon.ico" type="image/x-icon">
<title>AmpmPlay @yield('title')</title>
<meta name="keywords" content="@yield('keywords')"/>
<meta name="description" content="@yield('description')"/>	
<!--STYLE-->
<link href='https://fonts.googleapis.com/css?family=Cinzel:400,700' rel='stylesheet' type='text/css'>
<link href="{{url()}}/ampm/resources/css/style.css" rel="stylesheet" type="text/css" />
<link href="{{url()}}/ampm/resources/css/skitter.styles.min.css" rel="stylesheet" type="text/css" />
<link href="{{url()}}/ampm/resources/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="{{url()}}/ampm/resources/css/slick.css" rel="stylesheet" type="text/css" />
<link href="{{url()}}/ampm/resources/css/timetabs.css" rel="stylesheet" type="text/css" />
<link href="{{url()}}/ampm/resources/css/example-page.css" rel="stylesheet" type="text/css" />
<link href="{{url()}}/ampm/resources/css/jquery.dropdown.css" rel="stylesheet" type="text/css" />
@if( Auth::user()->check() )
<link href="{{url()}}/ampm/resources/css/acctMngmt.css" rel="stylesheet" type="text/css" />
<link href="{{url()}}/ampm/resources/css/jquery.bxslider.css" type="text/css" media="all" rel="stylesheet" />
@endif
<!--STYLE-->
<!--SCRIPT-->
<script src="{{url()}}/ampm/resources/js/jquery-2.1.3.min.js"></script>
<script src="{{url()}}/ampm/resources/js/jquery.skitter.min.js"></script>
<script src="{{url()}}/ampm/resources/js/jquery.easing.1.3.js"></script>
<script src="{{url()}}/ampm/resources/js/jquery.leanModal.min.js"></script>
<script src="{{url()}}/ampm/resources/js/jquery.timetabs.min.js"></script>
<script src="http://code.jquery.com/ui/1.9.1/jquery-ui.js"></script>
<script src="{{url()}}/ampm/resources/js/ddslick.js"></script>
<script src="{{url()}}/ampm/resources/js/jquery.tabSlideOut.v1.3.js"></script>
<script src="{{url()}}/ampm/resources/js/jquery.dropdown.min.js"></script>
<script type="text/javascript" language="javascript" src="{{url()}}/ampm/resources/js/jquery.bxslider.min.js"></script>
<!--SCRIPT-->
<script type="text/javascript">


function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
function enterpressalert(e, textarea){

	var code = (e.keyCode ? e.keyCode : e.which);
	 if(code == 13) { //Enter keycode
	   login();
	 }
}
function enterpressalert2(e, textarea){

	var code = (e.keyCode ? e.keyCode : e.which);
	 if(code == 13) { //Enter keycode
	   login2();
	 }
}
function login()
{
	$.ajax({
		type: "POST",
		url: "{{route('login')}}",
		data: {
			_token: "{{ csrf_token() }}",
			username: $('#username').val(),
			password: $('#password').val(),
			code:     $('#code').val(),
		},
	}).done(function( json ) {
			 obj = JSON.parse(json);
			 var str = '';
			 $.each(obj, function(i, item) {
				str += item + '\n';
			})
			if( "{{Lang::get('COMMON.SUCESSFUL')}}\n" == str ){
				location.reload(); 
			}else{
				alert(str);
			}
			
	});
}

function login2()
{
	$.ajax({
		type: "POST",
		url: "{{route('login')}}",
		data: {
			_token: "{{ csrf_token() }}",
			username: $('#username2').val(),
			password: $('#password2').val(),
			code:     $('#code2').val(),
		},
	}).done(function( json ) {
			 obj = JSON.parse(json);
			 var str = '';
			 $.each(obj, function(i, item) {
				str += item + '\n';
			})
			if( "{{Lang::get('COMMON.SUCESSFUL')}}\n" == str ){
				location.reload(); 
			}else{
				alert(str);
			}
			
	});
}

function register_popup(){
	
	$('#register').show();
}	
function register_close(){
	
	$('#register').hide();
}	

function register_submit(){
	 var dob = $('#dob_year').val()+'-'+$('#dob_month').val()+'-'+$('#dob_day').val();
	 
	$.ajax({
		type: "POST",
		url: "{{route('register_process')}}?dob="+dob+'&'+$('#register_form').serialize(),
	}).done(function( json ) {
			 $('.acctTextRegister').html('');
			 obj = JSON.parse(json);
			 var str = '';
			 $.each(obj, function(i, item) {
				if( '{{Lang::get('COMMON.SUCESSFUL')}}' == item ){
					alert('{{Lang::get('public.NewMember')}}');
					window.location.href = "{{route('homepage')}}";
				}else{
					$('.'+i+'_acctTextReg').html(item);
				}
			})
			
	});
}

@if (Auth::user()->check())
function update_mainwallet(){
	$.ajax({
		  type: "POST",
		  url: "{{route( 'mainwallet', [ '_token'=> csrf_token() ])}}",
		  beforeSend: function(balance){
		
		  },
		  success: function(balance){
			$('.main_wallet').html(parseFloat(Math.round(balance * 100) / 100).toFixed(2));
			total_balance += parseFloat(balance);
		  }
	 });
		
}
function getBalance(type){
	total_balance = 0;
	$.when(
	
		 $.ajax({
					  type: "POST",
					  url: "{{route('mainwallet', [ '_token'=> csrf_token()])}}",
					  beforeSend: function(balance){
				
					  },
					  success: function(balance){
						$('.main_wallet').html(parseFloat(Math.round(balance * 100) / 100).toFixed(2));
						total_balance += parseFloat(balance);
					  }
		 }),
		@foreach( Session::get('products_obj') as $prdid => $object)
		
	
				 $.ajax({
					  type: "POST",
					  url: "{{route( 'getbalance', [ '_token'=> csrf_token(), 'product'=> $object->code ])}}&rd=<?php echo rand ( 10000, 99999 ); ?>",
					  beforeSend: function(balance){
						$('.{{$object->code}}_balance').html('<img style="" src="{{url()}}/front/img/ajax-loading.gif" width="20" height="10">');
					  },
					  success: function(balance){
						  if( balance != '{{Lang::get('Maintenance')}}')
						  {
							$('.{{$object->code}}_balance').html(parseFloat(Math.round(balance * 100) / 100).toFixed(2));
							total_balance += parseFloat(balance);
						  }
						  else
						  {
							  $('.{{$object->code}}_balance').html(balance);
						  }
					  }
				 })
			  @if($prdid != Session::get('last_prdid')) 
				,
			  @endif
		@endforeach

		
	).then(function() {
			
			$('#total_balance').html(parseFloat(Math.round(total_balance * 100) / 100).toFixed(2));
			$('#top_total_balance').html(parseFloat(Math.round(total_balance * 100) / 100).toFixed(2));

	});

}
@endif

function slideup_loginbox(){
	
	$('.myaccountBox2').delay(100).fadeToggle();
	
}

$(document).ready(function () {
       
		
		  $('.bxslider').bxSlider({
            auto: true,
            autoControls: false,
			 hideControlOnEnd: false, 
			   pager: false,  
			   pagerSelector: null, 
			   controls: false, 
        });
    });

</script>

<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5770f9b45bc185a150656677/1amanc4nh';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>

<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function()
{ (i[r].q=i[r].q||[]).push(arguments)}

,i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-80273760-1', 'auto');
ga('send', 'pageview');

</script>
<style>

</style>

</head>
<body>
<!--Header-->



<div class="header">
<div class="headerInner">
<div class="logo">
<img src="{{url()}}/ampm/img/logo.png">
</div>
<div class="hrTopTime">
 <?php date_default_timezone_set("Asia/Bangkok");?>
{{date("d M Y")}}
{{substr(date("l"),0,3)}}
{{date(" H:i:s", time())}} (GMT +7)
</div>
<div class="cSelect">
<ul>
<li><a href="{{route( 'language', [ 'lang'=> 'en'])}}"><img src="{{url()}}/ampm/img/en-icon.png"></a></li>
<li><a href="{{route( 'language', [ 'lang'=> 'id'])}}"><img src="{{url()}}/ampm/img/indo-icon.png"></a></li>
</ul>
</div>

@if( !Auth::user()->check() )
	
<div class="adj2">
    <table cellspacing="5px" cellpadding="0" border="0" width="auto">
	<tbody>
            <tr>
                <td valign="middle"><div class="usr"><input type="text" placeholder="Username" name="" id="username2" onKeyPress="enterpressalert2(event, this)"></div></td>
                <td valign="middle"><div class="psw"><input type="password" placeholder="Password" name="" id="password2" onKeyPress="enterpressalert2(event, this)"></div></td>
                <td valign="middle"><img style="float: left;"  src="{{route('captcha', ['type' => 'login_captcha'])}}" width="60" height="24" /></td>
		<td valign="middle"><div class="cde"><input type="text" placeholder="Code" name="" id="code2" onKeyPress="enterpressalert2(event, this)"></div></td>
		<td width="65px" valign="middle"><div class="fp"><a href="{{route('forgotpassword')}}">Forgot<br>Password?</a></div></td>
		<td valign="middle"><div class="loginBtn"><a href="#signup"  onClick="login2()"></a></div></td>
                <td valign="middle"><div class="registerBtn"><a href="#signup"  onClick="register_popup()"></a></div></td>
	  </tr>
	</tbody>
    </table>
</div>

@endif

<!--<div class="livC">
<a onClick="window.open('http://bit.ly/ampm_livechat', 'live help', 'width=1150,height=830');" href="#">Live Help</a>
</div>-->
<div class="headerRight">
<div class="headerTop">

	@if( Auth::user()->check() )

		<div class="headerRightTable">
		<div class="adj2" style="right:300px;">
		<table width="auto" border="0" cellspacing="0" cellpadding="0">
		  <tr>
			
			<td valign="middle"><div class="welc"><em class="fa fa-user"></em> {{Lang::get('public.Welcome')}} {{Session::get('username')}} !<em style="margin-left:15px;" class="fa fa-ellipsis-v fa-1x"></em></div></td>
			<td valign="middle"><div class="welc1"><a href="#" data-dropdown="#dropdown-0"><i class="fa fa-caret-square-o-right"></i>{{Lang::get('public.Profile')}}</a></div>
			<div id="dropdown-0" class="dropdown dropdown-tip " >
				 <ul class="dropdown-menu" style="margin: 0px;">
					<li><a href="{{route('update-profile')}}">{{Lang::get('public.MyAccount')}}</a></li>
					<li><a href="{{route('update-password')}}">{{Lang::get('public.ChangePassword')}}</a></li>
					<li><a href="{{route('memberbank')}}">{{Lang::get('COMMON.CTABBANKSETTING')}}</a></li>
					<!--<li><a href="#4">Statement</a></li>-->
				 </ul>
				 </div></td>
			<td valign="middle"><div class="welc1"><a href="#" data-dropdown="#dropdown-1" onClick="getBalance()"><i class="fa fa-money"></i>{{Lang::get('public.Balance')}}</a></div>
			<div id="dropdown-1" class="dropdown dropdown-tip " >
			
			<div class="dropdown-menu wallcc">
			
				<div class="wallCont">
						<div class="wallIt head">{{Lang::get('public.Wallet')}}</div>
						<div class="wallIt head">{{Lang::get('public.Balance')}}</div>
						<div class="wallIt">{{Lang::get('public.MainWallet')}}</div>
						<div class="wallIt main_wallet">0.00</div>
					@foreach( Session::get('products_obj') as $prdid => $object)
						<div class="wallIt">{{$object->name}}</div>
						<div class="wallIt {{$object->code}}_balance" >0.00</div>
					@endforeach
						<div class="wallIt total" style="margin-top:4px;height:15px;"><strong id="">{{Lang::get('public.Total')}}</strong></span></div>
						<div class="wallIt total" style="margin-top:4px;height:15px;"><strong id="total_balance">00.00</strong></span></div>
				</div>
				
				<div class="clr"></div>
				
			
		
				 
				 </div>
				 
				 
				 </td>
			<td valign="middle">
		   <div class="welc1"> <a href="#" data-dropdown="#dropdown-2"><i class="fa fa-calculator"></i>{{Lang::get('public.Fund')}}</a></div>
			<div id="dropdown-2" class="dropdown dropdown-tip " >
				 <ul class="dropdown-menu" style="margin: 0px;">
					<li><a href="{{route('deposit')}}">{{Lang::get('public.Deposit')}}</a></li>
					<li><a href="{{route('withdraw')}}">{{Lang::get('public.Withdrawal')}}</a></li>
					<li><a href="{{route('transaction')}}">{{Lang::get('public.TransactionHistory')}}</a></li>
					<li><a href="{{route('transfer')}}">{{Lang::get('public.Transfer')}}</a></li>
					<!--<li><a href="{{route('transfer')}}">Multiple Transfer</a></li>-->
				 </ul>
				 </div></td>

			 <td valign="middle"><div class="logoutBtn"><a href="{{route('logout')}}"></a></div></td>
		   
		  </tr>
		  
		</table>
		</div>
		</div>


	@endif	
	
</div>

</div>
<div class="clr"></div>
<div class="navCont">
<ul>
<li>
<a href="{{route('homepage')}}"><img src="{{url()}}/ampm/img/home-icon-nav.png"></a>
</li>
<li>
<a href="{{route('livecasino')}}">{{Lang::get('public.LiveCasino')}}</a>
</li>
<li>
<a href="{{route('slot', [ 'type' => 'w88'  ])}}">{{Lang::get('public.SlotGames')}}</a>
</li>
<li>
<a href="{{route('sport')}}">{{Lang::get('public.Sports')}}</a>
</li>
<li>
<a href="{{route('promotion')}}">{{Lang::get('public.Promotion')}}</a>
</li>
<li>
<a href="{{route('mobile')}}">{{Lang::get('public.Mobile')}}</a>
</li>
<li>
<a href="{{route('contactus')}}">{{Lang::get('public.ContactUs')}}</a>
</li>
<li>
<a href="{{route('banking')}}">{{Lang::get('public.Banking')}}</a>
</li>



</ul>
</div>
</div>



</div>

<!--Mid Sect-->
<div class="midSect">
<div class="midSectInner">

<div class="midCont">
@if(!isset($website))
	<div class="anmnt">
	<div class="anmntInner">
	<marquee>{{App\Http\Controllers\User\AnnouncementController::index()}}</marquee>
	</div>
	</div>
@endif


@yield('content')





  <div class="clr"></div>
<!-- /container -->
</div>

<div class="clr"></div>
</div>

</div>
<!--Foooter-->
<div class="footer">
<div class="footerInner1">
<ul>
<li><a href="{{route('aboutus')}}">{{Lang::get('public.AboutUs')}}</a></li>
<li><a href="{{route('faq')}}">{{Lang::get('public.FAQ')}}</a></li>
<li class="ftlast"><a href="{{route('tnc')}}">{{Lang::get('public.TNC')}}</a></li>
</ul>
</div>

<div class="footerInner3">
<div class="vend">
<div class="venTit">
Powered by
</div>
<img src="{{url()}}/ampm/img/powered by.png" width="425" height="45">


</div>

<div class="bk">
<div class="venTit">
{{Lang::get('COMMON.PAYMENTMETHOD')}}
</div>
<img src="{{url()}}/ampm/img/logo all bank.png" width="350" height="28">



</div>

<div class="clr"></div>
</div>

</div>
<div id="register" style="display: none; position: fixed; opacity: 1; z-index: 11000; left: 57%; margin-left: -282.5px; top: 150px;">
			<div id="register-ct">
				<div id="register-header">
					<h2>{{Lang::get('public.Step1word')}}</h2>
					<p>It's simple, and free.</p>
					<a class="modal_close" href="#" onClick="register_close()" ></a>
				</div>
				
				<form action="" id="register_form" >
     
				  <div class="txt-fld">
				    <label for="">{{Lang::get('public.EmailAddress')}}</label>
				    <input id="" name="email" type="text" />
					<br>
					<span class="email_acctTextReg acctTextRegister" style="color: Red ! important; font-size: small; display: inline;"></span>
				  </div>
				  <div class="txt-fld">
				    <label for="">{{Lang::get('public.ContactNo')}}.*</label>
				    <span style="float: left;color: #000;margin-top: 8px;margin-left: 30px;">+62 -</span><input style="float: left; width: 100px !important;" id="" name="mobile" type="text" />
					<br>
					<span class="mobile_acctTextReg acctTextRegister" style="color: Red ! important; font-size: small; display: inline;"></span>
                    <div class="clr"></div>
				  </div>
                  <div class="txt-fld">
				    <label for="">{{Lang::get('public.Currency')}}*</label>
				    <span style="float: left;color: #000;margin-top: 8px;margin-left: 30px;">+IDR</span>
                    <div class="clr"></div>
				  </div>
                  <div class="txt-fld">
				    <label for="">{{Lang::get('public.FullName')}}*</label>
				    <input id="" name="fullname" type="text" />
 	

*Name must be matched with withdrawal bank account 
						<br>
					<span class="fullname_acctTextReg acctTextRegister" style="color: Red ! important; font-size: small; display: inline;"></span>
				  </div>
                  <!--
                 <div class="txt-fld">
				    <label for="">Gender</label>
				    
                    <select class="good_input"></select>
 	
				  </div>
                  -->
				  
                  <div class="txt-fld">
					 <label>{{Lang::get('public.DOB')}} :</label>
					  <select style="margin-right: 8px;" name="day" id="dob_day">
						<option value="01">01</option>
						<option value="02">02</option>
						<option value="03">03</option>
						<option value="04">04</option>
						<option value="05">05</option>
						<option value="06">06</option>
						<option value="07">07</option>
						<option value="08">08</option>
						<option value="09">09</option>
						<option value="10">10</option>
						<option value="11">11</option>
						<option value="12">12</option>
						<option value="13">13</option>
						<option value="14">14</option>
						<option value="15">15</option>
						<option value="16">16</option>
						<option value="17">17</option>
						<option value="18">18</option>
						<option value="19">19</option>
						<option value="20">20</option>
						<option value="21">21</option>
						<option value="22">22</option>
						<option value="23">23</option>
						<option value="24">24</option>
						<option value="25">25</option>
						<option value="26">26</option>
						<option value="27">27</option>
						<option value="28">28</option>
						<option value="29">29</option>
						<option value="30">30</option>
						<option value="31">31</option>
					  </select>
					  <select style="margin-right: 8px;" name="month" id="dob_month">
						<option value="01">01</option>
						<option value="02">02</option>
						<option value="03">03</option>
						<option value="04">04</option>
						<option value="05">05</option>
						<option value="06">06</option>
						<option value="07">07</option>
						<option value="08">08</option>
						<option value="09">09</option>
						<option value="10">10</option>
						<option value="11">11</option>
						<option value="12">12</option>
					  </select>
					  <select name="year" id="dob_year">
						<option value="1995">1995</option>
						<option value="1994">1994</option>
						<option value="1993">1993</option>
						<option value="1992">1992</option>
						<option value="1991">1991</option>
						<option value="1990">1990</option>
						<option value="1989">1989</option>
						<option value="1988">1988</option>
						<option value="1987">1987</option>
						<option value="1986">1986</option>
						<option value="1985">1985</option>
						<option value="1984">1984</option>
						<option value="1983">1983</option>
						<option value="1982">1982</option>
						<option value="1981">1981</option>
						<option value="1980">1980</option>
						<option value="1979">1979</option>
						<option value="1978">1978</option>
						<option value="1977">1977</option>
						<option value="1976">1976</option>
						<option value="1975">1975</option>
						<option value="1974">1974</option>
						<option value="1973">1973</option>
						<option value="1972">1972</option>
						<option value="1971">1971</option>
						<option value="1970">1970</option>
						<option value="1969">1969</option>
						<option value="1968">1968</option>
						<option value="1967">1967</option>
						<option value="1966">1966</option>
						<option value="1965">1965</option>
						<option value="1964">1964</option>
						<option value="1963">1963</option>
						<option value="1962">1962</option>
						<option value="1961">1961</option>
						<option value="1960">1960</option>
						
					  </select>
		  
					<br>
					<span class="dob_acctTextReg acctTextRegister" style="color: Red ! important; font-size: small; display: inline;"></span>
				  </div>
                  
                  <div class="txt-fld">
				    <label for="">{{Lang::get('public.Username')}}*</label>
				    <input id="" class="good_input" name="username" type="text" />
					<br>
					<span class="username_acctTextReg acctTextRegister" style="color: Red ! important; font-size: small; display: inline;"></span>
				  </div>
                  
                  <div class="txt-fld">
				    <label for="">{{Lang::get('public.Password')}}*</label>
				    <input id="" class="good_input" name="password" type="password" />
					<br>
					<span class="password_acctTextReg acctTextRegister" style="color: Red ! important; font-size: small; display: inline;"></span>
				  </div>
                  
                  <div class="txt-fld">
				    <label for="">{{Lang::get('public.ConfirmPassword')}}*</label>
				    <input id="" class="good_input" name="repeatpassword" type="password" />
 	{{Lang::get('public.PasswordPolicyContent')}} 
						<br>
					<span class="repeatpassword_acctTextReg acctTextRegister" style="color: Red ! important; font-size: small; display: inline;"></span>
				  </div>
				  <div class="txt-fld">
				    <label for="">Code</label><img style="float: left;margin-left:6px;"  src="{{route('captcha', ['type' => 'register_captcha'])}}" width="60" height="27" />
				    <input style="float: left; width: 100px !important;" id="" name="code" type="text" />
					<span class="code_acctTextReg acctTextRegister" style="color: Red ! important; font-size: small; display: inline;"></span>
                    <div class="clr"></div>*{{Lang::get('public.RequiredFields')}} 
				  </div>
                  
                  <div class="txt-fld">
				    <span style="float: right;"><input style="float: left;width: 15px;" type="checkbox">{{Lang::get('public.AtLeast18AndRead')}} </span>
				    
                    
                    <div class="clr"></div>
				  </div>
                  

				  <div class="btn-fld">
				  <button type="button" onClick="register_submit()" >{{Lang::get('public.Submit')}} &raquo;</button>
</div>
				 </form>
			</div>
		</div>
<div id="signup">
			<div id="signup-ct">
				<div id="signup-header">
					<h2><img src="{{url()}}/ampm/img/logo.png"></h2>
					<p>If you encounter any issues while logging in, please contact our Customer Service for further assistance. </p>
					<a class="modal_close" href="#"></a>
				</div>
				
				<form action="">
     
				  <div class="txt-fld">
				    <label for="">Username</label>
				    <input id="username" class="good_input" name="" type="text" onKeyPress="enterpressalert(event, this)" />

				  </div>
				  <div class="txt-fld">
				    <label for="">Password</label>
				    <input id="password" name="" type="password" onKeyPress="enterpressalert(event, this)" />
				  </div>
				  <div class="txt-fld">
				    <label for="">Code</label><img src="{{route('captcha', ['type' => 'login_captcha'])}}">
				    <input style="float: left; width: 100px !important;" id="code" name="" type="text" onKeyPress="enterpressalert(event, this)" />
                    <div class="clr"></div>
				  </div>
                  <div class="txt-fld">
				    <a style="color: #000;font-size: 12px;" href="#">Forgot Password?</a>
                    <div class="clr"></div>
				  </div>
				  <div class="btn-fld">
				  <button type="button" onClick="login()"><a href="index_logout.html">Login &raquo;</a></button>
</div>
				 </form>
			</div>
		</div>
        
<div id="mxbMob">
			<div id="signup-ct">
				<div id="mxbMob-header">
              <h2>Maxbet Suite - Android</h2>
					<a class="modal_close" href="#"></a>
				</div>
                <div class="mobQr">
				<img src="{{url()}}/ampm/img/qr-sample.jpg" width="100" height="100">
                </div>
                <div class="mobBtn">
                <a href="#">CASINO DOWNLOAD</a>
                </div>
                <form action="">
                <table width="90%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="100px"><label for="">Account</label></td>
    <td>724_club</td>
  </tr>
  <tr>
    <td><label for="">Password</label></td>
    <td>XXXXXX</td>
  </tr>
  <tr>
    <td><label for="">Change Password</label></td>
    <td>
    <div class="leftDD"><input id="" name="" type="text" /></div>
    <div class="leftDD"><button type="submit"><a href="index_logout.html">Submit</a></button></div>
    <div class="clr"></div>
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>*Min 6 characters and Max 10 characters</td>
  </tr>
</table>

     

				  
				  
                  
				  
				 </form>
			</div>
		</div>
        
        <div id="agMob">
			<div id="signup-ct">
				<div id="mxbMob-header">
              <h2>AG Suite - Android</h2>
					<a class="modal_close" href="#"></a>
				</div>
                <div class="mobQr">
				<img src="{{url()}}/ampm/img/qr-sample.jpg" width="100" height="100">
                </div>
                <div class="mobBtn">
                <a href="#">CASINO DOWNLOAD</a>
                </div>
                <form action="">
                <table width="90%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="100px"><label for="">Account</label></td>
    <td>724_club</td>
  </tr>
  <tr>
    <td><label for="">Password</label></td>
    <td>XXXXXX</td>
  </tr>
  <tr>
    <td><label for="">Change Password</label></td>
    <td>
    <div class="leftDD"><input id="" name="" type="text" /></div>
    <div class="leftDD"><button type="submit"><a href="index_logout.html">Submit</a></button></div>
    <div class="clr"></div>
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>*Min 6 characters and Max 10 characters</td>
  </tr>
</table>

     

				  
				  
                  
				  
				 </form>
			</div>
		</div>
        
        <div id="ptMob">
			<div id="signup-ct">
				<div id="mxbMob-header">
              <h2>Playtech Suite - Android</h2>
					<a class="modal_close" href="#"></a>
				</div>
                <div class="mobQr">
				<img src="{{url()}}/ampm/img/qr-sample.jpg" width="100" height="100">
                </div>
                <div class="mobBtn">
                <a href="#">CASINO DOWNLOAD</a>
                </div>
                <form action="">
                <table width="90%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="100px"><label for="">Account</label></td>
    <td>724_club</td>
  </tr>
  <tr>
    <td><label for="">Password</label></td>
    <td>XXXXXX</td>
  </tr>
  <tr>
    <td><label for="">Change Password</label></td>
    <td>
    <div class="leftDD"><input id="" name="" type="text" /></div>
    <div class="leftDD"><button type="submit"><a href="index_logout.html">Submit</a></button></div>
    <div class="clr"></div>
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>*Min 6 characters and Max 10 characters</td>
  </tr>
</table>

     

				  
				  
                  
				  
				 </form>
			</div>
		</div>
        
        <div id="gpMob">
			<div id="signup-ct">
				<div id="mxbMob-header">
              <h2>Gameplay Suite - Android</h2>
					<a class="modal_close" href="#"></a>
				</div>
                <div class="mobQr">
				<img src="{{url()}}/ampm/img/qr-sample.jpg" width="100" height="100">
                </div>
                <div class="mobBtn">
                <a href="#">CASINO DOWNLOAD</a>
                </div>
                <form action="">
                <table width="90%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="100px"><label for="">Account</label></td>
    <td>724_club</td>
  </tr>
  <tr>
    <td><label for="">Password</label></td>
    <td>XXXXXX</td>
  </tr>
  <tr>
    <td><label for="">Change Password</label></td>
    <td>
    <div class="leftDD"><input id="" name="" type="text" /></div>
    <div class="leftDD"><button type="submit"><a href="index_logout.html">Submit</a></button></div>
    <div class="clr"></div>
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>*Min 6 characters and Max 10 characters</td>
  </tr>
</table>

     

				  
				  
                  
				  
				 </form>
			</div>
		</div>

<div id="alMob">
			<div id="signup-ct">
				<div id="mxbMob-header">
              <h2>Allbet Suite - Android</h2>
					<a class="modal_close" href="#"></a>
				</div>
                <div class="mobQr">
				<img src="{{url()}}/ampm/img/qr-sample.jpg" width="100" height="100">
                </div>
                <div class="mobBtn">
                <a href="#">CASINO DOWNLOAD</a>
                </div>
                <form action="">
                <table width="90%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="100px"><label for="">Account</label></td>
    <td>724_club</td>
  </tr>
  <tr>
    <td><label for="">Password</label></td>
    <td>XXXXXX</td>
  </tr>
  <tr>
    <td><label for="">Change Password</label></td>
    <td>
    <div class="leftDD"><input id="" name="" type="text" /></div>
    <div class="leftDD"><button type="submit"><a href="index_logout.html">Submit</a></button></div>
    <div class="clr"></div>
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>*Min 6 characters and Max 10 characters</td>
  </tr>
</table>

     

				  
				  
                  
				  
				 </form>
			</div>
		</div>
        

        

        

        
        <!-- Just use this for popout -->
<div class="slide-out-div1" style="display: none;">
        <a class="handle1" href="http://link-for-non-js-users">Content</a>
        <div class="mbdl">
        <a id="go" rel="leanModal" name="mxbMob" href="#mxbMob">
        <img src="{{url()}}/ampm/img/tab-mxb-icon.png" width="140" height="45"> </a>
        <div class="mbdlTxt">
        <a id="go" rel="leanModal" name="mxbMob" href="#mxbMob">
        Suite
        </a>
        </div>
        </div>
        
        <div class="mbdl">
        <a id="go" rel="leanModal" name="mxbMob" href="#agMob">
        <img src="{{url()}}/ampm/img/tab-ag-icon.png" width="140" height="45"> </a>
        <div class="mbdlTxt">
        <a id="go" rel="leanModal" name="mxbMob" href="#agMob">
        Suite
        </a>
        </div>
        </div>
        
        <div class="mbdl">
        <a id="go" rel="leanModal" name="mxbMob" href="#ptMob">
        <img src="{{url()}}/ampm/img/tab-pt-icon.png" width="140" height="45"> </a>
        <div class="mbdlTxt">
        <a id="go" rel="leanModal" name="mxbMob" href="#ptMob">
        Suite
        </a>
        </div>
        </div>
        
        <div class="mbdl">
        <a id="go" rel="leanModal" name="mxbMob" href="#gpMob">
        <img src="{{url()}}/ampm/img/tab-gp-icon.png" width="140" height="45"> </a>
        <div class="mbdlTxt">
        <a id="go" rel="leanModal" name="mxbMob" href="#gpMob">
        Suite
        </a>
        </div>
        </div>
        
        <div class="mbdl">
        <a id="go" rel="leanModal" name="mxbMob" href="#alMob">
        <img src="{{url()}}/ampm/img/tab-al-icon.png" width="140" height="45"> </a>
        <div class="mbdlTxt">
        <a id="go" rel="leanModal" name="mxbMob" href="#alMob">
        Suite
        </a>
        </div>
        </div>
        
                </div>

    </div>
    
    <div class="slide-out-div" style="display: none;">
        <a class="handle" href="http://link-for-non-js-users">Content</a>
       <img src="{{url()}}/ampm/img/ct-bx-header.png" width="190" height="113">
      <div class="ct-bx">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><img src="{{url()}}/ampm/img/ct-bx-mail.png" width="30" height="30"></td>
    <td>Email:cs@724club.com</td>
  </tr>
  <tr>
    <td><img src="{{url()}}/ampm/img/ct-bx-phone.png" width="30" height="30"></td>
    <td>TEL: 018-8888888</td>
  </tr>
  <tr>
    <td><img src="{{url()}}/ampm/img/ct-bx-phone.png" width="30" height="30"></td>
    <td>TEL: 018-8888888</td>
  </tr>
  <tr>
    <td><img src="{{url()}}/ampm/img/ct-bx-skype.png" width="30" height="30"></td>
    <td>WECHAT ID: 724CLUB</td>
  </tr>
</table>
<div class="ct-qr">
<img src="{{url()}}/ampm/img/qr.png" width="100" height="100">
</div>
<div class="ct-submit">
<a href="#">Chat Now</a>
</div>
</div>

    </div>

<script type="text/javascript">
			$(function() {
    			$('a[rel*=leanModal]').leanModal({ top : 200, closeButton: ".modal_close" });		
			});
		</script>
<script>
$('#tabs')
    .tabs()
    .addClass('ui-tabs-vertical ui-helper-clearfix');
</script>
<script type="text/javascript" language="javascript">
    $(document).ready(function() {
      $('.box_skitter_large').skitter({
        theme: 'clean',
        numbers_align: 'center',
		controls: false,
		hideTools: true,
		label: false,
		numbers: false,
        preview: true
      });
    });
  </script>
  
<script src="{{url()}}/ampm/resources/js/slick.min.js"></script>
<script type="text/javascript">
jQuery(document).ready(function(){
	
	jQuery('dl#tabs4').addClass('enabled').timetabs({
		defaultIndex: 0,
		interval: 7000,
		continueOnMouseLeave: true,
		animated: 'fade',
		animationSpeed: 500
	});

	// animation preview
	jQuery('input[name=animation]').click(function() {
		$this = jQuery(this);
		jQuery.fn.timetabs.switchanimation($this.val());
	});
});
</script>
<script>
         $(function(){
             $('.slide-out-div1').tabSlideOut1({
                 tabHandle: '.handle1',                              //class of the element that will be your tab
                 pathToTabImage: '{{url()}}/ampm/img/mdTab.png',          //path to the image for the tab (optionaly can be set using css)
                 imageHeight: '257px',                               //height of tab image
                 imageWidth: '55px',                               //width of tab image    
                 tabLocation: 'left',                               //side of screen where tab lives, top, right, bottom, or left
                 speed: 300,                                        //speed of animation
                 action: 'click',                                   //options: 'click' or 'hover', action to trigger animation
                 topPos: '200px',                                   //position from the top
                 fixedPosition: false,
				 onLoadSlideOut: false                               //options: true makes it stick(fixed position) on scroll
             });
         });
		 
		 $(function(){
             $('.slide-out-div').tabSlideOut({
                 tabHandle: '.handle',                              //class of the element that will be your tab
                 pathToTabImage: '{{url()}}/ampm/img/ctTab.png',          //path to the image for the tab (optionaly can be set using css)
                 imageHeight: '327px',                               //height of tab image
                 imageWidth: '55px',                               //width of tab image    
                 tabLocation: 'right',                               //side of screen where tab lives, top, right, bottom, or left
                 speed: 300,                                        //speed of animation
                 action: 'click',                                   //options: 'click' or 'hover', action to trigger animation
                 topPos: '200px',                                   //position from the top
                 fixedPosition: false,
				 onLoadSlideOut: false                               //options: true makes it stick(fixed position) on scroll
             });
         });


         </script>


</body>
</html>
