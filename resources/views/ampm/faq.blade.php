@extends('ampm/master')

@section('title', 'FAQ')


@section('content')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script>
$(document).ready(function($){
	$(".accordion-contentAMPM").not($(this).next()).slideUp('');
		$('#accordionAMPM').find('.accordion-toggleAMPM').click(function(){

                //Expand or collapse this panel
                $(this).next().slideToggle('slow');

                //Hide the other panels
                $(".accordion-contentAMPM").not($(this).next()).slideUp('slow');

        });   
		
});
</script>

<style>
h4{color:#f1d97b;}
</style>

<div class="midSect">
<div class="midSectContent">

@if( Lang::getLocale() == 'en' )
		
    <br>
    <br>
	<h5>FAQ</h5>
<div id="accordionAMPM" style="cursor:pointer;">	
	<h4 class="accordion-toggleAMPM">1. How do I register a Player's account with AMPMPlay?</h4>
	<div class="accordion-contentAMPM default">
	<ul class="ululli" style=" list-style-type: none;width:700px;margin-left: 5px; padding: 5px 10px;color:white;">
		<li>
		<p>We have made it extremely easy for any user to set up a new Player account with us on AMPMPlay. Just follow the simple steps as detailed below to get started on your winning opportunities.</p>
		<br/>
		<table width="95%" border="0" cellspacing="0" cellpadding="0">
		<tr>
		<td colspan="2"><strong><u>REGISTRATION GUIDELINES   </u></strong>    </td>
		</tr>
		<tr>
		<td width="15%" valign="top"><strong>STEP 1</strong>      </td>
		<td width="85%">Just click on the “My Account” button found at the home page. Clicking this button will show you  Form the official registration form on the AMPMPlay website.</td>
		</tr>
		<tr>
		<td valign="top"><strong>STEP 2</strong>      </td>
		<td>Simply follow the instructions and provide the necessary information as stipulated in each field. The necessary information includes these mandatory information:<br/>
		<br/>
		<em><strong>Email Address </strong></em>
		<p>A valid and functional email address that is supplied by you. This information will be used in the future when for further communications or promotions.</p>
		<br/>
		<em><strong>Password</strong></em>
		<p>A sequence of 6 – 12 case sensitive characters (letters, numbers, symbols) used as secret key that’s mandatory for logging into your Player account.</p>
		</td>
		</tr>
		<tr>
		<td valign="top"><strong>STEP 3</strong>      </td>
		<td>
		After successfully completing and submitting the registration form, Please Login.
		</td>
		</tr>
		<tr>
		<td><strong>STEP 4</strong>     </td>
		<td>
		Deposit of minimum IDR 50,000 to your AMPMPlay Player account.
		</td>
		</tr>
		<tr>
		<td><strong>STEP 5</strong>      </td>
		<td>Submit your deposit form.</td>
		</tr>
		<tr>
		<td valign="top"><strong>STEP 6</strong>      </td>
		<td>Upon confirmation of receipt of your deposit by AMPMPlay customer care representative, you can start playing any game from AMPMPlay wide variety of casino games!</td>
		</tr>
		</table>
		<br/>
		<p>Please note that any user of AMPMPlay must be 21 years old and above. By submitting your registration, the user understands and agrees to be bound by the stipulated General Terms and Conditions of the Website.</p>
		</li>
	</ul>
	</div>
	
	<h4 class="accordion-toggleAMPM">2. How do I update, change and/or complete my registered account details?</h4>
	<div class="accordion-contentAMPM default">
	<p>Please log into your Player account, click on "Profile" and proceed to update/change/complete the details of your profile. Once the necessary changes have been made, click the "Save" button to complete the update.</p>
	</div>
	
	<h4 class="accordion-toggleAMPM">3. How do I change/update my password?</h4>
	<div class="accordion-contentAMPM default">
	<p>To change/update your password, log into your Player account and click on "Profile", followed by the "Change Pasword" tab.Key in your new desired password and click the "Update" button to complete the update.</p>
	</div>
	
	<h4 class="accordion-toggleAMPM">4. How do I do if I forget my password?</h4>
	<div class="accordion-contentAMPM default">
	<p>To reset your password, click on the "Forget Password" button on the home page. A new temporary password will be sent to your registered email’s inbox. Using that particular password, you will be log into your Player account and it is advisable for you to immediately update your password.</p>
	</div>
	
	<h4 class="accordion-toggleAMPM">5. How should I play on AMPMPlay instead of on other similar sites?</h4>
	<div class="accordion-contentAMPM default">
	<p>AMPMPlay differentiates and sets itself above the rest by attaining exclusive partnerships with all its AMPMiliates and partners. These then allows us at AMPMPlay the wonderful opportunity to offer the best values and highest rebates available in the market!</p>
	</div>
	
</div>
@else
	
	<br>
    <br>
	<h5>FAQ</h5>
<div id="accordionAMPM" style="cursor:pointer;">	
	<h4 class="accordion-toggleAMPM">1. Bagaimana saya mendaftar di AMPMPlay?</h4>
	<div class="accordion-contentAMPM default">
	<ul class="ululli" style="list-style-type: none;width:700px;margin-left: 5px; padding: 5px 10px;color:white;">
		<li>
		<p>Kami telah membuat cara pendaftaran yang sangat mudah bagi semua pengguna untuk mengatur akun Pemain baru dengan kami AMPMPlay. Cukup ikuti langkah-langkah sederhana seperti yang dijelaskan di bawah ini untuk memulai peluang menang Anda.</p>
		<br/>
		<table width="95%" border="0" cellspacing="0" cellpadding="0">
		<tr>
		<td colspan="2"><strong><u>PEDOMAN PENDAFTARAN</u></strong>    </td>
		</tr>
		<tr>
		<td width="15%" valign="top"><strong>Langkah 1</strong>      </td>
		<td width="85%">Cukup klik "My Account" tombol yang ditemukan di halaman beranda. Mengklik tombol ini akan keluar formulir pendaftaran resmi di situs AMPMPlay.</td>
		</tr>
		<tr>
		<td valign="top"><strong>Langkah 2</strong>      </td>
		<td>Cukup ikuti petunjuk dan berikan informasi yang diperlukan sebagaimana diatur dalam masing-masing bidang. Informasi yang diperlukan termasuk:<br/>
		<br/>
		<em><strong>Alamat Email </strong></em>
		<p>Sebuah alamat email yang aktif dan fungsional yang diberikan oleh Anda. Informasi ini akan digunakan di masa depan ketika masuk ke rekening pemain serta untuk komunikasi lebih lanjut atau promosi.</p>
		<br/>
		<em><strong>Kata Sandi</strong></em>
		<p>Sebuah urutan 6-12 karakter (huruf, angka, simbol) digunakan sebagai kunci rahasia yang wajib untuk login ke akun Pemain Anda.</p>
		</td>
		</tr>
		<tr>
		<td valign="top"><strong>Langkah 3</strong>      </td>
		<td>
		Setelah berhasil menyelesaikan dan menyerahkan formulir pendaftaran, Silahkan Login.
		</td>
		</tr>
		<tr>
		<td><strong>Langkah 4</strong>     </td>
		<td>
		Deposit minimal IDR 50.000 ke akun AMPMPlay pemain Anda.
		</td>
		</tr>
		<tr>
		<td><strong>Langkah 5</strong>      </td>
		<td>Kirim formulir deposit Anda.</td>
		</tr>
		<tr>
		<td valign="top"><strong>Langkah 6</strong>      </td>
		<td>Setelah deposit Anda dikonfirmasi melalui perwakilan layanan pelanggan AMPMPlay, Anda dapat mulai bermain permainan apapun di AMPMPlay.</td>
		</tr>
		</table>
		<br/>
		<p>Harap dicatat bahwa setiap pengguna untuk AMPMPlay harus berusia 18 tahun ke atas. Dengan mengirimkan pendaftaran Anda, pengguna memahami dan setuju untuk terikat dengan Syarat Umum yang ditetapkan dan Ketentuan Website.</p>
		</li>
	</ul>
	</div>

	<h4 class="accordion-toggleAMPM">2. Bagaimana cara memperbarui, mengubah dan / atau menyelesaikan detil akun terdaftar saya?</h4>
	<div class="accordion-contentAMPM default">
	<p>Silahkan login ke akun pemain Anda, klik pada "Profil" dan selanjutnya silahkan untuk memperbarui / mengubah / menyelesaikan rincian profil Anda. Setelah perubahan yang diperlukan telah dibuat, klik tombol "Simpan" untuk menyelesaikan memperbarui.</p>
	</div>
	
	<h4 class="accordion-toggleAMPM">3. Bagaimana saya merubah/memperbaruhi kata sandi saya?</h4>
	<div class="accordion-contentAMPM default">
	<p>Untuk mengubah / memperbarui sandi Anda, login ke akun pemain Anda dan klik "Profil" lalu pilih “Change Password”, isi password baru sesuai dengan yang anda inginkan dan klik "Update" tombol untuk menyelesaikan memperbarui.</p>
	</div>

	<h4 class="accordion-toggleAMPM">4. Apa yang harus saya lakukan jika saya lupa kata sandi saya?</h4>
	<div class="accordion-contentAMPM default">
	<p>Untuk mereset kata sandi Anda, klik pada "Lupa Kata Sandi" tombol di halaman beranda. Sandi sementara baru akan dikirim ke email yang terdaftar. Menggunakan kata sandi tertentu, Anda akan login ke akun pemain Anda dan dianjurkan bagi Anda untuk segera memperbarui sandi Anda.</p>
	</div>
	
	<h4 class="accordion-toggleAMPM">5. Kenapa saya harus bermain di AMPMPlay daripada di laman web lain?</h4>
	<div class="accordion-contentAMPM default">
	<p>AMPM membedakan dan menempatkan dirinya di atas sisanya dengan mencapai kemitraan eksklusif dengan semua afiliasi dan mitra. Ini kemudian memungkinkan AMPM memberikan kesempatan baik untuk menawarkan nilai-nilai terbaik dan keuntungan tertinggi yang tersedia di pasar.</p>
	</div>

	</div>
@endif
</div>
</div>
@stop

