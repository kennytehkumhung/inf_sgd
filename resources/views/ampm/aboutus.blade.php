@extends('ampm/master')

@section('title', 'Maintenance')


@section('content')

<div class="midSect">
<div class="midSectContent">

@if( Lang::getLocale() == 'en' )
	<h5>ABOUT AMPMPlay</h5>
	<p>Here at AMPM, the gaming experience is unlike any other. AMPM interface is user-friendly and easy to navigate, which allows our members extreme ease of access. AMPM was designed to also be as welcoming as it looks so you’ll feel right at home with us. AMPM prioritizes both the immersive gaming experience as well as the opportunity to earn real winnings that players can cash out on. We have combined state-of-the-art technology with a range of truly exciting online casino gameplay, as well as safe and secure online payouts to ensure your peace of mind. Additionally, our funds transfer process has been designed to provide all members with a quality experience. We value the loyalty of our members and nothing matters to us more than the satisfaction of our members on all legal fronts.</p>

	<p>AMPM offers a wide range of popular and innovative opportunities for everyone to make the best out of their bets:
	LIVE CASINO	Step into our live online casino any time of the day or night and be served by live dealers across 5 luxury suites.
	SPORTS	Capitalize on your innate sixth sense by making your best match predictions and placing winning sports bets.
	GAMES	Enjoy a more relaxed gaming experience with our wide range of online games (such as slots and arcade games) while still enjoying the opportunity to cash in on big wins.</p>

	<p>The AMPM team is always ready and willing to assist all valued members. If you have any enquiries or feedback, our Customer Service representatives can be contacted via Live Chat.</p>

	<p>For the latest updates, announcements and promotions, ‘LIKE’ us on Facebook and ‘FOLLOW’ us on Twitter!</p>
@else
	
	<h5>Tentang AMPMPlay</h5>
	<p>Pengalaman bermain di AMPM tidak sama seperti di tempat lain. AMPM selalu berusaha agar dekat dengan Pemain dan mudah untuk digunakan, yang memungkinkan semua pemain kami merasa nyaman. AMPM juga dirancang untuk membuat anda merasa bermain di rumah bersama kami. AMPM memprioritaskan pengalaman bermain yang nyata serta kesempatan yang lebih besar untuk menang. Kami telah menggabungngkan berbagai teknologi dari setiap Negara untuk membuat permainan kasino kami menjadi lebih menarik, didukung dengan pembayaran online yang aman dan nyaman untuk anda semua. Proses transfer dana kami telah dirancang untuk memberikan semua pemain kami kemudahan dalam bertransaksi. Kami menghargai loyalitas pemain kami dan tidak ada yang lebih penting bagi kami selain kepuasan semua pemain kami.</p>

	<p>AMPM memberikan kesempatan dan peluang yang lebar bagi setiap orang untuk melakukan yang terbaik dari taruhan mereka:
	LIVE CASINO	Nikmati Live Casino kami di siang atau malam hari selama 24 jam nonstop dan ditemani oleh dealer cantik di berbagai suite mewah yang berbeda.
	SPORTS	Manfaatkan kemampuan prediksi anda untuk membuat prediksi terbaik dan menempatkan taruhan anda.
	GAMES	Nikmati pengalaman bermain game lebih santai dengan banyaknya pilihan game online (seperti slot machine dan permainan arcade) sementara masih menikmati kesempatan besar untuk memenangkan uang tunai.</p>

	<p>Tim AMPM selalu siap dan bersedia untuk membantu semua member kami. Jika anda memiliki pertanyaan dan saran, customer servis kami dapat dihubungi melalui Live Chat.</p>

	<p>Untuk pengumuman dan promosi terbaru dari kami, ‘LIKE’ Facebook dan ‘FOLLOW’ Twitter kami.</p>

@endif
</div>
</div>
@stop

