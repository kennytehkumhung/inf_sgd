@extends('ampm/master')

@section('title', 'Withdrawal')


@section('content')
<script>

	function submit_transfer(){
	$.ajax({
			 type: "POST",
			 url: "{{route('withdraw-process')}}?"+$('#withdraw_form').serialize(),
			 data: {
				_token: 		 "{{ csrf_token() }}",
			 },
			 beforeSend: function(){
				
			 },
			 success: function(json){
					obj = JSON.parse(json);
					 var str = '';
					 $.each(obj, function(i, item) {
						
						if( '{{Lang::get('COMMON.SUCESSFUL')}}' == item ){
							alert('{{Lang::get('COMMON.SUCESSFUL')}}');
							window.location.href = "{{route('transaction')}}";
						}else{
							str += item + '<br>';
						}
					})
					$('.failed_message').html(str);
				
			}
		})
	} 

function load_bank(id,type){

	

		if( type == 'default' ){

			var id = id;

		}else{

			var id = id.value;

		}

		

		$.ajax({

			type: "POST",

			url: "{{route('loadbank')}}",

			data: {

				id:  id

			},

		}).done(function( result ) {

			  $('#accountno').val(result);

			  $('#accountnopass').val(result);

		});

	}

	@foreach( $banklists as $bank => $detail  )

	load_bank('{{$detail['code']}}','default');

    <?php break; ?>

	@endforeach
</script>


@include('ampm/top_manage', [ 'title' => Lang::get('public.Withdrawal') ] )

<form id="withdraw_form">
      <!--ACCOUNT TITLE-->
     <!--ACCOUNT CONTENT-->
      <div class="acctContent">
      <span class="wallet_title"><i class="fa fa-money"></i>{{Lang::get('public.Withdrawal')}}</span>
      <div class="acctRow">
      <label>{{Lang::get('public.Option_withdraw')}} :</label><span class="acctText">Bank Transfer</span>
      <div class="clr"></div>
      </div>
      <div class="acctRow">
      <label>Min/Max Limit :</label><span class="acctText">100,000 / 300,000,000</span>
      <div class="clr"></div>
      </div>
      <div class="acctRow">
      <label>Processing time :</label><span class="acctText">15-30 Mins</span>
      <div class="clr"></div>
      </div>
      <div class="remInfo">
      <i class="fa fa-exclamation"></i>All members are allowed to withdraw 1 time per day 
      </div>
      <hr style="width: 100%; float: left;">
      <div class="clr"></div>
      <br>
      <div class="acctRow">
      <label>{{Lang::get('public.Balance')}} (IDR) :</label><span class="acctText">{{App\Http\Controllers\User\WalletController::mainwallet()}}</span>
      <div class="clr"></div>
      </div>
      <div class="acctRow">
      <label>{{Lang::get('public.Amount')}} (IDR)* :</label><input type="text" style="color:black" name="amount"/>
      <div class="clr"></div>
      </div>
      <div class="acctRow">
      <label>{{Lang::get('public.BankName')}}* :</label>
					<select name="bank" onChange="load_bank(this,'none')">
						@foreach( $banklists as $bank => $detail  )
						<option value="{{$detail['code']}}">{{$bank}}</option>
						@endforeach
					</select>
      <div class="clr"></div>
      </div>
      <div class="acctRow">
      <label>{{Lang::get('public.FullName')}} :</label><span class="acctText">{{Session::get('fullname')}}</span>
      <div class="clr"></div>
      </div>
      <div class="acctRow">
      <label>{{Lang::get('public.BankAccountNo')}} * :</label><input type="text" id="accountno"  style="color:black" disabled />
	  <input type="hidden" id="accountnopass" name="accountno"  />
      <div class="clr"></div>
      </div>
      <div class="remInfo">
      <i class="fa fa-exclamation-circle"></i>Required Fields
      </div>
      <br><br>
	 
      <div class="submitAcct">
      <a href="javascript:void(0)" onClick="submit_transfer()">{{Lang::get('public.Submit')}}</a>
      </div>
       <span class="failed_message acctTextReg" style="display:block;float:left;height:100%;"></span>

      
      </div>
      <!--ACCOUNT CONTENT-->

</form>
<div class="clr"></div>






</div>

@stop
