@extends('ampm/master')

@section('title', Lang::get('COMMON.CTABBANKSETTING') )


@section('content')

<script>
 function update_bank(){

	if(  $('#bnkaccno').is(':disabled') ){
		alert('Bank Account number can update one time only!');
	}
	 
	$.ajax({
		type: "POST",
		url: "{{route('updatebank')}}?"+$('#bank_form').serialize(),
	
	}).done(function( result ) {
		if(result == 1){
			
			alert('{{Lang::get('COMMON.SUCESSFUL')}}');
		}

					
	});
} 
function load_bank(id,type){
	
	if( type == 'default' ){
		var id = id;
	}else{
		var id = id.value;
	}
	
	$.ajax({
		type: "POST",
		url: "{{route('loadbank')}}",
		data: {
			id:  id
		},
	}).done(function( result ) {
		  $('#bnkaccno').val(result);
		  if( result == '' )
			$("#bnkaccno").removeAttr('disabled');
		  else
			$("#bnkaccno").attr('disabled','disabled');
	});
}
@foreach( $bankObj as $bank)
load_bank('{{$bank->code}}','default');
<?php break; ?> 
@endforeach
</script>

@include('ampm/top_manage', [ 'title' => 'Bank Setting' ] )

<form id="bank_form">
      <!--ACCOUNT TITLE-->
      <!--ACCOUNT CONTENT-->
      <div class="acctContent">
      <span class="wallet_title"><i class="fa fa-pencil"></i>{{Lang::get('COMMON.CTABBANKSETTING')}}</span>
	  
      <div class="acctRow">
      <label>{{Lang::get('public.Bank')}} :</label>
	  <span class="acctText">
			<select name="bnkid" onChange="load_bank(this,'none')">
			@foreach( $bankObj as $bank)
				<option value="{{$bank->code}}" >{{$bank->name}}</option>
			@endforeach
			</select>
	  </span>
      <div class="clr"></div>
      </div>    

	  <div class="acctRow">
      <label>{{Lang::get('COMMON.BANKACCNAME')}} :</label>
		<span class="acctText">
		{{Session::get('fullname')}}
		</span>
      <div class="clr"></div>
      </div>  
	  
	  <div class="acctRow">
      <label>{{Lang::get('COMMON.BANKACCNO')}} :</label>
	  	<span class="acctText">
			<input type="text" name="bnkaccno" id="bnkaccno">
		</span>
      <div class="clr"></div>
      </div>  
	  


      <div class="submitAcct" onClick="update_bank()">
      <a href="#">{{Lang::get('public.Submit')}}</a>
      </div>
      </div>
      <!--ACCOUNT CONTENT-->

</form>




<div class="clr"></div>






</div>



@stop
