<ul class="megamenu">
	<li>
		<a href="{{route('homepage')}}">{{Lang::get('public.Home')}}</a>
		<div style="width: 500px;" class="dropPost">         
			<table border="0" cellpadding="0" cellspacing="0" id="tabular-content">
				<tr>
					<td>
						<a href="{{route('banking')}}" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('banking','','{{url()}}/front/img/home-banking-icon-hover.png',0)"><img src="{{url()}}/front/img/home-banking-icon.png" alt="" width="93" height="111" id="banking"></a>
					</td>
					<td>
						<a href="{{route('contactus')}}" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('contact','','{{url()}}/front/img/home-contact-icon-hover.png',0)"><img src="{{url()}}/front/img/home-contact-icon.png" alt="" width="93" height="111" id="contact"></a>
					</td>
					<td>
						<a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('chat','','{{url()}}/front/img/home-chat-icon-hover.png',0)" onClick="livechat()"><img src="{{url()}}/front/img/home-chat-icon.png" alt="" width="93" height="111" id="chat"></a>
					</td>
					@if ( !Auth::user()->check() )
					<td>
						<a href="{{route('register')}}" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('register','','{{url()}}/front/img/home-register-icon-hover.png',0)"><img src="{{url()}}/front/img/home-register-icon.png" alt="" width="93" height="111" id="register"></a>
					</td>
					@endif
				</tr>
			</table>
		</div>
	</li>
	<li>
		<a href="{{route('livecasino')}}">{{Lang::get('public.LiveCasino')}}</a>
		<div style="width: 500px;" class="dropPost2">
			<table border="0" cellpadding="0" cellspacing="0" id="tabular-content">
				<tr style="cursor:pointer;">
					@if(in_array('AGG',Session::get('valid_product')))
					<td>
						<a onclick="@if (Auth::user()->check())window.open('{{route('agg')}}', 'casino11', 'width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('ag','','{{url()}}/front/img/lc-ag-icon-hover.png',0)"><img src="{{url()}}/front/img/lc-ag-icon.png" alt="" width="93" height="111" id="ag"></a>
					</td>
					@endif
					@if(in_array('W88',Session::get('valid_product')))
					<td>
						<a onclick="@if (Auth::user()->check())window.open('{{route('w88' , [ 'type' => 'casino' , 'category' => 'live'] )}}', 'casino12', 'width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('gp','','{{url()}}/front/img/lc-gp-icon-hover.png',0)"><img src="{{url()}}/front/img/lc-gp-icon.png" alt="" width="93" height="111" id="gp"></a>
					</td>
					@endif
					@if(in_array('PLTB',Session::get('valid_product')))
					<td>
						<a onClick="@if (Auth::user()->check())window.open('{{route('pltbiframe')}}', 'casino13', 'width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('pt','','{{url()}}/front/img/lc-pt-icon-hover.png',0)"><img src="{{url()}}/front/img/lc-pt-icon.png" alt="" width="93" height="111" id="pt"></a>
					</td>
					@endif
					@if(in_array('MXB',Session::get('valid_product')))
					<td>
						<a onClick="@if (!Auth::user()->check())alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif" href="@if (Auth::user()->check()){{route('mxb', [ 'type' => 'casino' , 'category' => 'baccarat' ] )}}@endif" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('xp','','{{url()}}/front/img/lc-xp-icon-hover.png',0)"><img src="{{url()}}/front/img/lc-xp-icon.png" alt="" width="93" height="111" id="xp"></a>
					</td><br>
					@endif
					@if(in_array('ALB',Session::get('valid_product')))
					<td>
						<a href="" onClick="@if (Auth::user()->check())window.open('{{route('alb')}}', 'casino13', 'width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('coming','','{{url()}}/front/img/lc-coming-hover.png',0)"><img src="{{url()}}/front/img/lc-coming-.png" width="93" height="111" alt=""/></a>
					</td>
					@endif
				</tr>
			</table>
		</div>
	</li>
	<li>
		<a href="{{route('ibc')}}">{{Lang::get('public.SportsBook')}}</a>
	</li>
	<li>
		<a href="{{route('slot', [ 'type' => 'mxb'  ])}}">{{Lang::get('public.Slots')}}</a>
		<div style="width: 500px;" class="dropPost3">
			<table border="0" cellpadding="0" cellspacing="0" id="tabular-content">
				<tr>
					@if(in_array('MXB',Session::get('valid_product')))
					<td>
						<a href="{{route('slot', [ 'type' => 'mxb'  ])}}" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('bt','','{{url()}}/front/img/slt-bt-icon-hover.png',0)"><img src="{{url()}}/front/img/slt-bt-icon.png" alt="" width="161" height="110" id="bt"></a>
					</td>
					@endif
					@if(in_array('W88',Session::get('valid_product')))
					<td>
						<a href="{{route('slot', [ 'type' => 'w88'  ])}}" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('gps','','{{url()}}/front/img/slt-gp-icon-hover.png',0)"><img src="{{url()}}/front/img/slt-gp-icon.png" alt="" width="161" height="110" id="gps"></a>
					</td>
					@endif
					@if(in_array('PLTB',Session::get('valid_product')))
					<td>
						<a href="{{route('slot', [ 'type' => 'pltb'  ])}}" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('pltslot','','{{url()}}/front/img/slt-pt-icon-hover.png',0)"><img src="{{url()}}/front/img/slt-pt-icon.png" alt="" width="161" height="110" id="pltslot"></a>
					</td>
					@endif
					<!--
					@if(in_array('A1A',Session::get('valid_product')))
					<td>
						<a href="{{route('slot', [ 'type' => 'a1a'  ])}}" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('sltmicro','','{{url()}}/front/img/slt-topmicro-icon-hover.png',0)"><img src="{{url()}}/front/img/slt-topmicro-icon.png" alt="" width="161" height="110" id="sltmicro"></a>
					</td>
					@endif
					-->
				</tr>
			</table>
		</div>
	</li>
	@if( Session::get('currency') == "MYR" )
	<li >
		<img src="{{url()}}/front/img/hot.png" style="position:absolute;margin-top:-20px;margin-left:34px;">
		<a href="{{route('fortune')}}">Fortune 4D</a>
	</li>
	<li>
		<a href="{{route('4d')}}">4D</a>
	</li>
	@endif
	@if( Session::get('currency') != "VND" )
		<li>
			<a href="{{route('mobile')}}">{{Lang::get('public.Mobile')}}</a>
		
		</li>
	@endif
	<li style="margin-right: 0px;">
		<a style="margin-right: 0px;" href="{{route('promotion')}}">{{Lang::get('public.Promotion')}}</a>
	</li>	
	
</ul>
<div class="anmnt">
		<marquee>{{App\Http\Controllers\User\AnnouncementController::index()}}</marquee>
</div>