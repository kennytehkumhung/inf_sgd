<!DOCTYPE html>
<html lang="en">
  <head>
    <link rel="shortcut icon" href="{{url()}}/front/favicon.ico" type="image/icon"/>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Infiniwin @yield('title')</title>
	<meta name="keywords" content="@yield('keywords')"/>
	<meta name="description" content="@yield('description')"/>	
	<!--css-->
    <link href="{{url()}}/front/resources/css/style.css" rel="stylesheet">
	<link rel="alternate stylesheet" type="text/css" href="{{url()}}/front/resources/css/style-dark.css" title="styles2" media="screen" />
    <link href="{{url()}}/front/resources/css/jquery.megamenu.css" rel="stylesheet">
    <link href="{{url()}}/front/resources/css/skitter.styles.css" type="text/css" media="all" rel="stylesheet" />

    <link href="{{url()}}/front/resources/css/slick.css" rel="stylesheet">
	<link href="{{url()}}/front/resources/css/clickDrop.css" rel="stylesheet">
    <link href="{{url()}}/front/resources/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
	<!--js-->
    <script src="{{url()}}/front/resources/js/jq214.js"></script>
    <script src="{{url()}}/front/resources/js/script.js"></script>
    <script src="{{url()}}/front/resources/js/jquery.megamenu.js" type="text/javascript"></script>
    <script src="{{url()}}/front/resources/js/styleswitcher.js" type="text/javascript"></script>
	<script type="text/javascript" language="javascript" src="{{url()}}/front/resources/js/jquery.easing.1.3.js"></script>
	<script type="text/javascript" language="javascript" src="{{url()}}/front/resources/js/jquery.skitter.min.js"></script>
	
	<!-------------------------------NEW----------------------------------->
    <link rel="stylesheet" href="{{url()}}/front/resources/css/jquery.circular-carousel.css">
    <link rel="stylesheet" type="text/css" href="{{url()}}/front/resources/css/skin_modern_silver.css"/>
    <link rel="stylesheet" type="text/css" href="{{url()}}/front/resources/css/html_content.css"/>
	<script src="{{url()}}/front/resources/js/jquery.circular-carousel.js"></script> 
    <script src="{{url()}}/front/resources/js/liteaccordion.jquery.js"></script>
    <script src="{{url()}}/front/resources/js/script_carousel.js"></script> 
    <script type="text/javascript" src="{{url()}}/front/resources/js/FWDUltimate3DCarousel.js"></script>
    <!-------------------------------NEW----------------------------------->
    
	<script type="text/javascript">
	   $(document).ready(function() { 
		  $(".lgCont li a").click(function() { 
			 $("link").attr("href",$(this).attr('rel'));
			 $.cookie("css",$(this).attr('rel'), {expires: 365, path: '/'});
			 return false;
		  });
			
		   //$('.headerLeft').css("background-image", "url({{url()}}/front/img/logoRJ.png)");  
	   });
	  jQuery(function(){
		var SelfLocation = window.location.href.split('?');
		switch (SelfLocation[1]) {
		  case "justify_right":
			jQuery(".megamenu").megamenu({ 'justify':'right' });
			break;
		  case "justify_left":
		  default:
			jQuery(".megamenu").megamenu();
		}
	  });
	</script>
	@yield('top_js')
	<script type="text/javascript">
		function MM_swapImgRestore() { //v3.0
		  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
		}
		function MM_preloadImages() { //v3.0
		  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
			var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
			if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
		}

		function MM_findObj(n, d) { //v4.01
		  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
			d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
		  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
		  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
		  if(!x && d.getElementById) x=d.getElementById(n); return x;
		}

		function MM_swapImage() { //v3.0
		  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
		   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
		}
		function login(){
			$.ajax({
				type: "POST",
				url: "{{route('login')}}",
				data: {
					_token: "{{ csrf_token() }}",
					username: $('#username').val(),
					password: $('#password').val(),
					code:     $('#code').val(),
				},
			}).done(function( json ) {
					 obj = JSON.parse(json);
					 var str = '';
					 $.each(obj, function(i, item) {
						str += item + '\n';
					})
					if( "{{Lang::get('COMMON.SUCESSFUL')}}\n" == str ){
						location.reload(); 
					}else{
						alert(str);
					}
					
			});
		}
		function enterpressalert(e, textarea){

			var code = (e.keyCode ? e.keyCode : e.which);
			 if(code == 13) { //Enter keycode
			   login();
			 }
		}
		@if (Auth::user()->check())
			function update_mainwallet(){
				$.ajax({
					  type: "POST",
					  url: "{{route( 'mainwallet', [ '_token'=> csrf_token() ])}}",
					  beforeSend: function(balance){
					
					  },
					  success: function(balance){
						$('.main_wallet').html(parseFloat(Math.round(balance * 100) / 100).toFixed(2));
						total_balance += parseFloat(balance);
					  }
				 });
					
			}
			function getBalance(type){
				total_balance = 0;
				$.when(
				
					 $.ajax({
								  type: "POST",
								  url: "{{route('mainwallet', [ '_token'=> csrf_token()])}}",
								  beforeSend: function(balance){
							
								  },
								  success: function(balance){
									$('.main_wallet').html(parseFloat(Math.round(balance * 100) / 100).toFixed(2));
									total_balance += parseFloat(balance);
								  }
					 }),
					@foreach( Session::get('products_obj') as $prdid => $object)
					
				
							 $.ajax({
								  type: "POST",
								  url: "{{route( 'getbalance', [ '_token'=> csrf_token(), 'product'=> $object->code ])}}&rd=<?php echo rand ( 10000, 99999 ); ?>",
								  beforeSend: function(balance){
									$('.{{$object->code}}_balance').html('<img style="float:left;position:static;top:0px;margin-left:20px;" src="{{url()}}/front/img/ajax-loading.gif" width="20" height="20">');
								  },
								  success: function(balance){
									  if( balance != '{{Lang::get('Maintenance')}}')
									  {
										$('.{{$object->code}}_balance').html(parseFloat(Math.round(balance * 100) / 100).toFixed(2));
										total_balance += parseFloat(balance);
									  }
									  else
									  {
										  $('.{{$object->code}}_balance').html(balance);
									  }
								  }
							 })
						  @if($prdid != Session::get('last_prdid')) 
							,
						  @endif
					@endforeach

					
				).then(function() {
						
						$('#total_balance').html(parseFloat(Math.round(total_balance * 100) / 100).toFixed(2));

				});
			
			}
		@endif
	</script>
	<script>
	$(function() {
		// Clickable Dropdown
		$('.click-nav > ul').toggleClass('no-js js');
		$('.click-nav .js ul').hide();
		$('.click-nav .js').click(function(e) {
			$('.click-nav .js ul').slideToggle(200);
			$('.clicker').toggleClass('active');
			e.stopPropagation();
		});
		$(document).click(function() {
			if ($('.click-nav .js ul').is(':visible')) {
				$('.click-nav .js ul', this).slideUp();
				$('.clicker').removeClass('active');
			}
		});
	});
	
	$(function() {
		// Clickable Dropdown
		$('.click-nav1 > ul').toggleClass('no-js js');
		$('.click-nav1 .js ul').hide();
		$('.click-nav1 .js').click(function(e) {
			$('.click-nav1 .js ul').slideToggle(200);
			$('.clicker1').toggleClass('active');
			e.stopPropagation();
		});
		$(document).click(function() {
			if ($('.click-nav1 .js ul').is(':visible')) {
				$('.click-nav1 .js ul', this).slideUp();
				$('.clicker1').removeClass('active');
			}
		});
	});
	
	$(function() {
		// Clickable Dropdown
		$('.click-nav2 > ul').toggleClass('no-js js');
		$('.click-nav2 .js .wlt').hide();
		$('.click-nav2 .js').click(function(e) {
			if (!$('.click-nav2 .js .wlt').is(':visible')) {
				getBalance(true);
			}
			
			$('.click-nav2 .js .wlt').slideToggle(200);
			$('.clicker2').toggleClass('active');
			e.stopPropagation();
		});
		$(document).click(function() {
			
			if ($('.click-nav2 .js .wlt').is(':visible')) {
				$('.click-nav2 .js .wlt', this).slideUp();
				$('.clicker2').removeClass('active');
			}
		});
	});
	
	
	$(function() {
		// Clickable Dropdown
		$('.click-nav3 > ul').toggleClass('no-js js');
		$('.click-nav3 .js ul').hide();
		$('.click-nav3 .js').click(function(e) {
			$('.click-nav3 .js ul').slideToggle(200);
			$('.clicker3').toggleClass('active');
			e.stopPropagation();
		});
		$(document).click(function() {
			if ($('.click-nav3 .js ul').is(':visible')) {
				$('.click-nav3 .js ul', this).slideUp();
				$('.clicker3').removeClass('active');
			}
		});
	});
	function livechat() {
		@if( Session::get('currency') == 'MYR')
			window.open('https://secure.livechatinc.com/licence/3155342/open_chat.cgi?groups=2', '', 'width=525, height=520, top=150, left=250');
		@elseif( Session::get('currency') == 'VND')
			window.open('https://secure.livechatinc.com/licence/3155342/open_chat.cgi?groups=0', '', 'width=525, height=520, top=150, left=250');	
		@elseif( Session::get('currency') == 'IDR')
			window.open('https://secure.livechatinc.com/licence/5548521/open_chat.cgi?groups=0', '', 'width=525, height=520, top=150, left=250');
		@endif
	}
	</script>
	<script>
	var xmlHttp;
	function srvTime(){
	try {
		//FF, Opera, Safari, Chrome
		xmlHttp = new XMLHttpRequest();
	}
	catch (err1) {
		//IE
		try {
			xmlHttp = new ActiveXObject('Msxml2.XMLHTTP');
		}
		catch (err2) {
			try {
				xmlHttp = new ActiveXObject('Microsoft.XMLHTTP');
			}
			catch (eerr3) {
				//AJAX not supported, use CPU time.
				alert("AJAX not supported");
			}
		}
	}
	xmlHttp.open('HEAD',window.location.href.toString(),false);
	xmlHttp.setRequestHeader("Content-Type", "text/html");
	xmlHttp.send('');
	return xmlHttp.getResponseHeader("Date");
	}

	var st = srvTime();
	var date = new Date(st);
	</script>

		<style>
		.loading_wallet{
			
			float:left;position:static;top:0px;left:0px;
		}
			
		</style>
  </head>