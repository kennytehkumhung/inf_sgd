@extends('sbm/master')

@section('title', Lang::get('public.Tutorial'))

@section('top_js')
<link href="{{url()}}/front/resources/css/otherPg.css" rel="stylesheet">
@stop

<style>
.displayed {
    display: block;
    margin-left: auto;
    margin-right: auto }
	.promo table {
    color: black;
}
.promo table th {
    background: red none repeat scroll 0 0;
    border-right: 1px solid #000;
    color: white;
    padding: 11px;
}
.promo table td {
    border-right: 1px solid #000;
    padding: 11px;
    text-align: center;
}
.promo table th:last-child {
    border-right: 0 none;
}
.promo table td:last-child {
    border-right: 0 none;
}
.promo table tr:nth-child(2n+1) {
    background: #ccc none repeat scroll 0 0;
}
.promo table tr:nth-child(2n) {
    background: #fff none repeat scroll 0 0;
}

</style>

@section('content')
<!--MID SECTION-->
<div class="midSect tncbg">
    
<div class="midSectInner2">

<div class="midSectCont" style="font-weight:bold;">
@if( Lang::getLocale() == 'en' )
     <div style="background-color: grey;height:50px;text-align:center;padding-top:20px;color:white;font-size:30px;">{{ Lang::get('public.Deposit') }}</div>
	 <div style="position:relative; z-index:100;  border:1px grey; background-color: rgba(242, 235, 235, 0.5); padding: 10px 24px;">
	 Step1:
	 <img  width="970px" src="{{url()}}/sbm/img/tutorial/en/step1.png">
	 <br>
	 <br>
	 <br>
	 Step2:
	 <img  width="970px" src="{{url()}}/sbm/img/tutorial/en/step2.png">
	 <br>
	 <br>
	 <br>
	 Step3:
	 <img  width="970px" src="{{url()}}/sbm/img/tutorial/en/step3.png">
	 <br>
	 <br>
	 <br>
	 Step4:
	 <img  width="970px" src="{{url()}}/sbm/img/tutorial/en/step4.png">
	 <br>
	 <br>
	 <br>
	 Step5:
	 <img  width="970px" src="{{url()}}/sbm/img/tutorial/en/step5.png">
@else
	 <div style="background-color: grey;height:50px;text-align:center;padding-top:20px;color:white;font-size:30px;">{{ Lang::get('public.Deposit') }}</div>
	 <div style="position:relative; z-index:100;  border:1px grey; background-color: rgba(242, 235, 235, 0.5); padding: 10px 24px;">
	 Step1:
	 <img  width="970px" src="{{url()}}/sbm/img/tutorial/ch/step1_ch.png">
	 <br>
	 <br>
	 <br>
	 Step2:
	 <img  width="970px" src="{{url()}}/sbm/img/tutorial/ch/step2_ch.png">
	 <br>
	 <br>
	 <br>
	 Step3:
	 <img  width="970px" src="{{url()}}/sbm/img/tutorial/ch/step3_ch.png">
	 <br>
	 <br>
	 <br>
	 Step4:
	 <img  width="970px" src="{{url()}}/sbm/img/tutorial/ch/step4_ch.png">
	 <br>
	 <br>
	 <br>
	 Step5:
	 <img  width="970px" src="{{url()}}/sbm/img/tutorial/ch/step5_ch.png">
	 

@endif
</div>
</div>
</div>
<!--MID SECTION-->
@stop

@section('bottom_js')

@stop