@extends('sbm/master')

@section('title', 'Register')

@section('js')
@parent
<script>
	function enterpressalert(e, textarea)
	{
		var code = (e.keyCode ? e.keyCode : e.which);
		 if(code == 13) { //Enter keycode
		   register_submit();
		 }
	}
        function register_submit(){
		var dob = '0000-00-00';
		if ($('#dob_year').val() != undefined) {
			dob = $('#dob_year').val() + '-' + $('#dob_month').val() + '-' + $('#dob_day').val();
		}
		$.ajax({
			type: "POST",
			url: "{{route('register_process')}}",
			data: {
				_token:		"{{ csrf_token() }}",
				username:	$('#r_username').val(),
				password:	$('#r_password').val(),
				repeatpassword: $('#r_repeatpassword').val(),
				code:		$('#r_code').val(),
				//email:		$('#r_email').val(),
				mobile:		$('#r_mobile').val(),
				fullname:	$('#r_fullname').val(),
				//dob:		dob
			},
		}).done(function(json) {
			$('.acctTextReg').html('');
			obj = JSON.parse(json);
			var str = '';
			$.each(obj, function(i, item) {
				if ('{{Lang::get('COMMON.SUCESSFUL')}}' == item) {
					alert('{{Lang::get('public.NewMember')}}');
					window.location.href = "{{route('homepage')}}";
				} else {
					$('.' + i + '_acctTextReg').html('<font style="color:red">' + item + '</font>');
					//str += item + '\n';
				}
			})
			//alert(str);
		});
        }
</script>
@endsection

@section('content')
<div class="midSectInner">
	<div class="midSectCont">
		<div class="otherPg">
			<h2><i class="fa fa-user-plus"></i>REGISTRATION</h2> 
			<p>Create a new account. It's simple and free</p>
			<div class="acctContentReg">
			<!--
				<div class="acctRow">
					<label>{{ Lang::get('public.Email') }}* :</label><input type="text" id="r_email" name="r_email"><span class="email_acctTextReg acctTextReg"></span>
					<div class="clr"></div>
				</div>
				-->
				<div class="acctRow">
					<label>{{ Lang::get('public.ContactNo') }}* :</label><input type="text" id="r_mobile" name="r_mobile"><span class="mobile_acctTextReg acctTextReg"></span>
					<div class="clr"></div>
				</div>
				<div class="acctRow">
					<label>Currency :</label><label>MYR</label>
					<div class="clr"></div>
				</div>
				
				
				
				<div class="acctRow">
					<label>{{ Lang::get('public.FullName') }}* :</label><input type="text" id="r_fullname" name="r_fullname"><span class="fullname_acctTextReg acctTextReg"></span>
					<div class="clr"></div>
				</div>
				<!--
				<p>*Name must be matched with withdrawal bank account </p>
				<div class="acctRow">
					<label>{{ Lang::get('public.Gender') }} :</label>
					<select id="r_gender" name="r_gender">
						<option value="1">{{ Lang::get('public.Male') }}</option>
						<option value="2">{{ Lang::get('public.Female') }}</option>
					</select>
					<div class="clr"></div>
				</div>
				
				<div class="acctRow">
					<label>Date of birth :</label>
					<select id="dob_day">
						@foreach (\App\Models\Accountdetail::getDobDayOptions() as $key => $val)
						<option value="{{ $key }}">{{ $val }}</option>
						@endforeach
					</select>
					<select id="dob_month" style="margin-left: 5px;">
						@foreach (\App\Models\Accountdetail::getDobMonthOptions() as $key => $val)
						<option value="{{ $key }}">{{ $val }}</option>
						@endforeach
					</select>
					<select id="dob_year" style="margin-left: 5px;">
						@foreach (\App\Models\Accountdetail::getDobYearOptions() as $key => $val)
						<option value="{{ $key }}">{{ $val }}</option>
						@endforeach
					</select>
					<div class="clr"></div>
				</div>
				
				-->
				
				
				<div class="acctRow">
				    <label>{{ Lang::get('public.Username') }}* :</label><input type="text" id="r_username" name="r_username"><span class="username_acctTextReg acctTextReg"></span>
				    <div class="clr"></div>
				</div>
				<p>Username Policy: length at least 6 characters and maximum of 12 </p>
				<div class="acctRow">
					<label>{{ Lang::get('public.Password') }}* :</label><input type="password" id="r_password" name="r_password"><span class="password_acctTextReg acctTextReg"></span>
					<div class="clr"></div>
				</div>
				<p>*Password Policy: length at least 6 characters and maximum of 20 </p>
				<div class="acctRow">
					<label>{{ Lang::get('public.RepeatPassword') }}* :</label><input type="password" id="r_repeatpassword" name="r_repeatpassword">
					<div class="clr"></div>
				</div>
				<div class="acctRow">
				    <label> {{Lang::get('public.Code')}} :</label><img style="float: left;"  src="{{route('captcha', ['type' => 'register_captcha'])}}" width="60" height="27" /><input type="text" id="r_code" onKeyPress="enterpressalert(event, this)"> <span class="code_acctTextReg acctTextReg"></span>	 
				    <div class="clr"></div>
				</div>
				<div class="submitB">
					<a href="javascript:void(0);" onclick="register_submit();">{{Lang::get('public.Register')}}</a>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
