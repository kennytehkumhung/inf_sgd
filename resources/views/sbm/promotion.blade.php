@extends('sbm/master')

@section('title', 'Promotion')
@section('js')

<style>
.box    {
    border:3px double grey;
}

.promo table{
	
	color: black;
}

.promo table th  {
	background: red;
	color: white;
	padding:11px;
	border-right: 1px solid #000;
}

.promo table td{
	padding:11px;
	text-align:center;
	border-right: 1px solid #000;
	}
	
.promo table th:last-child {
	border-right: 0px;}
	
	.promo table td:last-child {
	border-right: 0px;}

.promo table tr:nth-child(odd) {
	background: #CCC}
	
.promo table tr:nth-child(even) {
	background: #FFF}
</style>

<link href="{{ asset('/sbm/resources/css/promo.css') }}" rel="stylesheet" type="text/css" />
<script type="text/javascript">
		$(document).ready(function($) {
			$('#accordion').find('.accordion-toggle').click(function(){
	
				//Expand or collapse this panel
				$(this).next().slideToggle('fast');
	
				//Hide the other panels
				$(".accordion-content").not($(this).next()).slideUp('fast');
	
			});
		});
	</script>
@endsection

@section('content')
<div class="midSect promobg">

<div class="midSectCont">

<div id="promo">
          
        <div id="accordion">
            
            @foreach ($promo as $key => $value )
            
                <h4 class="accordion-toggle">
                    <img src="{{$value['image']}}">
                </h4>
            
                <div class="accordion-content default">
                    <?php echo htmlspecialchars_decode($value['content']); ?>
                </div>
       
            @endforeach       
        </div>
    
</div>

</div>

</div>
  
@endsection