@extends('sbm/master')

@section('title', Lang::get('public.FAQ'))

@section('top_js')
<link href="{{url()}}/front/resources/css/otherPg.css" rel="stylesheet">
@stop

@section('content')
<!--MID SECTION-->
<div class="midSect">
<div class="midSectInner2">
<div class="midSectCont">
<?php echo htmlspecialchars_decode($content); ?>
</div>
</div>
</div>
<!--MID SECTION-->
@stop

@section('bottom_js')

@stop