@extends('../sbm/master')

@section('title', Lang::get('public.SportsBook'))
@section('keywords', 'football betting,sports betting')
@section('description', 'Gambling and bet on football at trusted Malaysia online sports betting site, Infiniwin. Win big today!')

@section('content')
<div class="midSect m8bg">
<div class="midSectInner">
<div class="midSectCont" style="width:1040px">
<div class="pgTit">
  <img src="{{ asset('/sbm/img/m8b-tit.png') }}" width="240" height="75">
</div>
@if (Auth::user()->check())

	<iframe id="" width="1040px" height="855px" frameborder="0" src="{{$login_url}}"></iframe>
@else
<?php
		$lang['en'] = 'EN-US';
		$lang['cn'] = 'ZH-CN';
		$lang['id'] = 'ID-ID';
		$lang['th'] = 'TH-TH';
?>
	<iframe name="sportFrame" id="" width="1040px" height="855px" frameborder="0" src="http://odds.mywinday.com?lang={{$lang[Lang::getLocale()]}}"></iframe>

@endif

</div>
</div>
</div>
@stop

