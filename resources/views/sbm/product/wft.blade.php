@extends('../sbm/master')

@section('title', Lang::get('public.SportsBook'))
@section('keywords', 'football betting,sports betting')
@section('description', 'Gambling and bet on football at trusted Malaysia online sports betting site, Infiniwin. Win big today!')

@section('content')
<div class="midSect wftbg">
<div class="midSectInner">
<div class="midSectCont">
<div class="pgTit">
  <img src="{{ asset('/sbm/img/wft-tit.png') }}" width="240" height="75">
</div>
<iframe name="sportFrame" id="" width="1024px" height="855px" frameborder="0" src="{{$login_url}}"></iframe>
</div>
</div>
</div>
@stop

