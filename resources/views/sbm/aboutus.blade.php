@extends('sbm/master')

@section('title', 'About Us')



@section('content')
<!--MID SECTION-->
<div class="midSect">
<div class="midSectInner2">
<div class="midSectCont" >

<?php echo htmlspecialchars_decode($content); ?>
</div>
</div>
</div>
<!--MID SECTION-->
@stop

@section('bottom_js')

@stop