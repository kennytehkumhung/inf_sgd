@extends('sbm/master')

@section('title', 'Transfer')

@section('js')
@parent
<script>
	$(document).ready(function() { 
		getBalance(true);
	});
	function submit_transfer() {
		$.ajax({
		type: "POST",
			url: "{{route('transfer-process')}}",
			data: {
				_token: 		 "{{ csrf_token() }}",
				amount:			 $('#transfer_amount').val(),
				from:     		 $('#transfer_from').val(),
				to: 			 $('#transfer_to').val(),
			},
			beforeSend: function() {
				$('#btn_submit_transfer').attr('onClick', '');
				$('#btn_submit_transfer').html('Loading...');
			},
			success: function(json) {
				obj = JSON.parse(json);
				var str = '';
				$.each(obj, function(i, item) {
					if ('{{Lang::get('COMMON.SUCESSFUL')}}' == item) {
						//$('.main_wallet').html(obj.main_wallet);
					}
					if (i != 'main_wallet') {
						str += item + '\n';
					}
				});
				load_balance($('#transfer_from').val(), 'from_balance');
				load_balance($('#transfer_to').val(), 'to_balance');
				getBalance(false);
				$('#btn_submit_transfer').attr('onClick', 'submit_transfer()');
				$('#btn_submit_transfer').html('{{Lang::get('public.Submit')}}');
				alert(str);
			}
		});
        }
        function load_balance($code, $type) {
		if ($code == 'MAIN'){
			url = '{{route('mainwallet')}}';
		} else {
			url = '{{route('getbalance')}}';
		}
		$.ajax({
			type: "POST",
			url:  url,
			data: {
				_token: 		 "{{ csrf_token() }}",
				product:		 $code,
			},
			beforeSend: function(balance) {
				$('#' + $type).html('<img style="float:left;position:static;top:0px;margin-left:20px;" src="{{asset('/front/img/ajax-loading.gif')}}" width="20" height="20">');
			},
			success: function(balance) {
				if ($code == 'MAIN') {
					$('.main_wallet').html(parseFloat(Math.round(balance * 100) / 100).toFixed(2));
				}
				$('#' + $type).html('{{Session::get('currency')}} ' + parseFloat(Math.round(balance * 100) / 100).toFixed(2));
				total_balance += parseFloat(balance);
			}
		});
        }
</script>
@endsection

@section('content')
<div class="midSectInner">
    <div class="midSectCont">
	<div class="midBottom">
	    
	    @include('sbm/include/balance_right')

	    <!--ACCOUNT MANAGAMENT MENU-->

	    <!--ACCOUNT MANAGAMENT MENU-->
	    <!--ACCOUNT TITLE-->
	    <div class="fltLeft">
		
		@include('sbm/include/fund_tab_top', array('active' => 3))
		
		<!--ACCOUNT TITLE-->
		<!--ACCOUNT CONTENT-->
		<div class="acctContent">
		    <span class="wallet_title"><i class="fa fa-credit-card"></i>{{ Lang::get('public.WalletTransfer') }}</span>
		    <div class="acctRow">
			<div class="eleLeft">
			    <div class="acctRow">
				<label>{{ Lang::get('public.TransferFrom') }} :</label>
				<select id="transfer_from" onchange="load_balance(this.value,'from_balance')" style="width:160px;">
					<option selected="selected" value="MAIN">{{ Lang::get('public.MainWallet') }}</option>
					@foreach( Session::get('products_obj') as $prdid => $object)
					<option value="{{ $object->code }}">{{ $object->name }}</option>
					@endforeach
				</select>
				<div class="clr"></div>
			    </div>
			</div>
			<div class="eleLeft">
			    <div class="acctRow">
				<label>{{Lang::get('public.TransferTo')}} :</label>
				<select id="transfer_to" onchange="load_balance(this.value,'to_balance')" style="width:160px;">
					@foreach( Session::get('products_obj') as $prdid => $object)
					<option value="{{ $object->code }}">{{ $object->name }}</option>
					@endforeach
					<option  value="MAIN">{{ Lang::get('public.MainWallet') }}</option>
				</select>
				<div class="clr"></div>
			    </div>
			</div>
			<div class="eleLeft">
			    <div class="acctRow">
				<label>{{ Lang::get('public.Balance') }} :</label><span id="from_balance" class="acctText">MYR {{ App\Http\Controllers\User\WalletController::mainwallet() }}</span>
				<div class="clr"></div>
			    </div>
			</div>
			<div class="eleLeft">
			    <div class="acctRow">
				<label></label><span id="to_balance" class="acctText">MYR 0.00</span>
				<div class="clr"></div>
			    </div>
			</div>
			<div class="clr"></div>
			<div class="acctRow">
			    <label style="width: 83px !important;">{{ Lang::get('public.Amount') }}</label>
			    <input id="transfer_amount" name="transfer_amount" type="text" value="0.00" />
			    <span id="error_deposit" style="color:Red;display:none;">*{!! Lang::get('public.BalanceZeroPleaseMakeDeposit', array('p1' => url('deposit'))) !!}</span>
			    <span id="error_require" style="color:Red;display:none;">{{ Lang::get('public.Required') }}</span>
			    <div class="clr"></div>
			</div>
			<div class="clr"></div>
		    </div>
		    <div class="clr"></div> 
		    <br>
		    <div class="submitAcct">
			<a id="btn_submit_transfer" href="javascript:void(0);" onclick="submit_transfer();">{{ Lang::get('public.Submit') }}</a>
			<span class="error_validation" style="color:Red;"></span>
		    </div>
		</div>
		<!--ACCOUNT CONTENT-->
		<div class="clr"></div>
	    </div>
	    <!-- /container -->
	</div>
	<div class="clr"></div>
    </div>
</div>
@endsection