@extends('sbm/master')

@section('content')
<div class="sliderCont">
    <div class="sliderContInner">
	<!--Slider-->
	<div class="box_skitter box_skitter_large">
	    <ul>

		 @if(isset($banners))
			@foreach( $banners as $key => $banner )
				<li><a href="@if($banner->url != 'none'){{$banner->url}}@endif"><img src="{{$banner->domain}}/{{$banner->path}}" class="directionLeft" /></a></li>
				
            @endforeach
		  @endif
	    </ul>
	</div>
	<!--Slider-->
	<div class="slideLeft">
	    <div class="bx01">
		<div class="txt1">LIVE</div>
		<div class="txt2">COMMENTARIES</div>
		<div class="txt3">
		    Live commentaries and odds 
		    statistic available
		</div>
	    </div>

	    <div class="bx1">
		<div class="txt1">LIVE</div>
		<div class="txt2">IN-PLAY</div>
		<div class="txt3">
		    Place bet in real time
		</div>
	    </div>

	    <div class="bx1">
		<div class="txt1">LIVE</div>
		<div class="txt2">STREAMING</div>
		<div class="txt3">
		    Watch live matches. More than 
		    100 events live weekly
		</div>
	    </div>

	</div>
	<div class="clr"></div>
    </div>
</div>

<div class="midSectInner">
    <div class="midSectCont" style="width:1030px;">
	<div class="thumb1">
	    <a href="{{route('asc')}}" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('thumb1', '', '{{ asset('/sbm/img/thumb-1-hover.png') }}', 0)"><img src="{{ asset('/sbm/img/thumb-1.png') }}" name="thumb1" width="505" height="197" border="0"></a>
	</div>

	<div class="thumb1">
	    <a href="{{route('m8b')}}" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('thumb2', '', '{{ asset('/sbm/img/thumb-2-hover.png') }}', 0)"><img src="{{ asset('/sbm/img/thumb-2.png') }}" name="thumb2" width="505" height="197" border="0"></a>
	</div>


	<div class="clr"></div>
    </div>
</div>

@endsection