@extends('sbm/master')

@section('title', 'Profile')

@section('js')
@parent
<script>
	$(document).ready(function() { 
		getBalance(true);
		
		$('#gender').val('{{ $acdObj->gender }}');
	});
        function update_profile() {
		var dob = '{{ $acdObj->dob }}';
		if ($('#dob_year').val() != undefined) {
			dob = $('#dob_year').val() + '-' + $('#dob_month').val() + '-' + $('#dob_day').val();
		}
		$.ajax({
			type: "POST",
			url: "{{action('User\MemberController@UpdateDetail')}}",
			data: {
				_token: "{{ csrf_token() }}",
				//dob:		$('#dob').val(),
				gender: $('#gender').val(),
				dob: dob
			}
		}).done(function (json) {
			obj = JSON.parse(json);
			var str = '';
			$.each(obj, function (i, item) {
			    str += item + '\n';
			});
			alert(str);
		});
        }
</script>
@endsection

@section('content')
<div class="midSectInner">
    <div class="midSectCont">
	<div class="midBottom">
	    
	    @include('sbm/include/balance_right')

	    <!--ACCOUNT MANAGAMENT MENU-->

	    <!--ACCOUNT MANAGAMENT MENU-->
	    <!--ACCOUNT TITLE-->
	    <div class="fltLeft">
		
		@include('sbm/include/profile_tab_top', array('active' => 0))
		
		<!--ACCOUNT TITLE-->
		<!--ACCOUNT CONTENT-->
		
		<div class="acctContent">
		    <span class="wallet_title"><i class="fa fa-pencil"></i>{{ Lang::get('public.ChangePassword') }}</span>
		    <div class="acctRow">
			<label>{{ Lang::get('public.Username') }} :</label><span>{{ $acdObj->fullname }}</span>
			<div class="clr"></div>
		    </div>
		    <div class="acctRow">
			<label>{{ Lang::get('public.FullName') }} :</label><span>{{ $acdObj->fullname }}</span>
			<div class="clr"></div>
		    </div>
		    <div class="acctRow">
			<label>{{ Lang::get('public.EmailAddress') }} :</label><span>{{ $acdObj->email }}</span>
			<div class="clr"></div>
		    </div>
		    <div class="acctRow">
			<label>{{ Lang::get('public.Currency') }} :</label><span>MYR</span>
			<div class="clr"></div>
		    </div>
		    <div class="acctRow">
			<label>{{ Lang::get('public.ContactNo') }} :</label><span>{{ $acdObj->telmobile }}</span>
			<div class="clr"></div>
		    </div>
		    <div class="acctRow">
			<label>{{ Lang::get('public.Gender') }} :</label>
			<select id="gender" name="gender">
				<option value="1">{{ Lang::get('public.Male') }}</option>
				<option value="2">{{ Lang::get('public.Female') }}</option>
			</select>
			<div class="clr"></div>
		    </div>
		    <div class="acctRow">
			<label>{{Lang::get('public.DOB')}} :</label>
			@if( $acdObj->dob == '0000-00-00' )
			<select id="dob_day">
			    @foreach (\App\Models\AccountDetail::getDobDayOptions() as $key => $val)
			    <option value="{{ $key }}">{{ $val }}</option>
			    @endforeach
			</select>
			<select id="dob_month" >
			    @foreach (\App\Models\AccountDetail::getDobMonthOptions() as $key => $val)
			    <option value="{{ $key }}">{{ $val }}</option>
			    @endforeach
			</select>
			<select id="dob_year">
			    @foreach (\App\Models\AccountDetail::getDobYearOptions() as $key => $val)
			    <option value="{{ $key }}">{{ $val }}</option>
			    @endforeach
			</select>
			@else
			{{ $acdObj->dob }}
			@endif
			<!--<br>
			<input type="text" class="datepicker" id="deposit_date" style="cursor:pointer;" name="date" value="{{$acdObj->dob}}"><br>-->
			<div class="clr"></div>
		    </div>
		    <div class="submitAcct">
			<a href="javascript:void(0);" onclick="update_profile()">{{ Lang::get('public.Submit') }}</a>
		    </div>
		    <div class="clr"></div>
		</div>
		<!--ACCOUNT CONTENT-->
		<div class="clr"></div>
	    </div>
	    <!-- /container -->
	</div>
	<div class="clr"></div>
    </div>
</div>
@endsection