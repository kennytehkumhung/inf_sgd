@extends('sbm/master')

@section('title', 'Forgot Password')

@section('js')

<script>
  function forgotpassword_submit(){

	$.ajax({
		type: "POST",
		url: "{{route('resetpassword')}}",
		data: {
			_token: "{{ csrf_token() }}",
			username:		 $('#fg_username').val(),
			email:    		 $('#fg_email').val(),

		},
	}).done(function( json ) {
			 $('.acctTextReg').html('');
			 obj = JSON.parse(json);
			 var str = '';
			 $.each(obj, function(i, item) {
				if( '{{Lang::get('COMMON.SUCESSFUL')}}' == item ){
					window.location.href = "{{route('homepage')}}";
				}else{
					$('.' + i + '_acctTextReg').html('<font style="color:red">' + item + '</font>');
					
				}
			})
	
	});
}
</script>
  
@endsection

@section('content')
<div class="midSectCont">
<br>
<div class="midContf">
<h2><i class="fa fa-lock"></i> {{Lang::get('public.ForgotPassword')}}</h2> 
     <p>
      We can help you reset your password and security info. First, enter your registered info and we will
send an account restoration link to your email
<br><br>
     </p>
<div class="acctContentReg">
    <div class="acctRow">
            <label>{{Lang::get('public.Username')}} :</label><input type="text" id="fg_username"><span class="username_acctTextReg acctTextReg"></span>
                <div class="clr"></div>
    </div>
    
    <div class="acctRow">
            <label>{{Lang::get('public.EmailAddress')}} :</label><input type="text" id="fg_email"><span class="email_acctTextReg acctTextReg"></span>
                <div class="clr"></div>
    </div>
      
    <div class="submitB" onClick="forgotpassword_submit();">
            <a href="javascript:void(0);">{{Lang::get('public.Submit')}}</a>
    </div>
    
</div>
</div>


</div>

@endsection