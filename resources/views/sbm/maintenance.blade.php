@extends('sbm/master')

@section('title', 'Maintenance')

@section('top_js')

@stop

@section('content')
<style>
.maintenance  {
	width: 100%;
	text-align: center;
	padding-top: 0px;
	padding-bottom: 50px;
	margin-top: -50px;
	margin-left: -30px;
}

.maintenance span  {
	display: block;
	font-size: 22px;
}

.maintainMSG  {
	font-weight: bold;
	padding-bottom: 2px;
	line-height: 33px;
	color: #000;
	font-weight: 20px;
}

.maintainMSGsub  {
	font-weight: bold;
	line-height: 33px;
	font-size: 18px !important;
	color: #000;
	margin-top: 20px;
}
.maintenance  {
	width: 100%;
	height: 490px;
	text-align: center;
	padding-top: 0px;
	padding-bottom: 50px;
	margin-top: 30px;
	background-image: url({{ asset('/sbm/img/maintenance-bg.jpg')}});
	background-repeat: no-repeat;
	background-position: center top;
	position: relative;
}

.maintenance span  {
	font-size: 28px;
	

}


</style>

<div class="midSect">
	<div class="midSectInner">
		<div class="spacingTop"></div>
		<div class="maintenance">      
			<span class="maintainMSG">We are upgrading our server and  <br>user interface for a <br>better gaming experience</span>
			<span class="maintainMSGsub">For more details, please contact us at marketing@sbm811.com</span>
		</div>
	</div>
</div>
@stop

@section('bottom_js')

@stop