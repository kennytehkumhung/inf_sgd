<ul>
	<li>{{ Lang::get('public.Wallet') }}</li>
	
	<li><p>{{Lang::get('public.Mainwallet')}}: <span class="main-setPrice main_wallet">{{App\Http\Controllers\User\WalletController::mainwallet()}}</span></p></li>
	
	<?php $count = 0; ?>
	@foreach( Session::get('products_obj') as $prdid => $object)
	<li><p>{{$object->name}}: <span class="main-setPrice {{$object->code}}_balance">0.00</span></p></li>
	@endforeach
	<li></li>
	<li><p><span id="top_total_balance" class="main-setPrice main_wallet">{{Lang::get('public.Total')}}: 0.00</span></p></li>
</ul>
