<?php 
if (!isset($active)) { $active = -1; }
$pttSettings = array(
    [
	'url' => url('deposit'),
	'class' => 'link4',
	'label' => Lang::get('public.Deposit'),
    ],
    [
	'url' => url('withdraw'),
	'class' => 'link5',
	'label' => Lang::get('public.Withdrawal'),
    ],
    [
	'url' => url('transaction'),
	'class' => 'link6',
	'label' => Lang::get('public.TransactionHistory'),
    ],
    [
	'url' => url('transfer'),
	'class' => 'link7',
	'label' => Lang::get('public.Transfer'),
    ],
//    [
//	'url' => '#',
//	'class' => 'link8',
//	'label' => Lang::get('public.MultipleTransfer'),
//    ],
);
?>

<div class="title_bar">
@for ($i = 0; $i < count($pttSettings); $i++)
    <a href="{{ $pttSettings[$i]['url'] }}"><span class="{{ $pttSettings[$i]['class'] }} {{ $active === $i ? 'selected2' : '' }}">{{ $pttSettings[$i]['label'] }}</span></a>
@endfor
</div>
