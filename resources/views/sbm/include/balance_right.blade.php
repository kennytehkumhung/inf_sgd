<div class="fltRight">
    <div class="amtHead">
	{{ strtoupper(Lang::get('public.Currentbalance')) }}
    </div>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
	    <td>{{Lang::get('public.Mainwallet')}}</td>
	    <td>RM <span class="main_wallet">{{App\Http\Controllers\User\WalletController::mainwallet()}}</span></td>
	</tr>
	<?php $count = 0; ?>
	@foreach( Session::get('products_obj') as $prdid => $object)
	<tr>
	    <td>{{$object->name}}</td>
	    <td>RM <span class="{{$object->code}}_balance">0.00</span></td>
	</tr>
	@endforeach
	<tr>
	    <td class="ttl">{{ strtoupper(Lang::get('public.Total')) }}</td>
	    <td class="ttl">RM <span id="total_balance" class="main_wallet">0.00</span></td>
	</tr>
    </table>
</div>