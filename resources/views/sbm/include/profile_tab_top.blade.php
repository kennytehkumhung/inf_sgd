<?php 
if (!isset($active)) { $active = -1; }
$pttSettings = array(
    [
	'url' => url('update-profile'),
	'class' => 'link1',
	'label' => Lang::get('public.Profile'),
    ],
    [
	'url' => url('update-password'),
	'class' => 'link2',
	'label' => Lang::get('public.AccountPassword2'),
    ],
    [
	'url' => url('bet-history'),
	'class' => 'link3',
	'label' => Lang::get('public.BetHistory'),
    ],
);
?>

<div class="title_bar">
@for ($i = 0; $i < count($pttSettings); $i++)
    <a href="{{ $pttSettings[$i]['url'] }}"><span class="{{ $pttSettings[$i]['class'] }} {{ $active === $i ? 'selected2' : '' }}">{{ $pttSettings[$i]['label'] }}</span></a>
@endfor
</div>
