@extends('sbm/master')

@section('title', 'Deposit')

@section('js')
@parent
<script>
        $(document).ready(function() {
		getBalance(true);
		
		// Setup date.
		$('#deposit_year').val('{{ date('Y') }}');
		$('#deposit_month').val('{{ date('m') }}');
		$('#deposit_day').val('{{ date('d') }}');
        });
        setInterval(update_mainwallet, 10000);
        function deposit_submit() {
            
       
            
		$('#date').val($('#deposit_year').val() + '-' + $('#deposit_month').val() + '-' + $('#deposit_day').val());
		var file_data = $("#receipt").prop("files")[0];
		var form_data = new FormData();
		form_data.append("file", file_data);
		var data = $('#deposit_form').serializeArray();
		var obj = {};
		for (var i = 0, l = data.length; i < l; i++) {
			if (data[i].name == "refno"){
				form_data.append(data[i].name, 'none');
			} else {
				form_data.append(data[i].name, data[i].value);
			}
		}
		

		form_data.append('_token', '{{csrf_token()}}');
		$.ajax({
		type: "POST",
			url: "{{route('deposit-process')}}?",
			dataType: 'script',
			cache: false,
			contentType: false,
			processData: false,
			data: form_data, // Setting the data attribute of ajax with file_data
			type: 'post',
			beforeSend: function() {
				$('#deposit_sbumit_btn').attr('onclick', '');
			},
			complete: function(json) {
				obj = JSON.parse(json.responseText);
				var str = '';
				$.each(obj, function(i, item) {
					if ('{{Lang::get('COMMON.SUCESSFUL')}}' == item) {
						alert('{{Lang::get('COMMON.SUCESSFUL')}}');
						window.location.href = "{{route('transaction')}}";
					} else {
						//$('.'+i+'_acctTextReg').html(item);
						$('#deposit_sbumit_btn').attr('onclick', 'deposit_submit()');
						str += item + '<br>';
					}
				});
				$('.failed_message').html(str);
			},
		});
		//alert('asd');
        }
	
    function checkAmount() {
        var amount = parseFloat($("#amount").val());
        
        if (isNaN(amount) || amount < 5000) {
            alert("{{ Lang::get('public.MinimumDepositAmountX', ['p1' => '5000.00']) }}");
            return false;
        }
        
        return true;
    }
    
	function onBkOptChanging(obj) {
		$('.bkOpt .box1').hide();

		var rid = $(obj).val();
		$('.bkOpt .box1_' + rid).show();
	}
</script>
@endsection

@section('content')
<div class="midSectInner">
    <div class="midSectCont">
	<div class="midBottom">

	    @include('sbm/include/balance_right')

	    <!--ACCOUNT MANAGAMENT MENU-->

	    <!--ACCOUNT MANAGAMENT MENU-->
	    <!--ACCOUNT TITLE-->
	    <div class="fltLeft">

		@include('sbm/include/fund_tab_top', array('active' => 0))

		<!--ACCOUNT TITLE-->
		<!--ACCOUNT CONTENT-->
		<div class="acctContent">
		    <form id="deposit_form">

			<input type="hidden" id="rules" name="rules" value="1" />

			<div class="depRight">
			    <span class="wallet_title"><i class="fa fa-money"></i>{{ Lang::get('public.Deposit') }}</span>

			    <div class="depRight">
				<div class="acctRowCT tp01">
					<div class="depWrap1">
                   
                        <input type="hidden" name="promo" class="radiobtn" value="0">
					    <div class="clr"></div>
					</div>
				    </div>
				    <div class="clr"></div>
			    </div>
			    <div class="clr"></div>

			    <div class="acctRowCT tp01">
				<span class="wallet_title">{{ Lang::get('public.BankingOptions') }}</span>
				
				<div>
				    @foreach($banks as $key => $bank)
				    <div class="bkOpt" style="float:left; margin: 0 16px 0 4px;">
					<div class="bkImg">
					    <label>
						<img src="{{ $bank['image'] }}" style="width: 160px; height: 45px;" />
						<span><input class="colorRadio bkRadio" type="radio" id="bank" name="bank" value="{{ $bank['bnkid'] }}" title="{{ $bank['name'] }}" onchange="onBkOptChanging(this);"></span>
					    </label>
					</div>
					<div class="bkTxt">
					    <div class="red box1 box1_{{ $bank['bnkid'] }}">
						<strong>{{ Lang::get('public.ImportantInfo') }}</strong><br>
						{{ $bank['bankaccname'] }}<br>
						{{ $bank['bankaccno'] }}
					    </div>
					</div>
				    </div>
				    @endforeach
				
                    <div class="clr"></div>
				</div>
			        <div class="clr"></div>
			    </div>
			    <div class="clr"></div>

			    <span class="wallet_title">{{ Lang::get('public.Amount') }}</span>
			    <div class="clr"></div>
			    <div class="acctRow">
				<input type="text" id="amount" name="amount" class="depRight5" />
				<div class="clr"></div>
			    </div>

			    <span class="wallet_title">{{ Lang::get('public.DepositMethod') }}</span>
			    <div class="clr"></div>
			    <div class="acctRow">
				<select id="type" name="type" class="depRight3">
				    <option value="3">{{ Lang::get('public.OverCounter') }}</option>
				    <option value="1">{{ Lang::get('public.InternetBanking') }}</option>
				    <option value="2">{{ Lang::get('public.ATMBanking') }}</option>
				</select>
				<div class="clr"></div>
			    </div>

			    <span class="wallet_title">{{ Lang::get('public.DateTime') }}</span>
			    <div class="clr"></div>
			    <div class="acctRow">
				<select id="deposit_day" class="depRight4" style="margin-right: 8px;">
				    @for ($i = 1; $i <= 31; $i++)
				    <option value="{{ str_pad($i, 2, '0', STR_PAD_LEFT) }}">{{ str_pad($i, 2, '0', STR_PAD_LEFT) }}</option>
				    @endfor
				</select>
				<select id="deposit_month" class="depRight4" style="margin-right: 8px;">
				    @for ($i = 1; $i <= 12; $i++)
				    <option value="{{ str_pad($i, 2, '0', STR_PAD_LEFT) }}">{{ str_pad($i, 2, '0', STR_PAD_LEFT) }}</option>
				    @endfor
				</select>
				<select id="deposit_year" class="depRight4">
				    <option value="{{ (new DateTime())->format('Y') }}">{{ (new DateTime())->format('Y') }}</option>
				    <option value="{{ (new DateTime('-1 year'))->format('Y') }}">{{ (new DateTime('-1 year'))->format('Y') }}</option>
				</select>
				<select name="hours" style="background: #fff none repeat scroll 0 0 !important;
    border: 1px solid #6b6b6b !important;
    width: 80px !important;">
				<option value="01">01</option>
				<option value="02">02</option>
				<option value="03">03</option>
				<option value="04">04</option>
				<option value="05">05</option>
				<option value="06">06</option>
				<option value="07">07</option>
				<option value="08">08</option>
				<option value="09">09</option>
				<option value="10">10</option>
				<option value="11">11</option>
				<option value="12">12</option>
				</select>
				<select name="minutes" style="background: #fff none repeat scroll 0 0 !important;
    border: 1px solid #6b6b6b !important;
    width: 80px !important;">
				<option value="00">00</option>
				<option value="01">01</option>
				<option value="02">02</option>
				<option value="03">03</option>
				<option value="04">04</option>
				<option value="05">05</option>
				<option value="06">06</option>
				<option value="07">07</option>
				<option value="08">08</option>
				<option value="09">09</option>
				<option value="10">10</option>
				<option value="11">11</option>
				<option value="12">12</option>
				<option value="13">13</option>
				<option value="14">14</option>
				<option value="15">15</option>
				<option value="16">16</option>
				<option value="17">17</option>
				<option value="18">18</option>
				<option value="19">19</option>
				<option value="20">20</option>
				<option value="21">21</option>
				<option value="22">22</option>
				<option value="23">23</option>
				<option value="24">24</option>
				<option value="25">25</option>
				<option value="26">26</option>
				<option value="27">27</option>
				<option value="28">28</option>
				<option value="29">29</option>
				<option value="30">30</option>
				<option value="31">31</option>
				<option value="32">32</option>
				<option value="33">33</option>
				<option value="34">34</option>
				<option value="35">35</option>
				<option value="36">36</option>
				<option value="37">37</option>
				<option value="38">38</option>
				<option value="39">39</option>
				<option value="40">40</option>
				<option value="41">41</option>
				<option value="42">42</option>
				<option value="43">43</option>
				<option value="44">44</option>
				<option value="45">45</option>
				<option value="46">46</option>
				<option value="47">47</option>
				<option value="48">48</option>
				<option value="49">49</option>
				<option value="50">50</option>
				<option value="51">51</option>
				<option value="52">52</option>
				<option value="53">53</option>
				<option value="54">54</option>
				<option value="55">55</option>
				<option value="56">56</option>
				<option value="57">57</option>
				<option value="58">58</option>
				<option value="59">59</option>
				</select>
				<select name="range" style="background: #fff none repeat scroll 0 0 !important;
    border: 1px solid #6b6b6b !important;
    width:80px !important;">
				<option value="AM">AM</option>
				<option value="PM">PM</option>
				</select>
				<input type="hidden" id="date" name="date" />
				<!--
				<input type="hidden" id="hours" name="hours" value="12" />
				<input type="hidden" id="minutes" name="minutes" value="00" />-->
				<div class="clr"></div>
			    </div>

			    <span class="wallet_title">{{ Lang::get('public.ReferenceNo') }}</span>
			    <div class="clr"></div>
			    <div class="acctRow">
				<input type="text" id="refno" name="refno" class="depRight5" />
				<div class="clr"></div>
			    </div>

			    <span class="wallet_title">{{ Lang::get('public.Attachment') }}</span>
			    <div class="clr"></div>
			    <div class="acctRow2">
				<input class="upload" type="file" id="receipt" name="receipt">
				<div class="clr"></div>
			    </div>

			    <div class="submitAcct">
				<a id="deposit_sbumit_btn" href="javascript:void(0);" onclick="deposit_submit()">{{ Lang::get('public.Submit') }}</a>
			    </div>

			    <br><br>
			    <span class="failed_message acctTextReg" style="display:block;float:left;height:100%;color:red;"></span>
			</div>
		    </form>
		    <div class="clr"></div>
		</div>
		<!--ACCOUNT CONTENT-->
		<div class="clr"></div>
	    </div>
	    <!-- /container -->
	</div>
	<div class="clr"></div>
    </div>
</div>
@endsection