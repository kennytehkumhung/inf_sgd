@extends('sbm/master')

@section('title', Lang::get('public.TNC'))

@section('top_js')
<link href="{{url()}}/front/resources/css/otherPg.css" rel="stylesheet">
@stop

<style>
.displayed {
    display: block;
    margin-left: auto;
    margin-right: auto }
	.promo table {
    color: black;
}
.promo table th {
    background: red none repeat scroll 0 0;
    border-right: 1px solid #000;
    color: white;
    padding: 11px;
}
.promo table td {
    border-right: 1px solid #000;
    padding: 11px;
    text-align: center;
}
.promo table th:last-child {
    border-right: 0 none;
}
.promo table td:last-child {
    border-right: 0 none;
}
.promo table tr:nth-child(2n+1) {
    background: #ccc none repeat scroll 0 0;
}
.promo table tr:nth-child(2n) {
    background: #fff none repeat scroll 0 0;
}

</style>

@section('content')
<!--MID SECTION-->
<div class="midSect tncbg">
    
<div class="midSectInner2">

<div class="midSectCont" style="font-weight:bold;">
@if( Lang::getLocale() == 'en' )
    <div style="background-color: grey;"><img class="displayed" src="{{url()}}/sbm/img/tnc_en.png"></div>
        <div style="position:relative; z-index:100;  border:1px grey; background-color: rgba(242, 235, 235, 0.5); padding: 10px 24px;">
            
			<div><div><div><div><div><div><div>​
			

						
    <p>SBM811 SPORTSBOOK</p>
    <p>Terms and Conditions</p>
    <ol>
        <li dir="ltr">
            <p>These Promotion starts on 2016 15 October 00:00:00 until 2016 31 October 23:59:59 .</p>
        </li>
    </ol>  

    <ol start="2">
		<li dir="ltr">
            <p>Your deposit plus the bonus is subjected to 1 time(s) turnover in Sbm811.com.</p>
        </li>
        <li dir="ltr">
            <p>Example : Deposit Amount = 5000</p>
        </li>
    </ol>

    <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Turnover Requirement = (5000) x 1 = 5,000 &nbsp; &nbsp;</p>
    <p>&nbsp;&nbsp;&nbsp;&nbsp;</p>
    <ol start="4">
        <li dir="ltr">
            <p>Draw result,both sides bet,voided or cancelled games are excluded in turnover calculation.</p>
        </li>
        <li dir="ltr">
            <p>All Withdrawal will be charges 5% of withdrawal amount.</p>
        </li>
        <li dir="ltr">
            <p>For all member balance amount above MYR1000 of previous deposit balance, the next deposit will be continuous counted towards the rollover requirement.</p>
        </li>
        <li dir="ltr">
            <p>Bets made below (Decimal odd 1.5; Hong Kong odd 0.5; Malay Positive Odds 0.5; Indo Odd -2.0; US Odds -200) and void, tie, cancelled, or made on opposite sides with same outcome will not be counted towards the rollover requirements.<br></p>
        </li>
        <li dir="ltr">
            <p class="MsoNormal">SBM811 reserves the right to cancel the promotion at any time,either for all players and individual player.</p>
        </li>
        <li dir="ltr">
            <p class="MsoNormal">Should disputes arise in all matters relating to the awarding of the bonus,bet and or the Terms and Conditions in the Promotion, the final decision shall lies with SBM811.com. The decision made will be binding and shall not be subject to review or appeal by any customer or third party.<br></p>
        </li>
        <li dir="ltr">
            <p>Please be advised that accounts belong to same member are not allowed to place bet in a same match. (Example: ABC001 placed bet in Manchester United vs Cheasea, ABC002, ABC003 are not allowed to place bet in the same match). Our company will freeze or forfeit offender's related match fund.</p>
        </li>
        <li dir="ltr">
            <p>Please make a deposit according to the bank provided by our website. Before making each deposit please refer to the bank shown on our deposit form. Thank you!</p>
        </li>
        <br>
        <p>This Promotion is subjected to SBM811 General Promotion Terms and Conditions.</p>
    </ol>​</div>​​</div>​</div>​​</div>​</div>​</div>​</div>
         
         
@else
	
    <div style="background-color: grey;"><img class="displayed" src="{{url()}}/sbm/img/tnc_ch.png"></div>
        <div style="position:relative; z-index:100;  border:1px grey; background-color: rgba(242, 235, 235, 0.5); padding: 10px 24px;">
            
           <div><div><div><div><div>
    <div></div>​
    <div>

        <p>SBM811活动优惠</p>
        <br>
        <p>规则与条款：</p>
        <p>1. &nbsp;此优惠活动时间开始于2016年10月15日00:00:00，截止于2016年10月31日 23:59:59（北京时间）。</p>
        <p>2. 流水要求: 会员必须使用存款及红利有效下注至少1倍</p>
        <p>3. 您的存款加优惠必须在SBM811平台下达到流水要就才能申请提款。</p>
        <p>&nbsp; 例如： &nbsp;存款金额 = 5000</p>
        <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;流水要求： 下注额要求 = （ 5000 ）x 1 = &nbsp;5,000</p>
        <p>4. 任何平局，对赌（双边下注)，无效注单(本金退还)，取消的注单以及任何低于香港盘0.5（马来盘0.5;欧洲盘1.5;印尼盘-2.00)的赔率注单将不计算在活动的有效下注额要求中。</p>
        <p>5. 所有会员申请提款的金额将会被扣除5%的提款服务费。</p>
        <p>6. 所有会员所剩余额超过MYR 1000，并再存款与享用优惠，流水要求量将会累计继续计算。</p><p>7.低于以下的投注（十进制投注赔率1.5; 香港盘0.5; 马来盘0.5; 印尼盘 -2.0; 美国盘 -200）和无效的，平局，取消的投注，或将取得相同结果的双方下注将不被计算在下注交易额内。<br></p>
        <p>7. SBM811有权在任何时间取消所有会员或者团伙投注会员此优惠，SBM811享有唯一及绝对决定权。</p>
        <p>8. 如果出现任何涉及优惠奖金,注单问题或条款规则的争议，SBM811单方面保留最终决定权利。所作出的决定将具约束力、必须被遵守，并不接受审查与任何客户或第三方提出的上诉。</p>
        <p>9. 每个会员旗下的3个户口不能同时下注同一场赛事。例如:abc001下注了赛事如‘’曼联对切尔西‘’,abc002和abc003户口则不能再下注同一场赛事‘’曼联对切尔西‘’。如有违规者，本公司将会冻结或没收有关的款项。</p>
        <p>10. 请根据本公司的网站所提供的银行进行存款。每一次的存款请参考我们的存款表格所显示的银行。</p>
        <br>
        <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SBM811优惠的一般条款与规则适用于此优惠。</p>
        
        <br>
    </div>​</div>​<br>​</div>​</div>​</div>​​</div>
@endif
</div>
</div>
</div>
<!--MID SECTION-->
@stop

@section('bottom_js')

@stop