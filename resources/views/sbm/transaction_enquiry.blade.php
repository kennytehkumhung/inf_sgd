@extends('sbm/master')

@section('title', 'Transaction History')

@section('css')
@parent
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<link href="{{ asset('/sbm/resources/css/table.css') }}" rel="stylesheet">
@endsection

@section('js')
@parent
<script src="{{ asset('/sbm/resources/js/jquery-ui.js') }}"></script>
<script>
$(document).ready(function () {
    getBalance(true);
});

$(function () {
    $(".datepicker").datepicker({dateFormat: 'yy-mm-dd', defaultDate: new Date()});
});

function transaction_history() {
    $.ajax({
        type: "POST",
        url: "{{action('User\TransactionController@transactionProcess')}}",
        data: {
            _token: "{{ csrf_token() }}",
            date_from: $('#date_from').val(),
            date_to: $('#date_to').val(),
            record_type: $('#record_type').val()

        },
    }).done(function (json) {

        //var str;
        var str = '';
                str += '	<tr ><td>{{Lang::get('public.ReferenceNo')}}</td><td>{{Lang::get('public.DateOrTime')}}</td><td>{{Lang::get('public.type')}}</td><td>{{Lang::get('public.Amount')}}(MYR)</td><td>{{Lang::get('public.Status')}}</td><td>{{Lang::get('public.reason')}}</td></tr>';
                obj = JSON.parse(json);
        //alert(obj);
        $.each(obj, function (i, item) {
            str += '<tr><td>' + item.id + '</td><td>' + item.created + '</td><td>' + item.type + '</td><td>' + item.amount + '</td><td>' + item.status + '</td><td>' + item.rejreason + '</td></tr>';

        })

        //alert(json);
        $('#trans_history').html(str);

    });
}

setInterval(updateTrans, 10000);
setInterval(update_mainwallet, 10000);

function updateTrans() {
    $("#trans_history_button").trigger("click");
}

</script>
@endsection

@section('content')
<div class="midSectInner">
    <div class="midSectCont">
	<div class="midBottom">
	    
	    @include('sbm/include/balance_right')

	    <!--ACCOUNT MANAGAMENT MENU-->

	    <!--ACCOUNT MANAGAMENT MENU-->
	    <!--ACCOUNT TITLE-->
	    <div class="fltLeft">
		
		@include('sbm/include/fund_tab_top', array('active' => 2))
		
		<!--ACCOUNT TITLE-->
		<!--ACCOUNT CONTENT-->
		<div class="acctContent">
		    <div class="depRight">
			<span class="wallet_title"><i class="fa fa-file-o"></i>{{ Lang::get('public.TransactionHistory') }}</span>
			<div class="acctRow">
			    <label>{{ Lang::get('public.DateFrom') }} :</label>
			    <input type="text" class="datepicker" id="date_from" style="cursor:pointer;" value="{{ date('Y-m-d') }}"><br>
			</div>
			<div class="acctRow">
			    <label>{{ Lang::get('public.DateTo') }}:</label>
			    <input type="text" class="datepicker" id="date_to" style="cursor:pointer;" value="{{ date('Y-m-d') }}">
			</div>
			<div class="acctRow">
			    <label>{{ Lang::get('public.RecordType') }}:</label>
			    <select id="record_type" style="width:200px;">
				<option value="0" selected>{{ Lang::get('public.CreditAndDebitRecords') }}</option>
				<option value="1">{{ Lang::get('public.CreditRecords') }}</option>
				<option value="2">{{ Lang::get('public.DebitRecords') }}</option>
			    </select>
			</div>
			<div class="acctRowR">
			    <div class="submitAcct">
				<a id="trans_history_button" href="javascript:void(0);" onClick="transaction_history();"> {{ Lang::get('public.Submit') }}</a>
				<br><br>
			    </div>
			</div>
			<div class="clr"></div>
		    </div>
		    <div class="table" style="display:block;">
			<table id="trans_history" style="width: 99%;">
			    <tr>
				<td>{{Lang::get('public.ReferenceNo')}}</td>
				<td>{{Lang::get('public.DateOrTime')}}</td>
				<td>{{Lang::get('public.Type')}}</td>
				<td>{{Lang::get('public.Amount')}}(MYR) </td>
				<td>{{Lang::get('public.Status')}}</td>
				<td>{{Lang::get('public.Reason')}}</td>
			    </tr>
			    <tr>
				<td colspan="6"><h2><center>{{ Lang::get('public.NoDataAvailable') }}</center></h2></td>
			    </tr>
			</table>
		    </div>
		</div>
		<!--ACCOUNT CONTENT-->
		<div class="clr"></div>
	    </div>
	    <!-- /container -->
	</div>
	<div class="clr"></div>
    </div>
</div>
@endsection