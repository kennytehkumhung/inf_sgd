<!doctype html>
<html>
    <head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
	<meta name="keywords" content="@yield('keywords')"/>
	<meta name="description" content="@yield('description')"/>
	<link href="{{ asset('/sbm/favicon.ico') }}" rel="shortcut icon" type="image/icon" />
	<link href="{{ asset('/sbm/favicon.ico') }}" rel="icon" type="image/icon" />
	<title>SBM811 @yield('title')</title>
	<!--[if IE 7]>
		<link rel="stylesheet" type="text/css" href="resources/css/iefix.css" />
	<![endif]-->
	<!--CSS -->

	<link href='https://fonts.googleapis.com/css?family=Changa+One:400,400italic' rel='stylesheet' type='text/css'>
	<link href="{{ asset('/sbm/resources/css/style.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('/sbm/resources/css/skitter.styles.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('/sbm/resources/css/slick.css') }}" rel="stylesheet" type="text/css" />
	<!--SCRIPT-->
	<script src="{{ asset('/sbm/resources/js/jquery-2.1.3.min.js') }}"></script>
	<script src="{{ asset('/sbm/resources/js/jquery.megamenu.js') }}"></script>
	<script src="{{ asset('/sbm/resources/js/jquery.easing.1.3.js') }}"></script>
	<script src="{{ asset('/sbm/resources/js/jquery.skitter.min.js') }}"></script>
	<script src="{{ asset('/sbm/resources/js/jquery.tabSlideOut.v1.3.js') }}"></script>
	<script src="{{ asset('/sbm/resources/js/jquery.leanModal.min.js') }}"></script>
	@if( Auth::user()->check() )
	<script src="{{ asset('/sbm/resources/js/jquery.dropdown.min.js') }}"></script>
	<link href="{{ asset('/sbm/resources/css/jquery.dropdown.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('/sbm/resources/css/acctMngmt.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('/sbm/resources/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('/ampm/resources/css/jquery.bxslider.css') }}" type="text/css" media="all" rel="stylesheet" />
	@endif
	
	<style type='text/css'>
		.fx-dropdown-menu .dropdown-menu a {
			color: #000 !important;
		}
	</style>
	@yield('css')
	<!-- Init Skitter -->
	<script type="text/javascript" language="javascript">
		$(document).ready(function() {
			$('.box_skitter_large').skitter({
				theme: 'clean',
				numbers_align: 'center',
				progressbar: false,
				dots: true,
				preview: false
			});
		});
	</script>
	<script type="text/javascript">
		function MM_swapImgRestore() { //v3.0
			var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
		}
		function MM_preloadImages() { //v3.0
			var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
			var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
			if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
		}
		function MM_findObj(n, d) { //v4.01
			var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
			d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
			if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
			for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
			if(!x && d.getElementById) x=d.getElementById(n); return x;
		}
		function MM_swapImage() { //v3.0
			var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
			if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
		}
	</script>
	<script type="text/javascript">
                jQuery(function(){
			var SelfLocation = window.location.href.split('?');
			switch (SelfLocation[1]) {
				case "justify_right":
					jQuery(".megamenu").megamenu({ 'justify':'right' });
					break;
				case "justify_left":
				default:
					jQuery(".megamenu").megamenu();
					break;
			}
                });
	</script>
	<script type="text/javascript">
		function enterpressalert(e, textarea)
		{
			var code = (e.keyCode ? e.keyCode : e.which);
			 if(code == 13) { //Enter keycode
			   login();
			 }
		}
                function login()
                {
			$.ajax({
			type: "POST",
				url: "{{route('login')}}",
				data: {
				_token: "{{ csrf_token() }}",
					username: $('#username').val(),
					password: $('#password').val(),
					code:     $('#code').val(),
				},
			}).done(function(json) {
				obj = JSON.parse(json);
				var str = '';
				$.each(obj, function(i, item) {
					str += item + '\n';
				});
				if ("{{Lang::get('COMMON.SUCESSFUL')}}\n" == str) {
					location.reload();
				} else {
					alert(str);
				}
			});
                }

                function login2() {
			$.ajax({
			type: "POST",
				url: "{{route('login')}}",
				data: {
				_token: "{{ csrf_token() }}",
					username: $('#username2').val(),
					password: $('#password2').val(),
					code:     $('#code2').val(),
				},
			}).done(function(json) {
				obj = JSON.parse(json);
				var str = '';
				$.each(obj, function(i, item) {
					str += item + '\n';
				});
				if ("{{Lang::get('COMMON.SUCESSFUL')}}\n" == str){
					location.reload();
				} else {
					alert(str);
				}
			});
                }

                @if (Auth::user()->check())
                        function update_mainwallet(){
				$.ajax({
					type: "POST",
					url: "{{route( 'mainwallet', [ '_token'=> csrf_token() ])}}",
					beforeSend: function(balance){
					},
					success: function(balance) {
						$('.main_wallet').html(parseFloat(Math.round(balance * 100) / 100).toFixed(2));
						total_balance += parseFloat(balance);
					}
				});
                        }
			function getBalance(type) {
				var total_balance = 0;
				$.when(
					$.ajax({
					type: "POST",
						url: "{{route('mainwallet', [ '_token'=> csrf_token()])}}",
						beforeSend: function(balance){
						},
						success: function(balance){
							$('.main_wallet').html(parseFloat(Math.round(balance * 100) / 100).toFixed(2));
							total_balance += parseFloat(balance);
						}
					}),
					@foreach(Session::get('products_obj') as $prdid => $object)
						$.ajax({
							type: "POST",
							url: "{{route( 'getbalance', [ '_token'=> csrf_token(), 'product'=> $object->code ])}}&rd=<?php echo rand(10000, 99999); ?>",
							beforeSend: function(balance){
								$('.{{$object->code}}_balance').html('<img style="" src="{{url()}}/front/img/ajax-loading.gif" width="20" height="10">');
							},
							success: function(balance){
								if (balance != '{{Lang::get('Maintenance')}}') {
									$('.{{$object->code}}_balance').html(parseFloat(Math.round(balance * 100) / 100).toFixed(2));
									total_balance += parseFloat(balance);
								} else {
									$('.{{$object->code}}_balance').html(balance);
								}
							}
						})
						@if ($prdid != Session::get('last_prdid'))
						,
						@endif
					@endforeach
				).then(function() {
					$('#total_balance').html(parseFloat(Math.round(total_balance * 100) / 100).toFixed(2));
					$('#top_total_balance').html(parseFloat(Math.round(total_balance * 100) / 100).toFixed(2));
				});
			}
                @endif
	</script>

	<script type="text/javascript">
                var Tawk_API = Tawk_API || {}, Tawk_LoadStart = new Date();
                (function(){
                var s1 = document.createElement("script"), s0 = document.getElementsByTagName("script")[0];
                s1.async = true;
                s1.src = 'https://embed.tawk.to/5770f9b45bc185a150656677/1amanc4nh';
                s1.charset = 'UTF-8';
                s1.setAttribute('crossorigin', '*');
                s0.parentNode.insertBefore(s1, s0);
                })();
	</script>

	<script>
                (function(i, s, o, g, r, a, m){i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function()
                { (i[r].q = i[r].q || []).push(arguments)}
                , i[r].l = 1 * new Date(); a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
                })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
                ga('create', 'UA-80273760-1', 'auto');
                ga('send', 'pageview');
	</script>
	


    </head>
    <body>
	<!--Header-->
	<div class="topMenu">
	    <div class="topMenuInner">
		<div class="adj3">
		    <marquee>{{App\Http\Controllers\User\AnnouncementController::index()}}</marquee>
		</div>
		
		<div class="adj1">
		    <table width="auto" border="0" cellspacing="0" cellpadding="0">
			<tr>
			    <td valign="middle"><a href="{{ route( 'language', [ 'lang'=> 'en']) }}"><img src="{{ asset('/sbm/img/en-icon.png') }}" alt=""/></a></td>
			    <td valign="middle"><a href="{{ route( 'language', [ 'lang'=> 'cn']) }}"><img src="{{ asset('/sbm/img/cn-icon.png') }}" alt=""/></a></td>
			</tr>
		    </table>
		</div>
		
		<div class="dtime">
		    <?php date_default_timezone_set("Asia/Kuala_Lumpur") ?>
		    {{ date('d F Y, l, H:i:s') }} (GMT +8)
		</div>

		<div class="clr"></div>
	    </div>
	</div>

	<div class="header">
	    <div class="headerInner">

		<div class="logo">
		    <img src="{{ asset('/sbm/img/logo.jpg') }}">
		</div>

		<div class="headerRight">
			
		    <div class="headerRightTable" @if(!Auth::user()->check()) style="font-size:10px;" @endif>
			
			<div class="adj2">
				@if (Auth::user()->check())
				<div  style="width:100%;position:absolute;margin-top:-10px;margin-left:130px;"><em class="fa fa-user"></em> {{ Lang::get('public.Welcome') }} {{ Session::get('username') }}!</div>
				<table class="fx-dropdown-menu" width="auto" border="0" cellspacing="0" cellpadding="0">
				    <tr>
					<td valign="middle">
					   <div class="welc"></div>
					</td>
					<td valign="middle">
					    <div class="welc1"><a href="#" data-dropdown="#dropdown-0"><i class="fa fa-caret-square-o-right"></i>{{ Lang::get('public.Profile') }}</a></div>
					    <div id="dropdown-0" class="dropdown dropdown-tip " >
						<ul class="dropdown-menu">
						    <li><a href="{{ route('update-profile') }}">{{ Lang::get('public.MyAccountDetail') }}</a></li>
						    <li><a href="{{ route('update-password') }}">{{ Lang::get('public.AccountPassword2') }}</a></li>
						    <li><a href="{{ route('profitloss') }}">{{ Lang::get('public.BetHistory') }}</a></li>
						</ul>
					    </div>
					</td>
					<td valign="middle">
					    <div class="welc1"><a href="#" onclick="getBalance(false);" data-dropdown="#dropdown-1"><i class="fa fa-money"></i>{{ Lang::get('public.Balance') }}</a></div>
					    <div id="dropdown-1" class="dropdown dropdown-tip " >
						<ul class="dropdown-menu">
						    <li class="noAttr">
							<div class="bal">
							    
							    @include('sbm/trans_top')
							    
							</div>
							<div class="clr"></div>
						    </li>
						</ul>
					    </div>
					</td>
					<td valign="middle">
					    <div class="welc1"> <a href="#" data-dropdown="#dropdown-2"><i class="fa fa-calculator"></i>{{ Lang::get('public.Fund') }}</a></div>
					    <div id="dropdown-2" class="dropdown dropdown-tip " >
						<ul class="dropdown-menu">
							<li><a href="{{ route('deposit') }}">{{Lang::get('public.Deposit')}}</a></li>
							<li><a href="{{ route('withdraw') }}">{{Lang::get('public.Withdrawal')}}</a></li>
							<li><a href="{{ route('transaction') }}">{{Lang::get('public.TransactionHistory')}}</a></li>
							<li><a href="{{ route('transfer') }}">{{Lang::get('public.Transfer')}}</a></li>
							<!--<li><a href="#4">{{ Lang::get('public.MultipleTransfer') }}</a></li>-->
						</ul>
					    </div>
					</td>
					<td valign="middle"><div class="loginBtn"><a href="{{ route('logout') }}">{{ Lang::get('public.Logout') }}</a></div></td>
				    </tr>
				</table>
				@else
				<table width="auto" border="0" cellspacing="0" cellpadding="0">
				    <tr>
					<td valign="middle"><input id="username" name="username" type="text" placeholder="{{ Lang::get('public.Username') }}" onKeyPress="enterpressalert(event, this)"></td>
					<td valign="middle"><input id="password" name="password" type="password" placeholder="{{ Lang::get('public.Password') }}" onKeyPress="enterpressalert(event, this)"></td>
					<td valign="middle"><img src="{{ route('captcha', ['type' => 'login_captcha']) }}" width="40" height="24"></td>
					<td valign="middle"><div class="code"><input id="code" name="code" type="text" placeholder="{{ Lang::get('public.Code') }}" onKeyPress="enterpressalert(event, this)"></div></td>
					<td valign="middle"><div class="loginBtn"><a id="go" rel="leanModal" name="signup" href="#" onclick="login();">{{ Lang::get('public.Login') }}</a></div></td>
				    </tr>
				</table>
			    
				<div class="fp"><a href="{{route('forgotpassword')}}">{{ Lang::get('public.ForgotPassword') }}</a></div>
				@endif
			</div>
		    </div>
		</div>

		<div class="join">
			@if (Auth::user()->check())
			<a href="{{ route('deposit') }}">{!! Lang::get('public.DepositNow') !!}</a>
			@else
			<a href="{{ route('register') }}">{{ Lang::get('public.RegisterNow') }}</a>
			@endif
		</div>
	    </div>
	</div>
	
	<div class="navContOuter">
	
	    <div class="navCont">
		<a href="#" onClick="window.open('https://chatserver.comm100.com/chatwindow.aspx?planId=482&siteId=219938&pageTitle=SBM811%20Terms%20And%20Conditions&pageUrl=http%3A%2F%2Fsbm811.com%2FTerms-and-Conditions&newurl=1&r=10&embInv=0&dirChat=0&guid=&chatGroup=','livechat','width=530,height=750')"><img src="{{ asset('/sbm/img/contactus/'.Lang::getLocale().'/livechat.png') }}" style="position:absolute;margin-left:1050px;margin-top: -40px;" onmouseout="this.src='{{ asset('/sbm/img/contactus/'.Lang::getLocale().'/livechat.png') }}'" onmouseover="this.src='{{ asset('/sbm/img/contactus/'.Lang::getLocale().'/livechat_hover.png') }}'"></a>
		<!--MegaMenu Starts-->
		<ul class="megamenu">
		    <li>
			<a href="{{ url() }}" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('home', '', '{{ asset('/sbm/img/home-icon-hover.png') }}', 0)"><img src="{{ asset('/sbm/img/home-icon.png') }}" name="home" width="30" height="30" border="0"></a>
		    </li>
                    
		    <li class="m8">
			<a href="{{route('m8b')}}" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('m8','','{{ asset('/sbm/img/m8-icon-hover.gif') }}',0)"><img src="{{ asset('/sbm/img/m8-icon.jpg') }}" name="m8" width="108" height="30" border="0"></a>
		    </li>
			<!--
                    <li class="m8">
			<a href="{{route('wft')}}" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('wft','','{{ asset('/sbm/img/wft-icon-hover.gif') }}',0)"><img src="{{ asset('/sbm/img/wft-icon.jpg') }}" name="wft" width="108" height="30" border="0"></a>
		    </li>
                    <li class="m8">
			<a href="{{route('tbs')}}" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('tbs','','{{ asset('/sbm/img/tbs-icon-hover.gif') }}',0)"><img src="{{ asset('/sbm/img/tbs-icon.jpg') }}" name="tbs" width="80" height="30" border="0"></a>
		    </li>
			-->
            <li class="m8">
			<a href="{{route('asc')}}" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('asc','','{{ asset('/sbm/img/asc-icon-hover.gif') }}',0)"><img src="{{ asset('/sbm/img/asc-icon.jpg') }}" name="asc" width="108" height="30" border="0"></a>
		    </li>
		    
		    <li @if(Lang::getLocale() == 'cn')style="margin-left:50px;"@endif>
			<a href="{{route('promotion')}}">{{ Lang::get('public.Promotion') }}</a>
		    </li>
                    
             <li @if(Lang::getLocale() == 'cn')style="margin-left:50px;"@endif>
                 <a href="{{route('tnc')}}">{{Lang::get('public.TNC')}}</a>
             </li>   
			<li @if(Lang::getLocale() == 'cn')style="margin-left:50px;"@endif>
                 <a href="{{route('contactus')}}">{{Lang::get('public.ContactUs')}}</a>
            </li>	
			<li @if(Lang::getLocale() == 'cn')style="margin-left:30px;"@endif>
                 <a href="{{route('deposit_tutorial')}}">{{Lang::get('public.Tutorial')}}</a>
            </li>	
			
                   
		</ul>
		<!--MegaMenu Ends-->
		 
	    </div>
	</div>
	

	<!--Mid Sect-->
	<div class="midSect">
		@yield('content')

		<div class="footer">
		    <div class="footerInner">
			<table width="auto" border="0" cellspacing="0" cellpadding="0">
			    <tr>
				<td><img src="{{ asset('/sbm/img/mbb-logo.png') }}" width="51" height="50"></td>
				<td><img src="{{ asset('/sbm/img/cimb-logo.png') }}" width="110" height="50"></td>
				<td><img src="{{ asset('/sbm/img/pbb-logo.png') }}" width="110" height="50"></td>
				<td><img src="{{ asset('/sbm/img/rhb-logo.png') }}" width="76" height="50"></td>
				<td><img src="{{ asset('/sbm/img/am-logo.png') }}" width="73" height="50"></td>
				<td><img src="{{ asset('/sbm/img/all-logo.png') }}" width="163" height="50"></td>
				<td><img src="{{ asset('/sbm/img/hl-logo.png') }}" width="164" height="50"></td>
			    </tr>
			</table>

			<div class="clr"></div>
		    </div>
		</div>

		<div class="footer1">
		    <div class="footerInner1">
			<div class="footerBottom">
				<ul>
					<li><a href="{{route('aboutus')}}">{{Lang::get('public.AboutUs')}}</a></li>
					<li><a href="{{route('banking')}}">{{Lang::get('public.BankingOptions')}}</a></li>
					<li><a href="{{route('contactus')}}">{{Lang::get('public.ContactUs')}}</a></li>
					<li><a href="{{route('faq')}}">{{Lang::get('public.FAQ')}}</a></li>
					<li><a href="{{route('howtojoin')}}">{{Lang::get('public.HowToJoin')}}</a></li>
					<li><a href="{{route('tnc2')}}">{{Lang::get('public.TNC')}}</a></li>

				</ul>
				<br>
				<br>
			    <span style="float:left;">Copyright © sbm811. All Rights Reserved</span>
			
			    <div class="brow">
				<table width="auto" border="0" cellspacing="0" cellpadding="0">
				    <tr>
					<td><img src="{{ asset('/sbm/img/fb-footer.png')}}" width="44" height="44"></td>
					<td><img src="{{ asset('/sbm/img/wechat-footer.png')}}" width="44" height="44"></td>
					<td>&nbsp;</td>
				    </tr>
				</table>

				<div class="clr"></div>
			    </div>

			    <div class="clr"></div>
			</div>
		    </div>
		</div>
	</div>
<!--
	<div class="slide-out-div">
	    <a class="handle" href="http://link-for-non-js-users">Content</a>
	    <img src="{{ asset('/sbm/img/ct-head.png') }}">
	    <div class="ct-bx">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		    <tr>
			<td><img src="{{ asset('/sbm/img/ml-icon.png') }}" ></td>
			<td>EMAIL:marketing@sbm811.com</td>
		    </tr>
		    <tr>
			<td><img src="{{ asset('/sbm/img/tl-icon.png') }}"></td>
			<td>TEL: 010-5263145</td>
		    </tr>
			
		    <tr>
			<td><img src="{{ asset('/sbm/img/wc-icon.png') }}"></td>
			<td>WECHAT ID: sbm811</td>
		    </tr>
		</table>
		<div class="ct-qr">
		    <img src="{{ asset('/sbm/img/qr-1.jpg') }}">
		</div>
		
	    </div>
	</div>
-->
	<script src="{{ asset('/sbm/resources/js/jquery.tabSlideOut.v1.3.js') }}"></script>
	<script type="text/javascript">
	       $(function() {
			$('a[rel*=leanModal]').leanModal({ top : 200, closeButton: ".modal_close" });
			$('.slide-out-div1').tabSlideOut1({
				tabHandle: '.handle1', //class of the element that will be your tab
				pathToTabImage: '{{ asset('/sbm/img/mdTab.png') }}', //path to the image for the tab (optionaly can be set using css)
				imageHeight: '257px', //height of tab image
				imageWidth: '55px', //width of tab image    
				tabLocation: 'right', //side of screen where tab lives, top, right, bottom, or left
				speed: 300, //speed of animation
				action: 'click', //options: 'click' or 'hover', action to trigger animation
				topPos: '200px', //position from the top
				fixedPosition: false,
				onLoadSlideOut: false                               //options: true makes it stick(fixed position) on scroll
			});
			$('.slide-out-div').tabSlideOut({
				tabHandle: '.handle', //class of the element that will be your tab
				pathToTabImage: '{{ asset('/sbm/img/ctTab.jpg') }}', //path to the image for the tab (optionaly can be set using css)
				imageHeight: '200px', //height of tab image
				imageWidth: '41px', //width of tab image    
				tabLocation: 'left', //side of screen where tab lives, top, right, bottom, or left
				speed: 300, //speed of animation
				action: 'click', //options: 'click' or 'hover', action to trigger animation
				topPos: '200px', //position from the top
				fixedPosition: false,
				onLoadSlideOut: false                               //options: true makes it stick(fixed position) on scroll
			});
		});
	</script>
	<script src="{{ asset('/sbm/resources/js/slick.min.js') }}"></script>
	<script>
		$('.caro').slick({
			infinite: true,
			slidesToShow: 3,
			slidesToScroll: 3,
			autoplay: true,
			variableWidth: false,
			arrows: true
		});
	</script>
	@yield('js')
	@yield('bottom_js')
    </body>
</html>
