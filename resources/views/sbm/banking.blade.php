@extends('sbm/master')

@section('title', Lang::get('public.Banking'))

@section('top_js')
<link href="{{ asset('/sbm/resources/css/promo.css') }}" rel="stylesheet" type="text/css" />
@stop

@section('content')

<div class="midSect">
<div class="midSectInner2">
<div class="midSectCont">

<?php echo htmlspecialchars_decode($content); ?>

</div>
</div>
</div>

@stop

@section('bottom_js')

@stop