@extends('sbm/master')

@section('title', Lang::get('public.TNC'))

@section('top_js')
<link href="{{url()}}/front/resources/css/otherPg.css" rel="stylesheet">
@stop

<style>
.displayed {
    display: block;
    margin-left: auto;
    margin-right: auto }
	.promo table {
    color: black;
}
.promo table th {
    background: red none repeat scroll 0 0;
    border-right: 1px solid #000;
    color: white;
    padding: 11px;
}
.promo table td {
    border-right: 1px solid #000;
    padding: 11px;
    text-align: center;
}
.promo table th:last-child {
    border-right: 0 none;
}
.promo table td:last-child {
    border-right: 0 none;
}
.promo table tr:nth-child(2n+1) {
    background: #ccc none repeat scroll 0 0;
}
.promo table tr:nth-child(2n) {
    background: #fff none repeat scroll 0 0;
}

</style>

@section('content')
<!--MID SECTION-->
<div class="midSect">
<div class="midSectInner2">
<div class="midSectCont">
<?php echo htmlspecialchars_decode($content); ?>
</div>
</div>
</div>
<!--MID SECTION-->
@stop

@section('bottom_js')

@stop