<div class="midSect bglc">
    <div class="midSectInner">
        <div class="midSectCont">
            <div class="slotTit">{{Lang::get('public.Slots')}}</div>
            <div class="lcCont">
                <table width="auto" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        @if(in_array('MXB',Session::get('valid_product')))
                        <td>
                            <div class="lcas1">
                                <div class="lcBox2">
                                    <div id="crossfade">
                                        <a href="javascript:void(0)" onClick="change_iframe('{{route('mxb' , [ 'type' => 'slot' , 'category' => 'Slots' ] )}}','bt-slot-thumb-hover.png','mxb')">
                                            @if( $name == 'mxb' )
                                                <img class="bottom" src="{{url()}}/c59/img/bt-slot-thumb-hover.png" />
                                            @else
                                                <img class="bottom" src="{{url()}}/c59/img/bt-slot-thumb-hover.png" />
                                                <img class="top slot_mxb_image" src="{{url()}}/c59/img/bt-slot-thumb.png" /> 
                                            @endif
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </td>
                        @endif
                        @if(in_array('PLTB',Session::get('valid_product')))
                        <td>
                            <div class="lcas1 marL">
                                <div class="lcBox2">
                                    <div id="crossfade">
                                        <a href="javascript:void(0)" onClick="change_iframe('{{route('pltb' , [ 'type' => 'pgames' , 'category' => '1' ] )}}','pt-slot-hover.png','pltb')">
                                            @if( $name == 'pltb' )
                                                <img class="bottom" src="{{url()}}/c59/img/pt-slot-thumb-hover.png" />
                                            @else
                                                <img class="bottom" src="{{url()}}/c59/img/pt-slot-thumb-hover.png" />
                                                <img class="top slot_plt_image" src="{{url()}}/c59/img/pt-slot-thumb.png" />
                                            @endif
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </td>
                        @endif
                    </tr>
                    <tr>
                        @if(in_array('W88',Session::get('valid_product')))
                        <td>
                            <div class="lcas1">
                                <div class="lcBox2">
                                    <div id="crossfade">
                                        <a href="javascript:void(0)" onClick="change_iframe('{{route('w88' , [ 'type' => 'slot' , 'category' => 'Arcades' ] )}}','gp-slot-hover.png','w88')">
                                            @if( $name == 'w88' )
                                            <img class="bottom" src="{{url()}}/c59/img/gp-slot-thumb-hover.png" />
                                            @else
                                            <img class="bottom" src="{{url()}}/c59/img/gp-slot-thumb-hover.png" />
                                            <img class="top slot_w88_image" src="{{url()}}/c59/img/gp-slot-thumb.png" />
                                            @endif
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </td>
                        @endif
                    </tr>
                </table>
            </div>