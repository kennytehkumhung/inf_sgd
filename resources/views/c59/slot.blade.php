@extends('c59/master')

@section('title', 'Thailand Online Casino, Gambling Games | Club5599')
@section('keywords', 'Thailand Online Casino, Gambling Games | Club5599')
@section('description', 'Enjoy gambling (kasino) games at Thailand most trusted online casino, Club5599. Play live casino, slots, horse racing and more.')

@section('top_js')
<link href="{{url()}}/c59/resources/css/slot.css" rel="stylesheet">

<script>
$(document).ready(function() { 
       @if( $type == 'mxb' )
               $('.slot_mxb_image').hide();
       @elseif( $type == 'plt' )
               $('.slot_plt_image').hide();	
       @elseif( $type == 'a1a' )
               $('.slot_a1a_image').hide();	
       @elseif( $type == 'w88' )
               $('.slot_w88_image').hide();
       @endif
});
function open_game(gameid){


}
  function resizeIframe(obj){
    obj.style.height = 0;
    obj.style.height = obj.contentWindow.document.body.scrollHeight + 'px';
 }

 function change_iframe(url,image_link,product){
         $('#iframe_game').attr('src',url);
         $('.top').show();
         $('.slot_'+product+'_image').hide();
 }
</script>
@stop

@section('content')
@include('c59/slot_top')
<!--Slot-->
	<iframe id="iframe_game" frameborder="0" style="overflow:hidden;overflow-x:hidden;overflow-y:hidden;height: 970px; width:1053px; margin-left: -11px;" src="
	@if( $type == 'mxb' )
		{{route('mxb' , [ 'type' => 'slot' , 'category' => 'Slots' ] )}}
	@elseif( $type == 'w88' )
		{{route('w88', [ 'type' => 'slot' , 'category' => 'Arcades' ] )}}
	@elseif( $type == 'plt' || $type == 'pltb')
		{{route('pltb' , [ 'type' => 'pgames' , 'category' => '1' ] )}}
	@elseif( $type == 'a1a' )
		{{route('a1a')}}
	@endif
	"></iframe>
<!--Slot-->
@stop