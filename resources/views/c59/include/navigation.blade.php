<div class="header">
    <div class="headerInner">
        <div class="logo">
        <img src="{{url()}}/c59/img/logo.png">
        </div>
        
        <div class="pnlWrap"></div>
        
        <div class="dtime">
            <!--30 Mar 2015, Mon, 15:03:12 (GMT +8)-->
            <?php date_default_timezone_set("Asia/Bangkok");?>
            <?php echo date("d M Y");?>,
            <?php echo substr(date("l"),0,3);?>,
            <?php echo date(" H:i:s", time());?> (GMT +7)
            <em style="margin-left:15px;" class="fa fa-ellipsis-v fa-1x"></em> <em class="fa fa-user"></em> {{Lang::get('public.Welcome')}}, {{Session::get('username')}}!<em style="margin-left:15px;" class="fa fa-ellipsis-v fa-1x"></em>
        </div>
        
        <div class="headerRight">
            <div class="headerRightTable">
                <div class="adj1">
                    <table width="auto" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td valign="middle"><a href="{{route( 'language', [ 'lang'=> 'en'])}}"><img src="{{url()}}/c59/img/eng-icon.png" alt=""/></a></td>
                            <td valign="middle"><a href="{{route( 'language', [ 'lang'=> 'th'])}}"><img src="{{url()}}/c59/img/thai-icon.png" alt=""/></a></td>
                        </tr>
                    </table>
                </div>
                
               
                <div class="adj2">
                    <table width="auto" border="0" cellspacing="0" cellpadding="0">
                        @if (!Auth::user()->check())
                        <tr>
                            <td valign="middle"><input type="text" placeholder="{{Lang::get('public.Username')}}" id="username"></td>
                            <td valign="middle"><input type="password" placeholder="{{Lang::get('public.Password')}}" id="password"></td>
                            <td valign="middle"><img style="float: left;"  src="{{route('captcha', ['type' => 'login_captcha'])}}" width="50px" height="24px" /></td>
                            <td valign="middle"><input type="text" style="width:40px !important;"placeholder="Code" id="code" onKeyPress="enterpressalert(event, this)"></td>
                            <td valign="middle" width="100px"><div class="fp"><a href="{{route('forgotpassword')}}">{{Lang::get('public.ForgotPassword')}}</a></div></td>
                            <td valign="middle"><div class="loginBtn"><a onclick="login()" href="#">{{Lang::get('public.Login')}}</a></div></td>
                        </tr>
                        @else
                            <tr>
                                <!--<td align="center"><div class="welc" style="font-size:11px;"><em class="fa fa-user"></em> {{Lang::get('public.Welcome')}}, {{Session::get('username')}}!<em style="margin-left:15px;" class="fa fa-ellipsis-v fa-1x"></em></div></td>-->
                            <td align="right"><div class="welc1"><a href="#" data-dropdown="#dropdown-0"><i class="fa fa-caret-square-o-right"></i>{{Lang::get('public.Profile')}}</a></div>
                                <div id="dropdown-0" class="dropdown dropdown-tip " >
                                    <ul class="dropdown-menu">
                                        <li><a href="{{route('update-profile')}}">{{Lang::get('public.MyAccount')}}</a></li>
                                        <li><a href="{{route('update-password')}}">{{Lang::get('public.ChangePassword')}}</a></li>
                                        <li><a href="{{route('memberbank')}}">{{Lang::get('COMMON.CTABBANKSETTING')}}</a></li>
                                    </ul>
                                </div>
                            </td>
                            <td>|</td>
                            <td align="center"><div class="welc1"><a href="#" data-dropdown="#dropdown-1" onclick="getBalance();"><i class="fa fa-money"></i>{{Lang::get('public.Balance')}}</a></div>
                                <div id="dropdown-1" class="dropdown dropdown-tip " >
                                    <ul class="dropdown-menu" style="min-width:245px;">
                                        <li class="noAttr">
                                            <div class="bal">
                                                <ul style="width:60%;">
                                                    <li style="font-size:13px;">{{Lang::get('public.Wallet')}}</li>
                                                    <table width='100%'>
                                                        <tr>
                                                            <td style="color:white;font-size:13px;" align="center">{{Lang::get('public.MainWallet')}}</td>
                                                        </tr>
                                                        @foreach( Session::get('products_obj') as $prdid => $object)
                                                        <tr>
                                                            <td style="color:white;font-size:13px;" align="center">{{$object->name}}</td>
                                                        </tr>
                                                        @endforeach
                                                    </table>
                                                    <li style="font-size:13px;">{{Lang::get('public.Total')}}</li>
                                                </ul>
                                                <ul style="width:40%;">
                                                    <li style="font-size:13px;">{{Lang::get('public.Balance')}}</li>
                                                    <table width='100%'>
                                                        <tr>
                                                            <td class="main_wallet" style="color:white;font-size:13px;" align="center">0.00</td>
                                                        </tr>
                                                        @foreach( Session::get('products_obj') as $prdid => $object)
                                                        <tr>
                                                            <td class="{{$object->code}}_balance" style="color:white;font-size:13px;" align="center">0.00</td>
                                                        </tr>
                                                        @endforeach
                                                    </table>
                                                    <li id="total_balance" style="font-size:13px;">0.00</li>
                                                </ul>
                                            </div>
                                            <div class="clr"></div>
                                        </li>
                                    </ul>
                                </div>
                            </td>
                            <td>|</td>
                            <td align="left">
                                <div class="welc1"> <a href="#" data-dropdown="#dropdown-2"><i class="fa fa-calculator"></i>{{Lang::get('public.Fund')}}</a></div>
                                <div id="dropdown-2" class="dropdown dropdown-tip " >
                                    <ul class="dropdown-menu">
                                        <li><a href="{{route('deposit')}}">{{Lang::get('public.Deposit')}}</a></li>
                                        <li><a href="{{route('withdraw')}}">{{Lang::get('public.Withdrawal')}}</a></li>
                                        <li><a href="{{route('transaction')}}">{{Lang::get('public.TransactionHistory')}}</a></li>
                                        <li><a href="{{route('transfer')}}">{{Lang::get('public.Transfer')}}</a></li>
                                    </ul>
                                </div>
                            </td>
                            <td align="center"><div class="loginBtn"><a href="{{route('logout')}}"><p style="padding-top:0px !important;">{{Lang::get('public.Logout')}}</p></a></div></td>
                            </tr>
                        @endif
                    </table>
                </div>
            </div>
        </div>
        
        <div class="adj3">
            <marquee>{{App\Http\Controllers\User\AnnouncementController::index()}}</marquee>
        </div>
        
        <div class="join">
            @if (!Auth::user()->check())
                <a href="{{route('register')}}">
                    @if(Lang::getLocale() == 'en')
                        <img src="{{url()}}/c59/img/joinNow.png">
                    @else
                        <img src="{{url()}}/c59/img/joinNow-th.png">
                    @endif    
                </a>
            @else
                <a href="{{route('deposit')}}">
                    @if(Lang::getLocale() == 'en')
                        <img src="{{url()}}/c59/img/depNow.png">
                    @else
                        <img src="{{url()}}/c59/img/depNow-th.png">
                    @endif
                </a>
            @endif
        </div>
    </div>
</div>