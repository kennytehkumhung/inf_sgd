<!DOCTYPE html>
<html lang="en">
    <head>
        <link rel="shortcut icon" href="{{url()}}/front/favicon.ico" type="image/icon"/>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
        <link href="" rel="shortcut icon" type="image/icon" />
        <link href="" rel="icon" type="image/icon" />
        <title>@yield('title')</title>
        <meta name="keywords" content="@yield('keywords')"/>
        <meta name="description" content="@yield('description')"/>	
        <!--css-->
        <link href='https://fonts.googleapis.com/css?family=Changa+One:400,400italic' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Rubik:700' rel='stylesheet' type='text/css'>
        <link href="{{url()}}/c59/resources/css/style.css" rel="stylesheet" type="text/css" />
        <link href="{{url()}}/c59/resources/css/jquery.megamenu.css" rel="stylesheet" type="text/css" />
        <link href="{{url()}}/c59/resources/css/jquery.bxslider.css" rel="stylesheet" type="text/css" />
        <link href="{{url()}}/c59/resources/css/slick.css" rel="stylesheet" type="text/css" />
        <link href="{{url()}}/c59/resources/css/leanmodal.css" rel="stylesheet" type="text/css" />
        <link href="{{url()}}/c59/resources/css/jquery.dropdown.css" rel="stylesheet" type="text/css" />
        @if (Auth::user()->check())
        <link href="{{url()}}/c59/resources/css/acctMngmt.css" rel="stylesheet" type="text/css" />
        @endif
        <link href="{{url()}}/c59/resources/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!--SCRIPT-->
        <script src="{{url()}}/c59/resources/js/jquery-2.1.3.min.js"></script>
        <script src="{{url()}}/c59/resources/js/jquery.megamenu.js"></script>
        <script src="{{url()}}/c59/resources/js/jquery.leanModal.min.js"></script>
        <script src="{{url()}}/c59/resources/js/jquery.easing.1.3.js"></script>
        <script src="{{url()}}/c59/resources/js/jquery.bxslider.min.js"></script>
        <script src="{{url()}}/c59/resources/js/jquery.dropdown.min.js"></script>

	<script type="text/javascript">
            $(document).ready(function() { 
                   $(".lgCont li a").click(function() { 
                          $("link").attr("href",$(this).attr('rel'));
                          $.cookie("css",$(this).attr('rel'), {expires: 365, path: '/'});
                          return false;
                   });

                    //$('.headerLeft').css("background-image", "url({{url()}}/front/img/logoRJ.png)");  
            });
            jQuery(function(){
              var SelfLocation = window.location.href.split('?');
              switch (SelfLocation[1]) {
                case "justify_right":
                  jQuery(".megamenu").megamenu({ 'justify':'right' });
                  break;
                case "justify_left":
                default:
                  jQuery(".megamenu").megamenu();
              }
            });
	</script>
	@yield('top_js')
	<script type="text/javascript">
		function login(){
			$.ajax({
				type: "POST",
				url: "{{route('login')}}",
				data: {
					_token: "{{ csrf_token() }}",
					username: $('#username').val(),
					password: $('#password').val(),
					code:     $('#code').val(),
				},
			}).done(function( json ) {
					 obj = JSON.parse(json);
					 var str = '';
					 $.each(obj, function(i, item) {
						str += item + '\n';
					})
					if( "{{Lang::get('COMMON.SUCESSFUL')}}\n" == str ){
						location.reload(); 
					}else{
						alert(str);
					}
					
			});
		}
		function enterpressalert(e, textarea){

			var code = (e.keyCode ? e.keyCode : e.which);
			 if(code == 13) { //Enter keycode
			   login();
			 }
		}
		@if (Auth::user()->check())
			function update_mainwallet(){
				$.ajax({
					  type: "POST",
					  url: "{{route( 'mainwallet', [ '_token'=> csrf_token() ])}}",
					  beforeSend: function(balance){
					
					  },
					  success: function(balance){
						$('.main_wallet').html(parseFloat(Math.round(balance * 100) / 100).toFixed(2));
						total_balance += parseFloat(balance);
					  }
				 });
					
			}
			function getBalance(type){
				total_balance = 0;
				$.when(
				
					 $.ajax({
								  type: "POST",
								  url: "{{route('mainwallet', [ '_token'=> csrf_token()])}}",
								  beforeSend: function(balance){
							
								  },
								  success: function(balance){
									$('.main_wallet').html(parseFloat(Math.round(balance * 100) / 100).toFixed(2));
									total_balance += parseFloat(balance);
								  }
					 }),
					@foreach( Session::get('products_obj') as $prdid => $object)
					
				
							 $.ajax({
								  type: "POST",
								  url: "{{route( 'getbalance', [ '_token'=> csrf_token(), 'product'=> $object->code ])}}",
								  beforeSend: function(balance){
									$('.{{$object->code}}_balance').html('<img style="float:left;position:static;top:0px;margin-left:20px;" src="{{url()}}/front/img/ajax-loading.gif" width="18" height="18">');
								  },
								  success: function(balance){
									  if( balance != '{{Lang::get('Maintenance')}}')
									  {
										$('.{{$object->code}}_balance').html(parseFloat(Math.round(balance * 100) / 100).toFixed(2));
										total_balance += parseFloat(balance);
									  }
									  else
									  {
										  $('.{{$object->code}}_balance').html(balance);
									  }
								  }
							 })
						  @if($prdid != Session::get('last_prdid')) 
							,
						  @endif
					@endforeach

					
				).then(function() {
						
						$('#total_balance').html(parseFloat(Math.round(total_balance * 100) / 100).toFixed(2));
                                                $('#top_total_balance').html(parseFloat(Math.round(total_balance * 100) / 100).toFixed(2));
				});
			
			}
		@endif
	</script>
	<script>
	$(function() {
		// Clickable Dropdown
		$('.click-nav > ul').toggleClass('no-js js');
		$('.click-nav .js ul').hide();
		$('.click-nav .js').click(function(e) {
			$('.click-nav .js ul').slideToggle(200);
			$('.clicker').toggleClass('active');
			e.stopPropagation();
		});
		$(document).click(function() {
			if ($('.click-nav .js ul').is(':visible')) {
				$('.click-nav .js ul', this).slideUp();
				$('.clicker').removeClass('active');
			}
		});
	});
	
	$(function() {
		// Clickable Dropdown
		$('.click-nav1 > ul').toggleClass('no-js js');
		$('.click-nav1 .js ul').hide();
		$('.click-nav1 .js').click(function(e) {
			$('.click-nav1 .js ul').slideToggle(200);
			$('.clicker1').toggleClass('active');
			e.stopPropagation();
		});
		$(document).click(function() {
			if ($('.click-nav1 .js ul').is(':visible')) {
				$('.click-nav1 .js ul', this).slideUp();
				$('.clicker1').removeClass('active');
			}
		});
	});
	
	$(function() {
		// Clickable Dropdown
		$('.click-nav2 > ul').toggleClass('no-js js');
		$('.click-nav2 .js .wlt').hide();
		$('.click-nav2 .js').click(function(e) {
			if (!$('.click-nav2 .js .wlt').is(':visible')) {
				getBalance(true);
			}
			
			$('.click-nav2 .js .wlt').slideToggle(200);
			$('.clicker2').toggleClass('active');
			e.stopPropagation();
		});
		$(document).click(function() {
			
			if ($('.click-nav2 .js .wlt').is(':visible')) {
				$('.click-nav2 .js .wlt', this).slideUp();
				$('.clicker2').removeClass('active');
			}
		});
	});
	
	
	$(function() {
		// Clickable Dropdown
		$('.click-nav3 > ul').toggleClass('no-js js');
		$('.click-nav3 .js ul').hide();
		$('.click-nav3 .js').click(function(e) {
			$('.click-nav3 .js ul').slideToggle(200);
			$('.clicker3').toggleClass('active');
			e.stopPropagation();
		});
		$(document).click(function() {
			if ($('.click-nav3 .js ul').is(':visible')) {
				$('.click-nav3 .js ul', this).slideUp();
				$('.clicker3').removeClass('active');
			}
		});
	});
	function livechat() {
		@if( Session::get('currency') == 'MYR')
			window.open('https://secure.livechatinc.com/licence/3155342/open_chat.cgi?groups=2', '', 'width=525, height=520, top=150, left=250');
		@elseif( Session::get('currency') == 'VND')
			window.open('https://secure.livechatinc.com/licence/3155342/open_chat.cgi?groups=0', '', 'width=525, height=520, top=150, left=250');	
		@elseif( Session::get('currency') == 'IDR')
			window.open('https://secure.livechatinc.com/licence/5548521/open_chat.cgi?groups=0', '', 'width=525, height=520, top=150, left=250');
		@endif
	}
	</script>
        <style>
        .loading_wallet{

                float:left;position:static;top:0px;left:0px;
        }

        </style>
    </head>