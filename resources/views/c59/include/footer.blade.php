            <div class="footer">
                <div class="footerMenu">
                    <div class="fmenuLeft">
                        <ul>
                            <li><a href="{{route('aboutus')}}">{{Lang::get('public.AboutUs')}}</a></li>
                            <li><a href="{{route('banking')}}">{{Lang::get('public.Banking')}}</a></li>
                            <li><a  class="last" href="{{route('contactus')}}">{{Lang::get('public.ContactUs')}}</a></li>
                        </ul>
                    </div>

                    <div class="fmenuRight">
                        Copyright © Club5599. All Rights Reserved.
                    </div>
                    <div class="clr"></div>
                </div>

                <div class="footerb1">
                    <div class="fbLeft">
                        <table width="auto" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td colspan="3">Connect With Us</td>
                            </tr>
                            <tr>
                                <td><img src="{{url()}}/c59/img/fb-foot-icon.png" width="36" height="36"></td>
                                <td><img src="{{url()}}/c59/img/fb-skype-icon.png" width="36" height="36"></td>
                                <td><img src="{{url()}}/c59/img/fb-twit-icon.png" width="36" height="36"></td>
                            </tr>
                        </table>
                    </div>

                    <div class="fbRight">
                    <table width="auto" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td colspan="6" align="right">Product Sponsor of </td>
                        </tr>
                        <tr>
                            <td><img src="{{url()}}/c59/img/pt-foot-icon.png" width="119" height="36"></td>
                            <td><img src="{{url()}}/c59/img/ag-foot-icon.png"></td>
                            <td><img src="{{url()}}/c59/img/gp-foot-icon.png"></td>
                            <td><img src="{{url()}}/c59/img/crown-foot-icon.png"></td>
                            <td><img src="{{url()}}/c59/img/all-foot-icon.png"></td>
                            <td><img src="{{url()}}/c59/img/xpro-foot-icon.png"></td>
                        </tr>
                    </table>
                    </div>

                    <div class="clr"></div>
                </div>

                <div class="footerb1 mar">
                    <div class="fbLeft">
                        <table width="auto" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td colspan="3">Supported Payment Methods</td>
                            </tr>
                            <tr>
                                <td align="center"><img src="{{url()}}/c59/img/bk2.jpg"></td>
                                <td align="center"><img src="{{url()}}/c59/img/bk3.jpg"></td>
                                <td align="center"><img src="{{url()}}/c59/img/bk5.jpg"></td>
                                <td align="center"><img src="{{url()}}/c59/img/bk6.jpg"></td>
                                <td align="center"><img src="{{url()}}/c59/img/bk3-foot-icon.png"></td>
                                <td align="center"><img src="{{url()}}/c59/img/bk1-foot-icon.png"></td>
                            </tr>
                        </table>
                    </div>

                    <div class="fbRight">
                        <table width="auto" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td colspan="6" align="right">Supported Browser</td>
                             </tr>
                            <tr>
                                <td><img src="{{url()}}/c59/img/mz-foot-icon.png"></td>
                                <td><img src="{{url()}}/c59/img/chrome-foot-icon.png"></td>
                                <td><img src="{{url()}}/c59/img/ie-foot-icon.png"></td>
                            </tr>
                        </table>
                    </div>

                    <div class="clr"></div>
                </div>

                <div class="clr"></div>
            </div>
        </div>
    </div>
</div>