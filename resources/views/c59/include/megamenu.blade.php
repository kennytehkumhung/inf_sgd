<div class="navContOuter">
    <div class="navCont">
    <!--MegaMenu Starts-->
        <ul class="megamenu">      
            <li>
                <a href="{{route('homepage')}}" style="width:55px;"><img src="{{url()}}/c59/img/home_icon.png" width="24" height="24"></a>
            </li> 
            <li>
                <a href="{{route('mobile')}}" style="width:55px;"><img src="{{url()}}/c59/img/phone_icon.png" width="24" height="24"></a>
            </li> 
            <li>
                <a href="{{route('livecasino')}}">{{Lang::get('public.LiveCasino')}}</a>
                <ul>
                    <li>
                        @if(in_array('PLTB',Session::get('valid_product')))
                        <a href="#" onClick="@if (Auth::user()->check())window.open('{{route('pltbiframe')}}', 'casinoplt', 'width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif">Playtech Casino</a>
                        @endif
                    </li>
                    <li>
                        @if(in_array('AGG',Session::get('valid_product')))
                        <a href="#" onClick="@if (Auth::user()->check())window.open('{{route('agg')}}', 'casino11', 'width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif">AG Casino</a>
                        @endif
                    </li>
                    <li>
                        @if(in_array('W88',Session::get('valid_product')))
                        <a href="#" onClick="@if (Auth::user()->check())window.open('{{route('w88', [ 'type' => 'casino' , 'category' => 'live' ] )}}', 'casino12', 'width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif">Gameplay Casino</a>
                        @endif
                    </li>
                    <li>
                        @if(in_array('MXB',Session::get('valid_product')))
                        <a onClick="@if (!Auth::user()->check())alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif" href="@if (Auth::user()->check()){{route('mxb',[ 'type' => 'casino' , 'category' => 'baccarat' ])}}@endif">Xpro Casino</a>
                        @endif
                    </li>
                    <li>
                        @if(in_array('ALB',Session::get('valid_product')))
                        <a href="#" onClick="@if (Auth::user()->check())window.open('{{route('alb', [ 'type' => 'casino' ] )}}', 'casino12', 'width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif">Allbet Casino</a>
                        @endif
                    </li>
                    <li>
                        @if(in_array('CT8',Session::get('valid_product')))
                        <a href="#" onClick="@if (Auth::user()->check())window.open('{{route('ct8')}}', 'casino13', 'width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif">Crown Casino</a>
                        @endif
                    </li>
                    
                </ul>
            </li>
            
            <li>
              <a href="{{route('ibc')}}">{{Lang::get('public.SportsBook')}}</a>
            </li>
            
            <li>
                <a href="{{route('slot', [ 'type' => 'mxb'  ])}}">{{Lang::get('public.Slots')}}</a>
                <ul>
                    <li>
                        @if(in_array('MXB',Session::get('valid_product')))
                        <a href="{{route('slot', [ 'type' => 'mxb'  ])}}">Betsoft Slot</a>
                        @endif
                    </li>
                    <li>
                        @if(in_array('PLTB',Session::get('valid_product')))
                        <a href="{{route('slot', [ 'type' => 'pltb'  ])}}">Playtech Slot</a>
                        @endif
                    </li>
                    <li>
                        @if(in_array('W88',Session::get('valid_product')))
                        <a href="{{route('slot', [ 'type' => 'w88'  ])}}">Gameplay Slot</a>
                        @endif
                    </li>
                </ul>
            </li>
            
            <li>
                <a href="{{route('lotto')}}">{{Lang::get('public.Lottery')}}</a>
            </li>
            
            <li>
                <a href="#" onClick="@if (Auth::user()->check())window.open('{{route('livefootball')}}', 'livefootball', 'width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif">{{Lang::get('public.LiveFootball')}}</a>
            </li>
            
            <li>
                <a href="{{route('promotion')}}">{{Lang::get('public.Promotion')}}</a>
            </li>
            
            <li style="margin-right: 0px;">
                <a style="margin-right: 0px;" href="{{route('contactus')}}">{{Lang::get('public.ContactUs')}}</a>
            </li>
            
        </ul>
    <!--MegaMenu Ends-->
    </div>
</div>