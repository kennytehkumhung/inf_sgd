@extends('c59/master')

@section('title', Lang::get('COMMON.CTABBANKSETTING'))
@section('keywords', 'Thailand Online Casino, Gambling Games | Club5599')
@section('description', 'Enjoy gambling (kasino) games at Thailand most trusted online casino, Club5599. Play live casino, slots, horse racing and more.')

@section('content')

<script>
 function update_bank(){

	if(  $('#bnkaccno').is(':disabled') ){
		alert('Bank Account number can update one time only!');
	}
	 
	$.ajax({
		type: "POST",
		url: "{{route('updatebank')}}?"+$('#bank_form').serialize(),
	
	}).done(function( result ) {
		if(result == 1){
			alert('{{Lang::get('COMMON.SUCESSFUL')}}');
                        location.reload();
		}

					
	});
} 
function load_bank(id,type){
	
	if( type == 'default' ){
		var id = id;
	}else{
		var id = id.value;
	}
	
	$.ajax({
		type: "POST",
		url: "{{route('loadbank')}}",
		data: {
			id:  id
		},
	}).done(function( result ) {
		  $('#bnkaccno').val(result);
		  if( result == '' )
			$("#bnkaccno").removeAttr('disabled');
		  else
			$("#bnkaccno").attr('disabled','disabled');
	});
}
@foreach( $bankObj as $bank)
load_bank('{{$bank->code}}','default');
<?php break; ?> 
@endforeach
</script>

@include('c59/transaction_mange_top', [ 'title' => 'Bank Setting' ] )

    <div class="title_bar">
        <span>{{Lang::get('COMMON.CTABBANKSETTING')}}</span>
    </div>
    <!--ACCOUNT TITLE-->
   <!--ACCOUNT CONTENT-->
    <div class="acctContent">
        <span class="wallet_title"><i class="fa fa-money"></i>{{Lang::get('COMMON.CTABBANKSETTING')}}</span>
        <form id="bank_form">
            <div class="acctRow">
                <label>{{Lang::get('public.Bank')}} :</label>
                @if($accbank != '')
                    <span class="acctText">{{$accbank->bankname}}</span>                 
                @else
                    <span class="acctText">
                        <select name="bnkid" onChange="load_bank(this,'none')">
                        @foreach( $bankObj as $bank)
                                <option value="{{$bank->code}}" >{{$bank->name}}</option>
                        @endforeach
                        </select>
                    </span>
                @endif
                <div class="clr"></div>
            </div>    

            <div class="acctRow">
                <label>{{Lang::get('COMMON.BANKACCNAME')}} :</label>
                <span class="acctText">{{Session::get('fullname')}}</span>
                <div class="clr"></div>
            </div>  

            <div class="acctRow">
                <label>{{Lang::get('COMMON.BANKACCNO')}} :</label>
                @if($accbank != '')
                    <span class="acctText">{{$accbank->bankaccno}}</span>                 
                @else
                    <span class="acctText"><input type="text" name="bnkaccno" id="bnkaccno"></span>
                @endif
                <div class="clr"></div>
            </div>  

                    @if($accbank == '')
			<div class="submitAcct" onClick="update_bank()">
				<a href="#"  >{{Lang::get('public.Submit')}}</a>
			</div>
                    @endif
        </form>
    </div>


@stop
