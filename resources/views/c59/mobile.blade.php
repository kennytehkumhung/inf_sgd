@extends('c59/master')

@section('title', 'Mobile | Club5599')

@section('content')
<!--Mobile-->
<div class="midSect bglc">
    <div class="midSectInner">
        <div class="midSectCont">
            <div class="mobTit">{{Lang::get('public.Mobile')}}</div>

            <div class="mobCont">
                <div class="mobR">
                    <div class="mobRtd">
                        <table border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td align="center" valign="middle"><img src="{{url()}}/c59/img/mobile/playtech_live_qr.jpg" width="110" height="110" alt=""/></td>
                                <td width="50">&nbsp;</td>
                                <td align="center" valign="middle"><img src="{{url()}}/c59/img/mobile/playtech_slot_qr.jpg" width="110" height="110" alt=""/></td>
                            </tr>
                            <tr>
                                <td>
                                    <table border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td align="center" valign="middle"><img src="{{url()}}/c59/img/AND-ICON.png" width="26" height="26" alt=""/></td>
                                            <td align="center" valign="middle">{{Lang::get('public.LiveCasino')}}</td>
                                        </tr>
                                    </table>
                                </td>
                                <td width="50" align="center" valign="middle"></td>
                                <td align="center" valign="middle">
                                    <table border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td align="center" valign="middle"><img src="{{url()}}/c59/img/AND-ICON.png" width="26" height="26" alt=""/></td>
                                            <td align="center" valign="middle">{{Lang::get('public.Slots')}}</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="clr"></div>
                </div>

                <div class="mobR1">
                    <div class="mobRtd ct">
                        <table border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td align="center" valign="middle"><img src="{{url()}}/c59/img/mobile/c59_gameplay_qr.jpg" width="110" height="110" alt=""/></td>
                            </tr>
                            <tr>
                                <td>
                                    <table border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td align="center" valign="middle"><img src="{{url()}}/c59/img/AND-ICON.png" width="26" height="26" alt=""/></td>
                                            <td align="center" valign="middle">{{Lang::get('public.LiveCasino')}}</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="clr"></div>
                </div>

                <div class="mobR2">
                    <div class="mobRtd ct">
                        <table border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td align="center" valign="middle"><img src="{{url()}}/c59/img/mobile/c59_maxbet_qr.jpg" width="110" height="110" alt=""/></td>
                            </tr>
                            <tr>
                                <td>
                                    <table border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td align="center" valign="middle"><img src="{{url()}}/c59/img/AND-ICON.png" width="26" height="26" alt=""/></td>
                                            <td align="center" valign="middle">{{Lang::get('public.LiveCasino')}} / {{Lang::get('public.Slots')}}</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="clr"></div>
                </div>

                <div class="mobR3">
                    <div class="mobRtd">
                        <table border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td align="center" valign="middle"><img src="{{url()}}/c59/img/mobile/ag_qr.jpg" width="110" height="110" alt=""/></td>
                                <td width="50">&nbsp;</td>
                                <td align="center" valign="middle"><img src="{{url()}}/c59/img/mobile/ag_qr.jpg" width="110" height="110" alt=""/></td>
                            </tr>
                            <tr>
                                <td>
                                    <table border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td align="center" valign="middle"><img src="{{url()}}/c59/img/apple-icon.png" width="26" height="26" alt=""/></td>
                                            <td align="center" valign="middle">{{Lang::get('public.LiveCasino')}}</td>
                                        </tr>
                                    </table>
                                </td>
                                <td width="50" align="center" valign="middle"></td>
                                <td align="center" valign="middle">
                                    <table border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td align="center" valign="middle"><img src="{{url()}}/c59/img/AND-ICON.png" width="26" height="26" alt=""/></td>
                                            <td align="center" valign="middle">{{Lang::get('public.LiveCasino')}}</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="clr"></div>
                </div>
                
                <div class="mobR4">
                  <div class="mobRtd ct">
                        <table border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td align="center" valign="middle"><img src="{{url()}}/c59/img/cs.png" width="110" height="110" alt=""/></td>
                            </tr>
                        </table>
                    </div>
                    <div class="clr"></div>
                </div>
                
                <div class="mobR5">
                    <div class="mobRtd ct">
                        <table border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td align="center" valign="middle"><img src="{{url()}}/c59/img/allbet_qr.jpg" width="110" height="110" alt=""/></td>
                            </tr>
                            <tr>
                                <td>
                                    <table border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td align="center" valign="middle"><img src="{{url()}}/c59/img/apple-icon.png" width="26" height="26" alt=""/><img src="{{url()}}/c59/img/AND-ICON.png" width="26" height="26" alt=""/></td>
                                            <td align="center" valign="middle">{{Lang::get('public.LiveCasino')}}</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="clr"></div>
                </div>
            </div>
<!--Mobile-->
@stop
