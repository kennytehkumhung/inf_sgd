<script>
 $(document).ready(function() { 
getBalance(true);
});
</script>
<!--MID SECTION-->
<div class="midSect bglc">
    <div class="midSectInner">
        <div class="midSectCont">
            <br>
            <div class="walletTop">
                <?php $count = 1 ; ?>
                <a onclick="getBalance()" href="#" style="text-decoration: none;">
                    <div class="wallet<?php echo $count++; ?>">
                        <div class="main-set-title">Main Wallet</div><div class="main-setPrice">{{App\Http\Controllers\User\WalletController::mainwallet()}}</div></a>
                    </div>
                </a>
                @foreach( Session::get('products_obj') as $prdid => $object)
                    <div class="wallet<?php echo $count++; ?>">
                        <div class="main-set-title">{{$object->name}}</div><div class="main-setPrice {{$object->code}}_balance">0.00</div>
                    </div>
                @endforeach
                <div class="wallet<?php echo $count++; ?>">
                    <div class="main-set-title">ทั้งหมด</div><div class="main-setPrice" id="top_total_balance">0.00</div>
                </div>
            </div>

        <!--ACCOUNT MANAGAMENT MENU-->
              <div class="inlineAccMenu">
                <ul>
                    @if( $title != 'Account Password' && $title != 'Account Profile' && $title != 'Bank Setting')
                    <li @if($title == Lang::get('public.Deposit') )class="selected" @endif ><a href="{{route('deposit')}}">{{Lang::get('public.Deposit')}}</a></li>
                    <li @if($title == Lang::get('public.Withdrawal') )class="selected" @endif><a href="{{route('withdraw')}}">{{Lang::get('public.Withdrawal')}}</a></li>
                    <li @if($title == Lang::get('public.TransactionHistory') )class="selected" @endif><a href="{{route('transaction')}}">{{Lang::get('public.TransactionHistory')}}</a></li>
                    <li @if($title == Lang::get('public.WalletTransfer') )class="selected" @endif><a href="{{route('transfer')}}">{{Lang::get('public.Transfer')}}</a></li>
                    @else
                    <li @if($title == 'Account Profile' )class="selected"@endif ><a href="{{route('update-profile')}}">{{Lang::get('public.AccountProfile')}}</a></li>	 
                    <li @if($title == 'Account Password' )class="selected"@endif ><a href="{{route('update-password')}}">{{Lang::get('public.AccountPassword')}}</a></li>
                    <li @if($title == 'Bank Setting' )class="selected"@endif ><a href="{{route('memberbank')}}">{{Lang::get('COMMON.CTABBANKSETTING')}}</a></li>
                    @endif
                </ul>
              </div>