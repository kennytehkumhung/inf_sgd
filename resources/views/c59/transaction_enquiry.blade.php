@extends('c59/master')

@section('title', 'Transaction Enquiry')

@section('top_js')
  <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
@stop

@section('content')

@include('c59/transaction_mange_top', [ 'title' => Lang::get('public.TransactionHistory') ] )
    <div class="title_bar">
    <span>{{Lang::get('public.TransactionHistory')}}</span>
    </div>
    <!--ACCOUNT TITLE-->
    <!--ACCOUNT CONTENT-->
    <div class="acctContent">
        <span class="wallet_title"><i class="fa fa-file-o"></i>{{Lang::get('public.TransactionHistory')}}</span>
        <div class="acctRow">
            <label>{{Lang::get('public.DateFrom')}} :</label><div class="cstInput1"><input type="text" class="datepicker" id="date_from" style="cursor:pointer;" value="{{date('d-m-Y')}}">-<input type="text" class="datepicker" id="date_to" style="cursor:pointer;" value="{{date('d-m-Y')}}"></div>
            <div class="clr"></div>
        </div>
        <div class="acctRow">
            <label>{{Lang::get('public.RecordType')}}:</label>
            <select id="record_type">
                <option value="0" selected>{{Lang::get('public.CreditAndDebitRecords')}}</option>
                <option value="1">{{Lang::get('public.CreditRecords')}}</option>
                <option value="2">{{Lang::get('public.DebitRecords')}}</option>
            </select>
            <div class="clr"></div>
        </div>
        <div class="acctRowR">
            <div class="submitAcct">
                <a id="trans_history_button" href="#" onClick="transaction_history()"> {{Lang::get('public.Submit')}}</a>
                <br><br>
            </div>
        </div>
        <div>
            <table border='1' width='98%' id="trans_history" style="color:white;">
              <tr>
                <th>{{Lang::get('public.ReferenceNo')}}</th>
                <th>{{Lang::get('public.DateOrTime')}}</th>
                <th>{{Lang::get('public.type')}}</th>
                <th>{{Lang::get('public.Amount')}} </th>
                <th>{{Lang::get('public.Status')}}</th>
                <th>{{Lang::get('public.reason')}}</th>
            </tr>
            <tr>
                <td colspan="6"><h2><center>{{Lang::get('public.NoRecord')}}</center></h2></td>
            </tr>
        </table>          
        </div>

    </div>

@stop

@section('bottom_js')
<script>
transaction_history();

$(function() {
	$( ".datepicker" ).datepicker({ dateFormat: 'yy-mm-dd'  , defaultDate: new Date() });
});
  
function transaction_history(){
    var str = $('#date_from').val();
    var res = str.split("-");
    var datefrom = res[2]+'-'+res[1]+'-'+res[0];
    
    var strto = $('#date_to').val();
    var resto = strto.split("-");
    var dateto = resto[2]+'-'+resto[1]+'-'+resto[0];
    
$.ajax({
	type: "POST",
	url: "{{action('User\TransactionController@transactionProcess')}}",
	data: {
		_token:   "{{ csrf_token() }}",
		date_from:                  datefrom,
		date_to:                    dateto,
		record_type:                $('#record_type').val()

	},
}).done(function( json ) {

			//var str;
			 var str = '';
			str += '	<tr><th>{{Lang::get("public.ReferenceNo")}}</th><th>{{Lang::get("public.DateOrTime")}}</th><th>{{Lang::get("public.type")}}</th><th>{{Lang::get("public.Amount")}}</th><th>{{Lang::get("public.Status")}}</th><th>{{Lang::get("public.reason")}}</th></tr>';
			
			 obj = JSON.parse(json);
			//alert(obj);
			 $.each(obj, function(i, item) {
                             var datetime = item.created;
                             var sdatetime = datetime.split(" ");
                             var date = sdatetime[0].split("-");
                             var rdatetime = date[2]+'-'+date[1]+'-'+date[0]+' '+sdatetime[1];
                             
				str +=  '<tr><td align="center">'+item.id+'</td><td align="center">'+rdatetime+'</td><td align="center">'+item.type+'</td><td align="center">'+item.amount+'</td><td align="center">'+item.status+'</td><td align="center">'+item.rejreason+'</td></tr>';
				
			})
				
			//alert(json);
			$('#trans_history').html(str);
			
	});
}

setInterval(updateTrans, 10000);
setInterval(update_mainwallet, 10000);

function updateTrans(){
	$( "#trans_history_button" ).trigger( "click" );
}

</script>
@stop