@extends('c59/master')

@section('title', 'Register | Club5599')

@section('top_js')
<script>
  function register_submit(){
        var dob = $('#dob_year').val()+'-'+$('#dob_month').val()+'-'+$('#dob_day').val();
	$.ajax({
		type: "POST",
		url: "{{route('register_process')}}",
		data: {
			_token: "{{ csrf_token() }}",
			username:		 $('#r_username').val(),
			password: 		 $('#r_password').val(),
			repeatpassword:          $('#r_repeatpassword').val(),
			code:    		 $('#r_code').val(),
			email:    		 $('#r_email').val(),
			mobile:    		 $('#r_mobile').val(),
			fullname:                $('#r_fullname').val(),
			dob:			 dob,
		},
	}).done(function( json ) {
			 $('.acctTextReg').html('');
			 obj = JSON.parse(json);
			 var str = '';
			 $.each(obj, function(i, item) {
				if( '{{Lang::get('COMMON.SUCESSFUL')}}' == item ){
					alert('{{Lang::get('public.NewMember')}}');
					window.location.href = "{{route('homepage')}}";
				}else{
					$('.'+i+'_acctTextReg').html(item);
					//str += item + '\n';
				}
			})
			
			//alert(str);
			
			
	});
}
</script>
@stop

@section('content')
<div class="midSect bglc">
    <div class="midSectInner">
        <div class="midSectCont">
            <div class="otherPg">
                <h2><i class="fa fa-user-plus"></i>{{Lang::get('public.Registration')}}</h2>
                @if(Lang::getLocale() == 'en')
                    <p>Create a new account. It's simple and free</p>
                @else
                    <p>สมัครสมาชิกใหม่ได้ง่ายๆ</p>
                @endif
                <div class="acctContentReg">
                    <div class="acctRow">
                        <label>LINE ID / {{Lang::get('public.EmailAddress')}} * :</label><input type="text" id="r_email"><span class="email_acctTextReg acctTextReg" style="color: red;"></span>
                        <div class="clr"></div>
                    </div>                
                    <div class="acctRow">
                        <label>{{Lang::get('public.ContactNo')}} * :</label><input type="text" id="r_mobile"><span class="mobile_acctTextReg acctTextReg" style="color: red;"></span>
                        <div class="clr"></div>
                    </div>
                    @if(Lang::getLocale() == 'en')
                        <p>*Name must be matched with withdrawal bank account </p>
                    @else
                        <p>*ชื่อ-นามสกุล จะต้องตรงกับบัญชีแจ้งถอนเงิน</p>
                    @endif
                    
                    <div class="acctRow">
                        <label>{{Lang::get('public.FullName')}} * :</label><input type="text" id="r_fullname"><span class="fullname_acctTextReg acctTextReg" style="color: red;"></span>
                        <div class="clr"></div>
                    </div>
                    
                    <div class="acctRow">
                        <label>{{Lang::get('public.DOB')}} :</label>
                            <select style="margin-right: 8px;" id="dob_day">
                                <option value="01">01</option>
                                <option value="02">02</option>
                                <option value="03">03</option>
                                <option value="04">04</option>
                                <option value="05">05</option>
                                <option value="06">06</option>
                                <option value="07">07</option>
                                <option value="08">08</option>
                                <option value="09">09</option>
                                <option value="10">10</option>
                                <option value="11">11</option>
                                <option value="12">12</option>
                                <option value="13">13</option>
                                <option value="14">14</option>
                                <option value="15">15</option>
                                <option value="16">16</option>
                                <option value="17">17</option>
                                <option value="18">18</option>
                                <option value="19">19</option>
                                <option value="20">20</option>
                                <option value="21">21</option>
                                <option value="22">22</option>
                                <option value="23">23</option>
                                <option value="24">24</option>
                                <option value="25">25</option>
                                <option value="26">26</option>
                                <option value="27">27</option>
                                <option value="28">28</option>
                                <option value="29">29</option>
                                <option value="30">30</option>
                                <option value="31">31</option>
                            </select>
                            <select style="margin-right: 8px;" id="dob_month">
                                <option value="01">01</option>
                                <option value="02">02</option>
                                <option value="03">03</option>
                                <option value="04">04</option>
                                <option value="05">05</option>
                                <option value="06">06</option>
                                <option value="07">07</option>
                                <option value="08">08</option>
                                <option value="09">09</option>
                                <option value="10">10</option>
                                <option value="11">11</option>
                                <option value="12">12</option>
                            </select>
                            <select id="dob_year">
                                <option value="2006">2006</option>
                                <option value="2005">2005</option>
                                <option value="2004">2004</option>
                                <option value="2003">2003</option>
                                <option value="2002">2002</option>
                                <option value="2001">2001</option>
                                <option value="2000">2000</option>
                                <option value="1999">1999</option>
                                <option value="1998">1998</option>
                                <option value="1997">1997</option>
                                <option value="1996">1996</option>
                                <option value="1995">1995</option>
                                <option value="1994">1994</option>
                                <option value="1993">1993</option>
                                <option value="1992">1992</option>
                                <option value="1991">1991</option>
                                <option value="1990">1990</option>
                                <option value="1989">1989</option>
                                <option value="1988">1988</option>
                                <option value="1987">1987</option>
                                <option value="1986">1986</option>
                                <option value="1985">1985</option>
                                <option value="1984">1984</option>
                                <option value="1983">1983</option>
                                <option value="1982">1982</option>
                                <option value="1981">1981</option>
                                <option value="1980">1980</option>
                                <option value="1979">1979</option>
                                <option value="1978">1978</option>
                                <option value="1977">1977</option>
                                <option value="1976">1976</option>
                                <option value="1975">1975</option>
                                <option value="1974">1974</option>
                                <option value="1973">1973</option>
                                <option value="1972">1972</option>
                                <option value="1971">1971</option>
                                <option value="1970">1970</option>
                                <option value="1969">1969</option>
                                <option value="1968">1968</option>
                                <option value="1967">1967</option>
                                <option value="1966">1966</option>
                                <option value="1965">1965</option>
                                <option value="1964">1964</option>
                                <option value="1963">1963</option>
                                <option value="1962">1962</option>
                                <option value="1961">1961</option>
                                <option value="1960">1960</option>
                            </select> @if(Lang::getLocale() == 'th') &nbsp;&nbsp;เพื่อรับสิทธิประโยชน์ ประจำเดือนเกิด @endif
                        <div class="clr"></div>
                    </div>
                    @if(Lang::getLocale() == 'en')
                        <p>*Username Policy: length at least 6 characters and maximum of 12 </p>
                    @else
                        <p>*โปรดระบุชื่อผู้ใช้งาน 6-12 ตัวอักษร</p>
                    @endif                       
                    
                    <div class="acctRow">
                        <label>{{Lang::get('public.Username')}} * :</label><input type="text" id="r_username"><span class="username_acctTextReg acctTextReg" style="color: red;"></span>
                        <div class="clr"></div>
                    </div>
                     @if(Lang::getLocale() == 'en')
                        <p>*Password Policy: length at least 6 characters and maximum of 20 </p>
                    @else
                        <p>*โปรดระบุรหัสผ่าน 6-20 ตัวอักษร</p>
                    @endif                                     
                    
                    
                    <div class="acctRow">
                        <label>{{Lang::get('public.Password')}} * :</label><input type="password" id="r_password"><span class="password_acctTextReg acctTextReg" style="color: red;"></span>
                        <div class="clr"></div>
                    </div>
                  
                    <div class="acctRow">
                        <label>{{Lang::get('public.ReenterPassword')}} * :</label><input type="password" id="r_repeatpassword">
                        <div class="clr"></div>
                    </div>
                  
                    <div class="submitAcct">
                        <a href="#" onClick="register_submit()" > {{Lang::get('public.Submit')}}</a>
                    </div>
                </div>
            </div>
@stop

@section('bottom_js')
@stop