@extends('c59/master')

@section('title', Lang::get('public.Banking'))

@section('content')
<div class="midSect bglc">
    <div class="midSectInner">
        <div class="midSectCont">
            <div class="bkTit1">{{Lang::get('public.Banking')}}</div>
            <table width="100%" border="0" cellpadding="0" cellspacing="0" class="bankingtable">
                <tr style="background-color: #32CBEB; font-weight: bold;">
                    <td width="15%" rowspan="2"><div align="center">{{Lang::get('public.BankName')}}</div></td>
                    <td colspan="2" align="center"><strong>{{Lang::get('public.TransactionLimit')}} (THB)</strong></td>
                    <td width="15%" rowspan="2"><div align="center"><strong>{{Lang::get('public.ProcessingTime')}}</strong></div></td>
                </tr>
                <tr style="background-color:#32CBEB;font-weight:bold;">
                    <td width="8%"><div align="center"><strong>{{Lang::get('public.min')}}</strong></div></td>
                    <td width="8%"><div align="center"><strong>{{Lang::get('public.max')}}</strong></div></td>
                </tr>
                <tr>
                    <td height="20" colspan="6" bgcolor="#f7cc2b" align="center"><span style="font-weight:bold;color:#000">{{Lang::get('public.Deposit')}}</span></td>
                </tr>
                <tr>
                    <td><div align="center">ฝากผ่านตู้เอทีเอ็ม</div></td>
                    <td rowspan="3"><div align="center">1000.00</div></td>
                    <td rowspan="3"><div align="center">ไม่จำกัด</div></td>
                    <td rowspan="3"><div align="center">15 นาที</div></td>
                </tr>
                <tr>
                    <td><div align="center">ฝากผ่านอินเตอร์เน็ต</div></td>
                </tr>
                <tr>
                    <td><div align="center">โอนเงินผ่านธนาคาร</div></td>
                </tr>
                <tr>
                    <td height="20" colspan="6" bgcolor="#F7CC2B" align="center"><span style="font-weight:bold;color:#000">{{Lang::get('public.Withdrawal')}}</span></td>
                </tr>
                <tr>
                    <td><div align="center">โอนเงินผ่านธนาคาร</div></td>
                    <td rowspan="3" align="center"><div align="center">1,000</div></td>
                    <td rowspan="3" align="center"><div align="center">200,000</div></td>
                    <td rowspan="3" align="center"><div align="center">15-30 นาที</div></td>
                </tr>
            </table>

            <p style="color: #000;">
                <strong>{{Lang::get('public.FriendlyReminder')}}:</strong>
                <br />
                <ul>
                    <li>{{Lang::get('public.Bankingnote1')}}</li>
                    <li>{{Lang::get('public.Bankingnote2')}}</li>
                    <li>{{Lang::get('public.Bankingnote3')}}</li>
                    <li>{{Lang::get('public.Bankingnote4')}}</li>
                    <li>{{Lang::get('public.Bankingnote5')}}</li>
                    <li>{{Lang::get('public.Bankingnote6')}}</li>
                    <li>{{Lang::get('public.Bankingnote7')}}</li>
                    <li>{{Lang::get('public.Bankingnote8')}}</li>
                    <li>{{Lang::get('public.Bankingnote9')}}</li>
                </ul>
            </p>
@stop

@section('bottom_js')

@stop