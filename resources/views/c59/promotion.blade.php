@extends('c59/master')

@section('title', 'Promotion | Club5599')

@section('content')
<div class="midSect bglc">
    <div class="midSectInner">
        <div class="midSectCont">
            <div class="promoTit">{{Lang::get('public.Promotion')}}</div>

            <div class="promo">
                <div id="accordion">
                    @foreach ($promo as $key => $value )
                        <h4 class="accordion-toggle">
                            <img src="{{$value['image']}}">
                            <div id="promo_bar"> 
                                <div class="promo_info"><a href="{{route('register')}}">{{Lang::get('public.JoinNow')}}</a></div>
                                <label class="promo_more" for="ac-1">{{Lang::get('public.MoreInfo')}}</label>
                                <div class="clr"></div>
                            </div>
                        </h4>

                        <div class="accordion-content default">
                            <?php echo htmlspecialchars_decode($value['content']); ?>
                        </div>
                    @endforeach
                </div>  
            </div>
@stop

@section('bottom_js')
<script>
    $(document).ready(function($) {
            $('#accordion').find('.accordion-toggle').click(function(){

                    //Expand or collapse this panel
                    $(this).next().slideToggle('fast');

                    //Hide the other panels
                    $(".accordion-content").not($(this).next()).slideUp('fast');

            });
    });
</script>
@stop