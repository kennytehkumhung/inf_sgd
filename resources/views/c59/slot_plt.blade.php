<html>
<head>
<link href="{{url()}}/c59/resources/css/slot.css" rel="stylesheet">
</head>
<body>
    <div class="slotTab">
        <div class="tab-container">
            <ul class="etabs">
                <li class="tab"><a href="{{route('plt', [ 'type' => 'pgames' , 'category' => '1' ] )}}">Progressive Games</a></li>
                <li class="tab"><a href="{{route('plt', [ 'type' => 'newgames' , 'category' => '1' ] )}}">New Games</a></li>
                <li class="tab"><a href="{{route('plt', [ 'type' => 'brand' , 'category' => '1' ] )}}">Branded Games</a></li>
                <li class="tab"><a href="{{route('plt', [ 'type' => 'slot' , 'category' => 'slot' ] )}}">Slot</a></li>
                <li class="tab"><a href="{{route('plt', [ 'type' => 'slot' , 'category' => 'videopoker' ] )}}">Video Poker</a></li>
                <li class="tab"><a href="{{route('plt', [ 'type' => 'slot' , 'category' => 'arcade' ] )}}">Arcade</a></li>
                <li class="tab"><a href="{{route('plt', [ 'type' => 'slot' , 'category' => 'tablecards' ] )}}">Tablecards</a></li>
                <li class="tab"><a href="{{route('plt', [ 'type' => 'slot' , 'category' => 'scratchcards' ] )}}">Scratchcards</a></li>
            </ul>
        </div>    
    </div>
    <div id="slot_lobby">			
            @foreach( $lists as $list )
            <div class="slot_box">
                <span>{{$list->gameName}}</span>
                <a href="javascript:void(0)" onclick="@if (Auth::user()->check())window.open('{{route('pltslotiframe' , [ 'gamecode' => $list['code'] ] )}}', 'plt_slot', 'width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif ">
                        <img src="{{url()}}/front/img/plt/{{$list->code}}.jpg" width="150" height="150" alt=""/>
                </a>
            </div>				 
            @endforeach
            <div class="clr"></div>
    </div>
    <div class="clr"></div> 
<!--Slot-->
</body>
</html>