@extends('c59/master')

@section('title', 'Contact Us | Club5599')

@section('content')
<!--MID SECTION-->
<div class="midSect bglc">
    <div class="midSectInner">
        <div class="midSectCont">
            <div class="ctTit1">{{Lang::get('public.ContactUs')}}</div>

            <div class="ctBg">
                <div class="clr"></div>
            </div>
<!--MID SECTION-->
@stop
