@extends('c59/master')

@section('title', 'Withdraw')

@section('top_js')
<script>
	setInterval(update_mainwallet, 10000);
	function submit_transfer(){
	$.ajax({
			 type: "POST",
			 url: "{{route('withdraw-process')}}?"+$('#withdraw_form').serialize(),
			 data: {
				_token: 		 "{{ csrf_token() }}",
			 },
			 beforeSend: function(){
				
			 },
			 success: function(json){
					obj = JSON.parse(json);
					 var str = '';
					 $.each(obj, function(i, item) {
						
						if( '{{Lang::get('COMMON.SUCESSFUL')}}' == item ){
							alert('{{Lang::get('COMMON.SUCESSFUL')}}');
							window.location.href = "{{route('transaction')}}";
						}else{
							str += item + '<br>';
						}
					})
					$('.failed_message').html(str);
				
			}
		})
	}
        
        function load_bank(id,type){

		if( type == 'default' ){
			var id = id;
		}else{
			var id = id.value;
		}

		$.ajax({
			type: "POST",
			url: "{{route('loadbank')}}",
			data: {
                            id:  id
			},
		}).done(function( result ) {
			  $('#accountno').val(result);
			  $('#accountnopass').val(result);
		});
	}
	@foreach( $banklists as $bank => $detail  )
	load_bank('{{$detail['code']}}','default');
        <?php break; ?>
	@endforeach
</script>
@stop

@section('content')

@include('c59/transaction_mange_top', [ 'title' => Lang::get('public.Withdrawal') ] )
    <div class="title_bar">
        <span>{{Lang::get('public.Withdrawal')}}</span>
    </div>
    <!--ACCOUNT TITLE-->
   <!--ACCOUNT CONTENT-->
    <div class="acctContent">
        <span class="wallet_title"><i class="fa fa-money"></i>{{Lang::get('public.Withdrawal')}}</span>
        <div class="acctRow">
            <label>{{Lang::get('public.Option')}} :</label><span class="acctText">{{Lang::get('public.BankTransfer')}}</span>
            <div class="clr"></div>
        </div>
        <div class="acctRow">
            <label>{{Lang::get('public.MinMaxLimit')}} :</label><span class="acctText">1000 บาท/ 200,000 บาท</span>
            <div class="clr"></div>
        </div>
        <div class="acctRow">
            <label>{{Lang::get('public.ProcessingTime')}} :</label><span class="acctText">15-30 นาที</span>
            <div class="clr"></div>
        </div>
        <i class="fa fa-exclamation"></i><span style="font-size:13px;color:#FFD200;">{{Lang::get('public.BankTransferNote2')}}</span>
        <hr style="width: 98%; float: left;">
        <div class="clr"></div>
        <br>
        <form id="withdraw_form">
            <div class="acctRow">
                <label>{{Lang::get('public.Balance')}} (THB) :</label><span class="acctText">{{App\Http\Controllers\User\WalletController::mainwallet()}}</span>
                <div class="clr"></div>
            </div>
            <div class="acctRow">
                <label>{{Lang::get('public.Amount')}} (THB) * :</label><input type="text" name="amount"/>
                <div class="clr"></div>
            </div>
            <div class="acctRow">
                <label>{{Lang::get('public.BankName')}} * :</label>
                @if($accbank != '')
                    <span class="acctText">{{$accbank->bankname}}</span>
                    <input type="hidden" id="accountnopass" name="bank"  value= "{{$accbank->bankcode}}"/>
                @endif
                <div class="clr"></div>
            </div>
            <div class="acctRow">
                <label>{{Lang::get('public.FullName')}} * :</label><span class="acctText">{{Session::get('fullname')}}</span>
                <div class="clr"></div>
            </div>
            <div class="acctRow">
                <label>{{Lang::get('public.BankAccountNo')}} * :</label>
                <span class="acctText">{{$accbank->bankaccno}}</span> 
                <input type="hidden" id="accountnopass" name="accountno" value= "{{$accbank->bankaccno}}"/>
                <div class="clr"></div>
            </div>
            <span style="font-size:13px;color:#FFD200;"><i class="fa fa-exclamation-circle"></i>{{Lang::get('public.RequiredFields')}}</span>
            <br><br>
            <span class="failed_message acctTextReg" style="display:block;float:left;height:100%;"></span>
            <div class="submitAcct">
                <a href="javascript:void(0)" onClick="submit_transfer()">{{Lang::get('public.Submit')}}</a>
            </div>
        </form>
    </div>
@stop

@section('bottom_js')
<script src="{{url()}}/front/resources/js/slick.min.js"></script>
<script>
$('.caro').slick({
  infinite: true,
  slidesToShow: 2,
  slidesToScroll: 2,
  autoplay: true,
  variableWidth: true,
  arrows: false
});
</script>
@stop