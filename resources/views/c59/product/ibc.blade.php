@extends('../c59/master')

@section('title', Lang::get('public.SportsBook'))
@section('keywords', 'football betting,sports betting')
@section('description', 'Gambling and bet on football at trusted Thailand online sports betting site, Club5599. Win big today!')

@section('content')
<div class="midSect bglc">
    <div class="midSectInner">
        <div class="midSectCont">
            <div class="spTit">{{Lang::get('public.SportsBook')}}</div>
            @if (!Auth::user()->check())
                <iframe id="ContentPlaceHolder1_iframe_game" width="1024px" height="655px" frameborder="0" src="http://mkt.{{$website}}/vender.aspx?lang={{$lang}}"></iframe>
            @else
                <iframe id="ContentPlaceHolder1_iframe_game" width="1024px" height="655px" frameborder="0" src="http://mkt.{{$website}}/Deposit_ProcessLogin.aspx?lang={{$lang}}"></iframe>
            @endif

@stop

