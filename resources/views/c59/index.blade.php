@extends('c59/master')

@section('title', 'Thailand Online Casino, Gambling Games | Club5599')
@section('keywords', 'Thailand Online Casino, Gambling Games | Club5599')
@section('description', 'Enjoy gambling (kasino) games at Thailand most trusted online casino, Club5599. Play live casino, slots, horse racing and more.')


@section('top_js')
<script type="text/javascript">
    $(document).ready(function() {
        $('.bxslider').bxSlider({
            auto: true,
            autoControls: false
        });
    });
</script>

<script>
          $(document).ready(function () {
              fakeJackport();
              setInterval(updateJackport, 3000);
          });

          function fakeJackport() {

              var value = Math.floor(Math.random() * 100);
              value = 1356289 + value * 5.496;


              var value2 = value.toFixed(2);

              $('#jp_1').text(addCommas(value2));

          }

          function updateJackport() {
              var val = $("span[id^='jp_1']").text();
              val = removeComma(val);

              if (val > 1468889)
                  val = 1356289;

              var value = Math.floor((Math.random() * 1000)+100);
              value = value * 1;
              
              var value2 = (parseFloat(val) + value).toFixed(2);

              $('#jp_1').text(addCommas(value2));
          }

          function addCommas(str) {
              var parts = (str + "").split("."),
          main = parts[0],
          len = main.length,
          output = "",
          i = len - 1;

              while (i >= 0) {
                  output = main.charAt(i) + output;
                  if ((len - i) % 3 === 0 && i > 0) {
                      output = "," + output;
                  }
                  --i;
              }
              // put decimal part back
              if (parts.length > 1) {
                  output += "." + parts[1];
              }
              return output;
          }

          function removeComma(str) {
              var parts = (str + "").split(",");

              var output = "";

              for (var i = 0; i < parts.length; i++) {
                  output += parts[i];
              }
              return output;
          }
</script>
@stop

@section('content')
<!-- Start of LiveChat (www.livechatinc.com) code -->
<script type="text/javascript">
window.__lc = window.__lc || {};
window.__lc.license = 7376791;
(function() {
  var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
  lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
})();
</script>
<!-- End of LiveChat code -->

<!--Mid Sect-->
<div class="midSect">
    <div class="sliderCont">
        <div class="sliderContInner">
            <!--Slider-->
            <!--HOMEPAGE LOGIN BOX-->
            <div class="floatLogin">
            </div>
            <!--HOMEPAGE LOGIN BOX-->
            <!--HOMEPAGE SLIDE-->         
             <ul class="bxslider">
		@foreach( $banners as $key => $banner )
		<li><img src="{{$banner->domain}}/{{$banner->path}}"/></li>	
                @endforeach
            </ul> 
            <!--HOMEPAGE SLIDE-->
            <!--Slider-->
            <div class="clr"></div>
            
            <div class="hpgBarInner">
                <div class="bar1">
                  
                </div>
                
                <div class="bar2">
                    <div class="pgTxt">THB <span id="jp_1">88,888.888</span></div>
                </div>

                <div class="clr"></div>
            </div>
        </div>
    </div>


    <div class="midSectInner">
        <div class="hpgBarBtm">
            <div class="hpgBarBtmInner">
                <div class="mobDl">
                    <a href="{{route('livecasino')}}">PLAY<br>NOW</a>
                </div>
            </div>
        </div>
    <div class="midSectCont">
        <div class="hpgBtm">
            <table width="auto" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td>
                        <div class="lc1">
                            <div class="lcBox">
                                <div id="crossfade">
                                        @if(Lang::getLocale() == 'en')
                                            <img class="bottom" src="{{url()}}/c59/img/thumb-1-hover.png" />
                                            <a href="{{route('livecasino')}}"><img class="top" src="{{url()}}/c59/img/thumb-1.png" /></a>
                                        @else
                                            <img class="bottom" src="{{url()}}/c59/img/thumb-1-th.png" />
                                            <a href="{{route('livecasino')}}"><img class="top" src="{{url()}}/c59/img/thumb-1-th-hover.png" /></a>
                                        @endif
                                </div>
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="lc1">
                            <div class="lcBox">
                                <div id="crossfade">
                                        @if(Lang::getLocale() == 'en')
                                            <img class="bottom" src="{{url()}}/c59/img/thumb-2-hover.png" />
                                            <a href="{{route('ibc')}}"><img class="top" src="{{url()}}/c59/img/thumb-2.png" /></a>
                                        @else
                                            <img class="bottom" src="{{url()}}/c59/img/thumb-2-th.png" />
                                            <a href="{{route('ibc')}}"><img class="top" src="{{url()}}/c59/img/thumb-2-th-hover.png" /></a>
                                        @endif
                                </div>
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="lc1">
                            <div class="lcBox">
                                <div id="crossfade">
                                    @if(Lang::getLocale() == 'en')
                                        <img class="bottom" src="{{url()}}/c59/img/thumb-3-hover.png" />
                                        <a href="{{route('slot', [ 'type' => 'mxb'  ])}}"><img class="top" src="{{url()}}/c59/img/thumb-3.png" /></a>
                                    @else
                                        <img class="bottom" src="{{url()}}/c59/img/thumb-3-th.png" />
                                        <a href="{{route('slot', [ 'type' => 'mxb'  ])}}"><img class="top" src="{{url()}}/c59/img/thumb-3-th-hover.png" /></a>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="lc1">
                            <div class="lcBox">
                                <div id="crossfade">
                                    @if(Lang::getLocale() == 'en')
                                        <img class="bottom" src="{{url()}}/c59/img/thumb-4-hover.png" />
                                        <a href="{{route('lotto')}}"><img class="top" src="{{url()}}/c59/img/thumb-4.png" /></a>
                                    @else
                                        <img class="bottom" src="{{url()}}/c59/img/thumb-4-th.png" />
                                        <a href="{{route('lotto')}}"><img class="top" src="{{url()}}/c59/img/thumb-4-th-hover.png" /></a>
                                    @endif   
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
            <div class="clr"></div>
        </div>
@stop

@section('bottom_js')
<script>
	$(document).ready(function(){
		activeItem = $("#accordion li:first");
		$(activeItem).addClass('active');
		$("#accordion li").hover(function(){
		$(activeItem).animate({width: "110px"}, {duration:300, queue:false});
		$(this).animate({width: "260px"}, {duration:300, queue:false});
		activeItem = this;
		});
	});

</script>

@stop