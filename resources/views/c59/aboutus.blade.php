@extends('c59/master')

@section('title', 'About Us')

@section('content')
<div class="midSect bglc">
    <div class="midSectInner">
        <div class="midSectCont">
            <div class="ctTit1">{{Lang::get('public.AboutUs')}}</div>
            <div>
                <!--MID SECTION-->
                <?php echo htmlspecialchars_decode($content); ?>
                <!--MID SECTION-->
                <br><br><br>
                
                <div class="clr"></div>
            </div>
@stop

@section('bottom_js')

@stop