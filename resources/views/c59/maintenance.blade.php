@include('c59/include/head')

@section('title', 'Maintenance')

<body>
<div class="midSect bglc">
    <div class="midSectInner">
        <div class="midSectCont">
            <div class="outer ht1">
            <div class="msgBox1">
                <div class="excl">
                    <img src="{{url()}}/c59/img/under-maintenance.png" alt=""/>
                </div>
                <div class="clr"></div>
            </div>
            </div>
        </div>
    </div>
</div>
    
</body>
            
