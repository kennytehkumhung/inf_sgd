<!-- head section-->
@include('c59/include/head')
<!-- head section-->
<body>
    <!-- navigation header starts-->	
    @include('c59/include/navigation')	
    <!-- navigation header ends-->

    <!--MegaMenu Starts-->

    @include('c59/include/megamenu')
    <!--MegaMenu Ends-->

    <!--Annoucement-->
    <!--<div class="anmnt">
            <marquee>{{App\Http\Controllers\User\AnnouncementController::index()}}</marquee>
    </div>-->
    <!--Annoucement-->

    @yield('content')

    <!--Footer-->
    @include('c59/include/footer')
    <!--Footer-->
    
    @yield('bottom_js')
</body>

@if( Session::get('currency') == 'MYR')
<script>
function hideminimize1() {
document.getElementById('minimize1').setAttribute('class', 'b');
}
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function()
{ (i[r].q=i[r].q||[]).push(arguments)}

,i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-43720111-1', 'auto');
ga('send', 'pageview');
</script>
@elseif( Session::get('currency') == 'VND')
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function()
{ (i[r].q=i[r].q||[]).push(arguments)}

,i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-43720111-1', 'auto');
ga('send', 'pageview');

</script>
@endif
</html>