@extends('c59/master')

@section('title', 'Account Profile')

@section('top_js')
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
@stop

@section('content')

@include('c59/transaction_mange_top', [ 'title' => 'Account Profile' ] )

    <div class="title_bar">
        <span>{{Lang::get('public.Profile')}}</span>
    </div>
      <!--ACCOUNT TITLE-->
      <!--ACCOUNT CONTENT-->
    <div class="acctContent">
        <span class="wallet_title"><i class="fa fa-pencil"></i>{{Lang::get('public.AccountProfile')}}</span>
        <div class="acctRow">
            <label>{{Lang::get('public.Username')}} :</label><span class="acctText">{{$acdObj->fullname}}</span>
            <div class="clr"></div>
        </div>
        <div class="acctRow">
            <label>{{Lang::get('public.EmailAddress')}} :</label><span class="acctText">{{$acdObj->email}}</span>
            <div class="clr"></div>
        </div>
        <div class="acctRow">
            <label>{{Lang::get('public.Currency')}} :</label><span class="acctText">บาท/THB</span>
            <div class="clr"></div>
        </div>
        <div class="acctRow">
            <label>{{Lang::get('public.ContactNo')}} :</label><span class="acctText">{{$acdObj->telmobile}}</span>
            <div class="clr"></div>
        </div>
        <div class="acctRow">
            <label>{{Lang::get('public.Gender')}} :</label>
            <select id="gender">
		<option value="1">{{Lang::get('public.Male')}}</option> 
		<option value="2">{{Lang::get('public.Female')}}</option>
            </select>
            <div class="clr"></div>
        </div>
        <div class="acctRow">
            <label>{{Lang::get('public.DOB')}} :</label>
            {{$acdObj->dob}}
            <div class="clr"></div>
        </div>
        <div class="submitAcct">
        <a href="#">Submit</a>
        </div>
    </div>
@stop

@section('bottom_js')
<script>
$(function() {
    $( ".datepicker" ).datepicker({ dateFormat: 'yy-mm-dd' });
  });

 function update_profile(){
	$.ajax({
		type: "POST",
		url: "{{action('User\MemberController@UpdateDetail')}}",
		data: {
			_token: "{{ csrf_token() }}",
			dob:		$('#dob').val(),
			mobile:    	$('#mobile').val(),
			gender:    	$('#gender').val()

		},
	}).done(function( json ) {
		
			 obj = JSON.parse(json);
			 var str = '';
			 $.each(obj, function(i, item) {
				str += item + '\n';
			})
			
			alert(str);
			
			
	});
}
</script>
@stop