@extends('c59/master')

@section('title', 'Thailand Online Casino, Gambling Games | Club5599')
@section('keywords', 'Thailand Online Casino, Gambling Games | Club5599')
@section('description', 'Enjoy gambling (kasino) games at Thailand most trusted online casino, Club5599. Play live casino, slots, horse racing and more.')

@section('content')

<!--Slider-->
<!--Mid Sect-->
<div class="midSect bglc">
    <div class="midSectInner">
        <div class="midSectCont">
            <div class="lcTit">{{Lang::get('public.LiveCasino')}}</div>

            <div class="lcCont">
                <table width="auto" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                            <div class="lcas1">
                                <div class="lcBox2">
                                    <div id="crossfade">
                                        @if(in_array('PLTB',Session::get('valid_product')))
                                        <img class="bottom" src="{{url()}}/c59/img/pt-lc-thumb-hover.png" />
                                        <a onClick="@if (Auth::user()->check())window.open('{{route('pltbiframe')}}', 'casinopltb', 'width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif">
                                            <img class="top" src="{{url()}}/c59/img/pt-lc-thumb.png" />
                                        </a>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="lcas1 marL">
                                <div class="lcBox2">
                                    <div id="crossfade">
                                        @if(in_array('AGG',Session::get('valid_product')))
                                        <img class="bottom" src="{{url()}}/c59/img/ag-lc-thumb-hover.png" />
                                        <a onClick="@if (Auth::user()->check())window.open('{{route('agg')}}', 'casino11', 'width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif">
                                            <img class="top" src="{{url()}}/c59/img/ag-lc-thumb.png" />
                                        </a>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="lcas1">
                                <div class="lcBox2">
                                    <div id="crossfade">
                                        @if(in_array('W88',Session::get('valid_product')))
                                        <img class="bottom" src="{{url()}}/c59/img/gp-lc-thumb-hover.png" />
                                        <a onClick="@if (Auth::user()->check())window.open('{{route('w88', [ 'type' => 'casino' , 'category' => 'live' ] )}}', 'casino12', 'width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif">
                                            <img class="top" src="{{url()}}/c59/img/gp-lc-thumb.png" />
                                        </a>   
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="lcas1 marL">
                                <div class="lcBox2">
                                    <div id="crossfade">
                                        @if(in_array('MXB',Session::get('valid_product')))
                                        <img class="bottom" src="{{url()}}/c59/img/xpro-lc-thumb-hover.png" />
                                        <a onClick="@if (!Auth::user()->check())alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif" href="@if (Auth::user()->check()){{route('mxb',[ 'type' => 'casino' , 'category' => 'baccarat' ])}}@endif">
                                            <img class="top" src="{{url()}}/c59/img/xpro-lc-thumb.png" />
                                        </a>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="lcas1">
                                <div class="lcBox2">
                                    <div id="crossfade">
                                        @if(in_array('ALB',Session::get('valid_product')))
                                        <img class="bottom" src="{{url()}}/c59/img/all-lc-thumb-hover.png" />
                                        <a onClick="@if (Auth::user()->check())window.open('{{route('alb', [ 'type' => 'casino' ] )}}', 'casino12', 'width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif" >
                                            <img class="top" src="{{url()}}/c59/img/all-lc-thumb.png" />
                                        </a>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="lcas1 marL">
                                <div class="lcBox2">
                                    <div id="crossfade">
                                        @if(in_array('CT8',Session::get('valid_product')))
                                        <img class="bottom" src="{{url()}}/c59/img/crw-lc-thumb-hover.png" />
                                        <a onClick="@if (Auth::user()->check())window.open('{{route('ct8')}}', 'casino13', 'width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif">
                                            <img class="top" src="{{url()}}/c59/img/crw-lc-thumb.png" />
                                        </a>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>

<!--Slider-->

@stop
