<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="">
		<meta name="author" content="">

		<title>{{Config::get('setting.website_name')}} - Agent</title>
		
		<!-- jQuery -->
		<script type="text/javascript" src="affiliate/jqwidgets/jquery-1.11.1.min.js"></script>

		<!-- Bootstrap Core JavaScript -->
		<script src="{{url()}}/affiliate/css/sb_bootstrap/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

		<!-- Metis Menu Plugin JavaScript -->
		<script src="{{url()}}/affiliate/css/sb_bootstrap/bower_components/metisMenu/dist/metisMenu.min.js"></script>

		<!-- Custom Theme JavaScript -->
		<script src="{{url()}}/affiliate/css/sb_bootstrap/dist/js/sb-admin-2.js"></script>


		<!-- Bootstrap Core CSS -->
		<link href="{{url()}}/affiliate/css/sb_bootstrap/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

		<!-- MetisMenu CSS -->
		<link href="{{url()}}/affiliate/css/sb_bootstrap/bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

		<!-- Timeline CSS -->
		<link href="{{url()}}/affiliate/css/sb_bootstrap/dist/css/timeline.css" rel="stylesheet">

		<!-- Custom CSS -->
		<link href="{{url()}}/affiliate/css/sb_bootstrap/dist/css/sb-admin-2.css" rel="stylesheet">

		<!-- Custom Fonts -->
		<link href="{{url()}}/affiliate/css/sb_bootstrap/bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
		
		
		@yield('top_js')
	</head>
	
<body>