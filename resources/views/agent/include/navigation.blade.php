<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom:0">
	<div class="navbar-header">
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</button>
		{{Lang::get('COMMON.AGENT')}}
	</div>
	<ul class="nav navbar-top-links navbar-right">
	<input id="text" type="hidden" value="http://{{Session::get('affiliate_username')}}.{{Config::get('setting.frontend')}}"/>
	<div id="qrcode" style="display:none;width:100px; height:100px; margin-top:15px;"></div>
		<li>
			<a href="javascript:void(0);" id="myImgBTN" onclick="qrPOPUP()">{{Lang::get('COMMON.QRCODE')}}</a>
		</li>
		<li>
		@if(  Config::get('setting.opcode') == 'GSC')
			<a href="javascript:void(0);" onclick='copyText()'>{{Lang::get('COMMON.AGENTLINK')}}</a>
		@else
			<a href="http://{{Session::get('affiliate_username')}}.{{Config::get('setting.frontend')}}" target='blank'>{{Lang::get('COMMON.AGENTLINK')}}</a>
		@endif
		</li>	
		<li>
			<a href="{{ route( 'languages', [ 'lang'=> 'en']) }}"><img src="{{url()}}/front/img/en.png"></a>
		</li>
		<li>
			<a href="{{ route( 'languages', [ 'lang'=> 'cn']) }}"><img src="{{url()}}/front/img/cn.png"></a>
		</li>
		<li class="dropdown">
			<a class="dropdown-toggle" data-toggle="dropdown" href="#">
				<i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
			</a>
			<ul class="dropdown-menu dropdown-user">
				<li><a href="#" onClick="addTab('{{Lang::get('COMMON.CTABCHANGEPASSWORD')}}','password')"><i class="fa fa-user fa-fw"></i> {{Lang::get('COMMON.CTABCHANGEPASSWORD')}}</a></li>
				<li class="divider"></li>
				<li><a href="{{route('affiliate_logout')}}"><i class="fa fa-sign-out fa-fw"></i> {{Lang::get('COMMON.LOGOUT')}}</a></li>
			</ul>
		</li>
	</ul>
	<div class="navbar-default sidebar" role="navigation">
		<div class="sidebar-nav navbar-collapse">
			<ul class="nav" id="side-menu">	
				<li>
					<a href="#"> {{Lang::get('COMMON.MEMBER')}}<span class="fa arrow"></span></a>
					<ul class="nav nav-second-level">
						<li>
							<a href="#" onClick="addTab('{{Lang::get('COMMON.MEMBERENQUIRY')}}','member?aflids={{Session::get('affiliate_userid')}}')"> {{Lang::get('COMMON.MEMBERENQUIRY')}}</a>
						</li>
					</ul>
				</li>

				@if(Config::get('setting.opcode') == 'GSC')
				<li>
					<a href="#"> {{Lang::get('COMMON.REPORT')}}<span class="fa arrow"></span></a>
					<ul class="nav nav-second-level">
						<li>
							<a href="#" onClick="addTab('{{Lang::get('COMMON.REPORTTOTAL')}}','profitloss?aflid={{Session::get('affiliate_userid')}}')">{{Lang::get('COMMON.REPORTTOTAL')}}</a>
						</li>
						<li>
							<a href="#" onClick="addTab('{{Lang::get('COMMON.REPORTTOTALNEW')}}','profitloss2?aflid={{Session::get('affiliate_userid')}}')"> {{Lang::get('COMMON.REPORTTOTALNEW')}}</a>
						</li>
						@if(Session::get('affiliate_parentid') == 0)
							<li>
								<a href="#" onClick="addTab('{{Lang::get('COMMON.INCENTIVEREPORT')}}','rebateReport?aflid={{Session::get('affiliate_userid')}}')"> {{Lang::get('COMMON.INCENTIVEREPORT')}}</a>
							</li>	
						@endif
						<li>
							<a href="#" onClick="addTab('{{Lang::get('COMMON.MEMBERREPORT')}}','memberReport?aflid={{Session::get('affiliate_userid')}}')"> {{Lang::get('COMMON.MEMBERREPORT')}}</a>
						</li>
						<li>
							<a href="#" onClick="addTab('{{Lang::get('COMMON.MEMBERWALLETREPORT')}}','showMemberWallet?aflid={{Session::get('affiliate_userid')}}')"> {{Lang::get('COMMON.MEMBERWALLETREPORT')}}</a>
						</li>
					</ul>
				</li>
				@endif
				
				<!--
				<li>
					<a href="#"> {{Lang::get('COMMON.REPORT')}}<span class="fa arrow"></span></a>
					<ul class="nav nav-second-level">
						<li>
							<a href="#" onClick="addTab('{{Lang::get('COMMON.PROFITLOSS')}}','profitloss')">{{Lang::get('COMMON.PROFITLOSS')}}</a>
						</li>
						<li>
							<a href="#" onClick="addTab('{{Lang::get('COMMON.AFFILIATEREPORT')}}','agentReport')"> {{Lang::get('COMMON.AGENTREPORT')}}</a>
						</li>
					</ul>
				</li>
				-->
				
				<li>
					<a href="#"> {{Lang::get('COMMON.AGENT')}}<span class="fa arrow"></span></a>
					<ul class="nav nav-second-level">
						<li>
							<a href="#" onClick="addTab('{{Lang::get('COMMON.AFFILIATELIST')}}','agent')"> {{Lang::get('COMMON.AFFILIATELIST')}}</a>
						</li>
						
						<li>
							<a href="#" onClick="addTab('{{Lang::get('COMMON.AGENTREPORT')}}','agentReport')"> {{Lang::get('COMMON.AGENTREPORT')}}</a>
						</li>
					
					</ul>
				</li>
				{{--
				@if(Config::get('setting.opcode') == 'GSC' && Session::get('affiliate_parentid') == 0)
				<li>
					<a href="#"> {{Lang::get('COMMON.TOOL')}}<span class="fa arrow"></span></a>
					<ul class="nav nav-second-level">
						<li>
							<a href="#" onClick="addTab('{{Lang::get('COMMON.IPLOOKUP')}}','iptracker?aflid={{Session::get('affiliate_userid')}}')"> {{Lang::get('COMMON.IPLOOKUP')}}</a>
						</li>
					</ul>
				</li>	
				@endif
				--}}
				<li>
					<a href="#"> {{Lang::get('COMMON.PROFILE')}}<span class="fa arrow"></span></a>
					<ul class="nav nav-second-level">
						<li>
							<a href="#" onClick="addTab('{{Lang::get('COMMON.CTABCHANGEPASSWORD')}}','password')"> {{Lang::get('COMMON.CTABCHANGEPASSWORD')}}</a>
						</li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
</nav>
<script type="text/javascript" src="{{url()}}/affiliate/js/qrcode.js"></script>
<script>
function copyText(){
	var x=prompt("{{Lang::get('COMMON.AGENTLINK')}}", "http://{{Session::get('affiliate_username')}}.{{Config::get('setting.frontend')}}");
}

function qrPOPUP(){
	var myImgSrc = document.getElementById("myImg").getAttribute('src');
	var myWindow=window.open("", "", "width=400,height=350");
	myWindow.document.title = 'testing';
	myWindow.document.write("<script type=\"text/javascript\">function prepHref(linkElement) {var myDiv = document.getElementById('Div_contain_image');var myImage = myDiv.children[0];linkElement.href = myImage.src;}<\/script>'");
	myWindow.document.write("<title>QRcode</title><div id=\"Div_contain_image\"><img src="+myImgSrc+" style='margin-left: 60px;'></div><h3 align='center'>({{Lang::get('COMMON.NOTRECOMMENDUSEWECHATTOSCAN')}})</h3><a href=\"#\" onclick=\"prepHref(this)\" download><h3 style='margin-left: 130px;'>下载二维码</h3></a>");
}
var qrcode = new QRCode(document.getElementById("qrcode"), {
	width : 256,
	height : 256
});

function makeCode () {		
	var elText = document.getElementById("text");
	
	if (!elText.value) {
		alert("Input a text");
		elText.focus();
		return;
	}
	
	qrcode.makeCode(elText.value);
}

makeCode();

$("#text").
	on("blur", function () {
		makeCode();
	}).
	on("keydown", function (e) {
		if (e.keyCode == 13) {
			makeCode();
		}
	});
</script>