﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<title>Affiliate - Home - Infiniwin</title>
	<link href="{{url()}}/affiliate/css/login.css" type="text/css" media="all" rel="stylesheet" />
	<!--<script type="text/javascript" language="javascript" src="{{url()}}/front/resources/js/jquery.bxslider.min.js"></script>-->
	<script type="text/javascript" src="http://gc.kis.scr.kaspersky-labs.com/1B74BD89-2A22-4B93-B451-1C9E1052A0EC/main.js" charset="UTF-8"></script><script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
</head>
	<body>
	<div id="main">
	<style>
	#login_btn
	{
		background:url({{url()}}/affiliate/images/en/login_btn.jpg) no-repeat;
	}
	#reg_btn
	{
		background:url({{url()}}/affiliate/images/en/join_now_btn.jpg) no-repeat;
	}		
	</style>
	
		<div id="header">
			<div class="center_content">
				<div id="logo"><img src="{{url()}}/affiliate/images/en/logo_main.png" width="514" height="129"/></div>
				<div id="form_portion">
					<form method="post" action="login_process" accept-charset="UTF-8" autocomplete="off">
						<a href="register">
							<div id="reg_btn"></div>
						</a>
						<input type="text" name="username" class="username" placeholder="Username" value=""/>						
						<input type="password" name="password" class="pass" placeholder="Password"/>
						
						<div id="code">
							<img src="captcha" width="50" height="22"/>
						</div>
						
						<input type="text" class="code_fill" name="code" placeholder="Code"/>
						<a href="#">
							<input id="login_btn" type="submit" value=""style="cursor:pointer;">
						</a>
						<input type="hidden" value="{{csrf_token()}}" name="_token">
					</form>
				</div>
			</div>
		</div>
		
		<div id="navbar">
			<div class="center_content">
				<ul>
					<li id="nav_home"><a href="index.php">Home</a></li>
					<div class="nav_divider"></div>
					<li id="nav_product"><a href="product.php">Our Products</a></li>
					<div class="nav_divider"></div>
					<li id="nav_cp"><a href="commission.php">Commission Plan</a></li>
					<div class="nav_divider"></div>
					<li id="nav_faq"><a href="faq.php">FAQ</a></li>
					<div class="nav_divider"></div>
					<li id="nav_contact"><a href="#">CONTACT US</a>
					<div class="contact_dd">
					<table border="0" cellspacing="0" cellpadding="2">
						<tr>
							<td>Email </td>
							<td>:</td>
							<td><a class="__cf_email__" href="/cdn-cgi/l/email-protection" data-cfemail="b9d8dfdfd0d5d0d8cddcf9d0d7dfd0d7d0ced0d797dad6d4">[email&#160;protected]</a><script data-cfhash='f9e31' type="text/javascript">
							/* <![CDATA[ */!function(){try{var t="currentScript"in document?document.currentScript:function(){for(var t=document.getElementsByTagName("script"),e=t.length;e--;)if(t[e].getAttribute("data-cfhash"))return t[e]}();if(t&&t.previousSibling){var e,r,n,i,c=t.previousSibling,a=c.getAttribute("data-cfemail");if(a){for(e="",r=parseInt(a.substr(0,2),16),n=2;a.length-n;n+=2)i=parseInt(a.substr(n,2),16)^r,e+=String.fromCharCode(i);e=document.createTextNode(e),c.parentNode.replaceChild(e,c)}t.parentNode.removeChild(t);}}catch(u){}}()/* ]]> */</script></td>
						</tr>
						<tr>
							<td>Skype </td>
							<td>:</td>
							<td>infiniwin</td>
						</tr>
						<tr>
							<td>Phone</td>
							<td>:</td>
							<td>6011 2780 7697</td>
						</tr>
						<tr>
							<td>Live Chat</td>
							<td>:</td>
							<td>24x7</td>
						</tr>
					</table>
					<div class="clear"></div>
					</div>
					</li>
				</ul>
			</div>
		</div> 
		
		<div id="banner_wrapper">
			<div id="slider_banner"><img src="{{url()}}/affiliate/images/en/banner1.jpg" width="1000" height="354"/></div>
		</div>
			
		<div id="steps">
			<div id="steps_content"><img src="{{url()}}/affiliate/images/en/steps.png" width="1000" height="113"/></div>
		</div> 
		
		<div id="footer_content"><div class="center_content">
			<div class="boxes" id="product" style="margin-left:-4px !important">
				<a href="product.php"><img src="{{url()}}/affiliate/images/en/product.png" width="329" height="255"/></a>
			</div>
			<div class="boxes" id="spinwheel">
				<img src="{{url()}}/affiliate/images/en/free-spin.png" width="329" height="255"/>
			</div>
			<div class="boxes" id="livechat">
				<a href="https://secure.livechatinc.com/licence/3155342/open_chat.cgi?groups=1" target="_blank"><img src="{{url()}}/affiliate/images/en/livechat.png" width="329" height="255" border="0"/></a>
			</div>
			</div> 
		</div> 
				
		<div id="copyright">
			<div class="center_content">
				<div id="icon">
					<img src="{{url()}}/affiliate/images/en/footer_icon.png" width="525" height="31"/>
				</div>
				<div id="left_copyright">© Copyright<span style="color:#767057"> {{Config::get('setting.website_name')}} Affiliate</span>. All rights reserved.</div>
				<div id="right_tnc">
					<a href="#">Terms and Conditions</a> &nbsp; &nbsp;&nbsp;
					<a href="#">Disclaimer</a> &nbsp;&nbsp;&nbsp;
					<a href="#">Privacy Policy</a> &nbsp;&nbsp;&nbsp;
					<a href="#">FAQ</a>
				</div>
			</div> 
		</div>  
		<iframe id="frameSubmit" name="frameSubmit" src="resources/script/index.html" border="0" marginwidth="0" marginheight="0" hspace="0" vspace="0" frameborder="0" scrolling="auto" style="position:absolute; z-index:0; visibility:hidden"></iframe>
	</body>
</html>
