<!DOCTYPE html>
<html lang="en">
<head>
    <title id='Description'>
    </title>
	<head>
	<link rel="stylesheet" href="affiliate/jqwidgets/styles/jqx.base.css" type="text/css" />
    <link rel="stylesheet" href="affiliate/jqwidgets/styles/jqx.bootstrap.css" type="text/css" />
    <script type="text/javascript" src="affiliate/jqwidgets/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="affiliate/jqwidgets/jqx-all.js"></script>
	 <script type="text/javascript">
        $(document).ready(function () {
			
			Start_Time = "21:30:00"; // Initial value retrieved from the grid.
			sTime = ConvertToDate(Start_Time); 

			$("#form_submit_btn").jqxButton({ width: '150', template: "success"});
			$("#form_submit_btn").on('click', function () {
                   $("#events").find('span').remove();
                   $("#events").append('<span>Submit Button Clicked</span>');
             });
			//$("#form_wapper").jqxExpander({ width: '100%', theme:'bootstrap', toggleMode: 'dblclick'});
			@foreach( $form['inputs'] as $input )
				@if($input['type'] == 'date')	
					var t = "{{$input['value']}}".split(/[- :]/);
					$("#{{$input['name']}}").jqxDateTimeInput({ width: '120px' , height: '25px' , formatString: "yyyy-MM-dd" ,openDelay: 0,closeDelay: 0});
					$('#{{$input['name']}}').jqxDateTimeInput('setDate', new Date(t[0], t[1]-1, t[2]));
				@endif
				@if($input['type'] == 'datetime')
					var t = "{{$input['value']}}".split(/[- :]/);
					$("#{{$input['name']}}").jqxDateTimeInput({  width: '220px' , height: '25px' , formatString: "yyyy-MM-dd h:mm:ss" ,openDelay: 0,closeDelay: 0,showTimeButton: true});
					 $('#{{$input['name']}}').jqxDateTimeInput('setDate', new Date(t[0], t[1]-1, t[2], t[3], t[4], t[5]));
				@endif
				@if($input['type'] == 'html')
					$('#{{$input['name']}}').jqxEditor({
						height: "300px",
						width: '1000px'
					});
				@endif
			@endforeach	
        });
		
		
		
		
		
		
		function ConvertToDate(stringTime)
		{
		var date = new Date();
		hours = stringTime.substr(0,2);
		minutes = stringTime.substr(3,2);
		date.setHours(parseInt(hours));
		date.setMinutes(parseInt(minutes));

		return date;
		}

		function submit_form(){
			
				var form_data = new FormData();                  
				var data = $('form').serializeArray();
				var obj = {};
				for (var i = 0, l = data.length; i < l; i++) {
					form_data.append(data[i].name, data[i].value);
				}
				@foreach( $form['inputs'] as $input )
					@if($input['type'] == 'datetime' || $input['type'] == 'date')
						form_data.append('{{$input['name']}}', $('#{{$input['name']}}').jqxDateTimeInput('getText'));
					@elseif($input['type'] == 'file')
						var file_data = $("#{{$input['name']}}").prop("files")[0];  
						form_data.append("{{$input['name']}}", file_data);
					@endif
				@endforeach	
				
				form_data.append('_token', '{{csrf_token()}}');

				$.ajax({
					type: "POST",
					url: "{{$module}}-do-add-edit?",
							dataType: 'script',
							cache: false,
							contentType: false,
							processData: false,
							data: form_data,         
							type: 'post',
							complete: function(json){

								 obj = JSON.parse(json.responseText);
								 $('.error_msg').html('');
								 $.each(obj, function(i, item) {
									if( '{{Lang::get('COMMON.SUCESSFUL')}}' == item ){
										alert('{{Lang::get('COMMON.SUCESSFUL')}}');
									}else{
										$('.'+i+'_error').html(item);
									}
								})
							
							},
								
				});

		}
		
		
		
    </script>
	</head>
	<body class='default'>

<form id="cboForm" method="post" action="" target="frameSubmit" @if($form['multipart'])enctype="multipart/form-data"@endif>
<div id="form_wapper" >
<table>
@foreach( $form['inputs'] as $input )
	<tr>
		<td class="formLabelLeft">{{$input['title']}}</td>
		<td>
			@if($input['type'] == 'text')
				<input id="{{$input['name']}}" type="text" name="{{$input['name']}}" value="{{$input['value']}}" @if( isset($input['params']['disabled']) && $input['params']['disabled'])disabled @endif>
			@elseif($input['type'] == 'password')
				<input type="password" name="{{$input['name']}}">
				@elseif($input['type'] == 'textarea')
				<textarea rows="5" cols="30" name="{{$input['name']}}">{{$input['value']}}</textarea>
			@elseif($input['type'] == 'checkbox_single')
				<input type="checkbox" name="{{$input['name']}}" value="1" @if($input['value'])checked @endif>{{$value}}
			@elseif($input['type'] == 'checkbox')	
				@foreach( $input['params']['options'] as $key => $value )
						<input type="checkbox" name="{{$input['name']}}[]" value="{{$key}}" @if(in_array($key,$input['params']['values']))checked @endif>{{$value}}
				@endforeach
				
			@elseif($input['type'] == 'radio')	
				@foreach( $input['params']['options'] as $key => $value )
					@if( $key == $input['value'])
						<input name="{{$input['name']}}" type="radio" value="{{$key}}" checked >{{$value}}
					@else
						<input name="{{$input['name']}}" type="radio" value="{{$key}}" >{{$value}}
					@endif
				@endforeach
			@elseif($input['type'] == 'select')	
				<select name="{{$input['name']}}">
					@foreach( $input['params']['options'] as $key => $value )
						<option  value="{{$key}}" @if($input['value'] == $key) selected @endif>{{$value}}</option>
					@endforeach
				</select>
			@elseif($input['type'] == 'date')	
				<div id="{{$input['name']}}"></div>
			@elseif($input['type'] == 'datetime')	
				<div id="{{$input['name']}}"></div>
			@elseif($input['type'] == 'daterange')	
			
			@elseif($input['type'] == 'showonly')	
				{{$input['value']}} <input type="hidden" name="{{$input['name']}}" value="{{$input['value']}}">
			@elseif($input['type'] == 'file')		
				<input class="upload" type="file" name="{{$input['name']}}" id="{{$input['name']}}">  [<a target="_blank" href="{{$input['value']}}">View</a>] 
			@elseif($input['type'] == 'hidden')
				<input type="hidden" name="{{$input['name']}}" value="{{$input['value']}}">
			@elseif($input['type'] == 'html')
				<textarea id="{{$input['name']}}" name="{{$input['name']}}" class="html_editor">{{$input['value']}}</textarea>
			@endif
		</td>
		<td>
			<div class="{{$input['name']}}_error error_msg" style="color:red;"></div>
		</td>
	</tr>
@endforeach		
</table>

		</div>
		<div class="clear">
            <input style='margin-top: 20px;cursor: pointer;' value="Submit" id='form_submit_btn' onClick="submit_form()"/>
        </div>
	
</form>

</body>
</html>