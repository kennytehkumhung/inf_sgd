@extends('royalewin/master')

@if (Session::get('currency') == 'MYR')
    @section('title', 'Best Online Live Casino Malaysia - FAQ')
    @section('keywords', 'Best Online Live Casino Malaysia - FAQ')
    @section('description', 'Find an answer to any of your queries regarding RoyaleWin Online Casino Malaysia.')
@elseif (Session::get('currency') == 'IDR')
    @section('title', 'Bandar Casino Online, Situs Judi Indonesia - Tanya Jawab')
    @section('keywords', 'Bandar Casino Online, Situs Judi Indonesia - Tanya Jawab')
    @section('description', 'Pertanyaan - RoyaleWin adalah agen bola, bandar casino online dan situs judi online terpercaya untuk Anda penggemar taruhan bola dan pecinta judi online di Indonesia.')
@endif

@section('css')
@parent

<style type="text/css">
    .index_links a {
        color: #fff;
    }
    
    .othersCont p {
        margin-bottom: 3px;
    }
    
    .othersCont .section_title {
        margin-top: 14px;
    }
</style>
@endsection

@section('js')
@parent

<script type="text/javascript">
    $(function () {
        var html = "";
        
        $(".section_title").each(function () {
            var obj = $(this);
            
            html += '<li><a href="#' + obj.attr("id") + '">' + obj.html() + '</a></li>';
        });
        
        $("ol.index_links").html(html);
    });
</script>
@endsection

@section('content')
<div class="midSect bgOthers">
    <div class="contOthers">
        <span>{{ strtoupper(Lang::get('public.FAQ')) }}</span>
    </div>
    <div class="othersMenu">
        @include('royalewin.include.helpmenu')
        <div class="clr"></div>
    </div>
    <div class="othersCont">
        {!! htmlspecialchars_decode($content) !!}
    </div>
</div>
@endsection