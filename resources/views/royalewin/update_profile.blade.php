@extends('royalewin/master')

@section('title', 'Profile')

@section('css')
@parent
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<link href="{{ asset('/royalewin/resources/css/table.css') }}" rel="stylesheet">

<style type="text/css">
    #bet_history a,
    #bet_history_product a,
    #trans_history a {
        color: #fff;
    }
    .popup_img { display: inline-block; position: absolute; z-index: 10001; margin: 20px auto; top: 0px; bottom:0; left:0; right: 0; }
</style>
@endsection

@section('js')
@parent
<script src="{{ asset('/royalewin/resources/js/jquery-ui.js') }}"></script>
<script type="text/javascript">
    $(function () {
        var tab = "{{ Request::input('tab', 0) }}";
        
        $(".example-a").accTabs({
            defaultTab: tab
        });
        
        $(".datepicker").datepicker({dateFormat: 'yy-mm-dd', defaultDate: new Date()});
        
        load_profile();
        load_deposit();
        load_withdrawal();
        $("#transfer_to").change();
        
        setTimeout(transaction_history, 3000);
        setTimeout(bet_history, 4000);
        
        $("#masklayer").click(function () {
            $(".popup_img").fadeOut(100);
            $("#masklayer").fadeOut(100);
        });
    });
    
    function closePopup() {
        $(".popup_img").fadeOut(100);
        $("#masklayer").fadeOut(100);
    }

    function closeTryMobileBox() {
        $('#tryMobileBox').hide();
    }
    
    // Reset form.
    function resetForm(formId) {
        var form = $("#" + formId);
        
        $("input[type='text']:not([readonly])", form).each(function () {
            console.log($(this));
        });
        
        // Standard reset method.
        $("input[type='text']:not([readonly])", form).val("");
        $("input[type='password']:not([readonly])", form).val("");
        $("input[type='radio']:not([readonly])", form).prop("checked", false);
        $("input[type='checkbox']:not([readonly])", form).prop("checked", false);
        $("select:not([readonly])", form).each(function () {
            $(this).val($("option:first", $(this)).val());
        });
    }
    // End reset form.
    
    // Update password.
    function checkPass() {
        //Store the password field objects into variables ...
        var new_password = document.getElementById('new_password');
        var confirm_new_password = document.getElementById('confirm_new_password');
        //Store the Confimation Message Object ...
        var message = document.getElementById('confirmMessage');
        //Set the colors we will be using ...
        var goodColor = "#66cc66";
        var badColor = "#ff6666";
        //Compare the values in the password field and the confirmation field
        if (new_password.value == confirm_new_password.value) {
            //The passwords match.
            //Set the color to the good color and inform the user that they have entered the correct password 
            confirm_new_password.style.backgroundColor = goodColor;
            message.style.color = goodColor;
            message.innerHTML = "Passwords Match!"
        } else {
            //The passwords do not match.
            //Set the color to the bad color and notify the user.
            confirm_new_password.style.backgroundColor = badColor;
            message.style.color = badColor;
            message.innerHTML = "Passwords Do Not Match!"
        }
    }

    function update_password() {
        $.ajax({
            type: "POST",
            url: "{{action('User\MemberController@ChangePassword')}}",
            data: {
                _token: "{{ csrf_token() }}",
                current_password: $('#current_password').val(),
                new_password: $('#new_password').val(),
                confirm_new_password: $('#confirm_new_password').val()
            },
        }).done(function (json) {
            obj = JSON.parse(json);
            var str = '';
            $.each(obj, function (i, item) {
                str += item + '\n';
            });
            if (str != '') {
                alert(str);
            }
        });
    }
    // End update password.
    
    // Update personal info.
    function load_profile() {
        $.ajax({
            url: "{{ route('update-profile') }}",
            type: "GET",
            dataType: "JSON",
            data: {
                _token: "{{ csrf_token() }}",
                as_json: true
            },
            success: function (result) {
                if (typeof result != undefined && result != null) {
                    $("#profile_email").val(result.email);
                    $("#profile_fullname").val(result.fullname);
                    $("#profile_telmobile").val(result.telmobile);
                    $("#profile_gender").val(result.gender);
                    $("#profile_address").val(result.resaddress);
                    $("#profile_city").val(result.rescity);
                    $("#profile_dob_hdn").val(result.dob);
                    $("#profile_referral_id").val(result.referralid);

                    $("#profile_referral_id_link").attr("href", "{{ route('register') }}?referralid=" + result.referralid);

                    if (result.dob == "0000-00-00") {
                        $("#profile_dob_writeable").show();
                        $("#profile_dob_readonly").hide();
                    } else {
                        $("#profile_dob_writeable").hide();
                        $("#profile_dob_readonly").show();
                        $("#profile_dob").val(result.dob);
                    }
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                setTimeout(function () {
                    load_profile();
                }, 1000);
            }
        });
    }
    
    function update_profile() {
		var dob = $("#profile_dob_hdn").val();
		if (typeof $('#dob_year').val() != undefined) {
			dob = $('#dob_year').val() + '-' + $('#dob_month').val() + '-' + $('#dob_day').val();
        }
        $.ajax({
            type: "POST",
            url: "{{action('User\MemberController@UpdateDetail')}}",
            data: {
                _token: "{{ csrf_token() }}",
                gender: $('#profile_gender').val(),
                dob: dob,
                address: $("#profile_address").val(),
                city: $("#profile_city").val()
            }
        }).done(function (json) {
            obj = JSON.parse(json);
            var str = '';
            $.each(obj, function (i, item) {
                str += item + '\n';
            });
            if (str != '') {
                alert(str);
            }
        });
    }
    // End update personal info.
    
    // Deposit.
    function load_deposit() {
        $.ajax({
            url: "{{ route('deposit') }}",
            type: "GET",
            dataType: "JSON",
            data: {
                _token: "{{ csrf_token() }}",
                as_json: true
            },
            success: function (result) {
                if (typeof result != undefined && result != null) {
                    var resultHtml = "";

                    for (var key in result.banks) {
                        if (result.banks.hasOwnProperty(key)) {
                            resultHtml += '<td>\n\
                                <label title="' + result.banks[key].name + '">\n\
                                    <table style="margin-top: -4px;">\n\
                                        <tr>\n\
                                            <td style="vertical-align: middle;">\n\
                                                <input class="colorRadio bkRadio" type="radio" name="bank" value="' + result.banks[key].bnkid + '" data-min="' + result.banks[key].min + '" data-max="' + result.banks[key].max + '" data-account-name="' + result.banks[key].bankaccname + '" data-account-number="' + result.banks[key].bankaccno + '" style="width: 16px;" onchange="depositBankChanged(this);">\n\
                                            </td>\n\
                                            <td style="vertical-align: middle;">\n\
                                                <img src="' + result.banks[key].image + '" style="max-width: 100px; height: auto;" />\n\
                                            </td>\n\
                                        </tr>\n\
                                    </table>\n\
                                </label>\n\
                            </td>';
                        }
                    }
                    
                    $("#deposit_banklist").html("<tr>" + resultHtml + "</tr>");
                    $("#deposit_banklist input[type='radio']:eq(0)").click();
                    
                    resultHtml = "";
                    var promoHeight = 0;
                    
                    for (var key in result.promos) {
                        if (result.promos.hasOwnProperty(key)) {
                            promoHeight += 61;
                            resultHtml += '<tr>\n\
                                <td>\n\
                                    <label>\n\
                                        <input type="radio" name="promo" value="' + result.promos[key].code + '">\n\
                                        <span>' + result.promos[key].name + '</span><br>\n\
                                        <img src="' + result.promos[key].image + '" style="max-height: 56px; margin-left: 23px; margin-bottom: 5px;" />\n\
                                    </label>\n\
                                </td>\n\
                            </tr>';
                        }
                    }
                    
                    if (resultHtml == "") {
                        $("#deposit_promolist").parent().hide();
                    } else {
                        var minHeight = parseFloat($("#deposit_form").parent().css("min-height"));
                        $("#deposit_form").parent().css("min-height", minHeight + promoHeight + "px");
                        
                        resultHtml += '<tr>\n\
                            <td>\n\
                                <label>\n\
                                    <input type="radio" name="promo" value="0">\n\
                                    <span>{{ Lang::get('public.ProceedWithoutAnyOfThePromotionsAbove') }}</span>\n\
                                </label>\n\
                            </td>\n\
                        </tr>';

                        $("#deposit_promolist").html(resultHtml);
                    }
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                setTimeout(function () {
                    load_deposit();
                }, 1000);
            }
        });
    }
    
    function depositBankChanged(obj) {
        var rObj = $(obj);
        
        $("#deposit_account_name").html(rObj.data("account-name"));
        $("#deposit_account_number").html(rObj.data("account-number"));
        $("#deposit_min").html(rObj.data("min"));
        $("#deposit_max").html(rObj.data("max"));
    }
    
    function deposit_submit(){
        var file_data = $("#deposit_receipt").prop("files")[0];  
        var form_data = new FormData();
        
        form_data.append("file", file_data);
	
        var data = $('#deposit_form').serializeArray();
        var obj = {};
        
        for (var i = 0, l = data.length; i < l; i++) {
            form_data.append(data[i].name, data[i].value);
        }
	
        form_data.append('_token', '{{csrf_token()}}');
        
        $.ajax({
            type: "POST",
            url: "{{route('deposit-process')}}?",
            dataType: 'script',
            cache: false,
            contentType: false,
            processData: false,
            data: form_data, // Setting the data attribute of ajax with file_data
            type: 'post',
            beforeSend: function() {
                $('#deposit_sbumit_btn').attr('onclick', '');
            },
            complete: function(json){
                obj = JSON.parse(json.responseText);
                var str = '';
                $.each(obj, function(i, item) {
                    if ('{{Lang::get('COMMON.SUCESSFUL')}}' == item) {
                        alert('{{Lang::get('COMMON.SUCESSFUL')}}');
                        window.location.href = "{{route('update-profile')}}?tab=4";
                    } else {
                        $('#deposit_sbumit_btn').attr('onclick', 'deposit_submit()');
                        str += item + '\n';
                    }
                });
                if (str != '') {
                    alert(str);
                }
            }
        });
    }
    // End deposit.
    
    // Withdrawal.
    function load_withdrawal() {
        $.ajax({
            url: "{{ route('withdraw') }}",
            type: "GET",
            dataType: "JSON",
            data: {
                _token: "{{ csrf_token() }}",
                as_json: true
            },
            success: function (result) {
                if (typeof result != undefined && result != null) {
                    $("#withdraw_min").html(result.min);
                    $("#withdraw_max").html(result.max);

                    var banklistObj = $("#withdraw_bank");

                    for (var key in result.banklists) {
                        if (result.banklists.hasOwnProperty(key)) {
                            banklistObj.append('<option value="' + result.banklists[key].code + '">' + key + '</option>');
                        }
                    }
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                setTimeout(function () {
                    load_withdrawal();
                }, 1000);
            }
        });
    }
    
    function submit_withdraw() {
        $.ajax({
            type: "POST",
			url: "{{route('withdraw-process')}}?" + $('#withdraw_form').serialize(),
			data: {
				_token: "{{ csrf_token() }}",
			},
			beforeSend: function() {
                            $('#withdraw_btn_submit').attr('onClick','');
                            $('#withdraw_btn_submit').html('Loading...');
			},
			success: function(json) {
				obj = JSON.parse(json);
				var str = '';
				$.each(obj, function(i, item) {
					if ('{{Lang::get('COMMON.SUCESSFUL')}}' == item) {
						alert('{{Lang::get('COMMON.SUCESSFUL')}}');
						window.location.href = "{{route('update-profile')}}?tab=4";
					} else {
						str += item + '\n';
					}
				});
                if (str != '') {
                    alert(str);
                }
			}
		});
    }
    // End withdrawal.
    
    // Transfer.
    function load_transfer_balance(code, type) {
        if(code == 'MAIN') {
            url = '{{route('mainwallet')}}';
        } else {
            url = '{{route('getbalance')}}';
        }
        $.ajax({
            type: "POST",
            url:  url,
            data: {
                _token:     "{{ csrf_token() }}",
                product:    code,
            },
            beforeSend: function(balance) {
                $('#' + type).html('<img style="float:left;position:static;top:0px;margin-left:20px;" src="{{asset('/front/img/ajax-loading.gif')}}" width="20" height="20">');
            },
            success: function(balance) {
                if (code == 'MAIN') {
                    $('.main_wallet').html(parseFloat(Math.round(balance * 100) / 100).toFixed(2));
                }
                $('#' + type).html('{{Session::get('currency')}} ' + parseFloat(Math.round(balance * 100) / 100).toFixed(2));
            }
        });
    }
    
    function submit_transfer() {
        $.ajax({
            type: "POST",
            url: "{{route('transfer-process')}}",
            data: {
				_token: 		 "{{ csrf_token() }}",
                amount:			 $('#transfer_amount').val(),
                from:     		 $('#transfer_from').val(),
                to: 			 $('#transfer_to').val(),
            },
            beforeSend: function() {
                $('#btn_submit_transfer').attr('onClick', '');
                $('#btn_submit_transfer').html('Loading...');
            },
            success: function(json) {
                obj = JSON.parse(json);
                var str = '';
                $.each(obj, function(i, item) {

                    if ('{{Lang::get('COMMON.SUCESSFUL')}}' == item){

                    //$('.main_wallet').html(obj.main_wallet);

                    }
                    if (i != 'main_wallet') {
                        str += item + '\n';
                    }
                });

                load_transfer_balance($('#transfer_from').val(), 'transfer_from_balance');
                load_transfer_balance($('#transfer_to').val(), 'transfer_to_balance');
                getBalance(false);
                $('#btn_submit_transfer').attr('onClick', 'submit_transfer()');
                $('#btn_submit_transfer').html('{{Lang::get('public.Submit')}}');
                if (str != '') {
                    alert(str);
                }
            }
        });
    }
    // End transfer.
    
    // Transfer history.
    function transaction_history() {
        $.ajax({
            type: "POST",
            url: "{{action('User\TransactionController@transactionProcess')}}",
            data: {
                _token: "{{ csrf_token() }}",
                date_from: $('#trans_history_date_from').val(),
                date_to: $('#trans_history_date_to').val(),
                record_type: $('#trans_history_record_type').val()
            },
        }).done(function (json) {
            var str = '';
            str += '	<tr><th>{{Lang::get('public.ReferenceNo')}}</th><th>{{Lang::get('public.DateOrTime')}}</th><th>{{Lang::get('public.Type')}}</th><th>{{Lang::get('public.Amount')}}({{Session::get('currency')}})</th><th>{{Lang::get('public.Status')}}</th><th>{{Lang::get('public.Reason')}}</th></tr>';
            obj = JSON.parse(json);
            var count = 0;
            
            $.each(obj, function (i, item) {
                str += '<tr><td>' + item.id + '</td><td>' + item.created + '</td><td>' + item.type + '</td><td>' + item.amount + '</td><td>' + item.status + '</td><td>' + item.rejreason + '</td></tr>';
                count++;
            });
            
            if (count <= 0) {
                str += '<tr><td colspan="6"><h2><center>{{ Lang::get('public.NoDataAvailable') }}</center></h2></td></tr>';
            }

            $('#trans_history').html(str);
        });
    }
    setInterval(update_mainwallet, 10000);

    setInterval(function() {
        $("#trans_history_button").trigger("click");
    }, 10000);
    // End transfer history.
    
    // Bet history.
    function bet_history() {
        $.ajax({
            type: "POST",
            url: "{{action('User\TransactionController@profitlossProcess')}}",
            data: {
                _token: "{{ csrf_token() }}",
                date_from: $('#bet_history_date_from').val(),
                date_to: $('#bet_history_date_to').val(),
                record_type: $('#bet_history_record_type').val()
            },
        }).done(function (json) {
            //var str;
            var str = '';
            str += '	<tr><th>{{ Lang::get('public.Date') }}</th><th>{{ Lang::get('public.Wager') }}</th><th>{{ Lang::get('public.Stake') }}</th><th>{{ Lang::get('public.WinLose') }}</th><th>{{ Lang::get('public.RealBet') }}</th></tr>';
            obj = JSON.parse(json);
            var count = 0;

            $.each(obj, function (i, item) {
                str += '<tr><td style="text-decoration: underline;color:blue;">' + item.date + '</td><td>' + item.wager + '</td><td>' + item.stake + '</td><td>' + item.winloss + '</td><td>' + item.validstake + '</td></tr>';
                count++;
            });
            
            if (count <= 0) {
                str += '<td colspan="6"><h2><center>{{ Lang::get('public.NoDataAvailable') }}</center></h2></td>';
            }

            $('#bet_history').html(str);
        });
    }
    
    function transaction_history_product(date_from, product) {
        $.ajax({
            type: "POST",
            url: "{{action('User\TransactionController@profitlossProcess')}}",
            data: {
                _token: "{{ csrf_token() }}",
                date_from: date_from,
                group_by: product,
            },
        }).done(function (json) {
            //var str;
            var str = '';
            str += '	<tr><th>{{ Lang::get('public.Product') }}</th><th>{{ Lang::get('public.Wager') }}</th><th>{{ Lang::get('public.Stake') }}</th><th>{{ Lang::get('public.WinLose') }}</th><th>{{ Lang::get('public.RealBet') }}</th></tr>';
            obj = JSON.parse(json);
            var count = 0;

            $.each(obj, function (i, item) {
                str += '<tr><td>' + item.product + '</td><td>' + item.wager + '</td><td>' + item.stake + '</td><td>' + item.winloss + '</td><td style="text-decoration: underline;color:blue;">' + item.validstake + '</td></tr>';
                count++;
            });
            
            if (count <= 0) {
                str += '<td colspan="6"><h2><center>{{ Lang::get('public.NoDataAvailable') }}</center></h2></td>';
            }

            $('#bet_history_product').html(str);
        });
    }

    setInterval(function() {
        $("#bet_history_button").trigger("click");
    }, 10000);
    // End bet history.
</script>
@endsection

@section('content')
<div class="midSect">
    <div class="main">
        <div class="tabs example-a">
            <h3 class="prof"><div class="prIco"><img src="{{ asset('/royalewin/img/prof-icon.png') }}" width="18" height="18" /></div>{{ Lang::get('public.Profile') }}</h3>
            <div style="min-height: 815px;">
                <div id="password_form" class="tbLeft">
                    <h2>{{ Lang::get('public.Profile') }}</h2>
                    <div class="wallHead">{{ Lang::get('public.ChangePassword') }}</div>
                    
                    <div class="wallRowL">
                        <div class="wallRowLTxt">{{ Lang::get('public.CurrentPassword') }}*</div>
                        <div class="wallRowLselect">
                            <input type="password" id="current_password" name="current_password" />
                        </div>
                        <div class="clr"></div>
                    </div>
                    
                    <div class="wallRowL">
                        <div class="wallRowLTxt">{{ Lang::get('public.NewPassword') }}*</div>
                        <div class="wallRowLselect">
                            <input type="password" id="new_password" name="new_password" />
                        </div>
                        <div class="wallRem">
                            {{ Lang::get('public.NewPasswordNote2') }}
                        </div>
                        <div class="clr"></div>
                    </div>
                    
                    <div class="wallRowL">
                        <div class="wallRowLTxt">{{ Lang::get('public.ReenterPassword') }}*</div>
                        <div class="wallRowLselect">
                            <input type="password" id="confirm_new_password" name="confirm_new_password" onkeyup="checkPass(); return false;" />
                        </div>
                        <div class="wallRem">
                            <span id="confirmMessage" class="confirmMessage"></span>
                        </div>
                        <div class="clr"></div>
                    </div>
                    
                    <div class="wallReset">
                        <a href="javascript:void(0);" onclick="resetForm('password_form');">{{ Lang::get('public.Reset') }}</a>
                    </div>
                    
                    <div class="wallSubmit">
                        <a href="javascript:void(0);" onclick="update_password()">{{ Lang::get('public.Submit') }}</a>
                    </div>
                </div>

                <div id="personal_info_form" class="tbLeft tp1">
                    <h2>{{ Lang::get('COMMON.PERSONALINFO') }}</h2>

                    <div class="wallRowL">
                        <div class="wallRowLTxt">{{ Lang::get('public.Email') }}</div>
                        <div class="wallRowLselect">
                            <input type="text" id="profile_email" readonly />
                        </div>
                        <div class="clr"></div>
                    </div>

                    <div class="wallRowL">
                        <div class="wallRowLTxt">{{ Lang::get('public.FullName') }}</div>
                        <div class="wallRowLselect">
                            <input type="text" id="profile_fullname" readonly />
                        </div>

                        <div class="clr"></div>
                    </div>

                    <div class="wallRowL" style="height: 66px;">
                        <div class="wallRowLTxt">{{ Lang::get('public.MyReferralID') }}</div>
                        <div class="wallRowLselect">
                            <input type="text" id="profile_referral_id" readonly />
                            <br>
                            <a href="#" id="profile_referral_id_link" target="_blank" style="color: #ffffff;">{{ Lang::get('public.RegisterDownline') }}</a>
                        </div>
                        <div class="clr"></div>
                    </div>

                    <div class="wallRowL">
                        <div class="wallRowLTxt">{{ Lang::get('public.MobileNumber') }}</div>
                        <div class="wallRowLselect">
                            <input type="text" id="profile_telmobile" readonly />
                        </div>
                        <div class="clr"></div>
                    </div>

                    <div class="wallRowL">
                        <div class="wallRowLTxt">{{ Lang::get('public.Gender') }}</div>
                        <div class="wallRowLselect">
                            <label>
                                <select id="profile_gender" name="gender">
                                    <option value="1">{{ Lang::get('public.Male') }}</option>
                                    <option value="2">{{ Lang::get('public.Female') }}</option>
                                </select>
                            </label>
                        </div>
                        <div class="clr"></div>
                    </div>

                    <div class="wallRowL" style="height: 92px;">
                        <div class="wallRowLTxt">{{ Lang::get('public.DOB') }}</div>
                        <div class="wallRowLselect">
                            <input type="hidden" id="profile_dob_hdn" />
                            <div id="profile_dob_writeable" style="display: none;">
                                <select id="dob_day" style="margin-bottom: 8px;">
                                    @foreach (\App\Models\AccountDetail::getDobDayOptions() as $key => $val)
                                        <option value="{{ $key }}">{{ $val }}</option>
                                    @endforeach
                                </select>
                                <select id="dob_month" style="margin-bottom: 8px;">
                                    @foreach (\App\Models\AccountDetail::getDobMonthOptions() as $key => $val)
                                        <option value="{{ $key }}">{{ $val }}</option>
                                    @endforeach
                                </select>
                                <select id="dob_year">
                                    @foreach (\App\Models\AccountDetail::getDobYearOptions() as $key => $val)
                                        <option value="{{ $key }}">{{ $val }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div id="profile_dob_readonly">
                                <input type="text" id="profile_dob" readonly />
                            </div>
                        </div>
                        <div class="clr"></div>
                    </div>

                    <div class="wallRowL">
                        <div class="wallRowLTxt">{{ Lang::get('COMMON.ADDRESS') }}</div>
                        <div class="wallRowLselect">
                            <input type="text" id="profile_address" />
                        </div>
                        <div class="clr"></div>
                    </div>

                    <div class="wallRowL">
                        <div class="wallRowLTxt">{{ Lang::get('COMMON.CITY') }}</div>
                        <div class="wallRowLselect">
                            <input type="text" id="profile_city" />
                        </div>
                        <div class="clr"></div>
                    </div>
                    
                    <div class="wallReset">
                        <a href="javascript:void(0);" onclick="resetForm('personal_info_form');">{{ Lang::get('public.Reset') }}</a>
                    </div>
                    
                    <div class="wallSubmit">
                        <a href="javascript:void(0);" onclick="update_profile()">{{ Lang::get('public.Submit') }}</a>
                    </div>
                </div>

                @include('royalewin.include.balance_right')
            </div>
            
            <h3>{{ Lang::get('public.Deposit') }}</h3>
            <div>
                    @if(!empty($popup_banner))
                        <div id="masklayer" style="height: 100%;position: fixed;opacity: 0.6;background-color: black;z-index: 10001;left: 0px;right: 0px;top: 0px;display:block;" onclick="closePopup();"></div>
                        <img src="{{$popup_banner}}" class="popup_img" style="max-width: 800px;">
                    @endif
                <div class="wallIntro">{{ Lang::get('public.DepositSteps1b') }}</div>
                <form id="deposit_form">
                    <div class="tbLeft tp2">
                        <h2>{{ Lang::get('public.BankingOptions') }}</h2>

                        <div class="wallRowL">
                            <div class="wallRowLTxt1">{{ Lang::get('public.Bank') }}*</div>
                            <div class="wallRowRadio">
                                <table id="deposit_banklist" width="auto" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td style="text-align: center;">{{ Lang::get('public.Loading') }}...</td>
                                    </tr>
                                </table>
                            </div>
                            <div class="clr"></div>
                        </div>
                        <div class="wallRowL" style="border-bottom: 1px dotted #e7c036;">
                            <div class="wallRowLTxt">{{ Lang::get('public.BankingInformation2') }}</div>
                            <div class="clr"></div>
                        </div>
                        <div class="wallRowL" style="height: 80px;">
                            <table style="padding-left: 5px; width: 99%;">
                                <tr>
                                    <td style="width: 35%;">{{ Lang::get('public.BankAccountName') }}</td>
                                    <td><span id="deposit_account_name"></span></td>
                                </tr>
                                <tr>
                                    <td>{{ Lang::get('public.BankAccountNo') }}</td>
                                    <td><span id="deposit_account_number"></span></td>
                                </tr>
                                <tr>
                                    <td>{{ Lang::get('public.MinimumAmount') }}</td>
                                    <td>{{Session::get('currency')}} <span id="deposit_min"></span></td>
                                </tr>
                                <tr>
                                    <td>{{ Lang::get('public.MaximumAmount') }}</td>
                                    <td>{{Session::get('currency')}} <span id="deposit_max"></span></td>
                                </tr>
                            </table>
                            <div class="clr"></div>
                        </div>
                    </div>
                    <div class="wallIntro tp4">{{ Lang::get('public.DepositSteps2') }}</div>
                    <div class="tbLeft tp3">
                        <h2>{{ Lang::get('public.DepositDetails') }}</h2>

                        <div class="wallRowL">
                            <div class="wallRowLTxt">{{ Lang::get('public.Amount') }}*</div>
                            <div class="wallRowLselect">
                                <input type="text" id="deposit_amount" name="amount" />
                            </div>
                            <div class="clr"></div>
                        </div>

                        <div class="wallRowL">
                            <div class="wallRowLTxt">{{ Lang::get('public.DepositMethod') }}*</div>
                            <div class="wallRowLselect">
                                <label>
                                    <select id="deposit_type" name="type">
                                        <option value="0">{{ Lang::get('public.OverCounter') }}</option>
                                        <option value="1">{{ Lang::get('public.InternetBanking') }}</option>
                                        <option value="2">{{ Lang::get('public.ATMBanking') }}</option>
                                    </select>
                                </label>
                            </div>

                            <div class="clr"></div>
                        </div>

                        <div class="wallRowL">
                            <div class="wallRowLTxt">{{ Lang::get('public.DateTime') }}*</div>
                            <div class="wallRowLselect">
                                <select id="deposit_hours" name="hours" style="width: 36px;">
                                    @for ($i = 1; $i <= 12; $i++)
                                        <option value="{{ str_pad($i, 2, '0', STR_PAD_LEFT) }}">{{ str_pad($i, 2, '0', STR_PAD_LEFT) }}</option>
                                    @endfor
                                </select>
                                <select id="deposit_minutes" name="minutes" style="width: 36px;">
                                    @for ($i = 0; $i <= 59; $i++)
                                        <option value="{{ str_pad($i, 2, '0', STR_PAD_LEFT) }}">{{ str_pad($i, 2, '0', STR_PAD_LEFT) }}</option>
                                    @endfor
                                </select>
                                <select id="deposit_range" name="range" style="width: 42px;">
                                    <option value="AM">AM</option>
                                    <option value="PM">PM</option>
                                </select>
                            </div>
                            <div class="wallRowLselects lft1">
                                <input type="text" class="datepicker" id="deposit_date" style="cursor:pointer;" name="date" value="{{date('Y-m-d')}}">
                            </div>
                            <div class="clr"></div>
                        </div>

                        <div class="wallRowL">
                            <div class="wallRowLTxt">{{ Lang::get('public.ReferenceNo') }}*</div>
                            <div class="wallRowLselect">
                                <input id="deposit_refno" name="refno" type="text" />
                            </div>
                            <div class="clr"></div>
                        </div>

                        <div class="wallRowL">
                            <div class="wallRowLTxt">{{ Lang::get('public.DepositReceipt') }}*</div>
                            <div class="wallRowLselect">
                                <input class="upload" type="file" name="receipt" id="deposit_receipt">
                            </div>
                            <div class="clr"></div>
                        </div>

                        <div class="wallRowL" style="min-height: 35px; height: auto;">
                            <table id="deposit_promolist" width="auto" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td style="text-align: center;">{{ Lang::get('public.Loading') }}...</td>
                                </tr>
                            </table>
                            <div class="clr"></div>
                        </div>
                        
                        <div class="wallRowL" style="min-height: 35px; height: auto;">
                            <label>
                                <input type="checkbox" id="deposit_rules" name="rules" >
                                {{ Lang::get('public.IAlreadyUnderstandRules') }}
                            </label>
                            <div class="clr"></div>
                        </div>

                        <div class="wallReset">
                            <a href="javascript:void(0);" onclick="resetForm('deposit_form');">{{ Lang::get('public.Reset') }}</a>
                        </div>

                        <div class="wallSubmit">
                            <a id="deposit_sbumit_btn" href="javascript:void(0);" onclick="deposit_submit();">{{ Lang::get('public.Submit') }}</a>
                        </div>
                    </div>
                </form>
                
                @include('royalewin.include.balance_right')
            </div>

            <h3>{{ Lang::get('public.Withdrawal') }}</h3>
            <div>
                <div class="wallIntro">{{ Lang::get('public.WithdrawalStep1') }}</div>
                <div class="wallIntro">{{ Lang::get('public.WithdrawalStep2') }}</div>
                <div class="tbLeft tp4" style="top: 85px;">
                    <h2>{{ Lang::get('public.Withdrawaldetails') }}</h2>
                    <div class="wallIntro">{{ Lang::get('public.BankDetailsContent') }}</div>
                    
                    <form id="withdraw_form">
                        <div class="wallRowL">
                            <div class="wallRowLTxt">{{ Lang::get('public.Amount') }}*</div>
                            <div class="wallRowLselect">
                                <input type="text" id="withdraw_amount" name="amount" value="0.00" />
                            </div>
                            <div class="wallRem">
                                {{ Lang::get('public.Minimum') }} {{Session::get('currency')}} <span id="withdraw_min">-</span> / {{ Lang::get('public.Maximum') }} {{Session::get('currency')}} <span id="withdraw_max">-</span>
                            </div>
                            <div class="clr"></div>
                        </div>

                        <div class="wallRowL">
                            <div class="wallRowLTxt">{{ Lang::get('public.Bank') }}*</div>
                            <div class="wallRowLselect">
                                <label>
                                    <select id="withdraw_bank" name="bank"></select>
                                </label>
                            </div>
                            <div class="clr"></div>
                        </div>

                        <div class="wallRowL">
                            <div class="wallRowLTxt">{{ Lang::get('public.FullName') }}</div>
                            <div class="wallRowLselect">
                                <input type="text" value="{{ Session::get('fullname') }}" readonly />
                            </div>
                            <div class="clr"></div>
                        </div>

                        <div class="wallRowL">
                            <div class="wallRowLTxt">{{ Lang::get('public.BankAccountNo') }}</div>
                            <div class="wallRowLselect">
                                <input id="withdraw_accountno" name="accountno" type="text" />
                            </div>
                            <div class="clr"></div>
                        </div>

                        <div class="wallReset">
                            <a href="javascript:void(0);" onclick="resetForm('withdraw_form');">{{ Lang::get('public.Reset') }}</a>
                        </div>

                        <div class="wallSubmit">
                            <a id="withdraw_btn_submit" href="javascript:void(0);" onclick="submit_withdraw();">{{ Lang::get('public.Submit') }}</a>
                        </div>
                    </form>
                </div>

                @include('royalewin.include.balance_right')
            </div>

            <h3>{{ Lang::get('public.Transfer') }}</h3>
            <div>
                <div class="tbLeft">
                    <h2>{{ Lang::get('public.TransferDetails') }}</h2>
                    <div class="wallRowL">
                        <div class="wallRowLTxt">{{ Lang::get('public.From') }}*</div>
                        <div class="wallRowLselect">
                            <label>
                                <select id="transfer_from" onchange="load_transfer_balance(this.value,'transfer_from_balance')">
                                    <option selected="selected" value="MAIN">{{Lang::get('public.MainWallet')}}</option>
                                    @foreach( Session::get('products_obj') as $prdid => $object)
                                        <?php

                                        $showPrd = true;
                                        if (Session::get('currency') == 'IDR' && ($object->code == 'PSB' || $object->code == 'OPU' || $object->code == 'CTB' || $object->code == 'SCR')) {
                                            $showPrd = false;
                                        } elseif (Session::get('currency') == 'MYR' && ($object->code == 'FBL' || $object->code == 'ISN' || $object->code == 'S128')) {
                                            $showPrd = false;
                                        }
                                        ?>
                                        @if ($showPrd)
                                            <option value="{{$object->code}}">{{$object->name}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </label>
                        </div>
                        <div class="wallRem">
                            {{ Lang::get('public.Balance') }}: <span id="transfer_from_balance" style="width: 132px !important;margin-right: 218px !important;">{{Session::get('currency')}} {{App\Http\Controllers\User\WalletController::mainwallet()}}</span>
                        </div>
                        <div class="clr"></div>
                    </div>

                    <div class="wallRowL">
                        <div class="wallRowLTxt">{{ Lang::get('public.To') }}*</div>
                        <div class="wallRowLselect">
                            <label>
                                <select id="transfer_to" onchange="load_transfer_balance(this.value,'transfer_to_balance')">
                                    @foreach( Session::get('products_obj') as $prdid => $object)
                                        <?php

                                        $showPrd = true;
                                        if (Session::get('currency') == 'IDR' && ($object->code == 'PSB' || $object->code == 'OPU' || $object->code == 'CTB' || $object->code == 'SCR')) {
                                            $showPrd = false;
                                        } elseif (Session::get('currency') == 'MYR' && ($object->code == 'FBL' || $object->code == 'ISN' || $object->code == 'S128')) {
                                            $showPrd = false;
                                        }
                                        ?>
                                        @if ($showPrd)
                                            <option value="{{$object->code}}">{{$object->name}}</option>
                                        @endif
                                    @endforeach
                                    <option  value="MAIN">{{Lang::get('public.MainWallet')}}</option>
                                </select>
                            </label>
                        </div>
                        <div class="wallRem">
                            {{ Lang::get('public.Balance') }}: <span id="transfer_to_balance" style="width: 132px !important;">{{Session::get('currency')}} 0.00</span>
                        </div>
                        <div class="clr"></div>
                    </div>

                    <div class="wallRowL">
                        <div class="wallRowLTxt">{{ Lang::get('public.Amount') }}*</div>
                        <div class="wallRowLselect">
                            <input name="" type="text" value="0.00" id="transfer_amount"/>
                        </div>
                        <div class="wallRem">
                            <span id="error_deposit" style="color:Red;display:none;">* {!! Lang::get('public.BalanceZeroPleaseMakeDeposit', array('p1' => route('update-profile').'?tab=1')) !!}</span>
                            <span id="error_require" style="color:Red;display:none;">{{Lang::get('public.Required')}}</span>
                        </div>
                        <div class="clr"></div>
                    </div>

                    <div class="wallSubmit">
                        <a id="btn_submit_transfer" href="javascript:void(0)" onClick="submit_transfer()" >{{Lang::get('public.Submit')}}</a>
                    </div>
                </div>

                @include('royalewin.include.balance_right')
            </div>

            <h3>{{ Lang::get('public.TransactionHistory') }}</h3>
            <div>
                <div class="tbLeft">
                    <h2>{{ Lang::get('public.TransactionHistory') }}</h2>
                    <div class="wallIntro"> {{  Lang::get('public.YouMayFindYourLastXTransactionsHere', array('p1' => 50)) }}</div>

                    <div class="wallRowL">
                        <div class="wallRowLTxt">{{ Lang::get('COMMON.TRANSACTIONTYPE') }}</div>
                        <div class="wallRowLselect">
                            <label>
                                <select id="trans_history_record_type" style="width:200px;">
                                    <option value="0" selected>{{ Lang::get('public.CreditAndDebitRecords') }}</option>
                                    <option value="1">{{ Lang::get('public.CreditRecords') }}</option>
                                    <option value="2">{{ Lang::get('public.DebitRecords') }}</option>
                                </select>
                            </label>
                        </div>
                        <div class="clr"></div>
                    </div>

                    <div class="wallRowL">
                        <div class="wallRowLTxt">{{ Lang::get('public.DateRange') }}</div>
                        <div class="wallRowLselects">
                            <input type="text" class="datepicker" id="trans_history_date_from" style="cursor:pointer;" value="{{ date('Y-m-d') }}"><br>
                            <div class="a1">
                                -
                            </div>
                        </div>
                        <div class="wallRowLselects lft1">
                            <input type="text" class="datepicker" id="trans_history_date_to" style="cursor:pointer;" value="{{ date('Y-m-d') }}">
                        </div>
                        <div class="clr"></div>
                    </div>

                    <div class="wallSubmit">
                        <a id="trans_history_button" href="javascript:void(0);" onClick="transaction_history();"> {{ Lang::get('public.Submit') }}</a>
                    </div>
                </div>

                <div class="tHist">
                    <div class="tHead">
                        <table id="trans_history" width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <th>{{Lang::get('public.ReferenceNo')}}</th>
                                <th>{{Lang::get('public.DateOrTime')}}</th>
                                <th>{{Lang::get('public.Type')}}</th>
                                <th>{{Lang::get('public.Amount')}}({{Session::get('currency')}})</th>
                                <th>{{Lang::get('public.Status')}}</th>
                                <th>{{Lang::get('public.Reason')}}</th>
                            </tr>
                            <tr>
                                <td colspan="6"><h2><center>{{ Lang::get('public.NoDataAvailable') }}</center></h2></td>
                            </tr>
                        </table>
                    </div>
                </div>

                @include('royalewin.include.balance_right')
            </div>
            
            <h3>{{ Lang::get('public.BetHistory') }}</h3>
            <div>
                <div class="tbLeft">
                    <h2>{{ Lang::get('public.BetHistory') }}</h2>
                    <div class="wallRowL">
                        <div class="wallRowLTxt">{{ Lang::get('public.DateRange') }}</div>
                        <div class="wallRowLselects">
                            <input type="text" class="datepicker" id="bet_history_date_from" style="cursor:pointer;" value="{{ date('Y-m-d') }}"><br>
                            <div class="a1">
                                -
                            </div>
                        </div>
                        <div class="wallRowLselects lft1">
                            <input type="text" class="datepicker" id="bet_history_date_to" style="cursor:pointer;" value="{{ date('Y-m-d') }}">
                        </div>
                        <div class="clr"></div>
                    </div>

                    <div class="wallSubmit">
                        <a id="bet_history_button" href="javascript:void(0);" onClick="bet_history();"> {{ Lang::get('public.Submit') }}</a>
                    </div>
                </div>

                <div class="tHist" style="top: 160px;">
                    <div class="tHead">
                        <table id="bet_history" width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <th>{{Lang::get('public.Date')}}</th>
                                <th>{{Lang::get('public.Wager')}}</th>
                                <th>{{Lang::get('public.Stake')}} </th>
                                <th>{{Lang::get('public.WinLose')}}</th>
                                <th>{{Lang::get('public.RealBet')}}</th>
                            </tr>
                            <tr>
                                <td colspan="6"><h2><center>{{ Lang::get('public.NoDataAvailable') }}</center></h2></td>
                            </tr>
                        </table>
                        <br><br>
                        <table id="bet_history_product" width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <th>{{Lang::get('public.Product')}}</th>
                                <th>{{Lang::get('public.Wager')}}</th>
                                <th>{{Lang::get('public.Stake')}} </th>
                                <th>{{Lang::get('public.WinLose')}}</th>
                                <th>{{Lang::get('public.RealBet')}}</th>
                            </tr>
                            <tr>
                                <td colspan="6"><h2><center>{{ Lang::get('public.NoDataAvailable') }}</center></h2></td>
                            </tr>
                        </table>
                        <br><br>
                    </div>
                </div>

                @include('royalewin.include.balance_right')
            </div>
        </div>
    </div>
</div>
@endsection