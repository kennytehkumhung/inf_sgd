<!doctype html>
<html>
    <head>

        @if (Session::get('currency') == 'MYR')
            <title>ROYALEWIN: Sky3888 and SCR888 Slot Games</title>
            <meta name="keywords" content="Sky3888 and SCR888 Slot Games"/>
            <meta name="description" content="Play Sky3888 and SCR888 Online Slots at RoyaleWin Online Casino Malaysia and claim a FREE SPIN bonus today!"/>
        @elseif (Session::get('currency') == 'IDR')
            <title>ROYALEWIN: Situs Judi Sky3888 & SCR888 Online Slot Indonesia</title>
            <meta name="keywords" content="Situs Judi Sky3888 & SCR888 Online Slot Indonesia"/>
            <meta name="description" content="RoyaleWin Agen Judi Slot Online menyediakan permainan Sky3888 and SCR888 slots dan taruhan online terpercaya dan terbaik di Indonesia."/>
        @endif

        <link href="{{ asset('/royalewin/resources/css/style.css') }}" rel="stylesheet" type="text/css" />
        
        <style type="text/css">
            .slot_box {
                background-color: #000;
                width: 150px; 
                height: 202px;
            }
            
            .slot_box img {
                width: 150px; 
                height: 150px;
            }
        </style>
    </head>
    <body style="background-color: transparent; overflow: hidden;">
        <div class="slotContainerBottom">
            <div class="slotMenuHolder">
                <div class="slot_menu">
                    <ul>
                        <li><a href="{{route('sky', [ 'type' => 'slot' , 'category' => 'singleplayer_games' ] )}}">Slots (85)</a></li>
                        <li><a href="{{route('sky', [ 'type' => 'slot' , 'category' => 'online_casino' ] )}}">Table Games (4)</a></li>
                        <li><a href="{{route('sky', [ 'type' => 'slot' , 'category' => 'multiplayer_p2p' ] )}}">Multiplayer (2)</a></li>
                        {{--<li><a href="{{route('sky', [ 'type' => 'slot' , 'category' => 'online_games' ] )}}">Arcades (1)</a></li>--}}
                    </ul>
                </div>
            </div>

            <div id="slot_lobby">
                
                @foreach( $lists as $list )
                     <div class="slot_box">
                        <a href="javascript:void(0)" onclick="@if (Auth::user()->check())window.open('{{route('sky-slot', array('gamecode' => $list['game_code']))}}','sky_slot','width=1000,height=750')@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif">
                            <img src="{{ $list['img_url'] }}" width="150" height="150" alt=""/>
                        </a>
                        <span>{{ $list['game_name'] }}</span>
                     </div>			 
                @endforeach

                <div class="clr"></div>
            </div>

            <div class="clr"></div>
        </div>
    </body>
</html>