@extends('royalewin/master')

@if (Session::get('currency') == 'MYR')
    @section('title', 'Best Online Live Casino Malaysia - Password')
    @section('keywords', 'Best Online Live Casino Malaysia - Password')
    @section('description', 'Forgot your password? Please provide the username or email address that you used when you signed up for your RoyaleWin Online Casino Malaysia account.')
@elseif (Session::get('currency') == 'IDR')
    @section('title', 'Bandar Casino Online, Situs Judi Indonesia - Password')
    @section('keywords', 'Bandar Casino Online, Situs Judi Indonesia - Password')
    @section('description', 'Password - RoyaleWin adalah agen bola, bandar casino online dan situs judi online terpercaya untuk Anda penggemar taruhan bola dan pecinta judi online di Indonesia.')
@endif

@section('js')
@parent

<script type="text/javascript">
        function forgotpassword_submit(){
            $.ajax({
            type: "POST",
            url: "{{route('resetpassword')}}",
            data: {
                _token: "{{ csrf_token() }}",
                username: $('#fg_username').val(),
                email: $('#fg_email').val()
            }
            }).done(function (json) {
                $('.acctTextReg').html('');
                obj = JSON.parse(json);
                var str = '';
                $.each(obj, function (i, item) {
                    if ('{{Lang::get('COMMON.SUCESSFUL')}}' == item) {
                        window.location.href = "{{route('homepage')}}";
                    } else {
                        $('.' + i + '_acctTextReg').html(item);
                    }
                });
            });
        }
</script>
@endsection

@section('content')
<div class="midSect bgOthers">
    <div class="contOthers">
        <span style="width: auto;">{{ strtoupper(Lang::get('public.ForgotPassword')) }}</span>
    </div>
    <div class="othersCont">
        <P>Please Contact Live Chat! <a href="#" onclick="livechat()" style="color: #f5f015;">Click here.</a></P>
        <div class="clr"></div>
    </div>
</div>
@endsection