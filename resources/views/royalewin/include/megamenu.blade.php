@section('js')
@parent

<script type="text/javascript">
    function autoSelectGame(obj) {
        var linkObj = $(obj);
        var childLinkObj = $("a:eq(0)", linkObj.next(".mega-menu"));

        if (childLinkObj.length) {
            if (typeof childLinkObj.attr("onclick") != "undefined") {
                return childLinkObj.attr("onclick");
            } else {
                window.location.href = childLinkObj.attr("href");
            }
        }
    }
</script>

@if (Lang::getLocale() == 'id')
    <style type="text/css">
        .menu-wrapper .nav a { padding: 0 13px; }
    </style>
@endif
@endsection

<div class="menu-wrapper" role="navigation">
    <ul class="nav" role="menubar">
        <li role="menuitem"><a href="{{ route('homepage') }}"><img class="homeIcon" src="{{ static_asset('/img/nav-home-icon.png') }}"></a></li>
<!--        <li role="menuitem"><a href="{{ route('switchwebtype') }}?type=m"><img class="homeIcon" src="{{ static_asset('/img/switch-mobile.png') }}"></a></li>-->
        <li role="menuitem">
            <!--<img src="{{url()}}/front/img/new.png" style="position:absolute;margin-top:-20px;margin-left:34px;">-->
            <a href="{{ route('livecasino') }}">{{ Lang::get('public.LiveCasino') }}</a>
            <div class="mega-menu" aria-hidden="true" role="menu">
                {{--@if(in_array('OPU',Session::get('valid_product')))--}}
                    {{--<div class="nav-column">--}}
                        {{--<h3><a href="#" onclick="{{ Auth::user()->check() ? 'window.open("'.route('opu' , [ 'type' => 'casino' , 'category' => 'live']).'","casino_gp","width=1146,height=866");' : 'alert("'.Lang::get('COMMON.PLEASELOGIN').'");' }}"><img src="{{ static_asset('/img/nav-op-casino.png') }}"></a></h3>--}}
                    {{--</div>--}}
                {{--@endif--}}
                @if(in_array('ALB',Session::get('valid_product')))
                    <div class="nav-column">
                        <h3><a href="#" onclick="{{ Auth::user()->check() ? 'window.open("'.route('alb').'","casino_gp","width=1146,height=866");' : 'alert("'.Lang::get('COMMON.PLEASELOGIN').'");' }}"><img src="{{ static_asset('/img/nav-allbet-casino.png') }}" style="height: 93px; width: 330px;"></a></h3>
                    </div>
                @endif
                @if(in_array('MXB',Session::get('valid_product')))
                    <div class="nav-column">
                        <h3><a href="#" onclick="{{ Auth::user()->check() ? 'window.location.href="'.route('mxb', [ 'type' => 'casino' , 'category' => 'baccarat' ]).'";' : 'alert("'.Lang::get('COMMON.PLEASELOGIN').'");' }}"><img src="{{ static_asset('/img/nav-euro-casino.png') }}"></a></h3>
                    </div>
                @endif
                @if(in_array('AGG',Session::get('valid_product')))
                    <div class="nav-column">
                        <h3><a href="#" onclick="{{ Auth::user()->check() ? 'window.open("'.route('agg').'","casino_ag","width=1150,height=830");' : 'alert("'.Lang::get('COMMON.PLEASELOGIN').'");' }}"><img src="{{ static_asset('/img/nav-crown-casino.png') }}"></a></h3>
                    </div>
                @endif
				@if(in_array('PLT',Session::get('valid_product')))
                    <div class="nav-column">
                        <h3><a href="#" onclick="{{ Auth::user()->check() ? 'window.open("'.route('pltiframe').'","casino_pt","width=1150,height=830");' : 'alert("'.Lang::get('COMMON.PLEASELOGIN').'");' }}"><img src="{{ static_asset('/img/nav-pt-casino.png') }}"></a></h3>
                    </div>
                @endif
				/*  */
				@if(Session::get('currency')=='MYR')
				@if(in_array('WMC',Session::get('valid_product')))
                    <div class="nav-column">
                        <h3><a href="#" onclick="{{ Auth::user()->check() ? 'window.open("'.route('wmcframe').'","casino_wm","width=1150,height=830");' : 'alert("'.Lang::get('COMMON.PLEASELOGIN').'");' }}"><img src="{{ asset('/royalewin/img/nav-wm-casino.png')}}"></a></h3>
                    </div>
                @endif
				@endif
				/*  */
                @if(in_array('VGS',Session::get('valid_product')))
                    <div class="nav-column">
                        <h3><a href="#" onclick="{{ Auth::user()->check() ? 'window.open("'.route('vgs', ['type' => 'casino']).'","casino_eg","width=1042,height=880");' : 'alert("'.Lang::get('COMMON.PLEASELOGIN').'");' }}"><img src="{{ static_asset('/img/nav-eg-casino.png') }}"></a></h3>
                    </div>
                @endif
                @if(in_array('HOG',Session::get('valid_product')))
                    <div class="nav-column">
                        <h3><a href="#" onclick="{{ Auth::user()->check() ? 'window.open("'.route('hog').'","casino_as","width=1150,height=830");' : 'alert("'.Lang::get('COMMON.PLEASELOGIN').'");' }}"><img src="{{ static_asset('/img/nav-asia-casino.png') }}"></a></h3>
                    </div>
                @endif
            </div>
        </li>
        <li role="menuitem">
            <!--<img src="{{url()}}/front/img/new.png" style="position:absolute;margin-top:-20px;margin-left:34px;">-->
            <a href="{{ route('slot', ['type' => 'mxb']) }}"  class="al_slot">{{ Lang::get('public.SlotGames') }}</a>
            <div class="mega-menu abs1" aria-hidden="true" role="menu">
				@if(in_array('PLT',Session::get('valid_product')))
                    <div class="nav-column">
                        <h3><a href="{{ route('slot', ['type' => 'plt']) }}"><img src="{{ static_asset('/img/nav-pt-slot.png') }}"></a></h3>
                    </div>
                @endif
                @if(in_array('MXB',Session::get('valid_product')))
                    <div class="nav-column">
                        <h3><a href="{{ route('slot', ['type' => 'mxb']) }}"><img src="{{ static_asset('/img/nav-3d-slot.png') }}"></a></h3>
                    </div>
                @endif
				@if(in_array('SCR',Session::get('valid_product')) && Session::get('currency') == 'MYR')
                    <div class="nav-column">
                        <h3><a href="{{ route('mobile') }}"><img src="{{ static_asset('/img/nav-item-scr.png') }}" style=" width: 330px;height: 99px;"></a></h3>
                    </div>
                @endif
                {{--@if(in_array('OPU',Session::get('valid_product')))--}}
                    {{--<div class="nav-column">--}}
                        {{--<h3><a href="{{ route('slot', ['type' => 'opu']) }}"><img src="{{ static_asset('/img/nav-gp-slot.png') }}"></a></h3>--}}
                    {{--</div>--}}
                {{--@endif--}}
                @if(in_array('SPG',Session::get('valid_product')))
                    <div class="nav-column">
                        <h3><a href="{{ route('slot', ['type' => 'spg']) }}"><img src="{{ static_asset('/img/nav-spg-slot.png') }}"></a></h3>
                    </div>
                @endif
                @if(in_array('JOK',Session::get('valid_product')))
                    <div class="nav-column">
                        <h3><a href="javascript:void(0);" onclick="@if (Auth::user()->check())window.open('{{route('jok')}}','jok_slot','width=1000,height=750')@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif"><img src="{{ static_asset('/img/nav-item-joker.PNG') }}"></a></h3>
                    </div>
                @endif
                @if(in_array('SKY',Session::get('valid_product')))
                    <div class="nav-column">
                        <h3><a href="{{ route('slot', ['type' => 'sky']) }}"><img src="{{ static_asset('/img/nav-item-sky.PNG') }}"></a></h3>
                    </div>
                @endif
             
            </div>
        </li>
        @if(in_array('AGG',Session::get('valid_product')) && Session::get('currency') == 'MYR')
            <li role="menuitem">
                <img src="{{url()}}/front/img/hot.png" style="position:absolute;margin-top:-20px;margin-left:34px;">
                <a href="#" onclick="{{ Auth::user()->check() ? 'window.open("'.route('agg', array('gametype' => '6')).'","casino_ag","width=1150,height=830");' : 'alert("'.Lang::get('COMMON.PLEASELOGIN').'");' }}">{{ Lang::get('public.FishingWorld') }}</a>
            </li>
        @endif
        <li role="menuitem">
            <a href="{{ route('ibc') }}" class="al_sportsbook">{{ Lang::get('public.SportsBook') }}</a>
            <div class="mega-menu abs2" aria-hidden="true" role="menu">
                @if(in_array('IBCX',Session::get('valid_product')))
                    <div class="nav-column">
                        <h3><a href="{{ route('ibc') }}"><img src="{{ static_asset('/img/nav-i-sport.png') }}"></a></h3>
                    </div>
                @endif
                @if(in_array('TBS',Session::get('valid_product')))
                    <div class="nav-column">
                        <h3><a href="{{ route('tbs') }}"><img src="{{ static_asset('/img/nav-w-sport.png') }}"></a></h3>
                    </div>
                @endif
            </div>
        </li>
        @if (Session::get('currency') == 'MYR')
            <li role="menuitem"><a href="{{ route('ctb') }}?page=intro">{{ Lang::get('public.Racebook') }}</a></li>
        @endif
        <li role="menuitem">
            @if (Session::get('currency') == 'MYR')
                <a href="{{ route('4d') }}">4D</a>
            @endif
        </li>
		@if (Session::get('currency') == 'IDR')
        <li role="menuitem">
	<img src="{{url()}}/front/img/new.png" style="position:absolute;margin-top:-20px;margin-left:34px;">
	<a href="{{ route('s128') }}">{{ Lang::get('public.CockFighting') }}</a></li>
        @endif
{{--        <li role="menuitem"><a href="{{ route('spg', [ 'type' => 'slot' , 'category' => 'slot' ] ) }}">{{ Lang::get('public.MobileSlot') }}</a></li>--}}
        <li role="menuitem"><a href="{{ route('promotion') }}">{{ Lang::get('public.Promotion') }}</a></li>
		@if(Session::get('currency')=='MYR')
		<img src="{{url()}}/royalewin/img/icon-ball.png" style="width:20px;position:absolute;margin-top:-11px;margin-left:15px;">
		<li role="menuitem " ><a href="javascript:void(0);" onclick="@if(Auth::user()->check()) window.location.href='{{ route('premier-league') }}';@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}');@endif">EPL</a></li>
		@endif
	 
    </ul>
</div>

@if (!Auth::user()->check())
    <div class="joinBtn"><a href="{{ route('register') }}">{{ Lang::get('public.JoinNow') }}</a></div>
@endif