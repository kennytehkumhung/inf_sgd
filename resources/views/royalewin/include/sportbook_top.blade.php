@section('js')
@parent

<script type="text/javascript">
    $(function () {
        var showBoxesMin = 11;
        var showBoxesMax = 12;
        
        for (var i = showBoxesMin; i <= showBoxesMax; i++) {
            $(".showbox" + i).hover(function() {
                $(this).addClass('transition');
            }, function() {
                $(this).removeClass('transition');
            });
        }
    });
</script>
@endsection

<div class="cont3" style="{{ Lang::getLocale() == 'cn' ? 'background-image: url('.asset('/royalewin/img/sp-cont-ch.PNG').');' : '' }}">
    @if(in_array('IBCX',Session::get('valid_product')))
        <div class="spI">
            <a href="{{ route('ibc') }}"><div class="showbox11"></div></a>
        </div>
    @endif
    @if(in_array('TBS',Session::get('valid_product')))
        <div class="spW">
            <a href="{{ route('tbs') }}"><div class="showbox12"></div></a>
        </div>
    @endif

    <div class="clr"></div>
</div>