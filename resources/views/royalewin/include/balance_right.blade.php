@section('js')
@parent

<script type="text/javascript">
    var transferringToMain = false;
    
    function transferToMain() {
        if (!transferringToMain) {
            if (confirm("{{ Lang::get('public.AreYouSureYouWanToTransferAllGameBalancesToMainWallet') }}")) {
                transferringToMain = true;
                
                $(".transfer_to_main_icon").hide();
                $(".transfer_to_main_loading").show();
                
                var gameBalances = {};
                var lastGameCode = "";
                
                $(".transferable_game_balance").each(function () {
                    try {
                        var gameCode = $(this).data("game-code");
                        var gameBalance = parseFloat($(this).val());

                        if (!isNaN(gameBalance) && gameBalance > 0) {
                            gameBalances[gameCode] = gameBalance;
                            lastGameCode = gameCode;
                        }
                    } catch (e) {
                        // Do nothing.
                    }
                });
                
                if (lastGameCode != "") {
                    for (var key in gameBalances) {
                        if (gameBalances.hasOwnProperty(key)) {
                            submitTransfer(key, gameBalances[key], lastGameCode);
                        }
                    }
                } else {
                    resetTransferStatus();
                }
            }
        }
    }
    
    function submitTransfer(gameCode, gameBalance, lastGameCode) {
        $.ajax({
            type: "POST",
            url: "{{route('transfer-process')}}",
            data: {
				_token: "{{ csrf_token() }}",
                amount: gameBalance,
                from: gameCode,
                to: "MAIN"
            },
            complete: function (jqXHR, textStatus) {
                if (gameCode == lastGameCode) {
                    resetTransferStatus();
                }
            }
        });
    }
    
    function resetTransferStatus() {
        transferringToMain = false;

        $(".transfer_to_main_loading").hide();
        $(".transfer_to_main_icon").show();

        getBalance(true);
    }
</script>
@endsection

<div class="tbRight">
    <h2>{{ Lang::get('public.Currentbalance') }}</h2>
    <div class="wallRow">
        <div class="wallRowTxt">{{ Lang::get('public.MainWallet') }}</div>
        <div class="wallRowAmt"><input class="main_wallet" type="text" value="0.00" style="text-align: right;" readonly /></div>
        <div class="clr"></div>
    </div>
	<?php $count = 0; ?>
    @foreach( Session::get('products_obj') as $prdid => $object)
        <?php
        
        $showPrd = true;
        if (Session::get('currency') == 'IDR' && ($object->code == 'PSB' || $object->code == 'OPU' || $object->code == 'SCR')) {
            $showPrd = false;
        }elseif (Session::get('currency') == 'MYR' && ($object->code == 'FBL' || $object->code == 'ISN' || $object->code == 'S128')) {
            $showPrd = false;
        }
        ?>
        @if ($showPrd)
            <div class="wallRow">
                <div class="wallRowTxt">{{$object->name}}</div>
                <div class="wallRowAmt">
                    <div style="position: relative;">
                        <input class="{{$object->code}}_balance transferable_game_balance" data-game-code="{{$object->code}}" type="text" value="0.00" style="text-align: right;" readonly />
                        <img class="{{$object->code}}_balance_loading" style="position: absolute; top: 4px; left: 8px; display: none; width: 20px; height: 20px;" src="{{ asset('/front/img/ajax-loading.gif') }}">
                    </div>
                    <div class="clr"></div>
                </div>
                <div class="clr"></div>
            </div>
        @endif
    @endforeach
    <div class="wallRowBt">
        <a href="javascrpit:void(0);" onclick="transferToMain();">
            <div class="tmImg">
                <img class="transfer_to_main_icon" src="{{ asset('/royalewin/img/tm-icon.jpg') }}" width="28" height="28" />
                <img class="transfer_to_main_loading" src="{{ asset('/front/img/ajax-loading.gif') }}" width="28" height="28" style="display: none;" />
            </div>
            <div class="tm">{{ Lang::get('public.TransferToMain') }}</div>
            <div class="clr"></div>
        </a>
    </div>
</div>