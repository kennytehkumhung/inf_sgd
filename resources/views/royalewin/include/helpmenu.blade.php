<ul>
    <li><a href="{{ route('faq') }}">{{ Lang::get('public.Help') }}</a></li>
    <li><a href="{{ route('aboutus') }}">{{ Lang::get('public.AboutUs') }}</a></li>
    <li><a href="{{ route('contactus') }}">{{ Lang::get('public.ContactUs') }}</a></li>
    <li><a href="{{ route('faq') }}">{{ Lang::get('public.FAQ') }}</a></li>
    <li><a href="{{ route('howtojoin') }}">{{ Lang::get('public.HowToJoin') }}</a></li>
    <li><a href="{{ route('tnc') }}">{{ Lang::get('public.TNC') }}</a></li>
    <li><a href="{{ route('banking') }}">{{ Lang::get('public.BankingInformation2') }}</a></li>
    <li><a href="{{ route('howtodeposit') }}">{{ Lang::get('public.HowToDeposit') }}</a></li>
</ul>