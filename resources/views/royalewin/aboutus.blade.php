@extends('royalewin/master')

@if (Session::get('currency') == 'MYR')
    @section('title', 'Best Online Live Casino Malaysia - About Us')
    @section('keywords', 'Best Online Live Casino Malaysia - About Us')
    @section('description', 'Learn about RoyaleWin Malaysia\'s Online Casino, a multiple award-winning online casino in Malaysia that gives you all the thrills you could possibly want from gaming online.')
@elseif (Session::get('currency') == 'IDR')
    @section('title', 'Bandar Casino Online, Situs Judi Indonesia - Tentang Kami')
    @section('keywords', 'Bandar Casino Online, Situs Judi Indonesia - Tentang Kami')
    @section('description', 'Tentang Kami - RoyaleWin adalah agen bola, bandar casino online dan situs judi online terpercaya untuk Anda penggemar taruhan bola dan pecinta judi online di Indonesia.')
@endif

@section('css')
@parent

<style type="text/css">
    .othersCont a {
        color: #fff;
    }
    
    .othersCont p {
        margin-bottom: 3px;
    }
</style>
@endsection

@section('content')
<div class="midSect bgOthers">
    <div class="contOthers">
        <span>{{ strtoupper(Lang::get('public.AboutUs')) }}</span>
    </div>
    <div class="othersMenu">
        @include('royalewin.include.helpmenu')
        <div class="clr"></div>
    </div>
    <div class="othersCont">
        {!! htmlspecialchars_decode($content) !!}
    </div>
</div>
@endsection