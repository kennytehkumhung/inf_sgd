<!doctype html>
<html>
    <head>
        <link href="{{ asset('/royalewin/resources/css/style.css') }}" rel="stylesheet" type="text/css" />
        
        <style type="text/css">
            .slot_box {
                background-color: #000;
                width: 150px; 
                height: 202px;
            }
            
            .slot_box img {
                width: 150px; 
                height: 150px;
            }
        </style>
    </head>
    <body style="background-color: transparent; overflow: hidden;">
        <div class="slotContainerBottom">
            <div class="slotMenuHolder">
                <div class="slot_menu">
                    <ul>
                        <li><a href="{{route('mxb', [ 'type' => 'slot' , 'category' => 'Slots' ] )}}">Slots (66)</a></li>
                        <li><a href="{{route('mxb', [ 'type' => 'slot' , 'category' => 'Table' ] )}}">Table(24)</a></li>
                        <li><a href="{{route('mxb', [ 'type' => 'slot' , 'category' => 'Soft Games' ] )}}">Mini (2)</a></li>
                        <li><a href="{{route('mxb', [ 'type' => 'slot' , 'category' => 'Video Poker' ] )}}">Video Poker (12)</a></li>
                    </ul>
                </div>
            </div>

            <div id="slot_lobby">
                @foreach( $lists as $list )
                <div class="slot_box">
                    <a href="javascript:void(0)" onclick="@if (Auth::user()->check())window.open('{{route('mxb-slot' , [ 'gameid' => $list['gameID'] ] )}}', 'mxb_slot', 'width=1150,height=830'); @else alert('{{Lang::get('COMMON.PLEASELOGIN')}}') @endif" >
                        <img src="{{$list['imageUrl']}}" alt=""/>
                    </a>
                    <span>{{$list['gameName']}}</span>
                </div>			 
                @endforeach

                <div class="clr"></div>
            </div>

            <div class="clr"></div>
        </div>
    </body>
</html>