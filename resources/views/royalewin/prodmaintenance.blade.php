@extends('royalewin/master')

@section('title', 'Maintenance')

@section('content')
<div class="midSect bgOthers">
    <div class="outer">
        <div class="msgBox1">
            <div class="excl">
                <img src="{{ asset('/royalewin/img/under-maintenance.png') }}" alt=""/></div>
            <span class="mnt">Website currently under maintenance</span>
            <span class="mnt1">Come Back Again Soon</span>
            <br><br>
            <div class="clr"></div>
        </div>
    </div>
</div>
@endsection