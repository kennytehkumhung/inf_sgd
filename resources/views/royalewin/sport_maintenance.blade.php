@extends('royalewin/master')

@section('title', 'SportsBook')

@section('content')
<div class="midSect bgSp">

    @include('royalewin.include.sportbook_top')

    <div class="spContainer">
			<div class="outer">
                <div class="msgBox1">
                    <div class="excl">
                    <img src="{{ asset('/royalewin/img/under-maintenance.png') }}" alt=""/></div>
                    <span class="mnt">Website currently under maintenance</span>
                    <span class="mnt1">Come Back Again Soon</span>
                    <div class="clr"></div>
                </div>
            </div>
    </div>
</div>
@endsection