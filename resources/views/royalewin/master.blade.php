<!doctype html>
<html>
    <head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />

    <meta name="keywords" content="@yield('keywords')"/>
    <meta name="description" content="@yield('description')"/>

    <link href="{{ static_asset('/img/favicon.ico') }}" rel="shortcut icon" type="image/icon" />
    <link href="{{ static_asset('/img/favicon.png') }}" rel="icon" type="image/icon" />
    <title>ROYALEWIN: @yield('title')</title>
	<!--[if IE 7]>
		<link rel="stylesheet" type="text/css" href="{{ static_asset('/resources/css/iefix.css') }}" />
	<![endif]-->

	<!--CSS -->
    <link href="{{ asset('/royalewin/resources/css/style.css') }}?v2" rel="stylesheet" type="text/css" />
    <link href="{{ static_asset('/resources/css/skitter.styles.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ static_asset('/resources/css/timetabs.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{url()}}/royalewin/resources/css/wc.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .tbl_social { width: auto; }
        .tbl_social td { vertical-align: middle; }
        h3.footerTitle { font-size: 12px; }
        .fbn_std { min-width: 150px; position: relative; }
    </style>

	<!--SCRIPT-->
	<script src="{{ static_asset('/resources/js/jquery-2.1.3.min.js') }}"></script>
	<!--<script src="{{ static_asset('/resources/js/jquery.tabSlideOut.v1.3.js') }}"></script>-->
    <script src="{{ static_asset('/resources/js/jquery.slidereveal.js') }}"></script>
	<script src="{{ static_asset('/resources/js/jquery.timetabs.min.js') }}"></script>
	<script src="{{ static_asset('/resources/js/jquery.easing.1.3.js') }}"></script>
	<script src="{{ static_asset('/resources/js/jquery.skitter.min.js') }}"></script>
    <script src="{{ static_asset('/resources/js/jquery.accTabs.js') }}"></script>
    <script src="{{url()}}/royalewin/resources/js/jquery.countdown.js"></script>
    <script src="{{ static_asset('/resources/js/jquery.megamenu.js') }}"></script>
	@if( Auth::user()->check() )
	<script src="{{ static_asset('/resources/js/jquery.dropdown.min.js') }}"></script>
	<link href="{{ static_asset('/resources/css/jquery.dropdown.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ static_asset('/resources/css/acctMngmt.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ static_asset('/resources/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ static_asset('/resources/css/jquery.bxslider.css') }}" type="text/css" media="all" rel="stylesheet" />
	@endif

	@yield('css')

    <style type="text/css">
        @if(Lang::getLocale() == 'cn')
            /* Fixes for CN language */
            body * { font-size: 14px; }
            .loginBtn a, .logoutBtn a { width: 26px !important; }
            .nav * { font-size: 18px !important; }
            .midSect *, .footer * { font-size: 14px !important; }
        @endif

        @if (Session::get('currency') == 'IDR')
            .menuAM { width: 45px; }
            .menuAM0 { width: 67px !important; }
            .menuAM1 { width: 58px !important; }
            .menuAM2 { width: 92px !important; }
        @endif
    </style>

    <style type="text/css">
        .box_skitter .prev_button,
        .box_skitter .next_button {
            display: none !important;
        }
        .isn-close-btn {
            position: absolute;
            z-index: 10001;
            top: 0;
            right: 0;
            cursor: pointer;
        }
    </style>
	<style>
	.closeBtn  {
	  cursor: pointer;
	  margin-right: -5px;
	  right: 0;
	  position: absolute;
	  z-index: 10000;
	}
	.closeBtn1  {
	  cursor: pointer;
	  margin-left: 175px;
	  left: 0;
	  position: fixed;
	  z-index: 10000;
	}

	#minimize4 {
	  position: fixed;
	  z-index: 9999;
	  top: 145px;
	  right: 1700px;
	  width: 117px;
	  height: 345px;
	}
	#minimize5 {
	  position: fixed;
	  z-index: 9999;
	  top: 420px;
	  left: 3px;
	}
	block1.a{
	  display:block;
	}
	block1.b{
	  display:none;
	}
	block2.a{
	  display:block;
	}
	block2.b{
	  display:none;
	}


	 </style>

	<!-- Init Skitter -->
	<script type="text/javascript" language="javascript">
		$(document).ready(function() {
			$('.box_skitter_large').skitter({
				theme: 'clean',
				numbers_align: 'center',
				progressbar: false,
				dots: true,
				preview: false
			});

            {{--$('.logo img').attr("src", "{{asset('/royalewin/img/header-logo-xmas.gif')}}");--}}
            {{--$('.midSect').css("background-image", "url({{asset('/royalewin/img/bg-large-xmas.png')}})");--}}
            {{--$('.sliderImg').css("background-image", "url({{asset('/royalewin/img/slider-img-1-xmas.png')}})");--}}
            {{--$('.sliderSelect').css("background-image", "url({{asset('/royalewin/img/slider-select-bg-raya.png')}})");--}}
		});
	</script>
	<script type="text/javascript">
		function MM_swapImgRestore() { //v3.0
			var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
		}
		function MM_preloadImages() { //v3.0
			var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
			var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
			if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
		}
		function MM_findObj(n, d) { //v4.01
			var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
			d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
			if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
			for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
			if(!x && d.getElementById) x=d.getElementById(n); return x;
		}
		function MM_swapImage() { //v3.0
			var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
			if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
		}
	</script>
	<script type="text/javascript">
        jQuery(function(){
			var SelfLocation = window.location.href.split('?');
			switch (SelfLocation[1]) {
				case "justify_right":
					jQuery(".megamenu").megamenu({ 'justify':'right' });
					break;
				case "justify_left":
				default:
					jQuery(".megamenu").megamenu();
					break;
			}
        });
	</script>
	<script type="text/javascript">
		function enterpressalert(e, textarea)
		{
			var code = (e.keyCode ? e.keyCode : e.which);
            if(code == 13) { //Enter keycode
              login();
            }
		}
        function login()
        {
            $("#login").hide();
            $("#loading").show();
			$.ajax({
                type: "POST",
				url: "{{route('login')}}",
				data: {
				_token: "{{ csrf_token() }}",
					username: $('#username').val(),
					password: $('#password').val(),
					code:     $('#code').val(),
				},
			}).done(function(json) {
				obj = JSON.parse(json);
				var str = '';
				$.each(obj, function(i, item) {
					str += item + '\n';
				});
				if ("{{Lang::get('COMMON.SUCESSFUL')}}\n" == str) {
//					location.reload();
                    //window.location.href = "{{route('homepage')}}?check=epl";
                    window.location.href = "{{route('homepage')}}";
                }else{
                    alert(str);
                    $("#login").show();
                    $("#loading").hide();
                }
			});
        }

        function login2() {
			$.ajax({
                type: "POST",
				url: "{{route('login')}}",
				data: {
                    _token: "{{ csrf_token() }}",
					username: $('#username2').val(),
					password: $('#password2').val(),
					code:     $('#code2').val(),
				},
			}).done(function(json) {
				obj = JSON.parse(json);
				var str = '';
				$.each(obj, function(i, item) {
					str += item + '\n';
				});
				if ("{{Lang::get('COMMON.SUCESSFUL')}}\n" == str){
					location.reload();
				} else {
					alert(str);
				}
			});
        }

        @if (Auth::user()->check())
            function update_mainwallet(){
				$.ajax({
					type: "POST",
					url: "{{route( 'mainwallet', [ '_token'=> csrf_token() ])}}",
					beforeSend: function(balance){
					},
					success: function(balance) {
                        var walletObj = $('.main_wallet');

                        if (walletObj.is("input[type='text']")) {
                            $('.main_wallet').val(parseFloat(Math.round(balance * 100) / 100).toFixed(2));
                        } else {
                            $('.main_wallet').html(parseFloat(Math.round(balance * 100) / 100).toFixed(2));
                        }
//						total_balance += parseFloat(balance);
					}
				});
            }
			function getBalance(type) {
				var total_balance = 0;
                var currency = "{{ Session::get('currency') }}";

				$.when(
					$.ajax({
                        type: "POST",
						url: "{{route('mainwallet', [ '_token'=> csrf_token()])}}",
						beforeSend: function(balance){
						},
						success: function(balance){
                            var walletObj = $('.main_wallet');

                            if (walletObj.is("input[type='text']")) {
                                $('.main_wallet').val(parseFloat(Math.round(balance * 100) / 100).toFixed(2));
                            } else {
                                $('.main_wallet').html(parseFloat(Math.round(balance * 100) / 100).toFixed(2));
                            }
							total_balance += parseFloat(balance);
						}
					}),
					@foreach(Session::get('products_obj') as $prdid => $object)

					  <?php

						$showPrd = true;
						if (Session::get('currency') == 'IDR' && ($object->code == 'PSB' || $object->code == 'OPU' || $object->code == 'CTB' || $object->code == 'SCR')) {
							$showPrd = false;
						}elseif (Session::get('currency') == 'MYR' && ($object->code == 'FBL' || $object->code == 'ISN' || $object->code == 'S128')) {
							$showPrd = false;
						}
					  ?>

					  @if ($showPrd)
						$.ajax({
							type: "POST",
							url: "{{route( 'getbalance', [ '_token'=> csrf_token(), 'product'=> $object->code ])}}&rd=<?php echo rand(10000, 99999); ?>",
							beforeSend: function(balance){
                                var balObj = $('.{{$object->code}}_balance');

                                if (balObj.is("input[type='text']")) {
                                    var balLoadingObj = $(".{{$object->code}}_balance_loading");

                                    if (balLoadingObj.length) {
                                        balLoadingObj.show();
                                    }
                                } else {
                                    balObj.html('<img style="" src="{{url()}}/front/img/ajax-loading.gif" width="20" height="10">');
                                }
							},
							success: function(balance){
                                var balObj = $('.{{$object->code}}_balance');

								if (balance != '{{Lang::get('Maintenance')}}') {
                                    if (balObj.is("input[type='text']")) {
                                        balObj.val(parseFloat(Math.round(balance * 100) / 100).toFixed(2));
                                    } else {
                                        balObj.html(parseFloat(Math.round(balance * 100) / 100).toFixed(2));
                                    }

                                    var skipTotal = false;
                                    if (currency == "IDR") {
                                        if ("{{$object->code}}" == "PSB" || "{{$object->code}}" == "OSG" || "{{$object->code}}" == "OPU" || "{{$object->code}}" == "SCR") {
                                            skipTotal = true;
                                        }
                                    } else {
                                        if ("{{$object->code}}" == "FBL" || "{{$object->code}}" == "S128") {
                                            skipTotal = true;
                                        }
                                    }

                                    if (!skipTotal) {
                                        total_balance += parseFloat(balance);
                                    }
								} else {
                                    if (balObj.is("input[type='text']")) {
                                        balObj.val(balance);
                                    } else {
                                        balObj.html(balance);
                                    }
								}

                                var balLoadingObj = $(".{{$object->code}}_balance_loading");

                                if (balLoadingObj.length) {
                                    balLoadingObj.hide();
                                }
							}
						})
						@if ($prdid != Session::get('last_prdid'))
						,
						@endif

						@endif
					@endforeach
				).then(function() {
                    var objs = $('#total_balance, #top_total_balance');

                    objs.each(function () {
                        var obj = $(this);
                        if (obj.is("input[type='text']")) {
                            obj.val(parseFloat(Math.round(total_balance * 100) / 100).toFixed(2));
                        } else {
                            obj.html(parseFloat(Math.round(total_balance * 100) / 100).toFixed(2));
                        }
                    });

                    $(".total_balance_loading").hide();
				});
			}
        @endif
	</script>

	<script type="text/javascript">
        var Tawk_API = Tawk_API || {}, Tawk_LoadStart = new Date();
        (function(){
            var s1 = document.createElement("script"), s0 = document.getElementsByTagName("script")[0];
            s1.async = true;
            s1.src = 'https://embed.tawk.to/5770f9b45bc185a150656677/1amanc4nh';
            s1.charset = 'UTF-8';
            s1.setAttribute('crossorigin', '*');
            s0.parentNode.insertBefore(s1, s0);
        })();
	</script>

    </head>
    <body>

    <div id="sample-1">
        <a class="handle" href="//link-for-non-js-users"></a>
        <div class="skype">Skype ID : <span>RoyaleWin</span></div>
        <div class="qrCon"><img src="{{ static_asset('/img/qr-contact.png') }}"></div>
        <div class="conJoin"><a href="javascript:void(0);"><img src="{{ static_asset('/img/contact-join-btn.png') }}"></a></div>
    </div>
    <button class="ctbtn ctbtn2" id="toggle-manually"><img src="{{ static_asset('/img/right-contact-btn.png') }}"></button>

	<!--Header-->
    <div class="header">
        <div class="headerInner">
            <div class="logo">
                <a href="{{ route('homepage') }}">
                    {{--<img src="{{ asset('/royalewin/img/logo2.png') }}">--}}
                    <img src="{{ asset('/royalewin/img/landing2/logo2.gif') }}">
                </a>
            </div>
            <div style="position: relative; display: inline-block;">
                <a href="{{ route('mobile') }}">
                    <img src="{{ static_asset('/img/mobile.png') }}" style="height: 86px; width: auto;">
                </a>
            </div>
            <div class="headerRight">
                @if (Auth::user()->check())
                    <div class="headerRightTable1 lg">
                        <table width="auto" border="0" cellspacing="0" cellpadding="0">
                            <tr>
								<td><div class="menuAM menuAM0"><a href="javascript:void(0);" onclick="inbox();">{{ Lang::get('public.Inbox')}}&nbsp;
									<b id="totalMessage"></b>
								</a></div></td>
                                <td><div class="menuAM menuAM0"><a href="{{ route('update-profile') }}?tab=3">{{ Lang::get('public.Transfer') }}</a></div></td>
                                <td><div class="menuAM"><a href="{{ route('update-profile') }}?tab=1">{{ Lang::get('public.Deposit') }}</a></div></td>
                                <td><div class="menuAM menuAM1"><a href="{{ route('update-profile') }}?tab=2">{{ Lang::get('public.Withdrawal') }}</a></div></td>
                                <td><div class="menuAM menuAM2"><a href="{{ route('update-profile') }}?tab=4">{{ Lang::get('public.TransactionHistory') }}</a></div></td>
                                <td><div class="menuAM menuAM2"><a href="{{ route('update-profile') }}?tab=5">{{ Lang::get('public.BetHistory') }}</a></div></td>
                                <td><div class="menuAM menuAM2"><a href="{{ route('update-profile') }}?tab=0">{{ Lang::get('public.ChangePassword') }}</a></div></td>
                            </tr>
                        </table>
                    </div>
                    <div class="logoutBtn"><a href="{{ route('logout') }}">{{ Lang::get('COMMON.LOGOUT') }}</a></div>
                @else
                    <div class="headerRightTable1">
                        <table width="auto%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td><input type="text" id="username" name="username" placeholder="{{ Lang::get('public.Username') }}" onKeyPress="enterpressalert(event, this)"></td>
                                <td><input type="password" id="password" name="password" placeholder="{{ Lang::get('public.Password') }}" onKeyPress="enterpressalert(event, this)"></td>
                                <!--<td><input type="text" id="code" name="code" placeholder="{{ Lang::get('public.Verify') }}" onKeyPress="enterpressalert(event, this)"></td>-->
                                <!--<td><img src="{{ route('captcha', ['type' => 'login_captcha']) }}" width="40" height="24"></td>-->
                            </tr>
                        </table>
                        <input type="hidden" id="code" name="code" value="0000" />
                    </div>
                    <div class="loginBtn" id="login"><a id="go" href="#" onclick="login();">{{ Lang::get('public.Login') }}</a></div>
                    <div class="loginBtn" id="loading" style="display:none;"><a id="go" href="#">{{ Lang::get('public.Loading') }}</a></div>
                    <div class="forgetP">
                        <a href="{{ route('forgotpassword') }}">{{ Lang::get('public.ForgotPassword') }}</a>
                    </div>
                @endif
            </div>
            <div class="headerRightTable">
                <table class="tbl_social" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <?php date_default_timezone_set("Asia/Kuala_Lumpur") ?>
                        <td valign="middle" id="sys_date">{{ date('d/m/Y, H:i') }} (GMT+8)</td>
                        <td valign="middle"><img src="{{ static_asset('/img/header-twit-icon.png') }}" alt=""/></td>
                        <td valign="middle"><img src="{{ static_asset('/img/header-g-icon.png') }}" alt=""/></td>
                        <td valign="middle"><a target="_blank" href="https://www.facebook.com/pages/Royalewin/114733715399238"><img src="{{ static_asset('/img/header-fb-icon.png') }}" alt=""/></a></td>
                        <td valign="middle" style="width: 84px; overflow: hidden;"><span style="" class="fb-like" data-href="https://www.facebook.com/pages/Royalewin/114733715399238" data-send="false" data-layout="button_count" data-width="50" data-show-faces="false"></span></td>
                        <td valign="middle"></td>
                        <td valign="middle"><img src="{{ static_asset('/img/header-contact-icon.png') }}" alt=""/></td>
                        <td valign="middle"><img src="{{ static_asset('/img/header-chat-icon.png') }}" alt=""/></td>

                        @if (Session::get('currency') == 'MYR')
                            <td valign="middle"><a href="{{ route( 'language', [ 'lang'=> 'en']) }}"><img src="{{ static_asset('/img/header-my-icon.png') }}" alt=""/></a></td>
                            <td valign="middle"><a href="{{ route( 'language', [ 'lang'=> 'cn']) }}"><img src="{{ static_asset('/img/header-cn-icon.png') }}" alt=""/></a></td>
                        @endif
                        <!--<td valign="middle"><a href="{{ route( 'language', [ 'lang'=> 'id']) }}"><img src="{{ static_asset('/img/header-id-icon.png') }}" alt=""/></a></td>-->
                    </tr>
                </table>
            </div>
            @if (Auth::user()->check())
                <div class="anmnt">
                    <div class="anmntInner" style="cursor: pointer;" onclick="window.open('{{route('announcement')}}?lang={{Lang::getLocale()}}', 'Notice', 'width=800,height=600');">
                        <span>{{ Lang::get('public.Notice') }} :</span>
                        <marquee>{{ App\Http\Controllers\User\AnnouncementController::index() }}</marquee>
                    </div>
                </div>
                <div class="clr"></div>
                <div class="memberST">
                    <span class="marg" onclick="window.location.href='{{ route('update-profile') }}?tab=0';" style="cursor: pointer;">{{ Lang::get('public.Username') }}: {{ Session::get('username') }}</span>
                    <img class="total_balance_loading" style="width: 12px; height: 12px;" src="{{ asset('/front/img/ajax-loading.gif') }}">
                    <span class="yellowed">{{ Lang::get('public.Balance') }}: <span id="top_total_balance">0.00</span></span>
                </div>
            @endif
        </div>
    </div>

    <!--Navigation-->
    <div class="navCont">
        <div class="navContInner">

            @include('royalewin.include.megamenu')

        </div>
    </div>

    <!--WC 18 Timer-->
<!--    <div class="wc">
        <div class="wcInner">
            <div class="timerWrap">
                <div id="getting-started" class="timerDig"></div>
            </div>
            <div class="wcWrap">
                <img src="{{url()}}/royalewin/img/wc-img.png">
            </div>

            <script type="text/javascript">
                $("#getting-started")
                .countdown("2018/06/14 23:00:00", function(event) {
                 $(this).text(
                   event.strftime('%D %H %M %S')
                 );
                 });
            </script>
        </div>
    </div>-->
    <!--WC 18 Timer-->

    <!--Mid Sect-->
    @yield('content')

    <!--Footer-->
    <div class="footer">
        <div class="footerInner">
            <div class="footerLeft">
                <ul>
                    <li class="nM"><a href="{{ route('aboutus') }}">{{ Lang::get('public.AboutUs') }}</a></li>
                    <li><a href="{{ route('contactus') }}">{{ Lang::get('public.ContactUs') }}</a></li>
                    <li><a href="{{ route('howtojoin') }}">{{ Lang::get('public.HowToJoin') }}</a></li>
                    <li><a href="{{ route('tnc') }}">{{ Lang::get('public.TNC') }}</a></li>
                    <li><a href="{{ route('howtodeposit') }}">{{ Lang::get('public.Tutorial') }}</a></li>
                    <li><a href="{{ route('switchwebtype') }}?type=m">{{ Lang::get('public.MobileSite') }}</a></li>
                    <li class="nM1"><a href="//affiliate.rwin888.com">Affiliate</a></li>
                </ul>
            </div>
            <div class="footerRight">
                <span>© 2016 ROYALEWIN All Right Reserved | 18+</span>
            </div>
            <div class="clr"></div>
            <div class="footerCenter">
                @if(Session::get('currency') == 'IDR')
                    <img src="{{ static_asset('/img/footer-banl-id.png') }}">
                @endif
            </div>
            <div class="footerLeft1">
                @if(Session::get('currency') == 'MYR')
                    <img src="{{ static_asset('/img/footer-vendor-my.png') }}">
                @else
                    <img src="{{ static_asset('/img/footer-vendor.png') }}">
                @endif
            </div>
            <div class="footerRight1">
                <ul>
                    <li class="nM"><img src="{{ static_asset('/img/footer-ie-icon.png') }}"></li>
                    <li><img src="{{ static_asset('/img/footer-ff-icon.png') }}"></li>
                    <li><img src="{{ static_asset('/img/footer-chrome-icon.png') }}"></li>
                    <li><img src="{{ static_asset('/img/footer-flash-icon.png') }}"></li>
                    <li><img src="{{ static_asset('/img/footer-twit-icon.png') }}"></li>
                    <li><a target="_blank" href="https://www.facebook.com/pages/Royalewin/114733715399238"><img src="{{ static_asset('/img/footer-fb-icon.png') }}"></a></li>
                </ul>
                <div class="clr"></div>
            </div>
            <div class="clr"></div>
            @yield('footer_seo');
            <div class="clr"></div>
        </div>
    </div>

    <!--<div class="slide-out-div">
        <a class="handle" href="//link-for-non-js-users"></a>
        <div class="skype">Skype ID : <span>RoyaleWin</span></div>
        <div class="qrCon"><img src="{{ static_asset('/img/qr-contact.png') }}"></div>
        <div class="conJoin"><a href="{{ route('contactus') }}"><img src="{{ static_asset('/img/contact-join-btn.png') }}"></a></div>
    </div>-->

    <div class="conChat" style="z-index: 10000;">
        <a href="javascript:void(0);" onclick="livechat();"><img src="{{ static_asset('/img/contact-livechat-icon.png') }}"></a>
    </div>

    <div class="conLink" style="z-index: 10000; {{ Session::get('currency') == 'MYR' ? '' : 'visibility: hidden;' }}">
        @if( Session::get('currency') == 'MYR' )
            <div class="fbn_std fbn_aff">
                @if (Lang::getLocale() == 'cn')
                <a href="//www.rwbola.com/my/christmas-promotion" target="_blank"><img src="{{ static_asset('/xmas/cn/img/icon-ch.gif') }}" style="width:150px;"></a>
                @else
                    <a href="//www.rwbola.com/my/christmas-promotion" target="_blank"><img src="{{ static_asset('/xmas/en/img/icon-.gif') }}" style="width:150px;"></a>
                @endif
                <img src="{{ static_asset('/img/isn_4d_close.png') }}" class="isn-close-btn" onclick="hideFloatingBanner('.fbn_aff');">
            </div>
            <br>

            <div class="fbn_std fbn_rywh">
                @if(Auth::user()->check())

                    <a href="{{ route('luckyspin') }}" target="_blank">
                        @if (Lang::getLocale() == 'cn')
                            <img src="{{ static_asset('/img/royalwh_cn.gif') }}" style="width: 150px;">
                        @else
                            <img src="{{ static_asset('/img/royalwh.gif') }}" style="width: 150px;">
                        @endif
                    </a>
                    {{--<br>--}}
                    {{--<block2 id="minimize3" class="a">--}}
                        {{--<div class="closeBtn" onclick="javascript:hideminimize3()" style="position: absolute;margin-left: 220px;">--}}
                            {{--<img src="{{url()}}/front/resources/img/closeBtn.png" onmouseover="this.src='{{url()}}/front/resources/img/closeBtn_hover.png'" onmouseout="this.src='{{url()}}/front/resources/img/closeBtn.png'">--}}
                        {{--</div>--}}
                        {{--<a href="{{route('year-end-promotion')}}" ><img src="{{ asset('royalewin/img/yearend/icon_'.Lang::getLocale().'.gif') }}"></a>--}}
                    {{--</block2>--}}
                @else

                    <a href="javascript:void(0);" onclick="alert('{{ Lang::get('COMMON.PLEASELOGIN') }}');">
                        @if (Lang::getLocale() == 'cn')
                            <img src="{{ static_asset('/img/royalwh_cn.gif') }}" style="width: 150px;">
                        @else
                            <img src="{{ static_asset('/img/royalwh.gif') }}" style="width: 150px;">
                        @endif
                    </a>
                    {{--<br>--}}
                    {{--<block2 id="minimize3" class="a">--}}
                        {{--<div class="closeBtn" onclick="javascript:hideminimize3()" style="position: absolute;margin-left: 220px;">--}}
                            {{--<img src="{{url()}}/front/resources/img/closeBtn.png" onmouseover="this.src='{{url()}}/front/resources/img/closeBtn_hover.png'" onmouseout="this.src='{{url()}}/front/resources/img/closeBtn.png'">--}}
                        {{--</div>--}}
                        {{--<a href="javascript:void(0);" onclick="alert('{{ Lang::get('COMMON.PLEASELOGIN') }}');" ><img src="{{ asset('royalewin/img/yearend/icon_'.Lang::getLocale().'.gif') }}"></a>--}}
                    {{--</block2>--}}
                @endif

                <img src="{{ static_asset('/img/isn_4d_close.png') }}" class="isn-close-btn" onclick="hideFloatingBanner('.fbn_rywh');">
            </div>

            <br>
        @endif
    </div>

	<style>
	.conLink2{
		  display: block;
		left: 3px;
		position: fixed;
		top:450px;
	}
	</style>




    <div class="conLink" style="z-index: 10000; {{ Session::get('currency') == 'IDR' ? '' : 'visibility: hidden;' }}">
        @if(Auth::user()->check())
            @if(!Session::has('master_royale_isn_html'))
                <?php
                Session::put('master_royale_isn_html', '<div class="fbn_std fbn_isn"><a href="'.route('isn').'" target="_blank"><img src="'.static_asset('/img/isn_4d.png').'" style="width: 150px;"></a><img src="'.static_asset('/img/isn_4d_close.png').'" class="isn-close-btn" onclick="hideFloatingBanner(\'.fbn_isn\');"></div>');
                ?>
            @endif

            {!! Session::get('master_royale_isn_html') !!}

            <div class="fbn_std fbn_rywh">
                <a href="{{ route('luckyspin') }}" target="_blank">
                    @if (Lang::getLocale() == 'cn')
                        <img src="{{ static_asset('/img/royalwh_cn.gif') }}" style="width: 150px;">
                    @else
                        <img src="{{ static_asset('/img/royalwh.gif') }}" style="width: 150px;">
                    @endif
                </a>

                <img src="{{ static_asset('/img/isn_4d_close.png') }}" class="isn-close-btn" onclick="hideFloatingBanner('.fbn_rywh');">
            </div>
        @else
            <div class="fbn_std fbn_isn"><a href="javascript:void(0);" onclick="alert('{{ Lang::get('COMMON.PLEASELOGIN') }}');"><img src="{{ static_asset('/img/isn_4d.png') }}" style="width: 150px;"></a><img src="{{ static_asset('/img/isn_4d_close.png') }}" class="isn-close-btn" onclick="hideFloatingBanner('.fbn_isn');"></div>

            <div class="fbn_std fbn_rywh">
                <a href="javascript:void(0);" onclick="alert('{{ Lang::get('COMMON.PLEASELOGIN') }}');">
                    @if (Lang::getLocale() == 'cn')
                        <img src="{{ static_asset('/img/royalwh_cn.gif') }}" style="width: 150px;">
                    @else
                        <img src="{{ static_asset('/img/royalwh.gif') }}" style="width: 150px;">
                    @endif
                </a>

                <img src="{{ static_asset('/img/isn_4d_close.png') }}" class="isn-close-btn" onclick="hideFloatingBanner('.fbn_rywh');">
            </div>
        @endif
    </div>


    <!-- Project template -->
	<script type="text/javascript">
	 function hideminimize3() {
        document.getElementById('minimize3').setAttribute('class', 'b');
    }
    function hideminimize4() {
        document.getElementById('minimize4').setAttribute('class', 'b');
    }
	function hideminimize5() {
        document.getElementById('minimize5').setAttribute('class', 'b');
    }

        $(function() {
@if (Auth::user()->check())
			load_message();
@endif
//            $('.slide-out-div').tabSlideOut({
//                tabHandle: '.handle',                              //class of the element that will be your tab
//                pathToTabImage: '{{ static_asset('/img/right-contact-btn.png') }}',          //path to the image for the tab (optionaly can be set using css)
//                imageHeight: '150px',                               //height of tab image
//                imageWidth: '61px',                               //width of tab image
//                tabLocation: 'right',                               //side of screen where tab lives, top, right, bottom, or left
//                speed: 300,                                        //speed of animation
//                action: 'click',
//                rightPos: '20px',                                 //options: 'click' or 'hover', action to trigger animation
//                topPos: '200px',                                   //position from the top
//                fixedPosition: false                               //options: true makes it stick(fixed position) on scroll
//             });

            $(function() {
                $('marquee').mouseover(function() {
                    this.stop();
                }).mouseout(function() {
                    this.start();
                });
            });

            var sample1 = $("#sample-1").slideReveal({
                trigger: $("#sample-1-btn")
            });

            $("#toggle-manually").click(function(){
                sample1.slideReveal("toggle");
            });

            $('dl#tabs4').addClass('enabled').timetabs({
                defaultIndex: 0,
                interval: 7000,
                continueOnMouseLeave: true,
                animated: 'fade',
                animationSpeed: 500
            });

            // animation preview
            $('input[name=animation]').click(function() {
                $this = $(this);
                $.fn.timetabs.switchanimation($this.val());
            });

            $('.box_skitter_large').skitter({
				theme: 'clean',
				numbers_align: 'center',
				progressbar: false,
				dots: false,
				preview: false,
				numbers: false
			});

            for (var i = 0; i <= 6; i++) {
                var objClass = ".showbox, .showbox" + (i <= 0 ? "" : i) + "{{ Lang::getLocale() == 'id' ? '-id' : '' }}";

                $(objClass).hover(function() {
                    $(this).addClass('transition');
                }, function() {
                    $(this).removeClass('transition');
                });
            }
        });

        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id))
                return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));

        function livechat() {
            window.open('https://chatserver.comm100.com/ChatWindow.aspx?planId=360&visitType=1&byHref=1&partnerId=-1&siteid=160856', '', 'width=525, height=520, top=150, left=250');
        }

        function hideFloatingBanner(selector) {
            $(selector).hide();
        }

        var sDate = new Date("{{date('c')}}");

        function updateTime() {
            sDate = new Date(sDate.getTime() + 1000);

            var month = parseFloat(sDate.getMonth()) + 1;
            var hour = parseFloat(sDate.getHours());
            var min = parseFloat(sDate.getMinutes());

            if (hour < 10)
                hour = "0" + hour;

            if (min < 10)
                min = "0" + min;

            if (month < 10)
                month = "0" + month;

            $("#sys_date").html(sDate.getDate() + "/" + month + "/" + sDate.getFullYear() + ", " + hour + ":" + min + " (GMT+8)");
        }

		function inbox() {
			window.open('{{route('inbox')	}}', '', 'width=800,height=600');
        }
@if (Auth::user()->check())
		function load_message() {
			$.ajax({
				url: "{{ route('showTotalUnseenMessage') }}",
				type: "GET",
				dataType: "text",
				data: {
				},
				success: function (result) {
					if(result>0){
						$("#totalMessage").html("<b style='color: red'>["+result+"]</b>");
					}else{
						$("#totalMessage").html("["+result+"]");
					}
				},
				error: function (XMLHttpRequest, textStatus, errorThrown) {
				}
			});
		}
        setInterval(load_message,20000);
@endif
        setInterval(updateTime, 1000);
	</script>

	@yield('js')

    @yield('bottom_js')

    @if (Auth::user()->check())
        <script type="text/javascript">
            $(function () {
                setTimeout(function () {
                    getBalance(true);
                }, 1000);
            });
        </script>
    @endif

    {{-- Update mobile\master.blade.php as well --}}
	@if($_SERVER['HTTP_HOST'] == 'rwinvip.com')

	<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function()
	{ (i[r].q=i[r].q||[]).push(arguments)}

	,i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	ga('create', 'UA-88529618-1', 'auto');
	ga('send', 'pageview');

	</script>

    @elseif($_SERVER['HTTP_HOST'] == 'rwin888.com')

    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-41261240-1', 'auto');
        ga('send', 'pageview');

    </script>

	@else

    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function()
                { (i[r].q=i[r].q||[]).push(arguments)}
                ,i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
        ga('create', 'UA-41261240-1', 'auto');
        ga('send', 'pageview');
    </script>

	@endif

    @if (Session::get('currency') == 'MYR')
        <script src="//my.rtmark.net/p.js?f=sync&lr=1&partner=3e24602a32457820b3d93b75fdd90cd91825dfa941197da6d5e7dd4530272db6" defer></script>
    @endif

    </body>
</html>
