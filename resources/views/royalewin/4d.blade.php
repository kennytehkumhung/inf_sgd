@extends('royalewin/master')

@if (Session::get('currency') == 'MYR')
    @section('title', 'Play Live 4D Malaysia Online Betting')
    @section('keywords', 'Play Live 4D Malaysia Online Betting')
    @section('description', 'Play Live 4D at RoyaleWin Online Casino Malaysia - awarded Best Online Casino and enjoy 110% Welcome Deposit Bonus. Join RoyaleWin casino online today.')
@endif

@section('css')
@parent

<link href="{{ asset('/royalewin/resources/css/leanmodal.css') }}" rel="stylesheet" type="text/css" />
<style type="text/css">
    .lmHolder {
        display: none;
        z-index: 9999 !important;
        position: absolute;
        right: 0;
        bottom: 0;
        left: 0;
        width: 560px;
        margin: auto;
        overflow: auto;
        background: #fff;
        border-radius: 5px;
        padding: 1em 2em;
        max-height: 455px !important;
        font-size: 11px;
    }
    
    .lmHolder table {
        border-collapse: collapse;
        width: 100%;
    }
    
    .lmHolder table, .lmHolder table th, .lmHolder table td {
        border: 1px solid black;
    }
    
    .lmHolder table tr:first-child td {
        font-weight: bold;
    }
    
    .lmHolder table td {
        padding: 6px 0;
        text-align: center;
        vertical-align: middle;
    }
    
    .btnClose {
        font-weight: bold;
        cursor: pointer;
        color: gray;
        font-size: 18px;
    }

    h1.title {
        font-size: 11px;
        padding: 0;
        margin: 0;
    }
</style>
@endsection

@section('js')
@parent

<script src="{{ asset('/royalewin/resources/js/jquery.leanModal.min.js') }}"></script>
<script type="text/javascript">
    $(function($) {
        $("a[rel*=leanModal]").leanModal({
            overlay : 0.7,
            closeButton: ".btnClose"
        });
    });
</script>
@endsection

@section('content')
<div class="midSect bg4d">
    <div class="cont4"></div>
    <div class="dContainerBottom">
        <div class="dContainerBottomInner">
            <!--magnum-->
            <div id="fD_table">
                <div id="fD_table_header" class="magnum">
                    <div id="fD_table_img">
                        <img src="{{ asset('/royalewin/img/4D_magnum.png') }}" width="80" height="40" />
                    </div>
                    <div id="fD_table_title">
                        <h1 class="title">MAGNUM 4D</h1>
                        <span>{{ $Magnum['date'] }}</span>
                    </div>
                </div>
                <div id="fD_table_result">
                    <table align="center" width="180" border="1" cellpadding="1" cellspacing="0" style="border-color:#999;">
                        <tr>
                            <td width="61" align="center" class="magnum fD_top3_description">1st</td>
                            <td width="61" align="center" class="magnum fD_top3_description">2nd</td>
                            <td align="center" class="magnum fD_top3_description">3rd</td>
                        </tr>
                        <tr>
                            <td align="center" class="fD_top3_no"><span>{{ $Magnum['first'] }}</span></td>
                            <td align="center" class="fD_top3_no"><span>{{ $Magnum['second'] }}</span></td>
                            <td align="center" class="fD_top3_no"><span>{{ $Magnum['third'] }}</span></td>
                        </tr>
                    </table>
                    <table align="center" width="180" border="1" cellpadding="1" cellspacing="0" class="fD_normalPrice">
                        <tr>
                            <td colspan="3" align="center" class="magnum">Special</td>
                        </tr>
                        <tr>
                            <td align="center"><span>{{ $Magnum['special'][0] }}</span></td>
                            <td align="center"><span>{{ $Magnum['special'][1] }}</span></td>
                            <td align="center"><span>{{ $Magnum['special'][2] }}</span></td>
                        </tr>
                        <tr>
                            <td align="center"><span>{{ $Magnum['special'][3] }}</span></td>
                            <td align="center"><span>{{ $Magnum['special'][4] }}</span></td>
                            <td align="center"><span>{{ $Magnum['special'][5] }}</span></td>
                        </tr>
                        <tr>
                            <td align="center"><span>{{ $Magnum['special'][6] }}</span></td>
                            <td align="center"><span>{{ $Magnum['special'][7] }}</span></td>
                            <td align="center"><span>{{ $Magnum['special'][8] }}</span></td>
                        </tr>
                        <tr>
                            <td align="center" bgcolor="#999">&nbsp;</td>
                            <td align="center"><span>{{ $Magnum['special'][9] }}</span></td>
                            <td align="center" bgcolor="#999">&nbsp;</td>
                        </tr>
                    </table>
                    <table align="center" width="180" border="1" cellpadding="1" cellspacing="0" class="fD_normalPrice">
                        <tr>
                            <td colspan="3" align="center" class="magnum">Consolation</td>
                        </tr>
                        <tr>
                            <td align="center"><span>{{ $Magnum['consolation'][0] }}</span></td>
                            <td align="center"><span>{{ $Magnum['consolation'][1] }}</span></td>
                            <td align="center"><span>{{ $Magnum['consolation'][2] }}</span></td>
                        </tr>
                        <tr>
                            <td align="center"><span>{{ $Magnum['consolation'][3] }}</span></td>
                            <td align="center"><span>{{ $Magnum['consolation'][4] }}</span></td>
                            <td align="center"><span>{{ $Magnum['consolation'][5] }}</span></td>
                        </tr>
                        <tr>
                            <td align="center"><span>{{ $Magnum['consolation'][6] }}</span></td>
                            <td align="center"><span>{{ $Magnum['consolation'][7] }}</span></td>
                            <td align="center"><span>{{ $Magnum['consolation'][8] }}</span></td>
                        </tr>
                        <tr>
                            <td align="center" bgcolor="#999">&nbsp;</td>
                            <td align="center"><span>{{ $Magnum['consolation'][9] }}</span></td>
                            <td align="center" bgcolor="#999">&nbsp;</td>
                        </tr>
                    </table>
                </div>
            </div>
            
            <!--end magnum-->
            <div class="dTxt">
                <div class="danimate">
                    <img src="{{ asset('/royalewin/img/4d.gif') }}">
                </div>
                <div class="BtnLeft">
                    <a href="#"></a>
                    <a href="#" onClick="@if (Auth::user()->check())window.open('{{route('psb')}}', 'pubContent', 'width=1050,height=650');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('betnow', '', '{{ asset("/royalewin/img/bet-now-hover.png") }}', 0)"><img src="{{ asset('/royalewin/img/bet-now.png') }}" name="betnow" width="225" height="81" border="0"></a>
                </div>
                <div class="boxRight">
                    <ul>
                        <li><a href="#lmRulesTable" rel="leanModal">RULES & REGULATIONS</a></li>
                        <li><a href="#lmPayoutTable" rel="leanModal">PAYOUT TABLE</a></li>
                    </ul>
                </div>
                <div class="clr"></div>
            </div>
            
            <!--damacai-->
            <div id="fD_table">
                <div id="fD_table_header" class="damacai">
                    <div id="fD_table_img">
                        <img src="{{ asset('/royalewin/img/4D_damacai.png') }}" width="80" height="40" />
                    </div>
                    <div id="fD_table_title">
                        <h1 class="title">DAMACAI 1+3D</h1>
                        <span>{{ $PMP['date'] }}</span>
                    </div>
                </div>
                <div id="fD_table_result">
                    <table width="180" border="1" cellpadding="1" cellspacing="0" style="border-color:#999;">
                        <tr>
                            <td width="61" align="center" class="damacai fD_top3_description">1st</td>
                            <td width="61" align="center" class="damacai fD_top3_description">2nd</td>
                            <td align="center" class="damacai fD_top3_description">3rd</td>
                        </tr>
                        <tr>
                            <td align="center" class="fD_top3_no"><span id="ContentPlaceHolder1_lbl_PMP_1st">{{ $PMP['first'] }}</span></td>
                            <td align="center" class="fD_top3_no"><span id="ContentPlaceHolder1_lbl_PMP_2nd">{{ $PMP['second'] }}</span></td>
                            <td align="center" class="fD_top3_no"><span id="ContentPlaceHolder1_lbl_PMP_3rd">{{ $PMP['third'] }}</span></td>
                        </tr>
                    </table>
                    <table width="180" border="1" cellpadding="1" cellspacing="0" class="fD_normalPrice">
                        <tr>
                            <td colspan="3" align="center" class="damacai">Special</td>
                        </tr>
                        <tr>
                            <td align="center"><span>{{ $PMP['special'][0] }}</span></td>
                            <td align="center"><span>{{ $PMP['special'][1] }}</span></td>
                            <td align="center"><span>{{ $PMP['special'][2] }}</span></td>
                        </tr>
                        <tr>
                            <td align="center"><span>{{ $PMP['special'][3] }}</span></td>
                            <td align="center"><span>{{ $PMP['special'][4] }}</span></td>
                            <td align="center"><span>{{ $PMP['special'][5] }}</span></td>
                        </tr>
                        <tr>
                            <td align="center"><span>{{ $PMP['special'][6] }}</span></td>
                            <td align="center"><span>{{ $PMP['special'][7] }}</span></td>
                            <td align="center"><span>{{ $PMP['special'][8] }}</span></td>
                        </tr>
                        <tr>
                            <td align="center" bgcolor="#999">&nbsp;</td>
                            <td align="center"><span>{{ $PMP['special'][9] }}</span></td>
                            <td align="center" bgcolor="#999">&nbsp;</td>
                        </tr>
                    </table>
                    <table width="180" border="1" cellpadding="1" cellspacing="0" class="fD_normalPrice">
                        <tr>
                            <td colspan="3" align="center" class="damacai">Consolation</td>
                        </tr>
                        <tr>
                            <td align="center"><span>{{ $PMP['consolation'][0] }}</span></td>
                            <td align="center"><span>{{ $PMP['consolation'][1] }}</span></td>
                            <td align="center"><span>{{ $PMP['consolation'][2] }}</span></td>
                        </tr>
                        <tr>
                            <td align="center"><span>{{ $PMP['consolation'][3] }}</span></td>
                            <td align="center"><span>{{ $PMP['consolation'][4] }}</span></td>
                            <td align="center"><span>{{ $PMP['consolation'][5] }}</span></td>
                        </tr>
                        <tr>
                            <td align="center"><span>{{ $PMP['consolation'][6] }}</span></td>
                            <td align="center"><span>{{ $PMP['consolation'][7] }}</span></td>
                            <td align="center"><span>{{ $PMP['consolation'][8] }}</span></td>
                        </tr>
                        <tr>
                            <td align="center" bgcolor="#999">&nbsp;</td>
                            <td align="center"><span>{{ $PMP['consolation'][9] }}</span></td>
                            <td align="center" bgcolor="#999">&nbsp;</td>
                        </tr>
                    </table>
                </div>
            </div>
            <!--end damacai-->
            
            <!--toto-->
            <div id="fD_table">
                <div id="fD_table_header" class="toto">
                    <div id="fD_table_img">
                        <img src="{{ asset('/royalewin/img/4D_toto.png') }}" width="80" height="40" />
                    </div>
                    <div id="fD_table_title">
                        <h1 class="title">TOTO 4D</h1>
                        <span>{{ $Toto['date'] }}</span>
                    </div>
                </div>
                <div id="fD_table_result">
                    <table width="180" border="1" cellpadding="1" cellspacing="0" style="border-color:#999;">
                        <tr>
                            <td width="61" align="center" class="toto fD_top3_description">1st</td>
                            <td width="61" align="center" class="toto fD_top3_description">2nd</td>
                            <td align="center" class="toto fD_top3_description">3rd</td>
                        </tr>
                        <tr>
                            <td align="center" class="fD_top3_no"><span>{{ $Toto['first'] }}</span></td>
                            <td align="center" class="fD_top3_no"><span>{{ $Toto['second'] }}</span></td>
                            <td align="center" class="fD_top3_no"><span>{{ $Toto['third'] }}</span></td>
                        </tr>
                    </table>
                    <table width="180" border="1" cellpadding="1" cellspacing="0" class="fD_normalPrice">
                        <tr>
                            <td colspan="3" align="center" class="toto">Special</td>
                        </tr>
                        <tr>
                            <td align="center"><span>{{ $Toto['special'][0] }}</span></td>
                            <td align="center"><span>{{ $Toto['special'][1] }}</span></td>
                            <td align="center"><span>{{ $Toto['special'][2] }}</span></td>
                        </tr>
                        <tr>
                            <td align="center"><span>{{ $Toto['special'][3] }}</span></td>
                            <td align="center"><span>{{ $Toto['special'][4] }}</span></td>
                            <td align="center"><span>{{ $Toto['special'][5] }}</span></td>
                        </tr>
                        <tr>
                            <td align="center"><span>{{ $Toto['special'][6] }}</span></td>
                            <td align="center"><span>{{ $Toto['special'][7] }}</span></td>
                            <td align="center"><span>{{ $Toto['special'][8] }}</span></td>
                        </tr>
                        <tr>
                            <td align="center" bgcolor="#999">&nbsp;</td>
                            <td align="center"><span>{{ $Toto['special'][9] }}</span></td>
                            <td align="center" bgcolor="#999">&nbsp;</td>
                        </tr>
                    </table>
                    <table width="180" border="1" cellpadding="1" cellspacing="0" class="fD_normalPrice">
                        <tr>
                            <td colspan="3" align="center" class="toto">Consolation</td>
                        </tr>
                        <tr>
                            <td align="center"><span>{{ $Toto['consolation'][0] }}</span></td>
                            <td align="center"><span>{{ $Toto['consolation'][1] }}</span></td>
                            <td align="center"><span>{{ $Toto['consolation'][2] }}</span></td>
                        </tr>
                        <tr>
                            <td align="center"><span>{{ $Toto['consolation'][3] }}</span></td>
                            <td align="center"><span>{{ $Toto['consolation'][4] }}</span></td>
                            <td align="center"><span>{{ $Toto['consolation'][5] }}</span></td>
                        </tr>
                        <tr>
                            <td align="center"><span>{{ $Toto['consolation'][6] }}</span></td>
                            <td align="center"><span>{{ $Toto['consolation'][7] }}</span></td>
                            <td align="center"><span>{{ $Toto['consolation'][8] }}</span></td>
                        </tr>
                        <tr>
                            <td align="center" bgcolor="#999">&nbsp;</td>
                            <td align="center"><span>{{ $Toto['consolation'][9] }}</span></td>
                            <td align="center" bgcolor="#999">&nbsp;</td>
                        </tr>
                    </table>
                </div>
            </div>
            <!--end toto-->
            
            <!--Singapore-->
            <div id="fD_table">
                <div id="fD_table_header" class="singapore">
                    <div id="fD_table_img">
                        <img src="{{ asset('/royalewin/img/4D_singapore.png') }}" width="80" height="40" />
                    </div>
                    <div id="fD_table_title">
                        <h1 class="title">SINGAPORE 4D</h1>
                        <span>{{ $Singapore['date'] }}</span>
                    </div>
                </div>
                <div id="fD_table_result">
                    <table width="180" border="1" cellpadding="1" cellspacing="0" style="border-color:#999;">
                        <tr>
                            <td width="61" align="center" class="singapore fD_top3_description">1st</td>
                            <td width="61" align="center" class="singapore fD_top3_description">2nd</td>
                            <td align="center" class="singapore fD_top3_description">3rd</td>
                        </tr>
                        <tr>
                            <td align="center" class="fD_top3_no"><span>{{ $Singapore['first'] }}</span></td>
                            <td align="center" class="fD_top3_no"><span>{{ $Singapore['second'] }}</span></td>
                            <td align="center" class="fD_top3_no"><span>{{ $Singapore['third'] }}</span></td>
                        </tr>
                    </table>
                    <table width="180" border="1" cellpadding="1" cellspacing="0" class="fD_normalPrice">
                        <tr>
                            <td colspan="3" align="center" class="singapore">Special</td>
                        </tr>
                        <tr>
                            <td align="center"><span>{{ $Singapore['special'][0] }}</span></td>
                            <td align="center"><span>{{ $Singapore['special'][1] }}</span></td>
                            <td align="center"><span>{{ $Singapore['special'][2] }}</span></td>
                        </tr>
                        <tr>
                            <td align="center"><span>{{ $Singapore['special'][3] }}</span></td>
                            <td align="center"><span>{{ $Singapore['special'][4] }}</span></td>
                            <td align="center"><span>{{ $Singapore['special'][5] }}</span></td>
                        </tr>
                        <tr>
                            <td align="center"><span>{{ $Singapore['special'][6] }}</span></td>
                            <td align="center"><span>{{ $Singapore['special'][7] }}</span></td>
                            <td align="center"><span>{{ $Singapore['special'][8] }}</span></td>
                        </tr>
                        <tr>
                            <td align="center" bgcolor="#999">&nbsp;</td>
                            <td align="center"><span>{{ $Singapore['special'][9] }}</span></td>
                            <td align="center" bgcolor="#999">&nbsp;</td>
                        </tr>
                    </table>
                    <table width="180" border="1" cellpadding="1" cellspacing="0" class="fD_normalPrice">
                        <tr>
                            <td colspan="3" align="center" class="singapore">Consolation</td>
                        </tr>
                        <tr>
                            <td align="center"><span>{{ $Singapore['consolation'][0] }}</span></td>
                            <td align="center"><span>{{ $Singapore['consolation'][1] }}</span></td>
                            <td align="center"><span>{{ $Singapore['consolation'][2] }}</span></td>
                        </tr>
                        <tr>
                            <td align="center"><span>{{ $Singapore['consolation'][3] }}</span></td>
                            <td align="center"><span>{{ $Singapore['consolation'][4] }}</span></td>
                            <td align="center"><span>{{ $Singapore['consolation'][5] }}</span></td>
                        </tr>
                        <tr>
                            <td align="center"><span>{{ $Singapore['consolation'][6] }}</span></td>
                            <td align="center"><span>{{ $Singapore['consolation'][7] }}</span></td>
                            <td align="center"><span>{{ $Singapore['consolation'][8] }}</span></td>
                        </tr>
                        <tr>
                            <td align="center" bgcolor="#999">&nbsp;</td>
                            <td align="center"><span>{{ $Singapore['consolation'][9] }}</span></td>
                            <td align="center" bgcolor="#999">&nbsp;</td>
                        </tr>
                    </table>
                </div>
            </div>
            <!--end singapore-->
            
            <!--t88-->
            <div id="fD_table">
                <div id="fD_table_header" class="t88">
                    <div id="fD_table_img">
                        <img src="{{ asset('/royalewin/img/4D_88.png') }}" width="80" height="40" />
                    </div>
                    <div id="fD_table_title">
                        <h1 class="title">SABAH 4D</h1>
                        <span>{{ $Sabah['date'] }}</span>
                    </div>
                </div>
                <div id="fD_table_result">
                    <table width="180" border="1" cellpadding="1" cellspacing="0" style="border-color:#999;">
                        <tr>
                            <td width="61" align="center" class="t88 fD_top3_description">1st</td>
                            <td width="61" align="center" class="t88 fD_top3_description">2nd</td>
                            <td align="center" class="t88 fD_top3_description">3rd</td>
                        </tr>
                        <tr>
                            <td align="center" class="fD_top3_no"><span>{{ $Sabah['first'] }}</span></td>
                            <td align="center" class="fD_top3_no"><span>{{ $Sabah['second'] }}</span></td>
                            <td align="center" class="fD_top3_no"><span>{{ $Sabah['third'] }}</span></td>
                        </tr>
                    </table>
                    <table width="180" border="1" cellpadding="1" cellspacing="0" class="fD_normalPrice">
                        <tr>
                            <td colspan="3" align="center" class="t88">Special</td>
                        </tr>
                        <tr>
                            <td align="center"><span>{{ $Sabah['special'][0] }}</span></td>
                            <td align="center"><span>{{ $Sabah['special'][1] }}</span></td>
                            <td align="center"><span>{{ $Sabah['special'][2] }}</span></td>
                        </tr>
                        <tr>
                            <td align="center"><span>{{ $Sabah['special'][3] }}</span></td>
                            <td align="center"><span>{{ $Sabah['special'][4] }}</span></td>
                            <td align="center"><span>{{ $Sabah['special'][5] }}</span></td>
                        </tr>
                        <tr>
                            <td align="center"><span>{{ $Sabah['special'][6] }}</span></td>
                            <td align="center"><span>{{ $Sabah['special'][7] }}</span></td>
                            <td align="center"><span>{{ $Sabah['special'][8] }}</span></td>
                        </tr>
                        <tr>
                            <td align="center" bgcolor="#999">&nbsp;</td>
                            <td align="center"><span>{{ $Sabah['special'][9] }}</span></td>
                            <td align="center" bgcolor="#999">&nbsp;</td>
                        </tr>
                    </table>
                    <table width="180" border="1" cellpadding="1" cellspacing="0" class="fD_normalPrice">
                        <tr>
                            <td colspan="3" align="center" class="t88">Consolation</td>
                        </tr>
                        <tr>
                            <td align="center"><span>{{ $Sabah['consolation'][0] }}</span></td>
                            <td align="center"><span>{{ $Sabah['consolation'][1] }}</span></td>
                            <td align="center"><span>{{ $Sabah['consolation'][2] }}</span></td>
                        </tr>
                        <tr>
                            <td align="center"><span>{{ $Sabah['consolation'][3] }}</span></td>
                            <td align="center"><span>{{ $Sabah['consolation'][4] }}</span></td>
                            <td align="center"><span>{{ $Sabah['consolation'][5] }}</span></td>
                        </tr>
                        <tr>
                            <td align="center"><span>{{ $Sabah['consolation'][6] }}</span></td>
                            <td align="center"><span>{{ $Sabah['consolation'][7] }}</span></td>
                            <td align="center"><span>{{ $Sabah['consolation'][8] }}</span></td>
                        </tr>
                        <tr>
                            <td align="center" bgcolor="#999">&nbsp;</td>
                            <td align="center"><span>{{ $Sabah['consolation'][9] }}</span></td>
                            <td align="center" bgcolor="#999">&nbsp;</td>
                        </tr>
                    </table>
                </div>
            </div>
            <!--End of t88-->
            
            <!--STC-->
            <div id="fD_table">
                <div id="fD_table_header" class="stc">
                    <div id="fD_table_img">
                        <img src="{{ asset('/royalewin/img/4D_stc.png') }}" width="80" height="40" />
                    </div>
                    <div id="fD_table_title">
                        <h1 class="title">SANDAKAN 4D</h1>
                        <span>{{ $Sandakan['date'] }}</span>
                    </div>
                </div>
                <div id="fD_table_result">
                    <table width="180" border="1" cellpadding="1" cellspacing="0" style="border-color:#999;">
                        <tr>
                            <td width="61" align="center" class="stc fD_top3_description">1st</td>
                            <td width="61" align="center" class="stc fD_top3_description">2nd</td>
                            <td align="center" class="stc fD_top3_description">3rd</td>
                        </tr>
                        <tr>
                            <td align="center" class="fD_top3_no"><span>{{ $Sandakan['first'] }}</span></td>
                            <td align="center" class="fD_top3_no"><span>{{ $Sandakan['second'] }}</span></td>
                            <td align="center" class="fD_top3_no"><span>{{ $Sandakan['third'] }}</span></td>
                        </tr>
                    </table>
                    <table width="180" border="1" cellpadding="1" cellspacing="0" class="fD_normalPrice">
                        <tr>
                            <td colspan="3" align="center" class="stc">Special</td>
                        </tr>
                        <tr>
                            <td align="center"><span>{{ $Sandakan['special'][0] }}</span></td>
                            <td align="center"><span>{{ $Sandakan['special'][1] }}</span></td>
                            <td align="center"><span>{{ $Sandakan['special'][2] }}</span></td>
                        </tr>
                        <tr>
                            <td align="center"><span>{{ $Sandakan['special'][3] }}</span></td>
                            <td align="center"><span>{{ $Sandakan['special'][4] }}</span></td>
                            <td align="center"><span>{{ $Sandakan['special'][5] }}</span></td>
                        </tr>
                        <tr>
                            <td align="center"><span>{{ $Sandakan['special'][6] }}</span></td>
                            <td align="center"><span>{{ $Sandakan['special'][7] }}</span></td>
                            <td align="center"><span>{{ $Sandakan['special'][8] }}</span></td>
                        </tr>
                        <tr>
                            <td align="center" bgcolor="#999">&nbsp;</td>
                            <td align="center"><span>{{ $Sandakan['special'][9] }}</span></td>
                            <td align="center" bgcolor="#999">&nbsp;</td>
                        </tr>
                    </table>
                    <table width="180" border="1" cellpadding="1" cellspacing="0" class="fD_normalPrice">
                        <tr>
                            <td colspan="3" align="center" class="stc">Consolation</td>
                        </tr>
                        <tr>
                            <td align="center"><span>{{ $Sandakan['consolation'][0] }}</span></td>
                            <td align="center"><span>{{ $Sandakan['consolation'][1] }}</span></td>
                            <td align="center"><span>{{ $Sandakan['consolation'][2] }}</span></td>
                        </tr>
                        <tr>
                            <td align="center"><span>{{ $Sandakan['consolation'][3] }}</span></td>
                            <td align="center"><span>{{ $Sandakan['consolation'][4] }}</span></td>
                            <td align="center"><span>{{ $Sandakan['consolation'][5] }}</span></td>
                        </tr>
                        <tr>
                            <td align="center"><span>{{ $Sandakan['consolation'][6] }}</span></td>
                            <td align="center"><span>{{ $Sandakan['consolation'][7] }}</span></td>
                            <td align="center"><span>{{ $Sandakan['consolation'][8] }}</span></td>
                        </tr>
                        <tr>
                            <td align="center" bgcolor="#999">&nbsp;</td>
                            <td align="center"><span>{{ $Sandakan['consolation'][9] }}</span></td>
                            <td align="center" bgcolor="#999">&nbsp;</td>
                        </tr>
                    </table>
                </div>
            </div>
            <!--End of STC-->
            
            <!--Cash-->
            <div id="fD_table">
                <div id="fD_table_header" class="cash">
                    <div id="fD_table_img">
                        <img src="{{ asset('/royalewin/img/4D_cash.png') }}" width="80" height="40" />
                    </div>
                    <div id="fD_table_title">
                        <h1 class="title">SPECIAL BIGSWEEP</h1>
                        <span>{{ $Sarawak['date'] }}</span>
                    </div>
                </div>
                <div id="fD_table_result">
                    <table width="180" border="1" cellpadding="1" cellspacing="0" style="border-color:#999;">
                        <tr>
                            <td width="61" align="center" class="cash fD_top3_description">1st</td>
                            <td width="61" align="center" class="cash fD_top3_description">2nd</td>
                            <td align="center" class="cash fD_top3_description">3rd</td>
                        </tr>
                        <tr>
                            <td align="center" class="fD_top3_no"><span>{{ $Sarawak['first'] }}</span></td>
                            <td align="center" class="fD_top3_no"><span>{{ $Sarawak['second'] }}</span></td>
                            <td align="center" class="fD_top3_no"><span>{{ $Sarawak['third'] }}</span></td>
                        </tr>
                    </table>
                    <table width="180" border="1" cellpadding="1" cellspacing="0" class="fD_normalPrice">
                        <tr>
                            <td colspan="3" align="center" class="cash">Special</td>
                        </tr>
                        <tr>
                            <td align="center"><span>{{ $Sarawak['special'][0] }}</span></td>
                            <td align="center"><span>{{ $Sarawak['special'][1] }}</span></td>
                            <td align="center"><span>{{ $Sarawak['special'][2] }}</span></td>
                        </tr>
                        <tr>
                            <td align="center"><span>{{ $Sarawak['special'][3] }}</span></td>
                            <td align="center"><span>{{ $Sarawak['special'][4] }}</span></td>
                            <td align="center"><span>{{ $Sarawak['special'][5] }}</span></td>
                        </tr>
                        <tr>
                            <td align="center"><span>{{ $Sarawak['special'][6] }}</span></td>
                            <td align="center"><span>{{ $Sarawak['special'][7] }}</span></td>
                            <td align="center"><span>{{ $Sarawak['special'][8] }}</span></td>
                        </tr>
                        <tr>
                            <td align="center" bgcolor="#999">&nbsp;</td>
                            <td align="center"><span>{{ $Sarawak['special'][9] }}</span></td>
                            <td align="center" bgcolor="#999">&nbsp;</td>
                        </tr>
                    </table>
                    <table width="180" border="1" cellpadding="1" cellspacing="0" class="fD_normalPrice">
                        <tr>
                            <td colspan="3" align="center" class="cash">Consolation</td>
                        </tr>
                        <tr>
                            <td align="center"><span>{{ $Sarawak['consolation'][0] }}</span></td>
                            <td align="center"><span>{{ $Sarawak['consolation'][1] }}</span></td>
                            <td align="center"><span>{{ $Sarawak['consolation'][2] }}</span></td>
                        </tr>
                        <tr>
                            <td align="center"><span>{{ $Sarawak['consolation'][3] }}</span></td>
                            <td align="center"><span>{{ $Sarawak['consolation'][4] }}</span></td>
                            <td align="center"><span>{{ $Sarawak['consolation'][5] }}</span></td>
                        </tr>
                        <tr>
                            <td align="center"><span>{{ $Sarawak['consolation'][6] }}</span></td>
                            <td align="center"><span>{{ $Sarawak['consolation'][7] }}</span></td>
                            <td align="center"><span>{{ $Sarawak['consolation'][8] }}</span></td>
                        </tr>
                        <tr>
                            <td align="center" bgcolor="#999">&nbsp;</td>
                            <td align="center"><span>{{ $Sarawak['consolation'][9] }}</span></td>
                            <td align="center" bgcolor="#999">&nbsp;</td>
                        </tr>
                    </table>
                </div>
            </div>
            <!--End of Cash-->
        </div>
    </div>
</div>

<div id="lmPayoutTable" class="lmHolder">
    <div class="modal-content">
        <div style="text-align: right;"><span class="btnClose">x</span></div>
        
        <h2 style="color:#FFBF00;">{{Lang::get('public.Payout')}} {{Lang::get('public.Table')}}</h2><br>
        <b>Prize money for Big Forecast</b><br><br>
        For this Forecast, as shown in the table below, with every RM1 bet, the 1st Prize pays RM3,400, 2nd Prize RM1,200, 3rd Prize RM600, Special Prize RM250 and Consolation Prize RM80.
        <br><br>
        <div class="table" >	
            <table>
                <tr>
                    <td>BIG FORECAST</td>
                    <td>PRIZE AMOUNT</td>
                </tr>
                <tr>
                    <td>1st Prize</td>
                    <td>RM 3,400.00</td>
                </tr>
                <tr>
                    <td>2nd Prize</td>
                    <td>RM 1,200.00</td>
                </tr>
                <tr>
                    <td>3rd Prize</td>
                    <td>RM 600.00</td>
                </tr>
                <tr>
                    <td>Special Prize</td>
                    <td>RM 250.00</td>
                </tr>
                <tr>
                    <td>Consolation Prize</td>
                    <td>RM 80.00</td>
                </tr>
            </table>
        </div>

        <b>Prize money for Small Forecast</b><br><br>
        For this Forecast, as shown in the table below, with every RM1 bet, the 1st Prize pays RM4,800, 2nd Prize RM2,400, 3rd Prize RM1,200.
        <br><br>

        <div class="table" >	
            <table>
                <tr>
                    <td>SMALL FORECAST</td>
                    <td>PRIZE AMOUNT</td>
                </tr>
                <tr>
                    <td>1st Prize</td>
                    <td>RM 4,800.00</td>
                </tr>
                <tr>
                    <td>2nd Prize</td>
                    <td>RM 2,400.00</td>
                </tr>
                <tr>
                    <td>3rd Prize</td>
                    <td>RM 1,200.00</td>
                </tr>
                <tr>
                    <td>4A Prize</td>
                    <td>RM 8,000.00</td>
                </tr>
            </table>
        </div>

        <b>Prize money for 3D</b><br><br>
        For this Forecast, as shown in the table below, with every RM1 bet, the 1st Prize pays RM 280, 2nd Prize RM280, and 3rd Prize RM280.
        <div class="table" >	
            <table>
                <tr>
                    <td>3D</td>
                    <td>PRIZE AMOUNT</td>
                </tr>
                <tr>
                    <td>1st Prize</td>
                    <td>RM 280.00</td>
                </tr>
                <tr>
                    <td>2nd Prize</td>
                    <td>RM 280.00</td>
                </tr>
                <tr>
                    <td>3rd Prize</td>
                    <td>RM 280.00</td>
                </tr>
                <tr>
                    <td>3A Prize</td>
                    <td>RM 840.00</td>
                </tr>
            </table>
        </div>

        <b>Prize money for 5D/6D</b><br><br>
        For this Forecast, as shown in the table below, with every RM1 bet.
        <div class="table" >	
            <table>
                <tr>
                    <td>5D</td>
                    <td>PRIZE AMOUNT</td>
                    <td>6D</td>
                    <td>PRIZE AMOUNT</td>
                </tr>
                <tr>
                    <td>1st Prize</td>
                    <td>RM 15,000.00</td>
                    <td>1st Prize</td>
                    <td>RM 100,000.00</td>
                </tr>
                <tr>
                    <td>2nd Prize</td>
                    <td>RM 5,000.00</td>
                    <td>2nd Prize</td>
                    <td>RM 3,000.00</td>
                </tr>
                <tr>
                    <td>3rd Prize</td>
                    <td>RM 3,000.00</td>
                    <td>3rd Prize</td>
                    <td>RM 300.00</td>
                </tr>
                <tr>
                    <td>4th Prize</td>
                    <td>RM 500.00</td>
                    <td>4th Prize</td>
                    <td>RM 30.00</td>
                </tr>
                <tr>
                    <td>5th Prize</td>
                    <td>RM 20.00</td>
                    <td>5th Prize</td>
                    <td>RM 4.00</td>
                </tr>
                <tr>
                    <td>6th Prize</td>
                    <td>RM 5.00</td>
                    <td></td>
                    <td></td>
                </tr>
            </table>
        </div>
    </div>
</div>

<div id="lmRulesTable" class="lmHolder">
    <div class="modal-content">
        <div style="text-align: right;"><span class="btnClose">x</span></div>
        
        <h2 style="color:#FFBF00;">Rules &amp; Regulations</h2><br>
        <b>IMPORTANT: PLEASE READ THE INFORMATION SET OUT BELOW CAREFULLY BEFORE ACCEPTING THESE TERMS AND CONDITIONS.</b>
        <p>
            Notice and acceptance of these terms and conditions constitutes the making of an agreement, which you accept by selecting the "Agree" on this page and proceeding to access the Website. When you do so, a legally binding agreement in accordance with these terms and conditions is concluded between (a) you, the end user of the Website (the "Customer"), and (b) the Company.
        </p>
        <b>General terms and conditions</b>
        <p>
            The following are the terms and conditions governing the use of the Website. All Customer activities on the Website are subject to, and governed by these Terms and Conditions.
        </p>
        <b>The company reserves the right to do the following:</b>
        <p>
            <ol>
                <li style="list-style:circle">The company reserves the right to void any unusual transaction.</li>
                <li style="list-style:circle">Our company reserves the right to refuse any investment without further correspondence.</li>
                <li style="list-style:circle">Investment placed after preset time will be void</li>
                <li style="list-style:circle">Cancel draw and refund to user whenever is necessary</li>
                <li style="list-style:circle">The content of the pages of this website is for your general information and use only. It is subject to change without notice.</li>
                <li style="list-style:circle">Neither we nor any third parties provide any warranty or guarantee as to the accuracy, timeliness, performance, completeness or suitability of the information and materials found or offered on this website for any particular purpose. You acknowledge that such information and materials may contain inaccuracies or errors and we expressly exclude liability for any such inaccuracies or errors to the fullest extent permitted by law.</li>
                <li style="list-style:circle">Your use of any information or materials on this website is entirely at your own risk, for which we shall not be liable. It shall be your own responsibility to ensure that any products, services or information available through this website meet your specific requirements.</li>
            </ol>
        </p>
        <b>Important Notice:</b>
        <p>
            Sabah Lotto Results - 3D results for 1st, 2nd and 3rd are NOT derived from the last 3 digits of 4D's 1st, 2nd and 3rd. They are drawn separately. Payment is based on these 2 sets of results.
        </p>
    </div>
</div>
@endsection

@section('footer_seo')
            <div class="footerContent">
                <div><h1 class="footerTitle">Check Live 4D Result on RoyaleWin</h1></div>
                <p>
                RoyaleWin is among the best online casino and gambling sites in Malaysia. There are hundreds of games to choose from and new games are being added periodically. You can play slot games and try your luck, or you can interact with the beautiful live dealers that will enhance your live casino experience. If you love sports betting, then you can head on to the sportsbook page to do your wager. You can also check 4d live results on RoyaleWin while you are at it. Who knows, you might get lucky with one of your 4d numbers?<br><br>
                You can find the most popular 4d companies in Malaysia at RoyaleWin, which are Magnum 4D and Toto 4D, as well as other smaller ones like Sabah 4D and Sandakan 4D. Aside from Malaysia 4D, there are also Singapore 4D results and Damacai 1+3D that has their own set of loyal customers.<br><br>
                In RoyaleWin, you can do a big forecast or a small forecast for your 4D results bet. The difference is of course the amount that you will be able to win if you get your live 4D numbers right. If you get the big forecast first prize, you will get RM3,400 for every RM1 bet while the small forecast first prize is RM4,800 for every RM1 bet. Can you imagine just how much you can get if you bet RM100 on a Magnum 4D number and you get the first price?<br><br>
                Aside from Magnum 4D and Toto 4D, RoyaleWin players can also bet on 5D and 6D numbers. The prizes for the these set of digits are much more higher but the difficulty increases many folds. If you are a new player, we suggest that you stick to Magnum 4D and Toto 4D because the chances to win are higher.<br><br>
                Lastly, before you do your bet, always make sure that you read the rules and regulations. This is important because you do not adhere to these rules, your winnings might be void. However, if you do not do anything wrong like cheating, you should be OK. Just make sure that you bet responsibly!<br><br>
                </p>
                <div class="footerTextL">
                    <div><h3 class="footerTitle">Why Royalewin</h3></div>
                    <div class="footerSub">Trusted Malaysia Online Casino - Your Premier Gaming Destination</div>
                    <p>
                        Play Baccarat, Blackjack, Roulette, Slots, Sports Betting with exciting promotions, 24/7 top of the line customer service & timely payouts with the highest level of security. <br><br>
                        Royalewin focused primarily in offering casino gaming products and services in Asia Pacific markets. Bringing you all the fun of a real casino in your home, we are committed to provide you gaming entertainment of premium quality and at exceptionally good value.<br><br>
                    </p>
                </div>
                <div class="footerTextL">
                    <div><h3 class="footerTitle">Our Games</h3></div>
                    <div class="footerSub">Live Dealer Casinos, Sportsbook, Slots, 4D</div>
                    <p>
                        Find a wide variety of top-class games, including classic favourites like Baccarat, Blackjack and Roulette, as well as popular themed slots, progressives, live casino with real dealers, live sportsbook betting, CFD (commodity, Forex, Index), and 4D.<br><br>
                        Royalewin offers superb quality games using WinningFT, MaxBet, Betsoft, XProGaming, Ho Gaming, AGGaming etc. softwares with no download required all in one advanced gaming platform.<br><br>
                    </p>
                </div>
                <div class="footerTextL">
                    <div><h3 class="footerTitle">24/7 Support</h3></div>
                    <div class="footerSub">24-Hour Customer Service</div>
                    <p>
                        Available 24 hours everyday, Royalewin Support Team is always there for you - to help answer your questions and resolve your issues as quickly and efficiently a possible.<br><br>
                        We focus on the needs of our customers by staying true to our core service principles: <br><br>
                        • The customer always come first  <br>
                        • Your satisfaction drives our business  <br>
                        • We will always strive to exceed your expectations  <br><br>
                    </p>
                </div>
                <div class="footerTextL">
                    <div><h3 class="footerTitle">Bonus Offers</h3></div>
                    <div class="footerSub">FREE Bonuses Up to 110%</div>
                    <p>
                        Royalewin loves to handsomely reward you for your support! Players at Royalewin.com are able to enjoy a fantastic selection of daily and weekly promotional offers. <br><br>
                        Our amazing range of promotions include an exclusive 110% Welcome Bonus for first-time depositors, double Daily Deposit Bonuses, Cashbacks for everything you played at our casino.<br><br>
                    </p>
                </div>
                <div class="clr"></div>
            </div>
@endsection