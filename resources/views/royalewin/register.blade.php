@extends('royalewin/master')

@if (Session::get('currency') == 'MYR')
    @section('title', 'FREE Best Online Live Casino Malaysia Registration')
    @section('keywords', 'FREE Best Online Live Casino Malaysia Registration')
    @section('description', 'RoyaleWin Online Live Casino Malaysia gives you Free Spins when you sign up NOW. Play the best online casino games – slots, roulette, video poker and more.')
@elseif (Session::get('currency') == 'IDR')
    @section('title', 'Bandar Casino Online, Situs Judi Indonesia Indonesia - Daftar')
    @section('keywords', 'Bandar Casino Online, Situs Judi Indonesia Indonesia - Daftar')
    @section('description', 'RoyaleWin adalah bandar judi bola online & agen bola terpercaya di Indonesia. Daftar Sekarang!')
@endif

@section('js')
@parent
<script type="text/javascript">
    $(function () {
        @if ($showSMSField != 1)
            $("#vcode_field").css("visibility", "hidden");
        @elseif (Session::get('currency') != 'MYR')
            $("#vcode_field").css("visibility", "hidden");
        @endif
		
		
    });
	
	auto_fillin();
	
	function auto_fillin(){
		var fullname = getQueryStringValue('fullname');
		var email = getQueryStringValue('email');
		var mobile = getQueryStringValue('mobile');
		$('#r_fullname').val(fullname);
		$('#r_email').val(email);
		$('#r_mobile').val(mobile);

		
	}
	
	function enterpressalert(e, textarea)
	{
		var code = (e.keyCode ? e.keyCode : e.which);
        if(code == 13) { //Enter keycode
            register_submit();
        }
    }
    
    function register_submit() {
        var dob = '0000-00-00';
        if (typeof $('#dob_year').val() != undefined) {
            dob = $('#dob_year').val() + '-' + $('#dob_month').val() + '-' + $('#dob_day').val();
        }
        $.ajax({
            type: "POST",
            url: "{{route('register_process')}}",
            data: {
                _token:     "{{ csrf_token() }}",
                username:	$('#r_username').val(),
                password:	$('#r_password').val(),
                repeatpassword: $('#r_repeatpassword').val(),
                code:		$('#r_code').val(),
                email:		$('#r_email').val(),
                mobile:		$('#r_mobile').val(),
                fullname:	$('#r_fullname').val(),
                referralid:	$('#r_referralid').val(),
                mobile_vcode: $("#r_mobile_vcode").val(),
                dob:		dob
            },
        }).done(function(json) {
            $('.acctTextReg').html('');
            obj = JSON.parse(json);
            var str = '';
            $.each(obj, function(i, item) {
                if ('{{Lang::get('COMMON.SUCESSFUL')}}' == item) {
                    alert('{{Lang::get('public.NewMember')}}');
                    window.location.href = "{{route('homepage')}}";
                } else {
                    $('.' + i + '_acctTextReg').html('<font style="color:red">' + item + '</font>');
                }
            });
        });
    }

    var countDownSecond = 0;
	var intervalObj = null;
    var btnObj = $("#btn_sms");

    function sendVerifySms() {
        if (intervalObj != null) {
            alert("{{ Lang::get('public.PleaseWaitForXSecondsToResendSMS') }}".replace(":p1", countDownSecond));
            return false;
        }

	    var mobileObj = $("#r_mobile");
	    var mobileNum = mobileObj.val();

	    if (mobileNum.length < 1) {
	        alert("{{ Lang::get('public.PleaseEnterContactNumberToContinue') }}");
            mobileObj.focus();

	        return false;
        }

	    if (confirm("{{ Lang::get('public.SendVerificationCodeViaSMSToThisNumber') }} " + mobileNum)) {
            countDownSecond = 60;
            smsResendCountDown();

            $.ajax({
                type: "POST",
                dataType: "json",
                url: "{{url('smsregcode')}}",
                data: {
                    _token:     "{{ csrf_token() }}",
                    tel_mobile:	mobileNum
                },
                success: function (result) {
                    if (result.code != 0) {
                        alert(result.msg);
                    }

                    if (result.code == 2) {
                        countDownSecond = 10;
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
//                    console.log('ok failed');
                }
            });
        }
    }

    function smsResendCountDown() {
        if (intervalObj == null) {
            intervalObj = setInterval(smsResendCountDown, 1000)
        }

        if (countDownSecond <= 1) {
            btnObj.text("Send SMS");
            clearInterval(intervalObj);
            intervalObj = null;
            return;
        }

        btnObj.text("Resend (" + --countDownSecond + ")");
    }
	function getQueryStringValue (key) {  
	  return decodeURIComponent(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + encodeURIComponent(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));  
	} 
</script>
@endsection

@section('content')
<div class="midSect" style="background-image: url('{{ asset('royalewin/img/landing2/bg-large.jpg') }}');">
{{--<div class="midSect bgOthers">--}}
    <div class="contOthers">
        <span>{{ strtoupper(Lang::get('public.Registration')) }}</span>
    </div>
    <div class="othersCont">
        <div class="inpLeft">
            <label>{{ Lang::get('public.FullName') }}* : </label><input type="text" id="r_fullname" name="r_fullname"><span class="fullname_acctTextReg acctTextReg"></span>
            <div class="clr"></div>
            <div class="rem">{{ Lang::get('public.NameCaution') }}</div>
            <div class="rowSp">
                <label>{{ Lang::get('public.Username2') }}* : </label><input type="text" id="r_username" name="r_username" class="sp1"><span class="username_acctTextReg acctTextReg"></span>
                <div class="clr"></div>
            </div>
            <div class="rowSp">
                <label>{{ Lang::get('public.Password') }}* : </label><input type="password" id="r_password" name="r_password" class="sp1"><span class="password_acctTextReg acctTextReg"></span>
                <div class="clr"></div>
            </div>
            <div class="rowSp">
                <label>{{ Lang::get('public.RepeatPassword') }}* : </label><input type="password" id="r_repeatpassword" name="r_repeatpassword">
                <div class="clr"></div>
            </div>
            <div class="rowSp">
                <label>{{Lang::get('public.Code')}}* : </label><input type="text" id="r_code" onKeyPress="enterpressalert(event, this)" class="coded"><img style="float: left;"  src="{{route('captcha', ['type' => 'register_captcha'])}}" width="60" height="27" class="coded blacked" /> <span class="code_acctTextReg acctTextReg"></span>
                <div class="clr"></div>
            </div>
            <div class="rowSp">
                <p>* {{ Lang::get('public.RequiredField') }}</p>
                <p>{{ Lang::get('public.DifficultyContact') }}</p>
                <div class="clr"></div>
            </div>
        </div>

        <div class="inpLeft">
            <label>{{ Lang::get('public.EmailAddress') }}* : </label><input type="text" id="r_email" name="r_email"><span class="email_acctTextReg acctTextReg"></span>
            <div class="clr"></div>
            <div class="rem">&nbsp;</div>
            <div class="rowSp">
                <label>{{ Lang::get('public.ReferralID') }} ({{ Lang::get('public.Optional') }}) : </label><input type="text" id="r_referralid" name="r_referralid" value="{{ Request::input('referralid') }}"><span class="r_referralid_acctTextReg acctTextReg"></span>
                <div class="clr"></div>
            </div>
            <div class="rowSp">
                <label>{{ Lang::get('public.ContactNumber') }}* : </label><input type="text" id="r_mobile" name="r_mobile"><span class="mobile_acctTextReg acctTextReg"></span>
                <div class="clr"></div>
            </div>

            <div class="rowSp" id="vcode_field">
                <label>{{ Lang::get('public.VerificationCode') }}* : </label><input type="text" id="r_mobile_vcode" name="r_mobile_vcode" style="width: 20% !important;"><a href="javascript:void(0);" id="btn_sms" style="height: 30px; line-height: 30px; padding-left: 5px; color: #ffffff;" onclick="sendVerifySms();">{{ Lang::get('public.ClickHereToGetVerificationCode') }}</a>
                <br><span class="mobile_vcode_acctTextReg acctTextReg"></span>
                <div class="clr"></div>
            </div>

            <div class="rowSp">
                <div class="sBtn">
					<a href="javascript:void(0);" onclick="register_submit();">{{Lang::get('public.Submit')}}</a>
                </div>
                <div class="clr"></div>
            </div>
        </div>
        <div class="clr"></div>
    </div>
</div>
@endsection

@section('footer_seo')
            <div class="footerContent">
                <div><h1 class="footerTitle">The First Step to Greatness: Registering An Account at RoyaleWin</h1></div>
                <p>
                For every online casino website, it is compulsory for you to create an account. It is the same if you want to play at RoyaleWin. Some online casinos require a lot of information in order to create an account and this sometimes hinder new players to join. RoyaleWin is not as these online casinos. RoyaleWin ensures that the process of creating a new account is hassle free and easy to understand.<br><br>
                To register a RoyaleWin account, all you need to do is to enter your full name, username, password, email address and contact number. These items are compulsory to key in because it will be used to handle any problems that you will have in the future, if there is any. The contact number is especially important because without it, it will be hard for the customer service to contact you and get more details on the problems you are facing. You must also make sure that you key in your full name correctly because putting a fake name can lead you to unable to withdraw your winnings.<br><br>
                If you are hooked to RoyaleWin based from your friends or families’ recommendation, then you can do them a favor and key in their referral ID. With this, they will get some referral bonus because they are creating a healthy growth to the best online casino in Malaysia. However, this is an optional item so if you come from the search engines, you can just leave it blank.<br><br>
                If you are still having problems registering a new account at RoyaleWin, you can always contact their customer service. The RoyaleWin customer service is known for their instant help and professional attitude in helping their users. Playing online casino and slot games at RoyaleWin will ensure that you are in good hands. You must also take advantage of the new player promotion currently available so register an account right now!<br><br>
                </p>
                <div class="footerTextL">
                    <div><h3 class="footerTitle">Why Royalewin</h3></div>
                    <div class="footerSub">Trusted Malaysia Online Casino - Your Premier Gaming Destination</div>
                    <p>
                        Play Baccarat, Blackjack, Roulette, Slots, Sports Betting with exciting promotions, 24/7 top of the line customer service & timely payouts with the highest level of security. <br><br>
                        Royalewin focused primarily in offering casino gaming products and services in Asia Pacific markets. Bringing you all the fun of a real casino in your home, we are committed to provide you gaming entertainment of premium quality and at exceptionally good value.<br><br>
                    </p>
                </div>
                <div class="footerTextL">
                    <div><h3 class="footerTitle">Our Games</h3></div>
                    <div class="footerSub">Live Dealer Casinos, Sportsbook, Slots, 4D</div>
                    <p>
                        Find a wide variety of top-class games, including classic favourites like Baccarat, Blackjack and Roulette, as well as popular themed slots, progressives, live casino with real dealers, live sportsbook betting, CFD (commodity, Forex, Index), and 4D.<br><br>
                        Royalewin offers superb quality games using WinningFT, MaxBet, Betsoft, XProGaming, Ho Gaming, AGGaming etc. softwares with no download required all in one advanced gaming platform.<br><br>
                    </p>
                </div>
                <div class="footerTextL">
                    <div><h3 class="footerTitle">24/7 Support</h3></div>
                    <div class="footerSub">24-Hour Customer Service</div>
                    <p>
                        Available 24 hours everyday, Royalewin Support Team is always there for you - to help answer your questions and resolve your issues as quickly and efficiently a possible.<br><br>
                        We focus on the needs of our customers by staying true to our core service principles: <br><br>
                        • The customer always come first  <br>
                        • Your satisfaction drives our business  <br>
                        • We will always strive to exceed your expectations  <br><br>
                    </p>
                </div>
                <div class="footerTextL">
                    <div><h3 class="footerTitle">Bonus Offers</h3></div>
                    <div class="footerSub">FREE Bonuses Up to 110%</div>
                    <p>
                        Royalewin loves to handsomely reward you for your support! Players at Royalewin.com are able to enjoy a fantastic selection of daily and weekly promotional offers. <br><br>
                        Our amazing range of promotions include an exclusive 110% Welcome Bonus for first-time depositors, double Daily Deposit Bonuses, Cashbacks for everything you played at our casino.<br><br>
                    </p>
                </div>
                <div class="clr"></div>
            </div>
@endsection