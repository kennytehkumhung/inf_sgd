<!doctype html>
<html>
    <head>
        <link href="{{ asset('/royalewin/resources/css/style.css') }}" rel="stylesheet" type="text/css" />
        
        <style type="text/css">
            .slot_box {
                background-color: #000;
                width: 150px; 
                height: 202px;
            }
            
            .slot_box img {
                width: 150px; 
                height: 150px;
            }
        </style>
    </head>
    <body style="background-color: transparent; overflow: hidden;">
        <div class="slotContainerBottom">
            <div class="slotMenuHolder">
                <div class="slot_menu">
                    <ul>
                        <li><a href="{{route('w88', [ 'type' => 'slot' , 'category' => 'Arcades' ] )}}">Arcades (16)</a></li>
                        <li><a href="{{route('w88', [ 'type' => 'slot' , 'category' => 'Card Games' ] )}}">Card Games(15)</a></li>
                        <li><a href="{{route('w88', [ 'type' => 'slot' , 'category' => 'Slots' ] )}}">Slots (30)</a></li>
                        <li><a href="{{route('w88', [ 'type' => 'slot' , 'category' => 'Table Games' ] )}}">Table Games (3)</a></li>
                        <li><a href="{{route('w88', [ 'type' => 'slot' , 'category' => 'Video Poker' ] )}}">Video Poker (22)</a></li>
                    </ul>
                </div>
            </div>

            <div id="slot_lobby">
                
                @foreach( $lists as $list )
                     <div class="slot_box">
                        <a href="javascript:void(0)" onclick="@if (Auth::user()->check())window.open('//casino.gpiops.com/mini/?op=INFINIWIN&game_code={{$list->code}}&language=en&playmode=real&ticket={{$token}}','w88_slot','width=1000,height=750')@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif">
                            <img src="{{url()}}/royalewin/img/w88/{{$list->image}}" width="150" height="150" alt=""/>
                        </a>
                        <span>{{$list->gamename_en}}</span>
                     </div>			 
                @endforeach

                <div class="clr"></div>
            </div>

            <div class="clr"></div>
        </div>
    </body>
</html>