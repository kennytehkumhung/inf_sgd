@extends('royalewin/master')

@section('title', 'Soccer')

@section('css')
@parent

<style type="text/css">
    .pl-holder * {
        color: #ffffff;
    }

    .pl-holder td {
        vertical-align: top;
    }

    .pl-holder .text-gold {
        text-align: center;
        font-weight: bold;
        color: #ffae00;
    }

    .pl-holder .side-col {
        background: url('{{ asset('/royalewin/img/pl-side-bg.png') }}') repeat-x 0 0;
        width: 210px;
    }

    .pl-holder .side-col-header {
        font-size: 18px;
        font-weight: bold;
        text-align: center;
        padding-top: 9px;
        padding-bottom: 10px;
    }

    .pl-holder .winner-team-score img {
        text-align: center;
        height: 100%;
    }

    .pl-holder .winner-team-info td {
        vertical-align: middle;
    }

    .pl-holder .winner-name-list li {
        padding: 4px 0;
    }

    .pl-holder input[type="text"] {
        border-radius: 10px;
        font-size: 26px;
        width: 130px;
        text-align: center;
        color: #000000;
    }

    .footer .footerCenter,
    .footer .footerLeft1,
    .footer .footerRight1 {
        display: none !important;
    }

    /*.footer {*/
        /*display: none !important;*/
    /*}*/

    .bet_history {
        width: 100%;
        border-collapse: collapse;
    }

    .bet_history td {
        border: 1px solid #fff;
        text-align: center;
    }
</style>
@endsection

@section('js')
@parent

<script type="text/javascript">
    $(function () {
        $(".footer .footerContent").html($("#footerContent").html());
    });

    function submit() {
        var bets = [$("#betA").val(), $("#betB").val()];
        var hasError = false;

        $.each(bets, function () {
            if (this == "" || isNaN(this)) {
                alert("Bet should be number.");
                hasError = true;
                return false;
            } else if (this < 0) {
                alert("Bet cannot be negative number.");
                hasError = true;
                return false;
            }
        });

        if (!hasError && confirm("Confirm submit your bet? Submitted bet is NOT allow to edit.")) {
            $.ajax({
                type: "POST",
                dataType: "json",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "_act": "save",
                    "bet_match": "{{ $betMatchId }}",
                    "bet_a": bets[0],
                    "bet_b": bets[1]
                },
                url: "{{ route('premier-league') }}",
                success: function(response) {
                    alert(response.msg);

                    if (response.code == 1) {
                        return window.location.href = "{{ route('premier-league') }}";
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert("Submit bet failed, please try again later.");
                }
            });
        }
    }
</script>
@endsection

@section('content')

@if (Request::input('reminder') == 'y')
    <script type="text/javascript">
        $(function () {
            alert("{{ Lang::get('public.PremierLeagueTokenReminder') }}");
        });
    </script>
@endif
@if(date("d/m")<date("27/12"))
<div class="midSect bg4d" style="background-image: url({{ asset('/royalewin/img/landing2/epl/bg-large.jpg') }}) !important;">
@else
<div class="midSect bg4d" style="background-image: url({{ asset('/royalewin/img/landing2/epl/SPORTBG.png') }}) !important;">
@endif
    <!--<div class="cont4"></div>-->
    <div class="dContainerBottom">
        <div class="dContainerBottomInner">
            <table class="pl-holder" cellpadding="0" cellspacing="0" border="0" style="height: 450px; width: 100%;">
                <tr>
                    <td class="side-col">
                        <div class="side-col-header" style="background: url('{{ asset('/royalewin/img/pl-winner.png') }}') repeat-x 0 0;">
                            WINNER
                        </div>
                        <div>
                            <ol class="winner-name-list" style="padding-left: 26px; margin-left: 0;">
                                @foreach($winnerList as $key => $val)
                                    <li>{{ $val }}</li>
                                @endforeach
                            </ol>
                        </div>
                        <div class="text-gold" style="padding: 14px;">
                            {{ $preWeek }}
                        </div>
                        <div style="padding-left: 5px;">
                            <table class="winner-team-info" cellspacing="0" cellpadding="2" style="width: 100%;">
                                <tr style="border: 1px solid #ffffff;">
                                    <td style="width: 26px;"><img alt="" src="{{ asset('/royalewin/img/pl-ball.png') }}" /></td>
                                    <td><span>{{ $preTeamAName }}</span></td>
                                    <td style="width: 26px;"><span style="font-weight: bold;">{{ $preTeamAScore }}</span></td>
                                </tr>
                                <tr style="border: 1px solid #ffffff;">
                                    <td>&nbsp;</td>
                                    <td><span class="text-gold">VS</span></td>
                                </tr>
                                <tr style="border: 1px solid #ffffff;">
                                    <td><img alt="" src="{{ asset('/royalewin/img/pl-ball.png') }}" /></td>
                                    <td><span>{{ $preTeamBName }}</span></td>
                                    <td style="width: 26px;"><span style="font-weight: bold;">{{ $preTeamBScore }}</span></td>
                                </tr>
                            </table>
                        </div>
                    </td>
                    <td>
                        <div style="position: relative;">
                            <img alt="" src="{{ $bgImagePath }}" style="width: 100%; height: auto;" />
                            @if ($betOpening === true)
                                <input type="text" id="betA" style="position: absolute; top: 240px; left: 27px;" />
                                <input type="text" id="betB" style="position: absolute; top: 240px; left: 461px;" />
                                <a href="javascript:void(0);" onclick="submit();" style="position: absolute; top: 324px; left: 215px;">
                                    <img alt="" src="{{ asset('/royalewin/img/pl-submit.png') }}" />
                                </a>
                            @endif
                        </div>
                    </td>
                    <td class="side-col" rowspan="3">
                        <div class="side-col-header" style="background: url('{{ asset('/royalewin/img/pl-week.png') }}') repeat-x 0 0;">
                            {{ $betWeek }}&nbsp;
                        </div>
                        <div style="padding-left: 5px; font-size: 13px;">
                            <p>BET CLOSING DATE:</p>
                            <span>{{ $betClosingDate }}</span>
                        </div>
                        <div style="padding-top: 20px; padding-left: 5px;">
                            <table class="winner-team-info" cellspacing="0" cellpadding="2" style="width: 100%;">
                                <tr style="border: 1px solid #ffffff;">
                                    <td style="width: 26px;"><img alt="" src="{{ asset('/royalewin/img/pl-ball.png') }}" /></td>
                                    <td><span>{{ $betTeamAName }}</span></td>
                                </tr>
                                <tr style="border: 1px solid #ffffff;">
                                    <td>&nbsp;</td>
                                    <td><span class="text-gold">VS</span></td>
                                </tr>
                                <tr style="border: 1px solid #ffffff;">
                                    <td><img alt="" src="{{ asset('/royalewin/img/pl-ball.png') }}" /></td>
                                    <td><span>{{ $betTeamBName }}</span></td>
                                </tr>
                            </table>
                        </div>
                        <div style="padding-top: 50px;text-align: center;">
                            <img alt="" src="{{ asset('/royalewin/img/pl-logo.png') }}" />
                        </div>
                        <div style="padding-left:5px;">
                            <table border="1" cellspacing="0" style="width:100%;">
                                <tbody>
                                    <tr>
                                        <th colspan="3" style="text-align:center;">Upcoming Match</th>
                                    </tr>
                                    <!--loop-->
                                    @foreach($UpcomingMatch as $key => $val)
                                    <tr style="text-align:center;">
                                        <td>{{ $val[0] }}</td>
                                        <td>VS</td>
                                        <td>{{ $val[1] }}</td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">{{ $val[2] }}</td>
                                    </tr>
                                    @endforeach
                                    <!--loop-->
                                </tbody>
                            </table>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>
                        <div style="position: relative;">
                            <img alt="" src="{{ asset('/royalewin/img/pl-chance.gif') }}" />
                            <span style="color: #000; font-weight: bold; position: absolute; top: 7px; left: 160px;">{{ $betTokenAmount }}</span>
                        </div>
                        <br>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>
                        Bet History:
                        <table class="bet_history" cellpadding="5" cellspacing="0">
                            <thead>
                            <tr>
                                <td style="font-weight: bold;">#</td>
                                <td style="font-weight: bold;">{{ $betTeamAName }}</td>
                                <td style="font-weight: bold;">{{ $betTeamBName }}</td>
                                <td style="font-weight: bold;">Status</td>
                                <td style="font-weight: bold;">Bet Time</td>
                            </tr>
                            </thead>
                            <tbody>
                                @if (count($betHistory) > 0)
                                    @foreach ($betHistory as $r)
                                        <tr>
                                            <td style="width: 1%;">{{ $r['no'] }}.</td>
                                            <td>{{ $r['bet_a'] }}</td>
                                            <td>{{ $r['bet_b'] }}</td>
                                            <td>{{ $r['status'] }}</td>
                                            <td>{{ $r['created_at'] }}</td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="5">No data available.</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </td>
                    <td>&nbsp;</td>
                </tr>
            </table>
            <div class="clr"></div>
        </div>
    </div>
</div>

<div id="footerContent" style="display: none;">
    <div class="footerContent">
        <h3 class="footerTitle">{{ Lang::get('public.TNC') }}</h3>
        <ol>
            <li>These promotion starts on 00:00:00 (GMT+8) 6 NOVEMBER 2017 until 23:59:59 (GMT+8) 31 MAY 2018</li>
            <li>
                Every Royalewin member(MYR) is eligible to participate in our English Premier League Correct Score Tournament , This promotion classify into 3 categories
                <br><br>
                <table class="bet_history" cellpadding="3" cellspacing="3" style="width: 200px;">
                    <thead>
                    <tr>
                        <td style="font-weight: bold;">Entitlement</td>
                        <td style="font-weight: bold;">Chance</td>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>100-499</td>
                        <td>1</td>
                    </tr>
                    <tr>
                        <td>500-999</td>
                        <td>3</td>
                    </tr>
                    <tr>
                        <td>1000 &amp; above</td>
                        <td>5</td>
                    </tr>
                    </tbody>
                </table>
                <br>
            </li>
            <li>Promotion applies to all Myroyalewin.net members using MYR only.</li>
            <li>All Correct Score chances get only valid before the next following match date from the date received. Example:- Correct Score chances get on will only valid until 5 minutes before match kick off time. All unclaimed Correct Score chance will be forfeited on the subsequent match kick off from the date.</li>
            <li>Please check before you confirm your bet, member are not allow to edit or change after the bet was confirmed.</li>
            <li>Members will be awarded with English Premier League correct score chance to a maximum of 5 chances per match.</li>
            <li>For every match a total prize of RM2000 will be given out to 10 winners from Royalewin (each winner gets RM200). Grab the chance to bet before this game kicks off.</li>
            <li>All English Premier League match results are based on Premierleague Official site results.</li>
            <li>Enjoy Bonus credit up to MYR 200. Bonus will be credited within 48 hours after verification from English Premier League official website.</li>
            <li>To redeem winnings from this promotion, there will be ONLY THREE (3) times rollover requirement attached to the winning prizes.</li>
            <li>The promotion is offered limited to individual member, family, account, email address, contact number, bank account or IP address. If a second account is opened by any member, all bonuses and winnings will be void on both accounts and original deposit will be returned.</li>
            <li>Royale Win have the right to forfeit any bonuses, if we found individuals are using improper ways to redeem their bonuses or they do not fulfill all the requirements.</li>
            <li>Subjected to myroyalewin.net General Promotion Terms and Conditions.</li>
        </ol>
    </div>
    <div class="clr"></div>
</div>
@endsection