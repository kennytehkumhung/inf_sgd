<!doctype html>
<html>
    <head>

        @if (Session::get('currency') == 'MYR')
            <title>ROYALEWIN: Online Slot Malaysia - PlayTech Slot Games</title>
            <meta name="keywords" content="Online Slot Malaysia - PlayTech Slot Games"/>
            <meta name="description" content="Play PlayTech Online Slots at RoyaleWin Online Casino Malaysia and claim a FREE SPIN bonus today!"/>
        @endif

        <link href="{{ asset('/royalewin/resources/css/style.css') }}" rel="stylesheet" type="text/css" />
        
        <style type="text/css">
            .slot_box {
                background-color: #000;
                width: 150px; 
                height: 202px;
            }
            
            .slot_box img {
                width: 150px; 
                height: 150px;
            }
        </style>
    </head>
    <body style="background-color: transparent; overflow: hidden;">
        <div class="slotContainerBottom">
            <div class="slotMenuHolder">
                <div class="slot_menu">
                    <ul>
                        <li><a href="{{route('plt', [ 'type' => 'brand' , 'category' => '1' ] )}}">Branded Games (21)</a></li>
                        <li><a href="{{route('plt', [ 'type' => 'slot' , 'category' => 'slot' ] )}}">Slot (152)</a></li>
                        <li><a href="{{route('plt', [ 'type' => 'slot' , 'category' => 'arcade' ] )}}">Arcade (4)</a></li>
                        <li><a href="{{route('plt', [ 'type' => 'slot' , 'category' => 'tablecards' ] )}}">Tablecards (14)</a></li>
                    </ul>
                </div>
            </div>

            <div id="slot_lobby">
                @if (!$isMobile)
                    <div class="slot_box">
                        <a onclick=" @if (Auth::user()->check())window.open('{{route('pltslotiframe' , [ 'gamecode' => 'sfh' ] )}}', 'plt_slot', 'width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif  " href="javascript:void(0)">
                            <img width="150" height="150" alt="" src="{{ asset('/royalewin/img/plt/sfh.jpg') }}">
                        </a>
                        <span>Safari Heat</span>
                    </div>
                @endif
                <div class="slot_box">
                    <a onclick=" @if (Auth::user()->check())window.open('{{route('pltslotiframe' , [ 'gamecode' => 'dnr' ] )}}', 'plt_slot', 'width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif  " href="javascript:void(0)">
                        <img width="150" height="150" alt="" src="{{ asset('/royalewin/img/plt/dnr.jpg') }}">
                    </a>
                    <span>Dolphin Reef</span>
                </div>
                
                @foreach( $lists as $list )
                    <div class="slot_box">
                        <a href="javascript:void(0)" onclick="@if (Auth::user()->check())window.open('{{route('pltslotiframe' , [ 'gamecode' => ($list->html5Code != '' ? $list->html5Code : $list->code) ] )}}', 'plt_slot', 'width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif ">
                            <img src="{{asset('/royalewin/img/plt/'.($list->code != '' ? $list->code : $list->html5Code).'.jpg')}}" alt=""/>
                        </a>

                        @if (Lang::getLocale() == 'cn')
                            <span>{{$list->gameNameCn}}</span>
                        @else
                            <span>{{$list->gameName}}</span>
                        @endif
                    </div>				 
                @endforeach

                <div class="clr"></div>
            </div>

            <div class="clr"></div>
        </div>
    </body>
</html>