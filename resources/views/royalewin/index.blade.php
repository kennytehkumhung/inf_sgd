@extends('royalewin/master')

@if (Session::get('currency') == 'MYR')
    @section('title', 'The Best Online Live Casino in Malaysia')
    @section('keywords', 'The Best Online Live Casino in Malaysia')
    @section('description', 'Play at RoyaleWin Online Casino Malaysia - awarded Best Online Casino and enjoy 110% Welcome Deposit Bonus. Join RoyaleWin casino online today.')
@elseif (Session::get('currency') == 'IDR')
    @section('title', 'Bandar Casino Online, Situs Judi Indonesia')
    @section('keywords', 'Bandar Casino Online, Situs Judi Indonesia')
    @section('description', 'RoyaleWin adalah agen bola, bandar casino online dan situs judi online terpercaya untuk Anda penggemar taruhan bola dan pecinta judi online di Indonesia.')
@endif

@section('css')
@parent
<style type="text/css">
    .midBGcontent3 { text-align: right; }
    .midBGcontent3 span { padding-right: 45px; }
    .popup_img { display: inline-block; position: absolute; z-index: 10001; margin: auto auto; top: 0px; bottom:0; left:0; right: 0; }
    h1.title { font-size: 14px; padding:0; margin: 0; }
    .fltLeft1New { color: #fff; text-align: left; vertical-align: middle; font-size: 11px; }
    .fd_title_row td { padding: 1px 0 1px 8px; }
    .fd_result_row td { color: #ffff00; font-size: 18px; font-weight: bold; text-align: center;  vertical-align: middle; padding: 3px; }
    .fltLeft1New .altcol { background-color: #383838; }
    .fltLeft1New .colfp { background-image: url('{{ static_asset('/img/red_star.png') }}'); background-position: 2px -1px; background-repeat: no-repeat; }

    .newsboard tbody * {
        color: #fff;
    }
</style>
@endsection

@section('js')
@parent
<script type="text/javascript">
    $(function () {
        $("#masklayer").click(function () {
            $(".popup_img").fadeOut(100);
            $("#masklayer").fadeOut(100);
        });
        
        load4dResults();
        setInterval(updateJackport, 3000);

        @if (isset($is_mobile_agent) && $is_mobile_agent == true)
            $("#tryMobileBox").slideDown(2000);
        @endif
    });
        
    function load4dResults() {
        // Load 4d data.
        var resultHolder = $("#holder_results_4d");
        var loading = $("#loading_4d");

        resultHolder.hide();
        loading.show();

        $.ajax({
            url: "{{ route('json4d') }}",
            type: "GET",
            dataType: "JSON",
            data: {
                _token: "{{ csrf_token() }}"
            },
            success: function (result) {
                for (var key in result) {
                    if (result.hasOwnProperty(key)) {
                        var resTd = null;

                        if (key == "Toto") {
                            resTd = "td:eq(0)";
                        } else if (key == "PMP") {
                            resTd = "td:eq(1)";
                        } else if (key == "Magnum") {
                            resTd = "td:eq(2)";
                        }

                        if (resTd != null) {
                            $(resTd, "#results_4d tr:eq(1)").text(result[key]["first"]);
                            $(resTd, "#results_4d tr:eq(3)").text(result[key]["second"]);
                            $(resTd, "#results_4d tr:eq(5)").text(result[key]["third"]);
                        }
                    }
                }
            },
            complete: function (jqXHR, textStatus ) {
                loading.hide();
                resultHolder.show();
            }
        });
    }
    
    function updateJackport() {
        var jps = ["jp_1", "jp_2", "jp_3"];
        
        for (var i = 0; i < jps.length; i++) {
            var val = $("span[id^='" + jps[i] + "']").text();
            val = removeComma(val);
        
            if (val > 1358889) {
                val = 1356289;
            }

            var value = Math.floor(Math.random() * 10);
            value = value * 0.027;

            var value2 = (parseFloat(val) + value).toFixed(2);

            $("#" + jps[i]).text(addCommas(value2));
        }
    }

    function addCommas(str) {
        var parts = (str + "").split("."),
            main = parts[0],
            len = main.length,
            output = "",
            i = len - 1;

        while (i >= 0) {
            output = main.charAt(i) + output;
            if ((len - i) % 3 === 0 && i > 0) {
                output = "," + output;
            }
            --i;
        }
        // put decimal part back
        if (parts.length > 1) {
            output += "." + parts[1];
        }
        return output;
    }

    function removeComma(str) {
        var parts = (str + "").split(",");
        var output = "";

        for (var i = 0; i < parts.length; i++) {
            output += parts[i];
        }
        return output;
    }
	
    function closePopup() {
        $(".popup_img").fadeOut(100);
        $("#masklayer").fadeOut(100);
    }

    function closeTryMobileBox() {
        $('#tryMobileBox').hide();
    }
</script>
@endsection

@section('content')

@if( $popup_banner )
    <div id="masklayer" style="height: 100%;position: fixed;opacity: 0.6;background-color: black;z-index: 10001;left: 0px;right: 0px;top: 0px;display:block;" onclick="closePopup();"></div>
    <img src="{!! $popup_banner !!}" class="popup_img" style="max-width: 800px;">
@endif

<div class="midSect" style="background-image: url('{{ asset('royalewin/img/landing2/bg-large.jpg') }}');">
{{--<div class="midSect">--}}
    <div class="sliderCont">
        <div class="sliderContInner">
            <div class="sliderImg" style="    background-image: url({{ asset('royalewin/img/landing2/slider-img-1.png') }});"></div>
            {{--<div class="sliderImg"></div>--}}
            <div class="slider">
                <!--Slider-->
                <div class="box_skitter box_skitter_large">
                    <ul>
                        @if(isset($banners))
                            @foreach( $banners as $key => $banner )
                                <li><a target="_blank" href="@if($banner->url != 'none'){{$banner->url}}@endif"><img src="{{$banner->domain}}/{{$banner->path}}" class="circles directionLeft" /></a></li>
                            @endforeach
                        @endif
                    </ul>
                </div>
                <!--Slider-->
            </div>
            <div class="clr"></div>
            <div class="sliderSelect">
                <div class="lcCont">
                    <a href="javascript:void(0);" onclick="$('.al_sportsbook').click();"><div class="showbox{{ Lang::getLocale() == 'id' ? '-id' : '' }}"></div></a>
                    <a href="{{ route('livecasino') }}"><div class="showbox2{{ Lang::getLocale() == 'id' ? '-id' : '' }}"></div></a>
                    @if (Session::get('currency') == 'MYR')
                        <a href="javascript:void(0);" onclick="@if (Auth::user()->check())window.open('{{route('jok')}}','jok_slot','width=1000,height=750')@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif"><div class="showbox1"></div></a>
                        <style type="text/css">
                            .showbox1, .showbox1:hover { background-image: url('{{ asset('royalewin/img/mid-jok.png') }}') !important; }
                        </style>
                    @else
                        <a href="javascript:void(0);" onclick="$('.al_slot').click();"><div class="showbox1{{ Lang::getLocale() == 'id' ? '-id' : '' }}"></div></a>
                    @endif
                    <a href="{{ route('slot', ['type' => 'sky']) }}"><div class="showbox4{{ Lang::getLocale() == 'id' ? '-id' : '' }}"></div></a>
                    @if (Session::get('currency') == 'MYR')
                        <a href="{{ route('4d') }}"><div class="showbox3"></div></a>
                    @else
                        <a href="{{ route('fbl') }}?page=intro"><div class="showbox3{{ Lang::getLocale() == 'id' ? '-id' : '' }}"></div></a>
                    @endif
                    @if (Session::get('currency') == 'MYR')
                        <a href="#"><div class="showbox5"></div></a>
                    @else
                        <a href="{{ route('mobile') }}"><div class="showbox5{{ Lang::getLocale() == 'id' ? '-id' : '' }}"></div></a>
                    @endif
                    <div class="clr"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="midSectInner">
        <div class="midBG">
            <div class="midBGbx ac1">
                @if (Session::get('currency') == 'IDR')
                    <div class="midBGhead"><h1 class="title">JADWAL BANK</h1></div>
                @else
                    <div class="midBGhead"><h1 class="title">4D {{ Lang::get('COMMON.RESULT') }}</h1></div>
                @endif
                <div class="midBGcontent">
                    @if (Session::get('currency') == 'IDR')
                        <img alt="" src="{{ static_asset('/img/id-bank-offline-w.png') }}" />
                    @else
                        <div id="loading_4d" style="text-align: center;"><span>{{ Lang::get('public.Loading') }}...</span></div>
                        <div id="holder_results_4d" style="cursor: pointer;" onclick="window.location.href='{{route('4d')}}';">
                            <div class="fltLeft1New">
                                <table id="results_4d" border="0" cellspacing="0" cellpadding="0" style="margin: 4px 0 0 0; width: 100%;">
                                    <tr class="fd_title_row">
                                        <td>{{ Lang::get('public.FirstPrize2') }}</td>
                                        <td class="altcol">{{ Lang::get('public.FirstPrize2') }}</td>
                                        <td>{{ Lang::get('public.FirstPrize2') }}</td>
                                    </tr>
                                    <tr class="fd_result_row">
                                        <td class="colfp"></td>
                                        <td class="colfp altcol"></td>
                                        <td class="colfp"></td>
                                    </tr>
                                    <tr class="fd_title_row">
                                        <td>{{ Lang::get('public.SecondPrize2') }}</td>
                                        <td class="altcol">{{ Lang::get('public.SecondPrize2') }}</td>
                                        <td>{{ Lang::get('public.SecondPrize2') }}</td>
                                    </tr>
                                    <tr class="fd_result_row">
                                        <td></td>
                                        <td class="altcol"></td>
                                        <td></td>
                                    </tr>
                                    <tr class="fd_title_row">
                                        <td>{{ Lang::get('public.ThirdPrize2') }}</td>
                                        <td class="altcol">{{ Lang::get('public.ThirdPrize2') }}</td>
                                        <td>{{ Lang::get('public.ThirdPrize2') }}</td>
                                    </tr>
                                    <tr class="fd_result_row">
                                        <td></td>
                                        <td class="altcol"></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td><img src="{{ static_asset('/img/index_toto.png') }}" style="max-width: 111px;"></td>
                                        <td><img src="{{ static_asset('/img/index_damachai.png') }}" style="max-width: 111px;"></td>
                                        <td><img src="{{ static_asset('/img/index_magnum.png') }}" style="max-width: 111px;"></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    @endif
                    <div class="clr"></div>
                </div>
            </div>
            <div class="midSep"></div>

            <div class="midBGbx">
                <div class="midBGhead2"><h1 class="title">{{ Lang::get('public.MobileApp') }}</h1></div>

                @if (Session::get('currency') == 'MYR')
                    <a href="#">
                        @if (Lang::getLocale() == 'cn')
                            <div class="midBGcontent2" style="background-image: url('{{static_asset('/img/mid-mob-bg-myr-cn.png')}}');"></div>
                        @else
                            <div class="midBGcontent2" style="background-image: url('{{static_asset('/img/mid-mob-bg-myr.png')}}');"></div>
                        @endif
                    </a>
                @else
                    <div class="midBGcontent2">
                        <div class="midBGcontentItem">
                            <img src="{{ static_asset('/img/mid-qr.png') }}" style="width: 82px; height: 82px;">
                        </div>
                        <div class="midBGcontentItem2">
                            <img src="{{ static_asset('/img/mid-mob-title.png') }}">
                        </div>
                        <div class="midBGcontentItem3">
                            @if (Lang::getLocale() == 'id')
                                Unduh aplikasi kami di ponsel Anda dan mulai Bermain<br>
                                Akun yang sama.<br>
                                Sandi yang sama
                            @else
                                Download our app on your mobile
                                and start Playing<br>
                                Same Account. <br>
                                Same Password
                            @endif
                        </div>
                    </div>
                @endif

            </div>
            <div class="midSep"></div>
            @if (isset($premier_league_thumbnail) && $premier_league_thumbnail)
                <div class="midBGbx2">
                    <div class="midBGhead3"><h1 class="title">Predict Score Now!</h1></div>
                    <div class="midBGcontent">
                        <a href="javascript:void(0);" onclick="@if(Auth::user()->check()) window.location.href='{{ route('premier-league') }}';@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}');@endif">
                        <!--<a href="javascript:void(0);" onclick="@if(Auth::user()->check()) alert('Maintenance!');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}');@endif">-->
                            <img alt="" src="{{ $premier_league_thumbnail }}" style="width: 313px; height: 166px;" />
                        </a>
                    </div>
                </div>
            @else
                <div class="midBGbx2">
                    <div class="midBGhead3"><h1 class="title">{{ Lang::get('public.Jackpot') }}</h1></div>
                    <div class="midBGcontent3">
                        <div class="midBGcounter1">
                            <span id="jp_1">1,356,300.79</span>
                        </div>
                        <div class="midBGcounter2">
                            <span id="jp_2">753,850.53</span>
                        </div>
                        <div class="midBGcounter3">
                            <span id="jp_3">5,200.81</span>
                        </div>
                    </div>
                </div>
            @endif
            <div class="clr"></div>
        </div>

        @if (in_array(Session::get('username'), array('badegg', 'test118', 'liangliang', 'peterchua')) && isset($news) && count($news) > 0)
        {{--@if (isset($news) && count($news) > 0)--}}
            <div style="width: 1024px; margin: 0 auto; padding-top: 15px;">
                <table class="newsboard" cellpadding="3" cellspacing="3" style="width: 100%;">
                    <thead>
                    <tr>
                        <td style="color: #ffae00; font-weight: bold;">
                            {{ Lang::get('COMMON.NEWS') }}
                        </td>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($news as $r)
                        <tr>
                            <td style="border-top: 1px solid #ffae00;">
                                <p>
                                    <span style="color: #ffae00; font-weight: bold;">{{ $r['title'] }}</span><br>
                                    <span style="color: #ffae00; font-size: 12px;">{{ $r['created_at'] }}</span>
                                </p>
                                <p>
                                    @if ($r['img_url'] != '')
                                        <img alt="" src="{{ $r['img_url'] }}" style="max-width: 450px; float: right;">
                                    @endif
                                    {!! $r['content'] !!}
                                </p>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        @endif
    </div>
</div>

<div id="tryMobileBox" style="display: none; background-color: #420001; border: 3px #e3bc31 solid; color: #fff; position: fixed; bottom: 0; right: 0; width: 500px; height: 350px; z-index: 10000;">
    <div style="position: relative;">
        <div style="text-align: right;">
            <img style="padding: 8px; cursor: pointer; width: auto; height: 30px;" onmouseout="this.src='{{ static_asset('/img/closeBtn.png') }}'" onmouseover="this.src='{{ static_asset('/img/closeBtn_hover.png') }}'" src="{{ static_asset('/img/closeBtn.png') }}" onclick="closeTryMobileBox();">
        </div>
        <div style="padding: 8px; text-align: center;">
            <span style="font-size: 3em;">{{ Lang::get('public.VisitOurMobileSiteForBetterExperience') }}</span><br><br><br>
            <table border="0" cellpadding="0" cellspacing="8" style="width: 100%; height: 90px;">
                <tr>
                    <td style="width: 50%;">
                        <button style="font-size: 2em; cursor: pointer; margin: 6px 0; height: 100%; width: 100%; border: none; background-color: #4CAF50; color: #fff; padding: 5px 0;" onclick="window.location.href='{{ route('switchwebtype') }}?type=m';">{{ Lang::get('COMMON.OK') }}</button><br>
                    </td>
                    <td>
                        <button style="font-size: 2em; cursor: pointer; margin: 6px 0; height: 100%; width: 100%; border: none; background-color: #f44336; color: #fff; padding: 5px 0;" onclick="closeTryMobileBox();">{{ Lang::get('public.NoThanks') }}</button><br>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>
@endsection

@section('footer_seo')
            <div class="footerContent">
                <div><h1 class="footerTitle">Top 3 Reasons RoyaleWin is the King of Online Casino</h1></div>
                <p>
                    RoyaleWin is one of the most reputable online casinos in Malaysia. If you are an avid gambler, you surely have heard of this casino. If you are a new player and looking to find an online casino that is fair, secured and fraudless, look no further! RoyaleWin is the place to go to.<br><br>
                    The first reason to love RoyaleWin is that it does not matter whether you are a new or seasoned player, small or big gambler, RoyaleWin has tons of games suitable to your liking. You can choose from live casinos, slot games, fishing world, sports betting, and many more. Furthermore, RoyaleWin is very active in giving discounts and promotions that give you more rewards and satisfaction! If you are late to join the fun, you might miss the many offers that RoyaleWin has to offer. <br><br>
                    The next reason to love this particular online casino is because they have a very professional, working around-the- clock customer service. Go to RoyaleWin’s social media pages and see the many satisfied customers that testify the customer service. There is also a live chat feature on the website, if you are in need of a quick help from the customer service.<br><br>
                    Last but not least, RoyaleWin is the best online casino in Malaysia because they have a mobile app! With a mobile app, you can play blackjack, slots, sports betting or any other casino games anywhere you want. The game quality in terms of graphics, connectivity and sound is not compromised even if you play on the mobile. This is because RoyaleWin uses state-of- the-art technology for the best user experience.<br><br>
                    It’s no wonder that RoyaleWin is the greatest, most popular casino in Malaysia as well as the Asia Pacific region. If you think lady luck is on your side, RoyaleWin is here to test it! You will surely love spending your time and money here. Do know where to stop and always gamble responsibly!<br><br>
                </p>
                <div class="footerTextL">
                    <div><h3 class="footerTitle">Why Royalewin</h3></div>
                    <div class="footerSub">Trusted Malaysia Online Casino - Your Premier Gaming Destination</div>
                    <p>
                        Play Baccarat, Blackjack, Roulette, Slots, Sports Betting with exciting promotions, 24/7 top of the line customer service & timely payouts with the highest level of security. <br><br>
                        Royalewin focused primarily in offering casino gaming products and services in Asia Pacific markets. Bringing you all the fun of a real casino in your home, we are committed to provide you gaming entertainment of premium quality and at exceptionally good value.<br><br>
                    </p>
                </div>
                <div class="footerTextL">
                    <div><h3 class="footerTitle">Our Games</h3></div>
                    <div class="footerSub">Live Dealer Casinos, Sportsbook, Slots, 4D</div>
                    <p>
                        Find a wide variety of top-class games, including classic favourites like Baccarat, Blackjack and Roulette, as well as popular themed slots, progressives, live casino with real dealers, live sportsbook betting, CFD (commodity, Forex, Index), and 4D.<br><br>
                        Royalewin offers superb quality games using WinningFT, MaxBet, Betsoft, XProGaming, Ho Gaming, AGGaming etc. softwares with no download required all in one advanced gaming platform.<br><br>
                    </p>
                </div>
                <div class="footerTextL">
                    <div><h3 class="footerTitle">24/7 Support</h3></div>
                    <div class="footerSub">24-Hour Customer Service</div>
                    <p>
                        Available 24 hours everyday, Royalewin Support Team is always there for you - to help answer your questions and resolve your issues as quickly and efficiently a possible.<br><br>
                        We focus on the needs of our customers by staying true to our core service principles: <br><br>
                        • The customer always come first  <br>
                        • Your satisfaction drives our business  <br>
                        • We will always strive to exceed your expectations  <br><br>
                    </p>
                </div>
                <div class="footerTextL">
                    <div><h3 class="footerTitle">Bonus Offers</h3></div>
                    <div class="footerSub">FREE Bonuses Up to 110%</div>
                    <p>
                        Royalewin loves to handsomely reward you for your support! Players at Royalewin.com are able to enjoy a fantastic selection of daily and weekly promotional offers. <br><br>
                        Our amazing range of promotions include an exclusive 110% Welcome Bonus for first-time depositors, double Daily Deposit Bonuses, Cashbacks for everything you played at our casino.<br><br>
                    </p>
                </div>
                <div class="clr"></div>
            </div>
@endsection