<!DOCTYPE html>
<html lang="{{ Lang::getLocale() }}">
<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>ROYALEWIN: The Best Online Live Casino in Malaysia and Indonesia</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="keywords" content="ROYALEWIN: The Best Online Live Casino in Malaysia and Indonesia">
    <meta name="description" content="Play at RoyaleWin Online Casino, awarded Best Online Casino and enjoy 110% Welcome Deposit Bonus. Join RoyaleWin casino online today.">
    <link href='//fonts.googleapis.com/css?family=Josefin+Slab:400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="{{ asset('/royalewin/img/landing2/css/demo.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('/royalewin/img/landing2/css/style.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('/royalewin/img/landing2/css/responsive.css') }}" />
    <script type="text/javascript" src="{{ asset('/royalewin/img/landing2/js/modernizr.custom.79639.js') }}"></script>
    <!--[if lte IE 8]>
    <link rel="stylesheet" type="text/css" href="{{ asset('/royalewin/img/landing2/css/simple.css') }}" />
    <![endif]-->
<!--previous bg header01_ori.jpg-->
    <style type="text/css">
        .bg-1 {
            background-image: url({{ asset('/royalewin/img/landing2/header01_cm.jpg') }});
{{--            background-image: url({{ asset('/royalewin/img/landing2/headerDV.jpg') }});--}}
        }
    </style>
</head>
<body>
<div class="container">
    <div class="st-container">
        <div class="st-scroll">

            <!-- Placeholder text from //hipsteripsum.me/ -->

            <section class="st-panel bg-1" id="st-panel-1">
			<!--previous logo2.png-->
                <div class="st-deco"><img src="{{ asset('/royalewin/img/landing2/logo2.png') }}" alt=""></div>
                <div class="btnWrap">
				<!--previous my-thumb_ori.png-->
				<!--previous idr-thumb_ori.png-->
                    <a href="{{url()}}/my" class="hvr-float"><img src="{{ asset('/royalewin/img/landing2/my-thumb_ori.png') }}"></a>
                    <a href="{{url()}}/id" class="hvr-float"><img src="{{ asset('/royalewin/img/landing2/idr-thumb_ori.png') }}"></a>
                </div>
            </section>
        </div><!-- // st-scroll -->
    </div><!-- // st-container -->
</div>
<div class="footer">
    <img class="left" src="{{ asset('/royalewin/img/landing2/provider.png') }}">
    <img class="right" src="{{ asset('/royalewin/img/landing2/social.png') }}">
    <div class="clr"></div>
</div>
</body>
</html>

<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
    ga('create', 'UA-41261240-1', 'auto');
    ga('send', 'pageview');
</script>
