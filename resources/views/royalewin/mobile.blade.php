
@extends('royalewin/master')

@if (Session::get('currency') == 'MYR')
    @section('title', 'Best Mobile Casino Malaysia - iPhone & Android Casino')
    @section('keywords', 'Best Mobile Casino Malaysia - iPhone & Android Casino')
    @section('description', 'Play at RoyaleWin Mobile Online Casino Malaysia - awarded Best Online Casino and enjoy 110% Welcome Deposit Bonus. Join RoyaleWin mobile casino online today.')
@elseif (Session::get('currency') == 'IDR')
    @section('title', 'Bandar Casino Online, Situs Judi Indonesia Indonesia - Mobile')
    @section('keywords', 'Bandar Casino Online, Situs Judi Indonesia Indonesia - Mobile')
    @section('description', 'RoyaleWin adalah bandar judi online Indonesia. Judi online di Aplikasi mobile seperti android dan apple untuk memudahkan anda.')
@endif

@section('css')
@parent

<link href="{{ asset('/royalewin/resources/css/leanmodal.css') }}" rel="stylesheet" type="text/css" />
<script src="{{url()}}/royalewin/resources/js/qrcode.js"></script>

<style type="text/css">
    .qr_thumbnail {
        height: 100px;
        width: 100px;
        padding: 0 0 8px 12px;
    }
    
    .lmHolder {
        top: 25px !important;
        display: none;
        z-index: 9999 !important;
        position: absolute;
        right: 0;
        bottom: 0;
        left: 0;
        width: 642px;
        margin: auto;
        overflow: auto;
        background: #fff;
        border-radius: 5px;
        padding: 1em 2em;
        max-height: 655px !important;
		background-color: black;
    }
    
    .btnClose {
        font-weight: bold;
        cursor: pointer;
        color: gray;
        font-size: 18px;
    }
	.text{
	display:block;
	position:relative;
	font-family: Arial, sans-serif;
	color:#e8b50b;
	font-size:30px;
	text-align:center;

		
}

.title01  {
	display: block;
	position: relative;
	width: 100%;
	text-align: center;
	color: #e8b50b;
	font-weight: bold;
	font-size: 22px;
	font-family: Arial;
	top:20px;
}

.title02  {
	display: block;
	position: relative;
	width: 100%;
	text-align: center;
	color: red;
	font-weight: bold;
	font-size: 22px;
	font-family: Arial;
	top:30px;
}


.title03  {
	display: block;
	position: relative;
	width: 100%;
	text-align: center;
	color: #fff;
	
	font-size: 16px;
	font-family: Arial;
	text-align:left;
	left:55px;
	top:50px;
}	
	
.bg{
	display: block;
	position: relative;
	width:570px;
	height: 600px;
	background-image: url({{asset('/royalewin/img/mobile/ag/bg.jpg')}})}

.Tlogo{
	display:block;
	position: relative;
	width:570px;
	height:70px;	
}

.Obg{
	display:block;
	position: absolute;
	width:530px;
	height:530px;
	top:60px;
	left:20px;}
	
.Lbg{
	background-image:url({{asset('/royalewin/img/mobile/ag/Lbg.png')}}); 
	display:block;
	position:absolute;
	width:530px;
	height:400px;
	bottom:3px;
	left:0px;
	}
	
.dBtn {
	display: block;
	position: relative;
	width: 100%
}
	
.dBtn a:link, .dBtn a:visited{
		background-image:url({{asset('/royalewin/img/mobile/ag/download_now.jpg')}});
		color:#FFFFFF;
		padding:14px 0px;
		text-align:center;
		text-decoration:none;
		display:block;
		position: relative;
		margin: 0 auto;
		width: 186px; 
		top:350px;
		
}
		
.dBtn a:hover {
		background-image:url({{asset('/royalewin/img/mobile/ag/download_now-hover.jpg')}});
		color: #000000;
		top:350px;
		}
		
		
.RWlogo  {
	display: block;
	position: relative;
	float: left;
	margin-left: 10px;
	margin-top: 4px;
}

.title01  {
	float: left;
	display: block;
	position: relative;
	width: auto;
	color: #efce50;
	font-family: "Times New Roman", Times, serif;
	font-size: 24px;
	margin-top: 18px;
	margin-left: 30px;
}
.title02  {
	display: block;
	position: relative;
	width: auto;
	color: #fff;
	font-family: "Times New Roman", Times, serif;
	font-size: 12px;
	font-weight: bold;
	margin-top: 12px;
	margin-bottom: 2px;
}

.box1  {
	display: block;
	position: relative;
	width: auto;
}

.box1 p  {
	display: block;
	position: relative;
	width: auto;
	font-family: "Times New Roman", Times, serif;
	font-size: 12px;
	color: #fff;
	margin: 0px 0px 10px 25px;
}

.box1 span  {
	color: #efce50;
}

.box2  {
	display: block;
	position: relative;
	width: 512px;
	margin: 25px 0px 0px 37px;
}

.box2 p  {
	display: block;
	position: relative;
	width: auto;
	font-family: "Times New Roman", Times, serif;
	font-size: 12px;
	color: #fff;
	margin: 0px 0px 10px 0px;
}

.box2 span  {
	color: #efce50;
}

.box2 label  {
	color: #fff;
	font-size: 14px;
	width: 120px;
	display: inline-block;
}

.box2  input  {
	border-radius: 5px;
	padding: 5px;
	border: 1px solid #fff;
}

.box3  {
	display: block;
	position: relative;
	width: auto;
}

.box3 p  {
	display: block;
	position: relative;
	width: auto;
	font-family: "Times New Roman", Times, serif;
	font-size: 12px;
	color: #fff;
	margin: 0px 0px 2px 0px;
}

.box3 span  {
	color: #efce50;
}

.a3  {
	margin-bottom: 8px;
}
.wrap  {
	display: block;
	position: relative;
	width: 570px;
	height: 600px;
	background-image: url({{asset('/royalewin/img/mobile/pt/pt-bg.jpg')}});
	background-position: top center;
	background-repeat: no-repeat;
	margin: 0 auto;
}

.dlBtn  {
	display: block;
	position: absolute;
	top: 0px;
	right: 20px;
	z-index: 999;
}

.dlBtn a:link, .dlBtn a:visited  {
	display: block;
	width: 140px;
	height: 35px;
	line-height: 35px;
	background: url({{asset('/royalewin/img/mobile/pt/dl-bg.jpg')}})repeat;
	text-decoration: none;
	color: #fff;
	font-weight: bold;
	text-align: center;
}

.dlBtn a:hover  {
	background: url({{asset('/royalewin/img/mobile/pt/dl-bg-hover.jpg')}})repeat;
	text-decoration: none;
}
.wrapM  {
	display: block;
	position: relative;
	width: 570px;
	height: 600px;
	background-image: url({{asset('/royalewin/img/mobile/pt/mxb-bg.jpg')}});
	background-position: top center;
	background-repeat: no-repeat;
	margin: 0 auto;
}
</style>



@endsection

@section('js')
@parent


<script src="{{ asset('/royalewin/resources/js/jquery.leanModal.min.js') }}"></script>
<script type="text/javascript">
    $(function($) {
        $("a[rel*=leanModal]").leanModal({
            overlay : 0.7,
            closeButton: ".btnClose"
        });
    });
</script>
@endsection

@section('content')
<div class="midSect bgLc">
    <div class="MDcont">
        <div class="MDbanner">
            <img src="{{ asset('/royalewin/img/MD-banner.jpg') }}" width="980" height="320">
        </div>

        <div class="MDbxCont">
            <div class="MDboxAG">
                <div class="qrDL">
                    <div class="MDtit">
                        <img src="{{ asset('/royalewin/img/ios-dl-tit.png') }}" width="125" height="40">
                    </div>
                    <div class="MDqr">
                      		<div class="QRcodeIMG" style="padding-top: 10px;background: white;margin-bottom: 12px;padding-bottom: 10px; margin-left: 14px; max-width: 86px; padding-left: 10px;">
								<input id="getLink5" type="text" value="{{$ios['agg']}}"	hidden>
									<a href="{{$ios['agg']}}">
									<div id="printQR5"></div>
									</a>
								</div>
                    </div>
                    <div class="MDlink">
                        <a href="#lmAgIos" rel="leanModal">{{ Lang::get('public.Download') }}</a>
                    </div>
                </div>
                <div class="qrDL">
                    <div class="MDtit">
                        <img src="{{ asset('/royalewin/img/andr-dl-tit.png') }}" width="125" height="40">
                    </div>
                    <div class="MDqr">
                       		<div class="QRcodeIMG" style="padding-top: 10px;background: white;margin-bottom: 12px;padding-bottom: 10px; margin-left: 14px; max-width: 86px; padding-left: 10px;">
								<input id="getLink6" type="text" value="{{$android['agg']}}"	hidden>
									<a href="{{$android['agg']}}">
									<div id="printQR6"></div>
									</a>
								</div>
                    </div>
                    <div class="MDlink">
                        <a href="#lmAgAndroid" rel="leanModal">{{ Lang::get('public.Download') }}</a>
                    </div>
                </div>
                <div class="clr"></div>
            </div>

			<div class="MDboxPT">
				<div class="qrDL">
					<div class="MDtit">
					
						<img src="{{ asset('/royalewin/img/andr-lc-dl-tit.png') }}" width="125" height="40">
					</div>
					<div class="MDqr">

								<div class="QRcodeIMG" style="padding-top: 10px;background: white;margin-bottom: 12px;padding-bottom: 10px; margin-left: 14px; max-width: 86px; padding-left: 10px;">
								<input id="getLink3" type="text" value="{{$android['pltlive']}}"	hidden>
									<a href="{{$android['pltlive']}}">
									<div id="printQR3"></div>
									</a>
								</div>
						</div>
					<div class="MDlink">
						<a href="#lmPtLive" rel="leanModal">{{ Lang::get('public.Download') }}</a>
					</div>
				</div>
				<div class="qrDL">
					<div class="MDtit">
						<img src="{{ asset('/royalewin/img/andr-slot-dl-tit.png') }}" width="125" height="40">
					</div>
					<div class="MDqr">
						<div class="QRcodeIMG" style="padding-top: 10px;background: white;margin-bottom: 12px;padding-bottom: 10px; margin-left: 14px; max-width: 86px; padding-left: 10px;">
								<input id="getLink4" type="text" value="{{$android['pltslot']}}"	hidden>
									<a href="{{$android['pltslot']}}">
									<div id="printQR4"></div>
									</a>
								</div>
					</div>
					<div class="MDlink">
						<a href="#lmPtSlot" rel="leanModal">{{ Lang::get('public.Download') }}</a>
					</div>
				</div>
				<div class="clr"></div>
			</div>
			
			
            <div class="MDboxMXB">
                <div class="qrDL">
                    <div class="MDtit">
                        <img src="{{ asset('/royalewin/img/andr-lc-dl-tit.png') }}" width="125" height="40">
                    </div>
                    <div class="MDqr">
                   
								<div class="QRcodeIMG" style="padding-top: 10px;background: white;margin-bottom: 12px;padding-bottom: 10px; margin-left: 14px; max-width: 86px; padding-left: 10px;">
								<input id="getLink2" type="text" value="{{$android['mxb']}}"	hidden>
									<a href="{{$android['mxb']}}">
									<div id="printQR2"></div>
									</a>
								</div>
                       
                    </div>
                    <div class="MDlink">
                        <a href="#lmMbLive" rel="leanModal">{{ Lang::get('public.Download') }}</a>
                    </div>
                </div>
                <div class="qrDL">
                    <div class="MDtit">
                        <img src="{{ asset('/royalewin/img/andr-slot-dl-tit.png') }}" width="125" height="40">
                    </div>
                    <div class="MDqr">
                  
						<div class="QRcodeIMG " style="padding-top: 10px;background: white;margin-bottom: 12px;padding-bottom: 10px; margin-left: 15px; max-width: 95px;">
							<input id="getLink1" type="text" value="{{$android['mxb']}}"	hidden>
							<a href="{{$android['mxb']}}">
							<center>
							<div id="printQR1" ></div>							
							</center></a>
						</div>

                       
                    </div>
                    <div class="MDlink">
                        <a href="#lmMbSlot" rel="leanModal">{{ Lang::get('public.Download') }}</a>
                    </div>
                </div>
                <div class="clr"></div>
            </div>
            
            <div class="MDboxGP">
                <div class="qrDL">
                    <div class="MDtit">
                        <img src="{{ asset('/royalewin/img/dt-dl-tit.png') }}" width="125" height="40">
                    </div>
                    <div class="MDqr">
                       	<div class="QRcodeIMG " style="padding-top: 10px;background: white;margin-bottom: 12px;padding-bottom: 10px; margin-left: 15px; max-width: 95px;">
							<input id="getLink7" type="text" value="{{$pc['pltpc']}}"	hidden>
							<a href="{{$pc['pltpc']}}">
							<center>
							<div id="printQR7" ></div>							
							</center></a>
						</div>
                    </div>
                    <div class="MDlink">
                        <a href="#lmPtDesktop" rel="leanModal">{{ Lang::get('public.Download') }}</a>
                    </div>
                </div>
                <div class="clr"></div>
            </div>

			<div class="MDboxJOK">
				{{--<div class="qrDL">--}}
					{{--<div class="MDtit">--}}
						{{--<img src="{{ asset('/royalewin/img/dt-dl-tit.png') }}" width="125" height="40">--}}
					{{--</div>--}}
					{{--<div class="MDqr">--}}
						{{--<img src="{{ asset('/royalewin/img/qr/jok_pc.png') }}" class="qr_thumbnail">--}}
					{{--</div>--}}
					{{--<div class="MDlink">--}}
						{{--<a href="#lmJokDesktop" rel="leanModal">{{ Lang::get('public.Download') }}</a>--}}
					{{--</div>--}}
				{{--</div>--}}
				<div class="qrDL">
					<div class="MDtit">
						<img src="{{ asset('/royalewin/img/ios-dl-tit.png') }}" width="125" height="40">
					</div>
					<div class="MDqr">
						<div class="MDqr">
                       	<div class="QRcodeIMG " style="padding-top: 10px;background: white;margin-bottom: 12px;padding-bottom: 10px; margin-left: 15px; max-width: 95px;">
							<input id="getLink8" type="text" value="{{$ios['jok']}}"	hidden>
							<a href="{{$ios['jok']}}">
							<center>
							<div id="printQR8" ></div>							
							</center></a>
						</div>
                    </div>
					</div>
					<div class="MDlink">
						<a href="#lmJok" rel="leanModal">{{ Lang::get('public.Download') }}</a>
					</div>
				</div>
				<div class="qrDL">
					<div class="MDtit">
						<img src="{{ asset('/royalewin/img/andr-lc-dl-tit.png') }}" width="125" height="40">
					</div>
					<div class="MDqr">
						<div class="MDqr">
                       	<div class="QRcodeIMG " style="padding-top: 10px;background: white;margin-bottom: 12px;padding-bottom: 10px; margin-left: 15px; max-width: 95px;">
							<input id="getLink9" type="text" value="{{$android['jok']}}"	hidden>
							<a href="{{$android['jok']}}">
							<center>
							<div id="printQR9" ></div>							
							</center></a>
						</div>
                    </div>
					</div>
					<div class="MDlink">
						<a href="#lmJok" rel="leanModal">{{ Lang::get('public.Download') }}</a>
					</div>
				</div>
				<div class="clr"></div>
			</div>

			@if (Session::get('currency') == 'MYR')
				<div class="MDboxSKY">
					<div class="qrDL">
						<div class="MDtit">
							<img src="{{ asset('/royalewin/img/ios-dl-tit.png') }}" width="125" height="40">
						</div>
						<div class="MDqr">							
							    
						<div class="QRcodeIMG " style="padding-top: 10px;background: white;margin-bottom: 12px;padding-bottom: 10px; margin-left: 15px; max-width: 95px;">
							<input id="getLink10" type="text" value="{{$ios['sky']}}"	hidden>
							<a href="{{$ios['sky']}}">
							<center>
							<div id="printQR10" ></div>							
							</center></a>
						</div>
						</div>
						<div class="MDlink">
							<a href="#lmSkyIos" rel="leanModal">{{ Lang::get('public.Download') }}</a>
						</div>
					</div>
					<div class="qrDL">
						<div class="MDtit">
							<img src="{{ asset('/royalewin/img/andr-dl-tit.png') }}" width="125" height="40">
						</div>
						<div class="MDqr">
							<div class="QRcodeIMG " style="padding-top: 10px;background: white;margin-bottom: 12px;padding-bottom: 10px; margin-left: 15px; max-width: 95px;">
							<input id="getLink11" type="text" value="{{$android['sky']}}"	hidden>
							<a href="{{$android['sky']}}">
							<center>
							<div id="printQR11" ></div>							
							</center></a>
							</div>
						</div>
						<div class="MDlink">
							<a href="#lmSkyAndroid" rel="leanModal">{{ Lang::get('public.Download') }}</a>
						</div>
					</div>
					<div class="clr"></div>
				</div>

				<div class="MDboxSCR">
					<div class="qrDL">
						<div class="MDtit">
							<img src="{{ asset('/royalewin/img/ios-dl-tit.png') }}" width="125" height="40">
						</div>
						<div class="MDqr">
							<div class="QRcodeIMG " style="padding-top: 10px;background: white;margin-bottom: 12px;padding-bottom: 10px; margin-left: 15px; max-width: 95px;">
							<input id="getLink12" type="text" value="{{$ios['scr']}}"	hidden>
							<a href="{{$ios['scr']}}">
							<center>
							<div id="printQR12" ></div>							
							</center></a>
							</div>
						</div>
						<div class="MDlink">
							<a href="#lmScrIos" rel="leanModal">{{ Lang::get('public.Download') }}</a>
						</div>
					</div>
					<div class="qrDL">
						<div class="MDtit">
							<img src="{{ asset('/royalewin/img/andr-dl-tit.png') }}" width="125" height="40">
						</div>
						<div class="MDqr">
							<div class="QRcodeIMG " style="padding-top: 10px;background: white;margin-bottom: 12px;padding-bottom: 10px; margin-left: 15px; max-width: 95px;">
							<input id="getLink13" type="text" value="{{$android['scr']}}"	hidden>
							<a href="{{$android['scr']}}">
							<center>
							<div id="printQR13" ></div>							
							</center></a>
							</div>
						</div>
						<div class="MDlink">
							<a href="#lmScrAndroid" rel="leanModal">{{ Lang::get('public.Download') }}</a>
						</div>
					</div>
					<div class="clr"></div>
				</div>

				<div class="MDboxALB">
					<div class="qrDL">
						<div class="MDtit">
							<img src="{{ asset('/royalewin/img/ios-dl-tit.png') }}" width="125" height="40">
						</div>
						<div class="MDqr">
							<div class="QRcodeIMG " style="padding-top: 10px;background: white;margin-bottom: 12px;padding-bottom: 10px; margin-left: 15px; max-width: 95px;">
							<input id="getLink14" type="text" value="{{$ios['alb']}}"	hidden>
							<a href="{{$ios['alb']}}">
							<center>
							<div id="printQR14" ></div>							
							</center></a>
							</div>
						</div>		
						<div class="MDlink">
							<a href="#lmAlbIos" rel="leanModal">{{ Lang::get('public.Download') }}</a>
						</div>
					</div>
					<div class="qrDL">
						<div class="MDtit">
							<img src="{{ asset('/royalewin/img/andr-dl-tit.png') }}" width="125" height="40">
						</div>
						<div class="MDqr">
							<div class="MDqr">
							<div class="QRcodeIMG " style="padding-top: 10px;background: white;margin-bottom: 12px;padding-bottom: 10px; margin-left: 15px; max-width: 95px;">
							<input id="getLink15" type="text" value="{{$android['alb']}}"	hidden>
							<a href="{{$android['alb']}}">
							<center>
							<div id="printQR15" ></div>							
							</center></a>
							</div>
						</div>	
						</div>
						<div class="MDlink">
							<a href="#lmAlbAndroid" rel="leanModal">{{ Lang::get('public.Download') }}</a>
						</div>
					</div>
					<div class="clr"></div>
				</div>
			@endif
            
            {{--<div class="MDboxOPU">--}}
                {{--<div class="qrDL">--}}
                    {{--<div class="MDtit">--}}
                        {{--<img src="{{ asset('/royalewin/img/ios-dl-tit.png') }}" width="125" height="40">--}}
                    {{--</div>--}}
                    {{--<div class="MDqr">--}}
                        {{--<img src="{{ asset('/royalewin/img/qr.png') }}" class="qr_thumbnail">--}}
                    {{--</div>--}}
                    {{--<div class="MDlink">--}}
                        {{--<a href="#lmAgIos" rel="leanModal">{{ Lang::get('public.Download') }}</a>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="qrDL">--}}
                    {{--<div class="MDtit">--}}
                        {{--<img src="{{ asset('/royalewin/img/andr-dl-tit.png') }}" width="125" height="40">--}}
                    {{--</div>--}}
                    {{--<div class="MDqr">--}}
                        {{--<img src="{{ asset('/royalewin/img/qr/agg_android.png') }}" class="qr_thumbnail">--}}
                    {{--</div>--}}
                    {{--<div class="MDlink">--}}
                        {{--<a href="#lmAgAndroid" rel="leanModal">{{ Lang::get('public.Download') }}</a>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="clr"></div>--}}
            {{--</div>--}}
            
            <div class="clr"></div>
        </div>
    </div>
</div>
]
<!-- Modals -->
<div id="lmAgAndroid" class="lmHolder">
    <div class="modal-content">
        <div style="text-align: right;"><span class="btnClose">x</span></div>
        <br>
  
            <div class="bg">
				<div class="Tlogo">
					<img src="{{asset('/royalewin/img/mobile/ag/top.png')}}" alt="Tlogo">
				</div>

				<div class="title01">Few Easy steps to Play Live Casino</div> 
<br>
<br>

				<div class="title02">Android and IOS Version</div> 

				<div class="title03">Step 1 : Kindly access crown casino and refer to the top image.
				<br>Step 2 : Click on the Try Now Mobile icon on the bottom left of the </br>
							Casino Lobby.
				<br>Step 3 : Follow the steps that is shown at the installation Guide to</br>
							Download and Login Via Mobile.</div>


				<div class="Lbg">

				<div class="dBtn">
                    <a href="{{$android['agg']}}" target="_blank">DOWNLOAD NOW</a>
				</div>
					</div>

				   
				
			</div>
    
    </div>
</div>

<div id="lmAgIos" class="lmHolder">
    <div class="modal-content">
        <div style="text-align: right;"><span class="btnClose">x</span></div>
        <br>
    
             <div class="bg">
				<div class="Tlogo">
					<img src="{{asset('/royalewin/img/mobile/ag/top.png')}}" alt="Tlogo">
				</div>

				<div class="title01">Few Easy steps to Play Live Casino</div> 
<br>
<br>

				<div class="title02">Android and IOS Version</div> 

				<div class="title03">Step 1 : Kindly access crown casino and refer to the top image.
				<br>Step 2 : Click on the Try Now Mobile icon on the bottom left of the </br>
							Casino Lobby.
				<br>Step 3 : Follow the steps that is shown at the installation Guide to</br>
							Download and Login Via Mobile.</div>


				<div class="Lbg">

				<div class="dBtn">
					<a href="{{$ios['agg']}}" target="_blank">DOWNLOAD NOW</a>
				</div>
					</div>

				   
				
			</div>
       
    </div>
</div>

<div id="lmPtSlot" class="lmHolder">
    <div class="modal-content">
        <div style="text-align: right;"><span class="btnClose">x</span></div>
        <br>
      
            <div class="wrap">
				<div class="headerT">
				<div class="RWlogo">
				  <img src="{{asset('/royalewin/img/mobile/pt/rw-logo.png')}}" width="220">
				</div>
				<div class="title01">
				Play Any Time Any Where
				</div>

				<div class="clr"></div>
				</div>

				<div class="box1">
				<div class="dlBtn">
					@if(Session::get('currency') == 'IDR')
						<a href="//m.gm175888.com/download.html" target="_blank">DOWNLOAD NOW</a>
					@else
						<a href="{{$android['pltslot']}}" target="_blank">DOWNLOAD NOW</a>
					@endif
				</div>
				<p><span>Step 1</span> Click on the Download button</p>
				<p><span>Step 2</span> Run apps, and install the file after download</p>
                @if(Session::get('currency') == 'IDR')
                    <p><span>Step 3</span> login with your ROYALEWIN username by adding a prefix  "RYW_"   infront of your username </p>
                @else
                    <p><span>Step 3</span> login with your ROYALEWIN username by adding a prefix  "RYL_"   infront of your username </p>
                @endif
				</div>

				<div class="box1">
				<p><span class="att">Attention! Username field must be ALL with Capital Letter </span></p>
				</div>

				<div class="box1">
							@if(Session::has('username'))
                                @if(Session::get('currency') == 'IDR')
                                    <p>RYW_{{strtoupper(Session::get('username'))}}<p>
                                @else
                                    <p>RYL_{{strtoupper(Session::get('username'))}}<p>
                                @endif
							@else
								<p>Please Login!<p>
							@endif
				</div>

				

			
			</div>
     
    </div>
</div>

<div id="lmPtLive" class="lmHolder">
    <div class="modal-content">
        <div style="text-align: right;"><span class="btnClose">x</span></div>
        <div class="wrap">
				<div class="headerT">
				<div class="RWlogo">
				  <img src="{{asset('/royalewin/img/mobile/pt/rw-logo.png')}}" width="220">
				</div>
				<div class="title01">
				Play Any Time Any Where
				</div>

				<div class="clr"></div>
				</div>

				<div class="box1">
				<div class="dlBtn">
					@if(Session::get('currency') == 'IDR')
						<a href="//m.gm175888.com/live/download.html" target="_blank">DOWNLOAD NOW</a>
					@else
						<a href="{{$android['pltlive']}}" target="_blank">DOWNLOAD NOW</a>
					@endif
				</div>
				<p><span>Step 1</span> Click on the Download button</p>
				<p><span>Step 2</span> Run apps, and install the file after download</p>
                @if(Session::get('currency') == 'IDR')
                    <p><span>Step 3</span> login with your ROYALEWIN username by adding a prefix  "RYW_"   infront of your username </p>
                @else
                    <p><span>Step 3</span> login with your ROYALEWIN username by adding a prefix  "RYL_"   infront of your username </p>
                @endif
				</div>

				<div class="box1">
				<p><span class="att">Attention! Username field must be ALL with Capital Letter </span></p>
				</div>

				<div class="box1">
							@if(Session::has('username'))
                                @if(Session::get('currency') == 'IDR')
                                    <p>RYW_{{strtoupper(Session::get('username'))}}<p>
                                @else
                                    <p>RYL_{{strtoupper(Session::get('username'))}}<p>
                                @endif
							@else
								<p>Please Login!<p>
							@endif
				</div>

				

			
			</div>
    </div>
</div>

<div id="lmMbSlot" class="lmHolder">
    <div class="modal-content">
        <div style="text-align: right;"><span class="btnClose">x</span></div>
        <br>
           <div class="wrapM">
				<div class="headerT">
				<div class="RWlogo">
				  <img src="{{asset('/royalewin/img/mobile/pt/rw-logo.png')}}" width="220">
				</div>
				<div class="title01">
				Play Any Time Any Where
				</div>

				<div class="clr"></div>
				</div>

				<div class="box1">
				<div class="dlBtn">
          

                    <a href="{{$android['mxb']}}" >DOWNLOAD</a>
				<!--<a href="//myroyalewin.net/royalewin/downloads/mxb-android-myr.apk">DOWNLOAD</a>-->
				</div>
				<p><span>Step 1</span> Click on the Download button</p>
				<p><span>Step 2</span> Run apps, and install the file after download</p>
				<p><span>Step 3</span> login with your ROYALEWIN username </p>
				</div>

				<div class="box1">
				<p><span class="att">Attention! Username field must be ALL with Capital Letter </span></p>
				</div>

				<div class="box1">
							@if(Session::has('username'))
								<p>{{strtoupper(Session::get('username'))}}<p>
							@else
								<p>Please Login!<p>
							@endif
				</div>

				

			
			</div>
    </div>
</div>

<div id="lmMbLive" class="lmHolder">
    <div class="modal-content">
        <div style="text-align: right;"><span class="btnClose">x</span></div>
           <div class="wrapM">
				<div class="headerT">
				<div class="RWlogo">
				  <img src="{{asset('/royalewin/img/mobile/pt/rw-logo.png')}}" width="220">
				</div>
				<div class="title01">
				Play Any Time Any Where
				</div>

				<div class="clr"></div>
				</div>

				<div class="box1">
				<div class="dlBtn">
                @if (Session::get('currency') == 'IDR')
                    <a href="{{$android['mxb']}}">DOWNLOAD</a>
                @else
                    <a href="{{$android['mxb']}}">DOWNLOAD</a>
                @endif
				</div>
				<p><span>Step 1</span> Click on the Download button</p>
				<p><span>Step 2</span> Run apps, and install the file after download</p>
				<p><span>Step 3</span> login with your ROYALEWIN username </p>
				</div>

				<div class="box1">
				<p><span class="att">Attention! Username field must be ALL with Capital Letter </span></p>
				</div>

				<div class="box1">
					@if(Session::has('username'))
						<p>{{strtoupper(Session::get('username'))}}<p>
					@else
						<p>Please Login!<p>
					@endif
				</div>

			</div>
    </div>
</div>

<div id="lmPtDesktop" class="lmHolder">
    <div class="modal-content">
        <div style="text-align: right;"><span class="btnClose">x</span></div>
         <div class="wrap">
				<div class="headerT">
				<div class="RWlogo">
				  <img src="{{asset('/royalewin/img/mobile/pt/rw-logo.png')}}" width="220">
				</div>
				<div class="title01">
				Play Any Time Any Where
				</div>

				<div class="clr"></div>
				</div>

				<div class="box1">
				<div class="dlBtn">
					@if(Session::get('currency') == 'IDR')
						<a href="//cdn.fruitfarm88.com/generic/d/setupglx.exe" target="_blank">DOWNLOAD</a>
					@else
						<a href="{{$pc['pltpc']}}" target="_blank">DOWNLOAD</a>
					@endif
				</div>
				<p><span>Step 1</span> Click on the Download button</p>
				<p><span>Step 2</span> Run apps, and install the file after download</p>
                @if(Session::get('currency') == 'IDR')
                    <p><span>Step 3</span> login with your ROYALEWIN username by adding a prefix  "RYW_"   infront of your username </p>
                @else
                    <p><span>Step 3</span> login with your ROYALEWIN username by adding a prefix  "RYL_"   infront of your username </p>
                @endif
				</div>

				<div class="box1">
				<p><span class="att">Attention! Username field must be ALL with Capital Letter </span></p>
				</div>

				<div class="box1">
                    @if(Session::has('username'))
                        @if(Session::get('currency') == 'IDR')
                            <p>RYW_{{strtoupper(Session::get('username'))}}<p>
                        @else
                            <p>RYL_{{strtoupper(Session::get('username'))}}<p>
                        @endif
					@else
						<p>Please Login!<p>
					@endif
				</div>

				

			
			</div>
    </div>
</div>

<div id="lmJok" class="lmHolder">
    <div class="modal-content">
        <div style="text-align: right;"><span class="btnClose">x</span></div>
           <div class="wrapM">
				<div class="headerT">
				<div class="RWlogo">
				  <img src="{{asset('/royalewin/img/mobile/pt/rw-logo.png')}}" width="220">
				</div>
				<div class="title01">
				Play Any Time Any Where
				</div>

				<div class="clr"></div>
				</div>

				<div class="box1">
				<div class="dlBtn">
                    <a href="{{$ios['jok']}}" target="_blank">DOWNLOAD</a>
				</div>
				<p><span>Step 1</span> Click on the Download button</p>
				<p><span>Step 2</span> Run apps, and install the file after download</p>
                @if(Session::get('currency') == 'IDR')
                    <p><span>Step 3</span> login with your ROYALEWIN username by adding a prefix  "{{ Config::get(Session::get('currency').'.jok.appid') }}.RWI_"   infront of your username </p>
                @else
                    <p><span>Step 3</span> login with your ROYALEWIN username by adding a prefix  "{{ Config::get(Session::get('currency').'.jok.appid') }}.RWM_"   infront of your username </p>
                @endif
				</div>

				<div class="box1">
				<p><span class="att">Attention! Username field must be ALL with Capital Letter </span></p>
				</div>

				<div class="box1">
                    @if(Session::has('username'))
                        @if(Session::get('currency') == 'IDR')
                            <p>{{ Config::get(Session::get('currency').'.jok.appid') }}.RWI_{{strtoupper(Session::get('username'))}}<p>
                        @else
                            <p>{{ Config::get(Session::get('currency').'.jok.appid') }}.RWM_{{strtoupper(Session::get('username'))}}<p>
                        @endif
					@else
						<p>Please Login!<p>
					@endif
				</div>

			</div>
    </div>
</div>

<div id="lmJokDesktop" class="lmHolder">
    <div class="modal-content">
        <div style="text-align: right;"><span class="btnClose">x</span></div>
           <div class="wrapM">
				<div class="headerT">
				<div class="RWlogo">
				  <img src="{{asset('/royalewin/img/mobile/pt/rw-logo.png')}}" width="220">
				</div>
				<div class="title01">
				Play Any Time Any Where
				</div>

				<div class="clr"></div>
				</div>

				<div class="box1">
				<div class="dlBtn">
                    <a href="{{$android['jok']}}" target="_blank">DOWNLOAD</a>
				</div>
				<p><span>Step 1</span> Click on the Download button</p>
				<p><span>Step 2</span> Run apps, and install the file after download</p>
                @if(Session::get('currency') == 'IDR')
                    <p><span>Step 3</span> login with your ROYALEWIN username by adding a prefix  "{{ Config::get(Session::get('currency').'.jok.appid') }}.RYW_"   infront of your username </p>
                @else
                    <p><span>Step 3</span> login with your ROYALEWIN username by adding a prefix  "{{ Config::get(Session::get('currency').'.jok.appid') }}.RYL_"   infront of your username </p>
                @endif
				</div>

				<div class="box1">
				<p><span class="att">Attention! Username field must be ALL with Capital Letter </span></p>
				</div>

				<div class="box1">
                    @if(Session::has('username'))
                        @if(Session::get('currency') == 'IDR')
                            <p>{{ Config::get(Session::get('currency').'.jok.appid') }}.RYW_{{strtoupper(Session::get('username'))}}<p>
                        @else
                            <p>{{ Config::get(Session::get('currency').'.jok.appid') }}.RYL_{{strtoupper(Session::get('username'))}}<p>
                        @endif
					@else
						<p>Please Login!<p>
					@endif
				</div>

			</div>
    </div>
</div>

<div id="lmScrIos" class="lmHolder">
	<div class="modal-content">
		<div style="text-align: right;"><span class="btnClose">x</span></div>
		<div class="wrapM">
			<div class="headerT">
				<div class="RWlogo">
					<img src="{{asset('/royalewin/img/mobile/pt/rw-logo.png')}}" width="220">
				</div>
				<div class="title01">
					Play Any Time Any Where
				</div>

				<div class="clr"></div>
			</div>

			<div class="box1">
				<div class="dlBtn">
					<a href="{{$ios['scr']}}" target="_blank">DOWNLOAD</a>
				</div>
				<p><span>Step 1</span> Click on the Download button</p>
				<p><span>Step 2</span> Run apps, and install the file after download</p>
				@if(Auth::user()->check())
					<p><span>Step 3</span> login with: {{ (new \App\libraries\providers\SCR())->getScrIdByUsername(Session::get('username'), Session::get('currency')) }}</p>
					<p><strong>Attention! For first time login please use this password: {{ (new \App\libraries\providers\SCR())->formatPlayerPassword('') }}[your login password]</strong></p>
				@else
					<p>Please Login!<p>
				@endif
			</div>

		</div>
	</div>
</div>

<div id="lmScrAndroid" class="lmHolder">
	<div class="modal-content">
		<div style="text-align: right;"><span class="btnClose">x</span></div>
		<div class="wrapM">
			<div class="headerT">
				<div class="RWlogo">
					<img src="{{asset('/royalewin/img/mobile/pt/rw-logo.png')}}" width="220">
				</div>
				<div class="title01">
					Play Any Time Any Where
				</div>

				<div class="clr"></div>
			</div>

			<div class="box1">
				<div class="dlBtn">
					<a href="{{$android['scr']}}" target="_blank">DOWNLOAD</a>
				</div>
				<p><span>Step 1</span> Click on the Download button</p>
				<p><span>Step 2</span> Run apps, and install the file after download</p>
				@if(Auth::user()->check())
					<p><span>Step 3</span> login with: {{ (new \App\libraries\providers\SCR())->getScrIdByUsername(Session::get('username'), Session::get('currency')) }}</p>
					<p><strong>Attention! For first time login please use this password: {{ (new \App\libraries\providers\SCR())->formatPlayerPassword('') }}[your login password]</strong></p>
				@else
					<p>Please Login!<p>
				@endif
			</div>

		</div>
	</div>
</div>

<div id="lmSkyIos" class="lmHolder">
    <div class="modal-content">
        <div style="text-align: right;"><span class="btnClose">x</span></div>
        <div class="wrapM">
            <div class="headerT">
                <div class="RWlogo">
                    <img src="{{asset('/royalewin/img/mobile/pt/rw-logo.png')}}" width="220">
                </div>
                <div class="title01">
                    Play Any Time Any Where
                </div>

                <div class="clr"></div>
            </div>

            <div class="box1">
                <div class="dlBtn">
                    <a href="{{$ios['sky']}}" target="_blank">DOWNLOAD</a>
                </div>
                <p><span>Step 1</span> Click on the Download button</p>
                <p><span>Step 2</span> Run apps, and install the file after download</p>
                @if(Auth::user()->check())
                    <p><span>Step 3</span> login with: {{Config::get(Session::get('currency').'.sky.agid').((new \App\libraries\providers\SKY())->formatUserId(Session::get('userid')))}} </p>
                @else
                    <p>Please Login!<p>
                @endif
            </div>

            <div class="box1">
                <p><span class="att">Attention! Username field must be ALL with Capital Letter </span></p>
            </div>

        </div>
    </div>
</div>

<div id="lmSkyAndroid" class="lmHolder">
    <div class="modal-content">
        <div style="text-align: right;"><span class="btnClose">x</span></div>
        <div class="wrapM">
            <div class="headerT">
                <div class="RWlogo">
                    <img src="{{asset('/royalewin/img/mobile/pt/rw-logo.png')}}" width="220">
                </div>
                <div class="title01">
                    Play Any Time Any Where
                </div>

                <div class="clr"></div>
            </div>

            <div class="box1">
                <div class="dlBtn">
                    <a href="{{$android['sky']}}" target="_blank">DOWNLOAD</a>
                </div>
                <p><span>Step 1</span> Click on the Download button</p>
                <p><span>Step 2</span> Run apps, and install the file after download</p>
                @if(Auth::user()->check())
                    <p><span>Step 3</span> login with: {{Config::get(Session::get('currency').'.sky.agid').((new \App\libraries\providers\SKY())->formatUserId(Session::get('userid')))}} </p>
                @else
                    <p>Please Login!<p>
                @endif
            </div>

            <div class="box1">
                <p><span class="att">Attention! Username field must be ALL with Capital Letter </span></p>
            </div>

        </div>
    </div>
</div>

<div id="lmAlbIos" class="lmHolder">
    <div class="modal-content">
        <div style="text-align: right;"><span class="btnClose">x</span></div>
        <div class="wrapM">
            <div class="headerT">
                <div class="RWlogo">
                    <img src="{{asset('/royalewin/img/mobile/pt/rw-logo.png')}}" width="220">
                </div>
                <div class="title01">
                    Play Any Time Any Where
                </div>

                <div class="clr"></div>
            </div>

            <div class="box1">
                <div class="dlBtn">
                    <a href="{{$ios['alb']}}" target="_blank">DOWNLOAD</a>
                </div>
                <p><span>Step 1</span> Click on the Download button</p>
                <p><span>Step 2</span> Run apps, and install the file after download</p>
                @if(Auth::user()->check())
                    <p><span>Step 3</span> login with: {{ strtolower(Config::get(Session::get('currency').'.alb.prefix')).'_'.strtolower(Session::get('username')).'av2' }} </p>
                @else
                    <p>Please Login!<p>
                @endif
            </div>

            <div class="box1">
                <p><span class="att">Attention! Username field must be ALL with Capital Letter </span></p>
            </div>

        </div>
    </div>
</div>

<div id="lmAlbAndroid" class="lmHolder">
    <div class="modal-content">
        <div style="text-align: right;"><span class="btnClose">x</span></div>
        <div class="wrapM">
            <div class="headerT">
                <div class="RWlogo">
                    <img src="{{asset('/royalewin/img/mobile/pt/rw-logo.png')}}" width="220">
                </div>
                <div class="title01">
                    Play Any Time Any Where
                </div>

                <div class="clr"></div>
            </div>

            <div class="box1">
                <div class="dlBtn">
                    <a href="{{$android['alb']}}" target="_blank">DOWNLOAD</a>
                </div>
                <p><span>Step 1</span> Click on the Download button</p>
                <p><span>Step 2</span> Run apps, and install the file after download</p>
                @if(Auth::user()->check())
                    <p><span>Step 3</span> login with: {{ strtolower(Config::get(Session::get('currency').'.alb.prefix')).'_'.strtolower(Session::get('username')).'av2' }} </p>
                @else
                    <p>Please Login!<p>
                @endif
            </div>

            <div class="box1">
                <p><span class="att">Attention! Username field must be ALL with Capital Letter </span></p>
            </div>

        </div>
    </div>
</div>
@endsection

@section('footer_seo')
            <div class="footerContent">
                <div><h1 class="footerTitle">RoyaleWin Mobile Live Casino Games Malaysia</h1></div>
                <p>
                Thanks to advances in technology, not only that you can play live casino games at the comfort of your couch, you can also play in on your mobile. Mobile casinos are the current hype going on in Malaysia. This is because of its convenience and you don’t have to bring your laptop everywhere just to do your mobile betting. RoyaleWin is perhaps the only provider that provides the most extensive mobile
                casino choices in Malaysia. There are more than 5 mobile casino providers available inside RoyaleWin and all of them are well known in the industry. We will review the top three of them here.<br><br>
                The first one will be 918Kiss mobile casino. Previously known as SCR888, the rebranding has clearly help in elevating their popularity to greater heights while maintaining their loyal SCR888 customers. 918Kiss mobile casino app is very polished and beautiful, making it a favorite among regular RoyaleWin players. Their mobile app is available on both Android and iPhone devices.<br><br>
                Next on the list is Joker123 mobile betting app. Joker123 has a lot of different type of games available for the customers. You can choose from playing the many slot games with up-to- date graphics or play the live casinos with beautiful live dealers from different ethnicities. Aside from mobile apps, Joker123 also has a native PC app. Therefore, you do not have to play Joker123 games on the web browser.<br><br>
                Last but not least is the ‘king’ of online casino, Sky3888. We really love Sky3888 because they not only have mobile casinos and slot games, but they also have player-versus- player games as well. If that is not enough, then you can play the popular Monkey King slot game but in multiplayer mode. This will surely bring a different kind of gambling experience even to regular players.<br><br>
                To wrap up, it does not matter which mobile casino is your preferred choice. What is important that you download the app from a trusted website. This is because your phone might get infected by virus and worse, your credit card information will be stolen. Therefore, always be alert and you will be in safe hands.<br><br>
                </p>
                <div class="footerTextL">
                    <div><h3 class="footerTitle">Why Royalewin</h3></div>
                    <div class="footerSub">Trusted Malaysia Online Casino - Your Premier Gaming Destination</div>
                    <p>
                        Play Baccarat, Blackjack, Roulette, Slots, Sports Betting with exciting promotions, 24/7 top of the line customer service & timely payouts with the highest level of security. <br><br>
                        Royalewin focused primarily in offering casino gaming products and services in Asia Pacific markets. Bringing you all the fun of a real casino in your home, we are committed to provide you gaming entertainment of premium quality and at exceptionally good value.<br><br>
                    </p>
                </div>
                <div class="footerTextL">
                    <div><h3 class="footerTitle">Our Games</h3></div>
                    <div class="footerSub">Live Dealer Casinos, Sportsbook, Slots, 4D</div>
                    <p>
                        Find a wide variety of top-class games, including classic favourites like Baccarat, Blackjack and Roulette, as well as popular themed slots, progressives, live casino with real dealers, live sportsbook betting, CFD (commodity, Forex, Index), and 4D.<br><br>
                        Royalewin offers superb quality games using WinningFT, MaxBet, Betsoft, XProGaming, Ho Gaming, AGGaming etc. softwares with no download required all in one advanced gaming platform.<br><br>
                    </p>
                </div>
                <div class="footerTextL">
                    <div><h3 class="footerTitle">24/7 Support</h3></div>
                    <div class="footerSub">24-Hour Customer Service</div>
                    <p>
                        Available 24 hours everyday, Royalewin Support Team is always there for you - to help answer your questions and resolve your issues as quickly and efficiently a possible.<br><br>
                        We focus on the needs of our customers by staying true to our core service principles: <br><br>
                        • The customer always come first  <br>
                        • Your satisfaction drives our business  <br>
                        • We will always strive to exceed your expectations  <br><br>
                    </p>
                </div>
                <div class="footerTextL">
                    <div><h3 class="footerTitle">Bonus Offers</h3></div>
                    <div class="footerSub">FREE Bonuses Up to 110%</div>
                    <p>
                        Royalewin loves to handsomely reward you for your support! Players at Royalewin.com are able to enjoy a fantastic selection of daily and weekly promotional offers. <br><br>
                        Our amazing range of promotions include an exclusive 110% Welcome Bonus for first-time depositors, double Daily Deposit Bonuses, Cashbacks for everything you played at our casino.<br><br>
                    </p>
                </div>
                <div class="clr"></div>
            </div>
			
			<script type="text/javascript">
@for ($i=1; $i<=15; $i++)
var qrcode = new QRCode(document.getElementById("printQR{{$i}}"), {
	width : 80,
	height : 80
});

function makeCode{{$i}} () {
	var elText = document.getElementById("getLink{{$i}}");

	if (!elText.value) {
		alert("Input a text");
		elText.focus();
		return;
	}

	qrcode.makeCode(elText.value);
}

makeCode{{$i}}();

$("#getLink{{$i}}").
	on("blur", function () {
		makeCode{{$i}}();
	}).
	on("keydown", function (e) {
		if (e.keyCode == 13) {
			makeCode{{$i}}();
		}
	});
@endfor
</script>
@endsection