@extends('royalewin/master')

@section('title', 'Promotions')

@section('css')
@parent

<link href="{{ asset('/royalewin/resources/css/promo.css') }}" rel="stylesheet" type="text/css" />
<style>
.yearend_bg
{
	display: block;
	height: 314px;
	margin: 10px auto 0px;
	padding: 0;
	position: relative;
	width: 979px;
	background: url({{ asset('/royalewin/img/yearend/top.png') }})no-repeat center;
}
.yearend_row1
{
	display: block;
	height: 84px;
	margin: 10px auto 0px;
	padding: 0;
	position: relative;
	width: 1006px;
	background: url({{ asset('/royalewin/img/yearend/1.png') }})no-repeat center;
}
.yearend_row2
{
	display: block;
	height: 84px;
	margin: 10px auto 0px;
	padding: 0;
	position: relative;
	width: 1006px;
	background: url({{ asset('/royalewin/img/yearend/2.png') }})no-repeat center;
}
.yearend_row3
{
	display: block;
	height: 84px;
	margin: 10px auto 0px;
	padding: 0;
	position: relative;
	width: 1006px;
	background: url({{ asset('/royalewin/img/yearend/3.png') }})no-repeat center;
}
.yearend_row4
{
	display: block;
	height: 84px;
	margin: 10px auto 0px;
	padding: 0;
	position: relative;
	width: 1006px;
	background: url({{ asset('/royalewin/img/yearend/4.png') }})no-repeat center;
}
.yearend_row5
{
	display: block;
	height: 84px;
	margin: 10px auto 0px;
	padding: 0;
	position: relative;
	width: 1006px;
	background: url({{ asset('/royalewin/img/yearend/5.png') }})no-repeat center;
}
.yearend_yellow
{
	color: #f6fd7e;
	margin-left: 330px;
	margin-top: 35px;
	float: left;
}
.yearend_white
{
	color: #ffffff;
	font-weight: bold;
	margin-top: 33px;
	margin-left: 10px;
	float: left;
	font-size: 18px;
	width: 150px;
}

.yearend_yellow2
{
	color: #f6fd7e;
	margin-left: 80px;
	margin-top: 35px;
	float: left;
}
.yearend_white2
{
	color: #ffffff;
	font-weight: bold;
	margin-top: 33px;
	margin-left: 10px;
	float: left;
	font-size: 18px;
}
.yearend_rank
{
	color: #ffffff;
	font-weight: bold;
	margin-top: 30px;
	margin-left: -690px;
	float: left;
	font-size: 18px;
	position: absolute;
}
</style>
@endsection

@section('js')
@parent

<script type="text/javascript">
    $(function($) {
        $('#accordion').find('.accordion-toggle').click(function(){
            //Expand or collapse this panel
            $(this).next().slideToggle('fast');

            //Hide the other panels
            $(".accordion-content").not($(this).next()).slideUp('fast');
        });
    });
</script>
@endsection

@section('content')
<div class="midSect bgpromo">
   <div class="yearend_bg" ></div>
   <?php echo htmlspecialchars_decode($article); ?>
</div>
@endsection