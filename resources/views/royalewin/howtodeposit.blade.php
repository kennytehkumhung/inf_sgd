@extends('royalewin/master')

@if (Session::get('currency') == 'MYR')
    @section('title', 'Best Online Live Casino Malaysia - How To Deposit')
    @section('keywords', 'Best Online Live Casino Malaysia - How To Deposit')
    @section('description', 'Choose your payment options and make Deposits at RoyaleWin Live Online Casino Malaysia, the best online casino games, online slots and sportbook.')
@elseif (Session::get('currency') == 'IDR')
    @section('title', 'Bandar Casino Online, Situs Judi Indonesia - Panduan')
    @section('keywords', 'Bandar Casino Online, Situs Judi Indonesia - Panduan')
    @section('description', 'Panduan - RoyaleWin adalah agen bola, bandar casino online dan situs judi online terpercaya untuk Anda penggemar taruhan bola dan pecinta judi online di Indonesia.')
@endif

@section('css')
@parent

<style type="text/css">
    .othersCont a {
        color: #fff;
    }
    
    .othersCont p {
        margin-bottom: 3px;
    }
    
    .tutImg {
        padding: 10px 0;
        width: 700px;
        height: auto;
    }
</style>
@endsection

@section('content')
<div class="midSect bgOthers">
    <div class="contOthers">
        <span>{{ strtoupper(Lang::get('public.HowToDeposit')) }}</span>
    </div>
    <div class="othersMenu">
        @include('royalewin.include.helpmenu')
        <div class="clr"></div>
    </div>
    <div class="othersCont">
        <div style="text-align: center;">
            @if (Lang::getLocale() == 'cn')
                <img alt="" src="{{ asset('/royalewin/img/how_to_deposit_ch_1.png') }}" class="tutImg" />
                <img alt="" src="{{ asset('/royalewin/img/how_to_deposit_ch_2.png') }}" class="tutImg" />
                <img alt="" src="{{ asset('/royalewin/img/how_to_deposit_ch_3.png') }}" class="tutImg" />
                <img alt="" src="{{ asset('/royalewin/img/how_to_deposit_ch_4.png') }}" class="tutImg" />
            @else
                <img alt="" src="{{ asset('/royalewin/img/how_to_deposit_en_1.png') }}" class="tutImg" />
                <img alt="" src="{{ asset('/royalewin/img/how_to_deposit_en_2.png') }}" class="tutImg" />
                <img alt="" src="{{ asset('/royalewin/img/how_to_deposit_en_3.png') }}" class="tutImg" />
                <img alt="" src="{{ asset('/royalewin/img/how_to_deposit_en_4.png') }}" class="tutImg" />
            @endif
        </div>
    </div>
</div>
@endsection