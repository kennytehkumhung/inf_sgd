@extends('royalewin/master')

@if (Session::get('currency') == 'MYR')
    @section('title', 'Best Online Live Casino Malaysia - Terms & Conditions')
    @section('keywords', 'Best Online Live Casino Malaysia - Terms & Conditions')
    @section('description', 'Find our more about RoyaleWin Online Live Casino Malaysias Terms and Conditions.')
@elseif (Session::get('currency') == 'IDR')
    @section('title', 'Bandar Casino Online, Situs Judi Indonesia - Syarat')
    @section('keywords', 'Bandar Casino Online, Situs Judi Indonesia - Syarat')
    @section('description', 'Syarat - RoyaleWin adalah agen bola, bandar casino online dan situs judi online terpercaya untuk Anda penggemar taruhan bola dan pecinta judi online di Indonesia.')
@endif

@section('css')
@parent

<style type="text/css">
    .othersCont a {
        color: #fff;
    }
    
    .othersCont p {
        margin-bottom: 12px;
    }
</style>
@endsection

@section('content')
<div class="midSect bgOthers">
    <div class="contOthers">
        <span>{{ strtoupper(Lang::get('public.TNC')) }}</span>
    </div>
    <div class="othersMenu">
        @include('royalewin.include.helpmenu')
        <div class="clr"></div>
    </div>
    <div class="othersCont">
        {!! htmlspecialchars_decode($content) !!}
    </div>
</div>
@endsection