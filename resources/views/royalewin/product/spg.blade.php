<!doctype html>
<html>
<head>
    <link href="{{ asset('/royalewin/resources/css/style.css') }}" rel="stylesheet" type="text/css" />

    <style type="text/css">
        .slot_box {
            background-color: #000;
            width: 150px;
            height: 202px;
        }

        .slot_box img {
            width: 150px;
            height: 150px;
        }
    </style>
</head>
<body style="background-color: transparent; overflow: hidden;">
<div class="slotContainerBottom">
    <div class="slotMenuHolder">
        <div class="slot_menu">
            <ul>
                <li><a href="{{route('spg', [ 'type' => 'slot' , 'category' => 'slot' ] )}}">Slots</a></li>
                <li><a href="{{route('spg', [ 'type' => 'slot' , 'category' => 'progressive' ] )}}">Progressive</a></li>
            </ul>
        </div>
    </div>

    <div id="slot_lobby">

        @foreach( $lists as $list )
            <div class="slot_box">
                <a href="javascript:void(0)" onclick="@if (Auth::user()->check())window.open('{{route('spg-slot' , [ 'gameid' => $list['gamecode'] ] )}}', 'spg_slot', 'width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif" >
                    <img src="{{url()}}/royalewin/img/spg/{{$list->gamecode}}.jpg" alt=""/>
                </a>
                <span>{{$list['gamename_en']}}</span>
            </div>
        @endforeach

        <div class="clr"></div>
    </div>

    <div class="clr"></div>
</div>
</body>
</html>
