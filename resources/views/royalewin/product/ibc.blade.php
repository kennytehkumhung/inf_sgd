@extends('royalewin/master')

@if (Session::get('currency') == 'MYR')
    @section('title', 'Sports Betting Malaysia - Sportsbook Malaysia')
    @section('keywords', 'Sports Betting Malaysia - Sportsbook Malaysia')
    @section('description', 'With RoyaleWin Malaysia Online Casino Sportsbook, you can take advantage of an amazing profit boost when you SignUp Today!')
@elseif (Session::get('currency') == 'IDR')
    @section('title', 'Situs Judi Bola Online dan Taruhan Indonesia')
    @section('keywords', 'Situs Judi Bola Online dan Taruhan Indonesia')
    @section('description', 'RoyaleWin Agen Bola yang menyediakan pembukaan akun Sportsbook taruhan bola, judi bola terpercaya di Indonesia.')
@endif

@section('content')
<div class="midSect bgSp">

    @include('royalewin.include.sportbook_top')

    <div class="spContainer">
        @if (!Auth::user()->check())
            <iframe id="ContentPlaceHolder1_iframe_game" width="1024px" height="655px" frameborder="0" src="https://mkt.avxsport.com/NewIndex?lang={{$lang}}"></iframe>
        @else
            <?php
            $useragent = $_SERVER['HTTP_USER_AGENT'];
            if (preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i', $useragent) || preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', substr($useragent, 0, 4))) {
                header('Location: //ismart.' . $website . '/deposit_ProcessLogin.aspx?lang=' . $lang . '&st=' . $token);
                //header('Location: //ismart.avxsport.com/deposit_ProcessLogin.aspx?lang=' . $lang . '&st=' . $token);
            } else {
                //echo '<iframe id="ContentPlaceHolder1_iframe_game" width="1024px" height="655px" frameborder="0" src="//www.avxsport.com/ibc?token='.$token.'&lang='.$lang.'"></iframe>';
                echo '<iframe id="ContentPlaceHolder1_iframe_game" width="1024px" height="970px" frameborder="0" src="//mkt.' . $website . '/Deposit_ProcessLogin.aspx?lang=en"></iframe>';
            }
            ?>
        @endif
    </div>
</div>
@endsection

@section('footer_seo')
            <div class="footerContent">
                <div><h1 class="footerTitle">Why IBC Sport is the Best Sportsbook in Malaysia</h1></div>
                <p>
                IBC Sport is one of the two sportsbook providers in RoyaleWin. This is because RoyaleWin maintains their quality by only choosing the best ones in the market. Sportsbook in Malaysia is a growing industry and IBC Sport aims to make it better. To start with, IBC Sport is regulated by First Cagayan Leisure &amp; Resorts Corporation and the Cagayan Economic Zone Authority of the Republic of the Philippines. This is a great proof to ensure that you are putting your money at a secured and monitored sportsbook provider. IBC Sport is also managed by people with many years of experience in the sportsbook industry.<br><br>
                Perhaps another reason why IBC Sport is chosen is because they have a great list of soccer games to wager on. Usually, sportsbook providers are only allowing wager on top football leagues like English Premier League, La Liga and Bundesliga. IBC Sport is not, because you can wager on lower or less known leagues as well. You can wager on who will win a match in the Cyprus Premier League and even Qatar Stars League. Just imagine the possibilities!<br><br>
                Another thing we like about IBC Sport sportsbook is because you can win your wager even before the game is completed. Most sportsbook will only allow bets that is calculated after the game ended. However, the same case cannot be applied to IBC Sport sportsbook. This is because IBC Sport allow their players to place bets on what the scores will be at halftime as well. There are also wagers to be placed on who will score the first goal of each half, the total score is odd or even, as well as who will score in the first ten minutes of the game.<br><br>
                Finally, the last reason why IBC is the best sportsbook in Malaysia is because they are already a great name in the gambling and online sportsbook industry. It has an extensive list of clients and all of them are satisfied with what is provided. It is hard to tackle all the customer needs as well as the regulatory body’s requests, but IBC is proving it wrong. Come join us an see the sportsbook magic by yourself!<br><br>
                </p>
                <div class="footerTextL">
                    <div><h3 class="footerTitle">Why Royalewin</h3></div>
                    <div class="footerSub">Trusted Malaysia Online Casino - Your Premier Gaming Destination</div>
                    <p>
                        Play Baccarat, Blackjack, Roulette, Slots, Sports Betting with exciting promotions, 24/7 top of the line customer service & timely payouts with the highest level of security. <br><br>
                        Royalewin focused primarily in offering casino gaming products and services in Asia Pacific markets. Bringing you all the fun of a real casino in your home, we are committed to provide you gaming entertainment of premium quality and at exceptionally good value.<br><br>
                    </p>
                </div>
                <div class="footerTextL">
                    <div><h3 class="footerTitle">Our Games</h3></div>
                    <div class="footerSub">Live Dealer Casinos, Sportsbook, Slots, 4D</div>
                    <p>
                        Find a wide variety of top-class games, including classic favourites like Baccarat, Blackjack and Roulette, as well as popular themed slots, progressives, live casino with real dealers, live sportsbook betting, CFD (commodity, Forex, Index), and 4D.<br><br>
                        Royalewin offers superb quality games using WinningFT, MaxBet, Betsoft, XProGaming, Ho Gaming, AGGaming etc. softwares with no download required all in one advanced gaming platform.<br><br>
                    </p>
                </div>
                <div class="footerTextL">
                    <div><h3 class="footerTitle">24/7 Support</h3></div>
                    <div class="footerSub">24-Hour Customer Service</div>
                    <p>
                        Available 24 hours everyday, Royalewin Support Team is always there for you - to help answer your questions and resolve your issues as quickly and efficiently a possible.<br><br>
                        We focus on the needs of our customers by staying true to our core service principles: <br><br>
                        • The customer always come first  <br>
                        • Your satisfaction drives our business  <br>
                        • We will always strive to exceed your expectations  <br><br>
                    </p>
                </div>
                <div class="footerTextL">
                    <div><h3 class="footerTitle">Bonus Offers</h3></div>
                    <div class="footerSub">FREE Bonuses Up to 110%</div>
                    <p>
                        Royalewin loves to handsomely reward you for your support! Players at Royalewin.com are able to enjoy a fantastic selection of daily and weekly promotional offers. <br><br>
                        Our amazing range of promotions include an exclusive 110% Welcome Bonus for first-time depositors, double Daily Deposit Bonuses, Cashbacks for everything you played at our casino.<br><br>
                    </p>
                </div>
                <div class="clr"></div>
            </div>
@endsection