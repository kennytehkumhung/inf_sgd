@extends('royalewin/master')

@if (Session::get('currency') == 'MYR')
    @section('title', 'Sports Betting Malaysia - WinningFT Malaysia')
    @section('keywords', 'Sports Betting Malaysia - WinningFT Malaysia')
    @section('description', 'With RoyaleWin Malaysia Online WinningFT Sportsbook, you can take advantage of an amazing profit boost when you SignUp Today!')
@elseif (Session::get('currency') == 'IDR')
    @section('title', 'Situs Judi Bola Online dan Taruhan WinningFT Indonesia')
    @section('keywords', 'Situs Judi Bola Online dan Taruhan WinningFT Indonesia')
    @section('description', 'RoyaleWin Agen Bola yang menyediakan permainan taruhan bola dan judi bola WinningFT terpercaya dan terbaik di Indonesia.')
@endif

@section('content')
<div class="midSect bgSp">
    
    @include('royalewin.include.sportbook_top')
    
    <div class="spContainer">
        <iframe name="sportFrame" id="" width="1120px" height="855px" frameborder="0" src="{{$login_url}}"></iframe>
    </div>
</div>
@endsection

@section('footer_seo')
            <div class="footerContent">
                <div><h1 class="footerTitle">WinningFT is your home of sports betting</h1></div>
                <p>
                Sportsbook is a place where players can put a wager on matches in sports competitions and get huge rewards if their bet is fulfilled. Some people said that sportsbook are pure luck, but if you really study the data and do your analysis on players’ form and match stats, you can actually deduce the outcome. If you are looking for a sportsbook in Malaysia, WinningFTRoyaleWin is the place to go.<br><br>
                Why we recommend WinningFT RoyaleWin is because they know the gambling industry in and out. With almost 15 years of experience in the industry, they also have an extensive list of sports competitions to wager on, ranging from football, golf, basketball, hockey, and even e-sports games like Dota 2 and Counter Strike: Global Offensive. There are also special bets being held from time to time, depending on the season. As an example, RoyaleWin even has a wager on who is going to win the Best Film in the Academy Awards.<br><br>
                If you are a new player and want to wager your money for greater returns, WinningFT in RoyaleWin is hard to miss. The WinningFT sportsbook user interface is easy to understand and putting your wager is even easier. The live matches is updated every minute to ensure that you get constant updates. Since there is no need for a spectacular graphics in sportsbook, RoyaleWin entices new players and old by its simplistic interface.<br><br>
                The best tip about doing wager in sportsbook is to choose a sports that you at least have some knowledge on. This is because the most rewarding wager is not always on getting the right score or the right winning team. You can sometimes win more on wagering who will get the most corners in a football match or who will score the first homerun in a baseball match. That is why you must have some knowledge so that you can do a calculated wager.<br><br>
                There is a lot of sportsbook websites in Malaysia, but not all of them are as good as what RoyaleWin provides in terms of sports available to wager and the variety of bets available. Not only that, RoyaleWin is also beginner friendly. Head over to RoyaleWin and bet on the sports you love!<br><br>
                </p>
                <div class="footerTextL">
                    <div><h3 class="footerTitle">Why Royalewin</h3></div>
                    <div class="footerSub">Trusted Malaysia Online Casino - Your Premier Gaming Destination</div>
                    <p>
                        Play Baccarat, Blackjack, Roulette, Slots, Sports Betting with exciting promotions, 24/7 top of the line customer service & timely payouts with the highest level of security. <br><br>
                        Royalewin focused primarily in offering casino gaming products and services in Asia Pacific markets. Bringing you all the fun of a real casino in your home, we are committed to provide you gaming entertainment of premium quality and at exceptionally good value.<br><br>
                    </p>
                </div>
                <div class="footerTextL">
                    <div><h3 class="footerTitle">Our Games</h3></div>
                    <div class="footerSub">Live Dealer Casinos, Sportsbook, Slots, 4D</div>
                    <p>
                        Find a wide variety of top-class games, including classic favourites like Baccarat, Blackjack and Roulette, as well as popular themed slots, progressives, live casino with real dealers, live sportsbook betting, CFD (commodity, Forex, Index), and 4D.<br><br>
                        Royalewin offers superb quality games using WinningFT, MaxBet, Betsoft, XProGaming, Ho Gaming, AGGaming etc. softwares with no download required all in one advanced gaming platform.<br><br>
                    </p>
                </div>
                <div class="footerTextL">
                    <div><h3 class="footerTitle">24/7 Support</h3></div>
                    <div class="footerSub">24-Hour Customer Service</div>
                    <p>
                        Available 24 hours everyday, Royalewin Support Team is always there for you - to help answer your questions and resolve your issues as quickly and efficiently a possible.<br><br>
                        We focus on the needs of our customers by staying true to our core service principles: <br><br>
                        • The customer always come first  <br>
                        • Your satisfaction drives our business  <br>
                        • We will always strive to exceed your expectations  <br><br>
                    </p>
                </div>
                <div class="footerTextL">
                    <div><h3 class="footerTitle">Bonus Offers</h3></div>
                    <div class="footerSub">FREE Bonuses Up to 110%</div>
                    <p>
                        Royalewin loves to handsomely reward you for your support! Players at Royalewin.com are able to enjoy a fantastic selection of daily and weekly promotional offers. <br><br>
                        Our amazing range of promotions include an exclusive 110% Welcome Bonus for first-time depositors, double Daily Deposit Bonuses, Cashbacks for everything you played at our casino.<br><br>
                    </p>
                </div>
                <div class="clr"></div>
            </div>
@endsection