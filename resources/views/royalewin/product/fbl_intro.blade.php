@extends('royalewin/master')

@if (Session::get('currency') == 'IDR')
    @section('title', 'Agen Judi Online Permainan FireBall Indonesia')
    @section('keywords', 'Agen Judi Online Permainan FireBall Indonesia')
    @section('description', 'RoyaleWin Agen Judi Online menyediaka permainan FireBall dan taruhan online Terpercaya dan Terbaik di Indonesia.')
@endif

@section('js')
@parent

<script type="text/javascript">
    $(function($) {
        //
    });
</script>
@endsection

@section('content')
<div class="midSect bg4d">
    <!--<div class="cont4"></div>-->
    <div class="dContainerBottom">
        <div class="dContainerBottomInner">
            <div style="position: relative;">
                <img src="{{ asset('/royalewin/img/fbl_bg.jpg') }}" />
                <div style="position: absolute; top: 296px; left: 621px; height: 66px; width: 148px; cursor: pointer;" onclick="{{ Auth::user()->check() ? 'window.open("'.route('fbl').'","fbl","width=1046,height=830");' : 'alert("'.Lang::get('COMMON.PLEASELOGIN').'");' }}">&nbsp;</div>
            </div>
            <div class="clr"></div>
        </div>
    </div>
</div>
@endsection