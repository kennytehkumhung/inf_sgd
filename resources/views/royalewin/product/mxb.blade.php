@extends('royalewin/master')

@section('title', 'Live Casino')

@section('css')
@parent

<link href="{{url()}}/front/resources/css/otherPg.css" rel="stylesheet">
<style type="text/css">
    .lobbyMenu a:visited {
        color: #fff;
    }

    .lobbyMenu ul li {
        margin-left: 2px !important;
    }
</style>
@endsection

@section('js')
@parent
<script type="text/javascript" src="{{url()}}/front/resources/js/slick.min.js"></script>
<script type="text/javascript" src="{{url()}}/front/resources/js/jquery.bxslider.min.js"></script>
<script type="text/javascript">
    $(function () {
        $('.bxslider').bxSlider({
            pagerCustom: '#bx-pager'
        });
        
        $('.caro').slick({
            infinite: true,
            slidesToShow: 2,
            slidesToScroll: 2,
            autoplay: true,
            variableWidth: true,
            arrows: false
        });
    });

    function callUrl(id,url,limitKey){
        var limitId = $("#" + limitKey).val();

        window.open('{{ route('mxb-casino') }}?g=' + id + '&l=' + limitId + '&c=' + url, 'maxbet', 'width=1150,height=830');
    }
</script>
@endsection

@section('content')
<div class="midSect bgLc">
    <div class="cont1">
        <div class="acctContainer" style="padding-left:0px;">
            <!--LOBBY MENU-->
            <div class="lobbyMenu">
                <ul>
                    <li><a href="{{route('mxb', [ 'type' => 'casino' , 'category' => 'baccarat' ] )}}">Bacarrat</a></li>
                    <li><a href="{{route('mxb', [ 'type' => 'casino' , 'category' => 'roulette' ] )}}">Roulette</a></li>
                    <li><a href="{{route('mxb', [ 'type' => 'casino' , 'category' => 'blackjack' ] )}}">Black Jack</a></li>
                    <li><a href="{{route('mxb', [ 'type' => 'casino' , 'category' => 'single_player_poker' ] )}}">Poker</a></li>
                    <li><a href="{{route('mxb', [ 'type' => 'casino' , 'category' => 'carribean_poker' ] )}}">Carribean Poker</a></li>
                    <li><a href="{{route('mxb', [ 'type' => 'casino' , 'category' => 'dragon_tiger' ] )}}">Dragon Tiger</a></li>
                    <li><a href="{{route('mxb', [ 'type' => 'casino' , 'category' => 'sicbo' ] )}}">Sic Bo</a></li>
                    <li><a href="{{route('mxb', [ 'type' => 'casino' , 'category' => 'wheel_of_fortune' ] )}}">Wheel of Fortune</a></li>
                </ul>
            </div>
            <!--LOBBY MENU-->

            <!--LOBBY-->
            <div class="lobby">	
                <div id="casino_table_wrapper">				
                    @foreach( $gamelist as $key => $list)		
                    <div class="tableInfo">			
                        <div class="roomTitle">{{$list['gameName']}}</div>
                        <div class="roomDetails">
                            <div class="photo">
                                <img width="138" height="182" src="{{$list['image']}}">
                            </div>
                            <div class="roomInfo">
                                Dealer: {{$list['dealerName']}}
                                <br>
                                <br>
                                Currency: {{Session::get('currency')}}
                                <br>
                                <br>

                                Table Limit:
                                <br>
                                <select id="limit_{{$key}}">
                                    @foreach( $list['limit'] as $url_key => $value)
                                    <option value="{{$value['limitSetID']}}">{{$value['minBet']}} ~ {{$value['maxBet']}}</option>							
                                    @endforeach
                                </select>
                                <br>
                                <a class="login_btn" href="#" onClick="callUrl('{{$key}}','{{Crypt::encrypt($list['url'])}}','limit_{{$key}}')">Play Now</a>
                            </div>
                        </div>						
                    </div>								
                    @endforeach			
                </div>				
            </div>	
            <!--LOBBY-->   
        </div>
    </div>
</div>
@endsection
