<html>
<head>
	<script type="text/javascript">
		@if ($is_mobile)
            window.location.href = "{!! $login_url !!}";
		@endif
	</script>
</head>
	<body bgcolor="black">
		@if (!$is_mobile)
			<iframe id="iframe_game" frameborder="0" style="overflow:hidden;overflow-x:hidden;overflow-y:hidden;height: 98%; min-height: 600px; width:100%;" src="{{$login_url}}"></iframe>
		@endif
	</body>
</html>