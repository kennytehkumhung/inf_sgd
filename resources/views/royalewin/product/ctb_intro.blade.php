@extends('royalewin/master')

@if (Session::get('currency') == 'MYR')
    @section('title', 'Horse Racing Betting Odds & Results in Malaysia')
    @section('keywords', 'Horse Racing Betting Odds & Results in Malaysia')
    @section('description', 'Get the latest Horse Racing Betting Odds information and all the best Horse Racing prices from RoyaleWin.')
@endif

@section('js')
@parent

<script type="text/javascript">
    $(function($) {
        //
    });
</script>
@endsection

@section('content')
<div class="midSect bg4d">
    <!--<div class="cont4"></div>-->
    <div class="dContainerBottom">
        <div class="dContainerBottomInner">
            <div style="position: relative;">
                <img src="{{ asset('/royalewin/img/ctb_bg.jpg') }}" style="width: 100%; height: auto;" />

                <div style="position: absolute; top: 0; left: 0; text-align: center; width: 100%;">
                    <a href="javascript:void(0);" onclick="{{ Auth::user()->check() ? 'window.open("'.route('ctb').'","ctb","width=1046,height=830");' : 'alert("'.Lang::get('COMMON.PLEASELOGIN').'");' }}">
                        <img src="{{ asset('/royalewin/img/ctb_button.png') }}">
                    </a>
                </div>
            </div>
            <div class="clr"></div>
        </div>
    </div>
</div>
@endsection

@section('footer_seo')
            <div class="footerContent">
                <div><h1 class="footerTitle">Horse Racing betting with RoyaleWin has never been easier</h1></div>
                <p>
                Horse racing is a very exciting sports for the ones betting as well as those who just want to enjoy the thrill. If you have ever been to Hong Kong, you will see how huge horse racing is there, with millions of dollars being transacted in one night. However, it does not mean that you need to go to Hong Kong to bet on a horse race. With RoyaleWin, it is easy to do so.<br><br>
                RoyaleWin, the top online casino in Malaysia, not only have casino and slot games but also horse racing bets. It is categorized as a different section with sportsbook, known as horsebook. This just shows how popular it is that it needs to be in a separate section. As like sportsbook, horse racing bets are a lot more dependant on your analysis. You will need to analyze the rider, their past races, as well as their horses and what is their current condition.<br><br>
                If you are new to horse racing, there are several things that you must learn. Firstly, the horse bets come in three types. The first one is win, meaning that you are wagering on a horse that will reach the finish line first. Next type is place, where you bet on what position will a horse be at the end of the race. Lastly is show, where you bet on a horse that will come in either first, second or third position. The last two bets are the easiest way for beginners to win and test their analysis, even though the reward is smaller. So, it is best that you make a couple of place and show bets to wet your feet. Once you are confident with your analysis, you can then start placing higher bets.<br><br>
                RoyaleWin is an all rounder online casino in Malaysia. If you enjoy horse racing or any types of gambling and casino games, you must have an account at RoyaleWin. Registering an account is easy at RoyaleWin, so you can jump on the fun as soon as you want to!<br><br>   
                </p>
                <div class="footerTextL">
                    <div><h3 class="footerTitle">Why Royalewin</h3></div>
                    <div class="footerSub">Trusted Malaysia Online Casino - Your Premier Gaming Destination</div>
                    <p>
                        Play Baccarat, Blackjack, Roulette, Slots, Sports Betting with exciting promotions, 24/7 top of the line customer service & timely payouts with the highest level of security. <br><br>
                        Royalewin focused primarily in offering casino gaming products and services in Asia Pacific markets. Bringing you all the fun of a real casino in your home, we are committed to provide you gaming entertainment of premium quality and at exceptionally good value.<br><br>
                    </p>
                </div>
                <div class="footerTextL">
                    <div><h3 class="footerTitle">Our Games</h3></div>
                    <div class="footerSub">Live Dealer Casinos, Sportsbook, Slots, 4D</div>
                    <p>
                        Find a wide variety of top-class games, including classic favourites like Baccarat, Blackjack and Roulette, as well as popular themed slots, progressives, live casino with real dealers, live sportsbook betting, CFD (commodity, Forex, Index), and 4D.<br><br>
                        Royalewin offers superb quality games using WinningFT, MaxBet, Betsoft, XProGaming, Ho Gaming, AGGaming etc. softwares with no download required all in one advanced gaming platform.<br><br>
                    </p>
                </div>
                <div class="footerTextL">
                    <div><h3 class="footerTitle">24/7 Support</h3></div>
                    <div class="footerSub">24-Hour Customer Service</div>
                    <p>
                        Available 24 hours everyday, Royalewin Support Team is always there for you - to help answer your questions and resolve your issues as quickly and efficiently a possible.<br><br>
                        We focus on the needs of our customers by staying true to our core service principles: <br><br>
                        • The customer always come first  <br>
                        • Your satisfaction drives our business  <br>
                        • We will always strive to exceed your expectations  <br><br>
                    </p>
                </div>
                <div class="footerTextL">
                    <div><h3 class="footerTitle">Bonus Offers</h3></div>
                    <div class="footerSub">FREE Bonuses Up to 110%</div>
                    <p>
                        Royalewin loves to handsomely reward you for your support! Players at Royalewin.com are able to enjoy a fantastic selection of daily and weekly promotional offers. <br><br>
                        Our amazing range of promotions include an exclusive 110% Welcome Bonus for first-time depositors, double Daily Deposit Bonuses, Cashbacks for everything you played at our casino.<br><br>
                    </p>
                </div>
                <div class="clr"></div>
            </div>
@endsection