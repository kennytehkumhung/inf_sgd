<!doctype html>
﻿<html>

<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="theme-color" content="#af0000">

	<title>Royalewin</title>

	<link rel="stylesheet" type="text/css" href="{{ static_asset('/xmas/en/css/tpl.css') }}">

	<meta name="description" content="">

	<!-- Favicons -->
	<link rel="shortcut icon" href="{{ static_asset('/xmas/en/favicon.ico') }}" type="image/icon" />


	<!-- CSS minified -->
	<link rel="stylesheet" href="{{ static_asset('/xmas/en/css/main.css') }}">
	<link rel="stylesheet" href="{{ static_asset('/xmas/en/css/reg.css') }}">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp"
	 crossorigin="anonymous">


	<!-- JS -->
	<script src="{{ static_asset('/xmas/en/js/main.js') }}"></script>
	<script type="text/javascript">
		function register_submit() {
                        $("#register_btn").hide();
                        $("#loading").show();
			var dob = '0000-00-00';
			if (typeof $('#dob_year').val() != undefined) {
				dob = $('#dob_year').val() + '-' + $('#dob_month').val() + '-' + $('#dob_day').val();
			}
			$.ajax({
				type: "POST",
				url: "{{route('register_process')}}",
				data: {
					_token:     "{{ csrf_token() }}",
					username:	$('#r_username').val(),
					password:	$('#r_password').val(),
					repeatpassword: $('#r_repeatpassword').val(),
					code:		$('#r_code').val(),
					email:		$('#r_email').val(),
					mobile:		$('#r_mobile').val(),
					fullname:	$('#r_fullname').val(),
					dob:		dob
				},
			}).done(function(json) {
				$('.acctTextReg').html('');
				obj = JSON.parse(json);
				var str = '';
				$.each(obj, function(i, item) {
					if ('{{Lang::get('COMMON.SUCESSFUL')}}' == item) {
						alert('{{Lang::get('public.NewMember')}}');
						window.location.href = "{{route('homepage')}}";
					} else {
						$('.' + i + '_acctTextReg').html('<font style="color:red">' + item + '</font>');
                                                $("#register_btn").show();
                                                $("#loading").hide();
					}
				});
			});
		}
                
                function enterpressalert(e, textarea){
                    var code = (e.keyCode ? e.keyCode : e.which);
                    if(code == 13) { //Enter keycode
                        register_submit();
                    }
                }
	</script>

</head>

<body lang="sc" class="sc">
	<input type="hidden" name="tpl_url_hidden" id="tpl-url-hidden" value="https://www.geiqianle.com/hf-tpl">
	<input type="hidden" name="tpl_mobile_responsive" id="tpl-mobile-responsive" value="1">

	<header class="tpl-header tpl-show-lang-text tpl-cf tpl-header5">
		<div class="tpl-inner tpl-cf">

			<div class="tpl-back-btn tpl-box">
				<a href="//m.rwbola.com/my/promotion" target="_blank" id="tpl-back-button">
					<img src="{{ static_asset('/xmas/en/img/back.png') }}">
				</a>
			</div>

			<div class="tpl-logo-wrap tpl-box">
				<a href="#" class="tpl-desk tpl-convert mobi-on" id="tpl-logo-mobi" target="_blank" title="ROYALEWIN">
					<img src="{{ static_asset('/xmas/en/img/logo_my_xmas.gif') }}">
				</a>
			</div>

			<div class="tpl-download-faq tpl-box">
				<a href="//m.rwbola.com/my/" target="_blank" id="tpl-download-btn-mobi" class="tpl-convert mobi-on">
					<img src="{{ static_asset('/xmas/en/img/download.png') }}">
				</a>
				<a href="{{ route( 'language', [ 'lang'=> 'cn']) }}" class="tpl-convert mobi-on">
					<img src="{{ static_asset('/xmas/en/img/ch.png') }}">
				</a>
			</div>

			<div class="tpl-right-nav">
				<div class="tpl-inner-right-nav tpl-cf">
					<a href="//www.rwbola.com/my" class="tpl-convert mobi-on" id="tpl-download-mobi" target="_blank">
						<div class="tpl-download-btn">
							Home </div>
					</a>

					<div class="tpl-lang-wrap">
						<div class="tpl-lang-trigger tpl-cf">
							<span class="tpl-sprt en"></span>
							<span class="tpl-txt-lang">English</span>
							<span class="tpl-sprt tpl-arw-lang"></span>
						</div>

						<ul class="tpl-lang-select tpl-cf">
							<li class="tpl-lang" id="tpl-lang-en">
								<a href="{{ route( 'language', [ 'lang'=> 'cn']) }}" id="tpl-en">
									<span class="tpl-sprt sc"></span> <span class="tpl-txt-lang">中文 </span>
									<span class="tpl-sprt"></span>
								</a>
							</li>

						</ul>
					</div>

					<a href="//www.rwbola.com/my/register" target="_blank" id="tpl-join-now">
						<div class="tpl-join-now">
							Register </div>
					</a>
				</div>
			</div>

		</div>
	</header>
	<div class="music-wrap">
		<iframe id="xmasbg" src="{{ static_asset('/xmas/en/xmasbg.mp3') }}" allow="autoplay" sandbox="allow-same-origin allow-scripts"></iframe>
		<img id="volume-on-trig" class="volume-on" src="{{ static_asset('/xmas/en/img/volume-on.png') }}">
		<img id="volume-off-trig" class="volume-off" src="{{ static_asset('/xmas/en/img/volume-off.png') }}">
	</div>
	<input type="hidden" id="musictrig" name="">
	<div class="main">
		<div class="main-wrapper">
			<div class="content-left">
				<div class="logo-wrap">
					<img src="{{ static_asset('/xmas/en/img/logo-sc.png') }}">
				</div>
				<div class="subtitle-wrap">
					<h1>Join now and receive up to MYR 788 of Welcome Bonus!</h1>
					<p>
						Hurry up and join the event now! <br>
						<span>starts on 24 December 2018 </span><br>
						<span>until 01 January 2019.</span><br>
					</p>
				</div>
				<div class="gift-wrap">
					<img class="gift-item gift-car" src="{{ static_asset('/xmas/en/img/car.png') }}">
				</div>
				<div class="qr-wrapper">
					<div class="qr-phone">
						<div class="qr-over"></div>
						<img class="qr-code" src="{{ static_asset('/xmas/en/img/qr-sc.png') }}">
					</div>
					<div class="qr-info">
						<div class="qr-arrow"></div>
						<p>More Info, Please visit<br>
							<span class="yellow">www.rwbola.com</span></p>
					</div>
				</div>
			</div>
			<div class="content-right">
				<div id="main-boxes">
					<div class="box-container box1 active" id="box1" tab-ref="1">
						<div class="box-header">
							<h2>SANTA’S GIFT</h2>
						</div>
						<div class="box-content" style="overflow: hidden; padding: 0px; width: 398px;">

							<div class="jspPane" style="padding: 0px; top: 0px; left: 0px; width: 391px;"><br><br><a id="sign-up-now-mobi"
								 class="cta-btn tpl-convert mobi-on" target="_blank" href="//www.rwbola.com/my/register">
									Register Now </a>
								<div class="jspContainer" style="width: 390px; height: 530px;  margin:17px auto;">



									<ol>
										<li>
											<p>Promotion applies to all Royalewin members using MYR only..</p>

										</li>
										<li>
											<p>Players must meet any minimum deposit requirement based on the table below during the event period.</p>
										</li>
										<li>
											<p>
												Players who meet the bonus requirement will receive their bonus for only ONCE during the event week (GMT+8).</p>
										</li>
										<li>
											<p>

												The promotion is not to be entitled along with any other deposit bonuses.	</li>
										<li>

											Bonus wagering requirement is 10 time(s). Bets on any Casino Games are valid for the rollover requirement.</p>
										</li>
									</ol>
									<h3 class="center hiw-title">
										HOW IT WORKS:</h3>

									<p>Achieve a Minimum Deposit Requirement during event period.</p>
									<table class="center">
										<tbody>
											<tr>
												<th> Package </th>

												<th> Minimum Deposit Requirement </th>

												<th> Bonus </th>

											</tr>
											<tr>

												<td> Classic Welcome Package </td>


												<td> MYR300 </td>


												<td>
													38% up To MYR 208</td>

											</tr>
											<tr>

												<td> Special Welcome Package (Limited time event) </td>


												<td> MYR2000</td>


												<td> 22% up To MYR 788</td>

											</tr>
										</tbody>
									</table>


									<p>* Player must Contact Live Chat Support to claim your bonus during event week (GMT+8).</p>


								</div>
							</div>
						</div>
					</div>
					<div class="box-container box2 inactive" id="box2" tab-ref="2">
						<div class="box-header">
							<h2>Register</h2>
						</div>
						<div class="box-content daily-boxes">
							<div class="bx-wrapper" style="max-width: 100%;">
								<div class="bx-viewport" aria-live="polite" style="width: 100%; overflow: hidden; position: relative; height: 509px;">

									<div class="form">
										<div class="rowed1">
											<label>Full Name*:</label>
											<input type="text" id="r_fullname"><span class="red email_acctTextReg acctTextReg"></span>
										</div>
										<div class="rowed1">
											<label>Contact*:</label>
											<input type="text" id="r_mobile"><span class="red mobile_acctTextReg acctTextReg"></span>
										</div>
										<div class="rowed1">
											<label>Email*:</label>
											<input type="text" id="r_email"><span class="red fullname_acctTextReg acctTextReg"></span>
										</div>
										

										<div class="rowed1">
											<label>User Name*:</label>
											<input type="text" id="r_username"><span class="red username_acctTextReg acctTextReg"></span>
										</div>

										<div class="rowed1">
											<label>Password*:</label>
											<input type="password" id="r_password"><span class="red password_acctTextReg acctTextReg"></span>
										</div>

										<div class="rowed1">
											<label>Re-enter Password*:</label>
											<input type="password" id="r_repeatpassword">
										</div>
										<div class="rowed1">
											<label>Code:</label>
											<input type="text" id="r_code" onKeyPress="enterpressalert(event, this)" class="coded">
											<img src="{{route('captcha', ['type' => 'register_captcha'])}}" width="60" height="27" class="coded blacked">
										</div>

										<div class="rowed1" id="register_btn"><a href="javascript:void(0);" onclick="register_submit();">Register Now!</a></div>
                                                                                <div class="rowed1" id="loading" style="display:none;"><a href="#">Loading...</a></div>
									</div>
                                                                                <div class="rowed1">
											<div class="note">
												*The name must match with your bank account name for withdrawal.
											</div>
										</div>

								</div>
								<div class="bx-controls"></div>
							</div>
						</div>

					</div>

					<div class="box-container">
						<div class="box-header">
							<h2>Royalewin Promotion</h2>
							<a href="//www.rwbola.com/my/promotion"><img class="Mobimg" src="{{ static_asset('/xmas/en/img/p1.png') }}" /></a>
							<p>
								<a href="//www.rwbola.com/my/promotion"><img class="Mobimg" src="{{ static_asset('/xmas/en/img/p2.png') }}" /></a>
								<p>
									<a href="//www.rwbola.com/my/promotion"><img class="Mobimg" src="{{ static_asset('/xmas/en/img/p3.png') }}" /></a>
									<p>
										<a href="//www.rwbola.com/my/promotion"><img class="Mobimg" src="{{ static_asset('/xmas/en/img/p4.png') }}" /></a>
										<p>
											<a href="//www.rwbola.com/my/promotion"><img class="Mobimg" src="{{ static_asset('/xmas/en/img/p5.png') }}" /></a>
											<p>
												<a href="//www.rwbola.com/my/promotion"><img class="Mobimg" src="{{ static_asset('/xmas/en/img/p6.png') }}" /></a>
												<p>



						</div>

					</div>
					<div class="box-container" style="display:none;">
						<div class="box-header">
							<h2>Royalewin Promotion</h2>
							<a href="//www.rwbola.com/my/promotion"><img src="{{ static_asset('/xmas/en/img/p1.png') }}" /></a>
							<p>
								<a href="//www.rwbola.com/my/promotion"><img src="{{ static_asset('/xmas/en/img/p2.png') }}" /></a>
								<p>
									<a href="//www.rwbola.com/my/promotion"><img src="{{ static_asset('/xmas/en/img/p3.png') }}" /></a>
									<p>
										<a href="//www.rwbola.com/my/promotion"><img src="{{ static_asset('/xmas/en/img/p4.png') }}" /></a>
										<p>
											<a href="//www.rwbola.com/my/promotion"><img src="{{ static_asset('/xmas/en/img/p5.png') }}" /></a>
											<p>
												<a href="//www.rwbola.com/my/promotion"><img src="{{ static_asset('/xmas/en/img/p6.png') }}" /></a>
												<p>



						</div>
						<div id="daily-slider"></div>

					</div>


					<div class="box-container" style="display:none;">

						<div id="dafanews-slider"></div>

					</div>




					<div class="box-container box6 active" id="box6" tab-ref="6">
						<div class="box-header">
						</div>
						<div class="box-content " tabindex="0" style="overflow: hidden; padding: 0px; outline: none; width: 397px;">


							<div class="jspContainer" style="width: 397px; height: 837px;">
								<div class="jspPane" style="padding: 0px; top: 0px; left: 0px; width: 380px;">
									<h2 class="accordion yellow active">
										T&C
									</h2>
									<div class="panel show">
										<ol>
											<li>
												<p>These promotion starts on 00:00:00 (GMT+8) 24 December 2018 until 23:59:59 (GMT+8) 01 January 2019.
</p>
											</li>
											<li>
												<p>Every player can claim their bonus only ONCE, try to choose the greatest amount of a single deposit to enjoy the maximum benefits during the event period.
</p>
											</li>
											<li>
												<p>Promotion applies for all Royalewin members. Member may only have one active account at any one time.
 </p>
											</li>

											<li>
												<p> Promotion bonus is subjected to 10 time(s) turnover in before any withdrawal may be made or getting your next bonus.
</p>

												<ul>
													<li> 
Single Deposit = MYR 300 </li>
													<li> 

Bonus = 38% x MYR 300 = MYR 114 </li>
													<li>Rollover requirement = MYR 114 x 10 = MYR 1,140 (or equivalent currency)
</li>
												</ul>
											</li>
											<li>
												<p>Draw result, both sides bet, voided or cancelled games are excluded in rollover calculation.
</p>
											</li>
											<li>
												<p>Deposit and Bonus claimed cannot be used in Playtech Live Casino, Fishing World & Joker Fish hunter 2, SCR888/918KISS and 4D. If any member determined to bet these games, Royalewin reserves the right to suspend all the winnings.
 </p>
											</li>
											<li>
												<p>The bonus amount and all winnings will be forfeited if the deposit and bonus turnover requirement are not met within 30 days of bonus credit.
 </p>

											</li>
											<li>
												<p> This promotion offer is only permitted once per IP address. Royalewin reserves the right to void all winnings and play should this term be disregarded.
 </p>
											</li>


											<li>
												<p>All other Terms & Conditions not stated above shall be governed by ROYALEWIN’s General Terms & Conditions.
 </p>
											</li>
										</ol>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="btm-wrapper">
					<a id="back-to-promo" href="//www.rwbola.com/my/promotion" class="btm-item" target="_blank">
						<span>Home</span>
					</a>
					<a id="gen-term" class="btm-item">
						<span>T&C</span>
					</a>
				</div>

				<div id="main-nav" class="main-navigation">
					<ul>
						<li tab-ref="1" class="active">
							<span class="nav-1 nav-item"></span>
							<span class="tooltip">Christmas Cash Prize!</span>
						</li>
						<li tab-ref="2" class="inactive">
							<span class="nav-2 nav-item"></span>
							<span class="tooltip">Register</span>
						</li>
						<li>
							<span class="nav-3 nav-item"></span>
							<span class="tooltip">Royalewin Promotion</span>
						</li>



						<div class="mob-bck-to-top">
							<span class="nav-6 nav-item"></span>
						</div>
					</ul>
				</div>
			</div>
		</div>
		<canvas id="canv" class="snow" width="411" height="823"></canvas>
	</div>

	<div id="opt-in-modal" class="modal">
		<div class="modal-icon">
			<img id="modal-icon-arcade" src="{{ static_asset('/xmas/en/img/arcade.png') }}">
			<img id="modal-icon-casino" src="{{ static_asset('/xmas/en/img/casino.png') }}">
			<img id="modal-icon-games" src="{{ static_asset('/xmas/en/img/games.png') }}">
			<img id="modal-icon-livedealer" src="{{ static_asset('/xmas/en/img/livedealer.png') }}">
		</div>
		<div class="modal-close">
			<img src="{{ static_asset('/xmas/en/img/close.png') }}">
		</div>
		<div class="modal-inner">
			<p class="optin-text">您已挑选 <span id="opt-in-gift" class="yellow">"&lt; insert gift &gt;"</span>!<br>填写以下信息正式加入活动:</p>
			<div class="optin-form">
				<input type="text" id="optin-username" class="optin-input" placeholder="用户名">
				<a id="optin-submit" class="cta-btn cta-opt">填写</a>
			</div>
			<div class="optin-msg center">
				<p id="optin-msg-1">请输入您的大发用户名。</p>
				<p id="optin-msg-2">您已挑选了一件礼物！<br>每日<span class="yellow go-to-chk-prg">查看您的进程！</span></p>
				<p id="optin-msg-3">您已挑选本周的圣诞礼物，请在下个活动周继续挑选！</p>
			</div>
		</div>
	</div>
	<div class="overlay-modal">

	</div>
	<footer class="tpl-footer tpl-show-sponsors">
		<div class="tpl-inner tpl-cf">
			<section class="tpl-partners">
				<img class="tpl-desktop-sponsor" src="{{ static_asset('/xmas/en/img/footer-vendor-my.png') }}">
			</section>

			<section class="tpl-contacts " id="company-social">
				<span class="tpl-toll">
					<a href="https://www.facebook.com/rwin888/"><i class="fab fa-facebook"></i></a></span>


				<a href="https://twitter.com/RoyaleWin"><i class="fab fa-twitter-square"></i></a>

				<a href="https://plus.google.com/+RoyaleWinMY"><i class="fab fa-google-plus-square"></i></a>

				<a href="https://www.instagram.com/royalewin/?hl=en"><i class="fab fa-instagram"></i></a>


				</span>
				</span>
			</section>
		</div>
		<div class="tpl-copyright">
			<div class="tpl-inner">
				©2018 ROYALEWIN All Right Reserved | 18+
			</div>
		</div>
	</footer>
        
        <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-41261240-1', 'auto');
        ga('send', 'pageview');

        </script>
    
	<script src="{{ static_asset('/xmas/en/js/tpf.js') }}" type="text/javascript"></script>


</body>

</html>