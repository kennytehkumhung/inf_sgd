@extends('royalewin/master')

@if (Session::get('currency') == 'MYR')
    <?php
    $title = '';
    $desc = '';
    ?>
    @if ($type == 'mxb')
        <?php $title = ' - Maxbet Slot Games'; $desc = 'Maxbet '; ?>
    @elseif ($type == 'plt' || $type == 'pltb')
        <?php $title = ' - PlayTech Slot Games'; $desc = 'PlayTech '; ?>
    @elseif ($type == 'sky')
        <?php $title = ' - Sky3888 and SCR888 Slot Games'; $desc = 'Sky3888 and SCR888 '; ?>
    @elseif ($type == 'spg')
        <?php $title = ' - SpadeGaming Slot Games'; $desc = 'SpadeGaming '; ?>
    @endif

    @section('title', 'Online Slot Malaysia'.$title)
    @section('keywords', 'Online Slot Malaysia'.$title)
    @section('description', 'Play '.$desc.'Online Slots at RoyaleWin Online Casino Malaysia and claim a FREE SPIN bonus today!')
@elseif (Session::get('currency') == 'IDR')
    <?php
    $title = 'Online Slot Malaysia';
    $desc = '';
    ?>
    @if ($type == 'mxb')
        <?php $title .= 'Maxbet '; $desc = 'Maxbet '; ?>
    @elseif ($type == 'plt' || $type == 'pltb')
        <?php $title .= 'PlayTech '; $desc = 'PlayTech '; ?>
    @elseif ($type == 'sky')
        <?php $title .= 'Sky3888 & SCR888 '; $desc = 'Sky3888 & SCR888 '; ?>
    @elseif ($type == 'spg')
        <?php $title .= 'SpadeGaming '; $desc = 'SpadeGaming '; ?>
    @endif

    @section('title', 'Situs Judi '.$title.'Online Slot Indonesia')
    @section('keywords', 'Situs Judi '.$title.'Online Slot Indonesia')
    @section('description', 'RoyaleWin Agen Judi Slot Online menyediakan permainan '.$desc.'slots dan taruhan online terpercaya dan terbaik di Indonesia.')
@endif

@section('js')
@parent
<script type="text/javascript">
    $(function () {
        var showBoxesMin = 7;
        var showBoxesMax = 15;

        for (var i = showBoxesMin; i <= showBoxesMax; i++) {
            $(".showbox" + i).hover(function() {
                $(this).addClass('transition');
            }, function() {
                $(this).removeClass('transition');
            });
        }
        
        initSlotGameList();
        
        $("#iframe_game").on('load', function () {
            resizeIframe(this)
        });
    });
    
    function initSlotGameList() {
        var game = "{{ request()->type }}";
        
        if (game != '') {
            var gameObj = $("#game_" + game);
            
            if (gameObj.data() != "Y") {
                gameObj.click()
            }
        }
    }
    
    function resizeIframe(obj) {
        obj.style.height = 0;
        obj.style.height = obj.contentWindow.document.body.scrollHeight + 'px';
    }

    function change_iframe(url, image_link, product) {
        $('#iframe_game').attr('src', url);
//        $('.top').show();
//        $('.slot_' + product + '_image').hide();
    }
</script>
@endsection

@section('content')
<div class="midSect bgSlot">
    <div class="cont2">
        @if(in_array('MXB',Session::get('valid_product')))
            <div class="slotMxb">
                {{--<a id="game_mxb" href="javascript:void(0);" onclick="change_iframe('{{route('mxb' , [ 'type' => 'slot' , 'category' => 'Slots' ] )}}',null,'mxb');"><div class="showbox7"></div></a>--}}
                <a id="game_mxb" href="{{ route('slot', ['type' => 'mxb']) }}"><div class="showbox7"></div></a>
            </div>
        @endif
        {{--@if(in_array('PLTB',Session::get('valid_product')) && Session::get('currency') == 'IDR' )--}}
            {{--<div class="slotPt">--}}
                {{--<a id="game_pltb" href="javascript:void(0);" onclick="change_iframe('{{route('pltb' , [ 'type' => 'pgames' , 'category' => '1' ] )}}',null,'pltb');"><div class="showbox8"></div></a>--}}
                {{--<a id="game_pltb" href="{{ route('slot', ['type' => 'pltb']) }}"><div class="showbox8"></div></a>--}}
            {{--</div>--}}
        {{--@endif --}}
		@if(in_array('PLT',Session::get('valid_product')))
            <div class="slotPt" >
                {{--<a id="game_plt" href="javascript:void(0);" onclick="change_iframe('{{route('plt' , [ 'type' => 'pgames' , 'category' => '1' ] )}}',null,'plt');"><div class="showbox8"></div></a>--}}
                <a id="game_plt" href="{{ route('slot', ['type' => 'plt']) }}"><div class="showbox8"></div></a>
            </div>
        @endif
        @if(in_array('SCR',Session::get('valid_product')))
            <div class="slotWft">
                {{--<a id="game_osg" href="javascript:void(0);" onclick="@if(session::get('currency')=='MYR') alert('{{Lang::get('public.ThisDeviceDoesnTSupportMobile')}}') @else alert('Comming Soon')  @endif"><div class="@if(session::get('currency')=='MYR') showbox9 @else showbox16  @endif"></div></a>--}}
                <a id="game_osg" href="javascript:void(0);" data-popup="Y" onclick="@if(session::get('currency')=='MYR') @if (Auth::user()->check()) alert('{{Lang::get('public.ThisDeviceDoesnTSupportMobile')}}') @else alert('{{Lang::get('COMMON.PLEASELOGIN')}}') @endif @else alert('Comming Soon')  @endif"><div class="@if(session::get('currency')=='MYR') showbox9 @else showbox16  @endif"></div></a>
            </div>
        @endif
        @if(in_array('JOK',Session::get('valid_product')))
            <div class="slotJok">
                <a id="game_jok" href="javascript:void(0);" onclick="@if (Auth::user()->check())window.open('{{route('jok')}}','jok_slot','width=1000,height=750')@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif"><div class="showbox13"></div></a>
            </div>
        @endif
        @if(in_array('SKY',Session::get('valid_product')))
            <div class="slotSky">
                {{--<a id="game_sky" href="javascript:void(0);" onclick="change_iframe('{{route('sky' , [ 'type' => 'slot' , 'category' => 'all' ] )}}',null,'sky');"><div class="showbox14"></div></a>--}}
                <a id="game_sky" href="{{ route('slot', ['type' => 'sky']) }}"><div class="showbox14"></div></a>
            </div>
        @endif
        @if(in_array('SPG',Session::get('valid_product')))
            <div class="slotSpg">
                {{--<a id="game_spg" href="javascript:void(0);" onclick="change_iframe('{{route('spg', [ 'type' => 'slot' , 'category' => 'slot' ] )}}',null,'spg');"><div class="showbox15"></div></a>--}}
                <a id="game_spg" href="{{ route('slot', ['type' => 'spg']) }}"><div class="showbox15"></div></a>
            </div>
        @endif
        {{--@if(in_array('OPU',Session::get('valid_product')))--}}
            {{--<div class="slotGp" style="{{ Session::get('currency') == 'IDR' ? 'right: 216px;' : '' }}">--}}
                {{--<a id="game_w88" href="javascript:void(0);" onclick="#"><div class="showbox10"></div></a>--}}
            {{--</div>--}}
        {{--@endif--}}
        <!--<div class="slotCount">-->
            <!--<span>2,123,666.88</span>-->
        <!--</div>-->

        <div class="slotPgtit">
            @if(Lang::getLocale() == 'cn')
                <img src="{{ asset('/royalewin/img/slot-pg-title-ch.PNG') }}">
            @else
                <img src="{{ asset('/royalewin/img/slot-pg-title.png') }}">
            @endif
        </div>

        <div class="clr"></div>
    </div>

    <iframe id="iframe_game" frameborder="0" style="overflow: hidden; overflow-x: hidden; overflow-y: hidden; height: 98%; min-height: 2000px; width: 100%;" src="
            @if( $type == 'mxb' )
                {{route('mxb' , [ 'type' => 'slot' , 'category' => 'Slots' ] )}}
            @elseif( $type == 'w88' || $type == 'OPU' )
                {{route('opu', [ 'type' => 'slot' , 'category' => 'Arcades' ] )}}
            @elseif( $type == 'plt' )
                {{route('plt' , [ 'type' => 'pgames' , 'category' => '1' ] )}}
            @elseif( $type == 'a1a' )
                {{route('a1a')}}
            @elseif( $type == 'spg' )
                {{route('spg', [ 'type' => 'slot' , 'category' => 'slot' ] )}}
            @elseif( $type == 'sky' )
                {{route('sky', [ 'type' => 'slot' , 'category' => 'all' ] )}}
            @endif
        "></iframe>
</div>
@endsection

@section('footer_seo')
@if( $type == 'mxb' )
            <div class="footerContent">
                <div><h1 class="footerTitle">Get More Rewards with MaxBet in RoyaleWin</h1></div>
                <p>
                RoyaleWin is one of the most visited online slot website in Malaysia. With more and more customers joining in to get a share of their prize, it is set in stone that RoyaleWin will be at the top soon. However, this cannot be achieved if RoyaleWin is not supported by the best. One of them is MaxBet.<br><br>
                MaxBet is one of the several slot game providers inside RoyaleWin website. With around 66 live slot games and more coming in the near future, you will see that MaxBet’s list of games are more organized compared to the others. On each slot game’s link, you can see how many reels being used as well as the number of lines that are accepted as wins. This help new players as well as those experienced ones to choose what kind of slot game that they want to play.<br><br>
                Aside from the helpful information on the slot game’s link, what is appealing about MaxBet’s games is that they are in various genre. You can find slot games from historical genre, music genre as well as adventure genre from MaxBet’s RoyaleWin page. With the various genre available, you can always find one that you want to play.<br><br>
                If you have had enough slot game rounds in a day, perhaps you can play other games that MaxBet offer. If you are skillful in playing cards, perhaps you can try playing the table casino games available like Blackjack, Roulette, and Poker. There are also some variations of Poker available like Triple Edge Poker and Caribbean Poker. These games provide generous rewards for the winners to prove their skills!<br><br>
                If table casino games is not your thing, then perhaps you can try playing the mini games by BetMax. Even though it is a mini game, the rewards are still something to be considered. The popular online casino mini game in RoyaleWin website will be the Virtual Racebook 3D. It is a horse racing game with up-to- date graphics that is sure to get your hearts racing, cheering your horse to the finish line.<br><br>
                RoyaleWin is a great online casino website for those who love handsome rewards and responsive call centre. If you ever have any problem with your bets or account, just head over to the 24 hour help service that is professionally trained to deliver the best experience for you.<br><br>
                </p>
                <div class="footerTextL">
                    <div><h3 class="footerTitle">Why Royalewin</h3></div>
                    <div class="footerSub">Trusted Malaysia Online Casino - Your Premier Gaming Destination</div>
                    <p>
                        Play Baccarat, Blackjack, Roulette, Slots, Sports Betting with exciting promotions, 24/7 top of the line customer service & timely payouts with the highest level of security. <br><br>
                        Royalewin focused primarily in offering casino gaming products and services in Asia Pacific markets. Bringing you all the fun of a real casino in your home, we are committed to provide you gaming entertainment of premium quality and at exceptionally good value.<br><br>
                    </p>
                </div>
                <div class="footerTextL">
                    <div><h3 class="footerTitle">Our Games</h3></div>
                    <div class="footerSub">Live Dealer Casinos, Sportsbook, Slots, 4D</div>
                    <p>
                        Find a wide variety of top-class games, including classic favourites like Baccarat, Blackjack and Roulette, as well as popular themed slots, progressives, live casino with real dealers, live sportsbook betting, CFD (commodity, Forex, Index), and 4D.<br><br>
                        Royalewin offers superb quality games using WinningFT, MaxBet, Betsoft, XProGaming, Ho Gaming, AGGaming etc. softwares with no download required all in one advanced gaming platform.<br><br>
                    </p>
                </div>
                <div class="footerTextL">
                    <div><h3 class="footerTitle">24/7 Support</h3></div>
                    <div class="footerSub">24-Hour Customer Service</div>
                    <p>
                        Available 24 hours everyday, Royalewin Support Team is always there for you - to help answer your questions and resolve your issues as quickly and efficiently a possible.<br><br>
                        We focus on the needs of our customers by staying true to our core service principles: <br><br>
                        • The customer always come first  <br>
                        • Your satisfaction drives our business  <br>
                        • We will always strive to exceed your expectations  <br><br>
                    </p>
                </div>
                <div class="footerTextL">
                    <div><h3 class="footerTitle">Bonus Offers</h3></div>
                    <div class="footerSub">FREE Bonuses Up to 110%</div>
                    <p>
                        Royalewin loves to handsomely reward you for your support! Players at Royalewin.com are able to enjoy a fantastic selection of daily and weekly promotional offers. <br><br>
                        Our amazing range of promotions include an exclusive 110% Welcome Bonus for first-time depositors, double Daily Deposit Bonuses, Cashbacks for everything you played at our casino.<br><br>
                    </p>
                </div>
                <div class="clr"></div>
            </div>
@elseif( $type == 'plt' )
            <div class="footerContent">
                <div><h1 class="footerTitle">Why PlayTech Slot Games Is The Best</h1></div>
                <p>
                It is indisputable that RoyaleWin is the best online casino in Malaysia. What makes it the best is because the company partners with a lot of prestigious game providers in the world. One of them is none other than PlayTech.<br><br>
                You may have heard about PlayTech if you have been an online casino player for a while. This is because PlayTech can be called as the behemoth of the online casino industry. Established almost 20 years ago, it is one of the few that have brand names on their slot games through licensing agreement. This agreement is what makes PlayTech slot games the best among the rest. In RoyaleWin, there are currently 21 branded games which includes Top Gun, Rocky, Marilyn Monroe and Kong that caters to everyone, new and experienced players alike.<br><br>
                Aside from the branded games, PlayTech is always progressive in building their online casino games portfolio. To date, there are currently more than 200 slot games that has been developed by PlayTech. This large array of games makes PlayTech favorable to online casinos as well as players. The more games you have, the wider range of audience that you can attract to get new players. From the 200 PlayTech slot games available, 150 of them is available inside RoyaleWin.<br><br>
                Another reason why PlayTech is the best slot game provider in RoyaleWin is because of the game quality. You can really differentiate PlayTech online casino game with the rest. If you see high quality graphics and experience mesmerizing sound effects from your online slot game, it is surely made by PlayTech. Aside from modern casino slots, PlayTech is also a provider for classic slot games that has basic gameplay and graphics that appeal to the older, more experienced players.<br><br>
                If you think you have what it takes to make a living from playing slot games and online casinos in Malaysia, you have to play the slot games by PlayTech that is available inside RoyaleWin. If you are not in front of your laptops or computers, you can always play RoyaleWin from your mobile phones.<br><br>
                </p>
                <div class="footerTextL">
                    <div><h3 class="footerTitle">Why Royalewin</h3></div>
                    <div class="footerSub">Trusted Malaysia Online Casino - Your Premier Gaming Destination</div>
                    <p>
                        Play Baccarat, Blackjack, Roulette, Slots, Sports Betting with exciting promotions, 24/7 top of the line customer service & timely payouts with the highest level of security. <br><br>
                        Royalewin focused primarily in offering casino gaming products and services in Asia Pacific markets. Bringing you all the fun of a real casino in your home, we are committed to provide you gaming entertainment of premium quality and at exceptionally good value.<br><br>
                    </p>
                </div>
                <div class="footerTextL">
                    <div><h3 class="footerTitle">Our Games</h3></div>
                    <div class="footerSub">Live Dealer Casinos, Sportsbook, Slots, 4D</div>
                    <p>
                        Find a wide variety of top-class games, including classic favourites like Baccarat, Blackjack and Roulette, as well as popular themed slots, progressives, live casino with real dealers, live sportsbook betting, CFD (commodity, Forex, Index), and 4D.<br><br>
                        Royalewin offers superb quality games using WinningFT, MaxBet, Betsoft, XProGaming, Ho Gaming, AGGaming etc. softwares with no download required all in one advanced gaming platform.<br><br>
                    </p>
                </div>
                <div class="footerTextL">
                    <div><h3 class="footerTitle">24/7 Support</h3></div>
                    <div class="footerSub">24-Hour Customer Service</div>
                    <p>
                        Available 24 hours everyday, Royalewin Support Team is always there for you - to help answer your questions and resolve your issues as quickly and efficiently a possible.<br><br>
                        We focus on the needs of our customers by staying true to our core service principles: <br><br>
                        • The customer always come first  <br>
                        • Your satisfaction drives our business  <br>
                        • We will always strive to exceed your expectations  <br><br>
                    </p>
                </div>
                <div class="footerTextL">
                    <div><h3 class="footerTitle">Bonus Offers</h3></div>
                    <div class="footerSub">FREE Bonuses Up to 110%</div>
                    <p>
                        Royalewin loves to handsomely reward you for your support! Players at Royalewin.com are able to enjoy a fantastic selection of daily and weekly promotional offers. <br><br>
                        Our amazing range of promotions include an exclusive 110% Welcome Bonus for first-time depositors, double Daily Deposit Bonuses, Cashbacks for everything you played at our casino.<br><br>
                    </p>
                </div>
                <div class="clr"></div>
            </div>
@elseif( $type == 'sky' )
            <div class="footerContent">
                <div><h1 class="footerTitle">Sky3888 Slot Games in RoyaleWin</h1></div>
                <p>
                One of the top slot game developer, Sky3888, is a part of RoyaleWin. Comapring Sky3888 with other game developers like PlayTech and Spade Gaming, you will see that Sky3888 is more likeable and preferred by Malaysians. This is because they are made in Malaysia; Sky3888 is one of the few successful casino and slot game developers that originated within the country.<br><br>
                There are a total of 85 Sky3888 slot games available in RoyaleWin. You can see that Sky3888 offers more variety in the slot games’ genre. There are sports slot games, anime slot games, oriental slot games as well as animal slot games. Moreover, all these slot games have outstanding graphics that will make you play more. With these kinds of slot games available in RoyaleWin online casino, you will surely have a good time winning your bets here.<br><br>
                If you want a game recommendation, perhaps the most beginner-friendly one will be Monkey King. In this game, you will be able to choose a character before starting the game. What is most interesting about Monkey King is that there are a set of events that can happen randomly that will give you different kinds of rewards.<br><br>
                Another unique thing about Sky3888 is that they even offer multiplayer games to RoyaleWin users, namely Texas Poker and Dou Di Zhu. As like other Sky3888 games, these two games also offer great rewards to the winners. If you want to take a short break after a losing or winning streak, playing these non-slot games can help you to refresh your mind and get your gambling mojo back.<br><br>
                If you would love to play one of the many games by Sky3888, head on to the game room in RoyaleWin to give it a go. RoyaleWin is currently having a promotion where there will be free bonuses up to 110% of the total bet! This is one of the reason why RoyaleWin is a head above the rest. RoyaleWin loves to handsomely reward their loyal players with great gifts and bonuses. Why don't you join the fun as well?<br><br>
                </p>
                <div class="footerTextL">
                    <div><h3 class="footerTitle">Why Royalewin</h3></div>
                    <div class="footerSub">Trusted Malaysia Online Casino - Your Premier Gaming Destination</div>
                    <p>
                        Play Baccarat, Blackjack, Roulette, Slots, Sports Betting with exciting promotions, 24/7 top of the line customer service & timely payouts with the highest level of security. <br><br>
                        Royalewin focused primarily in offering casino gaming products and services in Asia Pacific markets. Bringing you all the fun of a real casino in your home, we are committed to provide you gaming entertainment of premium quality and at exceptionally good value.<br><br>
                    </p>
                </div>
                <div class="footerTextL">
                    <div><h3 class="footerTitle">Our Games</h3></div>
                    <div class="footerSub">Live Dealer Casinos, Sportsbook, Slots, 4D</div>
                    <p>
                        Find a wide variety of top-class games, including classic favourites like Baccarat, Blackjack and Roulette, as well as popular themed slots, progressives, live casino with real dealers, live sportsbook betting, CFD (commodity, Forex, Index), and 4D.<br><br>
                        Royalewin offers superb quality games using WinningFT, MaxBet, Betsoft, XProGaming, Ho Gaming, AGGaming etc. softwares with no download required all in one advanced gaming platform.<br><br>
                    </p>
                </div>
                <div class="footerTextL">
                    <div><h3 class="footerTitle">24/7 Support</h3></div>
                    <div class="footerSub">24-Hour Customer Service</div>
                    <p>
                        Available 24 hours everyday, Royalewin Support Team is always there for you - to help answer your questions and resolve your issues as quickly and efficiently a possible.<br><br>
                        We focus on the needs of our customers by staying true to our core service principles: <br><br>
                        • The customer always come first  <br>
                        • Your satisfaction drives our business  <br>
                        • We will always strive to exceed your expectations  <br><br>
                    </p>
                </div>
                <div class="footerTextL">
                    <div><h3 class="footerTitle">Bonus Offers</h3></div>
                    <div class="footerSub">FREE Bonuses Up to 110%</div>
                    <p>
                        Royalewin loves to handsomely reward you for your support! Players at Royalewin.com are able to enjoy a fantastic selection of daily and weekly promotional offers. <br><br>
                        Our amazing range of promotions include an exclusive 110% Welcome Bonus for first-time depositors, double Daily Deposit Bonuses, Cashbacks for everything you played at our casino.<br><br>
                    </p>
                </div>
                <div class="clr"></div>
            </div>
@elseif( $type == 'spg' )
            <div class="footerContent">
                <div><h1 class="footerTitle">Three Spade Gaming Games You Must Play at RoyaleWin</h1></div>
                <p>
                RoyaleWin is one of the popular online casino provider in Malaysia. It is not an easy feat to be the best; and part of it is because of their collaboration with Spade Gaming. Spade Gaming is the better— if not the best— casino game provider in Asia region.Based in the Philippines, it has multiple offices in a lot of countries including Malaysia. With Spade Gaming’s expertise in creating fair and pleasurable games, RoyaleWin is able to be the champion of online slot games in Malaysia.<br><br>
                Spade Gaming offers more than a hundred unique online slot games for their customers. The more slot games offered, the greater the chance for players to win. Perhaps one of the more popular ones available is Golden Chicken. Inspired by Aesop‘s Fables with a creative Chinese twist, it is a perfect blend of ancient eastern and western cultures that make for a really fun and interesting game. It offers 1024 ways to win with a chance to win at most x40 of the players’ total bet.<br><br>
                If you prefer a simpler slot game without negating the reward and fun of winning a big bag of money, the Ho Yeah Monkey game is the one we recommend. It only offers 1 way to win but the reward is huge! Up to 2000x of the total bet! This slot game is easily one of the most playable online slot game in Malaysia because of the big reward.<br><br>
                If you only want to play the newest games by Spade Gaming, Gangster Axe will be the one to seek. Just released in January 2018, it has a unique slot gameplay which might be a bit difficult for new players to understand. However, once you get the hang of it, Gangster Axe will be the game that you will keep playing day after day. Because it is new, Gangster Axe offers enthralling graphic for a better player experience.<br><br>
                If you are still deciding on what slot game to play, you will miss the rewards that is being offered to early birds. Therefore, you must act now! Log in to RoyaleWin and play any of the games by Spade Gaming. You will be thankful that you made this choice.<br><br>
                </p>
                <div class="footerTextL">
                    <div><h3 class="footerTitle">Why Royalewin</h3></div>
                    <div class="footerSub">Trusted Malaysia Online Casino - Your Premier Gaming Destination</div>
                    <p>
                        Play Baccarat, Blackjack, Roulette, Slots, Sports Betting with exciting promotions, 24/7 top of the line customer service & timely payouts with the highest level of security. <br><br>
                        Royalewin focused primarily in offering casino gaming products and services in Asia Pacific markets. Bringing you all the fun of a real casino in your home, we are committed to provide you gaming entertainment of premium quality and at exceptionally good value.<br><br>
                    </p>
                </div>
                <div class="footerTextL">
                    <div><h3 class="footerTitle">Our Games</h3></div>
                    <div class="footerSub">Live Dealer Casinos, Sportsbook, Slots, 4D</div>
                    <p>
                        Find a wide variety of top-class games, including classic favourites like Baccarat, Blackjack and Roulette, as well as popular themed slots, progressives, live casino with real dealers, live sportsbook betting, CFD (commodity, Forex, Index), and 4D.<br><br>
                        Royalewin offers superb quality games using WinningFT, MaxBet, Betsoft, XProGaming, Ho Gaming, AGGaming etc. softwares with no download required all in one advanced gaming platform.<br><br>
                    </p>
                </div>
                <div class="footerTextL">
                    <div><h3 class="footerTitle">24/7 Support</h3></div>
                    <div class="footerSub">24-Hour Customer Service</div>
                    <p>
                        Available 24 hours everyday, Royalewin Support Team is always there for you - to help answer your questions and resolve your issues as quickly and efficiently a possible.<br><br>
                        We focus on the needs of our customers by staying true to our core service principles: <br><br>
                        • The customer always come first  <br>
                        • Your satisfaction drives our business  <br>
                        • We will always strive to exceed your expectations  <br><br>
                    </p>
                </div>
                <div class="footerTextL">
                    <div><h3 class="footerTitle">Bonus Offers</h3></div>
                    <div class="footerSub">FREE Bonuses Up to 110%</div>
                    <p>
                        Royalewin loves to handsomely reward you for your support! Players at Royalewin.com are able to enjoy a fantastic selection of daily and weekly promotional offers. <br><br>
                        Our amazing range of promotions include an exclusive 110% Welcome Bonus for first-time depositors, double Daily Deposit Bonuses, Cashbacks for everything you played at our casino.<br><br>
                    </p>
                </div>
                <div class="clr"></div>
            </div>
@endif
@endsection