@extends('royalewin/master')

@if (Session::get('currency') == 'MYR')
    @section('title', 'Best Online Live Casino Games in Malaysia')
    @section('keywords', 'Best Online Live Casino Games in Malaysia')
    @section('description', 'A new live online casino experience from RoyaleWin Malaysia, including roulette, blackjack and slots. Sign Up TODAY and get Free Spin Bonus!')
@elseif (Session::get('currency') == 'IDR')
    @section('title', 'Bandar Casino online, Situs Judi Live Casino Indonesia')
    @section('keywords', 'Bandar Casino online, Situs Judi Live Casino Indonesia')
    @section('description', 'RoyaleWin Agen Judi Online Live Casino menyediakan permainan taruhan live casino dan taruhan online Terpercaya dan Terbaik di Indonesia.')
@endif

@section('css')
@parent

<style type="text/css">
    .thumbGameImg { cursor: pointer; }
    
    #bigPlayButton {
        display: none;
        height: 55px;
        width: 207px;
        position: absolute;
        top: 305px;
        left: 429px;
        cursor: pointer;
    }
</style>

@endsection

@section('js')
@parent

<script type="text/javascript">
    $(function () {
        var game = "{{ Request::input('type') }}";
        
        if (game.length < 1) {
            onGameImgChanged($(".bigThumb li:eq(0)"), false);
        } else {
            onGameImgChanged($("li[data-game='" + game + "']"), false);
        }
    });
    
    function onGameImgChanged(obj, lunch) {
        var tObj = $(obj);
        
        if (tObj.length) {
            $(".lcSelected").removeClass("lcSelected");
            tObj.addClass("lcSelected");
            
            $("#bigGameImg").attr("src", "{{ asset('/royalewin/img') }}/lc-" + tObj.data("game") + "-big.png");
            $("#bigPlayButton").data("game", tObj.data("game")).show();
        } else {
            $("#bigPlayButton").hide();
        }
        
        if (lunch == true) {
             lunchGame(obj);
        }
    }
    
    function lunchGame(obj) {
        var game = $(obj).data("game");
        
        @if (!Auth::user()->check())
            alert('{{Lang::get('COMMON.PLEASELOGIN')}}');
        @else
            var gameObj = $("li[data-game='" + game + "']");
        
            if (gameObj.data("popup") == "N") {
                window.location.href = gameObj.data("url");
            } else if (game == 'eg') {
                window.open(gameObj.data("url"), "casino_" + game, "width=1042,height=880");
            } else if (game == 'gp') {
                window.open(gameObj.data("url"), "casino_" + game, "width=1146,height=866");
            } else {
                window.open(gameObj.data("url"), "casino_" + game, "width=1150,height=830");
            }
        @endif
    }
</script>

@endsection

@section('content')
<div class="midSect bgLc">
    <div class="cont1">
		<div class="bigSelect">
            <div style="position: relative;">
                <a href="{{ route('promotion') }}"><img src="{{ asset('/royalewin/img/RW.gif') }}"></a>
            </div>
        </div>
        <div class="bigSelect">
            <div style="position: relative;">
                <img id="bigGameImg" src="">
                <div id="bigPlayButton" onclick="lunchGame(this);">&nbsp;</div>
            </div>
        </div>
		
        <div class="bigThumb">
            <ul>
                @if(in_array('MXB',Session::get('valid_product')))
                    <li class="thumbGameImg" data-game="mxb" data-popup="N" data-url="{{ Auth::user()->check() ? route('mxb', [ 'type' => 'casino' , 'category' => 'baccarat' ]) : '' }}" onclick="onGameImgChanged(this, true);">
                        <img src="{{ asset('/royalewin/img/lc-mxb-thumb.png') }}" width="350">
                    </li>
                @endif
                @if(in_array('AGG',Session::get('valid_product')))
                    <li class="thumbGameImg" data-game="ag" data-popup="Y" data-url="{{ Auth::user()->check() ? route('agg') : '' }}" onclick="onGameImgChanged(this, true);">
                        <img src="{{ asset('/royalewin/img/lc-ag-thumb.png') }}" width="350">
                    </li>
                @endif
                @if(in_array('PLT',Session::get('valid_product')))
                    <li class="thumbGameImg" data-game="pt" data-popup="Y" data-url="{{ Auth::user()->check() ? route('pltiframe') : '' }}" onclick="onGameImgChanged(this, true);">
                        <img src="{{ asset('/royalewin/img/lc-pt-thumb.png') }}" width="350">
                    </li>
                @endif
                {{--@if(in_array('OPU',Session::get('valid_product')))--}}
                    {{--<li class="thumbGameImg" data-game="gp" data-popup="Y" data-url="{{ Auth::user()->check() ? route('opu' , [ 'type' => 'casino' , 'category' => 'live']) : '' }}" onclick="onGameImgChanged(this, true);">--}}
                        {{--<img src="{{ asset('/royalewin/img/lc-gp-thumb.png') }}" width="350">--}}
                    {{--</li>--}}
                {{--@endif--}}
                @if(in_array('VGS',Session::get('valid_product')))
                    <li class="thumbGameImg" data-game="eg" data-popup="Y" data-url="{{ Auth::user()->check() ? route('vgs', ['type' => 'casino']) : '' }}" onclick="onGameImgChanged(this, true);">
                        <img src="{{ asset('/royalewin/img/lc-eg-thumb.png') }}" width="350">
                    </li>
                @endif
                @if(in_array('HOG',Session::get('valid_product')))
                    <li class="thumbGameImg" data-game="as" data-popup="Y" data-url="{{ Auth::user()->check() ? route('hog') : '' }}" onclick="onGameImgChanged(this, true);">
                        <img src="{{ asset('/royalewin/img/lc-as-thumb.png') }}" width="350">
                    </li>
                @endif
                @if(in_array('ALB',Session::get('valid_product')))
                    <li class="thumbGameImg" data-game="allbet" data-popup="Y" data-url="{{ Auth::user()->check() ? route('alb') : '' }}" onclick="onGameImgChanged(this, true);">
                        <img src="{{ asset('/royalewin/img/lc-allbet-thumb.png') }}" width="350">
                    </li>
                @endif
				@if(Session::get('currency')=='MYR')
				@if(in_array('WMC',Session::get('valid_product')))
                    <li class="thumbGameImg" data-game="wm" data-popup="Y" data-url="{{ Auth::user()->check() ? route('wmcframe') : '' }}" onclick="onGameImgChanged(this, true);">
                        <img src="{{ asset('/royalewin/img/lc-wm-thumb.png') }}" width="350">
                    </li>
                @endif
				@endif
				
            </ul>
        </div>
        <div class="clr"></div>
    </div>
</div>
@endsection

@section('footer_seo')
            <div class="footerContent">
                <div><h1 class="footerTitle">Play Live Casino Games at RoyaleWin</h1></div>
                <p>
                RoyaleWin is a Malaysian online casino that is giving one of the most authentic casino experience to their customers. With live, beautiful dealers setting up the casinos as well as the high stakes that can be put on the line, players old and new will surely be enticed to play round after round. RoyaleWin knows what the customers want, so they have a lot of online casino to be chosen from, provided by different casino providers.<br><br>
                The first one in the list is the Euro Casino. The Euro casino is provided by one of the most trusted online casino provider in Malaysia, MaxBet. As the name implies, the Euro casino has live European dealers at your service. With RoyaleWin, you not only can play the games with the dealer; you can also have a chat with them.<br><br>
                The next live online casino in RoyaleWin is Crown Casino. This casino is created by a very big and established online casino game provider in China, Asia Gaming. With a big company providing support and service to it, it is no surprise that Crown Casino is one of the most sought live casino in Malaysia. Some of the Crown Casino games available for Crown Casino users are baccarat, roulette, blackjack and Texas Hold’em poker. With gorgeous and professional dealers, it will surely take the enjoyment of these games to a whole new level!<br><br>
                Joining the list is PlayTech Casino. PlayTech is the largest live casino supplier in Europe, so you will surely be dazzled by the quality of games and dealers provided. With state of the art broadcast technology implemented, you will not miss even one second of action while playing the games. Another company providing the same level of experience as PlayTech will be Evolution Casino by Evolution Club.<br><br>
                If you prefer to play with companies nearer to our country, then the live casino by AllBet is your best bet. AllBet is made in China, so they have native-speaking dealers available. Therefore, you can find Mandarin speaking dealers that are not only beautiful but also professionally trained for their role. As like AllBet, Asia Casino by Ho Gaming is also joining the list of live casino providers in RoyaleWin.<br><br>
                Lastly, no matter what casino you choose to go to, always ensure that you know what you are playing. If you are a new player, learn the games and ask for help from the more experienced players. If you do it right, you will get a lot of earnings from live casinos at the comfort of your home.<br><br>
                </p>
                <div class="footerTextL">
                    <div><h3 class="footerTitle">Why Royalewin</h3></div>
                    <div class="footerSub">Trusted Malaysia Online Casino - Your Premier Gaming Destination</div>
                    <p>
                        Play Baccarat, Blackjack, Roulette, Slots, Sports Betting with exciting promotions, 24/7 top of the line customer service & timely payouts with the highest level of security. <br><br>
                        Royalewin focused primarily in offering casino gaming products and services in Asia Pacific markets. Bringing you all the fun of a real casino in your home, we are committed to provide you gaming entertainment of premium quality and at exceptionally good value.<br><br>
                    </p>
                </div>
                <div class="footerTextL">
                    <div><h3 class="footerTitle">Our Games</h3></div>
                    <div class="footerSub">Live Dealer Casinos, Sportsbook, Slots, 4D</div>
                    <p>
                        Find a wide variety of top-class games, including classic favourites like Baccarat, Blackjack and Roulette, as well as popular themed slots, progressives, live casino with real dealers, live sportsbook betting, CFD (commodity, Forex, Index), and 4D.<br><br>
                        Royalewin offers superb quality games using WinningFT, MaxBet, Betsoft, XProGaming, Ho Gaming, AGGaming etc. softwares with no download required all in one advanced gaming platform.<br><br>
                    </p>
                </div>
                <div class="footerTextL">
                    <div><h3 class="footerTitle">24/7 Support</h3></div>
                    <div class="footerSub">24-Hour Customer Service</div>
                    <p>
                        Available 24 hours everyday, Royalewin Support Team is always there for you - to help answer your questions and resolve your issues as quickly and efficiently a possible.<br><br>
                        We focus on the needs of our customers by staying true to our core service principles: <br><br>
                        • The customer always come first  <br>
                        • Your satisfaction drives our business  <br>
                        • We will always strive to exceed your expectations  <br><br>
                    </p>
                </div>
                <div class="footerTextL">
                    <div><h3 class="footerTitle">Bonus Offers</h3></div>
                    <div class="footerSub">FREE Bonuses Up to 110%</div>
                    <p>
                        Royalewin loves to handsomely reward you for your support! Players at Royalewin.com are able to enjoy a fantastic selection of daily and weekly promotional offers. <br><br>
                        Our amazing range of promotions include an exclusive 110% Welcome Bonus for first-time depositors, double Daily Deposit Bonuses, Cashbacks for everything you played at our casino.<br><br>
                    </p>
                </div>
                <div class="clr"></div>
            </div>
@endsection