@extends('royalewin/mobile/master')

@if (Session::get('currency') == 'MYR')
    @section('title', 'Best Online Live Casino Malaysia - FAQ')
    @section('keywords', 'Best Online Live Casino Malaysia - FAQ')
    @section('description', 'Find an answer to any of your queries regarding RoyaleWin Online Casino Malaysia.')
@elseif (Session::get('currency') == 'IDR')
    @section('title', 'Bandar Casino Online, Situs Judi Indonesia - Tanya Jawab')
    @section('keywords', 'Bandar Casino Online, Situs Judi Indonesia - Tanya Jawab')
    @section('description', 'Pertanyaan - RoyaleWin adalah agen bola, bandar casino online dan situs judi online terpercaya untuk Anda penggemar taruhan bola dan pecinta judi online di Indonesia.')
@endif

@section('css')
    @parent

    <style type="text/css">
        .content span {
            color: #ffcc01;
            display: block;
            font-size: 12px;
            font-weight: bold;
            height: auto;
            margin: 15px auto 0;
            padding: 0;
            position: relative;
            text-align: left;
            text-transform: uppercase;
        }

        .content p {
            text-align: justify;
        }

        .index_links a {
            color: #fff;
        }
    </style>
@endsection

@section('js')
    @parent

    <script type="text/javascript">
        $(function () {
            var html = "";

            $(".section_title").each(function () {
                var obj = $(this);

                html += '<li><a href="#' + obj.attr("id") + '">' + obj.html() + '</a></li>';
            });

            $("ol.index_links").html(html);
        });
    </script>
@endsection

@section('content')
    <!-- Page Content -->
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-xs-12 contOthers">
                <h4>{{ strtoupper(Lang::get('public.FAQ')) }}</h4>
            </div>
            <div class="col-md-12 col-xs-12 contOthers content">
                {!! htmlspecialchars_decode($content) !!}
            </div>
        </div>
    </div>
@endsection
