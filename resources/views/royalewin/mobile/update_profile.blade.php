@extends('royalewin/mobile/master')

@section('title', 'Profile')

@section('css')
    @parent

    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
@endsection

@section('js')
    @parent

    <script src="{{ asset('/royalewin/resources/js/jquery-ui.js') }}"></script>
    <script type="text/javascript">
        $(function () {
            $(".datepicker").datepicker({dateFormat: 'yy-mm-dd', defaultDate: new Date()});

            load_profile();
        });

        // Update password.
        function checkPass() {
            //Store the password field objects into variables ...
            var new_password = document.getElementById('new_password');
            var confirm_new_password = document.getElementById('confirm_new_password');
            //Store the Confimation Message Object ...
            var message = document.getElementById('confirmMessage');
            //Set the colors we will be using ...
            var goodColor = "#66cc66";
            var badColor = "#ff6666";
            //Compare the values in the password field and the confirmation field
            if (new_password.value == confirm_new_password.value) {
                //The passwords match.
                //Set the color to the good color and inform the user that they have entered the correct password
                confirm_new_password.style.backgroundColor = goodColor;
                message.style.color = goodColor;
                message.innerHTML = "Passwords Match!"
            } else {
                //The passwords do not match.
                //Set the color to the bad color and notify the user.
                confirm_new_password.style.backgroundColor = badColor;
                message.style.color = badColor;
                message.innerHTML = "Passwords Do Not Match!"
            }
        }

        function update_password() {
            $.ajax({
                type: "POST",
                url: "{{action('User\MemberController@ChangePassword')}}",
                data: {
                    _token: "{{ csrf_token() }}",
                    current_password: $('#current_password').val(),
                    new_password: $('#new_password').val(),
                    confirm_new_password: $('#confirm_new_password').val()
                },
            }).done(function (json) {
                obj = JSON.parse(json);
                var str = '';
                $.each(obj, function (i, item) {
                    str += item + '\n';
                });
                if (str != '') {
                    alert(str);
                }
            });
        }
        // End update password.

        // Update personal info.
        function load_profile() {
            $.ajax({
                url: "{{ route('update-profile') }}",
                type: "GET",
                dataType: "JSON",
                data: {
                    _token: "{{ csrf_token() }}",
                    as_json: true
                },
                success: function (result) {
                    if (typeof result != undefined && result != null) {
                        $("#profile_email").val(result.email);
                        $("#profile_fullname").val(result.fullname);
                        $("#profile_telmobile").val(result.telmobile);
                        $("#profile_gender").val(result.gender);
                        $("#profile_address").val(result.resaddress);
                        $("#profile_city").val(result.rescity);
                        $("#profile_dob_hdn").val(result.dob);
                        $("#profile_referral_id").val(result.referralid);

                        $("#profile_referral_id_link").attr("href", "{{ route('register') }}?referralid=" + result.referralid);

                        if (result.dob == "0000-00-00") {
                            $("#profile_dob_writeable").show();
                            $("#profile_dob_readonly").hide();
                        } else {
                            $("#profile_dob_writeable").hide();
                            $("#profile_dob_readonly").show();
                            $("#profile_dob").val(result.dob);
                        }
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    setTimeout(function () {
                        load_profile();
                    }, 1000);
                }
            });
        }

        function update_profile() {
            var dob = $("#profile_dob_hdn").val();
            if (typeof $('#dob_year').val() != undefined) {
                dob = $('#dob_year').val() + '-' + $('#dob_month').val() + '-' + $('#dob_day').val();
            }
            $.ajax({
                type: "POST",
                url: "{{action('User\MemberController@UpdateDetail')}}",
                data: {
                    _token: "{{ csrf_token() }}",
                    gender: $('#profile_gender').val(),
                    dob: dob,
                    address: $("#profile_address").val(),
                    city: $("#profile_city").val()
                }
            }).done(function (json) {
                obj = JSON.parse(json);
                var str = '';
                $.each(obj, function (i, item) {
                    str += item + '\n';
                });
                if (str != '') {
                    alert(str);
                }
            });
        }
        // End update personal info.
    </script>
@endsection

@section('content')
    <!-- Page Content -->
    <div class="container">

        @include('royalewin/mobile/include/profile_menu', array('selected' => 'Profile'))

        <div id="password_form" class="row bdr2 nopad">
            <div class="col-lg-12 nopad">
                <div class="col-lg-12 nopad">
                    <div class="wTit">
                        {{ Lang::get('public.Profile') }} ({{ Lang::get('public.ChangePassword') }})
                    </div>
                </div>
            </div>

            <div class="col-lg-12 nopad wallRowtit">
                <div class="row">
                    <div class="col-xs-5">
                        <label>{{ Lang::get('public.CurrentPassword') }}*</label>
                    </div>
                    <div class="col-xs-5">
                        <input type="password" id="current_password" name="current_password" />
                    </div>
                </div>
            </div>

            <div class="col-lg-12 nopad wallRowtit">
                <div class="row">
                    <div class="col-xs-5">
                        <label>{{ Lang::get('public.NewPassword') }}*</label>
                    </div>
                    <div class="col-xs-5">
                        <input type="password" id="new_password" name="new_password" />
                        <p>{{ Lang::get('public.NewPasswordNote2') }}</p>
                    </div>
                </div>
            </div>

            <div class="col-lg-12 nopad wallRowtit">
                <div class="row">
                    <div class="col-xs-5">
                        <label>{{ Lang::get('public.ReenterPassword') }}*</label>
                    </div>
                    <div class="col-xs-5">
                        <input type="password" id="confirm_new_password" name="confirm_new_password" onkeyup="checkPass(); return false;" />
                        <p><span id="confirmMessage" class="confirmMessage"></span></p>
                    </div>
                </div>
            </div>
        </div>

        <div class="ht1"></div>

        <div class="row">
            <div class="col-xs-3">
                <button type="button" class="btn btn-primary" onclick="update_password()">{{ Lang::get('public.Submit') }}</button>
            </div>
            <div class="col-xs-3">
                <button type="button" class="btn btn-primary" onclick="resetForm('password_form');">{{ Lang::get('public.Reset') }}</button>
            </div>
        </div>

        <div class="ht1"></div>

        <div id="personal_info_form" class="row bdr2 nopad">
            <div class="col-lg-12 nopad">
                <div class="col-lg-12 nopad">
                    <div class="wTit">
                        {{ Lang::get('COMMON.PERSONALINFO') }}
                    </div>
                </div>
            </div>

            <div class="col-lg-12 nopad wallRowtit">
                <div class="row">
                    <div class="col-xs-5">
                        <label>{{ Lang::get('public.Email') }}</label>
                    </div>
                    <div class="col-xs-5">
                        <input type="text" id="profile_email" readonly />
                    </div>
                </div>
            </div>

            <div class="col-lg-12 nopad wallRowtit">
                <div class="row">
                    <div class="col-xs-5">
                        <label>{{ Lang::get('public.FullName') }}</label>
                    </div>
                    <div class="col-xs-5">
                        <input type="text" id="profile_fullname" readonly />
                    </div>
                </div>
            </div>

            <div class="col-lg-12 nopad wallRowtit">
                <div class="row">
                    <div class="col-xs-5">
                        <label>{{ Lang::get('public.MyReferralID') }}</label>
                    </div>
                    <div class="col-xs-5">
                        <input type="text" id="profile_referral_id" readonly />
                        <br>
                        <br>
                        <a href="#" id="profile_referral_id_link" target="_blank" style="color: #ffffff;">{{ Lang::get('public.RegisterDownline') }}</a>
                    </div>
                </div>
            </div>

            <div class="col-lg-12 nopad wallRowtit">
                <div class="row">
                    <div class="col-xs-5">
                        <label>{{ Lang::get('public.MobileNumber') }}</label>
                    </div>
                    <div class="col-xs-5">
                        <input type="text" id="profile_telmobile" readonly />
                    </div>
                </div>
            </div>

            <div class="col-lg-12 nopad wallRowtit">
                <div class="row">
                    <div class="col-xs-5">
                        <label>{{ Lang::get('public.Gender') }}</label>
                    </div>
                    <div class="col-xs-5">
                        <select id="profile_gender" name="gender">
                            <option value="1">{{ Lang::get('public.Male') }}</option>
                            <option value="2">{{ Lang::get('public.Female') }}</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="col-lg-12 nopad wallRowtit">
                <div class="row">
                    <div class="col-xs-5">
                        <label>{{ Lang::get('public.DOB') }}</label>
                        <input type="hidden" id="profile_dob_hdn" />
                    </div>
                    <div id="profile_dob_writeable" class="col-xs-3">
                        <select id="dob_day">
                            @foreach (\App\Models\AccountDetail::getDobDayOptions() as $key => $val)
                                <option value="{{ $key }}">{{ $val }}</option>
                            @endforeach
                        </select>
                        <select id="dob_month">
                            @foreach (\App\Models\AccountDetail::getDobMonthOptions() as $key => $val)
                                <option value="{{ $key }}">{{ $val }}</option>
                            @endforeach
                        </select>
                        <select id="dob_year">
                            @foreach (\App\Models\AccountDetail::getDobYearOptions() as $key => $val)
                                <option value="{{ $key }}">{{ $val }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div id="profile_dob_readonly" class="col-xs-3">
                        <input type="text" id="profile_dob" readonly />
                    </div>
                </div>
            </div>

            <div class="col-lg-12 nopad wallRowtit">
                <div class="row">
                    <div class="col-xs-5">
                        <label>{{ Lang::get('COMMON.ADDRESS') }}</label>
                    </div>
                    <div class="col-xs-5">
                        <input type="text" id="profile_address" />
                    </div>
                </div>
            </div>

            <div class="col-lg-12 nopad wallRowtit">
                <div class="row">
                    <div class="col-xs-5">
                        <label>{{ Lang::get('COMMON.CITY') }}</label>
                    </div>
                    <div class="col-xs-5">
                        <input type="text" id="profile_city" />
                    </div>
                </div>
            </div>
        </div>

        <div class="ht1"></div>

        <div class="row">
            <div class="col-xs-3">
                <button type="button" class="btn btn-primary" onclick="update_profile()">{{ Lang::get('public.Submit') }}</button>
            </div>
            <div class="col-xs-3">
                <button type="button" class="btn btn-primary" onclick="resetForm('personal_info_form');">{{ Lang::get('public.Reset') }}</button>
            </div>
        </div>
    </div>
@endsection
