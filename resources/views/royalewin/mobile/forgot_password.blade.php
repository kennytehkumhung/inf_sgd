@extends('royalewin/mobile/master')

@if (Session::get('currency') == 'MYR')
    @section('title', 'Best Online Live Casino Malaysia - Password')
    @section('keywords', 'Best Online Live Casino Malaysia - Password')
    @section('description', 'Forgot your password? Please provide the username or email address that you used when you signed up for your RoyaleWin Online Casino Malaysia account.')
@elseif (Session::get('currency') == 'IDR')
    @section('title', 'Bandar Casino Online, Situs Judi Indonesia - Password')
    @section('keywords', 'Bandar Casino Online, Situs Judi Indonesia - Password')
    @section('description', 'Password - RoyaleWin adalah agen bola, bandar casino online dan situs judi online terpercaya untuk Anda penggemar taruhan bola dan pecinta judi online di Indonesia.')
@endif

@section('js')
@parent

<script type="text/javascript">
        function forgotpassword_submit(btnObj){
            var btn = $(btnObj);

            btn.prop("disabled", true);

            $.ajax({
            type: "POST",
            url: "{{route('resetpassword')}}",
            data: {
                _token: "{{ csrf_token() }}",
                username: $('#fg_username').val(),
                email: $('#fg_email').val()
            }
            }).done(function (json) {
                $('.acctTextReg').html('');
                obj = JSON.parse(json);
                var str = '';
                $.each(obj, function (i, item) {
                    if ('{{Lang::get('COMMON.SUCESSFUL')}}' == item) {
                        alert("{{Lang::get('public.ResetPasswordSuccessPleaseCheckEmail')}}");

                        window.location.href = "{{route('homepage')}}";
                    } else {
                        alert(item);
//                        $('.' + i + '_acctTextReg').html(item);
                    }

                    btn.prop("disabled", false);
                });
            });
        }
</script>
@endsection

@section('content')
<div class="container">
    <br/>
    <br/>
    <P>Please Contact Live Chat! <a href="javascript:void(0);" onclick="livechat();" style="color: #f5f015;">Click here.</a></P>
</div>
@endsection