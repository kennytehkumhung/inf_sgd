@extends('royalewin/mobile/master')

@if (Session::get('currency') == 'MYR')
    @section('title', 'The Best Online Live Casino in Malaysia')
    @section('keywords', 'The Best Online Live Casino in Malaysia')
    @section('description', 'Play at RoyaleWin Online Casino Malaysia - awarded Best Online Casino and enjoy 110% Welcome Deposit Bonus. Join RoyaleWin casino online today.')
@elseif (Session::get('currency') == 'IDR')
    @section('title', 'Bandar Casino Online, Situs Judi Indonesia')
    @section('keywords', 'Bandar Casino Online, Situs Judi Indonesia')
    @section('description', 'RoyaleWin adalah agen bola, bandar casino online dan situs judi online terpercaya untuk Anda penggemar taruhan bola dan pecinta judi online di Indonesia.')
@endif

@section('js')
    @parent

    <!-- Script to Activate the Carousel -->
    <script src="{{ asset('/royalewin/mobile/js/jssor.slider-21.1.6.mini.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            var jssor_1_SlideoTransitions = [
                [{b:-1,d:1,o:-1},{b:0,d:1000,o:1}],
                [{b:1900,d:2000,x:-379,e:{x:7}}],
                [{b:1900,d:2000,x:-379,e:{x:7}}],
                [
                    {b:-1,d:1,o:-1,r:288,sX:9,sY:9},
                    {b:1000,d:900,x:-1400,y:-660,o:1,r:-288,sX:-9,sY:-9,e:{r:6}},
                    {b:1900,d:1600,x:-200,o:-1,e:{x:16}}
                ]
            ];

            var jssor_1_options = {
                $AutoPlay: true,
                $SlideDuration: 800,
                $SlideEasing: $Jease$.$OutQuint,
                $CaptionSliderOptions: {
                    $Class: $JssorCaptionSlideo$,
                    $Transitions: jssor_1_SlideoTransitions
                },
                $ArrowNavigatorOptions: {
                    $Class: $JssorArrowNavigator$
                },
                $BulletNavigatorOptions: {
                    $Class: $JssorBulletNavigator$
                }
            };

            var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

            /*responsive code begin*/
            /*you can remove responsive code if you don't want the slider scales while window resizing*/
            function ScaleSlider() {
                var refSize = jssor_1_slider.$Elmt.parentNode.clientWidth;
                if (refSize) {
                    refSize = Math.min(refSize, 1920);
                    jssor_1_slider.$ScaleWidth(refSize);
                }
                else {
                    window.setTimeout(ScaleSlider, 30);
                }
            }
            ScaleSlider();
            $(window).bind("load", ScaleSlider);
            $(window).bind("resize", ScaleSlider);
            $(window).bind("orientationchange", ScaleSlider);
            /*responsive code end*/

            load4dResults();
        });

        function load4dResults() {
            // Load 4d data.
            var resultHolder = $("#results_4d");
            var loading = $("#loading_4d");

            $.ajax({
                url: "{{ route('json4d') }}",
                type: "GET",
                dataType: "JSON",
                data: {
                    _token: "{{ csrf_token() }}"
                },
                success: function (result) {
                    for (var key in result) {
                        if (result.hasOwnProperty(key)) {
                            var resTd = null;

                            if (key == "Toto") {
                                resTd = ":eq(0)";
                            } else if (key == "PMP") {
                                resTd = ":eq(1)";
                            } else if (key == "Magnum") {
                                resTd = ":eq(2)";
                            }

                            if (resTd != null) {
                                $(".dtxtrs1" + resTd).text(result[key]["first"]);
                                $(".dtxtrs2" + resTd).text(result[key]["second"]);
                                $(".dtxtrs3" + resTd).text(result[key]["third"]);
                            }
                        }
                    }
                },
                complete: function (jqXHR, textStatus ) {
                    loading.hide();
                    resultHolder.show();
                }
            });
        }
    </script>
@endsection

@section('content')
    <div id="jssor_1" style="position: relative; margin: 0 auto; top: 0px; left: 0px; width: 1300px; height: 700px; overflow: hidden; visibility: hidden;">
        <!-- Loading Screen -->
        <div data-u="loading" style="position: absolute; top: 0px; left: 0px;">
            <div style="filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block; top: 0px; left: 0px; width: 100%; height: 100%;"></div>
            <div style="position:absolute;display:block;background:url('{{ asset('/royalewin/mobile/img/loading.gif') }}') no-repeat center center;top:0px;left:0px;width:100%;height:100%;"></div>
        </div>
        <div data-u="slides" style="cursor: default; position: relative; top: 0px; left: 0px; width: 1300px; height: 700px; overflow: hidden;">
            @if(isset($banners))
                @foreach( $banners as $key => $banner )
                    <div data-p="225.00" data-po="80% 55%" style="display: none;">
                        <a target="_blank" href="@if($banner->url != 'none'){{$banner->url}}@endif">
                            <img data-u="image" src="{{$banner->domain}}/{{$banner->path}}" />
                        </a>
                    </div>
                @endforeach
            @endif
        </div>
        <!-- Bullet Navigator -->
        <div data-u="navigator" class="jssorb05" style="bottom:26px;right:16px;" data-autocenter="1">
            <!-- bullet navigator item prototype -->
            <div data-u="prototype" style="width:16px;height:16px;"></div>
        </div>
        <!-- Arrow Navigator -->
        <span data-u="arrowleft" class="jssora22l" style="top:-50px !important;left:8px;width:40px;height:58px;" data-autocenter="2"></span>
        <span data-u="arrowright" class="jssora22r" style="top:0px;right:8px;width:40px;height:58px;" data-autocenter="2"></span>
    </div>

    <!-- Page Content -->
    <div class="container">

        <!-- Marketing Icons Section -->

        @if(Session::get('currency') == 'MYR')
            <div class="row">
                <div class="col-lg-12">
                    <br>
                    @if (Auth::user()->check())
                        <a href="{{ route('luckyspin') }}" target="_blank">
                            <img class="img-responsive img-portfolio img-hover" src="{{ asset('/royalewin/mobile/img/royale_wheel.png') }}" alt="">
                        </a>
                    @else
                        <img onclick="alert('{{Lang::get('COMMON.PLEASELOGIN')}}');" class="img-responsive img-portfolio img-hover" src="{{ asset('/royalewin/mobile/img/royale_wheel.png') }}" alt="">
                    @endif
                    <a href="//m.rwbola.com/my/christmas-promotion" target="_blank">
                        <img class="img-responsive img-portfolio img-hover" src="{{ static_asset('/xmas/en/img/mobilepromotion.png')}}" alt="">
                    </a>
                </div>
            </div>
        @elseif (Session::get('currency') == 'IDR')
            <div class="row">
                <div class="col-lg-12">
                    <br>
                    @if (Auth::user()->check())
                        <a href="{{ route('luckyspin') }}" target="_blank">
                            <img class="img-responsive img-portfolio img-hover" src="{{ asset('/royalewin/mobile/img/royale_wheel_idr.png') }}" alt="">
                        </a>
                    @else
                        <img onclick="alert('{{Lang::get('COMMON.PLEASELOGIN')}}');" class="img-responsive img-portfolio img-hover" src="{{ asset('/royalewin/mobile/img/royale_wheel_idr.png') }}" alt="">
                    @endif
                </div>
            </div>
        @endif

        <div class="row">
            <div class="col-lg-12">
                <h5>{{ Lang::get('public.LiveCasino') }}</h5>
            </div>
            @if(in_array('PLT',Session::get('valid_product')))
                <div class="col-md-6 col-xs-6">
                <a href="{{ route('playtechmobile', ['type' => 'live-casino']) }}">
                        <img class="img-responsive img-portfolio img-hover" src="{{ asset('/royalewin/mobile/img/lc-img-1.jpg') }}" alt="">
                    </a>
                </div>
            @endif
            @if(in_array('MXB',Session::get('valid_product')))
                <div class="col-md-6 col-xs-6">
                    <a href="{{ route('mobile') }}">
                        <img class="img-responsive img-portfolio img-hover" src="{{ asset('/royalewin/mobile/img/lc-img-2.jpg') }}" alt="">
                    </a>
                </div>
            @endif
            @if(in_array('AGG',Session::get('valid_product')))
                <div class="col-md-6 col-xs-6">
					@if(Auth::user()->check())
						<a href="{{ route('mobile-selection') }}?html5={{route('agg')}}&apk=//agmbet.com/" >
							<img class="img-responsive img-portfolio img-hover" src="{{ asset('/royalewin/mobile/img/lc-img-3.jpg') }}" alt="">
						</a>
					@else
						<a onClick="alert('{{Lang::get('COMMON.PLEASELOGIN')}}')" >
							<img class="img-responsive img-portfolio img-hover" src="{{ asset('/royalewin/mobile/img/lc-img-3.jpg') }}" alt="">
						</a>
					@endif
                </div>
            @endif
            {{--@if(in_array('OPU',Session::get('valid_product')))--}}
                {{--<div class="col-md-6 col-xs-6">--}}
                    {{--<a href="#" onclick="{{ Auth::user()->check() ? 'window.open("'.route('opu' , [ 'type' => 'casino' , 'category' => 'live']).'","casino_gp","width=1146,height=866");' : 'alert("'.Lang::get('COMMON.PLEASELOGIN').'");' }}">--}}
                    {{--<a href="{{ route('product-requirement') }}">--}}
                        {{--<img class="img-responsive img-portfolio img-hover" src="{{ asset('/royalewin/mobile/img/lc-img-4.jpg') }}" alt="">--}}
                    {{--</a>--}}
                {{--</div>--}}
            {{--@endif--}}
            @if(in_array('HOG',Session::get('valid_product')))
                <div class="col-md-6 col-xs-6">
                    <a href="#" onclick="{{ Auth::user()->check() ? 'window.open("'.route('hog').'","casino_gp","width=1150,height=830");' : 'alert("'.Lang::get('COMMON.PLEASELOGIN').'");' }}">
                        <img class="img-responsive img-portfolio img-hover" src="{{ asset('/royalewin/mobile/img/lc-img-5.jpg') }}" alt="">
                    </a>
                </div>
            @endif
            @if(in_array('VGS',Session::get('valid_product')))
                <div class="col-md-6 col-xs-6">
                    <a href="#" onclick="{{ Auth::user()->check() ? 'window.open("'.route('vgs' , [ 'type' => 'casino' ]).'","casino_eg","width=1042,height=880");' : 'alert("'.Lang::get('COMMON.PLEASELOGIN').'");' }}">
                        <img class="img-responsive img-portfolio img-hover" src="{{ asset('/royalewin/mobile/img/lc-img-6.png') }}" alt="">
                    </a>
                </div>
            @endif
            @if(in_array('ALB',Session::get('valid_product')))
                <div class="col-md-6 col-xs-6">
                    <a href="#" onclick="{{ Auth::user()->check() ? 'window.open("'.route('alb').'","casino_eg","width=1042,height=880");' : 'alert("'.Lang::get('COMMON.PLEASELOGIN').'");' }}">
                        <img class="img-responsive img-portfolio img-hover" src="{{ asset('/royalewin/mobile/img/lc-img-7.png') }}" alt="">
                    </a>
                </div>
            @endif
			 @if(Session::get('currency')=='MYR')
			 @if(in_array('WMC',Session::get('valid_product')))
			  <div class="col-md-6 col-xs-6">
                    <a href="#" onclick="{{ Auth::user()->check() ? 'window.open("'.route('wmcframe').'","casino_eg","width=1042,height=880");' : 'alert("'.Lang::get('COMMON.PLEASELOGIN').'");' }}">
                        <img class="img-responsive img-portfolio img-hover" src="{{ asset('/royalewin/mobile/img/lc-img-8.png') }}" alt="">
                    </a>
                </div>
		       @endif
			  @endif
			</div>

        <!-- /.row -->

        <div class="row">
            <div class="col-lg-12">
                <h5>{{ Lang::get('public.SlotGames') }}</h5>
            </div>
            {{--<div class="col-md-4 col-xs-4">--}}
                {{-- OPUS, not available yet --}}
                {{--<a href="#">--}}
                    {{--<img class="img-responsive img-portfolio img-hover" src="{{ asset('/royalewin/mobile/img/slot-img-1.png') }}" alt="">--}}
                {{--</a>--}}
            {{--</div>--}}
            <div class="col-md-4 col-xs-4">
                <a href="{{ route('mobile') }}">
                    <img class="img-responsive img-portfolio img-hover" src="{{ asset('/royalewin/mobile/img/slot-img-3.png') }}" alt="">
                </a>
            </div>
            <div class="col-md-4 col-xs-4">
                <a href="{{ route('playtechmobile', ['type' => 'all']) }}" target="_blank">
                    <img class="img-responsive img-portfolio img-hover" src="{{ asset('/royalewin/mobile/img/slot-img-4.png') }}" alt="">
                </a>
            </div>
            <div class="col-md-4 col-xs-4">
                <a href="{{ route('slot', ['type' => 'mxb']) }}" target="_blank">
                    <img class="img-responsive img-portfolio img-hover" src="{{ asset('/royalewin/mobile/img/slot-img-5.png') }}" alt="">
                </a>
            </div>
			
            @if(in_array('JOK',Session::get('valid_product')))
                <div class="col-md-4 col-xs-4">
                    {{--<a href="javascript:void(0);" onclick="@if (Auth::user()->check())window.open('{{route('jok')}}','jok_slot','width=1000,height=750')@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif">--}}
                    <a href="{{ route('mobile') }}" target="_blank">
                        <img class="img-responsive img-portfolio img-hover" src="{{ asset('/royalewin/mobile/img/slot-img-6.png') }}" alt="">
                    </a>
                </div>
            @endif
            <div class="col-md-4 col-xs-4">
                <a href="{{ route('spg', [ 'type' => 'slot' , 'category' => 'slot' ] ) }}" target="_blank">
                    <img class="img-responsive img-portfolio img-hover" src="{{ asset('/royalewin/mobile/img/slot-img-8.png') }}" alt="">
                </a>
            </div>
            {{--@if(in_array('AGG',Session::get('valid_product')) && Session::get('currency') == 'MYR')--}}
                {{--<div class="col-md-4 col-xs-4">--}}
                    {{--<a href="javascript:void(0);" onclick="@if (Auth::user()->check())window.open('{{route('agg', array('gametype' => '6'))}}','casino_ag','width=1000,height=750')@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif">--}}
                        {{--<img class="img-responsive img-portfolio img-hover" src="{{ asset('/royalewin/mobile/img/slot-img-9.png') }}" alt="">--}}
                    {{--</a>--}}
                {{--</div>--}}
            {{--@endif--}}
            @if(in_array('SCR',Session::get('valid_product')) && Session::get('currency') == 'MYR')
                <div class="col-md-4 col-xs-4">
                    <a href="{{ route('mobile') }}">
                        <img class="img-responsive img-portfolio img-hover" src="{{ asset('/royalewin/mobile/img/slot-img-10.png') }}" alt="">
                    </a>
                </div>
            @endif
        </div>

        <div class="row">
            <div class="col-lg-12">
                <h5>{{ Lang::get('public.SportsBook') }}</h5>
            </div>
            <div class="col-lg-12 sp">
                <div class="row">
                    <div class="col-md-6 col-xs-6">
                        <a {!! Auth::user()->check() ? 'href="'.route('ibc').'" target="_blank"' : 'href="javascript:void(0);" onclick="alert(\''.Lang::get('COMMON.PLEASELOGIN').'\');"' !!}>
                            <img class="img-responsive img-portfolio img-hover tp01" src="{{ asset('/royalewin/mobile/img/i-sport.png') }}" alt="">
                        </a>
                    </div>
                    <div class="col-md-6 col-xs-6">
                        <a href="{{ route('tbs') }}" target="_blank">
                            <img class="img-responsive img-portfolio img-hover tp01" src="{{ asset('/royalewin/mobile/img/w-sport.png') }}" alt="">
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="ht1"></div>

        <!-- Portfolio Section -->
        @if (Session::get('currency') == 'MYR')
            <div class="row" onclick="window.location.href='{{route('4d')}}';">
                <div class="col-md-12 col-xs-12 bdr1">
                    <div class="col-md-2 pull-left mt1">
                        4D {{ Lang::get('COMMON.RESULT') }}
                    </div>
                    <div class="col-md-1 pull-right mr1">
                        <img src="{{ asset('/royalewin/mobile/img/betnow.png') }}" style="width: auto; height: 30px;">
                    </div>
                </div>

                <div class="col-md-12 col-xs-12">
                    <div class="row mt2">
                        <div id="loading_4d" class="col-md-12 col-xs-12" style="text-align: center;">
                            <span>{{ Lang::get('public.Loading') }}...</span>
                        </div>

                        <div id="results_4d" class="col-md-12 col-xs-12" style="display: none;">
                            <div class="col-md-12">
                                <div class="col-md-4 col-xs-4 nopad">
                                    <span class="dTT">{{ Lang::get('public.FirstPrize2') }}</span>
                                    <div class="dtxt star dtxtrs1">-</div>
                                </div>
                                <div class="col-md-4 col-xs-4 nopad bgGrey">
                                    <span class="dTT">{{ Lang::get('public.FirstPrize2') }}</span>
                                    <div class="dtxt star dtxtrs1">-</div>
                                </div>
                                <div class="col-md-4 col-xs-4 nopad">
                                    <span class="dTT">{{ Lang::get('public.FirstPrize2') }}</span>
                                    <div class="dtxt star dtxtrs1">-</div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="col-md-4 col-xs-4 nopad">
                                    <span class="dTT">{{ Lang::get('public.SecondPrize2') }}</span>
                                    <div class="dtxt1 dtxtrs2">-</div>
                                </div>
                                <div class="col-md-4 col-xs-4 nopad bgGrey">
                                    <span class="dTT">{{ Lang::get('public.SecondPrize2') }}</span>
                                    <div class="dtxt1 dtxtrs2">-</div>
                                </div>
                                <div class="col-md-4 col-xs-4 nopad">
                                    <span class="dTT">{{ Lang::get('public.SecondPrize2') }}</span>
                                    <div class="dtxt1 dtxtrs2">-</div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="col-md-4 col-xs-4 nopad">
                                    <span class="dTT">{{ Lang::get('public.ThirdPrize2') }}</span>
                                    <div class="dtxt1 dtxtrs3">-</div>
                                </div>
                                <div class="col-md-4 col-xs-4 nopad bgGrey">
                                    <span class="dTT">{{ Lang::get('public.ThirdPrize2') }}</span>
                                    <div class="dtxt1 dtxtrs3">-</div>
                                </div>
                                <div class="col-md-4 col-xs-4 nopad">
                                    <span class="dTT">{{ Lang::get('public.ThirdPrize2') }}</span>
                                    <div class="dtxt1 dtxtrs3">-</div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="col-md-4 col-xs-4 nopad">
                                    <div class="dtxt1"><img src="{{ asset('/royalewin/mobile/img/index_toto.png') }}" width="104"></div>
                                </div>
                                <div class="col-md-4 col-xs-4 nopad">
                                    <div class="dtxt1"><img src="{{ asset('/royalewin/mobile/img/index_damachai.png') }}" width="104"></div>
                                </div>
                                <div class="col-md-4 col-xs-4 nopad">
                                    <div class="dtxt1"><img src="{{ asset('/royalewin/mobile/img/index_magnum.png') }}" width="104"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif

        <div class="ht1"></div>

        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <img class="img-responsive img-portfolio img-hover tp01" src="{{ asset('/royalewin/mobile/img/index-mobile.png') }}" onclick="window.location.href='{{ route('mobile') }}';">
            </div>
        </div>

        <div class="ht1"></div>

        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="col-md-12 col-xs-12 bdr1">
                    <div class="col-md-2 pull-left mt1">
                        {{ Lang::get('public.MobileApp') }}
                    </div>
                    <div class="col-md-1 pull-right mr1">
                        <img src="{{ asset('/royalewin/mobile/img/speaker.png') }}">
                    </div>
                </div>
                <div class="col-md-12 col-xs-12">
                    @if (Session::get('currency') == 'MYR')
                        <div class="bgsp" style="background-image: none;">
                            <a href="#">
                                @if (Lang::getLocale() == 'cn')
                                    <img class="img-responsive img-portfolio img-hover" src="{{ asset('/royalewin/mobile/img/mid-mob-bg-myr-cn.png') }}">
                                @else
                                    <img class="img-responsive img-portfolio img-hover" src="{{ asset('/royalewin/mobile/img/mid-mob-bg-myr.png') }}">
                                @endif
                            </a>
                        </div>
                        <div class="bgsp" style="background-image: none;">
                            <a href="https://api.whatsapp.com/send?phone=60102954176" target="_blank">
                                <img class="img-responsive img-portfolio img-hover" src="{{ asset('/royalewin/mobile/img/whapp.png') }}">
                            </a>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
