@extends('royalewin/mobile/master')

@if (Session::get('currency') == 'MYR')
    @section('title', 'Best Online Live Casino Malaysia - How To Join')
    @section('keywords', 'Best Online Live Casino Malaysia - How To Join')
    @section('description', 'Press the \'join now\' button on the site which will bring you to RoyaleWin Online Casino registration form. Simply enter your details and then click \'submit\'.')
@elseif (Session::get('currency') == 'IDR')
    @section('title', 'Bandar Casino Online, Situs Judi Indonesia - Bantuan')
    @section('keywords', 'Bandar Casino Online, Situs Judi Indonesia - Bantuan')
    @section('description', 'Bantuan -RoyaleWin adalah agen bola, bandar casino online dan situs judi online terpercaya untuk Anda penggemar taruhan bola dan pecinta judi online di Indonesia.')
@endif

@section('css')
    @parent

    <style type="text/css">
        .content span {
            color: #ffcc01;
            display: block;
            font-size: 12px;
            font-weight: bold;
            height: auto;
            margin: 15px auto 0;
            padding: 0;
            position: relative;
            text-align: left;
            text-transform: uppercase;
        }

        .content p {
            text-align: justify;
        }
    </style>
@endsection

@section('content')
    <!-- Page Content -->
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-xs-12 contOthers">
                <h4>{{ strtoupper(Lang::get('public.HowToJoin')) }}</h4>
            </div>
            <div class="col-md-12 col-xs-12 contOthers content">
                {!! htmlspecialchars_decode($content) !!}
            </div>
        </div>
    </div>
@endsection
