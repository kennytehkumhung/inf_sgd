@extends('royalewin/mobile/master')

@if (Session::get('currency') == 'MYR')
    @section('title', 'FREE Best Online Live Casino Malaysia Registration')
    @section('keywords', 'FREE Best Online Live Casino Malaysia Registration')
    @section('description', 'RoyaleWin Online Live Casino Malaysia gives you Free Spins when you sign up NOW. Play the best online casino games – slots, roulette, video poker and more.')
@elseif (Session::get('currency') == 'IDR')
    @section('title', 'Bandar Casino Online, Situs Judi Indonesia Indonesia - Daftar')
    @section('keywords', 'Bandar Casino Online, Situs Judi Indonesia Indonesia - Daftar')
    @section('description', 'RoyaleWin adalah bandar judi bola online & agen bola terpercaya di Indonesia. Daftar Sekarang!')
@endif

@section('js')
    @parent

    <script type="text/javascript">
        $(function () {
            @if ($showSMSField != 1)
                $("#vcode_field").css("visibility", "hidden");
            @elseif (Session::get('currency') != 'MYR')
                $("#vcode_field").css("visibility", "hidden");
            @endif
        });

        function enterpressalert(e, textarea)
        {
            var code = (e.keyCode ? e.keyCode : e.which);
            if(code == 13) { //Enter keycode
                register_submit();
            }
        }

        function register_submit() {
            var dob = '0000-00-00';
            if (typeof $('#dob_year').val() != undefined) {
                dob = $('#dob_year').val() + '-' + $('#dob_month').val() + '-' + $('#dob_day').val();
            }
            $.ajax({
                type: "POST",
                url: "{{route('register_process')}}",
                data: {
                    _token:     "{{ csrf_token() }}",
                    username:	$('#r_username').val(),
                    password:	$('#r_password').val(),
                    repeatpassword: $('#r_repeatpassword').val(),
                    code:		$('#r_code').val(),
                    email:		$('#r_email').val(),
                    mobile:		$('#r_mobile').val(),
                    fullname:	$('#r_fullname').val(),
                    referralid:	$('#r_referralid').val(),
                    mobile_vcode: $("#r_mobile_vcode").val(),
                    dob:		dob
                },
            }).done(function(json) {
                $('.acctTextReg').html('');
                obj = JSON.parse(json);
                var str = '';
                $.each(obj, function(i, item) {
                    if ('{{Lang::get('COMMON.SUCESSFUL')}}' == item) {
                        alert('{{Lang::get('public.NewMember')}}');
                        window.location.href = "{{route('homepage')}}";
                    } else {
                        $('.' + i + '_acctTextReg').html('<font style="color:red">' + item + '</font>');
                    }
                });
            });
        }

        var countDownSecond = 0;
        var intervalObj = null;
        var btnObj = $("#btn_sms");

        function sendVerifySms() {
            if (intervalObj != null) {
                alert("{{ Lang::get('public.PleaseWaitForXSecondsToResendSMS') }}".replace(":p1", countDownSecond));
                return false;
            }

            var mobileObj = $("#r_mobile");
            var mobileNum = mobileObj.val();

            if (mobileNum.length < 1) {
                alert("{{ Lang::get('public.PleaseEnterContactNumberToContinue') }}");
                mobileObj.focus();

                return false;
            }

            if (confirm("{{ Lang::get('public.SendVerificationCodeViaSMSToThisNumber') }} " + mobileNum)) {
                countDownSecond = 60;
                smsResendCountDown();

                $.ajax({
                    type: "POST",
                    dataType: "json",
                    url: "{{url('smsregcode')}}",
                    data: {
                        _token:     "{{ csrf_token() }}",
                        tel_mobile:	mobileNum
                    },
                    success: function (result) {
                        if (result.code != 0) {
                            alert(result.msg);
                        }

                        if (result.code == 2) {
                            countDownSecond = 10;
                        }
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
//                    console.log('ok failed');
                    }
                });
            }
        }

        function smsResendCountDown() {
            if (intervalObj == null) {
                intervalObj = setInterval(smsResendCountDown, 1000)
            }

            if (countDownSecond <= 1) {
                btnObj.text("Send SMS");
                clearInterval(intervalObj);
                intervalObj = null;
                return;
            }

            btnObj.text("Resend (" + --countDownSecond + ")");
        }
    </script>
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-xs-12 text-center contOthers">
                <h4>{{ strtoupper(Lang::get('public.Registration')) }}</h4>
            </div>
        </div>

        <div class="ht1"></div>

        <div class="row reg">
            <div class="col-md-3 col-xs-6">
                <label>{{ Lang::get('public.FullName') }}* : </label>
            </div>

            <div class="col-md-3 col-xs-6">
                <input type="text" id="r_fullname" name="r_fullname">
                <span class="fullname_acctTextReg acctTextReg"></span>
            </div>

            <div class="col-md-3 col-xs-6"></div>

            <div class="col-md-12 col-xs-12">
                <div class="regInfo">{{ Lang::get('public.NameCaution') }}</div>
            </div>
        </div>

        <div class="row reg">
            <div class="col-md-3 col-xs-6">
                <label>{{ Lang::get('public.Username2') }}* : </label>
            </div>

            <div class="col-md-3 col-xs-6">
                <input type="text" id="r_username" name="r_username" class="sp1">
                <span class="username_acctTextReg acctTextReg"></span>
            </div>
        </div>

        <div class="row reg">
            <div class="col-md-3 col-xs-6">
                <label>{{ Lang::get('public.Password') }}* : </label>
            </div>

            <div class="col-md-3 col-xs-6">
                <input type="password" id="r_password" name="r_password" class="sp1">
                <span class="password_acctTextReg acctTextReg"></span>
            </div>
        </div>

        <div class="row reg">
            <div class="col-md-3 col-xs-6">
                <label>{{ Lang::get('public.RepeatPassword') }}* : </label>
            </div>

            <div class="col-md-3 col-xs-6">
                <input type="password" id="r_repeatpassword" name="r_repeatpassword">
            </div>
        </div>

        <div class="row reg">
            <div class="col-md-3 col-xs-6">
                <label>{{ Lang::get('public.EmailAddress') }}* : </label>
            </div>

            <div class="col-md-3 col-xs-6">
                <input type="text" id="r_email" name="r_email">
                <span class="email_acctTextReg acctTextReg"></span>
            </div>
        </div>

        <div class="row reg">
            <div class="col-md-3 col-xs-6">
                <label>{{ Lang::get('public.ReferralID') }} ({{ Lang::get('public.Optional') }}) : </label>
            </div>

            <div class="col-md-3 col-xs-6">
                <input type="text" id="r_referralid" name="r_referralid" value="{{ Request::input('referralid') }}">
                <span class="referralid_acctTextReg acctTextReg"></span>
            </div>
        </div>

        <div class="row reg">
            <div class="col-md-3 col-xs-6">
                <label>{{ Lang::get('public.ContactNumber') }}* : </label>
            </div>

            <div class="col-md-3 col-xs-6">
                <input type="text" id="r_mobile" name="r_mobile">
                <span class="mobile_acctTextReg acctTextReg"></span>
            </div>
        </div>

        <div class="row reg" id="vcode_field">
            <div class="col-md-3 col-xs-6">
                <label>{{ Lang::get('public.VerificationCode') }}* : </label>
            </div>

            <div class="col-md-3 col-xs-6">
                <input type="text" id="r_mobile_vcode" name="r_mobile_vcode">
                <div class="logBtn">
                    <a href="javascript:void(0);" id="btn_sms" style="width: 224px; margin-top: 7px;" onclick="sendVerifySms();">{{ Lang::get('public.ClickHereToGetVerificationCode') }}</a>
                </div>
                <span class="mobile_vcode_acctTextReg acctTextReg"></span>
            </div>
        </div>

        <div class="row reg">
            <div class="col-md-3 col-xs-6">
                <label>{{ Lang::get('public.Code') }}* : </label>
            </div>

            <div class="col-md-3 col-xs-6">
                <input type="text" id="r_code" onKeyPress="enterpressalert(event, this)" class="coded">
                <span class="code_acctTextReg acctTextReg"></span>
            </div>
        </div>

        <div class="row reg">
            <div class="col-md-3 col-xs-6"></div>

            <div class="col-md-2 col-xs-6">
                <img src="{{ route('captcha', ['type' => 'register_captcha']) }}" class="coded blacked" style="height: 27px; width: 60px;" />
            </div>
        </div>

        <div class="row reg">
            <div class="col-md-12 col-xs-12">
                <p>* {{ Lang::get('public.RequiredField') }}</p>
                <p>{{ Lang::get('public.DifficultyContact') }}</p>
            </div>
        </div>

        <div class="row reg">
            <div class="col-md-2 col-xs-3">
                <a class="btn btn-primary" href="javascript:void(0);" onclick="register_submit();">{{ Lang::get('public.Submit') }}</a>
            </div>
        </div>
        <!-- /.row -->
    </div>
@endsection
