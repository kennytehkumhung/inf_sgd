@extends('royalewin/mobile/master')

@if (Session::get('currency') == 'MYR')
    @section('title', 'Best Mobile Casino Malaysia - iPhone & Android Casino')
    @section('keywords', 'Best Mobile Casino Malaysia - iPhone & Android Casino')
    @section('description', 'Play at RoyaleWin Mobile Online Casino Malaysia - awarded Best Online Casino and enjoy 110% Welcome Deposit Bonus. Join RoyaleWin mobile casino online today.')
@elseif (Session::get('currency') == 'IDR')
    @section('title', 'Bandar Casino Online, Situs Judi Indonesia Indonesia - Mobile')
    @section('keywords', 'Bandar Casino Online, Situs Judi Indonesia Indonesia - Mobile')
    @section('description', 'RoyaleWin adalah bandar judi online Indonesia. Judi online di Aplikasi mobile seperti android dan apple untuk memudahkan anda.')
@endif

@section('js')
    @parent

    <style type="text/css">
        .dl-holder {
            min-height: 112px;
        }

        .dl-holder img.dl-title {
            max-width: 80px;
        }

        .dl-holder img.dl-qr {
            max-height: 80px;
            max-width: 80px;
        }

        .modal-footer a {
            font-weight: bold;
            color: #ffae00;
        }
    </style>
@endsection

@section('js')
    @parent

    <script type="text/javascript">
        //
    </script>
@endsection

@section('content')
    <!-- Page Content -->
    <div class="container">

        <div class="row">
            <div class="col-md-12 col-xs-12">
                <img class="img-responsive" src="{{ asset('/royalewin/mobile/img/mob-banner-big.jpg') }}">
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12 mob1 dl-holder">
                <div class="col-md-6 col-xs-4"></div>

                <div class="col-md-3 col-xs-4 ">
                    <img class="dl-title" src="{{ asset('/royalewin/img/andr-lc-dl-tit.png') }}">

                    <a href="#" data-toggle="modal" data-target="#md_jok">
                       	<div class="QRcodeIMG" style="max-height: 80px;max-width: 80px;background: white;     padding-top: 10px;     padding-left: 10px;     padding-bottom: 10px;">
						     
							<input id="getLink1" type="text" value="{{$android['jok']}}"	hidden>
							
							<div id="printQR1"></div>
							</div>       
                    </a>
                </div>

                <div class="col-md-3 col-xs-4">
                    <img class="dl-title" src="{{ asset('/royalewin/img/ios-dl-tit.png') }}">

                    <a href="#" data-toggle="modal" data-target="#md_jok">
							<div class="QRcodeIMG" style="max-height: 80px;max-width: 80px;background: white;     padding-top: 10px;     padding-left: 10px;     padding-bottom: 10px;">
						     
							<input id="getLink2" type="text" value="{{$ios['jok']}}"	hidden>
							
							<div id="printQR2"></div>
							</div>                          </a>
                </div>

                <div class="clearfix"></div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12 mob2 dl-holder">
                <div class="col-md-6 col-xs-4"></div>

                <div class="col-md-3 col-xs-4">
                    <img class="dl-title" src="{{ asset('/royalewin/img/andr-slot-dl-tit.png') }}">

                    <a href="#" data-toggle="modal" data-target="#md_mxb_sl_and">
							<div class="QRcodeIMG" style="max-height: 80px;max-width: 80px;background: white;     padding-top: 10px;     padding-left: 10px;     padding-bottom: 10px; margin-left: 10px;">
						    
							<input id="getLink3" type="text" value="{{$android['mxb']}}"	hidden>
							
							<div id="printQR3"></div>
							</div>          
                    </a>
                </div>

                <div class="col-md-3 col-xs-4">
                    <img class="dl-title" src="{{ asset('/royalewin/img/andr-lc-dl-tit.png') }}">

                    <a href="#" data-toggle="modal" data-target="#md_mxb_lc_and">
                     
							<div class="QRcodeIMG" style="max-height: 80px;max-width: 80px;background: white;     padding-top: 10px;     padding-left: 10px;     padding-bottom: 10px;">
						     
							<input id="getLink4" type="text" value="{{$android['mxb']}}"	hidden>
							
							<div id="printQR4"></div>
							</div>                       
                    </a>
                </div>

                <div class="clearfix"></div>
            </div>
        </div>
		
        <div class="row">
            <div class="col-lg-12 mob3 dl-holder">
                <div class="col-md-6 col-xs-4"></div>

                <div class="col-md-3 col-xs-4">
                    <img class="dl-title" src="{{ asset('/royalewin/img/andr-slot-dl-tit.png') }}">

                    <a href="#" data-toggle="modal" data-target="#md_plt_sl_and">
							<div class="QRcodeIMG" style="max-height: 80px;max-width: 80px;background: white;     padding-top: 10px;     padding-left: 10px;     padding-bottom: 10px;">
						     
							<input id="getLink5" type="text" value="{{$android['pltslot']}}"	hidden>
							
							<div id="printQR5"></div>
							</div>                          
							</a>
                </div>

                <div class="col-md-3 col-xs-4">
                    <img class="dl-title" src="{{ asset('/royalewin/img/andr-lc-dl-tit.png') }}">

                    <a href="#" data-toggle="modal" data-target="#md_plt_lc_and">
                        	<div class="QRcodeIMG" style="max-height: 80px;max-width: 80px;background: white;     padding-top: 10px;     padding-left: 10px;     padding-bottom: 10px;">
						     
							<input id="getLink6" type="text" value="{{$android['pltlive']}}"	hidden>
							
							<div id="printQR6"></div>
							</div>     
                    </a>
                </div>

                <div class="clearfix"></div>
            </div>
        </div>
		
        <div class="row">
            <div class="col-lg-12 mob4 dl-holder">
                <div class="col-md-6 col-xs-4"></div>

                <div class="col-md-3 col-xs-4">
                    <img class="dl-title" src="{{ asset('/royalewin/img/andr-dl-tit.png') }}">

                    <a href="#" data-toggle="modal" data-target="#md_sky_and">
                        	<div class="QRcodeIMG" style="max-height: 80px;max-width: 80px;background: white;     padding-top: 10px;     padding-left: 10px;     padding-bottom: 10px;">
						     
							<input id="getLink7" type="text" value="{{$android['sky']}}"	hidden>
							
							<div id="printQR7"></div>
							</div> 
                    </a>
                </div>

                <div class="col-md-3 col-xs-4">
                    <img class="dl-title" src="{{ asset('/royalewin/img/ios-dl-tit.png') }}">

                    <a href="#" data-toggle="modal" data-target="#md_sky_ios">
                             <div class="QRcodeIMG" style="max-height: 80px;max-width: 80px;background: white;     padding-top: 10px;     padding-left: 10px;     padding-bottom: 10px;">
						     
							<input id="getLink8" type="text" value="{{$ios['sky']}}"	hidden>
							
							<div id="printQR8"></div>
							</div> 
                    </a>
                </div>

                <div class="clearfix"></div>
            </div>
        </div>

        <div class="row">
         <div class="col-lg-12 mob5 dl-holder">
                <div class="col-md-6 col-xs-4"></div>

                <div class="col-md-3 col-xs-4">
                    <img class="dl-title" src="{{ asset('/royalewin/img/dt-dl-tit.png') }}">

                    <a href="#" data-toggle="modal" data-target="#md_pltb_pc">
                       <div class="QRcodeIMG" style="max-height: 80px;max-width: 80px;background: white;     padding-top: 10px;     padding-left: 10px;     padding-bottom: 10px;">
						     
							<input id="getLink15" type="text" value="{{$pc['pltpc']}}"	hidden>
							
							<div id="printQR15"></div>
							</div> 
                    </a>
                </div>

                <div class="col-md-3 col-xs-4">
                </div>

                <div class="clearfix"></div>
            </div>
        </div>

        <div class="row">            
<div class="col-lg-12 mob6 dl-holder">
                <div class="col-md-6 col-xs-4"></div>

                <div class="col-md-3 col-xs-4">
                    <img class="dl-title" src="{{ asset('/royalewin/img/andr-dl-tit.png') }}">

                    <a href="#" data-toggle="modal" data-target="#md_agg_and">
						<div class="QRcodeIMG" style="max-height: 80px;max-width: 80px;background: white;     padding-top: 10px;     padding-left: 10px;     padding-bottom: 10px;">
						     
							<input id="getLink9" type="text" value="{{$android['agg']}}"	hidden>
							
							<div id="printQR9"></div>
							</div>                     </a>
                </div>

                <div class="col-md-3 col-xs-4">
                    <img class="dl-title" src="{{ asset('/royalewin/img/ios-dl-tit.png') }}">

                    <a href="#" data-toggle="modal" data-target="#md_agg_ios">
				<div class="QRcodeIMG" style="max-height: 80px;max-width: 80px;background: white;     padding-top: 10px;     padding-left: 10px;     padding-bottom: 10px;">
						     
							<input id="getLink10" type="text" value="{{$ios['agg']}}"	hidden>
							
							<div id="printQR10"></div>
							</div>                       
							</a>
                </div>

                <div class="clearfix"></div>
            </div>
        </div>

        @if( Session::get('currency') == 'MYR' )
            <div class="row">
                <div class="col-lg-12 mob9 dl-holder">
                    <div class="col-md-6 col-xs-4"></div>

                    <div class="col-md-3 col-xs-4">
                        <img class="dl-title" src="{{ asset('/royalewin/img/andr-dl-tit.png') }}">

                        <a href="#" data-toggle="modal" data-target="#md_scr_and">
							<div class="QRcodeIMG" style="max-height: 80px;max-width: 80px;background: white;     padding-top: 10px;     padding-left: 10px;     padding-bottom: 10px;">
						     
							<input id="getLink11" type="text" value="{{$android['scr']}}"	hidden>
							
							<div id="printQR11"></div>
							</div>                         
							</a>
                    </div>

                    <div class="col-md-3 col-xs-4">
                        <img class="dl-title" src="{{ asset('/royalewin/img/ios-dl-tit.png') }}">

                        <a href="#" data-toggle="modal" data-target="#md_scr_ios">
							<div class="QRcodeIMG" style="max-height: 80px;max-width: 80px;background: white;     padding-top: 10px;     padding-left: 10px;     padding-bottom: 10px;">
						     
							<input id="getLink12" type="text" value="{{$ios['scr']}}"	hidden>
							
							<div id="printQR12"></div>
							</div>    
							</a>
                    </div>

                    <div class="clearfix"></div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12 mob10 dl-holder">
                    <div class="col-md-6 col-xs-4"></div>

                    <div class="col-md-3 col-xs-4 ">
                        <img class="dl-title" src="{{ asset('/royalewin/img/andr-dl-tit.png') }}">

                        <a href="#" data-toggle="modal" data-target="#md_alb_and">
							<div class="QRcodeIMG" style="max-height: 80px;max-width: 80px;background: white;     padding-top: 10px;     padding-left: 10px;     padding-bottom: 10px;">
						     
							<input id="getLink13" type="text" value="{{$android['alb']}}"	hidden>
							
							<div id="printQR13"></div>
							</div>                           
							</a>
                    </div>

                    <div class="col-md-3 col-xs-4">
                        <img class="dl-title" src="{{ asset('/royalewin/img/ios-dl-tit.png') }}">

                        <a href="#" data-toggle="modal" data-target="#md_alb_ios">
							<div class="QRcodeIMG" style="max-height: 80px;max-width: 80px;background: white;     padding-top: 10px;     padding-left: 10px;     padding-bottom: 10px;">
						     
							<input id="getLink14" type="text" value="{{$ios['alb']}}"	hidden>
							
							<div id="printQR14"></div>
							</div>                           
							</a>
                    </div>

                    <div class="clearfix"></div>
                </div>
            </div>
        @endif
        <!-- /.row -->
    </div>
@endsection

@section('modal')
    <div class="modal fade" id="md_jok" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <h5>Play Any Time Any Where</h5>
                            <p>Android and IOS Version</p>
                            <ul>
                                <li>Step 1 : Click on the Download button.</li>
                                <li>Step 2 : Run apps, and install the file after download.</li>
                                @if(Session::get('currency') == 'IDR')
                                    <li>Step 3 : Login with your ROYALEWIN username by adding a prefix  "{{ Config::get(Session::get('currency').'.jok.appid') }}.RWI_"   infront of your username.</li>
                                @else
                                    <li>Step 3 : Login with your ROYALEWIN username by adding a prefix  "{{ Config::get(Session::get('currency').'.jok.appid') }}.RWM_"   infront of your username.</li>
                                @endif
                            </ul>
                            <p><strong>Attention! Username field must be ALL with Capital Letter</strong></p>
                            <p>
                                @if(Session::has('username'))
                                    @if(Session::get('currency') == 'IDR')
                                        {{ Config::get(Session::get('currency').'.jok.appid') }}.RWI_{{ strtoupper(Session::get('username')) }}
                                    @else
                                        {{ Config::get(Session::get('currency').'.jok.appid') }}.RWM_{{ strtoupper(Session::get('username')) }}
                                    @endif
                                @else
                                    Please login to check your username.
                                @endif
                            </p>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="{{$android['jok']}}}" target="_blank">DOWNLOAD NOW</a>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="md_mxb_sl_and" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <h5>Play Any Time Any Where</h5>
                            <p>Android Version</p>
                            <ul>
                                <li>Step 1 : Click on the Download button.</li>
                                <li>Step 2 : Run apps, and install the file after download.</li>
                                <li>Step 3 : Login with your ROYALEWIN username.</li>
                            </ul>
                            <p><strong>Attention! Username field must be ALL with Capital Letter</strong></p>
                            <p>
                                @if(Session::has('username'))
                                    {{ strtoupper(Session::get('username')) }}
                                @else
                                    Please login to check your username.
                                @endif
                            </p>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                        <a href="{{$android['mxb']}}" target="_blank">DOWNLOAD NOW</a>
                    
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="md_mxb_lc_and" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <h5>Play Any Time Any Where</h5>
                            <p>Android Version</p>
                            <ul>
                                <li>Step 1 : Click on the Download button.</li>
                                <li>Step 2 : Run apps, and install the file after download.</li>
                                <li>Step 3 : Login with your ROYALEWIN username.</li>
                            </ul>
                            <p><strong>Attention! Username field must be ALL with Capital Letter</strong></p>
                            <p>
                                @if(Session::has('username'))
                                    {{ strtoupper(Session::get('username')) }}
                                @else
                                    Please login to check your username.
                                @endif
                            </p>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
              
                        <a href="{{$android['mxb']}}" target="_blank">DOWNLOAD NOW</a>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="md_plt_sl_and" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <h5>Play Any Time Any Where</h5>
                            <p>Android Version</p>
                            <ul>
                                <li>Step 1 : Click on the Download button.</li>
                                <li>Step 2 : Run apps, and install the file after download.</li>
                                @if(Session::get('currency') == 'IDR')
                                    <li>Step 3 : Login with your ROYALEWIN username by adding a prefix  "RYW_"   infront of your username.</li>
                                @else
                                    <li>Step 3 : Login with your ROYALEWIN username by adding a prefix  "RYL_"   infront of your username.</li>
                                @endif
                            </ul>
                            <p><strong>Attention! Username field must be ALL with Capital Letter</strong></p>
                            <p>
                                @if(Session::has('username'))
                                    @if(Session::get('currency') == 'IDR')
                                        RYW_{{strtoupper(Session::get('username'))}}
                                    @else
                                        RYL_{{strtoupper(Session::get('username'))}}
                                    @endif
                                @else
                                    Please login to check your username.
                                @endif
                            </p>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
					@if(Session::get('currency') == 'IDR')
						<a href="//m.gm175888.com/download.html" target="_blank">DOWNLOAD NOW</a>
					@else
						<a href="{{$android['pltslot']}}" target="_blank">DOWNLOAD NOW</a>
					@endif
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="md_plt_lc_and" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <h5>Play Any Time Any Where</h5>
                            <p>Android Version</p>
                            <ul>
                                <li>Step 1 : Click on the Download button.</li>
                                <li>Step 2 : Run apps, and install the file after download.</li>
                                @if(Session::get('currency') == 'IDR')
                                    <li>Step 3 : Login with your ROYALEWIN username by adding a prefix  "RYW_"   infront of your username.</li>
                                @else
                                    <li>Step 3 : Login with your ROYALEWIN username by adding a prefix  "RYL_"   infront of your username.</li>
                                @endif
                            </ul>
                            <p><strong>Attention! Username field must be ALL with Capital Letter</strong></p>
                            <p>
                                @if(Session::has('username'))
                                    @if(Session::get('currency') == 'IDR')
                                        RYW_{{strtoupper(Session::get('username'))}}
                                    @else
                                        RYL_{{strtoupper(Session::get('username'))}}
                                    @endif
                                @else
                                    Please login to check your username.
                                @endif
                            </p>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
					@if(Session::get('currency') == 'IDR')
						<a href="//m.gm175888.com/live/download.html" target="_blank">DOWNLOAD NOW</a>
					@else
						<a href="{{$android['pltlive']}}" target="_blank">DOWNLOAD NOW</a>
					@endif
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="md_sky_and" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <h5>Play Any Time Any Where</h5>
                            <p>Android Version</p>
                            <ul>
                                <li>Step 1 : Click on the Download button.</li>
                                <li>Step 2 : Run apps, and install the file after download.</li>
                                @if(Auth::user()->check())
                                    <li>Step 3 : Login with : {{Config::get(Session::get('currency').'.sky.agid').((new \App\libraries\providers\SKY())->formatUserId(Session::get('userid')))}} </li>
                                @else
                                    <li>Please login to check your username.</li>
                                @endif
                            </ul>
                            <p><strong>Attention! Username field must be ALL with Capital Letter</strong></p>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="{{$android['sky']}}" target="_blank">DOWNLOAD NOW</a>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="md_sky_ios" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <h5>Play Any Time Any Where</h5>
                            <p>IOS Version</p>
                            <ul>
                                <li>Step 1 : Click on the Download button.</li>
                                <li>Step 2 : Run apps, and install the file after download.</li>
                                @if(Auth::user()->check())
                                    <li>Step 3 : Login with : {{Config::get(Session::get('currency').'.sky.agid').((new \App\libraries\providers\SKY())->formatUserId(Session::get('userid')))}} </li>
                                @else
                                    <li>Please login to check your username.</li>
                                @endif
                            </ul>
                            <p><strong>Attention! Username field must be ALL with Capital Letter</strong></p>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="{{$ios['sky']}}" target="_blank">DOWNLOAD NOW</a>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="md_pltb_pc" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <h5>Play Any Time Any Where</h5>
                            <p>Android and IOS Version</p>
                            <ul>
                                <li>Step 1 : Click on the Download button.</li>
                                <li>Step 2 : Run apps, and install the file after download.</li>
                                @if(Session::get('currency') == 'IDR')
                                    <li>Step 3 : Login with your ROYALEWIN username by adding a prefix  "RWI_"  infront of your username.</li>
                                @else
                                    <li>Step 3 : Login with your ROYALEWIN username by adding a prefix  "RWM_"  infront of your username.</li>
                                @endif
                            </ul>
                            <p><strong>Attention! Username field must be ALL with Capital Letter</strong></p>
                            <p>
                                @if(Session::has('username'))
                                    @if(Session::get('currency') == 'IDR')
                                        RWI_{{strtoupper(Session::get('username'))}}
                                    @else
                                        RWM_{{strtoupper(Session::get('username'))}}
                                    @endif
                                @else
                                    Please login to check your username.
                                @endif
                            </p>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="{{$pc['pltpc']}}" target="_blank">DOWNLOAD NOW</a>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="md_agg_and" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <h5>Few Easy steps to Play Live Casino</h5>
                            <p>Android and IOS Version</p>
                            <ul>
                                <li>Step 1 : Kindly access crown casino and refer to the top image.</li>
                                <li>Step 2 : Click on the Try Now Mobile icon on the bottom left of the Casino Lobby.</li>
                                <li>Step 3 : Follow the steps that is shown at the installation Guide to Download and Login Via Mobile.</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a style="float:left;" href="{{route('agg')}}" target="_blank">PLAY NOW</a>
					<a href="{{$android['agg']}}" target="_blank">DOWNLOAD NOW</a>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="md_agg_ios" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <h5>Few Easy steps to Play Live Casino</h5>
                            <p>Android and IOS Version</p>
                            <ul>
                                <li>Step 1 : Kindly access crown casino and refer to the top image.</li>
                                <li>Step 2 : Click on the Try Now Mobile icon on the bottom left of the Casino Lobby.</li>
                                <li>Step 3 : Follow the steps that is shown at the installation Guide to Download and Login Via Mobile.</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="{{$ios['agg']}}" target="_blank">DOWNLOAD NOW</a>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="md_scr_and" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <h5>Play Any Time Any Where</h5>
                            <p>Android Version</p>
                            <ul>
                                <li>Step 1 : Click on the Download button.</li>
                                <li>Step 2 : Run apps, and install the file after download.</li>
                                @if(Auth::user()->check())
                                    <li>Step 3 : Login with : {{ (new \App\libraries\providers\SCR())->getScrIdByUsername(Session::get('username'), Session::get('currency')) }}</li>
                                @else
                                    <li>Please login to check your username.</li>
                                @endif
                            </ul>
                            <p><strong>Attention! For first time login please use this password: {{ (new \App\libraries\providers\SCR())->formatPlayerPassword('') }}[your login password]</strong></p>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="{{$android['scr']}}" target="_blank">DOWNLOAD NOW</a>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="md_scr_ios" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <h5>Play Any Time Any Where</h5>
                            <p>IOS Version</p>
                            <ul>
                                <li>Step 1 : Click on the Download button.</li>
                                <li>Step 2 : Run apps, and install the file after download.</li>
                                @if(Auth::user()->check())
                                    <li>Step 3 : Login with : {{ (new \App\libraries\providers\SCR())->getScrIdByUsername(Session::get('username'), Session::get('currency')) }}</li>
                                @else
                                    <li>Please login to check your username.</li>
                                @endif
                            </ul>
                            <p><strong>Attention! For first time login please use this password: {{ (new \App\libraries\providers\SCR())->formatPlayerPassword('') }}[your login password]</strong></p>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="{{$ios['scr']}}" target="_blank">DOWNLOAD NOW</a>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="md_alb_and" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <h5>Play Any Time Any Where</h5>
                            <p>Android Version</p>
                            <ul>
                                <li>Step 1 : Click on the Download button.</li>
                                <li>Step 2 : Run apps, and install the file after download.</li>
                                @if(Auth::user()->check())
                                    <li>Step 3 : Login with : {{ strtolower(Config::get(Session::get('currency').'.alb.prefix')).'_'.strtolower(Session::get('username')).'av2' }} </li>
                                @else
                                    <li>Please login to check your username.</li>
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="{{$android['alb']}}" target="_blank">DOWNLOAD NOW</a>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="md_alb_ios" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <h5>Play Any Time Any Where</h5>
                            <p>IOS Version</p>
                            <ul>
                                <li>Step 1 : Click on the Download button.</li>
                                <li>Step 2 : Run apps, and install the file after download.</li>
                                @if(Auth::user()->check())
                                    <li>Step 3 : Login with : {{ strtolower(Config::get(Session::get('currency').'.alb.prefix')).'_'.strtolower(Session::get('username')).'av2' }} </li>
                                @else
                                    <li>Please login to check your username.</li>
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="{{$ios['alb']}}" target="_blank">DOWNLOAD NOW</a>
                </div>
            </div>
        </div>
    </div>
<script src="{{url()}}/royalewin/resources/js/qrcode.js"></script>
<script type="text/javascript">
@for ($i=1; $i<=15; $i++)
var qrcode = new QRCode(document.getElementById("printQR{{$i}}"), {
	width : 60,
	height : 60
});

function makeCode{{$i}} () {
	var elText = document.getElementById("getLink{{$i}}");

	if (!elText.value) {
		alert("Input a text");
		elText.focus();
		return;
	}

	qrcode.makeCode(elText.value);
}

makeCode{{$i}}();

$("#getLink{{$i}}").
	on("blur", function () {
		makeCode{{$i}}();
	}).
	on("keydown", function (e) {
		if (e.keyCode == 13) {
			makeCode{{$i}}();
		}
	});
@endfor
</script>
@endsection
