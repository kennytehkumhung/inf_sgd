@extends('royalewin/mobile/master')

@section('title', 'Statement')

@section('css')
    @parent

    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
@endsection

@section('js')
    @parent

    <script src="{{ asset('/royalewin/resources/js/jquery-ui.js') }}"></script>
    <script type="text/javascript">
        $(function () {
            $(".datepicker").datepicker({dateFormat: 'yy-mm-dd', defaultDate: new Date()});

            setTimeout(transaction_history, 3000);
        });

        // Transfer history.
        function transaction_history() {
            $.ajax({
                type: "POST",
                url: "{{action('User\TransactionController@transactionProcess')}}",
                data: {
                    _token: "{{ csrf_token() }}",
                    date_from: $('#trans_history_date_from').val(),
                    date_to: $('#trans_history_date_to').val(),
                    record_type: $('#trans_history_record_type').val()
                },
            }).done(function (json) {
                var headStr = '';
                var bodyStr = '';

                headStr += '<thead><tr><th>{{Lang::get('public.ReferenceNo')}}</th><th>{{Lang::get('public.DateOrTime')}}</th><th>{{Lang::get('public.Type')}}</th><th>{{Lang::get('public.Amount')}}(MYR)</th><th>{{Lang::get('public.Status')}}</th><th>{{Lang::get('public.Reason')}}</th></tr></thead>';
                obj = JSON.parse(json);
                var count = 0;

                $.each(obj, function (i, item) {
                    bodyStr += '<tr><td>' + item.id + '</td><td>' + item.created + '</td><td>' + item.type + '</td><td>' + item.amount + '</td><td>' + item.status + '</td><td>' + item.rejreason + '</td></tr>';
                    count++;
                });

                if (count <= 0) {
                    bodyStr += '<tr><td colspan="6"><h2><center>{{ Lang::get('public.NoDataAvailable') }}</center></h2></td></tr>';
                }

                bodyStr = '<tbody>' + bodyStr + '</tbody>';

                $('#trans_history').html(headStr + bodyStr);
            });
        }
        setInterval(update_mainwallet, 10000);

        setInterval(function() {
            $("#trans_history_button").trigger("click");
        }, 10000);
        // End transfer history.
    </script>
@endsection

@section('content')
    <!-- Page Content -->
    <div class="container">

        @include('royalewin/mobile/include/profile_menu', array('selected' => 'Statement'))

        <div class="row bdr2 nopad">
            <div class="col-lg-12 nopad">
                <div class="col-lg-12 nopad">
                    <div class="wTit">
                        {{ Lang::get('public.TransactionHistory') }}
                    </div>
                </div>

            </div>

            <div class="col-lg-12 nopad wallRowtit">
                <div class="row">
                    <div class="col-lg-12">
                        <p>{{  Lang::get('public.YouMayFindYourLastXTransactionsHere', array('p1' => 50)) }}</p>
                    </div>
                    <div class="col-xs-5">
                        <label>{{ Lang::get('COMMON.TRANSACTIONTYPE') }}</label>
                    </div>
                    <div class="col-xs-6">
                        <select id="trans_history_record_type" style="width:200px;">
                            <option value="0" selected>{{ Lang::get('public.CreditAndDebitRecords') }}</option>
                            <option value="1">{{ Lang::get('public.CreditRecords') }}</option>
                            <option value="2">{{ Lang::get('public.DebitRecords') }}</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="col-lg-12 nopad wallRowtit">
                <div class="row">
                    <div class="col-xs-5">
                        <label>{{ Lang::get('public.DateRange') }}</label>
                    </div>
                    <div class="col-xs-3">
                        <input type="text" class="datepicker" id="trans_history_date_from" style="cursor:pointer;" value="{{ date('Y-m-d') }}"><br>
                    </div>
                    <div class="col-xs-3">
                        <input type="text" class="datepicker" id="trans_history_date_to" style="cursor:pointer;" value="{{ date('Y-m-d') }}">
                    </div>
                </div>
            </div>
        </div>

        <div class="ht1"></div>

        <div class="row">
            <div class="col-xs-3">
                <button id="trans_history_button" type="button" class="btn btn-primary" onClick="transaction_history();">{{ Lang::get('public.Submit') }}</button>
            </div>
        </div>

        <div class="ht1"></div>

        <div class="row">
            <div class="table-responsive">
                <table id="trans_history" class="table">
                    <thead>
                    <tr>
                        <th>{{Lang::get('public.ReferenceNo')}}</th>
                        <th>{{Lang::get('public.DateOrTime')}}</th>
                        <th>{{Lang::get('public.Type')}}</th>
                        <th>{{Lang::get('public.Amount')}} ({{ Session::get('currency') }})</th>
                        <th>{{Lang::get('public.Status')}}</th>
                        <th>{{Lang::get('public.Reason')}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td colspan="6"><h2><center>{{ Lang::get('public.NoDataAvailable') }}</center></h2></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
