@extends('royalewin/mobile/master')

@section('title', 'Withdraw')

@section('js')
    @parent

    <script type="text/javascript">
        $(function () {
            load_withdrawal();
        });

        // Withdrawal.
        function load_withdrawal() {
            $.ajax({
                url: "{{ route('withdraw') }}",
                type: "GET",
                dataType: "JSON",
                data: {
                    _token: "{{ csrf_token() }}",
                    as_json: true
                },
                success: function (result) {
                    if (typeof result != undefined && result != null) {
                        $("#withdraw_min").html(result.min);
                        $("#withdraw_max").html(result.max);

                        var banklistObj = $("#withdraw_bank");

                        for (var key in result.banklists) {
                            if (result.banklists.hasOwnProperty(key)) {
                                banklistObj.append('<option value="' + result.banklists[key].code + '">' + key + '</option>');
                            }
                        }
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    setTimeout(function () {
                        load_withdrawal();
                    }, 1000);
                }
            });
        }

        function submit_withdraw() {
            $.ajax({
                type: "POST",
                url: "{{route('withdraw-process')}}?" + $('#withdraw_form').serialize(),
                data: {
                    _token: "{{ csrf_token() }}",
                },
                beforeSend: function() {
                },
                success: function(json) {
                    obj = JSON.parse(json);
                    var str = '';
                    $.each(obj, function(i, item) {
                        if ('{{Lang::get('COMMON.SUCESSFUL')}}' == item) {
                            alert('{{Lang::get('COMMON.SUCESSFUL')}}');
                            window.location.href = "{{route('update-profile')}}?tab=4";
                        } else {
                            str += item + '\n';
                        }
                    });
                    if (str != '') {
                        alert(str);
                    }
                }
            });
        }
        // End withdrawal.
    </script>
@endsection


@section('content')
    <!-- Page Content -->
    <div class="container">

        @include('royalewin/mobile/include/profile_menu', array('selected' => 'Withdrawal'))

        <div class="row">
            <div class="col-lg-12">
                <p>{{ Lang::get('public.WithdrawalStep1') }}</p>
                <p>{{ Lang::get('public.WithdrawalStep2') }}</p>
            </div>
        </div>

        <div class="row">
        </div>

        <div class="row bdr2 nopad">
            <form id="withdraw_form">
                <div class="col-lg-12 nopad">
                    <div class="col-lg-12 nopad">
                        <div class="wTit">
                            {{ Lang::get('public.Withdrawaldetails') }}
                        </div>
                    </div>
                </div>

                <div class="col-lg-12 nopad wallRowtit">
                    <div class="row">
                        <div class="col-lg-12">
                            <p>{{ Lang::get('public.BankDetailsContent') }}</p>
                        </div>
                        <div class="col-xs-5">
                            <label>{{ Lang::get('public.Amount') }}*</label>
                        </div>
                        <div class="col-xs-5">
                            <input type="text" id="withdraw_amount" name="amount" value="0.00" />
                        </div>
                        <div class="row"></div>
                        <div class="col-xs-5">
                        </div>
                        <div class="col-xs-5">
                            <div class="trsfDet">
                                {{ Lang::get('public.Minimum') }} MYR <span id="withdraw_min">-</span> / {{ Lang::get('public.Maximum') }} MYR <span id="withdraw_max">-</span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-12 nopad wallRowtit">
                    <div class="row">
                        <div class="col-xs-5">
                            <label>{{ Lang::get('public.Bank') }}*</label>
                        </div>
                        <div class="col-xs-5">
                            <select id="withdraw_bank" name="bank"></select>
                        </div>
                    </div>
                </div>

                <div class="col-lg-12 nopad wallRowtit">
                    <div class="row">
                        <div class="col-xs-5">
                            <label>{{ Lang::get('public.FullName') }}</label>
                        </div>
                        <div class="col-xs-5">
                            <input type="text" value="{{ Session::get('fullname') }}" readonly />
                        </div>
                    </div>
                </div>

                <div class="col-lg-12 nopad wallRowtit">
                    <div class="row">
                        <div class="col-xs-5">
                            <label>{{ Lang::get('public.BankAccountNo') }}</label>
                        </div>
                        <div class="col-xs-5">
                            <input id="withdraw_accountno" name="accountno" type="text" />
                        </div>
                    </div>
                </div>
            </form>
        </div>

        <div class="ht1"></div>

        <div class="row">
            <div class="col-xs-3">
                <button type="button" class="btn btn-primary" onclick="submit_withdraw();">{{ Lang::get('public.Submit') }}</button>
            </div>
            <div class="col-xs-3">
                <button type="button" class="btn btn-primary" onclick="resetForm('withdraw_form');">{{ Lang::get('public.Reset') }}</button>
            </div>
        </div>
    </div>
@endsection
