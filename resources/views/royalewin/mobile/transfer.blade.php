@extends('royalewin/mobile/master')

@section('title', 'Transfer')

@section('js')
    @parent

    <script type="text/javascript">
        $(function () {
            $("#transfer_to").change();
        });

        // Transfer.
        function load_transfer_balance(code, type) {
            if(code == 'MAIN') {
                url = '{{route('mainwallet')}}';
            } else {
                url = '{{route('getbalance')}}';
            }
            $.ajax({
                type: "POST",
                url:  url,
                data: {
                    _token:     "{{ csrf_token() }}",
                    product:    code,
                },
                beforeSend: function(balance) {
                    $('#' + type).html('<img style="float:left;position:static;top:0px;margin-left:20px;" src="{{asset('/front/img/ajax-loading.gif')}}" width="20" height="20">');
                },
                success: function(balance) {
                    if (code == 'MAIN') {
                        $('.main_wallet').html(parseFloat(Math.round(balance * 100) / 100).toFixed(2));
                    }
                    $('#' + type).html('{{Session::get('currency')}} ' + parseFloat(Math.round(balance * 100) / 100).toFixed(2));
                }
            });
        }

        function submit_transfer() {
            $.ajax({
                type: "POST",
                url: "{{route('transfer-process')}}",
                data: {
                    _token: 		 "{{ csrf_token() }}",
                    amount:			 $('#transfer_amount').val(),
                    from:     		 $('#transfer_from').val(),
                    to: 			 $('#transfer_to').val(),
                },
                beforeSend: function() {
                    $('#btn_submit_transfer').attr('onClick', '');
                    $('#btn_submit_transfer').html('Loading...');
                },
                success: function(json) {
                    obj = JSON.parse(json);
                    var str = '';
                    $.each(obj, function(i, item) {

                        if ('{{Lang::get('COMMON.SUCESSFUL')}}' == item){

                            //$('.main_wallet').html(obj.main_wallet);

                        }
                        if (i != 'main_wallet') {
                            str += item + '\n';
                        }
                    });

                    load_transfer_balance($('#transfer_from').val(), 'transfer_from_balance');
                    load_transfer_balance($('#transfer_to').val(), 'transfer_to_balance');
                    getBalance(false);
                    $('#btn_submit_transfer').attr('onClick', 'submit_transfer()');
                    $('#btn_submit_transfer').html('{{Lang::get('public.Submit')}}');
                    if (str != '') {
                        alert(str);
                    }
                }
            });
        }
        // End transfer.
    </script>
@endsection

@section('content')
    <!-- Page Content -->
    <div class="container">

        @include('royalewin/mobile/include/profile_menu', array('selected' => 'Transfer'))

        <div class="row bdr2 nopad">
            <div class="col-lg-12 nopad">
                <div class="col-lg-12 nopad">
                    <div class="wTit">
                        {{ Lang::get('public.TransferDetails') }}
                    </div>
                </div>
            </div>

            <div class="col-lg-12 nopad wallRowtit">
                <div class="row">
                    <div class="col-xs-3">
                        <label>{{ Lang::get('public.From') }}*</label>
                    </div>
                    <div class="col-xs-5">
                        <select id="transfer_from" onchange="load_transfer_balance(this.value,'transfer_from_balance')">
                            <option selected="selected" value="MAIN">{{ Lang::get('public.MainWallet') }}</option>
                            @foreach( Session::get('products_obj') as $prdid => $object)
                                <?php

                                $showPrd = true;
                                if (Session::get('currency') == 'IDR' && ($object->code == 'PSB' || $object->code == 'OPU' || $object->code == 'CTB' || $object->code == 'SCR')) {
                                    $showPrd = false;
                                } elseif (Session::get('currency') == 'MYR' && ($object->code == 'FBL' || $object->code == 'ISN' || $object->code == 'S128')) {
                                    $showPrd = false;
                                }
                                ?>
                                @if ($showPrd)
                                    <option value="{{$object->code}}">{{$object->name}}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                    <div class="col-xs-3">
                    </div>
                    <div class="col-xs-5">
                        <div class="trsfDet">
                            {{ Lang::get('public.Balance') }}: <span id="transfer_from_balance" style="width: 132px !important;margin-right: 218px !important;">{{Session::get('currency')}} {{App\Http\Controllers\User\WalletController::mainwallet()}}</span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-12 nopad wallRowtit">
                <div class="row">
                    <div class="col-xs-3">
                        <label>{{ Lang::get('public.To') }}*</label>
                    </div>
                    <div class="col-xs-5">
                        <select id="transfer_to" onchange="load_transfer_balance(this.value,'transfer_to_balance')">
                            @foreach( Session::get('products_obj') as $prdid => $object)
                                <?php

                                $showPrd = true;
                                if (Session::get('currency') == 'IDR' && ($object->code == 'PSB' || $object->code == 'OPU' || $object->code == 'CTB' || $object->code == 'SCR')) {
                                    $showPrd = false;
                                } elseif (Session::get('currency') == 'MYR' && ($object->code == 'FBL' || $object->code == 'ISN' || $object->code == 'S128')) {
                                    $showPrd = false;
                                }
                                ?>
                                @if ($showPrd)
                                    <option value="{{$object->code}}">{{$object->name}}</option>
                                @endif
                            @endforeach
                            <option  value="MAIN">{{Lang::get('public.MainWallet')}}</option>
                        </select>
                    </div>
                    <div class="col-xs-3">
                    </div>
                    <div class="col-xs-5">
                        <div class="trsfDet">
                            {{ Lang::get('public.Balance') }}: <span id="transfer_to_balance" style="width: 132px !important;">{{Session::get('currency')}} 0.00</span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-12 nopad wallRowtit">
                <div class="row">
                    <div class="col-xs-3">
                        <label>{{ Lang::get('public.Amount') }}*</label>
                    </div>
                    <div class="col-xs-5">
                        <input name="" type="text" value="0.00" id="transfer_amount"/>
                    </div>
                </div>

                <div class="row">
                    <span id="error_deposit" style="color:Red;display:none;">* {!! Lang::get('public.BalanceZeroPleaseMakeDeposit', array('p1' => route('update-profile').'?tab=1')) !!}</span>
                    <span id="error_require" style="color:Red;display:none;">{{ Lang::get('public.Required') }}</span>
                </div>
            </div>
        </div>

        <div class="ht1"></div>

        <div class="row">
            <div class="col-xs-3">
                <button type="button" class="btn btn-primary" onClick="submit_transfer()">{{ Lang::get('public.Submit') }}</button>
            </div>
        </div>
    </div>
@endsection
