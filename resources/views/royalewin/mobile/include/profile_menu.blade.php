@section('js')
    @parent

    <script type="text/javascript">
        $(function () {
            // Init selected menu.
            var selected = "{{ isset($selected) ? $selected : '' }}";

            if (selected != "") {
                $("." + selected).addClass("selected");
            }

            switch (selected) {
                case "Deposit":
                    setSwitchWebTypeBackUrl("{{ route('update-profile') }}?tab=1");
                    break;
                case "Transfer":
                    setSwitchWebTypeBackUrl("{{ route('update-profile') }}?tab=3");
                    break;
                case "Withdrawal":
                    setSwitchWebTypeBackUrl("{{ route('update-profile') }}?tab=2");
                    break;
                case "Statement":
                    setSwitchWebTypeBackUrl("{{ route('update-profile') }}?tab=4");
                    break;
                case "History":
                    setSwitchWebTypeBackUrl("{{ route('update-profile') }}?tab=5");
                    break;
                case "Profile":
                    setSwitchWebTypeBackUrl("{{ route('update-profile') }}?tab=0");
                    break;
            }

            setTimeout(function () {
                getBalance(true);
            }, 1000);
        });

        @if (Auth::user()->check())
            function update_mainwallet(){
				$.ajax({
					type: "POST",
					url: "{{route( 'mainwallet', [ '_token'=> csrf_token() ])}}",
					beforeSend: function(balance){
					},
					success: function(balance) {
                        var walletObj = $('.main_wallet');

                        if (walletObj.is("input[type='text']")) {
                            $('.main_wallet').val(parseFloat(Math.round(balance * 100) / 100).toFixed(2));
                        } else {
                            $('.main_wallet').html(parseFloat(Math.round(balance * 100) / 100).toFixed(2));
                        }
//						total_balance += parseFloat(balance);
					}
				});
            }
			function getBalance(type) {
				var total_balance = 0;
                var currency = "{{ Session::get('currency') }}";

				$.when(
					$.ajax({
                        type: "POST",
						url: "{{route('mainwallet', [ '_token'=> csrf_token()])}}",
						beforeSend: function(balance){
						},
						success: function(balance){
                            var walletObj = $('.main_wallet');

                            if (walletObj.is("input[type='text']")) {
                                $('.main_wallet').val(parseFloat(Math.round(balance * 100) / 100).toFixed(2));
                            } else {
                                $('.main_wallet').html(parseFloat(Math.round(balance * 100) / 100).toFixed(2));
                            }
							total_balance += parseFloat(balance);
						}
					}),
					@foreach(Session::get('products_obj') as $prdid => $object)
						$.ajax({
							type: "POST",
							url: "{{route( 'getbalance', [ '_token'=> csrf_token(), 'product'=> $object->code ])}}&rd=<?php echo rand(10000, 99999); ?>",
							beforeSend: function(balance){
                                var balObj = $('.{{$object->code}}_balance');

                                if (balObj.is("input[type='text']")) {
                                    var balLoadingObj = $(".{{$object->code}}_balance_loading");

                                    if (balLoadingObj.length) {
                                        balLoadingObj.show();
                                    }
                                } else {
                                    balObj.html('<img style="" src="{{url()}}/front/img/ajax-loading.gif" width="20" height="10">');
                                }
							},
							success: function(balance){
                                var balObj = $('.{{$object->code}}_balance');

								if (balance != '{{Lang::get('Maintenance')}}') {
                                    if (balObj.is("input[type='text']")) {
                                        balObj.val(parseFloat(Math.round(balance * 100) / 100).toFixed(2));
                                    } else {
                                        balObj.html(parseFloat(Math.round(balance * 100) / 100).toFixed(2));
                                    }

                                    var skipTotal = false;
                                    if (currency == "IDR") {
                                        if ("{{$object->code}}" == "PSB" || "{{$object->code}}" == "OPU" || "{{$object->code}}" == "CTB") {
                                            skipTotal = true;
                                        }
                                    } else {
                                        if ("{{$object->code}}" == "FBL" || "{{$object->code}}" == "S128") {
                                            skipTotal = true;
                                        }
                                    }

                                    if (!skipTotal) {
                                        total_balance += parseFloat(balance);
                                    }
								} else {
                                    if (balObj.is("input[type='text']")) {
                                        balObj.val(balance);
                                    } else {
                                        balObj.html(balance);
                                    }
								}

                                var balLoadingObj = $(".{{$object->code}}_balance_loading");

                                if (balLoadingObj.length) {
                                    balLoadingObj.hide();
                                }
							}
						})
						@if ($prdid != Session::get('last_prdid'))
						,
						@endif
					@endforeach
				).then(function() {
                    var objs = $('#total_balance, #top_total_balance');

                    objs.each(function () {
                        var obj = $(this);
                        if (obj.is("input[type='text']")) {
                            obj.val(parseFloat(Math.round(total_balance * 100) / 100).toFixed(2));
                        } else {
                            obj.html(parseFloat(Math.round(total_balance * 100) / 100).toFixed(2));
                        }
                    });

                    $(".total_balance_loading").hide();
				});
			}
        @endif

        // Reset form.
        function resetForm(formId) {
            var form = $("#" + formId);

            $("input[type='text']:not([readonly])", form).each(function () {
                console.log($(this));
            });

            // Standard reset method.
            $("input[type='text']:not([readonly])", form).val("");
            $("input[type='password']:not([readonly])", form).val("");
            $("input[type='radio']:not([readonly])", form).prop("checked", false);
            $("input[type='checkbox']:not([readonly])", form).prop("checked", false);
            $("select:not([readonly])", form).each(function () {
                $(this).val($("option:first", $(this)).val());
            });
        }
        // End reset form.
    </script>
@endsection

@section('modal')
    @parent

    <div id="md-wallet" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">{{ Lang::get('public.Wallet') }}</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-1 col-xs-6">
                            <div class="wallBox1" style="min-height: 80px;">
                                <span class="wallTit1">{{ Lang::get('public.MainWallet') }}</span>
                                <span id="top_total_balance" class="wallTotal main_wallet">0.00</span>
                            </div>
                        </div>

                        <?php $count = 0; ?>
                        @foreach( Session::get('products_obj') as $prdid => $object)
                            <div class="col-md-1 col-xs-6">
                                <div class="wallBox" style="min-height: 80px;">
                                    <span class="wallTit">{{ $object->name }}</span>
                                    <span class="wallTotal {{ $object->code }}_balance">0.00</span>
                                </div>
                            </div>
                        @endforeach

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">{{ Lang::get('COMMON.CLOSE') }}</button>
                </div>
            </div>
        </div>
    </div>
@endsection

<div class="ht1"></div>

<div class="row">
    <div class="col-md-2 col-xs-8">
        <div class="walletForm">
            <div class="col-xs-6">
                <label>{{ Lang::get('public.Balance') }}</label>
            </div>
            <div class="col-xs-6" style="position: relative;">
                <input id="total_balance" type="text" placeholder="0.00" disabled>
                <img class="total_balance_loading" style="position: absolute; top: 6px; left: 77px; width: 12px; height: 12px;" src="{{ asset('/front/img/ajax-loading.gif') }}">
            </div>
        </div>
    </div>

    <div class="col-md-2 col-xs-4">
        <!-- Trigger the modal with a button -->
        <button type="button" class="btn wallet1 btn-xs" data-toggle="modal" data-target="#md-wallet">{{ Lang::get('public.Wallet') }}</button>
    </div>
</div>

<div class="ht1"></div>

<div class="row">
    <div class="col-md-1 col-xs-4">
        <div class="wallnav">
            <a href="{{ route('deposit') }}" class="Deposit">{{ Lang::get('public.Deposit') }}</a>
        </div>
    </div>

    <div class="col-md-1 col-xs-4">
        <div class="wallnav">
            <a href="{{ route('transfer') }}" class="Transfer">{{ Lang::get('public.Transfer') }}</a>
        </div>
    </div>

    <div class="col-md-1 col-xs-4">
        <div class="wallnav">
            <a href="{{ route('withdraw') }}" class="Withdrawal">{{ Lang::get('public.Withdrawal') }}</a>
        </div>
    </div>

    <div class="col-md-1 col-xs-4">
        <div class="wallnav">
            <a href="{{ route('transaction') }}" class="Statement">{{ Lang::get('public.Statement') }}</a>
        </div>
    </div>

    <div class="col-md-1 col-xs-4">
        <div class="wallnav">
            <a href="{{ route('profitloss') }}" class="History">{{ Lang::get('public.History') }}</a>
        </div>
    </div>

    <div class="col-md-1 col-xs-4">
        <div class="wallnav">
            <a href="{{ route('update-profile') }}" class="Profile">{{ Lang::get('public.Profile') }}</a>
        </div>
    </div>
</div>

<div class="ht1"></div>
