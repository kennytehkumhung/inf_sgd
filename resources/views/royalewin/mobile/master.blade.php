<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="@yield('keywords')">
    <meta name="description" content="@yield('description')">

    <title>ROYALEWIN: @yield('title'))</title>

    <link href="{{ asset('/royalewin/img/favicon.ico') }}" rel="shortcut icon" type="image/icon" />
    <link href="{{ asset('/royalewin/img/favicon.png') }}" rel="icon" type="image/icon" />

    <!-- Bootstrap Core CSS -->
    <link href="{{ asset('/royalewin/mobile/css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="{{ asset('/royalewin/mobile/css/modern-business.css') }}" rel="stylesheet">
    <link href="{{url()}}/royalewin/mobile/css/wc.css" rel="stylesheet" type="text/css" />

    <!-- Custom Fonts -->
    <link href="{{ asset('/royalewin/mobile/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">

    <script src="{{url()}}/royalewin/mobile/js/jquery-2.1.3.min.js"></script>

    <script src="{{url()}}/royalewin/mobile/js/jquery.countdown.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="{{ asset('/royalewin/mobile/js/bootstrap.min.js') }}"></script>

    @yield('css')

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
<!-- Msg -->
<div class="modal fade" id="msgbox" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title text-white" id="myModalLabel">Inbox</h4>
      </div>
      <div class="modal-body">
      <div class="msgBx">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
		<thead>
			<th></th>
			<th>Title</th>
			<th>Content</th>
		</thead>
		<tbody id="msgTable">
		</tbody>
	</table>

    </div>
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>
<div id="msgINBOX">
</div>
<!-- Msg -->

<!-- Navigation -->
<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container tb">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"><img src="{{ asset('/royalewin/mobile/img/logo.png') }}"></a>

            <div class="dSwitch"><a id="dSwitchBtn" href="{{ route('switchwebtype') }}?type=d"><img src="{{ asset('/royalewin/mobile/img/switch-icon.png') }}"></a></div>

            @if (Auth::user()->check())
                <div class="mngWrap">
					<div class="userJoin">
					<a href="#msgbox" data-toggle="modal" data-target="#msgbox" onClick="getAllMessage()" id="totalMessage"></a>
					</div>
                    <div class="greet"><a href="{{ route('update-profile') }}">{{ Session::get('username') }}</a></div>

                    <div class="regBtn"><a href="{{ route('logout') }}">{{ Lang::get('COMMON.LOGOUT') }}</a></div>
                </div>
            @else
                <div class="mngWrap" style="margin-right: 12px;">
                    <div class="logBtn"><a href="#" data-toggle="modal" data-target="#login">{{ Lang::get('public.Login') }}</a></div>

                    <div class="regBtn"><a href="{{ route('register') }}">{{ Lang::get('public.Register') }}</a></div>
                </div>
            @endif

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="{{ route('homepage') }}"><img src="{{ asset('/royalewin/mobile/img/home-icon.png') }}"><span>{{ Lang::get('public.Home') }}</span></a>
                    </li>
                    <li>
                        <a href="{{ route('aboutus') }}"><img src="{{ asset('/royalewin/mobile/img/more-info.png') }}"><span>{{ Lang::get('public.MoreInfo') }}</span></a>
                    </li>
                    <li>
                        <a href="{{ route('promotion') }}"><img src="{{ asset('/royalewin/mobile/img/promo-icon.png') }}"><span>{{ Lang::get('public.Promotion') }}</span></a>
                    </li>
                    <li>
                        <a href="{{ route('mobile') }}"><img src="{{ asset('/royalewin/mobile/img/mobile-1-icon.png') }}"><span>{{ Lang::get('public.Mobile') }}</span></a>
                    </li>
                    <li>
                        <a href="{{ route('contactus') }}"><img src="{{ asset('/royalewin/mobile/img/contact-icon.png') }}"><span>{{ Lang::get('public.ContactUs') }}</span></a>
                    </li>
                    <li>
                        <a href="javascript:void(0);" onclick="livechat();"><img src="{{ asset('/royalewin/mobile/img/chat-icon.png') }}"><span>{{ Lang::get('public.LiveChat2') }}</span></a>
                    </li>
                    @if (Auth::user()->check())
                        <li>
                            <a href="{{ route('transfer') }}"><img src="{{ asset('/royalewin/mobile/img/wallet-icon.png') }}"><span>{{ Lang::get('public.Wallet') }}</span></a>
                        </li>
                    @else
                        <li>
                            <a href="{{ route('forgotpassword') }}"><img src="{{ asset('/royalewin/mobile/img/forgotpassword-icon.png') }}"><span>{{ Lang::get('public.ForgotPassword') }}</span></a>
                        </li>
                    @endif
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
    </div>
    <!-- /.container -->
</div>
<!--<div class="row">
    <div class="timerWrap">
        <div class="row">
            <div class="col-md-6 col-xs-12">
                <div class="wcWrap">
                <img class="img-responsive" src="{{url()}}/royalewin/mobile/img/wc-img.png">
                </div>
            </div>
            <div class="col-md-6 col-xs-12">
                <div id="getting-started" class="timerDig"></div>
                <script type="text/javascript">
                    $("#getting-started")
                        .countdown("2018/06/14 23:00:00", function(event) {
                        $(this).text(
                        event.strftime('%D Days  %H: %M: %S')
                        );
                    });
                </script>
            </div>
        </div>
    </div>
</div>-->
@yield('content')
<!-- /.row -->

<!-- Footer -->
<footer>

    <div class="row">
        <div class="col-lg-12">
            <div class="footerBank">
                <div class="row">
                    <div class="col-md-1 col-xs-4 text-center"><img src="{{ asset('/royalewin/mobile/img/sp-1.png') }}"></div>
                    <div class="col-md-1 col-xs-4 text-center"><img src="{{ asset('/royalewin/mobile/img/sp-2.png') }}"></div>
                    <div class="col-md-1 col-xs-4 text-center"><img src="{{ asset('/royalewin/mobile/img/betsoft.png') }}"></div>
                    <div class="col-md-1 col-xs-4 text-center"><img src="{{ asset('/royalewin/mobile/img/hgming.png') }}"></div>
                    <div class="col-md-1 col-xs-4 text-center"><img src="{{ asset('/royalewin/mobile/img/fuse.png') }}"></div>
                    <div class="col-md-1 col-xs-4 text-center"><img src="{{ asset('/royalewin/mobile/img/ag.png') }}"></div>
                    <div class="col-md-1 col-xs-4 text-center"><img src="{{ asset('/royalewin/mobile/img/joker.png') }}"></div>
                    <div class="col-md-1 col-xs-4 text-center"><img src="{{ asset('/royalewin/mobile/img/maxbet.png') }}"></div>
                    <div class="col-md-1 col-xs-4 text-center"><img src="{{ asset('/royalewin/mobile/img/pt.png') }}"></div>
                    <div class="col-md-1 col-xs-4 text-center"><img src="{{ asset('/royalewin/mobile/img/sky.png') }}"></div>
                    <div class="col-md-1 col-xs-4 text-center"><img src="{{ asset('/royalewin/mobile/img/wsp.png') }}"></div>
                    <div class="col-md-1 col-xs-4 text-center"><img src="{{ asset('/royalewin/mobile/img/ev.png') }}"></div>
                    <div class="col-md-1 col-xs-4 text-center"><img src="{{ asset('/royalewin/mobile/img/ibc.png') }}"></div>
                    <div class="col-md-1 col-xs-4 text-center"><img src="{{ asset('/royalewin/mobile/img/ps.png') }}"></div>
                    <div class="col-md-1 col-xs-4 text-center"><img src="{{ asset('/royalewin/mobile/img/sp.png') }}"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 text-center">
            <p class="foot">© 2016 ROYALEWIN All Right Reserved | 18+</p>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 text-center">
            <div class="footMenu">
                <ul>
                    <li><a href="{{ route('aboutus') }}">{{ Lang::get('public.AboutUs') }}</a></li>
                    <li><a href="{{ route('contactus') }}">{{ Lang::get('public.ContactUs') }}</a></li>
                    <li><a href="{{ route('howtojoin') }}">{{ Lang::get('public.FAQ') }}</a></li>
                    <li><a href="{{ route('howtojoin') }}">{{ Lang::get('public.HowToJoin') }}</a></li>
                    <li style="border-right: 0px;"><a href="{{ route('tnc') }}">{{ Lang::get('public.TNC') }}</a></li>
                </ul>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>

        <div class="row">
            <div class="col-lg-12 text-center">
                <br>
                <a href="{{ route('switchwebtype') }}?type=d" style="font-size: 12px; font-weight: bold; color: #ffae00;">{{ Lang::get('public.DesktopSite') }}</a>
            </div>
        </div>
</footer>

<script type="text/javascript">
    $(document).ready(function(){
		@if (Auth::user()->check())
			load_message();
		@endif
    });

    @if (!Auth::user()->check())
        function enterpressalert(e, textarea)
        {
            var code = (e.keyCode ? e.keyCode : e.which);
            if(code == 13) { //Enter keycode
                login();
            }
        }

        function login()
        {
            $("#login1").hide();
            $("#loading1").show();
            $.ajax({
                type: "POST",
                url: "{{route('login')}}",
                data: {
                    _token: "{{ csrf_token() }}",
                    username: $('#username').val(),
                    password: $('#password').val(),
                    code:     $('#code').val(),
                },
            }).done(function(json) {
                obj = JSON.parse(json);
                var str = '';
                $.each(obj, function(i, item) {
                    str += item + '\n';
                });
                if ("{{Lang::get('COMMON.SUCESSFUL')}}\n" == str) {
                    location.reload();
                } else {
                    alert(str);
                    $("#loading1").hide();
                    $("#login1").show();
                }
            });
        }
    @elseif (Auth::user()->check())
		function load_message() {
			$.ajax({
				url: "{{ route('showTotalUnseenMessage') }}",
				type: "GET",
				dataType: "text",
				data: {
				},
				success: function (result) {
					if(result>0){
						$("#totalMessage").html('<img src="{{ asset('/royalewin/mobile/img/msg-icon-in.gif') }}">');
					}else{
						$("#totalMessage").html('<img src="{{ asset('/royalewin/mobile/img/msg-icon.png') }}">');
					}
				},
				error: function (XMLHttpRequest, textStatus, errorThrown) {
				}
			});
		}
        setInterval(load_message,20000);
	@endif

    function setSwitchWebTypeBackUrl(url) {
        var obj = $("#dSwitchBtn");
        var href = obj.attr("href");
        var sym = "?";

        if (href.indexOf("?") > -1) {
            sym = "&";
        }

        obj.attr("href", href + sym + "to=" + encodeURIComponent(url));
    }

    function livechat() {
        window.open('https://chatserver.comm100.com/ChatWindow.aspx?planId=360&visitType=1&byHref=1&partnerId=-1&siteid=160856', '', 'width=525, height=520, top=150, left=250');
    }
	function getAllMessage(){
        $.ajax({
            type: "GET",
            url: "{{route('inbox')}}?type=getData",
            data: {
                _token: "{{ csrf_token() }}"
            },
        }).done(function (data) {
			var trHTML = '';
			var divContent='';
			var obj= data;
			obj = JSON.parse(obj);
			$.each(obj, function(i, item) {
				$.each(item, function(key, val) {
					//alert(val['id']+'   '+val['title']+'   '+val['content']+'   '+val['status']+'   '+val['date']+'   '+val['is_all']);
					if(val['status']==0){
						trHTML+='<tr><td><img src="{{ asset('/royalewin/mobile/img/mark-unread.png') }}"></td><td id="valTitle"><a href="#msgC'+val['id']+'" data-toggle="modal" data-target="#msgC'+val['id']+'" onClick="updateStatus('+val['id']+','+val['is_all']+')">'+val['title']+'</a></td><td id="valDate">'+val['date']+'</td></tr>';
					}else if(val['status']==1){
						trHTML+='<tr><td><img src="{{ asset('/royalewin/mobile/img/mark-read.png') }}"></td><td id="valTitle"><a href="#msgC'+val['id']+'" data-toggle="modal" data-target="#msgC'+val['id']+'">'+val['title']+'</a></td><td id="valDate">'+val['date']+'</td></tr>';
					}

					divContent+='<div class="modal fade" id="msgC'+val['id']+'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"><div class="modal-dialog" role="document"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button><h4 class="modal-title text-white" id="myModalLabel">Inbox</h4></div><div class="modal-body"><div class="msgBx">'+val['content']+'</div></div><div class="modal-footer"> </div></div></div></div>';

					$("#msgTable").html(trHTML);
					$("#msgINBOX").html(divContent);
					})
			})
		});
	}
	function updateStatus(id,is_all){
        $.ajax({
            type: "POST",
            url: "{{action('User\InboxMessage@viewMessage')}}?id="+id+"&is_all="+is_all,
            data: {
                _token: "{{ csrf_token() }}"
            },
        });
	}
</script>

@yield('js')

@if (!Auth::user()->check())
    <!-- Login -->
    <div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">{{ Lang::get('COMMON.PLEASELOGIN') }}</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-3 col-xs-3">
                            {{ Lang::get('public.Username') }}:
                        </div>
                        <div class="col-md-3 col-xs-3">
                            <input type="text" id="username" name="username" onKeyPress="enterpressalert(event, this)">
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-3 col-xs-3">
                            {{ Lang::get('public.Password') }}:
                        </div>
                        <div class="col-md-3 col-xs-3">
                            <input type="password" id="password" name="password" onKeyPress="enterpressalert(event, this)">
                            <input type="hidden" id="code" name="code" value="0000" />
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="login1" onclick="login();">{{ Lang::get('public.Login') }}</button>
                    <button type="button" class="btn btn-primary" id="loading1" style="display:none;">{{ Lang::get('public.Loading') }}</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Login -->
@endif

@yield('modal')

{{-- Update @\master.blade.php as well --}}
@if($_SERVER['HTTP_HOST'] == 'rwinvip.com')

    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function()
            { (i[r].q=i[r].q||[]).push(arguments)}

            ,i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-88529618-1', 'auto');
        ga('send', 'pageview');

    </script>

@elseif($_SERVER['HTTP_HOST'] == 'rwin888.com')

    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-97996888-1', 'auto');
        ga('send', 'pageview');

    </script>

@else

    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function()
            { (i[r].q=i[r].q||[]).push(arguments)}
            ,i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
        ga('create', 'UA-41261240-1', 'auto');
        ga('send', 'pageview');
    </script>

@endif

</body>
</html>
