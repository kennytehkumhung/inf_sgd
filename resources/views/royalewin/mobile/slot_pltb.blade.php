@extends('royalewin/mobile/master')

@if (Session::get('currency') == 'MYR')
    @section('title', 'Online Slot Malaysia - PlayTech Slot Games')
    @section('keywords', 'Online Slot Malaysia - PlayTech Slot Games')
    @section('description', 'Play PlayTech Online Slots at RoyaleWin Online Casino Malaysia and claim a FREE SPIN bonus today!')
@elseif (Session::get('currency') == 'IDR')
    @section('title', 'Situs Judi Playtech Online Slot Indonesia')
    @section('keywords', 'Situs Judi Playtech Online Slot Indonesia')
    @section('description', 'RoyaleWin Agen Judi Slot Online menyediakan permainan PlayTech Slot dan taruhan online terpercaya dan terbaik di Indonesia.')
@endif

@section('css')
    @parent

    <style type="text/css">
        .slot_box {
            margin-bottom: 25px;
            max-width: 195px;
            min-height: 147px;
            border: 1px solid #ffae00;
        }

        .slot_box img {
            color: #ffae00;
            font-weight: bold;
            width: 100%;
            height: auto;
            max-height: 175px;
        }

        .slot_box span {
            color: #ffae00;
            font-weight: bold;
            line-height: 28px;
        }
    </style>
@endsection

@section('js')
    @parent

    <script type="text/javascript">
        $(function () {
            resizeSlotBox();

            window.addEventListener("resize", resizeSlotBox);
        });

        function resizeSlotBox() {
            var maxSlotBoxHeight = 0;
            var boxHeight = 0;

            $(".slot_box ").each(function () {
                boxHeight = parseFloat($(this).css("height").replace("px", ""));

                if (boxHeight > maxSlotBoxHeight) {
                    maxSlotBoxHeight = boxHeight;
                }
            });

            $(".slot_box").css("height", maxSlotBoxHeight + "px");
        }
    </script>
@endsection

@section('content')
    <!-- Page Content -->
    <div class="container">

        <div class="ht1"></div>
		@if( Session::get('currency') == 'IDR' )
			<!--LOBBY MENU-->
			<div class="row">
				<div class="col-md-1 col-xs-4">
					<a class="category-btn" href="{{route('pltb', [ 'type' => 'brand' , 'category' => '1' ] )}}">
						<div>Branded Games</div>
					</a>
				</div>

				<div class="col-md-1 col-xs-4">
					<a class="category-btn" href="{{route('pltb', [ 'type' => 'slot' , 'category' => 'slot' ] )}}">
						<div>Slot</div>
					</a>
				</div>

				<div class="col-md-1 col-xs-4">
					<a class="category-btn" href="{{route('pltb', [ 'type' => 'slot' , 'category' => 'tablecards' ] )}}">
						<div>Tablecards</div>
					</a>
				</div>
			</div>
			<!--LOBBY MENU-->
			

			<div class="row">
				@foreach( $lists as $list )
					@if($list->html5Code != '')
					<div class="col-lg-3 col-md-4 col-xs-6 text-center">
						<div class="slot_box">
							<a href="javascript:void(0)" onclick="@if (Auth::user()->check())window.open('{{route('pltbslotiframe' , [ 'gamecode' => ($list->html5Code != '' ? $list->html5Code : $list->code) ] )}}', 'pltb_slot', 'width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif" >
								<img src="{{asset('/royalewin/img/plt/'.($list->code != '' ? $list->code : $list->html5Code).'.jpg')}}" alt=""/>

								@if (Lang::getLocale() == 'cn')
									<span>{{$list->gameNameCn}}</span>
								@else
									<span>{{$list->gameName}}</span>
								@endif
							</a>
						</div>
					</div>
					@endif
				@endforeach
			</div>
		@endif
		
		@if( Session::get('currency') == 'MYR' )
			<!--LOBBY MENU-->
			<div class="row">
				<div class="col-md-1 col-xs-4">
					<a class="category-btn" href="{{route('plt', [ 'type' => 'brand' , 'category' => '1' ] )}}">
						<div>Branded Games</div>
					</a>
				</div>

				<div class="col-md-1 col-xs-4">
					<a class="category-btn" href="{{route('plt', [ 'type' => 'slot' , 'category' => 'slot' ] )}}">
						<div>Slot</div>
					</a>
				</div>

				<div class="col-md-1 col-xs-4">
					<a class="category-btn" href="{{route('plt', [ 'type' => 'slot' , 'category' => 'tablecards' ] )}}">
						<div>Tablecards</div>
					</a>
				</div>
			</div>
			<!--LOBBY MENU-->
			

			<div class="row">
				@foreach( $lists as $list )
					@if($list->html5Code != '')
					<div class="col-lg-3 col-md-4 col-xs-6 text-center">
						<div class="slot_box">
							<a href="javascript:void(0)" onclick="@if (Auth::user()->check())window.open('{{route('pltslotiframe' , [ 'gamecode' => ($list->html5Code != '' ? $list->html5Code : $list->code) ] )}}', 'plt_slot', 'width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif" >
								<img src="{{asset('/royalewin/img/plt/'.($list->code != '' ? $list->code : $list->html5Code).'.jpg')}}" alt=""/>

								@if (Lang::getLocale() == 'cn')
									<span>{{$list->gameNameCn}}</span>
								@else
									<span>{{$list->gameName}}</span>
								@endif
							</a>
						</div>
					</div>
					@endif
				@endforeach
			</div>
		@endif
    </div>
@endsection
