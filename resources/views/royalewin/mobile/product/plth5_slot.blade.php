@extends('royalewin/mobile/master')

@if (Session::get('currency') == 'MYR')
    @section('title', 'Online Slot Malaysia - PLT Slot Games')
    @section('keywords', 'Online Slot Malaysia - PLT Slot Games')
    @section('description', 'Play PLT Online Slots at RoyaleWin Online Casino Malaysia and claim a FREE SPIN bonus today!')
@elseif (Session::get('currency') == 'IDR')
    @section('title', 'Situs Judi PLT Online Slot Indonesia')
    @section('keywords', 'Situs Judi PLT Online Slot Indonesia')
    @section('description', 'RoyaleWin Agen Judi Slot Online menyediakan permainan PLT slots dan taruhan online terpercaya dan terbaik di Indonesia.')
@endif

@section('css')
    @parent

    <style type="text/css">
        .slot_box {
            margin-bottom: 25px;
            max-width: 195px;
            min-height: 147px;
            border: 1px solid #ffae00;
        }

        .slot_box img {
            color: #ffae00;
            font-weight: bold;
            width: 100%;
            height: auto;
        }

        .slot_box span {
            color: #ffae00;
            font-weight: bold;
            line-height: 28px;
        }
    </style>
@endsection

@section('js')
    @parent

    <script type="text/javascript">
        $(function () {
            resizeSlotBox();

            window.addEventListener("resize", resizeSlotBox);
        });

        function resizeSlotBox() {
            var maxSlotBoxHeight = 0;
            var boxHeight = 0;

            $(".slot_box ").each(function () {
                boxHeight = parseFloat($(this).css("height").replace("px", ""));

                if (boxHeight > maxSlotBoxHeight) {
                    maxSlotBoxHeight = boxHeight;
                }
            });

            $(".slot_box").css("height", maxSlotBoxHeight + "px");
        }
    </script>
@endsection

@section('content')
    <!-- Page Content -->
    <div class="container">

        {{-- Only Slots are playable in mobile. --}}

        <div class="ht1"></div>

        <div class="row">
            @foreach( $lists as $list )
                <div class="col-lg-3 col-md-4 col-xs-6 text-center">
                    <div class="slot_box">
                        <a href="javascript:void(0)" onclick="@if (Auth::user()->check()) window.open('{{route('playtech-h5-slot' , [ 'gameid' => $list['code'] ] )}}', 'plt_slot', 'width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif" >
                            <img src="{{url()}}/front/img/plt/{{$list->code}}.jpg" alt="" onerror="this.onerror=null;this.src='{{url('/')}}/front/img/plt/notimage.png';" />
                            <span>{{$list['name']}}</span>
                        </a>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection
