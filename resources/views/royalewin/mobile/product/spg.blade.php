@extends('royalewin/mobile/master')

@section('title', 'Slot')

@section('css')
    @parent

    <style type="text/css">
        .slot_box {
            margin-bottom: 25px;
            max-width: 195px;
            min-height: 147px;
            border: 1px solid #ffae00;
        }

        .slot_box img {
            color: #ffae00;
            font-weight: bold;
            width: 100%;
            height: auto;
        }

        .slot_box span {
            color: #ffae00;
            font-weight: bold;
            line-height: 28px;
        }
    </style>
@endsection

@section('js')
    @parent

    <script type="text/javascript">
        $(function () {
            resizeSlotBox();

            window.addEventListener("resize", resizeSlotBox);
        });

        function resizeSlotBox() {
            var maxSlotBoxHeight = 0;
            var boxHeight = 0;

            $(".slot_box ").each(function () {
                boxHeight = parseFloat($(this).css("height").replace("px", ""));

                if (boxHeight > maxSlotBoxHeight) {
                    maxSlotBoxHeight = boxHeight;
                }
            });

            $(".slot_box").css("height", maxSlotBoxHeight + "px");
        }
    </script>
@endsection

@section('content')
    <!-- Page Content -->
    <div class="container">

        <div class="ht1"></div>

        <!--LOBBY MENU-->
        <div class="row">
            <div class="col-md-1 col-xs-6">
                <a class="category-btn" href="{{route('spg', [ 'type' => 'slot' , 'category' => 'slot' ] )}}">
                    <div>Slots</div>
                </a>
            </div>

            <div class="col-md-1 col-xs-6">
                <a class="category-btn" href="{{route('spg', [ 'type' => 'slot' , 'category' => 'progressive' ] )}}">
                    <div>Progressive</div>
                </a>
            </div>
        </div>
        <!--LOBBY MENU-->

        <div class="ht1"></div>

        <div class="row">
            @foreach( $lists as $list )
                <div class="col-lg-3 col-md-4 col-xs-6 text-center">
                    <div class="slot_box">
                        <a href="javascript:void(0)" onclick="@if (Auth::user()->check())window.open('{{route('spg-slot' , [ 'gameid' => $list['gamecode'] ] )}}', 'spg_slot', 'width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif" >
                            <img src="{{url()}}/royalewin/img/spg/{{$list->gamecode}}.jpg" alt=""/>
                            <span>{{$list['gamename_en']}}</span>
                        </a>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection
