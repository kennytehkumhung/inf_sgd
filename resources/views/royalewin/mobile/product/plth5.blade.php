<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<meta content="text/html; charset=UTF-8" http-equiv="Content-Type" />
<meta content="true" name="HandheldFriendly" />
<meta content="width=device-width; initial-scale=1; maximum-scale=1; minimum-scale=1" name="viewport" />
<title>HTML5 Login</title>
		
<script type="text/javascript" src="js/jquery-2.1.1.min.js"></script>
@if(Session::get('currency')=='MYR')
<script type="text/javascript" src="https://login.winforfun88.com/jswrapper/integration.js.php?casino=winforfun88"></script>
@elseif(Session::get('currency')=='THB')
<script type="text/javascript" src="https://login.winforfun88.com/jswrapper/integration.js.php?casino=winforfun88"></script>

@elseif(Session::get('currency')=='IDR')

@else
<script type="text/javascript" src="https://login.winforfun88.com/jswrapper/integration.js.php?casino=winforfun88"></script>

@endif 
<script type="text/javascript">
        var mobiledomain = "ld176988.com";
		var systemidvar = "424";
        gametype ="{{$code}}";
		var realMode = 1;
    </script>
	
<script type="text/javascript">

        iapiSetCallout('Login', calloutLogin);
        iapiSetCallout('GetTemporaryAuthenticationToken', calloutGetTemporaryAuthenticationToken);
        function login() {
            iapiSetClientPlatform("mobile&deliveryPlatform=HTML5");
            iapiLogin("{{$username}}",'{{Crypt::decrypt( Session::get('enpassword') )}}', realMode, "en");
        }
        function calloutLogin(response) {
            if (response.errorCode)
                alert("Login failed. " + response.playerMessage + " Error code: " + response.errorCode);
            else
                iapiRequestTemporaryToken(1, systemidvar, 'GamePlay');
        }
        function calloutGetTemporaryAuthenticationToken(response) {
            if (response.errorCode)
                alert("Token failed. " + response.playerMessage + " Error code: " + response.errorCode);
            else
                launchMobileClient(response.sessionToken.sessionToken);
        }
        function launchMobileClient(temptoken) {
            var clientUrl = 'http://hub.' + mobiledomain + '/igaming/' + '?gameId=' + gametype + '&real=1' + '&username={{$username}}&lang=en&tempToken=' + temptoken + '&lobby=' + location.href.substring(0, location.href.lastIndexOf('/') + 1) + 'lobby.html' + '&support=' + location.href.substring(0, location.href.lastIndexOf('/') + 1) + 'support.html' + '&logout=' + location.href.substring(0, location.href.lastIndexOf('/') + 1) + 'logout.html' + '&deposit=' + location.href.substring(0, location.href.lastIndexOf('/') + 1) + 'deposit.html';
            document.location = clientUrl;
        }
		function loading(){
			login();
			iapiSetCallout('Login', calloutLogin);
			iapiSetCallout('GetTemporaryAuthenticationToken', calloutGetTemporaryAuthenticationToken);
		}
    </script>


<style type="text/css">
		


body {
overflow: hidden!important;  
  background-color: #1f2323;
  margin: 0;
  padding:0;
  width:100%;
  height:100%;
  font-family: 'Helvetica Neue', Helvetica, sans;
  line-height:140%;
  font-size: 0.9em;
  background-image: -moz-radial-gradient(#3d5958, #1e2121);
  background-image: -webkit-gradient(radial,center center,0,center center,320,from(#3d5958),to(#1e2121));
}
a {
  color: #3d87d2;
}
.radial {
  padding:16px;
}
.inner {
  margin: 0 auto ;
  max-width: 350px;
  padding:5px;
  background-color:#f6f7f9;
  border-radius: 4px;
  border-radius: 4px;
  -webkit-border-radius: 4px;
  -moz-border-radius: 4px;
  color: #6b6b6b;
  text-shadow: #ffffff 0px 1px 0px;
  text-align: middle;
  vertical-align: middle;
  float: center;
  
}
.button {
  width: 90%;
  min-width:8em;
  text-decoration:none;
  display:block;
  font-size:1.5em;
  font-weight:bold;
  letter-spacing:-1px;
  text-shadow: #2a821c 0px 1px 0px;
  -moz-box-shadow: 0px 1px 2px #666;
  -webkit-box-shadow: 0px 1px 2px #666;
  box-shadow: 0px 1px 2px #666;
  color:#ffffff;
  background-color: #209d13;
  background-image: url('images/m-button-background.png');
  background-repeat: repeat-x;
  background-position: top;
  text-align:center;
  padding:0.8em 0.2em;
  margin:0.8em auto;
  border-radius: 4px;
  -webkit-border-radius: 4px;
  -moz-border-radius: 4px;
  background-image: -moz-linear-gradient(-90deg, #65d549, #1f9c13);
  background-image: -webkit-gradient(linear, center top, center bottom, from(#65d549), to(#1f9c13));
}
.button span {
  background-image:url('images/m-underline.png');
  background-repeat:repeat-x;
  background-position:0 bottom;
}
.button:hover {
  background-color: #40b734;
  background-image: url('images/m-button-background-hover.png');
  background-image: -moz-linear-gradient(-90deg, #72e359, #40b734);
  background-image: -webkit-gradient(linear, center top, center bottom, from(#72e359), to(#40b734));
  //border: 1px #63d347 solid;
}


.version {
  font-size:0.8em;
  color: #acacac;
}
.inner2 {
  margin: 0 auto;
  padding: 5px 15px;
  max-width: 300px;
  color: #f6f7f9;
  text-shadow: #212f2e 0px -1px 0px;
}

h2, h3 {
  margin-top:1.2em;
  font-weight:normal;
}

ul {
  margin:0;
  padding: 0px 0 0 18px;
}
ul li {
  list-style-type:none;
  text-indent:-18px;
  padding-bottom: 3px;
  color: #b8b9b9;
}
ul li:before {
  content:"\2016  ";
  display:inline;
}

a.plain-text-link {
  color:inherit;
  text-decoration:none;
}
a.plain-text-link:hover {
  color:inherit;
  text-decoration:none;
  cursor:pointer;
}
a.plain-text-link:active {
  color:#3d87d2;
  text-decoration:none;
  cursor:pointer;
}
a.plain-text-link:visited {
  color:inherit;
  text-decoration:none;
}
#superform{
height:200px;
padding:0px;
}
#loadingImage{
height:200px;
padding:0px;
}
</style>

</head>

<body>
<br>
<div class="inner">
	<div class="message" style="text-align:center">
	<img border="0" src="images/logo.png">
	</div>
	<div id="superform">
		<form id="loginform" name="loginform">
		<table border="0" cellpadding="10" cellspacing="0" width="250" align="center">
		<tr class="tableheader">
		<td align="center" colspan="2"></td>
		</tr>
		<tr class="tablerow">
		<td align="right" class="inner"><b>USERNAME</b></td>
		<td><input type="text" name="username" id="username" class="inner" style="width: 218px;" value="{{$username}}"></td>
		</tr>
		<tr class="tablerow">
		<td align="right" class="inner"><b>PASSWORD</b></td>
		<td><input type="password" name="password" id="password" class="inner" style="width: 218px;" value="{{Crypt::decrypt( Session::get('enpassword') )}}"></td>
		</tr>
		</table>
		</form>
		
		<a href="#" onClick="loading()" class="button" id="loginbutton"> login</a>
	</div>
	<div id="loadingimage" class="inner" style="display:none;">	
		<center>
		<table border="0" cellpadding="10" height="100%" cellspacing="0" width="250" align="center">
		<td align="center"><img src="images/loading.gif"></td>
		</table>
		</center>
	</div>
</div>
</body>
<script>
loading(); 
</script>
</html>