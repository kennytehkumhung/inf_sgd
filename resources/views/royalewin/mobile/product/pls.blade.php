@extends('royalewin/mobile/master')

@section('title', 'Slot')

@section('css')
    @parent

    <style type="text/css">
        .slot_box {
            margin-bottom: 25px;
            max-width: 195px;
            min-height: 147px;
            border: 1px solid #ffae00;
        }

        .slot_box img {
            color: #ffae00;
            font-weight: bold;
            width: 100%;
            height: auto;
        }

        .slot_box span {
            color: #ffae00;
            font-weight: bold;
            line-height: 28px;
        }
    </style>
@endsection

@section('js')
    @parent

    <script type="text/javascript">
        $(function () {
            resizeSlotBox();

            window.addEventListener("resize", resizeSlotBox);
        });

        function resizeSlotBox() {
            var maxSlotBoxHeight = 0;
            var boxHeight = 0;

            $(".slot_box ").each(function () {
                boxHeight = parseFloat($(this).css("height").replace("px", ""));

                if (boxHeight > maxSlotBoxHeight) {
                    maxSlotBoxHeight = boxHeight;
                }
            });

            $(".slot_box").css("height", maxSlotBoxHeight + "px");
        }
    </script>
@endsection

@section('content')
    <!-- Page Content -->
    <div class="container">

        <div class="ht1"></div>

        <!--LOBBY MENU-->
        <div class="row">
            <div class="col-md-1 col-xs-6">
                <a class="category-btn" href="javascript:void(0);">
                    <div>Slot</div>
                </a>
            </div>
        </div>
        <!--LOBBY MENU-->

        <div class="ht1"></div>

        <div class="row">
            @if (count($lists) > 0)
                <?php $gameCount = 0; ?>
                @foreach( $lists as $key => $list )

                    <div class="col-lg-3 col-md-4 col-xs-6 text-center">
                        <div class="slot_box">
                            <a href="javascript:void(0)" onclick="@if (Auth::user()->check())window.open('{{Config::get(Session::get('currency').'.pls.game_url')}}?host_id={{Config::get(Session::get('currency').'.pls.host_id')}}&game_id={{$list->game_id}}&lang=en-US&access_token={{$token}}&return_url={{Request::url()}}', 'plscasino', 'width=1150,height=830');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif">
                                <img src="{{url()}}/royalewin/img/playstar/{{$list->game_id}}.jpg" alt=""/>
                            </a>
                            <span>{{ Lang::get('public.PlayNow') }}</span>
                        </div>
                    </div>

                    <?php
                    if ($gameCount > 23) {
                        break;
                    }

                    $gameCount++;
                    ?>
                @endforeach
            @else
                <iframe id="iframe_game" frameborder="0" style="overflow: hidden; overflow-x: hidden; overflow-y: hidden; height: 98%; min-height: 600px; width: 100%;" src="{{ route('maintenance_slot') }}"></iframe>
            @endif
        </div>
    </div>
@endsection
