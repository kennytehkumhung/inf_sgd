<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Royalewin Malaysia Slot</title>

    <!-- Bootstrap Core CSS -->
    <link href="//myroyalewin.net/royalewin/mobile/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="//myroyalewin.net/royalewin/mobile/css/modern-business.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="//myroyalewin.net/royalewin/mobile/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

        

    <style type="text/css">
        .slot_box {
            margin-bottom: 25px;
            max-width: 195px;
            min-height: 147px;
            border: 1px solid #ffae00;
        }

        .slot_box img {
            color: #ffae00;
            font-weight: bold;
            width: 100%;
            height: auto;
        }

        .slot_box span {
            color: #ffae00;
            font-weight: bold;
            line-height: 28px;
        }
    </style>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>



    <!-- Page Content -->
    <div class="container">

        <div class="ht1"></div>

        <!--LOBBY MENU-->
        <div class="row">
            <div class="col-md-1 col-xs-6">
                <a class="category-btn" href="javascript:void(0);">
                    <div>Slot</div>
                </a>
            </div>
        </div>
        <!--LOBBY MENU-->

        <div class="ht1"></div>

        <div class="row">
                        
                    <?php $gameCount = 0; ?>
            @foreach( $lists as $key => $list )

                <div class="col-lg-3 col-md-4 col-xs-6 text-center">
                    <div class="slot_box">
                        <a href="javascript:void(0)" onclick="window.open('{{Config::get('MYR.pls.game_url')}}?host_id={{Config::get('MYR.pls.host_id')}}&game_id={{$list->game_id}}&lang=en-US&access_token={{$token}}&return_url={{Request::url()}}', 'plscasino', 'width=1150,height=830');">
                            <img src="{{url()}}/royalewin/img/playstar/{{$list->game_id}}.jpg" alt=""/>
                        </a>
                        <span>{{ Lang::get('public.PlayNow') }}</span>
                    </div>
                </div>

                <?php
                if ($gameCount > 23) {
                    break;
                }

                $gameCount++;
                ?>
            @endforeach

                        </div>
    </div>
<!-- /.row -->


<!-- jQuery -->
<script src="//myroyalewin.net/royalewin/mobile/js/jquery-1.11.3.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="//myroyalewin.net/royalewin/mobile/js/bootstrap.min.js"></script>

<script type="text/javascript">
    
    function setSwitchWebTypeBackUrl(url) {
        var obj = $("#dSwitchBtn");
        var href = obj.attr("href");
        var sym = "?";

        if (href.indexOf("?") > -1) {
            sym = "&";
        }

        obj.attr("href", href + sym + "to=" + encodeURIComponent(url));
    }

    function livechat() {
        window.open('https://chatserver.comm100.com/ChatWindow.aspx?planId=360&visitType=1&byHref=1&partnerId=-1&siteid=160856', '', 'width=525, height=520, top=150, left=250');
    }
</script>

    

    <script type="text/javascript">
        $(function () {
            resizeSlotBox();

            window.addEventListener("resize", resizeSlotBox);
        });

        function resizeSlotBox() {
            var maxSlotBoxHeight = 0;
            var boxHeight = 0;

            $(".slot_box ").each(function () {
                boxHeight = parseFloat($(this).css("height").replace("px", ""));

                if (boxHeight > maxSlotBoxHeight) {
                    maxSlotBoxHeight = boxHeight;
                }
            });

            $(".slot_box").css("height", maxSlotBoxHeight + "px");
        }
    </script>



</body>
</html>
