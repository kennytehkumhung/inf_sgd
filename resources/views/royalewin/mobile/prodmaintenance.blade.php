<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
        <link href="" rel="shortcut icon" type="image/icon" />
        <link href="" rel="icon" type="image/icon" />
        <title>Malaysia Largest Online Casino, Royalewin</title>
        <!--[if IE 7]>
            <link rel="stylesheet" type="text/css" href="{{ asset('/royalewin/resources/css/iefix.css') }}" />
        <![endif]-->
        <!--CSS -->

        <link href="{{ asset('/royalewin/resources/css/style.css') }}" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <!--Mid Sect-->
        <div class="midSect" style="background: none;">
            <div class="outer">
                <div class="msgBox1">
                    <div class="excl">
                    <img src="{{ asset('/royalewin/img/under-maintenance.png') }}" alt=""/></div>
                    <span class="mnt">Website currently under maintenance</span>
                    <span class="mnt1">Come Back Again Soon</span>
                    <div class="clr"></div>
                </div>
            </div>
        </div>
    </body>
</html>
