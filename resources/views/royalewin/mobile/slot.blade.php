@extends('royalewin/mobile/master')

@section('title', 'Slot')

@section('js')
    @parent

    <script type="text/javascript">

        var game = "{{ request()->type }}";
        var gameUrls = {
            mxb: "{{route('mxb' , [ 'type' => 'slot' , 'category' => 'Slots' ] )}}",
            opu: "{{route('opu', [ 'type' => 'slot' , 'category' => 'Arcades' ] )}}",
            plt: "{{route('plt' , [ 'type' => 'pgames' , 'category' => '1' ] )}}",
            a1a: "{{route('a1a')}}",
            spg: "{{route('spg', [ 'type' => 'slot' , 'category' => 'slot' ] )}}"
        };

        if (game != '' && gameUrls.hasOwnProperty(game)) {
            window.location.href = gameUrls[game];
        }
    </script>
@endsection
