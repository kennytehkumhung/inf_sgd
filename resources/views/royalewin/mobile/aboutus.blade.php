@extends('royalewin/mobile/master')

@if (Session::get('currency') == 'MYR')
    @section('title', 'Best Online Live Casino Malaysia - About Us')
    @section('keywords', 'Best Online Live Casino Malaysia - About Us')
    @section('description', 'Learn about RoyaleWin Malaysia\'s Online Casino, a multiple award-winning online casino in Malaysia that gives you all the thrills you could possibly want from gaming online.')
@elseif (Session::get('currency') == 'IDR')
    @section('title', 'Bandar Casino Online, Situs Judi Indonesia - Tentang Kami')
    @section('keywords', 'Bandar Casino Online, Situs Judi Indonesia - Tentang Kami')
    @section('description', 'Tentang Kami - RoyaleWin adalah agen bola, bandar casino online dan situs judi online terpercaya untuk Anda penggemar taruhan bola dan pecinta judi online di Indonesia.')
@endif

@section('css')
    @parent

    <style type="text/css">
        .content span {
            color: #ffcc01;
            display: block;
            font-size: 12px;
            font-weight: bold;
            height: auto;
            margin: 15px auto 0;
            padding: 0;
            position: relative;
            text-align: left;
            text-transform: uppercase;
        }

        .content p {
            text-align: justify;
        }
    </style>
@endsection

@section('content')
    <!-- Page Content -->
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-xs-12 contOthers">
                <h4>{{ strtoupper(Lang::get('public.AboutUs')) }}</h4>
            </div>
            <div class="col-md-12 col-xs-12 contOthers content">
                {!! htmlspecialchars_decode($content) !!}
            </div>
        </div>
    </div>
@endsection
