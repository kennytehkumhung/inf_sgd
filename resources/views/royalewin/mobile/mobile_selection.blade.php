@extends('royalewin/mobile/master')

@section('title', 'Mobile')

@section('js')
    @parent

    <style type="text/css">
.mobSelect {
    display: block;
    height: auto;
    margin-top: 20px;
    padding: 20px;
    width: 100%;
}
.mobSelect ul {
    margin: 0;
    padding: 0;
}
.mobSelect ul li {
    list-style-type: none;
    margin: 0;
    padding: 0;
}
.mobSelect ul li a:link, .mobSelect ul li a:visited {
    background: rgba(0, 0, 0, 0) linear-gradient(180deg, rgb(220, 209, 5) 30%, rgb(143, 116, 18) 70%) repeat scroll 0 0;
    border-radius: 6px;
    color: #000;
    display: block;
    font-size: 14px;
    font-weight: bold;
    height: 30px;
    line-height: 30px;
    margin: 0 0 20px;
    text-align: center;
    text-decoration: none;
    width: 100%;
}
.mobSelect ul li a:hover {
    background: rgba(0, 0, 0, 0) linear-gradient(0deg, rgb(220, 209, 5) 30%, rgb(143, 116, 18) 70%) repeat scroll 0 0;
    text-decoration: none;
}
    </style>
@endsection

@section('js')
    @parent

    <script type="text/javascript">
        //
    </script>
@endsection

@section('content')
  <div class="row">
    <div class="col-md-offset-3 col-sm-offset-3 col-sm-6 col-md-6 col-xs-12">
    <div class="mobSelect">
    <ul>
    <li><a href="{{$html5}}" >Play Now</a></li>
    <li><a href="{{$apk}}">Download</a></li>
    </ul>
    </div>
    </div>
 </div>

@endsection

@section('modal')

@endsection
