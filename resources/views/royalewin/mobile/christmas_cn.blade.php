<!doctype html>﻿
<html>

<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="theme-color" content="#af0000">

	<title>Royalewin</title>

	<link rel="stylesheet" type="text/css" href="{{ static_asset('/xmas/cn/css/tpl.css') }}">

	<meta name="description" content="">

	<!-- Favicons -->
	<link rel="shortcut icon" href="{{ static_asset('/xmas/cn/favicon.ico') }}" type="image/icon" />


	<!-- CSS minified -->
	<link rel="stylesheet" href="{{ static_asset('/xmas/cn/css/main.css') }}">
	<link rel="stylesheet" href="{{ static_asset('/xmas/cn/css/reg.css') }}">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp"
	 crossorigin="anonymous">


	<!-- JS -->
	<script src="{{ static_asset('/xmas/en/js/main.js') }}"></script>
        <script type="text/javascript">
		function register_submit() {
                        $("#register_btn").hide();
                        $("#loading").show();
			var dob = '0000-00-00';
			if (typeof $('#dob_year').val() != undefined) {
				dob = $('#dob_year').val() + '-' + $('#dob_month').val() + '-' + $('#dob_day').val();
			}
			$.ajax({
				type: "POST",
				url: "{{route('register_process')}}",
				data: {
					_token:     "{{ csrf_token() }}",
					username:	$('#r_username').val(),
					password:	$('#r_password').val(),
					repeatpassword: $('#r_repeatpassword').val(),
					code:		$('#r_code').val(),
					email:		$('#r_email').val(),
					mobile:		$('#r_mobile').val(),
					fullname:	$('#r_fullname').val(),
					dob:		dob
				},
			}).done(function(json) {
				$('.acctTextReg').html('');
				obj = JSON.parse(json);
				var str = '';
				$.each(obj, function(i, item) {
					if ('{{Lang::get('COMMON.SUCESSFUL')}}' == item) {
						alert('{{Lang::get('public.NewMember')}}');
						window.location.href = "{{route('homepage')}}";
					} else {
						$('.' + i + '_acctTextReg').html('<font style="color:red">' + item + '</font>');
                                                $("#register_btn").show();
                                                $("#loading").hide();
					}
				});
			});
		}
                
                function enterpressalert(e, textarea){
                    var code = (e.keyCode ? e.keyCode : e.which);
                    if(code == 13) { //Enter keycode
                        register_submit();
                    }
                }
	</script>


</head>

<body lang="sc" class="sc">
	<input type="hidden" name="tpl_url_hidden" id="tpl-url-hidden" value="https://www.geiqianle.com/hf-tpl">
	<input type="hidden" name="tpl_mobile_responsive" id="tpl-mobile-responsive" value="1">

	<header class="tpl-header tpl-show-lang-text tpl-cf tpl-header5">
		<div class="tpl-inner tpl-cf">

			<div class="tpl-back-btn tpl-box">
				<a href="//m.rwbola.com/my/promotion" target="_blank" id="tpl-back-button">
					<img src="{{ static_asset('/xmas/cn/img/back.png') }}">
				</a>
			</div>

			<div class="tpl-logo-wrap tpl-box">
				<a href="#" class="tpl-desk tpl-convert mobi-on" id="tpl-logo-mobi" target="_blank" title="ROYALEWIN">
					<img src="{{ static_asset('/xmas/cn/img/logo_my_xmas.gif') }}">
				</a>
			</div>

			<div class="tpl-download-faq tpl-box">
				<a href="//m.rwbola.com/my/" target="_blank" id="tpl-download-btn-mobi" class="tpl-convert mobi-on">
					<img src="{{ static_asset('/xmas/cn/img/download.png') }}">
				</a>
				<a href="{{ route( 'language', [ 'lang'=> 'en']) }}" class="tpl-convert mobi-on">
					<img src="{{ static_asset('/xmas/cn/img/en.png') }}">
				</a>
			</div>

			<div class="tpl-right-nav">
				<div class="tpl-inner-right-nav tpl-cf">
					<a href="//www.rwbola.com/my" class="tpl-convert mobi-on" id="tpl-download-mobi" target="_blank">
						<div class="tpl-download-btn">
							回到主页 </div>
					</a>

					<div class="tpl-lang-wrap">
						<div class="tpl-lang-trigger tpl-cf">
							<span class="tpl-sprt sc"></span>
							<span class="tpl-txt-lang">中文</span>
							<span class="tpl-sprt tpl-arw-lang"></span>
						</div>

						<ul class="tpl-lang-select tpl-cf">
							<li class="tpl-lang" id="tpl-lang-en">
								<a href="{{ route( 'language', [ 'lang'=> 'en']) }}" id="tpl-en">
									<span class="tpl-sprt en"></span> <span class="tpl-txt-lang">English</span>
									<span class="tpl-sprt"></span>
								</a>
							</li>

						</ul>
					</div>

					<a href="//www.rwbola.com/my/register" target="_blank" id="tpl-join-now">
						<div class="tpl-join-now">
							马上注册 </div>
					</a>
				</div>
			</div>

		</div>
	</header>
	<div class="music-wrap">
		<iframe id="xmasbg" src="{{ static_asset('/xmas/cn/xmasbg.mp3') }}" allow="autoplay" sandbox="allow-same-origin allow-scripts"></iframe>
		<img id="volume-on-trig" class="volume-on" src="{{ static_asset('/xmas/cn/img/volume-on.png') }}">
		<img id="volume-off-trig" class="volume-off" src="{{ static_asset('/xmas/cn/img/volume-off.png') }}">
	</div>
	<input type="hidden" id="musictrig" name="">
	<div class="main">
		<div class="main-wrapper">
			<div class="content-left">
				<div class="logo-wrap">
					<img src="{{ static_asset('/xmas/cn/img/logo-sc.png') }}">
				</div>
				<div class="subtitle-wrap">
					<h1>圣诞奖金高达 788 令吉！</h1>
					<p>
						赶快加入活动吧！ <br>
						<span>开始于2018年12月24日</span><br>
						<span>截止于 2019年1月1日</span><br>
					</p>
				</div>
				<div class="gift-wrap">
					<img class="gift-item gift-car" src="{{ static_asset('/xmas/cn/img/car.png') }}">
				</div>
				<div class="qr-wrapper">
					<div class="qr-phone">
						<div class="qr-over"></div>
						<img class="qr-code" src="{{ static_asset('/xmas/cn/img/qr-sc.png') }}">
					</div>
					<div class="qr-info">
						<div class="qr-arrow"></div>
						<p>更多细节请访问我们的网页<br>
							<span class="yellow">www.rwbola.com</span></p>
					</div>
				</div>
			</div>
			<div class="content-right">
				<div id="main-boxes">
					<div class="box-container box1 active" id="box1" tab-ref="1">
						<div class="box-header">
							<h2>圣诞奖金</h2>
						</div>
						<div class="box-content" style="overflow: hidden; padding: 0px; width: 398px;">

							<div class="jspPane" style="padding: 0px; top: 0px; left: 0px; width: 391px;"><br><br><a id="sign-up-now-mobi"
								 class="cta-btn tpl-convert mobi-on" target="_blank" href="//www.rwbola.com/my/register">
									立即注册 </a>
								<div class="jspContainer" style="width: 390px; height: 530px;  margin:35px auto;">



									<ol>
										<li>
											<p>此优惠适用于所有使用马币令吉 (MYR) 的豪赢会员。</p>

										</li>
										<li>
											<p>玩家须在活动期内达到任一最低存款要求。</p>
										</li>
										<li>
											<p>
												符合要求的玩家可在该活动周 (GMT+8) 获得相应的奖金仅一次。</p>
										</li>
										<li>
											<p>

												此优惠不能与其他存款红利同时享有。</p>
										</li>
										<li>

											奖金须符合10倍投注要求，在线娱乐场所有游戏的投注都将被计为有效投注。。</p>
										</li>
									</ol>
									<h3 class="center hiw-title">
										如何参与 </h3>

									<p>活动周内达到任一最低存款要求。</p>
									<table class="center">
										<tbody>
											<tr>
												<th> 礼包 </th>

												<th> 最低存款要求 </th>

												<th> 奖金 </th>

											</tr>
											<tr>

												<td> 经典欢迎礼包 </td>


												<td> MYR300 </td>


												<td>
													38% 高达 MYR 208</td>

											</tr>
											<tr>

												<td> 特别欢迎礼包<br>(限时活动) </td>


												<td> MYR2000</td>


												<td> 22% 高达 MYR 788</td>

											</tr>
										</tbody>
									</table>


									<p>* 玩家需在活动周 (GMT+8) 联系客服来领取奖金。</p>


								</div>
							</div>
						</div>
					</div>
					<div class="box-container box2 inactive" id="box2" tab-ref="2">
						<div class="box-header">
							<h2>注册</h2>
						</div>
						<div class="box-content daily-boxes">
							<div class="bx-wrapper" style="max-width: 100%;">
								<div class="bx-viewport" aria-live="polite" style="width: 100%; overflow: hidden; position: relative; height: 509px;">

									<div class="form">
										<div class="rowed1">
											<label>全名*:</label>
											<input type="text" id="r_fullname"><span class="red fullname_acctTextReg acctTextReg"></span>
										</div>
										<div class="rowed1">
											<label>电话号码* :</label>
											<input type="text" id="r_mobile"><span class="red mobile_acctTextReg acctTextReg"></span>
										</div>
										<div class="rowed1">
											<label>电邮地址* :</label>
											<input type="text" id="r_email"><span class="red email_acctTextReg acctTextReg"></span>
										</div>
										

										<div class="rowed1">
											<label>用户名*:</label>
											<input type="text" id="r_username"><span class="red username_acctTextReg acctTextReg"></span>
										</div>

										<div class="rowed1">
											<label>密码*:</label>
											<input type="password" id="r_password"><span class="red password_acctTextReg acctTextReg"></span>
										</div>

										<div class="rowed1">
											<label>确认密码:</label>
											<input type="password" id="r_repeatpassword">
										</div>

									

										<div class="rowed1">
											<label >验证码:</label>
											<input type="text" id="r_code" onKeyPress="enterpressalert(event, this)" class="coded">
											<img src="{{route('captcha', ['type' => 'register_captcha'])}}" width="60" height="27" class="coded blacked">
										</div>
                                                                                <div class="rowed1" id="register_btn"><a href="javascript:void(0);" onclick="register_submit();">立即注册</a></div>
                                                                                <div class="rowed1" id="loading" style="display:none;"><a href="#">装载中...</a></div>
									</div>
<div class="rowed1">
											<div class="note">
												*所有的注册姓名必须与您的银行帐户姓名一致以作提款和存款验证。
											</div>
										</div>

								</div>
								<div class="bx-controls"></div>
							</div>
						</div>

					</div>

					<div class="box-container">
						<div class="box-header">
							<h2>豪赢优惠</h2>
							<a href="//www.rwbola.com/my/promotion"><img class="Mobimg" src="{{ static_asset('/xmas/cn/img/p1.png') }}" /></a>
							<p>
								<a href="//www.rwbola.com/my/promotion"><img class="Mobimg" src="{{ static_asset('/xmas/cn/img/p2.png') }}" /></a>
								<p>
									<a href="//www.rwbola.com/my/promotion"><img class="Mobimg" src="{{ static_asset('/xmas/cn/img/p3.png') }}" /></a>
									<p>
										<a href="//www.rwbola.com/my/promotion"><img class="Mobimg" src="{{ static_asset('/xmas/cn/img/p4.png') }}" /></a>
										<p>
											<a href="//www.rwbola.com/my/promotion"><img class="Mobimg" src="{{ static_asset('/xmas/cn/img/p5.png') }}" /></a>
											<p>
												<a href="//www.rwbola.com/my/promotion"><img class="Mobimg" src="{{ static_asset('/xmas/cn/img/p6.png') }}" /></a>
												<p>



						</div>

					</div>
					<div class="box-container" style="display:none;">
						<div class="box-header">
							<h2>豪赢优惠</h2>
							<a href="//www.rwbola.com/my/promotion"><img src="{{ static_asset('/xmas/cn/img/p1.png') }}" /></a>
							<p>
								<a href="//www.rwbola.com/my/promotion"><img src="{{ static_asset('/xmas/cn/img/p2.png') }}" /></a>
								<p>
									<a href="//www.rwbola.com/my/promotion"><img src="{{ static_asset('/xmas/cn/img/p3.png') }}" /></a>
									<p>
										<a href="//www.rwbola.com/my/promotion"><img src="{{ static_asset('/xmas/cn/img/p4.png') }}" /></a>
										<p>
											<a href="//www.rwbola.com/my/promotion"><img src="{{ static_asset('/xmas/cn/img/p5.png') }}" /></a>
											<p>
												<a href="//www.rwbola.com/my/promotion"><img src="{{ static_asset('/xmas/cn/img/p6.png') }}" /></a>
												<p>



						</div>
						<div id="daily-slider"></div>

					</div>


					<div class="box-container" style="display:none;">

						<div id="dafanews-slider"></div>

					</div>




					<div class="box-container box6 active" id="box6" tab-ref="6">
						<div class="box-header">
						</div>
						<div class="box-content " tabindex="0" style="overflow: hidden; padding: 0px; outline: none; width: 397px;">


							<div class="jspContainer" style="width: 397px; height: 837px;">
								<div class="jspPane" style="padding: 0px; top: 0px; left: 0px; width: 380px;">
									<h2 class="accordion yellow active">
										条款与条件
									</h2>
									<div class="panel show">
										<ol>
											<li>
												<p> 此优惠开始于2018年12月24日 00:00:00（GMT+8）截止于 2019年1月1日 23:59:59（GMT+8）。</p>
											</li>
											<li>
												<p>每个会员只能在活动周领取相应的奖金仅一次，尽可能选择最高数额的单笔存款来领取奖金以达到最大的效益。</p>
											</li>
											<li>
												<p>此优惠适用于所有豪赢娱乐城玩家。会员只允许在同一时间拥有一个豪赢活动帐户。 </p>
											</li>

											<li>
												<p> 流水要求: 会员必须使用红利有效下注至少10倍后才可申请提款。例如: </p>

												<ul>
													<li> 单笔存款 = 300 令吉 </li>
													<li> 免费红利 = 38% 红利 x 300 令吉 = 114 令吉 </li>
													<li>流水要求 = (114令吉红利) x 10倍 = 1,140令吉 </li>
												</ul>
											</li>
											<li>
												<p>任何平局、对赌、取消或无效的注单将不计入流水要求。</p>
											</li>
											<li>
												<p>存款和奖金不能用于 Playtech 真人赌场，捕鱼游戏和 Joker 捕鱼王2，SCR888/ 918KISS和4D。如果玩家下注这些游戏，豪赢将有权利取消所有的赢额。 </p>
											</li>
											<li>
												<p>如果红利在30天内未达到流水要求，我们将有权取消红利及其盈利。 </p>

											</li>
											<li>
												<p> 豪赢有权在任何时间取消任何会员或者团伙投注会员；我们保留唯一及绝对决定权。 </p>
											</li>


											<li>
												<p>豪赢一般条款与规则适用于此优惠。 </p>
											</li>
										</ol>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="btm-wrapper">
					<a id="back-to-promo" href="//www.rwbola.com/my/promotion" class="btm-item" target="_blank">
						<span>返回活动页</span>
					</a>
					<a id="gen-term" class="btm-item">
						<span>规则和条款</span>
					</a>
				</div>

				<div id="main-nav" class="main-navigation">
					<ul>
						<li tab-ref="1" class="active">
							<span class="nav-1 nav-item"></span>
							<span class="tooltip">圣诞奖金</span>
						</li>
						<li tab-ref="2" class="inactive">
							<span class="nav-2 nav-item"></span>
							<span class="tooltip">注册</span>
						</li>
						<li>
							<span class="nav-3 nav-item"></span>
							<span class="tooltip">豪赢优惠</span>
						</li>



						<div class="mob-bck-to-top">
							<span class="nav-6 nav-item"></span>
						</div>
					</ul>
				</div>
			</div>
		</div>
		<canvas id="canv" class="snow" width="411" height="823"></canvas>
	</div>

	<footer class="tpl-footer tpl-show-sponsors">
		<div class="tpl-inner tpl-cf">
			<section class="tpl-partners">
				<img class="tpl-desktop-sponsor" src="{{ static_asset('/xmas/cn/img/footer-vendor-my.png') }}">
			</section>

			<section class="tpl-contacts " id="company-social">
				<span class="tpl-toll">
					<a href="https://www.facebook.com/rwin888/"><i class="fab fa-facebook"></i></a></span>


				<a href="https://twitter.com/RoyaleWin"><i class="fab fa-twitter-square"></i></a>

				<a href="https://plus.google.com/+RoyaleWinMY"><i class="fab fa-google-plus-square"></i></a>

				<a href="https://www.instagram.com/royalewin/?hl=en"><i class="fab fa-instagram"></i></a>


				</span>
				</span>
			</section>
		</div>
		<div class="tpl-copyright">
			<div class="tpl-inner">
				©2018 ROYALEWIN All Right Reserved | 18+
			</div>
		</div>
	</footer>
        
        <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-41261240-1', 'auto');
        ga('send', 'pageview');

        </script>
        
	<script src="{{ static_asset('/xmas/cn/js/tpf.js') }}" type="text/javascript"></script>


</body>

</html>