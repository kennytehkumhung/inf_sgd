@extends('royalewin/mobile/master')

@if (Session::get('currency') == 'MYR')
    @section('title', 'Play Live 4D Malaysia Online Betting')
    @section('keywords', 'Play Live 4D Malaysia Online Betting')
    @section('description', 'Play Live 4D at RoyaleWin Online Casino Malaysia - awarded Best Online Casino and enjoy 110% Welcome Deposit Bonus. Join RoyaleWin casino online today.')
@endif

@section('css')
    @parent

    <style type="text/css">
        .betInfo * {
            color: #fff;
        }
    </style>
@endsection

@section('content')
    <!-- Page Content -->
    <div class="container">
        <div class="row">

            <div class="col-md-12 col-xs-12">
                <div class="ht1"></div>

                <a href="#" onClick="@if (Auth::user()->check())window.open('{{route('psb')}}', 'pubContent', 'width=1050,height=650');@else alert('{{Lang::get('COMMON.PLEASELOGIN')}}')@endif" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('betnow', '', '{{ asset("/royalewin/img/bet-now-hover.png") }}', 0)"><img src="{{ asset('/royalewin/img/bet-now.png') }}" name="betnow" width="225" height="81" border="0"></a>

                <div class="ht1"></div>

                <ul class="betInfo">
                    <li><a href="javascript:void(0);" data-toggle="modal" data-target="#lmRulesTable">RULES & REGULATIONS</a></li>
                    <li><a href="javascript:void(0);" data-toggle="modal" data-target="#lmPayoutTable">PAYOUT TABLE</a></li>
                </ul>
            </div>

            <!--magnum-->
            <div class="col-md-12 col-xs-12">
                <div class="row mt2">
                    <div class="col-lg-12 nopad">
                        <div class="col-lg-12 nopad">
                            <div class="wTit">
                                MAGNUM 4D ({{ $Magnum['date'] }})
                            </div>
                        </div>
                    </div>

                    <div class="ht1"></div>

                    <div class="col-md-12 col-xs-12">
                        <div class="col-md-12">
                            <div class="col-md-4 col-xs-4 nopad">
                                <span class="dTT">1st</span>
                                <div class="dtxt">{{ $Magnum['first'] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad bgGrey">
                                <span class="dTT">2nd</span>
                                <div class="dtxt">{{ $Magnum['second'] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad">
                                <span class="dTT">3rd</span>
                                <div class="dtxt">{{ $Magnum['third'] }}</div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 col-xs-12" style="text-align: center;">
                        <h5>Special</h5>
                    </div>

                    <div class="col-md-12 col-xs-12">
                        <div class="col-md-12">
                            <div class="col-md-4 col-xs-4 nopad">
                                <div class="dtxt">{{ $Magnum['special'][0] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad bgGrey">
                                <div class="dtxt">{{ $Magnum['special'][1] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad">
                                <div class="dtxt">{{ $Magnum['special'][2] }}</div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 col-xs-12">
                        <div class="col-md-12">
                            <div class="col-md-4 col-xs-4 nopad">
                                <div class="dtxt">{{ $Magnum['special'][3] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad bgGrey">
                                <div class="dtxt">{{ $Magnum['special'][4] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad">
                                <div class="dtxt">{{ $Magnum['special'][5] }}</div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 col-xs-12">
                        <div class="col-md-12">
                            <div class="col-md-4 col-xs-4 nopad">
                                <div class="dtxt">{{ $Magnum['special'][6] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad bgGrey">
                                <div class="dtxt">{{ $Magnum['special'][7] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad">
                                <div class="dtxt">{{ $Magnum['special'][8] }}</div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 col-xs-12">
                        <div class="col-md-12">
                            <div class="col-md-4 col-xs-4 nopad">
                                &nbsp;
                            </div>
                            <div class="col-md-4 col-xs-4 nopad bgGrey">
                                <div class="dtxt">{{ $Magnum['special'][9] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad">
                                &nbsp;
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 col-xs-12" style="text-align: center;">
                        <h5>Consolation</h5>
                    </div>

                    <div class="col-md-12 col-xs-12">
                        <div class="col-md-12">
                            <div class="col-md-4 col-xs-4 nopad">
                                <div class="dtxt">{{ $Magnum['consolation'][0] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad bgGrey">
                                <div class="dtxt">{{ $Magnum['consolation'][1] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad">
                                <div class="dtxt">{{ $Magnum['consolation'][2] }}</div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 col-xs-12">
                        <div class="col-md-12">
                            <div class="col-md-4 col-xs-4 nopad">
                                <div class="dtxt">{{ $Magnum['consolation'][3] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad bgGrey">
                                <div class="dtxt">{{ $Magnum['consolation'][4] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad">
                                <div class="dtxt">{{ $Magnum['consolation'][5] }}</div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 col-xs-12">
                        <div class="col-md-12">
                            <div class="col-md-4 col-xs-4 nopad">
                                <div class="dtxt">{{ $Magnum['consolation'][6] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad bgGrey">
                                <div class="dtxt">{{ $Magnum['consolation'][7] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad">
                                <div class="dtxt">{{ $Magnum['consolation'][8] }}</div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 col-xs-12">
                        <div class="col-md-12">
                            <div class="col-md-4 col-xs-4 nopad">
                                &nbsp;
                            </div>
                            <div class="col-md-4 col-xs-4 nopad bgGrey">
                                <div class="dtxt">{{ $Magnum['consolation'][9] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad">
                                &nbsp;
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end magnum-->

            <div class="ht1"></div>

            <!--damacai-->
            <div class="col-md-12 col-xs-12">
                <div class="row mt2">
                    <div class="col-lg-12 nopad">
                        <div class="col-lg-12 nopad">
                            <div class="wTit">
                                DAMACAI 1+3D ({{ $PMP['date'] }})
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 col-xs-12">
                        <div class="col-md-12">
                            <div class="col-md-4 col-xs-4 nopad">
                                <span class="dTT">1st</span>
                                <div class="dtxt">{{ $PMP['first'] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad bgGrey">
                                <span class="dTT">2nd</span>
                                <div class="dtxt">{{ $PMP['second'] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad">
                                <span class="dTT">3rd</span>
                                <div class="dtxt">{{ $PMP['third'] }}</div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 col-xs-12" style="text-align: center;">
                        <h5>Special</h5>
                    </div>

                    <div class="col-md-12 col-xs-12">
                        <div class="col-md-12">
                            <div class="col-md-4 col-xs-4 nopad">
                                <div class="dtxt">{{ $PMP['special'][0] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad bgGrey">
                                <div class="dtxt">{{ $PMP['special'][1] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad">
                                <div class="dtxt">{{ $PMP['special'][2] }}</div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 col-xs-12">
                        <div class="col-md-12">
                            <div class="col-md-4 col-xs-4 nopad">
                                <div class="dtxt">{{ $PMP['special'][3] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad bgGrey">
                                <div class="dtxt">{{ $PMP['special'][4] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad">
                                <div class="dtxt">{{ $PMP['special'][5] }}</div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 col-xs-12">
                        <div class="col-md-12">
                            <div class="col-md-4 col-xs-4 nopad">
                                <div class="dtxt">{{ $PMP['special'][6] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad bgGrey">
                                <div class="dtxt">{{ $PMP['special'][7] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad">
                                <div class="dtxt">{{ $PMP['special'][8] }}</div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 col-xs-12">
                        <div class="col-md-12">
                            <div class="col-md-4 col-xs-4 nopad">
                                &nbsp;
                            </div>
                            <div class="col-md-4 col-xs-4 nopad bgGrey">
                                <div class="dtxt">{{ $PMP['special'][9] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad">
                                &nbsp;
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 col-xs-12" style="text-align: center;">
                        <h5>Consolation</h5>
                    </div>

                    <div class="col-md-12 col-xs-12">
                        <div class="col-md-12">
                            <div class="col-md-4 col-xs-4 nopad">
                                <div class="dtxt">{{ $PMP['consolation'][0] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad bgGrey">
                                <div class="dtxt">{{ $PMP['consolation'][1] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad">
                                <div class="dtxt">{{ $PMP['consolation'][2] }}</div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 col-xs-12">
                        <div class="col-md-12">
                            <div class="col-md-4 col-xs-4 nopad">
                                <div class="dtxt">{{ $PMP['consolation'][3] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad bgGrey">
                                <div class="dtxt">{{ $PMP['consolation'][4] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad">
                                <div class="dtxt">{{ $PMP['consolation'][5] }}</div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 col-xs-12">
                        <div class="col-md-12">
                            <div class="col-md-4 col-xs-4 nopad">
                                <div class="dtxt">{{ $PMP['consolation'][6] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad bgGrey">
                                <div class="dtxt">{{ $PMP['consolation'][7] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad">
                                <div class="dtxt">{{ $PMP['consolation'][8] }}</div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 col-xs-12">
                        <div class="col-md-12">
                            <div class="col-md-4 col-xs-4 nopad">
                                &nbsp;
                            </div>
                            <div class="col-md-4 col-xs-4 nopad bgGrey">
                                <div class="dtxt">{{ $PMP['consolation'][9] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad">
                                &nbsp;
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end damacai-->

            <div class="ht1"></div>

            <!--toto-->
            <div class="col-md-12 col-xs-12">
                <div class="row mt2">
                    <div class="col-lg-12 nopad">
                        <div class="col-lg-12 nopad">
                            <div class="wTit">
                                TOTO 4D ({{ $Toto['date'] }})
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 col-xs-12">
                        <div class="col-md-12">
                            <div class="col-md-4 col-xs-4 nopad">
                                <span class="dTT">1st</span>
                                <div class="dtxt">{{ $Toto['first'] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad bgGrey">
                                <span class="dTT">2nd</span>
                                <div class="dtxt">{{ $Toto['second'] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad">
                                <span class="dTT">3rd</span>
                                <div class="dtxt">{{ $Toto['third'] }}</div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 col-xs-12" style="text-align: center;">
                        <h5>Special</h5>
                    </div>

                    <div class="col-md-12 col-xs-12">
                        <div class="col-md-12">
                            <div class="col-md-4 col-xs-4 nopad">
                                <div class="dtxt">{{ $Toto['special'][0] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad bgGrey">
                                <div class="dtxt">{{ $Toto['special'][1] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad">
                                <div class="dtxt">{{ $Toto['special'][2] }}</div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 col-xs-12">
                        <div class="col-md-12">
                            <div class="col-md-4 col-xs-4 nopad">
                                <div class="dtxt">{{ $Toto['special'][3] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad bgGrey">
                                <div class="dtxt">{{ $Toto['special'][4] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad">
                                <div class="dtxt">{{ $Toto['special'][5] }}</div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 col-xs-12">
                        <div class="col-md-12">
                            <div class="col-md-4 col-xs-4 nopad">
                                <div class="dtxt">{{ $Toto['special'][6] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad bgGrey">
                                <div class="dtxt">{{ $Toto['special'][7] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad">
                                <div class="dtxt">{{ $Toto['special'][8] }}</div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 col-xs-12">
                        <div class="col-md-12">
                            <div class="col-md-4 col-xs-4 nopad">
                                &nbsp;
                            </div>
                            <div class="col-md-4 col-xs-4 nopad bgGrey">
                                <div class="dtxt">{{ $Toto['special'][9] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad">
                                &nbsp;
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 col-xs-12" style="text-align: center;">
                        <h5>Consolation</h5>
                    </div>

                    <div class="col-md-12 col-xs-12">
                        <div class="col-md-12">
                            <div class="col-md-4 col-xs-4 nopad">
                                <div class="dtxt">{{ $Toto['consolation'][0] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad bgGrey">
                                <div class="dtxt">{{ $Toto['consolation'][1] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad">
                                <div class="dtxt">{{ $Toto['consolation'][2] }}</div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 col-xs-12">
                        <div class="col-md-12">
                            <div class="col-md-4 col-xs-4 nopad">
                                <div class="dtxt">{{ $Toto['consolation'][3] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad bgGrey">
                                <div class="dtxt">{{ $Toto['consolation'][4] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad">
                                <div class="dtxt">{{ $Toto['consolation'][5] }}</div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 col-xs-12">
                        <div class="col-md-12">
                            <div class="col-md-4 col-xs-4 nopad">
                                <div class="dtxt">{{ $Toto['consolation'][6] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad bgGrey">
                                <div class="dtxt">{{ $Toto['consolation'][7] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad">
                                <div class="dtxt">{{ $Toto['consolation'][8] }}</div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 col-xs-12">
                        <div class="col-md-12">
                            <div class="col-md-4 col-xs-4 nopad">
                                &nbsp;
                            </div>
                            <div class="col-md-4 col-xs-4 nopad bgGrey">
                                <div class="dtxt">{{ $Toto['consolation'][9] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad">
                                &nbsp;
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end toto-->

            <div class="ht1"></div>

            <!--singapore-->
            <div class="col-md-12 col-xs-12">
                <div class="row mt2">
                    <div class="col-lg-12 nopad">
                        <div class="col-lg-12 nopad">
                            <div class="wTit">
                                SINGAPORE 4D ({{ $Singapore['date'] }})
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 col-xs-12">
                        <div class="col-md-12">
                            <div class="col-md-4 col-xs-4 nopad">
                                <span class="dTT">1st</span>
                                <div class="dtxt">{{ $Singapore['first'] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad bgGrey">
                                <span class="dTT">2nd</span>
                                <div class="dtxt">{{ $Singapore['second'] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad">
                                <span class="dTT">3rd</span>
                                <div class="dtxt">{{ $Singapore['third'] }}</div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 col-xs-12" style="text-align: center;">
                        <h5>Special</h5>
                    </div>

                    <div class="col-md-12 col-xs-12">
                        <div class="col-md-12">
                            <div class="col-md-4 col-xs-4 nopad">
                                <div class="dtxt">{{ $Singapore['special'][0] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad bgGrey">
                                <div class="dtxt">{{ $Singapore['special'][1] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad">
                                <div class="dtxt">{{ $Singapore['special'][2] }}</div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 col-xs-12">
                        <div class="col-md-12">
                            <div class="col-md-4 col-xs-4 nopad">
                                <div class="dtxt">{{ $Singapore['special'][3] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad bgGrey">
                                <div class="dtxt">{{ $Singapore['special'][4] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad">
                                <div class="dtxt">{{ $Singapore['special'][5] }}</div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 col-xs-12">
                        <div class="col-md-12">
                            <div class="col-md-4 col-xs-4 nopad">
                                <div class="dtxt">{{ $Singapore['special'][6] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad bgGrey">
                                <div class="dtxt">{{ $Singapore['special'][7] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad">
                                <div class="dtxt">{{ $Singapore['special'][8] }}</div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 col-xs-12">
                        <div class="col-md-12">
                            <div class="col-md-4 col-xs-4 nopad">
                                &nbsp;
                            </div>
                            <div class="col-md-4 col-xs-4 nopad bgGrey">
                                <div class="dtxt">{{ $Singapore['special'][9] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad">
                                &nbsp;
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 col-xs-12" style="text-align: center;">
                        <h5>Consolation</h5>
                    </div>

                    <div class="col-md-12 col-xs-12">
                        <div class="col-md-12">
                            <div class="col-md-4 col-xs-4 nopad">
                                <div class="dtxt">{{ $Singapore['consolation'][0] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad bgGrey">
                                <div class="dtxt">{{ $Singapore['consolation'][1] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad">
                                <div class="dtxt">{{ $Singapore['consolation'][2] }}</div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 col-xs-12">
                        <div class="col-md-12">
                            <div class="col-md-4 col-xs-4 nopad">
                                <div class="dtxt">{{ $Singapore['consolation'][3] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad bgGrey">
                                <div class="dtxt">{{ $Singapore['consolation'][4] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad">
                                <div class="dtxt">{{ $Singapore['consolation'][5] }}</div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 col-xs-12">
                        <div class="col-md-12">
                            <div class="col-md-4 col-xs-4 nopad">
                                <div class="dtxt">{{ $Singapore['consolation'][6] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad bgGrey">
                                <div class="dtxt">{{ $Singapore['consolation'][7] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad">
                                <div class="dtxt">{{ $Singapore['consolation'][8] }}</div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 col-xs-12">
                        <div class="col-md-12">
                            <div class="col-md-4 col-xs-4 nopad">
                                &nbsp;
                            </div>
                            <div class="col-md-4 col-xs-4 nopad bgGrey">
                                <div class="dtxt">{{ $Singapore['consolation'][9] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad">
                                &nbsp;
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end singapore-->

            <div class="ht1"></div>

            <!--t88-->
            <div class="col-md-12 col-xs-12">
                <div class="row mt2">
                    <div class="col-lg-12 nopad">
                        <div class="col-lg-12 nopad">
                            <div class="wTit">
                                SABAH 4D ({{ $Sabah['date'] }})
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 col-xs-12">
                        <div class="col-md-12">
                            <div class="col-md-4 col-xs-4 nopad">
                                <span class="dTT">1st</span>
                                <div class="dtxt">{{ $Sabah['first'] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad bgGrey">
                                <span class="dTT">2nd</span>
                                <div class="dtxt">{{ $Sabah['second'] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad">
                                <span class="dTT">3rd</span>
                                <div class="dtxt">{{ $Sabah['third'] }}</div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 col-xs-12" style="text-align: center;">
                        <h5>Special</h5>
                    </div>

                    <div class="col-md-12 col-xs-12">
                        <div class="col-md-12">
                            <div class="col-md-4 col-xs-4 nopad">
                                <div class="dtxt">{{ $Sabah['special'][0] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad bgGrey">
                                <div class="dtxt">{{ $Sabah['special'][1] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad">
                                <div class="dtxt">{{ $Sabah['special'][2] }}</div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 col-xs-12">
                        <div class="col-md-12">
                            <div class="col-md-4 col-xs-4 nopad">
                                <div class="dtxt">{{ $Sabah['special'][3] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad bgGrey">
                                <div class="dtxt">{{ $Sabah['special'][4] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad">
                                <div class="dtxt">{{ $Sabah['special'][5] }}</div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 col-xs-12">
                        <div class="col-md-12">
                            <div class="col-md-4 col-xs-4 nopad">
                                <div class="dtxt">{{ $Sabah['special'][6] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad bgGrey">
                                <div class="dtxt">{{ $Sabah['special'][7] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad">
                                <div class="dtxt">{{ $Sabah['special'][8] }}</div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 col-xs-12">
                        <div class="col-md-12">
                            <div class="col-md-4 col-xs-4 nopad">
                                &nbsp;
                            </div>
                            <div class="col-md-4 col-xs-4 nopad bgGrey">
                                <div class="dtxt">{{ $Sabah['special'][9] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad">
                                &nbsp;
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 col-xs-12" style="text-align: center;">
                        <h5>Consolation</h5>
                    </div>

                    <div class="col-md-12 col-xs-12">
                        <div class="col-md-12">
                            <div class="col-md-4 col-xs-4 nopad">
                                <div class="dtxt">{{ $Sabah['consolation'][0] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad bgGrey">
                                <div class="dtxt">{{ $Sabah['consolation'][1] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad">
                                <div class="dtxt">{{ $Sabah['consolation'][2] }}</div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 col-xs-12">
                        <div class="col-md-12">
                            <div class="col-md-4 col-xs-4 nopad">
                                <div class="dtxt">{{ $Sabah['consolation'][3] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad bgGrey">
                                <div class="dtxt">{{ $Sabah['consolation'][4] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad">
                                <div class="dtxt">{{ $Sabah['consolation'][5] }}</div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 col-xs-12">
                        <div class="col-md-12">
                            <div class="col-md-4 col-xs-4 nopad">
                                <div class="dtxt">{{ $Sabah['consolation'][6] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad bgGrey">
                                <div class="dtxt">{{ $Sabah['consolation'][7] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad">
                                <div class="dtxt">{{ $Sabah['consolation'][8] }}</div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 col-xs-12">
                        <div class="col-md-12">
                            <div class="col-md-4 col-xs-4 nopad">
                                &nbsp;
                            </div>
                            <div class="col-md-4 col-xs-4 nopad bgGrey">
                                <div class="dtxt">{{ $Sabah['consolation'][9] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad">
                                &nbsp;
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--End of t88-->

            <div class="ht1"></div>

            <!--STC-->
            <div class="col-md-12 col-xs-12">
                <div class="row mt2">
                    <div class="col-lg-12 nopad">
                        <div class="col-lg-12 nopad">
                            <div class="wTit">
                                SANDAKAN 4D ({{ $Sandakan['date'] }})
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 col-xs-12">
                        <div class="col-md-12">
                            <div class="col-md-4 col-xs-4 nopad">
                                <span class="dTT">1st</span>
                                <div class="dtxt">{{ $Sandakan['first'] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad bgGrey">
                                <span class="dTT">2nd</span>
                                <div class="dtxt">{{ $Sandakan['second'] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad">
                                <span class="dTT">3rd</span>
                                <div class="dtxt">{{ $Sandakan['third'] }}</div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 col-xs-12" style="text-align: center;">
                        <h5>Special</h5>
                    </div>

                    <div class="col-md-12 col-xs-12">
                        <div class="col-md-12">
                            <div class="col-md-4 col-xs-4 nopad">
                                <div class="dtxt">{{ $Sandakan['special'][0] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad bgGrey">
                                <div class="dtxt">{{ $Sandakan['special'][1] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad">
                                <div class="dtxt">{{ $Sandakan['special'][2] }}</div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 col-xs-12">
                        <div class="col-md-12">
                            <div class="col-md-4 col-xs-4 nopad">
                                <div class="dtxt">{{ $Sandakan['special'][3] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad bgGrey">
                                <div class="dtxt">{{ $Sandakan['special'][4] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad">
                                <div class="dtxt">{{ $Sandakan['special'][5] }}</div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 col-xs-12">
                        <div class="col-md-12">
                            <div class="col-md-4 col-xs-4 nopad">
                                <div class="dtxt">{{ $Sandakan['special'][6] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad bgGrey">
                                <div class="dtxt">{{ $Sandakan['special'][7] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad">
                                <div class="dtxt">{{ $Sandakan['special'][8] }}</div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 col-xs-12">
                        <div class="col-md-12">
                            <div class="col-md-4 col-xs-4 nopad">
                                &nbsp;
                            </div>
                            <div class="col-md-4 col-xs-4 nopad bgGrey">
                                <div class="dtxt">{{ $Sandakan['special'][9] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad">
                                &nbsp;
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 col-xs-12" style="text-align: center;">
                        <h5>Consolation</h5>
                    </div>

                    <div class="col-md-12 col-xs-12">
                        <div class="col-md-12">
                            <div class="col-md-4 col-xs-4 nopad">
                                <div class="dtxt">{{ $Sandakan['consolation'][0] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad bgGrey">
                                <div class="dtxt">{{ $Sandakan['consolation'][1] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad">
                                <div class="dtxt">{{ $Sandakan['consolation'][2] }}</div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 col-xs-12">
                        <div class="col-md-12">
                            <div class="col-md-4 col-xs-4 nopad">
                                <div class="dtxt">{{ $Sandakan['consolation'][3] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad bgGrey">
                                <div class="dtxt">{{ $Sandakan['consolation'][4] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad">
                                <div class="dtxt">{{ $Sandakan['consolation'][5] }}</div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 col-xs-12">
                        <div class="col-md-12">
                            <div class="col-md-4 col-xs-4 nopad">
                                <div class="dtxt">{{ $Sandakan['consolation'][6] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad bgGrey">
                                <div class="dtxt">{{ $Sandakan['consolation'][7] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad">
                                <div class="dtxt">{{ $Sandakan['consolation'][8] }}</div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 col-xs-12">
                        <div class="col-md-12">
                            <div class="col-md-4 col-xs-4 nopad">
                                &nbsp;
                            </div>
                            <div class="col-md-4 col-xs-4 nopad bgGrey">
                                <div class="dtxt">{{ $Sandakan['consolation'][9] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad">
                                &nbsp;
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--End of STC-->

            <div class="ht1"></div>

            <!--Cash-->
            <div class="col-md-12 col-xs-12">
                <div class="row mt2">
                    <div class="col-lg-12 nopad">
                        <div class="col-lg-12 nopad">
                            <div class="wTit">
                                SPECIAL BIGSWEEP ({{ $Sarawak['date'] }})
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 col-xs-12">
                        <div class="col-md-12">
                            <div class="col-md-4 col-xs-4 nopad">
                                <span class="dTT">1st</span>
                                <div class="dtxt">{{ $Sarawak['first'] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad bgGrey">
                                <span class="dTT">2nd</span>
                                <div class="dtxt">{{ $Sarawak['second'] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad">
                                <span class="dTT">3rd</span>
                                <div class="dtxt">{{ $Sarawak['third'] }}</div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 col-xs-12" style="text-align: center;">
                        <h5>Special</h5>
                    </div>

                    <div class="col-md-12 col-xs-12">
                        <div class="col-md-12">
                            <div class="col-md-4 col-xs-4 nopad">
                                <div class="dtxt">{{ $Sarawak['special'][0] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad bgGrey">
                                <div class="dtxt">{{ $Sarawak['special'][1] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad">
                                <div class="dtxt">{{ $Sarawak['special'][2] }}</div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 col-xs-12">
                        <div class="col-md-12">
                            <div class="col-md-4 col-xs-4 nopad">
                                <div class="dtxt">{{ $Sarawak['special'][3] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad bgGrey">
                                <div class="dtxt">{{ $Sarawak['special'][4] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad">
                                <div class="dtxt">{{ $Sarawak['special'][5] }}</div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 col-xs-12">
                        <div class="col-md-12">
                            <div class="col-md-4 col-xs-4 nopad">
                                <div class="dtxt">{{ $Sarawak['special'][6] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad bgGrey">
                                <div class="dtxt">{{ $Sarawak['special'][7] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad">
                                <div class="dtxt">{{ $Sarawak['special'][8] }}</div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 col-xs-12">
                        <div class="col-md-12">
                            <div class="col-md-4 col-xs-4 nopad">
                                &nbsp;
                            </div>
                            <div class="col-md-4 col-xs-4 nopad bgGrey">
                                <div class="dtxt">{{ $Sarawak['special'][9] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad">
                                &nbsp;
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 col-xs-12" style="text-align: center;">
                        <h5>Consolation</h5>
                    </div>

                    <div class="col-md-12 col-xs-12">
                        <div class="col-md-12">
                            <div class="col-md-4 col-xs-4 nopad">
                                <div class="dtxt">{{ $Sarawak['consolation'][0] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad bgGrey">
                                <div class="dtxt">{{ $Sarawak['consolation'][1] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad">
                                <div class="dtxt">{{ $Sarawak['consolation'][2] }}</div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 col-xs-12">
                        <div class="col-md-12">
                            <div class="col-md-4 col-xs-4 nopad">
                                <div class="dtxt">{{ $Sarawak['consolation'][3] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad bgGrey">
                                <div class="dtxt">{{ $Sarawak['consolation'][4] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad">
                                <div class="dtxt">{{ $Sarawak['consolation'][5] }}</div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 col-xs-12">
                        <div class="col-md-12">
                            <div class="col-md-4 col-xs-4 nopad">
                                <div class="dtxt">{{ $Sarawak['consolation'][6] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad bgGrey">
                                <div class="dtxt">{{ $Sarawak['consolation'][7] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad">
                                <div class="dtxt">{{ $Sarawak['consolation'][8] }}</div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 col-xs-12">
                        <div class="col-md-12">
                            <div class="col-md-4 col-xs-4 nopad">
                                &nbsp;
                            </div>
                            <div class="col-md-4 col-xs-4 nopad bgGrey">
                                <div class="dtxt">{{ $Sarawak['consolation'][9] }}</div>
                            </div>
                            <div class="col-md-4 col-xs-4 nopad">
                                &nbsp;
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--End of Cash-->

        </div>
    </div>
@endsection

@section('modal')
    <div class="modal fade" id="lmPayoutTable" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <h5>Prize money for Big Forecast</h5>
                            <div>
                                For this Forecast, as shown in the table below, with every RM1 bet, the 1st Prize pays RM3,400, 2nd Prize RM1,200, 3rd Prize RM600, Special Prize RM250 and Consolation Prize RM80.
                                <br><br>
                                <div class="table" >
                                    <table>
                                        <tr>
                                            <td>BIG FORECAST</td>
                                            <td>PRIZE AMOUNT</td>
                                        </tr>
                                        <tr>
                                            <td>1st Prize</td>
                                            <td>RM 3,400.00</td>
                                        </tr>
                                        <tr>
                                            <td>2nd Prize</td>
                                            <td>RM 1,200.00</td>
                                        </tr>
                                        <tr>
                                            <td>3rd Prize</td>
                                            <td>RM 600.00</td>
                                        </tr>
                                        <tr>
                                            <td>Special Prize</td>
                                            <td>RM 250.00</td>
                                        </tr>
                                        <tr>
                                            <td>Consolation Prize</td>
                                            <td>RM 80.00</td>
                                        </tr>
                                    </table>
                                </div>

                                <b>Prize money for Small Forecast</b><br><br>
                                For this Forecast, as shown in the table below, with every RM1 bet, the 1st Prize pays RM4,800, 2nd Prize RM2,400, 3rd Prize RM1,200.
                                <br><br>

                                <div class="table" >
                                    <table>
                                        <tr>
                                            <td>SMALL FORECAST</td>
                                            <td>PRIZE AMOUNT</td>
                                        </tr>
                                        <tr>
                                            <td>1st Prize</td>
                                            <td>RM 4,800.00</td>
                                        </tr>
                                        <tr>
                                            <td>2nd Prize</td>
                                            <td>RM 2,400.00</td>
                                        </tr>
                                        <tr>
                                            <td>3rd Prize</td>
                                            <td>RM 1,200.00</td>
                                        </tr>
                                        <tr>
                                            <td>4A Prize</td>
                                            <td>RM 8,000.00</td>
                                        </tr>
                                    </table>
                                </div>

                                <b>Prize money for 3D</b><br><br>
                                For this Forecast, as shown in the table below, with every RM1 bet, the 1st Prize pays RM 280, 2nd Prize RM280, and 3rd Prize RM280.
                                <div class="table" >
                                    <table>
                                        <tr>
                                            <td>3D</td>
                                            <td>PRIZE AMOUNT</td>
                                        </tr>
                                        <tr>
                                            <td>1st Prize</td>
                                            <td>RM 280.00</td>
                                        </tr>
                                        <tr>
                                            <td>2nd Prize</td>
                                            <td>RM 280.00</td>
                                        </tr>
                                        <tr>
                                            <td>3rd Prize</td>
                                            <td>RM 280.00</td>
                                        </tr>
                                        <tr>
                                            <td>3A Prize</td>
                                            <td>RM 840.00</td>
                                        </tr>
                                    </table>
                                </div>

                                <b>Prize money for 5D/6D</b><br><br>
                                For this Forecast, as shown in the table below, with every RM1 bet.
                                <div class="table" >
                                    <table>
                                        <tr>
                                            <td>5D</td>
                                            <td>PRIZE AMOUNT</td>
                                            <td>6D</td>
                                            <td>PRIZE AMOUNT</td>
                                        </tr>
                                        <tr>
                                            <td>1st Prize</td>
                                            <td>RM 15,000.00</td>
                                            <td>1st Prize</td>
                                            <td>RM 100,000.00</td>
                                        </tr>
                                        <tr>
                                            <td>2nd Prize</td>
                                            <td>RM 5,000.00</td>
                                            <td>2nd Prize</td>
                                            <td>RM 3,000.00</td>
                                        </tr>
                                        <tr>
                                            <td>3rd Prize</td>
                                            <td>RM 3,000.00</td>
                                            <td>3rd Prize</td>
                                            <td>RM 300.00</td>
                                        </tr>
                                        <tr>
                                            <td>4th Prize</td>
                                            <td>RM 500.00</td>
                                            <td>4th Prize</td>
                                            <td>RM 30.00</td>
                                        </tr>
                                        <tr>
                                            <td>5th Prize</td>
                                            <td>RM 20.00</td>
                                            <td>5th Prize</td>
                                            <td>RM 4.00</td>
                                        </tr>
                                        <tr>
                                            <td>6th Prize</td>
                                            <td>RM 5.00</td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">{{ Lang::get('COMMON.CLOSE') }}</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="lmRulesTable" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <h5>Rules &amp; Regulations</h5>
                            <div>
                                <b>IMPORTANT: PLEASE READ THE INFORMATION SET OUT BELOW CAREFULLY BEFORE ACCEPTING THESE TERMS AND CONDITIONS.</b>
                                <p>
                                    Notice and acceptance of these terms and conditions constitutes the making of an agreement, which you accept by selecting the "Agree" on this page and proceeding to access the Website. When you do so, a legally binding agreement in accordance with these terms and conditions is concluded between (a) you, the end user of the Website (the "Customer"), and (b) the Company.
                                </p>
                                <b>General terms and conditions</b>
                                <p>
                                    The following are the terms and conditions governing the use of the Website. All Customer activities on the Website are subject to, and governed by these Terms and Conditions.
                                </p>
                                <b>The company reserves the right to do the following:</b>
                                <ol>
                                    <li style="list-style:circle">The company reserves the right to void any unusual transaction.</li>
                                    <li style="list-style:circle">Our company reserves the right to refuse any investment without further correspondence.</li>
                                    <li style="list-style:circle">Investment placed after preset time will be void</li>
                                    <li style="list-style:circle">Cancel draw and refund to user whenever is necessary</li>
                                    <li style="list-style:circle">The content of the pages of this website is for your general information and use only. It is subject to change without notice.</li>
                                    <li style="list-style:circle">Neither we nor any third parties provide any warranty or guarantee as to the accuracy, timeliness, performance, completeness or suitability of the information and materials found or offered on this website for any particular purpose. You acknowledge that such information and materials may contain inaccuracies or errors and we expressly exclude liability for any such inaccuracies or errors to the fullest extent permitted by law.</li>
                                    <li style="list-style:circle">Your use of any information or materials on this website is entirely at your own risk, for which we shall not be liable. It shall be your own responsibility to ensure that any products, services or information available through this website meet your specific requirements.</li>
                                </ol>
                                <b>Important Notice:</b>
                                <p>
                                    Sabah Lotto Results - 3D results for 1st, 2nd and 3rd are NOT derived from the last 3 digits of 4D's 1st, 2nd and 3rd. They are drawn separately. Payment is based on these 2 sets of results.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">{{ Lang::get('COMMON.CLOSE') }}</button>
                </div>
            </div>
        </div>
    </div>
@endsection
