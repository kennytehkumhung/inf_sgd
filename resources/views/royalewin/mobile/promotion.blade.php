@extends('royalewin/mobile/master')

@if (Session::get('currency') == 'MYR')
    @section('title', 'Best Online Casino Malaysia Promotions & Bonus')
    @section('keywords', 'Best Online Casino Malaysia Promotions & Bonus')
    @section('description', 'Every month at RoyaleWin Live Online Casino Malaysia we have new promotion and bonus for you to enjoy. Don\'t wait, Sign Up Now!')
@elseif (Session::get('currency') == 'IDR')
    @section('title', 'Bandar Casino Online, Situs Judi Indonesia - Promosi')
    @section('keywords', 'Bandar Casino Online, Situs Judi Indonesia - Promosi')
    @section('description', 'RoyaleWin adalah agen bola, bandar casino online dan situs judi online terpercaya untuk Anda penggemar taruhan bola dan pecinta judi online di Indonesia.')
@endif

@section('content')
    <!-- Page Content -->
    <div class="container">
        <div class="row">
            @foreach ($promo as $key => $value )
                @if (isset($value['image']) && strlen($value['image']) > 0)
                    <div class="col-md-6 col-xs-12 promoC">
                        <img src="{{ $value['image'] }}" data-toggle="modal" data-target="#md_promo_details_{{ $key }}" style="height: auto; width: 100%;">
                    </div>
                @endif
            @endforeach
        </div>

        <div class="clearfix"></div>

        <div class="row">
            <div class="col-xs-12 text-center">
                <hr>
                <a data-toggle="modal" href="#md_tnc" style="font-size: 12px; font-weight: bold; color: #ffae00;">
                {{--<a data-toggle="modal" data-target="#md_tnc" style="font-size: 12px; font-weight: bold; color: #ffae00;">--}}
                    @if (Lang::getLocale() == 'cn')
                        优惠一般条款规则
                    @elseif (Lang::getLocale() == 'id')
                        Syarat &amp; Ketentuan Promosi Umum
                    @else
                        General Terms &amp; Conditions of Promotions
                    @endif
                </a>
            </div>
        </div>
    </div>
@endsection

@section('modal')
    @foreach ($promo as $key => $value )
        <div class="modal fade" id="md_promo_details_{{ $key }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12 col-xs-12">
                                <h5>{{ $value['title'] }}</h5>
                                <div>
                                    {!! htmlspecialchars_decode($value['content']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">{{ Lang::get('COMMON.CLOSE') }}</button>
                    </div>
                </div>
            </div>
        </div>

    @endforeach

    <div class="modal fade" id="md_tnc" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <h5>
                                @if (Lang::getLocale() == 'cn')
                                    优惠一般条款规则
                                @elseif (Lang::getLocale() == 'id')
                                    Syarat &amp; Ketentuan Promosi Umum
                                @else
                                    General Terms &amp; Conditions of Promotions
                                @endif
                            </h5>
                            @if (Lang::getLocale() == 'cn')
                                <ol style="list-style-type:decimal; padding-right:5px;">
                                    <li>您必须年满18岁以上或在您被司法管辖范围内属成年年龄; 拥有足够心理承受能力为自己的行为负责， 并接受这些条款与条件的约束。豪赢保留于任何或所有时间取消任何涉及未成年人的交易的权利。</li>
                                    <li>所有豪赢提供予客户的奖金/红利仅供娱乐或消遣用途。被发现或怀疑为非消遣性玩家（职业赌徒/只为寻求红利的玩家）或与其他方协作/串通/试图欺诈的用户将无资格参与我们的优惠活动。该失去资格的账户所获得的任何红利与奖金将能在豪赢的全权决定下被撤销。</li>
                                    <li>您只被允许在网站拥有一个活动帐户。 豪赢保留监控任何有意建立多个帐户的权利。在发现你已经注册了多个帐户的情况下，所有帐户将被关闭，恕不另行通知。豪赢有权拒绝任何人访问的权利。所有优惠仅限于每人，每家庭，每家庭住址，每电子邮件地址，每电话号码，每银行账号和每IP地址的一个帐户。</li>
                                    <li>所有的优惠活动将严格禁止多个账户登记与滥用。 豪赢拥有全权自行决定某行为构成了多个帐户滥用的权利。</li>
                                    <li>滥用奖励优惠. 所有的奖金优惠只限于娱乐性投注。我们有权定义其为职业玩家或者普通玩家，玩家以任何方式滥用奖金优惠将会被撤销奖金及进一步处理。滥用奖金优惠活动可以定义为（但不仅限于）客户提款后用于再次存款，或转入自己的新账户使用。其处罚手段会以增加投注额或者扣除奖金金额的形式，其他相关联账户亦将包括其中。如有必要我们有权开设特别的奖金优惠活动。其包括（但不仅限于）设置地域性限制规则以防止滥用优惠奖励。</li>
                                    <li>凡在优惠或促销如不符合任何条款，未达到资格，违反条规，滥用或被发现有一系列由同一个客户或一客户群的下注活动（于存款红利或任何其他优惠导致保证客户利润，不论结果如何及无论个别或集体的下注）的任何证据来源的账户持有人，， 豪赢 保留取消，取消或收回红利惠奖金加上所有的彩金的权利。此外，豪赢 保留向客户征收高达存款红利价值的费用以支付行政费用的权利。</li>
                                    <li>在达到下注交易额（rollover）要求之前，存款金额加上红利及有关奖金不可被提取。
                                        <ol type="a">
                                            <li>如果用户已申请取得优惠奖金，但奖金汇入该用户账户之前：</li>
                                            <ol type="i">
                                                <li>他的账户余额被视为不足以完成奖金的下注交易额（turnover）要求； 豪赢有权要求该用户补充账户余额或于奖金汇入完成之前做出同样数额的新存款。</li>
                                                <li>他做了提款申请；豪赢有权拒绝该提款申请，因该情况下会员将无法履行与完成优惠的下注交易额要求。一旦下注交易额要求已达到，用户可再次提交提款申请。</li>
                                            </ol>
                                            <li>同样的， 当优惠奖金汇入了用户账户后，该用户随后做的一次或以上的下注将受到优惠下注交易额的约束。豪赢将不会批准用户为了进行提款而希望取消或提取奖金的要求。用户必须首先满足该优惠下注交易额的要求才能提款。</li>
                                        </ol>
                                    </li>
                                    <li>只有产生胜/输结果的赌注将计算在下注交易额内以达到下注交易额要求（rollover requirement）。两方下注，取消/作废和平局的赌注将不计算在下注交易额内。体育博彩中的数字游戏（Number Games）赌注亦不计算在下注交易额内。</li>
                                    <li>低于以下的投注（十进制投注赔率1.5; 香港盘0.5; 马来盘0.5; 印尼盘 -2.0; 美国盘 -200）和无效的，平局，取消的投注，或将取得相同结果的双方下注将不被计算在下注交易额内。</li>
                                    <li>根据上文第7项条款，一旦奖金已经汇入会员账户，豪赢将有全部权利决定批准或拒绝该成员欲更改优惠的要求或于还未达到下注交易额要求之前的提款申请。任何此情况下给予的批准，该用户所拥有的存款和提取的奖金/红利数额将严格受到罚款；并且于提出此提款要求前所赢得/累积的任何彩金将被取消。</li>
                                </ol>
                            @elseif (Lang::getLocale() == 'id')
                                <ol style="list-style-type:decimal; padding-right:5px;">
                                    <li>Pemain telah berusia lebih dari 18 (delapan belas) tahun atau melebihi usia sah dalam hukum setempat, yang mana lebih tinggi dan memiliki kapasitas mental untuk menanggung tanggung jawab dari aksi perbuatan pemain sendiri dan dapat diikat dalam syarat dan ketentuan ini, kami berhak penuh setiap saat untuk membatalkan transakasi apapun yang berhubungan dengan pemain dibawah umur.</li>
                                    <li>Seluruh bonus yang ditawarkan ditujuan untuk keperluan rekreasional customer kami saja. Akun yang di curigai atau di identifikasi melakukan permainan yang bertujuan non-rekreasional atau melakukan kerjasama antara beberapa pihak tidak berhak untuk promosi ini dan apabila terdapat maka segala bonus atau kemenangan yang telah di dapatkan akan di tarik kembali.</li>
                                    <li>Anda hanya di ijinkan untuk memiliki satu akun yang aktif dalam situs. Kami berhak untuk mengawasi segala usaha / tindakan yang dilakukan untuk mendapatkan akun tambahan. Apabila ditemukan bahwa anda telah membuka lebih dari satu akun, maka seluruh akun yang ada akan di tutup tanpa pemberitahuan terlebih dahulu. Kami berhak untuk menolak akses masuk kepada siapapun juga. Segala penawaran yang kami berikan terbatas untuk satu akun per orang, keluarga, alamat rumah, alamat email, nomor telepon, akun bank dan alamat IP.</li>
                                    <li>Dalam segala promosi, peraturan atas penyalahgunaan batas jumlah akun akan diberlakukan dengan tegas. Dengan kapasitas dan kebijaksanaan pribadi nya Royalewin berhak untuk memutuskan aktifitas mana yang di anggap penyalahgunaan batas jumlah akun.</li>
                                    <li>Penyalahgunaan Program Bonus. Program bonus hanya ditujukan untuk pemain rekreasional saja. Pemain profesional atau pemain yang dalam kebijakan tunggal kami, dianggap menyalahgunakan sistem bonus dalam artian apapun akan ditarik kembali bonusnya dan akan dikenakan sangsi selanjutnya. Penyalahgunaan bonus bisa dikategorikan sebagai ( tetapi tidak terbatas pada) klien yang menarik uang dengan tujuan melakukan deposit berulang, atau mereferensikan account baru yang mereka gunakan sendiri. Sanksi tersebut bisa berupa meningkatnya rollover yang harus dicapai atau hilangnya hak untuk memperoleh bonus atau keduanya pada account yang melanggar demikian juga dengan account lainnya yang terkait. Kami mempunyai hak untuk membatasi siapa yang berhak memperoleh penawaran khusus dan bonus apabila diperlukan. Hal ini termasuk (tetapi tidak terbatas pada) menempatkan restriksi geografis pada bonus tersebut dikarenakan penyalahgunaan bonus.</li>
                                    <li>Dimana dalam jangka waktu penawaran atau promosi belum dipenuhi oleh pemegang akun yang sah, dilanggar, dilecehkan atau terdapat bukti dari serangkaian taruhan ditempatkan oleh pelanggan atau kelompok pelanggan, yang disebabkan oleh deposit bonus atau promosi lainnya menawarkan pelanggan dijamin menghasilkan keuntungan terlepas dari hasilnya, baik secara individual atau sebagai bagian dari kelompok, Royalewin.com berhak untuk menahan, membatalkan atau merebut kembali tambahan bonus kemenangan. Selain itu juga Royalewin.com berhak untuk memungut biaya administrasi pada pelanggan sampai nilai deposit bonus untuk menutupi biaya administrasi.</li>
                                    <li>Sebelum persyaratan turnover dalam promosi tercapai, jumlah deposit ditambah bonus dengan segala kemenangannya tidak di ijinkan untuk di withdraw / ditarik.
                                        <ol type="a">
                                            <li>apabila member telah mendaftar untuk berpartisipasi dalam sebuah promosi bonus, akan tetapi sebelum bonus di berikan ke akunnya:</li>
                                            <ol type="i">
                                                <li>sisa saldo akun dinilai tidak cukup untuk memenuhi syarat turnover bonus; maka kami berhak untuk meminta agar member melakukan isi ulang atau membuat deposit baru dengan jumlah deposit yang sama dengan sebelumnya sebelum bonus kami berikan kedalam credit member.</li>
                                                <li>member melakukan permintaan withdraw, kami berhak untuk menolak permintaan tersebut apabila kondisi syarat turnover promosi belum tercapai. Member dapat melakukan permintaan withdraw kembali setelah syarat turnover telah mencapai target ketentuannya.</li>
                                            </ol>
                                            <li>sebaliknya, pada saat bonus promosi sudah diberikan ke akun member dan kemudian member telah memasang bet satu kali atau lebih, permintaan penarikan kembali bonus atau pembatalan partisipasi promosi bonus agar dapat melakukan withdraw saldo akunnya tidak akan di ijinkan sampai member telah mencapai jumlah target syarat turnover nya.</li>
                                        </ol>
                                    </li>
                                    <li>Hanya bet taruhan dengan hasil menang / kalah yang akan diperhitungkan dalam ketentuan turnover promosi. Bet taruhan dengan hasil di batalkan, void atau seri tidak akan di hitung.</li>
                                    <li>Bet taruhan yang dibuat dibawah (peluang desimal 1,5; peluang HongKong 0,5; peluang Malay Positif 0,5; peluang Indo -0,2; peluang US -200) dan void, dibatalkan atau dibuat bet kiri kanan dengan hasil yang sama tidak akan dihitung dalam syarat ketentuan turnover.</li>
                                    <li>Sesuai dengan peraturan nomor 5 diatas, pada saat bonus telah masuk kedalam akun member, maka kami memiliki kebijakan mutlak untuk menyetujui atau menolak permintaan member atas penggantian partisipasi promosi, membatalkan bonus, atau untuk melakukan withdraw sebelum memenuhi syarat turnover nya. Segala persetujuan yang diberikan atas permintaan semacam ini dengan tegas akan dibebankan denda pada deposit, bonus atau segala kemenangan yang di dapatkan sebelum permintaan tersebut di ajukan akan hangus.</li>
                                </ol>
                            @else
                                <ol style="list-style-type:decimal; padding-right:5px;">
                                    <li>Customer is over the age of 18, or the age of consent in the customer's home jurisdiction, whichever is higher and has the mental capacity to take responsibility for the Customer's own actions and be bound by these terms and conditions. www.royalewin.com shall reserves the right at any and all times to void any transactions involving minors.</li>
                                    <li>All bonuses offered are intended for recreation customers only. Accounts identified or suspected as non-recreational play type or wager style or collaboration between parties is not eligible for this promotion and are subject to have any awarded bonuses plus winnings to be revoked at the sole discretion of www.royalewin.com.</li>
                                    <li>You are only permitted to have one active account on the Site. www.royalewin.com reserves the right to monitor any effort to establish multiple accounts. In the event it is discovered that you have opened more than one account, all accounts will be closed without notice. www.royalewin.com reserves the right to deny access to anyone. All offers is restricted to only one account per individual, family, household address, email address, telephone number, bank account and IP address.</li>
                                    <li>In all promotions, strict rules will be enforced on multiple accounts abuse. www.royalewin.com shall in its sole discretion decide on what activity constitutes multiple account abuse.</li>
                                    <li>Abuse of Bonus Programs: Bonus programs are intended for recreational bettors only. Professional players or players considered, in our sole discretion, to be abusing the bonus system by any means may have bonuses revoked and be subject to further sanctions. Bonus abuse may be defined as (but not restricted to) clients cashing out for the purpose of redepositing, or referring new accounts that they are using themselves. Sanctions may be in the form of increased rollover requirements or loss of bonus privileges altogether for the offending Account as well as any linked Accounts. We reserve the right to restrict eligibility for special offers and bonuses when necessary.</li>
                                    <li>Where any term of the offers or promotions have not been fulfilled by eligible account holders, are breached, abused or there is any evidence of a series of bets placed by a customer or group of customers, which due to a deposit bonus or any other promotional offer resulted in guaranteed customer profits irrespective of the outcome, whether individually or as part of a group, Royalewin.com reserves the right to withhold, cancel or reclaim the bonus plus all winnings. In addition Royalewin.com reserves the right to levy an administration charge on the customer up to the value of the deposit bonus to cover administrative costs.</li>
                                    <li>Before the promotional rollover requirements are met, the deposit amount plus the Bonus and any winnings attributable are not allowed for withdrawal.
                                        <ol type="a">
                                            <li>If a member has applied for a promotion bonus, but before the bonus has been credited in his account:</li>
                                            <ol type="i">
                                                <li>His account balance is deemed insufficient to complete the bonus turnover requirement; www.royalewin.com reserves the right to ask the member to top up the balance or make a fresh deposit of the same amount before the credit of the bonus is done.</li>
                                                <li>He makes a withdrawal request; www.royalewin.com reserves the right to reject the withdrawal due to non-fulfillment of the turnover condition of the promotion. A member can make another request for withdrawal once the turnover condition has been satisfied.</li>
                                            </ol>
                                            <li>Likewise, when a promotion bonus is credited in a member account and he subsequently places a bet or more, www.royalewin.com will not entertain his request to cancel or withdraw the bonus in order to allow him to make a withdrawal, unless he fulfills the required promotion turnover first.</li>
                                        </ol>
                                    </li>
                                    <li>Only wagers that generate a win/loss return will contribute to the promotional rollover requirement. Cancelled or void wagers or draw bets will not be counted.</li>
                                    <li>Bets made below (Decimal odd 1.5; Hong Kong odd 0.5; Malay Positive Odds 0.5; Indo Odd -2.0; US Odds -200) and void, tie, cancelled, or made on opposite sides with same outcome will not be counted towards the rollover requirements.</li>
                                    <li>Subject to Clause 5 above, once a bonus has been credited into a member's account, www.royalewin.com will have the sole discretion to approve or reject the member's request to change promotion; or to cancel the bonus; or to withdraw without completing the required rollover. Any approval given will be subjected strictly to a penalty being charged on the deposit, the bonus amount withdrawn and any winnings accrued prior to such a request will be voided.</li>
                                </ol>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">{{ Lang::get('COMMON.CLOSE') }}</button>
                </div>
            </div>
        </div>
    </div>
@endsection