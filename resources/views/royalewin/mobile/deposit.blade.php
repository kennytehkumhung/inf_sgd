@extends('royalewin/mobile/master')

@section('title', 'Deposit')

@section('css')
    @parent

    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    
    <style type="text/css">
    .popup_img { display: inline-block; position: absolute; z-index: 10001; margin: auto auto; top: 0px; bottom:0; left:0; right: 0; }
    </style>
@endsection

@section('js')
    @parent

    <script src="{{ asset('/royalewin/resources/js/jquery-ui.js') }}"></script>
    <script type="text/javascript">
        $(function () {
            $(".datepicker").datepicker({dateFormat: 'yy-mm-dd', defaultDate: new Date()});

            load_deposit();
            
            $("#masklayer").click(function () {
            $(".popup_img").fadeOut(100);
            $("#masklayer").fadeOut(100);
            });
        });
        
        function closePopup() {
        $(".popup_img").fadeOut(100);
        $("#masklayer").fadeOut(100);
        }

        function closeTryMobileBox() {
            $('#tryMobileBox').hide();
        }

        // Deposit.
        function load_deposit() {
            $.ajax({
                url: "{{ route('deposit') }}",
                type: "GET",
                dataType: "JSON",
                data: {
                    _token: "{{ csrf_token() }}",
                    as_json: true
                },
                success: function (result) {
                    if (typeof result != undefined && result != null) {
                        var resultHtml = "";

                        for (var key in result.banks) {
                            if (result.banks.hasOwnProperty(key)) {
                                resultHtml += '<option value="' + result.banks[key].bnkid + '" data-min="' + result.banks[key].min + '" data-max="' + result.banks[key].max + '" data-account-name="' + result.banks[key].bankaccname + '" data-account-number="' + result.banks[key].bankaccno + '">\n\
                                ' + result.banks[key].name + '\n\
                            </option>';
                            }
                        }

                        $("#deposit_banklist").html('<select name="bank" onchange="depositBankChanged(this);">' + resultHtml + '</select>');

                        var selectObj = $("#deposit_banklist select[name='bank']");
                        selectObj.val($("option:eq(0)", selectObj).val()).change();

                        resultHtml = "";

                        for (var key in result.promos) {
                            if (result.promos.hasOwnProperty(key)) {
                                resultHtml += '<div class="col-md-12">\n\
                                    <div class="col-xs-1">\n\
                                        <input type="radio" name="promo" value="' + result.promos[key].code + '">\n\
                                    </div>\n\
                                    <div class="col-xs-10 cut1">\n\
                                        <label>' + result.promos[key].name + '</label>\n\
                                    </div>\n\
                                    <div class="promoWrap">\n\
                                        <img src="' + result.promos[key].image + '" class="object-fit_cover" />\n\
                                    </div>\n\
                                </div>';
                            }
                        }

                        resultHtml += '<div class="col-md-12">\n\
                            <div class="col-xs-1">\n\
                                <input type="radio" name="promo" value="0">\n\
                            </div>\n\
                            <div class="col-xs-10 cut1">\n\
                                <label>{{ Lang::get('public.ProceedWithoutAnyOfThePromotionsAbove') }}</label>\n\
                            </div>\n\
                        </div>';

                        $("#deposit_promolist").html(resultHtml);
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    setTimeout(function () {
                        load_deposit();
                    }, 1000);
                }
            });
        }

        function depositBankChanged(obj) {
            var rObj = $(obj);

            if (rObj.length) {
                rObj = $("option[value='" + $(obj).val() + "']", $("#deposit_banklist select[name='bank']"));

                $("#deposit_account_name").html(rObj.data("account-name"));
                $("#deposit_account_number").html(rObj.data("account-number"));
                $("#deposit_min").html("{{Session::get('currency')}} " + rObj.data("min"));
                $("#deposit_max").html("{{Session::get('currency')}} " + rObj.data("max"));
            }
        }

        function deposit_submit(){
            var file_data = $("#deposit_receipt").prop("files")[0];
            var form_data = new FormData();

            form_data.append("file", file_data);

            var data = $('#deposit_form').serializeArray();
            var obj = {};

            for (var i = 0, l = data.length; i < l; i++) {
                form_data.append(data[i].name, data[i].value);
            }

            form_data.append('_token', '{{csrf_token()}}');

            $.ajax({
                type: "POST",
                url: "{{route('deposit-process')}}?",
                dataType: 'script',
                cache: false,
                contentType: false,
                processData: false,
                data: form_data, // Setting the data attribute of ajax with file_data
                type: 'post',
                beforeSend: function() {
                    $('#deposit_sbumit_btn').attr('onclick', '');
                },
                complete: function(json){
                    obj = JSON.parse(json.responseText);
                    var str = '';
                    $.each(obj, function(i, item) {
                        if ('{{Lang::get('COMMON.SUCESSFUL')}}' == item) {
                            alert('{{Lang::get('COMMON.SUCESSFUL')}}');
                            window.location.href = "{{route('update-profile')}}?tab=4";
                        } else {
                            $('#deposit_sbumit_btn').attr('onclick', 'deposit_submit()');
                            str += item + '\n';
                        }
                    });
                    if (str != '') {
                        alert(str);
                    }
                }
            });
        }
        // End deposit.
    </script>
@endsection

@section('content')
    <!-- Page Content -->
    <div class="container">
        <div id="masklayer" style="height: 100%;position: fixed;opacity: 0.6;background-color: black;z-index: 10001;left: 0px;right: 0px;top: 0px;display:block;" onclick="closePopup();"></div>
        @if (Lang::getLocale() == 'cn')
        <img src="{{ asset('/royalewin/img/banner/pop-up_CH.png') }}" class="popup_img" style="max-width: 800px;">
        @else
        <img src="{{ asset('/royalewin/img/banner/pop-up_EN.png') }}" class="popup_img" style="max-width: 800px;">
        @endif
        @include('royalewin/mobile/include/profile_menu', array('selected' => 'Deposit'))

        <div class="row">
            <div class="col-lg-12">
                <p>{{ Lang::get('public.DepositSteps1b') }}</p>
            </div>
        </div>

        <div>
            <form id="deposit_form">
                <div class="row bdr2 nopad">
                    <div class="col-lg-12 nopad">
                        <div class="col-lg-12 nopad">
                            <div class="wTit">
                                {{ Lang::get('public.BankingOptions') }}
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12 nopad wallRowtit">
                        <div class="row">
                            <div class="col-xs-6">
                                <label>{{ Lang::get('public.Bank') }}*</label>
                            </div>
                            <div id="deposit_banklist" class="col-xs-5">
                                {{ Lang::get('public.Loading') }}...
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12 nopad wallRow">
                        <div class="row">
                            <div class="col-xs-6">
                                <label>{{ Lang::get('public.BankingInformation2') }}</label>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12 nopad wallRow">
                        <div class="row">
                            <div class="col-xs-6">
                                <span>{{ Lang::get('public.BankAccountName') }}</span><br>
                                <span>{{ Lang::get('public.BankAccountNo') }}</span><br>
                                <span>{{ Lang::get('public.MinimumAmount') }}</span><br>
                                <span>{{ Lang::get('public.MaximumAmount') }}</span>
                            </div>

                            <div class="col-xs-6">
                                <span id="deposit_account_name"></span><br>
                                <span id="deposit_account_number"></span><br>
                                <span id="deposit_min"></span><br>
                                <span id="deposit_max"></span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="ht1"></div>

                <div class="row">
                    <div class="col-lg-12">
                        <p>{{ Lang::get('public.DepositSteps2') }}</p>
                    </div>
                </div>

                <div class="row bdr2 nopad">
                    <div class="col-lg-12 nopad">
                        <div class="col-lg-12 nopad">
                            <div class="wTit">
                                {{ Lang::get('public.DepositDetails') }}
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12 nopad wallRowtit">
                        <div class="row">
                            <div class="col-xs-6">
                                <label>{{ Lang::get('public.Amount') }}*</label>
                            </div>
                            <div class="col-xs-5">
                                <input type="text" id="deposit_amount" name="amount" />
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12 nopad wallRowtit">
                        <div class="row">
                            <div class="col-xs-6">
                                <label>{{ Lang::get('public.DepositMethod') }}*</label>
                            </div>
                            <div class="col-xs-5">
                                <select id="deposit_type" name="type">
                                    <option value="0">{{ Lang::get('public.OverCounter') }}</option>
                                    <option value="1">{{ Lang::get('public.InternetBanking') }}</option>
                                    <option value="2">{{ Lang::get('public.ATMBanking') }}</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12 nopad wallRowtit">
                        <div class="row">
                            <div class="col-xs-6">
                                <label>{{ Lang::get('public.DateTime') }}*</label>
                            </div>
                            <div class="col-xs-5">
                                <input type="text" class="datepicker" id="deposit_date" style="cursor:pointer;" name="date" value="{{date('Y-m-d')}}">
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12 nopad wallRowtit">
                        <div class="row">
                            <div class="col-xs-offset-6 col-xs-2">
                                <select id="deposit_hours" name="hours">
                                    @for ($i = 1; $i <= 12; $i++)
                                        <option value="{{ str_pad($i, 2, '0', STR_PAD_LEFT) }}">{{ str_pad($i, 2, '0', STR_PAD_LEFT) }}</option>
                                    @endfor
                                </select>
                            </div>
                            <div class="col-xs-2">
                                <select id="deposit_minutes" name="minutes">
                                    @for ($i = 0; $i <= 59; $i++)
                                        <option value="{{ str_pad($i, 2, '0', STR_PAD_LEFT) }}">{{ str_pad($i, 2, '0', STR_PAD_LEFT) }}</option>
                                    @endfor
                                </select>
                            </div>
                            <div class="col-xs-2">
                                <select id="deposit_range" name="range">
                                    <option value="AM">AM</option>
                                    <option value="PM">PM</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12 nopad wallRowtit">
                        <div class="row">
                            <div class="col-xs-6">
                                <label>{{ Lang::get('public.ReferenceNo') }}*</label>
                            </div>
                            <div class="col-xs-5">
                                <input id="deposit_refno" name="refno" type="text" />
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12 nopad wallRowtit">
                        <div class="row">
                            <div class="col-xs-6">
                                <label>{{ Lang::get('public.DepositReceipt') }}*</label>
                            </div>
                            <div class="col-xs-5">
                                <input class="upload" type="file" name="receipt" id="deposit_receipt">
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12 nopad wallRowtit">

                        <div id="deposit_promolist">
                            {{ Lang::get('public.Loading') }}...
                        </div>

                        <div class="clearfix"></div>

                        <div class="col-md-12">
                            <div class="col-xs-1">
                                <input type="checkbox" id="deposit_rules" name="rules" >
                            </div>
                            <div class="col-xs-11 cut1">
                                <label>{{ Lang::get('public.IAlreadyUnderstandRules') }}</label>
                            </div>
                        </div>

                        <div class="clearfix"></div>
                    </div>
                </div>

                <div class="ht1"></div>

                <div class="row">
                    <div class="col-xs-3">
                        <button type="button" class="btn btn-primary" onclick="deposit_submit();">{{ Lang::get('public.Submit') }}</button>
                    </div>
                    <div class="col-xs-3">
                        <button type="button" class="btn btn-primary" onclick="resetForm('deposit_form');">{{ Lang::get('public.Reset') }}</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
