@extends('royalewin/mobile/master')

@section('title', 'History')

@section('css')
    @parent

    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
@endsection

@section('js')
    @parent

    <script src="{{ asset('/royalewin/resources/js/jquery-ui.js') }}"></script>
    <script type="text/javascript">
        $(function () {
            $(".datepicker").datepicker({dateFormat: 'yy-mm-dd', defaultDate: new Date()});

            setTimeout(bet_history, 4000);
        });

        // Bet history.
        function bet_history() {
            $.ajax({
                type: "POST",
                url: "{{action('User\TransactionController@profitlossProcess')}}",
                data: {
                    _token: "{{ csrf_token() }}",
                    date_from: $('#bet_history_date_from').val(),
                    date_to: $('#bet_history_date_to').val(),
                    record_type: $('#bet_history_record_type').val()
                },
            }).done(function (json) {
                //var str;
                var headStr = '';
                var bodyStr = '';

                headStr += '<thead><tr><th>{{ Lang::get('public.Date') }}</th><th>{{ Lang::get('public.Wager') }}</th><th>{{ Lang::get('public.Stake') }}</th><th>{{ Lang::get('public.WinLose') }}</th><th>{{ Lang::get('public.RealBet') }}</th></tr></thead>';
                obj = JSON.parse(json);
                var count = 0;

                $.each(obj, function (i, item) {
                    bodyStr += '<tr><td style="text-decoration: underline;color:blue;">' + item.date + '</td><td>' + item.wager + '</td><td>' + item.stake + '</td><td>' + item.winloss + '</td><td>' + item.validstake + '</td></tr>';
                    count++;
                });

                if (count <= 0) {
                    bodyStr += '<td colspan="6"><h2><center>{{ Lang::get('public.NoDataAvailable') }}</center></h2></td>';
                }

                bodyStr = '<tbody>' + bodyStr + '</tbody>';

                $('#bet_history').html(headStr + bodyStr);
            });
        }

        function transaction_history_product(date_from, product) {
            $.ajax({
                type: "POST",
                url: "{{action('User\TransactionController@profitlossProcess')}}",
                data: {
                    _token: "{{ csrf_token() }}",
                    date_from: date_from,
                    group_by: product,
                },
            }).done(function (json) {
                //var str;
                var headStr = '';
                var bodyStr = '';

                headStr += '<thead><tr><th>{{ Lang::get('public.Product') }}</th><th>{{ Lang::get('public.Wager') }}</th><th>{{ Lang::get('public.Stake') }}</th><th>{{ Lang::get('public.WinLose') }}</th><th>{{ Lang::get('public.RealBet') }}</th></tr></thead>';
                obj = JSON.parse(json);
                var count = 0;

                $.each(obj, function (i, item) {
                    bodyStr += '<tr><td>' + item.product + '</td><td>' + item.wager + '</td><td>' + item.stake + '</td><td>' + item.winloss + '</td><td style="text-decoration: underline;color:blue;">' + item.validstake + '</td></tr>';
                    count++;
                });

                if (count <= 0) {
                    bodyStr += '<td colspan="6"><h2><center>{{ Lang::get('public.NoDataAvailable') }}</center></h2></td>';
                }

                bodyStr = '<tbody>' + bodyStr + '</tbody>';

                $('#bet_history_product').html(headStr + bodyStr);
            });
        }

        setInterval(function() {
            $("#bet_history_button").trigger("click");
        }, 10000);
        // End bet history.
    </script>
@endsection

@section('content')
    <!-- Page Content -->
    <div class="container">

        @include('royalewin/mobile/include/profile_menu', array('selected' => 'History'))

        <div class="row bdr2 nopad">
            <div class="col-lg-12 nopad">
                <div class="col-lg-12 nopad">
                    <div class="wTit">
                        {{ Lang::get('public.BetHistory') }}
                    </div>
                </div>
            </div>

            <div class="col-lg-12 nopad wallRowtit">
                <div class="row">
                    <div class="col-xs-5">
                        <label>{{ Lang::get('public.DateRange') }}</label>
                    </div>
                    <div class="col-xs-3">
                        <input type="text" class="datepicker" id="bet_history_date_from" style="cursor:pointer;" value="{{ date('Y-m-d') }}"><br>
                    </div>
                    <div class="col-xs-3">
                        <input type="text" class="datepicker" id="bet_history_date_to" style="cursor:pointer;" value="{{ date('Y-m-d') }}">
                    </div>
                </div>
            </div>
        </div>

        <div class="ht1"></div>

        <div class="row">
            <div class="col-xs-3">
                <button id="bet_history_button" type="button" class="btn btn-primary" onClick="bet_history();">{{ Lang::get('public.Submit') }}</button>
            </div>
        </div>

        <div class="ht1"></div>

        <div class="row">
            <div class="table-responsive">
                <table id="bet_history" class="table">
                    <thead>
                    <tr>
                        <th>{{Lang::get('public.Date')}}</th>
                        <th>{{Lang::get('public.Wager')}}</th>
                        <th>{{Lang::get('public.Stake')}} </th>
                        <th>{{Lang::get('public.WinLose')}}</th>
                        <th>{{Lang::get('public.RealBet')}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td colspan="6"><h2><center>{{ Lang::get('public.NoDataAvailable') }}</center></h2></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="ht1"></div>

        <div class="row">
            <div class="table-responsive">
                <table id="bet_history_product" class="table">
                    <thead>
                    <tr>
                        <th>{{Lang::get('public.Product')}}</th>
                        <th>{{Lang::get('public.Wager')}}</th>
                        <th>{{Lang::get('public.Stake')}} </th>
                        <th>{{Lang::get('public.WinLose')}}</th>
                        <th>{{Lang::get('public.RealBet')}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td colspan="6"><h2><center>{{ Lang::get('public.NoDataAvailable') }}</center></h2></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
