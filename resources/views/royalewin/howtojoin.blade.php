@extends('royalewin/master')

@if (Session::get('currency') == 'MYR')
    @section('title', 'Best Online Live Casino Malaysia - How To Join')
    @section('keywords', 'Best Online Live Casino Malaysia - How To Join')
    @section('description', 'Press the \'join now\' button on the site which will bring you to RoyaleWin Online Casino registration form. Simply enter your details and then click \'submit\'.')
@elseif (Session::get('currency') == 'IDR')
    @section('title', 'Bandar Casino Online, Situs Judi Indonesia - Bantuan')
    @section('keywords', 'Bandar Casino Online, Situs Judi Indonesia - Bantuan')
    @section('description', 'Bantuan -RoyaleWin adalah agen bola, bandar casino online dan situs judi online terpercaya untuk Anda penggemar taruhan bola dan pecinta judi online di Indonesia.')
@endif

@section('css')
@parent

<style type="text/css">
    .othersCont a {
        color: #fff;
    }
    
    .othersCont p {
        margin-bottom: 3px;
    }
</style>
@endsection

@section('content')
<div class="midSect bgOthers">
    <div class="contOthers">
        <span>{{ strtoupper(Lang::get('public.HowToJoin')) }}</span>
    </div>
    <div class="othersMenu">
        @include('royalewin.include.helpmenu')
        <div class="clr"></div>
    </div>
    <div class="othersCont">
        {!! htmlspecialchars_decode($content) !!}
    </div>
</div>
@endsection