@extends('royalewin/master')

@if (Session::get('currency') == 'MYR')
    @section('title', 'Banking for Online Casino Malaysia - Deposit/Withdrawal')
    @section('keywords', 'Banking for Online Casino Malaysia - Deposit/Withdrawal')
    @section('description', 'How to Deposit and Withdraw at RoyaleWin Online Casino Malaysia. Get your WELCOME BONUS NOW!')
@elseif (Session::get('currency') == 'IDR')
    @section('title', 'Bandar Casino Online, Situs Judi Indonesia - Banking')
    @section('keywords', 'Bandar Casino Online, Situs Judi Indonesia - Banking')
    @section('description', 'Banking - RoyaleWin adalah agen bola, bandar casino online dan situs judi online terpercaya untuk Anda penggemar taruhan bola dan pecinta judi online di Indonesia.')
@endif

@section('css')
@parent

<style type="text/css">
    .othersCont a {
        color: #fff;
    }
    
    .othersCont p {
        margin-bottom: 3px;
    }
</style>
@endsection

@section('content')
<div class="midSect bgOthers">
    <div class="contOthers">
        <span>{{ strtoupper(Lang::get('public.BankingInformation2')) }}</span>
    </div>
    <div class="othersMenu">
        @include('royalewin.include.helpmenu')
        <div class="clr"></div>
    </div>
    <div class="othersCont">
        {!! htmlspecialchars_decode($content) !!}
    </div>
</div>
@endsection