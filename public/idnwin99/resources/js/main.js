
jQuery(document).ready(function(){
	
	jQuery('dl#tabs4').addClass('enabled').timetabs({
		defaultIndex: 0,
		interval: 7000,
		continueOnMouseLeave: true,
		animated: 'fade',
		animationSpeed: 500
	});

	// animation preview
	jQuery('input[name=animation]').click(function() {
		$this = jQuery(this);
		jQuery.fn.timetabs.switchanimation($this.val());
	});
});



$(document).ready(function() {
			$('.box_skitter_large').skitter({
				theme: 'clean',
				numbers_align: 'center',
				progressbar: false, 
				dots: true, 
				preview: false
			});
		});
