var slotMachine = {
	curBet: 1,
	sounds: {},

	init: function() {
		//$('#betSpinUp').click(function() { slotMachine.change_bet(+1); });
		//$('#betSpinDown').click(function() { slotMachine.change_bet(-1); });
		//$('#spinButton').click(function() { slotMachine.spin(); });

		//$('#soundOffButton').click(function() { slotMachine.toggle_sound(); });

		/*soundManager.url = "js/";
		soundManager.onload = function(){
			slotMachine.sounds['payout'] = soundManager.createSound({id: "payout", url: 'sounds/payout.mp3'});
			slotMachine.sounds['fastpayout'] = soundManager.createSound({id: "fastpayout", url: 'sounds/fastpayout.mp3'});
			slotMachine.sounds['spinning'] = soundManager.createSound({id: "spinning", url: 'sounds/spinning.mp3'});
		};*/

	},

	//----------------------------------------------------

	change_bet: function(delta) {
		slotMachine.curBet += delta;
		slotMachine.curBet = Math.min(Math.max(1, slotMachine.curBet), maxBet);
		slotMachine.show_won_state(false); // Remove won state, so that they can't easily fake a screenshot to say "I bet 2 and got paid off only as 1"

		$('#bet').html(slotMachine.curBet);

		$('#prizes_list .tdPayout').each(function() {
			$(this).html(
				parseInt($(this).attr("data-basePayout"), 10) * slotMachine.curBet
			);
		});
	},

	/*toggle_sound: function() {
		if ($('#soundOffButton').hasClass("off")) {
			soundManager.unmute();
		} else {
			soundManager.mute();
		}
		$('#soundOffButton').toggleClass("off");
	},*/

	//----------------------------------------------------

	spin: function() {
		//var bidsLeft = parseInt($('#totalspinleft').html(), 10);
		
		// Validate that we can spin
		$('#spinAct').attr('onclick','').unbind('click');
		if ($('#spinButton').hasClass("disabled")) { 
			/*if(bidsLeft == 0){
				alert("Insufficient spin credit. Deposit now to gain more free credits.");
			}else{
				alert("Lucky Strike is spinning. Please wait");
			}
			*/
			return false; 
		}
		/*insertCoin(coin);
		if(coin == ''){
			alert("Please insert coin");
			return false;
		}*/
//		insertCoin(coin);

		
		// Clean up the UI
		slotMachine.show_won_state(false);
		
		rBlinkFlag = true;
		
		$('#spinButton').addClass("disabled");

		// Deduct the bet from the number of bids
		//$('#bids_left').html(bidsLeft - slotMachine.curBet);
		$('#'+coin+'Coin').html($('#'+coin+'Coin').html() - 1);

		// Make the reels spin
		slotMachine._start_reel_spin(1);
		slotMachine._start_reel_spin(2);
		slotMachine._start_reel_spin(3);
		
		if(coin == 'g'){
			slotMachine._start_reel_spin(4);
		}
		
		try {
			slotMachine.sounds['spinning'].play();
		} catch(err) {}

		// We need to make the reels end spinning at a certain time, synched with the audio, independently of how long the AJAX request takes.
		// Also, we can't stop until the AJAX request comes back. So we must have a timeout for the first reel stop, and a function that makes
		//   the magic happen, and whatever happens last (this timeout, or the AJAX response) calls this function.
		// The sound timings are at: 917ms, 1492ms and 2060ms, which needs to be adjusted by the animation timings
		//   (which is why i'm setting the first one at 250ms before 917ms)

		var fnStopReelsAndEndSpin = function() {
				// Make the reels stop spinning one by one
			//alert(spinData.forthDigit)
				//if(spinData.forthDigit==true){
				//$("#reel4").removeClass("reel_g").addClass("reel");
				//}else{
				//	$("#reel4").removeClass("reel").addClass("reel_g");
				//}
				
				//if(coin != 'g')
				//	$("#reel4").removeClass("reel").addClass("reel_g");
				
				var baseTimeout = 1000;
				window.setTimeout(function(){ slotMachine._stop_reel_spin(1, spinData.return_code[0]); }, baseTimeout);
				baseTimeout += 575;
				window.setTimeout(function(){ slotMachine._stop_reel_spin(2, spinData.return_code[1]); }, baseTimeout);
				baseTimeout += 568;
				window.setTimeout(function(){ slotMachine._stop_reel_spin(3, spinData.return_code[2]); }, baseTimeout);
				if(coin == 'g'){
					baseTimeout += 1068;
					window.setTimeout(function(){ slotMachine._stop_reel_spin(4, spinData.return_code[3]); }, baseTimeout);
				}
				
				baseTimeout += 700; // This must be related to the timing of the final animation. I'm making it a bit less, so the last reel is still bouncing when it lights up
				window.setTimeout(function(){ slotMachine.end_spin(spinData); }, baseTimeout);
		}

		var FirstReelTimeoutHit = false;
		var spinData = null;
		window.setTimeout(function(){ FirstReelTimeoutHit = true; if (spinData != null) { fnStopReelsAndEndSpin(); } }, 667);	
		$.ajax({
			url: 'u_spin_4d',
			type: "POST",
			data: { bet : slotMachine.curBet, machine_name: machine_name, windowID: windowID, coin: coin , time: time},
			dataType: "json",
			timeout: 10000,
			success: function(data){
				time++;
				spinData = data;
				if(spinData.return_code=='fail' || spinData.return_code=='0'){
					slotMachine.abort_spin_abruptly();
					alert('Spin failed');
					return false;
				}
				if (FirstReelTimeoutHit == true) { fnStopReelsAndEndSpin(); }
			},
			error: function() {
				slotMachine.abort_spin_abruptly();
				$('#failedRequestMessage').show();
			}
		});
	},

	show_won_state: function(bWon, prize_id) {
		if (bWon) {
			//$('#PageContainer').addClass("won");
			//$('#trPrize' + prize_id).addClass("won");
			//slotMachine.showCenter(prize_id);
		} else {
			$("#winAmt").html("");
			//$('.trPrize').removeClass("won");
			//$('#PageContainer').removeClass("won");
			$('#lastWin').html("");
		}
	},
	
	showCenter: function(amt){ 
		$("#winAmt").html(amt);
		$("#trPrize").show();
		$('#trPrize').css("background","transparent url(images/lucky_result.gif) no-repeat");
		var top = Math.max($(window).scrollTop() + $(window).height() / 2 - $("#trPrize")[0].offsetHeight / 2, 0);
		var left = Math.max($(window).scrollLeft() + $(window).width() / 2 - $("#trPrize")[0].offsetWidth / 2, 0);
		$("#trPrize").css('top', top + "px");
		$("#trPrize").css('left', left + "px");
		
	}, 
	getHistory: function(){
		$.ajax({
			url: "u_spin_4d_history",
			type: "POST",
			dataType: "json",
			timeout: 10000,
			success: function(data){
				$("#historyResult").html("");
				var str = '';
				$.each(data.hl, function(index, d){
					str += '<div class="hresult">'+d.l+'</div>';
					str += '<div class="hresult">'+d.dd+'</div>';
					str += '<div class="hresult1">'+d.r+'</div>';
					str += '<div class="hresult">'+d.wc+'</div>';
				});
				$("#historyResult").html($("#historyResult").html() + str);
			}
		});
	},
	showHistory: function(){ 
		slotMachine.getHistory();
		$("#history").show();
		var top = Math.max($(window).scrollTop() + $(window).height() / 2 - $("#history")[0].offsetHeight / 2, 0);
		var left = Math.max($(window).scrollLeft() + $(window).width() / 2 - $("#history")[0].offsetWidth / 2, 0);
		$("#history").css('top', top + "px");
		$("#history").css('left', left + "px");
		
	}, 

	end_spin: function(data) {
		//if (data.exp_info) {update_experience_data(data.exp_info);}  // We're not sending this down anymore, since users don't get experience from spinning

		$("#gp_prizebg").css("background","");
		rBlinkTimer = '';
		
		//if (data.prizes > 0) {
		//	slotMachine.show_won_state(true, data.prizeAmt);
		//	gp = data.gp;
			//slotMachine._increment_payout_counter(data); // _increment_payout_counter will call end_spin_after_payout, which is where this list of things to do at the end really ends
		//}else{
			clearTimeout(blinkTimer);
			for(x = 0; x < 7; x++){
				$("#gp_"+x).css("background","");
				$("#np_"+x).css("background","");
			}
		//}
		slotMachine._end_spin_after_payout(data);
		
		//LOOP FOR TEST DATA SET
		if(loop == '1'){
			to = setTimeout(function(){ $("#trPrize").hide(); slotMachine.spin() },1000);
		}
	},

	// These are the things that need to be done after the payout counter stops increasing, if there is a payout
	_end_spin_after_payout: function(data) {
		//if(data.prizes>0){ displayWinner();loadccl(); }
		//if(data.bids>0){
			$('#spinAct').attr('onclick','spinvalid();');
			$('#spinButton').removeClass("disabled");
			$('#spinButton').css({ "cursor": "pointer" });
		//}else{
		//	$('#spinButton').css({ "cursor": "default" });
		//}

		// This is technically redundant, since the payout incrementer updated them, and we decreased it when spinning,
		//   but just in case something got off sync
		$.ajax({
			url: 'u_spin_4d_count',
			type: 'post',
			dataType:"json",
			success: function (result) {
				$('#ticketCount').html(result.allspin);
				$('#allspin').val(result.allspin);
			}
		});	
		//current bet
		displayCurBet();
	},

	_increment_payout_counter: function(data) {
		var i=0;
		var bidsLeft = data.bids - data.prize.payout;
		var soundName = (data.prize.payout > 80 ? 'fastpayout' : 'payout' );
		var tickDelay = (data.prize.payout > 80 ? 50 : 200 );

		try {
			slotMachine.sounds[soundName].play({ onfinish: function(){ this.play(); }});
		} catch(err) {}
		
		var timerID = window.setInterval(function() {
			i++;
			$('#lastWin').html(i);
			$('#bids_left').html(bidsLeft + i);
			$('#totalspinleft').html(bidsLeft + i);

			if (i >= data.prize.payout) {
				window.clearInterval(timerID);

				try {
					slotMachine.sounds[soundName].stop();
				} catch(err) {}

				slotMachine._end_spin_after_payout(data);
			}
		}, tickDelay);
	},

	abort_spin_abruptly: function() {
		slotMachine._stop_reel_spin(1, eval(Math.floor((Math.random()*8)+1))+0.5);
		slotMachine._stop_reel_spin(2, eval(Math.floor((Math.random()*8)+1))+0.5);
		slotMachine._stop_reel_spin(3, eval(Math.floor((Math.random()*8)+1))+0.5);
		slotMachine._stop_reel_spin(4, eval(Math.floor((Math.random()*8)+1))+0.5);
		//slotMachine.sounds['spinning'].stop();
	},

	// -----------------------------------
	
	_start_reel_spin: function(i) {
		var elReel = $('#reel' + i); // cache for performance
		elReel.css({top: -(Math.random()*1400) }); // Change the initial position so that, if a screenshot is taken mid-spin, reels are mis-aligned
		var curPos = parseInt(elReel.css("top"), 10);

		var fnAnimation = function(){
			elReel.css({top: curPos});
			curPos += 100;
			if (curPos > 0) {curPos = -1456;}
		};
		var timerID = window.setInterval(fnAnimation ,20);
		elReel.data("spinTimer", timerID);
	},

	_stop_reel_spin: function(i, outcome) {
		//alert(i+'--'+outcome)
		var elReel = $('#reel' + i); // cache for performance
		var timerID = elReel.data("spinTimer");
		window.clearInterval(timerID);
		elReel.data("spinTimer", null);

		if (outcome != null) {
			if(i == 1){
				//setBlink(logoMap[outcome-1],1);
			}
			// 726 is the height of the whole strip, which repeats thrice, so we don't have to care about looping
			// 121 is the distance between icon centers, and 86 is kind of empirical...
			// 726->989, 121->123.6, 86->51
			if(outcome < 3){
				nn = 42;
			}else if(outcome >= 3 && outcome<5){
				nn = 50;
			}else if(outcome == 9){
				nn = 75;
			}else if(outcome == 5){
				nn = 60;
			}else if(outcome == 6){
				nn = 65;
			}else{
				nn = 70;
			}
			var finalPosition = -147 -((outcome - 1) * 145) + nn;
			//alert(outcome+'---'+nn+'---'+finalPosition);
			// Animation two: Elastic Easing
			elReel.css({ top: finalPosition - 989 })
				.animate({ top: finalPosition + 200}, 200, 'linear', function() {
					//alert(finalPosition);
					elReel.animate({top: finalPosition}, 1000, 'easeOutElastic');
				});
		}
	}
};

slotMachine.init();
