'use strict';

angular.module('famousAngularStarter')
  .controller('MainCtrl', function ($scope, $famous, $http, $window, $timeout, $interval, ApiCall) {

    var gameApiUrlPrefix = parent.getGameApiUrlPrefix();
    var QueryString = function () {
        // This function is anonymous, is executed immediately and
        // the return value is assigned to QueryString!
        var query_string = {};
        var query = window.location.search.substring(1);
        var vars = query.split("&");
        for (var i=0;i<vars.length;i++) {
            var pair = vars[i].split("=");
            // If first entry with this name
            if (typeof query_string[pair[0]] === "undefined") {
                query_string[pair[0]] = decodeURIComponent(pair[1]);
                // If second entry with this name
            } else if (typeof query_string[pair[0]] === "string") {
                var arr = [ query_string[pair[0]],decodeURIComponent(pair[1]) ];
                query_string[pair[0]] = arr;
                // If third or later entry with this name
            } else {
                query_string[pair[0]].push(decodeURIComponent(pair[1]));
            }
        }
        return query_string;
    }();

    var Transitionable = $famous['famous/transitions/Transitionable'];
    var Timer = $famous['famous/utilities/Timer'];
	var Easing = $famous['famous/transitions/Easing'];
	var Engine = $famous['famous/core/Engine'];
	
	var match_id = 0;
	var ApiReturnCount = 0;
	var crc = QueryString.crc;
	var sid = QueryString.sid;
    var qs = "sid=" + sid + "&crc=" + crc + "&gv=" + QueryString.gv;
	var ww = window.innerWidth;
	var hh = window.innerHeight;
	var sizePercen = 1;
	var runSpeed = 10;
	var runCount = 0;
	var runStatus = "startRun";
	var runTimer;
	var clickBoolean = false;
	var winResult;
	var bonusResult;
	var stopRad;
	var autoBoolean = false;
	var gameStartBoolean = false;
	var totalWinAmt;
	var winnerList;

	var imageDirPath = "images/";

    if (crc == "IDR") {
        imageDirPath = "images_idr/";

        $scope.spinLabel = "PUTAR";
        $scope.autoSpinHelp = "Fungsi<br>Putar Otomatis<br>" +
            "- Putar Otomatis<br>dengan token spin<br>yang tersedia.<br>" +
            "- Otomatis menerima<br>peluang double<br>perputaran.";
	} else {
    	$scope.spinLabel = "SPIN";
    	$scope.autoSpinHelp = "Auto Spin<br>Function<br>" +
            "- Auto spin with your<br>available token.<br>" +
            "- Auto accept<br>double chances<br>round.";
	}

	$scope.prizeLightShow = false;
	$scope.bonusLightShow = false;
	$scope.autoYesShow = false;
	$scope.yesnoShow = false;
	$scope.loadingMsg = "Loading...";
	$scope.loadingShow = true;
	$scope.imgPath = imageDirPath;
	$scope.winIconUrl = imageDirPath + "try2xchance.png";
	$scope.autoBtnImg = imageDirPath + "autoBtn_off.png";
	$scope.prizeArr = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15];//new Array();
	$scope.bonusArr = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15];//new Array();
	$scope.rotateZ = new Transitionable(0);
	$scope.rotateZ2 = new Transitionable(0);
	$scope.prizeRotateZ = new Array();
	$scope.bonushandler = bonusHandler;
	$scope.noBonushandler = resetGame;
	$scope.spin = spinHandler;
	$scope.confirmhandler = confirmHandler;
	$scope.noConfirmhandler = noConfirmHandler;
	// $scope.autoSpin = autoSpinHandler;
	$scope.autoSpin = showConfirmHandler;
	$scope.confirmBoxPos = new Transitionable([0,-1000,20]);
	$scope.resultBoxPos = new Transitionable([0,-1000,20]);
	$scope.winAmt = "";

	prizeRotateArrange();
	resizeHandler();

    createjs.Sound.registerSounds([
    	{src: "sound/win.mp3", id: "winS"},
    	{src: "sound/scratch.mp3", id: "spinS"}
	]);

    $timeout(getWheelPrize, 300);
    $timeout(getTokenBalance, 1000);
    $timeout(getMatchId, 1500);

    function getWheelPrize() {
        callAPI(gameApiUrlPrefix + "fortune-wheel/get_wheel_prize?" + qs);
	}

	function getTokenBalance() {
        callAPI(gameApiUrlPrefix + "fortune-wheel/get_token_balance?" + qs);
	}

	function getMatchId() {
        callAPI(gameApiUrlPrefix + "fortune-wheel/get_match_id?" + qs);
	}
	
	function prizeRotateArrange(){
		var rad;
		for(var i=0;i<16;i++){
			rad = new Transitionable([0.392699375*i]);
			$scope.prizeRotateZ.push(rad);
		}
	}
	
	function resizePercen(){
		ww = window.innerWidth;
		hh = window.innerHeight;
		if(ww>800){ww=800};
		
		if(ww < 800){
			sizePercen = ww/800;
		}else{
			sizePercen = 1;
		}
	}
	
	function resizeHandler(){
		resizePercen();
		$scope.iconSize = [(400*sizePercen),(400*sizePercen)];
		$scope.yesnoSize = [(108*sizePercen),(108*sizePercen)];
		$scope.tokenBoxSize = new Transitionable([(250*sizePercen),(72*sizePercen)]);
		$scope.winAmtPos = new Transitionable([0,0*sizePercen,23]);
		$scope.yesPos = new Transitionable([-220*sizePercen,200*sizePercen,21]);
		$scope.noPos = new Transitionable([180*sizePercen,200*sizePercen,21]);
		$scope.prizePos = new Transitionable([0,-(190*sizePercen),6]);
		$scope.bonusPos = new Transitionable([0,-(110*sizePercen),6]);
		$scope.wheelPos = new Transitionable([(19*sizePercen),-(hh*0.5)+((560*sizePercen)*0.5)+20,1]);
		$scope.blinkPos = new Transitionable([(-25*sizePercen),(-2*sizePercen),1]);
		$scope.logoPos = new Transitionable([(-26*sizePercen),0,15]);
		$scope.lightPos1 = new Transitionable([(-26*sizePercen),(-4*sizePercen),6]);
		$scope.lightPos2 = new Transitionable([0,-((185)*sizePercen),6]);
		$scope.lightPos3 = new Transitionable([0,-((67)*sizePercen),6]);
		$scope.stagePos = new Transitionable([0,-(hh*0.5)+(560*sizePercen)+((199*sizePercen)*0.5)+20,0]);
		$scope.tokenBoxPos = new Transitionable([0,-(hh*0.5)+(560*sizePercen)+20,2]);
		$scope.spinBtnPos = new Transitionable([-((299*sizePercen)*0.55),-(hh*0.5)+(560*sizePercen)+((199*sizePercen)*0.35)+20,2]);
		$scope.autoBtnPos = new Transitionable([(299*sizePercen)*0.55,-(hh*0.5)+(560*sizePercen)+((199*sizePercen)*0.35)+20,2]);
		if($scope.confirmBoxPos.get()[1] > -900){
			$scope.confirmBoxPos = new Transitionable([0,-(hh*0.5)+((560*sizePercen)*0.5)+20,20]);
		}
		if($scope.resultBoxPos.get()[1] > -900){
			$scope.resultBoxPos = new Transitionable([0,-(hh*0.5)+((560*sizePercen)*0.5)+20,20]);
		}
		$scope.stageStyle = {"width":(800*sizePercen)+"px","height":(199*sizePercen)+"px"};
		$scope.wheelStyle = {"width":(658*sizePercen)+"px","height":(560*sizePercen)+"px"};
		$scope.blinkStyle = {"width":(512*sizePercen)+"px","height":(514*sizePercen)+"px"};
		$scope.logoStyle = {"width":(124*sizePercen)+"px","height":(124*sizePercen)+"px"};
		$scope.lightStyle = {"width":(94*sizePercen)+"px","height":(101*sizePercen)+"px"};
		$scope.lightStyle2 = {"width":(56*sizePercen)+"px","height":(140*sizePercen)+"px"};
		$scope.prizeStyle = {"width":(60*sizePercen)+"px","height":(60*sizePercen)+"px"};
		$scope.bonusStyle = {"width":(40*sizePercen)+"px","height":(40*sizePercen)+"px"};
		$scope.spinBtnStyle = {'width':(299*sizePercen)+'px','height':(115*sizePercen)+'px','cursor':'pointer'};
		$scope.autoBtnStyle = {'width':(299*sizePercen)+'px','height':(115*sizePercen)+'px','cursor':'pointer'};
		$scope.resultBgStyle = {'width':(425*sizePercen)+'px','height':(434*sizePercen)+'px'};
		$scope.confirmBgStyle = {'width':(475*sizePercen)+'px','height':(484*sizePercen)+'px'};
		$scope.tokenBoxStyle = {'width':(250*sizePercen)+'px','height':(72*sizePercen)+'px'};
		$scope.tokenBgStyle = {'width':(250*sizePercen)+'px','height':(72*sizePercen)+'px',color:'#CBCBCB','font-size': (35*sizePercen)+'px','justify-content': 'center','display':'flex','align-items':'center'};
		$scope.winAmtStyle = {'width':(400*sizePercen)+'px','height':(150*sizePercen)+'px','font-size': (85*sizePercen)+'px'};
		$scope.confirmAutoStyle = {'width':(400*sizePercen)+'px','height':(150*sizePercen)+'px','font-size': (40*sizePercen)+'px','text-align':'center'};
	}
	
	function spinHandler(){
		if(clickBoolean) return;
		clickBoolean = true;
		if($scope.userToken <=0){
			//display no token message
			$scope.winAmt = "No Token!";
			$scope.winIconUrl = imageDirPath + "blank.png";
			$scope.resultBoxPos.set([0,-(hh*0.5)+((560*sizePercen)*0.5)+20,20]);

            if (autoBoolean) {
                autoSpinHandler();
            }

			$timeout(removeResultBox, 1500);
            $timeout(resetGame, 1500);
			return;
		}
		$scope.prizeLightShow = true;
		gameStartBoolean = true;
		$scope.userToken -= 1;
		runStatus = "startRun";
		runTimer = $interval(prizeLightRunning, 30);
		var getResultParam = gameApiUrlPrefix + "fortune-wheel/get_prize_result?" + qs + "&mid=" + match_id;
		callAPI(getResultParam);
	}
	
	function removeResultBox(){
		clickBoolean = false;
		$scope.resultBoxPos.set([0,-1000,20]);
	}
	
	function prizeLightRunning(){
		runCount ++;
		if(runStatus == "startRun"){
			if(runCount == runSpeed){
				runCount = 0;
				$scope.rotateZ.set($scope.rotateZ.get() +0.392699375);
				if($scope.rotateZ.get() > 6.28319)$scope.rotateZ.set($scope.rotateZ.get()-6.28319); 
				if(runSpeed > 1){
					runSpeed --;
				}else{
					if(stopRad == Math.round($scope.rotateZ.get()*1000))runStatus = "stopRun";
				}
				$scope.$apply();
			}
		}else if(runStatus == "stopRun"){
			if(runCount == runSpeed){
				runCount = 0;
				$scope.rotateZ.set($scope.rotateZ.get() +0.392699375);
				if($scope.rotateZ.get() > 6.28319)$scope.rotateZ.set($scope.rotateZ.get()-6.28319); 
				if(runSpeed < 10){
					runSpeed ++;
				}else{
					$interval.cancel(runTimer);
					$timeout(prizeResultDisplay, 1000);
				}
				$scope.$apply();
			}
		}
	}
	
	function bonusLightRunning(){
		runCount ++;
		if(runStatus == "startRun"){
			if(runCount == runSpeed){
				runCount = 0;
				$scope.rotateZ2.set($scope.rotateZ2.get() +0.392699375);
				if($scope.rotateZ2.get() > 6.28319)$scope.rotateZ2.set($scope.rotateZ2.get()-6.28319); 
				if(runSpeed > 1){
					runSpeed --;
				}else{
					if(stopRad == Math.round($scope.rotateZ2.get()*1000))runStatus = "stopRun";
				}
				$scope.$apply();
			}
		}else if(runStatus == "stopRun"){
			if(runCount == runSpeed){
				runCount = 0;
				$scope.rotateZ2.set($scope.rotateZ2.get() +0.392699375);
				if($scope.rotateZ2.get() > 6.28319)$scope.rotateZ2.set($scope.rotateZ2.get()-6.28319); 
				if(runSpeed < 10){
					runSpeed ++;
				}else{
					$interval.cancel(runTimer);
					$timeout(bonusResultDisplay, 1000);
				}
				$scope.$apply();
			}
		}
	}
	
	function checkWinPosition(){
		var stopNo;
		var prizeWinArr = new Array();
		var rand;
		for(var i=0;i<16;i++){
			if($scope.prizeArr[i].prize_id == winResult.prize_id){
				prizeWinArr.push(i);
			}
		}
		rand = Math.floor(Math.random()*prizeWinArr.length);
		stopNo = prizeWinArr[rand]-10;
		if(stopNo<0)stopNo+=16;
		stopRad = Math.round((stopNo*0.392699375)*1000);
	}
	
	function checkBonusWinPosition(){
		var stopNo;
		var bonusWinArr = new Array();
		var rand;
		for(var i=0;i<16;i++){
			if($scope.bonusArr[i].bonus_id == bonusResult.bonus_id){
				bonusWinArr.push(i);
			}
		}
		rand = Math.floor(Math.random()*bonusWinArr.length);
		stopNo = bonusWinArr[rand]-10;
		if(stopNo<0)stopNo+=16;
		stopRad = Math.round((stopNo*0.392699375)*1000);
	}
	
	function prizeResultDisplay(){
		$scope.resultBoxPos.set([0,-(hh*0.5)+((560*sizePercen)*0.5)+20,20],{duration: 0},winSoundPlay);
		if(autoBoolean){
			if(winResult.prize_bonus_spin == 1){
                $scope.autoYesShow = true;
				$timeout(bonusHandler, 1500);
			}else{
				$timeout(resetGame, 1500);
			}
			return;
		} else {
            $scope.autoYesShow = false;
        }
		if(winResult.prize_bonus_spin == 1 && $scope.userToken > 0){
			$timeout(showDoubleScreen, 1500);
		}else {
			$timeout(resetGame, 1500);
		}
	}
	
	function bonusResultDisplay(){
		$scope.resultBoxPos.set([0,-(hh*0.5)+((560*sizePercen)*0.5)+20,20],{duration: 0},winSoundPlay);
		if(autoBoolean){
			if(bonusResult.bonus_bonus_spin == 1){
                $scope.autoYesShow = true;
				$timeout(bonusHandler, 1500);
			}else {
				$timeout(resetGame, 1500);
			}
			return;
		} else {
            $scope.autoYesShow = false;
		}
		if(bonusResult.bonus_bonus_spin == 1 && $scope.userToken > 0){
			$timeout(showDoubleScreen, 1500);
		}else {
			$timeout(resetGame, 1500);
		}
	}
	
	function winSoundPlay(){
		//createjs.Sound.play('winS');
	}
	
	function showDoubleScreen(){
		$scope.winIconUrl = imageDirPath + "try2xchance.png";
		$scope.yesnoShow = true;
		$scope.winAmt = "";
		parent.updateTotalWin(totalWinAmt, winnerList);
	}
	
	function bonusHandler(){
		$scope.yesnoShow = false;
		$scope.resultBoxPos.set([0,-1000,6]);
		if($scope.userToken <=0){
			//display no token message
			$scope.winAmt = "No Token!";
			$scope.winIconUrl = imageDirPath + "blank.png";
			$scope.resultBoxPos.set([0,-(hh*0.5)+((560*sizePercen)*0.5)+20,20]);

            if (autoBoolean) {
                autoSpinHandler();
            }

			$timeout(removeResultBox, 1500);
            $timeout(resetGame, 1500);
			return;
		}
		$scope.bonusLightShow = true;
		runSpeed = 10;
		runCount = 0;
		stopRad = null;
		bonusResult = null;
		runStatus = "startRun";
		runTimer = $interval(bonusLightRunning, 30);
		var getBonusResultParam = gameApiUrlPrefix + "fortune-wheel/get_bonus_result?" + qs + "&mid=" + match_id;
		callAPI(getBonusResultParam);
	}
	
	function resetGame(){
		$scope.prizeLightShow = false;
		$scope.bonusLightShow = false;
		$scope.autoYesShow = false;
		$scope.yesnoShow = false;
		$scope.winAmt = "";
		$scope.resultBoxPos.set([0,-1000,6]);
		runSpeed = 10;
		runCount = 0;
		stopRad = null;
		winResult = null;
		bonusResult = null;
		ApiReturnCount = 2;
		$scope.loadingShow = true;
		var getMatchIDParam = gameApiUrlPrefix + "fortune-wheel/get_match_id?" + qs;
		callAPI(getMatchIDParam);
		clickBoolean = false;
		parent.updateTotalWin(totalWinAmt, winnerList);
	}

	function noConfirmHandler(){
        $scope.yesnoShow = false;
        $scope.confirmBoxPos.set([0,-1000,6]);
	}

      function confirmHandler(){
          noConfirmHandler();
          autoSpinHandler();
      }

    function showConfirmHandler(){
      	if (!clickBoolean) {
            if (!autoBoolean) {
                $scope.yesnoShow = true;
                $scope.confirmBoxPos.set([0,-(hh*0.5)+((560*sizePercen)*0.5)+20,20]);
            } else {
                autoSpinHandler();
            }
		} else if (autoBoolean) {
            autoBoolean = false;
            $scope.autoBtnImg = imageDirPath + "autoBtn_off.png";
		}
	}
	
	function autoSpinHandler(){
		if(!autoBoolean){
            $scope.autoBtnImg = imageDirPath + "autoBtn_on.png";
            autoBoolean = true;
			if(!gameStartBoolean){
				spinHandler();
			}
		}else{
			autoBoolean = false;
			$scope.autoBtnImg = imageDirPath + "autoBtn_off.png";
		}
	}
	
	function callBekFunc(param){
		ApiReturnCount++;
		//alert(param.action+" = "+JSON.stringify(param));
		if(param == "" || param == 'undefined' || param == 'null'){
			$scope.loadingImg = imageDirPath + "errorIcon.png";
			$scope.loadingMsg = "Connection Error !";
			$scope.loadingShow = true;
			return;
		}
		if(param.error != 0){
			$scope.loadingImg = imageDirPath + "errorIcon.png";
			$scope.loadingMsg = param.error;
			$scope.loadingShow = true;
			return;
		}
		switch(param.action){
			case '1000':
				$scope.prizeArr = param.prize;
				$scope.bonusArr = param.bonus;
				break;
			case '3000':
				$scope.userToken = param.user_token;
				break;
			case '4100':
				match_id = param.match_id;
				totalWinAmt = param.total_won;
				winnerList = param.winner_list
                $scope.userToken = param.user_token;
				parent.updateTotalWin(totalWinAmt, winnerList);
				break;
			case '4200':
				//alert(JSON.stringify(param));
				$scope.userToken = param.user_token;
				winResult = param.result;
				//$scope.winIconUrl = param.result.prize_img_big;
				$scope.winIconUrl = imageDirPath + "totalwin.png";
                match_id = param.match_id;
				if (param.match_won != "0") {
                    $scope.winAmt = param.match_won;
				} else {
                    $scope.winIconUrl = imageDirPath + "tryagain.png";
				}
				checkWinPosition();
				totalWinAmt = param.total_won;
				break;
			case '4300':
				//alert(JSON.stringify(param));
				$scope.winAmt = "";
                $scope.userToken -= 1;
				bonusResult = param.result;
                $scope.winIconUrl = imageDirPath + "totalwin.png";
                if (bonusResult.bonus_bonus_spin == 1) {
                    $scope.userToken = param.user_token;
				}
                if (param.match_won != "0") {
                    $scope.winAmt = param.match_won;
                } else {
                    $scope.winIconUrl = imageDirPath + "tryagain.png";
                }
				checkBonusWinPosition();
				totalWinAmt = param.total_won;
				break;
		}
		if(ApiReturnCount == 3){
			$scope.loadingShow = false;
			if(!autoBoolean){
				gameStartBoolean = false;
			}else {
				if($scope.userToken <=0){
					//display no token message
					$scope.winAmt = "No Token!";
					$scope.winIconUrl = imageDirPath + "blank.png";
					$scope.resultBoxPos.set([0,-(hh*0.5)+((560*sizePercen)*0.5)+20,20]);

                    if (autoBoolean) {
                        autoSpinHandler();
					}

					$timeout(removeResultBox, 1500);
                    $timeout(resetGame, 1500);
					return;
				}
				spinHandler();
			}
		}
	}
	
	function callAPI(param){
		ApiCall.PostApiCall('api/api_init.php',param,callBekFunc);
	}
	
	Engine.on('resize', function() {
	    resizeHandler();
		$scope.$apply();
	});
	
  });