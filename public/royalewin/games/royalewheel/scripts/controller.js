var app = angular.module('lobby', []);
app.controller('prizelists', function($scope, $timeout, ApiCall, $interval) {
	
    var gameApiUrlPrefix = getGameApiUrlPrefix();
    var QueryString = function () {
        // This function is anonymous, is executed immediately and
        // the return value is assigned to QueryString!
        var query_string = {};
        var query = window.location.search.substring(1);
        var vars = query.split("&");
        for (var i=0;i<vars.length;i++) {
            var pair = vars[i].split("=");
            // If first entry with this name
            if (typeof query_string[pair[0]] === "undefined") {
                query_string[pair[0]] = decodeURIComponent(pair[1]);
                // If second entry with this name
            } else if (typeof query_string[pair[0]] === "string") {
                var arr = [ query_string[pair[0]],decodeURIComponent(pair[1]) ];
                query_string[pair[0]] = arr;
                // If third or later entry with this name
            } else {
                query_string[pair[0]].push(decodeURIComponent(pair[1]));
            }
        }
        return query_string;
    }();

    var sid = QueryString.sid;
    var mid = QueryString.mid;
    var crc = QueryString.crc;
    var gv = '1.15';
    var currency = (crc == 'MYR' ? 'RM' : crc);
	var qs = "sid=" + sid + "&crc=" + crc + "&gv=" + gv;

	$scope.currencyPost = "";
	$scope.currency = currency;
    $scope.gameUrl = "www/index.html?" + qs + "&mid=" + mid;

    var InitParam = gameApiUrlPrefix + "fortune-wheel/get_wheel_data?" + qs;
	callAPI(InitParam,callBekFunc);
	
	$scope.totalWinAmt = currency + " 0";
	$scope.updateTotalWinAmt = updateWinAmt;

    var rootImageDirPath = "imgs/";
    var imageDirPath = "www/images/";

    if (crc == "IDR") {
        rootImageDirPath = "imgs_idr/";
        imageDirPath = "www/images_idr/";

        $scope.currencyPost = "K";
    }

    $scope.imgPath = rootImageDirPath;

	$scope.loadingMsg = "Loading...";
	$scope.loadingShow = true;
	$scope.loadingImg = imageDirPath + "loadingGif.gif";
	
	var jackpotTimer = $interval(function () {
		if (Math.floor((Math.random() * 10) + 1) >= 4) {
            $scope.jackpot = $scope.jackpot + Math.floor((Math.random() * 100) + 10);
		} else {
            $scope.jackpot = $scope.jackpot - Math.floor((Math.random() * 100) + 10);
		}
	},3000);

	function callBekFunc(param){
		//alert(JSON.stringify(param));
		if(param == "" || param == 'undefined' || param == 'null'){
			$scope.loadingImg = imageDirPath + "errorIcon.png";
			$scope.loadingMsg = "Connection error!";
			$scope.loadingShow = true;
			return;
		}
		if(param.error != 0){
			$scope.loadingImg = imageDirPath + "errorIcon.png";
			$scope.loadingMsg = param.error;
			$scope.loadingShow = true;
			return;
		}
		$scope.prizeArr = param.prize_list;
		$scope.winnerArr = param.winner_list;
		$scope.prizeHeight = 455/param.prize_list.length;
		$scope.jackpot = param.jackpot_amount;
		$scope.loadingShow = false;
	}
	
	function updateWinAmt(amt){
		$scope.totalWinAmt = amt;
		$scope.$apply();
	}

	function callAPI(param,callbek){
		ApiCall.PostApiCall('api/api_init.php',param,callbek);
	}

	function confirmClaimCallbek(param) {
		if (param.error != 0) {
			alert(param.error);
		} else {
			alert(param.message);
		}

        updateTotalWin(currency + " 0");
	}

    function updateWinnerListFn() {
        callAPI(getGameApiUrlPrefix() + "fortune-wheel/get_winner_list?" + qs, updateWinnerList);
    }

	$scope.confirmClaim = function () {
        if (confirm("Confirm transfer all credit to main wallet?")) {
            callAPI(gameApiUrlPrefix + "fortune-wheel/claim_total_won?" + qs,confirmClaimCallbek);
        }
	}
});

function updateTotalWin(amt, winnerList){
	var result = document.getElementsByClassName("main_wrapper");
	angular.element(result).scope().updateTotalWinAmt(amt);

    var scope = angular.element(document.getElementById("lobby")).scope();

    if(winnerList == "" || winnerList == 'undefined' || winnerList == 'null'){
        // Do nothing.
    } else {
        scope.winnerArr = winnerList;
    }
}

function getGameApiUrlPrefix(){
	return 'http://esgame.rwbola.com/';
}