<?php namespace App\Extensions;

use Illuminate\Validation\Validator as Validator;
use App\Models\Account;
use App\Models\Accountdetail;

class CustomValidator extends Validator
{

    public function validateReservedStr($attribute, $str)
    {
		$reserved = array('admin', 'root', 'moderator', 'ae88', 'royalewin', 'infiniwin');
        foreach ($reserved as $val) {
            if (stristr($str, $val))
                return false;
        }
        return true;
    }
    
    public function validatePhone($attribute, $str)
	{ 
		return preg_match("/^([0-9\s\-\+\(\)]*)$/", $str); 
	} 

}
