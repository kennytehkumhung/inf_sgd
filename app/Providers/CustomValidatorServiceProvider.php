<?php namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Extensions\CustomValidator as CustomValidator;

//use Validator;

/**
 * DouyasiValidator 扩展自定义验证类 服务提供者
 *
 * @author raoyc<raoyc2009@gmail.com>
 */
class CustomValidatorServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app['validator']->extend('decimal', function ($attribute, $value, $parameters)
        {
            if(!preg_match('/^[0-9]+(\.[0-9]{1,2})?$/', $value))
				return false;
			else
				return true;
        });  

		$this->app['validator']->extend('ReservedStr', function ($attribute, $value, $parameters)
        {
			$reserved = array('admin', 'root', 'moderator', 'ae88', 'royalewin', 'infiniwin');
			foreach ($reserved as $val) {
				if (stristr($value, $val))
					return false;
			}
			return true;
        }); 
		
		$this->app['validator']->extend('Phone', function ($attribute, $value, $parameters)
        {
			return preg_match("/^([0-9\s\-\+\(\)]*)$/", $value); 
        }); 
	
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
