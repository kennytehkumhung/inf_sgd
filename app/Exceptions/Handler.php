<?php

namespace App\Exceptions;

use Exception;
use Response;
use Request;
use Config;
use Redirect;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use App\Models\Errorlog;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        HttpException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $e
     * @return void
     */
    public function report(Exception $e)
    {
        return parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
		if ($e instanceof \Symfony\Component\HttpKernel\Exception\NotFoundHttpException || 
			$e instanceof \Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException
			)
		{
                    if($request->path() == 'my/keputusan-4d-results'){
                        return redirect('my/live-4d-malaysia-results');
                    }
                    if($request->path() == 'my/sports'){
                        return redirect('my/sports-ibc');
                    }
                    if($request->path() == 'my/ctb'){
                        return redirect('my/horse-racing');
                    }
				//return response()->view('404', [], 404);
		}
		else
		{	
			$error_log = new Errorlog;
			$error_log->error = Request::url().'|'.$e;
			$error_log->save();
		}

		if (!env('APP_DEBUG')) {
            return response()->view('404', [], 404);
        }
	
		return parent::render($request, $e);

    }
}
