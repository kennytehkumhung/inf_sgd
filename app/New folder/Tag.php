<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model {

	protected $table = 'tag';
	 
	const CREATED_AT = 'created';

    const UPDATED_AT = 'modified';

}
