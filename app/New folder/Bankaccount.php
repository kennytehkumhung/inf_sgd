<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bankaccount extends Model {

	protected $table = 'bankaccount';
	 
	const CREATED_AT = 'created';

    const UPDATED_AT = 'modified';

	function getStatusColorCode(){
		if($this->id == 0) return false;

		if($this->status == CBO_STANDARDSTATUS_ACTIVE)
		return '#C6FFD9';
		else if($this->status == CBO_STANDARDSTATUS_SUSPENDED)
		return '#FFCCCC';
	}
	
	
	function getAllAsOptions($bnkid = false, $deposit = false, $withdraw = false, $bhdid = false, $currency = false) {
		
		$channels = array();
		
		$condition = '1';
		
		if($bnkid)    $condition .= ' AND bnkid='.$bnkid;
		if($deposit)  $condition .= ' AND deposit=1';
		if($withdraw) $condition .= ' AND withdraw=1';
		if($currency) $condition .= ' AND crccode = "'.$currency.'"';
		if($bhdid)    $condition .= ' AND bhdid = "'.$bhdid.'"';
		
		if($objects = self::whereRaw($condition)->get() ) {
	
			$name = 'shortname';
			$bankname = '';
			foreach($objects as $object) {
				if($bnkObj = Bank::whereRaw($object->bnkid)->first() )
				$bankname = $bnkObj->$name;
				$channels[$object->id] = $object->bankaccname.' ['.$bankname.' '.substr($object->bankaccno, -4, 4).']';
			}
		}
		if (count($channels) > 0) return $channels;
		else return false;
	}
	
	public static function getStatusOptions(){
		
		$status = array(
			CBO_ACCOUNTSTATUS_ACTIVE 			=> Lang::get('COMMON.ACTIVE'),
			CBO_ACCOUNTSTATUS_SUSPENDED 		=> Lang::get('COMMON.SUSPENDED'),
		);

		return $status;
	}
}
