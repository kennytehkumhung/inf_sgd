<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rejectreason extends Model {

	protected $table = 'rejectreason';
	 
	const CREATED_AT = 'created';

    const UPDATED_AT = 'modified';
	
	public static function getAllReasonAsOptions() {
		$obj = new self;
		$reasons = array();
		if($objects = self::whereRaw('status = 1')->get()) {
			foreach($objects as $object) {
				$reasons[$object->desc] = $object->desc;
			}
		}
		if (count($reasons) > 0) return $reasons;
		else return false;
	}
	

	function getStatusColorCode(){
		if($this->id == 0) return false;

		if($this->status == CBO_STANDARDSTATUS_ACTIVE)
		return '#C6FFD9';
		else if($this->status == CBO_STANDARDSTATUS_SUSPENDED)
		return '#FFCCCC';
	}

}
