<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Lang;

class Bank extends Model {

	protected $table = 'bank';
	 
	const CREATED_AT = 'created';

    const UPDATED_AT = 'modified';
	
	static function getActiveBankByCurrency($crccode = '') {

		$array = array();
		if($crccode == '') $crccode = 0;
		
		if ($banks = self::whereRaw('status = 1 AND currencycode="'.$crccode.'"')->get() ) {
			$array = array();
			foreach($banks as $bank) {
				$array[$bank->id] = $bank->name;
			}
		}
		
		return $array;
	}

	static function getAddress(){
		if($this->id > 0){
			$address = $this->addr1;
			return $address;
		}
		return false;
	}

	static function getFundingMethodAsOptions(){
		$array = array();
		foreach($fundmethods as $fundmethod) {
			if(strpos($bank->currencycode, $crccode)){
				$array[$bank->id] = $bank->code;
			}
		}
		if(count($array) > 0)
		return $array;
	}
	
	static function getStatusColorCode(){
		if($this->id == 0) return false;

		if($this->status == CBO_STANDARDSTATUS_ACTIVE)
		return '#C6FFD9';
		else if($this->status == CBO_STANDARDSTATUS_SUSPENDED)
		return '#FFCCCC';
	}
	
	static function getAllAsOptions($shortname = false, $currency = false) {
		
		$currencies = array();
		if($objects = self::whereRaw('status=1')->get() ) {
			$name = 'name';
			if($shortname) $name = 'shortname';
			foreach($objects as $object) {
				$bank_name = $object->$name;
				if($currency) $bank_name = '['.$object->currencycode.'] '.$bank_name;
				$currencies[$object->id] = $bank_name;
			}
		}
		if (count($currencies) > 0) return $currencies;
		else return false;
	}
	
	public static function getStatusOptions(){
		
		$status = array(
			CBO_ACCOUNTSTATUS_ACTIVE 			=> Lang::get('COMMON.ACTIVE'),
			CBO_ACCOUNTSTATUS_SUSPENDED 		=> Lang::get('COMMON.SUSPENDED'),
		);

		return $status;
	}
	
	public static function getStatusText($status = null){
		if($status == null && $this->id != 0) $status = $this->status;

		if($status == CBO_PROMOCASHSTATUS_PENDING)
		return Lang::get('COMMON.PENDING');
		else if($status == CBO_PROMOCASHSTATUS_CONFIRMED)
		return Lang::get('COMMON.CONFIRMED');

	}

}
