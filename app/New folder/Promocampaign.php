<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\libraries\App;
use DB;
use App\Models\Config;
use Lang;

class Promocampaign extends Model {

	protected $table = 'promocampaign';
	 
	const CREATED_AT = 'created';

    const UPDATED_AT = 'modified';
	
	const CBO_METHOD_FIRSTTIMEDEPOSIT 	= 1;
	const CBO_METHOD_NEWMEMBER 			= 2;
	const CBO_METHOD_OLDMEMBER 			= 3;
	const CBO_METHOD_DAILYDEPOSIT 		= 4;
	const CBO_METHOD_DEPOSITREBATE 		= 5;
	const CBO_METHOD_AFFFIRSTDEPOSIT 	= 6;
	
	const CBO_TYPE_PERCENTAGE 			= 1;
	const CBO_TYPE_CASH 				= 2;
	
	public static function getPromoByMemberId($accid) {
		
		$object = new self;
		$pmcObj = new Promocash;
		$clgObj = new Cashledger;


		$crccode = '';
		if( $accObj = Account::find($accid) )
		$crccode = $accObj->crccode;
		
		$list = array();
		
		//first welcome bonus
		$now = App::now();
	
		if($accObj->created >= '2014-09-16 00:00:00') {

				if( !Cashledger::where( 'accid' , '=' , $accid )->where( 'chtcode' , '=' , CBO_CHARTCODE_DEPOSIT )->whereRaw( '(status='.CBO_LEDGERSTATUS_CONFIRMED.' OR status='.CBO_LEDGERSTATUS_MANCONFIRMED.')'  )->count() ) {
					if( !Promocash::where( 'accid' , '=' , $accid )->where( 'status' , '=' , CBO_STANDARDSTATUS_ACTIVE )->count() ) {
						if( $promos = self::whereRaw('crccode = "'.$crccode.'" AND pcpid = 0 AND startdate <= "'.$now.'" AND enddate >= "'.$now.'" AND method = '.self::CBO_METHOD_FIRSTTIMEDEPOSIT.' AND status = '.CBO_STANDARDSTATUS_ACTIVE)->get() ) {
							foreach($promos as $promo) {
								$list[$promo->id] = array(
									'code'	=> $promo->code,
									'name'	=> $promo->name,
									'desc'	=> $promo->desc,
									'min'	=> $promo->mindeposit,
								);
							}
						}
					}
				}
			}
		

			if( $pmcObj = DB::select( DB::raw( 
			'
				select  id as campaignid from cashledger where accid = '.$accid.' and chtcode = '.CBO_CHARTCODE_DEPOSIT.' and status IN ('.CBO_LEDGERSTATUS_CONFIRMED.','.CBO_LEDGERSTATUS_MANCONFIRMED.') and refobj = "PromoCashObject" and refid IN(
				select cash.id from promocampaign campaign, promocash cash 
				where campaign.id = cash.pcpid 
				and campaign.crccode = "'.$crccode.'" 
				and campaign.method = '.self::CBO_METHOD_FIRSTTIMEDEPOSIT.' 
				and cash.accid = '.$accid.'
				and cash.status = '.CBO_STANDARDSTATUS_ACTIVE.') 
			')) ) {
					if($promos = self::whereRaw('crccode = "'.$crccode.'" AND pcpid = '.$pmcObj->campaignid.' AND startdate <= "'.$now.'" AND enddate >= "'.$now.'" AND method = '.self::CBO_METHOD_FIRSTTIMEDEPOSIT.' AND status = '.CBO_STANDARDSTATUS_ACTIVE)->get() ) {
						foreach($promos as $promo) {
							$list[$promo->id] = array(
								'code'	=> $promo->code,
								'name'	=> $promo->name,
								'desc'	=> $promo->desc,
								'min'	=> $promo->mindeposit,
							);
						}
					}
			}
			
			
			//old member bonus
			if($promos = self::whereRaw('crccode = "'.$crccode.'" AND startdate <= "'.$now.'" AND enddate >= "'.$now.'" AND method = '.self::CBO_METHOD_OLDMEMBER.' AND status = '.CBO_STANDARDSTATUS_ACTIVE)->get() ) {
				foreach($promos as $promo) {
					$promoObj = Promocash::whereRaw('accid='.$accid.' AND pcpid='.$promo->id)->first();
					if($promoObj) {
						$list[$promo->id] = array(
							'code'	=> $promo->code,
							'name'	=> $promo->name,
							'desc'	=> $promo->desc,
							'min'	=> $promo->mindeposit,
						);
					}
				}
			}
			
			//daily deposit bonus
			if($promos = self::whereRaw('crccode = "'.$crccode.'" AND startdate <= "'.$now.'" AND enddate >= "'.$now.'" AND method = '.self::CBO_METHOD_DAILYDEPOSIT.' AND status = '.CBO_STANDARDSTATUS_ACTIVE)) {
				foreach($promos as $promo) {
					$promoObj = Promocash::whereRaw('accid='.$accid.' AND pcpid='.$promo->id.' AND status= '.CBO_PROMOCASHSTATUS_CONFIRMED.' AND created >= "'.$starttime.'"')->first();
					$starttime = date('Y-m-d').' 00:00:00';
					if(!$promoObj) {
						$list[$promo->id] = array(
							'code'	=> $promo->code,
							'name'	=> $promo->name,
							'desc'	=> $promo->desc,
							'min'	=> $promo->mindeposit,
						);
					}
				}
			}

		
		return $list;
	}	
	
	public static function getPromoDetail() {
		if($this->id <= 0) return false;
		
		if($this->type == self::CBO_TYPE_PERCENTAGE) {
			return $this->amount .'%';
		} else if($this->type == self::CBO_TYPE_CASH) {
			return Config::getParam('SYSTEM_BASE_CURRENCY').' '.$this->amount;
		}
	}
	
	public function getPromoCashAmount($deposit) {
		if($this->id <= 0) return false;
		if($deposit == 0) return false;
		
		if($this->type == self::CBO_TYPE_PERCENTAGE) {
			$amount = $this->amount/100 * $deposit;
			if($this->maxamount > 0 && $amount > $this->maxamount) $amount = $this->maxamount;
			return $amount;
		} else if($this->type == self::CBO_TYPE_CASH) {
			return $this->amount;
		}
	}
	
	public static function getBonusType(){
		$type = array(
			self::CBO_TYPE_PERCENTAGE 				=> Lang::get('COMMON.PERCENTAGE'),
			self::CBO_TYPE_CASH 					=> Lang::get('COMMON.CASH'),
		);

		return $type;
	}

}
