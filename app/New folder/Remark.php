<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Remark extends Model {

	protected $table = 'remark';
	 
	const CREATED_AT = 'created';

    const UPDATED_AT = 'modified';
	
	function getAllRemarkAsOptions() {

		$reasons = array();
		if($objects = self::whereRaw('status = 1')->get()) {
			foreach($objects as $object) {
				$reasons[$object->remark] = $object->remark;
			}
		}
		if (count($reasons) > 0) return $reasons;
		else return false;
	}
	
	/**
	* Return the current object status color code.
	*
	*
	* @return string The object status color code
	*/
	function getStatusColorCode(){
		if($this->id == 0) return false;

		if($this->status == CBO_STANDARDSTATUS_ACTIVE)
		return '#C6FFD9';
		else if($this->status == CBO_STANDARDSTATUS_SUSPENDED)
		return '#FFCCCC';
	}


}
