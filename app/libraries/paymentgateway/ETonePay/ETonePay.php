<?php namespace App\libraries\paymentgateway\ETonePay;

use App\libraries\paymentgateway\BasePaymentGateway;
use App\Models\Cashledger;
use App\Models\Paymentgateway;
use App\Models\Paymentgatewaysetting;
use Carbon\Carbon;
use Config;
use Illuminate\Http\Request;
use Lang;

include('ascii_class.php');

class ETonePay extends BasePaymentGateway
{
    const PG_CODE = 'ETP';

    private $currency;

    /**
     * PayWalo constructor.
     */
    public function __construct($currency)
    {
        $this->currency = $currency;
    }

    public function deposit($paymentgatewayObj)
    {
        if ($paymentgatewayObj->paybnkoption == 'bnk') {
            return array(
                'code' => 'OK',
                'action' => 'redirect',
                'action_mobile' => 'redirect',
                'url' => url(strtolower('pg/'.self::PG_CODE.'/'.$this->currency.'/'.$paymentgatewayObj->id)),
                'hint' => '',
                'hint_mobile' => '',
            );
        }

        $apiUrl = $this->getDepositUrl();

        $postfields = $this->getDepositPostfields($paymentgatewayObj);

        $Client = new HttpClient("");
        $pageContents = $Client->quickPost($apiUrl, $postfields);

        $this->log($apiUrl, var_export($pageContents, true), 'PGETP:DEPOSIT');

        if(!empty($pageContents) && $pageContents != 'none') {
            parse_str($pageContents, $parr);

            if (isset($parr['respCode']) && $parr['respCode'] == '0000') {
                $netways = collect(array(
                    'wxMicro' => '微信支付',
                    'alipayMicro' => '支付宝',
                ));

                return array(
                    'code' => 'OK',
                    'action' => 'drawqrcode',
                    'action_mobile' => 'drawqrcode',
//                    'url' => url('pg/'.self::PG_CODE.'/'.$this->currency.'/'.$paymentgatewayObj->id),
                    'url' => $parr['codeUrl'],
//                    'hint' => $netways->get($postfields['bankId'], Lang::get('COMMON.DEPOSIT')) . ': ' . Lang::get('public.PleaseScanQrToContinue'),
                    'hint' => '',
                    'hint_mobile' => '',
                );
            }
        }

        return false;
    }

    public function getDepositUrl()
    {
        return Config::get($this->currency . '.pg_etp.url_api') . 'BankSelect.action';
    }

    public function getDepositPostfields($paymentgatewayObj)
    {
        $postfields = array(
            'version' => '1.0.0',
            'transCode' => '8888',
            'merchantId' => Config::get($this->currency . '.pg_etp.merchant_id'),
            'merOrderNum' => $paymentgatewayObj->orderid,
            'bussId' => Config::get($this->currency . '.pg_etp.buss_id'),
//            'tranAmt' => $this->formatAmount($paymentgatewayObj->amount),
            'tranAmt' => $this->formatAmount(1),
            'sysTraceNum' => $paymentgatewayObj->orderid,
            'tranDateTime' => Carbon::parse($paymentgatewayObj->created_at)->format('YmdHis'),
            'currencyType' => Config::get($this->currency . '.pg_etp.currency_type'),
            'merURL' => Config::get($this->currency . '.pg_etp.url_callback_view'),
            'backURL' => Config::get($this->currency . '.pg_etp.url_callback'),
            'orderInfo' => 'JSKODE-'.mt_rand(1111, 9999),
            'userId' => '',
            'userIp' => $paymentgatewayObj->createdip,
            'bankId' => '',
//            'bankId' => '888880600002900', // 主扫
//            'bankId' => '888880601002900', // 被扫
            'stlmId' => '',
            'entryType' => '1',
            'authCode' => '',
            'activeTime' => '10',
            'channel' => '',
            'payPage' => 'false',
            'limitPay' => 'no_credit',
            'attach' => 'Season Pass - K'.mt_rand(1111, 9999),
            'reserver1' => '',
            'reserver2' => '',
            'reserver3' => '',
            'reserver4' => '',
            'signValue' => '',
        );

        if ($paymentgatewayObj->paybnkoption != 'bnk') {
            $postfields['bankId'] = '888880600002900';
            $postfields['payPage'] = 'true';
        }

        if(!empty($postfields['orderInfo'])){
            $postfields['orderInfo'] = strToHex($postfields['orderInfo']);
        }

        $postfields = $this->generateSignValue($this->currency, $postfields);

        return $postfields;
    }

    public function generateSignValue($currency, $postfields)
    {
        $txnString = $postfields['version']."|".$postfields['transCode']."|".$postfields['merchantId']
            ."|".$postfields['merOrderNum']."|".$postfields['bussId']."|".$postfields['tranAmt']."|".$postfields['sysTraceNum']
            ."|".$postfields['tranDateTime']."|".$postfields['currencyType']."|".$postfields['merURL']."|".$postfields['backURL']
            ."|".$postfields['orderInfo']."|".$postfields['userId'];

        $postfields['txnString'] = $txnString;
        $postfields['signValue'] = md5($txnString . Config::get($currency . '.pg_etp.data_key'));

        return $postfields;
    }

    public function withdraw($params)
    {
        // Not support.
        return false;
    }

    public function getBankDetailOptions($type)
    {
        if ($type == 'deposit') {
            return array(
                'wxMicro' => ['img' => false, 'label' => '微信支付'],
                'alipayMicro' => ['img' => false, 'label' => '支付宝'],
            );
        }

        // Withdrawal
        return array(
//            'ZFB' => ['img' => false, 'label' => '支付宝'],
//            'WX' => ['img' => false, 'label' => '微信支付'],
//            'QQ' => ['img' => false, 'label' => 'QQ钱包'],
//            'BOC' => ['img' => false, 'label' => '中国银行'],
//            'ABC' => ['img' => false, 'label' => '中国农业银行'],
//            'ICBC' => ['img' => false, 'label' => '中国工商银行'],
//            'CCB' => ['img' => false, 'label' => '中国建设银行'],
//            'BCM' => ['img' => false, 'label' => '交通银行'],
//            'CMB' => ['img' => false, 'label' => '中国招商银行'],
//            'CEB' => ['img' => false, 'label' => '中国光大银行'],
//            'CMBC' => ['img' => false, 'label' => '中国民生银行'],
//            'HXB' => ['img' => false, 'label' => '华夏银行'],
//            'CIB' => ['img' => false, 'label' => '兴业银行'],
//            'CNCB' => ['img' => false, 'label' => '中信银行'],
//            'SPDB' => ['img' => false, 'label' => '上海浦东发展银行'],
//            'PSBC' => ['img' => false, 'label' => '中国邮政储蓄银行'],
        );
    }

    public function formatOrderId($orderId)
    {
        return date('YmdHis') . str_pad($orderId, 5, '0', STR_PAD_LEFT);
    }

    public function resolveCallback(Request $request)
    {
        $this->log($request->url(), var_export($request->input(), true), 'PGETP:CALLBACK');

        $transCode = $request->input('transCode', '');
        $merchantId = $request->input('merchantId', '');
        $respCode = $request->input('respCode', '');
        $sysTraceNum = $request->input('sysTraceNum', '');
        $merOrderNum = $request->input('merOrderNum', '');
        $orderId = $request->input('orderId', '');
        $bussId = $request->input('bussId', '');
        $tranAmt = $request->input('tranAmt', '');
        $orderAmt = $request->input('orderAmt', '');
        $bankFeeAmt = $request->input('bankFeeAmt', '');
        $integralAmt = $request->input('integralAmt', '');
        $vaAmt = $request->input('vaAmt', '');
        $bankAmt = $request->input('bankAmt', '');
        $bankId = $request->input('bankId', '');
        $integralSeq = $request->input('integralSeq', '');
        $vaSeq = $request->input('vaSeq', '');
        $bankSeq = $request->input('bankSeq', '');
        $tranDateTime = $request->input('tranDateTime', '');
        $payMentTime = $request->input('payMentTime', '');
        $settleDate = $request->input('settleDate', '');
        $currencyType = $request->input('currencyType', '');
        $orderInfo = $request->input('orderInfo', '');
        $userId = $request->input('userId', '');
        $userIp = $request->input('userIp', '');
        $reserver1 = $request->input('reserver1', '');
        $reserver2 = $request->input('reserver2', '');
        $reserver3 = $request->input('reserver3', '');
        $reserver4 = $request->input('reserver4', '');
        $signValue = $request->input('signValue', '');

        $txnString =  $transCode ."|". $merchantId ."|". $respCode ."|". $sysTraceNum ."|". $merOrderNum ."|"
            . $orderId ."|". $bussId ."|". $tranAmt ."|". $orderAmt ."|" .$bankFeeAmt ."|". $integralAmt ."|"
            . $vaAmt ."|". $bankAmt ."|". $bankId ."|". $integralSeq ."|". $vaSeq ."|"
            . $bankSeq ."|". $tranDateTime ."|". $payMentTime ."|". $settleDate ."|". $currencyType ."|". $orderInfo ."|". $userId;

        $verifySign = md5($txnString . Config::get($this->currency . '.pg_etp.data_key'));

        if ($signValue == $verifySign) {
            $pgSettingObj = Paymentgatewaysetting::where('code', '=', self::PG_CODE)->first();

            if ($pgSettingObj) {
                $paymentObj = Paymentgateway::where('paytype', '=', $pgSettingObj->id)
                    ->where('orderid', '=', $merOrderNum)
//                    ->whereIn('status', [CBO_LEDGERSTATUS_PENDING, CBO_LEDGERSTATUS_MANPENDING])
                    ->first();

                if ($paymentObj) {
                    $paymentObj->refid = $merOrderNum;
                    $doSave = false;

                    if ($respCode == '0000') {
                        $paymentObj->status = CBO_LEDGERSTATUS_CONFIRMED;
                        $doSave = true;
                    } elseif ($respCode == '9999') {
                        $paymentObj->status = CBO_LEDGERSTATUS_CANCELLED;
                        $doSave = true;
                    }

                    if ($doSave) {
                        if ($paymentObj->save()) {
                            $cashLedgerObj = Cashledger::where('refobj', '=', 'Paymentgateway')
                                ->where('refid', '=', $paymentObj->id)
                                ->first();

                            if ($cashLedgerObj) {
                                $cashLedgerObj->status = $paymentObj->status;
                                $cashLedgerObj->save();

                                $this->onCashLedgerStatusUpdated($cashLedgerObj, CBO_CHARTCODE_DEPOSIT, ($paymentObj->status == CBO_LEDGERSTATUS_CONFIRMED));
                            }
                        }
                    }
                }
            }

            return response('success');
        }

        return response('error');
    }

    public function checkDepositTrans()
    {
        $pgSettingObj = Paymentgatewaysetting::where('code', '=', self::PG_CODE)->first();

        if ($pgSettingObj) {
            $pendingStatus = array(CBO_LEDGERSTATUS_PENDING, CBO_LEDGERSTATUS_MANPENDING);

            $paymentObjs = Paymentgateway::where('paytype', '=', $pgSettingObj->id)
                ->whereIn('status', $pendingStatus)
                ->get();

            foreach ($paymentObjs as $paymentObj) {
                $cashLedgerObj = Cashledger::where('refobj', '=', 'Paymentgateway')
                    ->where('refid', '=', $paymentObj->id)
                    ->first();

                // Query only if payment and cashledger record is both pending.
                if (in_array($cashLedgerObj->status, $pendingStatus)) {

                    $postfields = array (
                        'merchantId' => Config::get($this->currency . '.pg_etp.merchant_id'),
                        'merOrderNum' => $paymentObj->orderid,
                        'tranDate' => Carbon::parse($paymentObj->created_at)->format('YmdHis'),
                        'signValue' => '',
                    );

                    $txnString = $postfields['merchantId'] . "|" . $postfields['merOrderNum'] . "|" . $postfields['tranDate'];

                    $postfields['signValue'] = md5($txnString . Config::get($this->currency . '.pg_etp.data_key'));

                    $apiUrl = Config::get($this->currency . '.pg_etp.url_api');
                    $Client = new HttpClient('');
                    $pageContents = $Client->quickPost($apiUrl . 'MerOrderQuery.action', $postfields);

                    $refid = '';
                    $paystatus = null;

                    $this->log($apiUrl, var_export($pageContents, true), 'PGETP:CHECKDEPTRANS');

                    if (!empty($pageContents) && $pageContents != 'none'){
                        parse_str($pageContents, $parr);

                        $transCode = $parr["transCode"];
                        $merchantId = $parr["merchantId"];
                        $respCode = $parr["respCode"];
                        $sysTraceNum = $parr["sysTraceNum"];
                        $merOrderNum = $parr["merOrderNum"];
                        $orderId = $parr["orderId"];
                        $bussId = $parr["bussId"];
                        $tranAmt = $parr["tranAmt"];
                        $orderAmt = $parr["orderAmt"];
                        $bankFeeAmt = $parr["bankFeeAmt"];
                        $integralAmt = $parr["integralAmt"];
                        $vaAmt = $parr["vaAmt"];
                        $bankAmt = $parr["bankAmt"];
                        $bankId = $parr["bankId"];
                        $integralSeq = $parr["integralSeq"];
                        $vaSeq = $parr["vaSeq"];
                        $bankSeq = $parr["bankSeq"];
                        $tranDateTime = $parr["tranDateTime"];
                        $payMentTime = $parr["payMentTime"];
                        $settleDate = $parr["settleDate"];
                        $currencyType = $parr["currencyType"];
                        $orderInfo = $parr["orderInfo"];
                        $userId = $parr["userId"];
                        $userIp = $parr["userIp"];
                        $reserver1 = $parr["reserver1"];
                        $reserver2 = $parr["reserver2"];
                        $reserver3 = $parr["reserver3"];
                        $reserver4 = $parr["reserver4"];
                        $signValue = $parr["signValue"];

                        $txnString = $transCode . "|" . $merchantId . "|" . $respCode . "|" . $sysTraceNum . "|" . $merOrderNum . "|" . $orderId . "|" . $bussId . "|" . $tranAmt . "|" . $orderAmt . "|" . $bankFeeAmt . "|" . $integralAmt . "|" . $vaAmt . "|" . $bankAmt . "|" . $bankId . "|" . $integralSeq . "|" . $vaSeq . "|" . $bankSeq . "|" . $tranDateTime . "|" . $payMentTime . "|" . $settleDate . "|" . $currencyType . "|" . $orderInfo . "|" . $userId;

                        if (md5($txnString . Config::get($this->currency . '.pg_etp.data_key')) == $signValue) {
                            if ($respCode == '0000') {
                                $paystatus = CBO_LEDGERSTATUS_CONFIRMED;
                                $refid = $merOrderNum;
                            } elseif ($respCode == '9999') {
                                $paystatus = CBO_LEDGERSTATUS_CANCELLED;
                                $refid = $merOrderNum;
                            }

                            $this->log($apiUrl.'?'.implode('&', $postfields), var_export($parr, true), 'PGETP:CHECKDEPTRANS');
                        }
                    }else{
                        $this->log($apiUrl.'?'.implode('&', $postfields), '订单信息不存在！', 'PGETP:CHECKDEPTRANS');
                    }

                    if (!is_null($paystatus) && Carbon::now()->diffInMinutes($paymentObj->created_at) > 15) {
                        // Auto cancel transaction if created date is over 15 mins ago.
                        $paystatus = CBO_LEDGERSTATUS_CANCELLED;
                    }

                    if (!is_null($paystatus)) {
                        $paymentObj->refid = $refid;
                        $paymentObj->status = $paystatus;
                        $paymentObj->save();

                        $cashLedgerObj->status = $paymentObj->status;
                        $cashLedgerObj->save();

                        $this->onCashLedgerStatusUpdated($cashLedgerObj, CBO_CHARTCODE_DEPOSIT, ($paystatus == CBO_LEDGERSTATUS_CONFIRMED));
                    }

                    sleep(6); // API restriction 5 seconds, +1 for secure.
                } else {
                    $paymentObj->status = $cashLedgerObj->status;
                    $paymentObj->save();
                }
            }
        }
    }

    public function downloadTransFile($date = null)
    {
        $apiUrl = Config::get($this->currency . '.pg_etp.url_api') . 'loadTradeFile.action';

        $postfields = array(
            'merchantId' => Config::get($this->currency . '.pg_etp.merchant_id'),
            'tranDate' => (!is_null($date) ? $date : Carbon::now()->addDays(-1)->format('Ymd')),
            'signValue' => '',
        );

        $txnString = $postfields['merchantId']."|".$postfields['tranDate'];

        $postfields['signValue'] = md5($txnString . Config::get($this->currency . '.pg_etp.data_key'));

        $Client = new HttpClient("");
        $pageContents = $Client->quickPost($apiUrl, $postfields);

        $this->log($apiUrl, var_export($pageContents, true), 'PGETP:LOADTRADEFILE');

        if(!empty($pageContents) && $pageContents != 'none') {
            parse_str($pageContents, $parr);

            if (isset($parr['respCode']) && $parr['respCode'] == '0000') {

                $this->log($apiUrl, var_export($parr, true), 'PGETP:LOADTRADEFILECONTENT');

                return response(var_export($parr, true));
            }
        }

        return true;
    }

    public function checkWithdrawTrans()
    {
        // Not support.
        return;
    }

    private function formatAmount($amount)
    {
        $amt = number_format(floor($amount * 100) / 100, 2, '.', '');
        $amt = str_replace('.', '', $amt);

        return $amt;
    }
}
