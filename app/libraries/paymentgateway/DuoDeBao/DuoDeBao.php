<?php namespace App\libraries\paymentgateway\DuoDeBao;

use App\libraries\paymentgateway\BasePaymentGateway;
use App\Models\Cashledger;
use App\Models\Paymentgateway;
use App\Models\Paymentgatewaysetting;
use Carbon\Carbon;
use Config;
use Illuminate\Http\Request;
use Lang;

class DuoDeBao extends BasePaymentGateway
{
    const PG_CODE = 'DDB';

    private $currency;

    public function __construct($currency)
    {
        $this->currency = $currency;
    }

    public function deposit($paymentgatewayObj)
    {
        $apiUrl = Config::get($this->currency . '.pg_ddb.url_scan_pay');

        $postfields = array(
            'extend_param' => '',
            'extra_return_param' => '',
            'product_code' => 'JSKODE-'.mt_rand(1111, 9999),
            'product_desc' => '',
            'product_num' => '1',
            'merchant_code' => Config::get($this->currency . '.pg_ddb.merchant_code'),
            'service_type' => $paymentgatewayObj->paybnkoption,
            'notify_url' => Config::get($this->currency . '.pg_ddb.url_callback'),
            'interface_version' => 'V3.3',
            'sign_type' => '',
            'order_no' => $paymentgatewayObj->orderid,
            'client_ip' => $paymentgatewayObj->createdip,
            'sign' => '',
            'order_time' => Carbon::parse($paymentgatewayObj->created_at)->toDateTimeString(),
            'order_amount' => $this->formatAmount($paymentgatewayObj->amount),
            'product_name' => 'Season Pass - K'.mt_rand(1111, 9999),
        );

        // These for bank deposit.
//        $postfields = array(
//            'sign' => '',
//            'merchant_code' => Config::get($this->currency . '.pg_ddb.merchant_code'),
//            'bank_code' => $paymentgatewayObj->paybnkoption,
//            'order_no' => $paymentgatewayObj->orderid,
//            'order_amount' => $this->formatAmount($paymentgatewayObj->amount),
//            'service_type' => 'direct_pay',
//            'input_charset' => 'UTF-8',
//            'notify_url' => Config::get($this->currency . '.pg_ddb.url_callback'),
//            'interface_version' => 'V3.3',
//            'sign_type' => 'RSA-S',
//            'order_time' => $paymentgatewayObj->created_at,
//            'product_name' => '充值',
//            'client_ip' => $paymentgatewayObj->createdip,
//            'extend_param' => '',
//            'extra_return_param' => '',
//            'pay_type' => '',
//            'product_code' => '',
//            'product_desc' => '',
//            'product_num' => '',
//            'return_url' => '',
//            'show_url' => '',
//            'redo_flag' => '',
//        );

        ksort($postfields);
        $paramsToSign = array();

        foreach ($postfields as $key => $val) {
            if ($val != '') {
                $paramsToSign[] = $key.'='.$val;
            }
        }

        $merchantPrivateKey = openssl_get_privatekey(Config::get($this->currency . '.pg_ddb.merchant_private_key'));

        openssl_sign(implode('&', $paramsToSign), $sign, $merchantPrivateKey,OPENSSL_ALGO_MD5);

        $postfields['sign'] = base64_encode($sign);
        $postfields['sign_type'] = 'RSA-S';

        $header = array('content-Type: text/html; charset=UTF-8');

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $apiUrl);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 40);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postfields));

        $response = curl_exec($ch);

        curl_close($ch);

        $this->log($apiUrl, var_export($response, true), 'PGDDB:DEPOSIT');

        $xml = simplexml_load_string($response);

        if (isset($xml->response->resp_code) && strtoupper(strval($xml->response->resp_code)) == 'SUCCESS' && isset($xml->response->qrcode)) {
            $netways = collect(array(
                'weixin_scan' => '微信支付',
                'alipay_scan' => '支付宝',
                'qq_scan' => 'QQ钱包',
                'jd_scan' => '京东扫码支付',
                'ylpay_scan' => '银联扫码支付',
            ));

            return array(
                'code' => 'OK',
                'action' => 'drawqrcode',
                'action_mobile' => 'drawqrcode',
                'url' => strval($xml->response->qrcode),
                'hint' => $netways->get($paymentgatewayObj->paybnkoption, Lang::get('COMMON.DEPOSIT')) . ': ' . Lang::get('public.PleaseScanQrToContinue'),
                'hint_mobile' => '',
            );
        }

        return false;
    }

    public function withdraw($params)
    {
        // Not support.
        return false;
    }

    public function getBankDetailOptions($type)
    {
        if ($type == 'deposit') {
            return array(
                'weixin_scan' => ['img' => false, 'label' => '微信支付'],
                'alipay_scan' => ['img' => false, 'label' => '支付宝'],
                'qq_scan' => ['img' => false, 'label' => 'QQ钱包'],
                'jd_scan' => ['img' => false, 'label' => '京东扫码支付'],
                'ylpay_scan' => ['img' => false, 'label' => '银联扫码支付'],
//                'ABC' => ['img' => false, 'label' => '中国农业银行'],
//                'ICBC' => ['img' => false, 'label' => '中国工商银行'],
//                'CCB' => ['img' => false, 'label' => '中国建设银行'],
//                'BCOM' => ['img' => false, 'label' => '中国交通银行'],
//                'BOC' => ['img' => false, 'label' => '中国银行'],
//                'CMB' => ['img' => false, 'label' => '招商银行'],
//                'CMBC' => ['img' => false, 'label' => '中国民生银行'],
//                'CEBB' => ['img' => false, 'label' => '中国光大银行'],
//                'BOB' => ['img' => false, 'label' => '北京银行'],
//                'SHB' => ['img' => false, 'label' => '上海银行'],
//                'NBB' => ['img' => false, 'label' => '宁波银行'],
//                'HXB' => ['img' => false, 'label' => '华夏银行'],
//                'CIB' => ['img' => false, 'label' => '兴业银行'],
//                'PSBC' => ['img' => false, 'label' => '中国邮政储蓄银行'],
//                'SPABANK' => ['img' => false, 'label' => '中国邮政储蓄银行'],
//                'SPDB' => ['img' => false, 'label' => '浦发银行'],
//                'ECITIC' => ['img' => false, 'label' => '中信银行'],
//                'HZB' => ['img' => false, 'label' => '杭州银行'],
//                'GDB' => ['img' => false, 'label' => '广东发展银行'],
//                'FIXED_ALI' => ['img' => false, 'label' => ''],
//                'FIXED_WX' => ['img' => false, 'label' => ''],
            );
        }

        // Withdrawal
        return array(
//            'ZFB' => ['img' => false, 'label' => '支付宝'],
//            'WX' => ['img' => false, 'label' => '微信支付'],
//            'QQ' => ['img' => false, 'label' => 'QQ钱包'],
//            'BOC' => ['img' => false, 'label' => '中国银行'],
//            'ABC' => ['img' => false, 'label' => '中国农业银行'],
//            'ICBC' => ['img' => false, 'label' => '中国工商银行'],
//            'CCB' => ['img' => false, 'label' => '中国建设银行'],
//            'BCM' => ['img' => false, 'label' => '交通银行'],
//            'CMB' => ['img' => false, 'label' => '中国招商银行'],
//            'CEB' => ['img' => false, 'label' => '中国光大银行'],
//            'CMBC' => ['img' => false, 'label' => '中国民生银行'],
//            'HXB' => ['img' => false, 'label' => '华夏银行'],
//            'CIB' => ['img' => false, 'label' => '兴业银行'],
//            'CNCB' => ['img' => false, 'label' => '中信银行'],
//            'SPDB' => ['img' => false, 'label' => '上海浦东发展银行'],
//            'PSBC' => ['img' => false, 'label' => '中国邮政储蓄银行'],
        );
    }

    public function formatOrderId($orderId)
    {
        return date('YmdHis') . str_pad($orderId, 5, '0', STR_PAD_LEFT);
    }

    public function resolveCallback(Request $request)
    {
        $data = array(
//            'sign' => base64_decode($request->input('sign')),
            'sign' => '',
            'merchant_code' => $request->input('merchant_code'),
            'notify_type' => $request->input('notify_type'),
            'notify_id' => $request->input('notify_id'),
            'trade_status' => $request->input('trade_status'),
            'trade_time' => $request->input('trade_time'),
            'trade_no' => $request->input('trade_no'),
            'bank_seq_no' => $request->input('bank_seq_no'),
            'orginal_money' => $request->input('orginal_money'),
            'order_no' => $request->input('order_no'),
            'order_amount' => $request->input('order_amount'),
            'interface_version' => $request->input('interface_version'),
//            'sign_type' => $request->input('sign_type'),
            'sign_type' => '',
            'order_time' => $request->input('order_time'),
            'extra_return_param' => $request->input('extra_return_param'),
        );

        ksort($data);
        $paramsToSign = array();

        foreach ($data as $key => $val) {
            if ($val != '') {
                $paramsToSign[] = $key.'='.$val;
            }
        }

        $data['sign'] = base64_decode($request->input('sign'));
        $data['sign_type'] = $request->input('sign_type');

        $this->log($request->url(), var_export($data, true), 'PGDDB:CALLBACK');

        $dinpayPublicKey = openssl_get_publickey(Config::get($this->currency . '.pg_ddb.public_key'));

        $flag = openssl_verify(implode('&', $paramsToSign), $data['sign'], $dinpayPublicKey, OPENSSL_ALGO_MD5);

        if ($flag) {
            $pgSettingObj = Paymentgatewaysetting::where('code', '=', self::PG_CODE)->first();

            if ($pgSettingObj) {
                $paymentObj = Paymentgateway::where('paytype', '=', $pgSettingObj->id)
                    ->where('orderid', '=', $data['order_no'])
//                    ->where('status', '=', CBO_LEDGERSTATUS_PENDING)
                    ->first();

                $paymentObj->refid = $data['trade_no'];
                $paymentObj->status = ($data['trade_status'] == 'SUCCESS' ? CBO_LEDGERSTATUS_CONFIRMED : CBO_LEDGERSTATUS_CANCELLED);

                if ($paymentObj->save()) {
                    $cashLedgerObj = Cashledger::where('refobj', '=', 'Paymentgateway')
                        ->where('refid', '=', $paymentObj->id)
                        ->first();

                    if ($cashLedgerObj) {
                        $cashLedgerObj->status = $paymentObj->status;
                        $cashLedgerObj->save();

                        $this->onCashLedgerStatusUpdated($cashLedgerObj, CBO_CHARTCODE_DEPOSIT, ($paymentObj->status == CBO_LEDGERSTATUS_CONFIRMED));
                    }
                }
            }

            return response('SUCCESS');
        }

        return response('Verification Error');
    }

    public function checkDepositTrans()
    {
        $apiUrl = Config::get($this->currency . '.pg_ddb.url_scan_query');
        $pgSettingObj = Paymentgatewaysetting::where('code', '=', self::PG_CODE)->first();

        if ($pgSettingObj) {
            $pendingStatus = array(CBO_LEDGERSTATUS_PENDING, CBO_LEDGERSTATUS_MANPENDING);

            $paymentObjs = Paymentgateway::where('paytype', '=', $pgSettingObj->id)
                ->whereIn('status', $pendingStatus)
                ->get();

            foreach ($paymentObjs as $paymentObj) {
                $cashLedgerObj = Cashledger::where('refobj', '=', 'Paymentgateway')
                    ->where('refid', '=', $paymentObj->id)
                    ->first();

                // Query only if payment and cashledger record is both pending.
                if (in_array($cashLedgerObj->status, $pendingStatus)) {
                    $postfields = array(
                        'sign' => '',
                        'interface_version' => 'V3.3',
                        'service_type' => 'single_trade_query',
                        'merchant_code' => Config::get($this->currency . '.pg_ddb.merchant_code'),
                        'sign_type' => '',
                        'order_no' => $paymentObj->orderid,
                        'trade_no' => '',
                    );

                    ksort($postfields);
                    $paramsToSign = array();

                    foreach ($postfields as $key => $val) {
                        if ($val != '') {
                            $paramsToSign[] = $key.'='.$val;
                        }
                    }

                    $merchantPrivateKey = openssl_get_privatekey(Config::get($this->currency . '.pg_ddb.merchant_private_key'));

                    openssl_sign(implode('&', $paramsToSign), $sign, $merchantPrivateKey,OPENSSL_ALGO_MD5);

                    $postfields['sign'] = base64_encode($sign);
                    $postfields['sign_type'] = 'RSA-S';

                    $header = array('content-Type: text/html; charset=UTF-8');

                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, $apiUrl);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
                    curl_setopt($ch, CURLOPT_HEADER, false);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_POST, true);
                    curl_setopt($ch, CURLOPT_TIMEOUT, 40);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postfields));

                    $response = curl_exec($ch);

                    curl_close($ch);

                    $this->log($apiUrl, var_export($response, true), 'PGDDB:CHECKDEPTRANS');

                    $resData = simplexml_load_string($response);
                    $paystatus = null;

                    if (isset($resData->is_success) && isset($resData->trade_status)) {
                        if ($resData->is_success == '' && $resData->trade_status == '') {
                            $paystatus = CBO_LEDGERSTATUS_CONFIRMED;
                        } else {
                            $paystatus = CBO_LEDGERSTATUS_CANCELLED;
                        }
                    }

                    if (is_null($paystatus)) {
                        if (Carbon::now()->diffInMinutes($paymentObj->created_at) > 15) {
                            // Auto cancel transaction if created date is over 15 mins ago.
                            $paystatus = CBO_LEDGERSTATUS_CANCELLED;
                        }
                    }

                    if (!is_null($paystatus)) {
                        $paymentObj->refid = (isset($resData->trade_no) ? $resData->trade_no : '');
                        $paymentObj->status = $paystatus;
                        $paymentObj->save();

                        $cashLedgerObj->status = $paymentObj->status;
                        $cashLedgerObj->save();

                        $this->onCashLedgerStatusUpdated($cashLedgerObj, CBO_CHARTCODE_DEPOSIT, ($paystatus == CBO_LEDGERSTATUS_CONFIRMED));
                    }

                    sleep(5); // API restriction 5 seconds, +1 for secure.
                } else {
                    $paymentObj->status = $cashLedgerObj->status;
                    $paymentObj->save();
                }
            }
        }
    }

    public function checkWithdrawTrans()
    {
        // Not support.
        return;
    }

    private function formatAmount($amount)
    {
        $amt = number_format($amount, 2, '.', '');

        return $amt;
    }
}
