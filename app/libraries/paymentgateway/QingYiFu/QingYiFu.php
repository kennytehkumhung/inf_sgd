<?php namespace App\libraries\paymentgateway\QingYiFu;

use App\libraries\paymentgateway\BasePaymentGateway;
use App\Models\Cashledger;
use App\Models\Paymentgateway;
use App\Models\Paymentgatewaysetting;
use Carbon\Carbon;
use Config;
use Illuminate\Http\Request;
use Lang;

include('Util.php');

class QingYiFu extends BasePaymentGateway
{
    const PG_CODE = 'QYF';

    private $currency;
    private $rsaObj;
    private $private_key;

    /**
     * PayWalo constructor.
     */
    public function __construct($currency)
    {
        $this->currency = $currency;

        $data = array(
            'private_key' => Config::get($currency . '.pg_qyf.private_key'),
            'pay_public_key' => Config::get($currency . '.pg_qyf.pay_public_key'),
            'remit_public_key' => Config::get($currency . '.pg_qyf.remit_public_key'),
        );

        $this->private_key = Config::get($currency . '.pg_qyf.private_key');

        $this->rsaObj = new Rsa($data);
    }

    public function deposit($paymentgatewayObj)
    {
        $apiUrl = $this->resolvePayActionUrl($paymentgatewayObj->paybnkoption);

        $data['orderNum'] = $paymentgatewayObj->orderid;
        $data['version'] = 'V3.1.0.0';
        $data['charset'] = 'UTF-8';
        $data['random'] = (string) rand(1000,9999);
        $data['merNo'] = Config::get($this->currency . '.pg_qyf.merchant_no');
        $data['netway'] = $paymentgatewayObj->paybnkoption;   	//WX:微信支付,ZFB:支付宝支付
        $data['amount'] = $this->formatAmount($paymentgatewayObj->amount);	// 单位:分
        $data['goodsName'] = '充值';
        $data['callBackUrl'] = Config::get($this->currency . '.pg_qyf.url_callback');
        $data['callBackViewUrl'] = Config::get($this->currency . '.pg_qyf.url_callback_view');
        $data['sign'] = create_sign($data,Config::get($this->currency . '.pg_qyf.key'));

        $json = json_encode_ex($data);
        $dataStr = $this->rsaObj->encode_pay($json);
        $param = 'data=' . urlencode($dataStr) . '&merchNo=' . $data['merNo'] . '&version=V3.1.0.0';

        $result = wx_post($apiUrl,$param);
        $rows = json_to_array($result,Config::get($this->currency . '.pg_qyf.key'));

        if ($rows['stateCode'] == '00'){
            $this->log($apiUrl.'?'.implode('&', $data), "代码：00订单创建成功." . var_export($rows, true), 'PGQYF:DEPOSIT');

            $netways = collect(array(
                'WX' => '微信支付',
                'ZFB' => '支付宝',
                'QQ' => 'QQ 钱包',
            ));

            // QYF only support mobile pay.
            return array(
                'code' => 'OK',
                'action' => 'drawqrcode',
                'action_mobile' => 'drawqrcode',
                'url' => $rows['qrcodeUrl'],
                'hint' => $netways->get($data['netway'], Lang::get('COMMON.DEPOSIT')) . ': ' . Lang::get('public.PleaseScanQrToContinue'),
                'hint_mobile' => '',
            );
        }else{
            $this->log($apiUrl, "错误代码：" . $rows['stateCode'] . ' 错误描述:' . $rows['msg'], 'PGQYF:DEPOSIT');
        }

        return false;
    }

    public function withdraw($params)
    {
        // Not support.
        return false;
    }

    public function getBankDetailOptions($type)
    {
        if ($type == 'deposit') {
            return array(
                'WX' => ['img' => false, 'label' => '微信支付'],
                'WX_WAP' => ['img' => false, 'label' => '微信支付 (手机版)'],
                'ZFB' => ['img' => false, 'label' => '支付宝'],
                'ZFB_WAP' => ['img' => false, 'label' => '支付宝 (手机版)'],
                'QQ' => ['img' => false, 'label' => 'QQ钱包'],
                'QQ_WAP' => ['img' => false, 'label' => 'QQ钱包 (手机版)'],
            );
        }

        // Withdrawal
        return array(
            'ZFB' => ['img' => false, 'label' => '支付宝'],
            'WX' => ['img' => false, 'label' => '微信支付'],
            'QQ' => ['img' => false, 'label' => 'QQ钱包'],
            'BOC' => ['img' => false, 'label' => '中国银行'],
            'ABC' => ['img' => false, 'label' => '中国农业银行'],
            'ICBC' => ['img' => false, 'label' => '中国工商银行'],
            'CCB' => ['img' => false, 'label' => '中国建设银行'],
            'BCM' => ['img' => false, 'label' => '交通银行'],
            'CMB' => ['img' => false, 'label' => '中国招商银行'],
            'CEB' => ['img' => false, 'label' => '中国光大银行'],
            'CMBC' => ['img' => false, 'label' => '中国民生银行'],
            'HXB' => ['img' => false, 'label' => '华夏银行'],
            'CIB' => ['img' => false, 'label' => '兴业银行'],
            'CNCB' => ['img' => false, 'label' => '中信银行'],
            'SPDB' => ['img' => false, 'label' => '上海浦东发展银行'],
            'PSBC' => ['img' => false, 'label' => '中国邮政储蓄银行'],
        );
    }

    public function formatOrderId($orderId)
    {
        return date('YmdHis') . str_pad($orderId, 5, '0', STR_PAD_LEFT);
    }

    public function resolveCallback(Request $request)
    {
        $data = $request->input('data', '');

        if (!empty($data)){
//            $data = urldecode($data); // GONNA DISABLE THIS to work on PG returned data.
            $data = $this->rsaObj->decode($data);
            $rows = callback_to_array($data,Config::get($this->currency . '.pg_qyf.key'));

            $this->log($request->url(), '收到支付回调通知:' . var_export($rows, true), 'PGQYF:CALLBACK');

            $pgSettingObj = Paymentgatewaysetting::where('code', '=', self::PG_CODE)->first();

            if ($pgSettingObj) {
                $paymentObj = Paymentgateway::where('paytype', '=', $pgSettingObj->id)
                    ->where('orderid', '=', $rows['orderNum'])
//                    ->whereIn('status', [CBO_LEDGERSTATUS_PENDING, CBO_LEDGERSTATUS_MANPENDING])
                    ->first();

                if ($paymentObj) {
                    $paymentObj->refid = $rows['orderNum'];
                    $paymentObj->status = ($rows['payResult'] == '00' ? CBO_LEDGERSTATUS_CONFIRMED : CBO_LEDGERSTATUS_CANCELLED);

                    if ($paymentObj->save()) {
                        $cashLedgerObj = Cashledger::where('refobj', '=', 'Paymentgateway')
                            ->where('refid', '=', $paymentObj->id)
                            ->first();

                        if ($cashLedgerObj) {
                            $cashLedgerObj->status = $paymentObj->status;
                            $cashLedgerObj->save();

                            $this->onCashLedgerStatusUpdated($cashLedgerObj, CBO_CHARTCODE_DEPOSIT, ($paymentObj->status == CBO_LEDGERSTATUS_CONFIRMED));
                        }
                    }
                }
            }
        }

        return response('0');
    }

    public function checkDepositTrans()
    {
        $apiUrl = Config::get($this->currency . '.pg_qyf.url_check_pay');
        $pgSettingObj = Paymentgatewaysetting::where('code', '=', self::PG_CODE)->first();

        if ($pgSettingObj) {
            $pendingStatus = array(CBO_LEDGERSTATUS_PENDING, CBO_LEDGERSTATUS_MANPENDING);

            $paymentObjs = Paymentgateway::where('paytype', '=', $pgSettingObj->id)
                ->whereIn('status', $pendingStatus)
                ->get();

            foreach ($paymentObjs as $paymentObj) {
                $cashLedgerObj = Cashledger::where('refobj', '=', 'Paymentgateway')
                    ->where('refid', '=', $paymentObj->id)
                    ->first();

                // Query only if payment and cashledger record is both pending.
                if (in_array($cashLedgerObj->status, $pendingStatus)) {
                    $data = array();
                    $data['merNo'] = Config::get($this->currency . '.pg_qyf.merchant_no');
                    $data['netway'] = $paymentObj->paybnkoption; // WX:微信支付,ZFB:支付宝支付
                    $data['orderNum'] = $paymentObj->orderid;
                    $data['amount'] = $this->formatAmount($paymentObj->amount);	// 单位:分
                    $data['goodsName'] = '充值';
                    $data['payDate'] = Carbon::parse($paymentObj->created_at)->toDateString();
                    $data['sign'] = create_sign($data, Config::get($this->currency . '.pg_qyf.key'));

                    $json = json_encode_ex($data);
                    $dataStr = $this->rsaObj->encode_pay($json);
                    $param = 'data=' . urlencode($dataStr) . '&merchNo=' . Config::get($this->currency . '.pg_qyf.merchant_no') . '&version=V3.1.0.0';

                    $result = wx_post($apiUrl, $param);
                    $rows = json_to_array($result, Config::get($this->currency . '.pg_qyf.key'));

                    $paystatus = null;

                    // 支付状态 00:支付成功 01:支付失败 03:签名错误 04:其他错误 05:未知06:初始 50:网络异常 99:未支付
                    if ($rows['payStateCode'] == '00') {
                        $this->log($apiUrl.'?'.implode('&', $data), "代码：00订单查询成功." . var_export($rows, true), 'PGQYF:CHECKDEPTRANS');
                        $paystatus = CBO_LEDGERSTATUS_CONFIRMED;
                    } elseif ($rows['payStateCode'] != '99') {
                        $this->log($apiUrl.'?'.implode('&', $data), "错误代码：" . $rows['stateCode'] . ' 错误描述:' . $rows['msg'], 'PGQYF:CHECKDEPTRANS');
                        $paystatus = CBO_LEDGERSTATUS_CANCELLED;
                    } else {
                        if (Carbon::now()->diffInMinutes($paymentObj->created_at) > 15) {
                            // Auto cancel transaction if created date is over 15 mins ago.
                            $paystatus = CBO_LEDGERSTATUS_CANCELLED;
                        }
                    }

                    if (!is_null($paystatus)) {
                        $paymentObj->refid = (isset($rows['orderNum']) ? $rows['orderNum'] : '');
                        $paymentObj->status = $paystatus;
                        $paymentObj->save();

                        $cashLedgerObj->status = $paymentObj->status;
                        $cashLedgerObj->save();

                        $this->onCashLedgerStatusUpdated($cashLedgerObj, CBO_CHARTCODE_DEPOSIT, ($paystatus == CBO_LEDGERSTATUS_CONFIRMED));
                    }

                    sleep(6); // API restriction 5 seconds, +1 for secure.
                } else {
                    $paymentObj->status = $cashLedgerObj->status;
                    $paymentObj->save();
                }
            }
        }
    }

    public function checkWithdrawTrans()
    {
        // Not support.
        return;
    }

    private function resolvePayActionUrl($netway)
    {
        return Config::get($this->currency . '.pg_qyf.url_' . strtolower($netway));
    }

    private function formatAmount($amount)
    {
        $amt = number_format(floor($amount * 100) / 100, 2, '.', '');
        $amt = str_replace('.', '', $amt);

        return $amt;
    }
}
