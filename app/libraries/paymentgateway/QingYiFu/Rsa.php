<?php namespace App\libraries\paymentgateway\QingYiFu;

class Rsa{
	private $pay_public_key;
	private $remit_public_key;
	private $private_key;

	public function __construct($data, $key = null){
//		include('config.php');
		$this->private_key = "-----BEGIN RSA PRIVATE KEY-----".PHP_EOL;
		foreach (str_split($data['private_key'],64) as $str){
			$this->private_key = $this->private_key . $str . PHP_EOL;
		}
		$this->private_key = $this->private_key . "-----END RSA PRIVATE KEY-----";
		
		$this->pay_public_key = "-----BEGIN PUBLIC KEY-----\r\n";
		foreach (str_split($data['pay_public_key'],64) as $str){
			$this->pay_public_key = $this->pay_public_key . $str . "\r\n";
		}
		$this->pay_public_key = $this->pay_public_key . "-----END PUBLIC KEY-----";
		
		
		
		$this->remit_public_key = "-----BEGIN PUBLIC KEY-----\r\n";
		foreach (str_split($data['remit_public_key'],64) as $str){
		    $this->remit_public_key = $this->remit_public_key . $str . "\r\n";
		}
		$this->remit_public_key = $this->remit_public_key . "-----END PUBLIC KEY-----";
	}

	public function encode_pay($data){#加密
		$pu_key = openssl_pkey_get_public($this->pay_public_key);
		if ($pu_key == false){
			echo "打开密钥出错";
			die;
		}
		$encryptData = '';
		$crypto = '';
		foreach (str_split($data, 117) as $chunk) {
            openssl_public_encrypt($chunk, $encryptData, $pu_key);
            $crypto = $crypto . $encryptData;
        }

		$crypto = base64_encode($crypto);
		return $crypto;

	}
	
	
	public function encode_remit($data){#加密
	    $pu_key = openssl_pkey_get_public($this->remit_public_key);
	    if ($pu_key == false){
	        echo "打开密钥出错";
	        die;
	    }
	    $encryptData = '';
	    $crypto = '';
	    foreach (str_split($data, 117) as $chunk) {
	        openssl_public_encrypt($chunk, $encryptData, $pu_key);
	        $crypto = $crypto . $encryptData;
	    }
	    
	    $crypto = base64_encode($crypto);
	    return $crypto;
	    
	}



	public function decode($data){
		$pr_key = openssl_get_privatekey($this->private_key);
		if ($pr_key == false){
			echo "打开密钥出错";
			die;
		}
		$data = base64_decode($data);
		$crypto = '';
        foreach (str_split($data, 128) as $chunk) {
            openssl_private_decrypt($chunk, $decryptData, $pr_key);
            $crypto .= $decryptData;
        }
        return $crypto;
	}

	
}
