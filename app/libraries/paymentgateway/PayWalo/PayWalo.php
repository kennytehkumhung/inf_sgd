<?php namespace App\libraries\paymentgateway\PayWalo;

use App\Http\Controllers\Admin\CashledgerController;
use App\libraries\App;
use App\libraries\paymentgateway\BasePaymentGateway;
use App\Models\Cashledger;
use App\Models\Paymentgateway;
use App\Models\Paymentgatewaysetting;
use Carbon\Carbon;
use Config;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Lang;

class PayWalo extends BasePaymentGateway
{
    const PG_CODE = 'PWL';

    private $currency;
    private $url;
    private $clientKey;
    private $secretKey;
    private $saltingKey;

    /**
     * PayWalo constructor.
     */
    public function __construct($currency)
    {
        $this->currency = $currency;
        $this->url = Config::get($currency.'.pg_pwl.url');
        $this->clientKey = Config::get($currency.'.pg_pwl.client_key');
        $this->secretKey = Config::get($currency.'.pg_pwl.secret_key');
        $this->saltingKey = Config::get($currency.'.pg_pwl.salting_key');
    }

    public function deposit($paymentgatewayObj)
    {
        $apiUrl = $this->url.'api/Payment/Deposit';

        $postfields = array(
            'Amount' => $this->formatAmount($paymentgatewayObj->amount),
            'OrderDate' => Carbon::parse($paymentgatewayObj->created_at)->format('c'),
            'OrderNumber' => $paymentgatewayObj->orderid,
            'BankAccountID' => $paymentgatewayObj->paybnkoption,
            'Currency' => $this->currency,
            'CallbackUrl' => Config::get($this->currency.'.pg_pwl.url_callback'),
            'Notes' => '',
        );

        $concatenatedString = [];

        foreach($postfields as $key => $value) {
            $concatenatedString[] = $value;
        }

        // Generate sign.
        $concatenatedString = implode('', $concatenatedString);
        $concatenatedString = iconv(mb_detect_encoding($concatenatedString, mb_detect_order(), true), 'UTF-8', $concatenatedString);
        $postfields['Sign'] = strtoupper(hash_hmac('sha256', $concatenatedString, $this->saltingKey));

        // Prepare request params.
        $options = $this->getDefaultOptions();
        $options['json'] = $postfields;

        $this->log($apiUrl, var_export($postfields, true), 'PGPWL:DEPOSIT');

        // Execute request.
        $response = $this->getHttpClient()->request('POST', $apiUrl, $options);

        $this->log($apiUrl, var_export($response->getBody()->getContents(), true), 'PGPWL:DEPOSIT2');

        if ($response->getStatusCode() == 200) {
            $json = json_decode($response->getBody()->getContents());

            if (isset($json->Status) && $json->Status == 'Success' && isset($json->Data->TransactionNumber)) {
                $paymentgatewayObj->refid = $json->Data->TransactionNumber;
                $paymentgatewayObj->bankaccno = $json->Data->AccountNumber.'-'.$json->Data->AccountName;
                $paymentgatewayObj->bankaccname = $json->Data->BankName.'-'.$json->Data->BankCode;
                $paymentgatewayObj->save();

                return array(
                    'code' => 'OK',
                    'action' => 'redirect',
                    'action_mobile' => 'redirect',
                    'url' => route('transaction'),
                    'hint' => '',
                    'hint_mobile' => '',
                );
            }
        }

        return false;
    }

    public function getBankDetailOptions($type)
    {
        if ($type == 'deposit') {
            // Execute request.
            $apiUrl = $this->url.'api/Common/BankAccounts';
            $response = $this->getHttpClient()->request('GET', $apiUrl, $this->getDefaultOptions());
            $result = array();

            if ($response->getStatusCode() == 200) {
                $response = json_decode($response->getBody()->getContents(), true);

                $this->log($apiUrl, var_export($response, true), 'PGPWL:BANK');

                if (isset($response['Status']) && $response['Status'] == 'SUCCESS') {
                    foreach ($response['Data'] as $d) {
                        if (isset($d['BankCode']) && isset($d['CurrencyCode']) && $d['CurrencyCode'] == $this->currency) {
                            if ($d['BankCode'] != 'LP') {
                                // Exclude LinePay.
                                $result[$d['Id']] = [
                                    'img' => false,
                                    'label' => $d['BankName'].'<br>'.$d['AccountName'].'<br>'.$d['AccountNumber'].'<br>'.$d['CurrencyCode'],
                                    'bankname' => $d['BankName'],
                                    'bankaccno' => $d['AccountNumber'],
                                    'bankaccname' => $d['AccountName'],
                                    'bankcrccode' => $d['CurrencyCode'],
                                ];
                            }
                        }
                    }
                }
            }

            return $result;
        }

        // Withdrawal
        return array();
    }

    public function formatOrderId($orderId)
    {
        return date('YmdHis') . str_pad($orderId, 5, '0', STR_PAD_LEFT);
    }

    public function resolveCallback(Request $request)
    {
        $this->log($request->url(), var_export($request->input(), true), 'PGPWL:CALLBACK');

        $data = $request->json()->all();

        /*
         * json status:
         * pending = 0
         * success = 1
         * failed = 2
         * error = 3
         */
        if (isset($data['OrderNumber']) && $data['Status'] != '0') {
            $pgSettingObj = Paymentgatewaysetting::where('code', '=', self::PG_CODE)->first();

            if ($pgSettingObj) {
                $paymentObj = Paymentgateway::where('paytype', '=', $pgSettingObj->id)
                    ->where('orderid', '=', $data['OrderNumber'])
//                    ->where('status', '=', CBO_LEDGERSTATUS_PENDING)
                    ->first();

                $paymentObj->refid = $data['TransactionNumber'];
                $paymentObj->status = ($data['Status'] == '1' ? CBO_LEDGERSTATUS_CONFIRMED : CBO_LEDGERSTATUS_CANCELLED);

                if ($paymentObj->save()) {
                    $cashLedgerObj = Cashledger::where('refobj', '=', 'Paymentgateway')
                        ->where('refid', '=', $paymentObj->id)
                        ->first();

                    if ($cashLedgerObj) {
                        $cashLedgerObj->status = $paymentObj->status;
                        $cashLedgerObj->save();

                        $this->onCashLedgerStatusUpdated($cashLedgerObj, CBO_CHARTCODE_DEPOSIT, ($paymentObj->status == CBO_LEDGERSTATUS_CONFIRMED));
                    }
                }
            }

            return response('SUCCESS');
        }

        return response('ERROR');
    }

    public function checkDepositTrans()
    {
        $pgSettingObj = Paymentgatewaysetting::where('code', '=', self::PG_CODE)->first();

        if ($pgSettingObj) {
            $pendingStatus = array(CBO_LEDGERSTATUS_PENDING, CBO_LEDGERSTATUS_MANPENDING);

            $paymentObjs = Paymentgateway::where('paytype', '=', $pgSettingObj->id)
                ->whereIn('status', $pendingStatus)
                ->get();

            foreach ($paymentObjs as $paymentObj) {
                $cashLedgerObj = Cashledger::where('refobj', '=', 'Paymentgateway')
                    ->where('refid', '=', $paymentObj->id)
                    ->first();

                // Query only if payment and cashledger record is both pending.
                if (in_array($cashLedgerObj->status, $pendingStatus)) {
                    $apiUrl = $this->url.'api/CheckDeposit?transactionNo='.$paymentObj->refid;
                    $response = $this->getHttpClient()->request('GET', $apiUrl, $this->getDefaultOptions());
                    $paystatus = null;

                    $this->log($apiUrl, var_export($response, true), 'PGPWL:CHECKDEPTRANS');

                    if ($response->getStatusCode() == 200) {
                        $response = json_decode($response->getBody()->getContents(), true);

                        if (isset($response['Status']) && $response['Status'] == 'SUCCESS' && isset($response['Data']['Status'])) {
                            /*
                             * json status:
                             * requested = 0
                             * approved = 1
                             * cancelled = 2
                             */
                            if ($response['Data']['Status'] == 1) {
                                $paystatus = CBO_LEDGERSTATUS_CONFIRMED;
                            } elseif ($response['Data']['Status'] == 2) {
                                $paystatus = CBO_LEDGERSTATUS_CANCELLED;
                            }
                        }
                    }

                    if (!is_null($paystatus) && Carbon::now()->diffInMinutes($paymentObj->created_at) > 15) {
                        // Auto cancel transaction if created date is over 15 mins ago.
                        $paystatus = CBO_LEDGERSTATUS_CANCELLED;
                    }

                    if (!is_null($paystatus)) {
                        $paymentObj->status = $paystatus;
                        $paymentObj->save();

                        $cashLedgerObj->status = $paymentObj->status;
                        $cashLedgerObj->save();

                        $this->onCashLedgerStatusUpdated($cashLedgerObj, CBO_CHARTCODE_DEPOSIT, ($paystatus == CBO_LEDGERSTATUS_CONFIRMED));
                    }

                    sleep(6); // API restriction 5 seconds, +1 for secure.
                }
            }
        }

        return;
    }

    public function checkWithdrawTrans()
    {
        return;
    }

    private function formatAmount($amount)
    {
        $amt = number_format($amount, 2, '.', '');

        return $amt;
    }

    private function getHttpClient()
    {
        return new Client(['timeout' => 60.0]);
    }

    private function getDefaultOptions()
    {
        return array(
            'content-type' => 'application/json; charset=UTF-8',
            'headers' => array(
                'Content-Type' => 'application/json',
                'Accept' => 'application/json',
                'ClientKey' => $this->clientKey,
                'SecretKey' => $this->secretKey,
                'X-PAYWALO-PLATFORM' => 'MOB',
            )
        );
    }
}
