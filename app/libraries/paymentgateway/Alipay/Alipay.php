<?php namespace App\libraries\paymentgateway\Alipay;

use App\libraries\paymentgateway\BasePaymentGateway;
use App\Models\Cashledger;
use App\Models\Paymentgateway;
use App\Models\Paymentgatewaysetting;
use Carbon\Carbon;
use Config;
use Illuminate\Http\Request;
use Lang;

class Alipay extends BasePaymentGateway
{
    const PG_CODE = 'ALP';

    private $currency;
    private $rsaObj;

    /**
     * PayWalo constructor.
     */
    public function __construct($currency)
    {
        $this->currency = $currency;
    }

    public function deposit($paymentgatewayObj)
    {
//		return Lang::get('COMMON.SUCESSFUL');
        return array(
            'code' => 'OK',
            'action' => 'alert',
            'url' => route('transaction'),
            'hint' => Lang::get('COMMON.SUCESSFUL'),
        );
    }

    public function withdraw($params)
    {
        // Not support.
        return false;
    }

    public function getBankDetailOptions($type)
    {
		return [];
    }

    public function formatOrderId($orderId)
    {
        return date('YmdHis') . str_pad($orderId, 5, '0', STR_PAD_LEFT);
    }

    public function resolveCallback(Request $request)
    {
        return response('0');
    }

    public function checkDepositTrans()
    {
        // Not support.
        return;
    }

    public function checkWithdrawTrans()
    {
        // Not support.
        return;
    }
}
