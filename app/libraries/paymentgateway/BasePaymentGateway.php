<?php namespace App\libraries\paymentgateway;

use App\libraries\App;
use App\Models\Cashledger;
use App\Models\Promocash;

abstract class BasePaymentGateway
{
    protected function onCashLedgerStatusUpdated($cashLedger, $chartcode, $isApproved)
    {
        $clgObj = new Cashledger;

        if ($chartcode == CBO_CHARTCODE_DEPOSIT) {
            $clgObj->updateAccountBalance($cashLedger->accid, CBO_CHARTCODE_DEPOSIT);

            if ($isApproved) {
                $clgObj->updateDepositWithdrawalAmt($cashLedger->accid, CBO_CHARTCODE_DEPOSIT, $cashLedger->amount);
                $clgObj->addEditPromoCashLedger($cashLedger->fdmid, $cashLedger->id, $cashLedger->status);
            } else {
                Promocash::where('status', '=', Promocash::STATUS_PENDING)
                    ->where('clgid', '=', $cashLedger->id)
                    ->update(array(
                        'status' => Promocash::STATUS_REJECT,
                    ));
            }
        } elseif ($chartcode == CBO_CHARTCODE_WITHDRAWAL) {
            $clgObj->updateAccountBalance($cashLedger->accid, CBO_CHARTCODE_WITHDRAWAL);

            if ($isApproved) {
                $clgObj->updateDepositWithdrawalAmt($cashLedger->accid, CBO_CHARTCODE_WITHDRAWAL, $cashLedger->amount);
            } else {
                // Do nothing.
            }
        }
    }

    protected function log($url, $response, $method)
    {
        App::insert_api_log( array( 'request' => $url , 'return' => $response , 'method' => $method ) );
    }
}