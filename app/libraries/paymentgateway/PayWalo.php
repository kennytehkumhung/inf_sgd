<?php namespace App\libraries\paymentgateway;

use App\Http\Controllers\Admin\CashledgerController;
use App\libraries\App;
use App\Models\Cashledger;
use App\Models\Paymentgateway;
use App\Models\Paymentgatewaysetting;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Lang;

class PayWalo
{
    private $currency;
    private $url;
    private $clientKey;
    private $secretKey;
    private $saltingKey;

    /**
     * PayWalo constructor.
     */
    public function __construct($currency)
    {
        // TODO
        $this->currency = $currency;
        $this->url = 'http://stg.paywalo.com:8086';
        $this->clientKey = 'acf056e9-5d37-4459-8d77-d5190155e2bd';
        $this->secretKey = '3d0ac5d6-cd66-4746-9e28-8f5d3186a0cb';
        $this->saltingKey = '10da8449ca2d466e9a376c6f85a7d94b';
    }

    public function deposit($paymentGatewayId)
    {
        $pgObj = Paymentgateway::find($paymentGatewayId);

        if ($pgObj) {
            $params = array(
                'Amount' => $pgObj->amount,
                'OrderDate' => Carbon::parse($pgObj->created_at)->format('c'),
                'OrderNumber' => $pgObj->orderid,
                'BankAccountID' => $pgObj->bnkid,
                'Currency' => $this->currency,
                'CallbackUrl' => url() . '/pg/pwl/' . strtolower($this->currency) . '/callback',
                'Notes' => '',
            );

            $concatenatedString = [];

            foreach($params as $key => $value) {
                $concatenatedString[] = $value;
            }

            // Generate sign.
            $concatenatedString = implode('', $concatenatedString);
            $concatenatedString = iconv(mb_detect_encoding($concatenatedString, mb_detect_order(), true), 'UTF-8', $concatenatedString);
            $params['Sign'] = strtoupper(hash_hmac('sha256', $concatenatedString, $this->saltingKey));

            // Prepare request params.
            $options = $this->getDefaultOptions();
            $options['json'] = $params;

            // Execute request.
            $request = $this->getHttpClient()->request('POST', $this->url.'/api/Payment/Deposit', $options);

            App::insert_api_log( array( 'request' => $this->url.'/api/Payment/Deposit', 'return' => $request->getStatusCode() . '|' . $request->getBody()->getContents() , 'method' => 'walopay@deposit' ) );

            return ($request->getStatusCode() == 200);
        }

        return false;
    }

    public function getBankDetailOptions()
    {
        // Execute request.
        $request = $this->getHttpClient()->request('GET', $this->url.'/api/Common/BankAccounts', $this->getDefaultOptions());
        $result = array();

        if ($request->getStatusCode() == 200) {
            $response = json_decode($request->getBody()->getContents(), true);

            if (isset($response['Status']) && $response['Status'] == 'SUCCESS') {
                foreach ($response['Data'] as $d) {
                    if (isset($d['BankCode']) && isset($d['CurrencyCode']) && $d['CurrencyCode'] == $this->currency) {
                        if ($d['BankCode'] != 'LP') {
                            // Exclude LinePay.
                            $result[$d['Id']] = [
                                'bankname' => $d['BankName'],
                                'bankaccname' => $d['AccountName'],
                                'bankaccno' => $d['AccountNumber'],
                                'currency' => $d['CurrencyCode'],
                            ];
                        }
                    }
                }
            }
        }

        return $result;
    }

    public function resolveCallback(Request $request)
    {
        $jsonData = $request->json()->all();

        App::insert_api_log( array( 'request' => $request->url() , 'return' => json_encode($jsonData) , 'method' => 'walopay@callback' ) );
        exit;

        /*
         * json status:
         * pending = 0
         * success = 1
         * failed = 2
         * error = 3
         */
        if (isset($jsonData['OrderNumber']) && $jsonData['Status'] != '0') {
            $pgsObj = Paymentgatewaysetting::where('code', '=', 'PWL')->first();

            if ($pgsObj) {
                $pgObj = Paymentgateway::where('orderid', '=', $jsonData['OrderNumber'])
                    ->where('status', '=', CBO_LEDGERSTATUS_PENDING)
                    ->first();

                if ($pgObj) {
                    $cashLedgerObj = Cashledger::where('refid', '=', $pgObj->id)
                        ->where('refobj', '=', 'Paymentgateway')
                        ->first();

                    if ($cashLedgerObj) {
                        if ($jsonData['Status'] == '1') {
                            $cashLedgerObj->remarks = 'Approved by provider.';
                            $cashLedgerObj->status = CBO_LEDGERSTATUS_MANCONFIRMED;

                            $pgObj->status = CBO_LEDGERSTATUS_MANCONFIRMED;
                        } else {
                            $cashLedgerObj->remarks = 'Rejected by provider.';
                            $cashLedgerObj->rejreason = 'Error Code: ' . $jsonData['Status'];
                            $cashLedgerObj->status = CBO_LEDGERSTATUS_MANCANCELLED;

                            $pgObj->status = CBO_LEDGERSTATUS_MANCANCELLED;
                        }

                        $cashLedgerObj->save();
                        $pgObj->save();

                        $cashLedgerController = new CashledgerController();
                        $cashLedgerController->insertCashLedgerLog($cashLedgerObj->id);


                    }
                }
            }


            // TODO
//            $pgObj = Paymentgateway::where('orderid', '=', $jsonData['OrderNumber'])
//                ->where('paytype', '=', $pgSettingObj->id)
//                ->where('status', '=', CBO_LEDGERSTATUS_PENDING)
//                ->first();
//
//            if ($pgObj) {
//                switch ($jsonData['status']) {
//                    case 0:
//                        $status = CBO_LEDGERSTATUS_PENDING;
//                        break;
//                    case 1:
//                        $status = CBO_LEDGERSTATUS_CONFIRMED;
//                        break;
//                    case 2:
//                        $status = CBO_LEDGERSTATUS_CANCELLED;
//                }
//
//                $pgObj->datetime = Carbon::parse($jsonData['TransactionDate']);
//                $pgObj->transid = $jsonData['TransactionNumber'];
////                $pgObj->succ = ;
//                $pgObj->status = $status;
//
//                if ($pgObj->save()) {
//                    $clgObj = Cashledger::find($pgObj->clgid);
//
//                    if ($status == CBO_LEDGERSTATUS_CONFIRMED) {
//                        if ($clgObj && $clgObj->status == CBO_LEDGERSTATUS_PENDING) {
//                            $clgObj->status = CBO_LEDGERSTATUS_CONFIRMED;
//                            $clgObj->save();
//                        }
//                    } else {
//                        $clgObj->status = CBO_LEDGERSTATUS_CANCELLED;
//                        $clgObj->save();
//                    }
//                }
//            }
        }


        // TODO
//        dd($request);
        /*
         * {"Amount":100.0,"ActualDepositAmount":100.0,"TransactionNumber":"D-DEMOCLIENT-2303121710008474-007988",
         * "TransactionDate":"2017-10-03T12:23:52","OrderNumber":"OD0000032","OrderDate":"2017-08-30T15:55:16",
         * "ReferenceNumber":null,"ReferenceRemarks":null,"AccountNumber":"8580313362","AccountName":"Somboon Vansavang",
         * "BankCode":"BBL","BankName":"Bangkok Bank","StatusDate":"2017-10-03T12:24:27","Status":1,"UDAAmount":103.0,
         * "Notes":null}
         */

    }


    private function getHttpClient()
    {
        return new Client(['timeout' => 60.0]);
    }

    private function getDefaultOptions()
    {
        return array(
            'content-type' => 'application/json; charset=UTF-8',
            'headers' => array(
                'Content-Type' => 'application/json',
                'Accept' => 'application/json',
                'ClientKey' => $this->clientKey,
                'SecretKey' => $this->secretKey,
                'X-PAYWALO-PLATFORM' => 'MOB',
            )
        );
    }
}
