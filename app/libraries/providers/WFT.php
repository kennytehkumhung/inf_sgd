<?php namespace App\libraries\providers;

use App\libraries\App;
use App\Models\AccountProduct;
use App\Models\Product;
use App\Models\Account;
use Config;
use Carbon\Carbon;
use Lang;
use Cache;
use DB;
use Session;

//createuser, changepassword, changestatus, getbalance, deposit, withdraw

class WFT{
	
	public function getLoginUrl($username, $currency, $language_code = 'en' , $host){

	    $lang = collect(array(
            'en' => 'EN-US',
            'cn' => 'ZH-CN',
            'tw' => 'ZH-CN',
            'id' => 'ID-ID',
            'th' => 'TH-TH',
        ));
		
		$postfields =   'action=login'.
					    '&secret='.Config::get($currency.'.wft.secret').
						'&agent='.Config::get($currency.'.wft.agent').
						'&currency='.$currency.
						'&username='.$username.
						'&lang='.$lang->get($language_code, $lang->get('en')).
						'&host='.$host;

		$result = (  (App::curlGET(Config::get($currency.'.wft.url').'?'.$postfields , true, 3 , 'getLoginUrl')));
		$result = preg_replace('/&(?=[a-z_0-9]+=)/m','&amp;',$result);
		$result = simplexml_load_string($result);

		if(isset($result->result) )
		{
			return (string)$result->result;
		}
		
		return false;
		
	}
    
    public function createUser( $params = array() ){
	
	    $postfields =   'action=create'.
					    '&secret='.Config::get($params['currency'].'.wft.secret').
						'&agent='.Config::get($params['currency'].'.wft.agent').
						'&currency='.$params['currency'].
						'&username='.$params['username'];
		
		$result = simplexml_load_string(App::curlGET(Config::get($params['currency'].'.wft.url').'?'.$postfields , true, 3 , 'createUser'));

	    if( isset($result->errcode) && ( $result->errcode == '0' || ( $result->errcode == '1' && $result->errtext == 'Username exist' ) ) )
	    {
			Accountproduct::addAccountProduct($params['username'],'WFT','');
			return true;
	    }

	    return false;
    }   
    
	public function updateUser( $params = array() ){
	
	    $postfields =   'action=update'.
					    '&secret='.Config::get($params['currency'].'.wft.secret').
						'&agent='.Config::get($params['currency'].'.wft.agent').
						'&currency='.$params['currency'].
						'&username='.$params['username'].
						'&'.Config::get($params['currency'].'.wft.betlimit');

		
		$result = simplexml_load_string(App::curlGET(Config::get($params['currency'].'.wft.url').'?'.$postfields , true, 1 , 'updateUser'));

	    if( isset($result->errcode) && ( $result->errcode == '0'  ) )
	    {

			return true;
	    }

	    return false;
    }   
    
    public function getBalance($username ,$currency){
		
	     $postfields =   'action=balance'.
					     '&secret='.Config::get($currency.'.wft.secret').
						 '&agent='.Config::get($currency.'.wft.agent').
						 '&currency='.$currency.
						 '&username='.$username ;
		
		$result = simplexml_load_string(App::curlGET(Config::get($currency.'.wft.url').'?'.$postfields , true, 5 , 'getBalance'));

	    if(isset($result->errcode) && $result->errcode == '0')
	    {
			$dateTimeNow    = Carbon::now();
			$dateTimeNowStr = $dateTimeNow->toDateTimeString();
			
			if( Session::has('userid') ){
				DB::statement('call insert_cashbalance('.Session::get('userid').',"'.Session::get('username').'","'.$currency.'",'.$this->getPrdid().','.$result->result.',"'.$dateTimeNowStr.'","'.$dateTimeNowStr.'")');
			}else{
				DB::statement('call insert_cashbalance('.Account::whereNickname($username)->whereCrccode($currency)->pluck('id').',"'.$username.'","'.$currency.'",'.$this->getPrdid().','.$result->result.',"'.$dateTimeNowStr.'","'.$dateTimeNowStr.'")');
			}
			
			return $result->result;
	    }

	    return false;
    }
	
	private function getPrdid(){
		
		$prdid = Cache::rememberForever('WFT_PRDID', function() {
                return Product::where( 'code' , '=' , 'WFT')->pluck('id');
        });
		
		return $prdid;
	}
    
	public function deposit($username,$currency,$amount,$transid){
		
		if( $amount < 0 ) return false;
		
		return $this->transferCredit( $username, $currency, $transid, 'deposit', $amount);
		
	}
		
	public function withdraw($username,$currency,$amount,$transid){

		if( $amount < 0 ) return false;
		
		return $this->transferCredit( $username, $currency, $transid, 'withdraw', $amount);
	}
	
	private function transferCredit( $username, $currency, $billno, $type, $credit){
		
		if( $currency == 'IDR' || $currency == 'VND' )
		{
			$credit = $credit / 1000;
		}
		
		$postfields =   'action='.$type.
					     '&secret='.Config::get($currency.'.wft.secret').
						 '&agent='.Config::get($currency.'.wft.agent').
						 '&currency='.$currency.
						 '&serial='.$billno.
						 '&amount='.$credit.
						 '&username='.$username;
		
		$result = simplexml_load_string(App::curlGET(Config::get($currency.'.wft.url').'?'.$postfields , true, 10 , 'TransferCredit'));

	    if(isset($result->errcode) && $result->errcode == '0')
	    {
			return true;
	    }

	    return false;
		
	}
    
}
