<?php namespace App\libraries\providers;

use App\libraries\App;
use App\libraries\TripleDES;
use App\Models\Accountproduct;
use App\Models\Account;
use App\Models\Product;
use Config;
use Lang;
use Carbon\Carbon;
use Cache;
use DB;
use Session;
use Crypt;
//getLoginUrl
//getBalance
//deposit
//withdraw
//createUser


class ALB {
	
	public function getLoginUrl($username, $currency, $language_code = 'en' ){

	    $lang = collect(array(
	        'en' => 'en',
            'cn' => 'zh_CN',
            'tw' => 'zh_CN',
            'id' => 'ms',
        ));

		$this->modify($username,$currency);
		$this->updatePassword($username,Crypt::decrypt(Session::get('enpassword')));
		
		$real_param = http_build_query(array(
							'agent'    		=> Config::get(Session::get('currency').'.alb.agent'), 
							'random'   		=> mt_rand(),
							'client'   		=> Config::get(Session::get('currency').'.alb.prefix').'_'.$username,
							'password' 		=> Crypt::decrypt(Session::get('enpassword')),
							'targetSite'    => Config::get(Session::get('currency').'.alb.targetSite'),
							'language'      => $lang->get($language_code, 'en'),
					  ));
	
		$data = base64_encode(TripleDES::encryptText($real_param, Config::get(Session::get('currency').'.alb.des_key')));

		$to_sign = $data.Config::get(Session::get('currency').'.alb.md5_key');
	
		$sign = base64_encode(md5($to_sign, TRUE));

		$result = json_decode( App::curl( http_build_query(array('data' => $data, 'sign' => $sign, 'propertyId' => Config::get(Session::get('currency').'.alb.property_id'))) , Config::get(Session::get('currency').'.alb.url').'forward_game' , true, 10 , 'getLoginUrl') );

		if( isset($result->gameLoginUrl) && $result->error_code == 0 )
		{
			return $result->gameLoginUrl;
		}
		
		return false;
		
	}	
	
	private function modify($username, $currency ){
		
		$real_param = http_build_query(array(
							'agent'    		=> Config::get(Session::get('currency').'.alb.agent'), 
							'random'   		=> mt_rand(),
							'client'   		=> Config::get(Session::get('currency').'.alb.prefix').'_'.$username,
							'password' 		=> Crypt::decrypt(Session::get('enpassword')),
							'vipHandicaps'  => Config::get(Session::get('currency').'.alb.vip_oddtype'),
							'orHandicaps'   => Config::get(Session::get('currency').'.alb.oddtype'),

					  ));
	
		$data = base64_encode(TripleDES::encryptText($real_param, Config::get(Session::get('currency').'.alb.des_key')));

		$to_sign = $data.Config::get(Session::get('currency').'.alb.md5_key');
	
		$sign = base64_encode(md5($to_sign, TRUE));

		$result = json_decode( App::curl( http_build_query(array('data' => $data, 'sign' => $sign, 'propertyId' => Config::get(Session::get('currency').'.alb.property_id'))) , Config::get(Session::get('currency').'.alb.url').'modify_client' , true, 1 , 'getLoginUrl') );
		
		return true;
		
	}
	
	public function createUser( $params = array() ){
		
		$real_param = http_build_query(array(
							'agent'    		=> Config::get(Session::get('currency').'.alb.agent'), 
							'random'   		=> mt_rand(),
							'client'   		=> Config::get(Session::get('currency').'.alb.prefix').'_'.$params['username'],
							'password' 		=> Crypt::decrypt(Session::get('enpassword')),
							'vipHandicaps'  => Config::get(Session::get('currency').'.alb.vip_oddtype'),
							'orHandicaps'   => Config::get(Session::get('currency').'.alb.oddtype'),
							'orHallRebate'  => '0',
							'laxHallRebate' => '0',
					  ));

		$data = base64_encode(TripleDES::encryptText($real_param, Config::get(Session::get('currency').'.alb.des_key')));

		$to_sign = $data.Config::get(Session::get('currency').'.alb.md5_key');
	
		$sign = base64_encode(md5($to_sign, TRUE));

		$result = json_decode( App::curl( http_build_query(array('data' => $data, 'sign' => $sign, 'propertyId' => Config::get(Session::get('currency').'.alb.property_id'))) , Config::get(Session::get('currency').'.alb.url').'check_or_create' , true, 3 , 'createUser') );

		if( isset($result->error_code) && ( $result->error_code == 'OK' || $result->error_code == 'CLIENT_EXIST' ))
		{
			Accountproduct::addAccountProduct($params['username'],'ALB','');
			return true;
		}
		
		return false;
		
	}	
	
	public function query_handicap(){
		
		$real_param = http_build_query(array('agent' => Config::get(Session::get('currency').'.alb.agent'),  'random' => mt_rand()));
	
		$data = base64_encode(TripleDES::encryptText($real_param, Config::get(Session::get('currency').'.alb.des_key')));

		$to_sign = $data.Config::get(Session::get('currency').'.alb.md5_key');
	
		$sign = base64_encode(md5($to_sign, TRUE));

		$result = json_decode( App::curl( http_build_query(array('data' => $data, 'sign' => $sign, 'propertyId' => Config::get(Session::get('currency').'.alb.property_id'))) , Config::get(Session::get('currency').'.alb.url').'query_handicap' , false) );
		
		if( $result->error_code == 0 )
		{
			var_dump($result);
		}
		
		return false;
		
	}
	
	public function getBalance($username , $currency){

		$password = Session::has('enpassword') ? Session::get('enpassword') : Account::where( 'nickname' , '=' , $username )->pluck('enpassword');
		
	    $real_param = http_build_query(array(
							'agent'    		=> Config::get($currency.'.alb.agent'), 
							'random'   		=> mt_rand(),
							'client'   		=> Config::get($currency.'.alb.prefix').'_'.$username,
							'password' 		=> Crypt::decrypt($password),
					  ));
	
		$data = base64_encode(TripleDES::encryptText($real_param, Config::get($currency.'.alb.des_key')));

		$to_sign = $data.Config::get($currency.'.alb.md5_key');
	
		$sign = base64_encode(md5($to_sign, TRUE));

		$result = json_decode( App::curl( http_build_query(array('data' => $data, 'sign' => $sign, 'propertyId' => Config::get($currency.'.alb.property_id'))) , Config::get($currency.'.alb.url').'get_balance' , true, 8 , 'getBalance') );

		if( isset($result->error_code) &&  $result->error_code == 0 && isset($result->balance) )
		{
			$dateTimeNow    = Carbon::now();
			$dateTimeNowStr = $dateTimeNow->toDateTimeString();
			
			if( Session::has('userid') )	
			{
				DB::statement('call insert_cashbalance('.Session::get('userid').',"'.$username.'","'.$currency.'",'.$this->getPrdid().','.$result->balance.',"'.$dateTimeNowStr.'","'.$dateTimeNowStr.'")');
				
			}else{
				
				DB::statement('call insert_cashbalance('.Account::whereNickname($username)->whereCrccode($currency)->pluck('id').',"'.$username.'","'.$currency.'",'.$this->getPrdid().','.$result->balance.',"'.$dateTimeNowStr.'","'.$dateTimeNowStr.'")');
			}
			
			return $result->balance;
		}
		
		return false;
		
	}
	
	private function getPrdid(){
		
		$prdid = Cache::rememberForever('ALB_PRDID', function() {
                return Product::where( 'code' , '=' , 'ALB')->pluck('id');
        });
		
		return $prdid;
	}
	
	public function deposit($username,$currency,$amount,$transid){
		
		if( $amount < 0 ) return false;
		
		return $this->transferCredit( $username, $currency, $transid, 1, $amount);
		
	}
		
	public function withdraw($username,$currency,$amount,$transid){
		
		if( $amount < 0 ) return false;
		
		return $this->transferCredit( $username, $currency, $transid, 0, $amount);
	}
	
	private function transferCredit( $username,$currency, $billno, $type, $credit){
		
		if( $currency == 'IDR' || $currency == 'VND' )
		{
			$credit = $credit / 1000;
		}
		
		$accObj = Account::where('nickname', '=', $username)->where('crccode', '=', $currency)->first();
		
		$real_param = http_build_query(array(
							'agent'    		=> Config::get($currency.'.alb.agent'),
							'random'   		=> mt_rand(),
							'client'   		=> Config::get($currency.'.alb.prefix').'_'.$username,
							'password' 		=> Crypt::decrypt($accObj->enpassword),
							'sn'   			=> Config::get($currency.'.alb.property_id').Config::get($currency.'.alb.prefix').str_pad($billno, 11, "0", STR_PAD_LEFT),
							'operFlag'   	=> $type,
							'credit'   		=> $credit,
					  ));
	
		$data = base64_encode(TripleDES::encryptText($real_param, Config::get($currency.'.alb.des_key')));

		$to_sign = $data.Config::get($currency.'.alb.md5_key');
	
		$sign = base64_encode(md5($to_sign, TRUE));

		$result = json_decode( App::curl( http_build_query(array('data' => $data, 'sign' => $sign, 'propertyId' => Config::get($currency.'.alb.property_id'))) , Config::get($currency.'.alb.url').'agent_client_transfer' , true, 30 , 'transferCredit' ) );


		if( $result->error_code == 'OK' )
		{
			return true;
		}
		
		return false;
	}

	public function updatePassword($username,$password){
		
	    $real_param = http_build_query(array(
							'agent'    		=> Config::get(Session::get('currency').'.alb.agent'), 
							'random'   		=> mt_rand(),
							'client'   		=> Config::get(Session::get('currency').'.alb.prefix').'_'.$username,
							'newPassword' 	=> $password,
					  ));
	
		$data = base64_encode(TripleDES::encryptText($real_param, Config::get(Session::get('currency').'.alb.des_key')));

		$to_sign = $data.Config::get(Session::get('currency').'.alb.md5_key');
	
		$sign = base64_encode(md5($to_sign, TRUE));

		$result = json_decode( App::curl( http_build_query(array('data' => $data, 'sign' => $sign, 'propertyId' => Config::get(Session::get('currency').'.alb.property_id'))) , Config::get(Session::get('currency').'.alb.url').'setup_client_password' , true, 1 , 'updatePassword') );

		if( isset($result->error_code) &&  $result->error_code == 0 )
		{
			return true;
		}
		
		return false;
		
	}

	

	

	
   
}
?>