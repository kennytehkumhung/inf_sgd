<?php namespace App\libraries\providers;

use App\libraries\App;
use App\Models\Account;
use App\Models\AccountProduct;
use App\Models\Product;
use Config;
use Carbon\Carbon;
use Lang;
use Cache;
use DB;
use Session;
use Crypt;
//getLoginUrl
//getBalance
//deposit
//withdraw
//createUser


class SCR
{


    public function createUser($params = array())
    {
        $data = array(
            'apiuserid' => Config::get($params['currency'] . '.scr.ApiUserID'),
            'apipassword' => Config::get($params['currency'] . '.scr.ApiPassword'),
            'operation' => 'addnewplayer',
            'playername' => $params['username'],
            'playertelno' => '0163263489',
            'playerdescription' => $params['username'],
            'playerpassword' => $this->formatPlayerPassword(Crypt::decrypt($params['password'])),
        );

        $postfields = json_encode($data);

        $acc = Account::where('id', '=', $params['userid'])->first();

        if (strlen($acc->scrid) > 0) {
            return $this->updatePassword($params);
        } else {
            $acc->scrid = 0;
            $acc->save();

            try {
                $result = json_decode($this->curl(Config::get($params['currency'] . '.scr.BaseServiceURL') . 'player', $postfields, true, 3, 'createUser'));

                if (isset($result->returncode) && ($result->returncode == 0 || $result->returncode == -300)) {
                    $acc->scrid = $result->playerid;
                    $acc->save();

                    Accountproduct::addAccountProduct($params['username'], 'SCR', '');

                    return true;
                } else {
                    Account::where('id', '=', $params['userid'])
                        ->where('scrid', '=', 0)
                        ->update([
                            'scrid' => '',
                        ]);
                }
            } catch (\Exception $e) {
                Account::where('id', '=', $params['userid'])
                    ->where('scrid', '=', 0)
                    ->update([
                        'scrid' => '',
                    ]);
            }
        }

        return false;
    }

    public function getBalance($username, $currency)
    {
        $acc = Account::where('nickname', '=', $username)
            ->where('crccode', '=', $currency)
            ->first();

        $scrid = $acc->scrid;

        $data = array(
            'apiuserid' => Config::get($currency . '.scr.ApiUserID'),
            'apipassword' => Config::get($currency . '.scr.ApiPassword'),
            'operation' => 'getplayerinfo',
            'playerid' => $scrid,
        );

        $postfields = json_encode($data);

        $result = json_decode($this->curl(Config::get($currency . '.scr.BaseServiceURL') . 'player', $postfields, true, 3, 'getBalance'));

        if (isset($result->returncode) && ($result->returncode == 0 || $result->returncode == -300)) {

            $dateTimeNow = Carbon::now();
            $dateTimeNowStr = $dateTimeNow->toDateTimeString();

            if (Session::has('userid')) {
                DB::statement('call insert_cashbalance(' . Session::get('userid') . ',"' . Session::get('username') . '","' . $currency . '",' . $this->getPrdid() . ',' . $result->balance . ',"' . $dateTimeNowStr . '","' . $dateTimeNowStr . '")');
            } else {
                DB::statement('call insert_cashbalance(' . Account::whereNickname($username)->pluck('id') . ',"' . $username . '","' . $currency . '",' . $this->getPrdid() . ',' . $result->balance . ',"' . $dateTimeNowStr . '","' . $dateTimeNowStr . '")');
            }

            return $result->balance;
        }

        return false;
    }
	
	

    private function getPrdid()
    {
        $prdid = Cache::rememberForever('SCR_PRDID', function () {
            return Product::where('code', '=', 'SCR')->pluck('id');
        });

        return $prdid;
    }

    public function updatePassword($params = array())
    {
		
//        $acc = Account::where('id', '=', $params['userid'])->first();
//
//        $data = array(
//            'apiuserid' => Config::get($params['currency'] . '.scr.ApiUserID'),
//            'apipassword' => Config::get($params['currency'] . '.scr.ApiPassword'),
//            'operation' => 'updateplayerinfo',
//            'playerid' => $acc->scrid,
//            'playername' => $params['username'],
//            'playertelno' => '',
//            'playerdescription' => $params['username'],
//            'playerpassword' => $this->formatPlayerPassword(Crypt::decrypt($params['password'])),
//        );
//
//        $postfields = json_encode($data);
//
//        $result = json_decode($this->curl(Config::get($params['currency'] . '.scr.BaseServiceURL') . 'player', $postfields, true, 1, 'updatePassword'));
//
//        if (isset($result->returncode) && ($result->returncode == 0 || $result->returncode == -300)) {
//            return true;
//        }

        return false;
    }

    public function deposit($username, $currency, $amount, $transid)
    {
        if ($amount < 0) return false;

        return $this->transferCredit($username, $currency, $transid, 'deposit', $amount);
    }

    public function withdraw($username, $currency, $amount, $transid)
    {
        if ($amount < 0) return false;

        return $this->transferCredit($username, $currency, $transid, 'withdraw', $amount);
    }

    private function transferCredit($username, $currency, $billno, $type, $credit)
    {
        $acc = Account::where('nickname', '=', $username)
            ->where('crccode', '=', $currency)
            ->first();

        $scrid = $acc->scrid;

        $data = array(
            'apiuserid' => Config::get($currency . '.scr.ApiUserID'),
            'apipassword' => Config::get($currency . '.scr.ApiPassword'),
            'operation' => $type,
            'playerid' => $scrid,
            'amount' => (string)$credit,
        );
		
        $postfields = json_encode($data);

        $result = json_decode($this->curl(Config::get($currency . '.scr.BaseServiceURL') . 'funds', $postfields, true, 20, 'transferCredit'));

        if (isset($result->returncode) && $result->returncode == 0) {
            return true;
        }

        return false;
    }

    public function formatPlayerPassword($password)
    {
        // Password format sample: RW3xxxxxx, IW3xxxxxx
        return title_case(substr(Config::get('setting.opcode'), 0, 2)) . '3' . $password;
    }

    public function getScrIdByUsername($username, $currency)
    {
        $accObj = Account::where('nickname', '=', $username)
            ->where('crccode', '=', $currency)
            ->where('status', '=', 1)
            ->select(['scrid'])
            ->first();

        return $accObj->scrid;
    }

    function curl($url, $postFields, $log = true, $timeout = 30, $method = '')
    {
        $headers[] = "Content-Type:application/json";
        $data_json = $postFields;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Content-Length: ' . strlen($data_json)));
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);

        if ($log)
            App::insert_api_log(array('request' => $url . '?' . $data_json, 'return' => $response, 'method' => $method));

        return $response;
    }
}
