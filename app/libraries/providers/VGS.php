<?php namespace App\libraries\providers;

use App\libraries\App;
use App\Models\AccountProduct;
use App\Models\Product;
use App\Models\Account;
use Crypt;
use Config;
use Carbon\Carbon;
use Lang;
use Cache;
use DB;
use Session;

//createuser, changepassword, changestatus, getbalance, deposit, withdraw

class VGS{
    
    public function createUser( $params = array() ){
	
	    $password = Crypt::decrypt($params['password']);
	    
	    $postfields =   'operatorUsername='.Config::get($params['currency'].'.vgs.username').
						'&username='. $params['username'] .
						'&firstName=' . $params['username'] .
						'&lastName=' .$params['username'];

	    $accessPassword = strtoupper(md5(Config::get($params['currency'].'.vgs.password').str_replace(' ' , '%20', $postfields)));
	    
		$result = simplexml_load_string(App::curlGET(Config::get($params['currency'].'.vgs.url').'CreateAccount?accessKey='.$accessPassword.'&'.$postfields, true, 3 , 'createUser'));

	    if( isset($result->errorCode) && ( $result->errorCode == '0' || $result->errorCode == '3' ) )
	    {
			Accountproduct::addAccountProduct($params['username'],'VGS','');
			return true;
	    }

	    return false;
    }   
	
	
	public function getLoginUrl($params, $session, $language, $ip){

		$lang = collect(array(
		    'cn' => 'cn',
		    'tw' => 'cn',
		    'en' => 'en',
		    'id' => 'id',
		    'ja' => 'ja',
		    'ko' => 'ko',
		    'th' => 'th',
		    'vi' => 'vi',
        ));

	    $postfields =   'operatorUsername='.Config::get($params['currency'].'.vgs.username').
						'&username='. $params['username'] .
						'&sessionid=' . $session .
						'&page=103' .
						'&language=' . $lang->get($language, 'en') .
						'&clientip=' . $ip;

	    $accessPassword = strtoupper(md5(Config::get($params['currency'].'.vgs.password').$postfields));
	    
	    $result = simplexml_load_string(App::curlGET(Config::get($params['currency'].'.vgs.url').'GetGameURL?accessKey='.$accessPassword.'&'.$postfields,true,5));

	    if(isset($result->errorCode) && $result->errorCode == '0')
	    {
			return (string)$result->result;
	    }

	    return false;
    }
    
    public function getBalance($username ,$currency){
	    $postfields =   'operatorUsername='.Config::get($currency.'.vgs.username').
						'&username='. $username;
	    
	    $accessPassword = strtoupper(md5(Config::get($currency.'.vgs.password').$postfields));

	    $result = simplexml_load_string(App::curlGET(Config::get($currency.'.vgs.url').'GetAccountBalance?accessKey='.$accessPassword.'&'.$postfields, true, 5 , 'getBalance'));

	    if(isset($result->errorCode) && $result->errorCode == '0')
	    {
			
			$dateTimeNow    = Carbon::now();
			$dateTimeNowStr = $dateTimeNow->toDateTimeString();
				
			if( Session::has('userid') ){
			
				DB::statement('call insert_cashbalance('.Session::get('userid').',"'.Session::get('username').'","'.$currency.'",'.$this->getPrdid().','.$result->result.',"'.$dateTimeNowStr.'","'.$dateTimeNowStr.'")');
			}else{
				DB::statement('call insert_cashbalance('.Account::whereNickname($username)->whereCrccode($currency)->pluck('id').',"'.$username.'","'.$currency.'",'.$this->getPrdid().','.$result->result.',"'.$dateTimeNowStr.'","'.$dateTimeNowStr.'")');
			}
			
	        if ($currency == 'IDR') {
	            return ((float) $result->result / 1000);
            }

			return $result->result;
	    }

	    return false;
    }
	
	private function getPrdid(){
		
		$prdid = Cache::rememberForever('VGS_PRDID', function() {
                return Product::where( 'code' , '=' , 'VGS')->pluck('id');
        });
		
		return $prdid;
	}
    
    
    public function deposit($username, $currency, $amount , $transID){
		
		if( $currency == 'IDR' || $currency == 'VND' )
		{
//			$amount = $amount / 1000;
		}

	    $postfields =   'operatorUsername='.Config::get($currency.'.vgs.username').
			    '&username='. $username .
			    '&transactionid='. $transID .
			    '&amount='. $amount;
	    
	    $accessPassword = strtoupper(md5(Config::get($currency.'.vgs.password').$postfields));
	    
	    $result = simplexml_load_string(App::curlGET(Config::get($currency.'.vgs.url').'AccountDeposit?accessKey='.$accessPassword.'&'.$postfields, true, 10 , 'transferCredit'));	 
	  
	    if(isset($result->errorCode) && $result->errorCode == '0')
	    {
		return true;
	    }

	    return false;	    
    }
    
    public function withdraw($username, $currency , $amount , $transID){
		
		if( $currency == 'IDR' || $currency == 'VND' )
		{
//			$amount = $amount / 1000;
		}

	    $postfields =   'operatorUsername='.Config::get($currency.'.vgs.username').
			    '&username='. $username .
			    '&transactionid='. $transID .
			    '&amount='. $amount;
	    
	    $accessPassword = strtoupper(md5(Config::get($currency.'.vgs.password').$postfields));
	    
	    $result = simplexml_load_string(App::curlGET(Config::get($currency.'.vgs.url').'AccountWithdraw?accessKey='.$accessPassword.'&'.$postfields, true, 10 , 'transferCredit'));	 
	    
	    if(isset($result->errorCode) && $result->errorCode == '0')
	    {
		return true;
	    }

	    return false;	
    }
	

    
}
