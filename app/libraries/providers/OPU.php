<?php namespace App\libraries\providers;

use App\libraries\App;
use App\Models\AccountProduct;
use App\Models\Account;
use App\Models\Product;
use Carbon\Carbon;
use Crypt;
use Config;
use Lang;
use Cache;
use DB;
use Session;

//createuser, changepassword, changestatus, getbalance, deposit, withdraw

class OPU{
    
    public function createUser( $params, $language = 'en' ){
	
	    $postfields = 'vendor_id='.Config::get($params['currency'].'.opu.vendor_id').
					  '&operator_id='.Config::get($params['currency'].'.opu.operator_id').
					  '&user_id='.$params['username'].
					  '&currency='.strtoupper($params['currency']);

		$result = json_decode( App::curl( $postfields , Config::get($params['currency'].'.opu.url').'CheckOrCreateGameAccout' , true, 3 , 'createUser') );
		
		if( isset($result) && ( $result->error_code == '00' || $result->error_code == '30.02' ))
		{
			Accountproduct::addAccountProduct($params['username'],'OPU','');
			return true;
		}
		
		return false;
    }   
	
    
    public function getBalance($username ,$currency){
        
	    $postfields = 'vendor_id='.Config::get($currency.'.opu.vendor_id').
					  '&operator_id='.Config::get($currency.'.opu.operator_id').
					  '&user_id='.$username.
					  '&currency='.strtoupper($currency);

		$result = json_decode( App::curl( $postfields , Config::get($currency.'.opu.url').'GetBalance' , true, 5 , 'getBalance') );

		if( isset($result->amount) )
		{
			$dateTimeNow    = Carbon::now();
			$dateTimeNowStr = $dateTimeNow->toDateTimeString();
				
			if( Session::has('userid') ){

				DB::statement('call insert_cashbalance('.Session::get('userid').',"'.$username.'","'.$currency.'",'.$this->getPrdid().','.$result->amount.',"'.$dateTimeNowStr.'","'.$dateTimeNowStr.'")');
			
			}else{
				DB::statement('call insert_cashbalance('.Account::whereNickname($username)->whereCrccode($currency)->pluck('id').',"'.$username.'","'.$currency.'",'.$this->getPrdid().','.$result->amount.',"'.$dateTimeNowStr.'","'.$dateTimeNowStr.'")');
			}
			
			return floor($result->amount * 100) / 100;
		}
		
		return false;
    }
	
	private function getPrdid(){
		
		$prdid = Cache::rememberForever('OPU_PRDID', function() {
                return Product::where( 'code' , '=' , 'OPU')->pluck('id');
        });
		
		return $prdid;
	}
    
    
    public function deposit($username, $currency, $amount , $transID){

		if( $amount < 0 ) return false;
		
		return $this->transferCredit($username, $currency, $transID, 'IN', $amount);   
    }
    
    public function withdraw($username, $currency , $amount , $transID){

		if( $amount < 0 ) return false;
		
		return $this->transferCredit($username, $currency, $transID, 'OUT', $amount);
    }
	
	private function transferCredit( $username, $currency, $billno, $type, $credit){
		
	 	if( $currency == 'IDR' || $currency == 'VND' )
		{
			$credit = $credit / 1000;
		}

		$postfields = 'vendor_id='.Config::get($currency.'.opu.vendor_id').
					  '&operator_id='.Config::get($currency.'.opu.operator_id').
					  '&user_id='.$username.
					  '&currency='.strtoupper($currency).
					  '&billno='.str_pad($billno, 12, "0", STR_PAD_LEFT).
					  '&type='.$type.
					  '&credit='.$credit;

		$result = json_decode( App::curl( $postfields , Config::get($currency.'.opu.url').'TransferCredit' , true, 10 , 'transferCredit') );

		if( $result->error_code == '00' )
		{
			return true;
		}
		
		return false;
	}
	
  

}
