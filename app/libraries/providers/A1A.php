<?php namespace App\libraries\providers;

use App\libraries\App;
use App\Models\AccountProduct;
use Config;
//getLoginUrl
//getBalance
//deposit
//withdraw
//createUser


class A1A {

	public function getLoginUrl($username, $currency, $gameId ){
		
		$postfields = 'cmd=gameOpen'.
					  '&hallId='.Config::get($currency.'.a1a.hall').
					  '&hallKey='.Config::get($currency.'.a1a.key').
					  '&login='.strtoupper($username).
					  '&gameId='.$gameId.
					  'demo=0'.
					  'language=en';
	

		$result = json_decode( App::curl( $postfields , Config::get($currency.'.a1a.url') , true, 10 , 'getLoginUrl') );

		if( isset($result) && $result->status == 'success' )
		{
			return $result->content;
		}
		
		return false;
	}
	
	public function createUser( $params = array() ){
		
		$postfields = 'cmd=remoteLogin'.
					  '&hallId='.Config::get($params['currency'].'.a1a.hall').
					  '&hallKey='.Config::get($params['currency'].'.a1a.key').
					  '&login='.strtoupper($params['username']);
	
		$result = $this->getBalance( strtoupper($params['username']) , $params['currency'] );

		$result = json_decode( App::curl( $postfields , Config::get($params['currency'].'.a1a.url') , true, 5 , 'createUser') );

		if( isset($result) && $result->status == 'success' )
		{
			Accountproduct::addAccountProduct($params['username'],'a1a','');
			return $result->content;
		}
		
		return false;
		
		
	}
	
	public function getBalance($username,$currency){
		
		$postfields = 'cmd=getBalance'.
					  '&hallId='.Config::get($currency.'.a1a.hall').
					  '&hallKey='.Config::get($currency.'.a1a.key').
					  '&userLogin='.strtoupper($username);
	

		$result = json_decode( App::curl( $postfields , Config::get($currency.'.a1a.url') , true, 10 , 'getBalance') );

		if( isset($result) && $result->status == 'success' )
		{
			return $result->content->cash;
		}
		
		return false;
		
	}
	
	public function deposit($username,$currency,$amount,$transid){
		
		if( $amount < 0 ) return false;
		
		return $this->transferCredit( $username, $currency, $transid, 'in', $amount);
		
	}
		
	public function withdraw($username,$currency,$amount,$transid){

		if( $amount < 0 ) return false;
		
		return $this->transferCredit( $username, $currency, $transid, 'out', $amount);
	}	
	
	public function gamesList($currency){

		$postfields = 'cmd=gameList'.
					  '&hallId='.Config::get($currency.'.a1a.hall').
					  '&hallKey='.Config::get($currency.'.a1a.key');

	

		$result = json_decode( App::curl( $postfields , Config::get($currency.'.a1a.url') , false) );

		if( isset($result) && $result->status == 'success' )
		{
			return $result->content;
		}
		
		return false;
	}
	
	private function transferCredit( $username, $currency, $billno, $type, $credit){

		$postfields = 'cmd=cashOperation'.
					  '&hallId='.Config::get($currency.'.a1a.hall').
					  '&hallKey='.Config::get($currency.'.a1a.key').
					  '&login='.strtoupper($username).
					  '&operation='.$type.
					  '&cash='.$credit;
	

		$result = json_decode( App::curl( $postfields , Config::get($currency.'.a1a.url') , true, 30 , 'transferCredit') );

		if( isset($result) && $result->status == 'success' )
		{
			return $result->content->cash;
		}
		
		return false;
		
	}


	

	

	
   
}
?>