<?php namespace App\libraries\providers;

use App\libraries\App;
use App\Models\Accountproduct;
use App\Models\Account;
use Config;
use Session;
//getLoginUrl
//getBalance
//deposit
//withdraw
//createUser


class ASC {

	public function getLoginUrl($username, $currency, $language_code = null, $pageStyle = 1, $oddsStyle = 1){
		$languages = array(
		    'cn' => 'CH',
		    'en' => 'EN',
		    'th' => 'TH',
		    'vi' => 'VN',
		);
		$lang = $languages['en']; // Default language.
		
		if (!is_null($language_code) && array_key_exists($language_code, $languages)) {
			$lang = $languages[$language_code];
		}
		
		if (is_null($username)) {
			// No username given, use public page URL.
			return $data['login_url'] = Config::get($currency.'.asc.public_url').'?language='.  strtolower($languages[$language_code]);
		}
		
		$postfields = 'APIPassword='.Config::get($currency.'.asc.key').
			'&MemberAccount='.  strtolower($username);
		$result = simplexml_load_string( App::curlGET( Config::get($currency.'.asc.url').'/Login?'.$postfields, false, 10) );
		$loginCode = '';
		
		if(isset($result->errcode) && ($result->errcode == "0" || $result->errcode == "000000")) {
			$loginCode = $result->result;
		}

		$postfields = 'LoginCode='.$loginCode.
			'&Language='.$lang.
			'&PageStyle='.$pageStyle.
			'&OddsStyle='.$oddsStyle;
			
		$useragent=$_SERVER['HTTP_USER_AGENT'];
		if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4)))
		{
			return 'http://4g.api.info99bet.com/Memberlogin.aspx?'.$postfields;
		}
		elseif( stristr($_SERVER['HTTP_USER_AGENT'], 'Mozilla/5.0(iPad;') ){
			return 'http://4g.api.info99bet.com/Memberlogin.aspx?'.$postfields;
		}elseif(strstr($_SERVER['HTTP_USER_AGENT'], 'iPad')){
			return 'http://4g.api.info99bet.com/Memberlogin.aspx?'.$postfields;
		}
		else
		{
			return Config::get($currency.'.asc.login_url').'?'.$postfields;
		}
		
		
	}
	
	public function createUser( $params = array() ){
		//date_default_timezone_set("US/Eastern");
		$memAcc = strtolower($params['username']);
		$postfields = 'APIPassword='.Config::get($params['currency'].'.asc.key').
//			'&AgentAccount='.Config::get($params['currency'].'.asc.agtid').
			'&AgentAccount='.
			'&MemberAccount='.$memAcc.
			'&NickName='.$memAcc.
			'&Currency='.$params['currency'];

		$result = simplexml_load_string( App::curlGET( Config::get($params['currency'].'.asc.url').'/Register?'.$postfields  , true, 5 , 'createUser') );
		
		if( isset($result->errcode) && ($result->errcode == "0" || $result->errcode == "000000" || $result->errcode == "100002"))
		{
			// Error code 100002: member account exists.
			if( !Accountproduct::is_exist( $params['username'] ,'asc'))
			{
				Accountproduct::addAccountProduct($params['username'],'asc','');
			}
			return $result->result;
		}
		
		return false;
		
	}
	
	public function updateUser( $params = array() ){
		
		$postfields = 'APIPassword='.Config::get($params['currency'].'.asc.key').
			'&MemberAccount='.strtolower($params['username']).
			'&'.Config::get($params['currency'].'.asc.betlimit');
		
		$result = simplexml_load_string( App::curl( $postfields , Config::get($params['currency'].'.asc.url').'/UpdateMemberLimit' , true, 5 , 'updateUser') );
		
		if( isset($result->errcode) && ($result->errcode == '0' || $result->errcode == "000000" ))
		{
			return true;
		}
		
		return false;
		
	}	
	
	public function updateGroupComm( $params = array() ){
		
		$postfields = 'APIPassword='.Config::get($params['currency'].'.asc.key').
			'&MemberAccount='.strtolower($params['username']).
			'&GroupComm=B';
		
		$result = simplexml_load_string( App::curl( $postfields , Config::get($params['currency'].'.asc.url').'/UpdateGroupComm' , true, 5 , 'updateGroupComm') );
		
		if( isset($result->errcode) && ($result->errcode == '0' || $result->errcode == "000000" ))
		{
			return true;
		}
		
		return false;
		
	}
	
	public function getBalance($username,$currency){
		$postfields = 'APIPassword='.Config::get($currency.'.asc.key').
			'&MemberAccount='.  strtolower($username);

		$result = simplexml_load_string( App::curlGET( Config::get($currency.'.asc.url').'/GetBalance?'.$postfields , true, 10 , 'getBalance') );

		if( isset($result->result) && isset($result->result->Balance) )
		{
			return $result->result->Balance;
		}
		
		return false;
		
	}
	
	public function deposit($username,$currency,$amount,$transid){
		
		if( $amount < 0 ) return false;
		
		return $this->transferCredit( $username, $currency, $transid, '0', $amount);
		
	}
		
	public function withdraw($username,$currency,$amount,$transid){

		if( $amount < 0 ) return false;
		
		return $this->transferCredit( $username, $currency, $transid, '1', $amount);
	}
	
	private function transferCredit( $username, $currency, $billno, $type, $credit){
		
	 	if( $currency == 'IDR' || $currency == 'VND' )
		{
			$credit = $credit / 1000;
		}

		$apiPass = Config::get($currency.'.asc.key');
		$memAcc = strtolower($username);
		$amount = number_format($credit, 4, '.', '');
		$postfields = 'APIPassword='.$apiPass.
			'&MemberAccount='.$memAcc.
			'&SerialNumber='.str_pad($billno, 12, "0", STR_PAD_LEFT).
			'&Amount='.$amount.
			'&TransferType='.$type.
			'&Key='.  substr(md5($apiPass.$memAcc.$amount), -6);

		$result = simplexml_load_string( App::curlGET( Config::get($currency.'.asc.url').'/Transfer?'.$postfields , true, 30 , 'transferCredit') );

		if( isset($result->errcode) && ($result->errcode == "0" || $result->errcode == "000000") )
		{
			return true;
		}
		
		return false;
	}
}
?>