<?php namespace App\libraries\providers;

use App\libraries\App;
use App\Models\Account;
use App\Models\AccountProduct;
use App\Models\Product;
use Config;
use Carbon\Carbon;
use Cache;
use Crypt;
use DB;
use Session;

//getLoginUrl
//getBalance
//deposit
//withdraw

class MNT
{

    public function createUser($params = array())
    {
        $data = array(
            'gmAcc' => Config::get($params['currency'] . '.mnt.api_username'),
            'gmPwd' => md5(Config::get($params['currency'] . '.mnt.api_password')),
            'playerAcc' => strtoupper(Config::get($params['currency'] . '.mnt.prefix') . $params['username']),
            'playerPwd' => Crypt::decrypt($params['password']),
//            'washRatio' => '',
            'aliasName' => strtoupper(Config::get($params['currency'] . '.mnt.prefix') . $params['username']),
            'sign' => '',
        );

        $data['sign'] = md5($data['gmAcc'] . $data['gmPwd'] . $data['playerAcc'] . $data['playerPwd'] . Config::get($params['currency'] . '.mnt.auth'));

        $result = json_decode(App::curlGET(Config::get($params['currency'] . '.mnt.url') . '/appaspx/CreatePlayer.aspx?' . http_build_query($data), true, 3, 'createUser'));

        if (isset($result->result) && ($result->result == 0 || $result->result == 12)) {
            Accountproduct::addAccountProduct($params['username'], 'MNT', '');
            return true;
        }

        return false;
    }

    public function getLoginUrl($username, $currency, $password)
    {
        // MNT does not have web version, only downloadable exe, Android and iOS app.
        return false;
    }

    public function updatePassword($username, $password, $currency)
    {
        // Provider does not allow user deposit/withdrawal when user is playing game.
        if (strlen($password) > 18) {
            $password = Crypt::decrypt($password);
        }

        $data = array(
            'gmAcc' => Config::get($currency . '.mnt.api_username'),
            'gmPwd' => md5(Config::get($currency . '.mnt.api_password')),
            'playerAcc' => strtoupper(Config::get($currency . '.mnt.prefix') . $username),
            'newPwd' => $password,
            'sign' => '',
        );

        $data['sign'] = md5($data['gmAcc'] . $data['gmPwd'] . $data['playerAcc'] . $data['newPwd'] . Config::get($currency . '.mnt.auth'));

        $url = Config::get($currency . '.mnt.url') . '/appaspx/UpdatePlayerPwd.aspx?';

        $result = json_decode(App::curlGET($url . http_build_query($data), true, 5, 'updatePassword'));

        if (isset($result->result) && $result->result == 0) {
            return true;
        }
    }

    public function isUserOnline($username, $currency)
    {
        // Provider does not allow user deposit/withdrawal when user is playing game.
        $data = array(
            'gmAcc' => Config::get($currency . '.mnt.api_username'),
            'gmPwd' => md5(Config::get($currency . '.mnt.api_password')),
            'playerAcc' => strtoupper(Config::get($currency . '.mnt.prefix') . $username),
            'sign' => '',
        );

        $data['sign'] = md5($data['gmAcc'] . $data['gmPwd'] . $data['playerAcc'] . Config::get($currency . '.mnt.auth'));

        $url = Config::get($currency . '.mnt.url') . '/appaspx/QueryPlayerOnline.aspx?';

        $result = json_decode(App::curlGET($url . http_build_query($data), true, 5, 'isUserOnline'));

        if (isset($result->result) && $result->result == 0 && isset($result->online)) {
            return $result->online;
        }

        return false;
    }

    public function logoutUser($username, $currency)
    {
        $data = array(
            'gmAcc' => Config::get($currency . '.mnt.api_username'),
            'gmPwd' => md5(Config::get($currency . '.mnt.api_password')),
            'playerAcc' => strtoupper(Config::get($currency . '.mnt.prefix') . $username),
            'forbidTime' => 5,
            'sign' => '',
        );

        $data['sign'] = md5($data['gmAcc'] . $data['gmPwd'] . $data['playerAcc'] . $data['forbidTime'] . Config::get($currency . '.mnt.auth'));

        $url = Config::get($currency . '.mnt.url') . '/appaspx/LogoutPlayer.aspx?';

        $result = json_decode(App::curlGET($url . http_build_query($data), true, 5, 'logoutUser'));

//        if (isset($result->result) && $result->result == 0) {
            return true;
//        }

//        return false;
    }

    public function getBalance($username, $currency)
    {
        $data = array(
            'gmAcc' => Config::get($currency . '.mnt.api_username'),
            'gmPwd' => md5(Config::get($currency . '.mnt.api_password')),
            'playerAcc' => strtoupper(Config::get($currency . '.mnt.prefix') . $username),
            'sign' => '',
        );

        $data['sign'] = md5($data['gmAcc'] . $data['gmPwd'] . $data['playerAcc'] . Config::get($currency . '.mnt.auth'));

        $url = Config::get($currency . '.mnt.url') . '/appaspx/QueryPlayerInfo.aspx?';

        $result = json_decode(App::curlGET($url . http_build_query($data), true, 5, 'getBalance'));

        if (isset($result->result) && $result->result == 0) {
            $dateTimeNow    = Carbon::now();
            $dateTimeNowStr = $dateTimeNow->toDateTimeString();

            if( Session::has('userid') ){
                DB::statement('call insert_cashbalance('.Session::get('userid').',"'.Session::get('username').'","'.$currency.'",'.$this->getPrdid().','.$result->money.',"'.$dateTimeNowStr.'","'.$dateTimeNowStr.'")');
            }else{
                DB::statement('call insert_cashbalance('.Account::whereNickname($username)->whereCrccode($currency)->pluck('id').',"'.$username.'","'.$currency.'",'.$this->getPrdid().','.$result->money.',"'.$dateTimeNowStr.'","'.$dateTimeNowStr.'")');
            }

            return $result->money;
        }

        return false;
    }

    private function getPrdid()
    {
        $prdid = Cache::rememberForever('MNT_PRDID', function () {
            return Product::where('code', '=', 'MNT')->pluck('id');
        });

        return $prdid;
    }

    public function deposit($username, $currency, $amount, $transID)
    {
        if ($amount < 0) return false;

        return $this->transferCredit($username, $currency, $transID, 0, $amount);
    }

    public function withdraw($username, $currency, $amount, $transID)
    {
        if ($amount < 0) return false;

        return $this->transferCredit($username, $currency, $transID, 1, $amount);
    }

    private function transferCredit($username, $currency, $transID, $type, $amount)
    {
        if ($this->isUserOnline($username, $currency)) {
            return false;
        }

        $data = array(
            'gmAcc' => Config::get($currency . '.mnt.api_username'),
            'gmPwd' => md5(Config::get($currency . '.mnt.api_password')),
            'playerAcc' => strtoupper(Config::get($currency . '.mnt.prefix') . $username),
            'score' => intval($amount),
            'userOrderId' => $transID,
            'sign' => '',
        );

        $data['sign'] = md5($data['gmAcc'] . $data['gmPwd'] . $data['playerAcc'] . $data['score'] . $data['userOrderId'] . Config::get($currency . '.mnt.auth'));

        $url = Config::get($currency . '.mnt.url') . '/appaspx/' . ($type == 0 ? 'PlayerSaveMoney.aspx' : 'PlayerDrawMoney.aspx') . '?';

        $result = json_decode(App::curlGET($url . http_build_query($data), true, 10, 'transferCredit'));

        if (isset($result->result) && $result->result == 0) {
            return true;
        }

        return false;
    }
}
