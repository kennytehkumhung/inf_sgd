<?php namespace App\libraries\providers;

use App\libraries\App;
use App\Models\Accountproduct;
use App\Models\Betmigcategory;
use App\Models\Account;
use App\Models\Product;
use Illuminate\Http\Request;
use Config;
use Crypt;
use Carbon\Carbon;
use Lang;
use Cache;
use DB;
use Session;
use SoapClient;
use SoapVar;
use SoapHeader;

class MIG{

	public function createUser( $params = array() ){
		
		$url = "https://entv3-04.totalegame.net?WSDL";
		$client = new SoapClient($url, array('compression' => SOAP_COMPRESSION_ACCEPT | SOAP_COMPRESSION_GZIP));

		$result = $client->IsAuthenticate(
            array(
                'loginName' => Config::get($params['currency'].'.mig.UsernameWB'), 
                'pinCode' => Config::get($params['currency'].'.mig.PasswordWB')
            )
        );

		App::insert_api_log( array( 'request' => '' , 'return' => json_encode($result) , 'method' => 'createUser' ) );
		
		if(isset($result) && ($result->IsAuthenticateResult->ErrorCode == 0)){
			$SessionGUID = $result->IsAuthenticateResult->SessionGUID;
			$IPAddress = $result->IsAuthenticateResult->IPAddress;
		}
		
		$headerxml =  '<AgentSession xmlns="https://entservices.totalegame.net">'.
					  '<SessionGUID>' . $SessionGUID . '</SessionGUID>'.
					  '<IPAddress>' . $IPAddress . '</IPAddress>'.
					  '</AgentSession>'
        ;
		$xmlvar = new SoapVar($headerxml, XSD_ANYXML);
		$header = new SoapHeader('https://entservices.totalegame.net', 'AgentSession', $xmlvar);
		$client->__setSoapHeaders($header);
		
		$result3 = $client->AddAccount(
          array(
                'accountNumber' => '', 
                'password' => Crypt::decrypt($params['password']),//Crypt::decrypt($params['password']) ,
                'firstName' => $params['username'],
                'lastName' => $params['username'],
                'currency' => Config::get($params['currency'].'.mig.CurrencyID'),
                'isSendGame' => true,
                'email' => $params['email'],
                'BettingProfileId' => Config::get($params['currency'].'.mig.BetProfileID'),
                'rngBettingProfileId' => Config::get($params['currency'].'.mig.BetProfileID'),
                'moblieGameLanguageId' => Config::get($params['currency'].'.mig.BetProfileID'),
                'isProgressive' => true,
            )			
        );
		
		App::insert_api_log( array( 'request' =>  '' , 'return' => json_encode($result3) , 'method' => 'createUser' ) );
		
		if(isset($result3) && ($result3->AddAccountResult->ErrorCode == 0)){
			Accountproduct::addAccountProduct($params['username'],'MIG','');
			Account::where('id','=',$params['userid'])->update(['migid' => $result3->AddAccountResult->AccountNumber]);
			return true;
		}
		return false;
	}
	
	public function getBalance( $username , $currency ){
		
		$url = "https://entv3-04.totalegame.net?WSDL";
		$client = new SoapClient($url, array('compression' => SOAP_COMPRESSION_ACCEPT | SOAP_COMPRESSION_GZIP));

		$result = $client->IsAuthenticate(
            array(
                'loginName' => Config::get($currency.'.mig.UsernameWB'), 
                'pinCode' => Config::get($currency.'.mig.PasswordWB')
            )
        );
		
		if(isset($result) && ($result->IsAuthenticateResult->ErrorCode == 0)){
			$SessionGUID = $result->IsAuthenticateResult->SessionGUID;
			$IPAddress = $result->IsAuthenticateResult->IPAddress;
		}
		
		App::insert_api_log( array( 'request' => $currency.'loginName:'.Config::get($currency.'.mig.UsernameWB').' | '.Config::get($currency.'.mig.PasswordWB')  , 'return' => json_encode($result) , 'method' => 'getBalance' ) );
		
		$headerxml =  '<AgentSession xmlns="https://entservices.totalegame.net">'.
					  '<SessionGUID>' . $SessionGUID . '</SessionGUID>'.
					  '<IPAddress>' . $IPAddress . '</IPAddress>'.
					  '</AgentSession>'
        ;
		$xmlvar = new SoapVar($headerxml, XSD_ANYXML);
		$header = new SoapHeader('https://entservices.totalegame.net', 'AgentSession', $xmlvar);
		$client->__setSoapHeaders($header);
		
		$result4 = $client->GetAccountBalance (
			array(
				'delimitedAccountNumbers' => Account::where( 'nickname' , '=' , $username)->pluck('migid')//Account::where( 'nickname' , '=' , $username)->pluck('migid')
			)
		);

		if(isset($result4) && ($result4->GetAccountBalanceResult->BalanceResult->ErrorCode == 0)){
			return $result4->GetAccountBalanceResult->BalanceResult->Balance;
		}
		return false;
		
	}
	
	private function getPrdid(){
		
		$prdid = Cache::rememberForever('MIG_PRDID', function() {
                return Product::where( 'code' , '=' , 'MIG')->pluck('id');
        });
		
		return $prdid;
	}
	
	public function deposit($username,$currency,$amount,$transid){
		
		if( $amount < 0 ) return false;
		
		return $this->transferCredit( $username, $currency, $transid, 'Deposit', $amount);
		
	}
		
	public function withdraw($username,$currency,$amount,$transid){

		if( $amount < 0 ) return false;
		
		return $this->transferCredit( $username, $currency, $transid, 'Withdrawal', $amount);
	}
	
	private function transferCredit( $username, $currency, $billno, $type, $credit){
		
		$url = "https://entv3-04.totalegame.net?WSDL";
		$client = new SoapClient($url, array('compression' => SOAP_COMPRESSION_ACCEPT | SOAP_COMPRESSION_GZIP));

		$result = $client->IsAuthenticate(
           array(	//Config::get($params['currency'].'.mig.UsernameWB')
                'loginName' => Config::get($currency.'.mig.UsernameWB'), 
                'pinCode' => Config::get($currency.'.mig.PasswordWB')
            )
        );
		
		if(isset($result) && ($result->IsAuthenticateResult->ErrorCode == 0)){
			$SessionGUID = $result->IsAuthenticateResult->SessionGUID;
			$IPAddress = $result->IsAuthenticateResult->IPAddress;
		}
		
		$headerxml =  '<AgentSession xmlns="https://entservices.totalegame.net">'.
					  '<SessionGUID>' . $SessionGUID . '</SessionGUID>'.
					  '<IPAddress>' . $IPAddress . '</IPAddress>'.
					  '</AgentSession>'
        ;
		$xmlvar = new SoapVar($headerxml, XSD_ANYXML);
		$header = new SoapHeader('https://entservices.totalegame.net', 'AgentSession', $xmlvar);
		$client->__setSoapHeaders($header);
		
		$result4 = $client->$type(
			array(
				'accountNumber' =>  Account::where( 'nickname' , '=' , $username)->pluck('migid'),
				'amount' => $credit,
				'currency' => Config::get($currency.'.mig.CurrencyID'),
				'transactionReferenceNumber ' => $billno
			)
		);
		
		if($type != 'Withdrawal' && $type == 'Deposit'){
			if(isset($result4) && ($result4->DepositResult->ErrorCode == 0)){
				return true;
			}
		}elseif($type != 'Deposit' && $type == 'Withdrawal'){
			if(isset($result4) && ($result4->WithdrawalResult->ErrorCode == 0)){
				return true;
			}
		}
		
		return false;
	}
    
}
?>