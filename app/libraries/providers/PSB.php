<?php namespace App\libraries\providers;
use App\libraries\App;
use App\Models\Betpsbresult;
use App\Models\Account;
use App\Models\AccountProduct;
use App\Models\Product;
use Config;
use Carbon\Carbon;
use Lang;
use Cache;
use DB;
use Session;

//getLoginUrl
//doLogOut
//getBalance
//deposit
//withdraw
//updateStatus
//get4DResult

class PSB {
 
	public function createUser( $params = array() ){
		
		if($result = $this->getLoginUrl($params['username'],false,$params['currency']))
		{
			$accObj = Account::find(Session::get('userid'));
			$accObj->psbsid = $result['sid'];
			$accObj->save();
			if( isset($result['sid']) )
			{
				Accountproduct::addAccountProduct($params['username'],'PSB','');
				return true;
			}
		
		}
		
		return false;
		
	}
	
	public function createAgent($currency,$username){
		//default agent = dcg
		if( Config::get('setting.opcode') == 'LVG'){
			$result = App::curlGET(Config::get($currency.'.psb.url').'?m=13&a='.Config::get($currency.'.psb.agentA').'&p='.Config::get($currency.'.psb.agentP').'&u='.Config::get($currency.'.psb.agentPrefix').$username.'&c='.Config::get($currency.'.psb.master'), true, 3 , 'createAgent');
				if( isset($result) && substr($result,0,2) == 'OK' )
				{
					return true;
				}
		}
		return false;
		
	}
	
	
	
	public function createAgentDownline($currency,$username,$agent,$agentdownline){
		//set agent = agent upline / put to AgentController 
		if( Config::get('setting.opcode') == 'LVG'){
			$result = App::curlGET(Config::get($currency.'.psb.url').'?m=13&a='.Config::get($currency.'.psb.agentA').'_'.$agent.'&p='.Config::get($currency.'.psb.agentP').'&u='.Config::get($currency.'.psb.master').$agentdownline.'_'.$username.'&c='.Config::get($currency.'.psb.master'), true, 3 , 'createAgentDownline');
				if( isset($result) && substr($result,0,2) == 'OK' )
				{
					return true;
				}
		}
		return false;
		
	}
	
	public function setAgentFighting($currency,$agent,$game,$type,$amount){
		$login_agent = $this->getLoginUrlAgent(true,$currency);
		
		$postfields = 'u='.$agent.'&s='.$login_agent['sid'].'&'.$game.$type.'='.$amount;
		
			$result = $this->curl(Config::get($currency.'.psb.url').'?m=14',$postfields,true, 3 , 'createAgentDownline');
			
				if( isset($result) && substr($result,0,2) == 'OK' )
				{
					return true;
				}
		
		return false;
		
	}
	
	public function addMember($username, $isdeposit = false, $currency){
		
		if( !$isdeposit ){
			if($this->isMobileDevice()){
				$result = App::curlGET(Config::get($currency.'.psb.url').'?mob=1&m=16&a='.Config::get($currency.'.psb.agentA').'&u='.Config::get($currency.'.psb.agentPrefix').$username.'&p='.Config::get($currency.'.psb.agentP').'&c='.Config::get($currency.'.psb.master'), true, 3 , 'addMember');
			}else{
				$result = App::curlGET(Config::get($currency.'.psb.url').'?m=16&a='.Config::get($currency.'.psb.agentA').'&u='.Config::get($currency.'.psb.agentPrefix').$username.'&p='.Config::get($currency.'.psb.agentP').'&c='.Config::get($currency.'.psb.master'), true, 3 , 'addMember');
			}
			
		}else{
			$result = App::curlGET(Config::get($currency.'.psb.url').'?m=16&a='.Config::get($currency.'.psb.agentA').'&u='.Config::get($currency.'.psb.agentPrefix').$username.'&p='.Config::get($currency.'.psb.agentP').'&c='.Config::get($currency.'.psb.master'), true, 3 , 'addMember');
		}
		
		$result = explode( '%' , $result);

		if( isset($result[0]) && $result[0] == 'OK' )
		{
			return array( 'url' => $result[2] , 'sid' => $result[1] );
		}
		
		return false;
		
	}
	
	public function getLoginUrl($username, $isdeposit = false, $currency){
		
		if( !$isdeposit ){
			if ($this->isMobileDevice()) 
			{
				$result = App::curlGET(Config::get($currency.'.psb.url').'?mob=1&m=0&a='.Config::get($currency.'.psb.a').'&p='.Config::get($currency.'.psb.p').'&u='.Config::get($currency.'.psb.prefix').$username, true, 3 , 'createUser');
			}
			else
			{
				$result = App::curlGET(Config::get($currency.'.psb.url').'?m=0&a='.Config::get($currency.'.psb.a').'&p='.Config::get($currency.'.psb.p').'&u='.Config::get($currency.'.psb.prefix').$username, true, 3 , 'createUser');
			}
		}else{
		    if (Config::get('setting.opcode') == 'LVG') {
                $result = App::curlGET(Config::get($currency.'.psb.url').'?m=0&a='.Config::get($currency.'.psb.a').'&p='.Config::get($currency.'.psb.p').'&u='.Config::get($currency.'.psb.prefix').$username, true, 3 , 'createUser');
            } else {
                $result = App::curlGET(Config::get($currency.'.psb.url').'?m=0&a='.Config::get($currency.'.psb.a').'&p='.Config::get($currency.'.psb.p').'&u='.$username, true, 3 , 'createUser');
            }
		}
		$result = explode( '%' , $result);
		
		if( isset($result[0]) && $result[0] == 'OK' )
		{
			return array( 'url' => $result[2] , 'sid' => $result[1] );
		}
		
		return false;
		
	}
	
	public function getLoginUrlAgent($isdeposit = false, $currency){
		
		if( !$isdeposit ){
			if ($this->isMobileDevice()) 
			{
				$result = App::curlGET(Config::get($currency.'.psb.url').'?mob=1&m=0&a='.Config::get($currency.'.psb.agentA').'&p='.Config::get($currency.'.psb.agentP').'&u='.Config::get($currency.'.psb.master'), true, 3 , 'agentLogin');
			}
			else
			{
				$result = App::curlGET(Config::get($currency.'.psb.url').'?m=0&a='.Config::get($currency.'.psb.agentA').'&p='.Config::get($currency.'.psb.agentP').'&u='.Config::get($currency.'.psb.master'), true, 3 , 'agentLogin');
			}
		}else{
		    if (Config::get('setting.opcode') == 'LVG') {
                $result = App::curlGET(Config::get($currency.'.psb.url').'?m=0&a='.Config::get($currency.'.psb.master').'&p='.Config::get($currency.'.psb.agentP').'&u='.Config::get($currency.'.psb.master'), true, 3 , 'agentLogin');
            } 
		}
		
		$result = explode( '%' , $result);
		
		if( isset($result[0]) && $result[0] == 'OK' )
		{
			return array( 'url' => $result[2] , 'sid' => $result[1] );
		}
		
		return false;
		
	}
	
	public function getLoginUrl5D($username, $isdeposit = false, $currency){
		
		if( !$isdeposit ){
			if ($this->isMobileDevice()) 
			{
				$result = App::curlGET(Config::get($currency.'.psb.url').'?mob=1&m=0&a='.Config::get($currency.'.psb.a').'&p='.Config::get($currency.'.psb.p').'&u='.Config::get($currency.'.psb.prefix').$username.'&d=3', true, 3 , 'createUser');
			}
			else
			{
				$result = App::curlGET(Config::get($currency.'.psb.url').'?m=0&a='.Config::get($currency.'.psb.a').'&p='.Config::get($currency.'.psb.p').'&u='.Config::get($currency.'.psb.prefix').$username.'&d=1', true, 3 , 'createUser');
			}
		}else{
			$result = App::curlGET(Config::get($currency.'.psb.url').'?m=0&a='.Config::get($currency.'.psb.a').'&p='.Config::get($currency.'.psb.p').'&u='.Config::get($currency.'.psb.prefix').$username.'&d=1', true, 3 , 'createUser');
		}
		
		$result = explode( '%' , $result);

		if( isset($result[0]) && $result[0] == 'OK' )
		{
			return array( 'url' => $result[2] , 'sid' => $result[1] );
		}
		
		return false;
		
	}
	
	public function getLoginUrl4Dspc($username, $isdeposit = false, $currency){
		
		if ($this->isMobileDevice()){
		$result = App::curlGET(Config::get($currency.'.psb.url').'?mob=1&m=0&a='.Config::get($currency.'.psb.a').'&p='.Config::get($currency.'.psb.p').'&u='.Config::get($currency.'.psb.prefix').$username.'&d=4', true, 3 , 'createUser');
		}else{
			echo "Please use mobile to login.";
			return ;
		}
		
		$result = explode( '%' , $result);

		if( isset($result[0]) && $result[0] == 'OK' )
		{
			return array( 'url' => $result[2] , 'sid' => $result[1] );
		}
		
		return false;
		
	}
	
	public function doLogOut($sid,$currency){
		
		$result = App::curlGET(Config::get($currency.'.psb.url').'?m=1&a='.Config::get($currency.'.psb.a').'&p='.Config::get($currency.'.psb.p').'&s='.$sid,false);
		
		if( isset($result) && $result == 'OKSuccess' )
		{
			return true;
		}
		
		return false;
		
	}
	
	public function getBalance($username,$currency){

		$result = App::curlGET(Config::get($currency.'.psb.url').'?m=5&a='.Config::get($currency.'.psb.a').'&p='.Config::get($currency.'.psb.p').'&u='.Config::get($currency.'.psb.prefix').$username, true, 3 , 'getBalance');
		
		if( isset($result) && substr($result,0,2) == 'OK' )
		{
			$temp = explode( 'OK' , $result );
			$dateTimeNow    = Carbon::now();
			$dateTimeNowStr = $dateTimeNow->toDateTimeString();
				
			if( Session::has('userid') ){
				
				DB::statement('call insert_cashbalance('.Session::get('userid').',"'.Session::get('username').'","'.$currency.'",'.$this->getPrdid().','.str_replace(",","",$temp[1]).',"'.$dateTimeNowStr.'","'.$dateTimeNowStr.'")');
			
			}else{
				DB::statement('call insert_cashbalance('.Account::whereNickname($username)->whereCrccode($currency)->pluck('id').',"'.$username.'","'.$currency.'",'.$this->getPrdid().','.str_replace(",","",$temp[1]).',"'.$dateTimeNowStr.'","'.$dateTimeNowStr.'")');
			}
			
			return str_replace(",","",$temp[1]);
		}
		
		return false;
		
	}
	
	private function getPrdid(){
		
		$prdid = Cache::rememberForever('PSB_PRDID', function() {
                return Product::where( 'code' , '=' , 'PSB')->pluck('id');
        });
		
		return $prdid;
	}
	
	public function deposit($username, $currency, $amount , $transID){
		
		if( $amount < 0 ) return false;
		
		return $this->updateCredit($username,$transID,$amount,$currency);
		
	}
		
	public function withdraw($username, $currency, $amount , $transID){
		
		if( $amount < 0 ) return false;
		
		$current_balance = $this->getBalance( $username ,$currency);
		
		if( $current_balance == false || $amount > $current_balance )  return false;
		
		return $this->updateCredit($username,$transID,$amount*-1,$currency);
		
	}

	private function updateCredit($username,$transID,$amount,$currency){
		
		$login_result = $this->getLoginUrl(Config::get($currency.'.psb.a'),true,$currency);
		
		$result = App::curlGET(Config::get($currency.'.psb.url').'?m=2&a='.Config::get($currency.'.psb.a').'&p='.Config::get($currency.'.psb.p').'&u='.Config::get($currency.'.psb.prefix').$username.'&s='.$login_result['sid'].'&d='.$amount, true, 40 , 'transferCredit');


		if( isset($result) && substr($result,0,2) == 'OK' )
		{
			$temp = explode( 'OK' , $result );
			
			return $temp[1];
		}
		
		return false;
		
	}
	
	public function updateStatus($username,$status,$currency){
		
		$array['active'] 	= 'a';
		$array['close'] 	= 'c';
		$array['suspend']   = 's';
		
		$result = App::curlGET(Config::get($currency.'.psb.url').'?m=7&a='.Config::get($currency.'.psb.a').'&p='.Config::get($currency.'.psb.p').'&u='.Config::get($currency.'.psb.prefix').$username.'&st='.$array[$status],false);
		
		if( isset($result) && substr($result,0,2) == 'OK' )
		{
			return true;
		}
		
		return false;
		
	}
	
	public function get4DResult(){
		

		if (Cache::has('4d_result'))
		{
			return Cache::get('4d_result');
		}
		else
		{
			$value = Cache::rememberForever('4d_result', function()
			{
				return Betpsbresult::where( 'DrawDate' , '=' , Betpsbresult::orderBy('DrawDate','Desc')->pluck('DrawDate') )->get();
			});
		}
		
		return $value;
	}
	
	public function isMobileDevice() {

        $useragent = $_SERVER['HTTP_USER_AGENT'];

        if (preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4))) {
            // Mobile request.
            return true;
        } elseif (stristr($_SERVER['HTTP_USER_AGENT'], 'Mozilla/5.0(iPad;')) {
            // Mobile request.
            return true;
        } elseif (strstr($_SERVER['HTTP_USER_AGENT'], 'iPad')) {
            // Mobile request.
            return true;
        }

        return false;
    }
   
    function curl($url  , $postFields  , $log = true, $timeout = 30 , $method = ''){
		$headers[] = "Content-Type:application/x-www-form-urlencoded";
		$data = $postFields;
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($ch, CURLOPT_POSTFIELDS,$data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response  = curl_exec($ch);
		curl_close($ch);
        
        /* if($log)
            App::insert_api_log( array( 'request' => $url.'?'.$data_json , 'return' => $response , 'method' => $method ) ); */

        return $response;
    }
   
}
?>