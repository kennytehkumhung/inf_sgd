<?php namespace App\libraries\providers;

use App\libraries\App;
use App\Models\AccountProduct;
use App\Models\Betpltcategory;
use App\Models\Product;
use App\Models\Account;
use Config;
use Crypt;
use Lang;
use Carbon\Carbon;
use Cache;
use DB;
use Session;

//getBalance
//deposit
//withdraw
//createUser

class PLT {
 
	public function createUser( $params = array() ){
		
		$password = Crypt::decrypt($params['password']);

		$parameterStr  = "player/create";
        $parameterStr .= "/playername/" . Config::get($params['currency'].'.plt.prefix').strtoupper($params['username']);
        $parameterStr .= "/password/"   . $password;
        $parameterStr .= "/adminname/"  . Config::get($params['currency'].'.plt.adminname');
        $parameterStr .= "/kioskname/"  . Config::get($params['currency'].'.plt.adminname');
		
		$result = json_decode($this->CallPltFunction( $parameterStr , $params['currency'] , 'createUser' , 5 ));

		if( (isset($result->error) && $result->error == 'The username you requested is already being used by another player.') || (isset($result->result->result)  && $result->result->result == 'New player has been created' ) )
		{
			Accountproduct::addAccountProduct($params['username'],'plt','');
			return true;
		}
		
		return false;
		
	}

	
	public function getBalance( $username , $currency ){
		
		$parameterStr  = "player/info";
        $parameterStr .= "/playername/" . Config::get($currency.'.plt.prefix').strtoupper($username);
		
		$result = json_decode($this->CallPltFunction( $parameterStr , $currency , 'getBalance' ));

        if( isset($result->result->BALANCE) )
	    {
			$dateTimeNow    = Carbon::now();
            $dateTimeNowStr = $dateTimeNow->toDateTimeString();
			
			if( $currency == 'IDR' || $currency == 'VND')
			{
				return $result->result->BALANCE / 1000;
			}
			else
			{
				if( Session::has('userid') ){
				
					DB::statement('call insert_cashbalance('.Session::get('userid').',"'.$username.'","'.$currency.'",'.$this->getPrdid().','.$result->result->BALANCE .',"'.$dateTimeNowStr.'","'.$dateTimeNowStr.'")');
				
				}else{
					
					DB::statement('call insert_cashbalance('.Account::whereNickname($username)->whereCrccode($currency)->pluck('id').',"'.$username.'","'.$currency.'",'.$this->getPrdid().','.$result->result->BALANCE.',"'.$dateTimeNowStr.'","'.$dateTimeNowStr.'")');
				}
				return $result->result->BALANCE;
			}
		}
		
		return false;
		
	}	
	
	private function getPrdid(){
		
		$prdid = Cache::rememberForever('PLT_PRDID', function() {
                return Product::where( 'code' , '=' , 'PLT')->pluck('id');
        });
		
		return $prdid;
	}

	
	public function updatePassword( $username , $currency , $password){
		
		
		$parameterStr  = "player/update";
        $parameterStr .= "/playername/" . Config::get($currency.'.plt.prefix').strtoupper($username);
        $parameterStr .= "/frozen/0";
        $parameterStr .= "/suspended/0";
        $parameterStr .= "/password/".$password;
	
		$result = json_decode($this->CallPltFunction( $parameterStr , $currency , 'updatePassword', 5));
	
	}

	public function deposit($username,$currency,$amount,$transid){
		
		$parameterStr  = "player/deposit";
        $parameterStr .= "/playername/" . Config::get($currency.'.plt.prefix').strtoupper($username);
        $parameterStr .= "/amount/" . $amount;
        $parameterStr .= "/adminname/" . Config::get($currency.'.plt.adminname');
        $parameterStr .= "/externaltranid/" . Config::get($currency.'.plt.transprefix').$transid;
		
		$result = json_decode($this->CallPltFunction( $parameterStr , $currency , 'deposit', 20));

        if( isset($result->result) && $result->result->result == 'Deposit OK' )
	    {
			return true;
		}
		
		return false;
		
	}
		
	public function withdraw($username,$currency,$amount,$transid){
		
		$parameterStr  = "player/withdraw";
        $parameterStr .= "/playername/" . Config::get($currency.'.plt.prefix').strtoupper($username);
        $parameterStr .= "/amount/" . $amount;
        $parameterStr .= "/adminname/" .  Config::get($currency.'.plt.adminname');
        $parameterStr .= "/externaltranid/" . Config::get($currency.'.plt.transprefix').$transid;
        $parameterStr .= "/isForce/1";
	
		$result = json_decode($this->CallPltFunction( $parameterStr , $currency , 'withdraw', 20));

        if( isset($result->result) && $result->result->result == 'Withdraw OK' )
	    {
			return true;
		}
		
		return false;
	}
	
	public function gameList($currency, $type = 'all', $category) {

        $categoryBuilder = Betpltcategory::where('isActive', '=', 1);

        if ($this->isMobileDevice()) {
            $categoryBuilder->where('isHtml5', '=', 1)->where('html5Code', '!=', '');
        } else {
            $categoryBuilder->where('code', '!=', '');
        }

        if($type == 'slot') {
            $data['name']  = 'pltb';
            $categoryBuilder->where('category', '=', $category);
        } elseif ($type == 'newgames') {
            $data['name']  = 'pltb';
            $categoryBuilder->where('isNew', '=', $category);
        } elseif ($type == 'pgames') {
            $data['name']  = 'pltb';
            $categoryBuilder->where('jpCode', '!=', '');
        } elseif ($type == 'brand' ) {
            $data['name']  = 'pltb';
            $categoryBuilder->where('isBranded', '=', $category);
        } else {
            $data['name']  = 'pltb';
            $categoryBuilder->where('jpCode', '!=', '');
        }

        return $categoryBuilder->orderBy('isNew', 'desc')->get();
    }

	

	
	public static function CallFunction( $params , $currency, $method , $timeout = 10)
	{
	
		$header   = array();
		$header[] = "Accept:text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8"; 
		$header[] = "Cache-Control: max-age=0"; 
		$header[] = "Connection: keep-alive"; 
		$header[] = "Keep-Alive:timeout=5, max=100"; 
		$header[] = "Accept-Charset:ISO-8859-1,utf-8;q=0.7,*;q=0.3"; 
		$header[] = "Accept-Language:es-ES,es;q=0.8"; 
		$header[] = "operatorid: ".Config::get($currency.'.plt.operatorid'); 
		$header[] = "privatekey: ".Config::get($currency.'.plt.privatekey'); 
		$header[] = "currency: ".$currency; 

	    $tuCurl = curl_init(); 
	    curl_setopt($tuCurl, CURLOPT_URL, Config::get($currency.'.plt.proxy_url') ); 
		curl_setopt($tuCurl, CURLOPT_HTTPHEADER, $header);
		curl_setopt($tuCurl, CURLOPT_POST, true);
		curl_setopt($tuCurl, CURLOPT_TIMEOUT, $timeout);
		curl_setopt($tuCurl, CURLOPT_POSTFIELDS, 'currency='.$currency.'&entitykey='.Config::get($currency.'.plt.entitykey').'&param='.str_replace(' ', '%20', $params) );
	    curl_setopt($tuCurl, CURLOPT_HEADER, 0);      
		curl_setopt($tuCurl, CURLOPT_RETURNTRANSFER, 1); 
	    
		$result = '';
		
	    if(!curl_errno($tuCurl))
		{ 
			$result    = curl_exec($tuCurl);
	    } 


		curl_close($tuCurl);
		
		App::insert_api_log( array( 'request' => 'param='.Config::get($currency.'.plt.url').str_replace(' ', '%20', $params) , 'return' => $result , 'method' => $method ) );
		
		return $result; 
	}

	 public function isMobileDevice() {

        $useragent = $_SERVER['HTTP_USER_AGENT'];

        if (preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4))) {
            // Mobile request.
            return true;
        } elseif (stristr($_SERVER['HTTP_USER_AGENT'], 'Mozilla/5.0(iPad;')) {
            // Mobile request.
            return true;
        } elseif (strstr($_SERVER['HTTP_USER_AGENT'], 'iPad')) {
            // Mobile request.
            return true;
        }

        return false;
    }
	
	protected function CallPltFunction( $params , $currency, $method , $timeout = 10)
	{

		$header   = array();
		$header[] = "Accept:text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8"; 
		$header[] = "Cache-Control: max-age=0"; 
		$header[] = "Connection: keep-alive"; 
		$header[] = "Keep-Alive:timeout=15, max=100"; 
		$header[] = "Accept-Charset:ISO-8859-1,utf-8;q=0.7,*;q=0.3"; 
		$header[] = "Accept-Language:es-ES,es;q=0.8"; 
		$header[] = "Pragma: "; 
		$header[] = "X_ENTITY_KEY: " . Config::get($currency.'.plt.entitykey'); 
		
		$default_url = 'https://kioskpublicapi.redhorse88.com/';
		
	 	if($currency == 'CNY') {
//            $default_url = 'https://kioskpublicapi.grandmandarin88.com/';
            $default_url = 'https://kioskpublicapi.luckyspin88.com/';
        }
 
	    $tuCurl = curl_init(); 
	    curl_setopt($tuCurl, CURLOPT_URL, $default_url.str_replace(' ', '%20', $params)); 
	    curl_setopt($tuCurl, CURLOPT_PORT , 443); 
	    curl_setopt($tuCurl, CURLOPT_VERBOSE, 0); 
		curl_setopt($tuCurl, CURLOPT_HTTPHEADER, $header);
	    curl_setopt($tuCurl, CURLOPT_HEADER, 0); 
	    curl_setopt($tuCurl, CURLOPT_SSL_VERIFYPEER, 0);
	    curl_setopt($tuCurl, CURLOPT_SSL_VERIFYHOST, 0);     
		curl_setopt($tuCurl, CURLOPT_TIMEOUT, $timeout);
	    curl_setopt($tuCurl, CURLOPT_SSLCERT, str_replace('public', 'config', Config::get('setting.server_path')).'cert/'.$currency.'.pem' );
		curl_setopt($tuCurl, CURLOPT_SSLKEY,  str_replace('public', 'config', Config::get('setting.server_path')).'cert/'.$currency.'.key' );	    
		curl_setopt($tuCurl, CURLOPT_RETURNTRANSFER, 1); 
		$result = curl_exec($tuCurl);
		
		App::insert_api_log( array( 'request' => 'param='.$default_url.str_replace(' ', '%20', $params) , 'return' => $result , 'method' => $method ) );
	
		return $result;

	}
   
}
?>