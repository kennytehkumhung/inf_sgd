<?php namespace App\libraries\providers;

use App\libraries\App;
use App\Models\AccountProduct;
use App\Models\Account;
use App\Models\Product;
use Carbon\Carbon;
use Crypt;
use Config;
use Lang;
use Cache;
use DB;
use Session;

//createuser, changepassword, changestatus, getbalance, deposit, withdraw

class JOK{
    
    public function createUser( $params ){

        $appId = Config::get($params['currency'].'.jok.appid');

        if (strlen($appId) > 0) {
            $data = array(
                'Method' => 'CU',
                'Timestamp' => Carbon::now()->timestamp,
                'Username' => Config::get($params['currency'] . '.jok.prefix') . '_' . $params['username'],
            );

            $signature = $this->generateSignature($data, Config::get($params['currency'] . '.jok.key'));
            $postfields = json_encode($data);

            $result = json_decode(App::curlXML($postfields, Config::get($params['currency'] . '.jok.api_url') . '?AppID=' . $appId . '&Signature=' . $signature, true, 3, 'createUser', $this->getXmlHeader()));

            if (isset($result->Status) && ($result->Status == 'OK' || $result->Status == 'Created')) {
                $this->updatePassword($params);
                Accountproduct::addAccountProduct($params['username'], 'JOK', '');
                return true;
            }
        }
        
	    return false;
    }   
	
	public function updatePassword( $params = array() ){

        $appId = Config::get($params['currency'].'.jok.appid');

        if (strlen($appId) > 0) {

            $data = array(
                'Method' => 'SP',
                'Password' => Crypt::decrypt($params['password']),
                'Timestamp' => Carbon::now()->timestamp,
                'Username' => Config::get($params['currency'] . '.jok.prefix') . '_' . $params['username'],
            );

            $signature = $this->generateSignature($data, Config::get($params['currency'] . '.jok.key'));
            $postfields = json_encode($data);

            json_decode(App::curlXML($postfields, Config::get($params['currency'] . '.jok.api_url') . '?AppID=' . $appId . '&Signature=' . $signature, true, 1, 'updatePassword', $this->getXmlHeader()));

            return true;
        }

        return false;
	}	
	
	public function getLoginToken($params){

        $appId = Config::get($params['currency'].'.jok.appid');

        if (strlen($appId) > 0) {
            $data = array(
                'Method' => 'RT',
                'Timestamp' => Carbon::now()->timestamp,
                'Username' => Config::get($params['currency'] . '.jok.prefix') . '_' . $params['username'],
            );

            $signature = $this->generateSignature($data, Config::get($params['currency'] . '.jok.key'));
            $postfields = json_encode($data);

            $result = json_decode(App::curlXML($postfields, Config::get($params['currency'] . '.jok.api_url') . '?AppID=' . $appId . '&Signature=' . $signature, true, 5, 'getLoginToken', $this->getXmlHeader()));

            if (isset($result->Username) && isset($result->Token) && $this->isUsernameCorrect($result->Username, $data['Username'])) {
                return $result->Token;
            }
        }
        
	    return false;
    }
    
    public function getBalance($username ,$currency){

        $appId = Config::get($currency.'.jok.appid');

        if (strlen($appId) > 0) {
            $data = array(
                'Method' => 'GC',
                'Timestamp' => Carbon::now()->timestamp,
                'Username' => Config::get($currency.'.jok.prefix').'_'.$username,
            );

            $signature = $this->generateSignature($data, Config::get($currency.'.jok.key'));
            $postfields = json_encode($data);

            $result = json_decode(App::curlXML($postfields, Config::get($currency.'.jok.api_url').'?AppID='.$appId.'&Signature='.$signature, true, 5 , 'getBalance', $this->getXmlHeader()));

            if( isset($result->Username) && isset($result->Credit) && $this->isUsernameCorrect($result->Username, $data['Username']) )
            {
				
				$dateTimeNow    = Carbon::now();
				$dateTimeNowStr = $dateTimeNow->toDateTimeString();
					
				if( Session::has('userid') ){

					DB::statement('call insert_cashbalance('.Session::get('userid').',"'.Session::get('username').'","'.$currency.'",'.$this->getPrdid().','.$result->Credit.',"'.$dateTimeNowStr.'","'.$dateTimeNowStr.'")');
				}else{
					DB::statement('call insert_cashbalance('.Account::whereNickname($username)->whereCrccode($currency)->pluck('id').',"'.$username.'","'.$currency.'",'.$this->getPrdid().','.$result->Credit.',"'.$dateTimeNowStr.'","'.$dateTimeNowStr.'")');
				}
			
                return $result->Credit;
            }
        }
        
	    return false;
    }
	
	private function getPrdid(){
		
		$prdid = Cache::rememberForever('JOK_PRDID', function() {
                return Product::where( 'code' , '=' , 'JOK')->pluck('id');
        });
		
		return $prdid;
	}
    
    
    public function deposit($username, $currency, $amount , $transID){

		if( $amount < 0 ) return false;
		
		return $this->transferCredit($username, $currency, $transID, 0, $amount);   
    }
    
    public function withdraw($username, $currency , $amount , $transID){

		if( $amount < 0 ) return false;
		
		return $this->transferCredit($username, $currency, $transID, 1, $amount);
    }
	
	private function transferCredit( $username, $currency, $billno, $type, $credit){

        $appId = Config::get($currency.'.jok.appid');

        if (strlen($appId) > 0) {
            if ($currency == 'IDR' || $currency == 'VND') {
                $credit = $credit / 1000;
            }

            if ($type == 1) {
                $credit = $credit * -1;
            }

            $data = array(
                'Amount' => $credit,
                'Method' => 'TC',
                'Timestamp' => Carbon::now()->timestamp,
                'Username' => Config::get($currency . '.jok.prefix') . '_' . $username,
            );

            $signature = $this->generateSignature($data, Config::get($currency . '.jok.key'));
            $postfields = json_encode($data);

            $result = json_decode(App::curlXML($postfields, Config::get($currency . '.jok.api_url') . '?AppID=' . $appId . '&Signature=' . $signature, true, 10, 'transferCredit', $this->getXmlHeader()));

            if (isset($result->Username) && isset($result->Credit) && $this->isUsernameCorrect($result->Username, $data['Username'])) {
                return true;
            }
        }
        
		return false;
	}
	
	public function generateSignature($params, $privateKey = false)
	{
		if($privateKey == '')
		{
			return '';
		}
		
		ksort($params);
        $values = array();
        
        foreach ($params as $key => $val) {
            $values[] = $key.'='.$val;
        }
        
		return urlencode(base64_encode(hash_hmac('sha1', implode('&', $values), $privateKey, true)));
	}
    
    public function getXmlHeader()
    {
        return array(
		    'Cache-Control: max-age=0',
		    'Connection: keep-alive',
            'Content-Type: application/json; charset=utf-8',
        );
    }
    
    private function isUsernameCorrect($str1, $str2) {
        return (strtoupper($str1) == strtoupper($str2));
    }

}
