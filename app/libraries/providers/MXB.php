<?php namespace App\libraries\providers;

use App\libraries\App;
use App\Models\AccountProduct;
use App\Models\Account;
use App\Models\Product;
use Crypt;
use Carbon\Carbon;
use Cache;
use DB;
use Session;
use Config;

//createuser, changepassword, changestatus, getbalance, deposit, withdraw

class MXB{
    
    public function createUser( $params = array() ){
	
	    $password = Crypt::decrypt($params['password']);
	    
	    $postfields =   'operatorUsername='.Config::get($params['currency'].'.mxb.username').
			    '&username='. $params['username'] .
			    '&userPassword=' . $password .
			    '&email=' . $params['email'] .
			    '&firstName=' . $params['username'] .
			    '&lastName=' .$params['username'] .
			    '&accountID=' . $params['userid'];
		
	    $accessPassword = strtoupper(md5(Config::get($params['currency'].'.mxb.password').str_replace(' ' , '%20', $postfields)));
	    
		$result = simplexml_load_string(App::curlGET(Config::get($params['currency'].'.mxb.url').'CreateAccount?accessKey='.$accessPassword.'&'.$postfields, true, 3 , 'createUser'));

	    if( isset($result->errorCode) && ( $result->errorCode == '0' || $result->errorCode == '3' ) )
	    {
			Accountproduct::addAccountProduct($params['username'],'MXB','');
			return true;
	    }

	    return false;
    }

    public function logoutUser( $params = array() ) {

        $postfields =   'operatorUsername='.Config::get($params['currency'].'.mxb.username').
            '&username='. $params['username'];

        $accessPassword = strtoupper(md5(Config::get($params['currency'].'.mxb.password').$postfields));

        $result = simplexml_load_string(App::curlGET(Config::get($params['currency'].'.mxb.url').'KickPlayerFromGames?accessKey='.$accessPassword.'&'.$postfields, true, 1 , 'logoutUser'));

        if(isset($result->errorCode) && $result->errorCode == '0')
        {
            return true;
        }

        return false;
    }
	
	public function getSlotLobby($username, $language, $currency, $password){
		
		$password = Crypt::decrypt($password);
	    
	    $postfields =   'operatorUsername='.Config::get($currency.'.mxb.username').
						'&username='. $username.
						'&languageID='. $this->getLanguageId($language);
		
	    $accessPassword = strtoupper(md5(Config::get($currency.'.mxb.password').str_replace(' ' , '%20', $postfields)));
	    
		$cmdParameter = "accessKey=" . $accessPassword . "&" . $postfields;

		return "http://external.maxbet.mx/Game/EGamesLobby.aspx?" . $cmdParameter;
	}
	
	public function getLanguageId($lang = 'en'){

	    $languages = collect(array(
	        'en' => 2057,
	        'cn' => 2052,
	        'tw' => 2052,
	        'th' => 1054,
	        'id' => 1057,
        ));

		return $languages->get($lang, 'en');
	}
	
	public function getCasinoGameList($username, $gametypename, $currency, $language = 'en'){
	
		$list = array();
	
		$gametype['roulette']			 = 1;
        $gametype['blackjack']			 = 2;
        $gametype['baccarat'] 			 = 4;
        $gametype['single_player_poker'] = 8;
        $gametype['multi_player_poker']  = 10;
        $gametype['dragon_tiger'] 		 = 12;
        $gametype['sicbo']				 = 16;
        $gametype['carribean_poker'] 	 = 18;
        $gametype['wheel_of_fortune'] 	 = 22;
        $gametype['external_games'] 	 = 999;
		
	    $postfields   = 'operatorUsername='.Config::get($currency.'.mxb.username').
						'&username='.$username.
						'&gameType='.$gametype[$gametypename];

	    $accessPassword = strtoupper(md5(Config::get($currency.'.mxb.password').$postfields));
	    
	    $result = App::curlGET(Config::get($currency.'.mxb.url').'GetGamesTablesList?accessKey='.$accessPassword.'&'.$postfields, false, 8);
		$result = preg_replace('/xmlns[^=]*="[^"]*"/i', '', $result);
		$result = preg_replace('/[a-zA-Z]+:([a-zA-Z]+[=>])/', '$1', $result);
		$result = simplexml_load_string($result);

	    if(isset($result->errorCode) && $result->errorCode == '0')
	    {
			$xml_array = json_decode(json_encode($result),TRUE);
			if(isset($xml_array['gamesList']['game']))
			{	

		
				if( isset($xml_array['gamesList']['game']['gameName']) ){
					$count = 0;
					$list[$count]['gameName']    = $xml_array['gamesList']['game']['gameName'];
					$list[$count]['dealerName']  = $xml_array['gamesList']['game']['dealerName'];
					$list[$count]['image']       = $xml_array['gamesList']['game']['dealerImageUrl'];

					if ( isset($xml_array['gamesList']['game']['limitSetList']['limitSet']['limitSetID']) ) {
                        $list[$count]['limit'][]       = $xml_array['gamesList']['game']['limitSetList']['limitSet'];
                    } else {
                        $list[$count]['limit']       = $xml_array['gamesList']['game']['limitSetList']['limitSet'];
                    }
					
					$temp_url  = str_replace ( '{1}' , $this->getLanguageId($language) , $xml_array['gamesList']['game']['connectionUrl']);
					$temp_url  = str_replace ( '{2}' , '2' , $temp_url);
					$temp_url  = str_replace ( '{3}' , $username , $temp_url);
					$list[$count]['url']         = $temp_url;
						
				}else{
					foreach( $xml_array['gamesList']['game'] as $key => $game ){
			
						
						$list[$key]['gameName']    = $game['gameName'];
						$list[$key]['dealerName']  = $game['dealerName'];
						$list[$key]['image']       = $game['dealerImageUrl'];
						if( isset($game['limitSetList']['limitSet']['limitSetID']) )
							$list[$key]['limit'][]      = $game['limitSetList']['limitSet'];
						else
							$list[$key]['limit']      = $game['limitSetList']['limitSet'];
						
						$temp_url  = str_replace ( '{1}' , $this->getLanguageId($language) , $game['connectionUrl']);
						$temp_url  = str_replace ( '{2}' , '2' , $temp_url);
						$temp_url  = str_replace ( '{3}' , $username , $temp_url);
						$list[$key]['url']         = $temp_url;

					}
				}
			}

	    }

	   return $list;
    }	
	
	public function getSlotGameList($username,$type, $currency){
			
	    $postfields =   'operatorUsername='.Config::get($currency.'.mxb.username').
						'&username='. $username .
						'&providerType=BetSoft';

	    $accessPassword = strtoupper(md5(Config::get($currency.'.mxb.password').$postfields));
	    
	    $result = App::curlGET(Config::get($currency.'.mxb.url').'GetExternalGamesList?accessKey='.$accessPassword.'&'.$postfields,false,8);
		$result = preg_replace('/xmlns[^=]*="[^"]*"/i', '', $result);
		$result = preg_replace('/[a-zA-Z]+:([a-zA-Z]+[=>])/', '$1', $result);
		$result = simplexml_load_string($result);
		
		$exGame = array();	
	     if(isset($result->errorCode) && $result->errorCode == '0')
	    {
			$xml_array = json_decode(json_encode($result),TRUE);
				
			foreach( $xml_array['BetSoftGamesList']['game'] as $key => $game ){

				if( isset($game['typeID']) && $type == $game['typeID'] ){
					$exGame[$key]['gameID']      = $game['gameID'];
					$exGame[$key]['typeID']      = $game['typeID'];
					$exGame[$key]['gameName']    = $game['gameName'];				
					$exGame[$key]['imageUrl']    = !is_array($game['imageUrl']) ? $game['imageUrl'] : '';
					$exGame[$key]['languages']   = $game['languages'];
					//$exGame[$key]['url']  		 = $this->GetExternalGameURL($username,$game['gameID']);
				}
			}

	    }

	     return $exGame;
    }	
	
	public function GetExternalGameURL($username,$gameid, $currency){
	
		$postfields =   'operatorUsername='.Config::get($currency.'.mxb.username').		
						'&username='. $username .
						'&gameId='. $gameid .
						'&provider=BetSoft';

	    $accessPassword = strtoupper(md5(Config::get($currency.'.mxb.password').$postfields));
	    
	    $result = App::curlGET(Config::get($currency.'.mxb.url').'GetExternalGameURL?accessKey='.$accessPassword.'&'.$postfields,false,8);
		$result = preg_replace('/xmlns[^=]*="[^"]*"/i', '', $result);
		$result = preg_replace('/[a-zA-Z]+:([a-zA-Z]+[=>])/', '$1', $result);
		$result = simplexml_load_string($result);
		

		if(isset($result->errorCode) && $result->errorCode == '0')
	    {
			$xml_array = json_decode(json_encode($result),TRUE);
			return $xml_array['GameURL'];

	    } 
		return false;
	}
	
	public function getLoginUrl($username, $gameid, $provider){
	

	    $postfields =   'operatorUsername='.Config::get($currency.'.mxb.username').
						'&username='. $username .
						'&gameID=' . $gameid .
						'&provider=' . $provider;

	    $accessPassword = strtoupper(md5(Config::get($currency.'.mxb.password').$postfields));
	    
	    $result = simplexml_load_string(App::curlGET(Config::get($currency.'.mxb.url').'CreateAccount?accessKey='.$accessPassword.'&'.$postfields,false,3));

	    if(isset($result->errorCode) && $result->errorCode == '0')
	    {
			return $result->GameURL;
	    }

	    return false;
    }
    
    public function getBalance($username ,$currency){
	    $postfields =   'operatorUsername='.Config::get($currency.'.mxb.username').
			    '&username='. $username;
	    
	    $accessPassword = strtoupper(md5(Config::get($currency.'.mxb.password').$postfields));

	    $result = simplexml_load_string(App::curlGET(Config::get($currency.'.mxb.url').'GetAccountBalance?accessKey='.$accessPassword.'&'.$postfields, true, 5 , 'getBalance'));

	    if(isset($result->errorCode) && $result->errorCode == '0')
	    {
			$dateTimeNow    = Carbon::now();
            $dateTimeNowStr = $dateTimeNow->toDateTimeString();
			
			if( Session::has('userid') ){
				
				DB::statement('call insert_cashbalance('.Session::get('userid').',"'.$username.'","'.$currency.'",'.$this->getPrdid().','.$result->balance.',"'.$dateTimeNowStr.'","'.$dateTimeNowStr.'")');
			
			}else{
				DB::statement('call insert_cashbalance('.Account::whereNickname($username)->whereCrccode($currency)->pluck('id').',"'.$username.'","'.$currency.'",'.$this->getPrdid().','.$result->balance.',"'.$dateTimeNowStr.'","'.$dateTimeNowStr.'")');
			}
			
			return $result->balance;
	    }

	    return false;
    }
	
	private function getPrdid(){
		
		$prdid = Cache::rememberForever('MXB_PRDID', function() {
                return Product::where( 'code' , '=' , 'MXB')->pluck('id');
        });
		
		return $prdid;
	}
    
    public function changePassword($username, $newPwd, $currency){
	    $postfields = 'operatorUsername='.Config::get($currency.'.mxb.username').
			  '&username='. $username .
			  '&newPassword='. $newPwd;
	    
	    $accessPassword = strtoupper(md5(Config::get($currency.'.mxb.password').$postfields));
	    
	    $result = simplexml_load_string(App::curlGET(Config::get($currency.'.mxb.url').'ChangeAccountPassword?accessKey='.$accessPassword.'&'.$postfields, true, 1 , 'changePassword'));	

	    if(isset($result->errorCode) && $result->errorCode == '0')
	    {
		return true;
	    }

	    return false;	
    }
    
    public function changeStatus($username, $status,$currency){
	    $postfields =   'operatorUsername='.Config::get($currency.'.mxb.username').
			    '&username='. $username .
			    '&newStatus='. $status;
	    
	    $accessPassword = strtoupper(md5(Config::get($currency.'.mxb.password').$postfields));
	    
	    $result = simplexml_load_string(App::curlGET(Config::get($currency.'.mxb.url').'ChangeAccountStatus?accessKey='.$accessPassword.'&'.$postfields, true, 1 , 'changeStatus'));	 
	    
	    if(isset($result->errorCode) && $result->errorCode == '0')
	    {
		return true;
	    }

	    return false;	
    }
    
    public function deposit($username, $currency, $amount , $transID){
		
		if( $currency == 'IDR' || $currency == 'VND' )
		{
			$amount = $amount / 1000;
		}

	    $postfields =   'operatorUsername='.Config::get($currency.'.mxb.username').
			    '&username='. $username .
			    '&amount='. $amount;
	    
	    $accessPassword = strtoupper(md5(Config::get($currency.'.mxb.password').$postfields));
	    
	    $result = simplexml_load_string(App::curlGET(Config::get($currency.'.mxb.url').'AccountDeposit?accessKey='.$accessPassword.'&'.$postfields, true, 10 , 'transferCredit'));	 
	  
	    if(isset($result->errorCode) && $result->errorCode == '0')
	    {
		return true;
	    }

	    return false;	    
    }
    
    public function withdraw($username, $currency , $amount , $transID){
		
		if( $currency == 'IDR' || $currency == 'VND' )
		{
			$amount = $amount / 1000;
		}

	    $postfields =   'operatorUsername='.Config::get($currency.'.mxb.username').
			    '&username='. $username .
			    '&amount='. $amount;
	    
	    $accessPassword = strtoupper(md5(Config::get($currency.'.mxb.password').$postfields));
	    
	    $result = simplexml_load_string(App::curlGET(Config::get($currency.'.mxb.url').'AccountWithdraw?accessKey='.$accessPassword.'&'.$postfields, true, 10 , 'transferCredit'));	 
	    
	    if(isset($result->errorCode) && $result->errorCode == '0')
	    {
		return true;
	    }

	    return false;	
    }
	
	public function updatePassword($username,$password,$currency){
		
	   $postfields =   'operatorUsername='.Config::get($currency.'.mxb.username').
			    '&username='. $username .
			    '&newPassword='. $password;
	    
	    $accessPassword = strtoupper(md5(Config::get($currency.'.mxb.password').$postfields));
	    
	    $result = simplexml_load_string(App::curlGET(Config::get($currency.'.mxb.url').'ChangeAccountPassword?accessKey='.$accessPassword.'&'.$postfields, true, 1 , 'updatePassword'));	 

	    if(isset($result->errorCode) && $result->errorCode == '0')
	    {
			return true;
	    }

	    return false;	
		
	}
    
    
}
