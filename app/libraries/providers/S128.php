<?php namespace App\libraries\providers;

use App\libraries\App;
use App\Models\AccountProduct;
use Config;
use Session;
use Request;
//getLoginUrl
//getBalance 
//deposit 
//withdraw 
//createUser

class S128 {

	public function getLoginUrl($username, $currency){
            
		return $this->createUser(['username' => $username , 'currency' => $currency] , true);
                
	}
	
	public function createUser( $params = array() , $login = false){

	    $url = Config::get($params['currency'].'.s128.url');

	    if (strlen($url) < 1) {
	        return false;
            }

		//get session
		$postfields = 'api_key='.Config::get($params['currency'].'.s128.api_key').'&agent_code='.Config::get($params['currency'].'.s128.agent_code').'&login_id='.$params['username'].'&name='.$params['username'];

		$result = simplexml_load_string(App::curl($postfields , $url.'/get_session_id.aspx' , true, 10 , 'getSessionId'));
		
		if(isset($result->status_code) && (string)$result->status_code == '00' )
		{
		    if (!$login) {
		        // Share logic with getLoginUrl, use flag to determine wan to add account product or not.
                Accountproduct::addAccountProduct($params['username'], 'S128', '');
            }
                        //login to game lobby
                        $sessionid = (string) $result->session_id;
                        $postfields2 = 'api_key='.Config::get($params['currency'].'.s128.api_key').'&session_id='.$sessionid.'&lang=id-ID&login_id='.$params['username'];
                        
                        return Config::get($params['currency'].'.s128.game_url').'/api/auth_login.aspx?'.$postfields2;
//                        $result2 = $this->curl($postfields2 , Config::get($params['currency'].'.s128.game_url').'/api/auth_login.aspx' , true, 10 , 'loginGameLobby');
//			return $result2;
		}
		
		return false;       
	}
	
	public function getBalance($username,$currency){
        $url = Config::get($currency.'.s128.api_key');

        if (strlen($url) < 1) {
            return false;
        }

		$postfields = 'api_key='.$url.'&agent_code='.Config::get($currency.'.s128.agent_code').'&login_id='.$username;

		$result = simplexml_load_string(App::curl($postfields , Config::get($currency.'.s128.url').'/get_balance.aspx' , true, 10 , 'getBalance'));
              
		if(isset($result->status_code) && (string) $result->status_code == '00' )
		{
			return (string) $result->balance;
		}
	
		return false;
		
	}
	
	public function deposit($username,$currency,$amount,$transid){
		
		if( $amount < 0 ) return false;
		
		return $this->transferCredit( $username, $currency, $transid, 'IN', $amount);
		
	}
		
	public function withdraw($username,$currency,$amount,$transid){

		if( $amount < 0 ) return false;
		
		return $this->transferCredit( $username, $currency, $transid, 'OUT', $amount);
	}
	
	private function transferCredit( $username, $currency, $billno, $type, $credit){
		
	 	if( $currency == 'IDR' || $currency == 'VND' )
		{
			$credit = $credit / 1000;
		}

                if( $type == 'IN' ){
                    $postfields = 'api_key='.Config::get($currency.'.s128.api_key').'&agent_code='.Config::get($currency.'.s128.agent_code').'&login_id='.$username.'&name='.$username.'&amount='.$credit.'&ref_no='.$billno;
                    $result = simplexml_load_string(App::curl($postfields , Config::get($currency.'.s128.url').'/deposit.aspx ' , true, 30 , 'deposit')) ;
                } else {
                    $postfields = 'api_key='.Config::get($currency.'.s128.api_key').'&agent_code='.Config::get($currency.'.s128.agent_code').'&login_id='.$username.'&amount='.$credit.'&ref_no='.$billno;
                    $result = simplexml_load_string(App::curl($postfields , Config::get($currency.'.s128.url').'/withdraw.aspx' , true, 30 , 'withdraw')) ;
                } 
//                \Log::debug('s128: ' . (string)$result->status_code == '00');
                
		if(isset($result->status_code) && (string)$result->status_code == '00')
		{
			return true;
		}
		
		return false;
	}

}
?>