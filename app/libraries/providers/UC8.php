<?php namespace App\libraries\providers;

use App\libraries\App;
use App\Models\AccountProduct;
use App\Models\Product;
use App\Models\Account;
use Config;
use Lang;
use Carbon\Carbon;
use Cache;
use DB;
use Session;
use Crypt;
//getLoginUrl
//getBalance
//deposit
//withdraw
//createUser

class UC8
{

    public function getLoginUrl($username, $currency, $gameCode, $language_code = 'en')
    {
        $lang = collect(array(
            'en' => 'en',
            'cn' => 'cn',
            'tw' => 'tw',
        ));

        return Config::get($currency . '.uc8.api_url') . 'open/' . $gameCode . '?partner_id=' . Config::get($currency . '.uc8.partner_id') . '&token=' . Config::get($currency . '.uc8.partner_id') . '_' . $username . '_' . mt_rand(111, 999) . '&lang=' . $lang->get($language_code, 'en') . '&fun=0';
    }

    public function createUser($params = array())
    {
        $postfields = array(
            'currency' => strtoupper($params['currency']),
            'partner_id' => Config::get($params['currency'] . '.uc8.partner_id'),
            'password' => Crypt::decrypt($params['password']),
            'username' => $params['username'],
        );

        $hash = $this->generateSignature($params['currency'], $postfields);

        $postfields['hash'] = $hash;

        $result = json_decode(App::curl(json_encode($postfields), Config::get($params['currency'] . '.uc8.api_url') . 'game/user.html', true, 3, 'createUser'));if (isset($result) && ($result->error == '0' || $result->error == '3')) {
            Accountproduct::addAccountProduct($params['username'], 'UC8', '');

            return true;
        }

        return false;

    }

    public function getBalance($username, $currency)
    {
        $postfields = array(
            'partner_id' => Config::get($currency . '.uc8.partner_id'),
            'username' => $username,
        );

        $hash = $this->generateSignature($currency, $postfields);

        $postfields['hash'] = $hash;

        $result = json_decode(App::curl(json_encode($postfields), Config::get($currency . '.uc8.api_url') . 'game/balance.html', true, 5, 'getBalance'));

        if (isset($result->balance)) {
            $dateTimeNow = Carbon::now();
            $dateTimeNowStr = $dateTimeNow->toDateTimeString();

            if (Session::has('userid')) {
                DB::statement('call insert_cashbalance(' . Session::get('userid') . ',"' . $username . '","' . $currency . '",' . $this->getPrdid() . ',' . $result->balance . ',"' . $dateTimeNowStr . '","' . $dateTimeNowStr . '")');
            } else {
                DB::statement('call insert_cashbalance(' . Account::whereNickname($username)->pluck('id') . ',"' . $username . '","' . $currency . '",' . $this->getPrdid() . ',' . $result->balance . ',"' . $dateTimeNowStr . '","' . $dateTimeNowStr . '")');
            }

            return $result->balance;
        }

        return false;
    }


    public function deposit($username, $currency, $amount, $transid)
    {
        if ($amount < 0) return false;

        return $this->transferCredit($username, $currency, $transid, 'IN', $amount);
    }

    public function withdraw($username, $currency, $amount, $transid)
    {
        if ($amount < 0) return false;

        return $this->transferCredit($username, $currency, $transid, 'OUT', $amount);
    }

    private function transferCredit($username, $currency, $billno, $type, $credit)
    {
        if ($currency == 'IDR' || $currency == 'VND') {
            $credit = $credit / 1000;
        }

        $postfields = array(
            'amount' => $credit,
            'currency' => strtoupper($currency),
            'partner_id' => Config::get($currency . '.uc8.partner_id'),
            'ref_id' => $billno,
            'username' => $username,

        );

        $hash = $this->generateSignature($currency, $postfields);

        $postfields['hash'] = $hash;

        $result = json_decode(App::curl(json_encode($postfields), Config::get($currency . '.uc8.api_url') . 'game/' . ($type == 'IN' ? 'credit' : 'debit') . '.html', true, 30, 'transferCredit'));

        if (isset($result->error) && $result->error == '0') {
            return true;
        }

        return false;
    }

    public function gamesList($currency)
    {
        $postfields = array(
            'partner_id' => Config::get($currency . '.uc8.partner_id'),
        );

        $hash = $this->generateSignature($currency, $postfields);

        $postfields['hash'] = $hash;

        $result = json_decode(App::curl(json_encode($postfields), Config::get($currency . '.uc8.api_url') . 'game/list.html', true, 3, 'gamesList'));

        $lang = Lang::getLocale();

        //$lang = 'cn'; change language
        $language_code = 'en';
        if (!in_array($language_code, array('en', 'tw', 'cn'))) {
            $language_code = 'en';
        }

        if (isset($result->message) && $result->message == 'Success' && isset($result->games)) {
            $array = array();

            foreach ($result->games as $value) {
                if (isset($value->translation->{$lang})) {
                    $name = $value->translation->{$lang};
                } else {
                    $name = $value->name;
                }

                $code = $value->code;
                $image = $value->image;

                $array[] = array(
                    'code' => $code,
                    'name' => $name,
                    'image' => $image,
                );
            }

            return $array;
        }

        return false;
    }

    public function generateSignature($currency, $params)
    {
        $secret = Config::get($currency . '.uc8.secret');
        $values = [];


        foreach ($params as $key => $val) {
            $values[] = $key . '=' . $val;
        }

        return md5(implode('&', $values) . $secret);
    }

    private function getPrdid()
    {
        $prdid = Cache::rememberForever('UC8_PRDID', function () {
            return Product::where('code', '=', 'UC8')->pluck('id');
        });

        return $prdid;
    }
}