<?php namespace App\libraries\providers;

use App\libraries\App;
use App\Models\AccountProduct;
use App\Models\Product;
use App\Models\Account;
use Crypt;
use Config;
use Carbon\Carbon;
use Lang;
use Cache;
use DB;
use Session;

//createuser, changepassword, changestatus, getbalance, deposit, withdraw

class EVO{
    
    public function createUser( $params = array() ){
        $this->deposit($params['username'], $params['currency'], 0.01,Carbon::now()->format('md\THi'));
        $this->withdraw($params['username'], $params['currency'], 0.01,Carbon::now()->format('md\THi'));
                
        Accountproduct::addAccountProduct($params['username'],'EVO','');
        return true;
    }   
	
	
    public function getLoginUrl($params, $session, $language, $ip){
        $uuid = App::generateAlphanumeric(8).'-'.App::generateAlphanumeric(4).'-'.App::generateAlphanumeric(4).'-'.App::generateAlphanumeric(12);
        $data = array (
                    'uuid' => $uuid,
                    'player' => array (
                        'id' => $params['username'],
                        'update' => true,
                        'firstName' => $params['username'],
                        'lastName' => $params['username'],
                        'nickname' => $params['username'],
                        'country' => 'MY',
                        'language' => 'en',
                        'currency' => Config::get($params['currency'].'.evo.currency'),
                        'session' => array (
                            'id' => $session,
                            'ip' => $ip,
                        ),
                    ),
                    'config' => array (
                        'brand' => array (
                            'id' => '1',
                            'skin' => '1',
                        ),
                        'channel' => array (
                            'wrapped' => false,
                            'mobile' => false,
                        ),
                    ),
                );
        
        $postfields = json_encode($data);
        
        $result = json_decode($this->curl(Config::get($params['currency'].'.evo.url').'ua/v1/'.Config::get($params['currency'].'.evo.casino_key').'/'.Config::get($params['currency'].'.evo.api_token'), $postfields, true, 3, 'getLoginUrl'));
    
        if(isset($result->entry)){
            return $result->entry;
        }
        
        return false;
    }
    
    public function getBalance($username ,$currency){
	$postfields = 'cCode=RWA'.
                '&ecID='.Config::get($currency.'.evo.casino_key').
                '&euID='.$username.
                '&output=0';

        $result = json_decode( App::curlGET( Config::get($currency.'.evo.url').'api/ecashier?'.$postfields, true, 10, 'getBalance' ) );

        if( isset($result->userbalance->result) && $result->userbalance->result == 'Y' )
        {
            return $result->userbalance->abalance;
        }
    }
    
    public function deposit($username, $currency, $amount , $transID){
	if( $currency == 'IDR' || $currency == 'VND' )
        {
            //$amount = $amount / 1000;
        }

        $postfields = 'cCode=ECR'.
                '&ecID='.Config::get($currency.'.evo.casino_key').
                '&euID='.$username.
                '&amount='.$amount.
                '&eTransID=RW'.$transID.
                '&createuser=Y&output=0';

        $result = json_decode( App::curlGET( Config::get($currency.'.evo.url').'api/ecashier?'.$postfields, true, 10, 'Deposit' ) );

        if( isset($result->transfer->result) && $result->transfer->result == 'Y' )
        {
            return true;
        }
    }
    
    public function withdraw($username, $currency , $amount , $transID){
	if( $currency == 'IDR' || $currency == 'VND' )
        {
            //$amount = $amount / 1000;
        }

        $postfields = 'cCode=EDB'.
                '&ecID='.Config::get($currency.'.evo.casino_key').
                '&euID='.$username.
                '&amount='.$amount.
                '&eTransID=RW'.$transID.
                '&output=0';

        $result = json_decode( App::curlGET( Config::get($currency.'.evo.url').'api/ecashier?'.$postfields, true, 10, 'Withdraw' ) );

        if( isset($result->transfer->result) && $result->transfer->result == 'Y' )
        {
            return true;
        }
    }
    
    function curl($url, $postFields, $log = true, $timeout = 30, $method = '')
    {
        $headers[] = "Content-Type:application/json";
        $data_json = $postFields;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Content-Length: ' . strlen($data_json)));
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);

        if ($log)
            App::insert_api_log(array('request' => $url . '?' . $data_json, 'return' => $response, 'method' => $method));

        return $response;
    }

    
}
