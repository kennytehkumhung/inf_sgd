<?php namespace App\libraries\providers;

use App\libraries\App;
use App\Models\AccountProduct;
use Config;
//getLoginUrl
//getBalance
//deposit
//withdraw
//createUser


class PLS {

	public function createUser( $params = array() ){
		
		$postfields = 'host_id='.Config::get($params['currency'].'.pls.host_id').
					  '&member_id='.$params['username'];
		

		$result = json_decode(App::curlGET(Config::get($params['currency'].'.pls.url').'/funds/createplayer/?'.$postfields, true, 5 , 'createUser'));
		
		if( isset($result->status_code) && $result->status_code == '0' )
		{
			Accountproduct::addAccountProduct($params['username'],'PLS','');
			return true;
		}
		
		return false;
		
	}
	
	public function getBalance($username,$currency){
		
		$postfields = 'host_id='.Config::get($currency.'.pls.host_id').
					  '&member_id='.$username;
		

		$result = json_decode(App::curlGET(Config::get($currency.'.pls.url').'/funds/getbalance/?'.$postfields, true, 10 , 'getBalance'));
		
		if( isset($result->status_code) && $result->status_code == '0' )
		{
			return $result->balance/100;
		}
		
		return false;
		
	}
	
	public function deposit($username,$currency,$amount,$transid){
		
		if( $amount < 0 ) return false;
		
		return $this->transferCredit( $username, $currency, $transid, 'deposit', $amount);
		
	}
		
	public function withdraw($username,$currency,$amount,$transid){

		if( $amount < 0 ) return false;
		
		return $this->transferCredit( $username, $currency, $transid, 'withdraw', $amount);
	}
	
	private function transferCredit( $username, $currency, $billno, $type, $credit){
		
		if( $currency == 'IDR' || $currency == 'VND' )
		{
			$credit = $credit / 1000;
		}
		
		$postfields = 'host_id='.Config::get($currency.'.pls.host_id').
					  '&member_id='.$username.
					  '&txn_id='.$billno.
					  '&amount='.$credit * 100;
		

		$result = json_decode(App::curlGET(Config::get($currency.'.pls.url').'/funds/'.$type.'/?'.$postfields, true, 30 , 'transferCredit'));
		
		if( isset($result->status_code) && $result->status_code == '0' )
		{
			return true;
		}
		
		return false;
	}

	public function gameList($currency){
		
		$postfields = 'host_id='.Config::get($currency.'.pls.host_id');
		

		$result = json_decode(App::curlGET(Config::get($currency.'.pls.url').'feed/gamelist/?'.$postfields, true, 10 , 'gameList'));
		
		return $result;
		
	}
	

	

	
   
}
?>