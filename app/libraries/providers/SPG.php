<?php namespace App\libraries\providers;

use App\libraries\App;
use App\Models\AccountProduct;
use App\Models\Product;
use App\Models\Account;
use Config;
use Carbon\Carbon;
use Lang;
use Cache;
use DB;
use Session;
//getBalance
//deposit
//withdraw
//createUser

class SPG {
 
	public function getLoginUrl($acctId, $token, $game, $currency){
		
		$postfields = 'acctId='.Config::get($currency.'.spg.prefix').$acctId.
                            '&language=en_US'.
                            '&token='.$token.
                            '&game='.$game;
        App::insert_api_log(array('request' => Config::get($currency.'.spg.game_url').'?'.$postfields, 'return' => '', 'method' => '1'));
		return Config::get($currency.'.spg.game_url').'?'.$postfields;
	
	}
        
        public function createUser( $params = array() ){
                $this->deposit( $params['username'], $params['currency'], 0.01);
                $this->withdraw($params['username'], $params['currency'], 0.01);
                
                Accountproduct::addAccountProduct($params['username'],'SPG','');
                return true;
	}
	
	public function getBalance($username , $currency){
                date_default_timezone_set("Asia/Kuala_Lumpur");
                $date = date('YmdHis');
                $random = App::generateNum(6);
                $serialNo = $date.$random;
                
                $header[0] = 'Accept: text/xml,application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5'; 
                $header[] = 'Cache-Control: max-age=0'; 
                $header[] = 'Connection: keep-alive'; 
                $header[] = 'Keep-Alive: 300'; 
                $header[] = 'Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7'; 
                $header[] = 'Accept-Language: en-us,en;q=0.5'; 
                $header[] = 'Pragma: '; 
                $header[] = 'API: getAcctInfo';
                $header[] = 'DataType: XML';
                $header[] = 'Content-Type: text/xml';
                
                $return  = '<serialNo>'.$serialNo.'</serialNo>';
                $return .= '<merchantCode>'.Config::get($currency.'.spg.merchant_id').'</merchantCode>';
                $return .= '<pageIndex>0</pageIndex>';
                $return .= '<acctId>'.Config::get($currency.'.spg.prefix').$username.'</acctId>';
                
                $postfields = '<?xml version="1.0" encoding="UTF-8"?>
				   <GetAcctInfoRequest>
					'.$return.'
				   </GetAcctInfoRequest>';
                
		$result = simplexml_load_string( App::curlXML( $postfields , Config::get($currency.'.spg.api_url'), true, 5 , 'getBalance', $header) );

		if( isset($result->code) && $result->code == '0')
		{
			$dateTimeNow    = Carbon::now();
			$dateTimeNowStr = $dateTimeNow->toDateTimeString();
			
			if( isset($result->list->AcctInfo->balance) )
			{
				if( Session::has('userid') ){
					DB::statement('call insert_cashbalance('.Session::get('userid').',"'.Session::get('username').'","'.$currency.'",'.$this->getPrdid().','.$result->list->AcctInfo->balance.',"'.$dateTimeNowStr.'","'.$dateTimeNowStr.'")');
				}else{
					DB::statement('call insert_cashbalance('.Account::whereNickname($username)->whereCrccode($currency)->pluck('id').',"'.$username.'","'.$currency.'",'.$this->getPrdid().','.$result->list->AcctInfo->balance.',"'.$dateTimeNowStr.'","'.$dateTimeNowStr.'")');
				}
			}
			
			return $result->list->AcctInfo->balance;
		}
		
		return false;
	}
	
	private function getPrdid(){
		
		$prdid = Cache::rememberForever('SPG_PRDID', function() {
                return Product::where( 'code' , '=' , 'SPG')->pluck('id');
        });
		
		return $prdid;
	}

	public function deposit($username,$currency,$amount){
		
			if( $currency == 'IDR' || $currency == 'VND' )
            {
                $amount = $amount / 1000;
            }
            
                date_default_timezone_set("Asia/Kuala_Lumpur");
                $date = date('YmdHis');
                $random = App::generateNum(6);
                $serialNo = $date.$random;
                
                $header[0] = 'Accept: text/xml,application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5'; 
                $header[] = 'Cache-Control: max-age=0'; 
                $header[] = 'Connection: keep-alive'; 
                $header[] = 'Keep-Alive: 300'; 
                $header[] = 'Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7'; 
                $header[] = 'Accept-Language: en-us,en;q=0.5'; 
                $header[] = 'Pragma: '; 
                $header[] = 'API: deposit';
                $header[] = 'DataType: XML';
                $header[] = 'Content-Type: text/xml';
                
                $return  = '<serialNo>'.$serialNo.'</serialNo>';
                $return .= '<merchantCode>'.Config::get($currency.'.spg.merchant_id').'</merchantCode>';
                $return .= '<acctId>'.Config::get($currency.'.spg.prefix').$username.'</acctId>';
                $return .= '<currency>'.$currency.'</currency>';
                $return .= '<amount>'.$amount.'</amount>';
                
                $postfields = '<?xml version="1.0" encoding="UTF-8"?>
				   <DepositRequest>
					'.$return.'
				   </DepositRequest>';
                
		$result = simplexml_load_string( App::curlXML( $postfields , Config::get($currency.'.spg.api_url'), true, 10 , 'Deposit', $header) );
	
		if( isset($result->code) && $result->code == '0')
		{
			return true;
		}
		
		return false;
	}
		
	public function withdraw($username,$currency,$amount){
		
			if( $currency == 'IDR' || $currency == 'VND' )
            {
                $amount = $amount / 1000;
            }
            
                date_default_timezone_set("Asia/Kuala_Lumpur");
                $date = date('YmdHis');
                $random = App::generateNum(6);
                $serialNo = $date.$random;
                
                $header[0] = 'Accept: text/xml,application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5'; 
                $header[] = 'Cache-Control: max-age=0'; 
                $header[] = 'Connection: keep-alive'; 
                $header[] = 'Keep-Alive: 300'; 
                $header[] = 'Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7'; 
                $header[] = 'Accept-Language: en-us,en;q=0.5'; 
                $header[] = 'Pragma: '; 
                $header[] = 'API: withdraw';
                $header[] = 'DataType: XML';
                $header[] = 'Content-Type: text/xml';
                
                $return  = '<serialNo>'.$serialNo.'</serialNo>';
                $return .= '<merchantCode>'.Config::get($currency.'.spg.merchant_id').'</merchantCode>';
                $return .= '<acctId>'.Config::get($currency.'.spg.prefix').$username.'</acctId>';
                $return .= '<currency>'.$currency.'</currency>';
                $return .= '<amount>'.$amount.'</amount>';
                
                $postfields = '<?xml version="1.0" encoding="UTF-8"?>
				   <WithdrawRequest>
					'.$return.'
				   </WithdrawRequest>';
                
		$result = simplexml_load_string( App::curlXML( $postfields , Config::get($currency.'.spg.api_url'), true, 10 , 'Withdraw', $header) );
	
		if( isset($result->code) && $result->code == '0')
		{
			return true;
		}
		
		return false;
	}
}
?>