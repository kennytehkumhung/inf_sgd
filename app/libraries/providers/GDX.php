<?php namespace App\libraries\providers;

use App\libraries\App;
use App\Models\Accountproduct;
use App\Models\Product;
use Config;
//getBalance
//deposit
//withdraw
//createUser

class GDX {
 
	public function getLoginUrl($token , $currency, $userid ,$lang){
		
		$postfields = 'LoginTokenID='.$token.
					  '&OperatorCode='.Config::get($currency.'.gdx.operator_code').
					  '&lang='.$lang.
					  '&playerid='. $userid;
		
		return Config::get($currency.'.gdx.login_url').'?'.$postfields;
	
	}
	

	public function createUser( $params = array() ){
		
		$headerxml[0] = 'Accept: text/xml,application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5'; 
        $headerxml[] = 'Cache-Control: max-age=0'; 
        $headerxml[] = 'Connection: keep-alive'; 
        $headerxml[] = 'Keep-Alive: 300'; 
        $headerxml[] = 'Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7'; 
        $headerxml[] = 'Accept-Language: en-us,en;q=0.5'; 
        $headerxml[] = 'Pragma: '; 
        $headerxml[] = 'DataType: XML';
        $headerxml[] = 'Content-Type: text/xml';
		

        date_default_timezone_set("Asia/Kuala_Lumpur");
        $date = date('ymdHis');
        $random = App::generatePassword();
        $msgID = 'M'.$date.$random;
        
        $header  = '<Method>cCreateMember</Method>';
        $header .= '<MerchantID>'.Config::get($params['currency'].'.gdx.operator_code').'</MerchantID>';
        $header .= '<MessageID>'.$msgID.'</MessageID>';
        
        $param  = '<UserID>'.$params['username'].'</UserID>';
        $param .= '<CurrencyCode>'.$params['currency'].'</CurrencyCode>';
        
        $postfields = '<?xml version="1.0" encoding="UTF-8"?>
		   <Request>
                                <Header>
			'.$header.'
                                </Header>
                                <Param>
                                '.$param.'
                                </Param>
		   </Request>';
                
		$result = simplexml_load_string( App::curlXML( $postfields , Config::get($params['currency'].'.gdx.url'), true, 5 , '', $headerxml) );
	
		if( isset($result->Header->ErrorCode) && ($result->Header->ErrorCode == '0' || $result->Header->ErrorCode == '207' ))
		{
			Accountproduct::addAccountProduct($params['username'],'GDX','');
			return true;
		}
		
		return false;
		
	}
	
	private function getMsgid($id){
		
		date_default_timezone_set("Asia/Kuala_Lumpur");
        $date = date('ymdHis');
        $random = App::generatePassword();
        $msgID = $id.$date.$random;
		
		return $msgID;
		
	}
	
	private function getHeader(){
		
		
		$headerxml[0] = 'Accept: text/xml,application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5'; 
        $headerxml[] = 'Cache-Control: max-age=0'; 
        $headerxml[] = 'Connection: keep-alive'; 
        $headerxml[] = 'Keep-Alive: 300'; 
        $headerxml[] = 'Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7'; 
        $headerxml[] = 'Accept-Language: en-us,en;q=0.5'; 
        $headerxml[] = 'Pragma: '; 
        $headerxml[] = 'DataType: XML';
        $headerxml[] = 'Content-Type: text/xml';
		
		
		return $headerxml;
		
	}

	
	public function getBalance($username , $currency){
		
		
		$currencyCode['THB'] = '764';
		$currencyCode['CNY'] = '156';
		$currencyCode['IDR'] = '360';
		$currencyCode['VND'] = '704';
		$currencyCode['MYR'] = '458';
		
        $header  = '<Method>cCheckClient</Method>';
        $header .= '<MerchantID>'.Config::get($currency.'.gdx.operator_code').'</MerchantID>';
        $header .= '<MessageID>'.$this->getMsgid('C').'</MessageID>';
        
        $param   = '<UserID>'.$username.'</UserID>';
        $param  .= '<CurrencyCode>'.$currency.'</CurrencyCode>';
        $param  .= '<RequestBetLimit>1</RequestBetLimit>';
        
        $postfields = '<?xml version="1.0" encoding="UTF-8"?>
		   <Request>
                                <Header>
			'.$header.'
                                </Header>
                                <Param>
                                '.$param.'
                                </Param>
				   </Request>';

		$result = simplexml_load_string( App::curlXML( $postfields , Config::get($currency.'.gdx.url'), true, 10 , '', $this->getHeader() ) );

		if( isset($result->Header->ErrorCode) && $result->Header->ErrorCode == '0' )
		{
			return $result->Param->Balance;
		}
		
		return false;
		
	}

	public function deposit($username,$currency,$amount,$transid){
		
		if( $amount < 0 ) return false;

		return $this->transferCredit( $username, $currency, $transid, 'cDeposit', $amount);
		
	}
		
	public function withdraw($username,$currency,$amount,$transid){
		
		if( $amount < 0 ) return false;
		
		return $this->transferCredit( $username, $currency, $transid, 'cWithdrawal', $amount);
		
	}
	

	private function transferCredit( $username, $currency, $billno, $direction, $credit){
		
		if( $currency == 'IDR' || $currency == 'VND' )
		{
			$credit = $credit / 1000;
		}
		
		$id = $direction == 'cDeposit' ? 'D' : 'W';
		
		$currencyCode['THB'] = '764';
		$currencyCode['CNY'] = '156';
		$currencyCode['IDR'] = '360';
		$currencyCode['VND'] = '704';
		$currencyCode['MYR'] = '458';
			
		$header  = '<Method>'.$direction.'</Method>';
        $header .= '<MerchantID>'.Config::get($currency.'.gdx.operator_code').'</MerchantID>';
        $header .= '<MessageID>'.$this->getMsgid($id).'</MessageID>';
        
        $param   = '<UserID>'.$username.'</UserID>';
        $param  .= '<CurrencyCode>'.$currency.'</CurrencyCode>';
        $param  .= '<Amount>'.$credit.'</Amount>';
        $param  .= '<EnableInGameTransfer>1</EnableInGameTransfer>';
        
        $postfields = '<?xml version="1.0" encoding="UTF-8"?>
		   <Request>
                                <Header>
			'.$header.'
                                </Header>
                                <Param>
                                '.$param.'
                                </Param>
				   </Request>';

		$result = simplexml_load_string( App::curlXML( $postfields , Config::get($currency.'.gdx.url'), true, 20 , '', $this->getHeader() ) );

		if( isset($result->Header->ErrorCode) && $result->Header->ErrorCode == '0' )
		{
			return true;
		}
		
		return false;
	
	}
	

	
   
}
?>