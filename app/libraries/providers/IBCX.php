<?php namespace App\libraries\providers;

use App\libraries\App;
use App\Models\AccountProduct;
use App\Models\Account;
use App\Models\Product;
use Config;
use Lang;
use Carbon\Carbon;
use Cache;
use DB;
use Session;

//getBalance
//deposit
//withdraw
//createUser
//setMemberBetSetting


class IBCX {
 
    const API_URL     = 'http://hosay126.com/API/';
	
    const VENDOR_ID   = 'jsIsu65df787saiu92';
	
    const OPERATOR_ID = 'INFINIWIN';

	public function createUser( $params = array(), $oddstype = 1, $maxtransfer = 999999, $mintransfer = 1){

		$postfields = 'vendor_id='.Config::get($params['currency'].'.ibc.vendor_id').
					  '&operatorid='.Config::get($params['currency'].'.ibc.operator_id').
					  '&vendor_member_id='.$params['username'].
					  '&username='.$params['username'].
					  '&currency='.$this->getCurrencyId($params['currency']).
					  '&firstname='.$params['username'].
					  '&lastname='.$params['username'].
					  '&oddstype='.$oddstype.
					  '&mintransfer='.$mintransfer.
					  '&maxtransfer='.$maxtransfer;

		$result = json_decode( App::curl( $postfields , Config::get($params['currency'].'.ibc.url').'CreateMember' , true, 3 , 'createUser') );

		if( isset($result->error_code) && ( $result->error_code == '0' || $result->error_code == '6' ) )
		{
			$this->setMemberBetSetting( $params['username'] , Config::get($params['currency'].'.ibc.bet_setting') , $params['currency']);
			Accountproduct::addAccountProduct($params['username'],'IBCX','');
			return true;
		}
		
		return false;
		
	}
	
	public function setMemberBetSetting($username, $betsetting, $currency){
		
		$postfields = 'vendor_id='.Config::get($currency.'.ibc.vendor_id').
					  '&operatorid='.Config::get($currency.'.ibc.operator_id').
					  '&vendor_member_id='.$username.
					  '&bet_setting='.$betsetting;
	

		$result = json_decode( App::curl( $postfields , Config::get($currency.'.ibc.url').'SetMemberBetSetting' , true, 1 , 'setMemberBetSetting') );
		
		if( isset($result->error_code) && $result->error_code == '0' )
		{
			return true;
		}
		
		return false;
		
	}
	
	public function getBalance($username,$currency){
		
		//App::check_process('ibc_getbalance_process_'.$username);
		
		$postfields = 'vendor_id='.Config::get($currency.'.ibc.vendor_id').
					  '&operatorid='.Config::get($currency.'.ibc.operator_id').
					  '&vendor_member_ids='.$username;

		$result = json_decode( App::curl( $postfields , Config::get($currency.'.ibc.url').'CheckUserBalance' , true, 10 , 'getBalance') );
		
		//App::unlock_process('ibc_getbalance_process_'.$username);

		if( isset($result->error_code) && $result->error_code == '0' )
		{
			$dateTimeNow    = Carbon::now();
			$dateTimeNowStr = $dateTimeNow->toDateTimeString();
			$balance = $result->Data[0]->balance == null ? 0 : $result->Data[0]->balance;
			
			if( Session::has('userid') ){
				
				DB::statement('call insert_cashbalance('.Session::get('userid').',"'.$username.'","'.$currency.'",'.$this->getPrdid().','.$balance.',"'.$dateTimeNowStr.'","'.$dateTimeNowStr.'")');
			}else{
				DB::statement('call insert_cashbalance('.Account::whereNickname($username)->whereCrccode($currency)->pluck('id').',"'.$username.'","'.$currency.'",'.$this->getPrdid().','.$balance.',"'.$dateTimeNowStr.'","'.$dateTimeNowStr.'")');
				
			}
			
			return $result->Data[0]->balance;
		}
		
		return false;
		
	}
	
	private function getPrdid(){
		
		$prdid = Cache::rememberForever('IBCX_PRDID', function() {
                return Product::where( 'code' , '=' , 'IBCX')->pluck('id');
        });
		
		return $prdid;
	}
	
	public function deposit($username,$currency,$amount,$transid){
		
		if( $amount < 0 ) return false;
		
		return $this->transferCredit( $username, $currency, $transid, '1', $amount);
		
	}
		
	public function withdraw($username,$currency,$amount,$transid){
		
		if( $amount < 0 ) return false;
		
		return $this->transferCredit( $username, $currency, $transid, '0', $amount);
	}
	
	private function transferCredit( $username, $currency, $billno, $direction, $credit){

		if( $currency == 'IDR' || $currency == 'VND' )
		{
			$credit = $credit / 1000;
		}
		
		$postfields = 'vendor_id='.Config::get($currency.'.ibc.vendor_id').
					  '&operatorid='.Config::get($currency.'.ibc.operator_id').
					  '&vendor_member_id='.$username.
					  '&currency='.$this->getCurrencyId($currency).
					  '&vendor_trans_id='.$billno.
					  '&direction='.$direction.
					  '&amount='.$credit;

		$result = json_decode( App::curl( $postfields , Config::get($currency.'.ibc.url').'FundTransfer' , true, 15 , 'transferCredit') );

		if( isset($result->error_code) && $result->error_code == '0' )
		{
			return true;
		}
		
		return false;
	}

    private function getCurrencyId($code) {
        $currencies = collect(array(
            'MYR' => 2,
            'IDR' => 15,
            'VND' => 51,
            'THB' => 4,
            'RMB' => 13,
            'CNY' => 13,
        ));

        return $currencies->get($code);
    }
	

	

	
   
}
?>