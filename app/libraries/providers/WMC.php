<?php
namespace App\libraries\providers;
use App\libraries\App;
use App\Models\Account;
use App\Models\AccountProduct;
use App\Models\Product;
use Config;
use Carbon\Carbon;
use Lang;
use Cache;
use DB;
use Session;
use Crypt;
//getLoginUrl
//getBalance
//deposit
//withdraw
//createUser


class WMC{


	
	public function createUser($params = array()){
			
			$prdid 		= Product::where('code','=','WMC')->pluck('id');
			$checking	= Accountproduct::where('acccode','=',$params['username'])->where('prdid','=',$prdid)->pluck('id'); 
		
			
			if(empty($checking)){
			$pw			=$params['password'];
			$password 	= Crypt::decrypt($pw);
			$data 		= array(
            'cmd'		=> 'MemberRegister',
            'vendorId' 	=> Config::get($params['currency'] . '.wmc.vendorId'),
            'signature' => Config::get($params['currency'] . '.wmc.signature'),
            'user' 		=> Config::get($params['currency'] . '.wmc.prefix').$params['username'],
			'password'	=> $password,
			'username'	=> $params['username'], 
			 
                );
				
			$postfields	=$data; 
			
			$result		=json_decode($this-> curl(Config::get($params['currency'] . '.wmc.BaseServiceURL') , $postfields, true, 3,'createUser'));
			
			if(isset($result) && $result->errorCode == '0'){
			Accountproduct::addAccountProduct($params['username'], 'WMC', '');
			
            return true;
	     	}
			}
			return false;
		 
	}
	
	public function getBalance($username,$currency){
			
			
			$data 		= array(
            'cmd'		=> 'GetBalance',
            'vendorId' 	=> Config::get($currency. '.wmc.vendorId'), 
            'signature' => Config::get($currency. '.wmc.signature'), 
            'user' 		=> Config::get($currency.'.wmc.prefix').$username, 
                );
			$postfields=$data; 
			
			$result=json_decode($this-> curl(Config::get($currency. '.wmc.BaseServiceURL') , $postfields, true, 3,'getBalance'));
			
			if(isset($result) && $result->errorCode == '10501'){
			
			$data = array(
            'cmd'		=> 'GetBalance',
            'vendorId' 	=> Config::get($currency. '.wmc.vendorId'), 
            'signature' => Config::get($currency. '.wmc.signature'), 
            'user' 		=> $username, 
                );
			$postfields=$data; 
			$result=json_decode($this-> curl(Config::get($currency. '.wmc.BaseServiceURL') , $postfields, true, 3,'getBalance'));
			
			}
			
			if(isset($result) && $result->errorCode == '0'){
			if( $currency == 'IDR' || $currency == 'VND' )
			  {

			   return $result->result/1000;

			  }else {

			   return $result->result;

			  }
            
	     	}
			return false;
	
	
	}
	
		
	
	
	public function deposit($username, $currency, $amount , $transID){
		
			$data 		= array(
            'cmd'		=> 'ChangeBalance',
            'vendorId' 	=> Config::get($currency. '.wmc.vendorId'),
            'signature' => Config::get($currency. '.wmc.signature'),
            'user' 		=> Config::get($currency.'.wmc.prefix').$username,
			'money'		=> $amount,
			'order'		=> $transID, 
                );
				
			$postfields=$data;
			
			$result=json_decode($this-> curl(Config::get($currency . '.wmc.BaseServiceURL') , $postfields, true, 3,'deposit'));
			
			if(isset($result) && $result->errorCode == '10501'){
			
			$data = array(
			'cmd'		=> 'ChangeBalance',
            'vendorId' 	=> Config::get($currency. '.wmc.vendorId'),
            'signature' => Config::get($currency. '.wmc.signature'),
            'user' 		=> $username,
			'money'		=> $amount,
			'order'		=> $transID, 
                );
			$postfields=$data; 
			$result=json_decode($this-> curl(Config::get($currency. '.wmc.BaseServiceURL') , $postfields, true, 3,'deposit'));
			
			}
			
			if(isset($result) && $result->errorCode == '0'){
		
			return true;
					

	     	 }
			return false; 
			
			
		
		
	}
	
		public function withdraw($username, $currency, $amount , $transID){
		
		
			$data	 	=  array(
            'cmd'		=> 'ChangeBalance',
            'vendorId' 	=> Config::get($currency. '.wmc.vendorId'),
            'signature' => Config::get($currency. '.wmc.signature'),
            'user' 		=> Config::get($currency.'.wmc.prefix').$username,
			'money'		=> "-".$amount,
			'order'		=> $transID, 
                );
			
			$postfields=$data; 
			$result=json_decode($this-> curl(Config::get($currency . '.wmc.BaseServiceURL') , $postfields, true, 3,'withdraw'));
			if(isset($result) && $result->errorCode == '10501'){
				
				 $data = array(
				'cmd'		=> 'ChangeBalance',
				'vendorId' 	=> Config::get($currency. '.wmc.vendorId'),
				'signature' => Config::get($currency. '.wmc.signature'),
				'user' 		=> $username,
				'money'		=>"-".$amount,
				'order'		=>$transID, 
				);
				
			$postfields=$data; 
			$result=json_decode($this-> curl(Config::get($currency . '.wmc.BaseServiceURL') , $postfields, true, 3,'withdraw'));
				
			}
			if(isset($result) && $result->errorCode == '0'){
		
			 return true; 

	     	 }
			 return false;  
			
			
		
		
	}
	
	public function getLoginUrl($username,$currency,$password){
			$data = array(
            'cmd'		=> 'SigninGame',
            'vendorId'	=> Config::get($currency. '.wmc.vendorId'),
            'signature' => Config::get($currency. '.wmc.signature'),
			'user'		=> Config::get($currency.'.wmc.prefix').$username,
            'lang' 		=> 1,
			'password'	=> Crypt::decrypt($password),
			'isTest'	=> 0,
                );
			$result2=$this->updatepassword( Config::get($currency.'.wmc.prefix').$username,$password,$currency); 
			if($result2){
			$postfields=$data; 
			$result=json_decode($this-> curl(Config::get($currency . '.wmc.BaseServiceURL') , $postfields, true, 3,'getLoginUrl'));
			}else{
			$result2=$this->updatepassword($username,$password,$currency); 
			$data = array(
            'cmd'		=> 'SigninGame',
            'vendorId'	=> Config::get($currency. '.wmc.vendorId'),
            'signature' => Config::get($currency. '.wmc.signature'),
			'user'		=> $username,
            'lang' 		=> 1,
			'password'	=> Crypt::decrypt($password),
			'isTest'	=> 0,
                );
			$result2=$this->updatepassword($username,$password,$currency); 
			
			$postfields=$data; 
			$result=json_decode($this-> curl(Config::get($currency . '.wmc.BaseServiceURL') , $postfields, true, 3,'getLoginUrl'));
			}
			if(isset($result) && $result->errorCode == '0'){
				
				return $result->result; 
				
			}		
			 return false;  
    }
	
	public function updatepassword($usernameup,$passwordup,$currency){
		
			$datapass 	= array(
            'cmd'		=> 'ChangePassword',
            'vendorId'	=> Config::get($currency. '.wmc.vendorId'),
            'signature' => Config::get($currency. '.wmc.signature'),
			'user'		=> $usernameup,
            'syslang' 	=> 1,
			'newpassword'=> Crypt::decrypt($passwordup),
             );
				
			$postfields=$datapass; 
			$result=json_decode($this-> curl(Config::get($currency . '.wmc.BaseServiceURL') , $postfields, true, 3,'getLoginUrl'));
			if(isset($result) && ($result->errorCode == '0'||$result->errorCode == '1')){
				
				return true; 
			}	
		return false; 
	}
	
	private function getPrdid(){
		  
		  $prdid = Cache::rememberForever('WMC_PRDID', function() {
						return Product::where( 'code' , '=' , 'WMC')->pluck('id');
				});
		  
		  return $prdid;
	
	}
	
    function curl($url, $postFields, $log = true, $timeout = 30, $method = '')
    {
      
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postFields);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);

        if ($log)
			$postfields=http_build_query($postFields); 
            App::insert_api_log(array('request' => $url . '?'.$postfields, 'return' => $response, 'method' => $method));

        return $response;

    }
	
	
	
	}
	
	





?>