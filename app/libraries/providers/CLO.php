<?php namespace App\libraries\providers;
use App\libraries\App;
use App\Models\Betpsbresult;
use App\Models\Account;
use App\Models\AccountProduct;
use Cache;
use Session;
use Config;
use Crypt;

//getLoginUrl
//doLogOut
//getBalance
//deposit
//withdraw
//updateStatus
//get4DResult

class CLO {
 
	public function createUser( $params = array() ){
		
                $password = Crypt::decrypt($params['password']);
                
		$postfields =   'token='.Config::get($params['currency'].'.clo.token').
                                '&lid='.$params['username'].
                                '&pwd='.$password.
                                '&unm='.$params['username'].
                                '&minb='.Config::get($params['currency'].'.clo.minb').
                                '&maxb='.Config::get($params['currency'].'.clo.maxb').
                                '&comm='.Config::get($params['currency'].'.clo.comm').
                                '&pay='.Config::get($params['currency'].'.clo.pay');

		$result = json_decode( App::curl( $postfields , Config::get($params['currency'].'.clo.url').'CreateAccount.aspx' , true, 5 , 'createUser') );

		if( isset($result->status) && ( $result->status == 1 || $result->message == 'save_completed' ) )
		{
                        $accObj = Account::find(Session::get('userid'));
                        $accObj->cloid = $result->userid;
                        $accObj->clo_token = $result->token;
                        $accObj->save();
                        
			Accountproduct::addAccountProduct($params['username'],'CLO','');
			return true;
		}
		
		return false;
		
	}
	
	public function getLoginUrl($username, $currency){
		
		if($username == '') 
                    return false;
                else
                    $accObj = Account::where( 'nickname' , '=' , $username  )->first();

		return Config::get($currency.'.clo.loginurl').'?lng=cn&token='.urlencode($accObj->clo_token);
		
	}

	public function getBalance($username,$currency){
                
                if($username == '') 
                    return false;
                else
                    $accObj = Account::where( 'nickname' , '=' , $username  )->first();
                
		$postfields =   'token='.Config::get($currency.'.clo.token').
                                '&uid='.$accObj->cloid;

		$result = json_decode( App::curl( $postfields , Config::get($currency.'.clo.url').'GetAccountBalance.aspx' , true, 10 , 'getBalance') );

		if( isset($result->status) && $result->status == 1 )
		{
			return $result->creditbalance;
		}
		
		return false;
		
	}
	
	public function deposit($username, $currency, $amount){
		
		if( $amount < 0 ) return false;
		
                if($username == '') 
                    return false;
                else
                    $accObj = Account::where( 'nickname' , '=' , $username  )->first();
                
		$postfields =   'token='.Config::get($currency.'.clo.token').
                                '&uid='.$accObj->cloid.
                                '&pay='.$amount;

		$result = json_decode( App::curl( $postfields , Config::get($currency.'.clo.url').'SetAccountDeposit.aspx' , true, 30 , 'doDeposit') );

		if( isset($result->status) && $result->status == 1 )
		{
			return true;
		}
		
		return false;
		
	}
		
	public function withdraw($username, $currency, $amount , $transID){
		
		if( $amount < 0 ) return false;
		
                if($username == '') 
                    return false;
                else
                    $accObj = Account::where( 'nickname' , '=' , $username  )->first();
                
		$postfields =   'token='.Config::get($currency.'.clo.token').
                                '&uid='.$accObj->cloid.
                                '&pay='.$amount;

		$result = json_decode( App::curl( $postfields , Config::get($currency.'.clo.url').'SetAccountWithdrawal.aspx' , true, 30 , 'doWithdrawal') );

		if( isset($result->status) && $result->status == 1 )
		{
			return true;
		}
		
		return false;
		
	}
	
	public function updateStatus($username,$status,$currency){
		
		$array['active'] 	= 'a';
		$array['close'] 	= 'c';
		$array['suspend']   = 's';
		
		$result = App::curlGET(Config::get($currency.'.psb.url').'?m=7&a='.Config::get($currency.'.psb.a').'&p='.Config::get($currency.'.psb.p').'&u='.Config::get($currency.'.psb.prefix').$username.'&st='.$array[$status],false);
		
		if( isset($result) && substr($result,0,2) == 'OK' )
		{
			return true;
		}
		
		return false;
		
	}
	
	public function get4DResult(){
		

		if (Cache::has('4d_result'))
		{
			return Cache::get('4d_result');
		}
		else
		{
			$value = Cache::rememberForever('4d_result', function()
			{
				return Betpsbresult::where( 'DrawDate' , '=' , Betpsbresult::orderBy('DrawDate','Desc')->pluck('DrawDate') )->get();
			});
		}
		
		return $value;
	}
 
	
   
}
?>