<?php namespace App\libraries\providers;

use App\libraries\App;
use App\Models\Accountproduct;
use App\Models\Account;
use App\Models\Product;
use Illuminate\Http\Request;
use Config;
use Crypt;
use Carbon\Carbon;
use Lang;
use Cache;
use DB;
use Session;


class OSG {

	public function getLoginUrl($params, $language_code = null){
		$lang = $this->getLanguageCode($language_code);
		
//		if (is_null($username)) {
//			// No username given, use public page URL.
//			return $data['login_url'] = Config::get($currency.'.osg.public_url').'?lang='.  $lang;
//		}
        
        $useragent = $_SERVER['HTTP_USER_AGENT'];
		if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4))) {
			$clientType = 2;
		} elseif ( stristr($useragent, 'Mozilla/5.0(iPad;') ) {
			$clientType = 1;
		} elseif (strstr($useragent, 'iPad')) {
			$clientType = 1;
		} else {
			$clientType = 0;
		}
		
		$username = Config::get($params['currency'].'.osg.prefix').'@'.strtolower($params['username']);
		$optId = Config::get($params['currency'].'.osg.optId');
		$dtNow = date('Y-m-d H:i:s');
		$signature = md5($username.$optId.'PlayerLogin'.$dtNow.Config::get($params['currency'].'.osg.key'));
		$postfields = '<?xml version="1.0"?><PlayerLogin>'.
			'<PlayerID>'.$username.'</PlayerID>'.
			'<PlayerPassword>'.Crypt::decrypt($params['password']).'</PlayerPassword>'.
			'<RequestDateTime>'.$dtNow.'</RequestDateTime>'.
			'<OperatorID>'.$optId.'</OperatorID>'.
			'<OperatorPassword>'.Config::get($params['currency'].'.osg.optPassword').'</OperatorPassword>'.
			'<Ip>'.$params['ip'].'</Ip>'.
			'<GameID>'.$params['gameid'].'</GameID>'.
			'<ClientType>'.$clientType.'</ClientType>'.
			'<Lang>'.$lang.'</Lang>'.
			'<Signature>'.$signature.'</Signature>'.
			'</PlayerLogin>';
		
        $result = App::curlXML( $postfields , Config::get($params['currency'].'.osg.url').'OPAPI/PlayerLogin', true, 5 , 'getLoginUrl', $this->getXmlHeader());
        
        if (str_contains($result, '<Url>') && !str_contains($result, '<Url><![CDATA')) {
            $result = preg_replace('/<Url>(.+)<\/Url>/', '<Url><![CDATA[$1]]></Url>', $result);
        }
        
		$result = simplexml_load_string( $result );
        
		if(isset($result->Status) && $result->Status == 200) {
            if ($language_code == 'cn') {
                return ((String) $result->Url);
            }
            
			return ((String) $result->Url);
		}
		
		return false;
	}
    
    public function getLobbyUrl($params, $language_code = null) {
        
		$lang = $this->getLanguageCode($language_code);
		$username = Config::get($params['currency'].'.osg.prefix').'@'.strtolower($params['username']);
		$optId = Config::get($params['currency'].'.osg.optId');
		$dtNow = date('Y-m-d H:i:s');
		$signature = md5($username.$optId.'GetToken'.$dtNow.Config::get($params['currency'].'.osg.key'));
		$postfields = '<?xml version="1.0"?><GetToken>'.
			'<PlayerID>'.$username.'</PlayerID>'.
			'<PlayerPassword>'.Crypt::decrypt($params['password']).'</PlayerPassword>'.
			'<OperatorID>'.$optId.'</OperatorID>'.
			'<OperatorPassword>'.Config::get($params['currency'].'.osg.optPassword').'</OperatorPassword>'.
			'<GLLang>'.$lang.'</GLLang>'.
            '<RequestDateTime>'.$dtNow.'</RequestDateTime>'.
			'<Signature>'.$signature.'</Signature>'.
			'</GetToken>';
        
        $result = App::curlXML( $postfields , Config::get($params['currency'].'.osg.lobby_url').'GLAPI/GetToken', true, 5 , 'getToken', $this->getXmlHeader());
        
        if (str_contains($result, '<Url>') && !str_contains($result, '<Url><![CDATA')) {
            $result = preg_replace('/<Url>(.+)<\/Url>/', '<Url><![CDATA[$1]]></Url>', $result);
        }
        
		$result = simplexml_load_string( $result );
        
		if(isset($result->Status) && $result->Status == 200) {
            if ($language_code == 'cn') {
                return ((String) $result->Url);
            }
            
			return ((String) $result->Url);
		}
		
		return false;
    }
	
	public function createUser( $params = array() ){
		
		date_default_timezone_set("Asia/Kuala_Lumpur");
		
        if (!array_key_exists('ip', $params)) {
            $params['ip'] = request()->ip();
        }
        
		$username = Config::get($params['currency'].'.osg.prefix').'@'.strtolower($params['username']);
		$optId = Config::get($params['currency'].'.osg.optId');
		$dtNow = date('Y-m-d H:i:s');
		$signature = md5($username.$optId.'CreatePlayer'.$dtNow.Config::get($params['currency'].'.osg.key'));
		$postfields = '<?xml version="1.0"?><CreatePlayer>'.
			'<PlayerID>'.$username.'</PlayerID>'.
			'<PlayerDisplayName>'.$params['username'].'</PlayerDisplayName>'.
			'<PlayerPassword>'.Crypt::decrypt($params['password']).'</PlayerPassword>'.
			'<PlayerCurrency>'.$params['currency'].'</PlayerCurrency>'.
			'<RequestDateTime>'.$dtNow.'</RequestDateTime>'.
			'<OperatorID>'.$optId.'</OperatorID>'.
			'<OperatorPassword>'.Config::get($params['currency'].'.osg.optPassword').'</OperatorPassword>'.
			'<Ip>'.$params['ip'].'</Ip>'.
			'<Signature>'.$signature.'</Signature>'.
			'</CreatePlayer>';
		
		$result = simplexml_load_string( App::curlXML( $postfields , Config::get($params['currency'].'.osg.url').'OPAPI/CreatePlayer', true, 3 , 'createUser', $this->getXmlHeader()) );
		
		if (isset($result->Status) && ($result->Status == 200 || $result->Status == 900603)) {
			// Error code 900603: username already exists.
			if( !Accountproduct::is_exist( $params['username'] ,'osg'))
			{
				Accountproduct::addAccountProduct($params['username'],'osg','');
			}

			return $result;
		}
		
		return false;
		
	}
	
	public function updatePassword($username, $password, $currency){

        $username = Config::get($currency.'.osg.prefix').'@'.strtolower($username);
        $optId = Config::get($currency.'.osg.optId');
        $dtNow = date('Y-m-d H:i:s');
        $signature = md5($username.$optId.'UpdatePlayerPassword'.$dtNow.Config::get($currency.'.osg.key'));
        $postfields = '<?xml version="1.0"?><UpdatePlayerPassword>'.
            '<PlayerID>'.$username.'</PlayerID>'.
            '<PlayerPassword>'.Crypt::decrypt($password).'</PlayerPassword>'.
            '<OperatorID>'.$optId.'</OperatorID>'.
            '<OperatorPassword>'.Config::get($currency.'.osg.optPassword').'</OperatorPassword>'.
            '<RequestDateTime>'.$dtNow.'</RequestDateTime>'.
            '<Signature>'.$signature.'</Signature>'.
            '</UpdatePlayerPassword>';

        $result = simplexml_load_string( App::curlXML( $postfields , Config::get($currency.'.osg.url').'OPAPI/UpdatePlayerPassword', true, 3 , 'updatePassword', $this->getXmlHeader()) );

		return (isset($result->Status) && ($result->Status == 200));
		
	}
	
	public function getBalance($username,$currency){
		
		$real_username = $username;
		
		$username = Config::get($currency.'.osg.prefix').'@'.strtolower($username);
		$optId = Config::get($currency.'.osg.optId');
		$dtNow = date('Y-m-d H:i:s');
		$signature = md5($username.$optId.'CheckBalance'.$dtNow.Config::get($currency.'.osg.key'));
		$postfields = '<?xml version="1.0"?><CheckBalance>'.
			'<PlayerID>'.$username.'</PlayerID>'.
			'<OperatorID>'.$optId.'</OperatorID>'.
			'<OperatorPassword>'.Config::get($currency.'.osg.optPassword').'</OperatorPassword>'.
			'<RequestDateTime>'.$dtNow.'</RequestDateTime>'.
			'<Signature>'.$signature.'</Signature>'.
			'</CheckBalance>';
		
		$result = simplexml_load_string( App::curlXML( $postfields , Config::get($currency.'.osg.url').'OPAPI/CheckBalance', true, 5 , 'getBalance', $this->getXmlHeader()) );
		
		if (isset($result->PlayerBalance) && (isset($result->Status) && $result->Status == 200)) {
			
			$dateTimeNow    = Carbon::now();
			$dateTimeNowStr = $dateTimeNow->toDateTimeString();
			
			if( Session::has('userid') ){
				DB::statement('call insert_cashbalance('.Session::get('userid').',"'.Session::get('username').'","'.$currency.'",'.$this->getPrdid().','.$result->PlayerBalance.',"'.$dateTimeNowStr.'","'.$dateTimeNowStr.'")');
			}else{
				DB::statement('call insert_cashbalance('.Account::whereNickname($real_username)->pluck('id').',"'.$real_username.'","'.$currency.'",'.$this->getPrdid().','.$result->PlayerBalance.',"'.$dateTimeNowStr.'","'.$dateTimeNowStr.'")');
			}
			
			if( $currency == 'IDR' || $currency == 'VND')
            {
                return $result->PlayerBalance / 1000;
            }
            else
            {
                return $result->PlayerBalance;
            }
		}
		
		return false;
		
	}
	
	private function getPrdid(){
		
		$prdid = Cache::rememberForever('OSG_PRDID', function() {
                return Product::where( 'code' , '=' , 'OSG')->pluck('id');
        });
		
		return $prdid;
	}
	
	public function deposit($username,$currency,$amount,$transid){
		
		if( $amount < 0 ) return false;
		
		return $this->transferCredit( $username, $currency, $transid, '0', $amount);
		
	}
		
	public function withdraw($username,$currency,$amount,$transid){

		if( $amount < 0 ) return false;
		
		return $this->transferCredit( $username, $currency, $transid, '1', $amount);
	}
	
	private function transferCredit( $username, $currency, $billno, $type, $credit){
		
	 /* 	if( $currency == 'IDR' || $currency == 'VND' )
		{
			$credit = $credit / 1000;
		} */
		
		$action = '';

		if ($type == 0) {
			$action = 'Deposit';
		} elseif ($type == 1) {
			$action = 'Withdrawal';
		} else {
			// Unknown type.
			return false;
		}
		
		$username = Config::get($currency.'.osg.prefix').'@'.strtolower($username);
		$optId = Config::get($currency.'.osg.optId');
		$dtNow = date('Y-m-d H:i:s');
		$signature = md5($username.$optId.$credit.$action.$dtNow.Config::get($currency.'.osg.key'));
		$postfields = '<?xml version="1.0"?><'.$action.'>'.
			'<PlayerID>'.$username.'</PlayerID>'.
			'<Amount>'.$credit.'</Amount>'.
			'<RequestDateTime>'.$dtNow.'</RequestDateTime>'.
			'<OperatorID>'.$optId.'</OperatorID>'.
			'<OperatorPassword>'.Config::get($currency.'.osg.optPassword').'</OperatorPassword>'.
			'<ThirdPartyTransacID>'.str_pad($billno, 12, "0", STR_PAD_LEFT).'</ThirdPartyTransacID>'.
			'<Signature>'.$signature.'</Signature>'.
			'</'.$action.'>';
		
		$result = simplexml_load_string( App::curlXML( $postfields , Config::get($currency.'.osg.url').'OPAPI/'.$action, true, 10 , 'transferCredit', $this->getXmlHeader()) );
		
		if (isset($result->Status) && $result->Status == 200) {
			return true;
		}

		return false;
	}
    
    public function gameList($category, $currency, $langguage = 'en') {
        
		$optId = Config::get($currency.'.osg.optId');
		$dtNow = date('Y-m-d H:i:s');
		$signature = md5($optId.'GetGameList'.$dtNow.Config::get($currency.'.osg.key'));
		$postfields = '<?xml version="1.0"?><GetGameList>'.
			'<OperatorID>'.$optId.'</OperatorID>'.
			'<OperatorPassword>'.Config::get($currency.'.osg.optPassword').'</OperatorPassword>'.
			'<RequestDateTime>'.$dtNow.'</RequestDateTime>'.
			'<Signature>'.$signature.'</Signature>'.
			'</GetGameList>';
        
        if(is_null($category)) {
            $category = '';
        }
		
		$result = simplexml_load_string( App::curlXML( $postfields , Config::get($currency.'.osg.url').'OPAPI/GetGameList', true, 5 , 'gameList', $this->getXmlHeader()) );

		$games = array();
        if (isset($result->Status) && $result->Status == '200') {
            foreach ($result->Game as $game) {
                $doAdd = true;
                
                if ($category != '') {
                    $doAdd = (((String) $game->TypeID) == $category);
//                    dd($category);
                }
                
                if ($doAdd) {
                    $arr = array(
                        'gameId' => (String) $game->GameID,
                        'typeId' => (String) $game->TypeID,
                        'gameName' => (String) ($langguage == 'cn' ? $game->CGameName : $game->GameName),
                        'imageUrl' => (String) ($langguage == 'cn' ? $game->CImageUrl : $game->ImageUrl),
                    );
                    
                    $games[$arr['gameId']] = $arr;
                }
            }
        }
        
		return $games;
    }
	
	private function getXmlHeader()
	{
		return array(
		    'Accept: text/xml,application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5',
		    'Cache-Control: max-age=0',
		    'Connection: keep-alive',
		    'Keep-Alive: 300',
		    'Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7',
		    'Accept-Language: en-us,en;q=0.5',
		    'Pragma: ',
		    'DataType: XML',
//		    'Content-Type: text/xml',
		    'Content-Type: application/x-www-form-urlencoded',
		);
	}
    
    public function getLanguageCode($language_code) {
		$languages = array(
		    'cn' => 'zh-cn',
		    'en' => 'en-us',
		);
		$lang = $languages['en']; // Default language.
		
		if (!is_null($language_code) && array_key_exists($language_code, $languages)) {
			$lang = $languages[$language_code];
		}
        
        return $lang;
    }
	
}
?>