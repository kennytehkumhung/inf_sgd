<?php namespace App\libraries\providers;

use App\libraries\App;
use App\Models\Accountproduct;
use App\Models\Account;
use Config;
//getLoginUrl
//getBalance
//deposit
//withdraw
//createUser


class SBOF {

	
	public function createUser( $params = array() ){
		//date_default_timezone_set("US/Eastern");
		$date = date('Ymdhis');
		
                $username = $this->getUID($params['username']);
                
                if (is_null($username)) {
			// No username given, use public page URL.
			return $data['login_url'] = Config::get($params['currency'].'.sbof.public_url');
		}
                
		
		
		$postfields = 'agtid='.Config::get($params['currency'].'.sbof.agtid').
					  '&uid='.$username.
					  '&timestamp='.$date.
					  '&sign='.MD5(Config::get($params['currency'].'.sbof.Key').Config::get($params['currency'].'.sbof.agtid').$username.$date);
					


		$result = simplexml_load_string( App::curlGET( Config::get($params['currency'].'.sbof.url').'op=usr_author&'.$postfields  , true, 5 , 'createUser') );
		
		if( isset($result->error_code) && $result->error_code == "0" )
		{
			if( !Accountproduct::is_exist( $params['username'] ,'SBOF'))
			{
				Accountproduct::addAccountProduct($params['username'],'SBOF','');
			}
			return $result->result;
		}
		
		return false;
		
	}
	
	public function getBalance($username,$currency){
		
		$date = date('Ymdhis');
		
		$username = $this->getUID($username);
		
		$postfields = 'agtid='.Config::get($currency.'.sbof.agtid').
					  '&uid='.$username.
					  '&timestamp='.$date.
					  '&sign='.MD5(Config::get($currency.'.sbof.Key').Config::get($currency.'.sbof.agtid').$username.$date).
					  '&signparams='.Config::get($currency.'.sbof.Key').Config::get($currency.'.sbof.agtid').$username.$date;

		$result = simplexml_load_string( App::curlGET( Config::get($currency.'.sbof.url').'op=usr_profile&'.$postfields , true, 10 , 'getBalance') );

		if( isset($result->balance) )
		{
			return $result->balance;
		}
		
		return false;
		
	}
	
	public function deposit($username,$currency,$amount,$transid){
		
		if( $amount < 0 ) return false;
		
		return $this->transferCredit( $username, $currency, $transid, 'usr_wallet_opt_deposit', $amount);
		
	}
		
	public function withdraw($username,$currency,$amount,$transid){

		if( $amount < 0 ) return false;
		
		return $this->transferCredit( $username, $currency, $transid, 'usr_wallet_opt_withdraw', $amount);
	}
	
	private function transferCredit( $username, $currency, $billno, $type, $credit){
		
	 	if( $currency == 'IDR' || $currency == 'VND' )
		{
			$credit = $credit / 1000;
		}

		$date = date('Ymdhis');
		
		$username = $this->getUID($username);
		
		$postfields = 'agtid='.Config::get($currency.'.sbof.agtid').
					  '&uid='.$username.
					  '&timestamp='.$date.
					  '&amount='.$credit.
					  '&sign='.MD5(Config::get($currency.'.sbof.Key').Config::get($currency.'.sbof.agtid').$username.$credit.$date);

		$result = simplexml_load_string( App::curlGET( Config::get($currency.'.sbof.url').'op='. $type.'&'.$postfields , true, 30 , 'transferCredit') );

		if( isset($result->error_code) && $result->error_code == "0" )
		{
			return true;
		}
		
		return false;
	}
	
	private function getUID($nickname){
		
		return Account::where( 'nickname' , '=' , $nickname )->pluck('id');
	}

	

	

	
   
}
?>