<?php namespace App\libraries\providers;

use App\libraries\App;
use App\Models\Accountproduct;
use Config;

//getBalance
//deposit
//withdraw
//createUser



class CT8 {
 
	public function createUser( $params = array() ){
		
		$token = Md5( Config::get($params['currency'].'.ct8.securitykey') . $params['currency'] . Config::get($params['currency'].'.ct8.agent') . strtolower($params['username']) . Config::get($params['currency'].'.ct8.language_code') . Config::get($params['currency'].'.ct8.maxwin') );
		
		$postfields = 'action=1'.
					  '&agent='.Config::get($params['currency'].'.ct8.agent').
					  '&username='.strtolower($params['username']).
					  '&param='.Config::get($params['currency'].'.ct8.language_code'). '|' . $params['currency'] . '|' . Config::get($params['currency'].'.ct8.maxwin') . '|'.Config::get($params['currency'].'.ct8.betlimit').'|' . $token;

		$result = simplexml_load_string( App::curl( $postfields , Config::get($params['currency'].'.ct8.url') , true, 3 , 'createUser')  );

		if( isset($result->code) && $result->code == '0' )
		{
			if( !Accountproduct::is_exist( $params['username'] , 'CT8' ) )
			{
				Accountproduct::addAccountProduct($params['username'],'CT8','');
			}
			return $result->result;
		}
		
		return false;
		
	}

	
	public function getBalance($username,$currency){
		
		$postfields = 'action=2'.
					  '&agent='.Config::get($currency.'.ct8.agent').
					  '&username='.$username.
					  '&param=';

		$result = simplexml_load_string( App::curl( $postfields , Config::get($currency.'.ct8.url') , true, 5 , 'getBalance') );
		
		if( isset($result->code) && $result->code == '0' )
		{
			return $result->result->balance;
		}
		
		return false;
		
	}

	public function deposit($username,$currency,$amount,$transid){
		
		if( $amount < 0 ) return false;

		return $this->transferCredit( $username, $currency, $transid, '3', $amount);
		
	}
		
	public function withdraw($username,$currency,$amount,$transid){
		
		if( $amount < 0 ) return false;
		
		return $this->transferCredit( $username, $currency, $transid, '4', $amount);
		
	}
	

	private function transferCredit( $username, $currency, $billno, $direction, $credit){
			
		$token = Md5( Config::get($currency.'.ct8.securitykey') . $credit . Config::get($currency.'.ct8.agent') . strtolower($username) . $billno  );
		
		$postfields = 'action='. $direction.
					  '&agent='.Config::get($currency.'.ct8.agent').
					  '&username='.strtolower($username).
					  '&param=' . $billno . '|' . $credit . '||'  .  $token;
		
		$result = simplexml_load_string( App::curl( $postfields , Config::get($currency.'.ct8.url') , true, 10 , 'transferCredit') );
		
		if( isset($result->code) && $result->code == '0' )
		{
			return true;
		}
		
		return false;
	
	}
	

	
   
}
?>