<?php namespace App\libraries\providers;

use App\libraries\App;
use App\Models\Account;
use App\Models\AccountProduct;
use App\Models\Product;
use Config;
use Carbon\Carbon;
use Cache;
use Crypt;
use DB;
use Session;

//getLoginUrl
//getBalance
//deposit
//withdraw

// API DOC: https://github.com/animatorx999/lottoAPI/wiki

class ILT
{

    public function createUser($params = array())
    {
        $password = $params['password'];

        if (strlen($password) > 18) {
            // Default password length = 12-18
            // If password is longer means password is encrypted.
            $password = Crypt::decrypt($password);
        }

        $password = substr($password, 0, 12);

        $data = array(
            'key' => $this->getSignature($params['currency'], $params['username']),
            'com' => Config::get($params['currency'] . '.ilt.code'),
            'uid' => $params['username'],
            'pass' => $password,
        );

        $result = json_decode(App::curl(http_build_query($data), Config::get($params['currency'] . '.ilt.url_api') . '/CreatePlayer', true, 5, 'createUser'));

        /*
         * ERR 2002: Account or password invalid length, must >6
         * ERR 2004: Account or password invalid length, must >6
         * ERR 2012: Duplicate username
         * ERR 2018: Error in commission setting, contact us
         * ERR 9996: Wrong service code
         * ERR 9997: Necessary param not exist
         * ERR 9998: Invalid action
         * ERR 9999: Invalid sign key
         * ERR 8888: Under Maintenance
         */
        if (isset($result->status) && ($result->status == 0 || $result->status == 2012)) {
            $this->updatePassword($params['username'], $params['password'], $params['currency']);
            Accountproduct::addAccountProduct($params['username'],'ILT','');

            return true;
        }

        return false;
    }

    public function getLoginUrl($username, $currency, $password)
    {
        $data = array(
            'key' => $this->getSignature($currency, $username),
            'com' => Config::get($currency . '.ilt.code'),
            'uid' => $username,
        );

        $result = json_decode(App::curl(http_build_query($data), Config::get($currency . '.ilt.url_api') . '/GameToken', true, 5, 'getLoginUrl'));

        /*
         * ERR 1011: Account or password invalid
         * ERR 1014: Account access denied
         * ERR 1016: Account suspended
         * ERR 2001: Username not exist
         * ERR 9996: Wrong service code
         * ERR 9997: Necessary param not exist
         * ERR 9998: Invalid action
         * ERR 9999: Invalid sign key
         * ERR 8888: Under Maintenance
         */
        if (isset($result->status) && $result->status == 0 && $result->token) {
            return Config::get($currency . '.ilt.url_game') . '?token=' . $result->token . '&leaguetype=1';
        }

        return false;
    }

    public function updatePassword($username, $password, $currency)
    {
        if (strlen($password) > 18) {
            $password = Crypt::decrypt($password);
        }

        $password = substr($password, 0, 12);

        $data = array(
            'key' => $this->getSignature($currency, $username),
            'com' => Config::get($currency . '.ilt.code'),
            'uid' => $username,
            'newPass' => $password,
        );

        $result = json_decode(App::curl(http_build_query($data), Config::get($currency . '.ilt.url_api') . '/ChangePassword', true, 5, 'updatePassword'));

        /*
         * ERR 2005: Password length error, MUST >=6 AND <=12
         * ERR 2006: Update Fail
         * ERR 2001: Username not exist
         * ERR 9996: Wrong service code
         * ERR 9997: Necessary param not exist
         * ERR 9998: Invalid action
         * ERR 9999: Invalid sign key
         * ERR 8888: Under Maintenance
         */
        if (isset($result->status) && $result->status == 0) {
            return true;
        }

        return false;
    }

    public function getBalance($username, $currency)
    {
        $data = array(
            'key' => $this->getSignature($currency, $username),
            'com' => Config::get($currency . '.ilt.code'),
            'uid' => $username,
        );

        $result = json_decode(App::curl(http_build_query($data), Config::get($currency . '.ilt.url_api') . '/GetBalance', true, 5, 'getBalance'));

        /*
         * ERR 2001: Username not exist
         * ERR 9996: Wrong service code
         * ERR 9997: Necessary param not exist
         * ERR 9998: Invalid action
         * ERR 9999: Invalid sign key
         * ERR 8888: Under Maintenance
         */
        if (isset($result->status) && $result->status == 0 && isset($result->balance)) {
            $balance = $result->balance;
            $dateTimeNow = Carbon::now();
            $dateTimeNowStr = $dateTimeNow->toDateTimeString();

            if (Session::has('userid')) {
                DB::statement('call insert_cashbalance(' . Session::get('userid') . ',"' . Session::get('username') . '","' . $currency . '",' . $this->getPrdid() . ',' . $balance . ',"' . $dateTimeNowStr . '","' . $dateTimeNowStr . '")');
            } else {
                DB::statement('call insert_cashbalance(' . Account::whereNickname($username)->whereCrccode($currency)->pluck('id') . ',"' . $username . '","' . $currency . '",' . $this->getPrdid() . ',' . $balance . ',"' . $dateTimeNowStr . '","' . $dateTimeNowStr . '")');
            }

            return $balance;
        }

        return false;
    }

    private function getPrdid()
    {
        $prdid = Cache::rememberForever('ILT_PRDID', function () {
            return Product::where('code', '=', 'ILT')->pluck('id');
        });

        return $prdid;
    }

    public function deposit($username, $currency, $amount, $transID)
    {
        if ($amount < 0) return false;

        return $this->transferCredit($username, $currency, $transID, 0, $amount);
    }

    public function withdraw($username, $currency, $amount, $transID)
    {
        if ($amount < 0) return false;

        return $this->transferCredit($username, $currency, $transID, 1, $amount);
    }

    private function transferCredit($username, $currency, $transID, $type, $amount)
    {
        $data = array(
            'key' => $this->getSignature($currency, $username),
            'com' => Config::get($currency . '.ilt.code'),
            'uid' => $username,
            'type' => $type + 1, // 1: deposit; 2: withdraw
            'amt' => $amount,
            'orderid' => $transID,
        );

        $result = json_decode(App::curl(http_build_query($data), Config::get($currency . '.ilt.url_api') . '/TransferCl', true, 5, 'transferCredit'));

        /*
         * ERR 2001: Username not exist
         * ERR 3014: Order id duplicate
         * ERR 3016: Upline insufficient fund
         * ERR 9996: Wrong service code
         * ERR 9997: Necessary param not exist
         * ERR 9998: Invalid action
         * ERR 9999: Invalid sign key
         * ERR 8888: Under Maintenance
         */
        if (isset($result->status) && $result->status == 0) {
            return true;
        }

        return false;

    }

    public function getSignature($currency, $username)
    {
        return md5(Config::get($currency . '.ilt.agent') . $username . Config::get($currency . '.ilt.password') . Config::get($currency . '.ilt.server_ip'));
    }
}
