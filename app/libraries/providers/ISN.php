<?php namespace App\libraries\providers;

use App\libraries\App;
use App\Models\Accountproduct;
use Config;
//getLoginUrl
//getBalance
//deposit
//withdraw
//createUser


class ISN {

	
	public function createUser( $params = array() ){

        $this->deposit( $params['username'], $params['currency'], 0.01, date('U').mt_rand(1111,9999));
        $this->withdraw($params['username'], $params['currency'], 0.01, date('U').mt_rand(1111,9999));

		if (!Accountproduct::is_exist($params['username'],'ISN')) {
			Accountproduct::addAccountProduct($params['username'],'ISN','');
		}

		return true;
	}
    
	public function getLoginUrl($params, $language = 'en'){

	    $accId = Config::get($params['currency'].'.isn.prefix').strtoupper($params['username']);
        return Config::get($params['currency'].'.isn.url').'?acctId='.$accId.'&language='.$language.'&token='.md5(date('U').mt_rand(1111,9999));
    }

	public function getBalance($username,$currency){

        $accId = Config::get($currency.'.isn.prefix').strtoupper($username);

		$postfields = '<GetAcctInfoRequest>'.
			'<serialNo>'.date('U').mt_rand(1111,9999).'</serialNo>'.
			'<merchantCode>'.Config::get($currency.'.isn.merchant_code').'</merchantCode>'.
			'<acctId>'.strtoupper($accId).'</acctId>'.
			'</GetAcctInfoRequest>';
		
		if(Config::get($currency.'.isn.api_url') == '')
		{
			return false;
		}

		$result = simplexml_load_string( $this->curlXML( $postfields , Config::get($currency.'.isn.api_url') , true, 10 , 'getBalance', $this->getXmlHeader('getAcctInfo')) );

		if( isset($result->code) && $result->code == '0' && isset($result->list[0]->AcctInfo->balance) )
		{
			return (float) $result->list[0]->AcctInfo->balance;
		}
		
		return false;
		
	}
	
	public function deposit($username,$currency,$amount,$transid){
		
		if( $amount < 0 ) return false;
		
		return $this->transferCredit( $username, $currency, $transid, 'deposit', $amount);
		
	}
		
	public function withdraw($username,$currency,$amount,$transid){

		if( $amount < 0 ) return false;
		
		return $this->transferCredit( $username, $currency, $transid, 'withdraw', $amount);
	}
	
	private function transferCredit( $username, $currency, $billno, $type, $credit){
		
		if( $currency == 'IDR' || $currency == 'VND' )
		{
			$credit = $credit / 1000;
		}

        if ($type == 'deposit') {
            $interface = 'deposit';
            $request = 'DepositRequest';
        } elseif ($type == 'withdraw') {
            $interface = 'withdraw';
            $request = 'WithdrawRequest';
        } else {
            return false;
        }

        $accId = Config::get($currency.'.isn.prefix').strtoupper($username);
        $postfields = "<" . $request . ">" .
            "<serialNo>" . date('U').mt_rand(1111,9999) . "</serialNo>" .
            "<merchantCode>" . Config::get($currency.'.isn.merchant_code') . "</merchantCode>" .
            "<acctId>" . $accId . "</acctId>" .
		    "<currency>" . $currency . "</currency>" .
		    "<amount>" . $credit . "</amount>" .
		    "</" . $request . ">";

        $result = simplexml_load_string( $this->curlXML( $postfields , Config::get($currency.'.isn.api_url') , true, 10 , 'transferCredit', $this->getXmlHeader($interface)) );

		if( isset($result->code) && $result->code == '0' )
		{
			return true;
		}
		
		return false;
	}

    private function getXmlHeader($interface)
    {
        return array(
            'Accept:text/xml,application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5',
            'Cache-Control:max-age=0',
            'Connection:keep-alive',
            'Keep-Alive:300',
            'Accept-Charset:ISO-8859-1,utf-8;q=0.7,*;q=0.7',
            'Accept-Language:en-us,en;q=0.5',
            'Pragma:',
            'API:'.$interface,
            'DataType:XML',
            'Content-Type:text/xml',
        );
    }

    private function curlXML( $postfields, $url , $log = true , $timeout = 30 , $method = '', $header = false) {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        if( isset($_SERVER['HTTP_USER_AGENT'] ))
        {
            curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
        }
        $response = curl_exec($ch);
        curl_close($ch);

        if($log)
            App::insert_api_log( array( 'request' => $url.'?'.$postfields , 'return' => $response , 'method' => $method ) );

        return $response;
    }
}
?>