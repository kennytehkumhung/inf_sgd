<?php namespace App\libraries\providers;

use App\libraries\App;
use App\Models\Account;
use App\Models\AccountProduct;
use App\Models\Product;
use Config;
use Carbon\Carbon;
use Lang;
use Cache;
use DB;
use Session;
use Crypt;
//getLoginUrl
//getBalance
//deposit
//withdraw
//createUser


class SKY {
    
    public function getLoginUrl($username, $currency, $gameCode) {
        
        $data = array(
            'agid' => Config::get($currency . '.sky.agid'),
            'game_code' => $gameCode,
            'username' => $this->getUserIdByUsername($username, $currency),
        );
        
		$postfields = http_build_query($this->Signature_Genarate($data, Config::get($currency.'.sky.key')));

		$result = json_decode($this->curlGET(Config::get($currency.'.sky.url').'api/wsv1_0/user_play_game?'.$postfields, true, 5 , 'getLoginUrl'));
		
		if( isset($result->status) && $result->status == 'OK' )
		{
			return $result->url;
		}
		
		return false;
    }

    public function getLoginSession($params = array()) {

        if (!array_key_exists('enpassword', $params)) {
            $accObj = Account::find($params['userid']);

            $params['enpassword'] = $accObj->enpassword;
        }

        $data = array(
            'agid' => Config::get($params['currency'] . '.sky.agid'),
            'username' => $this->formatUserId($params['userid']),
            'password' => Crypt::decrypt($params['password']),
        );

        $postfields = http_build_query($this->Signature_Genarate($data, Config::get($params['currency'].'.sky.key')));

        $result = json_decode($this->curlGET(Config::get($params['currency'].'.sky.url').'api/wsv1_0/user_login?'.$postfields, true, 5 , 'getLoginSession'));

        if( isset($result->status) && $result->status == 'OK' )
        {
            return $result->session;
        }

        return false;
    }

	public function createUser( $params = array() ){
		
        if (!array_key_exists('enpassword', $params)) {
            $accObj = Account::find($params['userid']);
            
            $params['enpassword'] = $accObj->enpassword;
        }
        
        $data = array(
            'agid' => Config::get($params['currency'] . '.sky.agid'),
            'username' => $this->formatUserId($params['userid']),
            'password' => Crypt::decrypt($params['password']),
            'lang' => 'eng'
        );
        
		$postfields = http_build_query($this->Signature_Genarate($data, Config::get($params['currency'].'.sky.key')));

		$result = json_decode($this->curlGET(Config::get($params['currency'].'.sky.url').'api/wsv1_0/user_register?'.$postfields, true, 3 , 'createUser'));
		
		if( isset($result->status) && $result->status == 'OK' )
		{
            $this->updatePassword($params);
			Accountproduct::addAccountProduct($params['username'],'SKY','');
			return true;
		} elseif ( isset($result->error_code) && $result->error_code == 'USER_EXIST' ) {
            $this->updatePassword($params);
            Accountproduct::addAccountProduct($params['username'],'SKY','');
            return true;
        }
		
		return false;
		
	}
	
	public function updatePassword( $params = array() ){
		
        $data = array(
            'agid' => Config::get($params['currency'] . '.sky.agid'),
            'username' => $this->formatUserId($params['userid']),
            'password' => Crypt::decrypt($params['password']),
        );
        
		$postfields = http_build_query($this->Signature_Genarate($data, Config::get($params['currency'].'.sky.key')));

		$result = json_decode($this->curlGET(Config::get($params['currency'].'.sky.url').'api/wsv1_0/user_reset_password?'.$postfields, true, 1 , 'updatePassword'));
		
		if( isset($result->status) && $result->status == 'OK' )
		{
			return true;
		}
		
        return false;
		
	}
	
	public function getBalance($username, $currency){
		
		$real_username = $username;
		
        $data = array(
            'agid'      => Config::get($currency.'.sky.agid'),
            'username'  => $this->getUserIdByUsername($username, $currency),
        );
        
		$postfields = http_build_query($this->Signature_Genarate($data, Config::get($currency.'.sky.key')));

		$result = json_decode($this->curlGET(Config::get($currency.'.sky.url').'api/wsv1_0/user_detail?'.$postfields, true, 5 , 'getBalance'));
		
		if( isset($result->status) && $result->status == 'OK' ) {
			
			$dateTimeNow    = Carbon::now();
			$dateTimeNowStr = $dateTimeNow->toDateTimeString();
				
			if( Session::has('userid') ){
				DB::statement('call insert_cashbalance('.Session::get('userid').',"'.Session::get('username').'","'.$currency.'",'.$this->getPrdid().','.$result->balance.',"'.$dateTimeNowStr.'","'.$dateTimeNowStr.'")');
			}else{
				DB::statement('call insert_cashbalance('.Account::whereNickname($real_username)->pluck('id').',"'.$real_username.'","'.$currency.'",'.$this->getPrdid().','.$result->balance.',"'.$dateTimeNowStr.'","'.$dateTimeNowStr.'")');
			}
			
			
            if( $currency == 'IDR' || $currency == 'VND')
            {
				
                return floatval($result->balance) / 1000;
            }
		    
		    return $result->balance;
		}
		
		return false;
		
	}
	
	private function getPrdid(){
		
		$prdid = Cache::rememberForever('SKY_PRDID', function() {
                return Product::where( 'code' , '=' , 'SKY')->pluck('id');
        });
		
		return $prdid;
	}
	
	public function deposit($username,$currency,$amount,$transid){
		
		if( $amount < 0 ) return false;
		
		return $this->transferCredit( $username, $currency, $transid, 'deposit', $amount);
		
	}
		
	public function withdraw($username,$currency,$amount,$transid){

		if( $amount < 0 ) return false;
		
		return $this->transferCredit( $username, $currency, $transid, 'withdraw', $amount);
	}
	
	private function transferCredit( $username, $currency, $billno, $type, $credit){
		
//		if( $currency == 'IDR' || $currency == 'VND' )
//		{
//			$credit = $credit / 1000;
//		}
        
        if ($type == 'withdraw') {
            $credit = $credit * -1;
        }
        
        $data = array(
            'agid'      => Config::get($currency.'.sky.agid'),
            'amount'    => $credit,
            'username'  => $this->getUserIdByUsername($username, $currency),
            'orderid'   => $billno,
        );
		
		$postfields = http_build_query($this->Signature_Genarate($data, Config::get($currency.'.sky.key')));

		$result = json_decode($this->curlGET(Config::get($currency.'.sky.url').'api/wsv1_0/user_transfer?'.$postfields, true, 5 , 'transferCredit'));
		
		if( isset($result->status) && $result->status == 'OK' ) {
			return true;
		}
		
		return false;
	}

	public function gameList($currency, $type = 'all'){
		
        $data = array(
            'agid'      => Config::get($currency.'.sky.agid'),
        );
		
		$postfields = http_build_query($this->Signature_Genarate($data, Config::get($currency.'.sky.key')));

		$result = json_decode($this->curlGET(Config::get($currency.'.sky.url').'api/wsv1_0/user_game_list?'.$postfields, true, 5 , 'gameList'));

		$games = array();

		// Define game rank in list.
        $gameRanks = array(
            'runlight_monkeyking' => 1, 'slot_3kingdoms' => 2, 'slot_dolphinreef' => 3, 'race_thunderbolt' => 4,
            'race_horse' => 5, 'slot_fadacai' => 6, 'slot_jinpingmei' => 7, 'slot_avengers' => 8, 'race_frogjump' => 9,
            'slot_panthermoon' => 10, 'slot_safariheat' => 11, 'fishing_haiwang2_multiplayer' => 12, 'poker_bigshark' => 13
        );

        foreach($result as $key => $val) {
            //$arr = collect($val);
            if(is_array($val)) {
				foreach($val as $data){
		        	if ($data->game_code != '' || $data->game_code != null) {
		                $skip = ($type != 'all' && $type != $data->game_type);

		                if ($data->game_code == 'fishing_haiwang2_multiplayer') {
		                    // Skip problematic game.
		                    $skip = true;
			            }
		                
		                if (!$skip) {
		                    $imgUrl = Config::get('setting.front_path') . '/img/sky/' . $data->game_code . '.png';
		                    
		                    if (!file_exists(public_path($imgUrl))) {
		                        $imgUrl = Config::get('setting.front_path') . '/img/sky/slot_fadacai.png';
		                    }

		                    $code = $data->game_code;
		                    $gm = array(
		                        'game_code' => $code,
		                        'game_name' => $data->game_name,
		                        'game_type' => $data->game_type,
		                        'img_url' => asset($imgUrl),
		                        'rank' => (array_key_exists($code, $gameRanks) ? $gameRanks[$code] : 99),
		                    );
		                    
		                    $games[$data->game_code] = $gm;
			            }
			        }
				}
			}
	    }

        usort($games, function ($a, $b) {
            return $a['rank'] > $b['rank'];
        });
        
		return $games;
		
	}
    
    private function getUserIdByUsername($username, $currency) {
        
        $accObj = Account::where('nickname', '=', $username)
            ->where('crccode', '=', $currency)
            ->where('status', '=', 1)
            ->select(['id'])
            ->first();
        
        return $this->formatUserId($accObj->id);
        
    }
    
    function formatUserId($userId) {
        if (strlen($userId) < 9) {
            return str_pad($userId, 9, '0', STR_PAD_LEFT);
        }
        
        return $userId;
    }
	
	function Signature_Genarate($Params,$privateKey = false)
	{
		if(!empty($Params['signature']))
		{
			unset($Params['signature']);
		}
		ksort($Params);
		
		$Params['signature'] = sha1(implode("", $Params) . $privateKey);
		return $Params;
	}
	
	function Signature_Verify($Params , $privateKey = false)
	{
		if(!is_array($Params) || !$privateKey)
		{
			return false;
		}
		$CSignature = '';
		
		if(!empty($Params['signature']))	
		{
			$CSignature = $Params['signature'];
			unset($Params['signature']);
		}
		
		ksort($Params);
		
		$Signature = sha1(implode("", $Params) . $privateKey);
		
		return ($Signature === $CSignature) ? true : false;
	}

    function curlGET($url , $log = true, $timeout = 30 , $method = ''){

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, str_replace(' ' , '%20', $url));
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $response = curl_exec($ch);
        curl_close($ch);

        if($log)
            App::insert_api_log( array( 'request' => $url , 'return' => $response , 'method' => $method ) );

        return $response;
    }
	

	
   
}
