<?php namespace App\libraries\providers;

use App\libraries\App;
use App\Models\AccountProduct;
use App\Models\Betaggcategory;
use App\Models\Product;
use App\Models\Account;
use Config;
use Lang;
use Carbon\Carbon;
use Cache;
use DB;
use Session;
//getLoginUrl
//getBalance
//deposit
//withdraw
//createUser


class AGG {

	public function getLoginUrl($username, $currency, $language_code = 'en' , $dm, $gameType = 0){
		
		$lang['en'] = '3';
		$lang['cn'] = '1';
		$lang['vn'] = '8';
		$lang['id'] = '11';
		$lang['th'] = '6';

		if ($gameType == 0) {
            $useragent = $_SERVER['HTTP_USER_AGENT'];
            if (preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i', $useragent) || preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', substr($useragent, 0, 4))) {
                $gameType = 11; // AGIN HTML5 lobby.
            }
        }
		
		$postfields = 'vendor_id='.Config::get($currency.'.agg.vendor_id').
					  '&operator_id='.Config::get($currency.'.agg.operator_id').
					  '&user_id='.$username.
					  '&currency='.strtoupper($currency).
					  '&game_type='.$gameType.
					  '&lang='.$lang[$language_code].
					  '&dm='.$dm.
					  '&odd_type='.Config::get($currency.'.agg.odd_type');

		if ($gameType == 11) {
		    $postfields .= '&mh5=n';
        }

		$result = json_decode( App::curl( $postfields , Config::get($currency.'.agg.url').'forwardGame' , true , 5) );

		if( isset($result) && $result->error_code == '0' )
		{
			return $result->url;
		}
		
		return false;
		
	}

    public function updateStatus($username, $status, $currency){

	    $statusOptions = array(
	        '0' => 'suspend',
            '1' => 'active'
        );

        $postfields = 'vendor_id='.Config::get($currency.'.agg.vendor_id').
                      '&operator_id='.Config::get($currency.'.agg.operator_id').
                      '&user_id='.$username.
                      '&status='.$status.
                      '&currency='.$currency;

        $result = json_decode( App::curl( $postfields , Config::get($currency.'.agg.url').'updateStatus' , true, 3 , 'updateStatus') );
//        $result = json_decode( App::curlGET(Config::get($currency.'.agg.url').'updateStatus'.'?'.$postfields, true, 3, 'updateStatus') );

        if( isset($result) && $result->error_code == '0' )
        {
            return true;
        }

        return false;
    }
	
	public function createUser( $params = array() ){
		
		$postfields = 'vendor_id='.Config::get($params['currency'].'.agg.vendor_id').
					  '&operator_id='.Config::get($params['currency'].'.agg.operator_id').
					  '&user_id='.$params['username'].
					  '&currency='.strtoupper($params['currency']).
					  '&odd_type='.Config::get($params['currency'].'.agg.odd_type');

		$result = json_decode( App::curl( $postfields , Config::get($params['currency'].'.agg.url').'CheckOrCreateGameAccout' , true, 3 , 'createUser') );
		
		if( isset($result) && ( $result->error_code == '0' || ( $result->message == "error:21001,Duplicate account,") ))
		{
			Accountproduct::addAccountProduct($params['username'],'AGG','');
			return true;
		}
		
		return false;
		
	}
	
	public function getBalance($username,$currency){
		
		$postfields = 'vendor_id='.Config::get($currency.'.agg.vendor_id').
					  '&operator_id='.Config::get($currency.'.agg.operator_id').
					  '&user_id='.$username.
					  '&currency='.strtoupper($currency);

		$result = json_decode( App::curl( $postfields , Config::get($currency.'.agg.url').'GetBalance' , true, 5 , 'getBalance') );

		if( isset($result->amount) )
		{
			$dateTimeNow    = Carbon::now();
            $dateTimeNowStr = $dateTimeNow->toDateTimeString();
			
			if( Session::has('userid') ){
				
				DB::statement('call insert_cashbalance('.Session::get('userid').',"'.$username.'","'.$currency.'",'.$this->getPrdid().','.$result->amount.',"'.$dateTimeNowStr.'","'.$dateTimeNowStr.'")');
			
			}else{
				
				DB::statement('call insert_cashbalance('.Account::whereNickname($username)->whereCrccode($currency)->pluck('id').',"'.$username.'","'.$currency.'",'.$this->getPrdid().','.$result->amount.',"'.$dateTimeNowStr.'","'.$dateTimeNowStr.'")');
			}

			return $result->amount;
		}
		
		return false;
		
	}
	
	private function getPrdid(){
		
		$prdid = Cache::rememberForever('AGG_PRDID', function() {
                return Product::where( 'code' , '=' , 'AGG')->pluck('id');
        });
		
		return $prdid;
	}

	
	public function deposit($username,$currency,$amount,$transid){
		
		if( $amount < 0 ) return false;
		
		return $this->transferCredit( $username, $currency, $transid, 'IN', $amount);
		
	}
		
	public function withdraw($username,$currency,$amount,$transid){

		if( $amount < 0 ) return false;
		
		return $this->transferCredit( $username, $currency, $transid, 'OUT', $amount);
	}
	
	private function transferCredit( $username, $currency, $billno, $type, $credit){
		
		if( $currency == 'IDR' || $currency == 'VND' )
		{
			$credit = $credit / 1000;
		}

		$postfields = 'vendor_id='.Config::get($currency.'.agg.vendor_id').
					  '&operator_id='.Config::get($currency.'.agg.operator_id').
					  '&user_id='.$username.
					  '&currency='.strtoupper($currency).
					  '&billno='.str_pad($billno, 12, "0", STR_PAD_LEFT).
					  '&type='.$type.
					  '&credit='.$credit;

		$result = json_decode( App::curl( $postfields , Config::get($currency.'.agg.url').'TransferCredit' , true, 10 , 'transferCredit') );

		if( isset($result->error_code) && $result->error_code == '0' )
		{
			return true;
		}
		
		return false;
	}

	public function gameList($currency, $type = '1'){
        
        $builder = Betaggcategory::where('isActive', '=', '1')->orderBy('code', 'asc');
        
        if ($type != '1') {
            $builder->where('category', '=', $type);
        }
        
        $categoryObj = $builder->get();
        $useCN = (Lang::getLocale() == 'cn');
		$games = array();
        
        foreach ($categoryObj as $r) {
            $imgUrl = Config::get('setting.front_path') . '/img/agg/' . $r->imgCode . '.jpg';

            if (!file_exists(public_path($imgUrl))) {
                $imgUrl = Config::get('setting.front_path') . '/img/agg/SC01.jpg';
            }

            $games[strval($r->code)] = array(
                'game_code' => $r->code,
                'game_name' => ($useCN ? $r->gameNameCn : $r->gameName),
                'game_type' => $r->category,
                'img_url' => asset($imgUrl),
            );
        }
        
        return $games;
    }
    
}
?>