<?php namespace App\libraries\providers;

use App\libraries\App;
use App\Models\AccountProduct;
use Config;
//getBalance
//deposit
//withdraw
//createUser

class W88 {
 
	public function getLoginUrl($lang = 'en-us', $token , $currency){
		
		$postfields = 'op='.Config::get($currency.'.w88.merchant_id').
					  '&m=normal'.
					  '&lang='.$lang.
					  '&token='. $token;
		
		return Config::get($currency.'.w88.login_url').'?'.$postfields;
	
	}
	

	public function createUser( $params = array() ){
		
		$postfields = 'merch_id='.Config::get($params['currency'].'.w88.merchant_id').
					  '&merch_pwd='.Config::get($params['currency'].'.w88.merchant_pass').
					  '&cust_id=' . $params['username'] .
					  '&cust_name=' . $params['username'] .
					  '&currency='. $params['currency'];

		$result = simplexml_load_string( App::curl( $postfields , Config::get($params['currency'].'.w88.url') . 'createuser' , true, 5 , 'createUser') );
	
		if( isset($result->error_code) && ( $result->error_code == '0' || $result->error_code == '-203' ))
		{
			Accountproduct::addAccountProduct($params['username'],'W88','');
			return true;
		}
		
		return false;
		
	}

	
	public function getBalance($username , $currency){
		
		$postfields = 'merch_id='.Config::get($currency.'.w88.merchant_id').
					  '&merch_pwd='.Config::get($currency.'.w88.merchant_pass').
					  '&cust_id=' . $username .
					  '&currency='. $currency;

		$result = simplexml_load_string( App::curl( $postfields , Config::get($currency.'.w88.url') . 'getbalance' , true, 5 , 'getBalance') );

		if( isset($result->error_code) && $result->error_code == '0' )
		{
			return $result->balance;
		}
		
		return false;
		
	}

	public function deposit($username,$currency,$amount,$transid){
		
		if( $amount < 0 ) return false;

		return $this->transferCredit( $username, $currency, $transid, 'credit', $amount);
		
	}
		
	public function withdraw($username,$currency,$amount,$transid){
		
		if( $amount < 0 ) return false;
		
		return $this->transferCredit( $username, $currency, $transid, 'debit', $amount);
		
	}
	

	private function transferCredit( $username, $currency, $billno, $direction, $credit){
		
		if( $currency == 'IDR' || $currency == 'VND' )
		{
			$credit = $credit / 1000;
		}
			
		$postfields = 'merch_id='.Config::get($currency.'.w88.merchant_id').
					  '&merch_pwd='.Config::get($currency.'.w88.merchant_pass').
					  '&cust_id=' . $username .
					  '&currency='. $currency.
					  '&amount='. $credit.
					  '&trx_id='. $billno;

		$result = simplexml_load_string( App::curl( $postfields , Config::get($currency.'.w88.url') . $direction . '?' , true, 30 , 'transferCredit') );
		
		if( isset($result->error_code) && $result->error_code == '0' )
		{
			return true;
		}
		
		return false;
	
	}
	

	
   
}
?>