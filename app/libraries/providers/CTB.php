<?php namespace App\libraries\providers;

use App\libraries\App;
use App\Models\AccountProduct;
use App\Models\Product;
use App\Models\Account;
use Config;
use Carbon\Carbon;
use Lang;
use Cache;
use DB;
use Session;

//getLoginUrl
//getBalance
//deposit
//withdraw
//createUser


class CTB {

	public function getLoginUrl($username, $currency, $language_code = 'en'){

        $lang = collect(array(
            'en' => 'en',
            'cn' => 'cn',
            'tw' => 'cn',
            'id' => 'id',
            'th' => 'th',
            'vn' => 'vn',
        ));

		$postfields = 'tkCode='.Config::get($currency.'.ctb.tk_code').
					  '&userID='.strtolower($username);

		$result = json_decode( App::curlGET( Config::get($currency.'.ctb.api_url').'website/ssohandshake?'.$postfields, true, 5, 'getLoginUrl' ) );
		
		if( isset($result->status) && $result->status == 200 )
		{
			return Config::get($currency.'.ctb.api_url') .'ssologin?userID=' . strtolower($username) . '&webSiteTypeID=' . Config::get($currency.'.ctb.website_type_id') . '&loginKey=' . $result->loginKey . '&lang=' . $lang->get($language_code, 'en').'&extension1=' . Config::get($currency.'.ctb.extension1');
		}
		
		return false;
		
	}
	
	public function createUser($params = array()){

        $tkCode = Config::get($params['currency'].'.ctb.tk_code');

        if (strlen($tkCode) > 0) {
            $postfields = 'tkCode='.$tkCode.
                '&userID='.strtolower($params['username']).
                '&parentUserID='.strtolower(Config::get($params['currency'].'.ctb.parent_user_id'));

            $result = json_decode( App::curlGET( Config::get($params['currency'].'.ctb.api_url').'website/createplayerbyparent?'.$postfields, true, 3, 'createUser' ) );

            if( isset($result->status) )
            {
                if ($result->status == 200) {
                    Accountproduct::addAccountProduct($params['username'],'CTB','');
                    return true;
//                } elseif ($result->status == 500 && str_contains(strtolower($result->message), 'userid is not available')) {
//                    Accountproduct::addAccountProduct($params['username'],'CTB','');
//                    return true;
                }
            }
        }
		
		return false;
		
	}

    public function updateUser($params = array()){

        $tkCode = Config::get($params['currency'].'.ctb.tk_code');

        if (strlen($tkCode) > 0) {
            $postfields = 'tkCode='.$tkCode.
                '&userID='.strtolower($params['username']).
                '&parentUserID='.strtolower(Config::get($params['currency'].'.ctb.parent_user_id')).
                '&'.Config::get($params['currency'].'.ctb.bet_limit');

            /*
             * Bet limit value must be multiple of fare. Different Currency has different Fare.
             * Fare of different Currencies:
             * SGD: 1
             * MYR: 5
             * HKD: 10
             * CNY: 10
             * JPY: 100
             * AUD: 1
             * IDR: 20,000
             * USD: 1
             * KRW: 10,000
             * THB: 50
             * TWD: 50
             * VND: 30,000
             */

            $result = json_decode( App::curlGET( Config::get($params['currency'].'.ctb.api_url').'website/updatebetsetting?'.$postfields, true, 1, 'updateUser' ) );

            if( isset($result->status) && $result->status == 200 )
            {
                return true;
            }
        }

        return false;

    }

	public function getBalance($username,$currency){

        $tkCode = Config::get($currency.'.ctb.tk_code');

        if (strlen($tkCode) > 0) {
            $postfields = 'tkCode='.$tkCode.
                '&userID='.strtolower($username).
                '&parentUserID='.strtolower(Config::get($currency.'.ctb.parent_user_id'));

            $result = json_decode( App::curlGET( Config::get($currency.'.ctb.api_url').'website/getaccountinfo?'.$postfields, true, 5, 'getBalance' ) );

            if( isset($result->status) && $result->status == 200 && isset($result->account->cashBalance) )
            {
				$dateTimeNow    = Carbon::now();
				$dateTimeNowStr = $dateTimeNow->toDateTimeString();
				$balance 		= (float)$result->account->cashBalance;
					
				if( Session::has('userid') )
				{
					DB::statement('call insert_cashbalance('.Session::get('userid').',"'.Session::get('username').'","'.$currency.'",'.$this->getPrdid().','.$balance.',"'.$dateTimeNowStr.'","'.$dateTimeNowStr.'")');
					
				}else{
					
					DB::statement('call insert_cashbalance('.Account::whereNickname($username)->whereCrccode($currency)->pluck('id').',"'.$username.'","'.$currency.'",'.$this->getPrdid().','.$balance.',"'.$dateTimeNowStr.'","'.$dateTimeNowStr.'")');
				}
				
                return (float)$result->account->cashBalance;
            }
        }
		
		return false;
		
	}
	
	private function getPrdid(){
		
		$prdid = Cache::rememberForever('CTB_PRDID', function() {
                return Product::where( 'code' , '=' , 'CTB')->pluck('id');
        });
		
		return $prdid;
	}
	 
	public function deposit($username,$currency,$amount,$transid = ''){
		
		if( $amount < 0 ) return false;

		if ($transid == '') {
            $transid = $this->randomString();
        }

        return $this->transferCredit($username, $currency, $transid, 0, $amount);
	}
		
	public function withdraw($username,$currency,$amount,$transid = ''){
		
		if( $amount < 0 ) return false;

        if ($transid == '') {
            $transid = $this->randomString();
        }

        return $this->transferCredit($username, $currency, $transid, 1, $amount);
	}

	private function transferCredit( $username, $currency, $billno, $type, $credit){

        $tkCode = Config::get($currency.'.ctb.tk_code');

        if (strlen($tkCode) > 0) {
            if ($currency == 'IDR' || $currency == 'VND') {
                $credit = $credit / 1000;
            }

            $apiName = 'cashdeposit';
            $action = 'deposit';

            if ($type == 1) {
                $apiName = 'cashwithdraw';
                $action = 'withdraw';
            }

            $postfields = 'tkCode='.$tkCode.
                '&parentUserID='.strtolower(Config::get($currency.'.ctb.parent_user_id')).
                '&'.$action.'='.number_format($credit, 2, '.', '').
                '&randomKey='.str_pad($billno, 16, '0', STR_PAD_LEFT).
                '&userID='.strtolower($username);

            $result = json_decode( App::curlGET( Config::get($currency.'.ctb.api_url').'website/'.$apiName.'?'.$postfields, true, 10, 'transferCredit' ) );

            if( isset($result->status) && $result->status == 200 )
            {
                return true;
            }
        }

		return false;
	}
	
	private function randomString() 
	{
       return substr( MD5(rand(100, 200)) , 0 , 16);
	}

}
?>