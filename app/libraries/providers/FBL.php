<?php namespace App\libraries\providers;

use App\libraries\App;
use App\Models\Accountproduct;
use App\Models\Account;
use Config;
use Session;
//getLoginUrl
//getBalance
//deposit
//withdraw
//createUser


class FBL {

	public function getLoginUrl($username, $currency, $language_code = null){
        
        $accountObj = null;

        if (!is_null($username)) {
            $accountObj = Account::where('nickname', '=', $username)
                ->where('crccode', '=' , $currency)
                ->where('status', '=', 1)
                ->first();
        }
        
        if (is_null($username) || is_null($accountObj)) {
			// No username given or account not found, use public page URL.
			return $data['login_url'] = Config::get($currency.'.fbl.public_url');
		}
		
        // Get FBL userId from DB.
		$optUsername = Config::get($currency.'.fbl.opt_username');
		$fblUserId = $accountObj->fblid;
		$postfields = array(
            'SecurityKey' => '',
		    'AppUser' => $optUsername,
		    'UserID' => $fblUserId,
		);
		
		$postfields['SecurityKey'] = $this->generateSecurityKey($currency, $postfields);
		
		$result = simplexml_load_string(App::curlXML(json_encode($postfields), Config::get($currency.'.fbl.url').'GetGameToken' , true, 10 , '', $this->getXmlHeader()) );
		
		if (isset($result->Token) && isset($result->Result->StatusCode) && $result->Result->StatusCode == 0) {
			$params = array(
			    'UserID' => $fblUserId,
			    'Token' => $result->Token,
			);
			
			return Config::get($currency.'.fbl.login_url').'?accesskey='.md5(Config::get($currency.'.fbl.opt_id').$optUsername.$params['UserID'].$params['Token']).'&userid='.$params['UserID'].'&token='.$params['Token'];
		}
		
		return false;
	}
	
	public function createUser( $params = array() ){
		
		if(Config::get($params['currency'].'.fbl.url') == '')
		{
			return false;
		}
		
		date_default_timezone_set("Asia/Kuala_Lumpur");
		
		$username = strtoupper($params['username']);
		$postfields = array(
            'SecurityKey' => '',
		    'AppUser' => Config::get($params['currency'].'.fbl.opt_username'),
		    'Username' => $username,
		    'Name' => $username,
		);
        
        $postfields['SecurityKey'] = $this->generateSecurityKey($params['currency'], $postfields);
		
		$result = simplexml_load_string( App::curlXML(json_encode($postfields), Config::get($params['currency'].'.fbl.url').'CreateMember' , true, 10 , 'createUser' , $this->getXmlHeader()) );
		
		if (isset($result->Result->StatusCode)) {
			// Error code 2: username in use.
			if ($result->Result->StatusCode == 0 || $result->Result->StatusCode == 2) {
				if( !Accountproduct::is_exist( $params['username'] ,'fbl'))
				{
					Accountproduct::addAccountProduct($params['username'],'fbl','');
					
					// Store FBL user id to DB.
                    Account::where('id', '=', $params['userid'])->update(['fblid' => $result->UserID]);
				}

				return $result;
			}
		}
		
		return false;
		
	}

    public function getUserIdByUsername( $params = array() ){

        if(Config::get($params['currency'].'.fbl.url') == '')
        {
            return false;
        }

        date_default_timezone_set("Asia/Kuala_Lumpur");

        $username = strtoupper($params['username']);
        $postfields = array(
            'SecurityKey' => '',
            'AppUser' => Config::get($params['currency'].'.fbl.opt_username'),
            'Username' => $username,
        );

        $postfields['SecurityKey'] = $this->generateSecurityKey($params['currency'], $postfields);

        $result = simplexml_load_string( App::curlXML(json_encode($postfields), Config::get($params['currency'].'.fbl.url').'getUserIDByUsername' , true, 10 , 'getUserIdByUsername' , $this->getXmlHeader()) );

        if (isset($result->Result->StatusCode)) {
            // Error code 2: username in use.
            if ($result->Result->StatusCode == 0) {
                if( Accountproduct::is_exist( $params['username'] ,'fbl'))
                {
                    // Store FBL user id to DB.
                    Account::where('id', '=', $params['userid'])->update(['fblid' => $result->UserID]);
                }

                return $result;
            }
        }

        return false;

    }

	public function updateUser( $params = array() ){
		
		// TODO Not documented.
//		$username = strtoupper($params['username']);
//		$postfields = '';
//		
//		$result = simplexml_load_string( App::curlXML( $postfields , Config::get($params['currency'].'.fbl.url'), true, 30 , 'updateUser', $this->getXmlHeader()) );
//		$result = $this->xmlResponseToArray($result);
//		
//		if (array_key_exists('status', $result)) {
//			return ($result['status'] == 0);
//		}
		
		return false;
		
	}
	
	public function getBalance($username,$currency){
		
		if(Config::get($currency.'.fbl.url') == '')
		{
			return false;
		}
		
        $accountObj = Account::where('nickname', '=', $username)
            ->where('crccode', '=' , $currency)
            ->first();
        
        if ($accountObj) {
            $username = strtoupper($username);
            $fblUserId = $accountObj->fblid;
            $postfields = array(
                'SecurityKey' => '',
                'AppUser' => Config::get($currency.'.fbl.opt_username'),
                'UserIDs' => [$fblUserId],
            );

            $postfields['SecurityKey'] = $this->generateSecurityKey($currency, $postfields);

            $result = simplexml_load_string( App::curlXML(json_encode($postfields), Config::get($currency.'.fbl.url').'GetAccountBalances' , true, 10 , 'getBalance', $this->getXmlHeader()) );
            
            if (isset($result->AccountBalances) && isset($result->Result->StatusCode) && $result->Result->StatusCode == 0) {
                foreach ($result->AccountBalances->AccountBalance as $bal) {
                    if ($bal->UserID == $fblUserId) {
                        return floatval($bal->Balance);
                    }
                }
            }
        }
		
		return false;
		
	}
	
	public function deposit($username,$currency,$amount,$transid){
		
		if( $amount < 0 ) return false;
		
		return $this->transferCredit( $username, $currency, $transid, '0', $amount);
		
	}
		
	public function withdraw($username,$currency,$amount,$transid){

		if( $amount < 0 ) return false;
		
		return $this->transferCredit( $username, $currency, $transid, '1', $amount);
	}
	
	private function transferCredit( $username, $currency, $billno, $type, $credit){
		
        $accountObj = Account::where('nickname', '=', $username)
            ->where('crccode', '=' , $currency)
            ->first();
        
        if ($accountObj) {
            if( $currency == 'IDR' || $currency == 'VND' )
            {
                $credit = $credit / 1000;
            }

            $action = '';

            if ($type == 0) {
                $action = 'AccountDeposit';
            } elseif ($type == 1) {
                $action = 'AccountWithdrawal';
            } else {
                // Unknown type.
                return false;
            }

            $username = strtoupper($username);
            $fblUserId = $accountObj->fblid;
            $postfields = array(
                'SecurityKey' => '',
                'AppUser' => Config::get($currency.'.fbl.opt_username'),
                'UserID' => $fblUserId,
                'Amount' => $credit,
                'TransactionID' => str_pad($billno, 12, "0", STR_PAD_LEFT),
            );

            $postfields['SecurityKey'] = $this->generateSecurityKey($currency, $postfields);

            $result = simplexml_load_string( App::curlXML(json_encode($postfields) , Config::get($currency.'.fbl.url').$action , true, 10 , 'transferCredit', $this->getXmlHeader()) );

            if (isset($result->Result->StatusCode) && $result->Result->StatusCode == 0) {
                return true;
            }
        }
		
		return false;
	}
	
	public function generateSecurityKey($currency, $params = array())
	{
		$result = '';
		
		foreach ($params as $key => $val) {
            if ($key != 'SecurityKey') {
                if (is_array($val)) {
                    $result .= implode(',', $val);
                } else {
                    $result .= $val;
                }
            }
		}
        
		if (strlen($result) > 0) {
			$result = strtoupper(substr(md5(Config::get($currency.'.fbl.opt_id').$result), 0, 12));
		}
		
		return $result;
	}
	
	private function getXmlHeader()
	{
		return array(
		    'Cache-Control: no-cache',
		    'Content-Type: application/json',
		);
	}
}
?>