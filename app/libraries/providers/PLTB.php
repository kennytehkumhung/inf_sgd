<?php

namespace App\libraries\providers;

use App\libraries\App;
use App\Models\AccountProduct;
use App\Models\Betpltbcategory;
use App\Models\Product;
use App\Models\Account;
use Config;
use Carbon\Carbon;
use Lang;
use Cache;
use DB;
use Session;
use Crypt;

//getBalance
//deposit
//withdraw
//createUser

class PLTB {

	public function createUser($params = array()) {

		$postfields = array(
		    'membercode' => Config::get($params['currency'].'.pltb.prefix').'_'.strtoupper($params['username']),
		    'password' => Crypt::decrypt($params['password']),
		    'currency' => $params['currency'],
		);
        
        $result = json_decode($this->callFunction('player/createplayer', $postfields, $params['currency'], 'post', 'createUser', 3));

		if ((isset($result->Code) && ( $result->Code == '54' || $result->Code == '0' ))) {

            if (!Accountproduct::is_exist($params['username'], 'pltb')) {
                Accountproduct::addAccountProduct($params['username'], 'pltb', '');
            }
			return true;
		}

		return false;
	}

	public function getBalance($username, $currency) {

		$postfields = array(
		    'membercode' => Config::get($currency.'.pltb.prefix').'_'.strtoupper($username),
		    'producttype' => 0,
		);
		
        $result = json_decode($this->callFunction('account/getbalance/membercode/'.$postfields['membercode'].'/producttype/'.$postfields['producttype'], null, $currency, 'get', 'getBalance', 5));

	
		if ((isset($result->Code) && $result->Code == '0' )) {
			
			$dateTimeNow    = Carbon::now();
			$dateTimeNowStr = $dateTimeNow->toDateTimeString();
				
			if( Session::has('userid') ){
				DB::statement('call insert_cashbalance('.Session::get('userid').',"'.Session::get('username').'","'.$currency.'",'.$this->getPrdid().','.$result->Balance.',"'.$dateTimeNowStr.'","'.$dateTimeNowStr.'")');

			}else{
				DB::statement('call insert_cashbalance('.Account::whereNickname($username)->whereCrccode($currency)->pluck('id').',"'.$username.'","'.$currency.'",'.$this->getPrdid().','.$result->Balance.',"'.$dateTimeNowStr.'","'.$dateTimeNowStr.'")');
			}
			
            if( $currency == 'IDR' || $currency == 'VND')
            {
                return $result->Balance / 1000;
            }
            else
            {
                return $result->Balance;
            }
        }

		return false;
	}
	
	private function getPrdid(){
		
		$prdid = Cache::rememberForever('PLTB_PRDID', function() {
                return Product::where( 'code' , '=' , 'PLTB')->pluck('id');
        });
		
		return $prdid;
	}

	public function updatePassword($username, $currency, $password) {

		$postfields = array(
		    'membercode' => Config::get($currency.'.pltb.prefix').'_'.strtoupper($username),
		    'password' => $password,
		);
		
        $result = json_decode($this->callFunction('player/resetpassword', $postfields, $currency, 'put', 'updatePassword', 3));
        
		if (isset($result->Code) && $result->Code == '0') {
			return true;
		}
		
		return false;
	}

	public function deposit($username, $currency, $amount, $transid) {

		if( $amount < 0 ) return false;
		
		return $this->transferCredit( $username, $currency, $transid, '0', $amount);
	}

	public function withdraw($username, $currency, $amount, $transid) {

		if( $amount < 0 ) return false;
		
		return $this->transferCredit( $username, $currency, $transid, '1', $amount);
	}
	
	private function transferCredit( $username, $currency, $billno, $type, $credit ) {
		
	 /* 	if( $currency == 'IDR' || $currency == 'VND' )
		{
			$credit = $credit / 1000;
		} */
        
        if ($type == '1') {
            $credit = $credit * (-1);
        }
		
		$postfields = array(
		    'membercode' => Config::get($currency.'.pltb.prefix').'_'.strtoupper($username),
		    'amount' => $credit,
		    'externaltransactionid' => str_pad($billno, 12, "0", STR_PAD_LEFT),
		    'producttype' => 0,
		);
		
		$result = json_decode($this->callFunction('chip/createtransaction', $postfields, $currency, 'post', 'transferCredit', 10));
        
		if (isset($result->Code) && $result->Code == '0') {
			return (isset($result->Status) && strtolower($result->Status) == 'approved');
		}
		
		return false;
	}
	
	private function callFunction( $action , $params , $currency, $httpMethod, $method , $timeout = 10)
	{
	
		$header   = array();
		$header[] = "Cache-Control:no-cache"; 
		$header[] = "merchantname:".Config::get($currency.'.pltb.merchantname'); 
		$header[] = "merchantcode:".Config::get($currency.'.pltb.merchantcode'); 
        
	    $tuCurl = curl_init();
        
        switch (strtolower($httpMethod)) {
            case 'post':
                $header[] = "Content-Type:application/json";
                curl_setopt($tuCurl, CURLOPT_POST, true);
                break;
            case 'put':
                $header[] = "Content-Type:application/json";
                curl_setopt($tuCurl, CURLOPT_CUSTOMREQUEST, 'PUT');
                break;
            default:
                curl_setopt($tuCurl, CURLOPT_POST, false);
                break;
        }

        $url = Config::get($currency.'.pltb.url').$action;
        
	    curl_setopt($tuCurl, CURLOPT_URL, $url); 
		curl_setopt($tuCurl, CURLOPT_TIMEOUT, $timeout);
		curl_setopt($tuCurl, CURLOPT_RETURNTRANSFER, 1);
        
        if (!is_null($params)) {
            $params = json_encode($params);
            
            curl_setopt($tuCurl, CURLOPT_POSTFIELDS, $params);
        } else {
            $params = '';
        }
        
		curl_setopt($tuCurl, CURLOPT_HTTPHEADER, $header);
		$result = trim(curl_exec($tuCurl));
        
	    if(!$result)
		{
            dd(curl_error($tuCurl));
	    }

		curl_close($tuCurl);
		
		App::insert_api_log( array( 'request' => 'param='.$url.'?'.str_replace(' ', '%20', $params) , 'return' => $result , 'method' => $method ) );
		
		return $result; 
	}

    public function gameList($currency, $type = 'all', $category) {

        $categoryBuilder = Betpltbcategory::where('isActive', '=', 1);

        if ($this->isMobileDevice()) {
            $categoryBuilder->where('isHtml5', '=', 1)->where('html5Code', '!=', '');
        } else {
            $categoryBuilder->where('code', '!=', '');
        }

        if($type == 'slot') {
            $data['name']  = 'pltb';
            $categoryBuilder->where('category', '=', $category);
        } elseif ($type == 'newgames') {
            $data['name']  = 'pltb';
            $categoryBuilder->where('isNew', '=', $category);
        } elseif ($type == 'pgames') {
            $data['name']  = 'pltb';
            $categoryBuilder->where('jpCode', '!=', '');
        } elseif ($type == 'brand' ) {
            $data['name']  = 'pltb';
            $categoryBuilder->where('isBranded', '=', $category);
        } else {
            $data['name']  = 'pltb';
            $categoryBuilder->where('jpCode', '!=', '');
        }

        return $categoryBuilder->orderBy('isNew', 'desc')->get();
    }

    public function isMobileDevice() {

        $useragent = $_SERVER['HTTP_USER_AGENT'];

        if (preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4))) {
            // Mobile request.
            return true;
        } elseif (stristr($_SERVER['HTTP_USER_AGENT'], 'Mozilla/5.0(iPad;')) {
            // Mobile request.
            return true;
        } elseif (strstr($_SERVER['HTTP_USER_AGENT'], 'iPad')) {
            // Mobile request.
            return true;
        }

        return false;
    }

}

?>