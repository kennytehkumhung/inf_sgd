<?php namespace App\libraries\providers;

use App\libraries\App;
use App\Models\AccountProduct;
use App\Models\Bettbsteam;
use Config;
use Session;
use Request;
//getLoginUrl
//getBalance
//deposit
//withdraw
//createUser
//http://api.ugamingservice.com/SportAPI.asmx/RegisterByLimit?APIPassword=d6a8a112778f71df941f06a714fff0bd&MemberAccount=badegg@369&AgentAccount=y33333301&NickName=badegg@369&Currency=THB&MinBet=50&MaxBet=25000&PerMaxBet=150000&MixMinBet=50&MixMaxBet=25000&DayLimit=150000&GroupComm=A
//https://transferapi.ugamingservice888.com/ThirdApi.asmx/GetTeamName?APIPassword=3d882ed696b24aa88639bccafb2a4e74&TeamID=31895

class TBS {

    public function getLoginUrl($username, $currency, $ip, $language_code = 'en', $style, $ismobile = false  ){

        if (is_null($username)) {
            // No username given, use public page URL.
            return $data['login_url'] = Config::get($currency.'.tbs.public_url').'&language=CH';
        }

        if (strlen($ip) < 1 || $ip == '::1') {
            $ip = '127.0.0.1';
        }
        
        $webtype = 'PC';
        if($ismobile){
            $webtype = 'Smart';
        }
        
        if($language_code == 'cn'){
            $lang = 'CH';
        }else{
            $lang = 'EN';
        }

        $postfields = 'APIPassword='.Config::get($currency.'.tbs.password').
            '&MemberAccount='.$username.'@'.Config::get($currency.'.tbs.prefix').
            '&GameID=1&WebType='.$webtype.
            '&PageStyle=SP3'.
            '&LoginIP='.$ip.
            '&Language='.$lang.'&OddStyle='.Config::get($currency.'.tbs.oddstyle');

        $result = simplexml_load_string( App::curl( $postfields , Config::get($currency.'.tbs.url').'Login' , true, 5 , 'getLoginUrl') );

        if( isset($result->result) )
        {
            return $result->result;
        }

        return false;

    }

    public function createUser( $params = array() ){

        if($params['currency'] == 'CNY') $currency = 'RMB';
        else $currency = 'MYR';

        $postfields = 'APIPassword='.Config::get($params['currency'].'.tbs.password').
            '&AgentID='.Config::get($params['currency'].'.tbs.agentid').
            '&MemberAccount='.$params['username'].'@'.Config::get($params['currency'].'.tbs.prefix').
            '&NickName='.$params['username'].
            '&Currency='.strtoupper($currency);

        $result = simplexml_load_string( App::curl( $postfields , Config::get($params['currency'].'.tbs.url').'RegisterByAgent' , true, 5 , 'createUser') );

        if( isset($result->errcode) && $result->errcode == "000000" )
        {
            Accountproduct::addAccountProduct($params['username'],'TBS','');
            return true;
        }

        return false;

    }

    public function updateUser( $params = array() ){

        $postfields = 'APIPassword='.Config::get($params['currency'].'.tbs.password').
            '&MemberAccount='.$params['username'].'@'.Config::get($params['currency'].'.tbs.prefix').
            '&SportID=0'.
            '&OddsStyle=MY,EU,HK,ID'.
            '&'.Config::get($params['currency'].'.tbs.betlimit');

        $result = simplexml_load_string( App::curl( $postfields , Config::get($params['currency'].'.tbs.url').'UpdateMemberLimitBySport' , true, 5 , 'updateUser') );

        if( isset($result->errcode) && $result->errcode == "000000" )
        {
            return true;
        }

        return false;

    }

    public function getBalance($username,$currency){

        $postfields = 'APIPassword='.Config::get($currency.'.tbs.password').
            '&MemberAccount='.$username.'@'.Config::get($currency.'.tbs.prefix');

        $result = simplexml_load_string( App::curl( $postfields , Config::get($currency.'.tbs.url').'GetBalance' , true, 10 , 'getBalance') );

        if( isset($result->result->Balance) )
        {
            return (float)$result->result->Balance;
        }

        return false;

    }

    public function deposit($username,$currency,$amount,$transid){

        if( $amount < 0 ) return false;

        return $this->transferCredit( $username, $currency, $transid, '0', $amount);

    }

    public function withdraw($username,$currency,$amount,$transid){

        if( $amount < 0 ) return false;

        return $this->transferCredit( $username, $currency, $transid, '1', $amount);
    }

    private function transferCredit( $username, $currency, $billno, $type, $credit){

        if( $currency == 'IDR' || $currency == 'VND' )
        {
            $credit = $credit / 1000;
        }

        $apiPassword = Config::get($currency.'.tbs.password');
        $prefix = Config::get($currency.'.tbs.prefix');
        $memberAccount = $username.'@'.$prefix;
        $postfields = 'APIPassword='.$apiPassword.
            '&MemberAccount='.$memberAccount.
            '&SerialNumber='.$prefix.'_'.$billno.
            '&TransferType='.$type.
            '&Amount='.$credit.
            '&Key='.substr(MD5($apiPassword.$memberAccount.number_format($credit, 4, '.', '')) , -6);


        $result = simplexml_load_string( App::curl( $postfields , Config::get($currency.'.tbs.url').'Transfer' , true, 30 , 'transferCredit') );

        if( isset($result->errcode) && $result->errcode == "000000" )
        {
            return true;
        }

        return false;
    }

    public function getTeamName($teamID){

        $postfields = 'APIPassword='.Config::get(Session::get('currency').'.tbs.password').
            '&TeamID='.$teamID;

        $result = simplexml_load_string( App::curl( $postfields , Config::get(Session::get('currency').'.tbs.transferapi').'GetTeamName' , true, 10 , 'GetTeamName') );

        if( isset($result->result->team->TeamID)&&$result->result->team->TeamID==$teamID)
        {
            Bettbsteam::saveTeamName($result);
        }

        return false;

    }








}
?>