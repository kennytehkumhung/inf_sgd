<?php namespace App\libraries\providers;

use App\libraries\App;
use App\Models\Account;
use App\Models\AccountProduct;
use App\Models\Product;
use Config;
use Carbon\Carbon;
use Lang;
use Cache;
use DB;
use Session;
use Crypt;
//getLoginUrl
//getBalance
//deposit
//withdraw
//createUser


class BBN {

	public function getLoginUrl($username, $currency)
    {
		$username = strtoupper(Config::get($currency . '.bbn.Prefix') . $username);

		$A = substr(str_shuffle("abcdefghijklmnopqrstuvwxyz"), 0, 2);
		$B = MD5(Config::get($currency . '.bbn.Website').$username.Config::get($currency . '.bbn.KeyB_Login').date('Ymd',strtotime('-12 hours')));
		$C = substr(str_shuffle("abcdefghijklmnopqrstuvwxyz"), 0, 8);
		
		return Config::get($currency.'.bbn.LoginServiceURL').'/Login?'.'website='.Config::get($currency . '.bbn.Website').'&username='.$username.'&uppername='.Config::get($currency . '.bbn.Uppername').'&key='.$A.$B.$C;
		
    }

	public function slotGame($username, $currency)
    {
		$username = strtoupper(Config::get($currency . '.bbn.Prefix') . $username);

		$A = substr(str_shuffle("abcdefghijklmnopqrstuvwxyz"), 0, 2);
		$B = MD5(Config::get($currency . '.bbn.Website').$username.Config::get($currency . '.bbn.KeyB_Login').date('Ymd',strtotime('-12 hours')));
		$C = substr(str_shuffle("abcdefghijklmnopqrstuvwxyz"), 0, 8);
		
		return Config::get($currency.'.bbn.LoginServiceURL').'/Login?'.'website='.Config::get($currency . '.bbn.Website').'&username='.$username.'&uppername='.Config::get($currency . '.bbn.Uppername').'&page_site=mg'.'&key='.$A.$B.$C;
	
    }
	
	public function lottery($username, $currency)
    {
		$username = strtoupper(Config::get($currency . '.bbn.Prefix') . $username);

		$A = substr(str_shuffle("abcdefghijklmnopqrstuvwxyz"), 0, 2);
		$B = MD5(Config::get($currency . '.bbn.Website').$username.Config::get($currency . '.bbn.KeyB_Login').date('Ymd',strtotime('-12 hours')));
		$C = substr(str_shuffle("abcdefghijklmnopqrstuvwxyz"), 0, 8);
		
		return Config::get($currency.'.bbn.LoginServiceURL').'/Login?'.'website='.Config::get($currency . '.bbn.Website').'&username='.$username.'&uppername='.Config::get($currency . '.bbn.Uppername').'&page_site=Ltlottery'.'&key='.$A.$B.$C;
	
    }
	
	public function createUser( $params = array() ){

        $username = strtoupper(Config::get($params['currency'] . '.bbn.Prefix') . $params['username']);
	    $password = $params['password'];

	    if (strlen($password) > 18) {
	        $password = Crypt::decrypt($password);
        }
		
        $A = substr(str_shuffle("abcdefghijklmnopqrstuvwxyz"), 0, 4);
		$B = MD5(Config::get($params['currency'] . '.bbn.Website').$username.Config::get($params['currency'] . '.bbn.KeyB_CreateMember').date('Ymd',strtotime('-12 hours')));
		$C = substr(str_shuffle("abcdefghijklmnopqrstuvwxyz"), 0, 1);
		
        $data = array(
            'website' => Config::get($params['currency'] . '.bbn.Website'),
            'username' => $username,
            'uppername' => Config::get($params['currency'] . '.bbn.Uppername'),
            'password' => $password,
			'key' => $A.$B.$C,
        );

		$postfields = json_encode($data);
		
		$result = json_decode($this->curl(Config::get($params['currency'].'.bbn.BaseServiceURL').'/CreateMember',$postfields, true, 3 , 'createUser'));
		
		if(isset($result) && ($result->data->Code == '21100' || $result->data->Code == '21001')){
			Accountproduct::addAccountProduct($params['username'], 'BBN', '');
			
            return true;
		}
			return false;
	}

	public function getBalance($username, $currency){

        $usernamebbn = strtoupper(Config::get($currency . '.bbn.Prefix') . $username);
		
		$A = substr(str_shuffle("abcdefghijklmnopqrstuvwxyz"), 0, 5);
		$B = MD5(Config::get($currency . '.bbn.Website').$usernamebbn.Config::get($currency . '.bbn.KeyB_CheckUserBalance').date('Ymd',strtotime('-12 hours')));
		$C = substr(str_shuffle("abcdefghijklmnopqrstuvwxyz"), 0, 7);
		
         $data = array(
            'website' => Config::get($currency . '.bbn.Website'),
            'username' => $usernamebbn,
            'uppername' => Config::get($currency . '.bbn.Uppername'),
			'key' => $A.$B.$C,
        );
	
		$postfields = json_encode($data);
		
		$result = json_decode($this->curl(Config::get($currency.'.bbn.BaseServiceURL').'/CheckUsrBalance',$postfields, true, 3 , 'getBalance'));
		
		if( isset($result->result) && ($result->result == 'true')) {
			
			$dateTimeNow    = Carbon::now();
			$dateTimeNowStr = $dateTimeNow->toDateTimeString();
				
			if( Session::has('userid') ){
				DB::statement('call insert_cashbalance('.Session::get('userid').',"'.Session::get('username').'","'.$currency.'",'.$this->getPrdid().','.$result->data[0]->Balance.',"'.$dateTimeNowStr.'","'.$dateTimeNowStr.'")');
			}else{
				DB::statement('call insert_cashbalance('.Account::whereNickname($username)->pluck('id').',"'.$username.'","'.$currency.'",'.$this->getPrdid().','.$result->data[0]->Balance.',"'.$dateTimeNowStr.'","'.$dateTimeNowStr.'")');
			}
			
		    return $result->data[0]->Balance;
		}
		
		return false;
		
	}
	
	private function getPrdid(){
		
		$prdid = Cache::rememberForever('BBN_PRDID', function() {
                return Product::where( 'code' , '=' , 'BBN')->pluck('id');
        });
		
		return $prdid;
	}
	
	
	public function deposit($username,$currency,$amount,$transid){
		
		if( $amount < 0 ) return false;
		
		return $this->transferCredit( $username, $currency, $transid, 'IN', $amount);
		
	}
		
	public function withdraw($username,$currency,$amount,$transid){

		if( $amount < 0 ) return false;
		
		return $this->transferCredit( $username, $currency, $transid, 'OUT', $amount);
	}
	
	private function transferCredit( $username, $currency, $billno, $type, $credit){

        $username = strtoupper(Config::get($currency . '.bbn.Prefix') . $username);
        $billno = Config::get($currency.'.bbn.PrefixBillNo') . $billno;

		$A = substr(str_shuffle("abcdefghijklmnopqrstuvwxyz"), 0, 6);
		$B = MD5(Config::get($currency . '.bbn.Website').$username.$billno.Config::get($currency . '.bbn.KeyB_Transfer').date('Ymd',strtotime('-12 hours')));
		$C = substr(str_shuffle("abcdefghijklmnopqrstuvwxyz"), 0, 5);
		
        $data = array(
            'website'      => Config::get($currency.'.bbn.Website'),
			'username'	=> $username,
			'uppername'	=> Config::get($currency.'.bbn.Uppername'),
			'remitno'	=> $billno,
			'action'	=> $type,
			'remit' => $credit,
			'key'	=> $A.$B.$C,	
        );
		
		$postfields = json_encode($data);
		
		$result = json_decode($this->curl(Config::get($currency.'.bbn.BaseServiceURL').'/Transfer',$postfields, true, 20 , 'transferCredit'));
		
		if( isset($result->result) && $result->result == 'true' ) {
			return true;
		}
		
		return false;
	}

    function curl($url , $postFields , $log = true, $timeout = 30 , $method = ''){
		$headers[] = "Content-Type:application/json";
		$data_json = $postFields;
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json','Content-Length: ' . strlen($data_json)));
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($ch, CURLOPT_POSTFIELDS,$data_json);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response  = curl_exec($ch);
		curl_close($ch);
        
        if($log)
            App::insert_api_log( array( 'request' => $url.'?'.$data_json , 'return' => $response , 'method' => $method ) );

        return $response;
    }
	
   
}
