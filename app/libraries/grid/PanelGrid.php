<?php namespace App\libraries\grid;
use App\libraries\App;

class PanelGrid{



	protected $module = '';
	
	protected $action = '';
	
	protected $footer = false;
	
	protected $columns = array();

	protected $rows = array();

	protected $filters = array();

	protected $buttons = array();

	protected $searchform = '';

	protected $title = '';

	protected $csvUrl = '';
	
	protected $total = 1;

	protected $limit = 0;

	protected $start = 0;

	protected $searchs = array();

	protected $ranges = array();
	
	protected $params = array();

	protected $sid = '';

	protected $refreshUrl = '';

	protected $contextMenus = array();

	protected $uniqid = 0;

	protected $hasAutoRefresh = 0;

	protected $autoRefreshSecond = 0;

	protected $colorLegend = array();

	protected $all = false; //all page

	protected $playSoundAlert = false;

	protected $soundAlert = '';

	protected $showRowMarker = true;
	
	protected $dateOptions = array();
	
	protected $id = '';
	
	/**
	* Flag to indicate that this grid is a pop-up and meant for
	* selection into a form droptext/dropselect input
	*
	* @access	protected
	* @var      boolean
	*/
	protected $forSelection = false;


	/**
	* Boolean if activate debug mode
	*
	* @access 	private
	* @var      boolean
	*/
	private $debugMode = false;

	/**
	* Contructor.
	*
	* @return none
	*/
	function __construct() {
	}

	public function setupGrid($module, $action, $limit = 20, $footer = false, $options = array(), $id = 'dtGrid') {
		$this->module = $module;
		$this->action = $action;
		$this->limit = $limit;
		$this->footer = $footer;
		$this->options = $options;
		$this->id = $id;
		$this->setDateOptions();
	}

	public function setRowMarker($status) {
		$this->showRowMarker = $status;
	}

	/**
	* Set to debug mode
	*
	* @param boolean $mode 	Debug mode
	*
	* @access public
	* @return none
	*/
	function setDebugMode($mode = false) {
		$this->debugMode = $mode;
	}

	public function setColorLegend($options) {
		$this->colorLegend = $options;
	}

	public function setDoubleClickUrl($url, $param, $script = '') {
		$this->dblClickUrl = $url;
		$this->dblClickParam = $param;
		$this->dblClickScript = $script;
	}

	public function setNavigationUrl($url, $all = false) {
		$this->hasNavigation = true;
		$this->navigationUrl = $url;
		$this->all = $all;
	}

	public function setRefreshUrl($url) {
		$this->refreshUrl =  $url;
	}
	
	public function setCSV($url) {
		$this->hasCSV = true;
		$this->csvUrl = $url;
	}

	public function setTitle($title) {
		$this->title = $title;
	}

	public function setParams($params, $position = 0) {
		if(empty($position)) {
			$this->params[1] = $params;
			$this->params[2] = $params;
		} else {
			$this->params[$position] = $params;
		}
	}

	public function setAutoRefresh($second) {
		$this->hasAutoRefresh = true;
		$this->autoRefreshSecond = $second;
	}
	
	/**
	* Set the grid go into selection mode for dropping into another form
	*
	*
	* @access public
	* @return none
	*/
	public function setForSelection() {
		$this->forSelection = true;
	}
	
	public function setDateOptions($showTime = false) {
		if($showTime) {
			$this->dates = array(
				'todayfrom'	=> App::getDateTime(20),
				'todayto'	=> App::getDateTime(21),
				'yesterdayfrom'	=> App::formatDateTime(20, App::spanDateTime(-1 * CBO_DAY_SEC)),
				'yesterdayto'	=> App::formatDateTime(21, App::spanDateTime(-1 * CBO_DAY_SEC)),
				'sevendayfrom'	=> App::formatDateTime(20,  App::spanDateTime(-7 * CBO_DAY_SEC)),
				'sevendayto'	=> App::formatDateTime(21,  App::spanDateTime(-7 * CBO_DAY_SEC)),
				'monthstart'	=> App::getDateTime(22),
			);
		} else {
			$this->dates = array(
				'todayfrom'	=> App::getDateTime(3),
				'todayto'	=> App::getDateTime(3),
				'yesterdayfrom'	=> App::formatDateTime(3, App::spanDateTime(-1 * CBO_DAY_SEC)),
				'yesterdayto'	=> App::formatDateTime(3, App::spanDateTime(-1 * CBO_DAY_SEC)),
				'sevendayfrom'	=> App::formatDateTime(3,  App::spanDateTime(-7 * CBO_DAY_SEC)),
				'sevendayto'	=> App::formatDateTime(3,  App::spanDateTime(-7 * CBO_DAY_SEC)),
				'monthstart'	=> App::getDateTime(11),
			);
		}
	}

	/**
	* Add a column to the list table
	*
	* @param array $name   		The array of data for row cells
	* @param array $options   	An array of option for align, sorturl, color, editable
	*
	* @access public
	* @return none
	*/
	public function addColumn($index, $name, $width = '128', $options = array()) {
		$this->columns[] = array('index' => $index, 'name' => $name, 'width' => $width, 'options' => $options);
	}

	public function addRow($values, $options = array()) {
		$this->rows[] = array('values' => $values, 'options' => $options);
	}

	public function addFilter($name, $options, $params = array(), $type = 1) {
		$this->filters[] = array('name' => $name, 'options' => $options, 'params' => $params, 'type' => $type);
	}
	
	public function addCombo($name, $options, $params = array(), $type = 1) {
		$this->combos[] = array('name' => $name, 'options' => $options, 'params' => $params);
	}

	public function addSearchField($name, $options, $params = array()) {
		$this->searchs[] = array('name' => $name, 'options' => $options, 'params' => $params);
	}

	public function addRangeField($name, $params = array()) {
		$this->ranges[] = array('name' => $name, 'params' => $params);
		if(isset($params['time'])) $this->setDateOptions($params['time']);
	}

	public function addButton($position, $name, $value, $type = 'submit', $params = array()) {
		$this->buttons[$position][] = array('name' => $name, 'value' => $value, 'type' => $type, 'params' => $params);
	}
	
	public function addShowColumn($key, $name, $checked = false) {
		$this->showcolumns[] = array('name' => $name, 'key' => $key, 'checked' => $checked);
	}
	
	public function addHideColumn($key, $name, $checked = false) {
		$this->hidecolumns[] = array('name' => $name, 'key' => $key, 'checked' => $checked);
	}

	public function setContextMenu($title, $url = '', $sid = true, $seperator = false, $disabled = false, $script = '', $image = '', $col = false) {
		$this->addContextMenu(null, $title, $image, $url, '', $seperator, $disabled, $script, $sid, $col);
	}

	protected function addContextMenu($parent, $title, $image, $url, $target = '', $seperator = false, $disabled = false, $script = '', $sid = true, $col = false)	{
		$this->uniqid++;
		if ($parent === null) $parent = 0;
		$this->contextMenus[$parent][$this->uniqid] = array(
			'id' => $this->uniqid,
			'parent' => $parent,
			'title' => trim($title),
			'url' => $url,
			'target' => trim($target),
			'image' => $image,
			'sid' => $sid,
			'col' => $col,
			'script' => $script,
			'separator' => (($seperator == true)?true:false),
			'disabled' => (($disabled == true)?true:false)
		);
		return $this->uniqid;
	}

	public function getTemplateVars() {
		$grid = array();
		
		$grid['module'] = $this->module;
		$grid['action'] = $this->action;
		$grid['limit'] = $this->limit;
		$grid['footer']	= $this->footer;
		$grid['options'] = $this->options;
		$grid['title'] = $this->title;
		$grid['filters'] = $this->filters;
		$grid['ranges']	= $this->ranges;
		$grid['searches'] = $this->searchs;
		$grid['columns'] = $this->columns;
		$grid['buttons'] = $this->buttons;
		$grid['dates'] = $this->dates;
		$grid['id'] = $this->id;
		
		return $grid;
	}
	
}
?>
