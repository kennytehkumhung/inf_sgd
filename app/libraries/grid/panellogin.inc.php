<?php
//////////////////////////////////////////////////////////////////////
//
//  All Rights Reserved (c) 2010
//
//////////////////////////////////////////////////////////////////////
/**
* The panel library class for login. Extend from general library Login class
*
* This class is use to perform do login function where by the system perform the checking involved
* for logging into the system RCM panel
*
*
* @author   Henry Choo <kfchoo@yahoo.com>
* @version  1.0
* @package  rcmlib
* @ignore
*
*/
class PanelLogin extends Login{

	/**
	* Contructor.
	*
	*
	* @return none
	*/
	function __construct($module = null, $action = null) {
		parent::__construct($module, $action);
		$this->setUserType(CBO_LOGINTYPE_ADMIN);
	}

	/**
	* This function mainly for the login process by doing all the necessary check for the login attempt.
	*
	* @param string $username	The admin username
	* @param string $password	The password used
	*
	* @access public
	* @return boolean  True is success else return false
	*/
	public function doLogin($username, $password) {
		if(!$this->doGeneralLoginCheck()) {
			return false;
		}
		$userObject = new UserObject;
		if($userObject->getBy('$username = "'.strtolower(trim($username)).'"'))
			;
		else
			$userObject = false;
			
		if($userObject) {
			//Check to see if account status is active
			if( $userObject->status != (int) CBO_ADMINSTATUS_ACTIVE ) {
				$this->setErrorMsg(App::getText('CBO_GUI_ACCOUNTSUSPENDED', array(CBO_LOGINERROR_ACCOUNTSUSPENDED)));
				return false;
			}
				
			//Account exist and password is correct
			if($userObject->password == Login::getEncryptPassword($password)) {
				//Check to see if this account next login time is valid
				if(!$this->isNextLoginTimeValid($userObject)) {
					$this->setErrorMsg(App::getText('CBO_GUI_NEXTLOGINTEMPBLOCK', array(SYSTEM_LOGIN_LOGININTERVALUSER)));
					return false;
				}
				//Check to see if this account has been inactive for too long
				if(!$this->isAccountInactiveTooLong($userObject) ) {
					$this->doBlockAccount($userObject);
					$this->setErrorMsg(App::getText('CBO_GUI_ACCOUNTEXPIRED', array(CBO_LOGINERROR_ACCOUNTEXPIRED)));
					return false;
				}
				
				//Delete bad login record
				$this->deleteBadLogin($userObject);
				//Kill existing session tracker if any
				$this->doKillTracker($userObject);
				//Check if user password is expired
				if($this->isPasswordExpired($userObject)) {
					App::setUserSession('passwordexpired', true);
				}
				//Make a new tracker
				if($this->doMakeTracker($userObject)) {
					App::setRCMPersonalization();
					$userObject->setUserGroups();
					$userObject->lastlogin = App::getDateTime();
					$userObject->update();
					return true;
				}
			} else {
				//Account exist but password is wrong
				$this->doAddBadLogin($userObject);
				$this->isBadLoginMaxed($userObject);
				$this->setErrorMsg(App::getText('CBO_GUI_INVALIDLOGIN', array(CBO_LOGINERROR_WRONGPASSWORD)));
				return false;
			}
		} else {
			//Account not exist - increate counter in badlogin
			$this->doAddBadLogin();
			$this->isBadLoginMaxed($userObject);
			$this->setErrorMsg(App::getText('CBO_GUI_INVALIDLOGIN', array(CBO_LOGINERROR_WRONGPASSWORD)));
			return false;
		}
	}
}
?>