<?php namespace App\libraries\grid;

class PanelForm {

	/**
	* An of array inputs to be included in the form object
	*
	* @access 	private
	* @var      array
	*/
	private $inputs = array();

	/**
	* An of array buttons to be included in the form object
	*
	* @access 	private
	* @var      array
	*/
	private $buttons = array();

	/**
	* True/false if this form's encoding type is 'multipart/form-data'
	*
	* @access 	private
	* @var      array
	*/
	private $multiPart = false;

	/**
	* An of array form options such name,method,target etc
	*
	* @access 	private
	* @var      array
	*/
	private $options = array();

	/**
	* The title of the form
	*
	* @access 	private
	* @var      string
	*/
	private $formTitle = '';

	/**
	* Boolean if the form has a rich text box
	*
	* @access 	private
	* @var      boolean
	*/
	private $hasRichText = false;

	/**
	* Boolean if activate debug mode
	*
	* @access 	private
	* @var      boolean
	*/
	private $debugMode = false;

	/**
	* Boolean if activate table mode
	*
	* @access 	private
	* @var      boolean
	*/
	private $tableMode = false;

	/**
	* Boolean if to skip changing browser window title
	*
	* @access 	private
	* @var      boolean
	*/
	private $skipWindowTitle = false;

	/**
	* Array hidden input to encode if SYSTEM_ENCRYPTFORM is enabled
	*
	* @access 	private
	* @var      array
	*/
	private $hiddenParams = array();

	/**
	* Special charset to use for this form input value
	*
	* @access 	private
	* @var      array
	*/
	private $charset = '';
	
	/**
	* An array of input to be used to multilingual mode
	*
	* @access 	private
	* @var      array
	*/
	private $multilingualInputs = array();

	/**
	* Boolean to turn on and off multilingual mode in the form
	*
	* @access 	private
	* @var      boolean
	*/
	private $multilangMode = false;

	/**
	* Contructor.
	*
	* Setup the form object with default options values
	*
	* @return none
	*/
	function __construct() {
		$this->setForm();
	}

	/**
	* Setup the parameter in the form tag
	*
	* @param string $name   	The value in the name attribute of the form
	* @param string $action   	The value in the action attribute of the form
	* @param string $method    	The value in the method attribute of the form
	* @param string $target   	The value in the target attribute of the form
	*
	* @access public
	* @return none
	*/
	function setForm($name = 'spbform', $action = 'post.php', $method  = 'POST', $target = 'frameSubmit') {
		$this->options['name'] = trim($name);
		$this->options['action'] = trim($action);
		$this->options['method'] = trim($method);
		$this->options['target'] = trim($target);
	}

	/**
	* Setup the title of the form
	*
	* @param string $str   	The form title
	*
	* @access public
	* @return none
	*/
	function setFormTitle($str) {
		$this->formTitle	= $str;
	}
	
	/**
	* Set to enable or disable multilingual mode in the form
	*
	* @param string $str   	The form title
	*
	* @access public
	* @return none
	*/
	function setLangMode($toAdd) {
		if($toAdd == false)
		$this->multilangMode = true;
		if($toAdd == true)
		$this->multilangMode = false;
	}
	
	/**
	* Set to debug mode
	*
	* @param boolean $mode 	Debug mode
	*
	* @access public
	* @return none
	*/
	function setDebugMode($mode = false) {
		$this->debugMode = $mode;
	}

	/**
	* Set to table mode
	*
	* @param boolean $mode 	Debug mode
	*
	* @access public
	* @return none
	*/
	function setTableMode($mode = false) {
		$this->tableMode = $mode;
	}

	/**
	* Set to skip changing browser window title
	*
	*
	* @access public
	* @return none
	*/
	function skipWindowTitle() {
		$this->skipWindowTitle = true;
	}

	/**
	* Add button to the form
	*
	* @param string $title   		The value to appear on the button eg. OK, Cancel etc.
	* @param string $inputname   	The value in the name attribute of the button input
	* @param string $script   		The onlick event script to invoke
	* @param string $type   		Button type. Available choice are button, submit, reset
	*
	* @access public
	* @return none
	*/
	function addButton($title, $inputname, $script = '', $type = 'button') {
		$this->buttons[] = array(
			'title' => $title,
			'name' => $inputname,
			'script' => $script,
			'type' => $type
		);
	}

	/**
	* Add button to the form
	*
	* @param string $type   	The input type. Available type are 'text', 'password', 'hidden', 'textarea',
	*							'radio', 'checkbox_single', 'checkbox', 'select', 'multipleselect', 'file',
	*							'datetime', 'date', 'time', 'richtext', 'swapbox', 'droptext', 'dropselect',
	*							'rankbox', 'display', 'color'
	* @param string $title   	The title to appear beside the input
	* @param string $name    	The value in the name attribute of the input and form table row's Id
	* @param string $value   	The value in the value attribute of the form. Default empty.
	* @param array $params   	An array unique paramater applicable based on the $type above Eg. For textarea applicable parameters is 'rows' and 'cols'. Default empty.
	* @param boolean $required  If set to true is will add a mandatory market on the $title to denote this input is mandatory in the form. Default true.
	* @param string $class   	The css class to attach to the input. Default empty.
	* @param string $style   	The css style to attach to the input. Default empty.
	*
	* @access public
	* @return none
	*/
	function addInput($type, $title, $name, $value = '', $params = '', $required = true, $class = '', $style = '') {
		if ($type == 'file')
		$this->multiPart = true;
		
		if ($type == 'hidden' && defined('SYSTEM_ENCRYPTFORM') && true == SYSTEM_ENCRYPTFORM)
		$this->hiddenParams[htmlspecialchars(trim($name))] = htmlspecialchars(trim($value));
		else
		$this->inputs[] = array('type' => strtolower(trim($type)), 'title'=> trim($title), 'name' => trim($name), 'value' => trim($value), 'params' => $params, 'required' => $required, 'class' => trim($class), 'style' => $style);
	}

	/**
	* Add error message to form
	*
	* @param string $htmlContent   	The html code to use.
	* @param array 	$params   		An array unique paramater applicable to custome input type. Available are 'place', 'align'. Default empty.
	* @param string $name    		The form table row's Id
	*
	* @access public
	* @return none
	*/
	function addErrorMssg($error) {
		$error = '<font color="red">'.App::echostr($error, true, true).'</font>';
		$this->inputs[] = array('type' => 'custom', 'custom' => $error, 'params' => array(), 'name' => 'error');
	}

	/**
	* Add custome HTML content to the form
	*
	* @param string $htmlContent   	The html code to use.
	* @param array 	$params   		An array unique paramater applicable to custome input type. Available are 'place', 'align'. Default empty.
	* @param string $name    		The form table row's Id
	*
	* @access public
	* @return none
	*/
	function addCustom($htmlContent, $params = array(), $name = '', $required = true) {
		$this->inputs[] = array('type' => 'custom', 'custom' => trim($htmlContent), 'params' => $params, 'name' => trim($name), 'required' => $required);
	}

	/**
	* Auxillary function to translate string
	*
	* @param string $pString   		The string to translate
	*
	* @access private
	* @return none
	*/
	private function htmltext($pString){
		if($this->charset != '') {
			return htmlspecialchars($pString);
		} else {
			$trans=get_html_translation_table(HTML_ENTITIES);
			$trans["&"] = "&";
			return strtr($pString,$trans);
		}
	}

	/**
	* raw the HTML code for the input
	*
	* @param array $input   The input item to be drawn
	*
	* @access private
	* @return string The HTML code of the input item
	*/
	private function drawInput($input) {
		$output = '';
		$type = $input['type'];
		if (!isset($input['title'])) $title = '';
		else $title = $input['title'];
		if (!isset($input['name'])) $name = '';
		else $name = $input['name'];
		if (!isset($input['value'])) $value = '';
		else $value = $input['value'];
		if  (!isset($input['required'])) $required = '';
		else $required = $input['required'];
		if (!isset($input['params']['options'])) $options = '';
		else $options = $input['params']['options'];
		if (!isset($input['class'])) $class = '';
		else $class = $input['class'];
		if (isset($input['style'])) $style = $input['style'];
		else $style = '';
		if (! isset($input['params']['maxlength'])) $maxlength = 0;
		else $maxlength = $input['params']['maxlength'];
		if (! isset($input['params'])) $params = array();
		else $params = $input['params'];
		if (!isset($input['params']['rows'])) $rows = 5;
		else $rows = $input['params']['rows'];
		if (!isset($input['params']['cols'])) $cols = 40;
		else $cols = $input['params']['cols'];
		if (!isset($input['params']['size'])) $size = 41;
		else $size = $input['params']['size'];
		$disabled = '';
		if (isset($input['params']['disabled']) && $input['params']['disabled'] == true) $disabled = 'disabled';
		$readonly = '';
		if (isset($input['params']['readonly']) && $input['params']['readonly'] == true) $readonly = 'readonly';
		$wrap = 'on';
		if (isset($input['params']['wrap'])) $wrap = $input['params']['wrap'];
		$extra = '';
		if (isset($input['params']['extra'])) $extra = $input['params']['extra'];

		if (! isset($input['params']['width'])) $width = 228;
		else $width = $input['params']['width'];
		if (! isset($input['params']['height'])) $height = 68;
		else $height = $input['params']['height'];
		if (! isset($input['params']['rankable'])) $rankable = 0;
		else $rankable = $input['params']['rankable'];
		if (! isset($input['params']['duplicate'])) $duplicate = 0;
		else $duplicate = $input['params']['duplicate'];
		if (! isset($input['params']['eventscript'])) $eventscript = '';
		else $eventscript = $input['params']['eventscript'];
		if (! isset($input['params']['allowhtml'])) $allowhtml = false;
		else $allowhtml = $input['params']['allowhtml'];
		if (! isset($input['params']['displaytext'])) $displaytext = '';
		else $displaytext = $input['params']['displaytext'];
		if (! isset($input['params']['editbuttonaction'])) $editbuttonaction = '';
		else $editbuttonaction = $input['params']['editbuttonaction'];
		if (! isset($input['params']['hidden'])) $hidden = false;
		else $hidden = $input['params']['hidden'];
		if (! isset($input['params']['valueinputshowdependency'])) $valueinputshowdependency = false;
		else $valueinputshowdependency = $input['params']['valueinputshowdependency'];

		$fromyear = 1900;
		if (isset($input['params']['fromyear'])) $fromyear = $input['params']['fromyear'];
		$toyear = date("Y");
		if (isset($input['params']['toyear'])) $toyear = $input['params']['toyear'];

		$totalday = 30;
		if (isset($input['params']['totalday'])) $totalday = $input['params']['totalday'];

		/*
		select/checkbox_single/radio $valueinputshowdependency = array(array('1','iweight', 's'), array('1','ilogimpression', 'h'), array('2','ilogimpression', 's'), array('2','iweight', 'h'));

		multiple checkbox $valueinputshowdependency = array( 1 => array (array('1','iweight', 's'), array('0','iweight', 'h'),array('1','ilogimpression', 'h'), array('0','ilogimpression', 's')), 4 => array (array('1','iimpression', 's'), array('0','iimpression', 'h')) );
		*/

		if($hidden)
		$rowstyle = ' style="display: none"';
		else
		$rowstyle = '';

		if ($type != 'hidden' && $type != 'display' && $type != 'custom' && $type != 'richtext') {
			$reqstr = '';
			if ($required) {
				$reqstr = "&nbsp;<font color=red><b>*</b></font>";
			}
			$output .= '<tr id="'.htmlspecialchars(trim($name)).'_div" '.$rowstyle.'><td valign="top" nowrap '.(($type == 'showonly')?' height="22"':'').'>'.str_replace(' ', '&nbsp;', htmlspecialchars(trim($title))).$reqstr.'</td><td width="1" nowrap>&nbsp;&nbsp;&nbsp;</td><td valign="top" nowrap width="100%">';
		}
		/*
		if (strlen($readonly) > 0) {
			if ($type == 'datetime' || $type == 'date' || $type == 'time') {
				$type = 'text';
			}
		}
		*/

		if ($type == 'text') {
			$output .= '<input '.$disabled.' '.$readonly.' size="'.intval($size).'" '.(($maxlength > 0)?'maxlength="'.intval($maxlength).'"':'').' type="text" name="'.htmlspecialchars(trim($name)).'" value="'.$this->htmltext(trim($value)).'" class="'.htmlspecialchars(trim($class)).'" style="'.htmlspecialchars(trim($style)).'" '.$extra.'>';
		} else if ($type == 'password') {
			$output .= '<input '.$disabled.' '.$readonly.' size="'.intval($size).'" '.(($maxlength > 0)?'maxlength="'.intval($maxlength).'"':'').' type="password" name="'.htmlspecialchars(trim($name)).'" value="'.htmlspecialchars(trim($value)).'" class="'.htmlspecialchars(trim($class)).'" style="'.htmlspecialchars(trim($style)).'" '.$extra.'>';
		} else if ($type == 'hidden') {
			if(defined('SYSTEM_ENCRYPTFORM') && true == SYSTEM_ENCRYPTFORM)
			$this->hiddenParams[htmlspecialchars(trim($name))] = htmlspecialchars(trim($value));
			else
			$output .= '<input '.$disabled.' '.$readonly.' type="hidden" name="'.htmlspecialchars(trim($name)).'" value="'.htmlspecialchars(trim($value)).'">'."\n";
		} else if ($type == 'textarea') {
			$output .= '<textarea '.$disabled.' '.$readonly.' name="'.htmlspecialchars($name).'" class="'.htmlspecialchars($class).'" style="'.htmlspecialchars($style).'" rows="'.intval($rows).'" cols="'.intval($cols).'" wrap="'.$wrap.'" '.$extra.'>'.htmlspecialchars($value).'</textarea>';
		} else if ($type == 'radio') {
			// $options would be an array of $key => $val
			$br = '';
			if (count($options) >= 3) $br = '<br>';
			if (count($options) > 0) {
				foreach($options as $key => $val) {
					$key = htmlspecialchars(trim($key));
					$dependency = '';
					if (is_array($valueinputshowdependency)) {
						foreach($valueinputshowdependency as $key2 => $val2) {
							$dependency .= ',\''.$val2[0].'|'.$val2[1].'_div|'.$val2[2].'\'';
						}

						$dependency = ' onclick="showHideInputRadio( this'.$dependency.')"';
					}

					$output .= '<input '.$disabled.' '.$readonly.' type="radio" name="'.htmlspecialchars(trim($name)).'" value="'.htmlspecialchars(trim($key)).'" '.(($key == $value)?'checked':'').' class="'.htmlspecialchars(trim($class)).'" style="'.htmlspecialchars(trim($style)).'" '.$extra.$dependency.'>'.htmlspecialchars(trim($val)).$br."\n";
				}
			}
		} else if ($type == 'checkbox_single') {
			if (is_array($valueinputshowdependency)) {
				$dependency = '';
				foreach($valueinputshowdependency as $key => $val) {
					$dependency .= ',\''.$val[0].'|'.$val[1].'_div|'.$val[2].'\'';
				}

				$extra = $extra.'  onclick="showHideInputCheckbox( this'.$dependency.')"';
			}
			$output .= '<input '.$disabled.' '.$readonly.' type="checkbox" name="'.htmlspecialchars(trim($name)).'" value="1" '.((0 < $value)?'checked':'').' class="'.htmlspecialchars(trim($class)).'" style="'.htmlspecialchars(trim($style)).'" '.$extra.'> '.$displaytext;
		} else if ('checkbox' == $type) {
			//$value would be an array
			//$options would be an array of $key => $val
			//$value = explode(';', $value);
			$value = explode('||', $value);
			if (is_array($options)) {
				foreach($options as $key => $val) {
					$key = htmlspecialchars(trim($key));
					$dependency = '';
					if (is_array($valueinputshowdependency[$key])) {
						foreach($valueinputshowdependency[$key] as $key2 => $val2) {
							$dependency .= ',\''.$val2[0].'|'.$val2[1].'_div|'.$val2[2].'\'';
						}

						$dependency = ' onclick="showHideInputCheckbox( this'.$dependency.')"';
					}

					$output .= '<input '.$disabled.' '.$readonly.' type="checkbox" name="'.htmlspecialchars(trim($name)).'[]" value="'.htmlspecialchars(trim($key)).'" '.((in_array($key, $value))?'checked':'').' class="'.htmlspecialchars(trim($class)).'" style="'.htmlspecialchars(trim($style)).'" '.$extra.$dependency.'>'.htmlspecialchars(trim($val))."<br>\n";
				}
			}
		}else if ($type == 'select') {

			if (is_array($valueinputshowdependency)) {
				$dependency = '';
				foreach($valueinputshowdependency as $key => $val) {
					$dependency .= ',\''.$val[0].'|'.$val[1].'_div|'.$val[2].'\'';
				}

				$extra = $extra.'  onchange="showHideInputSelect( this'.$dependency.')"';
			}

			$output .= '<select '.$disabled.' '.$readonly.' name="'.htmlspecialchars(trim($name)).'" class="'.htmlspecialchars(trim($class)).'" style="'.htmlspecialchars(trim($style)).'" '.$extra.'>'."\n";
			if (is_array($options)) {
				foreach($options as $key => $val) {
					$key = htmlspecialchars(trim($key));
					$output .= '<option value="'.htmlspecialchars(trim($key)).'" '.(($key == $value)?'selected':'').'>'.htmlspecialchars(trim($val))."</option>\n";
				}
			}
			$output .= '</select>'."\n";
		} else if ($type == 'multipleselect') {
			if ($size == 0) $size = 5;
			$value = explode('||', $value);
			$output .= '<select '.$disabled.' '.$readonly.' name="'.htmlspecialchars(trim($name)).'[]" class="'.htmlspecialchars(trim($class)).'" style="'.htmlspecialchars(trim($style))."\" multiple size=\"".intval($size).'" '.$extra.'>'."\n";
			if (is_array($options)) {
				foreach($options as $key => $val) {
					$key = htmlspecialchars(trim($key));
					$output .= '<option value="'.htmlspecialchars(trim($key)).'" '.((in_array($key, $value))?'selected':'').'>'.htmlspecialchars(trim($val))."</option>\n";
				}
			}
			$output .= '</select>'."\n";
		} else if ($type == 'file') {
			$output .= '<input '.$disabled.' '.$readonly.' size="'.intval($size).'" type="file" name="'.htmlspecialchars(trim($name)).'" value="'.htmlspecialchars(trim($value)).'" class="'.htmlspecialchars(trim($class)).'" style="'.htmlspecialchars(trim($style)).'" '.$extra.'>';
		} else if ($type == 'datetime') { //Gecko Compliant
			$height = 214;
			$size = 19;
			$output .= '<table cellpadding=0 cellspacing=0 border=0 class="spbform"><tr valign=top><td>';
			$output .= '<input '.$disabled.' id="'.htmlspecialchars(trim($name)).'_input" '.$readonly.' type="text" size="'.$size.'" maxlength="'.$size.'" name="'.htmlspecialchars(trim($name)).'" value="'.htmlspecialchars(trim($value)).'" class="'.htmlspecialchars(trim($class)).'" style="'.htmlspecialchars(trim($style)).'" '.$extra.'>';
			$output .= '</td><td><a href="#" id="'.htmlspecialchars(trim($name)).'_trigger"><img src="resources/image/datetime.gif" border="0"></a></td></tr></table>';
			$output .= '<script type="text/javascript">
						Calendar.setup({
        				inputField     :    "'.htmlspecialchars(trim($name)).'_input",
        				ifFormat       :    "%Y-%m-%d %H:%M:%S",
        				button         :    "'.htmlspecialchars(trim($name)).'_trigger",
        				align          :    "Tl",
        				showsTime      :    true,
        				timeFormat     :	"24",
        				singleClick    :    true
        				});
        				</script>';
		}else if ($type == 'datehourmin') { //Gecko Compliant
			$height = 214;
			$size = 19;
			$output .= '<table cellpadding=0 cellspacing=0 border=0 class="spbform"><tr valign=top><td>';
			$output .= '<input '.$disabled.' id="'.htmlspecialchars(trim($name)).'_input" '.$readonly.' type="text" size="'.$size.'" maxlength="'.$size.'" name="'.htmlspecialchars(trim($name)).'" value="'.htmlspecialchars(trim($value)).'" class="'.htmlspecialchars(trim($class)).'" style="'.htmlspecialchars(trim($style)).'" '.$extra.'>';
			$output .= '</td><td><a href="#" id="'.htmlspecialchars(trim($name)).'_trigger"><img src="resources/image/datetime.gif" border="0"></a></td></tr></table>';
			$output .= '<script type="text/javascript">
						Calendar.setup({
        				inputField     :    "'.htmlspecialchars(trim($name)).'_input",
        				ifFormat       :    "%Y-%m-%d %H:%M",
        				button         :    "'.htmlspecialchars(trim($name)).'_trigger",
        				align          :    "Tl",
        				showsTime      :    true,
        				timeFormat     :	"24",
        				singleClick    :    true
        				});
        				</script>';
		} else if ($type == 'date') { //Gecko Compliant
			$size = 10;
			$height = 184;
			$output .= '<table cellpadding=0 cellspacing=0 border=0 class="spbform"><tr valign=top><td>';
			$output .= '<input '.$disabled.' id="'.htmlspecialchars(trim($name)).'_input" '.$readonly.' type="text" size="'.$size.'" maxlength="'.$size.'" name="'.htmlspecialchars(trim($name)).'" value="'.htmlspecialchars(trim($value)).'" class="'.htmlspecialchars(trim($class)).'" style="'.htmlspecialchars(trim($style)).'">';
			$output .= "</td><td><a href=\"#\" id=\"".htmlspecialchars(trim($name))."_trigger\"><img src=\"resources/image/datetime.gif\" border=\"0\"></a></td></tr></table>";
			$output .= '<script type="text/javascript">
						Calendar.setup({
        				inputField     :    "'.htmlspecialchars(trim($name)).'_input",
        				ifFormat       :    "%Y-%m-%d",
        				button         :    "'.htmlspecialchars(trim($name)).'_trigger",
        				align          :    "Tl",
        				singleClick    :    true
        				});
        				</script>';
		} else if ($type == 'time') { //Gecko Compliant
			$size = 8;
			$height = 50;
			$output .= '<table cellpadding=0 cellspacing=0 border=0 class="spbform"><tr valign=top><td>';
			$output .= '<input '.$disabled.' id="'.htmlspecialchars(trim($name)).'_input" '.$readonly.' type="text" size="'.$size.'" maxlength="'.$size.'" name="'.htmlspecialchars(trim($name)).'" value="'.htmlspecialchars(trim($value)).'" class="'.htmlspecialchars(trim($class)).'" style="'.htmlspecialchars(trim($style)).'">';
			$output .= "</td><td><a href=\"#\" id=\"".htmlspecialchars(trim($name))."_trigger\"><img src=\"resources/image/clock.gif\"></a></td></tr></table>";
			$output .= '<script type="text/javascript">
						Calendar.setup({
        				inputField     :    "'.htmlspecialchars(trim($name)).'_input",
        				ifFormat       :    "%H:%M:%S",
        				button         :    "'.htmlspecialchars(trim($name)).'_trigger",
        				align          :    "Tl",
        				showsTime      :    true,
        				timeFormat     :	"24",
        				timeOnly       :    true,
        				singleClick    :    true
        				});
        				</script>';
		} else if ($type == 'richtext') { //Gecko Compliant
			$this->hasRichText = true;
			$reqstr = '';
			if ($required) {
				$reqstr = "&nbsp;<font color=red><b>*</b></font>";
			}

			$output .= '<tr id="'.htmlspecialchars(trim($name)).'_div"'.$rowstyle.'><td valign="top" colspan="3" height="5">';

			$output .= '<table cellpadding=0 cellspacing=0 border=0 width=100% class="spbform"><tr><td valign="top" height="5"></td></tr>';
			$output .= '<tr><td valign="top">'.str_replace(' ', '&nbsp;', htmlspecialchars(trim($title))).$reqstr;
			$output .= '</td></tr><tr><td valign="top">';
			$output .= '<textarea id="'.$name.'" name="'.$name.'" style="background-color: #ffffff; border: 1px solid 000000; height: '.$height.'px; width:90%" rows="24" cols="80">'.htmlspecialchars($value).'</textarea>';
			$output .= "</td></tr></table>\n";
			$output .= "</td></tr>\n";

			$output .= '
			<script type="text/javascript">
				var editor_'.$name.' = null;
				htmlarea.push ("'.$name.'");
				function initEditor_'.$name.'()
				{
					editor_'.$name.' = new HTMLArea("'.$name.'");
					var config = new HTMLArea.Config();
					config.toolbar = [
						[
							"fontname", "space","fontsize", "space","formatblock", "space", "separator", "bold", "italic", "underline",  "strikethrough", "separator", "subscript", "superscript", "separator", "htmlmode", "separator", "popupeditor", "linebreak",
							"forecolor", "hilitecolor", "separator", "inserthorizontalrule", "createlink", "insertimage", "inserttable", "separator", "justifyleft", "justifycenter", "justifyright", "justifyfull", "separator", "lefttoright", "righttoleft", "separator", "insertorderedlist", "insertunorderedlist", "outdent", "indent", "separator", "copy", "cut", "paste", "separator", "undo", "redo", "linebreak"
						]
					];

					editor_'.$name.'.config = config;
					if(!is_win98) {
						editor_'.$name.'.registerPlugin(TableOperations);
						editor_'.$name.'.registerPlugin("ContextMenu");
					}
					editor_'.$name.'.config.pageStyle = "@import url(custom.css);";
					setTimeout(function() {
						editor_'.$name.'.generate();
					}, 500);
				  	return false;
				}

				if(document.getElementById("'.$name.'"))
				initEditor_'.$name.'();
			</script>';

		} else if ($type == 'swapbox') { //Gecko Compliant

			if (isset($input['params']['values'])) $values = $input['params']['values'];
			else $values = array();
			if(isset($input['params']['fixedids'])) {
				$selection_id = $input['params']['fixedids']['selection'];
				$selected_id = $input['params']['fixedids']['selected'];
			}else {
				$selection_id = $this->getUniqueId();
				$selected_id = $this->getUniqueId();
			}

			$output .= '<table cellpadding="1" cellspacing="0" border="0" class="spbform">
				<tr><td valign="top">&nbsp;&nbsp;'.App::getText('CBO_GUI_AVAILABLE').'<br>
				<select name="'.$selection_id.'" size="'.$size.'" style="width: '.$width.'px" ondblclick="swpAddSwapValue(this,\''.$selected_id.'\',\''.$name.'\','.$duplicate.')" id="'.$selection_id.'" style="'.htmlspecialchars($style).'">';
			if(is_array($options))
			foreach($options as $key => $val) {
				$key = htmlspecialchars(trim($key));
				$output .= "<option value=\"".htmlspecialchars(trim($key))."\">".htmlspecialchars(trim($val))."</option>\n";
			}

			$output .= '</select></td><td valign="middle"><div onclick="swpAddSwapValue(document.getElementById(\''.$selection_id.'\'),\''.$selected_id.'\',\''.$name.'\','.$duplicate.')" title="Add" style="position: relative"><img src="resources/image/right.png"></div><br><div onclick="swpRemoveSwapValue(document.getElementById(\''.$selected_id.'\'),\''.$selected_id.'\',\''.$name.'\')" title="Remove" style="position: relative"><img src="resources/image/left.png"></div><div style="position: relative"><img alt="" src="resources/image/spacer.gif" border=0></div></td><td valign="top">&nbsp;&nbsp;'.App::getText('CBO_GUI_CURRENT').'<br>
				<select name="'.$selected_id.'" size="'.$size.'" style="width: '.$width.'px" ondblclick="swpRemoveSwapValue(this,\''.$selected_id.'\',\''.$name.'\')" id="'.$selected_id.'" style="'.htmlspecialchars($style).'">';

			$newvalue = '';
			if(is_array($values))
			foreach($values as $key => $val) {
				$output .= "<option value=\"".htmlspecialchars(trim($key))."\">".htmlspecialchars(trim($val))."</option>\n";
				if (isset($newvalue)) $newvalue = empty($newvalue)?$key:$newvalue.",".$key;
				else $newvalue = $key;
			}

			$output .= '</select></td><td valign="middle">';

		if($rankable)
			$output .= '<div onclick="swpRank(0,\''.$selected_id.'\',\''.$name.'\')" title="Move Up" style="position: relative"><img src="resources/image/up.gif"></div><br><div onclick="swpRank(1,\''.$selected_id.'\',\''.$name.'\')" title="Move Down" style="position: relative"><img src="resources/image/down.gif"></div>';

			$output .= '</td></tr></table><input type="hidden" name="'.$name.'" value="'.$newvalue.'">';

		} else if ($type == 'droptext') { //Gecko Compliant
			$column_id = $this->getUniqueId();

			$output .= '<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="1%">
						<input readonly size="'.intval($size).'" type="text" name="'.$column_id.'" value="'.htmlspecialchars(trim($displaytext)).'" class="'.htmlspecialchars(trim($class)).'" style="'.htmlspecialchars(trim($style)).'">
					</td>
					<td width="1%" valign="top">
						<div '.$eventscript.' title="Add" style="position: relative"><img src="resources/image/new.gif"></div>
					</td>
					<td width="1%" valign="top">
						<div onclick="removeValue(\''.$column_id.'\',\''.$name.'\')" title="Remove" style="position: relative"><img src="resources/image/delete.gif"></div><div style="position: relative"><img alt="" src="resources/image/spacer.gif" border=0></div>
					</td>';

			if(!empty($editbuttonaction))
			$output .= '<td width="1%" valign="top">
						<div onclick="editItem(\''.htmlspecialchars($editbuttonaction).'\',\''.$name.'\')" title="Edit" style="position: relative"><img src="resources/image/edit.gif"></div>
					</td>';

			$output .= '
					<td width="96%">&nbsp;</td>
				</tr>
				<input type="hidden" name="'.$name.'" value="'.$value.'" columnid="'.$column_id.'">
			</table>';

		} else if ($type == 'dropselect') { //Gecko Compliant
			if (isset($input['params']['values'])) $values = $input['params']['values'];
			else $values = array();
			$column_id = $this->getUniqueId();

			$output  .= '<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="1%">
						<select name="'.$column_id.'" size="'.$size.'" style="width: '.$width.'px" id="'.$column_id.'" ondblclick="swpRemoveSwapValue(this,\''.$column_id.'\',\''.$name.'\')">';

			$newvalue = '';
			foreach($values as $key => $val) {
				$output .= "<option value=\"".htmlspecialchars(trim($key))."\">".htmlspecialchars(trim($val))."</option>\n";
				if (strlen($newvalue) > 0) $newvalue .= ',';
				$newvalue .= trim($key);
			}

			$output .= '
						</select>
					</td>
					<td>';

			$output .= '<table cellpadding="0" cellspacing="0" border="0">
							<td>&nbsp;</td>
							    <td>
							    	<div '.$eventscript.' title="Add" style="position: relative"><img src="resources/image/plus.gif"></div>
								</td>
							</tr>';

			if($rankable)
			$output .= '
							<tr>
								<td>&nbsp;</td>
								<td>
									<div onclick="swpRank(0,\''.$column_id.'\',\''.$name.'\')" title="Move Up" style="position: relative"><img src="resources/image/up.gif"></div>
								</td>
							</tr>
							<tr>
								<td>&nbsp;</td>
								<td>
									<div onclick="swpRank(1,\''.$column_id.'\',\''.$name.'\')" title="Move Down" style="position: relative"><img src="resources/image/down.gif"></div>
								</td>
							</tr>';

			$output .= '
							<tr>
								<td>&nbsp;</td>
								<td>
									<div onclick="swpRemoveSwapValue(document.getElementById(\''.$column_id.'\'),\''.$column_id.'\',\''.$name.'\');" title="Remove" style="position: relative"><img src="resources/image/minus.gif"></div><div style="position: relative"><img alt="" src="resources/image/spacer.gif" border=0></div>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<input type="hidden" name="'.$name.'" value="'.$newvalue.'" columnid="'.$column_id.'">
			</table>';
		} else if ($type == 'rankbox') { //Gecko Compliant
			$newvalue = '';
			if (isset($input['params']['values'])) $values = $input['params']['values'];
			else $values = array();
			$column_id = $this->getUniqueId();

			$output  .= '<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="1%">';

			//$output  .= '<select name="'.$column_id.'" size="'.$size.'" style="width: '.$width.'px" id="'.$column_id.'" ondblclick="swpRemoveSwapValue(this,\''.$column_id.'\',\''.$name.'\')">';
			$output  .= '<select name="'.$column_id.'" size="'.$size.'" style="width: '.$width.'px" id="'.$column_id.'">';

			foreach($values as $key => $val) {
				$val = explode('||', $val);
				$output .= "<option value=\"".htmlspecialchars(trim($val[0]))."\">".htmlspecialchars(trim($val[1]))."</option>\n";
				if (isset($newvalue)) $newvalue = $newvalue.",".$val[0];
				else $newvalue = $val[0];
			}
			$output .= '
						</select>
					</td>
					<td>
						<table cellpadding="0" cellspacing="0" border="0">
							<tr>
								<td>&nbsp;</td>
								<td>
									<div onclick="swpRank(0,\''.$column_id.'\',\''.$name.'\')" title="Move Up" style="position: relative"><img src="resources/image/up.gif"></div>
								</td>
							</tr>
							<tr>
								<td>&nbsp;</td>
								<td>
									<div onclick="swpRank(1,\''.$column_id.'\',\''.$name.'\')" title="Move Down" style="position: relative"><img src="resources/image/down.gif"></div>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<input type="hidden" name="'.$name.'" value="'.$newvalue.'" columnid="'.$column_id.'">
			</table>';
		} else if ($type == 'showonly') {
			if ($allowhtml == false)
			$output .= $this->htmltext(trim($value));
			else $output .= trim($value);
		} else if ($type == 'display') {
			$fieldset1 = '';
			$fieldset2 = '';
			if (isset($params['box']) && strlen($title) > 0) {
				$fieldset1 = '<table cellpadding=0 cellspacing=0 border=0><tr><td><fieldset class="fieldset">';
				$fieldset2 = '</fieldset></td></tr></table>';
			}
			$title = trim($title);
			if (strlen($title) == 0) $title = '&nbsp;';
			else {
				if ($allowhtml == false) {
					$title = htmlspecialchars($title);
				}
				if (isset($params['box'])) {
					$title = '<table cellpadding=2 cellspacing=0 border=0 class="spbform"><tr valign=top><td width="1"><img src="resources/image/info.gif"></td><td>'.$title.'</td></tr></table>';
				} else {
					$title = $title;
				}

				if (isset($params['bold'])) {
					$title = '<b>'.$title.'</b>';
				}
			}
			if (isset($params['place']) && $params['place'] == 'left') {
				$output = '<tr id="'.htmlspecialchars(trim($name)).'_div"'.$rowstyle.'><td valign="top" align="'.$params['align'].'">'.$fieldset1.trim($title).$fieldset2.'</td><td width="1"></td><td valign="top">'.htmlspecialchars(trim($value)).'</td></tr>';
			} else if (isset($params['place']) && $params['place'] == 'right') {
				$output = '<tr id="'.htmlspecialchars(trim($name)).'_div"'.$rowstyle.'><td valign="top">&nbsp;</td><td width="1"></td><td valign="top" align="'.$params['align'].'">'.$fieldset1.trim($title).$fieldset2.'</td></tr>';
			} else {
				$output = '<tr id="'.htmlspecialchars(trim($name)).'_div"'.$rowstyle.'><td valign="top" colspan="3" align="'.((isset($params['align']))?$params['align']:'').'">'.$fieldset1.trim($title).$fieldset2.'</td></tr>';
			}
			$output .= "\n";
		} else if ($type == 'custom') {
			if (isset($params['place']) && $params['place'] == 'left') {
				$output = '<tr id="'.htmlspecialchars(trim($name)).'_div"'.$rowstyle.'><td valign="top" align="'.$params['align'].'">'.$input['custom'].'</td><td width="1"></td><td valign="top">&nbsp;</td></tr>';
			} else if (isset($params['place']) && $params['place'] == 'right') {
				$output = '<tr id="'.htmlspecialchars(trim($name)).'_div"'.$rowstyle.'><td valign="top">'.str_replace(' ', '&nbsp;', htmlspecialchars(trim($name))).(($required)?'&nbsp;<font color=red><b>*</b></font>':'').'</td><td width="1"></td><td valign="top" align="'.(isset($params['align'])?$params['align']:'').'">'.$input['custom'].'</td></tr>';
			} else {
				$output = '<tr id="'.htmlspecialchars(trim($name)).'_div"'.$rowstyle.'><td valign="top" colspan="3">'.$input['custom'].'</td></tr>';
			}
		} else if ($type == 'color') {
			$size = 18;
			$height = 184;
			$output .= '<table cellpadding=0 cellspacing=0 border=0 class="spbform"><tr valign=top><td>';
			$output .= '<input '.$disabled.' '.$readonly.' type="text" size="'.$size.'" maxlength="'.$size.'" name="'.htmlspecialchars(trim($name)).'" value="'.htmlspecialchars(trim($value)).'" class="'.htmlspecialchars(trim($class)).'" style="'.htmlspecialchars(trim($style)).'">';
			$output .= "</td><td><a href=\"#\" onclick=\"pickColor('".htmlspecialchars(trim($name))."')\"><img src=\"resources/image/color.gif\"></a></td></tr></table>";
		} else if ($type == 'daymonthyear') {

			if(strlen($value) > 0) {
				$date = explode("-", $value);
			}else
				$date=explode('-','0000-00-00');

			$days[0] = App::getText('CBO_GUI_DAY');
			for($i = 1; $i < 32; $i++) {
				$days[$i] = $i;
			}

			$output .= '<select name="'.htmlspecialchars(trim($name)).'[iday]">';
			foreach ($days as $key => $val) {
				if($date[2] == $key) $selected = 'selected';
					else $selected = '';
				$output .= '<option '.$selected.' value="'.$key.'">'.$val.'</option>';
			}
			$output .= '</select>';

			$months = App::getSysVariable('shortmonth', true);
			$output .= '<select name="'.htmlspecialchars(trim($name)).'[imonth]">';
			$output .= '<option value="0">'.App::getText('CBO_GUI_MONTH').'</option>';
			foreach ($months as $key => $val) {
				if($date[1] == $key) $selected = 'selected';
					else $selected = '';
				$output .= '<option '.$selected.' value="'.$key.'">'.$val.'</option>';
			}
			$output .= '</select>';

			$output .= '<select name="'.htmlspecialchars(trim($name)).'[iyear]">';
			$output .= '<option value="0">'.App::getText('CBO_GUI_YEAR').'</option>';

			for($i = $toyear; $i >= $fromyear; $i--)
			{
				if($date[0] == $i) $selected = 'selected';
				else $selected = '';

				$output .= '<option '.$selected.' value="'.$i.'">'.$i.'</option>';
			}
			$output .= '</select>';

		} else if ($type == 'monthyear') {

			if(strlen($value) > 0) {
				$date = explode('-', $value);
			}else
				$date = explode('-','0000-00-00');

			$months[0] = App::getText('CBO_GUI_MONTH');
			for($i = 1; $i < 13; $i++) {
				$months[$i] = $i;
			}

			$output .= '<select  name="'.$name.'[imonth]">';
			foreach ($months as $key => $val) {
				if($date[0] == $key) $selected = 'selected';
					else $selected = '';
				$output .= '<option '.$selected.' value="'.$key.'">'.$val.'</option>';
			}
			$output .= '</select>';

			$output .= '<select  name="'.$name.'[iyear]">';
			$output .= '<option value="0">'.App::getText('CBO_GUI_YEAR').'</option>';

			for($i = $fromyear; $i <= $toyear; $i++)
			{
				if($date[1] == $i) $selected = 'selected';
				else $selected = '';

				$output .= '<option '.$selected.' value="'.$i.'">'.$i.'</option>';
			}
			$output .= '</select>';

		} else if ($type == 'dayhourmin') {

			if(strlen($value) > 0) {
				$days = explode("-", $value);
			}else
				$days = explode('-','0-0-0');

			$output .= '<select name="'.htmlspecialchars(trim($name)).'[iday]"  id="'.htmlspecialchars(trim($name)).'day_input" '.$extra.'>';
			for($i = 0; $i <= $totalday ; $i++) {
				if($days[0] == $i) $selected = 'selected';
					else $selected = '';
				$output .= '<option '.$selected.' value="'.$i.'">'.$i.'</option>';
			}
			$output .= '</select> ' . App::getText('CBO_GUI_DAY');

			$output .= ' <select name="'.htmlspecialchars(trim($name)).'[ihour]" id="'.htmlspecialchars(trim($name)).'hour_input" '.$extra.'>';
			for($i = 0; $i < 24 ; $i++) {
				if($days[1] == $i) $selected = 'selected';
					else $selected = '';
				$output .= '<option '.$selected.' value="'.$i.'">'.$i.'</option>';
			}
			$output .= '</select> ' . App::getText('CBO_GUI_HOUR');

			$output .= ' <select name="'.htmlspecialchars(trim($name)).'[imin]" id="'.htmlspecialchars(trim($name)).'min_input" '.$extra.'>';

			for($i = 0; $i < 60 ; $i++) {
				if($days[2] == $i) $selected = 'selected';
				else $selected = '';

				$output .= '<option '.$selected.' value="'.$i.'">'.$i.'</option>';
			}
			$output .= '</select> ' . App::getText('CBO_GUI_MINUTE');

		}

		if ($type != 'hidden' && $type != 'display' && $type != 'custom' && $type != 'richtext') {
			if (isset($params['trail'])) $output .= $params['trail'];
			$output .= "</td></tr>\n";
		}

		return $output;
	}



	/**
	* This draw out the form input and attached the required JS and CSS file in to the Gui Object
	*
	* @param object $gui   			The GUI object using the form
	* @param string $mode   		Define the mode type to use in drawing. Default empty.
	* @param string $charset   		The charset for this form input
	*
	* @access public
	* @return none			 	The HTML of the form
	*/
	function draw($gui, $mode = '', $charset = '') {
		if(strlen($charset) > 0) {
			$this->charset = $charset;
			$gui->setCharset($charset);
		} else {
			$this->charset = SYSTEM_CHARSET;
			$gui->setCharset(SYSTEM_CHARSET);
		}
		$gui->setCSSFiles('resources/style/form.css');
		$gui->setCSSFiles('resources/style/calendar.css');
		$gui->setJSFiles('resources/script/preload.inc.js');
		$gui->setJSFiles('resources/script/form.inc.js');
		$gui->setJSFiles('resources/script/calendar/calendar.js');
		$gui->setJSFiles('resources/script/calendar/calendar_setup.js');
		$gui->setJSFiles('resources/script/calendar/calendar_lang_en.js');
		if($mode == 'tree') {
			$gui->setCSSFiles('resources/style/treeinform.css');
			$gui->setJSFiles('resources/script/tree.inc.js');
			$gui->setJSFiles('resources/script/tree_preload.inc.js');
		}

		$str = '<script for="color" event="onscriptletevent(name, data)">
				color.style.display = "none";
				inputColor(whatColor ,data);
				</script>';

		//clear parent refresh interval
		$str .= "\n".'<script type="text/javascript">'; 
		$str .= "\n".'parent.clearInterval(parent.refreshTime);';
		$str .= "\n".'</script>';

		if($this->hasRichText) {
			$str .= "\n".'<script type="text/javascript">';
			$str .= "\n".'_editor_url = "'.dirname(App::getBaseURL()).'/resources/script/htmlarea/";';
			$str .= "\n".'_editor_lang = "en";';
			$str .= "\n".'</script>';
			//$str .= "\n".'<script type="text/javascript" src="resources/script/htmlarea/htmlarea.js"></script>';
			$str .= "\n".'<script type="text/javascript" src="resources/script/htmlarea.inc.js"></script>';

			$str .= "\n".'<script type="text/javascript">';
	  		$str .= "\n".'HTMLArea.loadPlugin("TableOperations");';
	  		$str .= "\n".'HTMLArea.loadPlugin("ContextMenu");';
			$str .= "\n".'</script>';
		}
		$gui->setHeaderString($str);
		//$gui->setOnBodyTag('onload="setProcess()"');
		$buf = '';
		if(!$this->skipWindowTitle)
		$buf .= '<script>
				setTitleFromGridOrForm("'.$this->formTitle.' - '.App::getText('CBO_GUI_SYSTEMNAME').'");
				</script>';

		$buf .= '<div onmouseover="hideMenuBar()" id="floater" style="top: 0px; left: 0px; position: absolute; padding: 0px; border: lightgrey 0px solid; height: 100%; width: 100%; background-color: buttonface; z-index: 1; filter:alpha(opacity=0); -moz-opacity:.0; opacity:.0;">
				<iframe id="frameFloater" name="frameFloater" src="resources/script/index.html" border="0" marginwidth="0" marginheight="0" hspace="0" vspace="0" frameborder="0" scrolling="auto" class="appframe" style="filter:alpha(opacity=0); -moz-opacity:.0; opacity:.0; width: 100%; height:100%px; position:absolute"></iframe>
				</div>';
		$buf .= '<div style="z-index: 2; overflow: auto; width: 100%; position: relative; height: 100%; padding: 0px; margin: 0px;"><form name="'.$this->options['name'].'" id="'.$this->options['name'].'" action="'.$this->options['action'].'" method="'.$this->options['method'].'" target="'.$this->options['target'].'" '.(($this->multiPart == true)?'enctype="multipart/form-data"':'').'>';
		$buf .= '<fieldset>';
		if(!empty($this->formTitle))
		$buf .= '<legend class="legend">&nbsp;'.App::upperCase($this->formTitle).'&nbsp;&nbsp;</legend>';
		$buf .= '<table border="0" cellspacing="0" cellpadding="2" width="100%" class="spbform">'."\n";
		if (is_array($this->inputs)) {
			foreach ($this->inputs as $input) {
				$buf .= $this->drawInput($input);
			}
		}
		if (is_array($this->hiddenParams) && count($this->hiddenParams) > 0) {
			$buf .= '<input type="hidden" name="sbhash" value="'.App::encryptQueryStr(App::formatParamsToUrl($this->hiddenParams)).'">'."\n";
		}
		$buf .= '</table></fieldset>';
		$buf .= '<table border="0" cellspacing="0" cellpadding="2" width="100%"><tr><td align="right">';
		foreach ($this->buttons as $button) {
			$buf .= '<input id="btt" class="formButton" type="'.$button['type'].'" name="'.htmlspecialchars(trim($button['name'])).'" value="'.htmlspecialchars(trim($button['title'])).'" onclick="'.htmlspecialchars(trim($button['script'])).'"> ';
		}
		$buf .= '</td></tr></table></form>';

		if($this->tableMode)
		$buf .= "\n".'<iframe id="frameSubmit" name="frameSubmit" src="resources/script/index.html" border="1" marginwidth="0" marginheight="0" hspace="0" vspace="0" frameborder="0" class="tblSummary" style="width: 100%; height:100%; overflow:auto; position:relative; z-index:0; visibility:visible;"></iframe>';
		else if($this->debugMode)
		$buf .= "\n".'<iframe id="frameSubmit" name="frameSubmit" src="resources/script/index.html" border="0" marginwidth="0" marginheight="0" hspace="0" vspace="0" frameborder="0" scrolling="yes" class="appframe" style="width: 100%; height:130px; overflow:auto; position:relative; z-index:0; visibility:visible; filter:alpha(opacity=50); -moz-opacity:.50; opacity:.50;"></iframe>';
		else
		$buf .= "\n".'<iframe id="frameSubmit" name="frameSubmit" src="resources/script/index.html" border="0" marginwidth="0" marginheight="0" hspace="0" vspace="0" frameborder="0" scrolling="auto" class="appframe" style="width: 0px; height:0px; position:absolute; z-index:0; visibility:hidden"></iframe>';

		$buf .= '</div>';
		$gui->setContent($buf);
		return $gui;
	}

	/**
	* Generate unique id for javascript.
	*
	* @param integer $count Unique ID length
	*
	* @access	private
	* @return	string
	*/
	private function getUniqueId($count = 8) {
		mt_srand((double)microtime()*1000000);
		$key = "";
		for ($i=0; $i<$count; $i++) {
			$c = mt_rand(0,2);
			if ($c==0) {
		 		$key .= chr(mt_rand(65,90));
			} elseif ($c==1) {
				$key .= chr(mt_rand(97,122));
			} else {
		 		$key .= mt_rand(0,9);
			}
		}
		return 'spb'.$key;
	}
	
	/**
	* Set the form to enable multilingual mode
	*
	* @param array $inputs   	An array of input to be use in multilingual mode
	*
	* @access public
	* @return none
	*/
	function setMultilingual($inputs) {
		$this->multilingualInputs = $inputs;
	}
	
	public function getTemplateVars() {
		$form = array();
		
		$form['title'] = $this->formTitle;
		$form['multipart'] = $this->multiPart;
		$form['inputs'] = $this->inputs;
		
		$form['hiddenParams'] = false;
		if (is_array($this->hiddenParams) && count($this->hiddenParams) > 0) {
			$form['hiddenParams'] = App::encryptQueryStr(App::formatParamsToUrl($this->hiddenParams));
		}
		
		return $form;
	}

}

?>
