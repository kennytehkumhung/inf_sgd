<?php
//////////////////////////////////////////////////////////////////////
//
//  All Rights Reserved (c) 2010
//
//////////////////////////////////////////////////////////////////////
/**
* The web GUI object class for user panel
*
* This class is use to manage the HTML and layout for user panel
* You may customise the overall outlook of your panel by over writting the available method in the parent class.
*
* @author   Henry Choo <kfchoo@yahoo.com>
* @version  1.0
* @package  rcmlib
* @ignore
*/
class PanelGuiGrid extends Gui {

	protected $columns = array();

	protected $rows = array();

	protected $filters = array();

	protected $buttons = array();

	protected $searchform = '';

	protected $title = '';

	protected $hasNavigation = false;

	protected $navigationUrl = '';

	protected $hasCSV = false;

	protected $csvUrl = '';
	
	protected $total = 1;

	protected $limit = 0;

	protected $start = 0;

	protected $searchs = array();

	protected $ranges = array();
	
	protected $showcolumns = array();
	
	protected $hidecolumns = array();

	protected $hasContectMenu = true;

	protected $params = array();

	protected $sid = '';

	protected $miniforms = array();

	protected $dblClickUrl = '';

	protected $dblClickParam = '';

	protected $dblClickScript = '';

	protected $refreshUrl = '';

	protected $contextMenus = array();

	protected $uniqid = 0;

	protected $hasAutoRefresh = 0;

	protected $autoRefreshSecond = 0;

	protected $colorLegend = array();

	protected $all = false; //all page

	protected $playSoundAlert = false;

	protected $soundAlert = '';

	protected $showRowMarker = true;
	
	/**
	* Flag to indicate that this grid is a pop-up and meant for
	* selection into a form droptext/dropselect input
	*
	* @access	protected
	* @var      boolean
	*/
	protected $forSelection = false;


	/**
	* Boolean if activate debug mode
	*
	* @access 	private
	* @var      boolean
	*/
	private $debugMode = false;

	/**
	* Contructor.
	*
	* @return none
	*/
	function __construct() {
	}

	public function setupGrid($total, $start = 0, $limit = 25, $options = array()) {
		$this->total = $total;
		$this->start = $start;
		$this->limit = $limit;
		$this->options = $options;
	}

	public function setRowMarker($status) {
		$this->showRowMarker = $status;
	}

	/**
	* Set to debug mode
	*
	* @param boolean $mode 	Debug mode
	*
	* @access public
	* @return none
	*/
	function setDebugMode($mode = false) {
		$this->debugMode = $mode;
	}

	public function setColorLegend($options) {
		$this->colorLegend = $options;
	}

	public function setDoubleClickUrl($url, $param, $script = '') {
		$this->dblClickUrl = $url;
		$this->dblClickParam = $param;
		$this->dblClickScript = $script;
	}

	public function setNavigationUrl($url, $all = false) {
		$this->hasNavigation = true;
		$this->navigationUrl = $url;
		$this->all = $all;
	}

	public function setRefreshUrl($url) {
		$this->refreshUrl =  $url;
	}
	
	public function setCSV($url) {
		$this->hasCSV = true;
		$this->csvUrl = $url;
	}

	public function setTitle($title) {
		$this->title = $title;
	}

	public function setParams($params, $position = 0) {
		if(empty($position)) {
			$this->params[1] = $params;
			$this->params[2] = $params;
		} else {
			$this->params[$position] = $params;
		}
	}

	public function setAutoRefresh($second) {
		$this->hasAutoRefresh = true;
		$this->autoRefreshSecond = $second;
	}
	
	/**
	* Set the grid go into selection mode for dropping into another form
	*
	*
	* @access public
	* @return none
	*/
	public function setForSelection() {
		$this->forSelection = true;
	}

	/**
	* Add a column to the list table
	*
	* @param array $name   		The array of data for row cells
	* @param array $options   	An array of option for align, sorturl, color, editable
	*
	* @access public
	* @return none
	*/
	public function addColumn($index, $name, $width = '128', $options = array()) {
		$this->columns[] = array('index' => $index, 'name' => $name, 'width' => $width, 'options' => $options);
	}

	public function addRow($values, $options = array()) {
		$this->rows[] = array('values' => $values, 'options' => $options);
	}

	public function addFilter($name, $options, $params = array(), $type = 1) {
		$this->filters[] = array('name' => $name, 'options' => $options, 'params' => $params, 'type' => $type);
	}

	public function addSearchField($name, $options, $params = array()) {
		$this->searchs[] = array('name' => $name, 'options' => $options, 'params' => $params);
	}

	public function addRangeField($name, $params = array()) {
		$this->ranges[] = array('name' => $name, 'params' => $params);
	}

	public function addButton($position, $name, $value, $type = 'submit', $params = array()) {
		$this->buttons[$position][] = array('name' => $name, 'value' => $value, 'type' => $type, 'params' => $params);
	}
	
	public function addShowColumn($key, $name, $checked = false) {
		$this->showcolumns[] = array('name' => $name, 'key' => $key, 'checked' => $checked);
	}
	
	public function addHideColumn($key, $name, $checked = false) {
		$this->hidecolumns[] = array('name' => $name, 'key' => $key, 'checked' => $checked);
	}

	public function setContextMenu($title, $url = '', $sid = true, $seperator = false, $disabled = false, $script = '', $image = '', $col = false) {
		$this->addContextMenu(null, $title, $image, $url, '', $seperator, $disabled, $script, $sid, $col);
	}

	protected function addContextMenu($parent, $title, $image, $url, $target = '', $seperator = false, $disabled = false, $script = '', $sid = true, $col = false)	{
		$this->uniqid++;
		if ($parent === null) $parent = 0;
		$this->contextMenus[$parent][$this->uniqid] = array(
			'id' => $this->uniqid,
			'parent' => $parent,
			'title' => trim($title),
			'url' => $url,
			'target' => trim($target),
			'image' => $image,
			'sid' => $sid,
			'col' => $col,
			'script' => $script,
			'separator' => (($seperator == true)?true:false),
			'disabled' => (($disabled == true)?true:false)
		);
		return $this->uniqid;
	}

	protected function getContextMenu($parent = 0) {
		$output = '';
		foreach ($this->contextMenus as $pid => $items) {
			if ($pid == $parent) {
				$count = 0;
				foreach ($items as $id => $item) {
					$href = '';
					$img = '';
					$target = '';
					$sid = '';
					$col = array('', '');
					$script = '';
					if (!empty($item['url'])) $href = $item['url'];
					if (!empty($item['image']))	$img = $item['image'];
					if ($item['sid'])	$sid = 1;
					if (is_array($item['col']) && count($item['col']) == 2)	$col = $item['col'];
					if (!empty($item['script']))	$script = $item['script'];

					$bSubLevel = false;
					if (isset($this->items[$item['id']]) && count($this->items[$item['id']]) > 0) {
						// this menu item has submenus
						$bSubLevel = true;
					}

					//create menu item
					$output .= "\n".'var ctxItem = new dhtmlXMenuItemObject("'.$href.'||'.$sid.'||'.$col[0].'||'.$col[1].'||'.addslashes($script).'","'.htmlspecialchars($item['title']).'","100%","'.$img.'","contextMenu");';
					$output .= "\n".'ctxMenu.menu.addItem(ctxMenu.menu, ctxItem);';


					if ($bSubLevel) {
						/*
							//create submenu
							var subMenu = new dhtmlXMenuBarPanelObject(aMenu.menu,item,false,120,true);
							//add item to submenu
							var item = new dhtmlXMenuItemObject("edit_Yellow","Yellow","120","","contextMenu");
							aMenu.menu.addItem(subMenu,item);
							var item = new dhtmlXMenuItemObject("edit_White","White","","120","contextMenu");
							aMenu.menu.addItem(subMenu,item);
						*/
					}

					if ($item['separator']) {
						$output .= "\n".'var ctxItem = new dhtmlXMenuDividerYObject();';
						$output .= "\n".'ctxMenu.menu.addItem(ctxMenu.menu, ctxItem);';
					}
					$count++;
				}
			}
		}
		return $output;
	}

	protected function drawContextMenu() {
		$buf = '<script>';
		$buf .= "\n".'ctxMenu = new dhtmlXContextMenuObject("","","resources/image/");';
		//$buf .= "\n".'ctxMenu.menu.setGfxPath();';
		$buf .= "\n".'ctxMenu.menu.tableCSS="menuTable";';
		$buf .= "\n".$this->getContextMenu();
		$buf .= "\n".'ctxMenu.setContextMenuHandler(onCtxClickAction);';
		$buf .= "\n".'ctxMenu.setContextZone("objtable",1);';
		$buf .= "\n".'</script>';
		return $buf;
	}

	protected function drawFilter() {
		$buf = '';
		$i = 0;

		foreach($this->filters as $filter) {
			$extra = ''; if(isset($filter['params']['extra'])) $extra = $filter['params']['extra'];
			$style = ''; if(isset($filter['params']['style'])) $style = $filter['params']['style'];
			$class = ''; if(isset($filter['params']['class'])) $class = $filter['params']['class'];
			$value = ''; if(isset($filter['params']['value'])) $value = $filter['params']['value'];
			$display = ''; if(isset($filter['params']['display'])) $display = $filter['params']['display'];

			/*
			$type
			1 = Dropdown menu
			2 = Button
			*/
			if($filter['type'] == 1) {

				//$buf .= $display.' <select name="aflt['.$filter['name'].']" id="'.$filter['name'].'" '.((!empty($class))?'class="'.$class.'"':'').' '.((!empty($style))?'style="'.$style.'"':'').' '.$extra.'>';
				$buf .= $display.' <select name="aflt['.$filter['name'].']" id="'.$filter['name'].'" '.((!empty($class))?'class="'.$class.'"':'').' '.((!empty($style))?'style="'.$style.'"':'').'>';
					$buf .= '<option value="">'.App::echostr(CBO_GUI_SELECT).'</option>';
					if(is_array($filter['options']) && count($filter['options']) > 0)
					foreach($filter['options'] as $key => $val) {
						$buf .= '<option value="'.App::echostr($key).'"'.((strlen($value) > 0 && $value == $key)?' selected':'').'>'.App::echostr($val).'</option>';
					}
				$buf .= '</select> ';
			}
			else if ($filter['type'] == 2) {
				$count = 0;
				$checked = "";

				foreach($filter['options'] as $key => $val) {
					if($key === "checked" && $value === "") {
						if($val === true)
							$checked = "checked";
					}
					else if($count == 1)
						$_key = $key;
					else if($key !== $value && $value !== "") {
						$_key = $key;
						if((bool)$value === true)
							$checked = "checked";
					}

					$count++;
				}

				$buf .= $display.' <input type="checkbox" name="aflt['.$filter['name'].']" id="'.$filter['name'].'" '.((!empty($class))?'class="'.$class.'"':'').' '.((!empty($style))?'style="'.$style.'"':'').' '.$extra.' value="'.App::echostr($_key).'" '.$checked.'> ';
			}

			if(isset($filter['params']['edit'])) {
				$buf .= '<input type="button" name="sedit" value="'. App::getText('CBO_GUI_EDIT') .'" onclick="goEditFilter(\''.$filter['params']['edit'].'\', \''.$filter['name'].'\')">&nbsp;';
			}
			if(count($this->filters) > 5 && $i == 3 && count($this->searchs) == 0 && count($this->ranges) == 0)
			$buf .= '<br>';
			$i++;
		}

		//if(isset($this->options['report']) && $this->options['report'] && (count($this->ranges) + count($this->searchs)) < 1)
		if((count($this->ranges) + count($this->searchs) + count($this->showcolumns)) < 1)
		$buf .= ' <input type="submit" class="btn1" name="submit" value="'.CBO_GUI_SEARCH.'"> ';
		return $buf;
	}

	protected function drawSearch() {
		$buf = '';
		$count = count($this->searchs);
		$i = 0;
		if(count($this->searchs) > 0 || count($this->ranges) > 0 || count($this->showcolumns) > 0) {
			$buf = '';
			foreach($this->searchs as $search) {
				$extra = ''; if(isset($search['params']['extra'])) $extra = $search['params']['extra'];
				$style = ''; if(isset($search['params']['style'])) $style = $search['params']['style'];
				$class = ''; if(isset($search['params']['class'])) $class = $search['params']['class'];
				$qtext = ''; if(isset($search['params']['qtext'])) $qtext = $search['params']['qtext'];
				$qfield = ''; if(isset($search['params']['qfield'])) $qfield = $search['params']['qfield'];
				$qoption = ''; if(isset($search['params']['qoption'])) $qoption = $search['params']['qoption'];
				$qtype = ''; if(isset($search['params']['qtype'])) $qtype = $search['params']['qtype'];

				if($i > 0 && $i < $count) {
					$buf .= ' <select name="aqopt['.$search['name'].']" '.((!empty($class))?'class="'.$class.'"':'').' '.((!empty($style))?'style="'.$style.'"':'').' '.$extra.'>';
						$buf .= '<option value="1"'.((strlen($qoption) > 0 && $qoption == '1')?' selected':'').'>'.App::upperCase(CBO_GUI_AND).'</option>';
						$buf .= '<option value="2"'.((strlen($qoption) > 0 && $qoption == '2')?' selected':'').'>'.App::upperCase(CBO_GUI_OR).'</option>';
					$buf .= '</select> ';
				}

				if(strlen($qtype) > 0 && $qtype == 'textarea')
					$buf .= '<textarea rows=3 cols=30 name="aqtxt['.$search['name'].']">'.((strlen($qtext) > 0)?App::echostr($qtext):'').'</textarea>';
				else
					$buf .= ' <input type="text" name="aqtxt['.$search['name'].']" value="'.((strlen($qtext) > 0)?App::echostr($qtext):'').'" '.((!empty($class))?'class="'.$class.'"':'').' '.((!empty($style))?'style="'.$style.'"':'').'> ';
				$buf .= CBO_GUI_IN.' <select name="aqfld['.$search['name'].']" '.((!empty($class))?'class="'.$class.'"':'').' '.((!empty($style))?'style="'.$style.'"':'').' '.$extra.'>';
					//$buf .= '<option value="">'.App::echostr(CBO_GUI_SELECT).'</option>';
					foreach($search['options'] as $key => $val) {
						$buf .= '<option value="'.App::echostr($key).'"'.((strlen($qfield) > 0 && $qfield == $key)?' selected':'').'>'.App::echostr($val).'</option>';
					}
				$buf .= '</select> ';

				$i++;
			}
			if(count($this->filters) <= 3 && (count($this->filters) + count($this->searchs)) > 3 && count($this->ranges) > 0)
			$buf .= '<br>';
			$buf .= $this->drawRange();
			if(count($this->showcolumns) > 0 ) {
				$buf .= '<br>';
				$buf .= CBO_GUI_SHOW. '&nbsp;&nbsp;';
				foreach($this->showcolumns as $column) {
					$buf .= '&nbsp;<input type="checkbox" name="ashowcolumn['.$column['key'].']"'.($column['checked']?' checked':'').' value="1">&nbsp;'.$column['name'];
				}
			}
			$buf .= ' <input type="submit" class="btn1" name="submit" value="'.CBO_GUI_SEARCH.'"> ';
		}
		return $buf;
	}

	protected function drawRange() {
		$buf = '<br>';
		$i = 0;

		if(count($this->ranges) > 0) {
			foreach($this->ranges as $range) {
				$extra = ''; if(isset($range['params']['extra'])) $extra = $range['params']['extra'];
				$style = ''; if(isset($range['params']['style'])) $style = $range['params']['style'];
				$class = ''; if(isset($range['params']['class'])) $class = $range['params']['class'];
				$type = ''; if(isset($range['params']['type'])) $type = $range['params']['type'];
				$display = ''; if(isset($range['params']['display'])) $display = $range['params']['display'];

				$value = '';
				$valuefrom = '';
				$valueto = '';

				if(isset($range['params']['value']) && $range['params']['value'] != ''){
					$value = $range['params']['value'];
					if(isset($value[$range['name'].'from']) && $value[$range['name'].'from'] != '')
						$valuefrom = $value[$range['name'].'from'];
					if(isset($value[$range['name'].'to']) && $value[$range['name'].'to'] != '')
						$valueto = $value[$range['name'].'to'];
				}


				//$inputfrom = ' '. $display .' '. App::getText('CBO_GUI_FROM');
				$inputfrom = ' '. $display .' ';
				$inputto = '<span class="txt_sp2"> ~ </span>';

				$addinputfrom = '';
				$addinputto = '';

				
				$script = '<script type="text/javascript">
				$(function() {
					$("#datefrom").datepicker({showOn: \'both\', buttonImage: \'resources/image/datetime.gif\', buttonImageOnly: true, changeMonth: true, changeYear: true, dateFormat: \'yy-mm-dd\', autoSize: true});
					$("#dateto").datepicker({showOn: \'both\', buttonImage: \'resources/image/datetime.gif\', buttonImageOnly: true, changeMonth: true, changeYear: true, dateFormat: \'yy-mm-dd\', autoSize: true});
				});
				</script>'."\n";
				
				if(isset($range['params']['mindate']) && isset($range['params']['maxdate'])) {
					$script = '<script type="text/javascript">
					$(function() {
						$("#datefrom").datepicker({showOn: \'both\', buttonImage: \'resources/image/datetime.gif\', buttonImageOnly: true, changeMonth: true, changeYear: true, dateFormat: \'yy-mm-dd\', autoSize: true, minDate: \''.$range['params']['mindate'].'\', maxDate: \''.$range['params']['maxdate'].'\'});
						$("#dateto").datepicker({showOn: \'both\', buttonImage: \'resources/image/datetime.gif\', buttonImageOnly: true, changeMonth: true, changeYear: true, dateFormat: \'yy-mm-dd\', autoSize: true, minDate: \''.$range['params']['mindate'].'\', maxDate: \''.$range['params']['maxdate'].'\'});
					});
					</script>'."\n";
				}
				$addinputfrom = '<input type="text" id="datefrom" name="aqrng['.$range['name'].'from]" value = "'. $valuefrom .'" readonly>';
				$addinputto = '<input type="text" id="dateto" name="aqrng['.$range['name'].'to]" value = "'. $valueto .'"  readonly>';
				
				$mdl = $range['params']['mdl'];
				$action = $range['params']['action'];
				$from = 'aqrng['.$range['name'].'from]';
				$to = 'aqrng['.$range['name'].'to]';
				$yesterday = App::spanDateTime(-1 * CBO_DAY_SEC);
				$last7days = App::spanDateTime(-7 * CBO_DAY_SEC);
				$options = '<span class="txt_sp3">
							<a href="javascript:populateDate(\''.App::getDateTime(3).'\', \''.App::getDateTime(3).'\')'.'">'.App::getText('CBO_GUI_TODAY').'</a><span class="sp1">|</span>
							<a href="javascript:populateDate(\''.App::formatDateTime(3, $yesterday).'\', \''.App::formatDateTime(3, $yesterday).'\')'.'">'.App::getText('CBO_GUI_YESTERDAY').'</a><span class="sp1">|</span>
							<a href="javascript:populateDate(\''.App::formatDateTime(3, $last7days).'\', \''.App::getDateTime(3).'\')'.'">'.App::getText('CBO_GUI_LAST7DAYS').'</a><span class="sp1">|</span>
							<a href="javascript:populateDate(\''.App::formatDateTime(3, App::getDateTime(22)).'\', \''.App::getDateTime(3).'\')'.'">'.App::getText('CBO_GUI_THISMONTH').'</a></span>';
				if(isset($range['params']['monthonly']) && $range['params']['monthonly']) {
					$options = '<span class="txt_sp3">
								<a href="javascript:populateDate(\''.App::getDateTime(3).'\', \''.App::getDateTime(3).'\')'.'">'.App::getText('CBO_GUI_TODAY').'</a><span class="sp1">|</span>
								<a href="javascript:populateDate(\''.App::formatDateTime(3, $yesterday).'\', \''.App::formatDateTime(3, $yesterday).'\')'.'">'.App::getText('CBO_GUI_YESTERDAY').'</a><span class="sp1">|</span>
								<a href="javascript:populateDate(\''.App::formatDateTime(3, App::getDateTime(22)).'\', \''.App::getDateTime(3).'\')'.'">'.App::getText('CBO_GUI_THISMONTH').'</a></span>';
				}
				//$buf .= '<table class="topMenu" align="left" border="1" cellpadding="0" cellspacing="0"><tr><td>'.$inputfrom.'</td><td>'.$addinputfrom.'</td><td>'.$inputto.'</td><td>'.$addinputto.'</td></tr></table>';
				$buf .= $script;
				$buf .= $inputfrom.$addinputfrom.$inputto.$addinputto.$options;
				$i++;
			}
		}
		return $buf;
	}

	public function drawButton($position) {
		$buf = '';
		if(isset($this->buttons[$position]))
		foreach($this->buttons[$position] as $button) {
			$extra = ''; if(isset($button['params']['extra'])) $extra = $button['params']['extra'];
			$style = ''; if(isset($button['params']['style'])) $style = $button['params']['style'];
			$class = ''; if(isset($button['params']['class'])) $class = $button['params']['class'];
			$divider = ''; if(isset($button['params']['divider'])) $divider = '<span class="divider">&nbsp;</span>';
			$display = ''; if(isset($button['params']['display'])) $display = $button['params']['display'];

			$buf .= $display.'<input name="'.$button['name'].'" type="'.$button['type'].'" class="btn1" value="'.App::echostr($button['value']).'" '.((!empty($class))?'class="'.$class.'"':'').' '.((!empty($style))?'style="'.$style.'"':'').' '.$extra.'> '.$divider;
		}
		return $buf.' ';
	}

	public function drawNavigation() {
		$pageRange = 10;
		$num = array(25, 50, 100);
		$buf = '';
		$buf .= CBO_GUI_SHOW.' <select name="ilt" onchange="changeShowLimit(this, \''.$this->navigationUrl.'\')">';
		foreach ($num as $val) {
			$buf .= '<option value="'.$val.'"'.(($this->limit == $val)?' selected':'').'>'.$val.'</option>';
		}
		$buf .= '</select> ';
		if($this->limit > 0) {
			$pages = ceil($this->total / $this->limit);
			$currentPage = ceil($this->start / $this->limit);
		} else {
			$pages = 1;
			$currentPage = 1;
		}

		if($pages > 1) {
			$buf .= ' | <span class="navbutton">';
			if($currentPage > 0) $buf .= '<a href="'.$this->navigationUrl.'&ist=0"><img src="resources/image/first.png"></a>';
			else $buf .= '<img src="resources/image/first_disabled.png">';
			if(($currentPage - 1) >= 0) $buf .= '&nbsp;<a href="'.$this->navigationUrl.'&ist='.($this->start - $this->limit).'"><img src="resources/image/previous.png"></a>';
			else $buf .= '&nbsp;<img src="resources/image/previous_disabled.png">';
			if(($currentPage + 1) < $pages) $buf .= '&nbsp;<a href="'.$this->navigationUrl.'&ist='.($this->start + $this->limit).'"><img src="resources/image/next.png"></a>';
			else $buf .= '&nbsp;<img src="resources/image/next_disabled.png">';
			if(($currentPage + 1) != $pages) $buf .= '&nbsp;<a href="'.$this->navigationUrl.'&ist='.(($pages - 1) * $this->limit).'"><img src="resources/image/last.png"></a>';
			else $buf .= '&nbsp;<img src="resources/image/last_disabled.png">';
			$buf .= '</span> | ';

			$buf .= CBO_GUI_GOTO.' <select name="ist" onchange="goToPage(this, \''.$this->navigationUrl.'\')">';
			$buf .= '<option value="0"'.(($this->start == 0)?' selected':'').'>1</option>';
			for($i = 1; $i < ($pages - 1); $i++) {
				if($i >= ($currentPage - $pageRange) && $i <= ($currentPage + $pageRange))
				$buf .= '<option value="'.($i * $this->limit).'"'.(($this->start == ($i * $this->limit))?' selected':'').'>'.($i + 1).'</option>';
				//else continue;
			}
			$buf .= '<option value="'.(($pages - 1) * $this->limit).'"'.(($this->start == (($pages - 1) * $this->limit))?' selected':'').'>'.$pages.'</option>';
			if($this->all){
				$buf .= '<option value="-1"'.(($this->start == "-1")?' selected':'').'>All</option>';
			}
			$buf .= '</select>';
		}
		return $buf;
	}
	
	protected function drawCSV() {

		$buf = '<script>
				function getReportType() {
					var node;
					node = document.getElementById("irpt");
					return node.value;
				}
				</script>';
		$buf .= '<select name="irpt" id="irpt">';
		$buf .= '<option value="1">This Page Only</option>';
		$buf .= '<option value="2">All</option>';
		$buf .= '<input name="btnGenerateCSV" type="button" value="'.App::getText('CBO_GUI_GENERATEREPORT').'" onclick="document.location.href = \''.$this->csvUrl.'&icsv=1&irpt=\'+getReportType();">'; //&ssport=\'+getSportCode(\'sptOptions\')+
		return $buf.' ';
	}
	
	public function generateCSV() {
		$newfilename = App::echostr(App::upperCase($this->title)).'_'.App::getDateTime(3).'.csv';
		$columns = array();
		$rows = array();
		
		foreach($this->columns as $column){
			$columns[] = $column['name'];
		}
		
		$counter = 0;
		for($j = 0; $j < count($this->rows); $j++) {

			if(isset($this->rows[$j]['options']['nomarker'])) {
			}else{
				if(count($this->rows) > $this->limit) //All
					$counter++;
				else{//This page only
					if($counter == 0) $counter = $this->start;
					$counter++;
				}
			}
			
			for($i = 0; $i < count($this->columns); $i++) {//do not show first column
				$colspans = array();
				if(isset($this->rows[$j]['options']['colspans'])) {
					$colspans = $this->rows[$j]['options']['colspans'];
					if(isset($colspans[$i])){
						for($h = 1; $h < $colspans[$i]; $h++){
							 $rows[$j][] = ',';
						}
						$rows[$j][] = $this->rows[$j]['values'][$i];
					}else{
						if(isset($this->rows[$j]['values'][$i])){
							$rows[$j][] = $this->rows[$j]['values'][$i];
						}
					}
				}else{
					if(isset($this->rows[$j]['values'][$i])){
						if(strlen($this->rows[$j]['values'][$i]) == 0){
							if($i == 0) $rows[$j][] = (($counter > 0)?$counter:'');
							else $rows[$j][] = ',';
						}else{
							if($i == 0) $rows[$j][] = (($counter > 0)?$counter:'');
							else $rows[$j][] = $this->rows[$j]['values'][$i];
						}
					}
				}
			} 
		}

		$printdata = App::generateCSVData($rows, $columns);
		$fsize = strlen($printdata);
		$fname = $newfilename;
		header("HTTP/1.1 200 OK");
		header("Content-Length: $fsize");
		header("Content-Type: application/force-download");
		header("Content-Disposition: attachment; filename=$fname");
		header("Content-Transfer-Encoding: 8bit");
		print($printdata);
		exit;
	}

	protected function drawParamsInput($position) {
		$buf = '';
		$params = $this->params[$position];
		$buf .= $this->drawParamsAsInput($params);
		return $buf;
	}

	protected function drawParamsAsInput($params, $parentKey = '') {
		$buf = '';
		$systemParams = array('sid');
		foreach($params as $key => $param) {
			if (!in_array($key, $systemParams) && is_array($param)) {
				if(strlen($parentKey) > 0)
				$key = $parentKey.'['.$key.']';
				$buf .= $this->drawParamsAsInput($param, $key);
			} else if(!in_array($key, $systemParams)) {
				if(strlen($parentKey) > 0)
				$buf .= '<input type="hidden" name="'.$parentKey.'['.$key.']'.'" value="'.urlencode($param).'">';
				else
				$buf .= '<input type="hidden" name="'.$key.'" value="'.urlencode($param).'">';
			}
		}
		return $buf;
	}

	protected function drawMiniForm() {
		//var_dump($this->params[1]);
		$buf = '';
		foreach($this->miniforms as $key => $miniform) {
			//$buf .= '<div id="miniformDiv'.$key.'" style="visibility: visible">';
			$buf .= '<div id="miniformDiv'.$key.'" style="visibility: hidden">';
			$buf .= '<form name="miniform'.$key.'" id="miniform'.$key.'" action="post.php" target="frameSubmit" method="post">';
			$buf .= '<input type="hidden" name="mdl" value="'.$this->params[1]['mdl'].'">';
			$buf .= '<input type="hidden" name="action" value="'.$miniform['action'].'">';
			$buf .= '<input type="hidden" id="miniformSid'.$key.'" name="sid" value="">';
			$buf .=	$miniform['form'];
			$buf .= '</form></div>';
		}
		return $buf;
	}

	protected function drawAutoRefresh() {
		if($this->hasAutoRefresh) {
			$checked = false;
			$intervals = array(5, 10, 20, 30, 40, 50, 60, 90);
			if($this->autoRefreshSecond > 0) {
				$checked = true;
				if (!in_array($this->autoRefreshSecond, $intervals)){
					$intervals[] = $this->autoRefreshSecond;
					sort($intervals);
				}
			}
			$buf = '<input type="button" value="'.App::getText('CBO_GUI_REFRESH').'" onclick="doRefresh()"><input id="iarf" type="checkbox" onClick="doRefresh()"'.(($checked)?' checked':'').'/><select style="font-size: 9px;" name="irfq" id="irfq" onchange="doRefresh()">';
			foreach($intervals as $interval) {
				if($interval == $this->autoRefreshSecond) $selected = ' selected';
				else $selected = '';

				$buf .= '<option value="'.$interval.'"'.$selected.'>'.$interval.'</option>';
			}
			$buf .= '</select> ';

			return $buf;
		}
	}

	function drawColorLegend() {
		$buf = '';
		if(is_array($this->colorLegend) && count($this->colorLegend) > 0) {
			$buf = '<table align="left" class="legend" border="0" cellspacing="0" cellpadding="1"><tr>';
			$buf .= '<td width="18">&nbsp;</td>';
			foreach($this->colorLegend as $color => $legend) {
				$buf .= '<td width="15" bgcolor="'.$color.'">&nbsp;</td>';
				$buf .= '<td>&nbsp;'.$legend.'&nbsp;</td>';

			}
			$buf .= '</tr></table>';
		}
		return $buf;
	}

	function setSoundAlert($filename) {
		$this->soundAlert = $filename;
	}

	function playSoundAlert() {
		$this->playSoundAlert = true;
	}

	public function drawGrid() {
		if(isset($this->options['sortColumnIndex'])){
			$left = 0;
			$skip = false;
			foreach($this->columns as $column) {
				if($column['index'] == $this->options['sortColumnIndex'])
				$skip = true;
				if(!$skip)
				$left = $left + $column['width'] + 0.5;
				else continue;
			}

			if(isset($this->options['sortColumnOrder'])){
				if($this->options['sortColumnOrder'] == 'ASC')
				$sortImage = 'resources/image/sort_asc.gif';
				else
				$sortImage = 'resources/image/sort_desc.gif';
			};
		}
		if($this->showRowMarker) {
			$left = 0;
			$counter = array();

			if($this->start == '-1'){
				$counter['start'] = $this->start + 2;
			}else{
				$counter['start'] = $this->start + 1;
			}
			
			$countLen = strlen($counter['start']) - 1;
			$counter['width'] = (21 + ($countLen * 3));
			$left += $counter['width'];
		}

		$buf = '<script>
		var strReduceSelection = "'.App::echostr(App::getText('CBO_GUI_CANNOTSELECTMORE')).'";
		var strPleaseSelect = "'.App::echostr(App::getText('CBO_GUI_PLEASESELECTANITEM')).'";
		var strPleaseSelectFilter = "'.App::echostr(App::getText('CBO_GUI_PLEASESELECT', array(App::getText('CBO_GUI_FILTER')))).'";
		var dblClickUrl = "'.$this->dblClickUrl.'";
		var dblClickParam = "'.$this->dblClickParam.'";
		var dblClickScript = "'.str_replace("\r\n", '', $this->dblClickScript).'";
		var hasAutoRefresh = "'.$this->hasAutoRefresh.'";
		var refreshUrl = "'.$this->refreshUrl.'";
		setTitleFromGridOrForm("'.$this->title.' - '.App::getText('CBO_GUI_SYSTEMNAME').'");
		</script>';
		
		if(!empty($this->soundAlert))
		$buf .= '<BGSOUND id=BGSOUND_ID src="">
		<embed name="soundalert" src="resources/sound/'.$this->soundAlert.'" hidden="true" autostart="false" mastersound></embed>';

		$buf .= '<div id="splash" style="top: 0px; left: 0px; position: absolute; z-index: 1000; background-color:white; height: 100%; width: 100%;">
		<div align="center"><br><br><img src="resources/image/loading_fast.gif"></div>
		</div>
		<div id="wrapper" style="height: 100%; width: 100%;">';

		if($this->forSelection == false)
		$buf .= '<div onmouseover="hideMenuBar()" id="floater" style="top: 0px; left: 0px; position: absolute; padding: 0px; border: lightgrey 0px solid; height: 100%; width: 100%; background-color: buttonface; z-index: 999; filter:alpha(opacity=0); -moz-opacity:.0; opacity:.0;">
				<iframe id="frameFloater" name="frameFloater" src="resources/script/index.html" border="0" marginwidth="0" marginheight="0" hspace="0" vspace="0" frameborder="0" scrolling="auto" class="appframe" style="filter:alpha(opacity=0); -moz-opacity:.0; opacity:.0; width: 100%; height:100%px; z-index: 1;"></iframe>
			</div>';
			
		$buf .= '<div id="gridwrapper" style="position: absolute; top: 0px; left: 0px; padding-right: 0px; padding-left: 0px; padding-bottom: 0px; padding-top: 0px; border-right: lightgrey 0px solid; border-top: lightgrey 0px solid; border-left: lightgrey 0px solid; border-bottom: lightgrey 0px solid; height: 100%; width: 100%; background-color: buttonface; ; z-index: 2">
		    <div class="gridbox" id="gridbox" style="width: 100%; cursor: default; height: 100%;">
			    <table id="gridTitle" width="100%" border="0" cellspacing="0" cellpadding="0" class="gridTitle">
					<form name="spbFormTop" id="spbFormTop" action="index.php" method="get">
					'.$this->drawParamsInput(1).'
					<input type="hidden" name="ist" value="0">
				    <tr>
						<td colspan="2"><span class="gridTitleText">'.App::echostr(App::upperCase($this->title)).'</span></td>
					</tr>
					<tr>
						<td>'.$this->drawFilter().' '.((count($this->filters) > 3 && !($this->hasAutoRefresh))?'<br>':'&nbsp;').' '.$this->drawSearch().$this->searchform.'</td>

						<td align="right">'.$this->drawButton(1).$this->drawAutoRefresh().'&nbsp;&nbsp;</td>
					</tr>
					</form>
			    </table>
				<table style="table-layout: fixed" height="100%" cellspacing="0" cellpadding="0" width="100%">
					<tr>
						<td height="100">
							<div id="hdrBox" style="overflow: hidden; width: 100%;>';
		if(isset($sortImage))
		$buf .= '<img style="display: inline; position: absolute; top: 5px; left: '.$left.'px" src="'.$sortImage.'" defleft="0" alt="">';
		$buf .= '
								<table border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td>
											<table class="hdr" style="border-right: gray 0px solid; table-layout: fixed; border-top: gray 0px solid; border-left: gray 0px solid; border-bottom: gray 0px solid" cellspacing="0" cellpadding="0" width="100%">
												<tr>';

		$i = 0;
		$totalWidth = 0;
		foreach($this->columns as $column) {
			if($this->showRowMarker) {
				if(1 == $i)
				$buf .= '<td style="width: '.$counter['width'].'px;">No.</td>';
			}
			$totalWidth += $column['width'];
			if(isset($this->columns[$i]['options']['miniform']) && isset($this->columns[$i]['options']['miniaction']))
			$img = ' <img src="resources/image/edit.png">';
			else
			$img = '';
			$buf .= '<td nowrap '.((isset($column['options']['sorturl']))?'class="sortable" onclick="document.location.href=\''.$column['options']['sorturl'].'\'"':'').'style="width: '.$column['width'].'px" id="c'.$i.'">'.((strlen($column['name']) > 0)?App::echostr($column['name']):'&nbsp;').$img.'</td>';
			$i++;
		}
		$buf .= '
													<td'.(($totalWidth > 1000)?' width="1000"':'').'>&nbsp;</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<div id="objbox" onscroll="doOnScroll()" class="objbox" style="overflow: auto; width: 100%; height:100%; position: absolute;">
								<div style="border-bottom: white 1px solid; overflow: auto;">
									<table id="objtable" class="obj" style="table-layout: fixed; width: 100%" cellspacing="0" cellpadding="0">';

		for($j = 0; $j < count($this->rows); $j++) {
			if(isset($this->rows[$j]['options']['selected'])) $class = "rowselected"; else $class = '';
			$buf .= '<tr onclick="doOnClick(this)" id="r'.$j.'" '.((!empty($class))?'class="'.$class.'"':'').'>';

			$colspanCounter = -1;

			for($i = 0; $i < count($this->columns); $i++) {
				$allowhtml = false;
				if(isset($this->rows[$j]['options']['selected'])) {
					if(strlen($this->sid) > 0) $this->sid .= '||'.$this->rows[$j]['values'][$i];
					else $this->sid = $this->rows[$j]['values'][$i];
				}

				$width = $this->columns[$i]['width'];

				if(isset($this->rows[$j]['options']['align']))
				$align = $this->rows[$j]['options']['align'];
				elseif(isset($this->columns[$i]['options']['align']))
				$align = $this->columns[$i]['options']['align'];
				else
				$align = 'left';

				if(isset($this->rows[$j]['options']['color']))
				$color = $this->rows[$j]['options']['color'];
				elseif(isset($this->columns[$i]['options']['color']))
				$color = $this->columns[$i]['options']['color'];
				else
				$color = 'white';

				if(isset($this->rows[$j]['options']['colspan']))
				$colspan = $this->rows[$j]['options']['colspan'];
				elseif(isset($this->columns[$i]['options']['colspan']))
				$colspan = $this->columns[$i]['options']['colspan'];
				else
				$colspan = 1;

				if(isset($this->rows[$j]['options']['rowspan']))
				$rowspan = $this->rows[$j]['options']['rowspan'];
				elseif(isset($this->columns[$i]['options']['rowspan']))
				$rowspan = $this->columns[$i]['options']['rowspan'];
				else
				$rowspan = 1;

				if(isset($this->columns[$i]['options']['allowhtml']))
				$allowhtml = true;

				$colors = array();
				if(isset($this->rows[$j]['options']['colors'])) {
					$colors = $this->rows[$j]['options']['colors'];
				}

				$aligns = array();
				if(isset($this->rows[$j]['options']['aligns'])) {
					$aligns = $this->rows[$j]['options']['aligns'];
				}

				$unsetWidths = array(); //unset width is used to enable alignment for last row. (e.g for total where there is colspan)
				if(isset($this->rows[$j]['options']['unsetWidths'])) {
					$unsetWidths = $this->rows[$j]['options']['unsetWidths'];
				}

				$colspans = array();
				if(isset($this->rows[$j]['options']['colspans'])) {
					$colspans = $this->rows[$j]['options']['colspans'];
					if(isset($colspans[$i]))
						$colspanCounter += $colspans[$i];
				}

				$rowspans = array();
				if(isset($this->rows[$j]['options']['rowspans'])) {
					$rowspans = $this->rows[$j]['options']['rowspans'];
				}
				$colspanCounter += $colspan;
				$editable = '';
				if(isset($this->columns[$i]['options']['miniform']) && isset($this->columns[$i]['options']['miniaction']) ) {
					$this->miniforms[$i] = array('form' => $this->columns[$i]['options']['miniform'], 'action' => $this->columns[$i]['options']['miniaction']);
					$editable = ' canedit = "'.$i.'"';
				}
				$noedit = array();
				if(isset($this->rows[$j]['options']['noedit'])) {
					$noedit = $this->rows[$j]['options']['noedit'];
					if(isset($noedit[$i]))
						$editable = '';
				}

				if($this->showRowMarker && 1 == $i) {
					if(isset($this->rows[$j]['options']['nomarker'])) {
						$buf .= '<td style="width: '.$counter['width'].'" class="rm">&nbsp;</td>';
						$counter['start'] -= 1;
					} elseif(!isset($colspans[0]) || (isset($colspans[0]) && $colspans[0] < 2))
					$buf .= '<td style="width: '.$counter['width'].'" class="rm">'.($counter['start'] + $j).'</td>';
				}

				if($this->showRowMarker && 0 == $i) {
					if(isset($colspans[$i]))
					$colspans[$i] += 1;
				}


				if($colspanCounter <= count($this->columns)){
					$buf .= '<td '.((isset($unsetWidths[$i]))?'':'style="width: '.$width.'"').' align="'.((isset($aligns[$i]))?$aligns[$i]:$align).'"'.((isset($colors[$i]))?' bgcolor="'.$colors[$i].'"':(($color != 'white')?' bgcolor="'.$color.'"':'')).((isset($colspans[$i]) && $colspans[$i] > 1)?' colspan="'.$colspans[$i].'"':(($colspan > 1)?' colspan="'.$colspan.'"':'')).((isset($rowspans[$i]) && $rowspans[$i] > 1)?' rowspan="'.$rowspans[$i].'"':(($rowspan > 1)?' rowspan="'.$rowspan.'"':'')).' id="r'.$j.'-c'.$i.'"'.$editable.'>';
					if(isset($this->rows[$j]['values'][$i]) && strlen($this->rows[$j]['values'][$i]) > 0) {
						if($allowhtml) $buf .= $this->rows[$j]['values'][$i];
						else $buf .= App::echostr($this->rows[$j]['values'][$i]);
					} else $buf .= "&nbsp;";
					$buf .= '</td>';//$this->options
				}
			}
			$buf .= '<td id="r'.$j.'-c'.($i + 1).'">&nbsp;</td></tr>';

		}
		$buf .= '
									</table>
								</div>
							</div>
						</td>
					</tr>
				</table>
			</div>
			<div id="gridFooter">
			<table width="100%" border="0" cellspacing="0" cellpadding="0" class="gridFooter">
			<form name="spbFormBottom" id="spbFormBottom" action="post.php" method="post">
			<input type="hidden" id="sid" name="sid" value="'.$this->sid.'">
			<input type="hidden" id="rid" name="rid" value="">
			'.$this->drawParamsInput(2).'
			<tr>
				<td width="1%" nowrap>&nbsp;&nbsp;'.$this->drawButton(2).'</td>
				<td nowrap align="left">'.$this->drawColorLegend().'</td>
				<td align="right">'.(($this->hasCSV)?$this->drawCSV():'').(($this->hasNavigation)?$this->drawNavigation():'').'&nbsp;&nbsp;</td>
			</tr>
			</form>
			</table>
			</div>
		<script type="text/javascript" src="resources/script/grid.inc.js"></script>
		</div>
		</div>';

		$buf .= $this->drawMiniForm();
		if($this->debugMode)
		$buf .= "\n".'<iframe id="frameSubmit" name="frameSubmit" src="resources/script/index.html" border="0" marginwidth="0" marginheight="0" hspace="0" vspace="0" frameborder="0" scrolling="auto" class="appframe" style="border:solid 1px gray; width: 100; height:100px; position:absolute; top:400px; left:0px; z-index:8; visibility:visible; filter:alpha(opacity=77);"></iframe>';
		else
		$buf .= "\n".'<iframe id="frameSubmit" name="frameSubmit" src="resources/script/index.html" border="0" marginwidth="0" marginheight="0" hspace="0" vspace="0" frameborder="0" scrolling="auto" class="appframe" style="width: 0px; height:0px; position:absolute; z-index:0; visibility:hidden"></iframe>';

		if(count($this->contextMenus) > 0) {
			$buf .= $this->drawContextMenu();
		} else {
			$buf .= '<script>';
			$buf .= "\n".'ctxMenu = false';
			$buf .= "\n".'</script>';
		}

		if($this->playSoundAlert) {
			$buf .= '<script>playSound("'.$this->soundAlert.'");</script>';
		}
		return $buf;
	}
	
	public function getColumns() {
		return $this->columns;
	}
	
	public function getRows() {
		return $this->rows;
	}
	
	public function getFilterForm() {
		$buf = '';
		
		if($this->hasAutoRefresh) {
			$buf .= '<script>'."\n";
			$buf .= "
			var hasAutoRefresh = \"".$this->hasAutoRefresh."\";
			var refreshUrl = \"".$this->refreshUrl."\";
			var tid;
			var sec = ".$this->autoRefreshSecond.";
			if(sec > 0){
				tid = setInterval('doRefresh()',sec*1000);
			}
			function doRefresh() {
				clearInterval(tid);
				sec = document.getElementById('irfq').value;
				document.location.href = refreshUrl+'&irfq='+sec;
				tid = setInterval('doRefresh()',sec*1000);
			}
			"
			;
			$buf .= '</script>';
		}
		
		$buf .= '<form name="spbFormTop" id="spbFormTop" action="index.php" method="get">
					'.$this->drawParamsInput(1).'
					<input type="hidden" name="ist" value="0">';	
		$buf .= '<tr>
					<td>'.$this->drawFilter().' '.((count($this->filters) > 3 && !($this->hasAutoRefresh))?'<br>':'&nbsp;').' '.$this->drawSearch().$this->searchform.$this->drawButton(1).$this->drawAutoRefresh().'&nbsp;&nbsp;</td>
				</tr>';
		$buf .= '</form>';
		return $buf;
	}
	
}
?>
