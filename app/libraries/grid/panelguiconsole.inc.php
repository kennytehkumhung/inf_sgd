<?php
//////////////////////////////////////////////////////////////////////
//
//  All Rights Reserved (c) 2010
//
//////////////////////////////////////////////////////////////////////
/**
* The web GUI object class for rcm/agent panel
*
* This class is use to manage the HTML and layout for rcm/agent panel
* You may customise the overall outlook of your panel by over writting the available method in the parent class.
*
* @author   Henry Choo <kfchoo@yahoo.com>
* @version  1.0
* @package  rcmlib
* @ignore
*/
class PanelGuiConsole extends Gui {

	/**
	* An array of menu item
	*
	* @access 	private
	* @var      array
	*/
	private $menu = array();

	/**
	* Incremental index number for menu id
	*
	* @access 	private
	* @var      integer
	*/
	private $index = 0;

	/**
	* Incremental index number for divider id
	*
	* @access 	private
	* @var      integer
	*/
	private $divideIndex = 0;

	/**
	* Grid frame source
	*
	* @access 	private
	* @var      string
	*/
	private $frameGridSrc = '';

	/**
	* Jump frame source
	*
	* @access 	private
	* @var      string
	*/
	private $frameJumpSrc = '';

	/**
	* Frameset source
	*
	* @access 	private
	* @var      string
	*/
	private $frameSetSrc = '';

	/**
	* XML menu source
	*
	* @access 	private
	* @var      string
	*/
	private $menuSrc = '';

	/**
	* Contructor.
	*
	* @param string  $menuUrl	The src url for XML menu
	*
	* @return none
	*/
	function __construct($menuUrl = '') {
		$this->menuSrc = $menuUrl;
	}

	/**
	* Method for adding menu item in the console
	*
	* @param integer $parentid	The menu parent id
	* @param string  $name		Name of the menu
	* @param string  $id		The menu JS id/code
	* @param string  $url		Menu action url src
	* @param string  $image		Menu image
	* @param boolean $divider	To append a divider
	* @param string  $style		CSS style to append
	* @param string  $target	Frame target for menu event click
	*
	* @access public
	* @return integer The added menu's unique id
	*/
	function addMenu($parentid = 0, $name, $id, $url = '', $image = '', $divider = false, $style = '', $target = 'frameGrid') {
		$this->index++;
		$this->menu[$parentid][] = array('index' => $this->index, 'name' => App::echostr($name), 'id' => $id, 'image' => $image, 'divider' => $divider, 'url' => App::echostr($url), 'target' => $target, 'style' => $style);
		return $this->index;
	}

	/**
	* Method for drawing the XML menu src
	*
	*
	* @access public
	* @return string The XML output
	*/
	function drawMenuXML() {
		$buf = '<?xml version="1.0" encoding="'.SYSTEM_CHARSET.'" ?>';
		$buf .= "\n".'<menu name="'.App::upperCase(App::getText('CBO_GUI_LOGINAS')).' : &lt;span class=&quot;consoleTimer&quot;&gt;'.strtoupper(App::getUserSession('username')).'&lt;/span&gt;" withoutImages="yes" menuAlign="right">';
		foreach($this->menu[0] as $menu) {
			$buf .= "\n".$this->drawXMLNode($menu);
		}
		$buf .= "\n".'</menu>';
		return $buf;
	}

	/**
	* Method for drawing the XML menu node
	*
	* @param array  $menu	An array of menu setting & options
	*
	* @access private
	* @return string The XML output
	*/
	private function drawXMLNode($menu) {
		$hasChild = false;

		if(isset($this->menu[$menu['index']]))
		$hasChild = true;

		$buf = "\n".'<MenuItem '.((strlen($menu['style']) > 0)?'style="'.$menu['style'].'; width:100%" ':'').' name="'.htmlspecialchars($menu['name']).'" id="'.$menu['id'].'"'.((strlen($menu['image']) > 0)?' src="'.$menu['image'].'" ':'').((strlen($menu['url']) > 0)?' href="'.$menu['url'].'" ':'').((strlen($menu['target']) > 0)?' target="'.$menu['target'].'" ':'').'withoutImages="no"'.(($hasChild)?'':'/').'>';
		if($hasChild) {
			foreach($this->menu[$menu['index']] as $submenu) {
				$buf .= "\n".$this->drawXMLNode($submenu);
			}
			$buf .=  "\n".'</MenuItem>';
		}

		if($menu['divider']) {
			$buf .= "\n".'<divider id="div_'.$this->divideIndex.'"/>';
			$this->divideIndex++;
		}
		return trim($buf);
	}

	/**
	* Set the grid frame URL source
	*
	* @param string  $src	The url source
	*
	* @access public
	* @return none
	*/
	function setFrameGridSrc($src) {
		$this->frameGridSrc = $src;
	}

	/**
	* Set the jump frame URL source
	*
	* @param string  $src	The url source
	*
	* @access public
	* @return none
	*/
	function setFrameJumpSrc($src) {
		$this->frameJumpSrc = $src;
	}

	/**
	* Set the frameset URL source
	*
	* @param string  $src	The url source
	*
	* @access public
	* @return none
	*/
	function setFrameSetSrc($src) {
		$this->frameSetSrc = $src;
	}

	/**
	* Method for drawing the framset
	*
	*
	* @access public
	* @return none
	*/
	function drawFrameSet()
	{
		$buf = '<html><head><title>Starting application, please wait...</title></head>'."\n";
		$buf .= '<frameset rows="1,*" frameborder="no" border="0" framespacing="0">'."\n";
		$buf .= '<frame src="resources/script/index.html" scrolling="no" noresize>'."\n";
		$buf .= '<frame src="'.$this->frameSetSrc.'" scrolling="no" noresize>'."\n";
		$buf .= '</frameset>'."\n";
		$buf .= '<noframes><body>'."\n";
		$buf .= '</body></noframes>'."\n";
		$buf .= '</html>';
		return $buf;
	}

	/**
	* Method for drawing the system console
	*
	*
	* @access public
	* @return none
	*/
	function drawConsole() {
		$filename = APPROOT.'/update.txt';
		$lastupdated = '';
		if (file_exists($filename)) {
		   $lastupdated = ' <span style="font-size: 8px; color: #FFFFFF">('.App::getText('CBO_GUI_UPDATED').":".gmdate("Y-m-d H:i", (filemtime($filename) + (3600 * SYSTEM_TIMEZONE)) ).')</span>';
		}

		$buf = '<script>
					var consoleMenuUrl = "'.$this->menuSrc.'";
					var invalidShortCut = "'.App::getText('CBO_GUI_INVALIDSHORTCUT').'";
			</script>';

		$buf .= "\n".'<iframe id="frameJump" name="frameJump" src="'.$this->frameJumpSrc.'" border="0" marginwidth="0" marginheight="0" hspace="0" vspace="0" frameborder="0" scrolling="no" class="appframe" style="border: 1px solid; border-color : white Gray Gray white; position:absolute; z-index:0; visibility:hidden; overflow: hidden;"></iframe>';
		$buf .= "\n".'<div id="wrapper" style="filter:alpha(opacity=30); -moz-opacity:.30;opacity:.30; background-color: #000000; position:absolute; left: 0px; top: 0px;  width: 100%; height: 100%; z-index:1"></div>';
		$buf .= '<table width="100%" cellspacing="0" cellpadding="0" border="0" style="padding-right: 1px">
				<tr>
				<td bgcolor="buttonface">
					<table class="topbar" width="100%" cellspacing="0" cellpadding="0" border="0"><tr><td>
					'.App::upperCase(App::getText('CBO_GUI_SYSTEMNAME')).$lastupdated.'
					</td><td align="right" style="color: #555555">';
		$localTimezone = App::float(App::getUserSession('timezone'));
		$systemTimezone = App::float(App::getGMTOffset());

		$sysDatetimeStr = App::breakDateTime(App::getDateTime());
		$sysDatetimeStr = $sysDatetimeStr['year'].','.( ((int) $sysDatetimeStr['month']) - 1).','.((int)$sysDatetimeStr['day']).','.((int)$sysDatetimeStr['hour']).','.((int)$sysDatetimeStr['minute']).','.((int)$sysDatetimeStr['second']);

		$localDatetimeStr = App::breakDateTime(App::getDateTime(1, $localTimezone));
		$localDatetimeStr = $localDatetimeStr['year'].','.( ((int)$localDatetimeStr['month']) - 1).','.((int)$localDatetimeStr['day']).','.((int)$localDatetimeStr['hour']).','.((int)$localDatetimeStr['minute']).','.((int)$localDatetimeStr['second']);

		if($localTimezone != $systemTimezone) {
			$buf .= App::upperCase(App::getText('CBO_GUI_LOCAL')).': <span id="localClock" class="consoleTimer">'.App::getDateTime(1, App::getUserSession('timezone')).'</span> (GMT '.$localTimezone.') / ';
			$buf .= App::upperCase(App::getText('CBO_GUI_SYSTEMTIME')).': <span id="systemClock" class="consoleTimer">'.App::getDateTime().'</span> (GMT '.App::getGMTOffset().')';
		} else $buf .= App::upperCase(App::getText('CBO_GUI_LOCAL')).'/'.App::upperCase(App::getText('CBO_GUI_SYSTEMTIME')).': <span id="systemClock" class="consoleTimer">'.App::getDateTime().'</span> (GMT '.App::getGMTOffset().')';

		$buf .= ' <input name="slogout" type="button" value="'.App::getText('CBO_GUI_LOGOUT').'" onclick="document.location.href = \''.App::getFormattedURL('login', 'logout').'\'">';

		$buf .= '
					</td></tr></table>
				</td>
				</tr>
				<tr>
				<td bgcolor="#ece9d8">
				<div id="xpstyle" style="z-index:4 width:100%;">
				</td>
				</tr>
				</table>
				';
		$buf .= '<script>
					var timesetter1 = new Date('.$sysDatetimeStr.');
					var timesetter2 = new Date('.$localDatetimeStr.');
					var month=new Array(12)
					month[0]="Jan"
					month[1]="Feb"
					month[2]="Mar"
					month[3]="Apr"
					month[4]="May"
					month[5]="Jun"
					month[6]="Jul"
					month[7]="Aug"
					month[8]="Sep"
					month[9]="Oct"
					month[10]="Nov"
					month[11]="Dec"
					function MakeTime(el, timerName){
						//setTimeout("MakeTime(el, timerName);",1000);
						el.setTime(el.getTime()+30000);
						var mth = month[el.getMonth()];
						var mthday = String(el.getDate());
						var yrN = String(el.getFullYear());
						var dayN = String(el.getDay());
						var hhN  = el.getHours();
					    var mm  = String(el.getMinutes());
					    var ss  = String(el.getSeconds());
						//TimeNow = mthday + " " + mth + " " + ((hhN < 10) ? "0" : "") + hhN + ((mm < 10) ? ":0" : ":") + mm + ((ss < 10) ? ":0" : ":") + ss + "";
						TimeNow = mthday + " " + mth + " " + ((hhN < 10) ? "0" : "") + hhN + ((mm < 10) ? ":0" : ":") + mm;
						document.getElementById(timerName).innerText = TimeNow;
					}'."\n";

		if($localTimezone != $systemTimezone)
		$buf .= 'MakeTime(timesetter2, "localClock");
		setInterval("MakeTime(timesetter2, \'localClock\')", 30000);'."\n";

		$buf .= ' MakeTime(timesetter1, "systemClock");
		setInterval("MakeTime(timesetter1, \'systemClock\')", 30000);
		</script>';
		$buf .= "\n".'<script type="text/javascript" src="resources/script/console.inc.js"></script>';
		$buf .= "\n".'<iframe id="frameGrid" name="frameGrid" src="'.$this->frameGridSrc.'" border="0" marginwidth="0" marginheight="0" hspace="0" vspace="0" frameborder="0" scrolling="no" class="appframe" style="overflow: auto; position:absolute; z-index:-1;"></iframe>';
		$buf .= '<script>
					function hideMenuBar(){
						aMenuBar._closePanel(aMenuBar.lastOpenedPanel)
					}
					addEvent(document, "dblclick", hideMenuBar);
					parent.parent.document.title = "'.App::getText('CBO_GUI_SYSTEMNAME').' - '.App::getText('CBO_GUI_SYSTEMVERS').'"
				</script>';
		return $buf;
	}
}
?>