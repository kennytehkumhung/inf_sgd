<?php
//////////////////////////////////////////////////////////////////////
//
//  All Rights Reserved (c) 2010
//
//////////////////////////////////////////////////////////////////////
/**
* The web GUI object class for user panel
*
* This is used to generate form on the system to be used by administrative panel.
* You may customise the overall outlook of your panel by over writting the available method in the parent class.
*
* @author   Seng Wee Keong <wkseng@yahoo.com>
* @version  1.0
* @package  rcmlib
* @ignore
*/
class PanelGui extends Gui {

	/**
	* Array of columns
	*
	* @var      array
	*/
	public $columns = array();

	/**
	* Array of columns
	*
	* @var      array
	*/
	public $firstColumns = array();

	
	/**
	* Array of columns
	*
	* @var      string
	*/
	public $title = '';

	/**
	* Array of columns
	*
	* @var      string
	*/
	public $tabletitle = '';

	/**
	* Indicate if onclick event enabled
	*
	* @var      boolean
	*/
	public $onclick = false;

	/**
	* Indicate if onclick event enabled
	*
	* @var      boolean
	*/
	public $ondblclick = false;

	/**
	* Array of rows
	*
	* @var      array
	*/
	public $rows = array();

	/**
	* Number of second to refresh a page
	*
	* @var		integer
	*/
	public $refreshRate = 0;

	/**
	* Url of double click action
	*
	* @var		string
	*/
	protected $dblClickUrl = '';

	/**
	* Param of double click action
	*
	* @var		string
	*/
	protected $dblClickParam = '';
	
	/**
	* An of array buttons to be included in the form object
	*
	* @access 	private
	* @var      array
	*/
	private $buttons = array();

	/**
	* Contructor.
	*
	*
	* @return none
	*/
	function __construct() {
		parent::__construct();
	}

	/**
	* Add a column to the form
	*
	* @param string $name	   	The name of the column
	* @param string $width   	The width of the column
	*
	* @return none
	*/
	function addColumn ($name, $width = '', $options = array()) {
		$this->columns[] = array($name, $width, 'options' => $options);
	//	var_dump($this->columns);
	}
	
	/**
	* Add a column to the form
	*
	* @param string $name	   	The name of the column
	* @param string $width   	The width of the column
	*
	* @return none
	*/
	function addFirstColumn ($name, $colspan = '', $rowspan ='', $width = '', $align = '') {
	
		$this->firstColumns[] = array($name, $colspan, $rowspan, $width, $align);
	}

	/**
	* Set OnClick event to be enabled.
	*
	* @param string $data   	The array of attributes for row
	*
	* @return none
	*/
	function setOnClickAction () {
		$this->onclick = true;
	}

	/**
	* Set OnDblClick event to be enabled.
	*
	* @param string $data   	The array of attributes for row
	*
	* @return none
	*/
	function setOnDoubleClickAction () {
		$this->ondblclick = true;
	}

	/**
	* Set url to be redirected upon double click action
	*
	* @param string $name	   	The name of the column
	*
	* @return none
	*/
	public function setDoubleClickUrl($url, $param) {
		$this->dblClickUrl = $url;
		$this->dblClickParam = $param;
	}

	/**
	* Add a column to the form
	*
	* @param string $name	   	The name of the column
	*
	* @return none
	*/
	function setTableTitle ($name) {
		$this->tabletitle = $name;
	}
	
	/**
	* Add a row to the form
	*
	* @param string $data   	The array of attributes for row
	* @param string $id   		Row Id
	*
	* @return none
	*/
	function addRow ($data, $id = '', $options = array()) {
		$this->rows[] = array('data' => $data, 'id' => $id, 'options' => $options);
	}
	
	/**
	* Draw a table in the page
	*
	* @param string $gui	   	The GUI object using the form
	*
	* @return none
	*/
	function drawTable($class = '') {
		
		$buf = '';
		$columnName = array();
		$color = App::getInstance();

		$ondblclick = '';
		if($this->ondblclick) {
			$buf .= "\n" . '<script>
						var dblClickUrl = "'.$this->dblClickUrl.'";
						var dblClickParam = "'.$this->dblClickParam.'";
					</script>';
			$ondblclick = 'ondblclick="doOnDoubleClick(this)"';
			$this->ondblclick = false;
		}

		$totalColumns = 0;
		if($this->firstColumns != NULL) {
			foreach($this->firstColumns as $column) {
				if(isset($column[1]) && $column[1] > 1) {
					$totalColumns += $column[1];
				}else {
					$totalColumns += 1;
				}
			}
		}else {
			$totalColumns = count($this->columns);
		}
		$colspan = '';
		if($totalColumns > 1) {
			$colspan = 'colspan="'.$totalColumns.'"';
		}
		$buf .= "\n" . '<table class="' . $class . '" cellpadding="0" cellspacing="0" border="0" width="100%">';
		$buf .= "\n" . '<tr valign="top">';
		$buf .= '<td bgcolor="buttonface" valign="top" align="left" '.$colspan.'><span class="gridTitleText">'. App::echostr(App::upperCase($this->tabletitle)) .'</span></td>';
		$buf .= '</tr>';
		if ($this->firstColumns != NULL){
		$buf .= "\n" . '<tr>';
		
			foreach($this->firstColumns as $firstcol) {
				$colspan = '';
				if($firstcol[1] > 1)
				$colspan = 'colspan="'.$firstcol[1].'"';
				$rowspan = '';
				if($firstcol[2] > 1)
				$rowspan = 'rowspan="'.$firstcol[2].'"';
				$buf .= '<td bgcolor="buttonface" align="' . $firstcol[4] . '" '.$colspan.' '.$rowspan.' '.((isset($firstcol[3]) && strlen($firstcol[3]) > 0)?'style="width:'. $firstcol[3] .';"':'').'><span class="tableTitleText">'. $firstcol[0].'</span></td>';
			}
			$buf .= '</tr>';
		}
		$buf .= "\n" . '<tr>';
		$num = count($this->columns);
		
		foreach($this->columns as $column) {
			$colspan = '';
			if(isset($column['options']['colspan']) && $column['options']['colspan'] > 1) {
				$colspan = 'colspan="'.$column['options']['colspan'].'"';
			}
			$align = ((isset($column['options']['align']))?$column['options']['align']:'left');
			if ($column[0] != ""){
					$buf .= '<td bgcolor="buttonface" align="'. $align .'" '.$colspan.' '.((isset($column[1]) && strlen($column[1]) > 0)?' style="width:'. $column[1] .';"':'').'><span class="tableTitleText">'. $column[0].'</span></td>';
			}
		}
		$buf .= '</tr>';

		$onclick = '';
		if($this->onclick) {
			$onclick = 'onclick="doOnClick(this)"';
			$this->onclick = false;
		}
	
		foreach($this->rows as $row) {
			$id = $row['id'];
			$buf .= "\n" . '<tr id="' . $id . '" ' . $onclick . '>';
				for($j = 0; $j < $totalColumns ; $j++) {	
					if(isset($row['options']['align'])){
						$align = $row['options']['align'];
					}else
						$align = 'left';
				
					$aligns = array();
					if(isset($row['options']['aligns'])) {
						$aligns = $row['options']['aligns'];
					}	
					
					if(isset($row['options']['color']))
						$color = $row['options']['color'];
					else
						$color = 'white';
						
					$colors = array();
					if(isset($row['options']['colors'])) {
						$colors = $row['options']['colors'];
					}
					$width = (isset($this->columns[$j][1]))?$this->columns[$j][1]:'auto';
					if(isset($row['options']['widths'][$j])) {
						$width 	= $row['options']['widths'][$j];
					}
				 	$colspan = '';
					if(isset($row['options']['colspan'])) {
						$colspan 	= 'colspan="'. $row['options']['colspan'] . '"';
						if('auto' != $width)
						$width		= $width * $row['options']['colspan'];
					}
					$colspans = array();
					if(isset($row['options']['colspans'])) {
						$colspans 	= $row['options']['colspans'];
						if(isset($colspans[$j])) {
							$colspan 	= 'colspan="'. $colspans[$j] . '"';
							if('auto' != $width)
							$width		= $width * $colspans[$j];
						}
					}
					
					$data = isset($row['data'][$j])?$row['data'][$j]:'';
					$buf .= '<td '. ((strlen($width) > 0)?'style="width:'. $width .';"':'').' id="c_' . $id . '" '. $ondblclick .' align="'.((isset($aligns[$j]))?$aligns[$j]:$align).'" bgcolor="'.((isset($colors[$j]))?$colors[$j]:$color).'" '. $colspan .' valign="top">'. $data .'</td>';
					if(isset($row['options']['colspan'])) {
						$j += $row['options']['colspan'] - 1;
					}
					if(isset($colspans[$j])) {
						$j += $colspans[$j] - 1;
					}
				}
			$buf .= '</tr>';
		}
		$buf .= "\n" . '</table>';
		
		if (count($this->buttons) > 0){
			$buf .= '<table border="0" cellspacing="0" cellpadding="2" width="100%"><tr><td align="right">';
			foreach ($this->buttons as $button) {
				$buf .= '<input id="btt" class="formButton" type="'.$button['type'].'" name="'.htmlspecialchars(trim($button['name'])).'" value="'.htmlspecialchars(trim($button['title'])).'" onclick="'.htmlspecialchars(trim($button['script'])).'"> ';
			}
			$buf .= '</td></tr></table>';
			$this->buttons = array();
		}
		$this->setContent($buf);
		return $buf;
	}
	
	/**
	* Get whole HTML page
	*
	* @param string $append	Additional output to append to end of the page
	* @param object $smarty	Smarty template engine object
	* @param string $classname	Class name of the module that invoke this action
	*
	* @access public
	* @return string The full HTML page to be output
	*/
	function getOutput($append = '', &$smarty, $classname) {
		if(strlen($this->content) > 0) {
			return parent::getOutput($append);
		} else {
			$classname = strtolower($classname);
			$template = '';
	
			$classname = App::lowerCase(str_replace('module', '', $classname));
			$template = 'rabbit/index.tpl';
			if(strlen($template) > 0 && file_exists(APPROOT.'/source/panel/'.PANEL.'/templates/'.$template)) {
				 return $smarty->fetch($template).$append;
			} else return 'Template not found';		
			
		}
	}

	
	function resetTable() {
		$this->columns 	= array();
		$this->rows 	= array();
	}

	/**
	* Set the refresh rate
	*
	* @param integer $sec		Number of seconds
	*
	* @return none
	*/
	public function setRefreshRate($sec) {
		$this->refreshRate = (int) $sec;
	}
	
	/**
	* Add button to the form
	*
	* @param string $title   		The value to appear on the button eg. OK, Cancel etc.
	* @param string $inputname   	The value in the name attribute of the button input
	* @param string $script   		The onlick event script to invoke
	* @param string $type   		Button type. Available choice are button, submit, reset
	*
	* @access public
	* @return none
	*/
	function addButton($title, $inputname, $script = '', $type = 'button') {
		$this->buttons[] = array(
			'title' => $title,
			'name' => $inputname,
			'script' => $script,
			'type' => $type
		);
	}
	
	function drawTable2($class = '') {
		
		$buf = '';
		$columnName = array();
		$color = App::getInstance();

		$ondblclick = '';
		if($this->ondblclick) {
			$buf .= "\n" . '<script>
						var dblClickUrl = "'.$this->dblClickUrl.'";
						var dblClickParam = "'.$this->dblClickParam.'";
					</script>';
			$ondblclick = 'ondblclick="doOnDoubleClick(this)"';
			$this->ondblclick = false;
		}

		$totalColumns = 0;
		if($this->firstColumns != NULL) {
			foreach($this->firstColumns as $column) {
				if(isset($column[1]) && $column[1] > 1) {
					$totalColumns += $column[1];
				}else {
					$totalColumns += 1;
				}
			}
		}else {
			$totalColumns = count($this->columns);
		}
		$colspan = '';
		if($totalColumns > 1) {
			$colspan = 'colspan="'.$totalColumns.'"';
		}
		$buf .= "\n" . '<table class="' . $class . '" cellpadding="0" cellspacing="0" border="0" width="100%">';
		$buf .= "\n" . '<tr valign="top">';
		$buf .= '<td bgcolor="buttonface" valign="top" align="left" '.$colspan.'><span class="gridTitleText">'. App::echostr(App::upperCase($this->tabletitle)) .'</span></td>';
		$buf .= '</tr>';
		if ($this->firstColumns != NULL){
		$buf .= "\n" . '<tr>';
		
			foreach($this->firstColumns as $firstcol) {
				$colspan = '';
				if($firstcol[1] > 1)
				$colspan = 'colspan="'.$firstcol[1].'"';
				$rowspan = '';
				if($firstcol[2] > 1)
				$rowspan = 'rowspan="'.$firstcol[2].'"';
				$buf .= '<td bgcolor="buttonface" align="' . $firstcol[4] . '" '.$colspan.' '.$rowspan.' '.((isset($firstcol[3]) && strlen($firstcol[3]) > 0)?'style="width:'. $firstcol[3] .';"':'').'><span class="tableTitleText">'. $firstcol[0].'</span></td>';
			}
			$buf .= '</tr>';
		}
		$buf .= "\n" . '<tr>';
		$num = count($this->columns);
		
		foreach($this->columns as $column) {
			$colspan = '';
			if(isset($column['options']['colspan']) && $column['options']['colspan'] > 1) {
				$colspan = 'colspan="'.$column['options']['colspan'].'"';
			}
			$align = ((isset($column['options']['align']))?$column['options']['align']:'left');
			if ($column[0] != ""){
					$buf .= '<td bgcolor="buttonface" align="'. $align .'" '.$colspan.' '.((isset($column[1]) && strlen($column[1]) > 0)?' style="width:'. $column[1] .';"':'').'><span class="tableTitleText">'. $column[0].'</span></td>';
			}
		}
		$buf .= '</tr>';

		$onclick = '';
		if($this->onclick) {
			$onclick = 'onclick="doOnClick(this)"';
			$this->onclick = false;
		}
	
		foreach($this->rows as $row) {
			$id = $row['id'];
			$buf .= "\n" . '<tr id="' . $id . '" ' . $onclick . '>';
				for($j = 0; $j < $totalColumns ; $j++) {	
					if(isset($row['options']['align'])){
						$align = $row['options']['align'];
					}else
						$align = 'left';
				
					$aligns = array();
					if(isset($row['options']['aligns'])) {
						$aligns = $row['options']['aligns'];
					}	
					
					if(isset($row['options']['color']))
						$color = $row['options']['color'];
					else
						$color = 'white';
						
					$colors = array();
					if(isset($row['options']['colors'])) {
						$colors = $row['options']['colors'];
					}
					$width = (isset($this->columns[$j][1]))?$this->columns[$j][1]:'auto';
					if(isset($row['options']['widths'][$j])) {
						$width 	= $row['options']['widths'][$j];
					}
				 	$colspan = '';
					if(isset($row['options']['colspan'])) {
						$colspan 	= 'colspan="'. $row['options']['colspan'] . '"';
						if('auto' != $width)
						$width		= $width * $row['options']['colspan'];
					}
					$colspans = array();
					if(isset($row['options']['colspans'])) {
						$colspans 	= $row['options']['colspans'];
						if(isset($colspans[$j])) {
							$colspan 	= 'colspan="'. $colspans[$j] . '"';
							if('auto' != $width)
							$width		= $width * $colspans[$j];
						}
					}
					
					$data = isset($row['data'][$j])?$row['data'][$j]:'';
					$buf .= '<td '. ((strlen($width) > 0)?'style="width:'. $width .';"':'').' id="c_' . $id . '" '. $ondblclick .' align="'.((isset($aligns[$j]))?$aligns[$j]:$align).'" bgcolor="'.((isset($colors[$j]))?$colors[$j]:$color).'" '. $colspan .' valign="top">'. $data .'</td>';
					if(isset($row['options']['colspan'])) {
						$j += $row['options']['colspan'] - 1;
					}
					if(isset($colspans[$j])) {
						$j += $colspans[$j] - 1;
					}
				}
			$buf .= '</tr>';
		}
		$buf .= "\n" . '</table>';
		
		if (count($this->buttons) > 0){
			$buf .= '<table border="0" cellspacing="0" cellpadding="2" width="100%"><tr><td align="right">';
			foreach ($this->buttons as $button) {
				$buf .= '<input id="btt" class="formButton" type="'.$button['type'].'" name="'.htmlspecialchars(trim($button['name'])).'" value="'.htmlspecialchars(trim($button['title'])).'" onclick="'.htmlspecialchars(trim($button['script'])).'"> ';
			}
			$buf .= '</td></tr></table>';
			$this->buttons = array();
		}
		//$this->setContent($buf);
		return $buf;
	}
}

?>