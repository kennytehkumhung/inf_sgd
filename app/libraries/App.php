<?php namespace App\libraries;

use App\libraries\grid\PanelForm;
use Lang;
use GeoIP;
use App\Models\Configs;
use App\Models\Apilog;
use Session;
use Config;

class App{
	
	static public $dbUnixTimestamp;

	 Public Static function curl( $postfields, $url , $log = true , $timeout = 30 , $method = '', $xml = false)
	 {
			
			$postfields = $xml == true ? $postfields = 'xmlRequest=' . $postfields : $postfields;
			
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_HEADER, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);
			if( isset($_SERVER['HTTP_USER_AGENT'] ))
			{
				curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
			}
			$response = curl_exec($ch);
			curl_close($ch);
			
			if($log)
			App::insert_api_log( array( 'request' => $url.'?'.$postfields , 'return' => $response , 'method' => $method ) );

			return $response;
	}
        
        Public Static function curlXML( $postfields, $url , $log = true , $timeout = 30 , $method = '', $header = false)
	{
			
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);
			if( isset($_SERVER['HTTP_USER_AGENT'] ))
			{
				curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
			}
			$response = curl_exec($ch);
			curl_close($ch);
			
			if($log)
			App::insert_api_log( array( 'request' => $url.'?'.$postfields , 'return' => $response , 'method' => $method ) );

			return $response;
	}
	

    Public Static function curlGET($url , $log = true, $timeout = 30 , $method = '', $header = false){

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, str_replace(' ' , '%20', $url));
			curl_setopt($ch, CURLOPT_HEADER, false);
                        if($header) 
                            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
			curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
			$response = curl_exec($ch);
			curl_close($ch);
			
			if($log)
			App::insert_api_log( array( 'request' => $url , 'return' => $response , 'method' => $method ) );
			
			return $response;
	}
	
	Public Static function insert_api_log($logs){
		
		$apilog = new Apilog;
		$apilog->accid   = Session::has('userid')  ? Session::get('userid')  : '';
		$apilog->acccode = Session::has('acccode') ? Session::get('acccode') : '';
		$apilog->method  = $logs['method'];
		$apilog->request = $logs['request'];
		$apilog->return  = $logs['return'];
		$apilog->save();
		
	}
	
	public static function maxmind_ip($ip){
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, 'https://geoip.maxmind.com/geoip/v2.1/city/'.$ip);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_USERPWD, "128582:iyY43EHEKzuE");
		curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($ch, CURLOPT_TIMEOUT, 5);
		$output = curl_exec($ch);

		curl_close($ch);

		return json_decode($output);
		
	}
	
	public static function taobao_ip($ip){
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, 'http://ip.taobao.com/service/getIpInfo.php?ip='.$ip);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($ch, CURLOPT_TIMEOUT, 20);
		$output = curl_exec($ch);

		curl_close($ch);


		return json_decode($output);
		
	}
	
	Public Static function numberFormatPrecision($number, $precision = 2, $separator = '.')
	{

		$numberParts = explode($separator, $number);

		$response = $numberParts[0];

		if(count($numberParts)>1){

			$response .= $separator;

			$response .= substr($numberParts[1], 0, $precision);

		}

		return $response;

	}
	
	public static function generateString($count) {
		$characters = 'abcdefghijklmnopqrstuvwxyz';
		$randomString = '';
		for ($i = 0; $i < $count; $i++) {
			$randomString .= $characters[rand(0, strlen($characters) - 1)];
		}
		return $randomString;
	}
        public static function generateAlphanumeric($count) {
		$characters = 'abcdefghijklmnopqrstuvwxyz1234567890';
		$randomString = '';
		for ($i = 0; $i < $count; $i++) {
			$randomString .= $characters[rand(0, strlen($characters) - 1)];
		}
		return $randomString;
	}
	
	Public Static function csv_to_array($url)
	{
		
		$row   = 1;
		$i     = 0;
		$lists = '';
		
		$context = stream_context_create(array('http'=>array('timeout'=>15)));
		
		if (($handle = fopen($url, "r", false, $context)) !== FALSE) {

			while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
				$num = count($data);
				$row++;

				for ($c=0; $c < $num; $c++) {
					$lists[$i][$c]   = $data[$c];
				}
				$i++;
			}
			fclose($handle); 
		}
		
		return $lists;
		
	}
	
	public static function zeroFill($str, $len)
    {
        settype($len, "integer");
        settype($str, "string");
        $buf = '';
        for ($i = strlen($str); $i <= $len; $i++) {
            $buf .= '0';
        }
        return $buf . $str;
    }
	
	public static function displayNumberFormat($input, $decimal = 2, $dec_point = ".", $thousands_sep = ",")
    {
        return number_format($input, $decimal, $dec_point, $thousands_sep);
    }
	
	public static function formatNegativeAmount($input, $decimal = 2)
    {
        if (round($input, 2) < 0)
            return '<font color="red">' . App::displayNumberFormat($input, $decimal) .
                '</font>';
        else
            return App::displayNumberFormat($input, $decimal);
    }
	
	public static function displayAmount($amount, $decimal = 2)
    {
        return number_format(abs($amount), $decimal);
    }
	
	public static function getRemoteIp()
    {
		if(isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			$ips = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
			return $ips[0];
        } else return $_SERVER['REMOTE_ADDR'];
    }
	
	public static function getServerIp()
    {
        return $_SERVER['SERVER_ADDR'];
    }
	
	public static function getSessionToken($userid, $type = CBO_LOGINTYPE_USER)
    {
		return hash('sha256', $type.$userid.App::getSessionId().date('Y-m-d H:i:s'));
        
    }
	
	public static function getSessionId()
    {
        if (session_id() == "") {
            session_start();
        } else
            return session_id();
    }
	
	public static function getSessionHash($userid)
    {
		$userAgent = (isset($_SERVER['HTTP_USER_AGENT']))?$_SERVER['HTTP_USER_AGENT']:'unknown';
        return md5($userAgent . App::getRemoteIp() . $userid);
    }
	
	public static function getCityNameByGeoIp($ip) {
		
		$location = GeoIP::getLocation($ip);

		return $location['country'] . ' - ' . $location['city'];
    }
	
	public static function generatePassword($min = 5, $max = 5)
    {

        $pwd = ""; // to store generated password

        for ($i = 0; $i < rand($min, $max); $i++) {
            $num = rand(48, 122);
            if (($num > 97 && $num < 122) && ($num != 108 && $num != 111)) { // a to z ,exclude l
                $pwd .= chr($num);
            } else
                if (($num > 48 && $num < 57) && ($num != 48 && $num != 49)) { // number ,exclude 0,1
                    $pwd .= chr($num);
                } else {
                    $i--;
                }
        }
        return $pwd;
    }
	
	public static function now()
    {
        return date('Y-m-d H:i:s');
    }
	
	public static function formatDateTime($format, $date)
    {
        return date(App::getDateFormatById($format), strtotime($date));
    }
	
	public static function spanDateTime($sec, $date = '', $format = 1)
    {
        if (strlen($date) > 0)
            $date = strtotime($date);
        else
            $date = strtotime(App::getDateTime());

        $sec = (int)$sec;
        $date = $date + $sec;

        return date(App::getDateFormatById($format), $date);
    }
	
	public static function getDateTime($format = 1, $gmt = '')
    {
        if (strlen($gmt) == 0)
            $gmt = App::getGMTOffset();

        /**
         * Use the web server time
         */
        //$timestamp = time();

        /**
         * Use the db server time
         */
        $now = time();
        if (!isset(self::$dbUnixTimestamp[$now])) {
            self::$dbUnixTimestamp[$now] = time();
        }
        $timestamp = self::$dbUnixTimestamp[$now];

        //return the time in GMT 0 plus the $gmt offset
        return gmdate(App::getDateFormatById($format), $timestamp + (3600 * $gmt));
    }
	
	public static function getDateFormatById($id = 1)
    {
        /*----------------
        1 = datetime
        2 = timestamp
        3 = date
        4 = time
        ----------------*/
        $format = array(
			1 => 'Y-m-d H:i:s', 
			2 => 'YmdHis', 
			3 => 'Y-m-d', 
			4 => 'H:i:s', 
			5 =>'H:i', 
			6 => 'Y-m-d H:i', 
			7 => 'F m, Y H:i:s', 
			8 => 'D, jS F Y g:ia', 
			9 => 'Y-m',
            10 => 'd-m-Y', 
			11 => 'Y-m-01', //Beginning of the month
            12 => 'Y-m-t', //End of the month
            13 => '01-m-Y', //Beginning of the month
            14 => 'd-m-Y H:i', 
			15 => 'l, jS M Y', 
			16 => 'd/m/Y', 
			17 => 'ymd', 
			18 => 'N', //day of the week - 1 (for Monday) through 7 (for Sunday)
            19 => 'd M', //day - month, eg: 01 Mar
            20 => 'Y-m-d 00:00:00', //beginning of the day
            21 => 'Y-m-d 23:59:59', //end of the day
            22 => 'Y-m-01 00:00:00', //Beginning of the month
            23 => 'Y-m-t 23:59:59', //End of the month
            24 => 't-m-Y', //End of the month (day/month/year)
            25 => 'd M y - (T) H:i:s', 
			26 => 'dmY', 
			27 => 'H:i:s A', 
			//28 => 'Y'.App::getText('CBO_GUI_YEAR').'m'.App::getText('CBO_GUI_MONTH').'d'.App::getText('CBO_GUI_DAY').' H:i:s A', 
			29 => 'ym',
			30 => 'Y-01-01', //Beginning of the year
			31 => 'D', //Beginning of the year
			32 => 'm/d H:i:s', 
		);
        if (isset($format[$id]))
            return $format[$id];
        else
            return '';
    }
	
	public static function getGMTOffset()
    {
        if (defined('SYSTEM_TIMEZONE'))
            $gmt = SYSTEM_TIMEZONE;
        else {
            $gmt = (int)date('O');
            $gmt = $gmt / 100;
        }
        return $gmt;
    }
	
	public static function arrayUnshift(&$array, $item)
    {
        $newArray = array();
        if (is_string($item)) {
            $newArray[0] = $item;
        } else
            if (is_array($item)) {
                foreach ($item as $key => $val) {
                    $newArray[$key] = $val;
                }
            }

        if (is_array($array)) {
            foreach ($array as $key => $val) {
                $newArray[$key] = $val;
            }
        }

        $array = $newArray;
        $newArray = array();
    }
	
	public static function float($str)
    {
        if (preg_match("/([0-9\.,-]+)/", $str, $match)) {
            $str = $match[0];
            if (strstr($str, ',')) {
                $str = str_replace(',', '', $str);
                return floatval($str);
            } else
                return floatval($str);
        } else
            return 0;
    }

	public static function formatAddTabUrl($title, $name, $url) {
		return '<a href="#" onclick="parent.addTab(\''.$title.'\', \''.$url.'\')">'.$name.'</a>';
	}
	
	public static function displayPercentage($amount, $decimal = 2, $negative = false)
    {
        $percentage = App::displayAmount($amount, $decimal) . '%';
        if ($negative) {
            if ($amount < 0)
                $percentage = '<font color="red">-' . $percentage . '</font>';
        }

        return $percentage;
    }
	
	public static function dialogShowMessage($msg) {

	  	if(strlen($msg) > 0) {
	  		header('Content-Type: text/html; charset=UTF8');
	  		$message = preg_replace("/'/", "\\'", $msg);
			echo '<html><style>html, body { background:	ThreeDFace; overflow-x: hidden;}</style><body>';	  			echo '</body></html>';
	  		echo "
	  		<script>
	  			alert('$message');
	    	</script>";
	    	echo '</body></html>';
	    }
	    
	}
	
	public static function getText($text, $values = array())
    {
  
        if (isset($text)) {
            $buf = $text;
            if (count($values) > 0) {
                for ($i = 0; $i < count($values); $i++) {
                    $buf = str_replace('%' . $i, $values[$i], $buf);
                }
            }
            return $buf;
        } else
            return false;
    }

	
	public static function getSysVariable($key, $langbased = false, $sortby = 'key', $value = true)
    {
        $sysVar = new Sysvar;
        if ($langbased)
            $key = $key . '_' . SYSTEM_LANGCODE;

        if ($variables = $sysVar->getVariable($key, $value)) {
            if (is_array($variables) && $key != 'countrycode_1') {
                if ($sortby == 'key')
                    ksort($variables);
                else
                    if ($sortby == 'val')
                        sort($variables);
            }
        }

        return $variables;
    }
	
	public static function setupPanelForm($action = '', $params = '', $options = '', $toAdd = true) {
		$form = new PanelForm;
		$form->addInput('hidden', '', 'mdl', '');
		if(!empty($action))
		$form->addInput('hidden', '', 'action', $action);

		if(isset($params['sid']) && strlen($params['sid']) > 0)
		$form->addInput('hidden', '', 'sid', $params['sid']);

		$systemParams = array('sid', 'ilt', 'ist', 'sfld', 'sodr', 'aflt', 'aqopt', 'aqfld', 'aqtxt' , 'aqrng');

		foreach($systemParams as $sysParam) {
			if(isset($params[$sysParam])) {
				if(is_string($params[$sysParam]) || is_int($params[$sysParam]))
				$form->addInput('hidden', '', $sysParam, $params[$sysParam]);
				else if(is_array($params[$sysParam])) {
					foreach($params[$sysParam] as $subkey => $subparam) {
						$form->addInput('hidden', '', $sysParam.'['.$subkey.']', $subparam);
					}
				}
			}
		}

		if(!isset($options['custombutton'])) {
			$form->addButton(Lang::get('COMMON.OK'), 'sbtt', '', 'submit');
			if($toAdd == false)
			$form->addButton(Lang::get('COMMON.APPLY'), 'sbtt', '', 'submit');
			$form->addButton(Lang::get('COMMON.BACK'), 'sbtt', 'document.location.href = ""');
		}
		return $form;
	}
	
	public static function multiRequest($data, $options = array()) {
 
		$curly = array();
		$result = array();
		
		$mh = curl_multi_init();
		
		foreach ($data as $id => $d) {
 
		$curly[$id] = curl_init();
	 
		$url = (is_array($d) && !empty($d['url'])) ? $d['url'] : $d;
		curl_setopt($curly[$id], CURLOPT_URL,            $url);
		curl_setopt($curly[$id], CURLOPT_HEADER,         0);
		curl_setopt($curly[$id], CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curly[$id], CURLOPT_TIMEOUT, 8);
	 
		if (is_array($d)) {
		  if (!empty($d['post'])) {
			curl_setopt($curly[$id], CURLOPT_POST,       1);
			curl_setopt($curly[$id], CURLOPT_POSTFIELDS, $d['post']);
		  }
		}
	 
		if (!empty($options)) {
		  curl_setopt_array($curly[$id], $options);
		}
	 
		curl_multi_add_handle($mh, $curly[$id]);
	  }
	 
	  $running = null;
	  do {
		curl_multi_exec($mh, $running);
	  } while($running > 0);
	 
	  foreach($curly as $id => $c) {
		$result[$id] = curl_multi_getcontent($c);
		curl_multi_remove_handle($mh, $c);
	  }
	 
	  curl_multi_close($mh);
	 
	  return $result;
	  
	}
	
	public static function sendMail($contactname, $contactemail, $subject, $message, $html = false, $forward = false, $sender = false, $charset = 'iso-8859-1') {
		$fields = array(
			'api_user' 	=> Configs::getParam("SYSTEM_SENDGRID_ACCOUNT"),
			'api_key'  	=> Configs::getParam("SYSTEM_SENDGRID_PASSWORD"),
			'to' 		=> $contactemail,
			'toname' 	=> $contactname,
			'subject' 	=> $subject,
			'from' 		=> Configs::getParam("SYSTEM_SUPPORTEMAIL"),
			'fromname'  => Configs::getParam("SYSTEM_SUPPORTNAME"),
		);
		
		if($html) $fields['html'] = $message;
		else $fields['text'] = $message;
		
		$fields_string = '';
		foreach($fields as $key => $value) { $fields_string .= $key.'='.$value.'&'; }
		rtrim($fields_string, '&');
		
		//open connection
		$ch = curl_init();

		//set the url, number of POST vars, POST data
		curl_setopt($ch,CURLOPT_URL, Configs::getParam("SYSTEM_SENDGRID_APIURL"));
		curl_setopt($ch,CURLOPT_POST, count($fields));
		curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		//execute post
		$result = curl_exec($ch);

		//close connection
		curl_close($ch);
		$result = json_decode($result);
		
		if($result && $result->message == 'success') return true;
		else return false;
    }

	public static function getRealIpAddr()
	{
		if (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //check ip from share internet
		{
		  $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
		}
		elseif (!empty($_SERVER['HTTP_CLIENT_IP']))   //to check ip is pass from proxy
		{
		  $ip=$_SERVER['HTTP_CLIENT_IP'];
		}
		else
		{
		  $ip=$_SERVER['REMOTE_ADDR'];
		}
		return $ip;
	}

	public static function getStatusOptions(){
		
		$status = array(
			1 => Lang::get('COMMON.ACTIVE'),
			0 => Lang::get('COMMON.SUSPENDED'),
		);

		return $status;
	}
	
	public static function generateKey($count)
        {
            mt_srand((double)microtime() * 1000000);
            $key = "";
            for ($i = 0; $i < $count; $i++) {
                $c = mt_rand(0, 2);
                if ($c == 0) {
                    $key .= chr(mt_rand(65, 90));
                } elseif ($c == 1) {
                    $key .= chr(mt_rand(97, 122));
                } else {
                    $key .= mt_rand(0, 9);
                }
            }
            return $key;
        }
        
        public static function generateNum($count)
        {
            mt_srand((double)microtime() * 1000000);
            $num = "";
            for ($i = 0; $i < $count; $i++) {
                if ($i == 0)
                    $num .= mt_rand(1, 9);
                else
                    $num .= mt_rand(0, 9);
            }
            return $num;
        }
	
	public static function getEncryptPassword($password) {
		return md5(trim($password).'t53h45lo7y');
	}
	
	public static function SMS($msg,$telmobile){
		
		$query_string = "?username=".Configs::getParam("SYSTEM_SMS_USERNAME")."&password=".Configs::getParam("SYSTEM_SMS_PASSWORD");
		$query_string .= "&message=".rawurlencode($msg)."&mobile=".rawurlencode($telmobile);
		
		if( Config::get('setting.front_path') == 'royalewin')
			$query_string .= "&sender=".rawurlencode('RW')."&type=1";   
		else
			$query_string .= "&sender=".rawurlencode('INFW')."&type=1";   

		//set POST variables
		$url = 'http://my.bulk8sms.com/websmsapi/ISendSMS.aspx'.$query_string;

		//open connection
		$ch = curl_init();

		//set the url
		curl_setopt($ch, CURLOPT_URL, $url);

		//receive server response
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);

		curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER,false);
		
		curl_setopt($ch, CURLOPT_TIMEOUT, 8 );

		//execute post
		$result = curl_exec($ch);

		//close connection
		curl_close($ch);
		
		return $result;
    }
	
	public static function checkIsAValidDate($myDateString){
		return (bool)strtotime($myDateString);
	}
	
	public static function check_process($process){
		
		$lockfile = storage_path('app').'/'.$process.'.lock';

		if(file_exists($lockfile))
		{
			echo 'Hey, another process is already running!';
			exit(0);

		}else
		{
			touch($lockfile);
		}
	}
	
	public static function unlock_process($process){
		
		unlink(storage_path('app').'/'.$process.'.lock');
	}

    public static function addMultiSubdomain($domain,$name,$content){

        $success = true;

        if (!is_array($domain)) {
            return self::addSubdomain($domain, $name, $content);
        } else {
            foreach ($domain as $d) {
                $res = self::addSubdomain($d, $name, $content);

                if ($success) {
                    $success = $res;
                }
            }
        }

        return $success;

    }
	
	public static function addSubdomain($domain,$name,$content){
        $success = false;

		$url = 'https://api.cloudflare.com/client/v4/zones/'.Config::get('setting.CF_API').'/dns_records';
        
		$fields = array('type' => 'A',
                        'name' => $name,
                        'content' => $content, 
			'ttl' => 120,
                        'proxied' => true);
        
		$ch = curl_init();
		curl_setopt($ch,CURLOPT_URL, $url);
		curl_setopt($ch,CURLOPT_POST, count($fields));
		curl_setopt($ch,CURLOPT_POSTFIELDS, json_encode($fields));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-Auth-Email: '.Config::get('setting.cloudflare_email'),'X-Auth-Key: '.Config::get('setting.cloudflare_token'),'Content-Type: application/json'));
		curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); 
	
		$result = curl_exec($ch);
		$json_output = json_decode($result);
        
        if($json_output->success == true){
            $success = true;
        }
	
		curl_close($ch);
		
		return $success;

    }


}