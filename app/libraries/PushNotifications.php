<?php namespace App\libraries;
// Server file
class PushNotifications {

	// (Android)API access key from Google API's Console.
	private static $API_ACCESS_KEY = 'AIzaSyBf5yeutFx6fEJxG3MBLb0Z7f7QJ4C-fu8';
	// (iOS) Private key's passphrase.
	private static $passphrase = 'joashp';
	// (Windows Phone 8) The name of our push channel.
        private static $channelName = "joashp";
	
	// Change the above three vriables as per your app.

	public function __construct() {
		exit('Init function is not allowed');
	}
	
        // Sends Push notification for Android users
	/* public static function android($data, $reg_id) {
	        $url = 'https://android.googleapis.com/gcm/send';
	        $message = array(
	            'body' 	 	 	=> '<body>',
	            'title' 	 	=> $data['mtitle'],
				'ads_id'     	=> '',
				'vibrate' 	 	=> 1,
				'sound' 	 	=> 1,
				'largeIcon'  	=> 'large_icon',
	            'smallIcon'  	=> 'small_icon',
	            'message' 	 	=> $data['mdesc'],
				'click_action'  => 'OPEN_NOTIFICATION_ACTIVITY',
	            'msgcnt' 	 	=> 1,
	            
	        ); 
			
			$notification = array(
	            'body' 	 	 	=> '<body>',
	            'title'      	=> '<title>',
	            'ads_id'    	=> '',
	            'vibrate'    	=> 1,
	            'sound' 	 	=> 1,
	            'largeIcon'  	=> 'large_icon',
	            'smallIcon'  	=> 'small_icon',
	            'click_action'  => 'OPEN_NOTIFICATION_ACTIVITY',
	        );
	        
	
			$headers   = array();
			$headers[] = 'Authorization: key=' .self::$API_ACCESS_KEY;
			$headers[] = 'Content-Type: application/json';
	
	        $fields = array(
	            'registration_ids' => array($reg_id),
	            'data' 			   => $message,
	            'notification' 	   => $notification,
	        );
	
	    	return PushNotifications::useCurl($url, $headers, json_encode($fields));
    	} */
		
	public static function android($data, $gcmid)
	{
        $API_ACCESS_KEY = 'AAAAHUlB3JI:APA91bEc5t5xjnmYXrnCF1I3HlvUc7bUF3aok1YY3JaX32q8DuobcU7UPZ6h7MJWuJmGKYrR0MH-w4UJDm7KRzw8SGrFdIucXp-DTOmGD1pkYH-fxDCLnTX1_WN0gBbvwZuznLC0lh7b';
         //FCM api URL
        $url = 'https://fcm.googleapis.com/fcm/send';
        //api_key available in Firebase Console -> Project Settings -> CLOUD MESSAGING -> Server key
        $server_key =  $API_ACCESS_KEY;
          $msg = array(
            'body'   		 =>  $data['mbody'],
            'title'     	 =>  $data['mtitle'],
            'vibrate'  		 =>  1,
            'sound'    		 =>  1,
            'largeIcon'		 =>  'large_icon',
            'smallIcon'		 =>  'small_icon',
			'icon'			 =>  'myicon',
            'click_action' 	 =>  'OPEN_NOTIFICATION_ACTIVITY'
            );
		
        
        $fields = array();
        $fields['notification'] = $msg;
        $fields['data'] = $msg;
        $input['ids'] = array($gcmid);
        if(is_array($input['ids'])){
            $fields['registration_ids'] = $input['ids'];
        }else{
            $fields['to'] = $input['ids'];
        }
        //header with content_type api key
        $headers = array(
            'Content-Type:application/json',
            'Authorization:key='.$server_key
        );
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('FCM Send Error: ' . curl_error($ch));
        }
        curl_close($ch);
        return $result;
    }
		
/* 	public static function android($data, $reg_id) {
	    $url = 'https://android.googleapis.com/gcm/send';
	    $message = array(
	        'title' => $data['mtitle'],
	        'message' => $data['mdesc'],
	        'subtitle' => '',
	        'tickerText' => '',
	        'msgcnt' => 1,
	        'vibrate' => 1
	    );
	    

		$headers   = array();
		$headers[] = 'Authorization: key=' .self::$API_ACCESS_KEY;
		$headers[] = 'Content-Type: application/json';
	
	    $fields = array(
	        'registration_ids' => array($reg_id),
	        'data' => $message,
	    );
	
		return PushNotifications::useCurl($url, $headers, json_encode($fields));
    } */
	
	// Sends Push's toast notification for Windows Phone 8 users
	public function WP($data, $uri) {
		$delay = 2;
		$msg =  "<?xml version=\"1.0\" encoding=\"utf-8\"?>" .
		        "<wp:Notification xmlns:wp=\"WPNotification\">" .
		            "<wp:Toast>" .
		                "<wp:Text1>".htmlspecialchars($data['mtitle'])."</wp:Text1>" .
		                "<wp:Text2>".htmlspecialchars($data['mdesc'])."</wp:Text2>" .
		            "</wp:Toast>" .
		        "</wp:Notification>";
		
		$sendedheaders =  array(
		    'Content-Type: text/xml',
		    'Accept: application/*',
		    'X-WindowsPhone-Target: toast',
		    "X-NotificationClass: $delay"
		);
		
		$response = $this->useCurl($uri, $sendedheaders, $msg);
		
		$result = array();
		foreach(explode("\n", $response) as $line) {
		    $tab = explode(":", $line, 2);
		    if (count($tab) == 2)
		        $result[$tab[0]] = trim($tab[1]);
		}
		
		return $result;
	}
	
        // Sends Push notification for iOS users
	public function iOS($data, $devicetoken) {

		$deviceToken = $devicetoken;

		$ctx = stream_context_create();
		// ck.pem is your certificate file
		stream_context_set_option($ctx, 'ssl', 'local_cert', 'ck.pem');
		stream_context_set_option($ctx, 'ssl', 'passphrase', self::$passphrase);

		// Open a connection to the APNS server
		$fp = stream_socket_client(
			'ssl://gateway.sandbox.push.apple.com:2195', $err,
			$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

		if (!$fp)
			exit("Failed to connect: $err $errstr" . PHP_EOL);

		// Create the payload body
		$body['aps'] = array(
			'alert' => array(
			    'title' => $data['mtitle'],
                'body' => $data['mdesc'],
			 ),
			'sound' => 'default'
		);

		// Encode the payload as JSON
		$payload = json_encode($body);

		// Build the binary notification
		$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

		// Send it to the server
		$result = fwrite($fp, $msg, strlen($msg));
		
		// Close the connection to the server
		fclose($fp);

		if (!$result)
			return 'Message not delivered' . PHP_EOL;
		else
			return 'Message successfully delivered' . PHP_EOL;

	}
	
	// Curl 
	private static function useCurl( $url, $headers, $fields = null) {
	        // Open connection
	        $ch = curl_init();
	        if ($url) {
	            // Set the url, number of POST vars, POST data
	            curl_setopt($ch, CURLOPT_URL, $url);
	            curl_setopt($ch, CURLOPT_POST, true);
	            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	     
	            // Disabling SSL Certificate support temporarly
	            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	            if ($fields) {
	                curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
	            }
	     
	            // Execute post
	            $result = curl_exec($ch);
	            if ($result === FALSE) {
	                die('Curl failed: ' . curl_error($ch));
	            }
	     
	            // Close connection
	            curl_close($ch);
	
	            return $result;
        }
    }
    
}
?>