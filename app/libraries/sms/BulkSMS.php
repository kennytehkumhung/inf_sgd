<?php namespace App\libraries\sms;

use App\Models\Configs;

class BulkSMS extends BaseSMS
{
	
	const NAME = 'bulksms';

	public function sms($message, $telMobile){
		
		$username = Configs::getParam('SYSTEM_BULKSMS_USERNAME');
		if (strlen($username) < 1) {
            return false;
        }
		
		$telMobile = str_replace('+', '', $telMobile);
		
		$url = 'http://www.bulksms2u.com/websmsapi/ISendSMS.aspx' .
            '?username=' . rawurlencode($username) .
            '&password=' . rawurlencode(Configs::getParam('SYSTEM_BULKSMS_PASSWORD')) .
            '&message=' . rawurlencode($message) .
            '&mobile=' . $telMobile .
            '&sender=' . $telMobile .
            '&type=1';
			
		return $this->sendRequest($url);	
	}
	
	public function creditBalance(){
		$username = Configs::getParam('SYSTEM_BULKSMS_USERNAME');
		
		if (strlen($username) < 1) {
            return false;
        }
		
		$url = 'http://api.sms88.my/websmsapi/creditsLeft.php' .
            '?username=' . rawurlencode($username) .
            '&password=' . rawurlencode(Configs::getParam('SYSTEM_BULKSMS_PASSWORD'));

        return $this->sendRequest($url);
	}
	
	private function sendRequest($url) {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
        curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER,false);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30 );

        $result = curl_exec($ch);

        curl_close($ch);

        return $result;
    }
}