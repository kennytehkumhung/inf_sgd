<?php namespace App\libraries\sms;

use Config;
use Session;

abstract class BaseSMS
{

    public function formatTel($currency, $telmobile) {

        if ($currency == 'MYR') {
            // New method has mobile number restriction (must starts with xx)
            // Old method remain here to support old customer's mobile number.
            if (starts_with($telmobile, '0')) {
                return '6' . $telmobile;
            } elseif (starts_with($telmobile, '60')) {
                return $telmobile;
            }
        } elseif ($currency == 'TWD') {
            if (starts_with($telmobile, '0')) {
                // Remove leading 0 and add country code.
                return '886' . substr($telmobile, 1);
            }
        }

        return $telmobile;
    }

    public function formatTelMYR($telmobile) {
        // Legacy function to support RYW operator.
        return $this->formatTel('MYR', $telmobile);
    }

    public function getOperatorName($opcode, $prepend = '', $postpend = '') {

        if (strlen($opcode) < 1) {
            $opcode = Config::get('setting.opcode');
        }

        $name = '';

        switch ($opcode) {
            case 'RYW':
                $name = 'RW';
                break;
            case 'IFW':
                $name = 'Infiniwin';
                break;
            case 'IFT':
                $name = '無極限娛樂網';
                break;
        }

        if ($name != '') {
            return $prepend . $name . $postpend;
        }

        return '';
    }

    public function getShortCurrency($crccode) {

        if (strlen($crccode) < 1) {
            $crccode = Session::get('currency');
        }

        switch ($crccode) {
            case 'MYR':
                return 'RM';
        }

        return $crccode;
    }
}
