<?php namespace App\libraries\sms;

use App\Models\Configs;

class SMSMoDing extends BaseSMS
{

    const NAME = 'smsmoding';

    public function sms($message, $telMobile, $language = 'tw') {

        $username = Configs::getParam('SYSTEM_SMSMODING_USERNAME');

        if (strlen($username) < 1) {
            return false;
        }

        $telMobile = str_replace('+', '', $telMobile);

        if ($language != 'en') {
            $message = urlencode($this->getUTF8($message));
        }

        // Decode message
        // $this->html_entity_decode_utf8($this->he2str($message));

        if (starts_with($username, '0')) {
            $username = substr($username, 1);
        }

        $url = 'http://www.moding.com.tw/openAPI/sendSMS/' .
            '?username=' . rawurlencode($username) .
            '&password=' . rawurlencode(Configs::getParam('SYSTEM_SMSMODING_PASSWORD')) .
            '&SMS=' . $message .
            '&destinatingAddress=' . $telMobile .
            '&returnMode=1' .
            '&type=1';

        return $this->cleanResponse($this->sendRequest($url));
    }

    public function creditBalance() {

        $username = Configs::getParam('SYSTEM_SMSMODING_USERNAME');

        if (strlen($username) < 1) {
            return false;
        }

        if (starts_with($username, '0')) {
            $username = substr($username, 1);
        }

        $url = 'http://www.moding.com.tw/openAPI/getUserBalance/' .
            '?username=' . rawurlencode($username) .
            '&password=' . rawurlencode(Configs::getParam('SYSTEM_SMSMODING_PASSWORD'));

        // Format: credit, expiry date, destination, accept request from
        // Return -2: 0 balance or invalid username/password; -100: internal error, contact their support; -9999: not enough params

        $res = $this->cleanResponse($this->sendRequest($url));
        $res = explode('<br />', nl2br($res));
        $totalBalance = 0;

        foreach ($res as $r) {
            if (str_contains($r, ',')) {
                $tmp = explode(',', $r);

                if (is_numeric($tmp[0])) {
                    $totalBalance += $tmp[0];
                }
            }
        }

        return $totalBalance;
    }

    private function cleanResponse($response) {
        return preg_replace('/<\/?pre>/i', '', $response);
    }

    private function sendRequest($url) {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
        curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER,false);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30 );

        $result = curl_exec($ch);

        curl_close($ch);

        return $result;
    }

    private function getUTF8($str){
        $output = "";
        $encStr = $str;
        for($i=0; $i<strlen($str); $i=$i+1){
            $tmpCh = $this->uniord($encStr);
            if($tmpCh){
                if($tmpCh > 254){
                    $encStr = substr($encStr, 3, strlen($encStr)-3);
                    $i = $i + 2;
                }else{
                    $encStr = substr($encStr, 1, strlen($encStr)-1);
                }
                $tmpCh = strtoupper(dechex($tmpCh));
                $tmpCh = $this->fillZero($tmpCh);
                $output = $output."&#x".$tmpCh.";";
            }else{ //Unknown charaters
                $output = $output.substr($encStr, 0, 1);
                $encStr = substr($str, 1, strlen($encStr)-1);
            }
        }
        return $output;
    }

    private function he2str($he){
        $tmpHe = $he;
        $output = "";
        for($i=0; $i<strlen($tmpHe); $i=$i+1){
            if($i < strlen($tmpHe)-6){
                $tmpCh = substr($tmpHe, $i, 2);
            }else{
                $tmpCh="";
            }
            if($tmpCh == "%u"){
                $output = $output."&#x".substr($tmpHe, $i+2, 4).";";
                $i=$i+5;
            }else{
                $output = $output.substr($tmpHe, $i, 1);
            }
        }

        return $output;
    }

    private function code2utf($num)
    {
        if ($num < 128) return chr($num);
        if ($num < 2048) return chr(($num >> 6) + 192) . chr(($num & 63) + 128);
        if ($num < 65536) return chr(($num >> 12) + 224) . chr((($num >> 6) & 63) + 128) . chr(($num & 63) + 128);
        if ($num < 2097152) return chr(($num >> 18) + 240) . chr((($num >> 12) & 63) + 128) . chr((($num >> 6) & 63) + 128) . chr(($num & 63) + 128);
        return '';
    }

    private function uniord($ch) {

        $n = ord($ch{0});

        if ($n < 128) {
            return $n; // no conversion required
        }

        if ($n < 192 || $n > 253) {
            return false; // bad first byte || out of range
        }

        $arr = array(1 => 192, // byte position => range from
            2 => 224,
            3 => 240,
            4 => 248,
            5 => 252,
        );

        foreach ($arr as $key => $val) {
            if ($n >= $val) { // add byte to the 'char' array
                $char[] = ord($ch{$key}) - 128;
                $range  = $val;
            } else {
                break; // save some e-trees
            }
        }

        $retval = ($n - $range) * pow(64, sizeof($char));

        foreach ($char as $key => $val) {
            $pow = sizeof($char) - ($key + 1); // invert key
            $retval += $val * pow(64, $pow);   // dark magic
        }

        return $retval;
    }

    private function fillZero($str){
        if(strlen($str) < 1){
            return "0000";
        }else if(strlen($str) < 2){
            return "000".$str;
        }else if(strlen($str) < 3){
            return "00".$str;
        }else if(strlen($str) < 4){
            return "0".$str;
        }else{
            return $str;
        }
    }

    private function html_entity_decode_utf8($string)
    {
        static $trans_tbl;

        // replace numeric entities
//        $string = preg_replace_callback('~&#x([0-9a-f]+);~i', function ($m) {
        $string = preg_replace_callback('/&#x([0-9a-f]+);/i', function ($m) {
            return $this->code2utf(hexdec($m[1]));
        }, $string);

//        $string = preg_replace_callback('~&#([0-9]+);~', function ($m) {
        $string = preg_replace_callback('/&#([0-9]+);/', function ($m) {
            return $this->code2utf($m[1]);
        }, $string);

        // replace literal entities
        if (!isset($trans_tbl))
        {
            $trans_tbl = array();

            foreach (get_html_translation_table(HTML_ENTITIES) as $val=>$key)
                $trans_tbl[$key] = utf8_encode($val);
        }

        return strtr($string, $trans_tbl);
    }
}
