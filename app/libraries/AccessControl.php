<?php namespace App\libraries;

class AccessControl {


	static protected $instance = null;


	public $modules = array (
		1 => array(
			'id' 		=> 'FINANCE',
			'name'		=>'TRANSACTION',
			'module'	=> array(
				1 	=> array(
					'id'	=> 'TRANSACTION',
					'name'	=>'TRANSACTIONENQUIRY',
					'url'	=> array('mdl' => 'enquiry'),
					'task'	=> array(
						1 => array('id' => 'ENQUIRY', 'name' =>'ENQUIRY'),
						2 => array('id' => 'MANAGE', 'name' =>'APPROVE'),
						3 => array('id' => 'BATCH', 'name' =>'APPROVE'),
					),
				),
				2 	=> array(
					'id'	=> 'INSTANT',
					'name'	=>'INSTANT_TRANSACTION',
					'url'	=> array('mdl' => 'instant' , 'popout' => true),
					'task'	=> array(
						1 => array('id' => 'ENQUIRY', 'name' =>'ENQUIRY'),
					),
				),
				3 	=> array(
					'id'	=> 'ADJUSTMENT',
					'name'	=>'TRANADJUST',
					'url'	=> array('mdl' => 'adjustment'), //a href
					'task'	=> array(
						1 => array('id' => 'DEPOSIT', 'name' =>'APPROVE'),
						2 => array('id' => 'ADJUST', 'name' =>'APPROVE'),
					),
				),
			/* 	4 	=> array(
					'id'	=> 'FAILTRANSFER',
					'name'	=> 'Failed Transfer',
					'url'	=> array('mdl' => 'failtrans'),
					'task'	=> array(
						1 => array('id' => 'DEPOSIT', 'name' =>'ENQUIRY'),
					),
				), */
			),
		),
		2 => array(
			'id' 		=> 'CS',
			'name'		=>'MEMBER',
			'module'	=> array(
				1 	=> array(
					'id'	=> 'ACCENQ',
					'name'	=>'MEMBERENQUIRY',
					'url'	=> array('mdl' => 'member'),
					'task'	=> array(
						1 => array('id' => 'ENQUIRY', 'name' =>'ENQUIRY'),
						2 => array('id' => 'EDIT', 'name' =>'EDIT'),
						3 => array('id' => 'STATUS', 'name' =>'EDITSTATUS'),
						4 => array('id' => 'TAGMEMBER', 'name' =>'TAGMEMBER'),
						6 => array('id' => 'EDITFULLNAME', 'name' => 'EDITFULLNAME'),
						7 => array('id' => 'VIEWCONTACTINFO', 'name' => 'VIEWCONTACTINFO'),
						8 => array('id' => 'VIEWCASHLEDGERINFO', 'name' => 'VIEWCASHLEDGERINFO'),
						9 => array('id' => 'MEMBERPNLFILTERPRODUCT', 'name' => 'MEMBERPNLFILTERPRODUCT'),
						10 => array('id' => 'VIEWFULLTRANSACTION', 'name' => 'VIEWFULLTRANSACTION'),
					),
				),
				
			),
		),
		3 => array(
			'id' 		=> 'REPORT',
			'name'		=>'REPORT',
			'module'	=> array(
				3 	=> array(
					'id'	=> 'REPORTSIMPLE',
					'name'	=>'REPORTSIMPLE',
					'url'	=> array('mdl' => 'summary'),
					'task'	=> array(
						1 => array('id' => 'ENQUIRY', 'name' =>'ENQUIRY'),
					),
				),
				1 	=> array(
					'id'	=> 'REPORTTOTAL',
					'name'	=>'REPORTTOTAL',
					'url'	=> array('mdl' => 'profitloss'),
					'task'	=> array(
						1 => array('id' => 'ENQUIRY', 'name' =>'ENQUIRY'),
					),
				),	
				2 	=> array(
					'id'	=> 'REPORTTOTAL2',
					'name'	=>'REPORTTOTALNEW',
					'url'	=> array('mdl' => 'profitloss2'),
					'task'	=> array(
						1 => array('id' => 'ENQUIRY', 'name' =>'ENQUIRY'),
					),
				),
		/* 		3 	=> array(
					'id'	=> 'ALLBET',
					'name'	=>'BETENQUIRY',
					'url'	=> array('mdl' => 'allbet'),
					'task'	=> array(
						1 => array('id' => 'ENQUIRY', 'name' =>'ENQUIRY'),
					),
				), */
				10 	=> array(
					'id'	=> 'DEPOSIT2',
					'name'	=>'DAILYSUMMARY',
					'url'	=> array('mdl' => 'dailyReport'),
					'task'	=> array(
						1 => array('id' => 'ENQUIRY', 'name' =>'ENQUIRY'),
					),
				),
				4 	=> array(
					'id'	=> 'BANKSUMMARY',
					'name'	=>'BANKSUMMARY',
					'url'	=> array('mdl' => 'bankReport'),
					'task'	=> array(
						1 => array('id' => 'DEPOSIT', 'name' =>'APPROVE'),
						2 => array('id' => 'ADJUST', 'name' =>'APPROVE'),
					),
				),
			/* 	8 	=> array(
					'id'	=> 'DEPOSIT',
					'name'	=>'DEPOSITREPORT',
					'url'	=> array('mdl' => 'depositReport'),
					'task'	=> array(
						1 => array('id' => 'ENQUIRY', 'name' =>'ENQUIRY'),
					),
				), */
				5 	=> array(
					'id'	=> 'INCENTIVE',
					'name'	=>'INCENTIVEREPORT',
					'url'	=> array('mdl' => 'rebateReport'),
					'task'	=> array(
						1 => array('id' => 'DEPOSIT', 'name' =>'APPROVE'),
						2 => array('id' => 'ADJUST', 'name' =>'APPROVE'),
					),
				),
				27 	=> array(
					'id'	=> 'DAILYREBATE',
					'name'	=> 'DAILYREBATEREPORT',
					'url'	=> array('mdl' => 'dailyrebateReport'),
					'task'	=> array(
						1 => array('id' => 'DEPOSIT', 'name' =>'APPROVE'),
						2 => array('id' => 'ADJUST', 'name' =>'APPROVE'),
					),
				),
				14 	=> array(
					'id'	=> 'MEMBERMONTH',
					'name'	=>'MEMBERREPORT',
					'url'	=> array('mdl' => 'memberReport'),
					'task'	=> array(
						1 => array('id' => 'ENQUIRY', 'name' =>'ENQUIRY'),
					),
				),	
				
				15 	=> array(
					'id'	=> 'PROMOTION',
					'name'	=>'PROMOTION',
					'url'	=> array('mdl' => 'promotionReport'),
					'task'	=> array(
						1 => array('id' => 'ENQUIRY', 'name' =>'ENQUIRY'),
					),
				), 
				16 	=> array(
					'id'	=> 'CHANNELREPORT',
					'name'	=>'CHANNELREPORT',
					'url'	=> array('mdl' => 'channelReport'),
					'task'	=> array(
						1 => array('id' => 'ENQUIRY', 'name' =>'ENQUIRY'),
					),
				),
				17 	=> array(
					'id'	=> 'CASHBACKREPORT',
					'name'	=>'CASHBACKREPORT',
					'url'	=> array('mdl' => 'cashbackReport'),
					'task'	=> array(
						1 => array('id' => 'ENQUIRY', 'name' =>'ENQUIRY'),
					),
				),
		/* 	    17 	=> array(
					'id'	=> 'BONUSREPORT',
					'name'	=>'BONUSSUMMARY',
					'url'	=> array('mdl' => 'bonusSummary'),
					'task'	=> array(
						1 => array('id' => 'ENQUIRY', 'name' =>'ENQUIRY'),
					),
				), */
			/* 	18 	=> array(
					'id'	=> 'ONLINEMEMBER',
					'name'	=> 'Online Member',
					'url'	=> array('mdl' => 'report'),
					'task'	=> array(
						1 => array('id' => 'ENQUIRY', 'name' =>'ENQUIRY'),
					),
				),	 */
				19 	=> array(
					'id'	=> 'BONUSREPORT2',
					'name'	=>'BONUSREPORT',
					'url'	=> array('mdl' => 'bonusReport'),
					'task'	=> array(
						1 => array('id' => 'ENQUIRY', 'name' =>'ENQUIRY'),
					),
				),
                20 	=> array(
                    'id'	=> 'REFERRALREPORT',
                    'name'	=>'REFERRALREPORT',
                    'url'	=> array('mdl' => 'referralReport'),
                    'task'	=> array(
                        1 => array('id' => 'ENQUIRY', 'name' =>'ENQUIRY'),
                    ),
                ),
				21  => array(
                    'id'	=> 'SMSLOGREPORT',
                    'name'	=> 'SMSLOGREPORT',
                    'url'	=> array('mdl' => 'smsLogReport'),
                    'task'	=> array(
                        1 => array('id' => 'ENQUIRY', 'name' =>'ENQUIRY'),
                    ),
				),
                22  => array(
                    'id'	=> 'LUCKYSPINREPORTMYR',
                    'name'	=> 'LUCKYSPINREPORTMYR',
                    'url'	=> array('mdl' => 'luckySpinReport?currency=MYR'),
                    'task'	=> array(
                        1 => array('id' => 'ENQUIRY', 'name' =>'ENQUIRY'),
                    ),
                ),
                25  => array(
                    'id'	=> 'LUCKYSPINREPORTIDR',
                    'name'	=> 'LUCKYSPINREPORTIDR',
                    'url'	=> array('mdl' => 'luckySpinReport?currency=IDR'),
                    'task'	=> array(
                        1 => array('id' => 'ENQUIRY', 'name' =>'ENQUIRY'),
                    ),
                ),
                23  => array(
                    'id'	=> 'MEMBERWALLETREPORT',
                    'name'	=> 'MEMBERWALLETREPORT',
                    'url'	=> array('mdl' => 'showMemberWallet'),
                    'task'	=> array(
                        1 => array('id' => 'ENQUIRY', 'name' =>'ENQUIRY'),
                    ),
                ),
                24  => array(
                    'id'	=> 'PREMIERLEAGUEREPORT',
                    'name'	=> 'PREMIERLEAGUEREPORT',
                    'url'	=> array('mdl' => 'premierLeagueMatchReport'),
                    'task'	=> array(
                        1 => array('id' => 'ENQUIRY', 'name' =>'ENQUIRY'),
                    ),
                ),
                26 	=> array(
                    'id'	=> 'REVERTBONUSREPORT',
                    'name'	=>'REVERTBONUSREPORT',
                    'url'	=> array('mdl' => 'revertBonusReport'),
                    'task'	=> array(
                        1 => array('id' => 'DEPOSIT', 'name' =>'APPROVE'),
                        2 => array('id' => 'ADJUST', 'name' =>'APPROVE'),
                    ),
                ),
				27 	=> array(
                    'id'	=> 'WCREPORT',
                    'name'	=>'WCREPORT',
                    'url'	=> array('mdl' => 'worldCupReport'),
                    'task'	=> array(
                        1 => array('id' => 'DEPOSIT', 'name' =>'APPROVE'),
                        2 => array('id' => 'ADJUST', 'name' =>'APPROVE'),
                    ),
                ),
				28 	=> array(
                    'id'	=> 'WCTOKENREPORT',
                    'name'	=>'WCTOKENREPORT',
                    'url'	=> array('mdl' => 'showTokenReport'),
                    'task'	=> array(
                        1 => array('id' => 'DEPOSIT', 'name' =>'APPROVE'),
                        2 => array('id' => 'ADJUST', 'name' =>'APPROVE'),
                    ),
                ),
            ),
		),
		4 => array(
			'id' 		=> 'AGENCY',
			'name'		=>'AFFILIATE',
			'module'	=> array(
				2 	=> array(
					'id'	=> 'REGISTRATION',
					'name'	=>'AGENTREG',
					'url'	=> array('mdl' => 'affiliateRegister'),
					'task'	=> array(
						1 => array('id' => 'ENQUIRY', 'name' =>'ENQUIRY'),
						2 => array('id' => 'EDIT', 'name' =>'EDIT'),
						3 => array('id' => 'VERIFY', 'name' =>'AUDIT'),
						4 => array('id' => 'APPROVE', 'name' =>'APPROVE'),
					),
				),
				3 	=> array(
					'id'	=> 'ENQUIRY',
					'name'	=>'AFFILIATEENQUIRY',
					'url'	=> array('mdl' => 'affiliateEnquiry'),
					'task'	=> array(
						1 => array('id' => 'ENQUIRY', 'name' =>'ENQUIRY'),
					),
				),
				4 	=> array(
					'id'	=> 'REPORT',
					'name'	=>'AFFILIATEREPORT',
					'url'	=> array('mdl' => 'affiliateReport'),
					'task'	=> array(
						1 => array('id' => 'ENQUIRY', 'name' =>'ENQUIRY'),
					),
				),
				8 	=> array(
                            'id'	=> 'AGTREPORT',
                            'name'	=>'AGENTREPORT',
                            'url'	=> array('mdl' => 'agentReport'),
                            'task'	=> array(
                                    1 => array('id' => 'ENQUIRY', 'name' =>'ENQUIRY'),
                            ),
                ),

	
		/* 		5 	=> array(
					'id'	=> 'COMMISSION',
					'name'	=>'AFFILIATECOMMISSION',
					'url'	=> array('mdl' => 'affiliateCommision'),
					'task'	=> array(
						1 => array('id' => 'ENQUIRY', 'name' =>'ENQUIRY'),
					),
				),
				6 	=> array(
					'id'	=> 'CAMPAIGN',
					'name'	=>'AGENTAD',
					'url'	=> array('mdl' => 'affiliate'),
					'task'	=> array(
						1 => array('id' => 'ENQUIRY', 'name' =>'ENQUIRY'),
						2 => array('id' => 'ADD', 'name' =>'ADDNEW'),
						3 => array('id' => 'EDIT', 'name' =>'EDIT'),
					),
				),
				7 	=> array(
					'id'	=> 'MOVE',
					'name'	=>'MOVEMEMBER',
					'url'	=> array('mdl' => 'affiliate'),
					'task'	=> array(
						1 => array('id' => 'ENQUIRY', 'name' =>'ENQUIRY'),
					),
				), */
				
			),
		),
		5 => array(
			'id' 		=> 'TOOL',
			'name'		=>'TOOL',
			'module'	=> array(
				1 	=> array(
					'id'	=> 'ANNOUCEMENT',
					'name'	=>'ANNOUNCEMENT',
					'url'	=> array('mdl' => 'announcement'),
					'task'	=> array(
						1 => array('id' => 'ENQUIRY', 'name' =>'ENQUIRY'),
						2 => array('id' => 'ADD', 'name' =>'ADDNEW'),
						3 => array('id' => 'EDIT', 'name' =>'EDIT'),
					),
				),
				2 	=> array(
					'id'	=> 'TRACKERLOG',
					'name'	=>'IPLOOKUP',
					'url'	=> array('mdl' => 'iptracker'),
					'task'	=> array(
						1 => array('id' => 'ENQUIRY', 'name' =>'ENQUIRY'),
					),
				),
		/* 		3 	=> array(
					'id'	=> 'BALANCE',
					'name'	=>'CHECKCURRENTBALANCE',
					'url'	=> array('mdl' => 'report'),
					'task'	=> array(
						1 => array('id' => 'ENQUIRY', 'name' =>'ENQUIRY'),
					),
				),
				4 	=> array(
					'id'	=> 'ACCOUNTNAME',
					'name'	=>'CHECKSAMENAMEACCOUNT',
					'url'	=> array('mdl' => 'report'),
					'task'	=> array(
						1 => array('id' => 'ENQUIRY', 'name' =>'ENQUIRY'),
					),
				),
				5 	=> array(
					'id'	=> 'ALLBALANCE',
					'name'	=>'CHECKALLBALANCE',
					'url'	=> array('mdl' => 'report'),
					'task'	=> array(
						1 => array('id' => 'ENQUIRY', 'name' =>'ENQUIRY'),
					),
				), */
			/* 	6 	=> array(
					'id'	=> 'IPAUDIT',
					'name'	=>'IPAUDIT',
					'url'	=> array('mdl' => 'iptracker'),
					'task'	=> array(
						1 => array('id' => 'ENQUIRY', 'name' =>'ENQUIRY'),
					),
				), */
				/* 10 	=> array(
					'id'	=> 'ROBOT',
					'name'	=>'ROBOT',
					'url'	=> array('mdl' => 'robot'),
					'task'	=> array(
						1 => array('id' => 'ENQUIRY', 'name' =>'ENQUIRY'),
					),
				), */
				11 	=> array(
					'id'	=> 'PRODUCT',
					'name'	=>'PRODUCTMAINTENANCE',
					'url'	=> array('mdl' => 'product'),
					'task'	=> array(
						1 => array('id' => 'ENQUIRY', 'name' =>'ENQUIRY'),
					),
				),
				7 	=> array(
					'id'	=> 'CMS',
					'name'	=>'CMS',
					'url'	=> array('mdl' => 'cms'),
					'task'	=> array(
						1 => array('id' => 'ENQUIRY', 'name' =>'ENQUIRY'),
					),
				),
			    8 	=> array(
					'id'	=> 'BANNER',
					'name'	=>'BANNERMANAGE',
					'url'	=> array('mdl' => 'banner'),
					'task'	=> array(
						1 => array('id' => 'ENQUIRY', 'name' =>'ENQUIRY'),
					),
				),
		/* 	    12	=> array(

					'id'	=> 'EXCEL',
					'name'	=>'EXCELFILE',
					'url'	=> array('mdl' => 'excel'),
					'task'	=> array(
						1 => array('id' => 'ENQUIRY', 'name' =>'ENQUIRY'),
					),
				),  */   
				13	=> array(

					'id'	=> '4DPROMO',
					'name'	=>'4DPROMO',
					'url'	=> array('mdl' => '4dpromo'),
					'task'	=> array(
						1 => array('id' => 'ENQUIRY', 'name' =>'ENQUIRY'),
					),
				),	
				14	=> array(

					'id'	=> '4DPROMOLOG',
					'name'	=>'4DPROMOLOG',
					'url'	=> array('mdl' => '4dpromolog'),
					'task'	=> array(
						1 => array('id' => 'ENQUIRY', 'name' =>'ENQUIRY'),
					),
				),	
				15	=> array(

					'id'	=> '4DPROMOBALANCE',
					'name'	=>'4DPROMOBALANCE',
					'url'	=> array('mdl' => '4dpromobalance'),
					'task'	=> array(
						1 => array('id' => 'ENQUIRY', 'name' =>'ENQUIRY'),
					),
				),
                17	=> array(

                    'id'	=> '4DPROMOCONFIG',
                    'name'	=>'4DPROMOCONFIG',
                    'url'	=> array('mdl' => '4dpromoconfig'),
                    'task'	=> array(
                        1 => array('id' => 'ENQUIRY', 'name' =>'ENQUIRY'),
                    ),
                ),
				16  => array(

                    'id'	=> 'LUCKYSPINCONFIGMYR',
                    'name'	=>'LUCKYSPINCONFIGMYR',
                    'url'	=> array('mdl' => 'luckyspinconfig?currency=MYR'),
                    'task'	=> array(
                        1 => array('id' => 'ENQUIRY', 'name' =>'ENQUIRY'),
                    ),
				),
				18  => array(

                    'id'	=> 'LUCKYSPINCONFIGIDR',
                    'name'	=>'LUCKYSPINCONFIGIDR',
                    'url'	=> array('mdl' => 'luckyspinconfig?currency=IDR'),
                    'task'	=> array(
                        1 => array('id' => 'ENQUIRY', 'name' =>'ENQUIRY'),
                    ),
				),
                19  => array(

                    'id'	=> 'MINIGAME',
                    'name'	=>'MINIGAME',
                    'url'	=> array('mdl' => 'minigame'),
                    'task'	=> array(
                        1 => array('id' => 'ENQUIRY', 'name' =>'ENQUIRY'),
                    ),
                ),
				
			),
		),
		6 => array(
			'id' 		=> 'SETTING',
			'name'		=>'SETTING',
			'module'	=> array(
			/* 	1 	=> array(
					'id'	=> 'PRODUCT',
					'name'	=>'PRODUCT',
					'url'	=> array('mdl' => 'product'),
					'task'	=> array(
						1 => array('id' => 'ENQUIRY', 'name' =>'ENQUIRY'),
						2 => array('id' => 'ADD', 'name' =>'ADDNEW'),
						3 => array('id' => 'EDIT', 'name' =>'EDIT'),
					),
				), */
				2 	=> array(
					'id'	=> 'WEBSITE',
					'name'	=>'WEBSITE',
					'url'	=> array('mdl' => 'website'),
					'task'	=> array(
						1 => array('id' => 'ENQUIRY', 'name' =>'ENQUIRY'),
						2 => array('id' => 'ADD', 'name' =>'ADDNEW'),
						3 => array('id' => 'EDIT', 'name' =>'EDIT'),
					),
				),
				3 	=> array(
					'id'	=> 'CURRENCY',
					'name'	=>'CURRENCY',
					'url'	=> array('mdl' => 'currency'),
					'task'	=> array(
						1 => array('id' => 'ENQUIRY', 'name' =>'ENQUIRY'),
						2 => array('id' => 'ADD', 'name' =>'ADDNEW'),
						3 => array('id' => 'EDIT', 'name' =>'EDIT'),
					),
				),
				4 	=> array(
					'id'	=> 'BANK',
					'name'	=>'BANK',
					'url'	=> array('mdl' => 'bank'),
					'task'	=> array(
						1 => array('id' => 'ENQUIRY', 'name' =>'ENQUIRY'),
						2 => array('id' => 'ADD', 'name' =>'ADDNEW'),
						3 => array('id' => 'EDIT', 'name' =>'EDIT'),
					),
				),
				5 	=> array(
					'id'	=> 'BANKACCOUNT',
					'name'	=>'MANAGEBANKACCOUNT',
					'url'	=> array('mdl' => 'bankaccount'),
					'task'	=> array(
						1 => array('id' => 'ENQUIRY', 'name' =>'ENQUIRY'),
						2 => array('id' => 'ADD', 'name' =>'ADDNEW'),
						3 => array('id' => 'EDIT', 'name' =>'EDIT'),
					),
				),
			/* 	6 	=> array(
					'id'	=> 'FUNDMETHOD',
					'name'	=>'FUNDINGMETHOD',
					'url'	=> array('mdl' => 'paymentmethod'),
					'task'	=> array(
						1 => array('id' => 'ENQUIRY', 'name' =>'ENQUIRY'),
						2 => array('id' => 'ADD', 'name' =>'ADDNEW'),
						3 => array('id' => 'EDIT', 'name' =>'EDIT'),
					),
				), */
				7 	=> array(
					'id'	=> 'PROMO',
					'name'	=>'BONUSSETTING',
					'url'	=> array('mdl' => 'promocampaign'),
					'task'	=> array(
						1 => array('id' => 'ENQUIRY', 'name' =>'ENQUIRY'),
						2 => array('id' => 'ADD', 'name' =>'ADDNEW'),
						3 => array('id' => 'EDIT', 'name' =>'EDIT'),
					),
				),
				8 	=> array(
					'id'	=> 'INCENTIVE',
					'name'	=>'INCENTIVESETTING',
					'url'	=> array('mdl' => 'incentive'),
					'task'	=> array(
						1 => array('id' => 'EDIT', 'name' =>'EDIT'),
					),
				),	
				9 	=> array(
					'id'	=> 'CASHBACK',
					'name'	=> 'CASHBACKSETTING',
					'url'	=> array('mdl' => 'cashback'),
					'task'	=> array(
						1 => array('id' => 'EDIT', 'name' =>'EDIT'),
					),
				),
			
				10 	=> array(
					'id'	=> 'TAG',
					'name'	=>'TAGMANAGE',
					'url'	=> array('mdl' => 'tag'),
					'task'	=> array(
						1 => array('id' => 'ENQUIRY', 'name' =>'ENQUIRY'),
						2 => array('id' => 'ADD', 'name' =>'ADDNEW'),
						3 => array('id' => 'EDIT', 'name' =>'EDIT'),
					),
				),

				11 	=> array(
					'id'	=> 'REMARK',
					'name'	=>'REMARKMANAGE',
					'url'	=> array('mdl' => 'remark'),
					'task'	=> array(
						1 => array('id' => 'ENQUIRY', 'name' =>'ENQUIRY'),
						2 => array('id' => 'ADD', 'name' =>'ADDNEW'),
						3 => array('id' => 'EDIT', 'name' =>'EDIT'),
					),
				),
				
				12 	=> array(
					'id'	=> 'REJECTREASON',
					'name'	=>'REJECTREASONMANAGE',
					'url'	=> array('mdl' => 'rejectreason'),
					'task'	=> array(
						1 => array('id' => 'ENQUIRY', 'name' =>'ENQUIRY'),
						2 => array('id' => 'ADD', 'name' =>'ADDNEW'),
						3 => array('id' => 'EDIT', 'name' =>'EDIT'),
					),
				),
				13 	=> array(
					'id'	=> 'CURRENCY',
					'name'	=>'CURRENCY',
					'url'	=> array('mdl' => 'currency'),
					'task'	=> array(
						1 => array('id' => 'ENQUIRY', 'name' =>'ENQUIRY'),
						2 => array('id' => 'ADD', 'name' =>'ADDNEW'),
						3 => array('id' => 'EDIT', 'name' =>'EDIT'),
					),
				),
				14 	=> array(
					'id'	=> 'PAYMENTGATEWAYSETTING',
					'name'	=>'AUTOPAYMENTGATEWAYSETTING',
					'url'	=> array('mdl' => 'paymentgatewaysetting'),
					'task'	=> array(
						1 => array('id' => 'ENQUIRY', 'name' =>'ENQUIRY'),
						2 => array('id' => 'ADD', 'name' =>'ADDNEW'),
						3 => array('id' => 'EDIT', 'name' =>'EDIT'),
					),
				),
				15 	=> array(
					'id'	=> 'PRIVATEPAYMENTGATEWAYSETTING',
					'name'	=>'MANUALPAYMENTGATEWAYSETTING',
					'url'	=> array('mdl' => 'privatepaymentgatewaysetting'),
					'task'	=> array(
						1 => array('id' => 'ENQUIRY', 'name' =>'ENQUIRY'),
						2 => array('id' => 'ADD', 'name' =>'ADDNEW'),
						3 => array('id' => 'EDIT', 'name' =>'EDIT'),
					),
				),
                16 	=> array(
                    'id'	=> 'REVERTBONUSSETTING',
                    'name'	=>'REVERTBONUSSETTING',
                    'url'	=> array('mdl' => 'revertbonus'),
                    'task'	=> array(
                        1 => array('id' => 'EDIT', 'name' =>'EDIT'),
                    ),
                ),
                17 	=> array(
                    'id'	=> 'WCPAYOUTREPORT',
                    'name'	=>'WCPAYOUTREPORT',
                    'url'	=> array('mdl' => 'worldCupPayoutReport'),
                    'task'	=> array(
                        1 => array('id' => 'DEPOSIT', 'name' =>'APPROVE'),
                        2 => array('id' => 'ADJUST', 'name' =>'APPROVE'),
                    ),
                ),

            ),
		),
		14 => array(
			'id' 		=> 'FINANCESETTING',
			'name'		=>'FINANCE',
			'module'	=> array(
				1 	=> array(
					'id'	=> 'RISKPROFILE',
					'name'	=>'RISKPROFILE',
					'url'	=> array('mdl' => 'payment'),
					'task'	=> array(
						1 => array('id' => 'ENQUIRY', 'name' =>'ENQUIRY'),
					),
				),
				2	=> array(
					'id'	=> 'ACCOUNT',
					'name'	=>'ACCOUNT',
					'url'	=> array('mdl' => 'payment'),
					'task'	=> array(
						1 => array('id' => 'ENQUIRY', 'name' =>'ENQUIRY'),
					),
				),
			),
		),
	
		9 => array(
			'id' 		=> 'ADMINUSER',
			'name'		=>'ADMIN',
			'module'	=> array(
				1 	=> array(
					'id'	=> 'USER',
					'name'	=>'USER',
					'url'	=> array('mdl' => 'user'),
					'task'	=> array(
						1 => array('id' => 'ENQUIRY', 'name' =>'ENQUIRY'),
						2 => array('id' => 'ADD', 'name' =>'ADDNEW'),
						3 => array('id' => 'EDIT', 'name' =>'EDIT'),
					),
				),
				2 	=> array(
					'id'	=> 'CHANGEPASS',
					'name'	=>'CHANGEPASS',
					'url'	=> array('mdl' => 'password'),
					'task'	=> array(
						1 => array('id' => 'EDIT', 'name' =>'EDIT'),
					),
				),

			),
		),
		10 => array(
			'id' 		=> 'SYSTEM',
			'name'		=>'SYSTEM',
			'module'	=> array(
				1 	=> array(
					'id'	=> 'CONFIG',
					'name'	=>'CONFIG',
					'url'	=> array('mdl' => 'config'),
					'task'	=> array(
						1 => array('id' => 'ENQUIRY', 'name' =>'ENQUIRY'),
						2 => array('id' => 'ADD', 'name' =>'ADDNEW'),
						3 => array('id' => 'EDIT', 'name' =>'EDIT'),
					),
				),
				2 	=> array(
					'id'	=> 'ROLE',
					'name'	=>'ROLE',
					'url'	=> array('mdl' => 'adminrole'),
					'task'	=> array(
						1 => array('id' => 'ENQUIRY', 'name' =>'ENQUIRY'),
						2 => array('id' => 'ADD', 'name' =>'ADDNEW'),
						3 => array('id' => 'EDIT', 'name' =>'EDIT'),
					),
				),
				3 	=> array(
					'id'	=> 'APILOG',
					'name'	=>'APILOG',
					'url'	=> array('mdl' => 'apilog'),
					'task'	=> array(
						1 => array('id' => 'ENQUIRY', 'name' =>'ENQUIRY'),
					),
				),
				4 	=> array(
					'id'	=> 'ERRORLOG',
					'name'	=>'ERRORLOG',
					'url'	=> array('mdl' => 'errorlog'),
					'task'	=> array(
						1 => array('id' => 'ENQUIRY', 'name' =>'ENQUIRY'),
					),
				),
			),
		),
	);




}

?>
