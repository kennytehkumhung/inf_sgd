<?php namespace App\libraries;

use App\Models\Onlinetracker;
use App\Models\Onlinetrackerlog;
use App\Models\Iptracker;
use Session;

class Login{

	 public function doMakeTracker($obj, $obj_type , $token = false, $ip = false, $prefix = '') {

		if($obj_type == 'Account') {
			$type = CBO_LOGINTYPE_USER;
			$username = $obj->nickname;
			$timezone = $obj->timezone;
			$language = $obj->lang;
		} else if($obj_type == 'Admin') {
			$type = CBO_LOGINTYPE_ADMIN;
			$username = $obj->username;
			$timezone = $obj->timezone;
			$language = $obj->lang;
		} else if($obj_type == 'Affiliate') {
			$type = CBO_LOGINTYPE_AFFILIATE;
			$username = $obj->username;
			$timezone = 8;
			$language = 'en';
		}
		else return false;
		
		$token = App::getSessionToken($obj->id, CBO_LOGINTYPE_USER);
		if(strlen($prefix) > 0) $token = $prefix.'_'.$token;

		$trackerObj = new Onlinetracker;
		$trackerObj->userid = $obj->id;
		$trackerObj->username = $username;
		if($type === CBO_LOGINTYPE_USER)
			$trackerObj->istest = $obj->istestacc;
		$trackerObj->type = $type;
		$trackerObj->sessionid = App::getSessionId();
		if($ip)
		$trackerObj->clientip = $ip;
		else 
		$trackerObj->clientip = App::getRemoteIp();
		$trackerObj->serverip = App::getServerIp();
		$trackerObj->hash = App::getSessionHash($obj->id);
		$trackerObj->token = $token;
		if($trackerObj->save()) {
			
			$ipObj = new Iptracker;
			$ipObj->userid 			= $trackerObj->userid;
			$ipObj->username 		= $trackerObj->username;
			$ipObj->type 			= CBO_IPTRACKERTYPE_ACCESS;
			$ipObj->createdpanel 	= $trackerObj->type;
			$ipObj->clientip 		= $trackerObj->clientip;
			$ipObj->serverip 		= $trackerObj->serverip;
			$ipObj->created 		= $trackerObj->created;
			$ipObj->save();
			
			$logObj = new Onlinetrackerlog;
			$logObj->userid = $trackerObj->userid;
			$logObj->username = $trackerObj->username;
			$logObj->type = $trackerObj->type;
			$logObj->sessionid = $trackerObj->sessionid;
			$logObj->clientip = $trackerObj->clientip;
			$logObj->longip = ip2long($trackerObj->clientip);
			$subIp = explode(".", $trackerObj->clientip);
			if(count($subIp) >=3)
			$logObj->longip3 = ip2long($subIp[0].'.'.$subIp[1].'.'.$subIp[2].'.255');
			$logObj->serverip = $trackerObj->serverip;
			$logObj->city = App::getCityNameByGeoIp($logObj->clientip);
			$logObj->env = serialize($_SERVER);
			$logObj->created = $trackerObj->created;
			$logObj->save();

			//$response = new Illuminate\Http\Response('Hello World');
			//$response->withCookie(cookie('hash', $trackerObj->hash, 60));

			Session::put('tracker_id', $trackerObj->id);
			Session::put('hash', $trackerObj->hash);
			Session::put('token', $trackerObj->token);
			Session::put('id', $obj->id);
			Session::put('username', $username);
			Session::put('timezone', $timezone);
			Session::put('type', $type);
			Session::put('timezone', $timezone);
			if($token) return $trackerObj->token;
			else return true;
		}
		return false;
	}
	
	

}