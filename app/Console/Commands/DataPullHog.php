<?php namespace App\Console\Commands;

use App\libraries\App;
use App\Models\Account;
use App\Models\Bethog;
use App\Models\Configs;
use App\Models\Currency;
use App\Models\Product;
use App\Models\Profitloss;
use App\Models\Wager;
use Cache;
use Carbon\Carbon;
use Config;
use DB;
use Illuminate\Console\Command;

class DataPullHog extends Command
{
    protected $signature = 'datapull:hog {currency} {--startdate= : Example: ytd|2017-01-31 00:00:00}';
    protected $description = 'Pull member bet ticket';

    // Product configs.
    protected $productCode = 'HOG';

    protected $accounts = array();
    protected $profitloss = array();

    public function handle()
    {
        // Check currency argument.
        $currency = strtoupper($this->argument('currency'));

        // Check product status.
        if (Product::where('status', '=', 2)->where('code', '=', $this->productCode)->count() > 0) {
            $this->error('Product is under maintenance.');
            return;
        }

        // Check start/end datetime.
        $lockname = strtolower($this->productCode) . '_datapull_process_'.$currency;
        $configPulldate = '';

        if ($this->option('startdate') == 'ytd') {
            // HOG unable to repull due to API call 3 minutes time interval restriction. Reset Last Pull Datetime and let robot run as usual.
            $yesterdayDt = Carbon::now()->addDays(-1)->setTime(0, 0, 0);
            Configs::updateParam( 'SYSTEM_HOG_PULLDATE_'.$currency , $yesterdayDt->toDateTimeString() );
            $this->comment('Set flag done, please check ticket after 1 hour: '.$this->productCode);

            return;
        } else {
            $startdate = $this->option('startdate');

            if (strlen($startdate) < 1) {
                // Backdate early 3 minutes in case some record is late insert from API service.
                $configPulldate = Configs::getParam('SYSTEM_HOG_PULLDATE_'.$currency);
            } else {
                // HOG unable to manual repull due to API call 3 minutes time interval restriction. Reset Last Pull Datetime and let robot run as usual.
                Configs::updateParam( 'SYSTEM_HOG_PULLDATE_'.$currency , Carbon::parse($startdate)->toDateTimeString() );
                $this->comment('Set flag done, please check ticket after 1 hour: '.$this->productCode);

                return;
            }
        }

        App::check_process($lockname);

        // Begin.
        $startTime = Carbon::parse($configPulldate)->addHours(-8)->addMinute(-3);
        $endTime = Carbon::now()->addHours(-8);
        $updatePullDateParam = false;

        // API only allow 90 seconds time interval on each calls.
        if( $endTime->diffInMinutes($startTime) > 29 ){
            // End datetime is over 30 mins, use start time +30 mins instead.
            $endTime = $startTime->copy()->addMinutes(30);
        }

        $header[] = 'DataType: XML';
        $header[] = 'Content-Type: text/xml';

        $postfields  = '<?xml version="1.0" encoding="UTF-8"?>';
        $postfields .= '<GetAllBetDetailsPerTimeInterval>';
        $postfields .= '<Username>'.Config::get($currency.'.hog.api_username').'</Username>';
        $postfields .= '<Password>'.Config::get($currency.'.hog.api_password').'</Password>';
        $postfields .= '<CasinoId>'.Config::get($currency.'.hog.casino_id').'</CasinoId>';
//		$postfields .= '<UserId></UserId>';
        $postfields .= '<StartTime>'.$startTime->format('Y/m/d H:i:s').'</StartTime>';
        $postfields .= '<EndTime>'.$endTime->format('Y/m/d H:i:s').'</EndTime>';
        $postfields .= '<UserType>Play</UserType>';
//		$postfields .= '<PageSize></PageSize>';
//		$postfields .= '<PageNumber></PageNumber>';
//		$postfields .= '<Status></Status>';
        $postfields .= '</GetAllBetDetailsPerTimeInterval>';

        $result = simplexml_load_string( trim(App::curlXML( $postfields , Config::get($currency.'.hog.bet_log_api_url').'GetAllBetDetailsPerTimeInterval', true, 30 , 'datapull:'.$this->productCode, $header)) );
        $result = simplexml_load_string('<Response>'.trim($result[0]).'</Response>');
        $result = simplexml_load_string($result->asXML());

        if( isset($result->BetInfos->Betinfo) ){
            $data = array();

            for ($i = 0; $i < count($result->BetInfos->Betinfo); $i++) {
                $bi = $result->BetInfos->Betinfo[$i];

                $dateTimeNow = Carbon::now();
                $dateTimeNowStr = $dateTimeNow->toDateTimeString();

                do {
                    // Generate ref ID.
                    $refId = $dateTimeNow->timestamp.mt_rand(1111, 9999);
                    $count = Bethog::where('refId', '=', $refId)->count();

                    if ($count > 0) {
                        // Ref ID exists.
                        continue;
                    }
                } while (false);

                $accIdTemp = explode('_', $bi->AccountId);

                $BetAmount   	  =	($bi->Currency == 'IDR')? $bi->BetAmount /1000:$bi->BetAmount;
                $Payout    		  = ($bi->Currency == 'IDR')? $bi->Payout    /1000:$bi->Payout;

                $data = array(
                    'refId'             => $refId,
                    'betStartDate'		=> Carbon::parse((string) $bi->BetStartDate)->addHours(8)->toDateTimeString(),
                    'betEndDate'		=> Carbon::parse((string) $bi->BetEndDate)->addHours(8)->toDateTimeString(),
                    'accountId'			=> $accIdTemp[1],
                    'tableId'			=> (string) $bi->TableId,
                    'gameId'			=> (string) $bi->GameId,
                    'betId'             => (string) $bi->BetId,
                    'betAmount'			=> (float) $BetAmount,
                    'payout'			=> (float) $Payout,
                    'currency'			=> (string) $bi->Currency,
                    'gameType'			=> (string) $bi->GameType,
                    'betSpot'			=> (string) $bi->BetSpot,
                    'betNo'             => (string) $bi->BetNo, // Unique index.
                    'created_at'		=> $dateTimeNowStr,
                    'updated_at'		=> $dateTimeNowStr,
                );

                foreach ($data as $key => $value) {
                    $insertData[$key] = '"'.$value.'"';
                }

                if (DB::statement('call insert_hog('.implode(',',$insertData).')')) {
                    $betObj = Bethog::where('betNo', '=', $data['betNo'])->first(['refId']);

                    if ($betObj) {
                        // Get ref id of inserted record.
                        $data['refId'] = $betObj->refId;
                    }

                    $this->InsertWager($data,$data['currency']);
                }
            }

            $updatePullDateParam = true;

            Profitloss::updatePNL2($this->profitloss);
        } elseif ( isset($result->PlayInfos->STATUS_CODE) && $result->PlayInfos->STATUS_CODE == '0002' ) {
            // Code 0002: No record Found
            $updatePullDateParam = true;
        }

//        if ($updatePullDateParam) {
        if ($updatePullDateParam && Configs::getParam('SYSTEM_HOG_PULLDATE_'.$currency) == $configPulldate) {
            Configs::updateParam( 'SYSTEM_HOG_PULLDATE_'.$currency , $endTime->addHours(8)->toDateTimeString() );
        }

        // End.
        App::unlock_process($lockname);

        $this->comment('DataPull done: ' . $this->productCode);
    }

    private function InsertWager( $bet , $currency){

        if( !isset($this->accounts[$bet['accountId']]) )
            $this->accounts[$bet['accountId']] = Account::where( 'nickname' , '=' , $bet['accountId'] )->whereCrccode($currency)->first();

        $accountObj = $this->accounts[$bet['accountId']];

        $prdObj = Cache::rememberForever('product_hog_obj', function(){
            return Product::where( 'code' , '=' , 'HOG' )->first();
        });

        $data['accid'] 		 		= $accountObj->id;
        $data['acccode'] 	 		= $accountObj->code;
        $data['nickname'] 	 		= $accountObj->nickname;
        $data['wbsid'] 		 		= $accountObj->wbsid;
        $data['prdid'] 		 		= $prdObj->id;
        $data['matchid'] 	 		= $bet['tableId'] == '' ? "''":$bet['tableId'];
        $data['gamename'] 	 		= $bet['gameType'];
        $data['category'] 	 		= Product::getCategory('Casino');
        $data['refid'] 		 		= $bet['refId'];
        $data['crccode'] 	 		= $accountObj->crccode;
        $data['crcrate'] 	 		= Currency::getCurrencyRate($data['crccode']);
        $data['datetime'] 	 		= $bet['betEndDate'];
        $data['ip'] 		 		= '';
        $data['payout'] 	 		= ((float) $bet['payout']) + ((float) $bet['betAmount']);
        $data['profitloss']			= (float) $bet['payout'];
        $data['accountdate']		= Carbon::parse($data['datetime'])->addHours(-8)->toDateString();
        $data['stake']		 		= $bet['betAmount'];
        $data['status'] 			= Wager::STATUS_SETTLED;


        if( $data['stake'] == $data['payout'] )	    $data['result']  = Wager::RESULT_DRAW;
        else if( $data['stake'] > $data['payout'] ) $data['result']  = Wager::RESULT_LOSS;
        else if( $data['stake'] < $data['payout'] ) $data['result']  = Wager::RESULT_WIN;
        $data['validstake'] 		= $bet['betAmount'];

        if ($data['result'] == Wager::RESULT_DRAW) {
            $data['validstake'] = 0;
        }

        $data['stakelocal'] 		= Currency::getLocalAmount($data['stake'], $data['crccode']);
        $data['payoutlocal']		= Currency::getLocalAmount($data['payout'], $data['crccode']);
        $data['profitlosslocal']	= Currency::getLocalAmount($data['profitloss'], $data['crccode']);
        $data['validstakelocal']	= Currency::getLocalAmount($data['validstake'], $data['crccode']);
        $data['iscalculated'] 	    = 0;
        $data['created']			= date("y-m-d H:i:s");
        $data['modified']			= date("y-m-d H:i:s");

        foreach( $data as $key => $value ){
            $insert_data[$key] = '"'. $value . '"';
        }


        if(DB::statement('call insert_wager('.implode(',',$insert_data).')'))
        {
            $this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['prdid']   = $data['prdid'];
            $this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['accobj']  = $accountObj;
            $this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['crcrate'] = $data['crcrate'];
        }

    }
}
