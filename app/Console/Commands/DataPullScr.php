<?php namespace App\Console\Commands;

use App\libraries\App;
use App\Models\Account;
use App\Models\Currency;
use App\Models\Configs;
use App\Models\Product;
use App\Models\Profitloss;
use App\Models\Wager;
use Cache;
use Carbon\Carbon;
use Config;
use DB;
use Illuminate\Console\Command;

class DataPullScr extends Command
{
    protected $signature = 'datapull:scr {currency} {--startdate=} {--enddate=}';
    protected $description = 'Pull member bet ticket';

    // Product configs.
    protected $productCode = 'SCR';

    protected $accounts = array();
    protected $profitloss = array();

    public function handle()
    {
        // Check currency argument.
        $currency = strtoupper($this->argument('currency'));

        // Check product status.
        if (Product::where('status', '=', 2)->where('code', '=', $this->productCode)->count() > 0) {
            $this->error('Product is under maintenance.');
            return;
        }

        // Check start/end datetime.
        $lockname = strtolower($this->productCode) . '_datapull_process_' . $currency;
        $dt = Carbon::now();
        $updatePullDateParam = false;

        if ($this->option('startdate') == 'ytd') {
            $lockname .= 'ytd';
            $yesterdayDt = Carbon::now()->addDays(-1);
            $startDt = $yesterdayDt->setTime(0, 0, 0)->toDateTimeString();
            $endDt = $yesterdayDt->setTime(23, 59, 59)->toDateTimeString();
        } else {
            $startDt = $this->option('startdate');
            $endDt = $this->option('enddate');

            if (strlen($startDt) < 1) {
                $startDt = Configs::getParam('SYSTEM_SCR_PULLDATE_' . $currency);
                $updatePullDateParam = true;

                if (strlen($startDt) < 1) {
                    $startDt = '2017-09-01 12:00:00';
                }

            }
        }

        // Begin.
        App::check_process($lockname);
        //
        if (is_null($endDt)) {
            $endDt = $dt->copy()->toDateTimeString();
        }

        $partnerId = Config::get($currency . '.scr.ApiUserID');
        $apiUrl = Config::get($currency . '.scr.BaseServiceURL');
        $apiPass = Config::get($currency . '.scr.ApiPassword');
        $header[] = 'Content-Type:application/json';
        $continue = false;
//        $yesterdayDt = Carbon::now();
//        $startDt = $yesterdayDt->setTime(0, 0, 0)->toDateTimeString();
//        $endDt = $yesterdayDt->setTime(23, 59, 59)->toDateTimeString();

        $postfields = array(
            'apiuserid' => $partnerId,
            'apipassword' => $apiPass,
            'operation' => 'totalreport',
            'startdate' => $startDt,
            'enddate' => $endDt,
        );

        $json = json_encode($postfields);

        $result = json_decode($this->curl(Config::get($currency . '.scr.BaseServiceURL') . 'reports', $json, true, 3));

        if (isset($result->returncode) && ($result->returncode == 0) && isset($result->player_bets)) {
            $data = array();

            foreach ($result->player_bets as $bi) {
                if ( isset($bi->win) && ((float)$bi->win > 0.00) ) {
                    $dateTimeNow = Carbon::now();
                    $dateNow = Carbon::now()->toDateString();
                    $dateTimeNowStr = $dateTimeNow->toDateTimeString();
                    $data = array(
                        'playerid' => $bi->playerid,
                        'win' => (float)$bi->win,
                        'game_time' => $dateNow,
                        'created_at' => $dateTimeNowStr,
                        'updated_at' => $dateTimeNowStr,
                    );

                    foreach ($data as $key => $value) {
                        $insertData[$key] = '"' . $value . '"';
                    }


                    if (DB::statement('call insert_scr(' . implode(',', $insertData) . ')')) {
                        // Get new inserted ID.
//                        $this->InsertWager($data, $currency); // SCR not including in turnover.
                    }
                } else {

                }
            }
        }

//        Profitloss::updatePNL2($this->profitloss); // SCR not including in turnover.

        // Reset array to prevent memory overflow.
        $this->accounts = array();
        $this->profitloss = array();


        if ($updatePullDateParam) {
            Configs::updateParam('SYSTEM_SCR_PULLDATE_' . $currency, $endDt);
        }

        // End.
        App::unlock_process($lockname);
        $this->comment('DataPull done: ' . $this->productCode);
    }

    private function InsertWager( $scr, $currency )
    {
		$dateNow = Carbon::now()->toDateString();
        if( !isset($this->accounts[$scr['playerid']]) )
            $this->accounts[$scr['playerid']] = Account::where( 'scrid' , '=' , $scr['playerid'] )->whereCrccode($currency)->first();
		
        $prdObj = Cache::rememberForever('product_scr_obj', function(){
            return Product::where( 'code' , '=' , 'SCR' )->first();
        });
        $account = $this->accounts[$scr['playerid']];

        $data['accid'] 		 		= $account->id;
        $data['acccode'] 	 		= $account->code;
        $data['nickname'] 	 		= $account->nickname;
        $data['wbsid'] 		 		= $account->wbsid;
        $data['prdid'] 		 		= $prdObj->id;
        $data['matchid'] 	 		= 0;
        $data['gamename'] 	 		= 'slotgame';
        $data['category'] 	 		= Product::getCategory('egames');
        $data['refid'] 		 		= $scr['playerid'];
        $data['crccode'] 	 		= $account->crccode;
        $data['crcrate'] 	 		= Currency::getCurrencyRate($data['crccode']);
        $data['datetime'] 	 		= $dateNow;
        $data['ip'] 		 		= '';
        $data['payout'] 	 		= ((float) $scr['win']);
        $data['profitloss']  		= $scr['win'];
        $data['accountdate'] 		= $dateNow;
        $data['stake']		 		= 0;
        $data['status'] 			= Wager::STATUS_SETTLED;

        if( $data['stake'] == $data['payout'] )	    $data['result']  = Wager::RESULT_DRAW;
        else if( $data['stake'] > $data['payout'] ) $data['result']  = Wager::RESULT_LOSS;
        else if( $data['stake'] < $data['payout'] ) $data['result']  = Wager::RESULT_WIN;
        $data['validstake'] 		= 0;
        $data['stakelocal'] 		= Currency::getLocalAmount($data['stake'], $data['crccode']);
        $data['payoutlocal']		= Currency::getLocalAmount($data['payout'], $data['crccode']);
        $data['profitlosslocal']	= Currency::getLocalAmount($data['profitloss'], $data['crccode']);
        $data['validstakelocal']	= Currency::getLocalAmount($data['validstake'], $data['crccode']);
        $data['iscalculated'] 	    = 0;
        $data['created']			= date("y-m-d H:i:s");
        $data['modified']			= date("y-m-d H:i:s");

        $insert_data = array();
	
        foreach( $data as $key => $value ){
            $insert_data[$key] = '"'. $value . '"';
        }

        if(DB::statement('call insert_wager('.implode(',',$insert_data).')'))
        {
            $this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['prdid']   = $data['prdid'];
            $this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['accobj']  = $this->accounts[$scr['playerid']];
            $this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['crcrate'] = $data['crcrate'];
        }

    }
	function curl($url , $postFields , $log = true, $timeout = 30 , $method = ''){
		$headers[] = "Content-Type:application/json";
		$data_json = $postFields;
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json','Content-Length: ' . strlen($data_json)));
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($ch, CURLOPT_POSTFIELDS,$data_json);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response  = curl_exec($ch);
		curl_close($ch);
        
        if($log)
            App::insert_api_log( array( 'request' => $url.'?'.$data_json , 'return' => $response , 'method' => $method ) );

        return $response;
    }
}
