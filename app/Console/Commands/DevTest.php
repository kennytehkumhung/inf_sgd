<?php

namespace App\Console\Commands;

use App\libraries\App;
use App\Models\Adjustment;
use App\Models\Account;
use App\Models\Accountdetail;
use App\Models\Agent;
use App\Models\Bethog;
use App\Models\Betosg;
use App\Models\Cashledger;
use App\Models\Channel;
use App\Models\Configs;
use App\Models\Currency;
use App\Models\Product;
use App\Models\Profitloss;
use App\Models\Wager;
use App\Models\Website;
use Cache;
use Carbon\Carbon;
use Config;
use Crypt;
use GeoIP;
use DB;
use Exception;
use Hash;
use Illuminate\Console\Command;
use Maatwebsite\Excel\Facades\Excel;

class DevTest extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dev:test';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Development command';

	protected $accounts = array();
	
	protected $profitloss = array();

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
		
		if( $accountdetails = Accountdetail::get() )
		{
			foreach( $accountdetails as $accountdetail ){
				
				$location = GeoIP::getLocation($accountdetail->createdip);
				
				
				if( $location['country'] == 'China' ){
					$location = App::taobao_ip($accountdetail->createdip);
					
					if(isset($location->data->city))
					{
						 $this->comment($accountdetail->createdip);
						$accountdetail->rescity  = $location->data->city;
						$accountdetail->resstate = $location->data->region;
						$accountdetail->save();
						
					} 
				}
				
				
			}
			
		}
		
        $this->comment('Done.');
    }
}
