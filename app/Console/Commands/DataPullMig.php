<?php namespace App\Console\Commands;

use App\libraries\App;
use App\Models\Account;
use App\Models\Currency;
use App\Models\Configs;
use App\Models\Product;
use App\Models\Profitloss;
use App\Models\Wager;
use App\Models\Betmig;
use Cache;
use Carbon\Carbon;
use Config;
use DB;
use Illuminate\Console\Command;
use SoapClient;
use SoapVar;
use SoapHeader;

class DataPullMig extends Command
{
    protected $signature = 'datapull:mig {currency} {--startdate=} {--enddate=}';
    protected $description = 'Pull member bet ticket';

    // Product configs.
    protected $productCode = 'MIG';

    protected $accounts = array();
    protected $profitloss = array();

    public function handle()
    {
        // Check currency argument.
        $currency = strtoupper($this->argument('currency'));

        // Check product status.
        if (Product::where('status', '=', 2)->where('code', '=', $this->productCode)->count() > 0) {
            $this->error('Product is under maintenance.');
            return;
        }

        // Check start/end datetime.
        $lockname = strtolower($this->productCode) . '_datapull_process_' . $currency;
        $dt = Carbon::now();
        $updatePullDateParam = false;

        if ($this->option('startdate') == 'ytd') {
            $lockname .= 'ytd';
            $yesterdayDt = Carbon::now()->addDays(-1);
            $startDt = $yesterdayDt->setTime(0, 0, 0)->toDateTimeString();
            $endDt = $yesterdayDt->setTime(23, 59, 59)->toDateTimeString();
        } else {
            $startDt = $this->option('startdate');
            $endDt = $this->option('enddate');

            if (strlen($startDt) < 1) {
                 $startDt = Carbon::now()->addDays(-1)->toDateString();
				 $endDt = Carbon::now()->toDateString();
            }
        }

        // Begin.
        App::check_process($lockname);			
		
      /*   if (is_null($endDt)) {
            $endDt = $dt->copy()->toDateTimeString();
        } */

		$continue = true;
		$page = 1;
		
		
	while($continue){
		$url = "https://entv3-04.totalegame.net?WSDL";
		$client = new SoapClient($url, array('compression' => SOAP_COMPRESSION_ACCEPT | SOAP_COMPRESSION_GZIP));

		$result = $client->IsAuthenticate(
            array(
                'loginName' => Config::get($currency.'.mig.UsernameSK'), 
                'pinCode' => Config::get($currency.'.mig.PasswordSK')
            )
        );
		
		if(isset($result) && ($result->IsAuthenticateResult->ErrorCode == 0)){
			$SessionGUID = $result->IsAuthenticateResult->SessionGUID;
			$IPAddress = $result->IsAuthenticateResult->IPAddress;
		}
		
		$headerxml =  '<AgentSession xmlns="https://entservices.totalegame.net">'.
					  '<SessionGUID>' . $SessionGUID . '</SessionGUID>'.
					  '<IPAddress>' . $IPAddress . '</IPAddress>'.
					  '</AgentSession>'
        ;
		$xmlvar = new SoapVar($headerxml, XSD_ANYXML);
		$header = new SoapHeader('https://entservices.totalegame.net', 'AgentSession', $xmlvar);
		$client->__setSoapHeaders($header);
		
		$result2 = $client->GetBetInfoDetails(
          array(
                'dateFrom' => $startDt, 
                'dateTo' => $endDt,
                'maxNumRowsPage' => 1000,
            )			
        );
		
		$result4 = $client->GetReportResult(
			array(
				  'id' => $result2->GetBetInfoDetailsResult,
				  'nPage' => $page,
			)
		);
		
		if( isset($result4->GetReportResultResult->CurrentPageData->any) )
		{
			$xmlresult = $result4->GetReportResultResult->CurrentPageData->any;
			if(isset($result4->GetReportResultResult->CurrentPageData->any)){
				$xml = simplexml_load_string($xmlresult);
			}
			
                if( isset($result4->GetReportResultResult) && ($result4->GetReportResultResult->ErrorCode == 0)) {
					
                    $data = array();
		
					if (is_array($xml->NewDataSet->Table) || is_object($xml->NewDataSet->Table))
					{
						foreach ($xml->NewDataSet->Table as $bi) {
								$dateTimeNow = Carbon::now();
								$dateTimeNowStr = $dateTimeNow->toDateTimeString();
								$data = array(
									'AccountNumber' => (String) $bi->AccountNumber,
									'Income' => (float) $bi->Income,
									'Payout' =>  (float)$bi->Payout,
									'WinAmount' => (float)$bi->WinAmount,
									'LoseAmount' => (float)$bi->LoseAmount,
									'Date' => $bi->Date,
									'GameType' => (String)$bi->GameType,
									'ModuleId' => (int)$bi->ModuleId,
									'ClientId' => (int)$bi->ClientId,
									'created_at' => $dateTimeNowStr,
									'updated_at' => $dateTimeNowStr,
								);
								
								foreach ($data as $key => $value) {
									$insertData[$key] = '"' . $value . '"';
								}
								
									if (DB::statement('call insert_mig(' . implode(',' , $insertData) . ')')) {
										// Get new inserted ID.
										$this->InsertWager($data,$currency);
									}
							
						}
				
						if(isset($result4->GetReportResultResult->Paging->PageNumber)){
							if($result4->GetReportResultResult->Paging->PageNumber == $result4->GetReportResultResult->Paging->TotalPage){
								$continue = false;
							}else{
								$page = $result4->GetReportResultResult->Paging->PageNumber + 1;
							}
						}else{
							$continue = false;
						}
					}
					
                }

                Profitloss::updatePNL2($this->profitloss);

                // Reset array to prevent memory overflow.
                $this->accounts = array();
                $this->profitloss = array();
								
		}
	}
          
        if ($updatePullDateParam) {
            Configs::updateParam('SYSTEM_MIG_PULLDATE_' . $currency, $endDt);
        }

        // End.
        App::unlock_process($lockname);
		$this->comment('DataPull done: ' . $this->productCode);
    }

    private function InsertWager( $mig, $currency )
    {
        if( !isset($this->accounts[$mig['AccountNumber']]) )
            $this->accounts[$mig['AccountNumber']] = Account::where( 'migid' , '=' , $mig['AccountNumber'] )->whereCrccode($currency)->first();
		
        $prdObj = Cache::rememberForever('product_mig_obj', function(){
            return Product::where( 'code' , '=' , 'MIG' )->first();
        });
		
		if( isset($this->accounts[$mig['AccountNumber']]) )
		{
		
			$account = $this->accounts[$mig['AccountNumber']];

			$data['accid'] 		 		= $account->id;
			$data['acccode'] 	 		= $account->code;
			$data['nickname'] 	 		= $account->nickname;
			$data['wbsid'] 		 		= $account->wbsid;
			$data['prdid'] 		 		= $prdObj->id;
			$data['matchid'] 	 		= 0;
			$data['gamename'] 	 		= $mig['GameType'];
			$data['category'] 	 		= Product::getCategory('egames');
			$data['refid'] 		 		= Betmig::whereRaw(' ClientId = '.$mig['ClientId'].' AND Date = "'.$mig['Date'].'" AND GameType = "'.$mig['GameType'].'" AND AccountNumber = "'.$mig['AccountNumber'].'" ')->pluck('id');
			$data['crccode'] 	 		= $account->crccode;
			$data['crcrate'] 	 		= Currency::getCurrencyRate($data['crccode']);
			$data['datetime'] 	 		= $mig['Date'];
			$data['ip'] 		 		= '';
			$data['payout'] 	 		= ((float) $mig['Payout']);
			$data['profitloss']  		= ((float)$mig['Payout']) - ((float)$mig['Income']);
			$data['accountdate'] 		= substr($mig['Date'],0,10);
			$data['stake']		 		= ((float) $mig['Income']);
			$data['status'] 			= Wager::STATUS_SETTLED;

			if( $data['stake'] == $data['payout'] )	    $data['result']  = Wager::RESULT_DRAW;
			else if( $data['stake'] > $data['payout'] ) $data['result']  = Wager::RESULT_LOSS;
			else if( $data['stake'] < $data['payout'] ) $data['result']  = Wager::RESULT_WIN;
			$data['validstake'] 		= $mig['Income'];
			$data['stakelocal'] 		= Currency::getLocalAmount($data['stake'], $data['crccode']);
			$data['payoutlocal']		= Currency::getLocalAmount($data['payout'], $data['crccode']);
			$data['profitlosslocal']	= Currency::getLocalAmount($data['profitloss'], $data['crccode']);
			$data['validstakelocal']	= Currency::getLocalAmount($data['validstake'], $data['crccode']);
			$data['iscalculated'] 	    = 0;
			$data['created']			= date("y-m-d H:i:s");
			$data['modified']			= date("y-m-d H:i:s");

			$insert_data = array();

			foreach( $data as $key => $value ){
				$insert_data[$key] = '"'. $value . '"';
			}

			if(DB::statement('call insert_wager('.implode(',',$insert_data).')'))
			{
				$this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['prdid']   = $data['prdid'];
				$this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['accobj']  = $this->accounts[$mig['AccountNumber']];
				$this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['crcrate'] = $data['crcrate'];
			}
		
		}

    }
	
}
