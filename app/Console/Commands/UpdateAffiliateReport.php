<?php

namespace App\Console\Commands;

use App\Http\Controllers\Controller\Api;
use App\Models\Account;
use App\Models\Affiliate;
use App\Models\Affiliatereport;
use App\Models\Affiliatesetting;
use App\Models\Cashledger;
use App\Models\Configs;
use App\Models\Incentivereferral;
use App\Models\Ledgersetting;
use App\Models\Product;
use App\Models\Wager;
use App\Models\Wagersetting;
use App\Models\Profitloss;
use App\Models\Currency;
use App\libraries\Array2XML;
use App\libraries\App;
use Illuminate\Http\Request;
use App\libraries\Login;
use Config;
use DB;
use Cache;
use Log;
use Auth;
use Carbon\Carbon;
use Illuminate\Console\Command;

class UpdateAffiliateReport extends Command {
	
    protected $signature = 'dev:affreport';

    protected $description = 'Caculate Affiliate report';

	public function handle(Request $request) 
	{
		
		//App::check_process('affiliate_report_calculate_process');
		
		$now = Carbon::now();
		$aflObjr = new Affiliate();

		$continue = true;
		
		$this->comment('Update Affreport Start');
		
		
			
		if( $affiliates = Affiliate::select(['id'])->whereType(1)->get() )
		{
			if(count($affiliates))
			{
				while($continue)
				{
					
					foreach($affiliates as $affiliate)
					{
						$afflids[] = $affiliate->id;
					}
				
					if ( Config::get('setting.opcode') == 'GSC' ){
						$wgrObj = Wager::where('status', '=', Wager::STATUS_SETTLED)
							->where('iscalculated', '=', 0)
							->where('status', '!=', Wager::STATUS_REJECTED)
							->whereRaw(' accid IN (select id from account where aflid IN ('.implode(',',$afflids).') ) ')
							->take(1200)
							->get();
					}else{
						$wgrObj = Wager::where('status', '=', Wager::STATUS_SETTLED)
							->where('category', '!=', Product::CATEGORY_LOTTERY)
							->where('iscalculated', '=', 0)
							->where('status', '!=', Wager::STATUS_REJECTED)
							->whereRaw(' accid IN (select id from account where aflid IN ('.implode(',',$afflids).') ) ')
							->take(1200)
							->get();
					}	
					
					
					if (count($wgrObj)) 
					{
						foreach ($wgrObj as $wager) 
						{
							$accObj = Account::find($wager->accid);

							if ($accObj && $accObj->aflid > 0) 
							{
								$agents = $aflObjr->getAllUplineById($accObj->aflid);

								if ($agents) 
								{
									foreach ($agents as $level => $aflid) 
									{

										$aflObj = Affiliate::where('id', '=', $aflid)
												->where('type', '=', 1)
												->first();
										
										if ($aflObj) {
											$rate = 0;
											$afsObj = Affiliatesetting::where('aflid', '=', $aflid)
													->where('created', '<=', $wager->datetime)
													->orderBy('id', 'desc')
													->first();
													
											$mutiply = 1;
											
											if( $wager->crccode == 'IDR' || $wager->crccode == 'VND' )
											{
												$mutiply = 1000;
											}
											
											$netprofitloss = $wager->profitloss;
									
											if( Config::get('setting.front_path') == 'front' )
											{
												$netprofitloss = $wager->profitloss * 0.85;
											}

											if (count($afsObj)) {
												$rate = $afsObj->commission / 100;
												$attrs = [
													'prdid' => $wager->prdid,
													'category' => $wager->category,
													'wgrid' => $wager->id,
													'aflid' => $accObj->aflid,
													'share1id' => $aflid,
													'share2id' => $aflObj->parentid,
													'afsid' => $afsObj->id,
													'accid' => $wager->accid,
													'crccode' => $wager->crccode,
													'crcrate' => $wager->crcrate,
													'stake' => $wager->stake * $mutiply,
													'validstake' => $wager->validstake * $mutiply,
													'profitloss' =>  $wager->profitloss * $mutiply,
													'netprofitloss' => $netprofitloss * $mutiply,
													'rate' => $afsObj->commission,
													'share1' => ($netprofitloss * $rate) * $mutiply,
													'share2' => ($netprofitloss * (1 - $rate) * -1) * $mutiply,
													'date' => $wager->accountdate,
												];

												$wgsObj = Wagersetting::where('wgrid', '=', $wager->id)
														->where('share1id', '=', $aflid)
														->first();

												if ($wgsObj) {
													$wgsObj->guard([])->update($attrs);
													$this->comment('Insert ws Done: ' .  $wager->accid);
												} else {
													$wgsObj = new Wagersetting();
													$wgsObj->guard([])->fill($attrs)->save();
													$this->comment('Update ws Done: ' .  $wager->accid);
												}
											}
										}
									}
								}

								
							}
							$wager->iscalculated = 1;
							$wager->save();
						}
					}else{
						$continue = false;
					}
					
				}
			}
		}
		
		$this->comment('Update Affreport End');
		
		
		$this->comment('Update Lggreport Start');
		
		$continue = true;
		
		while($continue)
		{
			
			if($ledgers = Cashledger::where('iscalculated', '=', 0)
					->whereIn('status', [CBO_LEDGERSTATUS_CONFIRMED, CBO_LEDGERSTATUS_MANCONFIRMED])
					->whereIn('chtcode', [CBO_CHARTCODE_BONUS, CBO_CHARTCODE_INCENTIVE])
					->whereRaw(' accid IN (select id from account where aflid != 0) ')
					->take(300)
					->get())
			{
			
				if(count($ledgers) == 0){
					$continue = false;
				}
				
				foreach ($ledgers as $ledger) {
					
					$accObj = Account::find($ledger->accid);
					
					if ($accObj) {
						$agents = $aflObjr->getAllUplineById($accObj->aflid);
						
						if ($agents) {
							foreach ($agents as $level => $aflid) {

								$aflObj = Affiliate::where('id', '=', $aflid)
										->where('type', '=', 1)
										->first();

								if ($aflObj) {
									$rate = 0;
									$afsObj = Affiliatesetting::where('aflid', '=', $aflid)
											->where('created', '<=', $ledger->created)
											->orderBy('id', 'desc')
											->first();

									if (count($afsObj)) {
										$rate = $afsObj->commission / 100;
										$attrs = [
											'clgid' => $ledger->id,
											'aflid' => $accObj->aflid,
											'share1id' => $aflid,
											'share2id' => $aflObj->parentid,
											'afsid' => $afsObj->id,
											'accid' => $ledger->accid,
											'crccode' => $ledger->crccode,
											'crcrate' => $ledger->crcrate,
											'amount' => $ledger->amount,
											'rate' => $afsObj->commission,
											'share1' => $ledger->amount * $rate * -1,
											'share2' => $ledger->amount * (1 - $rate) * -1,
											'date' => Carbon::parse($ledger->created)->toDateString(),
										];

										$lgsObj = Ledgersetting::where('clgid', '=', $ledger->id)
												->where('share1id', '=', $aflid)
												->first();

										if ($lgsObj) {
											$lgsObj->guard([])->update($attrs);
											$this->comment('Insert ls Done: ' .  $wager->accid);
										} else {
											$lgsObj = new Ledgersetting();
											$lgsObj->guard([])->fill($attrs)->save();
											$this->comment('Update ls Done: ' .  $wager->accid);
										}
									}
								}
							}
						}
						
						$ledger->iscalculated = 1;
						$ledger->save();
					}
				}
			
			}
		}
		
		$this->comment('Update Lggreport End');
		
		//App::unlock_process('affiliate_report_calculate_process');
		
		
	}
	
	

}
