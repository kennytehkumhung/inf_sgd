<?php namespace App\Console\Commands;

use App\libraries\App;
use App\Models\Account;
use App\Models\Bets128;
use App\Models\Configs;
use App\Models\Currency;
use App\Models\Product;
use App\Models\Profitloss;
use App\Models\Wager;
use Cache;
use Carbon\Carbon;
use Config;
use DB;
use Illuminate\Console\Command;

class DataPullS128 extends Command
{
    protected $signature = 'datapull:s128 {currency} {--startdate= : Example: ytd|2017-01-31 00:00:00}';
    protected $description = 'Pull member bet ticket';

    // Product configs.
    protected $productCode = 'S128';

    protected $accounts = array();
    protected $profitloss = array();

    public function handle()
    {
        // Check currency argument.
        $currency = strtoupper($this->argument('currency'));

        // Check product status.
        if (Product::where('status', '=', 2)->where('code', '=', $this->productCode)->count() > 0) {
            $this->error('Product is under maintenance.');
            return;
        }

        // Check start/end datetime.
        $lockname = strtolower($this->productCode) . '_datapull_process_'.$currency;
//        $dt = Carbon::now();
        $updatePullDateParam = false;

        if ($this->option('startdate') == 'ytd') {
            $lockname .= 'ytd';
            $yesterdayDt = Carbon::now()->addDays(-1);
            $dtFrom = $yesterdayDt->setTime(0, 0, 0)->toDateTimeString();
        } else {
            $dtFrom = $this->option('startdate');

            if (strlen($dtFrom) < 1) {
                $dtFrom = Configs::getParam('SYSTEM_S128_PULLDATE_'.$currency);
                $updatePullDateParam = true;

                if (strlen($dtFrom) < 1) {
                    $dtFrom = '2018-03-01 00:00:00';
                } 
            }
        }

        // Begin.
        App::check_process($lockname);

        $dtFrom = Carbon::parse($dtFrom);
        $dtTo = $dtFrom->copy()->addMinutes(10);
        App::insert_api_log( array( 'request' =>'dtFrom' , 'return' => $dtFrom , 'method' => 'dtFrom' ) );
        App::insert_api_log( array( 'request' =>'dtTo' , 'return' => $dtTo , 'method' => 'dtTo' ) );

        $url = Config::get($currency.'.s128.api_key');
        $postfields = 'api_key='.$url.'&agent_code='.Config::get($currency.'.s128.agent_code').'&start_datetime='.$dtFrom.'&end_datetime='.$dtTo;
        $result = simplexml_load_string(App::curl($postfields , Config::get($currency.'.s128.url').'/get_cockfight_processed_ticket_2.aspx' , true, 10 , 'datapull:'.$this->productCode));         

        $dateTimeNow = Carbon::now();
        $dateTimeNowStr = $dateTimeNow->toDateTimeString();

        if(isset($result->status_code) && (string) $result->status_code == '00' && (string) $result->total_records > 0){
            $alldatas = (string) $result->data;
            $singledata = explode("|", $alldatas);
            foreach($singledata as $key => $arraydata1){
                $datas = explode(',',$arraydata1);
                foreach($datas as $key1 => $data1){
                    $dataarray[$key][$key1] = $data1;
                }
            }

            foreach($dataarray as $arraykey => $arraydata){
                $data[$arraykey] = array(
                        'ticket_id'             => $arraydata[0],
                        'login_id'              => $arraydata[1],
                        'arena_code'            => $arraydata[2],
                        'arena_name_cn'         => $arraydata[3],
                        'match_no'              => $arraydata[4],
                        'match_type'            => $arraydata[5],
                        'match_date'            => $arraydata[6],
                        'fight_no'              => $arraydata[7],
                        'fight_datetime'        => $arraydata[8],
                        'meron_cock'            => $arraydata[9],
                        'meron_cock_cn'         => $arraydata[10],
                        'wala_cock'             => $arraydata[11],
                        'wala_cock_cn'          => $arraydata[12],
                        'bet_on'                => $arraydata[13], 
                        'odds_type'             => $arraydata[14], 
                        'odds_asked'            => $arraydata[15],
                        'odds_given'            => $arraydata[16],
                        'stake'                 => $arraydata[17],
                        'stake_money'           => $arraydata[18],
                        'balance_open'          => $arraydata[19],
                        'balance_close'         => $arraydata[20],
                        'created_datetime'      => $arraydata[21],
                        'fight_result'          => $arraydata[22],
                        'status'                => $arraydata[23],
                        'winloss'               => $arraydata[24],
                        'comm_earned'           => $arraydata[25],
                        'payout'                => $arraydata[26],
                        'balance_open1'         => $arraydata[27],
                        'balance_close1'        => $arraydata[28],
                        'processed_datetime'    => $arraydata[29],
                        'created_at'            => $dateTimeNowStr,
                        'updated_at'            => $dateTimeNowStr,
                );  
                foreach ($data[$arraykey] as $key => $value) {
                        $insertData[$key] = '"' . $value . '"';
                }

                if(DB::statement('call insert_s128('.implode(',',$insertData).')')){
                    $this->InsertWager($data[$arraykey], $currency);
                }

            Profitloss::updatePNL2($this->profitloss);
            }

        }

		if(isset($result->status_code) && (string) $result->status_code == '00'){
				if ($updatePullDateParam) {
					Configs::updateParam( 'SYSTEM_S128_PULLDATE_'.$currency , $dtTo );
				}
		}

        // End.
        App::unlock_process($lockname);

        $this->comment('DataPull done: ' . $this->productCode);
    }

    private function InsertWager( $bet , $currency ){

        if( !isset($this->accounts[$bet['login_id']]) )
            $this->accounts[$bet['login_id']] = Account::where( 'nickname' , '=' , $bet['login_id'] )->where('crccode', '=', $currency)->first();

        $prdObj = Cache::rememberForever('product_s128_obj', function(){
            return Product::where( 'code' , '=' , 'S128' )->first();
        });

        $data['accid'] 		 		= $this->accounts[$bet['login_id']]->id;
        $data['acccode'] 	 		= $this->accounts[$bet['login_id']]->code;
        $data['nickname'] 	 		= $this->accounts[$bet['login_id']]->nickname;
        $data['wbsid'] 		 		= $this->accounts[$bet['login_id']]->wbsid;
        $data['prdid'] 		 		= $prdObj->id;
        $data['matchid'] 	 		= substr($bet['match_no'],1);
        $data['gamename'] 	 		= $bet['arena_name_cn'];
        $data['category'] 	 		= Product::getCategory('racebook');
        $data['refid'] 		 		= $bet['ticket_id'];
        $data['crccode'] 	 		= $this->accounts[$bet['login_id']]->crccode;
        $data['crcrate'] 	 		= Currency::getCurrencyRate($data['crccode']);
        $data['datetime'] 	 		= $bet['created_datetime'];
        $data['ip'] 		 		= '';
        $data['payout'] 	 		= $bet['payout'];
        $data['profitloss']                     = $bet['winloss'];
        $data['accountdate']                    = $bet['match_date'];
        $data['stake']		 		= $bet['stake_money'];
        $data['status'] 			= Wager::STATUS_SETTLED;

        if( $data['stake'] == $data['payout'] )	    $data['result']  = Wager::RESULT_DRAW;
        else if( $data['stake'] > $data['payout'] ) $data['result']  = Wager::RESULT_LOSS;
        else if( $data['stake'] < $data['payout'] ) $data['result']  = Wager::RESULT_WIN;
        $data['validstake']                     = $bet['stake_money'];

        if( $data['profitloss'] == 0 ){
            $data['validstake'] = 0;
        }

        $data['stakelocal'] 		= Currency::getLocalAmount($data['stake'], $data['crccode']);
        $data['payoutlocal']		= Currency::getLocalAmount($data['payout'], $data['crccode']);
        $data['profitlosslocal']	= Currency::getLocalAmount($data['profitloss'], $data['crccode']);
        $data['validstakelocal']	= Currency::getLocalAmount($data['validstake'], $data['crccode']);
        $data['iscalculated'] 	    = 0;
        $data['created']			= date("y-m-d H:i:s");
        $data['modified']			= date("y-m-d H:i:s");

        foreach( $data as $key => $value ){
            $insert_data[$key] = '"'. $value . '"';
        }


        if(DB::statement('call insert_wager('.implode(',',$insert_data).')'))
        {
            $this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['prdid']   = $data['prdid'];
            $this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['accobj']  = $this->accounts[$bet['login_id']];
            $this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['crcrate'] = $data['crcrate'];
        }

    }

   
}
