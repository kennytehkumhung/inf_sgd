<?php namespace App\Console\Commands;

use App\libraries\App;
use App\Models\Account;
use App\Models\Currency;
use App\Models\Configs;
use App\Models\Product;
use App\Models\Profitloss;
use App\Models\Wager;
use Cache;
use Carbon\Carbon;
use Config;
use DB;
use Illuminate\Console\Command;

class DataPullUc8 extends Command
{
    protected $signature = 'datapull:uc8 {currency} {--startdate=} {--enddate=}';
    protected $description = 'Pull member bet ticket';

    // Product configs.
    protected $productCode = 'UC8';

    protected $accounts = array();
    protected $profitloss = array();

    public function handle()
    {
        // Check currency argument.
        $currency = strtoupper($this->argument('currency'));

        // Check product status.
        if (Product::where('status', '=', 2)->where('code', '=', $this->productCode)->count() > 0) {
            $this->error('Product is under maintenance.');
            return;
        }

        // Check start/end datetime.
        $lockname = strtolower($this->productCode) . '_datapull_process_' . $currency;
        $dt = Carbon::now();
        $updatePullDateParam = false;

        if ($this->option('startdate') == 'ytd') {
            $lockname .= 'ytd';
            $yesterdayDt = Carbon::now()->addDays(-1);
            $startDt = $yesterdayDt->setTime(0, 0, 0)->toDateTimeString();
            $endDt = $yesterdayDt->setTime(23, 59, 59)->toDateTimeString();
        } else {
            $startDt = $this->option('startdate');
            $endDt = $this->option('enddate');

            if (strlen($startDt) < 1) {
                $startDt = Configs::getParam('SYSTEM_UC8_PULLDATE_' . $currency);
                $updatePullDateParam = true;

                if (strlen($startDt) < 1) {
                    $startDt = '2017-08-16 12:00:00';
                }
            }
        }

        // Begin.
        App::check_process($lockname);

        if (is_null($endDt)) {
            $endDt = $dt->copy();
        }

        $startDt = Carbon::parse($startDt);
        $endDt = Carbon::parse($endDt);

        $minTemp = 0;
        $partnerId = Config::get($currency . '.uc8.partner_id');
        $apiUrl = Config::get($currency . '.uc8.api_url');
        $header[] = 'Content-Type:application/json';
        $continue = false;

        do {
            $continue = false;

            if ($endDt->diffInMinutes($startDt) > 30) {
                // End datetime is over 30 mins, use time interval of +30 mins instead.
                $dtFromStr = $startDt->addMinutes($minTemp)->toDateTimeString();
                $dtToStr = $startDt->copy()->addMinutes(30)->toDateTimeString();
                $minTemp = 30;
                $continue = true;
            } else {
                $dtFromStr = $startDt->toDateTimeString();
                $dtToStr = $endDt->toDateTimeString();
            }

            $currentPage = 1;
            $totalPages = 0;

            do {
                $postfields = array(
                    'endtime' => $dtToStr,
                    'partner_id' => $partnerId,
                    'page' => $currentPage,
                    'rows' => 500,
                    'starttime' => $dtFromStr,
                );

                $result = json_decode(App::curl(json_encode($postfields), $apiUrl . 'game/history.html', true, 30));

                if (isset($result->error) && $result->error == 0 && isset($result->bets)) {
                    $data = array();

                    foreach ($result->bets as $bi) {
                        $dateTimeNow = Carbon::now();
                        $dateTimeNowStr = $dateTimeNow->toDateTimeString();

                        $data = array(
                            'bet_id' => (int) $bi->bet_id,
                            'username' => (string) $bi->username,
                            'currency' => (string) $currency,
                            'game_type' => (int) $bi->game_type, // 1: Slot; 2: Live Casino; 3: Sportsbook
                            'game_code' => (string) $bi->game_code,
                            'game_name' => (string) $bi->game_name,
                            'bet' => (float) $bi->bet,
                            'win' => (float) $bi->win,
                            'report_date' => (string) $bi->report_date,
                            'game_time' => (string) $bi->game_time,
                            'status' => (int) $bi->status, // 0: Pending; 1: Settled; 2: Voided / Refunded
                            'created_at' => $dateTimeNowStr,
                            'updated_at' => $dateTimeNowStr,
                        );

                        foreach ($data as $key => $value) {
                            $insertData[$key] = '"' . $value . '"';
                        }

                        if ($bi->status == 1 || $bi->status == 2) {
                            if (DB::statement('call insert_uc8(' . implode(',', $insertData) . ')')) {
                                // Get new inserted ID.
                                $this->InsertWager($data, $data['currency']);
                            }
                        }
                    }

                    if ($totalPages <= 0) {
                        if (isset($result->total) && $result->total > 0) {
                            $totalPages = $result->total;
                        }
                    }
                }

                Profitloss::updatePNL2($this->profitloss);

                // Reset array to prevent memory overflow.
                $this->accounts = array();
                $this->profitloss = array();

                $currentPage++;
            } while ($currentPage <= $totalPages);
        } while ($continue);

        if ($updatePullDateParam) {
            Configs::updateParam('SYSTEM_UC8_PULLDATE_' . $currency, $endDt->toDateTimeString());
        }

        // End.
        App::unlock_process($lockname);
    }

    private function InsertWager( $uc8, $currency )
    {
        if( !isset($this->accounts[$uc8['username']]) )
            $this->accounts[$uc8['username']] = Account::where( 'nickname' , '=' , $uc8['username'] )->whereCrccode($currency)->first();

        $prdObj = Cache::rememberForever('product_uc8_obj', function(){
            return Product::where( 'code' , '=' , 'UC8' )->first();
        });

        $account = $this->accounts[$uc8['username']];

        $data['accid'] 		 		= $account->id;
        $data['acccode'] 	 		= $account->code;
        $data['nickname'] 	 		= $account->nickname;
        $data['wbsid'] 		 		= $account->wbsid;
        $data['prdid'] 		 		= $prdObj->id;
        $data['matchid'] 	 		= 0;
        $data['gamename'] 	 		= 'slotgame';
        $data['category'] 	 		= Product::getCategory('egames');
        $data['refid'] 		 		= $uc8['bet_id'];
        $data['crccode'] 	 		= $account->crccode;
        $data['crcrate'] 	 		= Currency::getCurrencyRate($data['crccode']);
        $data['datetime'] 	 		= $uc8['game_time'];
        $data['ip'] 		 		= '';
        $data['payout'] 	 		= ((float) $uc8['bet']) + ((float) $uc8['win']);
        $data['profitloss']  		= $uc8['win'];
        $data['accountdate'] 		= substr( $uc8['game_time'], 0 , 10 );
        $data['accountdate'] 		= $uc8['report_date'];
        $data['stake']		 		= $uc8['bet'];
        $data['status'] 			= Wager::STATUS_SETTLED;

        if( $data['stake'] == $data['payout'] )	    $data['result']  = Wager::RESULT_DRAW;
        else if( $data['stake'] > $data['payout'] ) $data['result']  = Wager::RESULT_LOSS;
        else if( $data['stake'] < $data['payout'] ) $data['result']  = Wager::RESULT_WIN;
        $data['validstake'] 		= $uc8['bet'];
        $data['stakelocal'] 		= Currency::getLocalAmount($data['stake'], $data['crccode']);
        $data['payoutlocal']		= Currency::getLocalAmount($data['payout'], $data['crccode']);
        $data['profitlosslocal']	= Currency::getLocalAmount($data['profitloss'], $data['crccode']);
        $data['validstakelocal']	= Currency::getLocalAmount($data['validstake'], $data['crccode']);
        $data['iscalculated'] 	    = 0;
        $data['created']			= date("y-m-d H:i:s");
        $data['modified']			= date("y-m-d H:i:s");

        $insert_data = array();

        foreach( $data as $key => $value ){
            $insert_data[$key] = '"'. $value . '"';
        }

        if(DB::statement('call insert_wager('.implode(',',$insert_data).')'))
        {
            $this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['prdid']   = $data['prdid'];
            $this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['accobj']  = $this->accounts[$uc8['username']];
            $this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['crcrate'] = $data['crcrate'];
        }

    }
}
