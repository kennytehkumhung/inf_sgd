<?php namespace App\Console\Commands;

use App\libraries\App;
use App\Models\Account;
use App\Models\Betmxb;
use App\Models\Configs;
use App\Models\Currency;
use App\Models\Product;
use App\Models\Profitloss;
use App\Models\Wager;
use Cache;
use Carbon\Carbon;
use Config;
use DB;
use Illuminate\Console\Command;

class DataPullMxb extends Command
{
    protected $signature = 'datapull:mxb {currency} {--startdate= : Example: ytd|2017-01-31 00:00:00}';
    protected $description = 'Pull member bet ticket';

    // Product configs.
    protected $productCode = 'MXB';

    protected $accounts = array();
    protected $profitloss = array();

    public function handle()
    {
        // Check currency argument.
        $currency = strtoupper($this->argument('currency'));

        // Check product status.
        if (Product::where('status', '=', 2)->where('code', '=', $this->productCode)->count() > 0) {
            $this->error('Product is under maintenance.');
            return;
        }

        // Check start/end datetime.
        $lockname = strtolower($this->productCode) . '_datapull_process_'.$currency;
//        $dt = Carbon::now();
        $updatePullDateParam = false;

        if ($this->option('startdate') == 'ytd') {
            $lockname .= 'ytd';
            $yesterdayDt = Carbon::now()->addDays(-1);
            $dtFrom = $yesterdayDt->setTime(0, 0, 0)->toDateTimeString();
        } else {
            $dtFrom = $this->option('startdate');

            if (strlen($dtFrom) < 1) {
                $dtFrom = Configs::getParam('SYSTEM_MXB_RECORDID_'.$currency);
                $updatePullDateParam = true;

                if (strlen($dtFrom) < 1) {
                    $dtFrom = $this->getLatestTransactionTime($currency);
                } else {
                    // Convert config mxb datetime format to support by Carbon.
                    $dtFrom = number_format(($dtFrom - 621355968000000000) / 10000000, 0, '.', '');
                    $dtFrom = Carbon::createFromTimestamp($dtFrom);
                }
            }
        }

        // Begin.
        App::check_process($lockname);

        $dtFrom = Carbon::parse($dtFrom);

        $apiUrl = Config::get($currency.'.mxb.url');
        $operatorUsername = Config::get($currency.'.mxb.username');
        $operatorPassword = Config::get($currency.'.mxb.password');
        $lastTimefrom = '';
        $timefrom = '';
        $continue = false;

        do {
//            $continue = false;

            if ($timefrom == '') {
                $timefrom = $this->getDtToTimestamp($dtFrom);
            }

            $paramstr = 'operatorUsername='.$operatorUsername.'&fromDate='.$timefrom;
            $accessPassword = strtoupper(md5($operatorPassword.$paramstr));
            $postfields = 'GetMBPlayersActivityRawByUpdate?accessKey='.$accessPassword.'&'.$paramstr;

            $result = simplexml_load_string(App::curlGET($apiUrl.$postfields , true, 5, 'datapull:'.$this->productCode));

            if( !isset($result->externalRecordID) || (string)$result->externalRecordID == (string)$timefrom ){
                $continue = false;
            } else {
                $continue = true;
            }

            if(isset($result->errorCode) && $result->errorCode == 0){
                foreach($result->CompressedData->DATA[0] as $key => $category){
                    $data['TransID'] 	   = (int)$category->TransID;
                    $data['RoundID'] 	   = (int)$category->RoundID;
                    $data['GameName'] 	   = (string)$category->GameName;
                    $data['RoundDateTime'] = date( "Y-m-d H:i:s", strtotime($category->RoundDateTime) );
                    $data['RoundResult']   = (int)$category->RoundResult;
                    $data['Username'] 	   = (string)$category->Username;
                    $data['BetID']		   = (int)$category->BetID;
                    $data['BetType'] 	   = (int)$category->BetType;
                    if( $key == 'CARIBBEANPOKER' )
                    {
                        $data['BetAmount'] 	   = (double)($category->AnteBetAmount + $category->RaiseBetAmount);
                        $data['PrizeAmount']   = (double)$category->PrizeAmount;
                    }
                    elseif( $key == 'SINGLEPOKER' ){
                        $data['BetAmount'] 	   = (double)($category->AnteBetAmount + $category->FlopBetAmount + $category->TurnBetAmount + $category->RiverBetAmount + $category->BonusBetAmount);
                        $data['PrizeAmount']   = (double) ($category->PrizeAmount + $category->BonusPrizeAmount);
                    }
                    else
                    {
                        $data['BetAmount'] 	   = (double)$category->BetAmount;
                        $data['PrizeAmount']   = (double)$category->PrizeAmount;
                    }

                    $data['PlayerCards']   = (string)$category->PlayerCards;
                    $data['BankerCards']   = (string)$category->BankerCards;
                    $data['StartBalance']  = (double)$category->StartBalance;
                    $data['EndBalance']    = (double)$category->EndBalance;
                    $data['modified']	   = date("y-m-d H:i:s");
                    $data['created']	   = date("y-m-d H:i:s");
                    $data['currency']	   = $currency;

                    if(DB::statement('call insert_mxb("'.implode('","',$data).'")')){
                        $this->InsertWager($data, $key, $currency);
                    }
                }

                Profitloss::updatePNL2($this->profitloss);
            }

            if( isset($result->externalRecordID) ) {
                $lastTimefrom = $timefrom = (string)$result->externalRecordID;
            } else {
                $continue = false;
            }
        } while ( $continue );

        if ($updatePullDateParam && strlen($lastTimefrom) > 0) {
            Configs::updateParam( 'SYSTEM_MXB_RECORDID_'.$currency , $lastTimefrom );
        }

        // End.
        App::unlock_process($lockname);

        $this->comment('DataPull done: ' . $this->productCode);
    }

    private function InsertWager( $mxb , $type , $currency ){

        if( !isset($this->accounts[$mxb['Username']]) )
            $this->accounts[$mxb['Username']] = Account::where( 'nickname' , '=' , $mxb['Username'] )->whereCrccode($currency)->first();

        $prdObj = Cache::rememberForever('product_mxb_obj', function(){
            return Product::where( 'code' , '=' , 'MXB' )->first();
        });

        $data['accid'] 		 		= $this->accounts[$mxb['Username']]->id;
        $data['acccode'] 	 		= $this->accounts[$mxb['Username']]->code;
        $data['nickname'] 	 		= $this->accounts[$mxb['Username']]->nickname;
        $data['wbsid'] 		 		= $this->accounts[$mxb['Username']]->wbsid;
        $data['prdid'] 		 		= $prdObj->id;
        $data['matchid'] 	 		= $mxb['RoundID'] == '' ? "''":$mxb['RoundID'];
        $data['gamename'] 	 		= $mxb['GameName'];
        $data['category'] 	 		= $type == 'EXTERNAL' ? Product::getCategory('egames') : Product::getCategory('Casino');
        $data['refid'] 		 		= $mxb['TransID'];
        $data['crccode'] 	 		= $this->accounts[$mxb['Username']]->crccode;
        $data['crcrate'] 	 		= Currency::getCurrencyRate($data['crccode']);
        $data['datetime'] 	 		= $mxb['RoundDateTime'];
        $data['ip'] 		 		= '';
        $data['payout'] 	 		= (float) $mxb['PrizeAmount'] < 0 ? $mxb['BetAmount'] : $mxb['PrizeAmount'];
        $data['profitloss']  		= $mxb['PrizeAmount'] < 0 ? 0 : $mxb['PrizeAmount'] - $mxb['BetAmount'];
        $data['accountdate'] 		= substr( $mxb['RoundDateTime'], 0 , 10 );
        $data['stake']		 		= $mxb['BetAmount'];
        $data['status'] 			= Wager::STATUS_SETTLED;

        if( $data['stake'] == $data['payout'] )	    $data['result']  = Wager::RESULT_DRAW;
        else if( $data['stake'] > $data['payout'] ) $data['result']  = Wager::RESULT_LOSS;
        else if( $data['stake'] < $data['payout'] ) $data['result']  = Wager::RESULT_WIN;
        $data['validstake'] 		= $mxb['PrizeAmount'] < 0 ? 0 : $mxb['BetAmount'];

        if( $data['profitloss'] == 0 ){

            $data['validstake'] = 0;
        }

        $data['stakelocal'] 		= Currency::getLocalAmount($data['stake'], $data['crccode']);
        $data['payoutlocal']		= Currency::getLocalAmount($data['payout'], $data['crccode']);
        $data['profitlosslocal']	= Currency::getLocalAmount($data['profitloss'], $data['crccode']);
        $data['validstakelocal']	= Currency::getLocalAmount($data['validstake'], $data['crccode']);
        $data['iscalculated'] 	    = 0;
        $data['created']			= date("y-m-d H:i:s");
        $data['modified']			= date("y-m-d H:i:s");

        foreach( $data as $key => $value ){
            $insert_data[$key] = '"'. $value . '"';
        }


        if(DB::statement('call insert_wager('.implode(',',$insert_data).')'))
        {
            $this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['prdid']   = $data['prdid'];
            $this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['accobj']  = $this->accounts[$mxb['Username']];
            $this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['crcrate'] = $data['crcrate'];
        }

    }

    private function getLatestTransactionTime($currency)
    {
        $date = Betmxb::where( 'currency' , '=' , $currency )->orderBy('RoundDateTime','Desc')->pluck('RoundDateTime');

        if(!$date)

            $date = '2017-01-01 00:00:00';

        return $date;

    }

    private function getDtToTimestamp($datetime) {
        return (Carbon::parse($datetime)->timestamp * 10000000)+621355968000000000;
    }
}
