<?php

namespace App\Console\Commands;

use App\libraries\App;
use App\libraries\PushNotifications;
use App\Models\Account;
use App\Models\Cashbalance;
use App\Models\Cashledger;
use App\Models\Currency;
use Cache;
use Carbon\Carbon;
use Config;
use Crypt;
use DB;
use Exception;
use Hash;
use Illuminate\Console\Command;


class MigrateProduct extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dev:migrateProduct';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Migrate Product';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
		
		$currency = 'MYR';
        $lastAccountId = Cache::get('TEMP_PLT_MIGRATE_LAST_ACCOUNT', 0);
        //$pltPrefix = Config::get($currency.'.plt.prefix');
        
        // Get product ID by code.
        $rows = Cashbalance::whereRaw(' prdid = 5 AND balance > 1 AND crccode = "'.$currency.'" ')->get();
        //$rows = Balancetransfer::whereRaw('`acccode` = "if_pkchan1987" group by accid')->get();
		
		$this->comment('Start!');

        foreach ($rows as $r) {
        
				
				
				$accObj = Account::find($r->accid);
				 
                $plt = new \App\libraries\providers\PLTB();
                $balance = $plt->getBalance($accObj->nickname, $currency);
				
				$this->comment($accObj->code);
				// $balance = 10;
              //var_dump($balance);
                if ($balance > 0) {
                    // Have remaining balance at PLT, transfer to main wallet.
                    // Copy from App\Http\Controllers\User\WalletController.
                    
                    $bltObj = new \App\Models\Balancetransfer();
                    $bltObj->accid 			= $r->accid;
                    $bltObj->acccode 		= $accObj->code;
                    $bltObj->from 			= 5;
                    $bltObj->to 			= 0;
                    $bltObj->crccode		= $currency;
                    $bltObj->crcrate		= Currency::getCurrencyRate($bltObj->crccode);
                    $bltObj->amount 		= $balance;
                    $bltObj->amountlocal 	= Currency::getLocalAmount($bltObj->amount, $bltObj->crccode);
                    $bltObj->status			= CBO_STANDARDSTATUS_SUSPENDED;
                    
                    if ($bltObj->save()) {
                        $productwalletObj = new \App\Models\Productwallet();
                        $productwalletObj->prdid 		= $bltObj->from;
                        $productwalletObj->accid 		= $bltObj->accid;
                        $productwalletObj->acccode 		= $bltObj->acccode;
                        $productwalletObj->chtcode 		= CBO_CHARTCODE_BALANCETRANSFER;
                        $productwalletObj->crccode 		= $bltObj->crccode;
                        $productwalletObj->crcrate 		= $bltObj->crcrate;
                        $productwalletObj->amount		= abs($bltObj->amount) * -1;
                        $productwalletObj->amountlocal	= abs($bltObj->amountlocal) * -1;
                        $productwalletObj->refobj		= 'Balancetransfer';
                        $productwalletObj->refid		= $bltObj->id;
						
						

                        if( $productwalletObj->save() )
                        {
    //                        $className  = "App\libraries\providers\\".$request->get('from');
    //                        $providerObj = new $className;
                            $providerObj = $plt;

                            if( $providerObj->withdraw( $accObj->nickname , $currency , $productwalletObj->amount * -1, $productwalletObj->id ) )
                            {
                                $cashLedgerObj = new Cashledger();
                                $cashLedgerObj->accid 			= $bltObj->accid;
                                $cashLedgerObj->acccode 		= $bltObj->acccode;
                                $cashLedgerObj->accname			= $accObj->fullname;
                                $cashLedgerObj->chtcode 		= CBO_CHARTCODE_BALANCETRANSFER;
                                $cashLedgerObj->cashbalance 	= $balance;
                                $cashLedgerObj->amount 			= $bltObj->amount;
                                $cashLedgerObj->amountlocal 	= $bltObj->amountlocal;
                                $cashLedgerObj->refid 			= $bltObj->id;
                                $cashLedgerObj->status 			= CBO_LEDGERSTATUS_CONFIRMED;
                                $cashLedgerObj->refobj 			= 'Balancetransfer';
                                $cashLedgerObj->crccode 		= $currency;
                                if( $cashLedgerObj->save() )
                                {
                                    $productwalletObj->status = CBO_LEDGERSTATUS_CONFIRMED;
                                    /* if( $fromlog = Apilog::whereAccid(Session::get('userid'))->whereMethod('transferCredit')->orderBy('id','desc')->pluck('return') )
                                    {
                                        $bltObj->fromlog = $fromlog;
                                    } */
                                    $bltObj->status     = CBO_LEDGERSTATUS_CONFIRMED;
                                    $productwalletObj->save();	
                                    $bltObj->save();		  	  
                                    $success = true;
									
									
                                }
								
							/* 	$password = $accObj->enpassword;
									
									if (strlen($password)) {
										$password = \Crypt::decrypt($password);
									}
									$parameterStr  = "player/update";
									$parameterStr .= "/playername/" . $pltPrefix . strtoupper($accObj->nickname);
									$parameterStr .= "/frozen/1";
									$parameterStr .= "/suspended/1";
									$parameterStr .= "/password/" . $password;

									var_dump(json_decode(\App\libraries\providers\PLT::CallFunction($parameterStr, $currency, 'suspendAccount'))); 	
								
								 	
									
									 */
								
									
                            }
                            else{
                                $productwalletObj->status = CBO_LEDGERSTATUS_CANCELLED;
                                $bltObj->status     	  = CBO_STANDARDSTATUS_SUSPENDED;
                                /* if( $fromlog = Apilog::whereAccid(Session::get('userid'))->whereMethod('transferCredit')->orderBy('id','desc')->pluck('return') )
                                {
                                    $bltObj->fromlog = $fromlog;
                                } */
                                $bltObj->save();		  	  
                                $productwalletObj->save();	
                                $success = false;
                            }

                        }
                    }
                }
					
							
         
                
         
		}
		
		
		$this->comment('Done!');
		
    }
}
