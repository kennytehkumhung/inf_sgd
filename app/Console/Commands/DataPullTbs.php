<?php namespace App\Console\Commands;

use App\libraries\App;
use App\Models\Account;
use App\Models\Configs;
use App\Models\Currency;
use App\Models\Product;
use App\Models\Profitloss;
use App\Models\Wager;
use Cache;
use Carbon\Carbon;
use Config;
use DB;
use Illuminate\Console\Command;

class DataPullTbs extends Command
{
    protected $signature = 'datapull:tbs {currency}';
    protected $description = 'Pull member bet ticket';

    // Product configs.
    protected $productCode = 'TBS';

    protected $accounts = array();
    protected $profitloss = array();

    public function handle()
    {
        // Check currency argument.
        $currency = strtoupper($this->argument('currency'));

        // Check product status.
        if (Product::where('status', '=', 2)->where('code', '=', $this->productCode)->count() > 0) {
            $this->error('Product is under maintenance.');
            return;
        }
        
        $lockname = strtolower($this->productCode) . '_datapull_process_'.$currency;
        App::check_process($lockname);
        
        $sortno = Configs::getParam('SYSTEM_TBS_PULLDATE_'.$currency);
		
        if (is_null($sortno)) {
                $sortno = 0;
        }
        
        //do something here...
        $postfields = 'APIPassword='.Config::get($currency.'.tbs.password').
                    '&AgentID='.Config::get($currency.'.tbs.agentid').
                    '&sortNo='.$sortno.
                    '&Rows=500';

        $result = simplexml_load_string( App::curl( $postfields , Config::get($currency.'.tbs.url').'GetBetSheetByAgent' ) );
            
        if( isset($result->errcode) && $result->errcode == 000000 && isset($result->result->bet) && count($result->result->bet) > 0 ){
            foreach( $result->result->bet as $key => $value ){
                $accountTemp = explode('@', $value->Account);
                if( isset($accountTemp[0]) && Account::where( 'nickname' , '=' , $accountTemp[0] )->count() != 0 ){
                    // Some columns are affected by new API.
                    $data['BetID'] 	   		= (string)substr($value->BetID, 2);
                    $data['Account']   		= $accountTemp[0];
                    $data['BetAmount'] 		= (string)$value->BetAmount;
                    $data['BetOdds']	   	= (string)$value->BetOdds;
                    $data['Win']	   		= (string)$value->Win;
                    $data['OddsStyle']  		= (string)$value->OddsStyle;
                    $data['BetDate']   		= (string)$value->BetDate;
                    $data['Status']    		= (string)$value->Status;
                    $data['Result']    		= (string)$value->Result;
                    $data['ReportDate']     = (string)$value->ReportDate;
                    $data['SportName']      = (string)$value->SportName;
                    $data['BetIP']     		= (string)$value->BetIP;
                    $data['GameID']			= (int)$value->GameID;
                    $data['SubGameID']		= (int)$value->SubGameID;
                    $data['BackAmount']		= $value->BackAmount;
                    $data['DeductAmount']		= $value->DeductAmount;
                    $data['AllWin']			= $value->AllWin;
                    $data['Turnover']		= $value->Turnover;
                    $data['BetUpdateTime']		= $value->UpdateTime;
                    $data['BetInfo']		= null;
                    $sortno = $value->SortNo;
                    
                    // Extract JSON data from BetInfo.
                    $betInfo = json_decode($value->BetInfo);
                    $betInfo = collect($betInfo[0]);

                    $data['BetSubID']		= (int)$betInfo->get('SubID');
                    $data['SportID']		= (int)$betInfo->get('SportID');
                    $data['LeagueID']		= (int)$betInfo->get('LeagueID');
                    $data['MatchID']		= (int)$betInfo->get('MatchID');
                    $data['HomeID']			= (int)$betInfo->get('HomeID');
                    $data['AwayID']			= (int)$betInfo->get('AwayID');
                    $data['Stage']			= (int)$betInfo->get('Stage');
                    $data['MarketID']		= (int)$betInfo->get('MarketID');
                    $data['Odds']			= $betInfo->get('Odds');
                    $data['Hdp']			= $betInfo->get('Hdp');
                    $data['HomeScore']		= (int)$betInfo->get('HomeScore');
                    $data['AwayScore']		= (int)$betInfo->get('AwayScore');
                    $data['HomeCard']		= (int)$betInfo->get('HomeCard');
                    $data['AwayCard']		= (int)$betInfo->get('AwayCard');
                    $data['BetPos']			= (int)$betInfo->get('BetPos');
                    $data['OutLevel']		= $betInfo->get('OutLevel');
                    //$data['BetInfoResult']		= (int)$betInfo->get('Result');
                    //$data['BetInfoStatus']		= (int)$betInfo->get('Status');

                    // Finishing extract data.
                    $data['updated_at']		= date("y-m-d H:i:s");
                    $data['created_at']		= date("y-m-d H:i:s");

                    foreach( $data as $key => $value ){
                            $insert_data[$key] = '"'. $value . '"';
                    }	

                    if(DB::statement('call insert_tbs('.implode(',',$insert_data).')')){
                            $this->InsertWager($data, $currency);
                    }	
                }
            }
            
            Configs::updateParam( 'SYSTEM_TBS_PULLDATE_'.$currency , $sortno );

            Profitloss::updatePNL2($this->profitloss);	
	}
        // End.
        App::unlock_process($lockname);

        $this->comment('DataPull done: ' . $this->productCode);
    }

    private function InsertWager( $tbs , $currency ){
		
		$temp = explode("@", $tbs['Account']);
		$tbs['Account'] = $temp[0];
		
		if( !isset($this->accounts[$tbs['Account']]) )
			$this->accounts[$tbs['Account']] = Account::where( 'nickname' , '=' , $tbs['Account'] )->where( 'crccode' , '=' , $currency )->first();
		
		$prdObj = Cache::rememberForever('product_tbs_obj', function(){
				return Product::where( 'code' , '=' , 'TBS' )->first();
		});

		$data['accid'] 		 		= $this->accounts[$tbs['Account']]->id;
		$data['acccode'] 	 		= $this->accounts[$tbs['Account']]->code;
		$data['nickname'] 	 		= $tbs['Account'];
		$data['wbsid'] 		 		= $this->accounts[$tbs['Account']]->wbsid;
		$data['prdid'] 		 		= $prdObj->id;
		$data['matchid'] 	 		= $tbs['MatchID'];
		$data['gamename'] 	 		= 'Sports';
		$data['category'] 	 		= Product::getCategory('sports');
		$data['refid'] 		 		= $tbs['BetID'];
		$data['crccode'] 	 		= $this->accounts[$tbs['Account']]->crccode;
		$data['crcrate'] 	 		= Currency::getCurrencyRate($data['crccode']);
		$data['datetime'] 	 		= $tbs['BetUpdateTime']; // $tbs['BetDate'];
		$data['ip'] 		 		= '';
	
			
		$data['payout'] 	 		= $tbs['Win'] + $tbs['BetAmount'];
		$data['profitloss']  		= (float) ( $tbs['Win'] );
		$data['accountdate'] 		= substr( $tbs['ReportDate'], 0 , 10 );
		$data['stake']		 		= $tbs['BetAmount'];

		// New API has changed status from string to numeric.
		if($tbs['Status'] == 'Running' || $tbs['Status'] == 1) {
			$data['status'] = Wager::STATUS_OPEN;
		}
		else if($tbs['Status'] == 'Refund') $data['status'] = Wager::STATUS_REFUND;
		else if($tbs['Status'] == 'Rejected' || $tbs['Status'] == 4) $data['status'] = Wager::STATUS_REJECTED;
		else $data['status'] = Wager::STATUS_SETTLED;
		$data['result'] 	 		= Wager::RESULT_PENDING;
		if($tbs['Result'] == '0')	   $data['result']  = Wager::RESULT_DRAW;
		else if($tbs['Result'] == '1')  $data['result']  = Wager::RESULT_WIN;
		else if($tbs['Result'] == '2') $data['result'] = Wager::RESULT_LOSS;
                else if($tbs['Result'] == '3') $data['result'] = Wager::RESULT_WINHALF;
                else if($tbs['Result'] == '4') $data['result'] = Wager::RESULT_LOSSHALF;
		$data['validstake'] 		= $tbs['Turnover'];
		
		$data['stakelocal'] 		= Currency::getLocalAmount($data['stake'], 		$data['crccode']);
		$data['payoutlocal']		= Currency::getLocalAmount($data['payout'],		$data['crccode']);
		$data['profitlosslocal']	= Currency::getLocalAmount($data['profitloss'], $data['crccode']);
		$data['validstakelocal']	= Currency::getLocalAmount($data['validstake'], $data['crccode']);
		$data['iscalculated']	    = 0;
		$data['created']			= date("y-m-d H:i:s");
		$data['modified']			= date("y-m-d H:i:s");

		foreach( $data as $key => $value ){
			$insert_data[$key] = '"'. $value . '"';
		}
		

		if(DB::statement('call insert_wager('.implode(',',$insert_data).')'))
		{
			$this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['prdid']   = $data['prdid'];
			$this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['accobj']  = $this->accounts[$tbs['Account']];
			$this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['crcrate'] = $data['crcrate'];
		}
	}

}
