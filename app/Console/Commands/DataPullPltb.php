<?php namespace App\Console\Commands;

use App\libraries\App;
use App\Models\Account;
use App\Models\Betpltb;
use App\Models\Configs;
use App\Models\Currency;
use App\Models\Product;
use App\Models\Profitloss;
use App\Models\Wager;
use Cache;
use Carbon\Carbon;
use Config;
use DB;
use Illuminate\Console\Command;

class DataPullPltb extends Command
{
    protected $signature = 'datapull:pltb {currency} {--startdate= : Example: ytd|2017-01-31 00:00:00} {--enddate= : Example: 2017-01-31 23:59:59} {--page= : Example: 1} {--minute= : Example: 10}';
    protected $description = 'Pull member bet ticket';

    // Product configs.
    protected $productCode = 'PLTB';

    protected $accounts = array();
    protected $profitloss = array();

    public function handle()
    {
        // Check currency argument.
        $currency = strtoupper($this->argument('currency'));

        // Check product status.
        if (Product::where('status', '=', 2)->where('code', '=', $this->productCode)->count() > 0) {
            $this->error('Product is under maintenance.');
            return;
        }

        // Check start/end datetime.
        $lockname = strtolower($this->productCode) . '_datapull_process_'.$currency;
        $page	  = 1;
        $minute	  = 10;
        $continue = true;
        $updatePullDateParam = true;
        $dateTimeFormat = 'Y-m-d H.i.s';

        if ($this->option('startdate') == 'ytd') {
            // PLTB unable to repull due to API call 1 minute time interval restriction. Reset Last Pull Datetime and let robot run as usual.
            App::check_process($lockname.'ytd');
            return;
        }

        if (is_numeric($this->option('page'))) {
            $page = $this->option('page');
        }

        if (is_numeric($this->option('minute'))) {
            $minute = $this->option('minute');
        }

        if (file_exists(storage_path('app').'/'.$lockname.'ytd.lock')) {
            // Reset Last Pull Datetime.
            $yesterdayDt = Carbon::now()->addDays(-1)->setTime(0, 0, 0);
            Configs::updateParam( 'SYSTEM_PLTB_PULLDATE_'.$currency , $yesterdayDt->toDateTimeString() );
            App::unlock_process($lockname.'ytd');
        }

        $startdate = $this->option('startdate');
        $todate = $this->option('enddate');

        if (strlen($startdate) < 1) {
            $startdate = Carbon::parse(Configs::getParam('SYSTEM_PLTB_PULLDATE_'.$currency));

//            if ($startdate->diffInHours() > 1) {
//                $startdate = Carbon::now()->addHour(-1);
//            }
        } else {
            $startdate = Carbon::parse($startdate);
            $updatePullDateParam = false;
        }

        if (strlen($todate) < 1) {
            $todate = $startdate->copy()->addMinutes($minute);
        } else {
            $todate = Carbon::parse($todate);
        }

        // Begin.
        App::check_process($lockname);

        while($continue) {
            $startdateStr = $startdate->format($dateTimeFormat);
            $todateStr = $todate->format($dateTimeFormat);

            $parameterStr  = "report/getbetlog";
            $parameterStr .= "/startdate/" . str_replace(' ', '%20', $startdateStr);
            $parameterStr .= "/enddate/" . str_replace(' ', '%20', $todateStr);
            $parameterStr .= "/page/" . $page;
            $parameterStr .= "/producttype/0";
            $parameterStr .= "/currency/" . $currency;

            $results = json_decode($this->callFunction($parameterStr, null, $currency, 'get', 'datapull:'.$this->productCode));

            if( isset($results->Result) ) {
                $currencyRate = ($currency == 'IDR' ? 1000 : 1);

                foreach( $results->Result as $key => $result ){
                    $username = $result->PlayerName;

                    if (!str_contains($username, '_')) {
                        // PLTB username should always contain prefix.
                        continue;
                    } else {
                        $temp = explode('_',$username);
                        $username = $temp[1];
                    }

                    $data['playerName']       = $result->PlayerName;
                    $data['username']         = $username;
                    $data['windowCode']       = $result->WindowCode;
                    $data['gameId']           = $result->GameId;
                    $data['gameCode']         = $result->GameCode;
                    $data['gameType']         = $result->GameType;
                    $data['gameName']         = $result->GameName;
                    $data['sessionId']        = $result->SessionId;
                    $data['bet']      	 	  = ((float) $result->Bet) / $currencyRate;
                    $data['win']      	 	  = ((float) $result->Win) / $currencyRate;
                    $data['progressiveBet']   = ((float)$result->ProgressiveBet) / $currencyRate;
                    $data['progressiveWin']   = ((float)$result->ProgressiveWin) / $currencyRate;
                    $data['currency']   	  = $currency;
                    $data['balance']   	  = $result->Balance;
                    $data['currentBet']   	  = $result->CurrentBet;
                    $data['gameDate']   	  = $result->GameDate;
                    $data['liveNetwork']   	  = $result->LiveNetwork;
                    $data['rNum']   	  = $result->RNum;
                    $data['created_at']	  = date("y-m-d H:i:s");
                    $data['updated_at']	  = date("y-m-d H:i:s");

                    foreach( $data as $key => $value ) {
                        $insert_data[$key] = '"'. $value . '"';
                    }

                    if(DB::statement('call insert_pltb('.implode(',',$insert_data).')')) {
//                        if ($data['bet'] > 0.000000 && $data['win'] > 0.000000) {
                            $this->InsertWager($data, $currency);
//                        }
                    }
                }

                if ($updatePullDateParam) {
                    if( $gamedate = Betpltb::where( 'currency' , '=' , $currency )->orderBy('gameDate','desc')->pluck('gameDate') ) {
                        Configs::updateParam( 'SYSTEM_PLTB_PULLDATE_'.$currency , $gamedate );
                    } else {
                        Configs::updateParam( 'SYSTEM_PLTB_PULLDATE_'.$currency , $todate->toDateTimeString() );
                    }
                }

                Profitloss::updatePNL2($this->profitloss);
            } elseif ($updatePullDateParam) {
                Configs::updateParam( 'SYSTEM_PLTB_PULLDATE_'.$currency , $todate->toDateTimeString() );
            }

            if( isset($results->Pagination->CurrentPage) ) {
                if( $results->Pagination->CurrentPage >= $results->Pagination->TotalPage ) {
                    // End of page reached.
                    $continue = false;
                } else {
                    $page = $results->Pagination->CurrentPage + 1;
                }
            } else {
                $continue = false;
            }

            if( $continue == false ) {
                if( Carbon::now()->gt($todate) ) {
                    $startdate = $todate->copy();
                    $todate->addMinutes($minute);
                    $continue = true;
                } else {
                    $continue = false;
                }
            }
        }

        // End.
        App::unlock_process($lockname);

        $this->comment('DataPull done: ' . $this->productCode);
    }

    private function InsertWager( $bet , $currency){

        if( !isset($this->accounts[$bet['username']]) )
            $this->accounts[$bet['username']] = Account::where( 'nickname' , '=' , $bet['username'] )->whereCrccode($currency)->first();

        $prdObj = Cache::rememberForever('product_pltb_obj', function(){
            return Product::where( 'code' , '=' , 'PLTB' )->first();
        });

        if(!isset($this->accounts[$bet['username']]->id))
            exit;

        $data['accid'] 		 		= $this->accounts[$bet['username']]->id;
        $data['acccode'] 	 		= $this->accounts[$bet['username']]->code;
        $data['nickname'] 	 		= $bet['username'];
        $data['wbsid'] 		 		= $this->accounts[$bet['username']]->wbsid;
        $data['prdid'] 		 		= $prdObj->id;
        $data['matchid'] 	 		= $bet['gameId'];
        $data['gamename'] 	 		= $bet['gameType'];
        $data['category'] 	 		= $bet['gameType'] == 'Live Games' ? Product::getCategory('Casino') : Product::getCategory('egames');
        $data['refid'] 		 		= $bet['gameCode'];
        $data['crccode'] 	 		= $this->accounts[$bet['username']]->crccode;
        $data['crcrate'] 	 		= Currency::getCurrencyRate($data['crccode']);
        $data['datetime'] 	 		= $bet['gameDate'];
        $data['ip'] 		 		= '';
        if(  $bet['bet'] == 0 )
            $data['payout'] = 0;
        else
            $data['payout'] 	 	= $bet['win'] + $bet['progressiveWin'];
        $data['profitloss']  		= $bet['win'] + $bet['progressiveWin'] - $bet['bet'];
        $data['accountdate'] 		= substr( $bet['gameDate'], 0 , 10 );
        $data['stake']		 		= $bet['bet'];
        $data['status'] 			= Wager::STATUS_SETTLED;

        if( $data['stake'] == $data['payout'] )	    $data['result']  = Wager::RESULT_DRAW;
        else if( $data['stake'] > $data['payout'] ) $data['result']  = Wager::RESULT_LOSS;
        else if( $data['stake'] < $data['payout'] ) $data['result']  = Wager::RESULT_WIN;
        $data['validstake'] 		= $bet['bet'];

        if ($data['result'] == Wager::RESULT_DRAW) {
            $data['validstake'] = 0;
        }

        $data['stakelocal'] 		= Currency::getLocalAmount($data['stake'], $data['crccode']);
        $data['payoutlocal']		= Currency::getLocalAmount($data['payout'], $data['crccode']);
        $data['profitlosslocal']	= Currency::getLocalAmount($data['profitloss'], $data['crccode']);
        $data['validstakelocal']	= Currency::getLocalAmount($data['validstake'], $data['crccode']);
        $data['iscalculated'] 	    = 0;
        $data['created']			= date("y-m-d H:i:s");
        $data['modified']			= date("y-m-d H:i:s");

        foreach( $data as $key => $value ){
            $insert_data[$key] = '"'. $value . '"';
        }


        if(DB::statement('call insert_wager('.implode(',',$insert_data).')'))
        {
            $this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['prdid']   = $data['prdid'];
            $this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['accobj']  = $this->accounts[$bet['username']];
            $this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['crcrate'] = $data['crcrate'];
        }


    }

    private function callFunction( $action , $params , $currency, $httpMethod, $method , $timeout = 10)
    {
        // Copy from PLTB provider lib.
        $header   = array();
        $header[] = "Cache-Control:no-cache";
        $header[] = "merchantname:".Config::get($currency.'.pltb.merchantname');
        $header[] = "merchantcode:".Config::get($currency.'.pltb.merchantcode');

        $tuCurl = curl_init();

        switch (strtolower($httpMethod)) {
            case 'post':
                $header[] = "Content-Type:application/json";
                curl_setopt($tuCurl, CURLOPT_POST, true);
                break;
            case 'put':
                $header[] = "Content-Type:application/json";
                curl_setopt($tuCurl, CURLOPT_CUSTOMREQUEST, 'PUT');
                break;
            default:
                curl_setopt($tuCurl, CURLOPT_POST, false);
                break;
        }

        $url = Config::get($currency.'.pltb.url').$action;

        curl_setopt($tuCurl, CURLOPT_URL, $url);
        curl_setopt($tuCurl, CURLOPT_TIMEOUT, $timeout);
        curl_setopt($tuCurl, CURLOPT_RETURNTRANSFER, 1);

        if (!is_null($params)) {
            $params = json_encode($params);

            curl_setopt($tuCurl, CURLOPT_POSTFIELDS, $params);
        } else {
            $params = '';
        }

        curl_setopt($tuCurl, CURLOPT_HTTPHEADER, $header);
        $result = trim(curl_exec($tuCurl));
        //var_dump(	$params);
        //var_dump(	$result);

        if(!$result)
        {
//            dd(curl_error($tuCurl));
        }

        curl_close($tuCurl);

//        App::insert_api_log( array( 'request' => 'param='.$url.'?'.str_replace(' ', '%20', $params) , 'return' => $result , 'method' => $method ) );

        return $result;
    }
}
