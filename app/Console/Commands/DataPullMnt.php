<?php namespace App\Console\Commands;

use App\libraries\App;
use App\Models\Account;
use App\Models\Betmnt;
use App\Models\Configs;
use App\Models\Currency;
use App\Models\Product;
use App\Models\Profitloss;
use App\Models\Wager;
use Cache;
use Carbon\Carbon;
use Config;
use DB;
use Illuminate\Console\Command;

class DataPullMnt extends Command
{
    protected $signature = 'datapull:mnt {currency} {--startdate= : Example: ytd|2017-01-31} {--enddate= : Example: 2017-01-31}';
    protected $description = 'Pull member bet ticket';

    // Product configs.
    protected $productCode = 'MNT';

    protected $accounts = array();
    protected $profitloss = array();

    public function handle()
    {
        // Check currency argument.
        $currency = strtoupper($this->argument('currency'));

        // Check product status.
        if (Product::where('status', '=', 2)->where('code', '=', $this->productCode)->count() > 0) {
            $this->error('Product is under maintenance.');
            return;
        }

        // Check start/end datetime.
        $lockname = strtolower($this->productCode) . '_datapull_process_'.$currency;
        $dt = Carbon::now();
        $updatePullDateParam = false;

        if ($this->option('startdate') == 'ytd') {
            $lockname .= 'ytd';
            $yesterdayDt = Carbon::now()->addDays(-1);
            $dtFrom = $yesterdayDt->toDateString();
            $dtTo = $yesterdayDt->toDateString();
        } else {
            $dtFrom = $this->option('startdate');
            $dtTo = $this->option('enddate');

            if (strlen($dtFrom) < 1) {
                $dtFrom = Configs::getParam('SYSTEM_MNT_PULLDATE_' . $currency);
                $updatePullDateParam = true;

                if (strlen($dtFrom) < 1) {
                    $dtFrom = '2017-10-10';
                }
            }
        }

        // Begin.
        App::check_process($lockname);

        if (is_null($dtTo)) {
            $dtTo = $dt->copy();
        }

        $dtFrom = Carbon::parse($dtFrom)->setTime(0, 0, 0);
        $dtTo = Carbon::parse($dtTo)->setTime(23, 59, 59);

        $url = Config::get($currency . '.mnt.url') . '/appaspx/QueryWinLosePlayerList.aspx?';
        $gmAcc = Config::get($currency . '.mnt.api_username');
        $gmPwd = md5(Config::get($currency . '.mnt.api_password'));
        $auth = Config::get($currency . '.mnt.auth');
        $prefix = Config::get($currency . '.mnt.prefix');

        while ($dtFrom->lte($dtTo)) {
            $data = array(
                'gmAcc' => $gmAcc,
                'gmPwd' => $gmPwd,
                'startTime' => $dtFrom->toDateString(),
                'endTime' => $dtFrom->toDateString(),
                'sign' => '',
            );

            $data['sign'] = md5($data['gmAcc'] . $data['gmPwd'] . $auth);

            $result = json_decode(App::curlGET($url . http_build_query($data), true, 30, 'datapull:'.$this->productCode));

            /*
             * {"result":0,"mesg":"Successful operation","winLoseList":[{"playerAcc":"GSCBADEGG","totalIncome":7,"totalOutlay":13,"washCountSum":0}],"moneyBase":10}
             */

            if (isset($result->result) && $result->result == 0 && isset($result->winLoseList)) {
                $accountCache = collect(array());
                $moneyBase = (int) $result->moneyBase;

                if ($moneyBase <= 0) {
                    $moneyBase = 1;
                }

                foreach ($result->winLoseList as $r) {
                    if (($r->totalIncome + $r->totalOutlay + $r->washCountSum) > 0) {
                        $dateTimeNow = Carbon::now();
                        $dateTimeNowStr = $dateTimeNow->toDateTimeString();

                        if (!$accountCache->has($r->playerAcc)) {
                            $accObj = Account::where('nickname', '=', substr($r->playerAcc, 3))->first();
                            $val = array('accid' => '', 'nickname' => '');

                            if ($accObj) {
                                $val['accid'] = $accObj->id;
                                $val['nickname'] = $accObj->nickname;
                            }

                            $accountCache->put($r->playerAcc, $val);
                        }

                        $accObj =$accountCache->get($r->playerAcc);

                        $data = array(
                            'bet_date'			=> $dtFrom->toDateString(),
                            'player_acc'		=> $r->playerAcc,
                            'accid'				=> $accObj['accid'],
                            'nickname'			=> $accObj['nickname'],
                            'currency'			=> $currency,
                            'total_income'		=> ((float) $r->totalIncome) / $moneyBase,
                            'total_outlay'		=> ((float) $r->totalOutlay) / $moneyBase,
                            'wash_count_sum'	=> ((float) $r->washCountSum) / $moneyBase,
                            'created_at'		=> $dateTimeNowStr,
                            'updated_at'		=> $dateTimeNowStr,
                        );

                        foreach ($data as $key => $value) {
                            $insertData[$key] = '"'.$value.'"';
                        }

                        if (DB::statement('call insert_mnt('.implode(',',$insertData).')')) {
                            $mntObj = Betmnt::where('bet_date', '=', $data['bet_date'])
                                ->where('player_acc' , '=', $data['player_acc'])
                                ->first(['id']);

                            if ($mntObj) {
                                $data['id'] = $mntObj->id;

                                $this->InsertWager($data, $currency);
                            }
                        }
                    }
                }

                Profitloss::updatePNL2($this->profitloss);
            }

            $dtFrom->addDays(1);
        }

        if ($updatePullDateParam) {
            Configs::updateParam('SYSTEM_MNT_PULLDATE_'.$currency, $dtTo->toDateTimeString());
        }

        // End.
        App::unlock_process($lockname);

        $this->comment('DataPull done: ' . $this->productCode);
    }

    private function InsertWager( $bet , $currency){

        if( !isset($this->accounts[$bet['nickname']]) )
            $this->accounts[$bet['nickname']] = Account::where( 'nickname' , '=' , $bet['nickname'] )->whereCrccode($currency)->first();

        $prdObj = Cache::rememberForever('product_mnt_obj', function(){
            return Product::where( 'code' , '=' , 'MNT' )->first();
        });

        $account = $this->accounts[$bet['nickname']];

        $data['accid'] 		 		= $account->id;
        $data['acccode'] 	 		= $account->code;
        $data['nickname'] 	 		= $account->nickname;
        $data['wbsid'] 		 		= $account->wbsid;
        $data['prdid'] 		 		= $prdObj->id;
        $data['matchid'] 	 		= $bet['id'] == '' ? "''":$bet['id'];
        $data['gamename'] 	 		= 'Fishing World';
        $data['category'] 	 		= Product::getCategory('egames');
        $data['refid'] 		 		= $bet['id'];
        $data['crccode'] 	 		= $account->crccode;
        $data['crcrate'] 	 		= Currency::getCurrencyRate($data['crccode']);
        $data['datetime'] 	 		= $bet['bet_date'];
        $data['ip'] 		 		= '';
        $data['payout'] 	 		= (float) $bet['total_income'];
        $data['profitloss']			= ((float) $bet['total_income']) - ((float) $bet['total_outlay']);
        $data['accountdate']			= substr( $data['datetime'], 0 , 10 );
        $data['stake']		 		= $bet['total_outlay'];
        $data['status'] 			= Wager::STATUS_SETTLED;


        if( $data['stake'] == $data['payout'] )	    $data['result']  = Wager::RESULT_DRAW;
        else if( $data['stake'] > $data['payout'] ) $data['result']  = Wager::RESULT_LOSS;
        else if( $data['stake'] < $data['payout'] ) $data['result']  = Wager::RESULT_WIN;
        $data['validstake'] 		= $bet['total_outlay'];
        $data['stakelocal'] 		= Currency::getLocalAmount($data['stake'], $data['crccode']);
        $data['payoutlocal']		= Currency::getLocalAmount($data['payout'], $data['crccode']);
        $data['profitlosslocal']	= Currency::getLocalAmount($data['profitloss'], $data['crccode']);
        $data['validstakelocal']	= Currency::getLocalAmount($data['validstake'], $data['crccode']);
        $data['iscalculated'] 	    = 0;
        $data['created']			= date("y-m-d H:i:s");
        $data['modified']			= date("y-m-d H:i:s");

        foreach( $data as $key => $value ){
            $insert_data[$key] = '"'. $value . '"';
        }


        if(DB::statement('call insert_wager('.implode(',',$insert_data).')'))
        {
            $this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['prdid']   = $data['prdid'];
            $this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['accobj']  = $account;
            $this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['crcrate'] = $data['crcrate'];
        }

    }
}
