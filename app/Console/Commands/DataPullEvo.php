<?php namespace App\Console\Commands;

use App\libraries\App;
use App\Models\Account;
use App\Models\Betevo;
use App\Models\Currency;
use App\Models\Product;
use App\Models\Profitloss;
use App\Models\Wager;
use Cache;
use Carbon\Carbon;
use Config;
use DB;
use Illuminate\Console\Command;

class DataPullEvo extends Command
{
    protected $signature = 'datapull:evo {currency} {--startdate= : Example: ytd|2017-01-31 00:00:00} {--enddate= : Example: 2017-01-31 23:59:59}';
    protected $description = 'Pull member bet ticket';

    // Product configs.
    protected $productCode = 'EVO';

    protected $accounts = array();
    protected $profitloss = array();

    public function handle()
    {
        // Check currency argument.
        $currency = strtoupper($this->argument('currency'));

        // Check product status.
        if (Product::where('status', '=', 2)->where('code', '=', $this->productCode)->count() > 0) {
            $this->error('Product is under maintenance.');
            return;
        }

        // Check start/end datetime.
        $lockname = strtolower($this->productCode) . '_datapull_process_'.$currency;

        if ($this->option('startdate') == 'ytd') {
            $lockname .= 'ytd';
            $yesterdayDt = Carbon::now()->addDays(-1);
            $stDate = $yesterdayDt->setTime(0, 0, 0)->toDateTimeString();
            $enDate = $yesterdayDt->setTime(23, 59, 59)->toDateTimeString();
        } else {
            $stDate = $this->option('startdate');
            $enDate = $this->option('enddate');

            if (strlen($stDate) < 1) {
                $stDate = $this->getLatestTransactionTime($currency);
            }

            if (strlen($enDate) < 1) {
                $enDate = Carbon::now()->toDateTimeString();
            }
        }

        // Begin.
        App::check_process($lockname);
        
        $stDate = Carbon::parse($stDate)->addHours(-8)->addMinute(-5)->format('Y-m-d\TH:i:s.000\Z');
        $enDate = Carbon::parse($enDate)->addHours(-8)->format('Y-m-d\T23:59:59.000\Z');
        
        $postfields = 'startDate='.$stDate.
                      '&endDate='.$enDate;
        $header   = array();
        $header[] = 'Authorization:Basic ZjBkbWVrZGNhZ2dkcXRjODo1Y2EwYjMyMjIxNmM1YTI2Y2NjZTE3YTQ5YjlhOGFlYQ==';
        
        $result = App::curlGET( Config::get($currency.'.evo.url').'api/gamehistory/v1/casino/games/stream?'.$postfields, true, 30, 'datapull:evo', $header);
        
        if($result != 'The requested resource could not be found.'){
            $result_arrays = explode("\n", $result);

            foreach($result_arrays as $result_array){
                if (!empty($result_array)) {
                    $dateTimeNow = Carbon::now()->toDateTimeString();
                    $record = json_decode($result_array);
                    $bets = $record->participants->bets;
                    $total_stake = 0;
                    foreach($bets as $bet){
                        $total_stake += $bet->stake;
                    }
                    $data = array(
                        'refid'         => date('U').mt_rand(11111,99999),
                        'username'      => (string) $record->participants->playerId,
                        'currency'      => $currency,
                        'gamename'      => (string) $record->gameType,
                        'gamerefid'     => (string) $record->id,
                        'gametableid'   => (string) $record->table->id,
                        'roundbetdatetime'   => (string) Carbon::parse($record->settledAt)->toDateTimeString(),
                        'betdatetime'   => (string) Carbon::parse($record->settledAt)->addHours(8)->toDateTimeString(),
                        'betamt'        => ((float) $total_stake),
                        'wlamt'         => ((float) $record->payout),
                        'bet_side'      => '',
                        'bet_result'    => '',
                        'created_at'    => $dateTimeNow,
                        'updated_at'    => $dateTimeNow,
                    );
                    foreach ($data as $key => $value) {
                        $insertData[$key] = '"'.$value.'"';
                    }
                    if (DB::statement('call insert_evo('.implode(',',$insertData).')')) {
                        $betObj = Betevo::where('gamerefid', '=', $data['gamerefid'])->first(['refid']);

                        if ($betObj) {
                            // Get ref id of inserted record.
                            $data['refid'] = $betObj->refid;
                        }
                        // Get new inserted ID.
                        $this->InsertWager($data,$data['currency']);
                    }                
                }
            } 
            Profitloss::updatePNL2($this->profitloss);
        }
        // Reset array to prevent memory overflow.
        $this->accounts = array();
        $this->profitloss = array();
        
        
        // End.
        App::unlock_process($lockname);

        $this->comment('DataPull done: ' . $this->productCode);
    }

    private function InsertWager( $evo  , $currency){

        if( !isset($this->accounts[$evo['username']]) )
            $this->accounts[$evo['username']] = Account::where( 'nickname' , '=' , $evo['username'] )->whereCrccode($currency)->first();

        $prdObj = Cache::rememberForever('product_evo_obj', function(){
            return Product::where( 'code' , '=' , 'EVO' )->first();
        });

        $account = $this->accounts[$evo['username']];

        $data['accid'] 		 		= $account->id;
        $data['acccode'] 	 		= $account->code;
        $data['nickname'] 	 		= $account->nickname;
        $data['wbsid'] 		 		= $account->wbsid;
        $data['prdid'] 		 		= $prdObj->id;
        $data['matchid'] 	 		= 0;
        $data['gamename'] 	 		= $evo['gamename'];
        $data['category'] 	 		= Product::getCategory('Casino');
        $data['refid'] 		 		= $evo['refid'];
        $data['crccode'] 	 		= $account->crccode;
        $data['crcrate'] 	 		= Currency::getCurrencyRate($data['crccode']);
        $data['datetime'] 	 		= $evo['betdatetime'];
        $data['ip'] 		 		= '';
        $data['payout'] 	 		= (float) $evo['wlamt'];
        $data['profitloss']  		= ((float) $evo['wlamt']) - ((float) $evo['betamt']);
        $data['accountdate'] 		= substr( $evo['betdatetime'], 0 , 10 );
        $data['stake']		 		= $evo['betamt'];
        $data['status'] 			= Wager::STATUS_SETTLED;

        if( $data['stake'] == $data['payout'] )	    $data['result']  = Wager::RESULT_DRAW;
        else if( $data['stake'] > $data['payout'] ) $data['result']  = Wager::RESULT_LOSS;
        else if( $data['stake'] < $data['payout'] ) $data['result']  = Wager::RESULT_WIN;
        $data['validstake'] 		= $data['stake'];

        if ($data['profitloss'] == 0 || $data['result'] == Wager::RESULT_DRAW) {
            $data['validstake'] = 0;
        }

        $data['stakelocal'] 		= Currency::getLocalAmount($data['stake'], $data['crccode']);
        $data['payoutlocal']		= Currency::getLocalAmount($data['payout'], $data['crccode']);
        $data['profitlosslocal']	= Currency::getLocalAmount($data['profitloss'], $data['crccode']);
        $data['validstakelocal']	= Currency::getLocalAmount($data['validstake'], $data['crccode']);
        $data['iscalculated'] 	    = 0;
        $data['created']			= date("y-m-d H:i:s");
        $data['modified']			= date("y-m-d H:i:s");

        foreach( $data as $key => $value ){
            $insert_data[$key] = '"'. $value . '"';
        }

        if(DB::statement('call insert_wager('.implode(',',$insert_data).')'))
        {
            $this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['prdid']   = $data['prdid'];
            $this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['accobj']  = $this->accounts[$evo['username']];
            $this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['crcrate'] = $data['crcrate'];
        }


    }

    private function getLatestTransactionTime($currency)
    {
        $date = Betevo::where('currency', '=', $currency)->orderBy('betdatetime','desc')->pluck('betdatetime');

        if(!$date) {
            $date = '2018-10-05 00:00:00';
        } else {
            $date = Carbon::parse($date)->toDateTimeString();
        }

        return $date;

    }
}
