<?php namespace App\Console\Commands;

use App\libraries\App;
use App\Models\Account;
use App\Models\Currency;
use App\Models\Configs;
use App\Models\Product;
use App\Models\Profitloss;
use App\Models\Wager;
use Cache;
use Carbon\Carbon;
use Config;
use DB;
use Illuminate\Console\Command;
use App\Models\Betwmc;

class DataPullWmc extends Command
{
    protected $signature = 'datapull:wmc {currency} {--startdate=} {--enddate=}';
    protected $description = 'Pull member bet ticket';

    // Product configs.
    protected $productCode = 'WMC';

    protected $accounts = array();
    protected $profitloss = array();

    public function handle()
    {

        // Check currency argument.
        $currency = strtoupper($this->argument('currency'));

        // Check product status.
        if (Product::where('status', '=', 2)->where('code', '=', $this->productCode)->count() > 0) {
            $this->error('Product is under maintenance.');
            return;
        }

        // Check start/end datetime.
		$lockname = strtolower($this->productCode) . '_datapull_process_' . $currency;
        $dt = Carbon::now();
        $updatePullDateParam = false;
	
        if ($this->option('startdate') == 'ytd') {
            $lockname .= 'ytd';
            $yesterdayDt = Carbon::now()->addDays(-1);
            $startDt = $yesterdayDt->setTime(0, 0, 0)->format('Ymd');
            $endDt = $yesterdayDt->setTime(23, 59, 59)->format('Ymd');
			
        } else {
            $startDt = $this->option('startdate');
            $endDt = $this->option('enddate');
		 
            if (strlen($startDt) < 1) {
                 $startDay = Configs::getParam('SYSTEM_WMC_PULLDATE_' . $currency); 
				  /*  $startDay = Betwmc::orderBy('betTime','Desc')->first()->betTime; */
				$updatePullDateParam = true;
				
				if($startDay!=null){
					$startDt=Carbon::parse($startDay)->subDay()->format('Ymd'); 
				}
                if (strlen($startDt) < 1) 
				{
                    $startDt = '20180419';
                }
				$endDt=Carbon::now()->format('Ymd'); 

            }
        }
	
        App::check_process($lockname);
	
		if (is_null($endDt)) {
            	$endDt=Carbon::now()->format('Ymd'); 
        }
		
        $website = Config::get($currency  . '.wmc.BaseServiceURL');
        $vendorid = Config::get($currency . '.wmc.vendorid');
		$signature = Config::get($currency. '.wmc.signature');
		$continue=true; 
		
		while($continue){
			$endtime=Carbon::parse($startDt)->addDay()->format('Ymd');
			$data = array(
			'cmd'=> 'GetDateTimeReport',
            'vendorId' => Config::get($currency. '.wmc.vendorId'),
            'signature' => Config::get($currency. '.wmc.signature'),
            'startTime' => $startDt,
			'endTime'	=>	$endtime, 
			);
				
		$postfields = $data;
		
		$result = json_decode($this->curl(Config::get($currency.'.wmc.BaseServiceURL'),$postfields, true, 30 ,'datapull:wmc' ));
	
			    if(isset($result) && $result->errorCode == '0'){
                    $data = array();
			
                    foreach ($result->result as $bi) {
							
                        $dateTimeNow = Carbon::now();
                        $dateNow = Carbon::now()->toDateString();
                        $dateTimeNowStr = $dateTimeNow->toDateTimeString();
						if($currency == 'IDR')
						{
						$bet = $bi->bet/1000;
						$winloss = $bi->winLoss/1000;
						}
						else
						{
						$bet = $bi->bet;
						$winloss = $bi->winLoss;
						}
                        $data = array(
                  
	
							'user' => (String)$bi->user,
							'betId' =>(int)$bi->betId,
                            'betTime' =>  $bi->betTime,
							'bet' => $bet,
							'validbet' =>  (float)$bi->validbet,
							'water' => (float)$bi->water,
							'result' => (float)$bi->result,
							'betResult' => (string)$bi->betResult,
							'waterbet' => (float)$bi->waterbet,
							'winLoss'	=> (String)$winloss, 
							'ip' => (string)$bi->ip,
							'gid' => (int)$bi->gid,
							'event' => (int)$bi->event,
							'eventChild' => (int)$bi->eventChild,
							'tableId' => (int)$bi->tableId,
							'gameResult' => (int)$bi->gameResult,
							'gname' => (String)$bi->gname,
                            'created_at' => $dateTimeNowStr,
                            'updated_at' => $dateTimeNowStr,
                        );
						
                        foreach ($data as $key => $value) {
                            $insertData[$key] = '"' . $value . '"';
                        }
								
                        if (DB::statement('call insert_wmc(' . implode(',' , $insertData) . ')')) {
                            $this->InsertWager($data,$currency);
                        }
                    
				    }
                }
			
                Profitloss::updatePNL2($this->profitloss);

                $this->accounts = array();
                $this->profitloss = array();
		
	
	
			if($gamedate = Betwmc::orderBy('betTime','Desc')->pluck('betTime')){
			Configs::updateParam( 'SYSTEM_WMC_PULLDATE_' . $currency, $gamedate );
			}

            $this->comment('DataPull date: ' . $startDt);
            if($result->errorCode == '10411'){
                $this->comment('DataPull done: ' . $result->errorCode);
                sleep(31);
            }else{
                if($endDt==$startDt){
                    $continue=false;
                }else{
                    $startDt=Carbon::parse($startDt)->addDay()->format('Ymd');
                    sleep(31);
                }
			}
		
		}

		
         App::unlock_process($lockname);
		$this->comment('DataPull done: ' . $this->productCode);
    }

    private function InsertWager( $wmc, $currency )
    {
		$prefix=substr($wmc['user'],0,3); 
		if($prefix=='IFW'||$prefix=='RYW'){
		$user=substr($wmc['user'],4); 
		}else {
		$user=$wmc['user'];
		}
		if( !isset($this->accounts[$user]) )
            $this->accounts[$user] = Account::where( 'nickname' , '=' , $user )->whereCrccode($currency)->first();
		
        $prdObj = Cache::rememberForever('product_wmc_obj', function(){
            return Product::where( 'code' , '=' , 'WMC' )->first();
        });
		
        $account = $this->accounts[$user];
        $data['accid'] 		 		= $account->id;
        $data['acccode'] 	 		= $account->code;
        $data['nickname'] 	 		= $account->nickname;
        $data['wbsid'] 		 		= $account->wbsid;
        $data['prdid'] 		 		= $prdObj->id;
        $data['matchid'] 	 		= 0;
        $data['gamename'] 	 		= $wmc['gname'];
		$data['category'] 	 		= Product::getCategory('Casino');
        $data['refid'] 		 		= $wmc['betId'];
        $data['crccode'] 	 		= $account->crccode;
        $data['crcrate'] 	 		= Currency::getCurrencyRate($data['crccode']);
        $data['datetime'] 	 		= $wmc['betTime'];
        $data['ip'] 		 		= '';
        $data['payout'] 	 		= ((float)$wmc['bet']) + ((float) $wmc['winLoss']);
        $data['profitloss']  		= $wmc['winLoss'];
        $data['accountdate'] 		= substr($wmc['betTime'],0,10);
        $data['stake']		 		= ((float)$wmc['bet']);
        $data['status'] 			= Wager::STATUS_SETTLED;
		
        if( $data['stake'] == $data['payout'] )	    $data['result']  = Wager::RESULT_DRAW;
        else if( $data['stake'] > $data['payout'] ) $data['result']  = Wager::RESULT_LOSS;
        else if( $data['stake'] < $data['payout'] ) $data['result']  = Wager::RESULT_WIN;
        
        if($data['result']  != Wager::RESULT_DRAW)
            $data['validstake'] 		= $wmc['bet'];
        else
            $data['validstake'] 		= 0;
        
        $data['stakelocal'] 		= Currency::getLocalAmount($data['stake'], $data['crccode']);
        $data['payoutlocal']		= Currency::getLocalAmount($data['payout'], $data['crccode']);
        $data['profitlosslocal']	= Currency::getLocalAmount($data['profitloss'], $data['crccode']);
        $data['validstakelocal']	= Currency::getLocalAmount($data['validstake'], $data['crccode']);
        $data['iscalculated'] 	    = 0;
        $data['created']			= date("y-m-d H:i:s");
        $data['modified']			= date("y-m-d H:i:s");

        $insert_data = array();
	
        foreach( $data as $key => $value ){
            $insert_data[$key] = '"'. $value . '"';
        }

        if(DB::statement('call insert_wager('.implode(',',$insert_data).')'))
        {
            $this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['prdid']   = $data['prdid'];
            $this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['accobj']  = $this->accounts[$user];
            $this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['crcrate'] = $data['crcrate'];
        }

    }
   function curl($url, $postFields, $log = true, $timeout = 30, $method = '')
    {
      
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postFields);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);

        if ($log)
			$strpostfield=http_build_query($postFields);
             App::insert_api_log(array('request' => $url . '?'.$strpostfield , 'return' => $response, 'method' => $method));

        return $response;

    }
}
