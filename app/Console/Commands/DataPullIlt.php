<?php namespace App\Console\Commands;

use App\libraries\App;
use App\libraries\providers\ILT;
use App\Models\Account;
use App\Models\Betilt;
use App\Models\Configs;
use App\Models\Currency;
use App\Models\Product;
use App\Models\Profitloss;
use App\Models\Wager;
use Cache;
use Carbon\Carbon;
use Config;
use DB;
use Illuminate\Console\Command;

class DataPullIlt extends Command
{
    protected $signature = 'datapull:ilt {currency} {--date= : Example: ytd|2017-01-31} {--betslipsid= : Start from given betslipsid}';
    protected $description = 'Pull member bet ticket';

    // Product configs.
    protected $productCode = 'ILT';

    protected $accounts = array();
    protected $profitloss = array();

    public function handle()
    {
        // Check currency argument.
        $currency = strtoupper($this->argument('currency'));

        // Check product status.
        if (Product::where('status', '=', 2)->where('code', '=', $this->productCode)->count() > 0) {
            $this->error('Product is under maintenance.');
            return;
        }

        // Compare unicode string.
//        $coll = collator_create('zh_CN');
//        collator_compare($coll, '', '');

        // Check start/end datetime.
        $lockname = strtolower($this->productCode) . '_datapull_process_'.$currency;
        $bid = -1;
        $updatePullDateParam = false;

        if ($this->option('date') == 'ytd') {
            $lockname .= 'ytd';
            $yesterdayDt = Carbon::now()->addDays(-1);
            $stDate = $yesterdayDt->toDateString();
        } else {
            $stDate = $this->option('date');
        }

        App::check_process($lockname);

        // Begin.
        if (strlen($stDate) > 0) {
            $record = Betilt::where('currency', '=', $currency)
                ->between('datecreated', array($stDate.' 00:00:00', $stDate.' 23:59:59'))
                ->orderBy('datecreated', 'asc')
                ->first(array('betslipsid'));

            if ($record) {
                $bid = $record->betslipsid;
            }
        } else {
            $bid = $this->option('betslipsid');

            if (empty($bid) || $bid < 1) {
                $bid = Configs::getParam('SYSTEM_ILT_BETSLIPSID_'.$currency);
                $updatePullDateParam = true;
            }
        }

        if ($bid < 1) {
            $this->comment('Invalid betslipsid or no betslipsid found on given date.');
            App::unlock_process($lockname);

            return;
        }

        $iltObj = new ILT();
        $apiUrl = Config::get($currency . '.ilt.url_api') . '/History';
        $com = Config::get($currency . '.ilt.code');
        $username = Config::get($currency . '.ilt.agent');
        $dkey = $iltObj->getSignature($currency, $username);

        for ($i = 0; $i < 200; $i++) { // Safe lock.
            $data = array(
                'key' => $dkey,
                'com' => $com,
                'uid' => $username,
                'bid' => $bid, // betslipsid
            );

            $result = json_decode(App::curl(http_build_query($data), $apiUrl, true, 10, 'datapull:'.$this->productCode), true);

            if (isset($result['status']) && $result['status'] == 0 && isset($result['record'])) {
                $dateTimeNowStr = Carbon::now()->toDateTimeString();
                $accountCache = collect(array());

                if (count($result['record']) > 0) {
                    foreach ($result['record'] as $bkey => $bet) {
                        $bid = (int) $bkey;

                        if (!$accountCache->has($bet['account'])) {
                            $accObj = Account::where('nickname', '=', substr($bet['account'], 4))->first();
                            $val = array('accid' => '', 'nickname' => '');

                            if ($accObj) {
                                $val['accid'] = $accObj->id;
                                $val['nickname'] = $accObj->nickname;
                            }

                            $accountCache->put($bet['account'], $val);
                        }

                        $accObj = $accountCache->get($bet['account']);

                        $data = array(
                            'bet_id' => $bkey,
                            'account' => $bet['account'],
                            'userid' => $accObj['accid'],
                            'username' => $accObj['nickname'],
                            'currency' => $currency,
                            'betslipid' => $bet['recid'],
                            'betref' => $bet['recReference'],
                            'eventtime' => $bet['eventtime'],
                            'league' => $bet['league'],
                            'type' => $bet['type'],
                            'teambet' => '',
                            'stake' => $bet['stake'],
                            'times' => '',
                            'mode' => '',
                            'odds' => '',
                            'result' => $bet['result'],
                            'win' => $bet['win'],
                            'status' => $bet['status'],
                            'datecreated' => $bet['datecreated'],
                            'created_at' => $dateTimeNowStr,
                            'updated_at' => $dateTimeNowStr,
                        );

                        $stmtParams = array();
                        $stmtValues = array();
                        foreach ($data as $key => $value) {
                            $stmtParams[] = ':'.$key;
                            $stmtValues[$key] = $value;
                        }

                        if (DB::statement('call insert_ilt('.implode(',',$stmtParams).')', $stmtValues)) {
                            $this->InsertWager($data,$currency);
                        }
                    }
                    
                    Profitloss::updatePNL2($this->profitloss);
                } else {
                    break;
                }
            } else {
                // No record anymore.
                break;
            }
        }

        if ($updatePullDateParam) {
            Configs::updateParam('SYSTEM_ILT_BETSLIPSID_'.$currency, $bid);
        }

        // End.
        App::unlock_process($lockname);

        $this->comment('DataPull done: ' . $this->productCode);
    }

    private function InsertWager($ilt  , $currency){

        if( !isset($this->accounts[$ilt['account']]) )
            $this->accounts[$ilt['account']] = Account::where( 'nickname' , '=' , $ilt['username'] )->whereCrccode($currency)->first();

        $prdObj = Cache::rememberForever('product_ilt_obj', function(){
            return Product::where( 'code' , '=' , 'ILT' )->first();
        });

        $account = $this->accounts[$ilt['account']];

        $data['accid'] 		 		= $account->id;
        $data['acccode'] 	 		= $account->code;
        $data['nickname'] 	 		= $account->nickname;
        $data['wbsid'] 		 		= $account->wbsid;
        $data['prdid'] 		 		= $prdObj->id;
        $data['matchid'] 	 		= 0;
        $data['gamename'] 	 		= $ilt['league'];
        $data['category'] 	 		= Product::getCategory('lottery');
        $data['refid'] 		 		= $ilt['betslipid'];
        $data['crccode'] 	 		= $account->crccode;
        $data['crcrate'] 	 		= Currency::getCurrencyRate($data['crccode']);
        $data['datetime'] 	 		= $ilt['datecreated'];
        $data['ip'] 		 		= '';
        $data['payout'] 	 		= (float) $ilt['win'];
        $data['profitloss']  		= ((float) $ilt['win']) - ((float) $ilt['stake']);
        $data['accountdate'] 		= substr( $ilt['datecreated'], 0 , 10 );
        $data['stake']		 		= $ilt['stake'];

        if(in_array($ilt['result'], array(1, -1))) {
            if($ilt['status'] == 1) {
                $data['status'] = Wager::STATUS_SETTLED;
            } else {
                $data['status'] = Wager::STATUS_REJECTED;
            }
        } else {
            $data['status'] = Wager::STATUS_OPEN;
        }

        if( $data['stake'] == $data['payout'] )	    $data['result']  = Wager::RESULT_DRAW;
        else if( $data['stake'] > $data['payout'] ) $data['result']  = Wager::RESULT_LOSS;
        else if( $data['stake'] < $data['payout'] ) $data['result']  = Wager::RESULT_WIN;
        $data['validstake'] 		= $data['stake'];

        if ($data['profitloss'] == 0) {
            $data['validstake'] = 0;
        }

        $data['stakelocal'] 		= Currency::getLocalAmount($data['stake'], $data['crccode']);
        $data['payoutlocal']		= Currency::getLocalAmount($data['payout'], $data['crccode']);
        $data['profitlosslocal']	= Currency::getLocalAmount($data['profitloss'], $data['crccode']);
        $data['validstakelocal']	= Currency::getLocalAmount($data['validstake'], $data['crccode']);
        $data['iscalculated'] 	    = 0;
        $data['created']			= date("y-m-d H:i:s");
        $data['modified']			= date("y-m-d H:i:s");

        foreach( $data as $key => $value ){
            $insert_data[$key] = '"'. $value . '"';
        }

        if(DB::statement('call insert_wager('.implode(',',$insert_data).')'))
        {
            $this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['prdid']   = $data['prdid'];
            $this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['accobj']  = $this->accounts[$ilt['account']];
            $this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['crcrate'] = $data['crcrate'];
        }


    }
}
