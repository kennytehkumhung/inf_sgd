<?php namespace App\Console\Commands;

use App\Models\Paymentgatewaysetting;
use Exception;
use Illuminate\Console\Command;

class PaymentGatewayCheckTrans extends Command
{
    protected $signature = 'pg:checktrans {currency} {--code=}';
    protected $description = 'Manual check payment gateway\'s transaction.';

    public function handle()
    {
        $currency = strtoupper($this->argument('currency'));
        $code = $this->option('code');
        $builder = Paymentgatewaysetting::where('class_name', '!=', '');

        if (strlen($code) > 0) {
            $builder->where('code', '=', $code);
        }

        $pgsObjs = $builder->get();

        foreach ($pgsObjs as $pgsObj) {
            $this->comment('Checking: ' . $pgsObj->code);

            try {
                $className = $pgsObj->class_name;
                $clsObj = new $className($currency);

                if ($pgsObj->check_deposit_trans == 1) {
                    $clsObj->checkDepositTrans();
                }

                if ($pgsObj->check_withdraw_trans == 1) {
                    $clsObj->checkWithdrawTrans();
                }
            } catch (Exception $e) {
                $this->comment('Error: ' . $pgsObj->code);
            }

            $this->comment('Done.');
        }

        $this->comment('PG check trans done: ' . $code);
    }
}
