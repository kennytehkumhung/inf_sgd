<?php namespace App\Console\Commands;

use Carbon\Carbon;
use File;
use Illuminate\Console\Command;

class ReleaseLockFile extends Command
{
    protected $signature = 'dev:releaselockfile {--min= : In minutes, default: 120}';
    protected $description = 'Release datapull lock file that exists after amount of minutes.';

    // Except those files.
    protected $exceptions = array(
        'SKY_datapull_process_MYRytd',
        'SKY_datapull_process_IDRytd',
        'JOK_datapull_process_MYRytd',
    );

    public function handle()
    {
        // Check for datapull lock file.
        $minutes = $this->option('min');

        if (strlen($minutes) < 1) {
            $minutes = 120;
        }

        $lockfilePaths = glob(storage_path('app').DIRECTORY_SEPARATOR.'*.lock');

        foreach ($lockfilePaths as $fpath) {
            if (File::exists($fpath)) {
                $name = File::name($fpath);

                $this->comment('Checking lock file: ' . $name);

                if (in_array($name, $this->exceptions)) {
                    $this->comment('File is in exception list, skipping file: ' . $name);
                    return;
                }

                $lastModified = File::lastModified($fpath);
                $dt = Carbon::createFromTimestamp($lastModified);

                // Delete files that exists over certain of minutes.
                if ($dt->diffInMinutes(Carbon::now()) >= $minutes) {
                    $this->comment('Deleting Lock file: ' . $name);

                    if (File::delete($fpath)) {
                        $this->comment('Lock file deleted: ' . $name);
                    } else {
                        $this->comment('Unable to delete file: ' . $name);
                    }
                } else {
                    $this->comment('Lock file is safe, skipping file: ' . $name);
                }
            }
        }

        $this->comment('Done.');
    }
}
