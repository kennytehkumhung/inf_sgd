<?php namespace App\Console\Commands;

use App\libraries\App;
use App\Models\Account;
use App\Models\Betvgs;
use App\Models\Currency;
use App\Models\Product;
use App\Models\Profitloss;
use App\Models\Wager;
use Cache;
use Carbon\Carbon;
use Config;
use DB;
use Illuminate\Console\Command;

class DataPullVgs extends Command
{
    protected $signature = 'datapull:vgs {currency} {--startdate= : Example: ytd|2017-01-31 00:00:00} {--enddate= : Example: 2017-01-31 23:59:59}';
    protected $description = 'Pull member bet ticket';

    // Product configs.
    protected $productCode = 'VGS';

    protected $accounts = array();
    protected $profitloss = array();

    public function handle()
    {
        // Check currency argument.
        $currency = strtoupper($this->argument('currency'));

        // Check product status.
        if (Product::where('status', '=', 2)->where('code', '=', $this->productCode)->count() > 0) {
            $this->error('Product is under maintenance.');
            return;
        }

        // Check start/end datetime.
        $lockname = strtolower($this->productCode) . '_datapull_process_'.$currency;
        $currentPage = 0;
        $totalPages = 0;

        if ($this->option('startdate') == 'ytd') {
            $lockname .= 'ytd';
            $yesterdayDt = Carbon::now()->addDays(-1);
            $stDate = $yesterdayDt->setTime(0, 0, 0)->toDateTimeString();
            $enDate = $yesterdayDt->setTime(23, 59, 59)->toDateTimeString();
        } else {
            $stDate = $this->option('startdate');
            $enDate = $this->option('enddate');

            if (strlen($stDate) < 1) {
                $stDate = $this->getLatestTransactionTime($currency);
            }

            if (strlen($enDate) < 1) {
                $enDate = Carbon::now()->toDateTimeString();
            }
        }

        // Begin.
        App::check_process($lockname);

        $stDate = str_replace(' ', 'T', $stDate);
        $enDate = str_replace(' ', 'T', $enDate);

        do {
            $postfields  =   'pageSize=300&page='.$currentPage;
            $postfields .=   '&operatorUsername='.Config::get($currency.'.vgs.username');
            $postfields .=   '&fromDate='.$stDate;
            $postfields .=   '&toDate='.$enDate;

            $accessPassword = strtoupper(md5(Config::get($currency.'.vgs.password').str_replace(' ' , '%20', $postfields)));

            $result = simplexml_load_string(App::curlGET(Config::get($currency.'.vgs.url').'GetPlayersActivity?accessKey='.$accessPassword.'&'.$postfields, true, 40 , 'datapull:'.$this->productCode));

            if(isset($result->errorCode) && $result->errorCode == '0') {
                $result = simplexml_load_string($result->result);
                $currencyRate = ($currency == 'IDR' ? 1000 : 1);

                foreach ($result->{'Bet'} as $value) {
                    $dateTimeNow = Carbon::now()->toDateTimeString();

                    $data = array(
                        'refid'         => date('U').mt_rand(11111,99999),
                        'ticketid'      => ((string) $value->gameRefID) . ((string) $value->username),
                        'username'      => (string) $value->username,
                        'currency'      => $currency,
                        'gamename'      => (string) $value->gameName,
                        'gamerefid'     => (string) $value->gameRefID,
                        'gametableid'   => (string) $value->gameTableID,
                        'betdatetime'   => (string) Carbon::parse($value->betDateTime)->addHours(8)->toDateTimeString(),
                        'betamt'        => ((float) $value->betAmt) / $currencyRate,
                        'wl'            => ((float) $value->wl) / $currencyRate,
                        'wlamt'         => ((float) $value->wlAmt) / $currencyRate,
                        'updated_at'    => $dateTimeNow,
                        'created_at'    => $dateTimeNow,
                    );

                    foreach ($data as $key => $value) {
                        $insertData[$key] = '"'.$value.'"';
                    }

                    if (DB::statement('call insert_vgs('.implode(',',$insertData).')')) {
                        $betObj = Betvgs::where('ticketid', '=', $data['ticketid'])->first(['refid']);

                        if ($betObj) {
                            // Get ref id of inserted record.
                            $data['refid'] = $betObj->refid;
                        }

                        $this->InsertWager($data,$currency);
                    }
                }

                if ($totalPages <= 0) {
                    if (isset($result->Page->totalPage) && $result->Page->totalPage > 0) {
                        $totalPages = $result->Page->totalPage;
                    }
                }

                Profitloss::updatePNL2($this->profitloss);
            }

            // Reset array to prevent memory overflow.
            $this->accounts = array();
            $this->profitloss = array();

            $currentPage++;
        } while ($currentPage <= $totalPages);

        // End.
        App::unlock_process($lockname);

        $this->comment('DataPull done: ' . $this->productCode);
    }

    private function InsertWager( $vgs  , $currency){

        if( !isset($this->accounts[$vgs['username']]) )
            $this->accounts[$vgs['username']] = Account::where( 'nickname' , '=' , $vgs['username'] )->whereCrccode($currency)->first();

        $prdObj = Cache::rememberForever('product_vgs_obj', function(){
            return Product::where( 'code' , '=' , 'VGS' )->first();
        });

        $account = $this->accounts[$vgs['username']];

        $data['accid'] 		 		= $account->id;
        $data['acccode'] 	 		= $account->code;
        $data['nickname'] 	 		= $account->nickname;
        $data['wbsid'] 		 		= $account->wbsid;
        $data['prdid'] 		 		= $prdObj->id;
        $data['matchid'] 	 		= 0;
        $data['gamename'] 	 		= $vgs['gamename'];
        $data['category'] 	 		= Product::getCategory('Casino');
        $data['refid'] 		 		= $vgs['refid'];
        $data['crccode'] 	 		= $account->crccode;
        $data['crcrate'] 	 		= Currency::getCurrencyRate($data['crccode']);
        $data['datetime'] 	 		= $vgs['betdatetime'];
        $data['ip'] 		 		= '';
        $data['payout'] 	 		= (float) $vgs['wlamt'];
        $data['profitloss']  		= ((float) $vgs['wlamt']) - ((float) $vgs['betamt']);
        $data['accountdate'] 		= substr( $vgs['betdatetime'], 0 , 10 );
        $data['stake']		 		= $vgs['betamt'];
        $data['status'] 			= Wager::STATUS_SETTLED;

        if( $data['stake'] == $data['payout'] )	    $data['result']  = Wager::RESULT_DRAW;
        else if( $data['stake'] > $data['payout'] ) $data['result']  = Wager::RESULT_LOSS;
        else if( $data['stake'] < $data['payout'] ) $data['result']  = Wager::RESULT_WIN;
        $data['validstake'] 		= $data['stake'];

        if ($data['profitloss'] == 0 || $data['result'] == Wager::RESULT_DRAW) {
            $data['validstake'] = 0;
        }

        $data['stakelocal'] 		= Currency::getLocalAmount($data['stake'], $data['crccode']);
        $data['payoutlocal']		= Currency::getLocalAmount($data['payout'], $data['crccode']);
        $data['profitlosslocal']	= Currency::getLocalAmount($data['profitloss'], $data['crccode']);
        $data['validstakelocal']	= Currency::getLocalAmount($data['validstake'], $data['crccode']);
        $data['iscalculated'] 	    = 0;
        $data['created']			= date("y-m-d H:i:s");
        $data['modified']			= date("y-m-d H:i:s");

        foreach( $data as $key => $value ){
            $insert_data[$key] = '"'. $value . '"';
        }

        if(DB::statement('call insert_wager('.implode(',',$insert_data).')'))
        {
            $this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['prdid']   = $data['prdid'];
            $this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['accobj']  = $this->accounts[$vgs['username']];
            $this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['crcrate'] = $data['crcrate'];
        }


    }

    private function getLatestTransactionTime($currency)
    {
        $date = Betvgs::where('currency', '=', $currency)->orderBy('betdatetime','desc')->pluck('betdatetime');

        if(!$date) {
            $date = '2016-11-09 00:00:00';
        } else {
            $date = Carbon::parse($date)->addHours(-8)->toDateTimeString ();
        }

        return $date;

    }
}
