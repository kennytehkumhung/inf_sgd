<?php namespace App\Console\Commands;

use App\libraries\App;
use App\Models\Account;
use App\Models\Betisn;
use App\Models\Currency;
use App\Models\Product;
use App\Models\Profitloss;
use App\Models\Wager;
use Cache;
use Carbon\Carbon;
use Config;
use DB;
use Illuminate\Console\Command;

class DataPullIsn extends Command
{
    protected $signature = 'datapull:isn {currency} {--startdate= : Example: ytd|2017-01-31 00:00:00} {--enddate= : Example: 2017-01-31 23:59:59}';
    protected $description = 'Pull member bet ticket';

    // Product configs.
    protected $productCode = 'ISN';

    protected $accounts = array();
    protected $profitloss = array();

    public function handle()
    {
        // Check currency argument.
        $currency = strtoupper($this->argument('currency'));

        // Check product status.
        if (Product::where('status', '=', 2)->where('code', '=', $this->productCode)->count() > 0) {
            $this->error('Product is under maintenance.');
            return;
        }

        // Check start/end datetime.
        $lockname = strtolower($this->productCode) . '_datapull_process_' . $currency;
        $currentPage = 1;
        $totalPages = 1;
        $xmlHeader = array(
            'Accept:text/xml,application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5',
            'Cache-Control:max-age=0',
            'Connection:keep-alive',
            'Keep-Alive:300',
            'Accept-Charset:ISO-8859-1,utf-8;q=0.7,*;q=0.7',
            'Accept-Language:en-us,en;q=0.5',
            'Pragma:',
            'API:getBetHistory',
            'DataType:XML',
            'Content-Type:text/xml',
        );
		
        if ($this->option('startdate') == 'ytd') {
            $lockname .= 'ytd';
            $yesterdayDt = Carbon::now()->addDays(-1);//
            $startDate = $yesterdayDt->setTime(0, 0, 0)->format('Ymd\THis');
            $endDate = $yesterdayDt->setTime(23, 59, 59)->format('Ymd\THis');
        } else {
            $startDate = $this->option('startdate');
            $endDate = $this->option('enddate');

            if (strlen($startDate) < 1) {
                $startDate = Carbon::now()->format('Ymd\T000000');
            }

            if (strlen($endDate) < 1) {
                $endDate = Carbon::now()->format('Ymd\T235959');
            }
        }

        App::check_process($lockname);

        do {
            $postfields = "<GetBetHistoryRequest>" .
                "<serialNo>" . date('U').mt_rand(1111,9999) . "</serialNo>" .
                "<merchantCode>" . Config::get($currency.'.isn.merchant_code') . "</merchantCode>" .
                "<pageIndex>" . $currentPage . "</pageIndex>" .
                "<beginDate>" . $startDate . "</beginDate>" .
                "<endDate>" . $endDate . "</endDate>" .
                "</GetBetHistoryRequest>";

            $result = simplexml_load_string( $this->curlXML( $postfields , Config::get($currency.'.isn.api_url'), true, 10, '', $xmlHeader) );
            $prefix = Config::get($currency.'.isn.prefix');
            $prefixLength = strlen($prefix);

            if(isset($result->resultCount) && $result->resultCount > 0 && isset($result->list)) {
                foreach($result->list->BetInfo as $betKey => $betValue){

                    $username = trim((string) $betValue->acctId);

                    if (strpos($username, $prefix) !== 0) {
                        continue;
                    }

                    $username = substr_replace($username, '', 0, $prefixLength);

                    $dateTimeNow = Carbon::now()->toDateTimeString();

                    $data = array(
                        'ticketId'      => (int) $betValue->ticketId,
                        'acctId'        => $username,
                        'betDate'       => Carbon::parse((string) $betValue->betDate),
                        'drawId'        => (string) $betValue->drawId,
                        'betType'       => (string) $betValue->betType,
                        'currency'      => (string) $betValue->currency,
                        'cancelled'     => (bool) $betValue->cancelled,
                        'msg'           => (string) $betValue->msg,
                        'msgFrom'       => (string) $betValue->msgFrom,
                        'betIp'         => (string) $betValue->betIp,
                        'odds'          => (float) $betValue->odds,
                        'betAmount'     => (float) $betValue->betAmount,
                        'successAmount' => (float) $betValue->successAmount,
                        'payAmount'     => (float) $betValue->payAmount,
                        'wlAmount'      => (float) $betValue->wlAmount,
                        'win'           => (float) $betValue->win,
                        'processDate'   => Carbon::parse((string) $betValue->processDate),
                        'created_at'    => $dateTimeNow,
                        'updated_at'    => $dateTimeNow,
                    );

                    foreach ($data as $key => $value) {
                        $insertData[$key] = '"'.$value.'"';
                    }

                    if (DB::statement('call insert_isn('.implode(',',$insertData).')')) {
                        $this->InsertWager($data, $currency);
                    }
                }

                if ($totalPages <= 1) {
                    if (isset($result->pageCount) && $result->pageCount > 0) {
                        $totalPages = $result->pageCount;
                    }
                }

                Profitloss::updatePNL2($this->profitloss);
            }

            // Reset array to prevent memory overflow.
            $this->accounts = array();
            $this->profitloss = array();

            $currentPage++;
        } while ($currentPage < $totalPages);

        // End.
        App::unlock_process($lockname);

        $this->comment('DataPull done: ' . $this->productCode);
    }
	
	private function InsertWager( $isn, $currency ){

		if( !isset($this->accounts[$isn['acctId']]) )
			$this->accounts[$isn['acctId']] = Account::where( 'nickname' , '=' , $isn['acctId'] )->whereCrccode($currency)->first();

		if (!$this->accounts[$isn['acctId']]) {
		    // Ignore account that is not found.
		    return;
        }

		$prdObj = Cache::rememberForever('product_isn_obj', function(){
				return Product::where( 'code' , '=' , 'ISN' )->first();
		});

        $account = $this->accounts[$isn['acctId']];

		$data['accid'] 		 		= $account->id;
		$data['acccode'] 	 		= $account->code;
		$data['nickname'] 	 		= $account->nickname;
		$data['wbsid'] 		 		= $account->wbsid;
		$data['prdid'] 		 		= $prdObj->id;
		$data['matchid'] 	 		= 0;
		$data['gamename'] 	 		= 'lottery';
		$data['category'] 	 		= Product::getCategory('lottery');
		$data['refid'] 		 		= $isn['ticketId'];
		$data['crccode'] 	 		= $account->crccode;
		$data['crcrate'] 	 		= Currency::getCurrencyRate($data['crccode']);
		$data['datetime'] 	 		= $isn['betDate'];
		$data['ip'] 		 		= '';
		$data['payout'] 	 		= (float) $isn['wlAmount'];
		$data['profitloss']  		= $isn['payAmount'];

        if($isn['wlAmount'] < 0 ){
            $data['payout']             = 0;
            $data['profitloss']  		= $isn['payAmount'] -  $isn['betAmount'];
        }

		$data['accountdate'] 		= substr( $isn['processDate'], 0 , 10 );
		$data['stake']		 		= $isn['betAmount'];
		$data['status'] 			= Wager::STATUS_SETTLED;

		if( $data['stake'] == $data['payout'] )	    $data['result']  = Wager::RESULT_DRAW;
		else if( $data['stake'] > $data['payout'] ) $data['result']  = Wager::RESULT_LOSS;
		else if( $data['stake'] < $data['payout'] ) $data['result']  = Wager::RESULT_WIN;
		$data['validstake'] 		= $isn['betAmount'];
		$data['stakelocal'] 		= Currency::getLocalAmount($data['stake'], $data['crccode']);
		$data['payoutlocal']		= Currency::getLocalAmount($data['payout'], $data['crccode']);
		$data['profitlosslocal']	= Currency::getLocalAmount($data['profitloss'], $data['crccode']);
		$data['validstakelocal']	= Currency::getLocalAmount($data['validstake'], $data['crccode']);
		$data['iscalculated'] 	    = 0;
		$data['created']			= date("y-m-d H:i:s");
		$data['modified']			= date("y-m-d H:i:s");

		foreach( $data as $key => $value ){
			$insert_data[$key] = '"'. $value . '"';
		}

		if(DB::statement('call insert_wager('.implode(',',$insert_data).')'))
		{
			$this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['prdid']   = $data['prdid'];
			$this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['accobj']  = $this->accounts[$isn['acctId']];
			$this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['crcrate'] = $data['crcrate'];
		}
		

	}
    
    private function arrayToXml($array) {
        $xmlStr = '';
        
        foreach ($array as $key => $val) {
            if (is_array($val)) {
                $xmlStr .= '<' . $key . '>' . $this->arrayToXml($val) . '</' . $key . '>';
            } else {
                $xmlStr .= '<' . $key . '>' . $val . '</' . $key . '>';
            }
        }
        
        return $xmlStr;
    }

    private function curlXML( $postfields, $url , $log = true , $timeout = 30 , $method = '', $header = false) {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        if( isset($_SERVER['HTTP_USER_AGENT'] ))
        {
            curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
        }
        $response = curl_exec($ch);
        curl_close($ch);

        if($log)
            App::insert_api_log( array( 'request' => $url.'?'.$postfields , 'return' => $response , 'method' => $method ) );

        return $response;
    }

}