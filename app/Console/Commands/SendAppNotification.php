<?php

namespace App\Console\Commands;

use App\libraries\App;
use App\libraries\PushNotifications;
use App\Models\Account;
use Cache;
use Carbon\Carbon;
use Config;
use Crypt;
use DB;
use Exception;
use Hash;
use Illuminate\Console\Command;


class SendAppNotification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dev:sendAppNotification {title} {body}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send App Notification command';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
		
		if( $accounts = Account::where( 'gcmid' , '!=' , '' )->get() )
		{
			foreach( $accounts as $account )
			{
				
				$msg_payload = array (
						'mtitle'   		  => $this->argument('title'),
						'mbody'    		  => $this->argument('body'),
				);
				
				;
				
				$this->comment('send done: ' . PushNotifications::android($msg_payload, $account->gcmid));
			}
		}
		
    }
}
