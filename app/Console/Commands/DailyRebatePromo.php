<?php

namespace App\Console\Commands;

use App\libraries\App;
use App\Models\Adjustment;
use App\Models\Account;
use App\Models\Accountdetail;
use App\Models\Agent;
use App\Models\Bethog;
use App\Models\Betosg;
use App\Models\Cashledger;
use App\Models\Channel;
use App\Models\Configs;
use App\Models\Currency;
use App\Models\Product;
use App\Models\Profitloss;
use App\Models\Wager;
use App\Models\Website;
use App\Models\Dailyrebate;
use Cache;
use Carbon\Carbon;
use Config;
use Crypt;
use DB;
use Exception;
use Hash;
use Illuminate\Console\Command;
use Maatwebsite\Excel\Facades\Excel;

class DailyRebatePromo extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dev:dailyRebatePromo';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Development command';


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
		$yesterday = Carbon::yesterday()->toDateString();
		
		$datas = DB::select( DB::raw("SELECT accid,nickname,date,crccode,SUM(totalstake) AS dailytotalstake FROM profitloss WHERE crccode='CNY' AND category='2' AND date='".$yesterday."'  GROUP BY accid"));
		
		$DailyrebateObj = new Dailyrebate;
		$dataSet = array();
		
		foreach( $datas as $data )
		{
			if($data->dailytotalstake>='5000'&& $data->dailytotalstake<'50000'){
				$dataSet[] = [
				'accid'			=> $data->accid,
				'date'   		=> $data->date,
				'nickname'    	=> $data->nickname,
				'crccode'    	=> $data->crccode,
				'status'    	=> 0,
				'totalstake'    => $data->dailytotalstake,
				'amount'    	=> 18,
				];
			}else if($data->dailytotalstake>='50000'&& $data->dailytotalstake<'200000'){
				$dataSet[] = [
				'accid'			=> $data->accid,
				'date'   		=> $data->date,
				'nickname'    	=> $data->nickname,
				'crccode'    	=> $data->crccode,
				'status'    	=> 0,
				'totalstake'    => $data->dailytotalstake,
				'amount'    	=> 56,
				];
			}else if($data->dailytotalstake>='200000'&& $data->dailytotalstake<'500000'){
				$dataSet[] = [
				'accid'			=> $data->accid,
				'date'   		=> $data->date,
				'nickname'    	=> $data->nickname,
				'crccode'    	=> $data->crccode,
				'status'    	=> 0,
				'totalstake'    => $data->dailytotalstake,
				'amount'    	=> 144,
				];
			}else if($data->dailytotalstake>='500000'&& $data->dailytotalstake<'1000000'){
				$dataSet[] = [
				'accid'			=> $data->accid,
				'date'   		=> $data->date,
				'nickname'    	=> $data->nickname,
				'crccode'    	=> $data->crccode,
				'status'    	=> 0,
				'totalstake'    => $data->dailytotalstake,
				'amount'    	=> 252,
				];
			}else if($data->dailytotalstake>='1000000'&& $data->dailytotalstake<'3000000'){
				$dataSet[] = [
				'accid'			=> $data->accid,
				'date'   		=> $data->date,
				'nickname'    	=> $data->nickname,
				'crccode'    	=> $data->crccode,
				'status'    	=> 0,
				'totalstake'    => $data->dailytotalstake,
				'amount'    	=> 540,
				];
			}else if($data->dailytotalstake>='3000000'&& $data->dailytotalstake<'10000000'){
				$dataSet[] = [
				'accid'			=> $data->accid,
				'date'   		=> $data->date,
				'nickname'    	=> $data->nickname,
				'crccode'    	=> $data->crccode,
				'status'    	=> 0,
				'totalstake'    => $data->dailytotalstake,
				'amount'    	=> 1128,
				];
			}else if($data->dailytotalstake>='10000000'&& $data->dailytotalstake<'30000000'){
				$dataSet[] = [
				'accid'			=> $data->accid,
				'date'   		=> $data->date,
				'nickname'    	=> $data->nickname,
				'crccode'    	=> $data->crccode,
				'status'    	=> 0,
				'totalstake'    => $data->dailytotalstake,
				'amount'    	=> 2718,
				];
			}else if($data->dailytotalstake>='30000000'&& $data->dailytotalstake<'60000000'){
				$dataSet[] = [
				'accid'			=> $data->accid,
				'date'   		=> $data->date,
				'nickname'    	=> $data->nickname,
				'crccode'    	=> $data->crccode,
				'status'    	=> 0,
				'totalstake'    => $data->dailytotalstake,
				'amount'    	=> 8604,
				];
			}else if($data->dailytotalstake>='60000000'&& $data->dailytotalstake<'100000000'){
				$dataSet[] = [
				'accid'			=> $data->accid,
				'date'   		=> $data->date,
				'nickname'    	=> $data->nickname,
				'crccode'    	=> $data->crccode,
				'status'    	=> 0,
				'totalstake'    => $data->dailytotalstake,
				'amount'    	=> 24492,
				];
			}else if($data->dailytotalstake>='100000000'){
				$dataSet[] = [
				'accid'			=> $data->accid,
				'date'   		=> $data->date,
				'nickname'    	=> $data->nickname,
				'crccode'    	=> $data->crccode,
				'status'    	=> 0,
				'totalstake'    => $data->dailytotalstake,
				'amount'    	=> 83380,
				];
			}
		}
		dailyrebate::insert($dataSet);
										
        $this->comment('Done.');
    }
}
