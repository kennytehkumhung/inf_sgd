<?php namespace App\Console\Commands;

use App\libraries\App;
use App\Models\Account;
use App\Models\Currency;
use App\Models\Configs;
use App\Models\Product;
use App\Models\Profitloss;
use App\Models\Wager;
use Cache;
use Carbon\Carbon;
use Config;
use DB;
use Illuminate\Console\Command;
use App\Models\Betbbn;

class DataPullBbn_LC extends Command
{
    protected $signature = 'datapull:bbn_lc {currency} {--startdate=} {--enddate=}';
    protected $description = 'Pull member bet ticket';

    // Product configs.
    protected $productCode = 'BBN_LC';

    protected $accounts = array();
    protected $profitloss = array();

    public function handle()
    {
        // Check currency argument.
        $currency = strtoupper($this->argument('currency'));

        // Check product status.
        if (Product::where('status', '=', 2)->where('code', '=', $this->productCode)->count() > 0) {
            $this->error('Product is under maintenance.');
            return;
        }

        // Check start/end datetime.
        $lockname = strtolower($this->productCode) . '_datapull_process_' . $currency;
        $dt = Carbon::now();

        if ($this->option('startdate') == 'ytd') {
            $lockname .= 'ytd';
            $yesterdayDt = Carbon::now()->addDays(-1);
            $startDt = $yesterdayDt->setTime(0, 0, 0)->toDateTimeString();
            $endDt = $yesterdayDt->setTime(23, 59, 59)->toDateTimeString();
        } else {
            $startDt = $this->option('startdate');
            $endDt = $this->option('enddate');

            if (strlen($startDt) < 1) {
                 $startDt = Carbon::now()->toDateString();
                 //$startDt = Carbon::now()->addDays(-1)->toDateString();
                //$startDt = Carbon::parse($this->getLatestTransactionTime($currency))->addHours(-1)->toDateString();
            }
        }

        // Begin.
        App::check_process($lockname);

        $website = Config::get($currency . '.bbn.Website');
        $uppername = Config::get($currency . '.bbn.Uppername');
		$continue = true;
		$page = 1;
	
		while($continue){
			$A = substr(str_shuffle("abcdefghijklmnopqrstuvwxyz"), 0, 9);
			$B = MD5(Config::get($currency . '.bbn.Website').Config::get($currency . '.bbn.KeyB_BetRecord').date('Ymd',strtotime('-12 hours')));
			$C = substr(str_shuffle("abcdefghijklmnopqrstuvwxyz"), 0, 5);

        $data = array(
            'website' => Config::get($currency . '.bbn.Website'),
            'uppername' => Config::get($currency . '.bbn.Uppername'),
            'rounddate' => $startDt,//$startDt,
			'starttime' => '00:00:00',
			'endtime' => '23:59:59',
            'gamekind' => 3,//$gamekind,
			'page' => $page,
			'pagelimit' => 500,
			'key' => $A.$B.$C,
        );

		$postfields = json_encode($data);
		
		$result = json_decode($this->curl(Config::get($currency.'.bbn.BaseServiceURL').'/BetRecord',$postfields, true, 30 ));
		
                if( isset($result->result) && ($result->result == 'true')) {
                    $prefixLength = strlen(Config::get($currency.'.bbn.Prefix'));
                    $data = array();

                    foreach ($result->data as $bi) {
                        $dateTimeNow = Carbon::now();
                        $dateNow = Carbon::now()->toDateString();
                        $dateTimeNowStr = $dateTimeNow->toDateTimeString();
						
                        $data = array(
                            'AccName' => (String) $bi->UserName,
                            'UserName' => substr((String) $bi->UserName, $prefixLength),
                            'WagersID' =>  $bi->WagersID,
							'WagerDate' =>  (String)$bi->WagersDate,
							'SerialID' => $bi->SerialID,
							'RoundNo' => (String)$bi->RoundNo,
							'GameType' => (int)$bi->GameType,
							'WagerDetail' => (String)$bi->WagerDetail,
							'GameCode' => (int)$bi->GameCode,
							'Result' => (String)$bi->Result,
							'ResultType' => (String)$bi->ResultType,
							'Card' => (String)$bi->Card,
							'BetAmount' => (int)$bi->BetAmount,
							'Origin' => (String)$bi->Origin,
							'Payoff' => (float)$bi->Payoff,
							'Currency' => (String)$bi->Currency,
							'ExchangeRate' => (float)$bi->ExchangeRate,
							'Commissionable' => (int)$bi->Commissionable,
                            'created_at' => $dateTimeNowStr,
                            'updated_at' => $dateTimeNowStr,
                        );
						
                        foreach ($data as $key => $value) {
                            $insertData[$key] = '"' . $value . '"';
                        }

                            if (DB::statement('call insert_bbn(' . implode(',' , $insertData) . ')')) {
                                // Get new inserted ID.
                                $this->InsertWager($data,$currency);
                            }
                    
				}
				
				if(isset($result->pagination)){
					if($result->pagination->Page == $result->pagination->TotalPage){
						$continue = false;
					}else{
						$page = $result->pagination->Page + 1;
					}
				}else{
					$continue = false;
				}
				
                Profitloss::updatePNL2($this->profitloss);
          
                // Reset array to prevent memory overflow.
                $this->accounts = array();
                $this->profitloss = array();
                }

		}
        // End.
        App::unlock_process($lockname);
		$this->comment('DataPull done: ' . $this->productCode);
    //}
}
    private function InsertWager( $bbn, $currency )
    {
        if( !isset($this->accounts[$bbn['UserName']]) )
            $this->accounts[$bbn['UserName']] = Account::where( 'nickname' , '=' , $bbn['UserName'] )->whereCrccode($currency)->first();
		
        $prdObj = Cache::rememberForever('product_bbn_obj', function(){
            return Product::where( 'code' , '=' , 'BBN' )->first();
        });
		
        $account = $this->accounts[$bbn['UserName']];
        $data['accid'] 		 		= $account->id;
        $data['acccode'] 	 		= $account->code;
        $data['nickname'] 	 		= $account->nickname;
        $data['wbsid'] 		 		= $account->wbsid;
        $data['prdid'] 		 		= $prdObj->id;
        $data['matchid'] 	 		= 0;
        $data['gamename'] 	 		= $bbn['GameType'];
		if(substr($bbn['GameType'],0,1) == '3'){	
			$data['category'] = Product::getCategory('Casino');
		}elseif(substr($bbn['GameType'],0,1) == '5'){
			$data['category'] = Product::getCategory('egames');
		}elseif($bbn['GameType'] == 'LT' || $bbn['GameType'] == 'D3' || $bbn['GameType'] == 'P3' || $bbn['GameType'] == 'BT' || $bbn['GameType'] == 'T3' || $bbn['GameType'] == 'CQ' || $bbn['GameType'] == 'JX' || $bbn['GameType'] == 'TJ' || $bbn['GameType'] == 'GXSF' || $bbn['GameType'] == 'GDSF' || $bbn['GameType'] == 'TJSF' || $bbn['GameType'] == 'BJKN' || $bbn['GameType'] == 'CAKN' || $bbn['GameType'] == 'AUKN' || $bbn['GameType'] == 'BBKN' || $bbn['GameType'] == 'BJPK'){
			$data['category'] = Product::getCategory('lottery');
		}elseif($bbn['GameType'] == 'BK' || $bbn['GameType'] == 'BS' || $bbn['GameType'] == 'F1' || $bbn['GameType'] == 'FB' || $bbn['GameType'] == 'FT' || $bbn['GameType'] == 'FU' || $bbn['GameType'] == 'IH' || $bbn['GameType'] == 'SP' || $bbn['GameType'] == 'TN'){
			$data['category'] = Product::getCategory('Sports');
		}else{
			$data['category'] = Product::getCategory('number');
		}
        $data['refid'] 		 		= $bbn['SerialID'];
        $data['crccode'] 	 		= $account->crccode;
        $data['crcrate'] 	 		= Currency::getCurrencyRate($data['crccode']);
        $data['datetime'] 	 		= $bbn['WagerDate'];
        $data['ip'] 		 		= '';
        $data['payout'] 	 		= ((float)$bbn['BetAmount']) + ((float) $bbn['Payoff']);
        $data['profitloss']  		= $bbn['Payoff'];
        $data['accountdate'] 		= substr( $bbn['WagerDate'], 0 , 10 );
        $data['stake']		 		= ((float)$bbn['BetAmount']);
        $data['status'] 			= Wager::STATUS_SETTLED;
		
        if( $data['stake'] == $data['payout'] )	    $data['result']  = Wager::RESULT_DRAW;
        else if( $data['stake'] > $data['payout'] ) $data['result']  = Wager::RESULT_LOSS;
        else if( $data['stake'] < $data['payout'] ) $data['result']  = Wager::RESULT_WIN;
        $data['validstake'] 		= $bbn['BetAmount'];
        $data['stakelocal'] 		= Currency::getLocalAmount($data['stake'], $data['crccode']);
        $data['payoutlocal']		= Currency::getLocalAmount($data['payout'], $data['crccode']);
        $data['profitlosslocal']	= Currency::getLocalAmount($data['profitloss'], $data['crccode']);
        $data['validstakelocal']	= Currency::getLocalAmount($data['validstake'], $data['crccode']);
        $data['iscalculated'] 	    = 0;
        $data['created']			= date("y-m-d H:i:s");
        $data['modified']			= date("y-m-d H:i:s");

        $insert_data = array();
	
        foreach( $data as $key => $value ){
            $insert_data[$key] = '"'. $value . '"';
        }

        if(DB::statement('call insert_wager('.implode(',',$insert_data).')'))
        {
            $this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['prdid']   = $data['prdid'];
            $this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['accobj']  = $this->accounts[$bbn['UserName']];
            $this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['crcrate'] = $data['crcrate'];
        }

    }

    private function getLatestTransactionTime($currency)
    {
		if ($currency == 'CNY') {
			$currency = 'RMB';
		}
		
        $date = Betbbn::where( 'currency' , '=' , $currency )->orderBy('WagerDate','Desc')->value('WagerDate');

        if(!$date)

            $date = '2018-01-01 00:00:00';

        return $date;

    }
	
	function curl($url , $postFields , $log = true, $timeout = 30 , $method = ''){
		$headers[] = "Content-Type:application/json";
		$data_json = $postFields;
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json','Content-Length: ' . strlen($data_json)));
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($ch, CURLOPT_POSTFIELDS,$data_json);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$response  = curl_exec($ch);
		curl_close($ch);
        
        if($log)
            App::insert_api_log( array( 'request' => $url.'?'.$data_json , 'return' => $response , 'method' => $method ) );

        return $response;
    }
}
