<?php

namespace App\Console\Commands;

use App\libraries\App;
use App\Models\Adjustment;
use App\Models\Account;
use App\Models\Accountdetail;
use App\Models\Agent;
use App\Models\Bethog;
use App\Models\Betosg;
use App\Models\Cashledger;
use App\Models\Channel;
use App\Models\Configs;
use App\Models\Currency;
use App\Models\Product;
use App\Models\Profitloss;
use App\Models\Wager;
use App\Models\Website;
use Cache;
use Carbon\Carbon;
use Config;
use Crypt;
use DB;
use Exception;
use Hash;
use Illuminate\Console\Command;
use Maatwebsite\Excel\Facades\Excel;

class UpdateMemberBalance extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dev:updateMemberBalance';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Development command';

	protected $accounts = array();
	
	protected $profitloss = array();

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
		$datas = DB::select( DB::raw("select c.crccode,c.acccode,d.code,c.balance from cashbalance c,product d where c.prdid = d.id and c.balance > 1 order by balance desc"));
		
		foreach( $datas as $data )
		{
			 $className = "App\libraries\providers\\".$data->code;
			 $obj = new $className;
			 $obj->getBalance( $data->acccode, $data->crccode);
			 
			 $this->comment($data->acccode.' - '.$data->balance);
			
		}
										
        $this->comment('Done.');
    }
}
