<?php namespace App\Console\Commands;

use App\libraries\App;
use App\Models\Account;
use App\Models\Configs;
use App\Models\Currency;
use App\Models\Product;
use App\Models\Profitloss;
use App\Models\Wager;
use Cache;
use Carbon\Carbon;
use Config;
use DB;
use Illuminate\Console\Command;

class DataPullCtb extends Command
{
    protected $signature = 'datapull:ctb {currency} {--startdate= : Example: ytd|2017-01-31 00:00:00}';
    protected $description = 'Pull member bet ticket';

    // Product configs.
    protected $productCode = 'CTB';

    protected $accounts = array();
    protected $profitloss = array();

    public function handle()
    {
        // Check currency argument.
        $currency = strtoupper($this->argument('currency'));

        // Check product status.
        if (Product::where('status', '=', 2)->where('code', '=', $this->productCode)->count() > 0) {
            $this->error('Product is under maintenance.');
            return;
        }

        // Check start/end datetime.
        $lockname = strtolower($this->productCode) . '_datapull_process_'.$currency;
        $updatePullDateParam = false;
		
        if ($this->option('startdate') == 'ytd') {
            $lockname .= 'ytd';
            $yesterdayDt = Carbon::now()->addDays(-1);
            $dtFrom = $yesterdayDt->setTime(0, 0, 0)->toDateTimeString();
        } else {
            $dtFrom = $this->option('startdate');

            if (strlen($dtFrom) < 1) {
				$dtFrom = Configs::getParam('SYSTEM_CTB_PULLDATE_' . $currency);
				$updatePullDateParam = true;

				if (strlen($dtFrom) < 1) {
					$dtFrom = '2016-12-30 10:00:00';
				}
            }
        }

        App::check_process($lockname);

        $dtFromStr = Carbon::parse($dtFrom)->getTimestamp() * 1000;

        $tkCode = Config::get($currency . '.ctb.tk_code');
        $parentUserId = strtolower(Config::get($currency . '.ctb.parent_user_id'));
        $apiUrl = Config::get($currency . '.ctb.api_url') . 'website/gettxnsbylastupdatetimebyparentuserid?';

		// $status_type[1]  = 'ACTIVE';
		// $status_type[2]  = 'SCR';
		// $status_type[4]  = 'CANCEL';
		// $status_type[8]  = 'SETTLE';
		// $status_type[16] = 'RACE VOID';

        /*
         * Only the value of different group can be combined.
         * BitOr 9: 1 (active) + 8 (settle)
         * BitOr 11: 1 (active) + 2 (scratched) + 8 (settle)
         * BitOr 13: 1 (active) + 4 (cancel) + 8 (settle)
         */
        $acceptedStatus = array(8, 9);
        $postfields = 'tkCode=' . $tkCode .
            '&parentUserID=' . $parentUserId .
            '&lastUpdateDate=' . $dtFromStr;

        $result = json_decode(App::curlGET($apiUrl . $postfields, true, 30, ''));

        if (isset($result->status) && $result->status == 200 && isset($result->transactionList)) {
            foreach ($result->transactionList as $biKey => $biValue) {
                $dateTimeNow = Carbon::now();
                $dateTimeNowStr = $dateTimeNow->toDateTimeString();

                $transactionDate = $biValue->transactionDate;
                $updateDate = $biValue->updateDate;

                if (is_numeric($transactionDate)) {
                    $transactionDate = Carbon::createFromTimestamp(((int) $transactionDate / 1000));
                } else {
                    $transactionDate = Carbon::parse($transactionDate);
                }

                if (is_numeric($updateDate)) {
                    $updateDate = Carbon::createFromTimestamp(((int) $updateDate / 1000));
                } else {
                    $updateDate = Carbon::parse($updateDate);
                }

                $data = array(
                    'refID'             => (int) $biValue->id,
                    'userID'            => (string) $biValue->userID,
                    'raceID'            => (int) $biValue->raceID,
                    'raceType'          => (string) $biValue->raceType,
                    'raceDate'          => (string) $biValue->raceDate,
                    'raceNo'            => (int) $biValue->raceNo,
                    'runnerNo'          => (int) $biValue->runnerNo,
                    'currencyName'      => (string) $biValue->currencyName,
                    'status'            => (string) $biValue->status,
                    'limit'             => (string) $biValue->limit,
                    'payout'            => (float) $biValue->payout,
                    'transactionDate'   => $transactionDate->toDateTimeString(),
                    'updateDate'        => $updateDate->toDateTimeString(),
                    'place'             => (float) $biValue->place, // Member bet on 'place' (1st, 2nd, 3rd...)
                    'win'               => (float) $biValue->win, // Member bet on 'win' (1st only)
                    'created_at'        => $dateTimeNowStr,
                    'updated_at'        => $dateTimeNowStr,
                );

                foreach ($data as $key => $value) {
                    $insertData[$key] = '"' . $value . '"';
                }

                if (DB::statement('call insert_ctb(' . implode(',', $insertData) . ')')) {
                    if (in_array($data['status'], $acceptedStatus)) {
                        // Get new inserted ID.
                        $this->InsertWager($data, $data['currencyName']);
                    }
                }
            }
        }

        Profitloss::updatePNL2($this->profitloss);

        if ($updatePullDateParam) {
            Configs::updateParam('SYSTEM_CTB_PULLDATE_' . $currency, Carbon::now()->toDateTimeString());
        }

        // End.
        App::unlock_process($lockname);

        $this->comment('DataPull done: ' . $this->productCode);
    }

	private function InsertWager( $bet, $currency )
    {

        if (!isset($this->accounts[$bet['userID']]))
            $this->accounts[$bet['userID']] = Account::where('nickname', '=', $bet['userID'])->where('crccode', '=', $currency)->first();

        $prdObj = Cache::rememberForever('product_ctb_obj', function () {
            return Product::where('code', '=', 'CTB')->first();
        });

        $account = $this->accounts[$bet['userID']];

        $data['accid'] = $account->id;
        $data['acccode'] = $account->code;
        $data['nickname'] = $account->nickname;
        $data['wbsid'] = $account->wbsid;
        $data['prdid'] = $prdObj->id;
        $data['matchid'] = $bet['raceID'];
        $data['gamename'] = 'Racebook';
        $data['category'] = Product::getCategory('racebook');
        $data['refid'] = $bet['refID'];
        $data['crccode'] = $account->crccode;
        $data['crcrate'] = Currency::getCurrencyRate($data['crccode']);
        $data['datetime'] = $bet['transactionDate'];
        $data['ip'] = '';
        $data['payout'] = $bet['payout'] + $bet['place'] + $bet['win'];
        $data['profitloss'] = $bet['payout'];
        $data['accountdate'] = $bet['raceDate'];
        $data['stake'] = $bet['place'] + $bet['win'];
        $data['status'] = Wager::STATUS_SETTLED;

        if ($data['stake'] == $data['payout']) $data['result'] = Wager::RESULT_DRAW;
        else if ($data['stake'] > $data['payout']) $data['result'] = Wager::RESULT_LOSS;
        else if ($data['stake'] < $data['payout']) $data['result'] = Wager::RESULT_WIN;
        $data['validstake'] = $bet['place'] + $bet['win'];
        $data['stakelocal'] = Currency::getLocalAmount($data['stake'], $data['crccode']);
        $data['payoutlocal'] = Currency::getLocalAmount($data['payout'], $data['crccode']);
        $data['profitlosslocal'] = Currency::getLocalAmount($data['profitloss'], $data['crccode']);
        $data['validstakelocal'] = Currency::getLocalAmount($data['validstake'], $data['crccode']);
        $data['iscalculated'] = 0;
        $data['created'] = date("y-m-d H:i:s");
        $data['modified'] = date("y-m-d H:i:s");

        foreach ($data as $key => $value) {
            $insert_data[$key] = '"' . $value . '"';
        }

        if (DB::statement('call insert_wager(' . implode(',', $insert_data) . ')')) {
            $this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['prdid'] = $data['prdid'];
            $this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['accobj'] = $this->accounts[$bet['userID']];
            $this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['crcrate'] = $data['crcrate'];
        }
    }

}
