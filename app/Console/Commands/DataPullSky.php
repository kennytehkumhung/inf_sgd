<?php namespace App\Console\Commands;

use App\libraries\App;
use App\libraries\providers\SKY;
use App\Models\Account;
use App\Models\Betsky;
use App\Models\Currency;
use App\Models\Product;
use App\Models\Profitloss;
use App\Models\Wager;
use Cache;
use Carbon\Carbon;
use Config;
use DB;
use Illuminate\Console\Command;

class DataPullSky extends Command
{
    protected $signature = 'datapull:sky {currency} {--startdate= : Example: ytd|2017-01-31 00:00:00} {--enddate= : Example: 2017-01-31 23:59:59} {--userid= : Example: 12345|12345,67890...} {--lockname= : Example: 1|2|3...} {--dumpsummary= : Example: y|1}';
    protected $description = 'Pull member bet ticket';

    // Product configs.
    protected $productCode = 'SKY';

    protected $accounts = array();
    protected $profitloss = array();

    public function handle()
    {
        // Check currency argument.
        $currency = strtoupper($this->argument('currency'));

        // Check product status.
        if (Product::where('status', '=', 2)->where('code', '=', $this->productCode)->count() > 0) {
            $this->error('Product is under maintenance.');
            return;
        }

        // Check start/end datetime.
        $lockname = strtolower($this->productCode) . '_datapull_process_'.$currency.$this->option('lockname');

        if ($this->option('startdate') == 'ytd') {
            $lockname .= 'ytd';
            $yesterdayDt = Carbon::now()->addDays(-1);
            $startDt = $yesterdayDt->setTime(0, 0, 0)->toDateTimeString();
            $endDt = $yesterdayDt->setTime(23, 59, 59)->toDateTimeString();
        } else {
            $startDt = $this->option('startdate');
            $endDt = $this->option('enddate');

            if (!$endDt) {
                $endDt = Carbon::now()->toDateTimeString();
            }
        }

        // Begin.
        App::check_process($lockname);

        $userIds = $this->option('userid');

        if (strlen($userIds) > 0) {
            // Search specified user IDs only.
            $builder = DB::table('account as acc')->whereIn('acc.id', explode(',', $userIds));
        } else {
            // Get member user ID from summery report.
            $summary = $this->reportSummary($currency, $startDt, $endDt);

            if (in_array($this->option('dumpsummary'), array('y', '1'))) {
                $this->comment(var_export($summary, true));
                App::unlock_process($lockname);
                return;
            }

            $agid = Config::get($currency.'.sky.agid');
            $userIds = array();

            foreach ($summary as $index => $array) {
                foreach ($array as $key => $val) {
                    if ($key == 'username') {
                        $userIds[] = (int) str_replace($agid, '', $val);
                        break; // Username found, break current loop and proceed to next $array.
                    }
                }
            }

            $builder = DB::table('account as acc')->whereIn('acc.id', $userIds);
        }

        $skyObj = new SKY();

        $builder->select(array('acc.id as userid', 'acc.nickname'))
            ->chunk(300, function ($users) use ($currency, $skyObj, $startDt, $endDt) {
                $hasData = false;

                foreach ($users as $user) {
                    $hasData = true;

                    if (is_null($startDt)) {
                        $betObj = Betsky::where('username', '=', $user->nickname)
                            ->orderBy('bet_time', 'desc')
                            ->first();

                        // API only allow pull max 3 days.
                        $startdate = null;

                        if ($betObj) {
                            // Have record in bet table, get last fetched bet time as start date.
                            $startdate = Carbon::parse($betObj->bet_time);

                            if ($startdate->diffInHours() > 72) {
                                // Last bet time already over max allowed pull date.
                                $startdate = Carbon::now()->addHours(-2);
                            }
                        }

                        if (is_null($startdate)) {
                            // Default will be start pulling from 3 days early.
                            $startdate = Carbon::now()->addHours(-1);
                        }
                    } else {
                        $startdate = Carbon::parse($startDt);
                    }

                    if (is_null($endDt)) {
                        $enddate = Carbon::now();
                    } else {
                        $enddate = Carbon::parse($endDt);
                    }

                    $continue = false;
                    $minTemp = 0;

                    do {
                        $continue = false;

                        // API only allow pull ticket of max 10 minutes.
                        if ($startdate->diffInMinutes($enddate) > 10) {
                            $startdateStr = $startdate->addMinutes($minTemp)->toDateTimeString();
                            $enddateStr = $startdate->copy()->addMinutes(10)->toDateTimeString();
                            $minTemp = 10;
                            $continue = true;
                        } else {
                            $startdateStr = $startdate->toDateTimeString();
                            $enddateStr = $enddate->toDateTimeString();
                        }


                        $data = array(
                            'agid' => Config::get($currency.'.sky.agid'),
                            'username' => $skyObj->formatUserId($user->userid),
                            'start_date' => $startdateStr,
                            'end_date' => $enddateStr,
                        );

                        $postfields = http_build_query($skyObj->Signature_Genarate($data, Config::get($currency.'.sky.key')));

                        $result = json_decode($this->curlGET(Config::get($currency.'.sky.url').'api/wsv1_0/user_game_history?'.$postfields, false, 10 , 'datapull:'.$this->productCode));

                        if( isset($result->status) && !isset($result->error_code) && $result->status == 'OK' ) {
                            $currencyRate = ($currency == 'IDR' ? 1000 : 1);

                            foreach ($result ->list as $key => $val) {
                                if (is_numeric($key)) {
                                    $dateTimeNow = Carbon::now()->toDateTimeString();

                                    $data = array(
                                        'ref_id'         => date('U').mt_rand(11111,99999),
                                        'user_id'       => (string) $val->username,
                                        'username'      => (string) $user->nickname,
                                        'currency'      => $currency,
                                        'match_id'      => (string) $val->matchid,
                                        'game_type'     => (string) $val->game_type,
                                        'game_code'     => (string) $val->game_code,
                                        'bet_time'      => (string) $val->bet_time,
                                        'bet'           => ((float) $val->bet) / $currencyRate,
                                        'win'           => ((float) $val->win) / $currencyRate,
                                        'winlose'       => ((float) $val->winlose) / $currencyRate,
                                        'jackpot'       => ((float) $val->jackpot) / $currencyRate,
                                        'bet_ip'        => (string) $val->bet_ip,
                                        'created_at'    => $dateTimeNow,
                                        'updated_at'    => $dateTimeNow,
                                    );

                                    foreach ($data as $key => $value) {
                                        $insertData[$key] = '"'.$value.'"';
                                    }

                                    if (DB::statement('call insert_sky('.implode(',',$insertData).')')) {
                                        $betskyObj = Betsky::where('username', '=', $data['username'])
                                            ->where('match_id', '=', $data['match_id'])
                                            ->first(['ref_id']);

                                        if ($betskyObj) {
                                            // Get ref id of inserted record.
                                            $data['ref_id'] = $betskyObj->ref_id;
                                        }

                                        $this->InsertWager($data, $currency);
										    if ($hasData) {
													Profitloss::updatePNL2($this->profitloss);
													$this->accounts = array();
													$this->profitloss = array();
											}
                                    }
                                }
                            }
                        }

                    } while ($continue);

                }

          
            });

        // End.
        App::unlock_process($lockname);

        $this->comment('DataPull done: ' . $this->productCode);
    }

    private function InsertWager( $sky, $currency )
    {
        if( !isset($this->accounts[$sky['username']]) )
            $this->accounts[$sky['username']] = Account::where( 'nickname' , '=' , $sky['username'] )->whereCrccode($currency)->first();

        $prdObj = Cache::rememberForever('product_sky_obj', function(){
            return Product::where( 'code' , '=' , 'SKY' )->first();
        });

        $account = $this->accounts[$sky['username']];

        $data['accid'] 		 		= $account->id;
        $data['acccode'] 	 		= $account->code;
        $data['nickname'] 	 		= $account->nickname;
        $data['wbsid'] 		 		= $account->wbsid;
        $data['prdid'] 		 		= $prdObj->id;
        $data['matchid'] 	 		= 0;
        $data['gamename'] 	 		= 'slotgame';
        $data['category'] 	 		= Product::getCategory('egames');
        $data['refid'] 		 		= $sky['ref_id'];
        $data['crccode'] 	 		= $account->crccode;
        $data['crcrate'] 	 		= Currency::getCurrencyRate($data['crccode']);
        $data['datetime'] 	 		= $sky['bet_time'];
        $data['ip'] 		 		= '';
        $data['payout'] 	 		= (float) ($sky['bet'] + $sky['winlose'] + $sky['jackpot']);
        $data['profitloss']  		= $sky['winlose'] + $sky['jackpot'];
        $data['accountdate'] 		= substr( $sky['bet_time'], 0 , 10 );
        $data['stake']		 		= $sky['bet'];
        $data['status'] 			= Wager::STATUS_SETTLED;

        if( $data['stake'] == $data['payout'] )	    $data['result']  = Wager::RESULT_DRAW;
        else if( $data['stake'] > $data['payout'] ) $data['result']  = Wager::RESULT_LOSS;
        else if( $data['stake'] < $data['payout'] ) $data['result']  = Wager::RESULT_WIN;
        $data['validstake'] 		= $sky['bet'];
        $data['stakelocal'] 		= Currency::getLocalAmount($data['stake'], $data['crccode']);
        $data['payoutlocal']		= Currency::getLocalAmount($data['payout'], $data['crccode']);
        $data['profitlosslocal']	= Currency::getLocalAmount($data['profitloss'], $data['crccode']);
        $data['validstakelocal']	= Currency::getLocalAmount($data['validstake'], $data['crccode']);
        $data['iscalculated'] 	    = 0;
        $data['created']			= date("y-m-d H:i:s");
        $data['modified']			= date("y-m-d H:i:s");

        $insert_data = array();

        foreach( $data as $key => $value ){
            $insert_data[$key] = '"'. $value . '"';
        }

        if(DB::statement('call insert_wager('.implode(',',$insert_data).')'))
        {
            $this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['prdid']   = $data['prdid'];
            $this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['accobj']  = $this->accounts[$sky['username']];
            $this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['crcrate'] = $data['crcrate'];
        }


    }

    private function reportSummary($currency, $startDt, $endDt)
    {

        $skyObj = new SKY();
        $agurl = Config::get($currency . '.sky.url') . 'api/wsv1_0/user_report_summary?';
        $agid = Config::get($currency . '.sky.agid');
        $agkey = Config::get($currency . '.sky.key');
        $summary = array();

        if (is_null($startDt)) {
            $startDt = Carbon::now()->addHours(-1)->toDateTimeString();
        }

        if (is_null($endDt)) {
            $endDt = Carbon::now()->toDateTimeString();
        }

        $data = array(
            'agid' => $agid,
            'start_date' => $startDt,
            'end_date' => $endDt,
        );

        $postfields = http_build_query($skyObj->Signature_Genarate($data, $agkey));

        $result = json_decode($this->curlGET($agurl . $postfields, true, 10 , 'reportSummary'));

        if( isset($result->status) && !isset($result->error_code) && $result->status == 'OK' ) {
            $summary = $result->{'0'};
        }

        return $summary;
    }

    private function curlGET($url , $log = true, $timeout = 30 , $method = '')
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, str_replace(' ' , '%20', $url));
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $response = curl_exec($ch);
        curl_close($ch);

        if($log) {
            App::insert_api_log(array('request' => $url, 'return' => $response, 'method' => $method));
        }

        return $response;
    }
}
