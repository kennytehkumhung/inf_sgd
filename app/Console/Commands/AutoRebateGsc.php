<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 16/10/2017
 * Time: 5:40 PM
 */

namespace App\Console\Commands;

use App\Models\Cashledger;
use App\Models\Incentive;
use App\Models\Incentivesetting;
use App\Models\Product;
use Carbon\Carbon;
use DB;
use Illuminate\Console\Command;

class AutoRebateGsc extends Command
{
    protected $signature = 'autorebate:gsc {--startdate= : Example: 2017-10-16} {--enddate= : Example: 2017-10-17} {--userid= : Example: 12345|12345,67890...} {--prdcode= : Product Code: AGG|AGG,SKY...} {--lockname= : Example: 1|2|3...}';
    protected $description = 'Calculate auto rebate';

    public function handle()
    {
        // Check start/end datetime.
        $lockname = 'autorebate_gsc_process'.$this->option('lockname');

        $startDt = $this->option('startdate');
        $endDt = $this->option('enddate');

        if (!$startDt) {
            $startDt = Carbon::now()->toDateString();
        }

        if (!$endDt) {
            $endDt = Carbon::now()->toDateString();
        }

        // Begin.
//        App::check_process($lockname);

        // TODO
        $incentivesettingObj = Incentivesetting::where('auto', '=', 1)
            ->where('status', '=', CBO_STANDARDSTATUS_ACTIVE)
            ->where('auto', '=', 1)
            ->where('datefrom', '<=', Carbon::now())
            ->where('dateto', '>=', Carbon::now())
            ->get();

        foreach ($incentivesettingObj as $icsObj) {

            $builder = DB::table('profitloss as a')
                ->join('account as b', 'b.id', '=', 'a.accid')
                ->whereBetween('a.date', array($startDt, $endDt));

            if (strlen($this->option('prdcode')) > 0) {
                $prdObj = Product::whereIn('code', explode(',', $this->option('prdcode')))->get(array('id'));
                $prdIds = array();

                foreach ($prdObj as $r) {
                    $prdIds[] = $r->id;
                }

                if (count($prdIds) > 0) {
                    $builder->whereIn('a.prdid', $prdIds);
                }

                unset($prdObj);
            }

            if (strlen($this->option('userid')) > 0) {
                $builder->whereIn('a.accid', explode(',', $this->option('userid')));
            }

            $builder->groupBy(array('a.date', 'a.prdid', 'a.accid'));
            $builder->select(array('a.wbsid', 'a.date', 'a.prdid', 'a.accid', 'a.acccode', 'a.nickname', 'b.fullname'));
            $builder->selectRaw('SUM(a.validstake) AS totalvalidstake');

            $builder->chunk(500, function ($rows) use ($icsObj) {
                foreach ($rows as $r) {
                    $incentiveObj = Incentive::where('accid', '=', $r->accid)
                        ->where('prdid', '=', $r->prdid)
                        ->where('datefrom', '=', $r->date)
                        ->where('dateto', '=', $r->date)
                        ->first();

                    $rebateStake = ((float) $r->totalvalidstake) * $icsObj->rate / 100;

                    if ($incentiveObj) {
                        // Update existing.
                        $incentiveObj->totalstake = $rebateStake;
                        $incentiveObj->totalstakelocal = $rebateStake;
                        $incentiveObj->amount = $rebateStake;
                        $incentiveObj->amountlocal = $rebateStake;
                        $incentiveObj->save();

                        $cashledgerObj = Cashledger::find($incentiveObj->clgid);

                        if ($cashledgerObj) {
                            $cashledgerObj->amount = $rebateStake;
                            $cashledgerObj->amountlocal = $rebateStake;
                            $cashledgerObj->save();
                        } else {
                            Cashledger::forceCreate(array(
                                'wbsid' => $r->wbsid,
                                'accid' => $r->accid,
                                'acccode' => $r->acccode,
                                'accname' => $r->fullname,
                                'chtcode' => CBO_CHARTCODE_INCENTIVE,
                                'remarkcode' => '',
                                'amount' => $rebateStake,
                                'amountlocal' => $rebateStake,
                                'fee' => 0,
                                'feelocal' => 0,
                                'cashbalance' => 0,
                                'crccode' => 'CNY',
                                'crcrate' => 1,
                                'fdmid' => 0,
                                'bnkid' => 0,
                                'accbnkid' => 0,
                                'bacid' => 0,
                                'refobj' => 'IncentiveObject',
                                'refid' => $incentiveObj->id,
                                'createdip' => '127.0.0.1',
                                'createdpanel' => 3,
                                'iscalculated' => 0,
                                'createdby' => 0,
                                'modifiedby' => 0,
                                'status' => CBO_LEDGERSTATUS_MANCONFIRMED,
                                'created' => Carbon::now(),
                                'modified' => Carbon::now(),
                            ));
                        }
                    } else {
                        // Create new.
                        $incentiveObj = Incentive::forceCreate(array(
                            'type' => 0,
                            'insid' => $icsObj->id, // incentive setting id
                            'product' => $icsObj->product, // product category
                            'prdid' => $r->prdid, // product id
                            'clgid' => 0,
                            'accid' => $r->accid,
                            'acccode' => $r->acccode,
                            'crccode' => 'CNY',
                            'crcrate' => 1,
                            'rate' => 1,
                            'amount' => $rebateStake,
                            'amountlocal' => $rebateStake,
                            'totalstake' => $rebateStake,
                            'totalstakelocal' => $rebateStake,
                            'datefrom' => $r->date,
                            'dateto' => $r->date,
                            'createdby' => 0,
                            'modifiedby' => 0,
                            'status' => 0,
                            'created' => Carbon::now(),
                            'modified' => Carbon::now(),
                        ));

                        $casLedgerObj = Cashledger::forceCreate(array(
                            'wbsid' => $r->wbsid,
                            'accid' => $r->accid,
                            'acccode' => $r->acccode,
                            'accname' => $r->fullname,
                            'chtcode' => CBO_CHARTCODE_INCENTIVE,
                            'remarkcode' => '',
                            'amount' => $rebateStake,
                            'amountlocal' => $rebateStake,
                            'fee' => 0,
                            'feelocal' => 0,
                            'cashbalance' => 0,
                            'crccode' => 'CNY',
                            'crcrate' => 1,
                            'fdmid' => 0,
                            'bnkid' => 0,
                            'accbnkid' => 0,
                            'bacid' => 0,
                            'refobj' => 'IncentiveObject',
                            'refid' => $incentiveObj->id,
                            'createdip' => '127.0.0.1',
                            'createdpanel' => 3,
                            'iscalculated' => 0,
                            'createdby' => 0,
                            'modifiedby' => 0,
                            'status' => CBO_LEDGERSTATUS_MANCONFIRMED,
                            'created' => Carbon::now(),
                            'modified' => Carbon::now(),
                        ));

                        $incentiveObj->clgid = $casLedgerObj->id;
                        $incentiveObj->save();
                    }
                }
            });
        }

        // End.
//        App::unlock_process($lockname);

        $this->comment('Auto rebate done: GSC');
    }
}