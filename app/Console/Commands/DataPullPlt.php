<?php namespace App\Console\Commands;

use App\libraries\App;
use App\Models\Betplt;
use App\Models\Account;
use App\Models\Product;
use App\Models\Currency;
use App\Models\Profitloss;
use App\Models\Wager;
use App\Models\Configs;
use Cache;
use Carbon\Carbon;
use Config;
use DB;
use Illuminate\Console\Command;

class DataPullPlt extends Command
{
    protected $signature = 'datapull:plt {currency} {--startdate= : Example: ytd|2017-01-31 00:00:00} {--enddate= : Example: 2017-01-31 23:59:59} {--page= : Example: 1} {--hour= : Example: 10}';
    protected $description = 'Pull member bet ticket';

    // Product configs.
    protected $productCode = 'PLT';

    protected $accounts = array();
    protected $profitloss = array();

    public function handle()
    {
        $currency = strtoupper($this->argument('currency'));

        // Check product status.
        if (Product::where('status', '=', 2)->where('code', '=', $this->productCode)->count() > 0) {
            $this->error('Product is under maintenance.');
            return;
        }

        // Check start/end datetime.
        $lockname = strtolower($this->productCode) . '_datapull_process_'.$currency;
        $configKey = 'SYSTEM_PLT_PULLDATE_' . $currency;
		$page	  = $this->option('page') ? $this->page : 1;
		$hour	  = $this->option('hour') ? $this->hour : 1;
		$perpage  = 5000;
		$stardate = $this->option('startdate') ? $this->option('startdate') : Configs::getParam($configKey);
		$todate   = date( "Y-m-d H:i:s", strtotime(Configs::getParam($configKey))+ $hour * 1800 );
		
		if($stardate > date( "Y-m-d H:i:s")){
			$stardate = date( "Y-m-d H:i:s", strtotime(date( "Y-m-d H:i:s"))- $hour * 900 );
			$todate   = date( "Y-m-d H:i:s", strtotime($stardate)+ $hour * 900 );
		}
		
		$todate = $this->option('enddate') ? $this->option('enddate') : $todate;

        // Begin.
        App::check_process($lockname);
		
		$continue = true;
		
		while($continue)
		{
			$parameterStr  = "customreport/getdata";
			$parameterStr .= "/reportname/PlayerGames";
			$parameterStr .= "/startdate/" . $stardate;
			$parameterStr .= "/enddate/" . $todate;
			$parameterStr .= "/frozen/" . "all";
			$parameterStr .= "/showinfo/1";
			$parameterStr .= "/page/" . $page;
			$parameterStr .= "/perPage/" . $perpage;
			
			$results = json_decode($this->CallPltFunction( $parameterStr , $currency));


			$this->comment('DataPull process: ' . $stardate.' - '.$todate);
			
	 		if( isset($results->result) )
			{
				$this->comment('Pulling');
				
				foreach( $results->result as $key => $result ){

					$temp = explode('_',$result->PLAYERNAME);
					
					$data['GAMECODE']         = $result->GAMECODE;
					$data['Username']         = $temp[1];
					$data['PLAYERNAME']       = $result->PLAYERNAME;
					$data['WINDOWCODE']       = $result->WINDOWCODE;
					$data['GAMEID']           = $result->GAMEID;
					$data['GAMETYPE']         = $result->GAMETYPE;
					$data['GAMENAME']         = $result->GAMENAME;
					$data['SESSIONID']        = $result->SESSIONID;
					$data['BET']      	      = ($currency == 'IDR')? $result->BET /1000:$result->BET;
					$data['WIN']      	      = ($currency == 'IDR')? $result->WIN /1000:$result->WIN;
					$data['PROGRESSIVEBET']   = $result->PROGRESSIVEBET;
					$data['PROGRESSIVEWIN']   = $result->PROGRESSIVEWIN;
					$data['BALANCE']   		  = ($currency == 'IDR')? $result->BALANCE /1000:$result->BALANCE;
					$data['CURRENTBET']   	  = $result->CURRENTBET;
					$data['GAMEDATE']   	  = $result->GAMEDATE;
					$data['LIVENETWORK']   	  = $result->LIVENETWORK;
					$data['RNUM']   		  = $result->RNUM;
					$data['created']		  = date("y-m-d H:i:s");
					$data['modified']		  = date("y-m-d H:i:s");
					$data['currency']		  = $currency;
					
					foreach( $data as $key => $value ){
						$insert_data[$key] = '"'. $value . '"';
					}					

					if(DB::statement('call insert_plt('.implode(',',$insert_data).')')){
						
							$this->InsertWager($data, $currency);
						
					}
					
				}
				
				if( $gamedate = Betplt::where( 'currency' , '=' , $currency )->orderBy('GAMEDATE','Desc')->pluck('GAMEDATE') ){
				
					Configs::updateParam( $configKey , $gamedate );
					
				}
				else
				{
					Configs::updateParam( $configKey , $todate );
				}
				
				Profitloss::updatePNL2($this->profitloss);
				
			}else{
				if( date( "Y-m-d H:i:s") > $todate )
				{
						Configs::updateParam( $configKey , $todate );
				}
			} 
			
			if( isset($results->pagination->currentPage) )
			{
				
				
				if( $results->pagination->currentPage >= $results->pagination->totalPages )
				{
					$continue = false;
				}
				else
				{
					$page = $results->pagination->currentPage + 1;
				}
			}
			else{
				$continue = false;
			}
			
			if( $continue == false )
			{
				if( date("Y-m-d H:i:s") > $todate ){
					$stardate  = $todate;
					$todate    = date( "Y-m-d H:i:s", strtotime($todate)+ $hour * 1800 );
					$continue = true;
				}else{
					$continue = false;
				}
			}
			
		}
		
		App::unlock_process($lockname);

        $this->comment('DataPull done: ' . $this->productCode);
    }

	protected function CallPltFunction( $params , $currency)
	{

		$header   = array();
		$header[] = "Accept:text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8"; 
		$header[] = "Cache-Control: max-age=0"; 
		$header[] = "Connection: keep-alive"; 
		$header[] = "Keep-Alive:timeout=15, max=100"; 
		$header[] = "Accept-Charset:ISO-8859-1,utf-8;q=0.7,*;q=0.3"; 
		$header[] = "Accept-Language:es-ES,es;q=0.8"; 
		$header[] = "Pragma: "; 
		$header[] = "X_ENTITY_KEY: " . Config::get($currency.'.plt.entitykey'); 
		
		$default_url = 'https://kioskpublicapi.redhorse88.com/';
		
	 	if($currency == 'CNY')
			$default_url = 'https://kioskpublicapi.grandmandarin88.com/';
 
	    $tuCurl = curl_init(); 
	    curl_setopt($tuCurl, CURLOPT_URL, $default_url.str_replace(' ', '%20', $params)); 
	    curl_setopt($tuCurl, CURLOPT_PORT , 443); 
	    curl_setopt($tuCurl, CURLOPT_VERBOSE, 0); 
		curl_setopt($tuCurl, CURLOPT_HTTPHEADER, $header);
	    curl_setopt($tuCurl, CURLOPT_HEADER, 0); 
	    curl_setopt($tuCurl, CURLOPT_SSL_VERIFYPEER, 0);
	    curl_setopt($tuCurl, CURLOPT_SSL_VERIFYHOST, 0);     
		curl_setopt($tuCurl, CURLOPT_TIMEOUT, 15);
	    curl_setopt($tuCurl, CURLOPT_SSLCERT, str_replace('public', 'config', Config::get('setting.server_path')).'cert/'.$currency.'.pem' );
		curl_setopt($tuCurl, CURLOPT_SSLKEY,  str_replace('public', 'config', Config::get('setting.server_path')).'cert/'.$currency.'.key' );	    
		curl_setopt($tuCurl, CURLOPT_RETURNTRANSFER, 1); 
		$result = curl_exec($tuCurl);
		
		return $result;

	}
   

   public static function CallFunction( $params , $currency)
	{
	
		$header   = array();
		$header[] = "Accept:text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8"; 
		$header[] = "Cache-Control: max-age=0"; 
		$header[] = "Connection: keep-alive"; 
		$header[] = "Keep-Alive:timeout=30, max=100"; 
		$header[] = "Accept-Charset:ISO-8859-1,utf-8;q=0.7,*;q=0.3"; 
		$header[] = "Accept-Language:es-ES,es;q=0.8"; 
		$header[] = "operatorid: ".Config::get($currency.'.plt.operatorid'); 
		$header[] = "privatekey: ".Config::get($currency.'.plt.privatekey'); 
		$header[] = "currency: ".$currency; 

	    $tuCurl = curl_init(); 
	    curl_setopt($tuCurl, CURLOPT_URL, Config::get($currency.'.plt.proxy_url') ); 
		curl_setopt($tuCurl, CURLOPT_HTTPHEADER, $header);
		curl_setopt($tuCurl, CURLOPT_POST, true);
		curl_setopt($tuCurl, CURLOPT_TIMEOUT, 30);
		curl_setopt($tuCurl, CURLOPT_POSTFIELDS, 'currency='.$currency.'&entitykey='.Config::get($currency.'.plt.entitykey').'&param='.str_replace(' ', '%20', $params) );
	    curl_setopt($tuCurl, CURLOPT_HEADER, 0);      
		curl_setopt($tuCurl, CURLOPT_RETURNTRANSFER, 1); 

		$result = '';
		
	    if(!curl_errno($tuCurl))
		{ 
			$result    = curl_exec($tuCurl);
	    } 

		curl_close($tuCurl);
		
		return $result; 
	}
	
	private function InsertWager( $bet , $currency){
		
		if( !isset($this->accounts[$bet['Username']]) )
			$this->accounts[$bet['Username']] = Account::where( 'nickname' , '=' , $bet['Username'] )->whereCrccode($currency)->first();
		
		$prdObj = Cache::rememberForever('product_plt_obj', function(){
				return Product::where( 'code' , '=' , 'PLT' )->first();
		});
		
		if(!isset($this->accounts[$bet['Username']]->id))
		exit;	
		
		$data['accid'] 		 		= $this->accounts[$bet['Username']]->id;
		$data['acccode'] 	 		= $this->accounts[$bet['Username']]->code;
		$data['nickname'] 	 		= $bet['Username'];
		$data['wbsid'] 		 		= $this->accounts[$bet['Username']]->wbsid;
		$data['prdid'] 		 		= $prdObj->id;
		$data['matchid'] 	 		= $bet['GAMEID'];
		$data['gamename'] 	 		= $bet['GAMETYPE'];
		$data['category'] 	 		= $bet['GAMETYPE'] == 'Live Games' ? Product::getCategory('Casino') : Product::getCategory('egames');
		$data['refid'] 		 		= $bet['GAMECODE'];
		$data['crccode'] 	 		= $this->accounts[$bet['Username']]->crccode;
		$data['crcrate'] 	 		= Currency::getCurrencyRate($data['crccode']);
		$data['datetime'] 	 		= $bet['GAMEDATE'];
		$data['ip'] 		 		= '';
		if(  $bet['BET'] == 0 )	
			$data['payout'] = 0;
		else
			$data['payout'] 	 	= $bet['WIN'];
		$data['profitloss']  		= $bet['WIN'] - $bet['BET'];
		$data['accountdate'] 		= substr( $bet['GAMEDATE'], 0 , 10 );
		$data['stake']		 		= $bet['BET'];
		$data['status'] 			= Wager::STATUS_SETTLED;
		
		if( $data['stake'] == $data['payout'] )	    $data['result']  = Wager::RESULT_DRAW;
		else if( $data['stake'] > $data['payout'] ) $data['result']  = Wager::RESULT_LOSS;
		else if( $data['stake'] < $data['payout'] ) $data['result']  = Wager::RESULT_WIN;
		$data['validstake'] 		= $bet['BET'];

        if ($data['result'] == Wager::RESULT_DRAW) {
            $data['validstake'] = 0;
        }

		$data['stakelocal'] 		= Currency::getLocalAmount($data['stake'], $data['crccode']);
		$data['payoutlocal']		= Currency::getLocalAmount($data['payout'], $data['crccode']);
		$data['profitlosslocal']	= Currency::getLocalAmount($data['profitloss'], $data['crccode']);
		$data['validstakelocal']	= Currency::getLocalAmount($data['validstake'], $data['crccode']);
		$data['iscalculated'] 	    = 0;
		$data['created']			= date("y-m-d H:i:s");
		$data['modified']			= date("y-m-d H:i:s");
		
		foreach( $data as $key => $value ){
			$insert_data[$key] = '"'. $value . '"';
		}
		

		if(DB::statement('call insert_wager('.implode(',',$insert_data).')'))
		{
			$this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['prdid']   = $data['prdid'];
			$this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['accobj']  = $this->accounts[$bet['Username']];
			$this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['crcrate'] = $data['crcrate'];
		}
		
		
	}
}
