<?php namespace App\Console\Commands;

use App\libraries\App;
use App\Models\Account;
use App\Models\Betspg;
use App\Models\Configs;
use App\Models\Currency;
use App\Models\Product;
use App\Models\Profitloss;
use App\Models\Wager;
use Cache;
use Carbon\Carbon;
use Config;
use DB;
use Illuminate\Console\Command;

class DataPullSpg extends Command
{
    protected $signature = 'datapull:spg {currency} {--startdate= : Example: ytd|2017-01-31 00:00:00} {--enddate= : Example: 2017-01-31 23:59:59}';
    protected $description = 'Pull member bet ticket';

    // Product configs.
    protected $productCode = 'SPG';

    protected $accounts = array();
    protected $profitloss = array();

    public function handle()
    {
        // Check currency argument.
        $currency = strtoupper($this->argument('currency'));

        // Check product status.
        if (Product::where('status', '=', 2)->where('code', '=', $this->productCode)->count() > 0) {
            $this->error('Product is under maintenance.');
            return;
        }

        // Check start/end datetime.
        $lockname = strtolower($this->productCode) . '_datapull_process_'.$currency;
        $dt = Carbon::now();
        $updatePullDateParam = false;

        if ($this->option('startdate') == 'ytd') {
            $lockname .= 'ytd';
            $yesterdayDt = Carbon::now()->addDays(-1);
            $dtFrom = $yesterdayDt->setTime(0, 0, 0)->toDateTimeString();
            $dtTo = $yesterdayDt->setTime(23, 59, 59)->toDateTimeString();
        } else {
            $dtFrom = $this->option('startdate');
            $dtTo = $this->option('enddate');

            if (strlen($dtFrom) < 1) {
                $dtFrom = Configs::getParam('SYSTEM_SPG_PULLDATE_' . $currency);
                $updatePullDateParam = true;

                if (strlen($dtFrom) < 1) {
                    $dtFrom = $this->getLatestTransactionTime($currency);
                }
            }
        }

        // Begin.
        App::check_process($lockname);

        if (is_null($dtTo)) {
            $dtTo = $dt->copy();
        }

        $dtFrom = Carbon::parse($dtFrom);
        $dtTo = Carbon::parse($dtTo);

        $header = [
            'Accept: text/xml,application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5',
            'Cache-Control: max-age=0',
            'Connection: keep-alive',
            'Keep-Alive: 300',
            'Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7',
            'Accept-Language: en-us,en;q=0.5',
            'Pragma: ',
            'API: getBetHistory',
            'DataType: XML',
            'Content-Type: text/xml',
        ];
        $merchantCode = Config::get($currency.'.spg.merchant_id');
        $apiUrl = Config::get($currency.'.spg.api_url');
        $prefix = Config::get($currency.'.spg.prefix');
        $dtFormat = 'Ymd\THis';
        $minTemp = 0;
        $continue = false;

        do {
            $continue = false;

            if( $dtTo->diffInMinutes($dtFrom) > 30 ){
                // End datetime is over 30 mins, use time interval of +30 mins instead.
                $start = $dtFrom->addMinutes($minTemp)->format($dtFormat);
                $end = $dtFrom->copy()->addMinutes(30)->format($dtFormat);
                $minTemp = 30;
                $continue = true;
            } else {
                $start = $dtFrom->format($dtFormat);
                $end = $dtTo->format($dtFormat);
            }

            $random = App::generateNum(6);
            $serialNo = $dtFrom.$random;
            $currentPage = 1;
            $totalPages = 0;

            do {
                $return  = '<serialNo>'.$serialNo.'</serialNo>';
                $return .= '<merchantCode>'.$merchantCode.'</merchantCode>';
                $return .= '<beginDate>'.$start.'</beginDate>';
                $return .= '<endDate>'.$end.'</endDate>';
                $return .= '<pageIndex>'.$currentPage.'</pageIndex>';

                $postfields = '<?xml version="1.0" encoding="UTF-8"?>
                                       <GetBetHistoryRequest>
                                            '.$return.'
                                       </GetBetHistoryRequest>';

                $result = simplexml_load_string( App::curlXML( $postfields , $apiUrl, true, 10 , 'datapull:'.$this->productCode, $header) );

                if (isset($result->code) && $result->code == '0') {
                    foreach($result->list->BetInfo as $key => $value) {
                        $data['ticketId']       = (int)$value->ticketId;
                        $data['user_id']        = (string) str_replace($prefix, '', $value->acctId);
                        $data['categoryId'] 	= (string) $value->categoryId;
                        $data['gameCode']       = (string) $value->gameCode;
                        $data['ticketTime'] 	= date('Y-m-d H:i:s',strtotime(str_replace('T', '', $value->ticketTime)));
                        $data['betIp']          = (string) $value->betIp;
                        $data['currency'] 		= (string) $value->currency;
                        $data['betAmount'] 		= $value->betAmount;
                        $data['winLoss'] 		= $value->winLoss;
                        $data['jackpotAmount'] 	= $value->jackpotAmount;
                        $data['result']         = (string) $value->result;
                        $data['completed'] 		= (string) $value->completed;
                        $data['luckyDrawId'] 	= (string) $value->luckyDrawId;
                        $data['roundId'] 		= (string) $value->roundId;
                        $data['sequence'] 		= (string) $value->sequence;
                        $data['channel'] 		= (string) $value->channel;
                        $data['balance'] 		= (string) $value->balance;
                        $data['created']		= date("y-m-d H:i:s");
                        $data['modified']		= date("y-m-d H:i:s");

                        foreach( $data as $key => $value ){
                            $insert_data[$key] = '"'. $value . '"';
                        }

                        if(DB::statement('call insert_spg('.implode(',',$insert_data).')')){
                            $this->InsertWager($data, $currency);
                        }
                    }

                    if ($totalPages <= 0) {
                        if (isset($result->pageCount) && $result->pageCount > 0) {
                            $totalPages = $result->pageCount;
                        }
                    }
                }

                Profitloss::updatePNL2($this->profitloss);

                // Reset array to prevent memory overflow.
                $this->accounts = array();
                $this->profitloss = array();

                $currentPage++;
            } while ($currentPage <= $totalPages);
        } while ( $continue );

        if ($updatePullDateParam) {
            Configs::updateParam('SYSTEM_SPG_PULLDATE_'.$currency, $dtTo->toDateTimeString());
        }

        // End.
        App::unlock_process($lockname);

        $this->comment('DataPull done: ' . $this->productCode);
    }

    private function InsertWager( $bet , $currency){

        if( !isset($this->accounts[$bet['user_id']]) )
            $this->accounts[$bet['user_id']] = Account::where( 'nickname' , '=' , $bet['user_id'] )->whereCrccode($currency)->first();

        $prdObj = Cache::rememberForever('product_spg_obj', function(){
            return Product::where( 'code' , '=' , 'SPG' )->first();
        });

        $data['accid'] 		 		= $this->accounts[$bet['user_id']]->id;
        $data['acccode'] 	 		= $this->accounts[$bet['user_id']]->code;
        $data['nickname'] 	 		= $bet['user_id'];
        $data['wbsid'] 		 		= $this->accounts[$bet['user_id']]->wbsid;
        $data['prdid'] 		 		= $prdObj->id;
        $data['matchid'] 	 		= 0;
        $data['gamename'] 	 		= $bet['gameCode'];
        $data['category'] 	 		= Product::getCategory('egames');
        $data['refid'] 		 		= $bet['ticketId'];
        $data['crccode'] 	 		= $this->accounts[$bet['user_id']]->crccode;
        $data['crcrate'] 	 		= Currency::getCurrencyRate($data['crccode']);
        $data['datetime'] 	 		= $bet['ticketTime'];
        $data['ip'] 		 		= $bet['betIp'];
        $data['payout'] 	 		= $bet['betAmount'] - $bet['winLoss'];
        $data['profitloss']                     = $bet['winLoss'];

        $data['accountdate']                    = substr( $bet['ticketTime'], 0 , 10 );
        $data['stake']		 		= $bet['betAmount'];
        $data['status'] 			= Wager::STATUS_SETTLED;


        if( $data['stake'] == $data['payout'] )	    $data['result']  = Wager::RESULT_DRAW;
        else if( $data['stake'] > $data['payout'] ) $data['result']  = Wager::RESULT_LOSS;
        else if( $data['stake'] < $data['payout'] ) $data['result']  = Wager::RESULT_WIN;
        $data['validstake'] 		= $bet['betAmount'];
        $data['stakelocal'] 		= Currency::getLocalAmount($data['stake'], $data['crccode']);
        $data['payoutlocal']		= Currency::getLocalAmount($data['payout'], $data['crccode']);
        $data['profitlosslocal']	= Currency::getLocalAmount($data['profitloss'], $data['crccode']);
        $data['validstakelocal']	= Currency::getLocalAmount($data['validstake'], $data['crccode']);
        $data['iscalculated'] 	    = 0;
        $data['created']			= date("y-m-d H:i:s");
        $data['modified']			= date("y-m-d H:i:s");

        foreach( $data as $key => $value ){
            $insert_data[$key] = '"'. $value . '"';
        }


        if(DB::statement('call insert_wager('.implode(',',$insert_data).')'))
        {
            $this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['prdid']   = $data['prdid'];
            $this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['accobj']  = $this->accounts[$bet['user_id']];
            $this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['crcrate'] = $data['crcrate'];
        }

    }

    private function getLatestTransactionTime( $currency )
    {
        $date = Betspg::where( 'currency' , '=' , $currency )->orderBy('ticketTime','desc')->pluck('ticketTime');

        if(!$date) {
            $date = '2017-06-20 00:00:00';
        }

        return $date;
    }
}
