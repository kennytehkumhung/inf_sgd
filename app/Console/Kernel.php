<?php

namespace App\Console;

use App\Models\Errorlog;
use App\Models\SchedulerSetting;
use Carbon\Carbon;
use DB;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \App\Console\Commands\AutoRebateGsc::class,
        \App\Console\Commands\DevTest::class,
        \App\Console\Commands\UpdateMemberBalance::class,
        \App\Console\Commands\SendAppNotification::class,
        \App\Console\Commands\UpdateAffiliateReport::class,
        \App\Console\Commands\MigrateProduct::class,
        \App\Console\Commands\Inspire::class,
        \App\Console\Commands\ReleaseLockFile::class,
        \App\Console\Commands\PaymentGatewayCheckTrans::class,
        \App\Console\Commands\DailyRebatePromo::class,

        // Datapull.
        \App\Console\Commands\DataPullCtb::class,
        \App\Console\Commands\DataPullFbl::class,
        \App\Console\Commands\DataPullHog::class,
        \App\Console\Commands\DataPullIlt::class,
        \App\Console\Commands\DataPullIsn::class,
        \App\Console\Commands\DataPullJok::class,
        \App\Console\Commands\DataPullM8b::class,
        \App\Console\Commands\DataPullMnt::class,
        \App\Console\Commands\DataPullMxb::class,
        \App\Console\Commands\DataPullOsg::class,
        \App\Console\Commands\DataPullPlt::class,
        \App\Console\Commands\DataPullPltb::class,
        \App\Console\Commands\DataPullPsb::class,
        \App\Console\Commands\DataPullSky::class,
        \App\Console\Commands\DataPullSpg::class,
        \App\Console\Commands\DataPullUc8::class,
        \App\Console\Commands\DataPullVgs::class,
        \App\Console\Commands\DataPullScr::class,
		\App\Console\Commands\DataPullMig::class,
        \App\Console\Commands\DataPullBbn::class,
        \App\Console\Commands\DataPullBbn_BYDR::class,
        \App\Console\Commands\DataPullBbn_BYDS::class,
        \App\Console\Commands\DataPullBbn_CP::class,
        \App\Console\Commands\DataPullBbn_LC::class,
		\App\Console\Commands\DataPullS128::class,
        \App\Console\Commands\DataPullWmc::class,
	\App\Console\Commands\DataPullTbs::class,	
        \App\Console\Commands\DataPullEvo::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        try {
            // Load scheduler settings from DB.
            $datapullSettings = DB::table('scheduler_setting')
                ->where('status', '=', 1)
                ->get(array('name', 'type', 'execute', 'options', 'cron'));

            foreach ($datapullSettings as $r) {
//                \Log::info('Scheduler Run: ' . $r->name);

                $options = json_decode($r->options, true);

                if (!is_array($options)) {
                    $options = array();
                }

                if ($r->type == SchedulerSetting::TYPE_COMMAND) {
                    $schedule->command($r->execute, $options)->name($r->name)->cron($r->cron);
                } elseif ($r->type == SchedulerSetting::TYPE_EXEC) {
                    $schedule->exec($r->execute, $options)->name($r->name)->cron($r->cron);
                }
            }
        } catch (\Exception $ex) {
            Errorlog::forceCreate(array(
                'error' => 'Scheduler Error: ' . $ex->getMessage(),
                'updated_at' => Carbon::now(),
                'created_at' => Carbon::now(),
            ));
        }
    }
}
