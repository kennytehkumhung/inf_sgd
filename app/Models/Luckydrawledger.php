<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Luckydrawledger extends Model {

	protected $table = 'luckydraw_ledger';


	function getBalance( $accid , $drawno ){
		
		return Luckydrawledger::where( 'accid' , '=' , $accid )->where( 'draw_no' , '=' , $drawno )->sum('amount');
	}	
	
	function debit_coin( $accid , $drawno , $acccode){
		
		$luckydraw = new self;
		$luckydraw->draw_no = $drawno;
		$luckydraw->accid   = $accid;
		$luckydraw->acccode = $acccode;
		$luckydraw->amount  = -1;
		$luckydraw->save();
		
	}
}
