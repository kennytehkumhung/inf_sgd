<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable;  
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;  
use Kbwebs\MultiAuth\PasswordResets\CanResetPassword;
use Kbwebs\MultiAuth\PasswordResets\Contracts\CanResetPassword as CanResetPasswordContract;
use App\Models\Roleusermap;
use App\Models\Role;
use App\Models\Usergroupmap;
use App\Models\Usergroup;
use DB;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract{ 

	use Authenticatable, CanResetPassword;  

	protected $table = 'user';
	 
	const CREATED_AT = 'created';

    const UPDATED_AT = 'modified';
	
	public function getRoleArrayByUserId($id = 0) {
		if($id == 0) $id = $this->id;
		if($id <= 0) return false;
		$roles = array();
		if($maps = Roleusermap::whereRaw('usrid = '.$id)->get()) {
			foreach($maps as $map) {
				if($roleObj = Role::find($map->rolid)) {
					$roles[$roleObj->id] = $roleObj->name;
				}
			}
		}
		if (count($roles) > 0) return $roles;
		else return false;
	}


	public function getGroupArrayByUserId($id = 0) {
		if($id == 0) $id = $this->id;
		if($id <= 0) return false;
		$groups = array();
		if($maps = Usergroupmap::whereRaw('usrid = '.$id)->get()) {
			foreach($maps as $map) {
				if($groupObj = Usergroup::find($map->ugrid)) {
					$groups[$groupObj->id] = $groupObj->name;
				}
			}
		}
		if (count($groups) > 0) return $groups;
		else return false;
	}
	
	public function updateRole($roles = array()) {

		DB::statement('delete from roleusermap where usrid = '.$this->id.' ');
		if(count($roles) > 0) {
			foreach($roles as $role) {
				$roleUserMapObj = new Roleusermap;
				$roleUserMapObj->rolid = $role;
				$roleUserMapObj->usrid = $this->id;
				$roleUserMapObj->save();
			}
		}

		return true;
	}

}
