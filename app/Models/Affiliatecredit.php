<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Affiliatecredit extends Model {

	protected $table = 'affiliatecredit';
	 
	const CREATED_AT = 'created';

    const UPDATED_AT = 'modified';
	
	static function getBalance($aflid , $default = 0 , $date = '' ) {
		$date = $date == '' ? date('Y-m-d H:i:s') : $date;
		if( $default == 0 )
		{
			$affiliatecredit = Affiliatecredit::whereUserid($aflid)->whereType(0)->where( 'created' , '<=' , $date )->sum('amount');
			$cashcard 		 = Cashcard::whereAflid($aflid)->where( 'created' , '<=' , $date )->sum('amount');
		}
		elseif( $default == 1 )
		{
			$affiliatecredit = Affiliatecredit::whereUserid($aflid)->whereType(1)->sum('amount');
			$cashcard 		 = Withdrawcard::whereAflid($aflid)->sum('amount');
		}
		
		return $affiliatecredit - $cashcard ;
		
	}
	

}
