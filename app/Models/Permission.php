<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model {

	protected $table = 'permission';
	 
	const CREATED_AT = 'created';

    const UPDATED_AT = 'modified';
	
	function hasRight($userid, $module, $task) {
		$roleObj = new Role;
		$roles = $roleObj->getRolesByUserId($userid);
		foreach ($roles as $role) {
			$permissionObj = new self;
			if($permissionObj = self::whereRaw('rolid = '.$role->id.' AND mdlid = '.$module.' AND taskid = '.$task)->first()) {
				return true;
			}
		}
		
		return false;
	}

}
