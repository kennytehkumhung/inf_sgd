<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Websiteurl extends Model {

	protected $table = 'websiteurl';
	 
	const CREATED_AT = 'created';

    const UPDATED_AT = 'modified';

}
