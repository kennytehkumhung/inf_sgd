<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Lang;

class SchedulerSetting extends Model {

    protected $table = 'scheduler_setting';

    const TYPE_COMMAND = 'command';
    const TYPE_EXEC = 'exec';

    const STATUS_DISABLED = 0;
    const STATUS_ENABLED = 1;

    public static function getCategoryOptions($prependOptionAll = false) {

        $options = [];

        if ($prependOptionAll) {
            $options[''] = Lang::get('COMMON.ALL');
        }

        $options[self::TYPE_COMMAND] = 'command';
        $options[self::TYPE_EXEC] = 'exec';

        return $options;
    }

    public static function getStatusOptions($prependOptionAll = false) {

        $options = [];

        if ($prependOptionAll) {
            $options[''] = Lang::get('COMMON.ALL');
        }

        $options[self::STATUS_DISABLED] = 'Disabled';
        $options[self::STATUS_ENABLED] = 'Enabled';

        return $options;
    }
}
