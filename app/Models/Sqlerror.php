<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sqlerror extends Model {

	protected $table = 'sqlerror';
	 
	const CREATED_AT = 'created';

    const UPDATED_AT = 'modified';

}
