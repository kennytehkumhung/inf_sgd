<?php namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class FortuneWheelAcc extends Model {

    protected $table = 'fortune_wheel_acc';

    public static function ensureExists($accid, $acccode, $probabilitySet = 3) {

        if (!FortuneWheelAcc::where('accid', '=', $accid)->first()) {
            FortuneWheelAcc::forceCreate([
                'accid' => $accid,
                'acccode' => $acccode,
                'probability_set' => $probabilitySet,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]);
        }
    }
}
