<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Worldcupteam extends Model {

	protected $table = 'worldcup_team';
	 
	const CREATED_AT = 'created';

    const UPDATED_AT = 'modified';

    public static function getCategoryText($category = null){
        if($category == null) return false;
        
        $category_name = self::whereRaw('category_id = '.$category)->pluck('category_name');
        
        return $category_name;
    }

    public static function getTeamText($category = null, $team = null){
        if($team == null || $category == null) return false;
        
        $team_name = self::whereRaw('category_id = '.$category.' AND team_id = '.$team)->pluck('team_name');
        
        return $team_name;
    }

    public static function getCategoryOptions() {

        $categories=array();
        $categoryNames = self::select('*')->get();
        foreach($categoryNames as $categoryName) {
            $categories[$categoryName->category_id]=$categoryName->category_name;
        }


        return $categories;
    }
    public static function getTeamNameOptions() {

        $Teams=array();
        $categoryNames = self::select('*')->get();
        foreach($categoryNames as $categoryName) {
                $Teams[$categoryName->category_id][$categoryName->team_id] = $categoryName->team_name;
        }
        return $Teams;
    }

    public static function getGroupOptions() {

        $Group=array();
        $groupNames = self::select('*')->whereCategory_id('8')->get();
        foreach($groupNames as $groupName) {
            $Group[$groupName->group][$groupName->team_id] = $groupName->team_name;
        }
        return $Group;
    }
    public static function getGroupName($teamID) {

        $groupNames = self::select('*')->whereCategory_id('8')->whereTeam_id($teamID)->value('group');

        return $groupNames;
    }
    public static function getAmount($catagoryID,$teamID) {

        if(($catagoryID==1)||($catagoryID==2)){
            $amount=600;
        }else if(($catagoryID==3)||($catagoryID==4)||($catagoryID==5)){
            $amount=200;
        }else if(($catagoryID==6)||($catagoryID==7)||($catagoryID==8)){
            $amount=400;
        }
        if($catagoryID!=8){
            $teamNames = self::whereTeam_id($teamID)->where('category_id','!=','8')->value('team_name');
        }else if($catagoryID==8){
            $teamNames = self::whereTeam_id($teamID)->where('category_id','=','8')->value('team_name');
        }

        if(($teamNames=="Argentina")||($teamNames=="Brazil")||($teamNames=="France")||($teamNames=="Germany")||($teamNames=="Spain")){
            $amount=$amount/2;
        }

        return $amount;
    }
}
