<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Objectlog extends Model {

	protected $table = 'objectlog';
	 
	const CREATED_AT = 'created';

    const UPDATED_AT = 'modified';
	
	function getSqlModeText(){
		if($this->id == 0) return false;
		if ($this->sqlmode == CBO_OBJECTLOG_INSERT)
		return Lang::get('COMMON.ADD');
		elseif ($this->sqlmode == CBO_OBJECTLOG_UPDATE)
		return Lang::get('COMMON.UPDATE');
		elseif ($this->sqlmode == CBO_OBJECTLOG_DELETE)
		return Lang::get('COMMON.DELETE');
	}

	/**
	* Return the current object user type color code.
	*
	*
	* @return string The object user type color code
	*/
	function getTypeColorCode(){
		if($this->id == 0) return false;

		if($this->usertype == CBO_LOGINTYPE_USER)
		return '#CC99FF';
		else if($this->usertype == CBO_LOGINTYPE_ADMIN)
		return '#CCFFCC';
		else if($this->usertype == CBO_LOGINTYPE_AFFILIATE)
		return '#CCCCFF';
	}

	/**
	* Return the current object type in text.
	*
	* @param integer $status	The status code
	*
	* @return string The object type in language text
	*/
	function getTypeText(){
		if($this->id == 0) return false;

		if ($this->usertype == CBO_LOGINTYPE_USER)
		return Lang::get('COMMON.MEMBER');
		elseif ($this->usertype == CBO_LOGINTYPE_ADMIN)
		return Lang::get('COMMON.ADMINUSER');
		elseif ($this->usertype == CBO_LOGINTYPE_AFFILIATE)
		return Lang::get('COMMON.AFFILIATE');
	}

	/**
	* Return an array of object type
	*
	*
	* @return array Array of object type key and value in text
	*/
	function getTypeOptions(){
		$type = array(
			CBO_LOGINTYPE_USER => Lang::get('COMMON.MEMBER'),
			CBO_LOGINTYPE_ADMIN => Lang::get('COMMON.ADMINUSER'),
			CBO_LOGINTYPE_AFFILIATE => Lang::get('COMMON.AFFILIATE'),
		);

		return $type;
	}

}
