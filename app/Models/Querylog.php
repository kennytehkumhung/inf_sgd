<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Querylog extends Model {

	protected $table = 'querylog';
	 
	const CREATED_AT = 'created';

    const UPDATED_AT = 'modified';


}
