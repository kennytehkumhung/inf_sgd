<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Incentiveproduct extends Model {

	protected $table = 'incentiveproduct';
	 
	const CREATED_AT = 'created';

    const UPDATED_AT = 'modified';

}
