<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Commissionreport extends Model {

	protected $table = 'commissionreport';
	 
	const CREATED_AT = 'created';

    const UPDATED_AT = 'modified';

}
