<?php namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class PremierLeagueToken extends Model {

    protected $table = 'premier_league_token';

    public static function getAvailableTokenAmount($accid, $crccode, $matchId)
    {
        $betCount = PremierLeagueBet::where('match_id', '=', $matchId)
            ->where('accid', '=', $accid)
            ->where('crccode', '=', $crccode)
            ->where('status', '!=', PremierLeagueBet::STATUS_VOID)
            ->count();

        $tokenAmount = PremierLeagueToken::where('match_id', '=', $matchId)
            ->where('accid', '=', $accid)
            ->where('crccode', '=', $crccode)
            ->sum('token_amount');

        $availableToken = $tokenAmount - $betCount;

        if ($availableToken < 0) {
            $availableToken = 0;
        }

        return $availableToken;
    }

    public static function addDepositToken($accid, $nickname, $crccode, $depositAmount, $cashLedgerObjId = 0)
    {
        $acceptedCrccodes = array('MYR');
        $maxTokenPerMatch = 5;

        if (in_array($crccode, $acceptedCrccodes)) {
            $matchObj = PremierLeagueMatch::getCurrentOpeningMatch();

            if ($matchObj) {
                // Have running match.
                $tokenAmount = PremierLeagueToken::where('match_id', '=', $matchObj->id)
                    ->where('accid', '=', $accid)
                    ->where('crccode', '=', $crccode)
                    ->sum('token_amount');

//                $tokenAmount = PremierLeagueToken::getAvailableTokenAmount($accid, $crccode, $matchObj->id);

                if ($tokenAmount < $maxTokenPerMatch) {
                    $tokenPay = 0;

                    if ($crccode == 'MYR') {
                        if ($depositAmount >= 100 && $depositAmount <= 499) {
                            $tokenPay = 1;
                        } elseif ($depositAmount >= 500 && $depositAmount <= 999) {
                            $tokenPay = 3;
                        } elseif ($depositAmount >= 1000) {
                            $tokenPay = 5;
                        }
                    }

                    if ($tokenAmount + $tokenPay > $maxTokenPerMatch) {
                        $tokenPay = $tokenPay - ($tokenAmount + $tokenPay - $maxTokenPerMatch);
                    }

                    if ($tokenPay > 0) {
                        PremierLeagueToken::forceCreate(array(
                            'match_id' => $matchObj->id,
                            'accid' => $accid,
                            'crccode' => $crccode,
                            'nickname' => $nickname,
                            'token_amount' => $tokenPay,
                            'remark' => 'CLREF#' . $cashLedgerObjId . ': Member deposit '. $crccode . ' ' . $depositAmount,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ));
                    }
                }
            }
        }
    }
}
