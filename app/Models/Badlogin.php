<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Badlogin extends Model {

	protected $table = 'badlogin';
	 
	const CREATED_AT = 'created';

    const UPDATED_AT = 'modified';

}
