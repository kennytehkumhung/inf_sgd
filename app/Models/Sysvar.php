<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sysvar extends Model {

	protected $table = 'sysvar';
	 
	const CREATED_AT = 'created';

    const UPDATED_AT = 'modified';

	function getVariable($key, $value = true) {
		$variables = array();
		if($vars = self::whereRaw('$status = 1 AND $param = "'.$key.'"')->get()) {
			foreach($vars as $var) {
				if($value)
				$variables[$var->value] = $var->name;
				else
				$variables[$var->name] = $var->value;
			}
		}
		if(count($variables) > 0) {
			return $variables;
		}
		else return false;
	}

	/**
	* Retrieve the object name by using its value
	*
	* @param string $value		The value
	* @param string $key 		The system variable key
	*
	* @access   public
	* @return 	string  The object's name
	*/
	function getNameByValue($value, $key) {

		if($object = self::whereRaw('$param = "'.$key.'" AND $value = "'.$value.'"')->first()) {
			return $object->name;
		}
		return false;
	}
	
	/**
	* Retrieve the object value by using its name
	*
	* @param string $name		The name
	* @param string $key 		The system variable key
	*
	* @access   public
	* @return 	string  The object's name
	*/
	function getValueByName($name, $key) {

		if($object = self::whereRaw('$param = "'.$key.'" AND $name = "'.$name.'"')->first()) {
			return $object->value;
		}
		return false;
	}

	/**
	* Return a set of hard-coded configuration groups
	*
	* @access   public
	* @return array Hard-coded configurable
	*/
	function getDistinctParam() {
		$params = array();

		if($sysvars = self::get()) {
			foreach($sysvars as $sysvar) {
				$params[$sysvar->param] = $sysvar->param;
			}
		}

		if (count($params) > 0) return $params;
		else return false;
	}
}
