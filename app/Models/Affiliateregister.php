<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Lang;

class Affiliateregister extends Model {

	protected $table = 'affiliateregister';
	 
	const CREATED_AT = 'created';

    const UPDATED_AT = 'modified';
	
	function getStatusColorCode(){
		if ($this->id == 0 || !isset($this->status)) return false;

		if ($this->status == CBO_AFFILIATEREGISTERSTATUS_APPROVE) return '#18833d';
		else if ($this->status == CBO_AFFILIATEREGISTERSTATUS_PENDING) return '#0000ce';
		else if ($this->status == CBO_AFFILIATEREGISTERSTATUS_REJECT) return '#CE0134';
	}
	
	function getStatusText($mdl = false){
		if($this->id == 0) return false;

		if($this->status == CBO_AFFILIATEREGISTERSTATUS_PENDING)
			$str = Lang::get('COMMON.PENDING');
		elseif($this->status == CBO_AFFILIATEREGISTERSTATUS_APPROVE)
			$str = Lang::get('COMMON.APPROVE');
		elseif($this->status == CBO_AFFILIATEREGISTERSTATUS_REJECT)
			$str = Lang::get('COMMON.REJECT');		
		
		return $str;
	}
	
	public static function getStatusOptions(){
		$status = array(
		    CBO_AFFILIATEREGISTERSTATUS_PENDING => Lang::get('COMMON.PENDING'),
		    CBO_AFFILIATEREGISTERSTATUS_APPROVE => Lang::get('COMMON.APPROVE'),
		    CBO_AFFILIATEREGISTERSTATUS_REJECT  => Lang::get('COMMON.REJECT'),
		);

		return $status;
	}

}
