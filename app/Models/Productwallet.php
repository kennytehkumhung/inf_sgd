<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Productwallet extends Model {

	protected $table = 'productwallet';
	 
	const CREATED_AT = 'created';

    const UPDATED_AT = 'modified';
	
	const TYPE_CREDIT			= 1;
	const TYPE_DEBIT			= 2;

	function formatCancelledAmount($value){
		if($this->id == 0) return false;

		if(strlen($value) == 0)
			return '';
		else if($this->status == CBO_LEDGERSTATUS_CANCELLED || $this->status == CBO_LEDGERSTATUS_MANCANCELLED || $this->status == CBO_LEDGERSTATUS_MEMCANCELLED)
			return '<s>'.App::displayAmount($value).'</s>';
		else
			return App::displayAmount($value);
	}
	
	public function doTransfer($refObj, $prdid, $type) {

		if($type === self::TYPE_CREDIT) {
			$amount = abs($refObj->amount);
			$amountlocal = abs($refObj->amountlocal);
			
		} else if($type === self::TYPE_DEBIT) {
			$amount = abs($refObj->amount) * -1;
			$amountlocal = abs($refObj->amountlocal) * -1;

		} else return false;
		
		$object = new self;
		$object->prdid 			= $prdid;
		$object->accid 			= $refObj->accid;
		$object->acccode 		= $refObj->acccode;
		$object->chtcode 		= CBO_CHARTCODE_BALANCETRANSFER;
		$object->crccode 		= $refObj->crccode;
		$object->crcrate 		= $refObj->crcrate;
		$object->amount			= $amount;
		$object->amountlocal	= $amountlocal;
		$object->refobj			= get_class($refObj);
		$object->refid			= $refObj->id;
		if($object->save()) {
			//if success update wallet and return true, else return false cancel transfer
			$success = ProductApi::doTransfer($type, $object->accid, $object->prdid, $object->id, $object->amount);
			
			if($success) {
				$refObj->status = CBO_STANDARDSTATUS_ACTIVE;
				$refObj->update();
				$object->status = CBO_STANDARDSTATUS_ACTIVE;
				return $object->save();
			} else {
				$object->status = 2;
				$object->save();
				return false;
			}
		}
	}
	
	public function doAddLedger(KenoWagerObject $refobj, $chtcode, $confirm = false) {
		$object = new self;
		$object->accid 			= $refobj->accid;
		$object->acccode 		= $refobj->acccode;
		$object->crccode 		= $refobj->crccode;
		$object->crcrate 		= $refobj->crcrate;
		$object->chtcode 		= $chtcode;
		
		if($chtcode == CBO_CHARTCODE_GAMETRANS){
			$object->amount 	= $refobj->amount * (-1);
			$object->amountlocal 	= $refobj->amountlocal * (-1);
		}elseif($chtcode == CBO_CHARTCODE_GAMEPAYOUT) {
			$object->amount 	= $refobj->payout;
			$object->amountlocal 	= $refobj->payoutlocal;
		}
		$object->refobj = str_replace('object', '', get_class($refobj));
		$object->refid = $refobj->refid;
		
		if($confirm) {
			$object->status = 1;
			if($chtcode == CBO_CHARTCODE_GAMETRANS){
				$accObj = new AccountObject;
				if($accObj = Account::whereRaw($object->accid)->first()) {
					$accObj->lastbetdate = App::getDateTime();
					$accObj->save();
				}
			}
		}
		
		return $object->save();
	}
	
	public function updateBalance(AccountObject $accObj, $prdid) {

		if($csbObj = CashBalance::whereRaw('accid='.$accObj->id.' AND prdid = '.$prdid)->first()) {
			$csbObj->balance = CashLedger::getWalletBalance($prdid, $accObj);
			$csbObj->save();
		}
	}
}
