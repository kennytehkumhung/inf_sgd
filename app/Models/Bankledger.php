<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bankledger extends Model {

	protected $table = 'bankledger';
	 
	const CREATED_AT = 'created';

    const UPDATED_AT = 'modified';
	
	const TYPE_CREDIT 				= 1;
	const TYPE_DEBIT 				= 2;

}
