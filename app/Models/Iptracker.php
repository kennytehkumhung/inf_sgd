<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Iptracker extends Model {

	protected $table = 'iptracker';
	 
	const CREATED_AT = 'created';

    const UPDATED_AT = 'modified';
	
	function getPanelColorCode(){
		if($this->id == 0) return false;

		if($this->createdpanel == CBO_LOGINTYPE_USER)
		return '#CC99FF';
		else if($this->createdpanel == CBO_LOGINTYPE_ADMIN)
		return '#CCFFCC';
		else if($this->createdpanel == CBO_LOGINTYPE_AFFILIATE)
		return '#CCCCFF';
	}


	function getPanelText(){
		if($this->id == 0) return false;

		if ($this->createdpanel == CBO_LOGINTYPE_USER)
		return Lang::get('COMMON.MEMBER');
		elseif ($this->createdpanel == CBO_LOGINTYPE_ADMIN)
		return Lang::get('COMMON.ADMINUSER');
		elseif ($this->createdpanel == CBO_LOGINTYPE_AFFILIATE)
		return Lang::get('COMMON.AFFILIATE');
	}


	function getPanelOptions(){
		
		$panel = array(
			CBO_LOGINTYPE_USER 		=> Lang::get('COMMON.MEMBER'),
			CBO_LOGINTYPE_ADMIN 	=> Lang::get('COMMON.ADMINUSER'),
			CBO_LOGINTYPE_AFFILIATE => Lang::get('COMMON.AFFILIATE'),
		);

		return $panel;
	}

}
