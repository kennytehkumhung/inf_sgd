<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Lang;

class Banktransfer extends Model {

	protected $table = 'banktransfer';
	 
	const CREATED_AT = 'created';

    const UPDATED_AT = 'modified';
	
	const TRANSFERTYPE_INTERNET 		= 1;
	const TRANSFERTYPE_ATM 				= 2;
	const TRANSFERTYPE_CASH 			= 3;
	const TRANSFERTYPE_PHONE 			= 4;
	const TRANSFERTYPE_ALIPAY 			= 5;
	
	static function getStatusOptions(){
		$status = array(
			CBO_LEDGERSTATUS_PENDING 	=> Lang::get('COMMON.PENDING'),
			CBO_LEDGERSTATUS_CONFIRMED 	=> Lang::get('COMMON.CONFIRMED'),
			CBO_LEDGERSTATUS_CANCELLED 	=> Lang::get('COMMON.REJECTED'),
		);

		return $status;
	}

	static function getStatusText($status = null){
		if($status == null && $this->id != 0) $status = $this->status;

		if($status == CBO_LEDGERSTATUS_CONFIRMED)
		return Lang::get('COMMON.CONFIRMED');
		else if($status == CBO_LEDGERSTATUS_PENDING)
		return Lang::get('COMMON.PENDING');
		else if($status == CBO_LEDGERSTATUS_CANCELLED)
		return Lang::get('COMMON.REJECTED');
	}

	static function getStatusColorCode(){
		if($this->id == 0) return false;

		if($this->status == CBO_LEDGERSTATUS_CONFIRMED)
		return '#C6FFD9';
		else if($this->status == CBO_LEDGERSTATUS_CANCELLED)
		return '#FFCCCC';
		else if($this->status == CBO_LEDGERSTATUS_PENDING)
		return '#FFFFCC';
	}
	
	static function getTypeText(){
		if($this->id == 0) return false;

		if($this->type == CBO_CHARTCODE_DEPOSIT)
		return Lang::get('COMMON.DEPOSIT');
		else if($this->type == CBO_CHARTCODE_WITHDRAWAL)
		return Lang::get('COMMON.WITHDRAWAL');
		else if($this->type == CBO_CHARTCODE_WITHDRAWALCHARGES)
		return Lang::get('COMMON.WITHDRAWALCHARGES');
	}
	
	static function formatCancelledAmount($value){
		if($this->id == 0) return false;

		if(strlen($value) == 0)
			return '';
		else if($this->status == CBO_LEDGERSTATUS_CANCELLED)
			return '<s>'.number_format($value, 2).'</s>';
		else
			return number_format($value, 2);
	}
	
	
	public static function getTransferTypeText(){
		if($this->id == 0) return false;

		if($this->type == self::TRANSFERTYPE_INTERNET)
		return Lang::get('COMMON.INTERNETTRANSFER');
		else if($this->type == self::TRANSFERTYPE_ATM)
		return Lang::get('COMMON.ATMTRANSFER');
		else if($this->type == self::TRANSFERTYPE_CASH)
		return Lang::get('COMMON.CASHTRANSFER');		
		else if($this->type == self::TRANSFERTYPE_PHONE)
		return Lang::get('COMMON.PHONETRANSFER');
		else if($this->type == self::TRANSFERTYPE_ALIPAY)
		return Lang::get('COMMON.ALIPAYTRANSFER');
	}
	
	public static function getTransferTypeOptions(){
		$options = array(
			self::TRANSFERTYPE_INTERNET 	=> Lang::get('COMMON.INTERNETTRANSFER'),
			self::TRANSFERTYPE_ATM 			=> Lang::get('COMMON.ATMTRANSFER'),
			self::TRANSFERTYPE_CASH 		=> Lang::get('COMMON.CASHTRANSFER'),
			self::TRANSFERTYPE_PHONE 		=> Lang::get('COMMON.PHONETRANSFER'),
			self::TRANSFERTYPE_ALIPAY 		=> Lang::get('COMMON.ALIPAYTRANSFER'),
		);

		return $options;
	}
	
	public static function getInterbankMethod() {
		if(defined('SYSTEM_PAYMENT_INTERBANK') && SYSTEM_PAYMENT_INTERBANK > 0) {
			return SYSTEM_PAYMENT_INTERBANK;
		} else return 0;
	}
	
	public static function isInterbankMethod($method) {
		if($method == self::TRANSFERTYPE_INTERNET)
		return true;
		else if($method == self::TRANSFERTYPE_ATM)
		return false;
		else if($method == self::TRANSFERTYPE_CASH)
		return false;
		else if($method == self::TRANSFERTYPE_PHONE)
		return false;
		else if($method == self::TRANSFERTYPE_ALIPAY)
		return false;
	}
	

}
