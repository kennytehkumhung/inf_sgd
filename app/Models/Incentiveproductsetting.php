<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Incentiveproductsetting extends Model {

	protected $table = 'incentiveproductsetting';
	 
	const CREATED_AT = 'created';

    const UPDATED_AT = 'modified';

}
