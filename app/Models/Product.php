<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Lang;
use App\libraries\App;

class Product extends Model {

	protected $table = 'product';
	 
	const CREATED_AT = 'created';

    const UPDATED_AT = 'modified';
	
	const STATUS_SUSPENDED 			= 0;
	const STATUS_ACTIVE 			= 1;
	const STATUS_MAINTENANCE 		= 2;
	
	const CATEGORY_SPORTSBOOK 		= 1;
	const CATEGORY_LIVECASINO 		= 2;
	const CATEGORY_NUMBER 			= 3;
	const CATEGORY_FINANCE 			= 4;
	const CATEGORY_RNG 				= 5;
	const CATEGORY_LOTTERY 			= 6;
	const CATEGORY_P2P 				= 7;
	const CATEGORY_RACEBOOK 		= 8;
	
	function getStatusColorCode(){
		if($this->id == 0) return false;

		if($this->status == self::STATUS_ACTIVE)
		return '#C6FFD9';
		else if($this->status == self::STATUS_SUSPENDED)
		return '#FFCCCC';
		else if($this->status == self::STATUS_MAINTENANCE)
		return '#FFCCCC';
	}
	
	static function getAllAsOptions($code = false, $active = false) {

		$products = array();
		$condition = '1';
		if($active) $condition .= ' AND status != '.self::STATUS_SUSPENDED;
		if($objects = self::whereRaw($condition)->get()) {
			foreach($objects as $object) {
				if($code) $products[$object->id] = $object->code;
				else
				$products[$object->id] = $object->name;
			}
		}
		if (count($products) > 0) return $products;
		else return false;
	}

	function getWebSiteProducts(){
		$productId   = array();
		$products    = array();
		$idStr       = '';
		
		$webSiteObj = new Website;
		$productId  = $webSiteObj->getWebSiteProducts();
		
		for($i = 0; $i < count($productId); $i++){
			if($i != (count($productId) - 1)) $idStr .= $productId[$i].',';
			else $idStr.= $productId[$i];
		}
		
		$name = 'name'.App::getGlobalSession('language');
	
		if($objects = self::whereRaw('id IN ('.$idStr.')')->get()){
			foreach($objects as $obj){
				$products[$obj->id]  = $obj->$name;
			}
		}
		
		if(count($products) > 0) return $products;
		else return false;
	}
	
	
	public static function getStatusOptions() {
		$status = array(
			self::STATUS_ACTIVE 			=> Lang::get('COMMON.ACTIVE'),
			self::STATUS_SUSPENDED 			=> Lang::get('COMMON.SUSPENDED'),
			self::STATUS_MAINTENANCE 		=> Lang::get('COMMON.MAINTENANCE'),
		);

		return $status;
	}
	
	/**
	* Return the current object status in text. For standard Active/Suspended object only
	* If the object has non-standard object please overwrite this function in the object class
	*
	*
	* @return string The object status in language text
	*/
	public function getStatusText($module = false){
		if($this->id == 0 || !isset($this->status)) return false;

		if($this->status == 1) {
			$action = 'suspend';
			$str = Lang::get('COMMON.ACTIVE');
		} else if($this->status == 0) {
			$action = 'activate';
			$str = Lang::get('COMMON.SUSPENDED');
		} else if($this->status == 2) {
			$str = Lang::get('COMMON.MAINTENANCE');
		}
		
		if($module && ($this->status == 1 || $this->status == 0))
		$str = '<a href="#" onclick="return doConfirmAction(\''.$module.'-active-suspend\', \''.$action.'\', \''.$this->id.'\')">'.$str.'</a>';
		
		return $str;
	}
	
	/**
	* Return the current object category in text. 
	*
	* @return string The object category in language text
	*/
	public static function getCategoryOptions() {
		$categories = array(
			self::CATEGORY_SPORTSBOOK 	=> Lang::get('COMMON.PRODUCTSPB'),
			self::CATEGORY_LIVECASINO 	=> Lang::get('COMMON.PRODUCTLVC'),
			self::CATEGORY_NUMBER 		=> Lang::get('COMMON.PRODUCTNUM'),
			self::CATEGORY_FINANCE 		=> Lang::get('COMMON.PRODUCTFIN'),
			self::CATEGORY_RNG 			=> Lang::get('COMMON.PRODUCTRNG'),
			self::CATEGORY_LOTTERY 		=> Lang::get('COMMON.PRODUCTLOTTERY'),
			self::CATEGORY_P2P 			=> Lang::get('COMMON.PRODUCTP2P'),
			self::CATEGORY_RACEBOOK 	=> Lang::get('COMMON.PRODUCTRCB'),
		);
		
		if( \Config::get('setting.opcode') == 'GSC' ){
			$categories  = array(
				self::CATEGORY_SPORTSBOOK 	=> Lang::get('COMMON.PRODUCTSPB'),
				self::CATEGORY_LIVECASINO 	=> Lang::get('COMMON.PRODUCTLVC'),
				self::CATEGORY_LOTTERY 		=> Lang::get('COMMON.PRODUCTLOTTERY'),
				self::CATEGORY_RNG 			=> Lang::get('COMMON.PRODUCTRNG'),
			);
		}

		return $categories;
	}
	
	/**
	* Return the current object category in text. 
	*
	* @return string The object category in language text
	*/
	public function getCategoryText(){
		if($this->id == 0 || !isset($this->category)) return false;
		
		$str = '';
		$options = self::getCategoryOptions();
	
		$categories = false;
		if(strlen($this->category) > 0)
		$categories = unserialize($this->category);
		if(is_array($categories)) {
			foreach($categories as $category) {
				$str .= $options[$category].', ';
			}
		}
		
		return $str;
	}

    public function getNameWithMultiLang($lang = 'en', $appendOriName = true) {
	    $name = $this->name;

        if ($lang != 'en') {
            $multiLang = json_decode($this->name_multi_lang, true);

            if (count($multiLang) > 0 && array_key_exists($lang, $multiLang)) {
                // Decode success.
                $ml = $multiLang[$lang];

                if (strlen($ml) > 0) {
                    if ($appendOriName) {
                        $name .= ' (' . $ml . ')';
                    } else {
                        $name = $ml;
                    }
                }
            }
        }

        return $name;
    }
	
	public static function getAccountBalance($id, AccountObject $accObj) {
		$object = new self;
		if($object->get($id)) {
			
			if(!$balance = ProductApi::getBalance($accObj->id, $id))
			$balance = 0;
			return array('name' => $object->name, 'balance' => $balance);
		}
		return false;
	}
	
	public static function getCategory($str) {
		if(strtolower($str) === 'sports') return Product::CATEGORY_SPORTSBOOK;
		else if(strtolower($str) === 'casino') return Product::CATEGORY_LIVECASINO;
		else if(strtolower($str) === 'number') return Product::CATEGORY_NUMBER;
		else if(strtolower($str) === 'lottery') return Product::CATEGORY_LOTTERY;
		else if(strtolower($str) === 'egames') return Product::CATEGORY_RNG;
		else if(strtolower($str) === 'racebook') return Product::CATEGORY_RACEBOOK;
	}
}
