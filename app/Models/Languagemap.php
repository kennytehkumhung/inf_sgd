<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Languagemap extends Model {

	protected $table = 'languagemap';
	 
	const CREATED_AT = 'created';

    const UPDATED_AT = 'modified';
	
	public function getValuesByObject($refobj, $member) {
		
		$classname = get_class($refobj );
		
		$langs = array();
		if($values = self::whereRaw('objectid = '.$refobj->id.' AND objectname = "'.$classname.'" AND objectmember = "'.$member.'"')->get()) {
			foreach($values as $value) {
				$langObj = Language::whereRaw('$langcode = "'.$value->lang.'"')->first();
				$langs[$value->lang] = mb_convert_encoding($value->objectvalue, 'utf-8' , $langObj->encoding);
			}
		}
		
		return $langs;	
	}

}
