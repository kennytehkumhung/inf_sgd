<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Apilog extends Model {

	protected $table = 'apilog';
	 
	const CREATED_AT = 'created';

    const UPDATED_AT = 'modified';

}
