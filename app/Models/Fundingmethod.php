<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Lang;

class Fundingmethod extends Model {

	protected $table = 'fundingmethod';
	 
	const CREATED_AT = 'created';

    const UPDATED_AT = 'modified';

	/**
	* Return the deposit fee in text of the current object
	*
	*
	* @return string The deposit fee
	*/
	function getDepositFeeText() {
		if($this->id == 0) return false;

		if($this->deposit == 1) {
			if($this->depositfeetype == 1)
			return $this->depositfeeamount;
			elseif($this->depositfeetype == 2)
			return $this->depositfeepercent.'%';
		} else return '';
	}
	
	function getStatusText($module = false){
		
		if($this->id == 0 || !isset($this->status)) return false;

		if($this->status == 1) {
			$action = 'suspend';
			$str = Lang::get('COMMON.ACTIVE');
		} else if($this->status == 0) {
			$action = 'activate';
			$str = Lang::get('COMMON.SUSPENDED');
		}

		if($module)
		$str = '<a href="#" onclick="return doConfirmAction(\''.$module.'-active-suspend\', \''.$action.'\', \''.$this->id.'\')">'.$str.'</a>';

		return $str;
	}
	
	/**
	* Return the deposit fee in text of the current object
	*
	*
	* @return string The deposit fee
	*/
	function getFeeTypeAsOptions() {
		$types = array(
			1 => COMMON.FIXEDCHARGES,
			2 => COMMON.PERCENTAGE,
		);
		return $types;
	}

	/**
	* Return the withdrawal fee in text of the current object
	*
	*
	* @return string The deposit fee
	*/
	function getWithdrawalFeeText() {
		if($this->id == 0) return false;

		if($this->withdraw == 1) {
			if($this->withdrawfeetype == 1)
			return $this->withdrawfeeamount;
			elseif($this->withdrawfeetype == 2)
			return $this->withdrawfeepercent.'%';
		} else return '';
	}

	/**
	* Return the unserialize array of limits on Minimun Deposit, Minimum Withdrawal and Maximum Daily Withrawal by a given crccode
	*
	* @param string $crccode
	*
	* @return array The limits
	*/
	function getLimits($crccode){
		if($crccode != ''){
			$limits  = unserialize($this->limitmatrix);
			foreach($limits as $currency => $values){
				if($currency == $crccode){
					return $values;
				}
			}
		}
	}

	/**
	* Get all funding method as array
	*
	* @param boolean $withdraw 	Flag to select the funding method that support withdrawal
	*
	* @access	public
	* @return 	array/boolean Array of active funding method or false if none found.
	*/
	static function getAllFundingMethodAsOptions($type = false, $showSelect = true) {

		$fundingMethods 		= array();
		$condition 				= 'status = 1';
		
		if($showSelect)
			$fundingMethods[0] 	= Lang::get('COMMON.SELECT');
		
		if($type == CBO_CHARTCODE_DEPOSIT) {
			$condition .= ' AND withdraw = 1';
		} else if($type == CBO_CHARTCODE_WITHDRAWAL) {
			$condition .= ' AND deposit = 1';
		}

		if($methods = self::whereRaw($condition)->get()) {
			foreach($methods as $method) {
				$fundingMethods[$method->id] = $method->name;
			}
		}

		if (count($fundingMethods) > 0) return $fundingMethods;
		else return false;
	}
	
	/**
	* Get all active funding method
	*
	* @param boolean $withdraw 	Flag to select the funding method that support withdrawal
	*
	* @access	public
	* @return 	array/boolean Array of active funding method or false if none found.
	*/
	function getAllFundingMethod($withdraw = false) {
		$methods = array();
		if($settings = Fundingmethodsetting::whereRaw('crccode = "'.SYSTEM_CURRENCY.'"')->get()) {
			$condition = 'deposit = 1';
			if($withdraw) $condition = 'withdraw = 1';
			foreach($settings as $setting) {
				$condition .= ' AND id='.$setting->fdmid;
				if(Fundingmethod::whereRaw($condition)->first) {
					$methods[$fdmObj->id] = $fdmObj->name;
				}
			}
		}
		return $methods;

		if (count($fundingMethods) > 0) return $fundingMethods;
		else return false;
	}
	
	/**
	* Get reference object constant by funding method remark code given.
	*
	* @access	public
	* @return 	array/boolean Array of reference object key and value in text.
	*/
	function getRefObjByRemarkCode() {
		if($this->remarkcode == 'BANK')
			return CBO_LEDREFOBJ_BANKTRANSFER;
		else if($this->remarkcode == 'CHEQUE')
			return CBO_LEDREFOBJ_CHEQUE;
		else if($this->remarkcode == 'CARD')
			return CBO_LEDREFOBJ_CCTRANSFER;
		else if($this->remarkcode == 'NETELLER')
			return CBO_LEDREFOBJ_NETELLER;	
		else if($this->remarkcode == 'NETELLER1PAY')
			return CBO_LEDREFOBJ_NETELLER1PAY;		
		else if($this->remarkcode == 'MONEYBOOKER')
			return CBO_LEDREFOBJ_MONEYBOOKER;
		else return false;
	}
	
	/**
	* Return the reference object's class name.
	*
	*
	* @return string The object refobj in class name
	*/
	function getRefObjectNameByRemarkCode(){

		if($this->remarkcode == 'BANK')
			return Lang::get('COMMON.BANKTRANSFEROBJECT');
		else if($this->remarkcode == 'CARD')
			return Lang::get('COMMON.CCTRANSFEROBJECT');
		else if($this->remarkcode == 'CHEQUE')
			return Lang::get('COMMON.CHEQUEOBJECT');
		else if($this->remarkcode == 'NETELLER')
			return Lang::get('COMMON.NETELLEROBJECT');		
		else if($this->remarkcode == 'NETELLER1PAY')
			return Lang::get('COMMON.NETELLER1PAYOBJECT');	
		else if($this->remarkcode == 'MONEYBOOKER')
			return Lang::get('COMMON.MONEYBOOKEROBJECT');
		else return false;
	}
	
	/**
	* Return the reference object in text.
	*
	*
	* @return string The object refobj in language text
	*/
	function getRefObjectTextByRemarkCode(){

		if($this->remarkcode == 'BANK')
			return Lang::get('COMMON.BANKTRANSFER');
		else if($this->remarkcode == 'CARD')
			return Lang::get('COMMON.CREDITCARD');
		else if($this->remarkcode == 'CHEQUE')
			return Lang::get('COMMON.CHEQUE');
		else if($this->remarkcode == 'NETELLER')
			return Lang::get('COMMON.NETELLER');		
		else if($this->remarkcode == 'NETELLER1PAY')
			return Lang::get('COMMON.NETELLER1PAY');
		else if($this->remarkcode == 'MONEYBOOKER')
			return Lang::get('COMMON.MONEYBOOKER');
		else return false;

	}
	
	/**
	* Return the current object status color code.
	*
	*
	* @return string The object status color code
	*/
	function getStatusColorCode(){
		if($this->id == 0) return false;

		if($this->status == CBO_STANDARDSTATUS_ACTIVE)
		return '#C6FFD9';
		else if($this->status == CBO_STANDARDSTATUS_SUSPENDED)
		return '#FFCCCC';
	}
	
	
	/**
	* Method to get the limitmatrix according the remarkcode and crccode
	*
	* @access	protected
	*
	* @return none
	*
	*/
	function getLimitMatrix($remarkcode, $crccode) {

		$object = self::whereRaw('remarkcode = "'.$remarkcode.'"')->first();

		$Currency = unserialize($object->limitmatrix);
		foreach($Currency as $key => $value){
			if($key == $crccode)	{
				$limitMatrixItem = array(
					'mindeposit' 		 => $value['mindeposit'],
					'minwithdrawal' 	 => $value['minwithdrawal'],
					'maxdailywithdrawal' => $value['maxdailywithdrawal'],
					'maxdeposit' 		 => $value['maxdeposit'],
				);
			}
		}
		
		if (isset($limitMatrixItem)) return $limitMatrixItem;
		else return false;
	}
	
	function getFee($type, $amount) {
		if($this->id == 0) return false;
		
		$fee = 0;
		if($type == CBO_CHARTCODE_DEPOSIT) {
			if($this->depositfeetype == 1)
			$fee = $this->depositfeeamount;
			else if($this->depositfeetype == 2) {
				$fee = $this->depositfeepercent/100 * $amount;
				if(($this->depositfeemin > 0) && ($fee < $this->depositfeemin)) $fee = $this->depositfeemin;
				else if(($this->depositfeemax > 0) && ($fee > $this->depositfeemax)) $fee = $this->depositfeemax;
			}
		} else if($type == CBO_CHARTCODE_WITHDRAWAL) {
			if($this->withdrawfeetype == 1)
			$fee = $this->withdrawfeeamount;
			else if($this->withdrawfeetype == 2) {
				$fee = $this->withdrawfeepercent/100 * $amount;
				if(($this->withdrawfeemin > 0) && ($fee < $this->withdrawfeemin)) $fee = $this->withdrawfeemin;
				else if(($this->withdrawfeemax > 0) && ($fee > $this->withdrawfeemax)) $fee = $this->withdrawfeemax;
			}
		}
		return $fee;
	}

	
}
