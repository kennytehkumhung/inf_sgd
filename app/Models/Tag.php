<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Lang;

class Tag extends Model {

	protected $table = 'tag';
	 
	const CREATED_AT = 'created';

    const UPDATED_AT = 'modified';

    const CATEGORY_CUSTOMER_TAG = 0;
    const CATEGORY_OPERATOR_TAG = 1;

	function getStatusText($module = false){
		
		if($this->id == 0 || !isset($this->status)) return false;

		if($this->status == 1) {
			$action = 'suspend';
			$str = Lang::get('COMMON.ACTIVE');
		} else if($this->status == 0) {
			$action = 'activate';
			$str = Lang::get('COMMON.SUSPENDED');
		}

		if($module)
		$str = '<a href="#" onclick="return doConfirmAction(\''.$module.'-active-suspend\', \''.$action.'\', \''.$this->id.'\')">'.$str.'</a>';

		return $str;
	}
        
        static function getAllAsOptions($name = false) {

		$tags = array();
		$condition = 'status = 1 && special = 1';
		if($objects = self::whereRaw($condition)->get()) {
			foreach($objects as $object) {
				if($name) $tags[$object->id] = $object->name;
				else
				$tags[$object->id] = $object->name;
			}
		}
		if (count($tags) > 0) return $tags;
		else return false;
	}
        
        public static function getStatusOptions(){
		
		$status = array(
			CBO_ACCOUNTSTATUS_ACTIVE 			=> Lang::get('COMMON.ACTIVE'),
			CBO_ACCOUNTSTATUS_SUSPENDED 		=> Lang::get('COMMON.SUSPENDED'),
		);

		return $status;
	}
//        public static function getSpecialOptions(){
//		
//		$special = array(
//			CBO_SPECIALTAG_NO 		=> Lang::get('COMMON.NO'),
//			CBO_SPECIALTAG_YES 		=> Lang::get('COMMON.YES'),
//		);
//
//		return $special;
//	}
        
        public static function getSpecialOptions(){
		
		$special = array(
			1 => Lang::get('COMMON.YES'),
			0 => Lang::get('COMMON.NO'),
		);

		return $special;
	}

    public static function getCategoryOptions($prependOptionAll = false) {

        $options = [];

        if ($prependOptionAll) {
            $options[''] = Lang::get('COMMON.ALL');
        }

        $options[self::CATEGORY_CUSTOMER_TAG] = Lang::get('COMMON.CUSTOMER');
        $options[self::CATEGORY_OPERATOR_TAG] = Lang::get('COMMON.OPERATOR');

        return $options;
    }

}
