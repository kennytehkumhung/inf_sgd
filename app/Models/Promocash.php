<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Promocash extends Model {

	protected $table = 'promocash';
	 
	const CREATED_AT = 'created';

    const UPDATED_AT = 'modified';
	
	const TYPE_DEPOSIT 		= 1;
	const TYPE_MANUAL 		= 2;

	const STATUS_PENDING	= 0;
	const STATUS_APPROVE	= 1;
	const STATUS_REJECT		= 2;
	
	function getStatusOptions(){
		$status = array(
			CBO_PROMOCASHSTATUS_PENDING		=> Lang::get('COMMON.PENDING'),
			CBO_PROMOCASHSTATUS_CONFIRMED 	=> Lang::get('COMMON.CONFIRMED'),
			CBO_PROMOCASHSTATUS_REJECTED 	=> Lang::get('COMMON.REJECTED'),
		);

		return $status;
	}
	
	function getStatusText($status = null){
		if($status == null && $this->id != 0) $status = $this->status;

		if($status == CBO_PROMOCASHSTATUS_PENDING)
		return Lang::get('COMMON.PENDING');
		else if($status == CBO_PROMOCASHSTATUS_CONFIRMED)
		return Lang::get('COMMON.CONFIRMED');
		else if($status == CBO_PROMOCASHSTATUS_REJECTED)
		return Lang::get('COMMON.REJECTED');
		else if($status == CBO_PROMOCASHSTATUS_CREDITED)
		return Lang::get('COMMON.CREDITED');
	}

	function getStatusColorCode($status = null) {
		if($status == null && $this->id != 0) $status = $this->status;

		if($status == CBO_PROMOCASHSTATUS_PENDING)
		return '#FFFFCC';
		else if($status == CBO_PROMOCASHSTATUS_CONFIRMED)
		return '#C6FFD9';
		else if($status == CBO_PROMOCASHSTATUS_REJECTED)
		return '#FFCCCC';
	}


	function formatCancelledAmount($value){
		if($this->id == 0) return false;

		if(strlen($value) == 0)
			return '';
		else if($this->status == CBO_PROMOCASHSTATUS_REJECTED)
			return '<s>'.number_format($value, 2).'</s>';
		else
			return number_format($value, 2);
	}

}
