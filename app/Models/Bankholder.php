<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Lang;

class Bankholder extends Model {

	protected $table = 'bankholder';
	 
	const CREATED_AT = 'created';

    const UPDATED_AT = 'modified';

	function getStatusColorCode(){
		if($this->id == 0) return false;

		if($this->status == CBO_STANDARDSTATUS_ACTIVE)
		return '#C6FFD9';
		else if($this->status == CBO_STANDARDSTATUS_SUSPENDED)
		return '#FFCCCC';
	}
	
	function getAllAsOptions($activeOnly = false, $currency = false) {
		
		$options = array();
		$condition = '1';
		if($activeOnly) $condition .= ' AND status = 1';
		if($currency)   $condition .= ' AND crccode = "'.$currency.'"';
		
		if($objects = self::whereRaw($condition)->get() ) {
			foreach($objects as $object) {
				$options[$object->id] = $object->name;
			}
		}
		if (count($options) > 0) return $options;
		else return false;
	}
	
	function getStatusText(){
		if($this->id == 0) return false;
		
		if($this->status == CBO_STANDARDSTATUS_SUSPENDED)
		$str = Lang::get('COMMON.SUSPENDED');
		else if($this->status == CBO_STANDARDSTATUS_ACTIVE)
		$str = Lang::get('COMMON.ACTIVE');
		
		return $str;
	}
        
        public static function getStatusOptions(){
		
		$status = array(
			CBO_ACCOUNTSTATUS_ACTIVE 			=> Lang::get('COMMON.ACTIVE'),
			CBO_ACCOUNTSTATUS_SUSPENDED 		=> Lang::get('COMMON.SUSPENDED'),
		);

		return $status;
	}
}
