<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Lang;

class Announcement extends Model {

	protected $table = 'announcement';
	 
	const CREATED_AT = 'created';

    const UPDATED_AT = 'modified';
	
	const CBO_TYPE_COMPANY 			= 1;
	const CBO_TYPE_AFFILIATE 		= 2;
	const CBO_TYPE_MEMBER 			= 4;

	function isCompanyType(){
		if($this->id == 0) return false;
		
		if($this->type & self::CBO_TYPE_COMPANY)
		return true;
		else return false;
	}
	

	function isAffiliateType(){
		if($this->id == 0) return false;
		
		if($this->type & self::CBO_TYPE_AFFILIATE)
		return true;
		else return false;
	}
	

	function isMemberType(){
		if($this->id == 0) return false;
		
		if($this->type & self::CBO_TYPE_MEMBER)
		return true;
		else return false;
	}
	
	function getStatusText($module = false){
		
		if($this->id == 0 || !isset($this->status)) return false;

		if($this->status == 1) {
			$action = 'suspend';
			$str = Lang::get('COMMON.ACTIVE');
		} else if($this->status == 0) {
			$action = 'activate';
			$str = Lang::get('COMMON.SUSPENDED');
		}

		if($module)
		$str = '<a href="#" onclick="return doConfirmAction(\''.$module.'-active-suspend\', \''.$action.'\', \''.$this->id.'\')">'.$str.'</a>';

		return $str;
	}
	
	public static function getTypeOptions(){
		$type = array(
			self::CBO_TYPE_COMPANY 			=> Lang::get('COMMON.COMPANY'),
			self::CBO_TYPE_AFFILIATE 		=> Lang::get('COMMON.AFFILIATE'),
			self::CBO_TYPE_MEMBER 			=> Lang::get('COMMON.MEMBER'),
		);

		return $type;
	}
}
