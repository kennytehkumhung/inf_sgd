<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Media;
use Lang;

class Banner extends Model {

	protected $table = 'banner';
	 
	const CREATED_AT = 'created';

    const UPDATED_AT = 'modified';
	
	public function getImage($type = Media::TYPE_IMAGE) {
		if($this->id == 0) return false;
		

		if($object = Media::whereRaw('refobj = "'.str_replace('App\Models\\', '', get_class($this)).'" AND refid = '.$this->id.' AND type = '.$type)->first()) {
			return $object->domain.'/'.$object->path;
		}
		
		return false;
	}
	
	function getStatusText($module = false){
		
		if($this->id == 0 || !isset($this->status)) return false;

		if($this->status == 1) {
			$action = 'suspend';
			$str = Lang::get('COMMON.ACTIVE');
		} else if($this->status == 0) {
			$action = 'activate';
			$str = Lang::get('COMMON.SUSPENDED');
		}

		if($module)
		$str = '<a href="#" onclick="return doConfirmAction(\''.$module.'-active-suspend\', \''.$action.'\', \''.$this->id.'\')">'.$str.'</a>';

		return $str;
	}
	 
}
