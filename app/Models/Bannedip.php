<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\libraries\App;

class Bannedip extends Model {

	protected $table = 'bannedip';
	 
	const CREATED_AT = 'created';

    const UPDATED_AT = 'modified';
	
	function getStatusColorCode(){
		if($this->id == 0) return false;

		if($this->status == CBO_BANNEDIPSTATUS_ACTIVE)
		return '#C6FFD9';
		else if($this->status == CBO_BANNEDIPSTATUS_SUSPENDED)
		return '#FFCCCC';
	}

	function getTypeOptions(){
		$type = array(
			1 => Lang::get('COMMON.WEBSITE'),
			2 => Lang::get('COMMON.INRUNNING'),
		);

		return $type;
	}

	function getTypeText(){
		if($this->id == 0) return false;
		if ($this->type == 1)
		return Lang::get('COMMON.WEBSITE');
		elseif ($this->type == 2)
		return Lang::get('COMMON.INRUNNING');
	}

	function isBannedFromWebsite() {

		$remoteIp = sprintf("%u", ip2long( App::getRemoteIp() ));
		if( self::whereRaw('mode = '. CBO_MODE_BANNEDIP .' AND type = 1 AND status = 1 AND fromlong <= '.$remoteIp.' AND tolong >= '.$remoteIp)->count() )
		return true;

		return false;
	}

	function isBannedFromRunningMarket() {

		$remoteIp = sprintf("%u", ip2long( App::getRemoteIp() ));
		if( self::whereRaw('mode = '. CBO_MODE_BANNEDIP .' AND type = 2 AND status = 1 AND fromlong <= '.$remoteIp.' AND tolong >= '.$remoteIp)->count() )
		return true;

		return false;
	}
	
	public static function isAllowAccess() {
		$remoteIp = sprintf("%u", ip2long( App::getRemoteIp() ));
		$object = new self;
		if( self::whereRaw('mode = '. CBO_MODE_ALLOWIP .' AND status = 1 AND fromlong = '.$remoteIp)->count() )
		return true;

		return false;
	}

}
