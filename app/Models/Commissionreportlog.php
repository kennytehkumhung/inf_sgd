<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Commissionreportlog extends Model {

	protected $table = 'commissionreportlog';
	 
	const CREATED_AT = 'created';

    const UPDATED_AT = 'modified';

}
