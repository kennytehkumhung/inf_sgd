<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Syslog extends Model {

	protected $table = 'syslog';
	 
	const CREATED_AT = 'created';

    const UPDATED_AT = 'modified';
	
	function getPanelOptions() {
		$type = array(
			'user' => Lang::get('COMMON.MEMBER'),
			'admin' => Lang::get('COMMON.ADMINUSER'),
			'agent' => Lang::get('COMMON.AFFILIATE'),
		);

		return $type;
	}

	/**
	* Return the current object user type color code.
	*
	*
	* @return string The object user type color code
	*/
	function getPanelColorCode(){
		if($this->id == 0) return false;

		if($this->panel == 'user')
		return '#CC99FF';
		else if($this->panel == 'rcm')
		return '#CCFFCC';
		else if($this->panel == 'agent')
		return '#CCCCFF';
	}

}
