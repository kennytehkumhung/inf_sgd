<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Lang;

class Paymentgatewaysetting extends Model {

	protected $table = 'paymentgatewaysetting';

    const TYPE_WECHAT = 'WXP';
    const TYPE_ALIPAY = 'ZFB';

    public static function getStatusOptions($prependOptionAll = false) {

        $options = [];

        if ($prependOptionAll) {
            $options[''] = Lang::get('COMMON.ALL');
        }

        $options[strval(CBO_STANDARDSTATUS_ACTIVE)] = Lang::get('COMMON.ACTIVE');
        $options[strval(CBO_STANDARDSTATUS_SUSPENDED)] = Lang::get('COMMON.SUSPENDED');

        return $options;
    }

    function getStatusText($module = false) {

        if($this->id == 0 || !isset($this->status)) return false;

        if($this->status == 1) {
            $action = 'suspend';
            $str = Lang::get('COMMON.ACTIVE');
        } else if($this->status == 0) {
            $action = 'activate';
            $str = Lang::get('COMMON.SUSPENDED');
        }

        if($module)
            $str = '<a href="#" onclick="return doConfirmAction(\''.$module.'-active-suspend\', \''.$action.'\', \''.$this->id.'\')">'.$str.'</a>';

        return $str;
    }

    public static function getInstantTransFlagOptions($prependOptionAll = false) {

        $options = [];

        if ($prependOptionAll) {
            $options[''] = Lang::get('COMMON.ALL');
        }

        $options[strval(Cashledger::INSTANT_TRANS_FLAG_DEFAULT)] = Lang::get('COMMON.MANUAL');
        $options[strval(Cashledger::INSTANT_TRANS_FLAG_HIDDEN)] = Lang::get('COMMON.AUTO');

        return $options;
    }

    public static function getPrivateTypeOptions($prependOptionAll = false) {
        $options = [];

        if ($prependOptionAll) {
            $options[''] = Lang::get('COMMON.ALL');
        }

        $options[self::TYPE_WECHAT] = '微信支付';
        $options[self::TYPE_ALIPAY] = '支付宝';

        return $options;
    }

    public function getImage($type = Media::TYPE_IMAGE) {
        if($this->id == 0) return false;

        if($object = Media::whereRaw('refobj = "PaymentGatewayObject" AND refid = '.$this->id.' AND type = '.$type)->first()) {
            return $object->domain.'/'.$object->path;
        }

        return false;
    }

    public static function generateCode($privateType) {
        $count = Paymentgatewaysetting::where('private_type', '=', $privateType)->count();
        $code = 'PP';

        if ($privateType == self::TYPE_ALIPAY) {
            $code = 'ZF';
        } elseif ($privateType == self::TYPE_WECHAT) {
            $code = 'WX';
        }

        return $code.$count;
    }
}
