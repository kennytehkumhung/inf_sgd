<?php namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;

class FortuneWheelTransIdr extends Model {

    protected $table = 'fortune_wheel_trans_idr';

    // GAME_ACT and STATUS follow back Models\FortuneWheelTrans

    public static function getTotalWon($accid, $startdate = null, $enddate = null) {

        $builder = DB::table('fortune_wheel_trans_idr as a')
            ->join('fortune_wheel_match_idr as b', 'b.id', '=', 'a.match_id')
            ->where('b.status', '=', FortuneWheelMatch::STATUS_CLOSED)
            ->where('a.prize_type', '!=', 'token')
            ->where('a.status', '=', FortuneWheelTrans::STATUS_OPENING)
            ->groupBy('a.match_id')
            ->selectRaw('MAX(a.id) as id');

        if (!is_null($accid)) {
            $builder->where('a.accid', '=', $accid);
        }

        if (!is_null($startdate)) {
            $builder->where('a.created_at', '>=', $startdate);
        }

        if (!is_null($enddate)) {
            $builder->where('a.created_at', '<=', $enddate);
        }

        $totalWon = FortuneWheelTransIdr::whereIn('id', $builder->lists('id'))->sum('amount');

        return $totalWon;
    }
}
