<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Accounttag extends Model {

	protected $table = 'accounttag';
	 
	const CREATED_AT = 'created';

    const UPDATED_AT = 'modified';

}
