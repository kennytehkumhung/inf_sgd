<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Lang;
use App\libraries\App;
use App\Models\Negativebalance;
use App\Models\Product;
use App\Models\Account;
use App\Models\Accountproduct;

class Cashledger extends Model {

	protected $table = 'cashledger';
	 
	const CREATED_AT = 'created';

    const UPDATED_AT = 'modified';

    const INSTANT_TRANS_FLAG_DEFAULT = 0;
    const INSTANT_TRANS_FLAG_HIDDEN = 1;

	function getStatusText($status = null, $color = false){
		if($status == null && $this->id != 0) $status = $this->status;
		
		if($color) {
			if(CBO_LEDGERSTATUS_CONFIRMED == $status)
			return '<font color="green">'.Lang::get('COMMON.AUTO_CONFIRMED').'</font>';
			else if(CBO_LEDGERSTATUS_PENDING == $status)
			return '<font color="blue">'.Lang::get('COMMON.NEWTRANSACTION').'</font>';
			else if(CBO_LEDGERSTATUS_CANCELLED == $status)
			return '<font color="red">'.Lang::get('COMMON.AUTO_CANCELLED').'</font>';
			else if(CBO_LEDGERSTATUS_MANPENDING  == $status)
			return '<font color="#306EFF">'.Lang::get('COMMON.PROCESSING').'</font>';
			else if(CBO_LEDGERSTATUS_MANCONFIRMED == $status)
			return '<font color="green">'.Lang::get('COMMON.APPROVED').'</font>';
			else if(CBO_LEDGERSTATUS_MANCANCELLED == $status)
			return '<font color="red">'.Lang::get('COMMON.REJECT').'</font>';
			else if(CBO_LEDGERSTATUS_MEMCANCELLED == $status)
			return '<font color="red">'.Lang::get('COMMON.MEMBERCANCELLED').'</font>';
			else if(CBO_LEDGERSTATUS_PROCESSED == $status)
			return '<font color="#5CB3FF">'.Lang::get('COMMON.PROCESSED').'</font>';
		} else {
			if(CBO_LEDGERSTATUS_CONFIRMED == $status)
			return Lang::get('COMMON.AUTO_CONFIRMED');
			else if(CBO_LEDGERSTATUS_PENDING == $status)
			return Lang::get('COMMON.NEWTRANSACTION');
			else if(CBO_LEDGERSTATUS_CANCELLED == $status)
			return Lang::get('COMMON.AUTO_CANCELLED');
			else if(CBO_LEDGERSTATUS_MANPENDING  == $status)
			return Lang::get('COMMON.PROCESSING');
			else if(CBO_LEDGERSTATUS_MANCONFIRMED == $status)
			return Lang::get('COMMON.APPROVE');
			else if(CBO_LEDGERSTATUS_MANCANCELLED == $status)
			return Lang::get('COMMON.REJECT');
			else if(CBO_LEDGERSTATUS_MEMCANCELLED == $status)
			return Lang::get('COMMON.MEMBERCANCELLED');
			else if(CBO_LEDGERSTATUS_PROCESSED == $status)
			return Lang::get('COMMON.PROCESSED');
		}
		
		//CBO_LEDGERSTATUS_PROCESSED
	}

	/**
	* Return the current object status color code.
	*
	*
	* @return string The object status color code
	*/
	function getStatusTextColorCode(){
		if($this->id == 0) return false;

		if($this->status == CBO_LEDGERSTATUS_CONFIRMED)
		return '#44862D';
		else if($this->status == CBO_LEDGERSTATUS_MANCONFIRMED)
		return '#18833D';
		else if($this->status == CBO_LEDGERSTATUS_CANCELLED)
		return '#E77471';
		else if($this->status == CBO_LEDGERSTATUS_MANCANCELLED)
		return '#E77471';
		else if($this->status == CBO_LEDGERSTATUS_PENDING)
		return '#0000CE';
		else if($this->status == CBO_LEDGERSTATUS_MANPENDING)
		return '#0099FF';
		else if($this->status == CBO_LEDGERSTATUS_MEMCANCELLED)
		return '#E77471';
		else if($this->status == CBO_LEDGERSTATUS_PROCESSED)
		return '#0099FF';
	}
	
	/**
	* Return the current object status color code.
	*
	*
	* @return string The object status color code
	*/
	function getStatusColorCode(){
		if($this->id == 0) return false;

		if($this->status == CBO_LEDGERSTATUS_CONFIRMED)
		return '#99CC67';
		else if($this->status == CBO_LEDGERSTATUS_MANCONFIRMED)
		return '#9DFCCD';
		else if($this->status == CBO_LEDGERSTATUS_CANCELLED)
		return '#CE0134';
		else if($this->status == CBO_LEDGERSTATUS_MANCANCELLED)
		return '#CE0134';
		else if($this->status == CBO_LEDGERSTATUS_PENDING)
		return '#33FFFE';
		else if($this->status == CBO_LEDGERSTATUS_MANPENDING)
		return '#0000CA';
		else if($this->status == CBO_LEDGERSTATUS_MEMCANCELLED)
		return '#CE0134';
		else if($this->status == CBO_LEDGERSTATUS_PROCESSED)
		return '#2B60DE';
	}

	/**
	* Return an array of cash ledger status options
	*
	*
	* @return array Array of cash ledger status key and value in text
	*/
	static function getStatusOptions(){
		$status = array(
			CBO_LEDGERSTATUS_PENDING 		=> Lang::get('COMMON.NEWTRANSACTION'),
			CBO_LEDGERSTATUS_CONFIRMED		=> Lang::get('COMMON.AUTO_CONFIRMED'),
			CBO_LEDGERSTATUS_CANCELLED 		=> Lang::get('COMMON.AUTO_CANCELLED'),
			CBO_LEDGERSTATUS_MANPENDING 	=> Lang::get('COMMON.PROCESSING'),
			CBO_LEDGERSTATUS_PROCESSED 		=> Lang::get('COMMON.PROCESSED'),
			CBO_LEDGERSTATUS_MANCONFIRMED	=> Lang::get('COMMON.APPROVED'),
			CBO_LEDGERSTATUS_MANCANCELLED 	=> Lang::get('COMMON.REJECT'),
			CBO_LEDGERSTATUS_MEMCANCELLED 	=> Lang::get('COMMON.MEMBERCANCELLED'),
		);

		return $status;
	}

	/**
	* Get all reference object as array
	*
	* @param boolean $withdraw 	Flag to select the funding method that support withdrawal
	*
	* @access	public
	* @return 	array/boolean Array of reference object key and value in text.
	*/
	function getRefObjAsFundingMethodOptions($withdraw = false) {
		if($withdraw){
			$fundingMethod = array(
				CBO_LEDREFOBJ_BANKTRANSFER 	=> Lang::get('COMMON.BANKTRANSFER'),
				CBO_LEDREFOBJ_CHEQUE 		=> Lang::get('COMMON.CHEQUE'),
				CBO_LEDREFOBJ_NETELLER	 	=> Lang::get('COMMON.NETELLER'),
				CBO_LEDREFOBJ_NETELLER1PAY	=> Lang::get('COMMON.NETELLER1PAY'),
				CBO_LEDREFOBJ_MONEYBOOKER	=> Lang::get('COMMON.MONEYBOOKER'),
			);
		}else{
			$fundingMethod = array(
				CBO_LEDREFOBJ_BANKTRANSFER 	=> Lang::get('COMMON.BANKTRANSFER'),
				CBO_LEDREFOBJ_CCTRANSFER 	=> Lang::get('COMMON.CREDITCARD'),
				CBO_LEDREFOBJ_CHEQUE 		=> Lang::get('COMMON.CHEQUE'),
				CBO_LEDREFOBJ_NETELLER	 	=> Lang::get('COMMON.NETELLER'),
				CBO_LEDREFOBJ_NETELLER1PAY	=> Lang::get('COMMON.NETELLER1PAY'),
				CBO_LEDREFOBJ_MONEYBOOKER	=> Lang::get('COMMON.MONEYBOOKER'),
			);
		}

		return $fundingMethod;
	}

	/**
	* Return the reference object in text.
	*
	* @param integer $refObj	The refobj code
	*
	* @return string The object refobj in language text
	*/
	function getRefObjectText($refObj = null){
		if($refObj == null && $this->id != 0) $refObj = $this->refobj;

		if($refObj == 1)
		return Lang::get('COMMON.BETSLIP');
		else if($refObj == 2)
		return Lang::get('COMMON.BANKTRANSFER');
		else if($refObj == 3)
		return Lang::get('COMMON.CREDITCARD');
		else if($refObj == 4)
		return Lang::get('COMMON.CHEQUE');
		else if($refObj == 5)
		return Lang::get('COMMON.EARTHPORT');
		else if($refObj == 6)
		return Lang::get('COMMON.NETELLER');
		else if($refObj == 7)
		return Lang::get('COMMON.ADJUSTMENT');
		else if($refObj == 8)
		return Lang::get('COMMON.CHARGEFEE');	
		else if($refObj == 9)
		return Lang::get('COMMON.CASHLEDGER');
		else if($refObj == 10)
		return Lang::get('COMMON.NETELLER1PAY');
		else if($refObj == 11)
		return Lang::get('COMMON.MONEYBOOKER');

	}

	/**
	* Return the reference object's class name.
	*
	* @param integer $refObj	The refobj code
	*
	* @return string The object refobj in class name
	*/
	function getRefObjectName($refObj = null){
		if($refObj == null && $this->id != 0) $refObj = $this->refobj;

		if($refObj == CBO_LEDREFOBJ_BETSLIP)
		return Lang::get('COMMON.BETSLIPOBJECT');
		else if($refObj == CBO_LEDREFOBJ_BANKTRANSFER)
		return Lang::get('COMMON.BANKTRANSFEROBJECT');
		else if($refObj == CBO_LEDREFOBJ_CCTRANSFER)
		return Lang::get('COMMON.CCTRANSFEROBJECT');
		else if($refObj == CBO_LEDREFOBJ_CHEQUE)
		return Lang::get('COMMON.CHEQUEOBJECT');
		else if($refObj == CBO_LEDREFOBJ_EARTHPORT)
		return Lang::get('COMMON.EARTHPORTOBJECT');
		else if($refObj == CBO_LEDREFOBJ_NETELLER)
		return Lang::get('COMMON.NETELLEROBJECT');
		else if($refObj == CBO_LEDREFOBJ_ADJUSTMENT)
		return Lang::get('COMMON.ADJUSTMENTOBJECT');
		else if($refObj == CBO_LEDREFOBJ_CHARGEFEE)
		return Lang::get('COMMON.CHARGEFEEOBJECT');
		else if($refObj == CBO_LEDREFOBJ_CASHLEDGER)
		return Lang::get('COMMON.CASHLEDGEROBJECT');	
		else if($refObj == CBO_LEDREFOBJ_NETELLER1PAY)
		return Lang::get('COMMON.NETELLER1PAYOBJECT');	
		else if($refObj == CBO_LEDREFOBJ_MONEYBOOKER)
		return Lang::get('COMMON.MONEYBOOKEROBJECT');

	}
	
	/**
	* Return the reference object's class name.
	*
	* @param string $refObjText	The refobj text
	*
	* @return integer The constant for the refobj.
	*/
	function getRefObjectConstant($refObjText){

		if($refObjText == Lang::get('COMMON.BETSLIPOBJECT'))
		return CBO_LEDREFOBJ_BETSLIP;
		else if($refObjText == Lang::get('COMMON.BANKTRANSFEROBJECT'))
		return CBO_LEDREFOBJ_BANKTRANSFER;
		else if($refObjText == Lang::get('COMMON.CCTRANSFEROBJECT'))
		return CBO_LEDREFOBJ_CCTRANSFER;
		else if($refObjText == Lang::get('COMMON.CHEQUEOBJECT'))
		return CBO_LEDREFOBJ_CHEQUE;
		else if($refObjText == Lang::get('COMMON.EARTHPORTOBJECT'))
		return CBO_LEDREFOBJ_EARTHPORT;
		else if($refObjText == Lang::get('COMMON.NETELLEROBJECT'))
		return CBO_LEDREFOBJ_NETELLER;
		else if($refObjText == Lang::get('COMMON.ADJUSTMENTOBJECT'))
		return CBO_LEDREFOBJ_ADJUSTMENT;
		else if($refObjText == Lang::get('COMMON.CHARGEFEEOBJECT'))
		return CBO_LEDREFOBJ_CHARGEFEE;	
		else if($refObjText == Lang::get('COMMON.NETELLER1PAYOBJECT'))
		return CBO_LEDREFOBJ_NETELLER1PAY;
		else if($refObjText == Lang::get('COMMON.MONEYBOOKEROBJECT'))
		return CBO_LEDREFOBJ_MONEYBOOKER;

	}
	
	/**
	* Return the formatted value for displaying purpose.
	*
	* @param integer $value		The value index
	*
	* @return string 			The formatted value
	*/
	function formatCancelledAmount($value){
		if($this->id == 0) return false;

		if(strlen($value) == 0)
			return '';
		else if($this->status == CBO_LEDGERSTATUS_CANCELLED || $this->status == CBO_LEDGERSTATUS_MANCANCELLED || $this->status == CBO_LEDGERSTATUS_MEMCANCELLED)
			return '<s>'.App::displayAmount($value).'</s>';
		else
			return App::displayAmount($value);
	}
	

	/**
	* Return the details/description/remarks of the object
	*
	*
	* @return string 	$details		The details
	*/
	static function getAllTypeAsOptions($excludes = false){
		$types = array(
			CBO_CHARTCODE_DEPOSIT 			=> Lang::get('COMMON.DEPOSIT'),
			CBO_CHARTCODE_WITHDRAWAL		=> Lang::get('COMMON.WITHDRAWAL'),
			CBO_CHARTCODE_WITHDRAWALCHARGES	=> Lang::get('COMMON.WITHDRAWALCHARGES'),
			CBO_CHARTCODE_BONUS 			=> Lang::get('COMMON.BONUS'),
			CBO_CHARTCODE_ADJUSTMENT 		=> Lang::get('COMMON.ADJUSTMENT'),
			CBO_CHARTCODE_INCENTIVE 		=> Lang::get('COMMON.INCENTIVE'),
			CBO_CHARTCODE_DAILYREBATE 		=> Lang::get('COMMON.DAILYREBATE'),
			CBO_CHARTCODE_BALANCETRANSFER 	=> Lang::get('COMMON.TRANSFER'),
			//CBO_CHARTCODE_GAMETRANS		=> Lang::get('COMMON.WAGER'),
			//CBO_CHARTCODE_GAMEPAYOUT 		=> Lang::get('COMMON.PAYOUT'),
			//CBO_CHARTCODE_CANCELMATCH 	=> Lang::get('COMMON.CANCELLEDMATCH'),
			//CBO_CHARTCODE_CANCELWAGER 	=> Lang::get('COMMON.CANCELLEDBET'),
		);
		
		if(is_array($excludes)) {
			foreach($excludes as $exclude) {
				if(isset($types[$exclude])) unset($types[$exclude]);
			}
		}

		return $types;
	}
	
	/**
	* Return the details/description/remarks of the object
	*
	*
	* @return string 	$details		The details
	*/
	public static function getTypeText($type = null){

		if($type == CBO_CHARTCODE_DEPOSIT)
		return Lang::get('COMMON.DEPOSIT');
		else if($type == CBO_CHARTCODE_WITHDRAWAL)
		return Lang::get('COMMON.WITHDRAWAL');
		else if($type == CBO_CHARTCODE_BONUS)
		return Lang::get('COMMON.BONUS');
		else if($type == CBO_CHARTCODE_ADJUSTMENT)
		return Lang::get('COMMON.ADJUSTMENT');
		else if($type == CBO_CHARTCODE_GAMETRANS)
		return Lang::get('COMMON.WAGER');
		else if($type == CBO_CHARTCODE_GAMEPAYOUT)
		return Lang::get('COMMON.PAYOUT');
		else if($type == CBO_CHARTCODE_CANCELMATCH)
		return Lang::get('COMMON.CANCELLEDMATCH');
		else if($type == CBO_CHARTCODE_CANCELWAGER)
		return Lang::get('COMMON.CANCELLEDBET');
		else if($type == CBO_CHARTCODE_INCENTIVE)
		return Lang::get('COMMON.INCENTIVE');
		else if($type == CBO_CHARTCODE_DAILYREBATE)
		return Lang::get('COMMON.DAILYREBATE');
		else if($type == CBO_CHARTCODE_BALANCETRANSFER)
		return Lang::get('COMMON.TRANSFER');
		else if($type == CBO_CHARTCODE_CASHCARD)
		return Lang::get('COMMON.CASHCARD');
	}
	
	function getTransactionId(){
	
		if($this->id == 0) return false;
		
		return substr($this->id, -6, 6);
	}
	
	function updateAccountBalance($accid, $chtcode = '') {
		
			$accountObj = Account::find($accid);
			$cashBalance = $this->getBalance($accountObj->id);
		
			if($cashBalance < 0){
				//$this->addNegativeBalanceLog($accid,$cashBalance,$chtcode);
			}
		
			if($chtcode == CBO_CHARTCODE_GAMETRANS){
				$accountObj->lastbetdate = App::getDateTime();
			}
			
			$accountObj->cashbalance = $cashBalance;
			$accountObj->save();	
	}
	
	public static function getBalance($accid = null, $date = '', $chartcode = 0, $localamount = false, $bbf = false) {
		
		if($date == '')	$date = App::getDateTime();

		$condition = '((status in ('.CBO_LEDGERSTATUS_PENDING.', '.CBO_LEDGERSTATUS_MANPENDING.','.CBO_LEDGERSTATUS_CONFIRMED.', '.CBO_LEDGERSTATUS_MANCONFIRMED.', '.CBO_LEDGERSTATUS_PROCESSED.') AND chtcode IN ('.CBO_CHARTCODE_WITHDRAWAL.', '.CBO_CHARTCODE_BALANCETRANSFER.', '.CBO_CHARTCODE_WITHDRAWALCHARGES.' )) OR (status in ('.CBO_LEDGERSTATUS_CONFIRMED.', '.CBO_LEDGERSTATUS_MANCONFIRMED.') AND chtcode NOT IN ('.CBO_CHARTCODE_WITHDRAWAL.', '.CBO_CHARTCODE_BALANCETRANSFER.' , '.CBO_CHARTCODE_WITHDRAWALCHARGES.' )))';
		if($bbf)
		$condition .= ' AND created < "'. $date . '"';
		else
		$condition .= ' AND created <= "'. $date . '"';
		
		if($localamount)
			$field = 'amountlocal';
		else
			$field = 'amount';

		if($accid != null)
			$condition .= ' AND accid = '. $accid;
		
		if($chartcode != 0)
			$condition .= ' AND chtcode = '.$chartcode;

		if($balance = Cashledger::whereRaw($condition)->sum($field) ){
			$balance = $balance;
		}else{
			$balance = 0;
		}
		
		if( $products = Product::whereStatus(1)->get() ){
			
			foreach( $products as $product ){
				
				$accObj = Account::find($accid);
				
				if(Accountproduct::whereAcccode($accObj->nickname)->wherePrdid($product->prdid)->count() == 1){
				
				
					
					$className  = "App\libraries\providers\\".$product->code;
					$obj 		= new $className;
					$temp       = $obj->getBalance( $accObj->nickname , $accObj->crccode );
					
					$temp = ($temp == false) ? 0 : $temp;
					$temp = $accObj->crccode == 'VND' || $accObj->crccode == 'IDR' ? $temp * 1000 : $temp;
					
					$balance +=  $temp ;
				
				}
			}
		}
		
		return App::float($balance);
		
	}
	
	function addNegativeBalanceLog($accid,$cashbalance,$chtcode){
		
		$negativeBalanceObj = new Negativebalance;
		$negativeBalanceObj->accid 		 = $accid;
		$negativeBalanceObj->cashbalance = $cashbalance;
		$negativeBalanceObj->chtcode 	 = $chtcode;
		$negativeBalanceObj->save();
		
	}
	
	function updateDepositWithdrawalAmt($accid, $chtcode = '', $amount){

		if($accountObj = Account::find($accid)){
			if(CBO_CHARTCODE_DEPOSIT == $chtcode){
				$accountObj->totaldepositamt = $accountObj->totaldepositamt + $amount;
				$accountObj->totaldeposit += 1;
			}elseif(CBO_CHARTCODE_WITHDRAWAL == $chtcode){
				$accountObj->totalwithdrawalamt  = $accountObj->totalwithdrawalamt - $amount;
				$accountObj->totalwithdrawal += 1;
			}
			
			$accountObj->save();
		}
	}
	
	function addEditPromoCashLedger($fdmid,$clgid,$status)
	{
		
		$rebate		  = true;
		
		
		
	 	if( $promoCashObj = Promocash::whereRaw('clgid ='.$clgid.' AND status = '.CBO_PROMOCASHSTATUS_CONFIRMED)->first() )
		{
			
		
			$starttime = date('Y-m-d').' 00:00:00';
			
			$method = Promocampaign::whereId( $promoCashObj->pcpid )->pluck('method');
			
			if( $method == Promocampaign::CBO_METHOD_DAILYDEPOSIT ){
				
				$total = Promocash::whereRaw('accid='.$promoCashObj->accid.' AND pcpid='.$promoCashObj->pcpid.' AND status= '.CBO_PROMOCASHSTATUS_CONFIRMED.' AND created >= "'.$starttime.'"')->count();
				
				if( $total > 1 ){
						$promoCashObj->status = CBO_PROMOCASHSTATUS_REJECTED;
						$promoCashObj->save();
						return true;
				}
			}
			

			
		
			
			if( CBO_LEDGERSTATUS_CONFIRMED == $status || CBO_LEDGERSTATUS_MANCONFIRMED == $status )
			{
				$rebate = false;
				if( Cashledger::whereRaw('chtcode='.CBO_CHARTCODE_BONUS.' AND refobj="Promocash" AND refid='.$promoCashObj->id.'')->count() == 0 )
				{
					$accObj = Account::find($promoCashObj->accid);
					$clgObj = Cashledger::find($clgid);
					
					$cashLedgerObj = new Cashledger;
					$cashLedgerObj->accid 			= $promoCashObj->accid;
					$cashLedgerObj->acccode 		= $promoCashObj->acccode;
					$cashLedgerObj->accname 		= $accObj->fullname;
					$cashLedgerObj->chtcode 		= CBO_CHARTCODE_BONUS;
					$cashLedgerObj->remarkcode 		= $promoCashObj->remarkcode;
					$cashLedgerObj->amount 			= $promoCashObj->amount;
					$cashLedgerObj->amountlocal 	= $promoCashObj->amountlocal;
					$cashLedgerObj->fee 			= $promoCashObj->amount;
					$cashLedgerObj->feelocal 		= $promoCashObj->amountlocal;
					$cashLedgerObj->crccode 		= $promoCashObj->crccode;
					$cashLedgerObj->crcrate 		= $promoCashObj->crcrate;
					$cashLedgerObj->refobj			= 'Promocash';
					$cashLedgerObj->refid			= $promoCashObj->id;
					$cashLedgerObj->cashbalance		= $clgObj->cashbalance + $clgObj->amount;
					$cashLedgerObj->status			= CBO_LEDGERSTATUS_PENDING;
					$cashLedgerObj->createdip		= $promoCashObj->createdip;
					$cashLedgerObj->save();
					
				}
			}elseif(CBO_LEDGERSTATUS_CANCELLED == $status || CBO_LEDGERSTATUS_MANCANCELLED == $status)
			{
				$promoCashObj->status = CBO_PROMOCASHSTATUS_REJECTED;
				$promoCashObj->save();
			}
		} 
		
	
		
		//add deposit rebate bonus
		if($fdmid == 1 && (CBO_LEDGERSTATUS_CONFIRMED == $status || CBO_LEDGERSTATUS_MANCONFIRMED == $status)) 
		{
			if($clgObj = Cashledger::find($clgid)) 
			{
				$is_claim_special_tag_bonus = false;
				
				if($special_tags = Accounttag::where( 'accid' , '=' , $clgObj->accid )->where( 'status' , '=' , 1 )->whereRaw(' tagid IN (select id from tag where special = 1) ')->get()){
					
					foreach( $special_tags as $special_tag )
					{
						$temp_tagids[] = $special_tag->tagid;
					}
					
					if( isset($temp_tagids) && count($temp_tagids) > 0 )
						$tagid = ' tag IN ('.implode(',',$temp_tagids).') AND ';
					
				}
				
				$querys[] = 'tag = 0 AND ';
		
				//query for member who having this tag
				if( isset($tagid) ){
					$querys[] = $tagid;
				}
		
		
		
				$agtid = 0;
				if($accObj = Account::find($clgObj->accid))
				{
					$agtid = $accObj->agtid;
				}
				$pcpid 	  = 0;
				$prdid 	  = 0;
				$amount   = 0;
				$turnover = 0;
				$now      = date('Y-m-d H:i:s');
				$fromtime = date('Y-m-d').' 00:00:00';
				$totime   = date('Y-m-d').' 23:59:59';
				
				$maxamount = 0;
				
				if( Accounttag::whereRaw('accid = '.$clgObj->accid.' AND tagid = 3 AND status = 1')->count() == 0 ) 
				{	
					foreach( $querys as $default )
					{
						if($is_claim_special_tag_bonus == false)
						{
							
							if($pcpObj = Promocampaign::whereRaw($default.'crccode = "'.$clgObj->crccode.'" AND agtid = 0 AND (method='.Promocampaign::CBO_METHOD_DEPOSITREBATE.' || method ='.Promocampaign::CBO_METHOD_EVERYTIME.') AND status='.CBO_STANDARDSTATUS_ACTIVE.'  AND startdate <= "'.$now.'" AND enddate >= "'.$now.'" ORDER BY id DESC')->first()) 
							{
								$pcpid 	  = $pcpObj->id;
								$prdid 	  = $pcpObj->prdid;
								$turnover = $pcpObj->turnover;
								$amount   = $clgObj->amount * $pcpObj->amount / 100;
								if($pcpObj->maxamount != 0) 
								{
									$maxamount = $pcpObj->maxamount;
									if($amount > $pcpObj->maxamount)
										$amount = $pcpObj->maxamount;
								}
								$is_claim_special_tag_bonus = true;
							}
							if($pcpObj = Promocampaign::whereRaw($default.'crccode = "'.$clgObj->crccode.'" AND agtid = '.$agtid.' AND (method='.Promocampaign::CBO_METHOD_DEPOSITREBATE.' || method ='.Promocampaign::CBO_METHOD_EVERYTIME.') AND status='.CBO_STANDARDSTATUS_ACTIVE.'  AND startdate <= "'.$now.'" AND enddate >= "'.$now.'" ORDER BY id DESC')->first()) 
							{
								$pcpid	  = $pcpObj->id;
								$prdid 	  = $pcpObj->prdid;
								$turnover = $pcpObj->turnover;
								$amount   = $clgObj->amount * $pcpObj->amount / 100;
								if($pcpObj->maxamount != 0) 
								{
									$maxamount = $pcpObj->maxamount;
									if($amount > $pcpObj->maxamount)
									$amount = $pcpObj->maxamount;
								}
								$is_claim_special_tag_bonus = true;
							}
						
						}
					
					}
				}
				
				//check daily deposit rebate limit
				if($maxamount != 0) {
					$old_amount = Promocash::whereRaw('accid='.$clgObj->accid.' AND pcpid='.$pcpid.' AND created >= "'.$fromtime.'" AND created <= "'.$totime.'"')->sum('amount');
					$maxamount = $maxamount - $old_amount;
					if($maxamount > 0) {
						if($amount > $maxamount)
						$amount = $maxamount;
					} else $amount = 0;
				}
				
			
				if($amount > 0 && $rebate && $pcpid > 0) 
				{
					if( Promocash::whereRaw('clgid = '.$clgObj->id)->count() == 0 ) 
					{
						$object = new Promocash;
						$object->type			= Promocash::TYPE_MANUAL;
						$object->accid	 		= $clgObj->accid;
						$object->acccode	 	= $clgObj->acccode;
						$object->crccode	 	= $clgObj->crccode;
						$object->crcrate	 	= Currency::getCurrencyRate($clgObj->crccode);
						$object->clgid			= $clgObj->id;
						$object->amount	 		= $amount;
						$object->amountlocal	= Currency::getLocalAmount($object->amount, $clgObj->crccode);
						$object->pcpid			= $pcpid;
						$object->status			= Promocash::STATUS_PENDING;
						if($object->save()) 
						{
							$cashLedgerObj = new Cashledger;
							$cashLedgerObj->accid 			= $object->accid;
							$cashLedgerObj->acccode 		= $object->acccode;
							$cashLedgerObj->accname 		= $accObj->fullname;
							$cashLedgerObj->chtcode 		= CBO_CHARTCODE_BONUS;
							$cashLedgerObj->remarkcode 		= '';
							$cashLedgerObj->amount 			= $object->amount;
							$cashLedgerObj->amountlocal 	= $object->amountlocal;
							$cashLedgerObj->fee 			= $object->amount;
							$cashLedgerObj->feelocal 		= $object->amountlocal;
							$cashLedgerObj->crccode 		= $object->crccode;
							$cashLedgerObj->crcrate 		= $object->crcrate;
							$cashLedgerObj->refobj			= 'Promocash';
							$cashLedgerObj->refid			= $object->id;
							$cashLedgerObj->cashbalance		= $clgObj->cashbalance + $clgObj->amount;
							$cashLedgerObj->status			= CBO_LEDGERSTATUS_PENDING;
							$cashLedgerObj->createdip		= '';
							$cashLedgerObj->save();
						} 
					}
				}
			}
		}
		
	}
	
		
	public static function getAuditTrans($userId, $reset = false, $endDate){
	
		$turnoverAmount  = 0;
		$validStake 	 = 0;
		$withdrawAmount  = 0;
		$count = 0;
		
		$transactionrow  = array();
		$firsDepositDate = false;
		$entryDate       = '';

		//search latest confirm withdraw
		$sql 	   = ' AND chtcode = '.CBO_CHARTCODE_WITHDRAWAL.' AND (status = '.CBO_LEDGERSTATUS_CONFIRMED.' or status = '.CBO_LEDGERSTATUS_MANCONFIRMED.' ) AND created < "'.$endDate.'" order by created desc'; 
		$lateswithdraw = $cashLedgerObj = cashledger::whereRaw('accid = '.$userId.$sql)->first();
		//var_dump($cashLedgerObj->created);
		if($lateswithdraw){
			$sql 	   = ' AND chtcode IN ('.CBO_CHARTCODE_WITHDRAWAL.', '.CBO_CHARTCODE_DEPOSIT.', '.CBO_CHARTCODE_ADJUSTMENT.' , '.CBO_CHARTCODE_BONUS.' , '.CBO_CHARTCODE_INCENTIVE.' , '.CBO_CHARTCODE_DAILYREBATE.') AND (status = '.CBO_LEDGERSTATUS_CONFIRMED.' or status = '.CBO_LEDGERSTATUS_MANCONFIRMED.' or status = '.CBO_LEDGERSTATUS_PENDING.' or status = '.CBO_LEDGERSTATUS_MANPENDING.' or status = '.CBO_LEDGERSTATUS_PROCESSED.') AND created >= "'.$cashLedgerObj->created.'" AND created <= "'.$endDate.'" '; 
		}else{
			$sql 	   = ' AND chtcode IN ('.CBO_CHARTCODE_WITHDRAWAL.','.CBO_CHARTCODE_DEPOSIT.', '.CBO_CHARTCODE_ADJUSTMENT.' , '.CBO_CHARTCODE_BONUS.' , '.CBO_CHARTCODE_INCENTIVE.' , '.CBO_CHARTCODE_DAILYREBATE.') AND (status = '.CBO_LEDGERSTATUS_CONFIRMED.' or status = '.CBO_LEDGERSTATUS_MANCONFIRMED.' or status = '.CBO_LEDGERSTATUS_PENDING.' or status = '.CBO_LEDGERSTATUS_MANPENDING.' or status = '.CBO_LEDGERSTATUS_PROCESSED.') AND created <= "'.$endDate.'" '; 
		}
		if($transactions = Cashledger::whereRaw('accid = '.$userId.$sql)->orderBy('created','asc')->get()){

		
			
			//get all confirm transaction from cashledger
			foreach($transactions as $transaction){
				$store_amount[$count] = $transaction->amount;
					$creditAmount  = false;
					$debitAmount  = false;
					
					if(isset($params['sreset'])){
						foreach($params as $key){
							if(substr($key, 0, 8) == 'resetsid'){
								$reset_sid = explode('_', $key);
								if($reset_sid[1] == $transaction->id){
									$turnoverAmount  = 0;
									$firsDepositDate = $transaction->modified;
								}
							}
						}
					}
					
				
				 
				$transactionType = Cashledger::getTypeText($transaction->chtcode);
				if($transaction->status == CBO_LEDGERSTATUS_MANPENDING || $transaction->status == CBO_LEDGERSTATUS_PENDING ){
					$transactionType .= ' ['.Lang::get('COMMON.PENDINGTRANSACTION').']';	
					
				}elseif($transaction->status == CBO_LEDGERSTATUS_PROCESSED){
				    $transactionType .= ' ['.Lang::get('COMMON.PROCESSED').']';
				}
				if($transaction->amount < 0) {
					$debitAmount = $transaction->amount * -1;
				} else $creditAmount = $transaction->amount;
						
				//promocash situation
				if($transaction->refobj == "Promocash"){
					if($promocashObj = Promocash::find($transaction->refid)){
						$turnover = 0;
						if($promocampaignObj = Promocampaign::find($promocashObj->pcpid)){
							$transactionStatus = $promocampaignObj->code;
							if($promocampaignObj->turnover == 0){
								$turnover = 1;
							}else{
								$turnover = $promocampaignObj->turnover;
							}
							$transactionType .= ' ['.$turnover.' X ]';
						}
		
						if($refCashLedger = Cashledger::find($promocashObj->clgid)){
							$transactionStatus = $transactionStatus.' ['.$refCashLedger->getTransactionId($refCashLedger->id).']';
						}else{
							$refCashLedger = new Cashledger;
							$refCashLedger->amount = $store_amount[$count-1];
						}
					}
						//caculate turn over
						
						$turnoverAmount = ($refCashLedger->amount + $transaction->amount) * $turnover;

						//caculate turn over
				}else{
					$transactionStatus = '';
			
					if($transaction->chtcode == CBO_CHARTCODE_DEPOSIT || $transaction->chtcode == CBO_CHARTCODE_ADJUSTMENT || $transaction->chtcode == CBO_CHARTCODE_ADJUSTMENT)
					{	
							$turnoverAmount = $transaction->amount;
					}
				}
						
				$difer = $turnoverAmount - $validStake;
				$difer = $difer * -1;
				if($difer <0 ){
					$difer = App::formatNegativeAmount($difer);
				}
	
				$checkbox = '<input type="checkbox" name="s'.$transaction->id.'" value="resetsid_'.$transaction->id.'" >';
		
				$transactionrow[] = array(
					$transaction->created, 
					$transactionType,
					$transaction->getTransactionId(),
					$transactionStatus,
					($creditAmount)?App::displayNumberFormat($creditAmount):'',
					($debitAmount)?App::displayNumberFormat($debitAmount):'',
					$turnoverAmount,
					$validStake,
					$difer,
					$transaction->cashbalance,
					$transaction->id,
					$transaction->auditflag,
					$turnoverAmount
				);
		
				$turnoverAmount  = '';
				$withdrawAmount  = '';
				$count++;
			}
		}
		
		return $transactionrow;
	}
	
	public static function getNextTrans($userId, $date){
	
		$cashLedgerObj 	 = new self;
		
		$sql 	   = ' AND chtcode IN ('.CBO_CHARTCODE_WITHDRAWAL.','.CBO_CHARTCODE_DEPOSIT.', '.CBO_CHARTCODE_ADJUSTMENT.' , '.CBO_CHARTCODE_BONUS.' , '.CBO_CHARTCODE_INCENTIVE.' , '.CBO_CHARTCODE_DAILYREBATE.') AND (status = '.CBO_LEDGERSTATUS_CONFIRMED.' or status = '.CBO_LEDGERSTATUS_MANCONFIRMED.' or status = '.CBO_LEDGERSTATUS_PENDING.' or status = '.CBO_LEDGERSTATUS_MANPENDING.' or status = '.CBO_LEDGERSTATUS_PROCESSED.') AND created > "'.$date.'" order by created '; 

		
		if($cashLedgerObj = Cashledger::whereRaw('accid = '.$userId.$sql)->first()){
			return $cashLedgerObj->created;
		}else{
			return false;
		}
	}	
	
	public static function getTotalCredit($dateFrom, $dateTo, $accid){
		//var_dump($dateTo);
		$csdObj    = new self;
		
		$total_credit = $csdObj->getSum('$amount', 'accid='.$accid.' AND $created <"'.$dateTo.'" AND $created >= "'.$dateFrom.'" AND $chtcode IN ('.CBO_CHARTCODE_WITHDRAWAL.', '.CBO_CHARTCODE_DEPOSIT.', '.CBO_CHARTCODE_ADJUSTMENT.' , '.CBO_CHARTCODE_BONUS.' , '.CBO_CHARTCODE_INCENTIVE.' , '.CBO_CHARTCODE_DAILYREBATE.') AND ($status = '.CBO_LEDGERSTATUS_CONFIRMED.' or $status = '.CBO_LEDGERSTATUS_MANCONFIRMED.') ');

		return $total_credit;
	}
	
	public static function getFirstDepositDate($accid){
	
	
			if($cldObj = Cashledger::whereRaw(' accid = "'.$accid.'" AND $chtcode IN ('.CBO_CHARTCODE_WITHDRAWAL.', '.CBO_CHARTCODE_DEPOSIT.', '.CBO_CHARTCODE_ADJUSTMENT.' , '.CBO_CHARTCODE_BONUS.' , '.CBO_CHARTCODE_INCENTIVE.' , '.CBO_CHARTCODE_DAILYREBATE.') AND ($status = '.CBO_LEDGERSTATUS_CONFIRMED.' or $status = '.CBO_LEDGERSTATUS_MANCONFIRMED.') ')->first()){
				return $cldObj->created;
			}else{
				return false;
			}
	}		
	

		
	public static function isSetIncentiveFlag($accid){
	
		if($cldObj = Cashledger::whereRaw('incentiveflag = 1 AND accid = '.$accid.' ')->first()){
			return $cldObj->id;
		}else{
			return false;
		}
		
	}		
	
	public static function isSetIncentiveFlagOnDateRange($accid, $startdate, $enddate){

		if($cldObj = Cashledger::whereRaw('incentiveflag = 1 AND accid = '.$accid.' AND created > "'.$startdate.'" AND created < "'.$enddate.'" ')->count()){
			return true;
		}else{
			return false;
		}
		
	}	
	
	
	
	function getIncentiveFlagDate($accid){
	
		if($cldObj = Cashledger::whereRaw('incentiveflag = 1 AND accid = '.$accid.' ')->count()){
			return $cldObj->created;
		}else{
			return false;
		}
		
	}	
	
	
	
	

}
