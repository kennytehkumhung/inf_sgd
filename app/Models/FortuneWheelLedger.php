<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Lang;

class FortuneWheelLedger extends Model {

    protected $table = 'fortune_wheel_ledger';

    const SYS_REF_TRANS = 'FortuneWheelTrans';
    const SYS_REF_FT_GAME_BONUS = 'FortuneWheelFTGameBonus';
    const SYS_REF_FT_DAILY_LOGIN = 'FortuneWheelFTDailyLogin';
    const SYS_REF_FT_WEEKLY_LOGIN = 'FortuneWheelFTWeeklyLogin';
    const SYS_REF_FT_MONTHLY_LOGIN = 'FortuneWheelFTMonthlyLogin';
    const SYS_REF_FT_BO = 'FortuneWheelFTBo';
    const SYS_REF_NT_BO = 'FortuneWheelNTBo';
    const SYS_REF_NT_DEPOSIT = 'FortuneWheelNTDeposit';

    const CATEGORY_FREE_TOKEN = '1';
    const CATEGORY_NORMAL_TOKEN = '2';

    public static function getSysRefOptions($prependOptionAll = false) {

        $options = [];

        if ($prependOptionAll) {
            $options[''] = Lang::get('COMMON.ALL');
        }

        $options[self::SYS_REF_TRANS] = 'Spin Game';
        $options[self::SYS_REF_FT_GAME_BONUS] = 'Free Token (Game Bonus)';
        $options[self::SYS_REF_FT_DAILY_LOGIN] = 'Free Token (Daily Login)';
        $options[self::SYS_REF_FT_WEEKLY_LOGIN] = 'Free Token (Weekly Login)';
        $options[self::SYS_REF_FT_MONTHLY_LOGIN] = 'Free Token (Monthly Login)';
        $options[self::SYS_REF_FT_BO] = 'Free Token (By BO)';
        $options[self::SYS_REF_NT_BO] = 'Normal Token (By BO)';
        $options[self::SYS_REF_NT_DEPOSIT] = 'Normal Token (Deposit)';

        return $options;
    }

    public static function getCategoryOptions($prependOptionAll = false) {

        $options = [];

        if ($prependOptionAll) {
            $options[''] = Lang::get('COMMON.ALL');
        }

        $options['0'] = 'Deducted Token';
        $options[self::CATEGORY_FREE_TOKEN] = 'Free Token';
        $options[self::CATEGORY_NORMAL_TOKEN] = 'Normal Token';

        return $options;
    }
}
