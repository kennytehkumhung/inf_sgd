<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Configs;
use Cache;
use Lang;
use DB;

class Currency extends Model {

	protected $table = 'currency';
	 
	const CREATED_AT = 'created';

    const UPDATED_AT = 'modified';
	
	static function getCurrencyRate($crccode, $date = ''){
		if($crccode == Configs::getParam('SYSTEM_BASE_CURRENCY')) return '1.00000';

		$object = Cache::rememberForever('crcrate_'.$crccode, function() use ($crccode) {
			return self::whereRaw('code = "'. $crccode .'"')->first();
		});

		if($object){
			if(strlen($date) == 0)
				return $object->rate;
			
		}else{
			return false;
		}
		
	}


	static function getLocalAmount($amt, $crccode, $baseCrccode = ''){
		
		if( $baseCrccode == '' ) $baseCrccode = Config::getParam('SYSTEM_BASE_CURRENCY');
		
		$object = Cache::rememberForever('crcrate_'.$crccode, function() use ($crccode) {
					return self::whereRaw('code = "'. $crccode .'"')->first();
		});

		if( $object ){
			if($baseCrccode != Config::getParam('SYSTEM_BASE_CURRENCY')){
				
				$currencyObj = Cache::rememberForever('crcrate_'.$baseCrccode, function() use ($baseCrccode) {
					return self::whereRaw('code = "'. $baseCrccode .'"')->first();
				});
				
				if( $currencyObj ){
					$amtl = round($amt * $object->rate / $currencyObj->rate, 2);
					return $amtl;
				}
				else return false;
			}else{
				$amtl = round($amt * $object->rate, 2);
				return $amtl;
			}
		}else{
			return false;
		}
	}

	static function getUserAmount($amtl, $crccode, $baseCrccode , $date = ''){
		
		if( $baseCrccode == '' ) $baseCrccode = Config::getParam('SYSTEM_BASE_CURRENCY');

		if($object = self::whereRaw('code = "'.$crccode.'"')->first() ){
			if($baseCrccode != Config::getParam('SYSTEM_BASE_CURRENCY')){

				if( $currencyObj = self::whereRaw('code = "'.$baseCrccode.'"')->first() ){
					if($date != ''){
						$userRate 		= Currency::getCurrencyRate($crccode, $date);
						$currencyRate 	= Currency::getCurrencyRate($baseCrccode, $date);
						$amt 			= round($amtl * $currencyRate / $userRate, 2);
					}else $amt = round($amtl * $currencyObj->rate / $object->rate, 2);
						
					return $amt;
				}else return false;
			}else{
				if($date != ''){
					$userRate 		= Currency::getCurrencyRate($crccode, $date);
					$amt 			= round($amtl / $userRate, 2);
				}else $amt = round($amtl / $object->rate, 2);
				
				return $amt;
			}
		}else{
			return false;
		}
	}

	/**
	* Get all currency as array
	*
	*
	* @access	public
	* @return 	array/boolean Array of active currency or false if none found.
	*/
	public static function  getAllCurrencyAsOptions() {

		$currencies = array();
		if($objects = self::whereRaw('status = 1')->get()) {
			foreach($objects as $object) {
				$currencies[$object->code] = $object->code;
			}
		}
		if (count($currencies) > 0) return $currencies;
		else return false;
	}
	
	/**
	* Return the current object status color code.
	*
	*
	* @return string The object status color code
	*/
	static function getStatusColorCode(){
		if($this->id == 0) return false;

		if($this->status == CBO_STANDARDSTATUS_ACTIVE)
		return '#C6FFD9';
		else if($this->status == CBO_STANDARDSTATUS_SUSPENDED)
		return '#FFCCCC';
	}
	
	/**
	* get the local amount converted to base currency by currency rate given
	*
	* @param array $amt				The amount in user's currency.
	* @param array $crcrate		   	User's currency rate.
	*
	* @access	public
	* @return   boolean True if successful.  Otherwise false.
	* @see set()
	*/
	static function getLocalAmountByRate($amt, $crcrate){
		$amtl = round($amt * $crcrate, 2);
		return $amtl;
	}
	
	/**
	* get the local amount converted to base currency
	*
	* @param array $amt				The amount in user's currency.
	* @param array $crccode		   	User's currency code.
	* @param array $baseCrccode		Base currency code.
	*
	* @access	public
	* @return   boolean True if successful.  Otherwise false.
	* @see set()
	*/
	public static function getMultiplier($crccode) {
		$multiplier = 1;
		if( $object = self::whereRaw('code = "'.$crccode.'"')->first()){
			if($object->multiplier != 0) $multiplier = abs($object->multiplier);
		}
		
		return $multiplier;
	}
	
	public static function prefix( $crccode ){
		
		$currency['my'] = 'myr';
		$currency['id'] = 'idr';
		$currency['vn'] = 'vnd';
		
		return $currency[$crccode];
	}
	
	function getStatusText($module = false){
		
		if($this->id == 0 || !isset($this->status)) return false;

		if($this->status == 1) {
			$action = 'suspend';
			$str = Lang::get('COMMON.ACTIVE');
		} else if($this->status == 0) {
			$action = 'activate';
			$str = Lang::get('COMMON.SUSPENDED');
		}

		if($module)
		$str = '<a href="#" onclick="return doConfirmAction(\''.$module.'-active-suspend\', \''.$action.'\', \''.$this->id.'\')">'.$str.'</a>';

		return $str;
	}
	
	public static function insertIgnore($array){
		$a = new static();
		if($a->timestamps){
			$now = \Carbon\Carbon::now();
			$array['created_at'] = $now;
			$array['updated_at'] = $now;
		}
		DB::insert('INSERT IGNORE INTO '.$a->table.' ('.implode(',',array_keys($array)).
			') values (?'.str_repeat(',?',count($array) - 1).')',array_values($array));
	}

}
