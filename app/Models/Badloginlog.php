<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Badloginlog extends Model {

	protected $table = 'badloginlog';
	 
	const CREATED_AT = 'created';

    const UPDATED_AT = 'modified';
	
	function getTypeColorCode(){
		if($this->id == 0) return false;

		if($this->type == CBO_LOGINTYPE_USER)
		return '#CC99FF';
		else if($this->type == CBO_LOGINTYPE_ADMIN)
		return '#CCFFCC';
		else if($this->type == CBO_LOGINTYPE_AFFILIATE)
		return '#CCCCFF';
	}
	
	function getTypeText(){
		if($this->id == 0) return false;

		if ($this->type == CBO_LOGINTYPE_USER)
		return Lang::get('COMMON.MEMBER');
		elseif ($this->type == CBO_LOGINTYPE_ADMIN)
		return Lang::get('COMMON.ADMINUSER');
		elseif ($this->type == CBO_LOGINTYPE_AFFILIATE)
		return Lang::get('COMMON.AFFILIATE');
	}
	
	function getTypeOptions(){
		$type = array(
			CBO_LOGINTYPE_USER 		=> Lang::get('COMMON.MEMBER'),
			CBO_LOGINTYPE_ADMIN		=> Lang::get('COMMON.ADMINUSER'),
			CBO_LOGINTYPE_AFFILIATE => Lang::get('COMMON.AFFILIATE'),
		);

		return $type;
	}

}
