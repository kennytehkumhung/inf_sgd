<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Lang;

class Revertbonussetting extends Model {

    protected $table = 'revertbonussetting';

    function getStatusText(){
        if($this->id == 0) return false;

        if($this->status == CBO_STANDARDSTATUS_SUSPENDED)
            $str = Lang::get('COMMON.UNSETTLED');
        else if($this->status == CBO_STANDARDSTATUS_ACTIVE)
            $str = Lang::get('COMMON.SETTLED');

        return $str;
    }

}
