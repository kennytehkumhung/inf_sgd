<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Paymentgateway extends Model {

	protected $table = 'paymentgateway';
	 
    /**
     * fdmid = 2 (fixed)
     * type = CBO_CHARTCODE_DEPOSIT 1 | CBO_CHARTCODE_WITHDRAW 2
     * paytype = Paymentgatewaysetting->id
     * paybnkoption = supported payment channel by PG provider, eg.: Alipay weixin etc
     * status = CBO_LEDGERSTATUS_PENDING 0 | CBO_LEDGERSTATUS_CONFIRMED 1 | CBO_LEDGERSTATUS_CANCELLED 2
     */

    public function getTypeText($payType = null) {
        if (is_null($payType)) {
            $payType = $this->paytype;
        }

        $pgsObj = Paymentgatewaysetting::find($payType);
        $result = '';

        if ($pgsObj) {
            if ($pgsObj->real_name == $pgsObj->name) {
                $result = $pgsObj->real_name;
            } else {
                $result = $pgsObj->real_name . ' (' . $pgsObj->name . ')';
            }
        }

        return $result;
    }

    public function getStatusText($status = null, $color = false) {
        if (is_null($status)) {
            $status = $this->status;
        }

        return (new Cashledger())->getStatusText($status, $color);
    }

    public function getUploadedTransferAttachment($type = Media::TYPE_IMAGE) {
        if($this->id == 0) return false;

        if($object = Media::whereRaw('refobj = "PGtransfer" AND refid = '.$this->id.' AND type = '.$type)->first()) {
            return $object->domain.'/'.$object->path;
        }

        return false;
    }
}
