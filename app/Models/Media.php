<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Media extends Model {

	protected $table = 'media';
	 
	const CREATED_AT = 'created';

    const UPDATED_AT = 'modified';
	
	const TYPE_IMAGE 		= 1;
	const TYPE_THUMBNAIL 	= 2;
	const TYPE_EXCEL		= 3;
	const TYPE_IMAGE_MOBILE	= 4;

	function getStatusColorCode(){
		if($this->id == 0) return false;

		if($this->status == CBO_STANDARDSTATUS_ACTIVE)
		return '#C6FFD9';
		else if($this->status == CBO_STANDARDSTATUS_SUSPENDED)
		return '#FFCCCC';
	}
	
	public static function doUploadFile($refobj, $file, $folder, $public = false, $type = self::TYPE_IMAGE) {
		
		/**
		*  Update file to aws
		*/
		try {
			
			// Undefined | Multiple Files | $_FILES Corruption Attack
			// If this request falls under any of them, treat it invalid.
			if (
				!isset($file['error']) ||
				is_array($file['error'])
			) {
				throw new RuntimeException('Invalid parameters!');
			}

			// Check $_FILES['upfile']['error'] value.
			switch ($file['error']) {
				case UPLOAD_ERR_OK:
					break;
				case UPLOAD_ERR_NO_FILE:
					throw new RuntimeException('No file sent.');
				case UPLOAD_ERR_INI_SIZE:
				case UPLOAD_ERR_FORM_SIZE:
					throw new RuntimeException('Exceeded filesize limit.');
				default:
					throw new RuntimeException('Unknown errors.');
			}

			// You should also check filesize here. 
			/*
			if ($file['size'] > 1000000) {
				throw new RuntimeException('Exceeded filesize limit.');
			}
			*/

			// DO NOT TRUST $_FILES['upfile']['mime'] VALUE !!
			// Check MIME Type by yourself.
			$finfo = new finfo(FILEINFO_MIME_TYPE);
			if (false === $ext = array_search(
				$finfo->file($file['tmp_name']),
				array(
					'jpg' => 'image/jpeg',
					'png' => 'image/png',
					'gif' => 'image/gif',
				),
				true
			)) {
				throw new RuntimeException('Invalid file format.');
			}

			// You should name it uniquely.
			// DO NOT USE $_FILES['upfile']['name'] WITHOUT ANY VALIDATION !!
			// On this example, obtain safe unique name from its binary data.
			$fileName = sprintf('%s.%s', sha1_file($file['tmp_name']),$ext);
			$filePath = sprintf(APPROOT.'/download/tmp/%s', $fileName);
			if (!move_uploaded_file($file['tmp_name'], $filePath)) {
				throw new RuntimeException('Failed to move uploaded file.');
			}
			
			// Instantiate the client.
			$s3 = S3Client::factory(array(
				'key'    => SYSTEM_AWS_ACCESSKEY,
				'secret' => SYSTEM_AWS_SYSTEMKEY,
				'region' => 'ap-southeast-1'
			));
			
			// Upload a file.
			$params = array(
				'Bucket'       => SYSTEM_AWS_S3BUCKET,
				'Key'          => $folder.'/'.$fileName,
				'SourceFile'   => $filePath,
				'ACL'          => 'authenticated-read',
			);
			if($public) $params['ACL'] = 'public-read';
			$result = $s3->putObject($params);
			
			$object = new self;
			if(!$object->getBy('$refobj = "'.str_replace('object', '', get_class($refobj)).'" AND $refid = '.$refobj->id.' AND $type = '.$type)) {
				$object->refobj = str_replace('object', '', get_class($refobj));
				$object->refid = $refobj->id;
				$object->type = $type;
				$object->domain = 'http://'.SYSTEM_AWS_S3BUCKET;
				$object->path = $folder.'/'.$fileName;
				$object->add();
			} else {
				$object->domain = 'http://'.SYSTEM_AWS_S3BUCKET;
				$object->path = $folder.'/'.$fileName;
				$object->update();
			}
			
			unlink($filePath);
			return true;
			
		} catch (RuntimeException $e) {
			$error = $e->getMessage();
			return false;
		}
		
		
	}
	
	public static function s3Upload($folder, $fileName, $public = false,$type = self::TYPE_EXCEL){
	    //upload file to aws
	    $filePath = sprintf(APPROOT.'/download/tmp/%s', $fileName);

	    // Instantiate the client.
	    $s3 = S3Client::factory(array(
		    'key'    => SYSTEM_AWS_ACCESSKEY,
		    'secret' => SYSTEM_AWS_SYSTEMKEY,
		    'region' => 'ap-southeast-1'
	    ));
	    
	    try{
		// Upload a file.
		$params = array(
			'Bucket'       => SYSTEM_AWS_S3BUCKET,
			'Key'          => $folder.'/'.$fileName,
			'SourceFile'   => $filePath,
			'ACL'          => 'authenticated-read'
		);

		if($public) $params['ACL'] = 'public-read'; 
		$result = $s3->putObject($params);
		
		$object = new self;
		$path = $folder.'/'.$fileName;
		if(!$object->getBy('$path = "'.$path.'" AND $domain = "'.SYSTEM_AWS_S3BUCKET.'"')) {
			$object->type = $type;
			$object->domain = SYSTEM_AWS_S3BUCKET;
			$object->path = $path;
			$object->add();
		} else {
			$object->type = $type;
			$object->domain = SYSTEM_AWS_S3BUCKET;
			$object->path = $path;
			$object->created = App::getDateTime(1);
			$object->update();
		}
		unlink($filePath);
		return true;

	    }catch (S3Exception $e) {
		$error = $e->getMessage();
		return false;
	    }
	}
	
	public static function getImage($refobj, $type = Media::TYPE_IMAGE) {

		$object = str_replace('object', '', get_class($refobj));
		$object = str_replace('App\Models\\', '', get_class($refobj));

		if( $object = self::whereRaw('refobj = "'.$object.'" AND refid = '.$refobj->id.' AND type = '.$type)->first()) {
			return $object->domain.'/'.$object->path;
		}
		
		return false;
	}
}
