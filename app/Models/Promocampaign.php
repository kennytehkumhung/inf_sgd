<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\libraries\App;
use DB;
use App\Models\Configs;
use Lang;
use Config;

class Promocampaign extends Model {

	protected $table = 'promocampaign';
	 
	const CREATED_AT = 'created';

    const UPDATED_AT = 'modified';
	
	const CBO_METHOD_FIRSTTIMEDEPOSIT 	= 1;
	const CBO_METHOD_NEWMEMBER 			= 2;
	const CBO_METHOD_OLDMEMBER 			= 3;
	const CBO_METHOD_DAILYDEPOSIT 		= 4;
	const CBO_METHOD_DEPOSITREBATE 		= 5;
	const CBO_METHOD_AFFFIRSTDEPOSIT 	= 6;
	const CBO_METHOD_EVERYTIME 			= 7;
	const CBO_METHOD_WEEKLY 			= 8;
	const CBO_METHOD_MONTHLY 			= 9;
	
	const CBO_TYPE_PERCENTAGE 			= 1;
	const CBO_TYPE_CASH 				= 2;
	
	public static function getPromoByMemberId($accid, $default = 'tag = 0 ') {
	
		$object = new self;
		$pmcObj = new Promocash;
		$clgObj = new Cashledger;

        $frontPath = Config::get('setting.front_path');

		$crccode = '';
		if( $accObj = Account::find($accid) )
		$crccode = $accObj->crccode;
		
		$list = array();
		
		//first welcome bonus
		$now = App::now();
		
		$querys[] = 'tag = 0 ';
		
		//query for member who having this tag
		if( $default != 'tag = 0 ' ){
			$querys[] = $default;
		}
		
		
		foreach( $querys as $default )
		{

			if( $frontPath == 'royalewin' ){
				
				if($accObj->created >= '2016-11-01 14:05:00') {

					if( !Cashledger::where( 'accid' , '=' , $accid )->where( 'chtcode' , '=' , CBO_CHARTCODE_DEPOSIT )->whereRaw( '(status='.CBO_LEDGERSTATUS_CONFIRMED.' OR status='.CBO_LEDGERSTATUS_MANCONFIRMED.')'  )->count() ) {
						if( !Promocash::where( 'accid' , '=' , $accid )->where( 'status' , '=' , CBO_STANDARDSTATUS_ACTIVE )->count() ) {
							if( $promos = self::whereRaw($default.' AND crccode = "'.$crccode.'" AND pcpid = 0 AND startdate <= "'.$now.'" AND enddate >= "'.$now.'" AND method = '.self::CBO_METHOD_FIRSTTIMEDEPOSIT.' AND status = '.CBO_STANDARDSTATUS_ACTIVE)->get() ) {
								foreach($promos as $promo) {
									$list[$promo->id] = array(
										'code'	=> $promo->code,
										'name'	=> $promo->name,
										'desc'	=> $promo->desc,
										'min'	=> $promo->mindeposit,
									);
								}
							}
						}
					}
				
				
						//tier welcome bonus
					if( $pmcObj = DB::select( DB::raw( 
					'
						select  id as campaignid from cashledger where accid = '.$accid.' and chtcode = '.CBO_CHARTCODE_DEPOSIT.' and status IN ('.CBO_LEDGERSTATUS_CONFIRMED.','.CBO_LEDGERSTATUS_MANCONFIRMED.') and refobj = "PromoCashObject" and refid IN(
						select cash.id from promocampaign campaign, promocash cash 
						where campaign.id = cash.pcpid 
						and campaign.crccode = "'.$crccode.'" 
						and campaign.method = '.self::CBO_METHOD_FIRSTTIMEDEPOSIT.' 
						and cash.accid = '.$accid.'
						and cash.status = '.CBO_STANDARDSTATUS_ACTIVE.') 
					')) ) {
							if($promos = self::whereRaw($default.' AND crccode = "'.$crccode.'" AND pcpid = '.$pmcObj->campaignid.' AND startdate <= "'.$now.'" AND enddate >= "'.$now.'" AND method = '.self::CBO_METHOD_FIRSTTIMEDEPOSIT.' AND status = '.CBO_STANDARDSTATUS_ACTIVE)->get() ) {
								foreach($promos as $promo) {
									$list[$promo->id] = array(
										'code'	=> $promo->code,
										'name'	=> $promo->name,
										'desc'	=> $promo->desc,
										'min'	=> $promo->mindeposit,
									);
								}
							}
					}
				}
			}else{
		
				if($accObj->created >= '2014-09-16 00:00:00') {

					if( !Cashledger::where( 'accid' , '=' , $accid )->where( 'chtcode' , '=' , CBO_CHARTCODE_DEPOSIT )->whereRaw( '(status='.CBO_LEDGERSTATUS_CONFIRMED.' OR status='.CBO_LEDGERSTATUS_MANCONFIRMED.')'  )->count() ) {
						if( !Promocash::where( 'accid' , '=' , $accid )->where( 'status' , '=' , CBO_STANDARDSTATUS_ACTIVE )->count() ) {
							if( $promos = self::whereRaw($default.' AND crccode = "'.$crccode.'" AND pcpid = 0 AND startdate <= "'.$now.'" AND enddate >= "'.$now.'" AND method = '.self::CBO_METHOD_FIRSTTIMEDEPOSIT.' AND status = '.CBO_STANDARDSTATUS_ACTIVE)->get() ) {
								foreach($promos as $promo) {
									$list[$promo->id] = array(
										'code'	=> $promo->code,
										'name'	=> $promo->name,
										'desc'	=> $promo->desc,
										'min'	=> $promo->mindeposit,
									);
								}
							}
						}
					}
				}
			
				//tier welcome bonus
				if( $pmcObj = DB::select( DB::raw( 
				'
					select  id as campaignid from cashledger where accid = '.$accid.' and chtcode = '.CBO_CHARTCODE_DEPOSIT.' and status IN ('.CBO_LEDGERSTATUS_CONFIRMED.','.CBO_LEDGERSTATUS_MANCONFIRMED.') and refobj = "PromoCashObject" and refid IN(
					select cash.id from promocampaign campaign, promocash cash 
					where campaign.id = cash.pcpid 
					and campaign.crccode = "'.$crccode.'" 
					and campaign.method = '.self::CBO_METHOD_FIRSTTIMEDEPOSIT.' 
					and cash.accid = '.$accid.'
					and cash.status = '.CBO_STANDARDSTATUS_ACTIVE.') 
				')) ) {
						if($promos = self::whereRaw($default.' AND crccodes = "'.$crccode.'" AND pcpid = '.$pmcObj->campaignid.' AND startdate <= "'.$now.'" AND enddate >= "'.$now.'" AND method = '.self::CBO_METHOD_FIRSTTIMEDEPOSIT.' AND status = '.CBO_STANDARDSTATUS_ACTIVE)->get() ) {
							foreach($promos as $promo) {
								$list[$promo->id] = array(
									'code'	=> $promo->code,
									'name'	=> $promo->name,
									'desc'	=> $promo->desc,
									'min'	=> $promo->mindeposit,
								);
							}
						}
				}
				
			}
				//old member bonus
				if($promos = self::whereRaw($default.' AND crccode = "'.$crccode.'" AND startdate <= "'.$now.'" AND enddate >= "'.$now.'" AND method = '.self::CBO_METHOD_OLDMEMBER.' AND status = '.CBO_STANDARDSTATUS_ACTIVE)->get() ) {
					foreach($promos as $promo) {
						$promoObj = Promocash::whereRaw('accid='.$accid.' AND pcpid='.$promo->id)->first();
						if(!$promoObj) {
							$list[$promo->id] = array(
								'code'	=> $promo->code,
								'name'	=> $promo->name,
								'desc'	=> $promo->desc,
								'min'	=> $promo->mindeposit,
							);
						}
					}
				}
				
				//daily deposit bonus
				if($promos = self::whereRaw($default.' AND crccode = "'.$crccode.'" AND startdate <= "'.$now.'" AND enddate >= "'.$now.'" AND method = '.self::CBO_METHOD_DAILYDEPOSIT.' AND status = '.CBO_STANDARDSTATUS_ACTIVE)->get()) {
					foreach($promos as $promo) {
						$starttime = date('Y-m-d').' 00:00:00';
						$promoObj = Promocash::whereRaw('accid='.$accid.' AND pcpid='.$promo->id.' AND status= '.CBO_PROMOCASHSTATUS_CONFIRMED.' AND created >= "'.$starttime.'"')->first();
						
						if(!$promoObj) {
							$list[$promo->id] = array(
								'code'	=> $promo->code,
								'name'	=> $promo->name,
								'desc'	=> $promo->desc,
								'min'	=> $promo->mindeposit,
							);
						}
					}
				}
				
				//EVERYTIME
				if($promos = self::whereRaw($default.' AND crccode = "'.$crccode.'" AND startdate <= "'.$now.'" AND enddate >= "'.$now.'" AND method = '.self::CBO_METHOD_EVERYTIME.' AND status = '.CBO_STANDARDSTATUS_ACTIVE)->get()) {
					foreach($promos as $promo) {
                                                $fromtime = date('Y-m-d').' 00:00:00';
                                                $totime   = date('Y-m-d').' 23:59:59';
						$total_amount = Promocash::whereRaw('accid='.$accid.' AND pcpid='.$promo->id.' AND status= '.CBO_PROMOCASHSTATUS_CONFIRMED.' AND created >= "'.$fromtime.'" AND created <= "'.$totime.'"')->sum('amount');
                                                if($promo->maxamount >= $total_amount){
							$list[$promo->id] = array(
								'code'	=> $promo->code,
								'name'	=> $promo->name,
								'desc'	=> $promo->desc,
								'min'	=> $promo->mindeposit,
							);
                                                }
					}
					
				}
				
				//weekly deposit bonus
				if($promos = self::whereRaw($default.' AND crccode = "'.$crccode.'" AND startdate <= "'.$now.'" AND enddate >= "'.$now.'" AND method = '.self::CBO_METHOD_DAILYDEPOSIT.' AND status = '.CBO_STANDARDSTATUS_ACTIVE)->get()) {
					foreach($promos as $promo) {
						$starttime = date('Y-m-d' ,strtotime('monday this week')).' 00:00:00';
						$promoObj = Promocash::whereRaw('accid='.$accid.' AND pcpid='.$promo->id.' AND status= '.CBO_PROMOCASHSTATUS_CONFIRMED.' AND created >= "'.$starttime.'"')->first();
						
						if(!$promoObj) {
							$list[$promo->id] = array(
								'code'	=> $promo->code,
								'name'	=> $promo->name,
								'desc'	=> $promo->desc,
								'min'	=> $promo->mindeposit,
							);
						}
					}
				}			
				
				//monthly deposit bonus
				if($promos = self::whereRaw($default.' AND crccode = "'.$crccode.'" AND startdate <= "'.$now.'" AND enddate >= "'.$now.'" AND method = '.self::CBO_METHOD_MONTHLY.' AND status = '.CBO_STANDARDSTATUS_ACTIVE)->get()) {
					foreach($promos as $promo) {
						$starttime = date('Y-m-d').' 00:00:00';
						$promoObj = Promocash::whereRaw('accid='.$accid.' AND pcpid='.$promo->id.' AND status= '.CBO_PROMOCASHSTATUS_CONFIRMED.' AND created >= "'.$starttime.'"')->first();
						
						if(!$promoObj) {
							$list[$promo->id] = array(
								'code'	=> $promo->code,
								'name'	=> $promo->name,
								'desc'	=> $promo->desc,
								'min'	=> $promo->mindeposit,
							);
						}
					}
				}


			// These promo only can redeem at FIRST deposit per day.
			// Will show again if that request is REJECTED.
			$oncePerDayPromo = array();

			if (str_contains($frontPath, 'front')) {
				$oncePerDayPromo['19'] = 'IW002';
				$oncePerDayPromo['33'] = 'rj004';
			} elseif (str_contains($frontPath, 'royalewin')) {
				$oncePerDayPromo['5'] = '003';
			} else {
				$oncePerDayPromo = null;
			}

			if (!is_null($oncePerDayPromo)) {
				$starttime = date('Y-m-d').' 00:00:00';

				foreach ($oncePerDayPromo as $opKey => $opVal) {
					if (array_key_exists($opKey, $list) && $list[$opKey]['code'] == $opVal) {
						$cashLedgerObj = Cashledger::where('accid', '=', $accid)
							->where('created', '>=', $starttime)
							->whereIn('chtcode', array(CBO_CHARTCODE_DEPOSIT))
							->whereIn('refobj', array('Promocash', 'Banktransfer'))
							->whereNotIn('status', array(CBO_LEDGERSTATUS_CANCELLED, CBO_LEDGERSTATUS_MANCANCELLED))
							->first();

						if($cashLedgerObj) {
							unset($list[$opKey]);
						}
					}
				}
			}
		
		}

        return $list;
	}	
	
	function getPromoDetail() {
	
		if($this->id <= 0) return false;
		
		if($this->type == self::CBO_TYPE_PERCENTAGE) {
			return $this->amount .'%';
		} else if($this->type == self::CBO_TYPE_CASH) {
			return Configs::getParam('SYSTEM_BASE_CURRENCY').' '.$this->amount;
		}
	}
	
	public function getPromoCashAmount($deposit,$accid) {
                if($this->id <= 0) return false;
		if($deposit == 0) return false;
		$maxamount = 0;
                $fromtime = date('Y-m-d').' 00:00:00';
		$totime   = date('Y-m-d').' 23:59:59';
                
		if($this->type == self::CBO_TYPE_PERCENTAGE) {
			$amount = $this->amount/100 * $deposit;
                        if($this->method == self::CBO_METHOD_EVERYTIME){
                            $maxamount = $this->maxamount;
                            if($maxamount != 0) {
                                $old_amount = Promocash::whereRaw('accid='.$accid.' AND pcpid='.$this->id.' AND created >= "'.$fromtime.'" AND created <= "'.$totime.'"')->sum('amount');
                                $maxamount = $maxamount - $old_amount;
                                if($maxamount > 0) {
                                        if($amount > $maxamount)
                                        $amount = $maxamount;
                                } else $amount = 0;
                            }
                        }else{
                            if($this->maxamount > 0 && $amount > $this->maxamount) $amount = $this->maxamount;
                        }
			return $amount;
		} else if($this->type == self::CBO_TYPE_CASH) {
			return $this->amount;
		}
	}
	
	public static function getBonusType(){
		$type = array(
			self::CBO_TYPE_PERCENTAGE 				=> Lang::get('COMMON.PERCENTAGE'),
			self::CBO_TYPE_CASH 					=> Lang::get('COMMON.CASH'),
		);

		return $type;
	}
	
	public static function getMethodOptions(){
		$type = array(
			self::CBO_METHOD_FIRSTTIMEDEPOSIT 	=> Lang::get('COMMON.FIRSTTIMEDEPOSIT'),
			self::CBO_METHOD_OLDMEMBER 			=> Lang::get('COMMON.OLDMEMBER'),
			self::CBO_METHOD_DAILYDEPOSIT 		=> Lang::get('COMMON.DAILYDEPOSIT'),
			self::CBO_METHOD_DEPOSITREBATE 		=> Lang::get('COMMON.DEPOSITREBATE'),
			self::CBO_METHOD_EVERYTIME 			=> Lang::get('COMMON.EVERYTIME'),
			self::CBO_METHOD_WEEKLY 			=> Lang::get('COMMON.WEEKLY'),
			self::CBO_METHOD_MONTHLY 			=> Lang::get('COMMON.MONTHLY'),
		);

		return $type;
	}
	
	public function getImage($type = Media::TYPE_IMAGE) {
		if($this->id == 0) return false;
		

		if($object = Media::whereRaw('refobj = "'.str_replace('App\Models\\', '', get_class($this)).'" AND refid = '.$this->id.' AND type = '.$type)->first()) {
			return $object->domain.'/'.$object->path;
		}
		
		return false;
	}
	
	public static function getAllAsOptions($date = false, $active = true,$crccode) {

		$options = array();
		
		if($active)
		$condition = 'status = '.CBO_STANDARDSTATUS_ACTIVE;
		else $condition = '1';
		if($date) 
		$condition .= ' AND startdate <= "'.App::getDateTime().'" AND enddate >= "'.App::getDateTime().'" ';
	
		$condition .= ' AND crccode = "'.$crccode.'" ';
	
		if($promos = self::whereRaw($condition)->get()) {
			foreach($promos as $promo) {
				$options[$promo->id] = $promo->name;
			}
		}
		
		return $options;
	}
	
	function getStatusText($module = false){
		
		if($this->id == 0 || !isset($this->status)) return false;

		if($this->status == 1) {
			$action = 'suspend';
			$str = Lang::get('COMMON.ACTIVE');
		} else if($this->status == 0) {
			$action = 'activate';
			$str = Lang::get('COMMON.SUSPENDED');
		}

		if($module)
		$str = '<a href="#" onclick="return doConfirmAction(\''.$module.'-active-suspend\', \''.$action.'\', \''.$this->id.'\')">'.$str.'</a>';

		return $str;
	}
        
        public static function getStatusOptions(){
		
		$status = array(
			CBO_ACCOUNTSTATUS_ACTIVE 			=> Lang::get('COMMON.ACTIVE'),
			CBO_ACCOUNTSTATUS_SUSPENDED 		=> Lang::get('COMMON.SUSPENDED'),
		);

		return $status;
	}

}
