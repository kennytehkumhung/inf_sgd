<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Lang;

class Rejectreason extends Model {

	protected $table = 'rejectreason';
	 
	const CREATED_AT = 'created';

    const UPDATED_AT = 'modified';
	
	public static function getAllReasonAsOptions() {
		$obj = new self;
		$reasons = array();
		if($objects = self::whereRaw('status = 1')->get()) {
			foreach($objects as $object) {
				$reasons[$object->desc] = $object->desc;
			}
		}
		if (count($reasons) > 0) return $reasons;
		else return false;
	}
	

	function getStatusColorCode(){
		if($this->id == 0) return false;

		if($this->status == CBO_STANDARDSTATUS_ACTIVE)
		return '#C6FFD9';
		else if($this->status == CBO_STANDARDSTATUS_SUSPENDED)
		return '#FFCCCC';
	}
	
	function getStatusText($module = false){
		
		if($this->id == 0 || !isset($this->status)) return false;

		if($this->status == 1) {
			$action = 'suspend';
			$str = Lang::get('COMMON.ACTIVE');
		} else if($this->status == 0) {
			$action = 'activate';
			$str = Lang::get('COMMON.SUSPENDED');
		}

		if($module)
		$str = '<a href="#" onclick="return doConfirmAction(\''.$module.'-active-suspend\', \''.$action.'\', \''.$this->id.'\')">'.$str.'</a>';

		return $str;
	}

}
