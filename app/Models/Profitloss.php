<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Profitloss extends Model {

	protected $table = 'profitloss';
	 
	const CREATED_AT = 'created';

    const UPDATED_AT = 'modified';
	
	function getStatusColorCode(){
		if($this->id == 0) return false;

		if($this->status == CBO_STANDARDSTATUS_ACTIVE)
		return '#C6FFD9';
		else if($this->status == CBO_STANDARDSTATUS_SUSPENDED)
		return '#FFCCCC';
	}
	
	public static function updatePNL($wgrObj, $accObj) {
		if($wgrObj->status == Wager::STATUS_OPEN)
		return false;
		
		//validate valid stake
		
		$condition = 'accid = "'.$wgrObj->accid.'" AND prdid = '.$wgrObj->prdid.' AND category = '.$wgrObj->category.' AND accountdate = "'.$wgrObj->accountdate.'" AND status NOT IN ('.Wager::STATUS_OPEN.', '.Wager::STATUS_REJECTED.', '.Wager::STATUS_REFUND.')';

		$wager 			= Wager::whereRaw($condition)->count();
		$valid 			= Wager::whereRaw($condition.' AND validstake > 0')->count();
		$totalstake 	= Wager::whereRaw($condition)->sum('stake');
		$totalvalid 	= Wager::whereRaw($condition)->sum('validstake');
		$totalwinloss 	= Wager::whereRaw($condition.' AND status != '.Wager::STATUS_VOIDED)->sum('profitloss');
		
		if( $object = self::whereRaw('accid = "'.$wgrObj->accid.'" AND prdid = '.$wgrObj->prdid.' AND category = '.$wgrObj->category.' AND date = "'.$wgrObj->accountdate.'"')->first()) {
			
		}
		else
		{
			$object = new self;
			$agtObj = new Agent;
			$agents = $agtObj->getAllUplineById($accObj->agtid);
			
			$object->wbsid  	= $wgrObj->wbsid;
			$object->level1 	= $agents[1];
			$object->level2 	= $agents[2];
			$object->level3 	= $agents[3];
			$object->level4 	= $agents[4];
			$object->accid 		= $wgrObj->accid;
			$object->acccode 	= $wgrObj->acccode;
			$object->nickname 	= $wgrObj->nickname;
			$object->prdid 		= $wgrObj->prdid;
			$object->category 	= $wgrObj->category;
			$object->date 		= $wgrObj->accountdate;
			$object->crccode 	= $wgrObj->crccode;
			$object->crcrate	= $wgrObj->crcrate;
			
		}
		
			$object->wager 			 = (int) $wager;
			$object->validwager 	 = (int) $valid;
			$object->totalstake 	 = (float) $totalstake;
			$object->totalstakelocal = Currency::getLocalAmount($object->totalstake, $object->crccode);
			$object->validstake 	 = (float) $totalvalid;
			$object->validstakelocal = Currency::getLocalAmount($object->validstake, $object->crccode);
			$object->amount 		 = (float) $totalwinloss;
			$object->amountlocal 	 = Currency::getLocalAmount($object->amount, $object->crccode);
		

		return $object->save();
	}
	
	public static function updatePNL2($wagers) {
	
		foreach( $wagers as $accid => $accids )
		{
			
			foreach( $accids as $category => $categorys )
			{
				
				foreach( $categorys as $accountdate => $value )
				{
				
					//validate valid stake
					
					$condition = 'accid = "'.$accid.'" AND prdid = '.$value['prdid'].' AND category = '.$category.' AND accountdate = "'.$accountdate.'" AND status NOT IN ('.Wager::STATUS_OPEN.', '.Wager::STATUS_REJECTED.', '.Wager::STATUS_REFUND.')';

					$totalwager 	= Wager::whereRaw($condition)->count();
					$valid 			= Wager::whereRaw($condition.' AND validstake > 0')->count();
					$totalstake 	= Wager::whereRaw($condition)->sum('stake');
					$totalvalid 	= Wager::whereRaw($condition)->sum('validstake');
					$totalwinloss 	= Wager::whereRaw($condition.' AND status != '.Wager::STATUS_VOIDED)->sum('profitloss');
					
					if( $object = self::whereRaw('accid = "'.$accid.'" AND prdid = '.$value['prdid'].' AND category = '.$category.' AND date = "'.$accountdate.'"')->first()) {
						
					}
					else
					{
						$object = new self;
						$agtObj = new Agent;
						$agents = $agtObj->getAllUplineById($value['accobj']->agtid);
						
						$object->wbsid  	= $value['accobj']->wbsid;
						$object->level1 	= $agents[1];
						$object->level2 	= $agents[2];
						$object->level3 	= $agents[3];
						$object->level4 	= $agents[4];
						$object->accid 		= $accid;
						$object->acccode 	= $value['accobj']->code;
						$object->nickname 	= $value['accobj']->nickname;
						$object->prdid 		= $value['prdid'];
						$object->category 	= $category;
						$object->date 		= $accountdate;
						$object->crccode 	= $value['accobj']->crccode;
						$object->crcrate	= $value['crcrate'];
						
					}
					
						$object->wager 			 = (int) $totalwager;
						$object->validwager 	 = (int) $valid;
						$object->totalstake 	 = (float) $totalstake;
						$object->totalstakelocal = Currency::getLocalAmount($object->totalstake, $object->crccode);
						$object->validstake 	 = (float) $totalvalid;
						$object->validstakelocal = Currency::getLocalAmount($object->validstake, $object->crccode);
						$object->amount 		 = (float) $totalwinloss;
						$object->amountlocal 	 = Currency::getLocalAmount($object->amount, $object->crccode);
						$object->save();
				
				}
		
			}
		}
		
		return true;
	}

}
