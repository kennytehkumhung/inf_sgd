<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Lang;
use Illuminate\Auth\Authenticatable;  
//use Illuminate\Auth\Passwords\CanResetPassword;  
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;  
//use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;  
use Kbwebs\MultiAuth\PasswordResets\CanResetPassword;
use Kbwebs\MultiAuth\PasswordResets\Contracts\CanResetPassword as CanResetPasswordContract;

class Account extends Model implements AuthenticatableContract, CanResetPasswordContract { 

	use Authenticatable, CanResetPassword;  

	protected $table = 'account';
	 
	const CREATED_AT = 'created';

    const UPDATED_AT = 'modified';
	
	
	public function detail()
    {
        return $this->hasOne('App\Models\Accountdetail', 'accid');
    }
	
	public function bankaccount()
    {
        return $this->hasMany('App\Models\Accountbank', 'accid');
    }
	
	public function cashledgers()
    {
        return $this->hasMany('App\Models\Cashledger', 'accid');
    }


	public function scopeActive($query)
    {
        return $query->where('status', '=', CBO_STANDARDSTATUS_ACTIVE);
    }
	
	public function scopeSuspend($query)
    {
        return $query->where('status', '=', CBO_STANDARDSTATUS_SUSPENDED);
    }
	
	public static function getStatusOptions(){
		
		$status = array(
			CBO_ACCOUNTSTATUS_ACTIVE 			=> Lang::get('COMMON.NORMAL'),
			CBO_ACCOUNTSTATUS_LOGINBLOCKED 		=> Lang::get('COMMON.BLOCKED'),
			CBO_ACCOUNTSTATUS_PAUSEGAMING 		=> Lang::get('COMMON.PAUSEGAMING'),
			CBO_ACCOUNTSTATUS_SUSPENDED 		=> Lang::get('COMMON.SUSPENDED'),
		);

		return $status;
	}
	
    function getStatusText($mdl = false, $link = true){
		if($this->id == 0) return false;

		$url = false;
		$action = '';
		if($this->status == CBO_ACCOUNTSTATUS_ACTIVE) {
			$str = Lang::get('COMMON.NORMAL');
			$url = true;
			$action = 'suspend';
		} else if($this->status == CBO_ACCOUNTSTATUS_SUSPENDED) {
			$str = Lang::get('COMMON.SUSPENDED');
			$url = true;
			$action = 'activate';
		} else if($this->status == CBO_ACCOUNTSTATUS_PENDINGACTIVATION)
			$str = Lang::get('COMMON.PENDING');
		else if($this->status == CBO_ACCOUNTSTATUS_FRAUDCASE)
			$str = Lang::get('COMMON.FRAUDCASE');
		else if($this->status == CBO_ACCOUNTSTATUS_LOGINBLOCKED) {
			$str = Lang::get('COMMON.BLOCKED');
			$url = true;
			$action = 'activate';
		} else if($this->status == CBO_ACCOUNTSTATUS_PAUSEGAMING) {
			$str = Lang::get('COMMON.PAUSEGAMING');
			$url = true;
			$action = 'activate';
		}
		
		if($mdl && $url && $link == true) $str = '<a href="#" onclick="return doConfirmAction(\''.$mdl.'\', \''.$action.'\', \''.$this->id.'\')">'.$str.'</a>';

		return $str;
	}
	
	function getStatusText2($module = false){
		
		if($this->id == 0 || !isset($this->status)) return false;

		if($this->status == 1) {
			$action = 'suspend';
			$str = Lang::get('COMMON.ACTIVE');
		} else if($this->status == 0) {
			$action = 'activate';
			$str = Lang::get('COMMON.SUSPENDED');
		}

		if($module)
		$str = '<a href="#" onclick="return doConfirmAction(\''.$module.'-active-suspend\', \''.$action.'\', \''.$this->id.'\')">'.$str.'</a>';

		return $str;
	}

	/**
	* Return the current object status color code.
	*
	*
	* @return string The object status color code
	*/
	public static function getStatusColorCode($status = null){
		if($this->id == 0) return false;

		if($this->status == CBO_ACCOUNTSTATUS_ACTIVE)
		return '#C6FFD9';
		else if($this->status == CBO_ACCOUNTSTATUS_SUSPENDED)
		return '#FFCCCC';
		else if($this->status == CBO_ACCOUNTSTATUS_PENDINGACTIVATION)
		return '#FFFFCC';
		else if($this->status == CBO_ACCOUNTSTATUS_FRAUDCASE)
		return '#CCCCFF';
		else if($this->status == CBO_ACCOUNTSTATUS_LOGINBLOCKED)
		return '#CCCCFF';
		else if($this->status == CBO_ACCOUNTSTATUS_PAUSEGAMING)
		return '#CCCCFF';
	}

	/**
	* Check if the current account is on credit term
	*
	*
	* @return boolean True of false
	*/
	static function isAccountOnCreditTerm(){
		if($this->term & CBO_ACCOUNTTERM_CREDIT)
		return true;
		else return false;
	}

	/**
	* Check if the current account is on cash term
	*
	*
	* @return boolean True of false
	*/
	static function isAccountOnCashTerm(){
		if($this->term & CBO_ACCOUNTTERM_CASH)
		return true;
		else return false;
	}

	/**
	* Return the AccountDetailObject of the current AccountObject.
	*
	* @return object The correspoding AccountDetailObject, false on error
	*/
	static function getAgentObj() {
		if($this->id == 0 || $this->aflid == 0) return false;
		else {
			if( $agentObj = Affiliate::find($this->aflid))
			return $agentObj;
			else {
				return false;
			}
		}
	}

	/**
	* Return the AccountDetailObject of the current AccountObject.
	*
	* @return object The correspoding AccountDetailObject, false on error
	*/
	function getAccountDetailObject() {
		if(!isset($this->id) || $this->id == 0) return false;
		else {
			if( $accountDetailObj = Accountdetail::whereRaw( 'accid = '.$this->id )->first() )
			return $accountDetailObj;
			else return false;
		}
	}

	/**
	* Return the AccountBankObject of the current AccountObject.
	*
	* @return object The correspoding AccountBankObject, false on error
	*/
	static function getAccountBankObject() {
		if(!isset($this->id) || $this->id == 0) return false;
		else {
			if( $accountBankObj = Accountbank::whereRaw('accid = '.$this->id.' ORDER BY id DESC')->get() )
			return $accountBankObj;
			else {
				return false;
			}
		}
	}

	/**
	* Return the AccountBankObject of the current AccountObject.
	*
	* @return object The correspoding AccountBankObject, false on error
	*/
	static function getActiveAccountBankObject() {
		if(!isset($this->id) || $this->id == 0) return false;
		else {
			if( $accountBankObj = Accountbank::whereRaw('accid = '.$this->id .' AND status = 1')->get())
			return $accountBankObj;
			else {
				return false;
			}
		}
	}

	/**
	* Return the account registratin channel in text. If status not provided it will use the current account status
	*
	* @param integer $status	The status code from constant value CBO_ACCOUNTSTATUS_SUSPENDED, CBO_ACCOUNTSTATUS_ACTIVE, CBO_ACCOUNTSTATUS_PENDINGACTIVATION etc
	*
	* @return string The account status in language text
	*/
	static function getRegChannelText($regchannel = null) {
		if($regchannel == null && $this->id != 0) $regchannel = $this->regchannel;

		if($regchannel == CBO_ACCOUNTREGMODE_WEB)
		return Lang::get('COMMON.WEB');
		else if($regchannel == CBO_ACCOUNTREGMODE_PHONE)
		return Lang::get('COMMON.PHONE');
		else if($regchannel == CBO_ACCOUNTREGMODE_OTHER)
		return Lang::get('COMMON.OTHER');
	}
	
	/**
	* Get all active accounts as array
	*
	* @access	public
	* @return 	array/boolean Array of active account code or false if none found.
	*/
	static function getAllAccountsAsOptions($betback = false, $aflid = '') {

		$accounts = array();
		
		$condition = 'status = 1';
		
		if($betback){
			$condition .= ' AND isbetback = 1';
		}
		
		if($aflid != ''){
			$condition .= ' AND aflid = '.$aflid;
		}
		
		if($objects = self::whereRaw($condition)->get()) {
			foreach($objects as $object) {
				if($aflid != ''){
					$accounts[$object->id] = $object->code.'('.$object->crccode.' '.$object->cashbalance.')';
				}else{
					$accounts[$object->id] = $object->code;
				}
			}
		}
		if (count($accounts) > 0) return $accounts;
		else return false;
	}
	
	
	

	/**
	* Get fraud accounts for a given date range
	*
	* @param string   $datefrom 		The transaction date
	*
	* @access protected
	* @return array   The array of fraud accounts
	*/
	static function getFraudAccountByDate($datefrom = '', $dateto = ''){
		$condition = 'status = '.CBO_ACCOUNTSTATUS_FRAUDCASE;
		
		if($dateto == '')
		$condition .= ' AND modified <= "'. App::now() . '"';
		else 
		$condition .= ' AND modified <= "'. $dateto . '"';
		
		if($datefrom != '')
		$condition .= ' AND modified >= "'. $datefrom . '"';
		
		if($accounts = self::whereRaw($condition)->get() ){
			return $accounts;
		}else return false;
	}
	

	
	
	/**
	* Method to get the user  accepted payment gateway
	*
	* @access	protected
	*
	* @return none
	*
	*/
	static function getAcceptedFundingMethodByCrccode($crccode, $withdraw = false) {
		$methods 	= array();

		if($settings = Fundingmethodsetting::whereRaw('crccode = "'.$crccode.'"')->get() ) {
			$condition = 'deposit = 1';
			if($withdraw) $condition = 'withdraw = 1';
			foreach($settings as $setting) {
				$condition .= ' AND id='.$setting->fdmid;
				if(Fundingmethod::whereRaw($condition)->first()) {
					$methods[$fdmObj->id] = $fdmObj->name;
				}
			}
		}
		return $methods;
	}
	
	public static function getPhoneStatusOptions() {
		$options = array(
			0 => Lang::get('COMMON.NOTVERIFIED'),
			1 => Lang::get('COMMON.VALID'),
			2 => Lang::get('COMMON.INVALID'),
		);
		
		return $options;
	}
	
	public static function getPhoneStatusText($status) {
		if($status == 0) 	  return Lang::get('COMMON.NOTVERIFIED');
		else if($status == 1) return Lang::get('COMMON.VALID');
		else if($status == 2) return Lang::get('COMMON.INVALID');
	}
	
	public static function getEmailStatusOptions() {
		$options = array(
			0 => Lang::get('COMMON.NOTVERIFIED'),
			1 => Lang::get('COMMON.VALID'),
			2 => Lang::get('COMMON.INVALID'),
		);
		
		return $options;
	}

    public static function getEmailStatusText($status) {
        if($status == 0) 	  return Lang::get('COMMON.NOTVERIFIED');
        else if($status == 1) return Lang::get('COMMON.VALID');
        else if($status == 2) return Lang::get('COMMON.INVALID');
    }

    public static function getAccCode($id) {

        $acccode = self::whereId($id)->pluck('code');

        return $acccode;
    }
    
    public static function getNickname($id) {

        $nickname = self::whereId($id)->pluck('nickname');

        return $nickname;
    }

    public static function getAccObject($username) {

        $AccObject = self::whereNickname($username)->first();

        return $AccObject;
    }
}
