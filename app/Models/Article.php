<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Lang;
use App\libraries\App;

class Article extends Model {

	protected $table = 'article';

	const CREATED_AT = 'created';
	const UPDATED_AT = 'modified';
	
	const STATUS_SUSPENDED 		= 0;
	const STATUS_ACTIVE 		= 1;
	
	const TYPE_GENERAL 		= 1;
	const TYPE_PROMOTION 		= 2;
	const TYPE_EMAIL 		= 3;
	// TYPE 4: for sms template, not using here. //
	const TYPE_NEWS 		= 5;

	function getStatusColorCode(){
		if($this->id == 0) return false;

		if($this->status == CBO_STANDARDSTATUS_ACTIVE)
		return '#C6FFD9';
		else if($this->status == CBO_STANDARDSTATUS_SUSPENDED)
		return '#FFCCCC';
	}
	
	public static function getTypeOptions() {
		$options = array(
			self::TYPE_GENERAL 		=>  Lang::get('COMMON.GENERAL'),
			self::TYPE_PROMOTION		=>  Lang::get('COMMON.PROMOTION'),
			self::TYPE_EMAIL 		=>  Lang::get('COMMON.EMAIL'),
			self::TYPE_NEWS 		=>  Lang::get('COMMON.NEWS'),
		);
		
		return $options;
	}
	
	public function getTypeType($type) {
		$options = self::getTypeOptions();
		
		if(isset($options[$type])) return $options[$type];
		else return false;
	}
	
	public function getImage($type = Media::TYPE_IMAGE) {
		if($this->id == 0) return false;
		

		if($object = Media::whereRaw('refobj = "'.str_replace('App\Models\\', '', get_class($this)).'" AND refid = '.$this->id.' AND type = '.$type)->first()) {
			return $object->domain.'/'.$object->path;
		}
		
		return false;
	}
	
	public static function getStatusOptions(){
		$status = array(
			self::STATUS_ACTIVE => Lang::get('COMMON.ACTIVE'),
			self::STATUS_SUSPENDED => Lang::get('COMMON.SUSPENDED'),
		);

		return $status;
	}
	
	public function getStatusText($module = false){
		if($this->id == 0 || !isset($this->status)) return false;

		if($this->status == 1) {
			$action = 'suspend';
			$str = Lang::get('COMMON.ACTIVE');
		} else if($this->status == 0) {
			$action = 'activate';
			$str = Lang::get('COMMON.SUSPENDED');
		} else if($this->status == 2) {
			$str = Lang::get('COMMON.MAINTENANCE');
		}
		
		if($module && ($this->status == 1 || $this->status == 0))
		$str = '<a href="#" onclick="return doConfirmAction(\''.$module.'-active-suspend\', \''.$action.'\', \''.$this->id.'\')">'.$str.'</a>';
		
		return $str;
	}
		
}
