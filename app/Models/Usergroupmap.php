<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Usergroupmap extends Model {

	protected $table = 'usergroupmap';
	 
	const CREATED_AT = 'created';

    const UPDATED_AT = 'modified';

}
