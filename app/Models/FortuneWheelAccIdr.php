<?php namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class FortuneWheelAccIdr extends Model {

    protected $table = 'fortune_wheel_acc_idr';

    public static function ensureExists($accid, $acccode, $probabilitySet = 3) {

        if (!FortuneWheelAccIdr::where('accid', '=', $accid)->first()) {
            FortuneWheelAccIdr::forceCreate([
                'accid' => $accid,
                'acccode' => $acccode,
                'probability_set' => $probabilitySet,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]);
        }
    }
}
