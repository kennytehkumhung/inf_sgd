<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cashback extends Model {

	protected $table = 'cashback';
	 
	const CREATED_AT = 'created';

    const UPDATED_AT = 'modified';

}
