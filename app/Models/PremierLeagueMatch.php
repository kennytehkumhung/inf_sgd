<?php namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Lang;

class PremierLeagueMatch extends Model {

    protected $table = 'premier_league_match';

    const STATUS_CLOSED = 0;
    const STATUS_OPENING = 1;
    const STATUS_VOID = 2;
    const STATUS_REOPENED = 3;

    public static function getStatusOptions($prependOptionAll = false) {

        $options = [];

        if ($prependOptionAll) {
            $options[''] = Lang::get('COMMON.ALL');
        }

        $options[self::STATUS_OPENING] = 'Opening';
        $options[self::STATUS_CLOSED] = 'Closed';
        $options[self::STATUS_VOID] = 'Void';
        $options[self::STATUS_REOPENED] = 'Reopened';

        return $options;
    }

    public static function getTeamListOptions($prependOptionAll = false) {

        $options = [];

        if ($prependOptionAll) {
            $options[''] = Lang::get('COMMON.ALL');
        }

        $teamList = [
            1 => 'Arsenal',
            2 => 'Bournemouth',
            3 => 'Brighton',
            4 => 'Burnley',
            5 => 'Cardiff',
            6 => 'Chelsea',
            7 => 'Crystal Palace',
            8 => 'Everton',
            9 => 'Fulham',
            10 => 'Huddersfield',
            11 => 'Leicester',
            12 => 'Liverpool',
            13 => 'Man City',
            14 => 'Man Utd',
            15 => 'Newcastle',
            16 => 'Southampton',
            17 => 'Tottenham', 
            18 => 'Watford',
            19 => 'West Ham',
            20 => 'Wolves',
        ];

        for ($i = 1; $i <= count($teamList); $i++) {
            $options[$i] = $teamList[$i];
        }

        return $options;
    }

    public function getThumbnailImage() {
        if($this->id == 0) return false;

        if($object = Media::whereRaw('refobj = "'.str_replace('App\Models\\', '', get_class($this)).'" AND refid = '.$this->id.' AND type = '.Media::TYPE_THUMBNAIL)->first()) {
            return $object->domain.'/'.$object->path;
        }

        return false;
    }

    public function getBackgroundImage() {
        if($this->id == 0) return false;

        if($object = Media::whereRaw('refobj = "'.str_replace('App\Models\\', '', get_class($this)).'" AND refid = '.$this->id.' AND type = '.Media::TYPE_IMAGE)->first()) {
            return $object->domain.'/'.$object->path;
        }

        return false;
    }

    public static function getCurrentOpeningMatch() {
        $matchObj = PremierLeagueMatch::where('status', '=', PremierLeagueMatch::STATUS_OPENING)
            ->where('bet_opened_at', '<=', Carbon::now())
            ->where('bet_closed_at', '>', Carbon::now())
            ->first();

        return $matchObj;
    }

    public static function getPreviousMatch() {
        $matchObj = PremierLeagueMatch::where('bet_closed_at', '<', Carbon::now())
            ->whereIn('status', array(PremierLeagueMatch::STATUS_OPENING, PremierLeagueMatch::STATUS_CLOSED))
            ->orderBy('bet_closed_at', 'desc')
            ->first();

        return $matchObj;
    }

    public static function getHomepageThumbnail() {
        $matchObj = self::getCurrentOpeningMatch();

        if ($matchObj) {
            return $matchObj->getThumbnailImage();
        } else {
            $matchObj = self::getPreviousMatch();

            if ($matchObj) {
                return $matchObj->getThumbnailImage();
            }
        }

        return false;
    }
}
