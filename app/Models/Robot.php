<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Robot extends Model {

	protected $table = 'robot';
	 
	const CREATED_AT = 'created';

    const UPDATED_AT = 'modified';

}
