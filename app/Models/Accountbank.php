<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Accountbank extends Model {

	protected $table = 'accountbank';
	 
	const CREATED_AT = 'created';

    const UPDATED_AT = 'modified';

    public function getImage($type = Media::TYPE_IMAGE) {
        if($this->id == 0) return false;

        if($object = Media::whereRaw('refobj = "BankAccountAttachmentObject" AND refid = '.$this->id.' AND type = '.$type)->first()) {
            return $object->domain.'/'.$object->path;
        }

        return false;
    }
}
