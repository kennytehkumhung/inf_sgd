<?php namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;
use Lang;

class FortuneWheelTrans extends Model {

    protected $table = 'fortune_wheel_trans';

    const GAME_ACT_CLAIM = 'claim';
    const GAME_ACT_SPIN_BONUS = 'spin_bonus';
    const GAME_ACT_SPIN_PRIZE = 'spin_prize';

    const STATUS_VOIDED = 0;
    const STATUS_OPENING = 1;
    const STATUS_CLAIMED = 2;

    public static function getStatusOptions($prependOptionAll = false) {

        $options = [];

        if ($prependOptionAll) {
            $options[''] = Lang::get('COMMON.ALL');
        }

        $options[self::STATUS_VOIDED] = 'Void';
        $options[self::STATUS_OPENING] = 'Opening';
        $options[self::STATUS_CLAIMED] = 'Claimed';

        return $options;
    }

    public static function getTotalWon($accid, $startdate = null, $enddate = null) {

        $builder = DB::table('fortune_wheel_trans as a')
            ->join('fortune_wheel_match as b', 'b.id', '=', 'a.match_id')
            ->where('b.status', '=', FortuneWheelMatch::STATUS_CLOSED)
            ->where('a.prize_type', '!=', 'token')
            ->where('a.status', '=', FortuneWheelTrans::STATUS_OPENING)
            ->groupBy('a.match_id')
            ->selectRaw('MAX(a.id) as id');

        if (!is_null($accid)) {
            $builder->where('a.accid', '=', $accid);
        }

        if (!is_null($startdate)) {
            $builder->where('a.created_at', '>=', $startdate);
        }

        if (!is_null($enddate)) {
            $builder->where('a.created_at', '<=', $enddate);
        }

        $totalWon = FortuneWheelTrans::whereIn('id', $builder->lists('id'))->sum('amount');

        return $totalWon;
    }
}
