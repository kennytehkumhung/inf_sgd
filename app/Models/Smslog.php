<?php namespace App\Models;

use App\libraries\sms\SMS88;
use App\libraries\sms\SMSMoDing;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Lang;

class Smslog extends Model
{

    protected $table = 'smslog';

    public static function quickCreate($senderAccId, $senderAccCode, $senderIp, $provider, $method, $telMobile, $request, $response, $refCode, $refStatus) {

        return static::forceCreate([
            'sender_accid' => $senderAccId,
            'sender_acccode' => $senderAccCode,
            'sender_ip' => $senderIp,
            'provider' => $provider,
            'method' => $method,
            'tel_mobile' => $telMobile,
            'request' => $request,
            'response' => $response,
            'ref_code' => $refCode,
            'ref_status' => $refStatus,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
    }

    public static function getProviderOptions($prependOptionAll = false) {

        $options = [];

        if ($prependOptionAll) {
            $options[''] = Lang::get('COMMON.ALL');
        }

        $options[SMS88::NAME] = 'SMS88';
        $options[SMSMoDing::NAME] = 'SMS Mo Ding';

        return $options;
    }

    public static function getMethodOptions($prependOptionAll = false) {

        $options = [];

        if ($prependOptionAll) {
            $options[''] = Lang::get('COMMON.ALL');
        }

        $options['regcode'] = 'Registration Code';
        $options['deposit'] = 'Deposit';
        $options['withdrawal'] = 'Withdrawal';
        $options['memenq'] = 'Member Enquiry';

        return $options;
    }
}
