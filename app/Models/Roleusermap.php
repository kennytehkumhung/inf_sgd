<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Roleusermap extends Model {

	protected $table = 'roleusermap';
	 
	const CREATED_AT = 'created';

    const UPDATED_AT = 'modified';

}
