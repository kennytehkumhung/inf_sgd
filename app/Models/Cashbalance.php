<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cashbalance extends Model {

	protected $table = 'cashbalance';
	
	protected $fillable = ['balance'];
	 
	const CREATED_AT = 'created';

    const UPDATED_AT = 'modified';
	
	const BALANCETYPE_MAIN			= 0;
	const BALANCETYPE_ALL			= -1;
	
	public function getBalance($accid) {
		
		$object = new self;

		if( $object = self::whereRaw('$accid = \''.$accid.'\'')->first() ) {
			return $object->cashbalance;
		} else {
			if( $accObj = Account::find($accid) ) {
				$object->accid 		 = $accid;
				$object->crccode 	 = $accObj->crccode;
				$object->cashbalance = $accObj->cashbalance;
				if($object->save()) return $object->cashbalance;
			}
		}
		return false;
	}
	
	/**
	* Update cash balance of the member
	*
	*
	* @access  public
	* @return  boolean True on success, false on failed.
	*/
	public function updateBalance(Account $accObj) {
		$object = new self;
		$balance = 0;
		
		if(!$object::whereRaw('accid = '.$accObj->id.' AND prdid = '.self::BALANCETYPE_MAIN)->first()) {
			$object->accid 	 = $accObj->id;
			$object->acccode = $accObj->code;
			$object->crccode = $accObj->crccode;
			$object->prdid 	 = self::BALANCETYPE_MAIN;
			$object->balance = CashLedger::getBalance($accObj);
			$object->save();
		} else {
			$object->balance = CashLedger::getBalance($accObj);
			$object->save();
		}
		$balance += $object->balance;
		
		if($wbsObj = Website::find($accObj->wbsid)) {
			$products = unserialize($wbsObj->products);
			if(is_array($products)) {
				foreach($products as $product) {
					$balance += self::getMemberBalance($accObj->id, $product);
				}
			}
		}
		
	}
	
	public function getAllWalletAsOptions() {
		$wallets = array(
			self::BALANCETYPE_MAIN 		=> Lang::get('COMMON.MAINWALLET'),
			self::BALANCETYPE_SPORT 	=> Lang::get('COMMON.SPORTWALLET'),
			self::BALANCETYPE_CASINO 	=> Lang::get('COMMON.CASINOWALLET'),
			self::BALANCETYPE_NEWKENO 	=> '数字彩',
		);
		
		return $wallets;
	}
	
 	

}
