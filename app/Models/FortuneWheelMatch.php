<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Lang;

class FortuneWheelMatch extends Model {

    protected $table = 'fortune_wheel_match';

    const STATUS_CLOSED = 0;
    const STATUS_OPENING = 1;

    public static function getStatusOptions($prependOptionAll = false) {

        $options = [];

        if ($prependOptionAll) {
            $options[''] = Lang::get('COMMON.ALL');
        }

        $options[self::STATUS_OPENING] = 'Opening';
        $options[self::STATUS_CLOSED] = 'Closed';

        return $options;
    }
}
