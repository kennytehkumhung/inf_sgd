<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\libraries\App;

class Affiliatesetting extends Model {

	protected $table = 'affiliatesetting';
	 
	const CREATED_AT = 'created';

    const UPDATED_AT = 'modified';

    public static function getCommissionOptions($max = 70, $min = 0, $step = 1) {
		$options = array();
		$i = $min;
		while($i <= $max) {
			$percentage = App::displayAmount($i);
			$options[$percentage] = App::displayPercentage($i, 2);
			$i += $step;
		}

		return $options;
    }
	
}