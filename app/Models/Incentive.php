<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Incentive extends Model {

	protected $table = 'incentive';
	 
	const CREATED_AT = 'created';

    const UPDATED_AT = 'modified';

}
