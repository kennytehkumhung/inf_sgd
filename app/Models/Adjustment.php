<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Lang;

class Adjustment extends Model {

	protected $table = 'adjustment';
	 
	const CREATED_AT = 'created';

    const UPDATED_AT = 'modified';
	
	const CBO_TYPE_CREDIT 		= 1;
	const CBO_TYPE_DEBIT		= 2;
	
	const CBO_STATUS_PENDING 	= 0;
	const CBO_STATUS_CONFIRMED	= 1;
	const CBO_STATUS_REJECTED	= 2;
	
	public static function getTypeOptions(){
		
		$type = array(
			self::CBO_TYPE_CREDIT		=> Lang::get('COMMON.CREDIT'),
			self::CBO_TYPE_DEBIT		=> Lang::get('COMMON.DEBIT'),
		);

		return $type;
	}


}
