<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Accountdetail extends Model {

	protected $table = 'accountdetail';
	 
	const CREATED_AT = 'created';

    const UPDATED_AT = 'modified';

	function getAddress(){

		if($this->id != 0){
			$address = $this->resaddress;
			if($this->rescity != '')
				$address .= ', '.$this->rescity;
			if($this->respostcode != '')
				$address .= ', '.$this->respostcode;
			
			return $address;
		}
		return false;
	}
	
	/**
	 * @param int $minDay
	 * @param int $maxDay
	 * @return array
	 */
	public static function getDobDayOptions($minDay = 1, $maxDay = 31) {
		$result = array();
		
		for ($i = $minDay; $i < $maxDay + 1; $i++) {
			$key = str_pad($i, 2, '0', STR_PAD_LEFT);
			$result[strval($key)] = $key;
		}
		
		return $result;
	}
	
	/**
	 * @param int $minMonth
	 * @param int $maxMonth
	 * @return array
	 */
	public static function getDobMonthOptions($minMonth = 1, $maxMonth = 12) {
		$result = array();
		$months = array(
			'January',
			'February',
			'March',
			'April',
			'May',
			'June',
			'July',
			'August',
			'September',
			'October',
			'November',
			'December',
		);
		
		for ($i = $minMonth; $i < $maxMonth + 1; $i++) {
			$key = str_pad($i, 2, '0', STR_PAD_LEFT);
			$result[$key] = $months[$i - 1];
		}
		
		return $result;
	}
	
	/**
	 * @param int $yearRange
	 * @param int $fromYear
	 * @return array
	 */
	public static function getDobYearOptions($yearRange = 61, $fromYear = null) {
		$result = array();
		
		if (!is_numeric($fromYear)) {
			// If from year not given, use current year as default.
			$fromYear = intval((new Carbon('-9 years'))->format('Y'));
		}
		
		for ($i = $yearRange; $i > 0; $i--) {
			$fromYear--;
			$result[strval($fromYear)] = $fromYear;
		}
		
		return $result;
	}
}
