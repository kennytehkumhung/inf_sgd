<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Lang;

class Agent extends Model {

	protected $table = 'agent';
	 
	const CREATED_AT = 'created';

    const UPDATED_AT = 'modified';
	
	const LEVEL_BIGSHAREHOLDER 		= 1;
	const LEVEL_SHAREHOLDER 		= 2;
	const LEVEL_MASTERAGENT 		= 3;
	const LEVEL_AGENT 				= 4;
	const LEVEL_MEMBER 				= 5;
	
	static function getLevelText($level) {

		if($level == self::LEVEL_BIGSHAREHOLDER)
		return Lang::get('COMMON.BIGSHAREHOLDER');
		else if($level == self::LEVEL_SHAREHOLDER)
		return Lang::get('COMMON.SHAREHOLDER'); 
		else if($level == self::LEVEL_MASTERAGENT)
		return Lang::get('COMMON.MASTERAGENT');
		else if($level == self::LEVEL_AGENT)
		return Lang::get('COMMON.AGENT');
		else if($level == self::LEVEL_MEMBER)
		return Lang::get('COMMON.MEMBER');
		
	}
	
	static function getLevelOptions($member = false) {
		$levels = array(
			self::LEVEL_BIGSHAREHOLDER  => Lang::get('COMMON.BIGSHAREHOLDER'),
			self::LEVEL_SHAREHOLDER 	=> Lang::get('COMMON.SHAREHOLDER'),
			self::LEVEL_MASTERAGENT 	=> Lang::get('COMMON.MASTERAGENT'),
			self::LEVEL_AGENT 		 	=> Lang::get('COMMON.AGENT'),
		);
		if($member) 
		$levels[self::LEVEL_MEMBER] = Lang::get('COMMON.MEMBER');
		
		return $levels;
	}
	
	function getAllUplineById($id, $level = 0) {
		
		$agents = array();

		if($agentObj = self::find($id)) {
			$agents[] = array($agentObj->level, $agentObj->id);
			if($agentObj->parentid != 0) {
				if($parent = $this->getAllUplineById($agentObj->parentid, $level + 1))
				$agents = array_merge($agents, $parent);
			}
		}
		
		if($level == 0) {
			$uplines = $agents;
			$agents = array();
			foreach($uplines as $aff) {
				$agents[$aff[0]] = $aff[1];
			}
		}
		if (count($agents) > 0) return $agents;
		else return false;
	}

}
