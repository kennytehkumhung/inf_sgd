<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Inbox extends Model {

	protected $table = 'inbox';
	 
	const CREATED_AT = 'created';

    const UPDATED_AT = 'modified';

}
