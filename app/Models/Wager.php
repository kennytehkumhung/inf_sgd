<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Lang;

class Wager extends Model {
	
	const STATUS_OPEN 			= 0;
	const STATUS_SETTLED 		= 1;
	const STATUS_VOIDED 		= 2;
	const STATUS_REFUND 		= 3;
	const STATUS_REJECTED 		= 4;
	
	const RESULT_PENDING		= 0;
	const RESULT_WIN			= 1;
	const RESULT_WINHALF		= 2;
	const RESULT_DRAW			= 3;
	const RESULT_LOSS			= 4;
	const RESULT_LOSSHALF		= 5;

	protected $table = 'wager';
	 
	const CREATED_AT = 'created';

    const UPDATED_AT = 'modified';
	
	public function getResultText() {
		if($this->status == self::STATUS_REFUND || $this->status == self::STATUS_VOIDED) {
			return Lang::get('COMMON.REFUND'); 
		} else if($this->status == self::STATUS_REJECTED) {
			return Lang::get('COMMON.REJECTED'); 
		} else {
			if($this->result == self::RESULT_PENDING) 		return Lang::get('COMMON.PENDING'); 
			else if($this->result == self::RESULT_WIN) 		return Lang::get('COMMON.WIN'); 
			else if($this->result == self::RESULT_WINHALF)  return Lang::get('COMMON.WINHALF'); 
			else if($this->result == self::RESULT_DRAW)	    return Lang::get('COMMON.DRAW'); 
			else if($this->result == self::RESULT_LOSS) 	return Lang::get('COMMON.LOSS'); 
			else if($this->result == self::RESULT_LOSSHALF) return Lang::get('COMMON.LOSSHALF');
		}		
	}

}
