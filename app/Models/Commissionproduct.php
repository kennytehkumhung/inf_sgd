<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Commissionproduct extends Model {

	protected $table = 'commissionproduct';
	 
	const CREATED_AT = 'created';

    const UPDATED_AT = 'modified';

}
