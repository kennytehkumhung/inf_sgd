<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Lang;

class FortuneWheelLedgerIdr extends Model {

    protected $table = 'fortune_wheel_ledger_idr';

    // SYS_REF and CATEGORY follow back Models\FortuneWheelLedger
}
