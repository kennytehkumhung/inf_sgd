<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Commissionsetting extends Model {

	protected $table = 'commissionsetting';
	 
	const CREATED_AT = 'created';

    const UPDATED_AT = 'modified';
	
	function getStatusColorCode(){
		if($this->id == 0) return false;

		if($this->status == CBO_CURRENCYSTATUS_ACTIVE)
		return '#C6FFD9';
		else if($this->status == CBO_CURRENCYSTATUS_SUSPENDED)
		return '#FFCCCC';
	}


}
