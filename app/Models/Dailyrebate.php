<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Lang;

class Dailyrebate extends Model {

	protected $table = 'dailyrebate';
	 
	const CREATED_AT = 'created';

    const UPDATED_AT = 'modified';
	
	function getStatusText(){
		if($this->id == 0) return false;
		
		if($this->status == CBO_STANDARDSTATUS_SUSPENDED)
		$str = Lang::get('COMMON.UNSETTLED');
		else if($this->status == CBO_STANDARDSTATUS_ACTIVE)
		$str = Lang::get('COMMON.SETTLED');
		
		return $str;
	}

}
