<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cache;

class Configs extends Model {

	protected $table = 'config';
	 
	const CREATED_AT = 'created';

    const UPDATED_AT = 'modified';
	
	protected static function getParam($param){
		
		if( !Cache::has( 'db_config_'.$param ) )
		{
			Cache::forever( 'db_config_'.$param , Config::where( 'param' , '=' , $param )->pluck('value') );
		}
		
		return Cache::get( 'db_config_'.$param );
		
	}	
	
	protected static function updateParam($param , $value){
		
		Cache::forget( 'db_config_'.$param );
		
		return Config::where( 'param' , '=' , $param )->update(array( 'value' => $value));
		
	}
	
	function getValueTypesOptions() {
		$type = array(
			1 => Lang::get('COMMON.INTEGER'),
			2 => Lang::get('COMMON.STRING'),
			3 => Lang::get('COMMON.FLOAT'),
			4 => Lang::get('COMMON.BOOLEAN'),
		);
		return $type;
	}
	
	static function getConfigGroups() {
		$group = array(
			1 => COMMON.CGSYSTEM,
			2 => COMMON.CGLOGIN,
			3 => COMMON.CGPAYMENT,
			4 => COMMON.CGTRANSACTION,
		);
		return $group;
	}

}
