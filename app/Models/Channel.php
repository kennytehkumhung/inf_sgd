<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Channel extends Model {

	protected $table = 'channel';
	 
	const CREATED_AT = 'created';

    const UPDATED_AT = 'modified';
	
	public static function getAllAsOptions() {
		$options = array();
		if($channels = self::get() ) {
			foreach($channels as $channel) {
				$options[$channel->id] = $channel->name;
			}
		}
		
		return $options;
	}

}
