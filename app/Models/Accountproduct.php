<?php namespace App\Models;

use Config;
use Illuminate\Database\Eloquent\Model;
use Session;

class Accountproduct extends Model {

	protected $table = 'accountproduct';
	 
	const CREATED_AT = 'created';

    const UPDATED_AT = 'modified';
	
	public static function addAccountProduct( $acccode , $product_code , $password = ''){
		
		$accprdObj = new Accountproduct;
		$accprdObj->acccode  = $acccode;
		$accprdObj->prdid 	 = Product::where( 'code' , '=' , $product_code)->pluck('id');
		$accprdObj->password = $password;
        
        if (Config::get('setting.opcode') == 'RYW') {
            $accprdObj->crccode = Session::get('user_currency');
        }
        
		$accprdObj->save();
	}	
	
	public static function is_exist( $acccode , $product_code ){
		
		$builder = Accountproduct::where( 'prdid', '=' , Product::where( 'code' , '=' , $product_code)->pluck('id') )->where( 'acccode' , '=' , $acccode);

        if (in_array(Config::get('setting.front_path'), array('royalewin', 'royalewin/mobile'))) {
            $builder->where( 'crccode' , '=' , Session::get('user_currency'));
        }
        
        return $builder->count();
	}
	
}
