<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cashledgerlog extends Model {

	protected $table = 'cashledgerlog';
	 
	const CREATED_AT = 'created';

    const UPDATED_AT = 'modified';

}
