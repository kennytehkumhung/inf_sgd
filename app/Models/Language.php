<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Lang;
use App\libraries\App;

class Language extends Model {

	protected $table = 'language';
	 
	const CREATED_AT = 'created';

    const UPDATED_AT = 'modified';
	
	public static function getAllLanguageAsOptions($key = 'langcode', $value = 'name'){
		$options = array();
		if($languages = self::whereRaw('status = 1')->get()) {
			foreach($languages as $language) {
				$options[$language->$key] = $language->$value;
			}
		}
		return $options;
	}
        
        public static function getLanguageAsOptions(){
		$options = array();
		if($languages = self::whereRaw('status = 1')->get()) {
			foreach($languages as $language) {
				$options[$language->langcode] = $language->name;
			}
		}
		return $options;
	}

}
