<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Balancetransfer extends Model {

	protected $table = 'balancetransfer';
	 
	const CREATED_AT = 'created';

    const UPDATED_AT = 'modified';
	
	public function getStatusText($mdl = false){
		if($this->id == 0) return false;
		
		if($this->status == CBO_STANDARDSTATUS_ACTIVE) {
			return Lang::get('COMMON.SUCCESS');
		} else if($this->status == CBO_STANDARDSTATUS_SUSPENDED) {
			return Lang::get('COMMON.FAILED');
		}
	}

}
