<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Lang;

class Role extends Model {

	protected $table = 'role';
	 
	const CREATED_AT = 'created';

    const UPDATED_AT = 'modified';
	
	function getStatusText($module = false){
		
		if($this->id == 0 || !isset($this->status)) return false;

		if($this->status == 1) {
			$action = 'suspend';
			$str = Lang::get('COMMON.ACTIVE');
		} else if($this->status == 0) {
			$action = 'activate';
			$str = Lang::get('COMMON.SUSPENDED');
		}

		if($module)
		$str = '<a href="#" onclick="return doConfirmAction(\''.$module.'-active-suspend\', \''.$action.'\', \''.$this->id.'\')">'.$str.'</a>';

		return $str;
	}
	
	public function getAllRoles() {
		$roles = array();
		if($objects = self::whereRaw('status = 1')->get()) {
			foreach($objects as $object) {
				$roles[] = $object;
			}
		}
		if (count($roles) > 0) return $roles;
		else return false;
	}

}
