<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Commission extends Model {

	protected $table = 'commission';
	 
	const CREATED_AT = 'created';

    const UPDATED_AT = 'modified';
	
	const CBO_TYPE_REGISTRATION 		= 1;
	const CBO_TYPE_REBATE 				= 2;
	const CBO_TYPE_INCENTIVE 			= 3;
	
	function getStatusColorCode(){
		if($this->id == 0) return false;

		if($this->status == CBO_STANDARDSTATUS_ACTIVE)
		return '#C6FFD9';
		else if($this->status == CBO_STANDARDSTATUS_SUSPENDED)
		return '#FFCCCC';
	}
	
	function getAllCommSchemeAsOptions() {
		$comms = array();
		if( $objects = self::whereRaw('status = 1')->get() ) {
			foreach($objects as $object) {
				$comms[$object->id] = $object->code;
			}
		}
		if (count($comms) > 0) return $comms;
		else return false;
	}

	
	public function getTypeText(){
		if($this->id == 0) return false;

		if($this->type == self::CBO_TYPE_REGISTRATION)
		return Lang::get('COMMON.COMMISSIONTYPE_1');
		else if($this->type == self::CBO_TYPE_REBATE)
		return Lang::get('COMMON.COMMISSIONTYPE_2');
		else if($this->type == self::CBO_TYPE_INCENTIVE)
		return Lang::get('COMMON.COMMISSIONTYPE_3');
	}
	
	public static function getTypeOptions(){
		$type = array(
			self::CBO_TYPE_REGISTRATION 	=> Lang::get('COMMON.COMMISSIONTYPE_1'),
			self::CBO_TYPE_REBATE 			=> Lang::get('COMMON.COMMISSIONTYPE_2'),
			self::CBO_TYPE_INCENTIVE 		=> Lang::get('COMMON.COMMISSIONTYPE_3'),
		);

		return $type;
	}
	
	public function getAllAsOptions($active = true) {

		$options = array();
		$condition = '1';
		if($active) $condition = '$status=1';
		if( $commissions = self::whereRaw($condition)->get() ) {
			foreach($commissions as $commission) {
				$options[$commission->id] = $commission->name;
			}
		}
		
		if (count($options) > 0) return $options;
		else return false;
	}


}
