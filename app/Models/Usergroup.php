<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Usergroup extends Model {

	protected $table = 'usergroup';
	 
	const CREATED_AT = 'created';

    const UPDATED_AT = 'modified';
	
	public function getAllGroups() {
		$users = array();
		if($objects = self::whereRaw('status = 1')->get()) {
			foreach($objects as $object) {
				$users[] = $object;
			}
		}
		if (count($users) > 0) return $users;
		else return false;
	}

}
