<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Worldcuptrans extends Model {

	protected $table = 'worldcup_trans';
	 
	const CREATED_AT = 'created';

    const UPDATED_AT = 'modified';

    public static function getStatusText($status = null){

        if($status == 0)
            return 'Pending';
        else if($status == 1)
            return 'Won';
        else if($status == 2)
            return 'Lose';

        if($status == null) return false;
    }
	

}
