<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FortuneWheelPrize extends Model {

    protected $table = 'fortune_wheel_prize';

    const POSITION_TYPE_MISC = "0";
    const POSITION_TYPE_PRIZE_LIST_RIGHT = "1";
    const POSITION_TYPE_PRIZE = "2";
    const POSITION_TYPE_BONUS = "3";
}
