<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable;  
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;  
use Kbwebs\MultiAuth\PasswordResets\CanResetPassword;
use Kbwebs\MultiAuth\PasswordResets\Contracts\CanResetPassword as CanResetPasswordContract;
use Lang;
use Config;
use Cache;

class Affiliate extends Model implements AuthenticatableContract, CanResetPasswordContract { 

	use Authenticatable, CanResetPassword;

	protected $table = 'affiliate';
	 
	const CREATED_AT = 'created';

        const UPDATED_AT = 'modified';
    
        const TYPE_PT					= 1;
        
	const TYPE_COMM					= 2;
	
	public static function getStatusOptions(){
		
		$status = array(
            CBO_AFFILIATESTATUS_ACTIVE  => Lang::get('COMMON.ACTIVE'),
            CBO_ACCOUNTSTATUS_SUSPENDED => Lang::get('COMMON.SUSPENDED'),
		);

		return $status;
	}
        
        public static function getTypeAsOptions(){
		$types = array(
			self::TYPE_PT => Lang::get('COMMON.AGENT'),
			self::TYPE_COMM => Lang::get('COMMON.AFFILIATE2'),
		);

		return $types;
	}
	
	public function getAllAccountById($id, $wholedownline = false, $active = false) {
		$accounts = array();

		if($allAccount = Account::whereRaw('aflid = '.$id)->get()) {
			foreach($allAccount as $account) {
				if($active){
					if($account->status == CBO_ACCOUNTSTATUS_ACTIVE)
					$accounts[$account->id] = $account;
				}else $accounts[$account->id] = $account;
			}
		}
		if($wholedownline) {

			if($allAgents = Affiliate::whereRaw('parentid = '.$id)->get()) {
				foreach($allAgents as $agent) {
					if($childAgentMemberAccounts = $this->getAllAccountById($agent->id, true, $active)) {
						$accounts = array_merge($accounts, $childAgentMemberAccounts);
					}
				}
			}
		}

		if (count($accounts) > 0) return $accounts;
		else return false;
	}
        
    public static function getAllDownlineById($id, $wholedownline = false) {
		$agents = array();

		if($allAgents = Affiliate::whereRaw('parentid = '.$id)->get()) {
		
			foreach($allAgents as $agent) {
				$agents[] = $agent->id;
			
				if($wholedownline) {
					if($childAgents = self::getAllDownlineById($agent->id, true)) {
						foreach($childAgents as $childAgent) {
							$agents[] = $childAgent;
						}
					}
				}
			}
		}
	
		if (count($agents) > 0) return $agents;
		else return false;
	}
	
	function getAllDownlineById2($id, $wholedownline = false) {
		$agents = array();
		$agentObj = new self;
		if($allAgents = Affiliate::whereRaw('parentid = '.$id)->get()) {
			
			foreach($allAgents as $agent) {
	
				$agents[$agent->id] = $agent;
				if($wholedownline) {
					if($childAgents = $this->getAllDownlineById2($agent->id, true)) {
						$agents = array_merge($agents, $childAgents);
					}
				}
			}
		}
		
		if (count($agents) > 0) return $agents;
		else return false;
	}
        
	public function getAllUplineById($id, $level = 0) {
		$affiliates = array();
		//$affiliateObj = new self;
		if($affiliateObj = self::find($id)) {
			$affiliates[] = array($affiliateObj->level, $affiliateObj->id);
			if($affiliateObj->parentid != 0) {
				if($parent = $this->getAllUplineById($affiliateObj->parentid, $level + 1))
				$affiliates = array_merge($affiliates, $parent);
			}
		}
		
		if($level == 0) {
			$uplines = $affiliates;
			$affiliates = array();
			foreach($uplines as $aff) {
				$affiliates[$aff[0]] = $aff[1];
			}
		}
		if (count($affiliates) > 0) return $affiliates;
		else return false;
	}
	
	function getStatusText($module = false){
		
		if($this->id == 0 || !isset($this->status)) return false;

		if($this->status == 1) {
			$action = 'suspend';
			$str = Lang::get('COMMON.ACTIVE');
		} else if($this->status == 0) {
			$action = 'activate';
			$str = Lang::get('COMMON.SUSPENDED');
		}

		if($module)
		$str = '<a href="#" onclick="return doConfirmAction(\''.$module.'-active-suspend\', \''.$action.'\', \''.$this->id.'\')">'.$str.'</a>';

		return $str;
	}
	
	public static function check($url){
		
		if( $url == 'www' || $url == 'm' || $url == 'esgame' ){
			
		}else{
		
			if( $affObj = Affiliate::where( 'username' , '=' , $url)->where( 'status' , '=' , 1 )->first() )
			{
				if( Config::get('setting.opcode') == 'IFW' && $affObj->crccode == 'TWD' ){
					header('Location: http://'.Config::get('setting.frontend').'/tw?utm_source=affiliate&utm_medium='.$affObj->code.'&refcode='.$affObj->code);exit;
				}else{
					header('Location: http://'.Config::get('setting.frontend').'?utm_source=affiliate&utm_medium='.$affObj->code.'&refcode='.$affObj->code);exit;
				}
			}
		
		}
	}
	public static function getStatusOptions2(){
		
		$status2 = array(
            '0'  => Lang::get('COMMON.DISALLOW'),
            '1'	 => Lang::get('COMMON.ALLOW'),
		);

		return $status2;
	}

}
