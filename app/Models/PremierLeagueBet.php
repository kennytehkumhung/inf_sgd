<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Lang;

class PremierLeagueBet extends Model
{

    protected $table = 'premier_league_bet';

    const STATUS_PENDING = 0;
    const STATUS_VOID = 1;
    const STATUS_LOSE = 2;
    const STATUS_WON = 3;
    const STATUS_WON_SELECTED = 4;

    public static function getStatusOptions($prependOptionAll = false)
    {

        $options = [];

        if ($prependOptionAll) {
            $options[''] = Lang::get('COMMON.ALL');
        }

        $options[self::STATUS_PENDING] = 'Pending';
        $options[self::STATUS_VOID] = 'Void';
        $options[self::STATUS_LOSE] = 'Lose';
        $options[self::STATUS_WON] = 'Won';
        $options[self::STATUS_WON_SELECTED] = 'Selected Winner';

        return $options;
    }
}
