<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Memo extends Model {

	protected $table = 'memo';
	 
	const CREATED_AT = 'created';

    const UPDATED_AT = 'modified';
	
	const CBO_TYPE_AFFILIATE 		= 2;
    const CBO_TYPE_MEMBER 			= 4;
    const CBO_TYPE_ADMIN 			= 5;

	function getTypeOptions(){
        $type = array(
            self::CBO_TYPE_AFFILIATE 		=> Lang::get('COMMON.AFFILIATE'),
            self::CBO_TYPE_MEMBER 			=> Lang::get('COMMON.MEMBER'),
        );

        return $type;
    }

    /**
     * Return the current object method in text.
     *
     * @param integer $method	The method code
     *
     * @return string The object method in language text
     */
    function getTypeText(){
        if($this->id == 0) return false;

        if($this->type == self::CBO_TYPE_AFFILIATE)
            return Lang::get('COMMON.AFFILIATE');
        else if($this->type == self::CBO_TYPE_MEMBER)
            return Lang::get('CBO_TYPE_MEMBER');
    }

    /**
     * Return the current object method in text.
     *
     * @param integer $method	The method code
     *
     * @return string The object method in language text
     */
    function isAffiliateType(){
        if($this->id == 0) return false;

        if($this->type & self::CBO_TYPE_AFFILIATE)
            return true;
        else return false;
    }

    /**
     * Return the current object method in text.
     *
     * @param integer $method	The method code
     *
     * @return string The object method in language text
     */
    function isMemberType(){
        if($this->id == 0) return false;

        if($this->type & self::CBO_TYPE_MEMBER)
            return true;
        else return false;
    }

}
