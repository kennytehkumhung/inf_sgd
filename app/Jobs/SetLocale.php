<?php

namespace App\Jobs;

use App\Jobs\Job;
use Request;
use Illuminate\Contracts\Bus\SelfHandling;

class setLocale extends Job implements SelfHandling
{

    protected $languages;

    public function __construct()
    {
        $this->languages = 'en';
    }

    public function handle()
    {
	
		if(Request::is('vn*')){
			session()->put('locale', 'vi');
		} 
		
		if(!session()->has('locale'))
		{
			if(Request::is('my*'))
			{
				 session()->put('locale', 'en');
			}
			if(Request::is('vn*')){
				 session()->put('locale', 'vi');
			} 
			if(Request::is('id*')){
				 session()->put('locale', 'id');
			}
			
			if( $_SERVER['HTTP_HOST'] == 'rajabet.com' ||  $_SERVER['HTTP_HOST'] == 'rajabet.us' ||  $_SERVER['HTTP_HOST'] == 'rajabet.biz' ||  $_SERVER['HTTP_HOST'] == 'rajabets.com' ||  $_SERVER['HTTP_HOST'] == 'rajabet.com' )
			{
				 session()->put('locale', 'id');
			}
			elseif( $_SERVER['HTTP_HOST'] == 'iw8vn.com' ||  $_SERVER['HTTP_HOST'] == 'vn368.com' ||  $_SERVER['HTTP_HOST'] == 'vn368.net' ||  $_SERVER['HTTP_HOST'] == 'iw8id.com' )
			{
				 session()->put('locale', 'vi');
			}
			elseif( $_SERVER['HTTP_HOST'] == 'club5599.com'  ||  $_SERVER['HTTP_HOST'] == 'test.club5599.com' || $_SERVER['HTTP_HOST'] == 'www.club5599.com' || $_SERVER['HTTP_HOST'] == '369thai.com' ||  $_SERVER['HTTP_HOST'] == 'www.369thai.com')
			{
				 session()->put('locale', 'th');
			}
			elseif( $_SERVER['HTTP_HOST'] == 'jsh138.com'  ||  $_SERVER['HTTP_HOST'] == 'jsh128.com' )
			{
				 session()->put('locale', 'cn');
			}
			else{
				 session()->put('locale', 'en');
			}
		}		


		app()->setLocale(session('locale'));
		
	
    }
}