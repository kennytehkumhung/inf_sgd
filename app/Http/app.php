<?php	


	Route::post('op/register', 'App\ApiController@postRegister' );
	Route::post('op/info', 'App\ApiController@postMemberInfo' );
	Route::post('op/change-password', 'App\ApiController@postChangePassword' );
	Route::post('op/check-username', 'App\ApiController@postCheckUsername' );
	Route::post('op/update-profile', 'App\ApiController@postUpdateProfile' );
	Route::post('op/login', 'App\ApiController@postLogin' );
	Route::post('op/logout', 'App\ApiController@postLogout' );
	Route::post('op/status', 'App\ApiController@postStatus' );
	Route::post('op/deposit', 'App\ApiController@postDeposit' );
	Route::post('op/withdrawal', 'App\ApiController@postWithdrawal' );
	Route::post('op/transfer', 'App\ApiController@postTransfer' );
	Route::post('op/getbalance', 'App\ApiController@getBalance' );
	Route::post('op/bank', 'App\ApiController@getBank' );
	Route::post('op/bonus', 'App\ApiController@getBonus' );
	Route::post('op/wallet', 'App\ApiController@getWallet' );
	Route::post('op/wallet-by-product', 'App\ApiController@getWalletByProduct' );
	Route::post('op/banner', 'App\ApiController@getBanner' );
	Route::post('op/promotion', 'App\ApiController@getPromotion' );
	Route::post('op/appinfo', 'App\ApiController@getAppInfo' );
	Route::post('op/4dresult', 'App\ApiController@get4dResult' );
	Route::post('op/announcement', 'App\ApiController@getAnnouncement' );
	Route::post('op/reset-password', 'App\ApiController@postResetPassword' );
	Route::post('op/history', 'App\ApiController@postTransactionHistory' );
	Route::post('op/register-affiliate', 'App\ApiController@postRegisterAffiliate' );
	Route::post('op/betslip-history', 'App\ApiController@postBetslipHistory' );
	Route::post('op/betslip-history-detail', 'App\ApiController@postBetslipHistoryDetail' );
	Route::post('op/version', 'App\ApiController@postVersion' );
	Route::post('op/pushmessage', 'App\ApiController@postPushNotification' );
	Route::post('op/sendsms', 'App\ApiController@postSendRegistrationSms' );
	Route::get('promotion/content/{id}',  [ 'uses' =>  'User\HomeController@promotionContent',   'as' => 'promo_content']);
	Route::get('playstar',		    [ 'uses' => 'App\ApiController@PLS',  	 		   'as' => 'pls']);
    Route::get('spadegaming/{type}/{category}', [ 'uses' => 'App\ApiController@SPG',  		   'as' => 'spg' ]);
	Route::get('spadegaming-slot/{gameid}/{token}',  [ 'uses' => 'App\ApiController@SPGSLOT',  		'as' => 'spg-slot' ]);
	Route::get('sports-wft', 		    [ 'uses' => 'App\ApiController@WFT',  	   'as' => 'wft']);
	

	
	Route::post('login', 			'App\Lasvegas\ApiController@Login' );
	Route::post('logout', 			'App\Lasvegas\ApiController@Logout' );
	Route::post('coupon', 			'App\Lasvegas\ApiController@GenerateCode' );
	Route::post('withdraw', 		'App\Lasvegas\ApiController@Withdraw' );
	Route::post('withdraw-verify', 	'App\Lasvegas\ApiController@WithdrawVerify' );
	Route::post('history', 			'App\Lasvegas\ApiController@History' );
	Route::post('balance', 			'App\Lasvegas\ApiController@Balance' );
	Route::post('password-verify', 	'App\Lasvegas\ApiController@PasswordVerify' );
	Route::post('last-reprint', 	'App\Lasvegas\ApiController@LastReprint' );
	Route::post('reprint', 			'App\Lasvegas\ApiController@RePrint' );
	Route::post('print-report', 	'App\Lasvegas\ApiController@printReport' );

