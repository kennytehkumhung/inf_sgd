<?php namespace App\Http\Middleware;

use Closure;
use Validator;
use Lang;
use Hash;
use Auth;
use Session;
use App\Models\Account;

class Password {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{

		$validator = Validator::make(
			[
				'oldpass' => $request->input('oldpass'),
				'newpass' => $request->input('newpass'),
			],
			[
				'oldpass'   => 'required|alpha_num|min:6',
				'newpass'   => 'required|alpha_num|min:6',
			]
		);
		
		if ($validator->fails())
		{
			echo json_encode($validator->errors());
			exit;
		}
		
		if( $request->input('oldpass') == $request->input('newpass') )
		{
			echo json_encode( array( 'newpass' => Lang::get('validation.newpass_same') ) );
			exit;
		}

		if ( !Hash::check( $request->input('oldpass') , Account::where( 'id' , '=' , Session::get('userid') )->pluck('password') ) )
		{
			echo json_encode( array( 'newpass' => Lang::get('validation.oldpass_wrong') ) );
			exit;
		}

        return $next($request);
	}

}
