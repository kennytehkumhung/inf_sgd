<?php namespace App\Http\Middleware;

use Closure;
use Session;
use Redirect;
use App\Models\Onlinetracker;

class MutipleLoginAdmin {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	 
	public function handle($request, Closure $next)
	{
		
		if(Session::get('token') != Onlinetracker::whereUserid(Session::get('admin_userid'))->whereType(3)->pluck('token'))
		{
			
			return redirect::route('admin_login');
		}
		else
		{
			return $next($request);
		}
	}

}
