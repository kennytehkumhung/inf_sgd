<?php namespace App\Http\Middleware;

use Closure;
use Session;
use Cache;
use Auth;
use App;
use Lang;
use Response;
use Config;
use App\Models\Website;
use App\Models\Product;

class Currency {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	 
    protected $products    = '';
	
    protected $website_code = '';
	 
	public function handle($request, Closure $next)
	{

		$website = Cache::rememberForever('website_'.$_SERVER['HTTP_HOST'], function() {
			return Website::where('url', 'LIKE', '%'.$_SERVER['HTTP_HOST'].'%')->first();
		});
		
		if( !$website ){
			 return Response::view('404', [], 404); 
		}
		
		if($request->is('vn*'))
		{
			$website = Website::find(4);
		}
		
		$crccode = unserialize($website->currencies);
		Session::put('opcode',$website->code);

		if( $request->is('my*'))
		{
            if(Config::get('setting.opcode') == 'IFW') {
                if (Lang::getLocale() == 'tw') {
                    Session::set('locale', 'en');
                    Lang::setLocale('en');
                }
            }

			Session::put('currency','MYR');
		}
		elseif($request->is('vn*'))
		{
			Session::put('currency','VND');
		}
		elseif($request->is('tw*'))
        {
            $website = Website::find(5);
            Session::put('opcode',$website->code);
            Lang::setLocale('tw');
            Session::put('currency','TWD');
        }
        elseif($request->is('id*'))
		{
            if(Config::get('setting.opcode') == 'RYW') {
                Lang::setLocale('id');
                Session::put('currency','IDR');
            } else {
                // Compatible with other old projects.
                Session::put('currency',$crccode[0]);
            }
		}
		elseif(Config::get('setting.opcode') == 'GSC') {
            Lang::setLocale('cn');
			 Session::put('currency','CNY');
		}
		else
		{
			Session::put('currency',$crccode[0]);
		}
		
		if( Session::has('user_currency') && Session::has('currency') ){
			if( Session::get('currency') != Session::get('user_currency') ){
				Auth::logout();
			}
		}
		
		$this->website_code = $website->code;

		$website_products = Cache::rememberForever('website_product_'.$this->website_code, function() {
			return Website::where( 'code' , '=' , $this->website_code )->pluck('products');
		});

		$this->products = unserialize($website_products);

		$productObjs = Cache::rememberForever('valid_product_'.$this->website_code, function() {
			return Product::whereRaw( ' id IN('.implode( ',' , $this->products ).')  AND  status IN(1,2)' )->get();
		});
		
		foreach( $productObjs as $productObj ){
				$products_code[] = $productObj->code;
		}
		
		Session::put('valid_product',$products_code);
        $urls = explode( '.' , $_SERVER['HTTP_HOST'] );

		if( count( $urls ) > 2 && $urls[0] != 'www' && $urls[0] != 'm')
		{
			Session::put('user_affname',  array_shift((explode(".",$_SERVER['HTTP_HOST']))) );
		}
		elseif( $request->has('refcode') )
		{
			Session::put('user_refcode',  $request->input('refcode') );
		}	
		
		if( $request->has('channel') ){
			Session::put('user_channel',  $request->input('channel') );
		}

		if( $request->has('utm_source') ){
			Session::put('user_channel',  $request->input('utm_source') );
		}
		
		if( $request->has('utm_medium') ){
			Session::put('utm_medium',  $request->input('utm_medium') );
		}

        return $next($request);
	}

}
