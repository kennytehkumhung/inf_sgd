<?php namespace App\Http\Middleware;

use Closure;
use Session;
use Redirect;
use Auth;
use App\Models\Onlinetracker;
use Lang;

class MutipleLoginUser {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */ 
	 
	public function handle($request, Closure $next)
	{
		
		if(Session::has('token') && Session::get('token') != Onlinetracker::whereUserid(Session::get('userid'))->whereType(1)->pluck('token'))
		{
			$clientip = Onlinetracker::whereUserid(Session::get('userid'))->whereType(1)->pluck('clientip');
			$createdtime = Onlinetracker::whereUserid(Session::get('userid'))->whereType(1)->pluck('created');
			// $clientip = $Onlinetracker->clientip;
			$value=array('checklogin'=>Lang::get('public.LoginSomewhereElse'),'clientip'=>$clientip);
			Session::flush();
			Auth::user()->logout();
			return redirect::route('homepage')->with('checklogin', Lang::get('public.LoginSomewhereElse').'\n'.$clientip.'\n'.$createdtime);
		}
		else
		{
			return $next($request);
		}
	}

}
