<?php namespace App\Http\Middleware;

use App\Models\Configs;
use Closure;
use Validator;
use Session;
use Lang;
use Config;


class Register {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
        if ( Config::get('setting.opcode') == 'IFW' && Session::get('currency') == 'TWD' ) {
            // Infiniwin TWD no need verify captcha.
        } elseif ( Session::get('register_captcha') != $request->input('code') ) {
            echo json_encode( array( 'code' => Lang::get('validation.wrong_captcha') ) );
            exit;
        }

		if( Config::get('setting.front_path') == 'c59')
		{
			$validator = Validator::make(
				[
		
					'username' => $request->input('username'),
					'password' => $request->input('password'),
					'fullname' => $request->input('fullname'),
					'mobile'   => $request->input('mobile'),
				],
				[

					'username' => 'required|min:6|max:12|alpha_num|reserved_str|unique:account,nickname',
					'password' => 'required|alpha_num|min:6|max:12',
					'fullname' => 'required|min:2',
					'mobile'   => 'required|phone|unique:accountdetail,telmobile',
				]
			);
		}
		elseif( Config::get('setting.front_path') == '369bet' )
		{
			$validator = Validator::make(
				[		
					'username' => $request->input('username'),
					'password' => $request->input('password'),
				],
				[
					'username' => 'required|min:6|max:12|alpha_num|reserved_str|unique:account,nickname',
					'password' => 'required|alpha_num|min:6|max:12',
				]
			);
		}
		elseif( Config::get('setting.front_path') == 'sbm' )
		{
			$validator = Validator::make(
				[
		
					'username' => $request->input('username'),
					'password' => $request->input('password'),
					'mobile'   => $request->input('mobile'),
					'fullname' => $request->input('fullname'),
				],
				[

					'username' => 'required|min:6|max:12|alpha_num|reserved_str|unique:account,nickname',
					'password' => 'required|alpha_num|min:6|max:12',
					'mobile'   => 'required|phone',
					'fullname' => 'required|min:2|unique:account,fullname',
				]
			);
		}	
		elseif( Config::get('setting.opcode') == 'RYW' )
		{
		    $rules = [

                'username' => 'required|min:6|max:12|alpha_num|reserved_str|unique:account,nickname,NULL,id,crccode,'.Session::get('currency'),
                'password' => 'required|alpha_num|min:6|max:12',
                'email'    => 'required|email|unique:accountdetail,email',
                'mobile'   => 'required|phone|unique:accountdetail,telmobile',
                'fullname' => 'required|min:2',
                'referralid' => 'integer|min:1|exists:account,id',
            ];

		    if (Session::get('currency') == 'MYR') {
		        $rules['fullname'] = 'required|min:2|unique:account,fullname';

		        if (str_contains(Configs::getParam('SYSTEM_SMS_ONREGISTER'), 'MYR')) {
                    $rules['mobile_vcode'] = 'required|min:6';
                }
            }

			$validator = Validator::make(
				[
					'username' => $request->input('username'),
					'password' => $request->input('password'),
					'email'	   => $request->input('email'),
					'mobile'   => $request->input('mobile'),
					'fullname' => $request->input('fullname'),
                    'referralid' => $request->input('referralid'),
                    'mobile_vcode' => $request->input('mobile_vcode'),
				],
                $rules
			);
			$validator->setAttributeNames([
			    'referralid' => Lang::get('public.Referral', [], 'en'),
                'mobile_vcode' => Lang::get('public.VerificationCode', [], 'en'),
            ]);
		}
		elseif ( Config::get('setting.opcode') == 'IFW' && Session::get('currency') == 'TWD' )
        {
            // Special for Infiniwin TWD
            $rules = [
                'username' => 'required|min:6|max:12|regex:/^[a-zA-Z0-9]+$/|reserved_str|unique:account,nickname',
                'email'    => 'required|email|unique:accountdetail,email',
                'password' => 'required|alpha_num|min:6|max:12',
                'mobile'   => 'required|phone|unique:accountdetail,telmobile',
                'referralid' => 'integer|min:1|exists:account,id',
            ];

            // TODO
//            if (str_contains(Configs::getParam('SYSTEM_SMS_ONREGISTER'), 'TWD')) {
//                $rules['mobile_vcode'] = 'required|min:6';
//            }

            $validator = Validator::make(
                [
                    'username' => $request->input('username'),
                    'email'	   => $request->input('email'),
                    'password' => $request->input('password'),
                    'fullname' => $request->input('fullname'),
                    'mobile'   => $request->input('mobile'),
                    'referralid' => $request->input('referralid'),
                    'mobile_vcode' => $request->input('mobile_vcode'),
                ],
                $rules
            );
            $validator->setAttributeNames([
                'username' => Lang::get('public.Username'),
                'email' => Lang::get('public.Email'),
                'password' => Lang::get('public.Password'),
                'repeatpassword' => Lang::get('public.RepeatPassword'),
                'mobile' => Lang::get('public.ContactNo'),
                'referralid' => Lang::get('public.Referral'),
                'mobile_vcode' => Lang::get('public.VerificationCode'),
            ]);
        }
        elseif ( Config::get('setting.opcode') == 'GSC' )
        {
			if($request->input('typer')=="fast"){
				$validator = Validator::make(
					[
						'username' => $request->input('username'),
						'password' => $request->input('password'),
						'mobile'   => $request->input('mobile'),
						'referralcode'   => $request->input('referralcode'),
					],
					[
						'username' => 'required|min:6|max:12|regex:/^[a-zA-Z0-9]+$/|alpha_num|reserved_str|unique:account,nickname',
						'password' => 'required|alpha_num|min:6|max:12',
						'mobile'   => 'required|phone|unique:accountdetail,telmobile',
						'referralcode'   => 'required',
					],
					array(),
					[
						'username' => Lang::get('public.Username'),
						'password' => Lang::get('public.Password'),
						'mobile'   => Lang::get('public.ContactNo'),
						'referralcode' => Lang::get('public.RegisterCode'),
					]
				);
			}else{
				$validator = Validator::make(
					[
						'username' => $request->input('username'),
						'email'	   => $request->input('email'),
						'password' => $request->input('password'),
						'fullname' => $request->input('fullname'),
						'mobile'   => $request->input('mobile'),
						'referralcode'   => $request->input('referralcode'),
					],
					[
						'username' => 'required|min:6|max:12|regex:/^[a-zA-Z0-9]+$/|alpha_num|reserved_str|unique:account,nickname',
						'email'    => 'required|email|unique:accountdetail,email',
						'password' => 'required|alpha_num|min:6|max:12',
						'fullname' => 'required|min:2',
						'mobile'   => 'required|phone|unique:accountdetail,telmobile',
						'referralcode'   => 'required',
					],
					array(),
					[
						'username' => Lang::get('public.Username'),
						'email'	   => Lang::get('public.EmailAddress'),
						'password' => Lang::get('public.Password'),
						'fullname' => Lang::get('public.FullName'),
						'mobile'   => Lang::get('public.ContactNo'),
						'referralcode' => Lang::get('public.RegisterCode'),
					]
				);
			}
        }
        elseif ( Config::get('setting.opcode') == 'LVG' )
        {
            $validator = Validator::make(
                [

                    'username' => $request->input('username'),
                    'email'	   => $request->input('email'),
                    'password' => $request->input('password'),
                    'fullname' => $request->input('fullname'),
                    'mobile'   => $request->input('mobile'),
                    'mobile_vcode'=> $request->input('mobile_vcode'),
                ],
                [

                    'username' => 'required|min:6|max:12|regex:/^[a-zA-Z0-9]+$/|alpha_num|reserved_str|unique:account,nickname',
                    'email'    => 'required|email|unique:accountdetail,email',
                    'password' => 'required|alpha_num|min:6|max:12',
                    'fullname' => 'required|min:2|unique:account,fullname',
                    'mobile'   => 'required|phone|unique:accountdetail,telmobile',
                    'mobile_vcode'   => 'required|min:6',
                ]
            );
        }
		else
		{
			$validator = Validator::make(
				[
		
					'username' => $request->input('username'),
					'email'	   => $request->input('email'),
					'password' => $request->input('password'),
					'fullname' => $request->input('fullname'),
					'mobile'   => $request->input('mobile'),
				],
				[

					'username' => 'required|min:6|max:12|regex:/^[a-zA-Z0-9]+$/|alpha_num|reserved_str|unique:account,nickname',
					'email'    => 'required|email|unique:accountdetail,email',
					'password' => 'required|alpha_num|min:6|max:12',
					'fullname' => 'required|min:2|unique:account,fullname',
					'mobile'   => 'required|phone|unique:accountdetail,telmobile',
				]
			);
			
		}
		
		
		if ($validator->fails())
		{
			echo json_encode($validator->errors());
			exit;
		}
		
		

        return $next($request);
	}

}
