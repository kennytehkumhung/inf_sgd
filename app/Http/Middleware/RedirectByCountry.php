<?php namespace App\Http\Middleware;

use Closure;
use GeoIP;
use Config;

class RedirectByCountry {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	 
	public function handle($request, Closure $next)
	{
	
		 $geoip = GeoIP::getLocation($request->ip());

		 //For Infiniwin Taiwan Market, redirect to /tw url
		 if( isset($geoip['isoCode']) && Config::get('setting.opcode') == 'IFW' && $geoip['isoCode'] == 'TW' )
		 {
			 return redirect('/tw');
		 }
		
	
		 return $next($request);
	}

}
