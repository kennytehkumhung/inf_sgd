<?php namespace App\Http\Middleware;

use Closure;
use Session;
use Redirect;
use Auth;
use App\Models\Onlinetracker;
use Lang;
use App\Models\Account;

class LockSuspendedUser {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */ 
	 
	public function handle($request, Closure $next)
	{
		if(Account::where('id','=',Session::get('userid'))->where('status','=',0)->count() > 0){
			Session::flush();
			//Auth::user()->logout();
			return redirect::route('homepage')->with('checklogin', Lang::get('public.AccountSuspended'));
		}
		else
		{
			return $next($request);
		}
	}

}
