<?php namespace App\Http\Middleware;

use Closure;
use App\Models\Bannedip;

class Allowip {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	 
	public function handle($request, Closure $next)
	{
	     if(Bannedip::isAllowAccess())
		 {
			return $next($request);
		 }
	}

}
