<?php	
Route::group(['middleware' => 'allow_ip' ], function()
{
	
	Route::get('login', [ 'uses' => 'Admin\LoginController@index' , 'as' => 'admin_login'] );
	Route::get('', [ 'uses' => 'Admin\LoginController@index' , 'as' => 'admin_login'] );
	Route::post('login_process', 'Admin\LoginController@process');
	Route::get('captcha', 'Admin\LoginController@getCaptcha');
	Route::get('logout', [ 'uses' => 'Admin\LogoutController@index' , 'as' => 'admin_logout'] );
	Route::get('setlanguage',       [ 'uses' => 'Admin\DashboardController@setLanguage' ,   'as' => 'languages' ]);
	

	Route::group(['middleware' =>  ['auth_admin', 'mutiple_login_admin'] ], function()
	{
		
		//Route::get('home', ['as' => 'admin_home', function () {
		//	 return view('admin.index2');
		//}]);
		
		Route::get('home', [ 'uses' => 'Admin\DashboardController@show',  'as' => 'admin_home']);
		
		Route::get('enquiry', 'Admin\CashledgerController@index');
		Route::get('enquiry-data', 'Admin\CashledgerController@doGetData');
		Route::post('enquiry-data', 'Admin\CashledgerController@doGetData');
		Route::get('show-deposit', 'Admin\CashledgerController@showDeposit');
		Route::get('show-withdraw', 'Admin\CashledgerController@showWithdraw');
		Route::get('show-adjustment', 'Admin\CashledgerController@showAdjustment');
		Route::post('deposit-edit', 'Admin\CashledgerController@doDepositEdit');
		Route::post('withdraw-edit', 'Admin\CashledgerController@doWithdrawalEdit');
		Route::post('adjustment-edit', 'Admin\CashledgerController@doAdjustmentEdit');
		Route::post('cashledger-batchapprove', 'Admin\CashledgerController@batchApprove');
        Route::get('deposit-withdraw-resendsms', 'Admin\CashledgerController@resendDepositWithdrawSMS');
		Route::get('instant', 'Admin\InstantTransactionController@index');
		Route::get('instant-data', 'Admin\InstantTransactionController@doGetData');
		Route::get('failtrans', 'Admin\FailtransController@index');
		
		Route::get('failtrans-data', 'Admin\FailtransController@doGetData');
		
		Route::get('member', 'Admin\MemberEnquiryController@index');
		Route::get('member-data', 'Admin\MemberEnquiryController@doGetData');
		Route::post('member-data', 'Admin\MemberEnquiryController@doGetData');
		Route::post('member-active-suspend', 'Admin\MemberEnquiryController@doActivateSuspend');
		
		//Online member list
		Route::get('online-member-list', 'Admin\MemberEnquiryController@showonlinememberlist');
		
		
		Route::get('banner', 'Admin\BannerController@index');
		Route::get('banner-data', 'Admin\BannerController@doGetData');
		Route::get('banner-add', 'Admin\BannerController@drawAddEdit');
		Route::post('banner-do-add-edit', 'Admin\BannerController@doAddEdit');
		Route::post('banner-active-suspend', 'Admin\BannerController@doActivateSuspend');
		
		Route::get('iptracker', 'Admin\IpTrackerController@index');
		Route::get('iptracker-data', 'Admin\IpTrackerController@doGetData');
                
                Route::get('apilog', 'Admin\ApilogController@index');
		Route::get('apilog-data', 'Admin\ApilogController@doGetData');
                
                Route::get('errorlog', 'Admin\ErrorlogController@index');
		Route::get('errorlog-data', 'Admin\ErrorlogController@doGetData');
		
		Route::get('bankaccount', 'Admin\BankAccountController@index');
		Route::get('bankaccount-data', 'Admin\BankAccountController@doGetData');
		Route::get('bankaccount-edit', 'Admin\BankAccountController@drawAddEdit');
		Route::post('bankaccount-do-add-edit','Admin\BankAccountController@doAddEdit');
		Route::post('bankaccount-active-suspend', 'Admin\BankAccountController@doActivateSuspend');
		
		
		Route::get('bankprofile', 'Admin\BankProfileController@index');
		Route::get('bankprofile-data', 'Admin\BankProfileController@doGetData');
		Route::get('bankprofile-edit', 'Admin\BankProfileController@drawAddEdit');
		Route::post('bankprofile-do-add-edit','Admin\BankProfileController@doAddEdit');
		
		Route::get('bank-cashinout', 'Admin\BankController@drawCashInOut');
		Route::post('bank-cashinout-do-add-edit', 'Admin\BankController@doCashInOut');	
		
		Route::get('bank-transfer', 'Admin\BankController@drawTransfer');
		Route::post('bank-transfer-do-add-edit', 'Admin\BankController@doTransfer');
		Route::get('doUpdateBankSummary', 'Admin\BankController@doUpdateBankSummary');
	
		
		Route::get('rejectreason', 'Admin\RejectreasonController@index');
		Route::get('rejectreason-data', 'Admin\RejectreasonController@doGetData');
		Route::post('rejectreason-do-add-edit','Admin\RejectreasonController@doAddEdit');
		Route::get('rejectreason-edit', 'Admin\RejectreasonController@drawAddEdit');
		Route::post('rejectreason-active-suspend', 'Admin\RejectreasonController@doActivateSuspend');
		
		Route::get('remark', 'Admin\RemarkController@index');
		Route::get('remark-data', 'Admin\RemarkController@doGetData');
		Route::post('remark-do-add-edit','Admin\RemarkController@doAddEdit');
		Route::get('remark-edit', 'Admin\RemarkController@drawAddEdit');
		Route::post('remark-active-suspend', 'Admin\RemarkController@doActivateSuspend');
		
		Route::get('incentive', 'Admin\IncentiveController@index');
		Route::get('incentive-data', 'Admin\IncentiveController@doGetData');
		Route::post('incentive-do-add-edit','Admin\IncentiveController@doAddEdit');
		Route::get('incentive-edit', 'Admin\IncentiveController@drawAddEdit');
		Route::post('incentive-docalculate', 'Admin\IncentiveController@doCalculateIncentive');
		Route::get('incentivelist', 'Admin\IncentiveController@showincentive');
		Route::get('incentivelist-data', 'Admin\IncentiveController@doGetIncentivelistData');
		Route::post('incentivelist-batchapprove', 'Admin\IncentiveController@batchApprove');

        Route::get('revertbonus', 'Admin\RevertBonusController@index');
        Route::get('revertbonus-data', 'Admin\RevertBonusController@doGetData');
        Route::post('revertbonus-do-add-edit','Admin\RevertBonusController@doAddEdit');
        Route::get('revertbonus-edit', 'Admin\RevertBonusController@drawAddEdit');
        Route::post('revertbonus-docalculate', 'Admin\RevertBonusController@doCalculateRevertbonus');
        Route::get('revertbonuslist', 'Admin\RevertBonusController@showrevertbonus');
        Route::get('revertbonuslist-data', 'Admin\RevertBonusController@doGetRevertbonuslistData');
        Route::post('revertbonuslist-batchapprove', 'Admin\RevertBonusController@batchApprove');

        Route::get('revertBonusReport','Admin\RevertBonusController@showrevertBonusReport');
        Route::get('revertBonusReport-data','Admin\RevertBonusController@showrevertBonusReportdoGetData');

        Route::get('cashback', 'Admin\CashbackController@index');
		Route::get('cashback-data', 'Admin\CashbackController@doGetData');
		Route::post('cashback-do-add-edit','Admin\CashbackController@doAddEdit');
		Route::get('cashback-edit', 'Admin\CashbackController@drawAddEdit');
		Route::post('cashback-docalculate', 'Admin\CashbackController@doCalculateCashback');
		Route::get('cashbacklist', 'Admin\CashbackController@showcashback');
		Route::get('cashbacklist-data', 'Admin\CashbackController@doGetCashbacklistData');
		Route::post('cashbacklist-batchapprove', 'Admin\CashbackController@batchApprove');
		
		Route::get('promocampaign', 'Admin\PromocampaignController@index');
		Route::get('promocampaign-data', 'Admin\PromocampaignController@doGetData');
		Route::post('promocampaign-do-add-edit','Admin\PromocampaignController@doAddEdit');
		Route::get('promocampaign-edit', 'Admin\PromocampaignController@drawAddEdit');
		Route::post('promocampaign-active-suspend', 'Admin\PromocampaignController@doActivateSuspend');
		
		Route::get('tag', 'Admin\TagController@index');
		Route::get('tag-data', 'Admin\TagController@doGetData');
		Route::post('tag-do-add-edit','Admin\TagController@doAddEdit');
		Route::get('tag-edit', 'Admin\TagController@drawAddEdit');
		Route::post('tag-active-suspend', 'Admin\TagController@doActivateSuspend');
		
		Route::get('profitloss', 'Admin\ReportController@showMainReport');
		Route::get('profitloss-data', 'Admin\ReportController@doGetData');
		Route::post('profitloss-data', 'Admin\ReportController@doGetData');
		Route::get('showmain ', 'Admin\ReportController@showmain');
		Route::get('show', 'Admin\ReportTotalController@show');
		
		Route::get('profitloss2', 'Admin\ReportController@showMainReport2');
		Route::get('profitloss2-data', 'Admin\ReportController@doGetData2');
		
		//Report
		Route::get('config','Admin\ConfigController@index');
		Route::get('config-data','Admin\ConfigController@doGetData');
		
		Route::get('summary','Admin\ReportTotalController@show');
		
		Route::get('product', 'Admin\ProductController@show');
		Route::get('product-data', 'Admin\ProductController@doGetData');
		Route::get('product-edit', 'Admin\ProductController@drawAddEdit');
		Route::post('product-do-add-edit', 'Admin\ProductController@doAddEdit');
		Route::post('product-active-suspend', 'Admin\ProductController@doActivateSuspend');
		Route::post('product-checkBalance', 'Admin\ProductController@checkBalance');
		
		Route::get('cms','Admin\ContentController@index');
		Route::get('cms-data','Admin\ContentController@doGetData');
		Route::get('cms-showarticle','Admin\ContentController@showArticle');
		Route::get('cms-articledata','Admin\ContentController@doGetArticleData');
		Route::get('cms-addarticle','Admin\ContentController@drawAddEditArticle');
		Route::post('cms-do-add-edit','Admin\ContentController@doAddEditArticle');
		Route::post('cms-active-suspend', 'Admin\ContentController@doActivateSuspend');

		 //Settings tabs
		Route::get('bank','Admin\BankController@show');
		Route::get('bank-data','Admin\BankController@doGetData');
		Route::post('bank-do-add-edit','Admin\BankController@doAddEdit');
		Route::get('bank-edit', 'Admin\BankController@drawAddEdit');
		Route::post('bank-active-suspend', 'Admin\BankController@doActivateSuspend');
		
		 //get balance 
		Route::post('balance/{product}','Admin\CustomertabController@getMemberBalance');
		Route::post('mainwallet','Admin\CustomertabController@mainwallet');
		
		Route::get('audit-data','Admin\CashledgerController@getAuditTransaction');
		
		//Admin tabs
		Route::get('user','Admin\UserController@index');
		Route::get('user-data','Admin\UserController@doGetData');
		Route::post('user-do-add-edit','Admin\UserController@doAddEdit');
		Route::get('user-edit', 'Admin\UserController@drawAddEdit');
		
		//Configuration tab
		Route::get('config','Admin\ConfigController@index');
		Route::get('config-data','Admin\ConfigController@doGetData');
		Route::post('config-do-add-edit','Admin\ConfigController@doAddEdit');
		Route::get('config-edit', 'Admin\ConfigController@drawAddEdit');
		
		
		
		 //Member tabs
		Route::get('customer-tab','Admin\CustomertabController@index');
		Route::get('customer-tab1','Admin\CustomertabController@memberdetail');
		
		//customer tab-2-->transaction
		Route::get('customer-tab2','Admin\CustomertabController@transaction'); //transaction tab index
		Route::get('customer-tab2-data','Admin\CustomertabController@TransactiondoGetData'); //transaction tab dogetdata
		
		//customer tab-4-->tools
		Route::get('customer-tab4','Admin\CustomertabController@tool');
		Route::get('customer-tab4-data','Admin\CustomertabController@TooldoGetData');
		
		Route::get('customer-tab3','Admin\ReportController@showMainReport');
		Route::get('customer-tab3-data','Admin\ReportController@doGetData');
		
		Route::post('customer-updateprofile', 'Admin\CustomertabController@updateProfile');
		Route::post('customer-verifypassword', 'Admin\CustomertabController@verifyPassword');
		Route::get('customer-memo', 'Admin\CustomertabController@showMemo');
		Route::get('customer-bank', 'Admin\CustomertabController@showBank');
		Route::get('customer-bank-update', 'Admin\CustomertabController@updateBank');
        Route::post('customer-logout-game', 'Admin\CustomertabController@logoutGame');
		
		Route::post('sms', 'Admin\CustomertabController@sms');
		
		Route::post('inbox', 'Admin\CustomertabController@inbox');
		Route::get('inboxMessage', 'Admin\CustomertabController@showMessage');
		Route::post('sendToAll', 'Admin\CustomertabController@sendToAll');
		

		Route::get('adjustment', [ 'uses' => 'Admin\AdjustmentController@index' , 'as' => 'adjustment'] );
		
		
		Route::get('announcement','Admin\AnnouncementController@index');
		Route::get('announcement-data','Admin\AnnouncementController@doGetData');
		Route::get('announcement-edit','Admin\AnnouncementController@drawAddEdit');
		Route::post('announcement-do-add-edit','Admin\AnnouncementController@doAddEdit');
		Route::post('announcement-active-suspend', 'Admin\AnnouncementController@doActivateSuspend');

		Route::get('password','Admin\PasswordController@index');
		Route::post('password-edit','Admin\PasswordController@doAddEdit');
		
		Route::get('bankledger','Admin\BankController@showBankLedger');
		Route::get('bankledger-data','Admin\BankController@doGetBankLedgerData');

		Route::get('affiliateEnquiry-edit','Admin\AffiliateController@drawAddEdit');
		Route::post('affiliateEnquiry-do-add-edit','Admin\AffiliateController@doAddEdit');
		Route::post('affiliateRegister-do-add-edit','Admin\AffiliateController@RegisterDoAddEdit');
		
		//Affiliate Member report tab
		Route::get('affiliateMemberReport','Admin\AffiliateController@showMemberReport');
		Route::get('affiliateMemberReport-data','Admin\AffiliateController@doGetMemberReportData');		
		
		Route::get('affiliateDetailReport','Admin\AffiliateController@showDetailReport');
		Route::get('affiliateDetailReport-data','Admin\AffiliateController@doGetDetailReportData');		
		
		Route::get('affiliate-topupcredit'			,'Admin\AffiliateController@drawTopupCredit');
			
		Route::post('affiliate-topupcredit-do-add-edit'  ,'Admin\AffiliateController@TopupCreditProcess');
		
		Route::get('showdetail',['uses'=>'Admin\ReportController@showdetail','as'=>'showdetail']);
		
		//member report tab
		Route::get('memberReport','Admin\ReportController@showMemberMonthReport');
		Route::get('memberReport-data','Admin\ReportController@MemberdoGetData');

		//member referral tab
        Route::get('referralReport','Admin\ReportController@showReferralReport');
        Route::get('referralReport-data','Admin\ReportController@ReferraldoGetData');

        //sms log report tab
        Route::get('smsLogReport','Admin\ReportController@showSmsLogReport');
        Route::get('smsLogReport-data','Admin\ReportController@SmsLogdoGetData');
        Route::get('smsLogReport-do-check-balance','Admin\ReportController@SmsLogdoCheckBalance');

		//channel report tab
		Route::get('channelReport','Admin\ReportController@showChannelReport');
		Route::get('channelReport-data','Admin\ReportController@ChannelReportdoGetData');
		
		//daily report tab
		Route::get('dailyReport','Admin\ReportController@showdailyReport');
		Route::get('dailyReport-data','Admin\ReportController@showdailyReportdoGetData');
		
		//bank report tab
		Route::get('bankReport','Admin\BankController@showbankReport');
		Route::get('bankReport-data','Admin\BankController@showbankReportdoGetData');
		
		//rebate report tab
		Route::get('rebateReport','Admin\IncentiveController@showrebateReport');
		Route::get('rebateReport-data','Admin\IncentiveController@showrebateReportdoGetData');	

		//daily rebate report tab
		Route::get('dailyrebateReport','Admin\IncentiveController@showdailyrebateReport');
		Route::get('dailyrebateReport-data','Admin\IncentiveController@showdailyrebateReportdoGetData');		
		// Route::post('incentive-docalculate', 'Admin\IncentiveController@doCalculateIncentive');
		Route::get('dailyrebatelist', 'Admin\IncentiveController@showdailyrebatelist');
		Route::get('dailyrebate-data', 'Admin\IncentiveController@doGetDailylistData');
		Route::post('dailyrebate-batchapprove', 'Admin\IncentiveController@dailyrebatebatchApprove');
		
		//cashback report tab
		Route::get('cashbackReport','Admin\CashbackController@showcashbackReport');
		Route::get('cashbackReport-data','Admin\CashbackController@showcashbackReportdoGetData');
		
		//Affiliate report tab
		Route::get('affiliateReport','Admin\AffiliateController@showaffiliateReport');
		Route::get('affiliateReport-data','Admin\AffiliateController@showaffiliateReportdoGetData');
		
		//Affiliate Commision tab
		Route::get('affiliateCommision','Admin\AffiliateController@showCommissionScheme');
		Route::get('affiliateCommision-data','Admin\AffiliateController@showCommissionSchemedoGetData');
		
		//Agent report tab
		Route::get('agentReport',['uses'=>'Admin\AffiliateController@showagentReport','as'=>'agentReport']);
		Route::get('agentReport-data','Admin\AffiliateController@showagentReportdoGetData');
		
		// Agent member tab
		Route::get('agentMemberReport',['uses'=>'Admin\AffiliateController@showagentmemberReport','as'=>'agentMemberReport']);
		Route::get('agentMemberReport-data',['uses'=>'Admin\AffiliateController@showagentmemberReportdoGetData','as'=>'agentMemberReport-data']);
		
		//Affiliate Enquiry tab
		Route::get('affiliateEnquiry','Admin\AffiliateController@showEnquiry');
		Route::get('affiliateEnquiry-data','Admin\AffiliateController@EnquirydoGetData');
		
		//Affiliate Register tab
		Route::get('affiliateRegister','Admin\AffiliateController@showRegister');
		Route::get('affiliateRegister-data','Admin\AffiliateController@showRegisterdoGetData');
		Route::get('affiliateRegister-edit','Admin\AffiliateController@RegisterdrawAddEdit');
		
		//Affiliate Advertisement tab
		Route::get('affiliateAdvertisement','Admin\AffiliateController@showAdvertisement');
		Route::get('affiliateAdvertisement-data','Admin\AffiliateController@showAdvertisementdoGetData');
		Route::get('affiliateAdvertisement-data','Admin\AffiliateController@showAdvertisementdrawAddEdit');
		
		//BONUS SUMMARY
		Route::get('bonusSummary', 'Admin\ReportController@bonusSummaryShow');
		Route::get('bonusSummary-data', 'Admin\ReportController@bonusSummarydoGetData');
		Route::get('bonusSummary-edit', 'Admin\ReportController@bonusSummarydrawAddEdit');
		Route::post('bonusSummary-do-add-edit','Admin\ReportController@bonusSummarydoAddEdit');

        //BONUS REPORT
        Route::get('bonusReport', 'Admin\ReportController@bonusReportShow');
        Route::get('bonusReport-data', 'Admin\ReportController@bonusReportdoGetData');

		//Promo Report
		Route::get('promoReport', 'Admin\PromocampaignController@showPromoReport');
		Route::get('promoReport-data', 'Admin\PromocampaignController@PromoReportdoGetData');
		Route::get('promoReport-edit', 'Admin\PromocampaignController@PromoReportdrawAddEdit');
		Route::post('promoReport-do-add-edit','Admin\PromocampaignController@PromoReportdoAddEdit');		
		
		//Promotion Report
		Route::get('promotionReport', 		'Admin\PromotionReportController@index');
		Route::get('promotionReport-data',  'Admin\PromotionReportController@doGetData');

		
		//Deposit Report
		Route::get('depositReport', 'Admin\ReportController@showDepositReport');
		Route::get('depositReport-data', 'Admin\ReportController@DepositReportdoGetData');
		Route::get('depositReport-edit', 'Admin\ReportController@DepositReportdrawAddEdit');
		Route::post('depositReport-do-add-edit','Admin\ReportController@DepositReportdoAddEdit');
		
		//Website Report
		Route::get('website', 'Admin\WebsiteController@websiteShow');
		Route::get('website-data', 'Admin\WebsiteController@websiteShowdoGetData');
		Route::get('website-edit', 'Admin\WebsiteController@websiteShowdrawAddEdit');
		Route::post('website-do-add-edit','Admin\WebsiteController@websiteShowdoAddEdit');
		Route::post('website-active-suspend', 'Admin\WebsiteController@doActivateSuspend');
		
		//Payment Report
		Route::get('payment', 'Admin\PaymentController@paymentShow');
		Route::get('payment-data', 'Admin\PaymentController@paymentdoGetData');
		Route::get('payment-edit', 'Admin\PaymentController@paymentdrawAddEdit');
		Route::post('payment-do-add-edit','Admin\PaymentController@paymentdoAddEdit');

		//Payment Gateway Setting
		Route::get('paymentgatewaysetting', 'Admin\PaymentGatewaySettingController@paymentgatewaysettingShow');
		Route::get('paymentgatewaysetting-data', 'Admin\PaymentGatewaySettingController@paymentgatewaysettingdoGetData');
		Route::get('paymentgatewaysetting-edit', 'Admin\PaymentGatewaySettingController@paymentgatewaysettingdrawAddEdit');
		Route::post('paymentgatewaysetting-do-add-edit','Admin\PaymentGatewaySettingController@paymentgatewaysettingdoAddEdit');

        Route::get('privatepaymentgatewaysetting','Admin\PaymentGatewaySettingController@showPrivate');
        Route::get('privatepaymentgatewaysetting-data','Admin\PaymentGatewaySettingController@doGetPrivateData');
        Route::post('privatepaymentgatewaysetting-do-add-edit','Admin\PaymentGatewaySettingController@doAddEditPrivate');
        Route::get('privatepaymentgatewaysetting-edit', 'Admin\PaymentGatewaySettingController@drawAddEditPrivate');
        Route::post('privatepaymentgatewaysetting-active-suspend', 'Admin\PaymentGatewaySettingController@doActivateSuspend');
		
		Route::get('showdetail-data', 'Admin\ReportController@showdetailpaymentdoGetData');
		
		
		Route::get('shownavigation', 'Admin\DashboardController@showNav');
		
		//Admin Role
		Route::get('adminrole', 'Admin\AdminroleController@show');
		Route::get('adminrole-data', 'Admin\AdminroleController@doGetData');
		Route::get('adminrole-edit', 'Admin\AdminroleController@drawAddEdit');
		Route::post('adminrole-do-add-edit','Admin\AdminroleController@doAddEdit');
		Route::post('adminrole-active-suspend', 'Admin\AdminroleController@doActivateSuspend');
		
		//Currency
		Route::get('currency', 'Admin\CurrencyController@show');
		Route::get('currency-data', 'Admin\CurrencyController@doGetData');
		Route::get('currency-edit', 'Admin\CurrencyController@drawAddEdit');
		Route::post('currency-do-add-edit','Admin\CurrencyController@doAddEdit');
		Route::post('currency-active-suspend', 'Admin\CurrencyController@doActivateSuspend');
		
		Route::get('4dpromo', 'Admin\Promo4dController@index');
		Route::get('4dpromo-data', 'Admin\Promo4dController@doGetData');
		Route::get('4dpromo-list', 'Admin\Promo4dController@promolist');
		Route::get('4dpromolog', 'Admin\Promo4dController@ledger');
		Route::get('4dpromolog-data', 'Admin\Promo4dController@doGetLedgerData');		
		Route::get('4dpromobalance', 'Admin\Promo4dController@Balance');
		Route::get('4dpromobalance-data', 'Admin\Promo4dController@doGetBalanceData');
		Route::get('4dpromo-add-credit', 'Admin\Promo4dController@drawAddEdit');
		Route::post('4dpromo-do-add-edit', 'Admin\Promo4dController@doAddEdit');
		Route::get('4dpromoconfig', 'Admin\Promo4dController@showConfig');
		Route::post('4dpromoconfig-edit', 'Admin\Promo4dController@doEditConfig');

		Route::post('revert-balance','Admin\CustomertabController@revertBalance');

		// Lucky Spin
        Route::get('luckySpinReport', 'Admin\LuckySpinController@index');
        Route::get('luckySpinReport-data', 'Admin\LuckySpinController@doGetData');
        Route::get('luckySpinReport-data-edit', 'Admin\LuckySpinController@doEditData');
        Route::get('luckySpinReportDetail', 'Admin\LuckySpinController@doGetReportDetail');
        Route::get('luckySpinReportDetail-data', 'Admin\LuckySpinController@doGetReportDetailData');
        Route::get('luckySpinReportMatchDetail', 'Admin\LuckySpinController@doGetReportMatchDetail');
        Route::get('luckySpinReportMatchDetail-data', 'Admin\LuckySpinController@doGetReportMatchDetailData');
        Route::get('luckySpinReportTokenLedger', 'Admin\LuckySpinController@doGetReportTokenLedger');
        Route::get('luckySpinReportTokenLedger-data', 'Admin\LuckySpinController@doGetReportTokenLedgerData');
        Route::get('luckySpinReportTokenLedger-data-edit', 'Admin\LuckySpinController@doEditReportTokenLedgerData');
        Route::get('luckyspinconfig', 'Admin\LuckySpinController@showConfig');
        Route::post('luckyspinconfig-edit', 'Admin\LuckySpinController@doEditConfig');
        Route::post('luckyspinconfig-freetoken-edit', 'Admin\LuckySpinController@doEditFreeToken');
        Route::post('luckyspinconfig-tokensettings-edit', 'Admin\LuckySpinController@doEditTokenSettings');
        Route::post('luckyspinconfig-winnerlist-edit', 'Admin\LuckySpinController@doEditWinnerList');

        // EPL Premier League
        Route::get('premierLeagueMatchReport', 'Admin\PremierLeagueController@index');
        Route::get('premierLeagueMatchReport-data', 'Admin\PremierLeagueController@doGetData');
        Route::get('premierLeagueMatchReport-data-edit', 'Admin\PremierLeagueController@doEditData');
        Route::get('premierLeagueMatchReport-add-edit', 'Admin\PremierLeagueController@drawAddEditMatch');
        Route::post('premierLeagueMatchReport-do-add-edit', 'Admin\PremierLeagueController@doAddEditMatch');
        Route::get('premierLeagueBetReport', 'Admin\PremierLeagueController@doGetBetReport');
        Route::get('premierLeagueBetReport-data', 'Admin\PremierLeagueController@doGetBetReportData');
        Route::get('premierLeagueTokenReport', 'Admin\PremierLeagueController@doGetTokenReport');
        Route::get('premierLeagueTokenReport-data', 'Admin\PremierLeagueController@doGetTokenReportData');
        Route::get('premierLeagueWinnerListConfig-add-edit', 'Admin\PremierLeagueController@drawWinnerListConfig');
        Route::post('premierLeagueWinnerListConfig-do-add-edit', 'Admin\PremierLeagueController@doAddEditWinnerListConfig');
        Route::get('premierLeagueGiveToken-add-edit', 'Admin\PremierLeagueController@drawGiveToken');
        Route::post('premierLeagueGiveToken-do-add-edit', 'Admin\PremierLeagueController@doAddEditGiveToken');
        Route::get('premierLeagueUpComingMatch-add-edit', 'Admin\PremierLeagueController@drawUpComingMatch');
        Route::post('premierLeagueUpComingMatch-do-add-edit', 'Admin\PremierLeagueController@doAddEditUpComingMatch');

		//member wallet
		Route::get('showMemberWallet', 		'Admin\ReportController@showMemberWallet');
		Route::get('showMemberWallet-data',  'Admin\ReportController@doGetMemberWalletData');		
		
		//
		Route::get('showMemberWallet', 		'Admin\ReportController@showMemberWallet');
		Route::get('showMemberWallet-data',  'Admin\ReportController@doGetMemberWalletData');
		
		Route::get('showCashcard', 		 	 'Admin\CashcardController@index');
		Route::get('showCashcard-data',  	 'Admin\CashcardController@doGetData');
		
		Route::get('showWithdrawcard', 		 'Admin\WithdrawcardController@index');
		Route::get('showWithdrawcard-data',  'Admin\WithdrawcardController@doGetData');
		
		Route::get('showBankList','Admin\CustomertabController@showBankList');
		Route::post('updateBankInfo','Admin\CustomertabController@updateBankInfo');

		//world cup report
                Route::get('worldCupReport','Admin\WorldCupController@showWorldCupReport');
                Route::get('worldCupReport-data','Admin\WorldCupController@showWorldCupReportdoGetData');
                Route::get('worldCupPayoutReport','Admin\WorldCupController@showWorldCupPayoutReport');
                Route::post('worldCupPayoutReport-data','Admin\WorldCupController@showWorldCupPayoutReportdoGetData');
                Route::post('worldCupPayoutAddToken','Admin\WorldCupController@addToken');
                Route::get('showTokenReport','Admin\WorldCupController@showTokenReport');
                Route::get('showTokenReport-data','Admin\WorldCupController@showTokenReporttdoGetData');


        Route::get('notification','Admin\NotificationController@showTotalUnseenMessage');
        Route::any('minigame','Admin\ConfigController@minigamedate');

	});
	
});