<?php	
	
	
		//Route::group(['prefix' => 'affiliate'], function () {
			
			Route::get('', [ 'uses' => 'Affiliate\LoginController@index' , 'as' => 'affiliate_login'] );
			Route::get('register', [ 'uses' => 'Affiliate\RegisterController@index' , 'as' => 'register'] );
			Route::post('register_process', [ 'uses' => 'Affiliate\RegisterController@process' , 'as' => 'register_process'] );
			Route::post('login_process', 'Affiliate\LoginController@process');
			Route::get('captcha', 'Affiliate\LoginController@getCaptcha');
			Route::get('logout', [ 'uses' => 'Affiliate\LogoutController@index' , 'as' => 'affiliate_logout'] );
			Route::get('setlanguage',       [ 'uses' => 'Affiliate\LoginController@setLanguage' ,   'as' => 'languages' ]);

			Route::get('commision', ['as' => 'affiliate_commision', function () {
				if (Config::get('setting.front_path') == 'idnwin99') {
				    return view('affiliate.commision_idnwin99');
                }
			    return view('affiliate.commision');
			}]); 	
			
			Route::get('faq', ['as' => 'affiliate_faq', function () {
				 return view('affiliate.faq');
			}]); 
			
			Route::group(['middleware' => 'auth_affiliate'], function()
			{
				 Route::get('affiliates', ['as' => 'affiliate_home', function () {
					 return view('affiliate.index');
				}]); 
				
				Route::get('agents', ['as' => 'agent_home', function () {
					 return view('agent.index');
				}]);

                if (Config::get('setting.opcode') == 'GSC') {
                    //MEMBER ENQUIRY TAB
                    Route::get('member', 'Admin\MemberEnquiryController@index');
                    Route::get('member-data', 'Admin\MemberEnquiryController@doGetData');
                    Route::post('member-data', 'Admin\MemberEnquiryController@doGetData');
                    Route::post('member-active-suspend', 'Admin\MemberEnquiryController@doActivateSuspend');

                    Route::get('customer-tab','Admin\CustomertabController@index');
                    Route::get('customer-tab1','Admin\CustomertabController@memberdetail');

                    //customer tab-2-->transaction
                    Route::get('customer-tab2','Admin\CustomertabController@transaction'); //transaction tab index
                    Route::get('customer-tab2-data','Admin\CustomertabController@TransactiondoGetData'); //transaction tab dogetdata

                    //customer tab-4-->tools
                    Route::get('customer-tab4','Admin\CustomertabController@tool');
                    Route::get('customer-tab4-data','Admin\CustomertabController@TooldoGetData');

                    Route::get('customer-tab3','Admin\ReportController@showMainReport');
                    Route::get('customer-tab3-data','Admin\ReportController@doGetData');

                    Route::post('customer-updateprofile', 'Admin\CustomertabController@updateProfile');
                    Route::post('customer-verifypassword', 'Admin\CustomertabController@verifyPassword');
                    Route::get('customer-memo', 'Admin\CustomertabController@showMemo');
                    Route::get('customer-bank', 'Admin\CustomertabController@showBank');
                    Route::get('customer-bank-update', 'Admin\CustomertabController@updateBank');
                    Route::post('customer-logout-game', 'Admin\CustomertabController@logoutGame');
                    Route::post('sms', 'Admin\CustomertabController@sms');
                    Route::post('inbox', 'Admin\CustomertabController@inbox');
                    Route::get('inboxMessage', 'Admin\CustomertabController@showMessage');
                    Route::post('sendToAll', 'Admin\CustomertabController@sendToAll');

                    Route::get('profitloss', 'Admin\ReportController@showMainReport');
                    Route::get('profitloss-data', 'Admin\ReportController@doGetData');
                    Route::post('profitloss-data', 'Admin\ReportController@doGetData');
                    Route::get('showmain ', 'Admin\ReportController@showmain');
                    Route::get('show', 'Admin\ReportTotalController@show');

                    Route::get('profitloss2', 'Admin\ReportController@showMainReport2');
                    Route::get('profitloss2-data', 'Admin\ReportController@doGetData2');

                    //rebate report tab
                    Route::get('rebateReport','Admin\IncentiveController@showrebateReport');
                    Route::get('rebateReport-data','Admin\IncentiveController@showrebateReportdoGetData');
                    Route::get('incentivelist', 'Admin\IncentiveController@showincentive');
                    Route::get('incentivelist-data', 'Admin\IncentiveController@doGetIncentivelistData');
                    Route::post('incentivelist-batchapprove', 'Admin\IncentiveController@batchApprove');

                    Route::post('balance/{product}','Admin\CustomertabController@getMemberBalance');
                    Route::post('mainwallet','Admin\CustomertabController@mainwallet');

                    //member wallet
                    Route::get('showMemberWallet', 		'Affiliate\ReportController@showMemberWallet');
                    Route::get('showMemberWallet-data',  'Affiliate\ReportController@doGetMemberWalletData');

                    Route::get('agentMemberReport',['uses'=>'Admin\AffiliateController@showagentmemberReport','as'=>'agentMemberReport']);
					Route::get('agentMemberReport-data',['uses'=>'Admin\AffiliateController@showagentmemberReportdoGetData','as'=>'agentMemberReport-data']);

                    //member report tab
                    Route::get('memberReport','Admin\ReportController@showMemberMonthReport');
                    Route::get('memberReport-data','Admin\ReportController@MemberdoGetData');

                    Route::get('iptracker', 'Admin\IpTrackerController@index');
                    Route::get('iptracker-data', 'Admin\IpTrackerController@doGetData');

                    Route::get('showdetail',['uses'=>'Admin\ReportController@showdetail','as'=>'showdetail']);
                    Route::get('showdetail-data', 'Admin\ReportController@showdetailpaymentdoGetData');
                } else {
                    //MEMBER ENQUIRY TAB
                    Route::get('member', 'Affiliate\MemberEnquiryController@index');
                    Route::get('member-data', 'Affiliate\MemberEnquiryController@doGetData');
                    Route::post('member-do-add-edit','Affiliate\MemberEnquiryController@doAddEdit');
                    Route::get('member-edit', 'Affiliate\MemberEnquiryController@drawAddEdit');
                    Route::post('member-active-suspend', 'Affiliate\MemberEnquiryController@doActivateSuspend');

                    //PROFIT LOSS TAB
                    Route::get('profitloss', 'Affiliate\ProfitlossController@index');
                    Route::get('profitloss-data', 'Affiliate\ProfitlossController@doGetData');
                    Route::post('profitloss-do-add-edit','Affiliate\ProfitlossController@doAddEdit');
                    Route::get('profitloss-edit', 'Affiliate\ProfitlossController@drawAddEdit');

                    Route::get('showdetail','Affiliate\ProfitlossController@showdetail');
                    Route::get('showdetail-data', 'Affiliate\ProfitlossController@showdetailpaymentdoGetData');
                }

				//AGENT LIST
				Route::get('agent', 'Affiliate\AgentController@index');
				Route::get('agent-data', 'Affiliate\AgentController@doGetData');
				Route::post('agent-do-add-edit','Affiliate\AgentController@doAddEdit');
				Route::get('agent-edit', 'Affiliate\AgentController@drawAddEdit');
				
				//Top Agent Credit
				Route::get('agent-topupcredit'			,'Affiliate\AgentController@drawTopupCredit');
				Route::post('agent-topupcredit-do-add-edit'  ,'Affiliate\AgentController@TopupCreditProcess');
				
				//AGENT REPORT

				Route::get('agentReport',['uses'=>'Affiliate\AgentController@showagentReport','as'=>'agentReport']);
				Route::get('agentReport-data', 'Affiliate\AgentController@showagentReportdoGetData');
				
	
				//AFFILIATE REPORT TAB
				Route::get('affiliateReport', 'Affiliate\ProfitlossController@showAffiliateReport');
				Route::get('affiliateReport-data', 'Affiliate\ProfitlossController@showAffiliateReportdoGetData');
				Route::post('affiliateReport-do-add-edit','Affiliate\ProfitlossController@showAffiliateReportdoAddEdit');
				Route::get('affiliateReport-edit', 'Affiliate\ProfitlossController@showAffiliateReportdrawAddEdit');
				Route::get('affiliateReport-edit', 'Affiliate\ProfitlossController@showMemberReport');
				
				//CHANGE PASSWORD TAB
				Route::get('password','Affiliate\PasswordController@index');
				Route::post('password-edit','Affiliate\PasswordController@doAddEdit');			
				
				Route::get('purchase-password','Affiliate\PasswordController@purchasePassword');
				Route::post('purchase-password-edit','Affiliate\PasswordController@doAddEditPurchasePassword');
				
				

			});
		//}
	