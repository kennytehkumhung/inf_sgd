<?php

namespace App\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * @var array
     */
    protected $middleware = [
        \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
        \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
        \Illuminate\Session\Middleware\StartSession::class,
        \Illuminate\View\Middleware\ShareErrorsFromSession::class,
        //\App\Http\Middleware\VerifyCsrfToken::class,
        \App\Http\Middleware\Localization::class,
    ];

    /**
     * The application's route middleware.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth' => \App\Http\Middleware\Authenticate::class,
        'auth_admin' => \App\Http\Middleware\Admin::class,
        'auth_affiliate' => \App\Http\Middleware\Affiliate::class,
        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'guest' => \App\Http\Middleware\RedirectIfAuthenticated::class,
		'demo' => Middleware\demo::class,
		'Register' => Middleware\Register::class,
		'Password' => Middleware\Password::class,
		'Currency' => Middleware\Currency::class,
		'allow_ip' => Middleware\Allowip::class,
		'mutiple_login_admin' => Middleware\MutipleLoginAdmin::class,
		'mutiple_login_user'  => Middleware\MutipleLoginUser::class,
        'mobile_route' => Middleware\MobileRoute::class,
        'redirect_countrycode' => Middleware\RedirectByCountry::class,
		'lock_suspended_user' => Middleware\LockSuspendedUser::class,
    ];
}
