<?php
/* Event::listen('illuminate.query', function($sql) { Log::info($sql); });
Log::listen(function($level, $message, $context)
{

}); */
if( isset($_SERVER['HTTP_HOST']) && ( in_array( $_SERVER['HTTP_HOST'] , Config::get('setting.affiliate') )))
{
	require __DIR__.'/affiliate.php';
	
}
else if( isset($_SERVER['HTTP_HOST'])  && ( in_array( $_SERVER['HTTP_HOST'] , Config::get('setting.admin') )))
{
	require __DIR__.'/admin.php';
	
}
else if( isset($_SERVER['HTTP_HOST'])  && ( in_array( $_SERVER['HTTP_HOST'] , Config::get('setting.app') )))
{
	require __DIR__.'/app.php';
	
}
else if( isset($_SERVER['HTTP_HOST'])  && $_SERVER['HTTP_HOST'] == 'w88.'. Config::get('setting.domain')  )
{
	Route::get('auth.php', 		 'Api\W88Controller@doValidateTicket');
	Route::post('mobile.php',	 'Api\W88Controller@doValidateMobile');
	Route::get('yar31EiWRm.php', 'Api\W88Controller@doValidateTicket');
} 
else if( isset($_SERVER['HTTP_HOST'])  && $_SERVER['HTTP_HOST'] == 'gdxauth.'. Config::get('setting.domain')  )
{
	Route::get('', 'Api\GdxController@doValidateTicket');
	Route::post('', 'Api\GdxController@doValidateTicket');
}
else if (isset($_SERVER['HTTP_HOST']) && starts_with($_SERVER['HTTP_HOST'], 'esgame.'))
{
    Route::get('fortune-wheel/{act}',     [ 'uses' => 'User\FortuneWheelController@index' ,  			'as' => 'fortune-wheel' ] );
}
else
{

	if (isset($_SERVER['HTTP_HOST'])) {
		$url = explode('.',$_SERVER['HTTP_HOST']);

		if(count($url) == 3) {
			\App\Models\Affiliate::check($url[0]);
		}
	}

	Route::get('promotion/content/{id}',  [ 'uses' =>  'User\HomeController@promotionContent',   'as' => 'promo_content']);
	Route::get('content/{id}/',           [ 'uses' => 'User\HomeController@doGetContent',  	     'as' => 'content' ]);
 	Route::get('announcement',            [ 'uses' => 'User\AnnouncementController@getList',     'as' => 'announcement' ]);
 	Route::get('Api/agg', 'Api\AggController@index');
	Route::get('Api/ibc', 'Api\IbcController@index');
	Route::get('Api/ibc/repull', 'Api\IbcController@doRepull');
	Route::post('Api/ibc/auth', 'Api\IbcController@doAuthenticateToken');
	Route::post('Api/opu/auth', 'Api\OpuController@doAuthenticateToken');
	Route::get('Api/ct8', 'Api\Ct8Controller@index');
	Route::get('Api/w88', 'Api\W88Controller@index');
	Route::get('yar31EiWRm.php', 'Api\W88Controller@doValidateTicket');
	Route::get('yjashd8sos0.php', 'Api\SPGController@doValidateTicket');
	Route::get('yjashd8sos1.php', 'Api\SPGController@doValidateTicket');
    Route::post('yjashd8sos0.php', 'Api\SPGController@doValidateTicket');
	Route::post('yjashd8sos1.php', 'Api\SPGController@doValidateTicket');
	Route::get('playstarauth', 'Api\PlsController@doValidateTicket');
	Route::get('Api/psb', 'Api\PsbController@index');
	Route::get('Api/mxb', 'Api\MxbController@index');
	Route::get('Api/ctb', 'Api\CtbController@index');
	Route::get('Api/alb', 'Api\AlbController@index');
	Route::get('Api/a1a', 'Api\A1AController@index');
	Route::get('Api/hog', 'Api\HogController@index');
	Route::get('Api/plt', 'Api\PltController@index');
	Route::get('Api/pltb', 'Api\PltbController@index');
    Route::get('Api/spg', 'Api\SPGController@index');
	Route::get('Api/m8b', 'Api\M8bController@index');
    Route::get('Api/fbl', 'Api\FblController@index');
	Route::get('Api/psb/getresult', 'Api\PsbController@getResult');
	Route::get('Api/psb/settlebet', 'Api\PsbController@getSettleBet');
	Route::get('Api/psb/InsertWager', 'Api\PsbController@InsertWager');
	Route::get('captcha',  [ 'uses' =>  'User\LoginController@getCaptcha',  'as' => 'captcha' ]);
	Route::get('Api/dev', 'Api\DevController@index');
	Route::get('Api/task/{act}', 'Api\TaskController@index')->where(['act'=>'[a-zA-Z]+']);
    Route::get('Api/clo', 'Api\CloController@index');
    Route::get('Api/clo/getresult', 'Api\CloController@getResult');
	Route::get('Api/tbs', 'Api\TbsController@index');
	Route::get('Api/s128', 'Api\S128Controller@index');
	Route::get('Api/pls', 'Api\PlsController@index');
	Route::get('Api/wft', 'Api\WftController@index');
	Route::get('Api/wft/repull', 'Api\WftController@doRepull');
	Route::get('Api/gdx', 'Api\GdxController@index');
	Route::get('Api/sbof', 'Api\SbofController@index');
	Route::get('Api/asc', 'Api\AscController@index');
	Route::get('Api/ascleague', 'Api\AscController@getLeagueName');
	Route::get('Api/ascteam', 'Api\AscController@getTeamName');
	Route::get('Api/affiliatelreport', 'Api\AffiliateReportController@index');
	Route::get('Api/referral', 'Api\AffiliateReportController@referral');
	Route::get('Api/osg', 'Api\OsgController@index');
	Route::get('Api/vgs', 'Api\VgsController@index');
	Route::get('Api/opu', 'Api\OpuController@index');
	Route::get('Api/sky', 'Api\SkyController@index');
	Route::match(['get', 'post'], 'Api/jok/{currency}', 'Api\JokController@index')->where(['currency'=>'[A-Z]{3}']);
	Route::get('Api/isn', 'Api\IsnController@index');
	Route::any('api/isn/authorize/{currency}', 'Api\IsnController@authorizePlayer')->where(['currency'=>'[a-z]{3}']);
	Route::any('api/uc8/authorize/{currency}', 'Api\Uc8Controller@authorizePlayer')->where(['currency'=>'[a-z]{3}']);
    Route::any('api/spg/validticket/{currency}', 'Api\SPGController@doValidateTicket2')->where(['currency'=>'[a-z]{3}']);
	Route::get('android.apk', 'User\HomeController@androidApk');
	Route::get('android.apk. 1', 'User\HomeController@androidApk');
	Route::get('android.apk.1', 'User\HomeController@androidApk');
	Route::get('updatebalance', 'Api\DevController@updateBalance');
	Route::post('JKHA98SDYAs98yd', 'Api\DevController@addBonus');
        Route::any('api/balancetransfer', 'Api\DevController@migratePltWalletToMain');
    Route::post('smsregcode', 'User\RegisterController@sendRegistrationSms');
    Route::post('smschangepasscode', 'User\RegisterController@sendChangePassSms');
    Route::any('pg/{pgscode}/{currency}/{pgid}', 'User\PaymentGatewayController@index')->where(['pgscode' => '[a-zA-Z]{3}', 'currency' => '[a-zA-Z]{3}', 'pgid' => '[0-9]+']);
    Route::any('pgcbk/{pgscode}/{currency}', 'User\PaymentGatewayController@callback')->where(['pgscode' => '[a-zA-Z]{3}', 'currency' => '[a-zA-Z]{3}']);
    Route::any('pgrvw/{pgscode}/{currency}', 'User\PaymentGatewayController@returnView')->where(['pgscode' => '[a-zA-Z]{3}', 'currency' => '[a-zA-Z]{3}']);
    Route::match(['get', 'post'], 'appdownload', 'User\HomeController@delayedDownload');
    Route::get('drawqr', 'User\HomeController@drawQR');
        Route::any('Api/wcdeposittoken','Admin\WorldCupController@doDepositToken');
        Route::any('Api/wcpnltoken','Admin\WorldCupController@doPnlToken');
		Route::any('LandingPageRegister', 'Api\LandingPageController@index');
		Route::any('gettoken',  'Api\LandingPageController@passtoken' ) ;

         //Mini Game API
        Route::any('Api/minigame/1/getprize', 'Api\MinigameController@RainingGame');
        Route::any('Api/minigame/1/setprize', 'Api\MinigameController@RainingGameResult');
        
	/*  */
//	Route::get('dev', function () {
//		$params = ['username' => 'badeggt' , 'currency' => 'IDR'];
//		//$params = ['userid'=>1,'currency' => 'MYR', 'username' => 'badeggtest', 'mobileNumber' => '0123456789','email' => 'k@gmail.com','password' => 'mcb123'];
//		//$a = new App\libraries\providers\UC8();
//		//$a = new App\libraries\providers\SCR();
//		$a = new App\libraries\providers\S128();
//		//$a = new App\libraries\providers\MIG();
//		//$a = new App\libraries\providers\PLT();
//		//$a = new App\libraries\providers\PLTB();
//		//$a = new App\libraries\providers\PSB();
//		//$a = new App\libraries\providers\AGG();
//		//$a = new App\libraries\providers\FSW();
//		//$a = new App\libraries\sms\BulkSMS();
//		
//		//$res = $a->createAgentDownline('MYR','badbad','bigbig');
//		//$res = $a->addMember('badegg','false','MYR');
//		$res = $a->getBalance('kel123','IDR'); 
//		//$res = $a->createAgent('MYR','aaaa'); 
//		//$res = $a->createUser($params); 
//		//$res = $a->getLoginUrl('dcg','false','MYR'); 
//		//$res = $a->getLoginUrlAgent('dcg','false','MYR'); 
//		//$res = $a->getLoginUrl4Dspc('badegg','false','MYR'); 
//		//$res = $a->lottery('badegg','MYR'); 
//		//$res = $a->sms('','+60169259382');
//		//$res = $a->updatePassword($params);
//		//$res = $a->passwordFormat();
//		//$res = $a->gamesList('TWD');
//		//$res = $a->gameList();
//		//$res = $a->createUser($params);
//		//$res = $a->slotGame('badegg','CNY');
//		//$res = $a->getBalance('badeggtest','badeggtest','MYR','mcb123','0123456789','k@gmail.com');
//		//$res = $a->getBalance('badegg','MYR');
//		//$res = $a->deposit('badegg','badegg','MYR','mcb123','0123456789','k@gmail.com','10',date('U').mt_rand(1111,9999));
//		//$res = $a->withdraw('badegg','badegg','MYR','mcb123','0123456789','k@gmail.com','10',date('U').mt_rand(1111,9999));
//		//$res = $a->withdraw('badegg','MYR','20',date('U').mt_rand(1111,9999));
//		//$res = $a->deposit('badegg','MYR','50',date('U').mt_rand(1111,9999));
//		//$res = $a->getLoginUrl('badegg','CNY');
//		//$res = $a->getLoginUrlAgent('false','MYR');
//		//$res = $a->getLoginUrl('badeggtw','TWD','UCSlotMachine_NutsCommander');
//		//$res = $a->InsertWager('','MYR');
//		//$res = $a->updateCredit('badegg','MYR','50',date('U').mt_rand(1111,9999));
//		//$res = $a->setAgentFighting('MYR','dcgag','tot','3a','10');
//		
//		
//		dd($res);
//	});
	/*  */
	
	require __DIR__.'/default.php';

	Route::group(['prefix' => 'admin'], function () 
	{
		require __DIR__.'/admin.php';
	});	
	

	

}














