<?php

if(  Config::get('setting.opcode') == 'IFW' )
{

	if (Request::is('my*') || Request::is('vn*') || Request::is('tw*'))
	{
		if (Request::is('my*'))
		{

			Route::group(['prefix' => 'my'], function () {
			
				require __DIR__.'/user_routes.php';
			});
		}
		elseif (Request::is('tw*'))
        {

            Route::group(['prefix' => 'tw'], function () {

                require __DIR__.'/user_routes.php';
            });
        }

	}
	else
	{
		if( isset($_SERVER['HTTP_HOST']) && ($_SERVER['HTTP_HOST'] == 'localhost' || $_SERVER['HTTP_HOST'] == 'infiniwin.follow178.com' || $_SERVER['HTTP_HOST'] == 'infiniwin.com' || $_SERVER['HTTP_HOST'] == 'infiniwin.net' || $_SERVER['HTTP_HOST'] == 'www.infiniwin.net' || $_SERVER['HTTP_HOST'] == 'infiniwin1.com' || $_SERVER['HTTP_HOST'] == 'infiniwin2.com' || $_SERVER['HTTP_HOST'] == 'infiniwin3.com' || $_SERVER['HTTP_HOST'] == 'infiniwin.dev' || $_SERVER['HTTP_HOST'] == 'm.infiniwin.dev' ))
		{
			Route::group(['middleware' => ['redirect_countrycode']], function() 
			{
				Route::get('', 			 		[ 'uses' => 'User\HomeController@home' ,  		   'as' => 'home'])->middleware('mobile_route');
			});
			
		}else{
			
			require __DIR__.'/user_routes.php';	
		}
	}
	
}
elseif (  Config::get('setting.opcode') == 'RYW' )
{
	if (Request::is('my*') || Request::is('id*'))
	{
		if (Request::is('my*'))
		{

			Route::group(['prefix' => 'my'], function () {
			
				require __DIR__.'/user_routes.php';
			});
		}elseif(Request::is('id*'))
		{

			Route::group(['prefix' => 'id'], function () {
			
				require __DIR__.'/user_routes.php';
			});
		}

	}
	else
	{
		if( isset($_SERVER['HTTP_HOST']) && ( $_SERVER['HTTP_HOST'] == 'test.avx99.com' || $_SERVER['HTTP_HOST'] == 'myroyalewin.com' || $_SERVER['HTTP_HOST'] == 'rwbola.com' || $_SERVER['HTTP_HOST'] == 'idnwin99.com' || $_SERVER['HTTP_HOST'] == 'rwindo.com' || $_SERVER['HTTP_HOST'] == 'rwidr.com' || $_SERVER['HTTP_HOST'] == 'rwin888.com' || $_SERVER['HTTP_HOST'] == 'rwinvip.com' || $_SERVER['HTTP_HOST'] == 'myroyalewin.net' || $_SERVER['HTTP_HOST'] == 'infiniwin.dev' || $_SERVER['HTTP_HOST'] == 'm.infiniwin.dev' )){
			Route::get('', 			 		[ 'uses' => 'User\HomeController@home' ,  		   'as' => 'home'])->middleware('mobile_route');
			
		}else{
			
			require __DIR__.'/user_routes.php';	
		}
	}
	
}
elseif( Config::get('setting.front_path') == 'c59' ||  Config::get('setting.front_path') == 'demo' ||  Config::get('setting.front_path') == 'ampm' || Config::get('setting.front_path') == 'dadu' || Config::get('setting.front_path') == '369bet' ||  Config::get('setting.front_path') == 'sbm' || Config::get('setting.front_path') == 'idnwin99' || Config::get('setting.front_path') == 'lasvegas' || Config::get('setting.front_path') == 'gsc')
{
		require __DIR__.'/user_routes.php';	
}