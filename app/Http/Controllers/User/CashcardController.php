<?php namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller\User;
use App\Models\Agent;
use App\Models\Fundingmethodsetting;
use App\Models\Configs;
use App\Models\Bankaccount;
use App\Models\Accounttag;
use App\Models\Tag;
use App\Models\Promocampaign;
use App\Models\Media;
use App\Models\Fundingmethod;
use App\Models\Bank;
use App\Models\Product;
use App\Models\Cashledger;
use App\Models\Article;
use App\Models\Banktransfer;
use App\Models\Currency;
use App\Models\Promocash;
use App\Models\Accountbank;
use App\Models\Bankledger;
use App\Models\Wager;
use App\Models\Account;
use App\Models\Cashcard;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\libraries\App;
use Session;
use Lang;
use Validator;
use Storage;
use File;
use Cache;
use Config;
use Auth;
use Redirect;
use DB;
use Log;
use App\Jobs\ChangeLocale;



class CashcardController extends Controller{
	
	public function __construct()
	{
		
	}
	
	public function depositProcess(Request $request){
		
		$hasbonus = false;
		
		$inputs = [
            'serial'   => $request->input('serial'),
            'code'	   => $request->input('code'),
        ];

		$rules = [
            'serial'   => 'required',
            'code'     => 'required',
        ];

		
        $validator = Validator::make($inputs, $rules);

		
		if ($validator->fails())
		{
			echo json_encode($validator->errors());
			exit;
		}
		
		if( $request->has('promo') && $request->promo != '0' ) {
			$promos = Promocampaign::getPromoByMemberId(Session::get('userid'));
			if($promoObj = Promocampaign::whereRaw('startdate <= "'.date('Y-m-d H:i:s').'" AND enddate >= "'.date('Y-m-d H:i:s').'" AND status = '.CBO_STANDARDSTATUS_ACTIVE.' AND code = "'.$request->promo.'"')->first()){
				if(isset($promos[$promoObj->id])) {
					$mindeposit = $promoObj->mindeposit;
					$hasbonus = true;
				} else { echo json_encode( array( 'code' => Lang::get('COMMON.FAILED') ) );exit; }
			} else{ echo json_encode( array( 'code' => Lang::get('COMMON.FAILED') ) );exit; }
		}
		
		if( $cashcard = Cashcard::whereId($request->input('serial'))->whereNumber($request->input('code'))->whereStatus(0)->first() ){
			$cashcard->accid  = Session::get('userid');
			$cashcard->status = 1;
			
		}else{
			echo json_encode( array( 'code' => Lang::get('COMMON.FAILED') ) );exit;
		}

		
			
		if($cashcard->save())
		{
			$accountObj = Account::find($cashcard->accid);

				$cashLedgerObj = new Cashledger;
				$cashLedgerObj->accid 			= $cashcard->accid;
				$cashLedgerObj->acccode 		= $accountObj->code;
				$cashLedgerObj->accname			= Session::get('fullname');
				$cashLedgerObj->chtcode 		= CBO_CHARTCODE_CASHCARD;
				$cashLedgerObj->cashbalance 	= self::getAllBalance();
				$cashLedgerObj->amount 			= $cashcard->amount;
				$cashLedgerObj->amountlocal 	= $cashcard->amount;
				$cashLedgerObj->refid 			= $cashcard->id;
				$cashLedgerObj->status 			= 1;
				$cashLedgerObj->refobj 			= 'Cashcard';
				$cashLedgerObj->crccode 		= $cashcard->crccode;
				$cashLedgerObj->crcrate 		= Currency::getCurrencyRate($cashcard->crccode);
				$cashLedgerObj->fdmid			= 3;
				$cashLedgerObj->fee 			= 0;
				$cashLedgerObj->feelocal 		= 0;
				$cashLedgerObj->createdip		= App::getRemoteIp();

				
				if($cashLedgerObj->save())
				{
					
					$cashcard->clgid  = $cashLedgerObj->id;
					$cashcard->save();
					
					if($hasbonus)
					{
						$promoCashObj = new Promocash;
						$promoCashObj->accid 		= $cashLedgerObj->accid;
						$promoCashObj->acccode  	= $cashLedgerObj->acccode;
						$promoCashObj->chtcode  	= CBO_CHARTCODE_CASHCARD;
						$promoCashObj->pcpid 		= $promoObj->id;
						$promoCashObj->crccode  	= $cashLedgerObj->crccode;
						$promoCashObj->type     	= $promoObj->type;
						$promoCashObj->amount 		= $promoObj->getPromoCashAmount($cashLedgerObj->amount);
						$promoCashObj->amountlocal  = Currency::getLocalAmount($promoCashObj->amount, $promoCashObj->crccode);
						$promoCashObj->crcrate 		= Currency::getCurrencyRate($promoCashObj->crccode);
						$promoCashObj->clgid 		= $cashLedgerObj->id;
						$promoCashObj->remarkcode 	= $promoObj->code;
						$promoCashObj->status 		= CBO_PROMOCASHSTATUS_CONFIRMED;
						$promoCashObj->createdip 	= App::getRemoteIp();
						
						if($promoCashObj->save())
						{
							$cashLedger = new Cashledger;
							$cashLedger->addEditPromoCashLedger($cashLedgerObj->fdmid, $cashLedgerObj->id, $cashLedgerObj->status);
							
						}
							
						
					}
				}
			
		}
			
		echo json_encode( array( 'code' => Lang::get('COMMON.SUCESSFUL') ) );
		
	}
	
	public static function getAllBalance(){
		
		 $total = 0;
		
		$productObj = Product::select(['code','status'])->get();
		
	    foreach( $productObj as $object )
		{
			$total += Cache::has(Session::get('username').'_'.strtoupper($object->code).'_balance') ? (float)Cache::get(Session::get('username').'_'.strtoupper($object->code).'_balance') : 0;
		}  
	 


		
		$total += WalletController::getBalance(Session::get('userid'));
		
		return $total;
	}


	
}
