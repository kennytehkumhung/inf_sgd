<?php namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller\User;
use App\Models\Inbox;
use App\Models\Configs;
use App\Models\Toallinbox;
use Illuminate\Http\Request;
use App\libraries\App;
use Session;
use Validator;
use Lang;
use Auth;
use Config;
use DB;


class InboxMessage extends Controller {


	
	public function __construct()
	{
		
	}

	public function showMessage(Request $request){
		
		$rows = array();
		
		$total = Inbox::whereRaw('accid = '.Session::get('userid').' ')->count();
		//if($messages = Inbox::whereRaw('accid = '.Session::get('userid').' ')->Where( 'status' , '=' , 0)
		//				->skip(0)->take(20)->orderBy('id','desc')->get()){
		$crccode = Session::get('user_currency');
		$accid = Session::get('userid');
		if( $messages = DB::select( DB::raw("SELECT DISTINCT toallinbox.inboxid,inbox.* FROM inbox LEFT JOIN toallinbox 
											ON inbox.id=toallinbox.inboxid WHERE inbox.crccode = '".$crccode."' AND (inbox.accid=".$accid." OR is_all=1)
											 ORDER BY inbox.created DESC LIMIT 5"))){
		
			foreach( $messages as $message ){
				
				$status = $message->status;
				
				if( $message->is_all == 1 && Toallinbox::whereAccid(Session::get('userid'))->whereInboxid($message->id)->count() == 1 ){
					$status = 1;
				}
					$rows[] = array(
						'id'     	=> $message->id, 
						'title'     => $message->title, 
						'content'   => $message->content, 
						'status'	=> $status, 
						'date'   	=> $message->created, 
						'is_all'   	=> $message->is_all, 
					);	
				
				
				
			}
				
		}
		if ($request->has('type')) {
			if( $request->type == 'getData' ){
				echo json_encode(array('rows' => $rows));
				exit;
			}
		}
		$data['inboxMessage'] = $rows;
		

		return view(Config::get('setting.front_path').'/InboxMessage',$data);
		
	}
	public function viewMessage(Request $request){
		$Toallinbox = new Toallinbox;
		if ($request->has('id')&&$request->has('is_all')){
			$checkExist = Toallinbox::where( 'accid' , '=' , Session::get('userid'))->where( 'inboxid' , '=' , $request->id)->count('id');
			if ($request->is_all == 0 ){
				Inbox::where( 'id' , '=' , $request->id )->update( [ 'status' => 1 ] );
			}else if($request->is_all == 1 ){
				if($checkExist==0){
					$Toallinbox->accid 	 		= Session::get('userid');
					$Toallinbox->inboxid 	 	= $request->id;
					$Toallinbox->save();
				}
			}
		}
		//return view(Config::get('setting.front_path').'/InboxMessage',$data);
	}
	
	public function showTotalUnseenMessage(Request $request){
		if(  Auth::user()->check())
		{
			$data['unseenMessage'] = Inbox::where( 'accid' ,'=',Session::get('userid'))
			->where( 'status' , '=' ,0)->where( 'is_all' , '=' ,0)
			->where( 'crccode' , '=' ,Session::get('user_currency'))
                        ->orderBy('created', 'desc')
                        ->limit(5)
			->count('id');
			
			$data['toAllUnseenMessage'] = Inbox::where( 'status' , '=' ,0)->where( 'is_all' , '=' ,1)->where( 'crccode' , '=' ,Session::get('user_currency'))->orderBy('created', 'desc')->limit(5)->count('id') - Toallinbox::where( 'accid' ,'=',Session::get('userid'))->count('id');
			if($data['toAllUnseenMessage']<=0){
                            $data['toAllUnseenMessage']=0;
                        }
			$data['totalUnseenMessage']=$data['unseenMessage']+$data['toAllUnseenMessage'];
			
			if($data['totalUnseenMessage']<=0){
				$data['totalUnseenMessage']=0;
			} else if ($data['totalUnseenMessage']>=5){
                                $data['totalUnseenMessage']=5;
                        }
			
			echo $data['totalUnseenMessage'];
		}
	}
	

}