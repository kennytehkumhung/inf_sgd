<?php namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller\User;
use Illuminate\Http\Request;
use App\libraries\App;
use App\Models\Betpsbresult;
use Session;
use Cache;


class LandingpageController extends Controller{
	
	public function __construct()
	{
		
	}

	public function page4d(){
		
		$result = Cache::rememberForever('4dresult', function() {
			return Betpsbresult::get();
		});
		
		foreach( $result as $key => $value ){
			
			$data[$value->GameName]['first']  = $value->FirstPrizeNumber;
			$data[$value->GameName]['second'] = $value->SecondPrizeNumber;
			$data[$value->GameName]['third']  = $value->ThirdPrizeNumber;
			$data[$value->GameName]['fourth'] = $value->FourthPrizeNumber;
			$data[$value->GameName]['fifth']  = $value->FifthPrizeNumber;
			$data[$value->GameName]['six']    = $value->SixPrizeNumber;

		}
	var_dump($data);
		return view('front.4d');
		
	}	
	

	
}
