<?php namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller\User;
use App\Models\Account;
use App\Models\Accountdetail;
use App\Models\Article;
use App\Models\Configs;
use App\Models\Website;
use Crypt;
use DB;
use Illuminate\Http\Request;
use App\libraries\App;
use Mail;
use Lang;
use Hash;
use Cache;
use Auth;
use Validator;
use Session;
use Illuminate\Contracts\Auth\Guard;
use App\libraries\sms\BulkSMS;
use App\Models\Smslog;
use Carbon\Carbon;

class PasswordController extends Controller{
	
	public function __construct()
	{
		//\$this->middleware('Password');
	}

	public function index(Request $request)
	{
		echo Account::where( 'id' , '=' , Session::get('userid') )->
				 update(
					array(
						'password'       => Hash::make( $request->input('newpass') ),
						'lastpwdchange'  => date('Y-m-d H:i:s') ,
						'resetpass'      => 0 ,
					)
				 );
		 
	}	
	
	public function ResetPassword(Request $request)
	{
		$validator = Validator::make(
			[
				'username' => $request->input('username'),
				'email'    => $request->input('email'),
			],
			[
				'username' => 'required',
				'email'    => 'required|email',
			]
		);

        $validator->setAttributeNames(array(
            'username' => Lang::get('public.Username'),
            'email' => Lang::get('public.Email'),
        ));
		
		if ($validator->fails())
		{
			echo json_encode($validator->errors());
			exit;
		}
		
		$website = Website::where( 'code' , '=' , Session::get('opcode') )->first();
//        $acdObj = Accountdetail::where( 'email' , '=' , $request->input('email') )->where( 'acccode' , '=' , $website->memberprefix . '_' . $request->input('username') )->first();
        $acdObj = DB::table('accountdetail as a')
            ->join('account as b', 'b.id', '=', 'a.accid')
            ->where('b.crccode', '=', Session::get('currency'))
            ->where('a.email', '=', $request->input('email'))
            ->where('a.acccode', '=', $website->memberprefix . '_' . $request->input('username'))
            ->select(array('a.*'))
            ->first();


		if( $acdObj )
		{
			
			$random_password = App::generatePassword(6, 10);
			
			$success = Account::where( 'id' , '=' , $acdObj->accid )->
						   update(
								[
								'password'			=>	Hash::make( $random_password ) , 
								'lastpwdchange'		=>	date('Y-m-d H:i:s'),
                                'enpassword'        => Crypt::encrypt( $random_password )
								]
						   );
			if( $success )
			{
				$code = 'PWRESET_EN';

                if( $atcObj = Article::where( 'code' , '=' , 'PWRESET_EN' )->first() )
				{
                    $content = str_replace('&nbsp;', ' ', html_entity_decode($atcObj->content));
					$content = str_replace('%username%', $request->input('username') , $content);
					$content = str_replace('%password%', $random_password, $content);

//					$status = Mail::raw(html_entity_decode($content), function ($message) use ($atcObj,$acdObj)
					//$status = Mail::raw('Sending emails with Mailgun and Laravel is easy!', function($message) use ($atcObj,$acdObj){
                    $status = Mailgun::send('front.mail', compact('content'), function ($message) use ($atcObj,$acdObj){
						$message->to($acdObj->email);
						$message->subject($atcObj->title);
					});

					if($status){
                    echo json_encode( array( 'code' => Lang::get('COMMON.SUCESSFUL') ) );
					}
                }
				
			}
        }else{
			echo json_encode( array( 'code' => Lang::get('public.InvalidUsernameOrEmail') ) );
		}

    }

	public function SMSResetPassword(Request $request)
	{
		Lang::setLocale($request->input('language', 'en'));
			$supportedRegion = array('MYR', 'TWD');

	    // Only enable for MYR currency.
        if (!in_array(Session::get('currency'), $supportedRegion)) {
            return response()->json(array(
                'code' => 1,
                'msg' => Lang::get('public.UnsupportedRegion')
            ));
        }

        // SMS config.
        $smsMethod = 'forgotpass';
	
        $error = Lang::get('validation.regex', array('attribute' => Lang::get('public.ContactNumber')));
        $telmobile = $request->tel_mobile;

        /* if (Session::get('currency') == 'MYR') {
            if (!starts_with($telmobile, '01')) {
                return response()->json(array(
                    'code' => 2,
                    'msg' => Lang::get('public.ContactNumberMustStartsWithX', array('p1' => '01')),
                ));
            }
        } elseif (Session::get('currency') == 'TWD') {
            if (!starts_with($telmobile, '09')) {
                return response()->json(array(
                    'code' => 2,
                    'msg' => Lang::get('public.ContactNumberMustStartsWithX', array('p1' => '09')),
                ));
            }
        } else {
            // Should not happen.
            return response()->json(array(
                'code' => 1,
                'msg' => Lang::get('public.UnsupportedRegion'),
            ));
        } */

        // Check contact number format.
        if (preg_match('/\d{9,}/', $telmobile) == 1) {
            // Check if number is in use.
            $obj = Accountdetail::where('telmobile', '=', $telmobile)->first();

				$smsObj = new BulkSMS();
				$smsProvider = BulkSMS::NAME;
		
				// Add country code to tel number.
                $telmobile = $smsObj->formatTel(Session::get('currency'), $telmobile);

                // Check IP spam.
                $requestCount = Smslog::where('sender_ip', '=', $request->ip())
                    ->where('created_at', '>=', Carbon::now()->addMinutes(-60)->toDateTimeString())
                    ->where('method', '=', $smsMethod)
//                    ->where('ref_status', '=', '0') // Strict only 3 SMS within # seconds
                    ->count();

                if ($requestCount >= 3) {
                    // IP has requested # of times on last minutes, consider as spam.
                    $error = Lang::get('public.PleaseWaitForXSecondsToResendSMS', array('p1' => 60));
                } else {
                    // IP is safe.
                    // Check latest user request for verify code.
                    $smslogObj = Smslog::where('tel_mobile', '=', $telmobile)
                        ->where('method', '=', $smsMethod)
                        ->where('ref_status', '=', '0')
                        ->orderBy('created_at', 'desc')
                        ->first();

                    if ($smslogObj) {
                        // Has record found, check if send within period to prevent SMS spam.
                        $sendDt = Carbon::parse($smslogObj->created_at);
                        $resendSecLimit = 60;
                        $sentSecInterval = $sendDt->diffInSeconds(Carbon::now());

                        if ($sentSecInterval >= $resendSecLimit) {
                            // Is ok to send SMS.
                            $error = '';
                        } else {
                            $error = Lang::get('public.PleaseWaitForXSecondsToResendSMS', array('p1' => ($resendSecLimit - $sentSecInterval)));
                        }
                    } else {
                        // No record found, is ok to send SMS.
                        $error = '';
                    }
                }
               
			
			$website = Website::where( 'code' , '=' , Session::get('opcode') )->first();
//        	$acdObj = Accountdetail::where( 'email' , '=' , $request->input('email') )->where( 'acccode' , '=' , $website->memberprefix . '_' . $request->input('username') )->first();
			$acdObj = DB::table('accountdetail as a')
            ->join('account as b', 'b.id', '=', 'a.accid')
            ->where('b.crccode', '=', Session::get('currency'))
            ->where('a.telmobile', '=', $request->input('tel_mobile'))
            ->where('a.acccode', '=', $website->memberprefix . '_' . $request->input('username'))
            ->select(array('a.*'))
            ->first();


		if( $acdObj )
		{
			
			$random_password = substr(str_shuffle("0123456789"), 0, 6);
			
			
			
			$success = Account::where( 'id' , '=' , $acdObj->accid )->
						   update(
								[
								'password'			=>	Hash::make( $random_password ) , 
								'lastpwdchange'		=>	date('Y-m-d H:i:s'),
                                'enpassword'        => Crypt::encrypt( $random_password )
								]
						   );
	
                $smsRequest = $smsObj->getShortCurrency(Session::get('currency')) . '0.00 ' .
                'Lasvegas : Hi,'.$request->input('username').'.'.' This message has been sent to you so you can reset your login account password with '. $random_password.'.'.
				' Please change your password after login your account. This code will expire in half hour.';
				
                $smslogObj = Smslog::quickCreate(0, '', $request->ip(), $smsProvider, $smsMethod, $telmobile, $smsRequest, '', $random_password, 0);
                $response = '';

                try {
                    $response = $smsObj->sms($smsRequest, $telmobile);
                } catch (\Exception $ex) {
                    // Nothing.
                }

                $smslogObj->response = $response;
                $smslogObj->save();

                return response()->json(array(
                    'code' => 0,
                    'msg' => '',
                ));
            }else{
			echo json_encode( array( 'code' => Lang::get('public.InvalidUsernameOrEmail') ) );
		}
        }

        return response()->json(array(
            'code' => 1,
            'msg' => $error,
        ));
	
	}
}
