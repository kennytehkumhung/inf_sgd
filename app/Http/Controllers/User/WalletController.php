<?php namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller\User;
use App\libraries\App;
use App\Models\Account;
use App\Models\Cashledger;
use App\Models\Accountproduct;
use App\Models\Balancetransfer;
use App\Models\Productwallet;
use App\Models\Product;
use App\Models\Currency;
use App\Models\Fundingmethod;
use App\Models\Bankaccount;
use App\Models\Apilog;
use Session;
use Illuminate\Http\Request;
use Config;
use Lang;
use Validator;
use Cache;



class WalletController extends Controller{
	
	public function __construct()
	{
		
	}

	public static function index(Request $request, $product)
	{

		if($request->has('product')){
			$product = $request->get('product');
		}
		
		$productObj = Product::select(['code','status'])->get();
		
		foreach( $productObj as $object )
		{
			$products[$object->code] = $object->status;
		}
		
		$className = "App\libraries\providers\\".$product;
		$obj = new $className;
		
		if( $products[$product] == 2 )
		{
			$balance = Lang::get('Maintenance');
		}
		else
		{
			$balance = $obj->getBalance( Session::get('username'), Session::get('currency'));
			$balance = ($balance == false) ? 0 : $balance;
			$balance = Session::get('currency') == 'VND' || Session::get('currency') == 'IDR' ? $balance * 1000 : $balance;
			
			Cache::put(Session::get('username').'_'.strtoupper($product).'_balance', (string)$balance, 60);
	
		}
		
		echo $balance;
	}
	
	public static function mainwallet($numberformat = true){
		
		$condition = '((status in ('.CBO_LEDGERSTATUS_PENDING.', '.CBO_LEDGERSTATUS_MANPENDING.','.CBO_LEDGERSTATUS_CONFIRMED.', '.CBO_LEDGERSTATUS_MANCONFIRMED.', '.CBO_LEDGERSTATUS_PROCESSED.') AND chtcode IN ('.CBO_CHARTCODE_WITHDRAWAL.', '.CBO_CHARTCODE_BALANCETRANSFER.', '.CBO_CHARTCODE_WITHDRAWALCHARGES.' )) OR (status in ('.CBO_LEDGERSTATUS_CONFIRMED.', '.CBO_LEDGERSTATUS_MANCONFIRMED.') AND chtcode NOT IN ('.CBO_CHARTCODE_WITHDRAWAL.', '.CBO_CHARTCODE_BALANCETRANSFER.' , '.CBO_CHARTCODE_WITHDRAWALCHARGES.' )))';


		$condition .= ' AND accid = '. Session::get('userid');
		
		$balance = Cashledger::whereRaw($condition)->sum('amount');

		if( $numberformat == true){
			echo number_format($balance, 2, '.', '');
		}else{
			return $balance;
		}
		
	}
	
	public function deposit(Request $request){
	

		
	
		//return view('front/deposit',$data);
	}
	
	public function transferProcess(Request $request){

		$validator = Validator::make(
			[
				'amount'   => $request->input('amount'),
			],
			[
				'amount'   => 'required|decimal|min:1.00',
			]
		);
		
		if ($validator->fails())
		{
			echo json_encode($validator->errors());
			exit;
		}

		if  (str_contains($request->input('amount'), '.')) {
            if (preg_match('/^[\d]{1,}\.[\d]{1,2}$/', $request->input('amount')) < 1) {
                echo json_encode(array('amount' => Lang::get('public.InvalidNumberFormatMustBe') . '#.##'));
                exit;
            }
        }
	
		if( $request->from != "MAIN" && Product::where( 'status' , '=' , 2 )->where( 'code' , '=' , $request->from )->count() == 1 )
		{
			echo json_encode( array( 'code' => $request->from.' '.Lang::get('public.WalletMaintenanceTryLater') ) );
			exit;
		}	
		if( $request->to != "MAIN" )
		{
		    if ( Product::where( 'status' , '=' , 2 )->where( 'code' , '=' , $request->to )->count() == 1 ) {
                echo json_encode( array( 'code' => $request->to.' '.Lang::get('public.WalletMaintenanceTryLater') ) );
                exit;
            } else {
		        $accObj = Account::find(Session::get('userid'));

		        if ($accObj->suspendedproducts != '') {
		            try {
                        $arr = unserialize($accObj->suspendedproducts);
                        $prdObj = Product::where( 'code' , '=' , $request->to )->first(array('id'));

                        if (in_array($prdObj->id, $arr)) {
                            // Product is suspended for this user.
                            echo json_encode( array( 'code' => $request->to.' '.Lang::get('public.WalletMaintenanceTryLater') ) );
                            exit;
                        }
                    } catch (\Exception $ex) {
		                // Do nothing.
                    }
                }
            }
		}
		
		if( (int)$request->input('amount') < 1 ){
			echo json_encode( array( 'code' => Lang::get('TransferFailed')) );
			exit;
		}
		
		
		if( $request->from == $request->to )
		{
			echo json_encode( array( 'code' => Lang::get('validation.wallet_error1') ) );
			exit;
		}
		
		if( $request->get('from') == 'MAIN' )
		{
			$from_balance = self::mainwallet(false);
		}
		else
		{
			$className  	 = "App\libraries\providers\\".$request->get('from');
			$obj 			 = new $className;
			$from_balance    = $obj->getBalance( Session::get('username'), Session::get('currency'));
			$from_balance 	 = Session::get('currency') == 'VND' || Session::get('currency') == 'IDR' ? $from_balance * 1000: $from_balance;
		}
				
	 	if( $from_balance < $request->amount )
		{
			echo json_encode( array( 'code' => Lang::get('validation.wallet_error2') ) );
			exit;
		} 
		
		if( Session::get('currency') == 'IDR' && $request->get('amount') < 1000 )
		{
			echo json_encode( array( 'code' => Lang::get('validation.TransferFailed') ) );
			exit;
		}
		

		$success = false;
		
		$from_created_result = true;

		if( $request->from != 'MAIN' )
		{

			if( !Accountproduct::is_exist( Session::get('username') , $request->from ) )
			{
				$className  = "App\libraries\providers\\".$request->get('from');
				$obj 		= new $className;
				$from_created_result = $obj->createUser( array(
														'username' 	=> Session::get('username') , 
														'currency' 	=> Session::get('currency') , 
														'password' 	=> Session::get('enpassword') ,
														'email'    	=> Session::get('email') ,
														'firstname' => Session::get('fullname') ,
														'lastname'  => Session::get('fullname') ,
														'userid'    => Session::get('userid') 
														)
														);
			}	
		}
		
		$to_created_result = true;
		
		if( $request->to != 'MAIN' )
		{

			if( !Accountproduct::is_exist( Session::get('username') , $request->to ) )
			{
				$className  = "App\libraries\providers\\".$request->get('to');
				$obj 		= new $className;
				$to_created_result = $obj->createUser( array(
														'username' 	=> Session::get('username') , 
														'currency' 	=> Session::get('currency') , 
														'password' 	=> Session::get('enpassword') ,
														'email'    	=> Session::get('email') ,
														'firstname' => Session::get('fullname') ,
														'lastname'  => Session::get('fullname') ,
														'userid'    => Session::get('userid')  
														)
													 );
			}
		}
		
		if( $from_created_result && $to_created_result )
		{
			
			$bltObj = new Balancetransfer;
			$bltObj->accid 			= Session::get('userid');
			$bltObj->acccode 		= Session::get('acccode');
			$bltObj->from 			= $request->from == 'MAIN' ? 0 : Product::where( 'code' , '=' , $request->from)->pluck('id');
			$bltObj->to 			= $request->to == 'MAIN'   ? 0 : Product::where( 'code' , '=' , $request->to)->pluck('id');
			$bltObj->crccode		= Session::get('currency');
			$bltObj->crcrate		= Currency::getCurrencyRate($bltObj->crccode);
			$bltObj->amount 		= $request->amount;
			$bltObj->amountlocal 	= Currency::getLocalAmount($bltObj->amount, $bltObj->crccode);
			$bltObj->status			= CBO_STANDARDSTATUS_SUSPENDED;
			
			if( $bltObj->save() ) 
			{
				if($request->from == 'MAIN' ) 
				{
					$cashLedgerObj = new Cashledger;
					$cashLedgerObj->accid 			= $bltObj->accid;
					$cashLedgerObj->acccode 		= $bltObj->acccode;
					$cashLedgerObj->accname			= Session::get('fullname');
					$cashLedgerObj->chtcode 		= CBO_CHARTCODE_BALANCETRANSFER;
					$cashLedgerObj->cashbalance 	= self::getBalance(Session::get('userid'));
					$cashLedgerObj->amount 			= $bltObj->amount * -1;
					$cashLedgerObj->amountlocal 	= $bltObj->amountlocal * -1;
					$cashLedgerObj->refid 			= $bltObj->id;
					$cashLedgerObj->status 			= $bltObj->status;
					$cashLedgerObj->crccode 		= Session::get('currency');
					$cashLedgerObj->refobj 			= 'Balancetransfer';
					
					if( $cashLedgerObj->save() )
					{
						$productwalletObj = new Productwallet;
						$productwalletObj->prdid 		= $bltObj->to;
						$productwalletObj->accid 		= $bltObj->accid;
						$productwalletObj->acccode 		= $bltObj->acccode;
						$productwalletObj->chtcode 		= CBO_CHARTCODE_BALANCETRANSFER;
						$productwalletObj->crccode 		= $bltObj->crccode;
						$productwalletObj->crcrate 		= $bltObj->crcrate;
						$productwalletObj->amount		= abs($bltObj->amount);
						$productwalletObj->amountlocal	= abs($bltObj->amountlocal);
						$productwalletObj->refobj		= 'Balancetransfer';
						$productwalletObj->refid		= $bltObj->id;
						
						if( $productwalletObj->save() )
						{
							$className  = "App\libraries\providers\\".$request->get('to');
							$providerObj = new $className;
							
							if( $providerObj->deposit( Session::get('username') , Session::get('currency') , $productwalletObj->amount , $productwalletObj->id ) )
							{
								$bltObj->status 		  = CBO_LEDGERSTATUS_CONFIRMED;
								$cashLedgerObj->status 	  = CBO_LEDGERSTATUS_CONFIRMED;
								$productwalletObj->status = CBO_LEDGERSTATUS_CONFIRMED;
								$success = true;	
							}
							else
							{
								//$bltObj->status			  = CBO_STANDARDSTATUS_SUSPENDED;
								//$cashLedgerObj->status 	  = CBO_LEDGERSTATUS_CANCELLED;
								//$productwalletObj->status = CBO_LEDGERSTATUS_CANCELLED;
								$success = false;
							}
								
						/* 	if( $tolog = Apilog::whereAccid(Session::get('userid'))->whereMethod('transferCredit')->orderBy('id','desc')->pluck('return') )
							{
								$bltObj->tolog = $tolog;
							} */
								
								$bltObj->save();		  
								$cashLedgerObj->save();		  
								$productwalletObj->save();	
							
						}
						
					}
				}
				else
				{
					
					$productwalletObj = new Productwallet;
					$productwalletObj->prdid 		= $bltObj->from;
					$productwalletObj->accid 		= $bltObj->accid;
					$productwalletObj->acccode 		= $bltObj->acccode;
					$productwalletObj->chtcode 		= CBO_CHARTCODE_BALANCETRANSFER;
					$productwalletObj->crccode 		= $bltObj->crccode;
					$productwalletObj->crcrate 		= $bltObj->crcrate;
					$productwalletObj->amount		= abs($bltObj->amount) * -1;;
					$productwalletObj->amountlocal	= abs($bltObj->amountlocal) * -1;;
					$productwalletObj->refobj		= 'Balancetransfer';
					$productwalletObj->refid		= $bltObj->id;
					
					if( $productwalletObj->save() )
					{
						$className  = "App\libraries\providers\\".$request->get('from');
						$providerObj = new $className;
						
						if( $providerObj->withdraw( Session::get('username') , Session::get('currency') , $productwalletObj->amount * -1, $productwalletObj->id ) )
						{
							if( $request->to == 'MAIN' ) 
							{
									$cashLedgerObj = new Cashledger;
									$cashLedgerObj->accid 			= $bltObj->accid;
									$cashLedgerObj->acccode 		= $bltObj->acccode;
									$cashLedgerObj->accname			= Session::get('fullname');
									$cashLedgerObj->chtcode 		= CBO_CHARTCODE_BALANCETRANSFER;
									$cashLedgerObj->cashbalance 	= self::getBalance(Session::get('userid'));
									$cashLedgerObj->amount 			= $bltObj->amount;
									$cashLedgerObj->amountlocal 	= $bltObj->amountlocal;
									$cashLedgerObj->refid 			= $bltObj->id;
									$cashLedgerObj->status 			= CBO_LEDGERSTATUS_CONFIRMED;
									$cashLedgerObj->refobj 			= 'Balancetransfer';
									$cashLedgerObj->crccode 		= Session::get('currency');
									if( $cashLedgerObj->save() )
									{
										$productwalletObj->status = CBO_LEDGERSTATUS_CONFIRMED;
										/* if( $fromlog = Apilog::whereAccid(Session::get('userid'))->whereMethod('transferCredit')->orderBy('id','desc')->pluck('return') )
										{
											$bltObj->fromlog = $fromlog;
										} */
										$bltObj->status     = CBO_LEDGERSTATUS_CONFIRMED;
										$productwalletObj->save();	
										$bltObj->save();		  	  
										$success = true;
									}
							} 
							else 
							{
								
								$productwalletObj2 = new Productwallet;
								$productwalletObj2->prdid 		= $bltObj->to;
								$productwalletObj2->accid 		= $bltObj->accid;
								$productwalletObj2->acccode 	= $bltObj->acccode;
								$productwalletObj2->chtcode 	= CBO_CHARTCODE_BALANCETRANSFER;
								$productwalletObj2->crccode 	= $bltObj->crccode;
								$productwalletObj2->crcrate 	= $bltObj->crcrate;
								$productwalletObj2->amount		= abs($bltObj->amount);
								$productwalletObj2->amountlocal	= abs($bltObj->amountlocal);
								$productwalletObj2->refobj		= 'Balancetransfer';
								$productwalletObj2->refid		= $bltObj->id;
								
								if( $productwalletObj2->save() )
								{
									$className  = "App\libraries\providers\\".$request->get('to');
									$providerObj = new $className;
									if( $providerObj->deposit( Session::get('username') , Session::get('currency') , $productwalletObj2->amount , $productwalletObj2->id ) )
									{
										$productwalletObj->status  = CBO_LEDGERSTATUS_CONFIRMED;
										$productwalletObj2->status = CBO_LEDGERSTATUS_CONFIRMED;
										$bltObj->status     	   = CBO_LEDGERSTATUS_CONFIRMED;
										$productwalletObj->save();	
										$productwalletObj2->save();	
										/* if( $tolog = Apilog::whereAccid(Session::get('userid'))->whereMethod('transferCredit')->orderBy('id','desc')->pluck('return') )
										{
											$bltObj->tolog = $tolog;
										} */
										$bltObj->save();		
										$success = true;
									}
									else
									{
										//$className  = "App\libraries\providers\\".$request->get('from');
										//$providerObj = new $className;
										//$providerObj->deposit( Session::get('username') , Session::get('currency') , $productwalletObj->amount * -1, $productwalletObj2->id );
									}
								}
							}
						
						}
						else{
							$productwalletObj->status = CBO_LEDGERSTATUS_CANCELLED;
							$bltObj->status     	  = CBO_STANDARDSTATUS_SUSPENDED;
							/* if( $fromlog = Apilog::whereAccid(Session::get('userid'))->whereMethod('transferCredit')->orderBy('id','desc')->pluck('return') )
							{
								$bltObj->fromlog = $fromlog;
							} */
							$bltObj->save();		  	  
							$productwalletObj->save();	
							$success = false;
						}
						
					}

				}
			}
			
		}
		
		if( $success ){
			echo json_encode( array( 'code' => Lang::get('COMMON.SUCESSFUL') , 'main_wallet' => self::mainwallet(false) ) );
			exit;
		}
		
		echo json_encode( array( 'code' => Lang::get('COMMON.FAILED') , 'main_wallet' => self::mainwallet(false) ) );
		
	}
	
	 
	public static function getBalance($accid = null, $date = '', $chartcode = 0, $localamount = false, $bbf = false) {
		
		if($date == '')	$date = date('Y-m-d H:i:s');

		$condition = '((status in ('.CBO_LEDGERSTATUS_PENDING.', '.CBO_LEDGERSTATUS_MANPENDING.','.CBO_LEDGERSTATUS_CONFIRMED.', '.CBO_LEDGERSTATUS_MANCONFIRMED.', '.CBO_LEDGERSTATUS_PROCESSED.') AND chtcode IN ('.CBO_CHARTCODE_WITHDRAWAL.', '.CBO_CHARTCODE_BALANCETRANSFER.')) OR (status in ('.CBO_LEDGERSTATUS_CONFIRMED.', '.CBO_LEDGERSTATUS_MANCONFIRMED.') AND chtcode NOT IN ('.CBO_CHARTCODE_WITHDRAWAL.', '.CBO_CHARTCODE_BALANCETRANSFER.')))';
		if($bbf)
		$condition .= ' AND created < "'. $date . '"';
		else
		$condition .= ' AND created <= "'. $date . '"';
		
		if($localamount)
			$field = 'amountlocal';
		else
			$field = 'amount';

		if($accid != null)
			$condition .= ' AND accid = '. $accid;
		
		if($chartcode != 0)
			$condition .= ' AND chtcode = '.$chartcode;


		if($balance = Cashledger::whereRaw($condition)->sum($field)){
			$balance = $balance;
		}else{
			$balance = 0;
		}
		
		return App::float($balance);
		
	}
	

}
