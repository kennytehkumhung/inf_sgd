<?php namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\libraries\App;
use Session;
use Cache;
use Cookie;
use Config;
use Crypt;
use Auth;
use Log;
use Lang;
use App\libraries\providers\ASC;
use App\libraries\providers\AGG;
use App\libraries\providers\IBCX;
use App\libraries\providers\ALB;
use App\libraries\providers\MXB;
use App\libraries\providers\W88;
use App\libraries\providers\PLT;
use App\libraries\providers\PLTB;
use App\libraries\providers\FBL;
use App\libraries\providers\PSB;
use App\libraries\providers\CT8;
use App\libraries\providers\CTB;
use App\libraries\providers\A1A;
use App\libraries\providers\CLO;
use App\libraries\providers\TBS;
use App\libraries\providers\SBOF;
use App\libraries\providers\SPG;
use App\libraries\providers\PLS;
use App\libraries\providers\WFT;
use App\libraries\providers\M8B;
use App\libraries\providers\GDX;
use App\libraries\providers\HOG;
use App\libraries\providers\OSG;
use App\libraries\providers\VGS;
use App\libraries\providers\EVO;
use App\libraries\providers\JOK;
use App\libraries\providers\OPU;
use App\libraries\providers\ILT;
use App\libraries\providers\SKY;
use App\libraries\providers\UC8;
use App\libraries\providers\ISN;
use App\libraries\providers\SCR;
use App\libraries\providers\BBN;
use App\libraries\providers\MNT;
use App\libraries\providers\MIG;
use App\libraries\providers\S128;
use App\libraries\providers\WMC;
use App\Models\Accountproduct;
use App\Models\Account;
use App\Models\Onlinetracker;
use App\Models\Currency;
use App\Models\Betw88category;
use App\Models\Betpltcategory;
use App\Models\Betpltbcategory;
use App\Models\Betmigcategory;
use App\Models\Betspgcategory;
use App\Models\Product;
use App\Models\Affiliate;
use App\Models\h5plt_category;

class ProductController extends Controller{
	
	protected $not_available_products = array();
	
	public function __construct()
	{
		if( Session::has('userid')  )
		{

		    $accObj = Account::find(Session::get('userid'));
			if( $not_available_products = $accObj->products )
			{
				$not_available_products = unserialize($not_available_products);
				foreach($not_available_products as $not_available_product){
			
					$this->not_available_products[] = Product::whereId($not_available_product)->pluck('code');
				}
			}

			// Load suspended products.
            if ($accObj->suspendedproducts != '') {
			    $suspendedProducts = unserialize($accObj->suspendedproducts);

			    foreach ($suspendedProducts as $sprd) {
			        if (!in_array($sprd, $this->not_available_products)) {
			            $this->not_available_products[] = Product::whereId($sprd)->pluck('code');
                    }
                }
            }
		
		}
	
	}
	public function playtechmobile(Request $request){
		
		$pltobj=new h5plt_category; 
	    $data['lists'] = $pltobj->gameList($request->type);
		if ($request->category){
		$data['lists'] = $pltobj->gameList($request->category);
		}
		$data['type']  = $request->type;
		
		return view( Config::get('setting.front_path').'/product/plth5_slot', $data );

	}
	
	
	public function playtechh5slot(Request $request){
		$data['code'] =$request->gamecode; 
		$data['username'] =Config::get(Session::get('currency').'.plt.prefix').strtoupper(Session::get('username'));
		$data['password'] =Crypt::decrypt(Session::get('enpassword')); 
		return view( Config::get('setting.front_path').'/product/plth5', $data );
	}
	private function check_maintainence($product){
		
	}
	
	public function slot(Request $request){

		$data = array();
		$data['name']  = 'slot';
		$data['type']  = $request->type;
		
		$mxb = new MXB;
		if(  Auth::user()->check() )
		{
			$data['mxb_slot'] = $mxb->getSlotLobby(Session::get('username'),Session::get('locale'),Session::get('currency'),Session::get('enpassword'));
		}

		return view( Config::get('setting.front_path').'/slot' , $data );
		
	}
	
	public function ASC(Request $request) {
		if (Product::where('status', '=', 2)->where('code', '=', 'ASC')->count() == 1) {
			return view( Config::get('setting.front_path').'/maintenance' );
		} elseif ( in_array('ASC',$this->not_available_products) ) {
		    return $this->productTimeout($request);
        }
		
		$ascObj = new ASC;
		
		if (Auth::user()->check()) {
			if (!Accountproduct::is_exist(Session::get('username'), 'ASC')) {
				$result = $ascObj->createUser(array(
				    'username' => Session::get('username'),
				    'currency' => Session::get('currency'),
				    'password' => Session::get('enpassword'),
				    'email' => Session::get('email'),
				    'firstname' => Session::get('fullname'),
				    'lastname' => Session::get('fullname'),
				    'userid' => Session::get('userid'),
				));
			}

			$ascObj->updateGroupComm( array(
				'username' 	=> Session::get('username') , 
				'currency' 	=> Session::get('currency')
			));

			$data['login_url'] = $ascObj->getLoginUrl(Session::get('username'), Session::get('currency'), Lang::getLocale(), 1, 1);
		} else {
			$data['login_url'] = $ascObj->getLoginUrl(null, Session::get('currency'), Lang::getLocale(), 1, 1);
		}
		
		return view(Config::get('setting.front_path') . '/product/asc', $data);
	}
	
	public function AGG(Request $request){

		if( Product::where( 'status' , '=' , 2 )->where( 'code' , '=' , 'AGG' )->count() == 1) {
            return view(Config::get('setting.front_path') . '/maintenance');
        } elseif ( in_array('AGG',$this->not_available_products) ) {
            return $this->productTimeout($request);
        }

		$aggObj = new AGG;
		
		if( !Accountproduct::is_exist( Session::get('username') , 'AGG' ) )
		{
			$result = $aggObj->createUser( array(
														'username' 	=> Session::get('username') , 
														'currency' 	=> Session::get('currency') , 
														'password' 	=> Session::get('enpassword') ,
														'email'    	=> Session::get('email') ,
														'firstname' => Session::get('fullname') ,
														'lastname'  => Session::get('fullname') ,
														'userid'    => Session::get('userid') 
														)  );
		}
        
        $gametype = 0;
        if ($request->has('gametype')) {
            $gametype = $request->gametype;
        }
		
		$data['login_url'] = $aggObj->getLoginUrl( 
													Session::get('username') , 
													Session::get('currency') , 
													Config::get(Session::get('currency').'.agg.lang') , 
													Config::get(Session::get('currency').'.agg.website') ,
                                                    $gametype
												 );
                
		return view( Config::get('setting.front_path').'/product/agg' , $data );
		
	}	
    
    public function AGGSLOT(Request $request) {
        
		if (Product::where('status', '=', 2)->where('code', '=', 'AGG')->count() == 1 || in_array('AGG',$this->not_available_products)) {
			return view( Config::get('setting.front_path').'/maintenance' );
		}
		
		$aggObj = new AGG;
        
		if (Auth::user()->check()) {
			if (!Accountproduct::is_exist(Session::get('username'), 'AGG')) {
                $aggObj->createUser(array(
                    'username' 	=> Session::get('username') , 
                    'currency' 	=> Session::get('currency') , 
                    'password' 	=> Session::get('enpassword') ,
                    'email'    	=> Session::get('email') ,
                    'firstname' => Session::get('fullname') ,
                    'lastname'  => Session::get('fullname') ,
                    'userid'    => Session::get('userid') 
                ));
			}
		}
        
        $data['lists'] = $aggObj->gameList(Session::get('currency'), $request->category);
		
		return view(Config::get('setting.front_path') . '/slot_agg', $data);
    }
	
	public function GDX(Request $request){

		if( Product::where( 'status' , '=' , 2 )->where( 'code' , '=' , 'GDX' )->count() == 1 ) {
            return view(Config::get('setting.front_path') . '/maintenance');
        } elseif ( in_array('GDX',$this->not_available_products) ) {
            return $this->productTimeout($request);
        }

		$gdxObj = new GDX;
		
		if( !Accountproduct::is_exist( Session::get('username') , 'GDX' ) )
		{
			$result = $gdxObj->createUser( array(
														'username' 	=> Session::get('username') , 
														'currency' 	=> Session::get('currency') , 
														'password' 	=> Session::get('enpassword') ,
														'email'    	=> Session::get('email') ,
														'firstname' => Session::get('fullname') ,
														'lastname'  => Session::get('fullname') ,
														'userid'    => Session::get('userid') 
														)  );
		}
		$token = Onlinetracker::where( 'username' , '=' , Session::get('username'))->pluck('token');
		$data['login_url'] = $gdxObj->getLoginUrl( 
													$token , 
													Session::get('currency') , 
													Session::get('username') , 
													Session::get('locale')
												 );
                
		return view( Config::get('setting.front_path').'/product/gdx' , $data );
		
	}
	
	public function WFT(Request $request){

		if( Product::where( 'status' , '=' , 2 )->where( 'code' , '=' , 'WFT' )->count() == 1 ) {
            return view(Config::get('setting.front_path') . '/maintenance');
        } elseif ( in_array('WFT',$this->not_available_products) ) {
            return $this->productTimeout($request);
        }

		$wftObj = new WFT;
		
		if( Auth::user()->check() && !Accountproduct::is_exist( Session::get('username') , 'WFT' ) )
		{
			$result = $wftObj->createUser( array(
														'username' 	=> Session::get('username') , 
														'currency' 	=> Session::get('currency') , 
														'password' 	=> Session::get('enpassword') ,
														'email'    	=> Session::get('email') ,
														'firstname' => Session::get('fullname') ,
														'lastname'  => Session::get('fullname') ,
														'userid'    => Session::get('userid') 
														)  );
		}
		
		if(  Auth::user()->check() ){
			
			 $wftObj->updateUser( array(
											'username' 	=> Session::get('username') , 
											'currency' 	=> Session::get('currency') , 
											'password' 	=> Session::get('enpassword') ,
											'email'    	=> Session::get('email') ,
											'firstname' => Session::get('fullname') ,
											'lastname'  => Session::get('fullname') ,
											'userid'    => Session::get('userid') 
											)  );

			 if ($this->isMobileAgent($request)) {
                 $data['login_url'] = $wftObj->getLoginUrl(
                     Session::get('username') ,
                     Session::get('currency') ,
                     Lang::getLocale() ,
                     Config::get(Session::get('currency').'.wft.hostmobile')
                 );
             } else {
                 $data['login_url'] = $wftObj->getLoginUrl(
                     Session::get('username') ,
                     Session::get('currency') ,
                     Lang::getLocale() ,
                     Config::get(Session::get('currency').'.wft.host')
                 );
             }
		}else{
			$data['login_url'] = $wftObj->getLoginUrl( 
													'PublicPlayer' , 
													Session::get('currency') , 
													Lang::getLocale() , 
													'odds.sn.1sgames.com'
												 );
		}

		return view( Config::get('setting.front_path').'/product/wft' , $data );
		
	}
	
	public function M8B(Request $request){
		
		$data = array();

		if( Product::where( 'status' , '=' , 2 )->where( 'code' , '=' , 'M8B' )->count() == 1 ) {
            return view(Config::get('setting.front_path') . '/maintenance');
        } elseif ( in_array('M8B',$this->not_available_products) ) {
            return $this->productTimeout($request);
        }

		$m8bObj = new M8B;
		
		if( Auth::user()->check() && !Accountproduct::is_exist( Session::get('username') , 'M8B' ) )
		{
			$result = $m8bObj->createUser( array(
														'username' 	=> Session::get('username') , 
														'currency' 	=> Session::get('currency') , 
														'password' 	=> Session::get('enpassword') ,
														'email'    	=> Session::get('email') ,
														'firstname' => Session::get('fullname') ,
														'lastname'  => Session::get('fullname') ,
														'userid'    => Session::get('userid') 
														)  );
		}
		
		if(  Auth::user()->check() ){
			
			 $m8bObj->updateUser( array(
											'username' 	=> Session::get('username') , 
											'currency' 	=> Session::get('currency') , 
											'password' 	=> Session::get('enpassword') ,
											'email'    	=> Session::get('email') ,
											'firstname' => Session::get('fullname') ,
											'lastname'  => Session::get('fullname') ,
											'userid'    => Session::get('userid') 
											)  );
											
			$useragent=$_SERVER['HTTP_USER_AGENT'];
			if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4)))
			{
				$data['login_url'] = $m8bObj->getLoginUrl( 
													Session::get('username') , 
													Session::get('currency') , 
													Lang::getLocale() , 
													Config::get(Session::get('currency').'.m8b.mobile') 
												 );
			}
			else
			{
				$data['login_url'] = $m8bObj->getLoginUrl( 
													Session::get('username') , 
													Session::get('currency') , 
													Lang::getLocale() , 
													Config::get(Session::get('currency').'.m8b.host') 
												 );
			}								
			
		}

		return view( Config::get('setting.front_path').'/product/m8b' , $data );
		
	}
	
        public function TBS(Request $request) {

        if (Product::where('status', '=', 2)->where('code', '=', 'TBS')->count() == 1 ) {
            return view(Config::get('setting.front_path') . '/sport_maintenance');
        } elseif ( in_array('TBS',$this->not_available_products) ) {
            return $this->productTimeout($request);
        }

        if(  Auth::user()->check() ){

            $tbsObj = new TBS;


            $data['style'] = $request->has('style') ? $request->style : 'SP1';

            if (Auth::user()->check()) {
                if (!Accountproduct::is_exist(Session::get('username'), 'TBS')) {
                    $result = $tbsObj->createUser(array(
                        'username' => Session::get('username'),
                        'currency' => Session::get('currency'),
                        'password' => Session::get('enpassword'),
                        'email' => Session::get('email'),
                        'firstname' => Session::get('fullname'),
                        'lastname' => Session::get('fullname'),
                        'userid' => Session::get('userid')
                    ));
                }

                $tbsObj->updateUser(array(
                    'username' => Session::get('username'),
                    'currency' => Session::get('currency'),
                    'password' => Session::get('enpassword'),
                    'email' => Session::get('email'),
                    'firstname' => Session::get('fullname'),
                    'lastname' => Session::get('fullname'),
                    'userid' => Session::get('userid')
                ));
                
                $useragent = $_SERVER['HTTP_USER_AGENT'];
                if (preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i', $useragent) || preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', substr($useragent, 0, 4)))
                {
                    $data['login_url'] = $tbsObj->getLoginUrl(Session::get('username'), Session::get('currency'), $request->ip(), Lang::getLocale(), $data['style'],true);
                }else{
                    $data['login_url'] = $tbsObj->getLoginUrl(Session::get('username'), Session::get('currency'), $request->ip(), Lang::getLocale(), $data['style']);
                }
            } else {
                $data['login_url'] = $tbsObj->getLoginUrl(null, Session::get('currency'), $request->ip(), Lang::getLocale(), $data['style']);
            }
        }else if(!Auth::user()->check() ){
            $data['login_url'] = Config::get(Session::get('currency').'.tbs.public_url');
        }
        //wft replace tbs
        return view(Config::get('setting.front_path') . '/product/wft', $data);
    }
	
	public function FBL(Request $request) {

		if (Product::where('status', '=', 2)->where('code', '=', 'FBL')->count() == 1 ) {
            return view(Config::get('setting.front_path') . '/maintenance');
        } elseif ( in_array('FBL',$this->not_available_products) ) {
            return $this->productTimeout($request);
        }

        if ($request->get('page') == 'intro')
            return view(Config::get('setting.front_path') . '/product/fbl_intro');
        
		$fblObj = new FBL;

		if (Auth::user()->check()) {
			if (!Accountproduct::is_exist(Session::get('username'), 'FBL')) {
				$result = $fblObj->createUser(array(
				    'username' => Session::get('username'),
				    'currency' => Session::get('currency'),
				    'password' => Session::get('enpassword'),
				    'email' => Session::get('email'),
				    'firstname' => Session::get('fullname'),
				    'lastname' => Session::get('fullname'),
				    'userid' => Session::get('userid')
				));
			}

			$fblObj->updateUser(array(
			    'username' => Session::get('username'),
			    'currency' => Session::get('currency'),
			    'password' => Session::get('enpassword'),
			    'email' => Session::get('email'),
			    'firstname' => Session::get('fullname'),
			    'lastname' => Session::get('fullname'),
			    'userid' => Session::get('userid')
			));

			$data['login_url'] = $fblObj->getLoginUrl(Session::get('username'), Session::get('currency'), Lang::getLocale());
		} else {
			$data['login_url'] = $fblObj->getLoginUrl(null, Session::get('currency'), Lang::getLocale());
		}

		return view(Config::get('setting.front_path') . '/product/fbl', $data);
	}

	public function CLO(Request $request){

		if( Product::where( 'status' , '=' , 2 )->where( 'code' , '=' , 'CLO' )->count() == 1 ) {
            return view(Config::get('setting.front_path') . '/maintenance');
        } elseif ( in_array('CLO',$this->not_available_products) ) {
            return $this->productTimeout($request);
        }

		$cloObj = new CLO;
		
		if( !Accountproduct::is_exist( Session::get('username') , 'CLO' ) )
		{
			$result = $cloObj->createUser( array(
                                                            'username' 	=> Session::get('username') , 
                                                            'currency' 	=> Session::get('currency') , 
                                                            'password' 	=> Session::get('enpassword') ,
                                                            'email'    	=> Session::get('email') ,
                                                            'firstname' => Session::get('fullname') ,
                                                            'lastname'  => Session::get('fullname') ,
                                                            'userid'    => Session::get('userid') 
                                                            )  );
		}
		
		$data['login_url'] = $cloObj->getLoginUrl(Session::get('username') , Session::get('currency'));

		return view( Config::get('setting.front_path').'/product/clo' , $data );
		
	}
	
	public function IBC(Request $request){

        if( Product::where( 'status' , '=' , 2 )->where( 'code' , '=' , 'IBCX' )->count() == 1 )
        {
            if( Config::get('setting.opcode') == 'RYW' )
            {
                return view(Config::get('setting.front_path') . '/sport_maintenance');
            }
            else
            {
                return view(Config::get('setting.front_path') . '/sport_maintenance');
            }

        }  elseif ( in_array('IBCX',$this->not_available_products) ) {
            return $this->productTimeout($request);
        }
		
		$url = explode('.',$_SERVER['HTTP_HOST']);
		$i = count($url);
		$website = '';
		if( isset($url[($i-2)]) )
		$website = $url[($i-2)].'.'.$url[($i-1)];
		
		$data['website'] = $website;
		
		$data['lang']  = Session::get('locale') == 'cn' ? 'cs' : Session::get('locale');
		$ibcObj = new IBCX;
		if( !Auth::user()->check() ){

			return response()->view( Config::get('setting.front_path').'/product/ibc',$data);
		
			
		}else{
			$currency = Session::get('currency');
			$ibcObj->setMemberBetSetting( Session::get('username') , Config::get($currency.'.ibc.bet_setting') , Session::get('currency'));
	
		}
		
	
		
		
		
		if( !Accountproduct::is_exist( Session::get('username') , 'IBCX' ) && Auth::user()->check() )
		{
			if( $ibcObj->createUser( array(
														'username' 	=> Session::get('username') , 
														'currency' 	=> Session::get('currency') , 
														'password' 	=> Session::get('enpassword') ,
														'email'    	=> Session::get('email') ,
														'firstname' => Session::get('fullname') ,
														'lastname'  => Session::get('fullname') ,
														'userid'    => Session::get('userid') 
														)  ) )
			{
				
			}
		}
		

		
		
		
		$data['token'] = Config::get(Session::get('currency').'.ibc.operator_id').'_'.Onlinetracker::where( 'username' , '=' , Session::get('username'))->where( 'type' , '=' , 1)->pluck('token');

		return response()->view( Config::get('setting.front_path').'/product/ibc',$data)
						 ->withCookie( cookie( 'g' , Config::get(Session::get('currency').'.ibc.operator_id').'_'.Onlinetracker::where( 'username' , '=' , Session::get('username'))->where( 'type' , '=' , 1)->pluck('token') , 3600 , null , '.'.$website) );
		
	}		
	
	public function IBCCASINO(Request $request){
		
		
		$url = explode('.',$_SERVER['HTTP_HOST']);
		$i = count($url);
		$website = '';
		if( isset($url[($i-2)]) )
		$website = $url[($i-2)].'.'.$url[($i-1)];
		
		$data['website'] = $website;
		
		$data['lang']  = Session::get('locale') == 'cn' ? 'cs' : Session::get('locale');
		
		if( !Auth::user()->check() ){

			return response()->view( Config::get('setting.front_path').'/product/ibc',$data);
		
			
		}
		
		
		if( Product::where( 'status' , '=' , 2 )->where( 'code' , '=' , 'IBCX' )->count() == 1 ) {
			return view( Config::get('setting.front_path').'/maintenance' );
        }  elseif ( in_array('IBCX',$this->not_available_products) ) {
            return $this->productTimeout($request);
        }
		
	
		
		$ibcObj = new IBCX;
		
		if( !Accountproduct::is_exist( Session::get('username') , 'IBCX' ) && Auth::user()->check() )
		{
			if( $ibcObj->createUser( array(
														'username' 	=> Session::get('username') , 
														'currency' 	=> Session::get('currency') , 
														'password' 	=> Session::get('enpassword') ,
														'email'    	=> Session::get('email') ,
														'firstname' => Session::get('fullname') ,
														'lastname'  => Session::get('fullname') ,
														'userid'    => Session::get('userid') 
														)  ) )
			{
				
			}
		}
		

		
		
		
		$data['token'] = Config::get('setting.website_name').'_'.Onlinetracker::where( 'username' , '=' , Session::get('username'))->where( 'type' , '=' , 1)->pluck('token');

		return response()->view( Config::get('setting.front_path').'/product/ibc_casino',$data)
						 ->withCookie( cookie( 'g' , Config::get('setting.website_name').'_'.Onlinetracker::where( 'username' , '=' , Session::get('username'))->where( 'type' , '=' , 1)->pluck('token') , 3600 , null , '.'.$website) );
		
	}		
	
	public function ALB(Request $request){
	
		if( Product::where( 'status' , '=' , 2 )->where( 'code' , '=' , 'ALB' )->count() == 1 ) {
            return view(Config::get('setting.front_path') . '/maintenance');
        } elseif ( in_array('ALB',$this->not_available_products) ) {
		    return $this->productTimeout($request);
        }

		$albObj = new ALB;
		
		if( !Accountproduct::is_exist( Session::get('username') , 'ALB' ) )
		{
			$albObj->createUser( array(
														'username' 	=> Session::get('username') , 
														'currency' 	=> Session::get('currency') , 
														'password' 	=> Session::get('enpassword') ,
														'email'    	=> Session::get('email') ,
														'firstname' => Session::get('fullname') ,
														'lastname'  => Session::get('fullname') ,
														'userid'    => Session::get('userid') 
														)  ) ;
		}
		
		$data['login_url'] = $albObj->getLoginUrl(Session::get('username'),Session::get('currency'), Lang::getLocale());
	
		if( $data['login_url'] )
			return view(Config::get('setting.front_path').'/product/alb',$data);
		else
			return view(Config::get('setting.front_path').'/maintenance');
	}		
	
	public function CT8(Request $request){
		
		if( Product::where( 'status' , '=' , 2 )->where( 'code' , '=' , 'CT8' )->count() == 1 ) {
            return view(Config::get('setting.front_path') . '/maintenance');
        } elseif ( in_array('CT8',$this->not_available_products) ) {
		    return $this->productTimeout($request);
        }

		$object = new CT8;
		
		if(Auth::user()->check())
		{
			 $data['login_url'] = (string) $object->createUser( array(
														'username' 	=> Session::get('username') , 
														'currency' 	=> Session::get('currency') , 
														'password' 	=> Session::get('enpassword') ,
														'email'    	=> Session::get('email') ,
														'firstname' => Session::get('fullname') ,
														'lastname'  => Session::get('fullname') ,
														'userid'    => Session::get('userid') 
														)  ) ;
		}

		if( $data['login_url'] )
			return view(Config::get('setting.front_path').'/product/ct8',$data);
		else
			return view(Config::get('setting.front_path').'/maintenance');
	}	
	
	public function SBOF(Request $request){
		
		if( Product::where( 'status' , '=' , 2 )->where( 'code' , '=' , 'SBOF' )->count() == 1 ) {
            return view(Config::get('setting.front_path') . '/maintenance');
        } elseif ( in_array('SBOF',$this->not_available_products) ) {
		    return $this->productTimeout($request);
        }

		$object = new SBOF;
		
		$data = array();
		
		if(Auth::user()->check()){
			
			 $data['login_url'] = (string) $object->createUser( array(
														'username' 	=> Session::get('username') , 
														'currency' 	=> Session::get('currency') , 
														'password' 	=> Session::get('enpassword') ,
														'email'    	=> Session::get('email') ,
														'firstname' => Session::get('fullname') ,
														'lastname'  => Session::get('fullname') ,
														'userid'    => Session::get('userid') 
														)  ) ;
		}else{
                        $data['login_url'] = (string) $object->createUser( array(
														'username' 	=> null, 
														'currency' 	=> Session::get('currency') , 
														'password' 	=> null,
														'email'    	=> null,
														'firstname'     => null,
														'lastname'      => null,
														'userid'        => null 
														)  ) ;
                }
		
		


		return view(Config::get('setting.front_path').'/product/sbof',$data);

	}
        
        public function SPG(Request $request)
        {
            if ($request->type == 'slot') {
                $data['name'] = 'spg';
                $data['token'] = Onlinetracker::where('userid', '=', Session::get('userid'))->pluck('token');
                $data['lists'] = Betspgcategory::where('category', '=', $request->category)->where('isActive', '=', 1)->get();
                if ($request->category == 'progressive')
                    $data['lists'] = Betspgcategory::where('isProgressive', '=', 1)->where('isActive', '=', 1)->get();

                $data['category'] = $request->category;

                return view(Config::get('setting.front_path') . '/product/spg', $data);
            }

        }
        
        public function SPGSLOT(Request $request)
        {
            if (Product::where('status', '=', 2)->where('code', '=', 'SPG')->count() == 1 ) {
                return view(Config::get('setting.front_path') . '/maintenance_slot');
            } elseif ( in_array('SPG', $this->not_available_products) ) {
                return $this->productTimeout($request);
            }

            if (!Accountproduct::is_exist(Session::get('username'), 'SPG') && Auth::user()->check()) {
                Accountproduct::addAccountProduct(Session::get('username'), 'SPG', '');
                return true;
            }

            $spgObj = new SPG;

            if (Auth::user()->check()) {
                //getLoginUrl($acctId, $token, $game, $currency)
                $data['login_url'] = $spgObj->getLoginUrl(Session::get('username'), Onlinetracker::where('userid', '=', Session::get('userid'))->where('type', '=', 1)->pluck('token'), $request->gameid, Session::get('currency'));
                $data['is_mobile'] = $this->isMobileSite();
                //Log::info($data['login_url']);
                return view(Config::get('setting.front_path') . '/product/spg_slot', $data);
            }
        }
        
    public function OSG(Request $request) {
        
        if( Product::where( 'status' , '=' , 2 )->where( 'code' , '=' , 'OSG' )->count() == 1 ){
            return view( Config::get('setting.front_path').'/maintenance_slot' );
		} elseif ( in_array('OSG',$this->not_available_products) ) {
            return $this->productTimeout($request);
        }
	
		$osgObj = new OSG;
		
		if( !Accountproduct::is_exist( Session::get('username') , 'OSG' ) && Auth::user()->check() )
		{
			$osgObj->createUser( array(
                                'username' 	=> Session::get('username') , 
                                'currency' 	=> Session::get('currency') , 
                                'password' 	=> Session::get('enpassword') ,
                                'email'    	=> Session::get('email') ,
                                'firstname' => Session::get('fullname') ,
                                'lastname'  => Session::get('fullname') ,
                                'userid'    => Session::get('userid') ,
                                'ip'        => $request->ip() ,
                                ) ) ;
		}

        if (Auth::user()->check()) {
            $osgObj->updatePassword(Session::get('username'), Session::get('enpassword'), Session::get('currency'));
        }

        if ($request->type == 'gamelobby') {
            $data['login_url'] = $osgObj->getLobbyUrl(array(
                    'username' => Session::get('username'),
                    'password' => Session::get('enpassword'),
                    'currency' => Session::get('currency'),
                    'ip' => $request->ip(),
                ), Lang::getLocale());
            
            return view( Config::get('setting.front_path').'/product/osg', $data );
        }

        $data['name']  = 'osg';
        $data['lists'] = $osgObj->gameList($request->category, Session::get('currency'), Lang::locale());
        
        $data['category'] = $request->category;

        return view( Config::get('setting.front_path').'/slot_osg', $data );
    }
        
    public function OSGSLOT(Request $request, $gameid) {
        
        if (Product::where('status', '=', 2)->where('code', '=', 'OSG')->count() == 1 ) {
            return view(Config::get('setting.front_path') . '/maintenance_slot');
        } elseif ( in_array('OSG',$this->not_available_products) ) {
            return $this->productTimeout($request);
        }

        if (!Accountproduct::is_exist(Session::get('username'), 'OSG') && Auth::user()->check()) {
            Accountproduct::addAccountProduct(Session::get('username'), 'OSG', '');
            return true;
        }
        
        if (is_null($gameid)) {
            $gameid = '';
        }

        $osgObj = new OSG;

        if (Auth::user()->check()) {
            //getLoginUrl($acctId, $token, $game, $currency)
            $data['login_url'] = $osgObj->getLoginUrl(array(
                'username' => Session::get('username'),
                'password' => Session::get('enpassword'),
                'currency' => Session::get('currency'),
                'gameid' => $gameid,
                'ip' => $request->ip(),
            ), Lang::locale());

            $osgObj->updatePassword(Session::get('username'), Session::get('enpassword'), Session::get('currency'));

            return view(Config::get('setting.front_path') . '/product/osg_slot', $data);
        }
    }

    public function W88(Request $request){
		
		if( Product::where( 'status' , '=' , 2 )->where( 'code' , '=' , 'W88' )->count() == 1 ){
			if($request->type == 'casino')
				return view( Config::get('setting.front_path').'/maintenance' );
			else
				return view( Config::get('setting.front_path').'/maintenance_slot' );
		} elseif ( in_array('W88',$this->not_available_products) ) {
		    return $this->productTimeout($request);
        }
		
		$w88Obj = new W88;
		
		if( !Accountproduct::is_exist( Session::get('username') , 'W88' ) && Auth::user()->check() )
		{
			$w88Obj->createUser( array(
														'username' 	=> Session::get('username') , 
														'currency' 	=> Session::get('currency') , 
														'password' 	=> Session::get('enpassword') ,
														'email'    	=> Session::get('email') ,
														'firstname' => Session::get('fullname') ,
														'lastname'  => Session::get('fullname') ,
														'userid'    => Session::get('userid') 
														)  ) ;
		}

		if( $request->type == 'casino' )
		{
			if (Auth::user()->check())
			{
				$data['login_url'] = $w88Obj->getLoginUrl('en-us',Onlinetracker::where( 'username' , '=' , Session::get('username'))->where( 'type' , '=' , 1)->pluck('token'), Session::get('currency'));

				return view(Config::get('setting.front_path').'/product/w88',$data);
			}
		}
		elseif( $request->type == 'slot' )
		{
			$data['name']  = 'w88';
			$data['token'] = Onlinetracker::where( 'username' , '=' , Session::get('username'))->where( 'type' , '=' , 1)->pluck('token');
			$data['lists'] = Betw88category::where( 'category' , '=' , $request->category )->where( 'isActive' , '=' , 1 )->get();
			$data['category'] = $request->category;
		
			return view( Config::get('setting.front_path').'/slot_w88', $data );
		}
		
	}
	
	
	public function MXB(Request $request){
		
		if( Product::where( 'status' , '=' , 2 )->where( 'code' , '=' , 'MXB' )->count() == 1 ){
			if($request->type == 'casino')
				return view( Config::get('setting.front_path').'/maintenance' );
			else
				return view( Config::get('setting.front_path').'/maintenance_slot' );
		} elseif ( in_array('MXB',$this->not_available_products) ) {
		    return $this->productTimeout($request);
        }
	
		$mxbObj = new MXB;
		
		if( !Accountproduct::is_exist( Session::get('username') , 'MXB' ) && Auth::user()->check() )
		{
			$mxbObj->createUser( array(
														'username' 	=> Session::get('username') , 
														'currency' 	=> Session::get('currency') , 
														'password' 	=> Session::get('enpassword') ,
														'email'    	=> Session::get('email') ,
														'firstname' => Session::get('fullname') ,
														'lastname'  => Session::get('fullname') ,
														'userid'    => Session::get('userid') 
														) ) ;
		} else {
		    /* $mxbObj->updatePassword(
		        Session::get('username'),
		        Crypt::decrypt(Session::get('enpassword')),
                Session::get('currency')
            ); */
        }
		
		if( $request->type == 'casino' )
		{
			if (Auth::user()->check())
			{
				$data['gamelist'] = $mxbObj->getCasinoGameList(Session::get('username'),$request->category, Session::get('currency'), Lang::getLocale());

				return view(Config::get('setting.front_path').'/product/mxb',$data);
			}
		}
		elseif( $request->type == 'slot' )
		{
			
			$data['name']  = 'mxb';
			if(Auth::user()->check())
				$data['lists'] = $mxbObj->getSlotGameList(Session::get('username'),$request->category, Session::get('currency'));
			else
				$data['lists'] = $mxbObj->getSlotGameList('badegg',$request->category, Session::get('currency'));
			
			$data['category'] = $request->category;

			return view( Config::get('setting.front_path').'/slot_mxb', $data );
		}
		
		
	}

	public function MXBCASINO(Request $request) {

        if( Product::where( 'status' , '=' , 2 )->where( 'code' , '=' , 'MXB' )->count() == 1 ){
            return view( Config::get('setting.front_path').'/maintenance' );
        } elseif ( in_array('MXB',$this->not_available_products) ) {
            return $this->productTimeout($request);
        }

        $data['login_url'] = '';

	    try {
            $url = Crypt::decrypt($request->input('c'));
            $url = str_replace('{4}', $request->input('l'), $url);

            $data['login_url'] = $url;
        } catch (\Exception $e) {
	        abort(404);
        }

        return view( Config::get('setting.front_path').'/product/mxb_casino', $data );
    }
	
	public function MXBSLOT(Request $request){
		
		if( Product::where( 'status' , '=' , 2 )->where( 'code' , '=' , 'MXB' )->count() == 1 ) {
            return view(Config::get('setting.front_path') . '/maintenance');
        } elseif ( in_array('MXB',$this->not_available_products) ) {
		    return $this->productTimeout($request);
        }
		
		$mxbObj = new MXB;
		
		$data['login_url'] = urldecode($mxbObj->GetExternalGameURL(Session::get('username'),$request->gameid, Session::get('currency')));
		
	
		return view( Config::get('setting.front_path').'/product/mxb_slot', $data );
	}
	
	public function MIG(Request $request){
		if( Product::where( 'status' , '=' , 2 )->where( 'code' , '=' , 'MIG' )->count() == 1 ) {
            return view(Config::get('setting.front_path') . '/maintenance_slot');
        } elseif ( in_array('MIG',$this->not_available_products) ) {
		    return $this->productTimeout($request);
        }
	
		$migObj = new MIG;
		
	 	if( !Accountproduct::is_exist( Session::get('username') , 'MIG' ) )
		{
			$migObj->createUser( array(
														'username' 	=> Session::get('username') , 
														'currency' 	=> Session::get('currency') , 
														'password' 	=> Session::get('enpassword') ,
														'email'    	=> Session::get('email') ,
														'firstname' => Session::get('fullname') ,
														'lastname'  => Session::get('fullname') ,
														'userid'    => Session::get('userid') 
														) ) ;
		}
		
		
		
		$data['gamelist'] = Betmigcategory::where( 'is_true' , '=' , '1')->get();
		$data['username'] = Account::where( 'nickname' , '=' , Session::get('username'))->pluck('migid');
		$data['password'] = Crypt::decrypt(Session::get('enpassword'));
		return view( Config::get('setting.front_path').'/product/mig' , $data);
	}
	
	public function MIGMOBILE(Request $request){
		if( Product::where( 'status' , '=' , 2 )->where( 'code' , '=' , 'MIG' )->count() == 1 ) {
            return view(Config::get('setting.front_path') . '/maintenance_slot');
        } elseif ( in_array('MIG',$this->not_available_products) ) {
		    return $this->productTimeout($request);
        }
	
		$migObj = new MIG;
		
	 	if( !Accountproduct::is_exist( Session::get('username') , 'MIG' ) )
		{
			$migObj->createUser( array(
														'username' 	=> Session::get('username') , 
														'currency' 	=> Session::get('currency') , 
														'password' 	=> Session::get('enpassword') ,
														'email'    	=> Session::get('email') ,
														'firstname' => Session::get('fullname') ,
														'lastname'  => Session::get('fullname') ,
														'userid'    => Session::get('userid') 
														) ) ;
		}
		
		
		
		$data['gamelist'] = Betmigcategory::where( 'is_true' , '=' , '1')->get();
		$data['username'] = Account::where( 'nickname' , '=' , Session::get('username'))->pluck('migid');
		$data['password'] = Crypt::decrypt(Session::get('enpassword'));
		return view( Config::get('setting.front_path').'/product/mig_mobile' , $data);
	}
	
	public function PLTIFRAME(Request $request){
		return view( Config::get('setting.front_path').'/product/plt_login' );
		
	}	

	public function PLTSLOTIFRAME(Request $request){
		$data['gamecode'] = $request->gamecode;
		
		return view( Config::get('setting.front_path').'/product/plt_slot_login' , $data);
		
	}


	public function PLT(Request $request){
		
		if( Product::where( 'status' , '=' , 2 )->where( 'code' , '=' , 'PLT' )->count() == 1 ){
			if($request->type == 'casino')
				return view( Config::get('setting.front_path').'/maintenance' );
			else
				return view( Config::get('setting.front_path').'/maintenance_slot' );
		} elseif ( in_array('PLT',$this->not_available_products) ) {
		    return $this->productTimeout($request);
        } 
		
	
		$pltObj = new PLT;
		
	  	if( !Accountproduct::is_exist( Session::get('username') , 'PLT' ) && Auth::user()->check())
		{
			$pltObj->createUser( array(
														'username' 	=> Session::get('username') , 
														'currency' 	=> Session::get('currency') , 
														'password' 	=> Session::get('enpassword') ,
														'email'    	=> Session::get('email') ,
														'firstname' => Session::get('fullname') ,
														'lastname'  => Session::get('fullname') ,
														'userid'    => Session::get('userid') 
														) ) ;
		}else{
			if(Auth::user()->check())
				$pltObj->updatePassword(Session::get('username'),Session::get('currency'),Crypt::decrypt(Session::get('enpassword')));
		}
		 
		if( $request->type == 'casino' )
		{
			if (Auth::user()->check())
			{
				$data['username'] = Config::get(Session::get('currency').'.plt.prefix').strtoupper(Session::get('username'));
				return view(Config::get('setting.front_path').'/product/plt', $data);
			}
		}
		else
        {
            $data['lists'] = $pltObj->gameList(Session::get('currency'), $request->type, $request->category);
        }
		 
		$data['isMobile']  = $pltObj->isMobileDevice(); 
	    $data['category']  = $request->category;
       
	    $data['type']  = $request->type;
	
		
		return view( Config::get('setting.front_path').'/slot_plt', $data );
	}	

	public function PLTSLOT(Request $request){
		
		$data['gamecode'] = $request->gamecode;
		$data['username'] = Config::get(Session::get('currency').'.plt.prefix').strtoupper(Session::get('username'));
		
		return view( Config::get('setting.front_path').'/product/plt_slot', $data );
	}
	
    public function PLTB(Request $request){
		
		if( Product::where( 'status' , '=' , 2 )->where( 'code' , '=' , 'PLTB' )->count() == 1 ){
			if($request->type == 'casino')
				return view( Config::get('setting.front_path').'/maintenance' );
			else
				return view( Config::get('setting.front_path').'/maintenance_slot' );
		} elseif ( in_array('PLTB',$this->not_available_products) ) {
		    return $this->productTimeout($request);
        }
		
	
		$pltObj = new PLTB;
		
	  	if( !Accountproduct::is_exist( Session::get('username') , 'PLTB' ) && Auth::user()->check())
		{
			$pltObj->createUser( array(
														'username' 	=> Session::get('username') , 
														'currency' 	=> Session::get('currency') , 
														'password' 	=> Session::get('enpassword'),
														'email'    	=> Session::get('email') ,
														'firstname' => Session::get('fullname') ,
														'lastname'  => Session::get('fullname') ,
														'userid'    => Session::get('userid') 
														) ) ;
		}else{
			if(Auth::user()->check())
				$pltObj->updatePassword(Session::get('username'),Session::get('currency'),Crypt::decrypt(Session::get('enpassword')));
		}
		 
		if( $request->type == 'casino' )
		{
			if (Auth::user()->check())
			{
				$data['username'] = Config::get(Session::get('currency').'.pltb.prefix').'_'.strtoupper(Session::get('username'));
				return view(Config::get('setting.front_path').'/product/pltb', $data);
			}
		}
		else
        {
            $data['lists'] = $pltObj->gameList(Session::get('currency'), $request->type, $request->category);
        }

	    $data['category']  = $request->category;
	    $data['type']  = $request->type;
        $data['isMobile'] = $pltObj->isMobileDevice();
		
		return view( Config::get('setting.front_path').'/slot_pltb', $data );
	}
	
	public function PLTBIFRAME(Request $request){
		
		return view( Config::get('setting.front_path').'/product/pltb_login' );
		
	}	
	
	public function PLTBSLOTIFRAME(Request $request){
		
		$data['gamecode'] = $request->gamecode;
		
		return view( Config::get('setting.front_path').'/product/pltb_slot_login' , $data);
		
	}
	
	public function PLTBSLOT(Request $request){

        $pltObj = new PLTB;
		$data['gamecode'] = $request->gamecode;
		$data['username'] = Config::get(Session::get('currency').'.pltb.prefix').'_'.strtoupper(Session::get('username'));
        $data['isMobile'] = $pltObj->isMobileDevice();
		
		return view( Config::get('setting.front_path').'/product/pltb_slot', $data );
	}
	
	public function PSB(Request $request){
		
		if( Product::where( 'status' , '=' , 2 )->where( 'code' , '=' , 'PSB' )->count() == 1 ) {
            return view(Config::get('setting.front_path') . '/maintenance');
        } elseif ( in_array('PSB',$this->not_available_products) ) {
		    return $this->productTimeout($request);
        }
	
		$psbObj = new PSB;
		
	 	if( !Accountproduct::is_exist( Session::get('username') , 'PSB' ) )
		{
			$psbObj->createUser( array(
														'username' 	=> Session::get('username') , 
														'currency' 	=> Session::get('currency') , 
														'password' 	=> Session::get('enpassword') ,
														'email'    	=> Session::get('email') ,
														'firstname' => Session::get('fullname') ,
														'lastname'  => Session::get('fullname') ,
														'userid'    => Session::get('userid') 
														) ) ;											
		}
		$aflid  = Session::get('aflid');
		$affObj = Affiliate::where('id', '=', Session::get('userid'))->pluck('username');
		if($aflid == 1){
			$result = $psbObj->addMember($affObj.'_'.Session::get('username'), false, Session::get('currency'));
		}else{ 
			$result = $psbObj->getLoginUrl(Session::get('username'), false, Session::get('currency'));
		}
		
		$data['login_url'] = $result['url'];

		return view(Config::get('setting.front_path').'/product/psb',$data);
		
	}
	
	public function PSB5D(Request $request){
		
		if( Product::where( 'status' , '=' , 2 )->where( 'code' , '=' , 'PSB' )->count() == 1 ) {
            return view(Config::get('setting.front_path') . '/maintenance');
        } elseif ( in_array('PSB',$this->not_available_products) ) {
		    return $this->productTimeout($request);
        }
	
		$psbObj = new PSB;
		
	 	if( !Accountproduct::is_exist( Session::get('username') , 'PSB' ) )
		{
			$psbObj->createUser( array(
														'username' 	=> Session::get('username') , 
														'currency' 	=> Session::get('currency') , 
														'password' 	=> Session::get('enpassword') ,
														'email'    	=> Session::get('email') ,
														'firstname' => Session::get('fullname') ,
														'lastname'  => Session::get('fullname') ,
														'userid'    => Session::get('userid') 
														) ) ;
		}
		 
		$result = $psbObj->getLoginUrl5D(Session::get('username'), false, Session::get('currency'));
	
		$data['login_url'] = $result['url'];

		return view(Config::get('setting.front_path').'/product/psb',$data);
		
	}
	
	public function PSB4DSPC(Request $request){
		
		if( Product::where( 'status' , '=' , 2 )->where( 'code' , '=' , 'PSB' )->count() == 1 ) {
            return view(Config::get('setting.front_path') . '/maintenance');
        } elseif ( in_array('PSB',$this->not_available_products) ) {
		    return $this->productTimeout($request);
        }
	
		$psbObj = new PSB;
		
	 	if( !Accountproduct::is_exist( Session::get('username') , 'PSB' ) )
		{
			$psbObj->createUser( array(
														'username' 	=> Session::get('username') , 
														'currency' 	=> Session::get('currency') , 
														'password' 	=> Session::get('enpassword') ,
														'email'    	=> Session::get('email') ,
														'firstname' => Session::get('fullname') ,
														'lastname'  => Session::get('fullname') ,
														'userid'    => Session::get('userid') 
														) ) ;
		}
		 
		$result = $psbObj->getLoginUrl4Dspc(Session::get('username'), false, Session::get('currency'));
	
		$data['login_url'] = $result['url'];

		return view(Config::get('setting.front_path').'/product/psb',$data);
		
	}
	
	public function SCR(Request $request){
		
		if( Product::where( 'status' , '=' , 2 )->where( 'code' , '=' , 'SCR' )->count() == 1 ) {
            return view(Config::get('setting.front_path') . '/maintenance');
        } elseif ( in_array('SCR',$this->not_available_products) ) {
		    return $this->productTimeout($request);
        }
	
		$scrObj = new SCR;
		
	 	if( !Accountproduct::is_exist( Session::get('username') , 'SCR' ) )
		{
			$scrObj->createUser( array(
														'username' 	=> Session::get('username') , 
														'currency' 	=> Session::get('currency') , 
														'password' 	=> Session::get('enpassword') ,
														'email'    	=> Session::get('email') ,
														'firstname' => Session::get('fullname') ,
														'lastname'  => Session::get('fullname') ,
														'userid'    => Session::get('userid') 
														) ) ;
		}
		 
		$data['scrid'] = Account::where( 'nickname' , '=' , Session::get('username'))->pluck('scrid');
		
		return view(Config::get('setting.front_path').'/product/scr',$data);
		
	}	
	
	public function A1A(Request $request){
		
		if( Product::where( 'status' , '=' , 2 )->where( 'code' , '=' , 'A1A' )->count() == 1 ) {
            return view(Config::get('setting.front_path') . '/maintenance_slot');
        } elseif ( in_array('A1A',$this->not_available_products) ) {
		    return $this->productTimeout($request);
        }
	
		$a1aObj = new A1A;
		
	 	if( !Accountproduct::is_exist( Session::get('username') , 'A1A' ) && Auth::user()->check())
		{
			$a1aObj->createUser( array(
														'username' 	=> Session::get('username') , 
														'currency' 	=> Session::get('currency') , 
														'password' 	=> Session::get('enpassword') ,
														'email'    	=> Session::get('email') ,
														'firstname' => Session::get('fullname') ,
														'lastname'  => Session::get('fullname') ,
														'userid'    => Session::get('userid') 
														) ) ;
		}
		 
		$data['name']  = 'a1a';
		$data['lists'] = $a1aObj->gamesList(Session::get('currency'));
	

		return view(Config::get('setting.front_path').'/product/a1a',$data);
		
	}	
	
	public function A1AIFRAME(Request $request){
		
		/* if( Product::where( 'status' , '=' , 2 )->where( 'code' , '=' , 'A1A' )->count() == 1 )
			return view( Config::get('setting.front_path').'/maintenance' ); */
	
		$a1aObj = new A1A;
		
	 	if( !Accountproduct::is_exist( Session::get('username') , 'A1A' ) && Auth::user()->check())
		{
			$a1aObj->createUser( array(
														'username' 	=> Session::get('username') , 
														'currency' 	=> Session::get('currency') , 
														'password' 	=> Session::get('enpassword') ,
														'email'    	=> Session::get('email') ,
														'firstname' => Session::get('fullname') ,
														'lastname'  => Session::get('fullname') ,
														'userid'    => Session::get('userid') 
														) ) ;
		}
		 
		$data['name']  = 'a1a';
		$data['lists'] = $a1aObj->gamesList(Session::get('currency'));
	

		return view(Config::get('setting.front_path').'/product/a1a_iframe',$data);
		
	}
	
	public function A1ASLOT(Request $request){
		
	/* 	if( Product::where( 'status' , '=' , 2 )->where( 'code' , '=' , 'A1A' )->count() == 1 )
			return view( Config::get('setting.front_path').'/maintenance' ); */
		
		$a1aObj = new A1A;
		
		$data['url'] = $a1aObj->getLoginUrl(Session::get('username'), Session::get('currency'), $request->gameid);

		return view( Config::get('setting.front_path').'/product/a1a_slot', $data );
	}
		
	public function PLS(Request $request){

		$plsObj = new PLS;

        if (Product::where('status', '=', 2)->where('code', '=', 'PLS')->count() == 1 || in_array('PLS',$this->not_available_products)) {
            $data['lists'] = array();
        } else {
            $data['lists'] = $plsObj->gameList(Session::get('currency'));
        }
		
		if( !Accountproduct::is_exist( Session::get('username') , 'PLS' ) && Auth::user()->check())
		{
			$plsObj->createUser( array(
														'username' 	=> Session::get('username') , 
														'currency' 	=> Session::get('currency') , 
														'password' 	=> Session::get('enpassword') ,
														'email'    	=> Session::get('email') ,
														'firstname' => Session::get('fullname') ,
														'lastname'  => Session::get('fullname') ,
														'userid'    => Session::get('userid') 
														) ) ;
		}else{
			$data['token'] = Onlinetracker::where( 'username' , '=' , Session::get('username'))->pluck('token');
		}

		return view( Config::get('setting.front_path').'/product/pls', $data );
	}
	
	public function HOG(Request $request) {
		if (Product::where('status', '=', 2)->where('code', '=', 'HOG')->count() == 1 ) {
			return view(Config::get('setting.front_path') . '/maintenance');
		} elseif ( in_array('HOG',$this->not_available_products) ) {
		    return $this->productTimeout($request);
        }
		
		$hogObj = new HOG();
		
		if (Auth::user()->check()) {
            $params = array(
                'username' => Session::get('username'),
                'currency' => Session::get('currency'),
                'password' => Session::get('enpassword'),
                'email' => Session::get('email'),
                'firstname' => Session::get('fullname'),
                'lastname' => Session::get('fullname'),
                'userid' => Session::get('userid'),
            );
            
			if (!Accountproduct::is_exist(Session::get('username'), 'HOG')) {
				$result = $hogObj->createUser($params);
			}

			/* $hogObj->updateUser( array(
				'username' 	=> Session::get('username') , 
				'currency' 	=> Session::get('currency')
			)); */

			$data['login_url'] = $hogObj->getLoginUrl($params, $params['currency'], Lang::getLocale());
		} else {
			$data['login_url'] = $hogObj->getLoginUrl(null, Session::get('currency'), Lang::getLocale(), 1, 1);
		}

		if (str_contains($data['login_url'], 'mobile=true')) {
		    // iOS device cannot run with iframe, workaround is all mobile straight redirect to game's login URL.
            return redirect()->away($data['login_url']);
        }

		return view(Config::get('setting.front_path') . '/product/hog', $data);
	}
	
	public function VGS(Request $request) {
		if (Product::where('status', '=', 2)->where('code', '=', 'VGS')->count() == 1 ) {
			return view(Config::get('setting.front_path') . '/maintenance');
		} elseif ( in_array('VGS',$this->not_available_products) ) {
		    return $this->productTimeout($request);
        }
		
		$hogObj = new VGS();
		
		if (Auth::user()->check()) {
            $params = array(
                'username' => Session::get('username'),
                'currency' => Session::get('currency'),
                'password' => Session::get('enpassword'),
                'email' => Session::get('email'),
                'firstname' => Session::get('fullname'),
                'lastname' => Session::get('fullname'),
                'userid' => Session::get('userid'),
            );
            
			if (!Accountproduct::is_exist(Session::get('username'), 'VGS')) {
				$result = $hogObj->createUser($params);
			}

			$data['login_url'] = $hogObj->getLoginUrl($params, date('U').mt_rand(1111,9999), Lang::getLocale(), $request->ip());
		} else {
			$data['login_url'] = '';
		}
		
		return view(Config::get('setting.front_path') . '/product/vgs', $data);
	}
        
        public function EVO(Request $request) {
		if (Product::where('status', '=', 2)->where('code', '=', 'EVO')->count() == 1 ) {
			return view(Config::get('setting.front_path') . '/maintenance');
		} elseif ( in_array('EVO',$this->not_available_products) ) {
		    return $this->productTimeout($request);
        }
		
		$evoObj = new EVO();
		
		if (Auth::user()->check()) {
                    $params = array(
                        'username' => Session::get('username'),
                        'currency' => Session::get('currency'),
                        'password' => Session::get('enpassword'),
                        'email' => Session::get('email'),
                        'firstname' => Session::get('fullname'),
                        'lastname' => Session::get('fullname'),
                        'userid' => Session::get('userid'),
                    );
            
                    if (!Accountproduct::is_exist(Session::get('username'), 'EVO')) {
                            $result = $evoObj->createUser($params);
                    }

                    $data['login_url'] = $evoObj->getLoginUrl($params, date('U').mt_rand(1111,9999), Lang::getLocale(), $request->ip());
		} else {
			$data['login_url'] = '';
		}
		
		return view(Config::get('setting.front_path') . '/product/evo', $data);
	}
	
	public function JOK(Request $request) {
		if (Product::where('status', '=', 2)->where('code', '=', 'JOK')->count() == 1 ) {
			return view(Config::get('setting.front_path') . '/maintenance');
		} elseif ( in_array('JOK',$this->not_available_products) ) {
		    return $this->productTimeout($request);
        }
		
		$jokObj = new JOK();
        $data = array(
            'login_url' => Config::get(Session::get('currency').'.jok.login_url'),
            'token' => '',
        );
		
		if (Auth::user()->check()) {
            $params = array(
                'username' => Session::get('username'),
                'currency' => Session::get('currency'),
                'password' => Session::get('enpassword'),
                'email' => Session::get('email'),
                'firstname' => Session::get('fullname'),
                'lastname' => Session::get('fullname'),
                'userid' => Session::get('userid'),
            );
            
			if (!Accountproduct::is_exist(Session::get('username'), 'JOK')) {
				$result = $jokObj->createUser($params);
			}

			$jokObj->updatePassword( array(
				'username' 	=> Session::get('username') , 
				'password' 	=> Session::get('enpassword') ,
				'currency' 	=> Session::get('currency') ,
			));

			$data['token'] = $jokObj->getLoginToken($params);
		} else {
			$data['token'] = '';
		}
		
		return view(Config::get('setting.front_path') . '/product/jok', $data);
	}
	
	public function OPU(Request $request) {
        
		
		$url = explode('.',$_SERVER['HTTP_HOST']);
		$i = count($url);
		$website = '';
		if( isset($url[($i-2)]) )
		$website = $url[($i-2)].'.'.$url[($i-1)];
		$website = str_replace("http://m.","http://",$website);
		
		$data['website'] = $website;
		
		$data['lang']  = Session::get('locale') == 'cn' ? 'cs' : Session::get('locale');
		
		if( !Auth::user()->check() ){

			return response()->view( Config::get('setting.front_path').'/product/opu',$data);
		
			
		}
		
		
		if( Product::where( 'status' , '=' , 2 )->where( 'code' , '=' , 'OPU' )->count() == 1 ) {
            return view(Config::get('setting.front_path') . '/maintenance');
        } elseif ( in_array('OPU',$this->not_available_products) ) {
		    return $this->productTimeout($request);
        }
		
	
		
		$opuObj = new OPU;
		
		if( !Accountproduct::is_exist( Session::get('username') , 'OPU' ) && Auth::user()->check() )
		{
			if( $opuObj->createUser( array(
														'username' 	=> Session::get('username') , 
														'currency' 	=> Session::get('currency') , 
														'password' 	=> Session::get('enpassword') ,
														'email'    	=> Session::get('email') ,
														'firstname' => Session::get('fullname') ,
														'lastname'  => Session::get('fullname') ,
														'userid'    => Session::get('userid') 
														)  ) )
			{
				
			}
		}
		

		
		
		
		$data['token'] = Config::get(Session::get('currency').'.opu.operator_id').'_'.Onlinetracker::where( 'username' , '=' , Session::get('username'))->where( 'type' , '=' , 1)->pluck('token');

		return response()->view( Config::get('setting.front_path').'/product/opu',$data)
						 ->withCookie( cookie( 's' , Config::get(Session::get('currency').'.opu.operator_id').'_'.Onlinetracker::where( 'username' , '=' , Session::get('username'))->where( 'type' , '=' , 1)->pluck('token') , 3600 , null , '.'.$website) );

    }

    public function ILT(Request $request){

        if (Product::where('status', '=', 2)->where('code', '=', 'ILT')->count() == 1 ) {
            return view(Config::get('setting.front_path') . '/maintenance');
        } elseif ( in_array('ILT',$this->not_available_products) ) {
            return $this->productTimeout($request);
        }

        $iltObj = new ILT();
        $data = array(
            'login_url' => '',
        );

        if (Auth::user()->check()) {
            $params = array(
                'username' => Session::get('username'),
                'currency' => Session::get('currency'),
                'password' => Session::get('enpassword'),
                'email' => Session::get('email'),
                'firstname' => Session::get('fullname'),
                'lastname' => Session::get('fullname'),
                'userid' => Session::get('userid'),
            );

            if (!Accountproduct::is_exist(Session::get('username'), 'ILT')) {
                $iltObj->createUser($params);
            }

            $iltObj->updatePassword($params['username'], $params['password'], $params['currency']);

            $data['login_url'] = $iltObj->getLoginUrl($params['username'], $params['currency'], $params['password']);
        }

        return view(Config::get('setting.front_path') . '/product/ilt', $data);

    }
	
	public function SKY(Request $request){
        
		if (Product::where('status', '=', 2)->where('code', '=', 'SKY')->count() == 1 ) {
			return view(Config::get('setting.front_path') . '/maintenance');
		} elseif ( in_array('SKY',$this->not_available_products) ) {
		    return $this->productTimeout($request);
        }
		
		$skyObj = new SKY();
        
		if (Auth::user()->check()) {
            $params = array(
                'username' => Session::get('username'),
                'currency' => Session::get('currency'),
                'password' => Session::get('enpassword'),
                'email' => Session::get('email'),
                'firstname' => Session::get('fullname'),
                'lastname' => Session::get('fullname'),
                'userid' => Session::get('userid'),
            );
            
			if (!Accountproduct::is_exist(Session::get('username'), 'SKY')) {
				$result = $skyObj->createUser($params);
			}
            
            $skyObj->updatePassword($params);
		}

        $data['lists'] = $skyObj->gameList(Session::get('currency'), $request->category);

		return view(Config::get('setting.front_path') . '/slot_sky', $data);
		
	}
	
	public function SKYSLOT(Request $request){
		
		$skyObj = new SKY();
        
		$data['login_url'] = $skyObj->getLoginUrl(Session::get('username'), Session::get('currency'), $request->gamecode);
		
		return view( Config::get('setting.front_path').'/product/sky_slot', $data );
	}
	
	public function UC8(Request $request){
        
		if (Product::where('status', '=', 2)->where('code', '=', 'UC8')->count() == 1 ) {
			return view(Config::get('setting.front_path') . '/maintenance');
		} elseif ( in_array('UC8',$this->not_available_products) ) {
		    return $this->productTimeout($request);
        }
		
		$uc8Obj = new UC8();
        
		if (Auth::user()->check()) {
            $params = array(
                'username' => Session::get('username'),
                'currency' => Session::get('currency'),
                'password' => Session::get('enpassword'),
                'email' => Session::get('email'),
                'firstname' => Session::get('fullname'),
                'lastname' => Session::get('fullname'),
                'userid' => Session::get('userid'),
            );
            
			if (!Accountproduct::is_exist(Session::get('username'), 'UC8')) {
				$result = $uc8Obj->createUser($params);
			}
            
            //$uc8Obj->updatePassword($params);
		}
        
        $data['lists'] = $uc8Obj->gamesList(Session::get('currency'));
		
		return view(Config::get('setting.front_path') . '/slot_uc8', $data);
		
	}
	
	public function UC8SLOT(Request $request){
		
		$uc8Obj = new UC8();
        
		$data['login_url'] = $uc8Obj->getLoginUrl(Session::get('username'), Session::get('currency'), $request->gamecode, Lang::getLocale());
		
		return view( Config::get('setting.front_path').'/product/uc8_slot', $data );
	}

	public function BBN(Request $request){
		
		if( Product::where( 'status' , '=' , 2 )->where( 'code' , '=' , 'BBN' )->count() == 1 ) {
            return view(Config::get('setting.front_path') . '/maintenance');
        } elseif ( in_array('BBN',$this->not_available_products) ) {
		    return $this->productTimeout($request);
        }
	
		$bbnObj = new BBN;
		
	 	if( !Accountproduct::is_exist( Session::get('username') , 'BBN' ) )
		{
			$bbnObj->createUser( array(
														'username' 	=> Session::get('username') , 
														'currency' 	=> Session::get('currency') , 
														'password' 	=> Session::get('enpassword') ,
														'email'    	=> Session::get('email') ,
														'firstname' => Session::get('fullname') ,
														'lastname'  => Session::get('fullname') ,
														'userid'    => Session::get('userid') 
														) ) ;
		}
		 
		
		$data['login_url'] = $bbnObj->getLoginUrl( 
													Session::get('username') , 
													Session::get('currency') 
												 );
                
		
		return view(Config::get('setting.front_path').'/product/bbn',$data);
		
	}

	public function BBNSLOT(Request $request){
		
		if( Product::where( 'status' , '=' , 2 )->where( 'code' , '=' , 'BBN' )->count() == 1 ) {
            return view(Config::get('setting.front_path') . '/maintenance');
        } elseif ( in_array('BBN',$this->not_available_products) ) {
		    return $this->productTimeout($request);
        }
	
		$bbnObj = new BBN;
		
	 	if( !Accountproduct::is_exist( Session::get('username') , 'BBN' ) )
		{
			$bbnObj->createUser( array(
														'username' 	=> Session::get('username') , 
														'currency' 	=> Session::get('currency') , 
														'password' 	=> Session::get('enpassword') ,
														'email'    	=> Session::get('email') ,
														'firstname' => Session::get('fullname') ,
														'lastname'  => Session::get('fullname') ,
														'userid'    => Session::get('userid') 
														) ) ;
		}
		 
		
		$data['login_url'] = $bbnObj->slotGame( 
													Session::get('username') , 
													Session::get('currency') 
												 );
                
		
		return view(Config::get('setting.front_path').'/product/bbn_slot',$data);
		
	}
	
	public function BBNLOTTERY(Request $request){
		
		if( Product::where( 'status' , '=' , 2 )->where( 'code' , '=' , 'BBN' )->count() == 1 ) {
            return view(Config::get('setting.front_path') . '/maintenance');
        } elseif ( in_array('BBN',$this->not_available_products) ) {
		    return $this->productTimeout($request);
        }
	
		$bbnObj = new BBN;
		
	 	if( !Accountproduct::is_exist( Session::get('username') , 'BBN' ) )
		{
			$bbnObj->createUser( array(
														'username' 	=> Session::get('username') , 
														'currency' 	=> Session::get('currency') , 
														'password' 	=> Session::get('enpassword') ,
														'email'    	=> Session::get('email') ,
														'firstname' => Session::get('fullname') ,
														'lastname'  => Session::get('fullname') ,
														'userid'    => Session::get('userid') 
														) ) ;
		}
		 
		
		$data['login_url'] = $bbnObj->lottery( 
													Session::get('username') , 
													Session::get('currency') 
												 );
                
		
		return view(Config::get('setting.front_path').'/product/bbn_lottery',$data);
		
	}

    public function MNT(Request $request){

        if (Product::where('status', '=', 2)->where('code', '=', 'MNT')->count() == 1 ) {
            return view(Config::get('setting.front_path') . '/maintenance');
        } elseif ( in_array('MNT',$this->not_available_products) ) {
            return $this->productTimeout($request);
        }

        $mntObj = new MNT();

        if (Auth::user()->check()) {
            $params = array(
                'username' => Session::get('username'),
                'currency' => Session::get('currency'),
                'password' => Session::get('enpassword'),
                'email' => Session::get('email'),
                'firstname' => Session::get('fullname'),
                'lastname' => Session::get('fullname'),
                'userid' => Session::get('userid'),
            );

            if (!Accountproduct::is_exist(Session::get('username'), 'MNT')) {
                $result = $mntObj->createUser($params);
            }

            $mntObj->updatePassword($params['username'], $params['password'], $params['currency']);
        }

        return view(Config::get('setting.front_path') . '/product/mnt');

    }

	public function ISN(Request $request) {

        if (Product::where('status', '=', 2)->where('code', '=', 'ISN')->count() == 1 ) {
            return view(Config::get('setting.front_path') . '/maintenance');
        } elseif ( in_array('ISN',$this->not_available_products) ) {
            return $this->productTimeout($request);
        }

        $isnObj = new ISN();
        $data = array('login_url' => '');

        if (Auth::user()->check()) {
            $params = array(
                'username' => Session::get('username'),
                'currency' => Session::get('currency'),
                'password' => Session::get('enpassword'),
                'email' => Session::get('email'),
                'firstname' => Session::get('fullname'),
                'lastname' => Session::get('fullname'),
                'userid' => Session::get('userid'),
            );

            if (!Accountproduct::is_exist(Session::get('username'), 'ISN')) {
                $result = $isnObj->createUser($params);
            }

            $data['login_url'] = $isnObj->getLoginUrl($params, Lang::getLocale());
        }

        return view(Config::get('setting.front_path') . '/product/isn', $data);
    }

    public function CTB(Request $request)
    {

        if (Product::where('status', '=', 2)->where('code', '=', 'CTB')->count() == 1 ) {
            return view(Config::get('setting.front_path') . '/maintenance');
        } elseif ( in_array('CTB', $this->not_available_products) ) {
            return $this->productTimeout($request);
        }

        if ($request->get('page') == 'intro') {
            return view(Config::get('setting.front_path') . '/product/ctb_intro');
        }

        $ctbObj = new CTB();
        $data = array('login_url' => '');

        if (Auth::user()->check()) {
            $params = array(
                'username' => Session::get('username'),
                'currency' => Session::get('currency'),
                'password' => Session::get('enpassword'),
                'email' => Session::get('email'),
                'firstname' => Session::get('fullname'),
                'lastname' => Session::get('fullname'),
                'userid' => Session::get('userid'),
            );

            if (!Accountproduct::is_exist(Session::get('username'), 'CTB')) {
                $result = $ctbObj->createUser($params);
            }

            $ctbObj->updateUser($params);

            $data['login_url'] = $ctbObj->getLoginUrl($params['username'], $params['currency'], Lang::getLocale());
        }

        return view(Config::get('setting.front_path') . '/product/ctb', $data);
    }
	public function WMC(Request $request) {

        if (Product::where('status', '=', 2)->where('code', '=', 'WMC')->count() == 1 ) {
            return view(Config::get('setting.front_path') . '/maintenance');
        } elseif ( in_array('WMC',$this->not_available_products) ) {
            return $this->productTimeout($request);
        }

        $isnObj = new WMC();
        $data = array('login_url' => '');

        if (Auth::user()->check()) {
            $params = array(
                'username' => Session::get('username'),
                'currency' => Session::get('currency'),
                'password' => Session::get('enpassword'),
                'email' => Session::get('email'),
                'firstname' => Session::get('fullname'),
                'lastname' => Session::get('fullname'),
                'userid' => Session::get('userid'),
            );

            if (!Accountproduct::is_exist(Session::get('username'), 'WMC')) {
                $result = $isnObj->createUser($params);
            }

            $data['login_url'] = $isnObj->getLoginUrl(Session::get('username'),  $params['currency'],$params['password'],1);
        }

        return view(Config::get('setting.front_path') . '/product/wmc', $data);
    }
	public function S128(Request $request){
	
		if( Product::where( 'status' , '=' , 2 )->where( 'code' , '=' , 'S128' )->count() == 1 ) {
            return view(Config::get('setting.front_path') . '/maintenance');
        } elseif ( in_array('S128',$this->not_available_products) ) {
		    return $this->productTimeout($request);
        }

		$s128Obj = new S128();
		
		if( !Accountproduct::is_exist( Session::get('username') , 'S128' ) )
		{
			$s128Obj->createUser( array(
														'username' 	=> Session::get('username') , 
														'currency' 	=> Session::get('currency') , 
														'password' 	=> Session::get('enpassword') ,
														'email'    	=> Session::get('email') ,
														'firstname' => Session::get('fullname') ,
														'lastname'  => Session::get('fullname') ,
														'userid'    => Session::get('userid') 
														)  ) ;
		}
		
		$data['login_url'] = $s128Obj->getLoginUrl(Session::get('username'),Session::get('currency'));
	
		if( $data['login_url'] )
			return view(Config::get('setting.front_path').'/product/s128',$data);
		else
			return view(Config::get('setting.front_path').'/maintenance');
	}
	
	

    public function productTimeout(Request $request) {
        sleep(mt_rand(9, 15));
        
        return response('<!doctype html><html><head></head>'.
            '<body style="background-color: #000; color: #fff; text-align: center;"><p style="margin-top: 24px;">Connection timeout, please check your network and try again.</p></body>'.
            '</html>')->header('Content-Type', 'text/html; charset=UTF-8');
    }

    public function productRequirement(Request $request) {
        return response('<!doctype html><html><head></head>'.
            '<body style="background-color: #000; color: #fff; text-align: center;"><p style="margin-top: 24px; font-size: 1.5em;">This game currently not available in mobile version, please use PC to access. Thank you!<br>此游戏目前没有提供手机版本，请使用电脑进行游戏。谢谢!</p></body>'.
            '</html>')->header('Content-Type', 'text/html; charset=UTF-8');
    }
}
