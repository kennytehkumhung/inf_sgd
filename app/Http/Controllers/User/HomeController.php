<?php namespace App\Http\Controllers\User;

use App\Models\Configs;
use App\Models\Luckydrawledger;
use App\Models\Onlinetracker;
use App\Models\PremierLeagueBet;
use App\Models\PremierLeagueMatch;
use App\Models\PremierLeagueToken;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\Worldcuptoken;
use Session;
use Cache;
use Lang;
use App\Models\Article;
use App\Models\Media;
use App\Models\Betpsbresult;
use App\Models\Betcloresult;
use App\Models\Banner;
use App\Models\Currency;
use App\Models\Product;
use App\Models\Account;
use App\Models\Promocash;
use App\Models\Promocampaign;
use App\Models\Cashledger;
use App\Models\Bankaccount;
use App\Models\Bankledger;
use App\Models\Apklog;
use App\Models\Inbox;
use App\Models\Toallinbox;
use Config;
use App\libraries\providers\MXB;
use App\libraries\providers\ALB;
use App\libraries\providers\SBOF;
use App\libraries\providers\PLS;
use Crypt;
use Banksetting;
use Redirect;
use DB;
use Auth;
use App\Jobs\ChangeLocale;
use App\libraries\providers\TBS;
use App\libraries\App;

class HomeController extends Controller{

	public function __construct()
	{

	}

	public function index(Request $request){

        $data = array();

        if ($request->input('check') == 'f4d' && Config::get('setting.opcode') == 'IFW' && Session::get('user_currency') == 'MYR') {

            try {
                $p4dController = new Promo4dController();
                $p4dController->addTokenFirstTimeLogin();
            } catch (\Exception $e) {
                // Do nothing.
            }

            $luckydraw = new Luckydrawledger;
            $f4dToken = $luckydraw->getBalance( Session::get('userid') , Configs::getParam('SYSTEM_4D_DRAWNO') );


            if ($f4dToken > 0) {
                return redirect()->route('fortune', array('reminder' => 'y'));
            }
        } elseif (Config::get('setting.opcode') == 'RYW') {
            if (in_array(Session::get('currency'), array('MYR', 'IDR'))) {
                if(Session::has('userid')) {
                    FortuneWheelController::addDailyLoginFreeToken( Session::get('user_currency'), Session::get('userid'), Session::get('username') );
                }
            }

            if (Session::get('currency') == 'MYR') {
                if ($request->input('check') == 'epl' && Session::has('userid')) {
                    try {
                        $matchObj = PremierLeagueMatch::getCurrentOpeningMatch();

                        if ($matchObj) {
                            $token = PremierLeagueToken::getAvailableTokenAmount(Session::get('userid'), Session::get('user_currency'), $matchObj->id);

                            if ($token > 0) {
                                return redirect()->route('premier-league', array('reminder' => 'y'));
                            }
                        }
                    } catch (\Exception $e) {
                        // Do nothing.
                    }
                }

                $data['premier_league_thumbnail'] = Cache::rememberForever('premier_league_thumbnail', function() {
                    try {
                        return PremierLeagueMatch::getHomepageThumbnail();
                    } catch (\Exception $e) {
                        // Do nothing.
                    }

                    return false;
                });
            }

            // Get news.
            $newsArr = array();
//            $articleObj = Article::where('type', '=', 2)
//            $articleObj = Article::where('type', '=', Article::TYPE_NEWS)
//                ->where('crccode', '=', Session::get('currency'))
//                ->where('lang', '=', $request->input('lang', Lang::getLocale()))
//                ->where('status', '=', 1)
//                ->orderBy('id', 'desc')
//                ->limit(5)
//                ->get();
//
//            foreach ($articleObj as $r) {
//                $imgUrl = $r->getImage();
//
//                $newsArr[] = array(
//                    'title' => $r->title,
//                    'content' => $r->content,
//                    'img_url' => ($imgUrl === false ? '' : $imgUrl),
//                    'created_at' => Carbon::parse($r->created)->format('d/m/Y h:iA'),
//                );
//            }

            $data['news'] = $newsArr;
        }

        if ($request->has('setlang')) {
            if (preg_match('/[a-zA-Z]{2}/', $request->input('setlang'))) {
                Lang::setLocale($request->input('setlang'));
            }
        }

		$data['popup_banner'] = false;
        $data['is_mobile_agent'] = $this->isMobileAgent($request);

        if ($this->isMobileSite()) {
            $banners = Cache::rememberForever('Banner_Mobile_'.Lang::getLocale().'_'.Session::get('currency'), function() {
                return Banner::where('status','=',1)->where('crccode','=', Session::get('currency'))->where('media.refobj','=','Banner')->where('lang','=',Lang::getLocale())->leftJoin('media', 'banner.id', '=', 'media.refid')->where('media.type', '=', Media::TYPE_IMAGE_MOBILE)->orderBy( 'rank' , 'asc')->get();
            });
        } else {
            $banners = Cache::rememberForever('Banner_'.Lang::getLocale().'_'.Session::get('currency'), function() {
                return Banner::where('status','=',1)->where('crccode','=', Session::get('currency'))->where('media.refobj','=','Banner')->where('lang','=',Lang::getLocale())->leftJoin('media', 'banner.id', '=', 'media.refid')->where('media.type', '=', Media::TYPE_IMAGE)->orderBy( 'rank' , 'asc')->get();
            });
        }

		foreach( $banners as $banner )
		{
			if( $banner->categories == 2 )
			{
				$data['popup_banner'] = $banner->domain . '/' . $banner->path;
			}else{
				$data['banners'][] = $banner;
			}
		}


		if(  Auth::user()->check() && Config::get('setting.front_path') == "369bet")
		{
			$mxb = new MXB;
			$data['mxb_slot'] = $mxb->getSlotLobby(Session::get('username'),Session::get('locale'),Session::get('currency'),Session::get('enpassword'));
			Session::put( 'mxb_slot' , $data['mxb_slot']);
		}
                
                $data['startdate'] = '2018-11-05 18:02:00';

		return view(Config::get('setting.front_path').'/index',$data);
	}

	public function home(Request $request){

		if( $request->has('refcode') )
		{
			Session::put('user_refcode',  $request->input('refcode') );
		}

		if( $request->has('utm_source') ){
			Session::put('user_channel',  $request->input('utm_source') );
		}

		if( $request->has('channel') ){
			Session::put('user_channel',  $request->input('channel') );
		}

		$data['is_mobile_agent'] = $this->isMobileAgent($request);

		return view(Config::get('setting.front_path').'/home',$data);
	}
        

	public function mobile(Request $request){
		$ios=array();
		$android=array();
		$pc=array();
		if(session::get('opcode')=='IFW'){
			$ios			['mxb']	   ="https://xprogaming.hs.llnwd.net/mobileHTMLapk/62_1/133XPG-MYR-PROD-6200.0.1.apk";
			$android		['mxb']	   ='https://xprogaming.hs.llnwd.net/mobileHTMLapk/62_1/133XPG-MYR-PROD-6200.0.1.apk';
			$android		['alb']	   ='https://www.abgapp.net/';
			$ios			['alb']	   ='https://www.abgapp.net/';
			$android		['pltslot']	   ='http://m.ld176988.com/download.html';
			$android		['pltlive']	   ='http://m.ld176988.com/live/download.html';
			$pc 			['pc']	   	   ='http://cdn.s8star.com/setupglx.exe';
			$android 		['agg']	   	   ='http://agmbet.com/';
			$ios 			['agg']	   	   ='http://agmbet.com/';
			$android 		['jok']	   	   ='http://joker688.net/mobile';
			$ios 			['jok']	   	   ='http://joker688.net/mobile';
			$android 		['sky']	   	   ='https://ed.sky1388.com/sky1388_android.html';
			$ios 			['sky']	   	   ='https://ed.sky1388.com/sky1388.html';

		}elseif(session::get('opcode')=='RYW'){
		if(session::get('currency')=='MYR'){
			$android['mxb']='https://xprogaming.hs.llnwd.net/mobileHTMLapk/62_1/710EuroCasino-MYR-PROD-6200.0.1.apk';
		}else{
			$android['mxb']='https://xprogaming.hs.llnwd.net/mobileHTMLapk/62_1/711EuroCasino-IDR-PROD-6200.0.1.apk';
		}
		}
		if(session::get('opcode')=='RYW'){
		$ios			['sky']    ='https://ed.sky1388.com/sky1388.html';
		$android		['sky']	   ='https://ed.sky1388.com/sky1388_android.html';
		$pc				['sky']    ='https://ed.sky1388.com/sky1388_kiosk.html';
		$android		['alb']	   ='https://www.abgapp.net/';
		$ios			['alb']	   ='https://www.abgapp.net/';
		$android		['pltslot']='http://m.ld176988.com/download.html ';
		$android		['pltlive']='http://m.ld176988.com/live/download.html ';
		$pc				['pltpc']  ='http://cdn.s8star.com/setupglx.exe';
		$android		['agg']	   ='http://agmbet.com/ ';
		$ios			['agg']	   ='http://agmbet.com/ ';
		$android		['jok']	   ='http://joker688.net/mobile';
		$ios			['jok']	   ='http://joker688.net/mobile';
		$android		['scr']	   ='http://dl.918kiss.com/';
		$ios			['scr']	   ='http://dl.918kiss.com/';

		}

		$data['android']= $android;
		$data['ios']	= $ios;
		$data['pc']		= $pc;
		return view(Config::get('setting.front_path').'/mobile',$data);
	}

	public function main(Request $request){

		return view(Config::get('setting.front_path').'/main');
	}

	public function wallet(Request $request){

		return view(Config::get('setting.front_path').'/wallet');
	}

	public function mobileSelection(Request $request){

		$data['html5'] = '';
		$data['apk'] = '';

		if($request->has('html5'))
		{
			$data['html5'] = $request->get('html5');
		}
		if($request->has('apk'))
		{
			$data['apk'] = $request->get('apk');
		}

		return view(Config::get('setting.front_path').'/mobile_selection',$data);
	}

	public function livecasino(Request $request){

		return view(Config::get('setting.front_path').'/livecasino');
	}

	public function sport(Request $request){

		return view(Config::get('setting.front_path').'/sport');
	}

	public function slot(Request $request){

		return view(Config::get('setting.front_path').'/slot');
	}

	public function promo(Request $request){

		return view(Config::get('setting.front_path').'/promotion');
	}

	public function shishicai(Request $request){

		return view(Config::get('setting.front_path').'/shishicai');
	}

	public function fishingworld(Request $request){

		return view(Config::get('setting.front_path').'/fishingworld');
	}

	public function downloadfishing(Request $request){

		return view(Config::get('setting.front_path').'/downloadfishing');
	}

	public function tnc(Request $request){

		$data['content'] = Article::where('code','=','TNC')->where('crccode', '=', Session::get('currency'))->where('lang','=',Lang::getLocale())->pluck('content');
		return view(Config::get('setting.front_path').'/tnc',$data);
	}

	public function tnc2(Request $request){

		$data['content'] = Article::where('code','=','TNC')->where('crccode', '=', Session::get('currency'))->where('lang','=',Lang::getLocale())->pluck('content');
		return view(Config::get('setting.front_path').'/tnc2',$data);
	}

	public function contactus(Request $request){

		$data['content'] = Article::where('code','=','CONTACTUS')->where('lang','=',Lang::getLocale())->pluck('content');

		return view(Config::get('setting.front_path').'/contactus',$data);
	}

	public function howtojoin(Request $request){

		$data['content'] = Article::where('code','=','HOWTOJOIN')->where('crccode', '=', Session::get('currency'))->where('lang','=',Lang::getLocale())->pluck('content');
		return view(Config::get('setting.front_path').'/howtojoin',$data);
	}

	public function howtodeposit(Request $request){

		$data['content'] = Article::where('code','=','HOWTODEPOSIT')->where('lang','=',Lang::getLocale())->pluck('content');
		return view(Config::get('setting.front_path').'/howtodeposit',$data);
	}

	public function aboutus(Request $request){

		$data['content'] = Article::where('code','=','ABOUTUS')->where('crccode', '=', Session::get('currency'))->where('lang','=',Lang::getLocale())->pluck('content');
		return view(Config::get('setting.front_path').'/aboutus',$data);
	}

	public function banking(Request $request){
		if( Session::get('currency') == 'VND' ){
			$data['content'] = Article::where('code','=','BANKING_'.Lang::getLocale())->pluck('content');
		}else{
			$data['content'] = Article::where('code','=','BANKING')->where('crccode', '=', Session::get('currency'))->where('lang','=',Lang::getLocale())->pluck('content');
		}
		return view(Config::get('setting.front_path').'/banking',$data);
	}

	public function faq(Request $request){

		$data['content'] = Article::where('code','=','FAQ')->where('crccode', '=', Session::get('currency'))->where('lang','=',Lang::getLocale())->pluck('content');
		return view(Config::get('setting.front_path').'/faq',$data);
	}

	public function responsible(Request $request){

		$data['content'] = Article::where('code','=','RESPONSIBLE')->where('crccode', '=', Session::get('currency'))->where('lang','=',Lang::getLocale())->pluck('content');
		return view(Config::get('setting.front_path').'/responsible',$data);
	}

	public function forgotPassword(Request $request){



		return view(Config::get('setting.front_path').'/forgot_password');
	}

	public function maintenance(Request $request){



		return view(Config::get('setting.front_path').'/maintenance');
	}

	public function maintenance_slot(Request $request){



		return view(Config::get('setting.front_path').'/maintenance_slot');
	}

	public function mobile_slot(Request $request){


		return view(Config::get('setting.front_path').'/mobile_slot');
	}

        public function agent(Request $request){


		return view(Config::get('setting.front_path').'/agent');
	}

    public function load4dResults() {

        $data = array();

		if ( !Cache::has('4d_result') )
		{
			$types[] = 'Magnum';
			$types[] = 'PMP';
			$types[] = 'Toto';
			$types[] = '5D';
			$types[] = '6D';
			$types[] = 'Singapore';
			$types[] = 'Sabah';
			$types[] = 'Sandakan';
			$types[] = 'Sarawak';

			foreach( $types as $key => $type ){

				$value = Betpsbresult::where( 'GameName' , '=' , $type )->orderBy( 'DrawDate' , 'desc')->first();

                if ($value) {
                    $data[$value->GameName]['date']  	  	= substr($value->DrawDate,0,10);
                    $data[$value->GameName]['first']  	  	= $value->FirstPrizeNumber;
                    $data[$value->GameName]['second'] 	  	= $value->SecondPrizeNumber;
                    $data[$value->GameName]['third']  	  	= $value->ThirdPrizeNumber;
                    $data[$value->GameName]['fourth'] 	  	= $value->FourthPrizeNumber;
                    $data[$value->GameName]['fifth']  	  	= $value->FifthPrizeNumber;
                    $data[$value->GameName]['six']    	  	= $value->SixPrizeNumber;
                    $data[$value->GameName]['special']    	= explode(',',$value->SpecialPrizeNumber);
                    $data[$value->GameName]['consolation']	= explode(',',$value->ConsolationPrizeNumber);
                }

			}
			Cache::forever('4d_result', $data);
		}
		else
		{
			$data = Cache::get('4d_result');
		}

        return $data;
    }

	public function page4d(Request $request){

        $data = $this->load4dResults();

		return view(Config::get('setting.front_path').'./4d',$data);

	}

    public function json4d (Request $request) {

        $data = $this->load4dResults();

        return response(json_encode($data));
    }

	public function promotion(Request $request){

		$data['promo'] = array();

		$result = Cache::rememberForever('promotion_'.Lang::getLocale().'_'.Session::get('currency'), function() {
			return Article::where('type','=',2)->where( 'crccode' , '=' , Session::get('currency') )->where( 'lang' , '=' , Lang::getLocale())->where( 'status' , '=' , 1 )->orderBy( 'rank' , 'asc')->get();
		});
		//var_dump($result);
		foreach( $result as $key => $value ){
			$data['promo'][$key]['title']   = $value->title;
			$data['promo'][$key]['content'] = $value->content;
			$id = $value->id;

            if ($this->isMobileSite()) {
                $media = Cache::rememberForever('promotion_mobile_'.$value->id, function() use ($id) {
                    return Media::where('refObj','=','Article')->where('refid','=',$id)->where('type', '=', Media::TYPE_IMAGE_MOBILE)->first();
                });
            } else {
                $media = Cache::rememberForever('promotion_'.$value->id, function() use ($id) {
                    return Media::where('refObj','=','Article')->where('refid','=',$id)->where('type', '=', Media::TYPE_IMAGE)->first();
                });
            }

			$data['promo'][$key]['image'] = '';
			if(isset($media->domain)){
				$data['promo'][$key]['image']   = $media->domain . '/' . $media->path;
			}
		}


		return view(Config::get('setting.front_path').'/promotion',$data);
	}


	public function setLanguage(Request $request){

	    $changeLocale = new ChangeLocale($request->input('lang'));
        $this->dispatch($changeLocale);

        // Fix bug of if previous URL contain 'setlang' will cause unable to change language.
        $backUrl = redirect()->getUrlGenerator()->previous();
        if (str_contains($backUrl, array('?setlang=', '&setlang='))) {
            return redirect(preg_replace('/[\?\&]setlang=[a-zA-Z]{2}/', '', $backUrl));
        }

        return redirect()->back();

	}

	public function switchWebType(Request $request) {

	    $webType = strtolower($request->input('type')); // $webType: d = Desktop / m = Mobile / null or empty = Not Set

        $httpHost = $request->server('HTTP_HOST') . $request->server('REQUEST_URI');

        // Refer to App\Http\Middleware@handle
        $route = preg_replace('/(\/[\S]{2})(.*)/', '$1', $httpHost, 1);
        $url = explode('.', $route);
        $urlPartCount = count($url);

        if ($urlPartCount >= 3 && ($url[0] == 'www' || $url[0] == 'm')) {
            if ($webType == 'm') {
                $url[0] = 'm';
            } else {
                $url[0] = 'www';
            }

            $route = implode('.', $url);
        } elseif ($urlPartCount == 2) {
            if ($webType == 'm') {
                $route = 'm.' . implode('.', $url);
            } else {
                $route = 'www.' . implode('.', $url);
            }
        } else {
            // Should not happen.
            return redirect()->route('homepage');
        }

        if (str_contains($route, '?')) {
            $route .= '&';
        } else {
            $route .= '?';
        }

        // Follow app\Http\Middleware\Currency@handle
        $route .= 'refcode=' . Session::get('user_refcode') . '&channel=' . Session::get('user_channel') . '&utm_source=' . Session::get('user_channel');

        if (count($url) >= 3) {
            $domain = $url[1].'.'.$url[2];
        } else {
            $domain = $url[0].'.'.$url[1];
        }

        return redirect()->to('//' . $route)->withCookie(cookie('webtype', $webType, 1440, null, '.'.$domain));
    }

	public function omt(Request $request){

	   $data['url'] = '';




	   if(Session::has('username')){

		    $key = MD5( '11s_'.strtoupper(Session::get('username')).'h978S9gsO09s9o' );
			$data['url'] = 'http://follow178.com/infinicny/game2?gid=11&userhash=s_'.strtoupper(Session::get('username')).'&key='.$key;

	   }

	   return view(Config::get('setting.front_path').'/omt',$data);

	}

	public function luckySpin(Request $request) {
	if(Config::get('setting.opcode') == "RYW"){
	    if (Session::has('username')) {
            $accObj = Account::where('nickname', '=', Session::get('username'))
                ->where('crccode', '=', Session::get('user_currency'))
                ->first();

            if ($accObj) {
                $otObj = Onlinetracker::where('userid', '=', $accObj->id)
                    ->where('username', '=', $accObj->nickname)
                    ->where('type', '=', 1)
                    ->first();
                return redirect()->away('http://esgame.rwbola.com/royalewin/games/royalewheel/index.html?sid=' . $otObj->token . '&crc=' . Session::get('user_currency'));
            }
        }else{
			return redirect()->route('homepage');
		}
	}else{
		return redirect()->route('homepage');
	}
        // abort(404);
    }
    
    public function launchMinigame(Request $request){
        $date = json_decode(Configs::getParam('SYSTEM_MINIGAME_DATETIME'));
        
        $startdate = Carbon::parse($date->startdate)->format('Y-m-d H:i:s');
        $enddate = Carbon::parse($date->enddate)->format('Y-m-d H:i:s');
        
        if(Carbon::now() >= $startdate && Carbon::now() <= $enddate){
            echo '1';
        }else{
            echo '0';
        }
    }
    
    public function Minigame(Request $request){
	if(Config::get('setting.front_path') == "front" || Config::get('setting.front_path') == "front/mobile"){
	    if (Session::has('username')) {
                $accObj = Account::where('nickname', '=', Session::get('username'))
                    ->where('crccode', '=', Session::get('user_currency'))
                    ->first();

                if ($accObj) {
                    $otObj = Onlinetracker::where('userid', '=', $accObj->id)
                        ->where('username', '=', $accObj->nickname)
                        ->where('type', '=', 1)
                        ->first();
                    
                    $token = md5($accObj->nickname.$otObj->token.$accObj->id);
                    
                    return redirect()->away('http://minigame.infiniwin.info/drop_and_grab.php?token='.$token.'&sid='.$accObj->id);
                }
            }else{
                return redirect()->route('homepage');
            }
	}else{
		return redirect()->route('homepage');
	}       
    }

    public function premierLeague(Request $request) {

	    // Only for Royalewin MYR
	    if (Session::has('username') && Config::get('setting.opcode') == 'RYW' && Session::get('currency') == 'MYR') {
            if ($request->isJson() || $request->wantsJson()) {
                if ($request->input('_act') == 'save') {
                    try {
                        // Check user has already bet or not.
                        $matchId = Crypt::decrypt($request->input('bet_match'));

                        $matchObj = PremierLeagueMatch::where('id', '=', $matchId)
                            ->where('status', '=', PremierLeagueMatch::STATUS_OPENING)
                            ->where('bet_closed_at', '>', Carbon::now())
                            ->first();

                        if (!$matchObj) {
                            return response()->json(array('code' => 0, 'msg' => 'Invalid match, please refresh page and try again.'));
                        }

                        // Check member available token.
                        $availableToken = PremierLeagueToken::getAvailableTokenAmount(Session::get('userid'), Session::get('user_currency'), $matchObj->id);

                        if ($availableToken < 0) {
                            return response()->json(array('code' => 0, 'msg' => 'Bet rejected. You have 0 chance to place bet.'));
                        }

                        $validator = \Validator::make([
                            'bet_a' => $request->input('bet_a'),
                            'bet_b' => $request->input('bet_b'),
                        ], [
                            'bet_a' => 'required|integer|min:0|max:99',
                            'bet_b' => 'required|integer|min:0|max:99',
                        ]);

                        $validator->addCustomAttributes([
                            'bet_a' => 'Bet',
                            'bet_b' => 'Bet',
                        ]);

                        if ($validator->passes()) {
                            PremierLeagueBet::forceCreate(array(
                                'match_id' => $matchObj->id,
                                'accid' => Session::get('userid'),
                                'crccode' => Session::get('user_currency'),
                                'nickname' => Session::get('username'),
                                'bet_a' => $request->input('bet_a'),
                                'bet_b' => $request->input('bet_b'),
                                'status' => PremierLeagueBet::STATUS_PENDING,
                                'created_at' => Carbon::now(),
                                'updated_at' => Carbon::now(),
                            ));

                            return response()->json(array('code' => 1, 'msg' => 'Bet submitted successfully.'));
                        }

                        return response()->json(array('code' => 0, 'msg' => implode(PHP_EOL, $validator->errors()->all())));
                    } catch (\Exception $e) {
                        // Do nothing.
                    }
                }

                return response()->json(array(), 404);
            }

            $data = array(
                'hasMatchCurrently' => false,
                'bgImagePath' => asset('/royalewin/img/pl-match-bg.png'),
                'preWeek' => '',
                'preTeamAName' => '-',
                'preTeamBName' => '-',
                'preTeamAScore' => '',
                'preTeamBScore' => '',
                'preWinnerTeamName' => '',
                'winnerList' => array(),
                'betWeek' => '',
                'betTeamAName' => '-',
                'betTeamBName' => '-',
                'betClosingDate' => '',
                'betOpening' => false,
                'betMatchId' => '',
                'betTokenAmount' => 0,
                'betHistory' => array(),
            );

            // Get previous match.
            $matchObj = PremierLeagueMatch::getPreviousMatch();

            if ($matchObj) {
                // Previous match found.
                //$data['preWeek'] = 'WEEK '.$matchObj->week_no;
                $data['preWeek'] = $matchObj->week_no;
                $data['preTeamAName'] = strtoupper($matchObj->team_a_name);
                $data['preTeamBName'] = strtoupper($matchObj->team_b_name);
                $data['preTeamAScore'] = str_pad($matchObj->team_a_score, 2, 0, STR_PAD_LEFT);
                $data['preTeamBScore'] = str_pad($matchObj->team_b_score, 2, 0, STR_PAD_LEFT);

                if ($matchObj->team_a_score == $matchObj->team_b_score) {
                    $data['preWinnerTeamName'] = 'DRAW';
                } elseif ($matchObj->team_a_score > $matchObj->team_b_score) {
                    $data['preWinnerTeamName'] = strtoupper($matchObj->team_a_name);
                } else {
                    $data['preWinnerTeamName'] = strtoupper($matchObj->team_b_name);
                }

                // Load dummy winner list.
                $winnerArr = json_decode(Configs::getParam('SYSTEM_PREMIERLEAGUE_WINNER_LIST'), true);

                $winnerObj = PremierLeagueBet::where('match_id', '=', $matchObj->id)
                    ->where('status', '=', PremierLeagueBet::STATUS_WON_SELECTED)
                    ->get();

                $i = 1;

                foreach ($winnerObj as $r) {
                    if ($i > 10) {
                        break; // Only select 10 winners enough.
                    }

                    $winnerArr['w'.$i] = str_limit($r->nickname, 5, '***');
                    $i++;
                }

                $data['winnerList'] = $winnerArr;
            }

                // Load UpComing Match
                $upComingMatchs = json_decode(Configs::getParam('SYSTEM_PREMIERLEAGUE_UPCOMING_MATCH'));
                foreach($upComingMatchs as $key => $upComingMatch){
                    $matchArr[$key] = explode(',',$upComingMatch);
                }
                $data['UpcomingMatch'] = $matchArr;

            // Get current match.
            $matchObj = PremierLeagueMatch::getCurrentOpeningMatch();

            if ($matchObj) {
                // Has current match.
                $bgPath = $matchObj->getBackgroundImage();

                if ($bgPath && strlen($bgPath) > 0) {
                    $data['bgImagePath'] = $bgPath;
                }

                $data['hasMatchCurrently'] = true;
                //$data['betWeek'] = 'WEEK '.$matchObj->week_no;
                $data['betWeek'] = $matchObj->week_no;
                $data['betTeamAName'] = strtoupper($matchObj->team_a_name);
                $data['betTeamBName'] = strtoupper($matchObj->team_b_name);
                $data['betClosingDate'] = Carbon::parse($matchObj->bet_closed_at)->format('d/m/Y h:i A');
                $data['betTokenAmount'] = PremierLeagueToken::getAvailableTokenAmount(Session::get('userid'), Session::get('user_currency'), $matchObj->id);

                // Check bet placed by this user.
                $betObj = PremierLeagueBet::where('match_id', '=', $matchObj->id)
                    ->where('accid', '=', Session::get('userid'))
                    ->where('crccode', '=', Session::get('user_currency'))
//                    ->where('status', '!=', PremierLeagueBet::STATUS_VOID)
                    ->get();

                $betHistoryList = array();
                $i = 1;

                $statusOptions = collect(PremierLeagueBet::getStatusOptions());

                foreach ($betObj as $r) {
                    $betHistoryList[] = array(
                        'no' => $i++,
                        'bet_a' => $r->bet_a,
                        'bet_b' => $r->bet_b,
                        'status' => $statusOptions->get($r->status),
                        'created_at' => $r->created_at,
                    );
                }

                $data['betHistory'] = $betHistoryList;

                if ($data['betTokenAmount'] > 0) {
                    $data['betOpening'] = true;
                    $data['betMatchId'] = Crypt::encrypt($matchObj->id);
                }
            }

	        return view(Config::get('setting.front_path').'/premier_league', $data);
        }

	    abort(404);
    }

	public function claimbonus(Request $request){


		if(  Account::whereRaw('nickname = "'.$request->username.'"')->count() != 0 && $request->opcode == "IFW" && $request->auth == '7E47C6131DEF' )
		{

			if( $pcpObj = Promocampaign::whereRaw(' code = "'.$request->bonuscode.'" ')->first() )
			{
				$accObj = Account::whereRaw('nickname = "'.$request->username.'"')->first();
				$promoCashObj = new Promocash;
				$promoCashObj->type			= Promocash::TYPE_MANUAL;
				$promoCashObj->accid	 	= $accObj->id;
				$promoCashObj->acccode	 	= $accObj->code;
				$promoCashObj->crccode	 	= $accObj->crccode;
				$promoCashObj->crcrate	 	= Currency::getCurrencyRate($accObj->crccode);
				$promoCashObj->amount	 	= $request->amount;
				$promoCashObj->amountlocal 	= Currency::getLocalAmount($request->amount, $accObj->crccode);
				$promoCashObj->pcpid		= $pcpObj->id;
				$promoCashObj->status		= Promocash::STATUS_PENDING;

				if($promoCashObj->save()) {

					$cashLedgerObj = new Cashledger;
					$cashLedgerObj->accid 			= $promoCashObj->accid;
					$cashLedgerObj->acccode 		= $promoCashObj->acccode;
					$cashLedgerObj->accname 		= $accObj->fullname;
					$cashLedgerObj->chtcode 		= CBO_CHARTCODE_BONUS;
					$cashLedgerObj->amount 			= $promoCashObj->amount;
					$cashLedgerObj->amountlocal 	= $promoCashObj->amountlocal;
					$cashLedgerObj->fee 			= $promoCashObj->amount;
					$cashLedgerObj->feelocal 		= $promoCashObj->amountlocal;
					$cashLedgerObj->crccode 		= $promoCashObj->crccode;
					$cashLedgerObj->crcrate 		= $promoCashObj->crcrate;
					$cashLedgerObj->refobj			= 'Promocash';
					$cashLedgerObj->refid			= $promoCashObj->id;
					$cashLedgerObj->status			= CBO_LEDGERSTATUS_PENDING;
					$cashLedgerObj->save();

					echo json_encode( array( 'status' => 'Success' ) );
					exit;
				} else{
					echo json_encode( array( 'status' => 'Fail' ) );
					exit;
				}
			}
			echo json_encode( array( 'status' => 'Fail' ) );
			exit;

		}
		echo json_encode( array( 'status' => 'Fail' ) );
		exit;
	}

	public function fortune(Request $request){


		return view(Config::get('setting.front_path').'/fortune');
	}

    public function livefootball(Request $request){


		return view(Config::get('setting.front_path').'/livefootball');
	}

	public function clolotto(Request $request){

        $data['drawdate'] = Article::where('code','=','LOTTODATE')->where('lang','=',Lang::getLocale())->pluck('content');

                if ( !Cache::has('clo_result') )
		{
                    $condition = '1';
                    if($value = Betcloresult::whereRaw($condition)->orderBy( 'drawdate' , 'desc')->first()){

						$data['date']  	  	= $value->drawdate;
						$data['sixtop']  	  	= $value->sixtop;
						$data['threebottom'] 	  	= str_replace(',', '',$value->threebottom);
						$data['twobottom']  	  	= $value->twobottom;
					}
                    //Cache::forever('clo_result', $data);
		}
		else
		{
			$data = Cache::get('clo_result');
		}

		return view(Config::get('setting.front_path').'.lotto',$data);

	}

	public function promotionContent(Request $request){


		return view(Config::get('setting.front_path').'/promotion_content', [ 'artcileObj' => Article::find($request->id) ] );

	}

	public function doGetContent(Request $request){


		echo Article::whereId($request->id)->pluck('content');

	}


	public function androidApk(Request $request){

		$apklog = new Apklog;
		$apklog->ip = App::getRemoteIp();
		$apklog->save();
		$opcode = Config::get('setting.opcode');

		if ($opcode == 'IFW') {
            return redirect()->away('http://infiniwin.net/front/mobile/android.apk');
        } elseif ($opcode == 'RYW') {
            return redirect()->away('/royalewin/mobile/android.apk');
        }

		abort(404);

	}

	public function login(Request $request){


		return view(Config::get('setting.front_path').'.login');

	}

	public function register_tutorial(Request $request){


		return view(Config::get('setting.front_path').'.tutorial_register');

	}

	public function deposit_tutorial(Request $request){


		return view(Config::get('setting.front_path').'.tutorial_deposit');

	}

	public function withdraw_tutorial(Request $request){


		return view(Config::get('setting.front_path').'.tutorial_withdraw');

	}

	public function transfer_tutorial(Request $request){


		return view(Config::get('setting.front_path').'.tutorial_transfer');

	}

	public function sbo_tutorial(Request $request){


		return view(Config::get('setting.front_path').'.tutorial_sbobet');

	}

	public function android(Request $request){


		return view(Config::get('setting.front_path').'.android');

	}

	public function delayedDownload(Request $request) {
	    $url = $request->input('url');

	    // No worry about design for now.
        return view('front.delayed_download', compact('url'));
    }

    public function drawQR(Request $request) {
	    $url = $request->input('url');
	    $label = $request->input('lbl');

        return view(Config::get('setting.front_path').'.drawqr', compact('url', 'label'));
    }

	public function yearendpromotion(Request $request) {

		$data['article'] = Article::where('code','=','YEAR_END_PROMO')->pluck('content');

        return view(Config::get('setting.front_path').'.promotion_year_end',$data);
    }
    
    public function xmasPromotion(Request $request) {
        if(Session::get('locale') == 'en'){
            return view(Config::get('setting.front_path').'.christmas_en');
        }else{
            return view(Config::get('setting.front_path').'.christmas_cn');
        }
        
    }
}
