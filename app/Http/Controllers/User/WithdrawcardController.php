<?php namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller\User;
use App\Models\Agent;
use App\Models\Fundingmethodsetting;
use App\Models\Configs;
use App\Models\Bankaccount;
use App\Models\Accounttag;
use App\Models\Tag;
use App\Models\Promocampaign;
use App\Models\Media;
use App\Models\Fundingmethod;
use App\Models\Bank;
use App\Models\Product;
use App\Models\Cashledger;
use App\Models\Article;
use App\Models\Banktransfer;
use App\Models\Currency;
use App\Models\Promocash;
use App\Models\Accountbank;
use App\Models\Bankledger;
use App\Models\Wager;
use App\Models\Account;
use App\Models\Withdrawcard;
use App\Models\Affiliate;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\libraries\App;
use Session;
use Lang;
use Validator;
use Storage;
use File;
use Cache;
use Config;
use Auth;
use Redirect;
use DB;
use Log;
use App\Jobs\ChangeLocale;



class WithdrawcardController extends Controller{
	
	public function __construct()
	{
		
	}
	
	public function withdrawcardProcess(Request $request){
		
		$inputs = [
            'amount'     => $request->input('withdrawcard_amount'),
            'parentid'   => $request->input('parentid'),
        ];

		$rules = [
            'amount'     => 'required|numeric',
            'parentid'   => 'required|numeric',
        ];

		
        $validator = Validator::make($inputs, $rules);

		
		if ($validator->fails())
		{
			echo json_encode($validator->errors());
			exit;
		}
		
		if( $request->input('withdrawcard_amount') > WalletController::mainwallet(false) ) {
			echo json_encode( array( 'amount' => Lang::get('COMMON.ERROR_NETAMOUNT') ) );
			exit;
		}
		
		if($request->input('amount') > 0) 
		{
			$from = date('Y-m-d').' 00:00:00';
			$to   = date('Y-m-d').' 23:59:59';
			$withdrawl_count = Cashledger::whereRaw('accid = '.Session::get('userid').' AND chtcode = '.CBO_CHARTCODE_WITHDRAWAL.' AND created >= "'.$from.'" AND created <= "'.$to.'" AND status NOT IN ('.CBO_LEDGERSTATUS_CANCELLED.', '.CBO_LEDGERSTATUS_MANCANCELLED.', '.CBO_LEDGERSTATUS_MEMCANCELLED.')')->count();
			if($withdrawl_count >= Session::get('withdrawlimit')) {
				echo json_encode( array( 'code' => Lang::get('COMMON.ERROR_ALLOWWITHDRAWAL') ) );
				exit;
			}
		}
		
		$continue = true;
		
		while($continue)
		{
			$number = App::generateNum(8);
			if( Withdrawcard::where( 'number' , '=' , $number )->count() >= 1 ){
				$continue = true;
			}else{
				$continue = false;
			}
		}
		
		$withdrawcard = new Withdrawcard;
		$withdrawcard->parentid 	= $request->input('parentid');
		$withdrawcard->aflid 		= '';
		$withdrawcard->accid 		= Session::get('userid');
		$withdrawcard->crccode  	= Session::get('currency');
		$withdrawcard->crcrate  	= 1;
		$withdrawcard->amount  		= $request->input('withdrawcard_amount');
		$withdrawcard->number  		= $number;
		$withdrawcard->clgid  		= 0;
		$withdrawcard->status  		= 0;
		
		if($withdrawcard->save())
		{
			
			$account = Account::find($withdrawcard->accid);
			
			$cashLedgerObj = new Cashledger;
			$cashLedgerObj->accid 			= $withdrawcard->accid;
			$cashLedgerObj->acccode 		= $account->code;
			$cashLedgerObj->accname			= Session::get('fullname');
			$cashLedgerObj->chtcode 		= CBO_CHARTCODE_WITHDRAWAL;
			$cashLedgerObj->cashbalance 	= self::getAllBalance();
			$cashLedgerObj->amount 			= $withdrawcard->amount * -1;
			$cashLedgerObj->amountlocal 	= $withdrawcard->amount * -1;
			$cashLedgerObj->refid 			= $withdrawcard->id;
			$cashLedgerObj->status 			= CBO_LEDGERSTATUS_PENDING;
			$cashLedgerObj->refobj 			= 'Withdrawcard';
			$cashLedgerObj->crccode 		= $withdrawcard->crccode;
			$cashLedgerObj->crcrate 		= Currency::getCurrencyRate($withdrawcard->crccode);
			$cashLedgerObj->fdmid			= 3;
			$cashLedgerObj->fee 			= 0;
			$cashLedgerObj->feelocal 		= 0;
			$cashLedgerObj->createdip		= App::getRemoteIp();
			$cashLedgerObj->save();
		}
			
		echo json_encode( array( 'code' => Lang::get('COMMON.SUCESSFUL') ) );
		
	}
	
	public static function getAllBalance(){
		
		 $total = 0;
		
		$productObj = Product::select(['code','status'])->get();
		
	    foreach( $productObj as $object )
		{
			$total += Cache::has(Session::get('username').'_'.strtoupper($object->code).'_balance') ? (float)Cache::get(Session::get('username').'_'.strtoupper($object->code).'_balance') : 0;
		}  
	 


		
		$total += WalletController::getBalance(Session::get('userid'));
		
		return $total;
	}
	
	public function getAllBranch(){
		
		$branchs = array();
		
		if( $affiliates = Affiliate::whereWithdrawal(1)->whereType(1)->whereParentid(0)->get() )
		{
			foreach( $affiliates as $affiliate )
			{
				$branchs[$affiliate->id] = $affiliate->name;
			}
			
		}
		
		echo json_encode( $branchs );

	}


	
}
