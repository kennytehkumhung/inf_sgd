<?php namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller\User;
use App\Models\Announcement;
use Cache;
use Config;
use Illuminate\Http\Request;
use Lang;


class AnnouncementController extends Controller {


	public static function index() {

        echo implode('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;', (array) self::getAnnouncement());
	}

    public function getList(Request $request) {

        $data['announcement'] = (array) self::getAnnouncement($request->input('lang', 'en'));

        return view(Config::get('setting.front_path') . '/announcement', compact('data'));
    }

    private static function getAnnouncement($lang = null) {

	    if (is_null($lang)) {
            $lang = Lang::getLocale();
        }

	    Cache::forget('Announcement_' . $lang);

        return Cache::rememberForever('Announcement_' . $lang, function() use ($lang) {
            $arr = array();
            $column = 'body' . $lang;

            $announcements = Announcement::where('status', '=', 1)->get();

            foreach($announcements as $announcement){
                $content = $announcement->{$column};

                if (strlen($content) > 0) {
                    $arr[] = $content;
                }
            }

            return $arr;
        });
    }

}
