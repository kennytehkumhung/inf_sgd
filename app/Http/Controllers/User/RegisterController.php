<?php namespace App\Http\Controllers\User;

use App\libraries\sms\SMS88;
use App\libraries\sms\SMSMoDing;
use App\libraries\sms\BulkSMS;
use App\Models\Smslog;
use Carbon\Carbon;
use App\Http\Controllers\Controller\User;
use App\Models\Account;
use App\Models\Accountdetail;
use App\Models\Affiliate;
use App\Models\Agent;
use App\Models\Article;
use App\Models\Website;
use App\Models\Configs;
use App\Models\Channel;
use App\Models\Currency;
use Illuminate\Http\Request;
use App\libraries\App;
use Hash;
use Cache;
use Config;
use Lang;
use Crypt;
use Session;
use Mail;
use Validator;



class RegisterController extends Controller{

	public function __construct()
	{

	}

	public function index(Request $request){

	    $data = array(
	        'showSMSField' => str_contains(Configs::getParam('SYSTEM_SMS_ONREGISTER'), 'MYR'),
        );

		return view(Config::get('setting.front_path').'/register', $data);
	}

	public function submit(Request $request)
	{
		if( $request->has('landing')){
			 $channel='landing-page';
		$opcode = 'RYW';
		$currency = 'MYR';
		$rules = [

                'username' => 'required|min:6|max:12|alpha_num|reserved_str|unique:account,nickname,NULL,id,crccode,'.Session::get('currency'),
                'password' => 'required|alpha_num|min:6|max:12',
                'email'    => 'required|email|unique:accountdetail,email',
                'mobile'   => 'required|phone|unique:accountdetail,telmobile',
                'fullname' => 'required|min:2',
                'referralid' => 'integer|min:1|exists:account,id',
            ];

		    if (Session::get('currency') == 'MYR') {
		        $rules['fullname'] = 'required|min:2|unique:account,fullname';

		        if (str_contains(Configs::getParam('SYSTEM_SMS_ONREGISTER'), 'MYR')) {
                    $rules['mobile_vcode'] = 'required|min:6';
                }
            }

			$validator = Validator::make(
				[
					'username' => $request->input('username'),
					'password' => $request->input('password'),
					'email'	   => $request->input('email'),
					'mobile'   => $request->input('mobile'),
					'fullname' => $request->input('fullname'),
                    'referralid' => $request->input('referralid'),
                    'mobile_vcode' => $request->input('mobile_vcode'),
				],
                $rules
			);
			$validator->setAttributeNames([
			    'referralid' => Lang::get('public.Referral', [], 'en'),
                'mobile_vcode' => Lang::get('public.VerificationCode', [], 'en'),
            ]);
			if ($validator->fails())
			{
			echo json_encode($validator->errors());
			exit;
			}

		}else{
		$this->middleware('Register');
		$currency = Session::get('currency');
		$opcode   = Session::get('opcode');
		}

		$isPhoneValid = 0;
        $smslogObj    = null;
        if (strlen($request->input('mobile_vcode')) > 1) {
            // App\Http\Middleware\Register.php will detect if project is using r_mobile_vcode
            // Here just simply check if request has r_mobile_cvode then only verify the code.
            $mobileTel = $request->input('mobile');

            if (starts_with($mobileTel, '0')) {
                $mobileTel = substr($mobileTel, 1);
            }

            $smslogObj = Smslog::where('ref_code', '=', trim($request->input('mobile_vcode')))
                ->where('tel_mobile', 'like', '%' . $mobileTel)
                ->where('created_at', '>=', Carbon::now()->addMinutes(-30)->toDateTimeString()) // Each verify code available for 30 minutes.
                ->where('method', '=', 'regcode')
                ->where('ref_status', '=', 0)
                ->first();

            if ($smslogObj) {
                // Code is found.
                $smslogObj->ref_status = 1;
                $isPhoneValid = 1;
            } else {
                // Code not found or expired.
                return json_encode(array('mobile_vcode' => Lang::get('public.InvalidVerificationCode')));
            }
        }


        if ($request->has('referralid')) {
            $refaccObj = Account::where('id', '=', $request->input('referralid'))
                ->where('crccode', '=', $currency)
                ->first();

            if (!$refaccObj) {
                return json_encode(array('referralid' => Lang::get('public.InvalidReferralID')));
            }

            unset($refaccObj);


		}

		$refcode = true;

	 	$website = Website::where( 'code' , '=' , $opcode )->first();
		if( Session::has('user_refcode'))
		{
			$refcode = Session::get('user_refcode');
			if( Affiliate::where( 'code' , '=' , Session::get('user_refcode') )->count() == 0 ){
				$refcode = false;
			}else{
				$channel  = 'affiliate';
			}

		}
		elseif ($request->has('referralcode'))
        {
            // Same logic as above.
            $refcode = $request->input('referralcode');
            if( Affiliate::where( 'code' , '=' , $request->input('referralcode') )->count() == 0 ){
                return json_encode(array('referralcode' => Lang::get('public.InvalidReferralID')));
            }else{
                $channel  = 'affiliate';
            }
        }
		else
		{
			$refcode = false;
		}

		if( Session::has('user_affname') )
		{

			if( Affiliate::where( 'username' , '=' , Session::get('user_affname') )->count() == 1 )
			{
				$refcode = Affiliate::where( 'username' , '=' , Session::get('user_affname') )->pluck('code');
				$channel  = 'affiliate';
			}

		}
		$magtcode = $website->code.'_'.$currency.(($refcode)?'_AFF':'_COM');
		$agtcode  = $magtcode.'_'.($refcode?strtoupper($refcode):date('ym'));

		if(isset($request->channel) && strlen($request->channel) > 0) {
			$channel = $request->channel;
		}

		if( !isset($channel) )
		{
			$channel  = Session::has('user_channel') ? Session::get('user_channel') : 'Walk-in';
		}



		if( Channel::where( 'name' , '=' , $channel )->count() == 0 ) {
			$chnObj = new Channel;
			$chnObj->name = $channel;
			$chnObj->save();
		}

		$agtObj = Agent::where( 'code' , '=' , $agtcode )->first();

		$aflid = 0;

		if( $agtObj == false )
		{

			$parentid = 0;
			if( $magtObj = Agent::where( 'code' , '=' , $magtcode )->first() )
			$parentid 			= $magtObj->id;

			if( $channel == 'affiliate' ){
			    if ($request->has('referralcode')) {
                    $aflid = Affiliate::where( 'code' , '=' , $request->input('referralcode') )->pluck('id');
                } else {
                    $aflid = Affiliate::where( 'code' , '=' , Session::get('user_refcode') )->pluck('id');
                }
			}

			$agtObj  = new Agent;
			$agtObj->code 		= $agtcode;
			$agtObj->parentid   = $parentid;
			$agtObj->aflid 		= $aflid;
			$agtObj->wbsid 		= $website->id;
			$agtObj->bhdid 		= $magtObj->bhdid;
			$agtObj->level 		= Agent::LEVEL_AGENT;
			$agtObj->crccode 	= strtoupper($currency);
			$agtObj->isexternal = ($refcode)?1:0;
			$agtObj->status 	= CBO_AFFILIATESTATUS_ACTIVE;
			$agtObj->save();

		}

		$accObj = new Account;
		$acdObj = new Accountdetail;

		$chnObj = Channel::where( 'name' , '=' , $channel )->first();

		if($aflid!=0){
			$accObj->regchannel    =  2;
		}else{
			$accObj->regchannel    = $refcode? 2 : $chnObj->id;
		}
		$accObj->nickname      = strtolower( $request->input('username') );
		$accObj->code	       = strtolower( $website->memberprefix . '_' . $request->input('username') );
		$accObj->fullname      = $request->has('fullname') ? $request->input('fullname') : '';
		$accObj->password      = Hash::make( $request->input('password') );
		$accObj->enpassword    = Crypt::encrypt( $request->input('password') );
		$accObj->crccode       = $currency;
		$accObj->term  	       = CBO_ACCOUNTTERM_CASH;
		$accObj->timezone      = Configs::getParam("SYSTEM_TIMEZONE");
		$accObj->createdby     = 0;
		$accObj->isphonevalid  = $isPhoneValid;
		$accObj->islogin   	   = 1;
		$accObj->withdrawlimit = Configs::getParam("SYSTEM_ACCOUNT_WITHDRAWLIMIT");
		$accObj->lastpwdchange = date('Y-m-d H:i:s');
		$accObj->status 	   = CBO_ACCOUNTSTATUS_ACTIVE;
		$accObj->aflid 		   = (Config::get('setting.front_path') == 'sbm' && $agtObj->aflid == 0) ? 4  : $agtObj->aflid;
		$accObj->agtid 		   = (Config::get('setting.front_path') == 'sbm' && $agtObj->aflid == 0) ? 10 : $agtObj->id;

		if (Config::get('setting.opcode') == 'GSC' && $accObj->aflid == 0 )
		{
			$location = App::taobao_ip(App::getRemoteIp());


			if(isset($location->data->city)&&$location->data->city!='')
			{

				$acdObj->rescity  = $location->data->city;

				if(isset($location->data->region))
				{
					$acdObj->resstate = $location->data->region;
				}

				if(  $affObj = Affiliate::where( 'city' , '=' , $location->data->city)->first() )
				{
					if(isset($affObj->id))
					{
						$accObj->aflid 		   = $affObj->id;
						$accObj->regchannel    = 2;
						$accObj->agtid 		   = Agent::whereAflid($affObj->id)->pluck('id');
					}
				}
			}
			elseif(isset($location->data->region)&&$location->data->region!='')
			{

				$acdObj->resstate = $location->data->region;

				if(  $affObj = Affiliate::where( 'province' , '=' , $location->data->region)->first() )
				{
					if(isset($affObj->id))
					{
						$accObj->aflid		   = $affObj->id;
						$accObj->regchannel    = 2;
						$accObj->agtid 		   = Agent::whereAflid($affObj->id)->pluck('id');
					}
				}
			}
		}

		$accObj->referralid    = $request->input('referralid', 0);
		$accObj->wbsid 		   = $website->id;
		if (Config::get('setting.front_path') == 'lasvegas'){
			$accObj->isupdatepassword    = '1';
		}

		if( Session::has('utm_medium') )
		{
			$accObj->utm_medium = Session::get('utm_medium');
		} else {
			if(isset($request->utm_medium) && strlen($request->utm_medium) > 0) {
				$accObj->utm_medium = $request->utm_medium;
			}
		}

		/* if( $_SERVER['HTTP_HOST'] == 'rwinvip.com' ){
			$accObj->regchannel = 2;
			$accObj->aflid		= 7;
			$accObj->agtid 		= 26;
		} */

		if($accObj->save())
		{
            if ($smslogObj && $isPhoneValid == 1) {
                $smslogObj->save();
            }

			$acdObj->fullname 	   = $request->has('fullname') ? $request->input('fullname') : '';
			$acdObj->email 		   = $request->has('email') ? $request->input('email') : '';
			$acdObj->telmobile 	   = $request->input('mobile');
			$acdObj->dob		   = $request->has('dob') ? $request->input('dob'): '0000-00-00';
            $acdObj->line		   = ($request->has('line')) ? $request->input('line') : '';
            $acdObj->wechat		   = ($request->has('wechat')) ? $request->input('wechat') : '';
			$acdObj->gender		   = ($request->has('gender')) ? $request->input('gender') : '';
            $acdObj->verifyno      = App::generatePassword();
			$acdObj->createdip	   = App::getRemoteIp();
			$acdObj->id 		   = $accObj->id;
			$acdObj->accid 		   = $accObj->id;
			$acdObj->wbsid 		   = $accObj->wbsid;
			$acdObj->acccode 	   = $accObj->code;
			$acdObj->referenceno   = 1000 + $accObj->id;
			$acdObj->save();
		}
                if (Config::get('setting.front_path') == 'ampm' || Config::get('setting.front_path') == 'dadu' || Config::get('setting.opcode') == 'LVG'){
                    if( $atcObj = Article::where( 'code' , '=' , 'WELCOME_EN' )->first() ) {
                            $content = str_replace('&nbsp;', ' ', html_entity_decode($atcObj->content));
                            $content = str_replace('%username%', $request->input('username') , $content);
                            $content = str_replace('%verify%', $acdObj->verifyno, $content);
                            $status = Mail::raw(html_entity_decode($content), function ($message) use ($atcObj,$acdObj)
                            {
                                    if(Config::get('setting.front_path') == 'ampm') $from = 'admin@ampmplay.com';
                                    else $from = 'admin@dadukembar.com';

                                    $message->to($acdObj->email);
                                    $message->from($from);
                                    $message->subject($atcObj->title);
                            });

//                            if($status){
//                                    echo json_encode( array( 'code' => Lang::get('COMMON.SUCESSFUL') ) );
//                            }
                    }
                }

                echo json_encode( array( 'code' => Lang::get('COMMON.SUCESSFUL') ) );

	}

	public function indexasd(Request $request)
	{
		echo Account::where( 'id' , '=' , Session::get('userid') )->
				 update(
					array(
						'password'       => Hash::make( $request->input('newpass') ),
						'lastpwdchange'  => date('Y-m-d H:i:s') ,
						'resetpass'      => 0 ,
					)
				 );

	}

    public function sendRegistrationSms(Request $request) {

	    Lang::setLocale($request->input('language', 'en'));
	    $supportedRegion = array('MYR', 'TWD');

	    // Only enable for MYR currency.
        if (!in_array(Session::get('currency'), $supportedRegion)) {
            return response()->json(array(
                'code' => 1,
                'msg' => Lang::get('public.UnsupportedRegion')
            ));
        }

        // SMS config.
        $smsMethod = 'regcode';

        $error = Lang::get('validation.regex', array('attribute' => Lang::get('public.ContactNumber')));
        $telmobile = $request->tel_mobile;

        if (Session::get('currency') == 'MYR') {
            if (!starts_with($telmobile, '01')) {
                return response()->json(array(
                    'code' => 2,
                    'msg' => Lang::get('public.ContactNumberMustStartsWithX', array('p1' => '01')),
                ));
            }
        } elseif (Session::get('currency') == 'TWD') {
            if (!starts_with($telmobile, '09')) {
                return response()->json(array(
                    'code' => 2,
                    'msg' => Lang::get('public.ContactNumberMustStartsWithX', array('p1' => '09')),
                ));
            }
        } else {
            // Should not happen.
            return response()->json(array(
                'code' => 1,
                'msg' => Lang::get('public.UnsupportedRegion'),
            ));
        }

        // Check contact number format.
        if (preg_match('/\d{9,}/', $telmobile) == 1) {
            // Check if number is in use.
            $obj = Accountdetail::where('telmobile', '=', $telmobile)->first();

            if (Session::get('currency') == 'TWD') {
                $smsObj = new SMSMoDing();
                $smsProvider = SMSMoDing::NAME;
            }elseif(Config::get('setting.opcode') == 'LVG'){
				$smsObj = new BulkSMS();
				$smsProvider = BulkSMS::NAME;
			}elseif(Config::get('setting.opcode') == 'RYW') {
                $smsObj = new SMS88();
                $smsProvider = SMS88::NAME;
            }
			if ($obj) {
                $error = Lang::get('public.ContactNumberIsAlreadyInUse');
            }else {
                // Add country code to tel number.
                $telmobile = $smsObj->formatTel(Session::get('currency'), $telmobile);

                // Check IP spam.
                $requestCount = Smslog::where('sender_ip', '=', $request->ip())
                    ->where('created_at', '>=', Carbon::now()->addMinutes(-60)->toDateTimeString())
                    ->where('method', '=', $smsMethod)
//                    ->where('ref_status', '=', '0') // Strict only 3 SMS within # seconds
                    ->count();

                if ($requestCount >= 3) {
                    // IP has requested # of times on last minutes, consider as spam.
                    $error = Lang::get('public.PleaseWaitForXSecondsToResendSMS', array('p1' => 60));
                } else {
                    // IP is safe.
                    // Check latest user request for verify code.
                    $smslogObj = Smslog::where('tel_mobile', '=', $telmobile)
                        ->where('method', '=', $smsMethod)
                        ->where('ref_status', '=', '0')
                        ->orderBy('created_at', 'desc')
                        ->first();

                    if ($smslogObj) {
                        // Has record found, check if send within period to prevent SMS spam.
                        $sendDt = Carbon::parse($smslogObj->created_at);
                        $resendSecLimit = 60;
                        $sentSecInterval = $sendDt->diffInSeconds(Carbon::now());

                        if ($sentSecInterval >= $resendSecLimit) {
                            // Is ok to send SMS.
                            $error = '';
                        } else {
                            $error = Lang::get('public.PleaseWaitForXSecondsToResendSMS', array('p1' => ($resendSecLimit - $sentSecInterval)));
                        }
                    } else {
                        // No record found, is ok to send SMS.
                        $error = '';
                    }
                }
            }

		if(Config::get('setting.opcode') == 'LVG'){
            if ($error == '') {
                // No error, log request and send SMS.
                $vchars = '0123456789';
                $vcharsCount = strlen($vchars) - 1;
                $smsRefCode = '';

                for ($i = 0; $i < 6; $i++) {
                    $smsRefCode .= $vchars[mt_rand(0, $vcharsCount)];
                }

                if (Config::get('setting.opcode') == 'IFW' && Session::get('currency') == 'TWD') {
                    $smsRequest = $smsObj->getShortCurrency(Session::get('currency')) . '0.00 ' .
                        $smsObj->getOperatorName('IFT', '', '验证码: ') . $smsRefCode;
                } else {
                    $smsRequest = $smsObj->getShortCurrency(Session::get('currency')) . '0.00 ' .
                        $smsObj->getOperatorName(Config::get('setting.opcode'), '', ': ') .
                        'Your verification code: ' . $smsRefCode;
                }

                $smslogObj = Smslog::quickCreate(0, '', $request->ip(), $smsProvider, $smsMethod, $telmobile, $smsRequest, '', $smsRefCode, 0);
                $response = '';

                try {
                    $response = $smsObj->sms($smsRequest, $telmobile);
                } catch (\Exception $ex) {
                    // Nothing.
                }

                $smslogObj->response = $response;
                $smslogObj->save();

                return response()->json(array(
                    'code' => 0,
                    'msg' => '',
                ));
            }
		}else{
			if ($error == '') {
                // No error, log request and send SMS.
                $vchars = '23456789ABCDEFGHJKLMNPQRTUVWXY';
                $vcharsCount = strlen($vchars) - 1;
                $smsRefCode = '';

                for ($i = 0; $i < 6; $i++) {
                    $smsRefCode .= $vchars[mt_rand(0, $vcharsCount)];
                }

                if (Config::get('setting.opcode') == 'IFW' && Session::get('currency') == 'TWD') {
                    $smsRequest = $smsObj->getShortCurrency(Session::get('currency')) . '0.00 ' .
                        $smsObj->getOperatorName('IFT', '', '验证码: ') . $smsRefCode;
                } else {
                    $smsRequest = $smsObj->getShortCurrency(Session::get('currency')) . '0.00 ' .
                        $smsObj->getOperatorName(Config::get('setting.opcode'), '', ': ') .
                        'Your verification code: ' . $smsRefCode;
                }

                $smslogObj = Smslog::quickCreate(0, '', $request->ip(), $smsProvider, $smsMethod, $telmobile, $smsRequest, '', $smsRefCode, 0);
                $response = '';

                try {
                    $response = $smsObj->sms($smsRequest, $telmobile);
                } catch (\Exception $ex) {
                    // Nothing.
                }

                $smslogObj->response = $response;
                $smslogObj->save();

                return response()->json(array(
                    'code' => 0,
                    'msg' => '',
                ));
            }
		}
    }

        return response()->json(array(
            'code' => 1,
            'msg' => $error,
        ));
    }

	 public function sendChangePassSms(Request $request) {

	    Lang::setLocale($request->input('language', 'en'));
	    $supportedRegion = array('MYR', 'TWD');

	    // Only enable for MYR currency.
        if (!in_array(Session::get('currency'), $supportedRegion)) {
            return response()->json(array(
                'code' => 1,
                'msg' => Lang::get('public.UnsupportedRegion')
            ));
        }

        // SMS config.
        $smsMethod = 'changepass';

        $error = Lang::get('validation.regex', array('attribute' => Lang::get('public.ContactNumber')));
        $telmobile = $request->tel_mobile;

        if (Session::get('currency') == 'MYR') {
            if (!starts_with($telmobile, '01')) {
                return response()->json(array(
                    'code' => 2,
                    'msg' => Lang::get('public.ContactNumberMustStartsWithX', array('p1' => '01')),
                ));
            }
        } elseif (Session::get('currency') == 'TWD') {
            if (!starts_with($telmobile, '09')) {
                return response()->json(array(
                    'code' => 2,
                    'msg' => Lang::get('public.ContactNumberMustStartsWithX', array('p1' => '09')),
                ));
            }
        } else {
            // Should not happen.
            return response()->json(array(
                'code' => 1,
                'msg' => Lang::get('public.UnsupportedRegion'),
            ));
        }

        // Check contact number format.
        if (preg_match('/\d{9,}/', $telmobile) == 1) {
            // Check if number is in use.
            $obj = Accountdetail::where('telmobile', '=', $telmobile)->first();

            if (Session::get('currency') == 'TWD') {
                $smsObj = new SMSMoDing();
                $smsProvider = SMSMoDing::NAME;
            }elseif(Config::get('setting.opcode') == 'LVG'){
				$smsObj = new BulkSMS();
				$smsProvider = BulkSMS::NAME;
			}elseif(Config::get('setting.opcode') == 'RYW') {
                $smsObj = new SMS88();
                $smsProvider = SMS88::NAME;
            }

                // Add country code to tel number.
                $telmobile = $smsObj->formatTel(Session::get('currency'), $telmobile);

                // Check IP spam.
                $requestCount = Smslog::where('sender_ip', '=', $request->ip())
                    ->where('created_at', '>=', Carbon::now()->addMinutes(-60)->toDateTimeString())
                    ->where('method', '=', $smsMethod)
//                    ->where('ref_status', '=', '0') // Strict only 3 SMS within # seconds
                    ->count();

                if ($requestCount >= 3) {
                    // IP has requested # of times on last minutes, consider as spam.
                    $error = Lang::get('public.PleaseWaitForXSecondsToResendSMS', array('p1' => 60));
                } else {
                    // IP is safe.
                    // Check latest user request for verify code.
                    $smslogObj = Smslog::where('tel_mobile', '=', $telmobile)
                        ->where('method', '=', $smsMethod)
                        ->where('ref_status', '=', '0')
                        ->orderBy('created_at', 'desc')
                        ->first();

                    if ($smslogObj) {
                        // Has record found, check if send within period to prevent SMS spam.
                        $sendDt = Carbon::parse($smslogObj->created_at);
                        $resendSecLimit = 60;
                        $sentSecInterval = $sendDt->diffInSeconds(Carbon::now());

                        if ($sentSecInterval >= $resendSecLimit) {
                            // Is ok to send SMS.
                            $error = '';
                        } else {
                            $error = Lang::get('public.PleaseWaitForXSecondsToResendSMS', array('p1' => ($resendSecLimit - $sentSecInterval)));
                        }
                    } else {
                        // No record found, is ok to send SMS.
                        $error = '';
                    }
                }


            if ($error == '') {
                // No error, log request and send SMS.
                $vchars = '0123456789';
                $vcharsCount = strlen($vchars) - 1;
                $smsRefCode = '';

                for ($i = 0; $i < 6; $i++) {
                    $smsRefCode .= $vchars[mt_rand(0, $vcharsCount)];
                }

                if (Config::get('setting.opcode') == 'IFW' && Session::get('currency') == 'TWD') {
                    $smsRequest = $smsObj->getShortCurrency(Session::get('currency')) . '0.00 ' .
                        $smsObj->getOperatorName('IFT', '', '验证码: ') . $smsRefCode;
                } else {
                    $smsRequest = $smsObj->getShortCurrency(Session::get('currency')) . '0.00 ' .
                        $smsObj->getOperatorName(Config::get('setting.opcode'), '', ': ') .
                        'Your verification code: ' . $smsRefCode;
                }

                $smslogObj = Smslog::quickCreate(0, '', $request->ip(), $smsProvider, $smsMethod, $telmobile, $smsRequest, '', $smsRefCode, 0);
                $response = '';

                try {
                    $response = $smsObj->sms($smsRequest, $telmobile);
                } catch (\Exception $ex) {
                    // Nothing.
                }

                $smslogObj->response = $response;
                $smslogObj->save();

                return response()->json(array(
                    'code' => 0,
                    'msg' => '',
                ));
            }
        }

        return response()->json(array(
            'code' => 1,
            'msg' => $error,
        ));
    }

}
