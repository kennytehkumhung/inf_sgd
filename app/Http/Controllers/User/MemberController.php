<?php namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller\User;
use App\libraries\providers\JOK;
use App\libraries\providers\MNT;
use App\libraries\providers\OSG;
use App\libraries\providers\SKY;
use App\Models\Account;
use App\Models\Accountbank;
use App\Models\Accountdetail;
use App\Models\Article;
use App\Models\Bank;
use App\Models\Banner;
use App\Models\Agent;
use App\Models\Bankaccount;
use App\Models\Configs;
use App\Models\Inbox;
use App\Models\Media;
use App\Models\Product;
use File;
use Illuminate\Http\Request;
use App\libraries\App;
use Session;
use Storage;
use Validator;
use Hash;
use Crypt;
use Lang;
use Auth;
use Config;
use App\libraries\providers\ALB;
use App\libraries\providers\MXB;
use App\libraries\providers\PLT;
use App\libraries\providers\PLTB;
use App\Models\Smslog;
use Carbon\Carbon;

class MemberController extends Controller{
	
	public function __construct()
	{
		
	}

	public function AccountPassword(Request $request){
		$data['content'] = Article::where('code','=','USER_PASSWORD')->where('lang','=',Lang::getLocale())->pluck('content');
		
		return view(Config::get('setting.front_path').'/update_password',$data);
	
	}

	
	public function ChangePassword(Request $request){
		
		if(Config::get('setting.opcode') == 'LVG'){
			
			
			$validator = Validator::make(
			[
				'new_password' 		   => $request->input('new_password'),
				'confirm_new_password' => $request->input('confirm_new_password'),
				'mobile_vcode'		   => $request->input('mobile_vcode'),
			],
			[
				'new_password'   		 => 'required|alpha_num|min:6|max:12',
				'confirm_new_password'   => 'required|alpha_num|min:6|max:12',
				'mobile_vcode'			 => 'required|min:6',
			]
		 );
		 
			 
		}else{
			$validator = Validator::make(
			[
				'new_password' 		   => $request->input('new_password'),
				'confirm_new_password' => $request->input('confirm_new_password'),
			],
			[
				'new_password'   		 => 'required|alpha_num|min:6|max:12',
				'confirm_new_password'   => 'required|alpha_num|min:6|max:12',

			]
		 );
		}

		
		
		$validator->setAttributeNames(array(
		    'new_password' => Lang::get('public.NewPassword'),
        ));
		
		if ($validator->fails())
		{
			echo json_encode($validator->errors());
			exit;
		}
		

		if ( !Hash::check($request->input('current_password'), Auth::user()->get()->password))
		{ 
			echo json_encode( array('current_password' => Lang::get('validation.oldpass_wrong')));
			exit;
		}
		
		if( $request->input('new_password') != $request->input('confirm_new_password') )
		{
			echo json_encode( array('current_password' => Lang::get('validation.newandconpass_notsame')));
			exit;
		}
		
			$mobileTel = '';
			$savePhone = false;
			$smslogObj = null;
			
			$accdetail = Accountdetail::where('id', '=', Session::get('userid'))->first();
			$acdObj = Account::where( 'id' , '=' , Session::get('userid') )->first();
			$acdObj->password      = Hash::make( $request->input('new_password') );
			$acdObj->enpassword    = Crypt::encrypt( $request->input('new_password') );
			$acdObj->isupdatepassword    = '1';
			
			if(Config::get('setting.opcode') == 'LVG'){
				// Check mobile from DB first, if empty then get from user input.
				$mobileTel = $accdetail->telmobile;
				
				if (empty($mobileTel)) {
					$mobileTel = $request->input('mobile');
					
					if (empty(trim($mobileTel))) {
						// If input also is empty.
						echo json_encode(array('mobile_vcode' => Lang::get('public.PleaseEnterContactNumberToContinue')));
						exit;
					}
					
					if (Session::get('currency') == 'MYR') {
						if (!starts_with($mobileTel, '01')) {
							echo json_encode(array('mobile' => Lang::get('public.ContactNumberMustStartsWithX', array('p1' => '01'))));
							exit;
						}
					}
					
					// Check mobile is already exists.
					$count = AccountDetail::where('telmobile', '=', $mobileTel)->count();
					
					if ($count > 0) {
						echo json_encode(array('mobile' => Lang::get('public.ContactNumberIsAlreadyInUse')));
						exit;
					}
					
					$savePhone = true;
				}
				
				$smslogObj = Smslog::where('ref_code', '=', trim($request->input('mobile_vcode')))
					//->where('tel_mobile', 'like', '%' . $mobileTel)
					->where('created_at', '>=', Carbon::now()->addMinutes(-30)->toDateTimeString()) // Each verify code available for 30 minutes.
					->where('method', '=', 'changepass') 
					->where('ref_status', '=', 0)
					->first();
				
				if($smslogObj){
					$smslogObj->ref_status = 1;
					$smslogObj->save();
				}else{
					echo json_encode(array('mobile_vcode' => Lang::get('public.InvalidVerificationCode')));
					exit;
				}
            }
			
			if($acdObj->save())
			{
                Session::put('enpassword', $acdObj->enpassword );

				if ($savePhone) {
					$accdetail->telmobile = $mobileTel;
					$accdetail->save();
				}
				
				if( $this->checkIsActive('ALB') )
				{
					$albObj = new ALB;
					$albObj->updatePassword(Session::get('username'),$request->input('new_password'));		
				}
				
				if( $this->checkIsActive('MXB') )
				{
					$mxbObj = new MXB;
					$mxbObj->updatePassword(Session::get('username'),$request->input('new_password'),Session::get('user_currency'));			
				}
				
				if( $this->checkIsActive('PLTB') )
				{
					$pltObj = new PLTB;
					$pltObj->updatePassword(Session::get('username'),Session::get('user_currency') ,$request->input('new_password'));
				}
				
				if( $this->checkIsActive('JOK') )
				{
					$jokObj = new JOK();
					$jokObj->updatePassword(array('username' => Session::get('username'), 'currency' => Session::get('user_currency'), 'password' => $acdObj->enpassword));
				}
				
				if( $this->checkIsActive('SKY') )
				{
					$skyObj = new SKY();
					$skyObj->updatePassword(array('userid' => Session::get('userid'), 'currency' => Session::get('user_currency'), 'password' => $acdObj->enpassword));
				}

				if( $this->checkIsActive('OSG') )
				{
					$osgObj = new OSG();
					$osgObj->updatePassword(Session::get('username'), $acdObj->enpassword, Session::get('user_currency'));
				}

                if( $this->checkIsActive('MNT') )
                {
                    $mntObj = new MNT();
                    $mntObj->updatePassword(Session::get('username'),$request->input('new_password'),Session::get('user_currency'));
                }
				echo json_encode( array( 'code' => Lang::get('COMMON.SUCESSFUL') ) );
			}
			
	} 	
	
	private function checkIsActive($code){
		
		return Product::whereCode( $code )->whereStatus( 1 )->count();
	}
	
	public function AccountProfile(Request $request){
		$data['content'] = Article::where('code','=','UPDATE_PROFILE')->where('lang','=',Lang::getLocale())->pluck('content');

		$acdObj = Accountdetail::where( 'accid' , '=' , Session::get('userid') )->first();
		
		if ($request->input('as_json') == true) {
		    $response = array(
                'email'         => $acdObj->email,
                'fullname'      => $acdObj->fullname,
                'telmobile'     => $acdObj->telmobile,
                'gender'        => $acdObj->gender,
                'dob'           => $acdObj->dob,
                'resaddress'    => $acdObj->resaddress,
                'rescity'       => $acdObj->rescity,
                'referralid'    => 0,
            );

		    if ( Config::get('setting.opcode') == 'RYW' ) {
		        // Only for RW.
		        $response['referralid'] = $acdObj->accid;
            }

            return response(json_encode($response));
        }
        if ( Config::get('setting.opcode') == 'RYW' ) {
            $banners = Banner::where('status', '=', 1)->whereCategories(3)->where('crccode', '=', Session::get('currency'))->where('media.refobj', '=', 'Banner')->where('lang', '=', Lang::getLocale())->leftJoin('media', 'banner.id', '=', 'media.refid')->where('media.type', '=', Media::TYPE_IMAGE)->orderBy('rank', 'asc')->first();
            if (!empty($banners)) {
                $data['popup_banner'] = $banners->domain . '/' . $banners->path;
            }
        }
		
		$data['acdObj'] = $acdObj;
	
		return view(Config::get('setting.front_path').'/update_profile',$data);
	
	}
	
	public function AccountPass(Request $request){
		$data['content'] = Article::where('code','=','UPDATE_PROFILE')->where('lang','=',Lang::getLocale())->pluck('content');

		$acdObj = Accountdetail::where( 'accid' , '=' , Session::get('userid') )->first();		
			 if(Config::get('setting.opcode') == 'LVG'){
				$mobileTel = $acdObj->telmobile;
				
				if (empty($mobileTel)){
					//$mobileTel = $request->input('mobile');
					$data['require_mobile'] = 'needmobile';		
				}else{
					$data['require_mobile'] = 'noneedmobile';
				}
			}
			
		
		if ($request->input('as_json') == true) {
		    $response = array(
                'email'         => $acdObj->email,
                'fullname'      => $acdObj->fullname,
                'telmobile'     => $acdObj->telmobile,
                'gender'        => $acdObj->gender,
                'dob'           => $acdObj->dob,
                'resaddress'    => $acdObj->resaddress,
                'rescity'       => $acdObj->rescity,
                'referralid'    => 0,
            );

            return response(json_encode($response));
        }
		
		$data['acdObj'] = $acdObj;
	
		return view(Config::get('setting.front_path').'/update_password',$data);
	
	}
	
	public function UpdateDetail(Request $request){

		$fullname   = $request->has('fullname') ? $request->input('fullname')    : false;
		$email     	= $request->has('email') 	? $request->input('email')    : false;
		$dob    	= $request->has('dob') 	  	? $request->input('dob')    : false;
		$gender  	= $request->has('gender') 	? $request->input('gender') : false;
		$mobile  	= $request->has('mobile') 	? $request->input('mobile') : false;
		$address 	= $request->has('address') 	? $request->input('address') : false;
		$city    	= $request->has('city') 	? $request->input('city') : false;
		
		if( $dob || $gender || $mobile ) 
		{
			$acdObj = Accountdetail::where( 'accid' , '=' , Session::get('userid') )->first();
			
			if($fullname)   $acdObj->fullname     	= $fullname;
			if($email)    	$acdObj->email          = $email;
			if($dob)     	$acdObj->dob          	= $dob;
			if($gender)  	$acdObj->gender        	= $gender;
			if($mobile)  	$acdObj->telmobile     	= $mobile;
			if($address) 	$acdObj->resaddress    	= $address;
			if($city)    	$acdObj->rescity       	= $city;
			
			if($acdObj->save())
			{
				echo json_encode( array( 'code' => Lang::get('COMMON.SUCESSFUL') ) );
			}
		}
		
	}	
	
	public function UpdateBank(Request $request) {

		$bank   		= $request->has('bnkid') 		  ? $request->input('bnkid')      : false;
		$bank_branch	= $request->has('bank_branch')    ? $request->input('bank_branch'): '';
		$account_holder = $request->has('bnkaccname')     ? $request->input('bnkaccname') : false;
		$account_no	    = $request->has('bnkaccno') 	  ? $request->input('bnkaccno')	  : false;
		$fullname	    = $request->has('fullname') 	  ? $request->input('fullname')	  : false;
		
		if(!$bank) 		 exit;
		if(!$account_no) exit;
		
		
		$acdObj = Accountdetail::where( 'accid' , '=' , Session::get('userid') )->first();
		$accountObj = Account::where( 'id' , '=' , Session::get('userid') )->first();

		if($fullname) {
			$acdObj->fullname = $fullname;
			$accountObj->fullname = $fullname;
			$acdObj->save();
			$accountObj->save();
		}
		 
		if( $bnkObj = Bank::whereRaw('code = "'.$bank.'"')->first() ) {
			
			if(Accountbank::whereAccid(Session::get('userid'))->count() >= 3){
				return 2;
			}
                	
			if( $accBnkObj = Accountbank::whereBankcode($bank)->whereAccid(Session::get('userid'))->first() ){
				
			}else{
				$accBnkObj = new Accountbank;
			}
			$accBnkObj->accid 				= Session::get('userid');
			$accBnkObj->bankid 				= $bnkObj->id;
			$accBnkObj->acccode 			= Session::get('acccode');
			$accBnkObj->bankaccname 		= $acdObj->fullname;
			$accBnkObj->bankcode 			= $bnkObj->code;
			$accBnkObj->bankname 			= $bnkObj->name;
			$accBnkObj->bankaccno 			= str_replace('-', '', $account_no);
			$accBnkObj->bankholdingbranch 	= $bank_branch;
			
			if($request->has('province'))
			{	
				$accBnkObj->bankprovince		= $request->input('province');
				$accBnkObj->bankcity 			= $request->input('city');
				$accBnkObj->bankdistrict		= $request->input('district');
				$accBnkObj->bankholdingbranch	= $request->input('bankbranch');
			}
			
			if($accBnkObj->save()) {
               

				echo 1;
			}else
			{
                echo 2;
            }
                    
		}
		

	}
	
	public function MemberDetail() {
		
		$data = Account::find(Session::get('userid'))->detail()->first();
		var_dump($data);
	} 	
	
	public function MemberBank() {
		
		$data = array();
		
		if($acbObj = Account::find(Session::get('userid'))->bankaccount()->first()) {
			$bankname = '';
			if($bnkObj = Bank::find($acbObj->bankid)) $bankname = $bnkObj->name;
			$data['userbank'] = array(
				'bank_name' 	 => $bankname,
				'bank_branch'	 => $acbObj->bankholdingbranch,
				'account_holder' => $acbObj->bankaccname,
				'account_no' 	 => $acbObj->bankaccno,
			);
		}
		
		$user = Auth::user()->get();
		
		$bhdid = 0;
		if($agtObj = Agent::find(Session::get('agtid'))) {
			$bhdid = $user->bhdid == 0 ? $agtObj->bhdid : $user->bhdid;
		}
		
		
		$condition = 'bhdid = '.$bhdid.' AND crccode="'.Session::get('currency').'" AND status = '.CBO_STANDARDSTATUS_ACTIVE;

		if(Session::get('currency') == 'IDR')
                    $condition .= ' AND deposit = 1';
                else
                    $condition .= ' AND withdraw = 1';

		$bankaccounts = Bankaccount::whereRaw($condition)->get();
		
		$data['bankObj'] = Bank::whereRaw(' id IN (select bnkid from bankaccount where '.$condition.' )' )->get();
                
        $data['accbank'] = Accountbank::whereAccid(Session::get('userid'))->first();
		
		if( Session::get('currency') == "CNY" ){
			if( $banks = Bank::whereStatus(1)->orderBy('seq', 'desc')->get() )
			{
				foreach($banks as $bnkObj){
					$banklists[$bnkObj->name] = array
					(
							'code'		=> $bnkObj->code
					);
				}
			}
			$data['banklists'] = $banklists;
		}
		$acdObj = Accountdetail::where( 'accid' , '=' , Session::get('userid') )->first();
		if($acdObj->fullname){
			$data['fullname']=$acdObj->fullname;
		}
		
		return view(Config::get('setting.front_path').'/memberbank',$data);
	} 
	
	public function loadbank(Request $request) {
		
		if( Session::get('currency') == "CNY" ){
			return json_encode(Accountbank::whereBankcode($request->id)->whereAccid(Session::get('userid'))->first());
		}
		return Accountbank::whereBankcode($request->id)->whereAccid(Session::get('userid'))->orderBy('id','desc')->pluck('bankaccno');
	}
        
        public function Memberverify() {
                $data = array();
                
                $data['verify'] = Account::whereId(Session::get('userid'))->first();
                
                return view(Config::get('setting.front_path').'/memberverify',$data);
        }
        
        public function UpdateTac(Request $request) {

		$tac = $request->has('tac')?$request->input('tac'):false;
                
                if(!$tac) exit;
                
			if( $acdObj = Accountdetail::whereRaw('accid = '.Session::get('userid').' AND verifyno = "'.$tac.'"')->first() ) {
				if($accObj = Account::whereRaw('id = '.$acdObj->accid.' AND isemailvalid = 0')->first()){
					$accObj->isemailvalid = 1;
					if($accObj->save()) {
						echo 1;
					}
				}else{
					echo 2;
				}
			}
        }
	
	public function checkUpdatePassword() {
		if(  Auth::user()->check()){
			if(Account::whereId(Session::get('userid'))->whereIsupdatepassword(0)->count()==1)
			{
					echo json_encode( array('code' => Lang::get('COMMON.PLEASERESETPASSWORD')));
			}
		}
	}

	
	

	
}
