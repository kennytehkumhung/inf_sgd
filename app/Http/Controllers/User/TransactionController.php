<?php namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller\User;
use App\libraries\paymentgateway\DuoDeBao\DuoDeBao;
use App\libraries\paymentgateway\PayWalo;
use App\Models\Agent;
use App\Models\Fundingmethodsetting;
use App\Models\Configs;
use App\Models\Bankaccount;
use App\Models\Accounttag;
use App\Models\Paymentgateway;
use App\Models\Paymentgatewaysetting;
use App\Models\Tag;
use App\Models\Promocampaign;
use App\Models\Media;
use App\Models\Fundingmethod;
use App\Models\Bank;
use App\Models\Product;
use App\Models\Cashledger;
use App\Models\Article;
use App\Models\Banktransfer;
use App\Models\Currency;
use App\Models\Promocash;
use App\Models\Accountbank;
use App\Models\Bankledger;
use App\Models\Wager;
use App\Models\Account;
use App\Models\Withdrawcard;
use App\Models\Accountdetail;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\libraries\App;
use Session;
use Lang;
use Validator;
use Storage;
use File;
use Cache;
use Config;
use Auth;
use Redirect;
use DB;
use Log;
use App\Jobs\ChangeLocale;



class TransactionController extends Controller{
	
	public function __construct()
	{
		
	}

	public function deposit(Request $request){
        
        if( (Config::get('setting.front_path') == 'dadu') && Account::whereRaw('id='.Session::get('userid'))->pluck('isemailvalid') == 0)	return Redirect::route('memberverify');
        if( (Config::get('setting.front_path') == 'ampm' || Config::get('setting.front_path') == 'dadu' || Config::get('setting.front_path') == 'c59' || Config::get('setting.front_path') == '369bet') && Accountbank::whereAccid(Session::get('userid'))->count() == 0 )	return Redirect::route('memberbank');
		
		$data = array();
		$promotions = array();
		
		$min = $max = 0;
		if($fmdObj = Fundingmethod::find(1)) { 
			$banks = unserialize($fmdObj->depositbanks);
			
			if($fmsObj = Fundingmethodsetting::whereRaw('fdmid=1 AND crccode = "'.Session::get('currency').'"')->first()) {
				$min = $fmsObj->mindeposit;
				$max = $fmsObj->maxdeposit;
			}
		}
		
		$user = Auth::user()->get();
		
		$bhdid = 0;
		if($agtObj = Agent::find(Session::get('agtid'))) {
			$bhdid = $user->bhdid == 0 ? $agtObj->bhdid : $user->bhdid;
		}
		
		
		$condition = 'bhdid = '.$bhdid.' AND crccode="'.Session::get('currency').'" AND bankaccount.status = '.CBO_STANDARDSTATUS_ACTIVE;

		
		$condition .= ' AND deposit = 1';

		
		
		if(  Config::get('setting.front_path') == 'ampm' || Config::get('setting.front_path') == 'dadu'){
			
			$condition .= ' AND bnkid IN ( select bankid from accountbank where acccode = "'.Session::get('acccode').'" )';


                        if($tag = Tag::whereRaw('special = 1')->first()){
                            $tagcondition = 'AND tag = 0';
                            if($specialAcc = Accounttag::whereRaw('accid ='.Session::get('userid').' AND tagid ='.$tag->id.' AND status = 1')->first()){
                                $tagcondition = ' AND tag = '.$tag->id.' AND bnkid IN ( select bankid from accountbank where acccode = "'.Session::get('acccode').'" )';
                                $tagid = $tag->id;
                            }


                            $condition .= $tagcondition;
                        }
		}

		//Log::info($condition);
		
		$bankaccounts = Bankaccount::whereRaw($condition)->get();
		
		$bankings = array();
		$count = 0;
		foreach($bankaccounts as $bankaccount){
			$bank = Bank::find( $bankaccount->bnkid);
			$media = Media::where( 'refobj' , '=' ,'BankObject' )->where( 'refid' , '=' , $bankaccount->bnkid)->first();
			$bankings[$count]['image'] = isset( $media->domain ) ?  $media->domain.'/'.$media->path : '';
			$bankings[$count]['bnkid'] = $bankaccount->id;
			$bankings[$count]['bankaccno'] = $bankaccount->bankaccno;
			$bankings[$count]['bankaccname'] = $bankaccount->bankaccname;
			$bankings[$count]['name'] = $bank->name;
			$bankings[$count]['min']  = App::formatNegativeAmount($bank->mindeposit);
			$bankings[$count++]['max'] = App::formatNegativeAmount($bank->maxdeposit);
		}
		
		if($special_tags = Accounttag::where( 'accid' , '=' , Session::get('userid') )->whereRaw(' tagid IN (select id from tag where special = 1) ')->get()){
			
			foreach( $special_tags as $special_tag )
			{
				$temp_tagids[] = $special_tag->tagid;
			}
			
			if( isset($temp_tagids) && count($temp_tagids) > 0 )
				$tagid = ' tag IN ('.implode(',',$temp_tagids).')';
			
		}

                if (isset ($tagid)){
                    $promos = Promocampaign::getPromoByMemberId( Session::get('userid'), $tagid );
                }
                else {
                    $promos = Promocampaign::getPromoByMemberId( Session::get('userid') );
                }
                    foreach( $promos as $id => $promo )
                    {

                            $promotions[$id]['code'] = $promo['code'];
                            $promotions[$id]['image'] = '';
                            $promotions[$id]['name'] = $promo['name'];

                        if ($this->isMobileSite()) {
                            if($media = Media::where( 'refobj' , '=' ,'PromoCampaign' )->where( 'refid' , '=' , $id)->where('type', '=', Media::TYPE_IMAGE_MOBILE)->first()){
                                $promotions[$id]['image'] = $media->domain.'/'.$media->path;
                            }
                        } else {
                            if($media = Media::where( 'refobj' , '=' ,'PromoCampaign' )->where( 'refid' , '=' , $id)->where('type', '=', Media::TYPE_IMAGE)->first()){
                                $promotions[$id]['image'] = $media->domain.'/'.$media->path;
                            }
                        }
                    }


        $data['fullname'] = Session::get('fullname');
		$data['banks']  = $bankings;
		$data['promos'] = $promotions;
		$data['content'] = Article::where('code','=','USER_DEPOSIT')->where('lang','=',Lang::getLocale())->pluck('content');
		$data['withdrawalCodeRequired'] = false;
		$data['bankbookRequired'] = false;
		$data['checkBankAccountNumber'] = cashledger::whereIn( 'status' , array(1,4))
			->where( 'accid' , '=' , Session::get('userid'))
			->count('id');

        $pgSettingObj = array();
        $pgSettings = array();

		if (Config::get('setting.opcode') == 'GSC' && Session::get('username') == 'test74') {
            // Get payment gateway settings
            $pgSettingObj = Paymentgatewaysetting::whereIn('code', array('QYF', 'DDB', 'ETP'))
                ->orderBy('group_code', 'asc')
                ->orderBy('seq', 'asc')
                ->get();
        } else {
            // Get payment gateway settings
            $pgSettingObj = Paymentgatewaysetting::where('deposit', '=', 1)
                ->where('status', '=', 1)
                ->orderBy('group_code', 'asc')
                ->orderBy('seq', 'asc')
                ->get();
        }

        $privateTypeOptions = collect(Paymentgatewaysetting::getPrivateTypeOptions());

        foreach ($pgSettingObj as $r) {
            if (!array_key_exists($r->group_name, $pgSettings)) {
                $pgSettings[$r->group_name] = array();
            }

            $imageUrl = '';

            if ($r->is_private == 1) {
                $pgObj = Paymentgatewaysetting::find($r->id);
                $imageUrl = $pgObj->getImage();
            }

            $pgSettings[$r->group_name][] = array(
                'code' => $r->code,
                'name' => $r->name,
                'deposit_min' => $r->deposit_min,
                'deposit_max' => $r->deposit_max,
                'is_private' => $r->is_private,
                'private_type' => $r->private_type,
                'private_type_label' => $privateTypeOptions->get($r->private_type),
                'image_url' => $imageUrl,
                'url' => $r->url,
            );
        }

        $data['paymentGatewaySetting'] = $pgSettings;

        if (Config::get('setting.opcode') == 'IFW' && Session::get('currency') == 'TWD') {
		    // IFW TWD member required to upload bankbook for first deposit.
            $data['withdrawalCodeRequired'] = (strlen($user->withdrawalcode) < 1);
            $data['bankbookRequired'] = (Media::where('refobj', '=', 'Bankbook')->where('refid', '=', Session::get('userid'))->first(array('id')) ? false : true);
        }

        if ($request->input('as_json') == true) {
            return response(json_encode($data));
        }
        
		return view(Config::get('setting.front_path').'/deposit',$data);
	}
	
	public function depositProcess(Request $request){

        if ($request->input('deposit_pay_channel', 'BNK') != 'BNK') {
            // Deposit method BNK: local bank transfer (set at paymentgatewaysetting).
            return $this->depositPaymentGatewayProcess($request);
        }
		
		$hasbonus = false;
		$inputs = [
            'bank'	   => $request->input('bank'),
            'promo'	   => $request->input('promo'),
            'amount'   => $request->input('amount'),
            //'refno'    => $request->input('refno'),
            'type'     => $request->input('type'),
            'minutes'  => $request->input('minutes'),
            'hours'    => $request->input('hours'),
            'date' 	   => $request->input('date'),
            'rules'    => $request->input('rules'),
        ];

		$rules = [
            'bank' 	   => 'required|numeric',
            'promo'    => 'alpha_num',
            'amount'   => 'required|decimal|min:1',
            //'refno'    => 'required|alpha_num',
            'type'     => 'required|numeric',
            'minutes'  => 'required|numeric',
            'hours'    => 'required|numeric',
            'date' 	   => 'required|date',
            'rules'    => 'required',
        ];

		if (Config::get('setting.opcode') == 'IFW' && Session::get('currency') == 'TWD') {
		    if (strlen(Session::get('fullname')) < 1) {
		        // User does not provide full name yet.
                $inputs['fullname'] = trim($request->input('fullname'));
                $rules['fullname'] = 'required';
            }

            if (strlen(Auth::user()->get()->withdrawalcode) < 1) {
		        // User does not provide withdrawal code yet.
                $inputs['withdrawalcode'] = $request->input('withdrawalcode');
                $inputs['confirmwithdrawalcode'] = $request->input('confirmwithdrawalcode');
                $rules['withdrawalcode'] = 'required|size:4|same:confirmwithdrawalcode';
            }
        }

//		if(  Config::get('setting.front_path') == '369bet' ){
//		$validator = Validator::make(
//			[
//	
//				'bank'	   => $request->input('bank'),
//				'promo'	   => $request->input('promo'),
//				'amount'   => $request->input('amount'),
//				'refno'    => $request->input('refno'),
//				'type'     => $request->input('type'),
//				'minutes'  => $request->input('minutes'),
//				'hours'    => $request->input('hours'),
//				'date' 	   => $request->input('date'),
//				'rules'    => $request->input('rules'),
//			],
//			[
//
//				'bank' 	   => 'required|numeric',
//				'promo'    => 'required|alpha_num',
//				'amount'   => 'required|decimal|min:1',
//				'refno'    => 'required|alpha_num',
//				'type'     => 'required|numeric',
//				'minutes'  => 'required|numeric',
//				'hours'    => 'required|numeric',
//				'date' 	   => 'required|date',
//				'rules'    => 'required',
//			]
//		);
//                }
//                else {
                    $validator = Validator::make($inputs, $rules);
//                }

        $validator->setAttributeNames(array(
            'bank' 	                    => Lang::get('public.Bank'),
            'promo'                     => Lang::get('public.Promotion'),
            'amount'                    => Lang::get('public.Amount'),
            'refno'                     => Lang::get('public.DepositReceiptReferenceNo'),
            'type'                      => Lang::get('public.DepositMethod'),
            'minutes'                   => Lang::get('COMMON.MINUTES'),
            'hours'                     => Lang::get('COMMON.HOURS'),
            'date' 	                    => Lang::get('COMMON.DATE'),
            'rules'                     => Lang::get('public.DepositPromoRules'),
            'withdrawalcode'            => Lang::get('public.WithdrawalCode'),
            'confirmwithdrawalcode'     => Lang::get('public.ConfirmWithdrawalCode'),
            'depositmethod'             => Lang::get('public.DepositMethod'),
        ));

		if ($validator->fails())
		{
			echo json_encode($validator->errors());
			exit;
		}

        if (!$request->hasFile('file')) {
            if (Config::get('setting.front_path') == 'idnwin99') {
                // Projects that require must upload attachment files.
                echo json_encode( array( 'code' => Lang::get('public.PleaseUploadDepositSupportDocument') ) );
                exit;
            }
        }

        if (Config::get('setting.opcode') == 'IFW' && Session::get('currency') == 'TWD') {
		    // IFW TWD member required to setup withdrawal code.
            if (strlen(Auth::user()->get()->withdrawalcode) < 1 && strlen($request->input('withdrawalcode')) > 0) {
                if (!preg_match('/[0-9]{4}/', $request->input('withdrawalcode'))) {
                    echo json_encode( array( 'code' => Lang::get('public.WithdrawalCodeMustBe4Digits') ) );
                    exit;
                }

                Account::where('id', '=', Session::get('userid'))->update(array(
                    'withdrawalcode' => $request->input('withdrawalcode'),
                ));
            }

            // IFW TWD member required to upload bankbook for first deposit.
            if (Media::where('refobj', '=', 'Bankbook')->where('refid', '=', Session::get('userid'))->count() < 1) {
                if (!$request->hasFile('file_bankbook')) {
                    echo json_encode( array( 'code' => Lang::get('public.PleaseUploadBankbookPicture') ) );
                    exit;
                }
            }
        }

        $fdmObj = Fundingmethod::find(Configs::getParam('SYSTEM_PAYMENT_ID_BANKTRANSFER'));
		$maxPendingDepositLimit = 1;

		if (Config::get('setting.opcode') == 'GSC') {
		    $maxPendingDepositLimit = 20;
        }
		
		if( Cashledger::whereAccid(Session::get('userid'))->whereChtcode(CBO_CHARTCODE_DEPOSIT)->whereFdmid($fdmObj->id)->whereStatus(0)->count() >= $maxPendingDepositLimit ){
			echo json_encode( array( 'code' => Lang::get('COMMON.FAILED').' [E302]' ) );
			exit;
		}
		
		$bankaccountObj = Bankaccount::find($request->bank);

		if( $bnkObj = Bank::find($bankaccountObj->bnkid) ) {
				$mindeposit = $bnkObj->mindeposit;
				$maxdeposit = $bnkObj->maxdeposit;
		}
		if( $request->has('promo') && $request->promo != '0' ) {
                        if($special_tags = Accounttag::where( 'accid' , '=' , Session::get('userid') )->whereRaw(' tagid IN (select id from tag where special = 1) ')->get()){
                            foreach( $special_tags as $special_tag )
                            {
                                    $temp_tagids[] = $special_tag->tagid;
                            }

                            if( isset($temp_tagids) && count($temp_tagids) > 0 )
                                    $tagid = ' tag IN ('.implode(',',$temp_tagids).')';
                        }

                        if (isset ($tagid)){
                            $promos = Promocampaign::getPromoByMemberId( Session::get('userid'), $tagid );
                        }
                        else {
                            $promos = Promocampaign::getPromoByMemberId( Session::get('userid') );
                        }

			if($promoObj = Promocampaign::whereRaw('startdate <= "'.date('Y-m-d H:i:s').'" AND enddate >= "'.date('Y-m-d H:i:s').'" AND status = '.CBO_STANDARDSTATUS_ACTIVE.' AND code = "'.$request->promo.'"')->first()){
				if(isset($promos[$promoObj->id])) {
					$mindeposit = $promoObj->mindeposit;
					$hasbonus = true;
				} else { echo json_encode( array( 'code' => Lang::get('COMMON.FAILED').' [E303]' ) );exit; }
			} else{ echo json_encode( array( 'code' => Lang::get('COMMON.FAILED').' [E304]' ) );exit; }
		}
		
		
		if($request->amount < $mindeposit) { echo json_encode( array( 'code' => Lang::get('public.BankMinDepositCriteria') ) );exit; }
		if($request->amount > $maxdeposit) { echo json_encode( array( 'code' => Lang::get('public.BankMaxExceeded') ) );exit; }
         
		

			
			$paymentObj 					= new Banktransfer;
			$paymentObj->fdmid 				= $fdmObj->id;
			$paymentObj->type 				= CBO_CHARTCODE_DEPOSIT;
			$paymentObj->bnkid 				= $bankaccountObj->bnkid;
			$paymentObj->accid 				= Session::get('userid');
			$paymentObj->acccode 			= Session::get('acccode');
			$paymentObj->crccode 			= Session::get('currency');
			$paymentObj->crcrate 			= Currency::getCurrencyRate($paymentObj->crccode);
			$paymentObj->amount 			= $request->amount;
			$paymentObj->amountlocal 		= Currency::getLocalAmount($request->amount, $paymentObj->crccode);
			//$paymentObj->fee 				= round($fdmObj->getFee(CBO_CHARTCODE_DEPOSIT, $paymentObj->amount), 2);
			//$paymentObj->feelocal 			= Currency::getLocalAmount($paymentObj->fee, $paymentObj->crccode);
			$paymentObj->status 			= CBO_LEDGERSTATUS_PENDING;
			$paymentObj->createdip			= App::getRemoteIp();;
			$paymentObj->receiveaccname 	= $bankaccountObj->bankaccname;
			$paymentObj->receiveaccno   	= $bankaccountObj->bankaccno;
			$paymentObj->receiveacc 		= $bankaccountObj->id;
			$hours = $request->input('range') == 'PM' ? $request->input('hours') + 12 : $request->input('hours');
			
			if( $request->input('hours') == 12 && $request->input('range') == 'PM' ){
				$hours = 12;
			}
                        if( $request->input('range') == '' ){
                            $hours = $request->input('hours');
                        }
			if(  Config::get('setting.front_path') == '369bet' ){
				$dates = explode('-',$request->input('date'));
				$paymentObj->transferdatetime   = $dates[2].'-'.$dates[1].'-'.$dates[0].' '. $hours . ':' . $request->input('minutes') . ':00';
			}
			else
			{
				$paymentObj->transferdatetime   = $request->input('date').' '. $hours . ':' . $request->input('minutes') . ':00';
			}
			$paymentObj->refno 			    = $request->input('refno', '');
			$paymentObj->transfertype 	    = $request->type;
			
			if($paymentObj->save())
			{
                $imageMemes = array('image/jpeg', 'image/jpg', 'image/png', 'image/gif');
                $fileKeys = array(
                    'file' => array(
                        'refobj' => 'Banktransfer',
                        'refid' => $paymentObj->id,
                        'uploadDir' => 'deposit',
                    ),
                    'file_bankbook' => array(
                        'refobj' => 'Bankbook',
                        'refid' => $paymentObj->accid,
                        'uploadDir' => 'bankbook',
                    ),
                );

                foreach ($fileKeys as $fKey => $fVals) {
                    if( $request->hasFile($fKey)) {
                        $file = $request->file($fKey);
                        $clientName = md5($file->getClientOriginalName().time()).'.'. $file->getClientOriginalExtension();
                        $filename = $fVals['uploadDir'].'/'.$clientName;
                        $memeType = $file->getMimeType();

                        if (Config::get('setting.front_path') == 'idnwin99' && in_array($memeType, $imageMemes) && $file->getSize() > 4 * pow(1024, 2)) {
                            // Only open for IDNWIN99 currently.
                            // If upload file is image, and file size is over 4MB (AWS limit), do compress image.
                            $tempFilename = 'tempUpload/'.$clientName;
                            $uploaded = Storage::disk('local')->put($tempFilename, File::get($file));

                            if ($uploaded) {
                                // File uploaded to temp folder, now compress uploaded image file.
                                $tempSource = Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix() . '/' . $tempFilename;
                                $image = null;

                                if ($memeType == 'image/jpeg' || $memeType == 'image/jpg') {
                                    $image = imagecreatefromjpeg($tempSource);
                                } elseif ($memeType == 'image/gif') {
                                    $image = imagecreatefromgif($tempSource);
                                } elseif ($memeType == 'image/png') {
                                    $image = imagecreatefrompng($tempSource);
                                }

                                imagejpeg($image, $tempSource, 90);     // Compress quality to certain %
                                $file = $tempSource;                    // Set compressed file path to be uploaded to AWS
                            }
                        }

                        $result = Storage::disk('s3')->put($filename,  File::get($file), 'public');

                        if($result) {
                            $medObj = new Media;
                            $medObj->refobj = $fVals['refobj'];
                            $medObj->refid  = $fVals['refid'];
                            $medObj->type   = Media::TYPE_IMAGE;
                            $medObj->domain = 'http://'.Configs::getParam('SYSTEM_AWS_S3BUCKET');
                            $medObj->path   = $filename;
                            $medObj->save();
                        }
                    }
                }
			
					$cashLedgerObj = new Cashledger;
					$cashLedgerObj->accid 			= $paymentObj->accid;
					$cashLedgerObj->acccode 		= $paymentObj->acccode;
					$cashLedgerObj->accname			= Session::get('fullname');
					$cashLedgerObj->chtcode 		= CBO_CHARTCODE_DEPOSIT;
					$cashLedgerObj->cashbalance 	= self::getAllBalance();
					$cashLedgerObj->amount 			= $paymentObj->amount;
					$cashLedgerObj->amountlocal 	= $paymentObj->amountlocal;
					$cashLedgerObj->refid 			= $paymentObj->id;
					$cashLedgerObj->status 			= CBO_LEDGERSTATUS_PENDING;
					$cashLedgerObj->refobj 			= 'Banktransfer';
					$cashLedgerObj->crccode 		= $paymentObj->crccode;
					$cashLedgerObj->crcrate 		= Currency::getCurrencyRate($paymentObj->crccode);
					$cashLedgerObj->fdmid			= $fdmObj->id;
					$cashLedgerObj->remarkcode 		= $request->input('remarkcode', '');
					$cashLedgerObj->bnkid			= $paymentObj->bnkid;
					$cashLedgerObj->fee 			= round($fdmObj->getFee(CBO_CHARTCODE_DEPOSIT, $paymentObj->amount), 2);
					$cashLedgerObj->feelocal 		= Currency::getLocalAmount($paymentObj->fee, $paymentObj->crccode);
					$cashLedgerObj->bacid			= $paymentObj->receiveacc;
					$cashLedgerObj->createdip		= $paymentObj->createdip;

                    if (Config::get('setting.opcode') == 'IFW' && Session::get('currency') == 'TWD' && strlen($cashLedgerObj->accname) < 1) {
                        $fullname = trim($request->input('fullname'));

                        if ($fullname != $cashLedgerObj->accname) {
                            // Full name is different with old one.
                            Account::where('id', '=', Session::get('userid'))->update(array(
                                'fullname' => $fullname,
                            ));

                            Accountdetail::where('accid', '=', Auth::user()->get()->id)->update(array(
                                'fullname' => $fullname,
                            ));

                            Session::put('fullname', $fullname);
                            $cashLedgerObj->accname = $fullname;
                        }
                    }
					
					if($cashLedgerObj->save())
					{
						if($hasbonus){
							$promoCashObj = new Promocash;
							$promoCashObj->accid 	= $cashLedgerObj->accid;
							$promoCashObj->acccode  = $cashLedgerObj->acccode;
							$promoCashObj->chtcode  = CBO_CHARTCODE_DEPOSIT;
							$promoCashObj->pcpid 	= $promoObj->id;
							$promoCashObj->crccode  = $cashLedgerObj->crccode;
							$promoCashObj->type     = $promoObj->type;
							$promoCashObj->amount 	= $promoObj->getPromoCashAmount($cashLedgerObj->amount,$cashLedgerObj->accid);
							$promoCashObj->amountlocal = Currency::getLocalAmount($promoCashObj->amount, $promoCashObj->crccode);
							$promoCashObj->crcrate = Currency::getCurrencyRate($promoCashObj->crccode);
							$promoCashObj->clgid = $cashLedgerObj->id;
							$promoCashObj->remarkcode = $promoObj->code;
							$promoCashObj->status = CBO_PROMOCASHSTATUS_CONFIRMED;
							$promoCashObj->createdip = App::getRemoteIp();
							$promoCashObj->save();
							
						}
					}
				
			}
			
			$this->checkRotaeBankAccount();
			
			echo json_encode( array( 'code' => Lang::get('COMMON.SUCESSFUL') ) );
		
	}

    public function depositPaymentGatewayProcess(Request $request){

        $hasbonus = false;
        $inputs = [
            'promo'	            => $request->input('promo'),
            'amount'            => $request->input('amount'),
            'type'              => $request->input('type'),
            'minutes'           => $request->input('minutes'),
            'hours'             => $request->input('hours'),
            'date' 	            => $request->input('date'),
            'rules'             => $request->input('rules'),
            'deposit_pay_channel'    => $request->input('deposit_pay_channel'),
            'deposit_pay_channel_option' => $request->input('deposit_pay_channel_option'),
        ];

        $rules = [
            'promo'             => 'alpha_num',
            'amount'            => 'required|decimal|min:1',
            'type'              => 'numeric',
            'minutes'           => 'numeric',
            'hours'             => 'numeric',
            'date' 	            => 'date',
            'rules'             => 'required',
            'deposit_pay_channel'    => 'required',
            'deposit_pay_channel_option' => 'required',
        ];
		
        if (Config::get('setting.opcode') == 'IFW' && Session::get('currency') == 'TWD') {
            if (strlen(Session::get('fullname')) < 1) {
                // User does not provide full name yet.
                $inputs['fullname'] = trim($request->input('fullname'));
                $rules['fullname'] = 'required';
            }

            if (strlen(Auth::user()->get()->withdrawalcode) < 1) {
                // User does not provide withdrawal code yet.
                $inputs['withdrawalcode'] = $request->input('withdrawalcode');
                $inputs['confirmwithdrawalcode'] = $request->input('confirmwithdrawalcode');
                $rules['withdrawalcode'] = 'required|size:4|same:confirmwithdrawalcode';
            }
        }

        $validator = Validator::make($inputs, $rules);
        $validator->setAttributeNames(array(
            'promo'                     => Lang::get('public.Promotion'),
            'amount'                    => Lang::get('public.Amount'),
            'refno'                     => Lang::get('public.DepositReceiptReferenceNo'),
            'type'                      => Lang::get('public.DepositMethod'),
            'minutes'                   => Lang::get('COMMON.MINUTES'),
            'hours'                     => Lang::get('COMMON.HOURS'),
            'date' 	                    => Lang::get('COMMON.DATE'),
            'rules'                     => Lang::get('public.DepositPromoRules'),
            'withdrawalcode'            => Lang::get('public.WithdrawalCode'),
            'confirmwithdrawalcode'     => Lang::get('public.ConfirmWithdrawalCode'),
            'bank'                      => Lang::get('public.DepositBank'),
            'deposit_pay_channel'       => Lang::get('public.PaymentChannel'),
            'deposit_pay_channel_option' => Lang::get('public.PaymentChannelOption'),
        ));

        if ($validator->fails())
        {
            echo json_encode($validator->errors());
            exit;
        }

        if (Config::get('setting.opcode') == 'IFW' && Session::get('currency') == 'TWD') {
            // IFW TWD member required to setup withdrawal code.
            if (strlen(Auth::user()->get()->withdrawalcode) < 1 && strlen($request->input('withdrawalcode')) > 0) {
                if (!preg_match('/[0-9]{4}/', $request->input('withdrawalcode'))) {
                    echo json_encode( array( 'code' => Lang::get('public.WithdrawalCodeMustBe4Digits') ) );
                    exit;
                }

                Account::where('id', '=', Session::get('userid'))->update(array(
                    'withdrawalcode' => $request->input('withdrawalcode'),
                ));
            }

            // IFW TWD member required to upload bankbook for first deposit.
            if (Media::where('refobj', '=', 'Bankbook')->where('refid', '=', Session::get('userid'))->count() < 1) {
                if (!$request->hasFile('file_bankbook')) {
                    echo json_encode( array( 'code' => Lang::get('public.PleaseUploadBankbookPicture') ) );
                    exit;
                }
            }
        }

        $paymentGatewaySettingObj = Paymentgatewaysetting::where('code', '=', $request->input('deposit_pay_channel'))->first();
        $pgClsObj = null;

        if (!$paymentGatewaySettingObj) {
            echo json_encode( array( 'code' => Lang::get('public.UnsupportedPaymentChannel') ) );
            exit;
        } elseif ($paymentGatewaySettingObj->is_private == 0) {
            try {
                $pgClassName = $paymentGatewaySettingObj->class_name;
                $pgClsObj = new $pgClassName(Session::get('currency'));
            } catch (\Exception $e) {
                echo json_encode( array( 'code' => Lang::get('public.UnsupportedPaymentChannel') ) );
                exit;
            }
        }

        if ($paymentGatewaySettingObj->deposit_min > $request->input('amount')) {
            echo json_encode( array( 'code' => Lang::get('public.MinimumAmount').': '.number_format($paymentGatewaySettingObj->deposit_min, 2) ) );
            exit;
        }

        if ($paymentGatewaySettingObj->deposit_max < $request->input('amount')) {
            echo json_encode( array( 'code' => Lang::get('public.MaximumAmount').': '.number_format($paymentGatewaySettingObj->deposit_max, 2) ) );
            exit;
        }

        $fdmObj = Fundingmethod::find(Configs::getParam('SYSTEM_PAYMENT_ID_PG'));

        if( Cashledger::whereAccid(Session::get('userid'))->whereChtcode(CBO_CHARTCODE_DEPOSIT)->whereFdmid($fdmObj->id)->whereStatus(0)->count() >= 20 ){
            echo json_encode( array( 'code' => Lang::get('public.FailedTooManyPendingTransaction') ) );
            exit;
        }

        if( $request->has('promo') && $request->promo != '0' ) {
            $promos = Promocampaign::getPromoByMemberId(Session::get('userid'));
            if($promoObj = Promocampaign::whereRaw('startdate <= "'.date('Y-m-d H:i:s').'" AND enddate >= "'.date('Y-m-d H:i:s').'" AND status = '.CBO_STANDARDSTATUS_ACTIVE.' AND code = "'.$request->promo.'"')->first()){
                if(isset($promos[$promoObj->id])) {
                    $hasbonus = true;
                } else { echo json_encode( array( 'code' => Lang::get('COMMON.FAILED') ) );exit; }
            } else{ echo json_encode( array( 'code' => Lang::get('COMMON.FAILED') ) );exit; }
        }

        $pgsObj = Paymentgatewaysetting::where('code', '=', $request->input('deposit_pay_channel'))->first();

        if (!$pgsObj) {
            echo json_encode( array( 'code' => Lang::get('public.InvalidDepositMethod') ) );
            exit;
        }

        $paymentObj 					= new Paymentgateway();
        $paymentObj->paytype			= $pgsObj->id;
        $paymentObj->paybnkoption		= $request->input('deposit_pay_channel_option');
        $paymentObj->fdmid 				= $fdmObj->id;
        $paymentObj->type 				= CBO_CHARTCODE_DEPOSIT;
        $paymentObj->accid 				= Session::get('userid');
        $paymentObj->acccode 			= Session::get('acccode');
        $paymentObj->crccode 			= Session::get('currency');
        $paymentObj->crcrate 			= Currency::getCurrencyRate($paymentObj->crccode);
        $paymentObj->amount 			= $request->amount;
        $paymentObj->amountlocal 		= Currency::getLocalAmount($request->amount, $paymentObj->crccode);
        //$paymentObj->fee 				= round($fdmObj->getFee(CBO_CHARTCODE_DEPOSIT, $paymentObj->amount), 2);
        //$paymentObj->feelocal 			= Currency::getLocalAmount($paymentObj->fee, $paymentObj->crccode);
        $paymentObj->status 			= CBO_LEDGERSTATUS_PENDING;
        $paymentObj->createdip			= App::getRemoteIp();
        $paymentObj->remark_code 		= $request->input('remarkcode', '');
        $paymentObj->bnkid			= $request->input('bank', 0);

        if($paymentObj->save())
        {
            $cashLedgerObj = new Cashledger;
            $cashLedgerObj->accid 			= $paymentObj->accid;
            $cashLedgerObj->acccode 		= $paymentObj->acccode;
            $cashLedgerObj->accname			= Session::get('fullname');
            $cashLedgerObj->chtcode 		= CBO_CHARTCODE_DEPOSIT;
            $cashLedgerObj->cashbalance 	= self::getAllBalance();
            $cashLedgerObj->amount 			= $paymentObj->amount;
            $cashLedgerObj->amountlocal 	= $paymentObj->amountlocal;
            $cashLedgerObj->refid 			= $paymentObj->id;
            $cashLedgerObj->remarkcode 		= $request->input('remarkcode', '');
            $cashLedgerObj->status 			= $paymentObj->status;
            $cashLedgerObj->refobj 			= 'Paymentgateway';
            $cashLedgerObj->crccode 		= $paymentObj->crccode;
            $cashLedgerObj->crcrate 		= Currency::getCurrencyRate($paymentObj->crccode);
            $cashLedgerObj->fdmid			= $fdmObj->id;
            $cashLedgerObj->bnkid			= 0;
            $cashLedgerObj->fee 			= round($fdmObj->getFee(CBO_CHARTCODE_DEPOSIT, $paymentObj->amount), 2);
            $cashLedgerObj->feelocal 		= Currency::getLocalAmount($paymentObj->fee, $paymentObj->crccode);
            $cashLedgerObj->bacid			= 0;
            $cashLedgerObj->createdip		= $paymentObj->createdip;
            $cashLedgerObj->instanttransflag = $pgsObj->instant_trans_flag;

            if (Config::get('setting.opcode') == 'IFW' && Session::get('currency') == 'TWD' && strlen($cashLedgerObj->accname) < 1) {
                $fullname = trim($request->input('fullname'));

                if ($fullname != $cashLedgerObj->accname) {
                    // Full name is different with old one.
                    Account::where('id', '=', $paymentObj->accid)
                        ->update(array(
                            'fullname' => $fullname,
                        ));

                    Session::put('fullname', $fullname);
                    $cashLedgerObj->accname = $fullname;
                }
            }

            if($cashLedgerObj->save())
            {
                if($hasbonus){
                    $promoCashObj = new Promocash;
                    $promoCashObj->accid 	= $cashLedgerObj->accid;
                    $promoCashObj->acccode  = $cashLedgerObj->acccode;
                    $promoCashObj->chtcode  = CBO_CHARTCODE_DEPOSIT;
                    $promoCashObj->pcpid 	= $promoObj->id;
                    $promoCashObj->crccode  = $cashLedgerObj->crccode;
                    $promoCashObj->type     = $promoObj->type;
                    $promoCashObj->amount 	= $promoObj->getPromoCashAmount($cashLedgerObj->amount);
                    $promoCashObj->amountlocal = Currency::getLocalAmount($promoCashObj->amount, $promoCashObj->crccode);
                    $promoCashObj->crcrate = Currency::getCurrencyRate($promoCashObj->crccode);
                    $promoCashObj->clgid = $cashLedgerObj->id;
                    $promoCashObj->remarkcode = $promoObj->code;
                    $promoCashObj->status = CBO_PROMOCASHSTATUS_CONFIRMED;
                    $promoCashObj->createdip = App::getRemoteIp();
                    $promoCashObj->save();
                }

                // Upload file.
                $imageMemes = array('image/jpeg', 'image/jpg', 'image/png', 'image/gif');
                $fileKeys = array(
                    'file' => array(
//                        'refobj' => 'Banktransfer',
                        'refobj' => 'PGtransfer',
                        'refid' => $paymentObj->id,
                        'uploadDir' => 'deposit',
                    ),
//                    'file_bankbook' => array(
//                        'refobj' => 'Bankbook',
//                        'refid' => $paymentObj->accid,
//                        'uploadDir' => 'bankbook',
//                    ),
                );

                foreach ($fileKeys as $fKey => $fVals) {
                    if( $request->hasFile($fKey)) {
                        $file = $request->file($fKey);
                        $clientName = md5($file->getClientOriginalName().time()).'.'. $file->getClientOriginalExtension();
                        $filename = $fVals['uploadDir'].'/'.$clientName;
                        $memeType = $file->getMimeType();

                        if (Config::get('setting.front_path') == 'idnwin99' && in_array($memeType, $imageMemes) && $file->getSize() > 4 * pow(1024, 2)) {
                            // Only open for IDNWIN99 currently.
                            // If upload file is image, and file size is over 4MB (AWS limit), do compress image.
                            $tempFilename = 'tempUpload/'.$clientName;
                            $uploaded = Storage::disk('local')->put($tempFilename, File::get($file));

                            if ($uploaded) {
                                // File uploaded to temp folder, now compress uploaded image file.
                                $tempSource = Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix() . '/' . $tempFilename;
                                $image = null;

                                if ($memeType == 'image/jpeg' || $memeType == 'image/jpg') {
                                    $image = imagecreatefromjpeg($tempSource);
                                } elseif ($memeType == 'image/gif') {
                                    $image = imagecreatefromgif($tempSource);
                                } elseif ($memeType == 'image/png') {
                                    $image = imagecreatefrompng($tempSource);
                                }

                                imagejpeg($image, $tempSource, 90);     // Compress quality to certain %
                                $file = $tempSource;                    // Set compressed file path to be uploaded to AWS
                            }
                        }

                        $result = Storage::disk('s3')->put($filename,  File::get($file), 'public');

                        if($result) {
                            $medObj = new Media;
                            $medObj->refobj = $fVals['refobj'];
                            $medObj->refid  = $fVals['refid'];
                            $medObj->type   = Media::TYPE_IMAGE;
                            $medObj->domain = 'http://'.Configs::getParam('SYSTEM_AWS_S3BUCKET');
                            $medObj->path   = $filename;
                            $medObj->save();
                        }
                    }
                }

                if (!is_null($pgClsObj)) {
                    // Call payment gateway deposit API to process order.
//                $paymentObj->orderid = Carbon::now()->format('YmdHisu');
                    $paymentObj->orderid = $pgClsObj->formatOrderId($paymentObj->id);
                    $paymentObj->save();

                    $pgResult = $pgClsObj->deposit($paymentObj);

                    if ($pgResult !== false) {
                        echo json_encode($pgResult);
                        exit;
                    } else {
                        $paymentObj->status = CBO_LEDGERSTATUS_CANCELLED;
                        $paymentObj->save();

                        $cashLedgerObj->status = CBO_LEDGERSTATUS_CANCELLED;
                        $cashLedgerObj->save();

                        echo json_encode( array( 'code' => Lang::get('public.PaymentFailedPleaseTryOtherChannel') ) );
                        exit;
                    }
                } else {
                    $paymentObj->orderid = $request->input('refno', '');
                    $paymentObj->save();

                    echo json_encode( array( 'code' => Lang::get('COMMON.SUCESSFUL') ) );
                    exit;
                }
            }

        }

//        echo json_encode( array( 'code' => Lang::get('COMMON.SUCESSFUL') ) );
        echo json_encode( array( 'code' => Lang::get('public.PaymentFailedPleaseTryOtherChannel') ) );
    }

    public function getPgBankDetails(Request $request)
    {
        $paymentGatewaySettingObj = Paymentgatewaysetting::where('code', '=', $request->input('deposit_pay_channel'))->first();
        $pgClsObj = null;

        if (!$paymentGatewaySettingObj) {
            echo json_encode( array( 'code' => Lang::get('public.UnsupportedPaymentChannel') ) );
            exit;
        } elseif ($paymentGatewaySettingObj->is_private == 0 && !empty($paymentGatewaySettingObj->class_name)) {
            try {
                $pgClassName = $paymentGatewaySettingObj->class_name;
                $pgClsObj = new $pgClassName(Session::get('currency'));
            } catch (\Exception $e) {
                echo json_encode( array( 'code' => Lang::get('public.UnsupportedPaymentChannel') ) );
                exit;
            }
        }

        $resJson = array();

        if (!is_null($pgClsObj)) {
            $resJson = $pgClsObj->getBankDetailOptions($request->input('type'));
        }

        if (!is_array($resJson)) {
            echo json_encode(array('code' => array()));
        } else {
            echo json_encode(array('code' => $resJson));
        }

        exit;
    }
	
	public function withdraw(Request $request){

        if( (Config::get('setting.front_path') == 'dadu') && Account::whereRaw('id='.Session::get('userid'))->pluck('isemailvalid') == 0)	return Redirect::route('memberverify');
                
		if( (Config::get('setting.front_path') == 'ampm' || Config::get('setting.front_path') == 'dadu' || Config::get('setting.front_path') == 'c59' || Config::get('setting.front_path') == '369bet' || Config::get('setting.opcode') == 'GSC') && Accountbank::whereAccid(Session::get('userid'))->count() == 0 ){	
		return Redirect::route('memberbank');
		
		}
		
		if( Config::get('setting.front_path') == 'ampm' || Config::get('setting.front_path') == 'dadu' ){
			$banklist = $this->BankList(1,1);
		}else{
			$banklist = $this->BankList(1,2);
		}
		
		$data['banklists'] = $banklist;
		
		if( Session::get('currency') == "CNY" ){
			if( $banks = Bank::whereStatus(1)->get() )
			{
				foreach($banks as $bnkObj){
					$banklists[$bnkObj->name] = array
					(
							'code'		=> $bnkObj->code
					);
				}
			}
			$data['banklists'] = $banklists;
			
			$data['binded_banks'] = Accountbank::whereAccid( Session::get('userid') )->get();
		}
		
		$data['bankcode']  = '';
		$data['bankaccno'] = '';
		
		if( $accountbank = Accountbank::whereAccid( Session::get('userid') )->first() ){
			$data['accbank'] = $accountbank;
			$data['bankcode']  = Bank::whereId( $accountbank->bankid )->pluck('code') ;
			$data['bankaccno'] = $accountbank->bankaccno;
		}
                
                $data['withdrawlimit'] = Account::whereRaw('id='.Session::get('userid'))->pluck('withdrawlimit');

                $min  = 0;
                $max  = 0;

                $bankObj = Bank::where('status', '=', 1)
                    ->where('currencycode', '=', Session::get('currency'))
                    ->first();

                if ($bankObj) {
                    $min = $bankObj->minwithdraw;
                    $max = $bankObj->maxwithdraw;
                }

                $data['min'] = App::formatNegativeAmount($min);
                $data['max'] = App::formatNegativeAmount($max);
		
		$data['content'] = Article::where('code','=','USER_WITHDRAW')
            ->where('crccode', '=', Session::get('currency'))
            ->where('lang','=',Lang::getLocale())
            ->pluck('content');
			
		$acdObj = Accountdetail::where( 'accid' , '=' , Session::get('userid') )->first();
		$data['fullname'] = '';
		if($acdObj->fullname){
			$data['fullname']=$acdObj->fullname;
		}
		
        
        if ($request->input('as_json') == true) {
            return response(json_encode($data));
        }
        
		return view(Config::get('setting.front_path').'/withdraw',$data);
	}	
	
	public function withdrawProcess(Request $request){

		$validator = Validator::make(
			[
	
				'bank'	      => $request->input('bank'),
				'accountno'   => $request->input('accountno'),
				'amount'      => $request->input('amount'),
			],
			[

				'bank' 	      => 'required',
				'accountno'   => 'required',
				'amount'      => 'required|decimal|min:1',

			]
		);

		$validator->setAttributeNames(array(
            'bank' 	      => Lang::get('public.BankName'),
            'accountno'   => Lang::get('public.BankAccountNo'),
            'amount'      => Lang::get('public.Amount'),
        ));
		
		if ($validator->fails())
		{
			echo json_encode($validator->errors());
			exit;
		}

		
		if($request->input('amount') > WalletController::mainwallet(false)) {
			echo json_encode( array( 'amount' => Lang::get('COMMON.ERROR_NETAMOUNT') ) );
			exit;
		}
		
		if( Cashledger::whereAccid(Session::get('userid'))->whereChtcode(CBO_CHARTCODE_WITHDRAWAL)->whereStatus(0)->count() >= 1 ){
			echo json_encode( array( 'code' => Lang::get('COMMON.FAILED') ) );
			exit;
		}

		if (Config::get('setting.opcode') == 'IFW' && Session::get('currency') == 'TWD') {
            if ($request->input('withdrawalcode') != Auth::user()->get()->withdrawalcode) {
                echo json_encode( array( 'code' => Lang::get('public.IncorrectWithdrawalCode') ) );
                exit;
            }
        }
		
		if (Config::get('setting.opcode') == 'GSC'){
				if($accountObj = Account::where('id', '=', Session::get('userid'))->first()) {
					if($request->input('amount') > $accountObj->maxwithdrawamt){
						echo json_encode( array( 'amount' => Lang::get('COMMON.ERROR_ALLOWWITHDRAWAL') ) );
						exit;
					}
				}
		}

		$success = false;



			if($request->input('amount') > 0) {

				$from = date('Y-m-d').' 00:00:00';
				$to   = date('Y-m-d').' 23:59:59';
				$withdrawl_count = Cashledger::whereRaw('accid = '.Session::get('userid').' AND chtcode = '.CBO_CHARTCODE_WITHDRAWAL.' AND created >= "'.$from.'" AND created <= "'.$to.'" AND status NOT IN ('.CBO_LEDGERSTATUS_CANCELLED.', '.CBO_LEDGERSTATUS_MANCANCELLED.', '.CBO_LEDGERSTATUS_MEMCANCELLED.')')->count();
				$hasLimit = true;

				if (Config::get('setting.opcode') == 'GSC' && Session::get('withdrawlimit') == 0) {
				    // GSC allow unlimited withdraw.
				    $hasLimit = false;
                }

				if($hasLimit && $withdrawl_count >= Session::get('withdrawlimit')) {
					echo json_encode( array( 'code' => Lang::get('COMMON.ERROR_ALLOWWITHDRAWAL') ) );
					exit;
				}
			}
			
			
			if($bnkObj = Bank::whereRaw('code = "'.$request->input('bank').'"')->first()) {
				
				if($request->input('amount') < $bnkObj->minwithdraw){
					echo json_encode( array( 'amount' => Lang::get('COMMON.ERROR_MINWITHDRAWAL') ) );
					exit;
				}
					
				if($request->input('amount') > $bnkObj->maxwithdraw){
					echo json_encode( array( 'amount' => Lang::get('COMMON.MAXWITHDRAWAL') ) );
					exit;
				}
			
				if(!$accBnkObj = Accountbank::whereRaw('bankid = '.$bnkObj->id.' AND accid = '. Session::get('userid').' AND bankaccno = "'. $request->input('accountno').'"')->first()) {
					
					$accBnkObj = new Accountbank;
					$accBnkObj->accid 		= Session::get('userid');
					$accBnkObj->bankid 	    = $bnkObj->id;
					$accBnkObj->acccode	    = Session::get('acccode');
					$accBnkObj->bankaccname = Session::get('fullname');
					$accBnkObj->bankcode 	= $bnkObj->code;
					$accBnkObj->bankname 	= $bnkObj->name;
					$accBnkObj->bankaccno 	= $request->input('accountno');
					if($request->has('province'))
					{
						$accBnkObj->bankprovince= $request->input('province');
						$accBnkObj->bankcity 	= $request->input('city');
						$accBnkObj->bankdistrict= $request->input('district');
						$accBnkObj->bankholdingbranch= $request->input('bankbranch');
					}
					
					$accBnkObj->save();
				}
				
				$fdmObj = Fundingmethod::find(1);
	
				$paymentObj = new Banktransfer;
				$paymentObj->fdmid = $fdmObj->id;
				$paymentObj->type = CBO_CHARTCODE_WITHDRAWAL;
				$paymentObj->bnkid = $accBnkObj->bankid;
				$paymentObj->accid = Session::get('userid');
				$paymentObj->acccode = Session::get('acccode');
				$paymentObj->crccode = Session::get('currency');
				if($request->has('province'))
				{
					$paymentObj->bankprovince = $request->input('province');
					$paymentObj->bankcity = $request->input('city');
					$paymentObj->bankdistrict = $request->input('district');
					$paymentObj->bankbranch = $request->input('bankbranch');
				}
				
				$paymentObj->crcrate = Currency::getCurrencyRate($paymentObj->crccode);
				
				if( Config::get('setting.front_path') == 'sbm' )
				{
					$paymentObj->amount 	 = -1 * ( $request->input('amount') * 95 / 100 );
					$paymentObj->amountlocal = -1 * Currency::getLocalAmount(( $request->input('amount') * 95 / 100 ), $paymentObj->crccode);
				}
				elseif ( Config::get('setting.opcode') == 'IFW' && Session::get('currency') == 'TWD' )
                {
                    // Withdraw over 2 times per week will charge 5%
                    $withdrawl_count = Cashledger::where('accid', '=', Session::get('userid'))
                        ->where('chtcode', '=', CBO_CHARTCODE_WITHDRAWAL)
                        ->whereBetween('created', array(Carbon::parse('last sunday'), Carbon::now()))
                        ->whereNotIn('status', array(CBO_LEDGERSTATUS_CANCELLED, CBO_LEDGERSTATUS_MANCANCELLED, CBO_LEDGERSTATUS_MEMCANCELLED))
                        ->count();

                    if ($withdrawl_count > 2) {
                        // Minimum charge 100, maximum charge 1000.
                        // Charges calculation: 30 (fixed) + 1% of withdraw amount
                        $charges = 30 + ($request->input('amount') * 0.01);

                        if ($charges < 100) {
                            $charges = 100;
                        } elseif ($charges > 1000) {
                            $charges = 1000;
                        }

                        $paymentObj->amount 	 = -1 * ( $request->input('amount') - $charges );
                        $paymentObj->amountlocal = -1 * Currency::getLocalAmount(( $request->input('amount') - $charges ), $paymentObj->crccode);
                    } else {
                        // Normal.
                        $paymentObj->amount 	 = -1 * $request->input('amount');
                        $paymentObj->amountlocal = -1 * Currency::getLocalAmount($request->input('amount'), $paymentObj->crccode);
                    }
                }
				else
				{
					$paymentObj->amount 	 = -1 * $request->input('amount');
					$paymentObj->amountlocal = -1 * Currency::getLocalAmount($request->input('amount'), $paymentObj->crccode);
				}
			
				
				$paymentObj->bankaccno = $accBnkObj->bankaccno;
				$paymentObj->bankaccname = $accBnkObj->bankaccname;
				$paymentObj->status = CBO_LEDGERSTATUS_PENDING;
				$paymentObj->createdip		= App::getRemoteIp();;
				
				if ($paymentObj->save()) {
					
					$cashLedgerObj = new Cashledger;
					$cashLedgerObj->accid 			= $paymentObj->accid;
					$cashLedgerObj->acccode 		= $paymentObj->acccode;
					$cashLedgerObj->accname			= Session::get('fullname');
					$cashLedgerObj->chtcode 		= CBO_CHARTCODE_WITHDRAWAL;
					$cashLedgerObj->cashbalance 	= self::getAllBalance();
					$cashLedgerObj->amount 			= $paymentObj->amount;
					$cashLedgerObj->amountlocal 	= $paymentObj->amountlocal;
					$cashLedgerObj->refid 			= $paymentObj->id;
					$cashLedgerObj->status 			= CBO_LEDGERSTATUS_PENDING;
					$cashLedgerObj->refobj 			= 'Banktransfer';
					$cashLedgerObj->crccode 		= $paymentObj->crccode;
					$cashLedgerObj->crcrate 		= Currency::getCurrencyRate($paymentObj->crccode);
					$cashLedgerObj->fdmid			= $fdmObj->id;
					$cashLedgerObj->bnkid			= $paymentObj->bnkid;
					$cashLedgerObj->fee 			= round($fdmObj->getFee(CBO_CHARTCODE_DEPOSIT, $paymentObj->amount), 2);
					$cashLedgerObj->feelocal 		= Currency::getLocalAmount($paymentObj->fee, $paymentObj->crccode);
					$cashLedgerObj->bacid			= 0;
					$cashLedgerObj->createdip		= $paymentObj->createdip;
					
					if($cashLedgerObj->save())
					{
						if( Config::get('setting.front_path') == 'sbm' )
						{
							$cashLedgerObj = new Cashledger;
							$cashLedgerObj->accid 			= $paymentObj->accid;
							$cashLedgerObj->acccode 		= $paymentObj->acccode;
							$cashLedgerObj->accname			= Session::get('fullname');
							$cashLedgerObj->chtcode 		= CBO_CHARTCODE_WITHDRAWALCHARGES;
							$cashLedgerObj->cashbalance 	= self::getAllBalance();
							$cashLedgerObj->amount 			= -1 * ( $request->input('amount') * 5 / 100 );
							$cashLedgerObj->amountlocal 	= -1 * Currency::getLocalAmount(( $request->input('amount') * 5 / 100 ), $paymentObj->crccode);
							$cashLedgerObj->refid 			= $paymentObj->id;
							$cashLedgerObj->status 			= CBO_LEDGERSTATUS_PENDING;
							$cashLedgerObj->refobj 			= 'Banktransfer';
							$cashLedgerObj->crccode 		= $paymentObj->crccode;
							$cashLedgerObj->crcrate 		= Currency::getCurrencyRate($paymentObj->crccode);
							$cashLedgerObj->fdmid			= $fdmObj->id;
							$cashLedgerObj->bnkid			= $paymentObj->bnkid;
							$cashLedgerObj->fee 			= round($fdmObj->getFee(CBO_CHARTCODE_DEPOSIT, $paymentObj->amount), 2);
							$cashLedgerObj->feelocal 		= Currency::getLocalAmount($paymentObj->fee, $paymentObj->crccode);
							$cashLedgerObj->bacid			= 0;
							$cashLedgerObj->createdip		= $paymentObj->createdip;
							$cashLedgerObj->save();
						}
						$success = true;

					}
				}
				
			}
			
		if($success){
			echo json_encode( array( 'code' => Lang::get('COMMON.SUCESSFUL') ) );
		}else{
			echo json_encode( array( 'code' => Lang::get('COMMON.FAILED') ) );
		}
	}
	
	public function transaction(Request $request){

		
		$data['content'] = Article::where('code','=','TRANSACTION_ENQUIRY')->where('lang','=',Lang::getLocale())->pluck('content');
		return view(Config::get('setting.front_path').'/transaction_enquiry',$data);
		
	}
	
	public function profitloss(Request $request){

		
		$data['content'] = Article::where('code','=','TRANSACTION_ENQUIRY')->where('lang','=',Lang::getLocale())->pluck('content');
		return view(Config::get('setting.front_path').'/profitloss_enquiry',$data);
		
	}
	
	public function profitlossProcess(Request $request){
		
		$rows = array();
		
		if( $request->has('group_by') ){
			
			$profitlosses = DB::select( DB::raw("SELECT date, SUM(wager) as sumwager, SUM(totalstake) as sumstake, SUM(amount) as sumwinloss, SUM(validstake) as sumvalid, prdid
											FROM `profitloss`
											WHERE accid = ".Session::get('userid')." and date = '".$request->date_from."' 
											GROUP BY prdid 
											"));
			foreach( $profitlosses as $profitlosse ){

				$rows[] = array(
					'product'  			=> Product::whereId($profitlosse->prdid)->pluck('code'), 
					'wager'  			=> $profitlosse->sumwager,
					'stake'  			=> App::formatNegativeAmount($profitlosse->sumstake),
					'winloss'  			=> App::formatNegativeAmount($profitlosse->sumwinloss),
					'validstake'  		=>'<a href="javascript:void(0)" onClick="window.open(\''.route('betslip',array( 'from' => $profitlosse->date , 'to' => $profitlosse->date , 'prdid' => $profitlosse->prdid )).'\', \'betslip\', \'width=1150,height=830\')">'.App::formatNegativeAmount($profitlosse->sumvalid).'</a>',

				);	
			}
			
		}else{
		
			$profitlosses = DB::select( DB::raw("SELECT date, SUM(wager) as sumwager, SUM(totalstake) as sumstake, SUM(amount) as sumwinloss, SUM(validstake) as sumvalid, prdid
											FROM `profitloss`
											WHERE accid = ".Session::get('userid')." and date >= '".$request->date_from."' and date <= '".$request->date_to."'
											GROUP BY date 
											"));
			foreach( $profitlosses as $profitlosse ){

				$rows[] = array(
					'date'  			=> '<a href="javascript:void(0)" onClick="transaction_history_product(\''.$profitlosse->date.'\','.$profitlosse->prdid.')">'.$profitlosse->date.'</a>', 
					'wager'  			=> $profitlosse->sumwager,
					'stake'  			=> App::formatNegativeAmount($profitlosse->sumstake),
					'winloss'  			=> App::formatNegativeAmount($profitlosse->sumwinloss),
					'validstake'  		=> $profitlosse->sumvalid,

				);	
			}
		
		}
		echo json_encode($rows);
		
	}
	
	public function betslip(Request $request){
		
		$data['prdid']			= $request->prdid;
		$data['from']	= $request->from;
		$data['to']		= $request->to;
		
		return view(Config::get('setting.front_path').'/betslip',$data);
	}
	
	public function betslipData(Request $request){

		$from 		= $request->has('createdfrom')?$request->get('createdfrom'):App::getDateTime(11);
		$to   		= $request->has('createdto')?$request->get('createdto'):App::getDateTime(3);
		$timefrom   = $request->has('timefrom')?$request->get('timefrom'):'00:00:00';
		$timeto     = $request->has('timeto')?$request->get('timeto'):'23:59:59';
		
		if( $request->get('prdid') == 0 )
			$condition  = 'datetime >= "'.$from.'" AND datetime <= "'.$to.'" AND accid = '.Session::get('userid').'';
		else
			$condition  = 'datetime >= "'.$from.' '.$timefrom.'" AND datetime <= "'.$to.' '.$timeto.'" AND prdid = '.$request->get('prdid').' AND accid = '.Session::get('userid').'';

		$total = Wager::whereRaw($condition)->count();

		$rows = array();
		$footer = array();
		$totalstake = 0;
		$totalwinloss = 0;
		$totalvalid = 0;
	
		if($betslips = Wager::whereraw($condition)->skip($request->recordstartindex)->take( $request->pagesize )->orderBy('id','desc')->get()) {
			
			$vendors = Product::getAllAsOptions();
			$products = Product::getCategoryOptions();
			foreach($betslips as $betslip) {
				//$winloss = $betslip->profitloss;
				$valid = $betslip->validstake;
				
				$betslip->stake 	 = $betslip->crccode == 'VND' || $betslip->crccode == 'IDR' ? $betslip->stake * 1000 : $betslip->stake;
				$betslip->validstake = $betslip->crccode == 'VND' || $betslip->crccode == 'IDR' ? $betslip->validstake * 1000 : $betslip->validstake;
				$betslip->profitloss = $betslip->crccode == 'VND' || $betslip->crccode == 'IDR' ? $betslip->profitloss * 1000 : $betslip->profitloss;
				
				$rows[] = array(
					'id' => $betslip->id,
					'date' => $betslip->datetime,
					'member' => $betslip->nickname,
					'vendor' => $vendors[$betslip->prdid],
					'product' => $products[$betslip->category],
					'detail' => '#'.$betslip->category.'-'.$betslip->id.'-'.$betslip->refid,
					'currency' => $betslip->crccode,
					'stake' => App::formatNegativeAmount($betslip->stake),
					'winloss' => App::formatNegativeAmount($betslip->profitloss),
					'valid' => App::formatNegativeAmount($betslip->validstake),
					'result' => $betslip->getResultText(),
				);
				$totalvalid += $betslip->validstake;
				$totalstake += $betslip->stake;
				$totalwinloss += $betslip->profitloss;
				
			}
		}
		
		$footer[] = array(
			'currency' => Lang::get('COMMON.TOTAL'),
			'stake' => App::formatNegativeAmount($totalstake),
			'winloss' => App::formatNegativeAmount($totalwinloss),
			'valid' => App::formatNegativeAmount($totalvalid),
		);
		echo json_encode(array('total' => $total, 'rows' => $rows, 'footer' => $footer));
		exit;
		
	}
	
	public function transactionProcess(Request $request){
		
		$rows = array();
		
		if( $request->has('record_type') && $request->input('record_type') == 0 ){
			$chtcode = CBO_CHARTCODE_DEPOSIT.','.CBO_CHARTCODE_WITHDRAWAL.','.CBO_CHARTCODE_BONUS.','.CBO_CHARTCODE_INCENTIVE.','.CBO_CHARTCODE_CASHCARD;
		}else{
			$chtcode = $request->input('record_type');
		}
	

		$cashledgers = Cashledger::where('accid','=',Session::get('userid'))->
								   where('created','>',$request->date_from.' 00:00:00')->
								   where('created','<',$request->date_to.' 23:59:59')->
								   whereRaw('chtcode IN('.$chtcode.')')->take(25)->orderBy( 'id' , 'desc')->get();
                
                    foreach( $cashledgers as $cashledger ){
                        if( Config::get('setting.front_path') == '369bet' )
                        {
                            $typename = '';
                            if($cashledger->chtcode == CBO_CHARTCODE_BONUS){
                                //$refobj = $cashledger->refobj;
                                
//                                select a.refid, c.name as refname
//                                from cashledger a 
//                                inner join promocash as b on b.id = a.refid
//                                inner join promocampaign as c on c.id = b.pcpid
                                
                                //$bonusname = 'select name from promocampaign where id IN (select pcpid from '.$refobj.')';
                                
                                $obj = DB::table('promocash as pcsh')
                                        ->join('promocampaign as pcp', 'pcp.id', '=', 'pcsh.pcpid')
                                        ->where('pcsh.id', '=', $cashledger->refid)
                                        ->first(['pcp.name']);
                                
                                if ($obj) {
                                    $typename = ' [ '.$obj->name.' ]';
                                }
                                
                            }
                            $type = Cashledger::getTypeText($cashledger->chtcode);
                            $amount = ( $cashledger->amount < 0 ) ? $cashledger->amount * -1 : $cashledger->amount;
                            $rows[] = array(
                                    'id'  			=> $cashledger->id, 
                                    'created'  			=> $cashledger->created->toDateTimeString(), 
                                    'status'  			=> $cashledger->getStatusText(),
                                    'amount'  			=> App::displayAmount($amount),
                                    'rejreason'  		=> $cashledger->rejreason,
                                    'type'  			=> $type.$typename,
                                    //'type'  			=> $type,
                            );	
                        }
                        else{
                            $rejReason = '';
							$cardno    = '';
							
                            if ($cashledger->status == CBO_LEDGERSTATUS_MANCANCELLED) {
                                // Only show reject reason when status is rejected by admin.
                                $rejReason = $cashledger->rejreason;
                            }
							
							if( $cashledger->refobj == 'Withdrawcard' && $cashledger->status == 4 ){
								$cardno = Withdrawcard::whereId($cashledger->refid)->pluck('number');
							}

                            $type = Cashledger::getTypeText($cashledger->chtcode);
                            $amount = ( $cashledger->amount < 0 ) ? $cashledger->amount * -1 : $cashledger->amount;
                            $rows[] = array(
                                    'id'  		=> $cashledger->id, 
                                    'created'  	=> $cashledger->created->toDateTimeString(), 
                                    'status'  	=> $cashledger->getStatusText(),
                                    'amount'  	=> App::displayAmount($amount),
                                    'rejreason' => $rejReason,
                                    'type'  	=> $type,
                                    'cardno'  	=> $cardno,
                            );
                        }
                    }
                
		echo json_encode($rows);
		
	}	
	
	public function transfer(Request $request){
		
		
		$data['content'] = Article::where('code','=','TRANSFER')->where('lang','=',Lang::getLocale())->pluck('content');
		return view(Config::get('setting.front_path').'/transfer',$data);
	}
	
	public function transfertomain(Request $request){
		return view(Config::get('setting.front_path').'/transfertomain');
	}
	
	
	public function BankList( $method , $type ){
		
		$data = array();
		
		$fdmid   = $method 	? $method : Configs::getParam('SYSTEM_PAYMENT_ID_BANKTRANSFER');
		$type    = $type 	? $type  : 1;
		
		$min = $max = 0;
		if( $fmdObj = Fundingmethod::find($fdmid) ) {
			if($type == 1) 
			$banks = unserialize($fmdObj->depositbanks);
			else if($type == 2)
			$banks = unserialize($fmdObj->withdrawbanks);
			
			if( $fmsObj = Fundingmethodsetting::whereRaw('fdmid='.$fdmid.' AND crccode = "'.Session::get('currency').'"')->first() ) {
				if($type == 1)  {
					$min = $fmsObj->mindeposit;
					$max = $fmsObj->maxdeposit;
				} else if($type == 2) {
					$min = $fmsObj->minwithdraw;
					$max = $fmsObj->maxwithdraw;
				}
			}
		}
		
		$bhdid = 0;
		if( $agtObj = Agent::find(Session::get('agtid')) ) {
			$bhdid = $agtObj->bhdid;
		}
		
		$condition = 'bhdid = '.$bhdid.' AND crccode="'.Session::get('currency').'" AND status = '.CBO_STANDARDSTATUS_ACTIVE;
		
		if(  Config::get('setting.front_path') == 'ampm' || Config::get('setting.front_path') == 'dadu'){
			
			$condition .= ' AND bnkid IN ( select bankid from accountbank where acccode = "'.Session::get('acccode').'" )';

		}
		
		if($type == 1) {
			$condition .= ' AND deposit = 1';
			$minfield = 'mindeposit';
			$maxfield = 'maxdeposit';
		} else if($type == 2) {
			$condition .= ' AND withdraw = 1';
			$minfield = 'minwithdraw';
			$maxfield = 'maxwithdraw';
		}
		
		if( $bankAccounts = Bankaccount::whereRaw($condition)->get() ) 
		{
			foreach($bankAccounts as $bankAccount) 
			{
				if( $bnkObj = Bank::find($bankAccount->bnkid) )
				{
	
					$data[$bnkObj->name] = array
					(
							'code'		=> $bnkObj->code,
							'name'		=> $bankAccount->bankaccname,
							'account'	=> $bankAccount->bankaccno,
							'branch'	=> $bankAccount->holdingbranch,
							'min'		=> $bnkObj->$minfield,
							'max'		=> $bnkObj->$maxfield,
							'image'		=> Media::getImage($bnkObj),
					);
				}
			}
		}
		return $data;
	}	
	
	
	public function BonusList() {

		$list = Promocampaign::getPromoByMemberId(Session::get('userid'));
		
		if(count($list) > 0) 
		{
			foreach($list as $pcpid => $promo) 
			{
				if( $pcpObj = Promocampaign::whereRaw($pcpid)->first() ) 
				{
					$data[$pcpid] = array
					(
							'code'		=> $promo['code'],
							'name'		=> $promo['name'],
							'min'		=> $promo['min'],
							'image'		=> Media::getImage($pcpObj),
							'thumbnail'	=> Media::getImage($pcpObj, Media::TYPE_THUMBNAIL), 
							'desc'		=> $promo['desc'], 
					);
				}
			}
			
		}
		
		var_dump($data);
		
	}
	
	public function WalletList(Request $request, $currency) {
		
	
		$data['wallet'][0] = array(
									'code'	 => 'MAIN',
									'name'	 => '',
									'active' => 1,
									'desc'	 => '',
							);
							
		$products = Session::get('products');
		if(count($products) > 0) {

			$exclude_myr = array('CT8', 'WFT');
			$exclude_vnd = array('PSB', 'PLT');
			$exclude_idr = array('WFT', 'CT8', 'PSB');
			foreach($products as $prdid) {
				if( $prdObj = Product::find($prdid) ) {
					if($currency == 'MYR' && in_array($prdObj->code, $exclude_myr)) continue;
					if($currency == 'VND' && in_array($prdObj->code, $exclude_vnd)) continue;
					if($currency == 'IDR' && in_array($prdObj->code, $exclude_idr)) continue;
                                        
					if($prdObj->status == Product::STATUS_ACTIVE) {
						$data['wallet'][$prdObj->id] = array(
									'code'	 => $prdObj->code,
									'name'	 => $prdObj->desc,
									'active' => $prdObj->status,
									'desc'	 => $prdObj->desc,
							);
					}
				}
			}
		}
		var_dump($data);
		
	}
	
	public static function getAllBalance(){
		
		 $total = 0;
		
		$productObj = Product::select(['code','status'])->get();
		
	    foreach( $productObj as $object )
		{
			$total += Cache::has(Session::get('username').'_'.strtoupper($object->code).'_balance') ? (float)Cache::get(Session::get('username').'_'.strtoupper($object->code).'_balance') : 0;
		}  
	 
	/* 	if( $products = unserialize(Website::where( 'code' , '=' , Session::get('opcode') )->pluck('products')) ){
				
			foreach( $products as $prdid ){
				
				$prdObj = Product::find($prdid);
				
				$className  = "App\libraries\providers\\".$code;
				$obj 		= new $className;
				$temp       = $obj->getBalance( Session::get('username') , Session::get('currency') );
				
				$temp = ($temp == false) ? 0 : $temp;
                $temp = Session::get('currency') == 'VND' || Session::get('currency') == 'IDR' ? $temp * 1000 : $temp;
				
				$total +=  $temp ;
				
				
			}
		}  */

		
		$total += WalletController::getBalance(Session::get('userid'));
		
		return $total;
	}
	
	private function checkRotaeBankAccount(){
		
		$user = Auth::user()->get();
		
		$bhdid = 0;
		if($agtObj = Agent::find(Session::get('agtid'))) {
			$bhdid = $user->bhdid == 0 ? $agtObj->bhdid : $user->bhdid;
		}
		
		$condition = '`limit` != 0 AND bhdid = '.$bhdid.' AND crccode="'.Session::get('currency').'" AND bankaccount.status = '.CBO_STANDARDSTATUS_ACTIVE;
	
		if( $bankaccounts = Bankaccount::whereRaw($condition)->get() )
		{
			foreach( $bankaccounts as $bankaccount )
			{
				$credit = Bankledger::whereRaw('bacid = '.$bankaccount->id.' AND type = '.Bankledger::TYPE_CREDIT)->sum('amount');

				$debit  = Bankledger::whereRaw('bacid = '.$bankaccount->id.' AND type = '.Bankledger::TYPE_DEBIT)->sum('amount');
				
				$balance = abs($credit) - abs($debit);
				
				if( $balance > $bankaccount->limit )
				{

					Bankaccount::where( 'id' , '=' , $bankaccount->id )->
								 update(
									 array(
										 'bhdid'	 => 0,
									 )
								 );
					Bankaccount::where( 'id' , '=' , $bankaccount->tobnkid )->
								 update(
									 array(
										 'bhdid'	 => $bankaccount->bhdid,
									 )
								 );
				}
				
			}
			
		}
		
		
	}
		public function TransactionCount() {
		if(  Auth::user()->check()){
			if(cashledger::whereIn( 'status' , array(1,4))->where( 'accid' , '=' , Session::get('userid'))->count('id'))
			{
					echo "1";
			}else{
					echo "2";
			}
		}
		
	}


	
}
