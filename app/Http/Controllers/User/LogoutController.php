<?php namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller\User;
use App\libraries\Login;
use Auth;
use Session;
use Illuminate\Http\Request;
use App\Models\Onlinetracker;
use Redirect;

class LogoutController extends Controller{
	
	public function __construct()
	{
		
	}

	public function index(Request $request)
	{
		Onlinetracker::whereType(CBO_LOGINTYPE_USER)->whereUserid(Session::get('userid'))->delete();
		Session::flush();
		Auth::user()->logout();
		return Redirect::route('homepage')->with('message', 'Login Failed');
	}	
	

	
}
