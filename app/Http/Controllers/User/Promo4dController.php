<?php namespace App\Http\Controllers\User;

use Illuminate\Http\Request;

use Session;
use Cache;
use Lang;
use App\Models\Article;
use App\Models\Media;
use App\Models\Betpsbresult;
use App\Models\Banner;
use App\Models\Currency;
use App\Models\Product;
use App\Models\Luckydrawledger;
use App\Models\Luckydrawtrans;
use App\Models\Luckydraw;
use App\Models\Configs;
use App\Models\Promocampaign;
use App\Models\Account;
use App\Models\Promocash;
use App\Models\Cashledger;
use Config;
use Crypt;
use App;
use DB;
use App\Jobs\ChangeLocale;


class Promo4dController extends Controller{

	public function __construct()
	{

	}

	public function index(Request $request){

		$luckydraw = new Luckydrawledger;

		if( Session::has('userid') ){
			$this->addTokenFirstTimeLogin();
		}

		$data['balance'] = $luckydraw->getBalance( Session::get('userid') , Configs::getParam('SYSTEM_4D_DRAWNO') );
		$data['drawno'] = Configs::getParam('SYSTEM_4D_DRAWNO')-1;


		if ($this->isMobileSite()) {
			return view(Config::get('setting.front_path').'/4d_promo',$data);
		}else{
			return view(Config::get('setting.front_path').'/4d_promo_desktop',$data);
		}
	}

	public function addTokenFirstTimeLogin(){

		$today_login_token = Luckydrawledger::where( 'accid' ,     '=' , Session::get('userid') )->
											  where( 'draw_no' ,   '=' , Configs::getParam('SYSTEM_4D_DRAWNO') )->
											  whereBetween( 'created_at', array( date('Y-m-d') . ' 00:00:00', date('Y-m-d') . ' 23:59:59') )->
											  where( 'logintype' , '=' , 1 )->sum('amount');

		if( $today_login_token <= 0 )
		{

			$object = Account::find(Session::get('userid'));

			$luckydraw = new Luckydrawledger;
			$luckydraw->draw_no    = Configs::getParam('SYSTEM_4D_DRAWNO');
			$luckydraw->accid      = $object->id;
			$luckydraw->acccode    = $object->code;
			$luckydraw->amount     = 1;
			$luckydraw->refid      = 0;
			$luckydraw->object     = 'Free';
			$luckydraw->logintype  = 1;
			$luckydraw->save();

		}
	}



	public function u_spin_4d(Request $request){

		$timestamp = time();

		if(
			(date('D', $timestamp) === 'Wed' && ( date('G', $timestamp) == '18' || date('G', $timestamp) == '19')) ||
			(date('D', $timestamp) === 'Sat' && ( date('G', $timestamp) == '18' || date('G', $timestamp) == '19')) ||
			(date('D', $timestamp) === 'Sun' && ( date('G', $timestamp) == '18' || date('G', $timestamp) == '19'))
		)
		{
			exit;
		}

		$luckydraws = Luckydrawtrans::select('betno')->where( 'drawno' , '=' , Configs::getParam('SYSTEM_4D_DRAWNO') )->get();
		$excludes = array();

		foreach( $luckydraws as $luckydraw ){
			$excludes[] = $luckydraw->betno;
		}

		$luckydraw = new Luckydrawledger;

		$continue = true;

		if( $luckydraw->getBalance( Session::get('userid') , Configs::getParam('SYSTEM_4D_DRAWNO') ) > 0 )
		{

			$default = $request->time == 0 ? 1.5 : 3;

			while($continue)
			{
				$draw_number = str_pad($this->random_exclude(0, 9999,$excludes),4,"0",STR_PAD_LEFT);
				if( Luckydrawtrans::where( 'drawno' , '=' , Configs::getParam('SYSTEM_4D_DRAWNO') )->where( 'betno' , '=' , $draw_number )->count() >= 1 ){
					$continue = true;
				}else{
					$continue = false;
				}
			}

			$data['return_code'][0] = $draw_number[0]+$default;
			$data['return_code'][1] = $draw_number[1]+$default;
			$data['return_code'][2] = $draw_number[2]+$default;
			$data['return_code'][3] = $draw_number[3]+$default;

			$luckydraw = new Luckydrawtrans;
			$luckydraw->accid   = Session::get('userid');
			$luckydraw->acccode = Session::get('acccode');
			$luckydraw->drawno  = Configs::getParam('SYSTEM_4D_DRAWNO') ;
			$luckydraw->betno   = $draw_number;

			if($luckydraw->save())
			{
				$luckydrawledger = new Luckydrawledger;
				$luckydrawledger->draw_no = Configs::getParam('SYSTEM_4D_DRAWNO') ;
				$luckydrawledger->accid   = Session::get('userid');
				$luckydrawledger->acccode = Session::get('acccode');
				$luckydrawledger->amount  = -1;
				$luckydrawledger->refid   = $luckydraw->id;
				$luckydrawledger->object  = 'Luckydrawtrans';
				$luckydrawledger->save();
			}
		}

		echo json_encode($data);
	}

	private function random_exclude($min, $max, $exarr = array()) {

		if ($max - count($exarr) < $min) {
			return false;
		}

		// $pos is the position that the random number will take
		// of all allowed positions
		$pos = rand(0, $max - $min - count($exarr));

		// $num being the random number
		$num = $min;

		// while $pos > 0, step to the next position
		// and decrease if the next position is available
		for ($i = 0; $i < count($exarr); $i += 1) {

			// if $num is on an excluded position, skip it
			if ($num == $exarr[$i]) {
				$num += 1;
				continue;
			}

			$dif = $exarr[$i] - $num;

			// if the position is after the next excluded number,
			// go to the next excluded number
			if ($pos >= $dif) {
				$num += $dif;

				// -1 because we're now at an excluded position
				$pos -= $dif - 1;
			} else {
				// otherwise, return the free position
				return $num + $pos;
			}
		}

		// return the number plus the open positions we still had to go
		return $num + $pos;
	}

	public function u_spin_4d_count(Request $request){

		$data = array();

		$luckydraw = new Luckydrawledger;
		$data['allspin']= $luckydraw->getBalance( Session::get('userid') , Configs::getParam('SYSTEM_4D_DRAWNO') );


		echo json_encode($data);
	}

	public function u_spin_4d_current_bet(Request $request){

		$data = array();

		if( $luckydraws = Luckydrawtrans::select('betno')->where( 'accid' , '=' , Session::get('userid') )->where( 'drawno' , '=' , Configs::getParam('SYSTEM_4D_DRAWNO') )->take(5)->get() )
		{
			$i = 0;

			foreach( $luckydraws as $luckydraw )
			{
				$data['cb'][$i++]['r'] = $luckydraw->betno;
			}
		}

		echo json_encode($data);
	}

	public function u_spin_4d_history(Request $request){

		$data = array();

		if( $luckydraws = Luckydrawtrans::where( 'accid' , '=' , Session::get('userid') )->get() )
		{
			$i = 0;

			foreach( $luckydraws as $luckydraw )
			{
				$drawdate = Luckydraw::where( 'id' , '=' , $luckydraw->drawno )->pluck('draw_date');

				$data['hl'][$i]['l']    = $luckydraw->created_at->toDateTimeString();
				$data['hl'][$i]['dd']   = $drawdate;
				$data['hl'][$i]['r']    = $luckydraw->betno;
				$data['hl'][$i++]['wc'] = $luckydraw->prize;
			}
		}

		echo json_encode($data);
	}

	public function u_winner_lucky_4d(Request $request){

		$data = array();
		$count = 0;
		$drawno = Configs::getParam('SYSTEM_4D_DRAWNO');
        $drawdate = Configs::getParam('SYSTEM_PROMO4D_WINNER_LIST_DATE');
        $drawdatestrlen = strlen($drawdate);

        // Use dummy winner list, disabled auto add real winner for now.
//		while( $count <= 0 ){
//
//			if( $drawno != 1 )
//				$drawno -= 1;
//
//            $builder = Luckydrawtrans::where( 'drawno' , '=' , $drawno )
//                ->where( 'status' , '=' , 1 )
//                ->where( 'prize'  , '>' , 0 );
//
//            $luckydraws = $builder->get();
//
//			if( count($luckydraws) > 0 )
//			{
//				foreach( $luckydraws as $luckydraw ){
//					$drawdatestr = substr(Luckydraw::whereRaw(' id = '.$luckydraw->drawno)->pluck('draw_date') , 0 , 10 );
//
//					if ($drawdatestrlen < 1) {
//                        $drawdate = $drawdatestr;
//                        $drawdatestrlen = strlen($drawdate);
//                    }
//
//					$data['d']['mem'][$luckydraw->prize][$count][0] = '***'.substr($luckydraw->acccode, 6);
//					$data['d']['mem'][$luckydraw->prize][$count++][1] = $luckydraw->prize.' ('.$drawdatestr.')';
//				}
//			} else {
//                $data['d']['mem'] = array();
//                $count++;
//            }
//
//		}

		$prizeDummyList = array(
		    2500 => Configs::getParam('SYSTEM_PROMO4D_WINNER_LIST_A'),
            1000 => Configs::getParam('SYSTEM_PROMO4D_WINNER_LIST_B'),
            500 => Configs::getParam('SYSTEM_PROMO4D_WINNER_LIST_C'),
            200 => Configs::getParam('SYSTEM_PROMO4D_WINNER_LIST_D'),
            60 => Configs::getParam('SYSTEM_PROMO4D_WINNER_LIST_E'),
        );

        foreach ($prizeDummyList as $key => $val) {
            if (isset($prizeDummyList[$key]) && strlen($prizeDummyList[$key]) > 0) {
                if ($key == 2500 || $key == 1000 || $key == 500) {
                    // Only have 1 winner.
                    if (!isset($data['d']['mem'][$key])) {

                        $data['d']['mem'][$key][0][0] = '***'.substr($val, 6);
                        $data['d']['mem'][$key][0][1] = $key . ' (' . $drawdate . ')';
                    }
                } else {
                    // Have multiple winners.
                    $confVals = explode(',', $prizeDummyList[$key]);
                    $count = 0;

                    if (isset($data['d']['mem'][$key])) {
                        $count = count($data['d']['mem'][$key]);
                    }

                    $addedWinnerName = array();

                    foreach ($confVals as $c) {
                        if ($count >= 10) {
                            break;
                        } elseif (in_array($c, $addedWinnerName)) {
                            break;
                        }

                        $addedWinnerName[] = $c;

                        $data['d']['mem'][$key][$count][0] = '***'.substr($c, 6);
                        $data['d']['mem'][$key][$count++][1] = $key . ' (' . $drawdate . ')';
                    }
                }
            }
        }


	    $data['status'] = 0;
		$data['msg'] = 'Done';
/* 		$data['d']['mem'][2500][0][0] = '***';
		$data['d']['mem'][2500][0][1] = '2500';
		$data['d']['mem'][1000][0][0] = '***';
		$data['d']['mem'][1000][0][1] = '1000';
		$data['d']['mem'][500][0][0] = '***';
		$data['d']['mem'][500][0][1] = '500';
		$data['d']['mem'][200][0][0] = '***';
		$data['d']['mem'][200][0][1] = '200';
		$data['d']['mem'][200][1][0] = '***';
		$data['d']['mem'][200][1][1] = '200';
		$data['d']['mem'][200][2][0] = '***';
		$data['d']['mem'][200][2][1] = '200';
		$data['d']['mem'][200][3][0] = '***';
		$data['d']['mem'][200][3][1] = '200';
		$data['d']['mem'][200][4][0] = '***';
		$data['d']['mem'][200][4][1] = '200';
		$data['d']['mem'][200][5][0] = '***';
		$data['d']['mem'][200][5][1] = '200';
		$data['d']['mem'][200][6][0] = '***';
		$data['d']['mem'][200][6][1] = '200';
		$data['d']['mem'][200][7][0] = '***';
		$data['d']['mem'][200][7][1] = '200';
		$data['d']['mem'][200][8][0] = '***';
		$data['d']['mem'][200][8][1] = '200';
		$data['d']['mem'][200][9][0] = '***';
		$data['d']['mem'][200][9][1] = '200';

		$data['d']['mem'][60][0][0] = '***';
		$data['d']['mem'][60][0][1] = '200';
		$data['d']['mem'][60][1][0] = '***';
		$data['d']['mem'][60][1][1] = '200';
		$data['d']['mem'][60][2][0] = '***';
		$data['d']['mem'][60][2][1] = '200';
		$data['d']['mem'][60][3][0] = '***';
		$data['d']['mem'][60][3][1] = '200';
		$data['d']['mem'][60][4][0] = '***';
		$data['d']['mem'][60][4][1] = '200';
		$data['d']['mem'][60][5][0] = '***';
		$data['d']['mem'][60][5][1] = '200';
		$data['d']['mem'][60][6][0] = '***';
		$data['d']['mem'][60][6][1] = '200';
		$data['d']['mem'][60][7][0] = '***';
		$data['d']['mem'][60][7][1] = '200';
		$data['d']['mem'][60][8][0] = '***';
		$data['d']['mem'][60][8][1] = '200';
		$data['d']['mem'][60][9][0] = '***';
		$data['d']['mem'][60][9][1] = '200';
		 */
		$data['d']['wwlist'][] = '2500';
		$data['d']['wwlist'][] = '1000';
		$data['d']['wwlist'][] = '500';
		$data['d']['wwlist'][] = '200';
		$data['d']['wwlist'][] = '60';



		echo json_encode($data);
		/* 	echo '{"status":0,"msg":"Done","d":{"mem":{"2500":[["***","2,500"]],"1000":[["james7***","1,000"]],"500":[["***","500"]],"200":[["da9***","200"],["***","200"],["***","200"],["***","200"],["***","200"],["***","200"],["***","200"],["***","200"],["***","200"],["***","200"]],"60":[["kht6***","60"],["***","60"],["***","60"],["***","60"],["***","60"],["***","60"],["***","60"],["***","60"],["***","60"],["***","60"]]},"wwlist":[2500,1000,500,200,60]}}'; */
	}

	public function get_result(){

		if( date('D') == 'Sat' || date('D') == 'Sun' || date('D') == 'Wed' )
		{

			if( Luckydraw::where( 'draw_date' , '=' ,  date("Y-m-d").' 20:00:00' )->count() == 0 )
			{

				$lkdObj = new Luckydraw;
				$lkdObj->draw_date = date("Y-m-d").' 20:00:00';
				$lkdObj->status    = 0;
				$lkdObj->save();

			}
		}

		if( $luckydraw = Luckydrawtrans::where('status' ,'=' , 0 )->first() )
		{
			$date = substr( Luckydraw::where( 'id' , '=' ,  $luckydraw->drawno )->pluck('draw_date') , 0 , 10 );

			if( $results = Betpsbresult::where( 'DrawDate' , '=' , $date.' 00:00:00')->where( 'GameName' , '=' , 'Toto' )->first() )
			{
				Luckydrawtrans::where( 'drawno' , '=' , $luckydraw->drawno )->
								where( 'betno' , '=' , $results->FirstPrizeNumber )->
								update( [ 'status' => 1 , 'prize' => 2500] );

				Luckydrawtrans::where( 'drawno' , '=' , $luckydraw->drawno )->
								where( 'betno' , '=' , $results->SecondPrizeNumber )->
								update( [ 'status' => 1 , 'prize' => 1000] );

				Luckydrawtrans::where( 'drawno' , '=' , $luckydraw->drawno )->
								where( 'betno' , '=' , $results->ThirdPrizeNumber )->
								update( [ 'status' => 1 , 'prize' => 500] );

				$specialPrizenumbers     = explode( ',' , $results->SpecialPrizeNumber);

				foreach( $specialPrizenumbers as $specialPrizenumber )
				{
					Luckydrawtrans::where( 'drawno' , '=' , $luckydraw->drawno )->
									where( 'betno' , '=' , $specialPrizenumber )->
									update( [ 'status' => 1 , 'prize' => 200 ] );
				}

				$consolationPrizenumbers = explode( ',' , $results->ConsolationPrizeNumber);

				foreach( $consolationPrizenumbers as $consolationPrizenumber )
				{
					Luckydrawtrans::where( 'drawno' , '=' , $luckydraw->drawno )->
									where( 'betno' , '=' , $consolationPrizenumber )->
									update( [ 'status' => 1 , 'prize' => 60 ] );
				}

				Luckydrawtrans::where( 'drawno' , '=' , $luckydraw->drawno )->
								where( 'status' , '=' , 0 )->
								update( [ 'status' => 1 , 'prize' => 0 ] );

				Luckydraw::where( 'id' , '=' , $luckydraw->drawno )->update( [ 'status' => 1 ] );
			}

		}
		if( isset( $luckydraw->drawno) ){
			if( $luckydraw_settles = Luckydrawtrans::where( 'drawno' , '=' , $luckydraw->drawno)->where( 'prize' , '>' , 0)->get() )
			{
				foreach( $luckydraw_settles as $luckydraw_settle )
				{
					if( $pcpObj = Promocampaign::whereRaw(' code = "4dpromo" ')->first() )
					{
						$accObj = Account::whereRaw('code = "'.$luckydraw_settle->acccode.'"')->first();
						$promoCashObj = new Promocash;
						$promoCashObj->type			= Promocash::TYPE_MANUAL;
						$promoCashObj->accid	 	= $accObj->id;
						$promoCashObj->acccode	 	= $accObj->code;
						$promoCashObj->crccode	 	= $accObj->crccode;
						$promoCashObj->crcrate	 	= Currency::getCurrencyRate($accObj->crccode);
						$promoCashObj->amount	 	= $luckydraw_settle->prize;
						$promoCashObj->amountlocal 	= Currency::getLocalAmount($luckydraw_settle->prize, $accObj->crccode);
						$promoCashObj->pcpid		= $pcpObj->id;
						$promoCashObj->status		= Promocash::STATUS_PENDING;

						if($promoCashObj->save()) {

							$cashLedgerObj = new Cashledger;
							$cashLedgerObj->accid 			= $promoCashObj->accid;
							$cashLedgerObj->acccode 		= $promoCashObj->acccode;
							$cashLedgerObj->accname 		= $accObj->fullname;
							$cashLedgerObj->chtcode 		= CBO_CHARTCODE_BONUS;
							$cashLedgerObj->amount 			= $promoCashObj->amount;
							$cashLedgerObj->amountlocal 	= $promoCashObj->amountlocal;
							$cashLedgerObj->fee 			= $promoCashObj->amount;
							$cashLedgerObj->feelocal 		= $promoCashObj->amountlocal;
							$cashLedgerObj->crccode 		= $promoCashObj->crccode;
							$cashLedgerObj->crcrate 		= $promoCashObj->crcrate;
							$cashLedgerObj->refobj			= 'Promocash';
							$cashLedgerObj->refid			= $promoCashObj->id;
							$cashLedgerObj->status			= CBO_LEDGERSTATUS_PENDING;
							$cashLedgerObj->save();

							echo json_encode( array( 'status' => 'Success' ) );

						} else{
							echo json_encode( array( 'status' => 'Fail' ) );

						}
					}
				}
			}

		}

	}

	public function updateDrawNo(){

		Configs::updateParam( 'SYSTEM_4D_DRAWNO' , Configs::getParam('SYSTEM_4D_DRAWNO') + 1);
	}


}
