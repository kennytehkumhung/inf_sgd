<?php namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller\User;
use App\Models\Cashledger;
use App\Models\Currency;
use App\Models\Account;
use App\Models\Worldcupteam;
use App\Models\Worldcuptoken;
use App\Models\Worldcuptrans;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\libraries\App;
use Session;
use Lang;
use Validator;
use Storage;
use File;
use Cache;
use Config;
use Auth;
use Redirect;
use DB;
use Log;
use App\Jobs\ChangeLocale;



class WorldCupController extends Controller{
	
	public function __construct()
	{
		
	}
	
        public function WcLobby(Request $request){
            $rows = array();
            if($trans = Worldcuptrans::whereRaw('nickname = "'.Session::get('username').'" AND crccode ="'.Session::get('user_currency').'" AND category_id != 0')->orderBy('category_id','ASC')->orderBy('created','DESC')->get()){
                foreach($trans as $tran){
                    $rows[] = array(
                            'id'     	=> $tran->id, 
                            'category'     => Worldcupteam::getCategoryText($tran->category_id), 
                            'selection'   => Worldcupteam::getTeamText($tran->category_id,$tran->team_id), 
                            'status'	=> $tran->getStatusText($tran->status), 
                            'date'   	=> $tran->created, 
                    );
                }
            }
            $data['ledgers'] = $rows;
            $data['total_token'] = Worldcuptoken::where('nickname','=', Session::get('username'))->pluck('token_amount');
            return view(Config::get('setting.front_path').'/wc/wc_lobby',$data);
        }
        
        public function WcPlaylist(Request $request){
            $data['total_token'] = Worldcuptoken::where('nickname','=', Session::get('username'))->pluck('token_amount');
            return view(Config::get('setting.front_path').'/wc/wc_playlist',$data);
        }
        
        public function WcTnc(Request $request){
            $data['total_token'] = Worldcuptoken::where('nickname','=', Session::get('username'))->pluck('token_amount');
            return view(Config::get('setting.front_path').'/wc/wc_tnc',$data);
        }
	
        public function WcWinner(Request $request){
            $rows = array();
            if($trans = Worldcuptrans::whereRaw('nickname = "'.Session::get('username').'" AND crccode ="'.Session::get('user_currency').'" AND category_id = 1')->get()){
                foreach($trans as $tran){
                    $rows[] = array(
                        'team_id' => $tran->team_id,
                    );
                }
            }
            $data['team_id'] = $rows;
            $data['total_token'] = Worldcuptoken::where('nickname','=', Session::get('username'))->pluck('token_amount');
            $data['token'] = 3 - Worldcuptoken::where('nickname','=', Session::get('username'))->pluck('category_one');
            return view(Config::get('setting.front_path').'/wc/wc_winner',$data);
        }
        
        public function WcWinnerProcess(Request $request){
            
            if(Worldcuptoken::where('nickname','=', $request->username)->pluck('token_amount') == 0){
                echo json_encode( array( 'code' => Lang::get('COMMON.FAILED')) );
                exit();
            }
            
            $teams = $request->team;
            $current_token = Worldcuptoken::where('nickname','=', $request->username)->pluck('category_one');
            $total_token = Worldcuptoken::where('nickname','=', Session::get('username'))->pluck('token_amount');
            
            if(isset($teams) && count($teams) > 0){
                if((count($teams)+ $current_token) <= 3 && $total_token >= count($teams) ){
                    foreach($teams as $team){
                        if(Worldcuptrans::where('nickname','=',$request->username)->where('category_id', '=', 1)->where('team_id','=', $team)->count() == 0){
                            $object = new Worldcuptrans;
                            $object->accid = Session::get('userid');
                            $object->nickname = $request->username;
                            $object->crccode = Session::get('user_currency');
                            $object->category_id = 1;
                            $object->team_id = (int) $team;
                            $object->status = 0;
                            if($object->save()){
                                $wctObj = Worldcuptoken::where('nickname','=',$request->username)->first();
                                if($wctObj){
                                    $wctObj->category_one += 1;
                                    $wctObj->token_amount -= 1;
                                    $wctObj->save();
                                }
                            }
                        }
                    }
                    echo json_encode( array( 'code' => Lang::get('COMMON.SUCESSFUL')));
                }
                else
                    echo json_encode( array( 'code' => Lang::get('COMMON.FAILED')) );
            }
        }
        
        public function WcFinalists(Request $request){
            $data['total_token'] = Worldcuptoken::where('nickname','=', Session::get('username'))->pluck('token_amount');
            $data['token'] = 7 - Worldcuptoken::where('nickname','=', Session::get('username'))->pluck('category_two');
            return view(Config::get('setting.front_path').'/wc/wc_finalist',$data);
        }
        
        public function WcFinalistsProcess(Request $request){
            
            if(Worldcuptoken::where('nickname','=', $request->username)->pluck('token_amount') == 0){
                echo json_encode( array( 'code' => Lang::get('COMMON.FAILED')) );
                exit();
            }
            
            $teams = $request->team;
            $current_token = Worldcuptoken::where('nickname','=', $request->username)->pluck('category_two');
            $total_token = Worldcuptoken::where('nickname','=', Session::get('username'))->pluck('token_amount');
            
            if(isset($teams) && count($teams) > 0){
                if((count($teams)+ $current_token) <= 7 && $total_token >= count($teams)){
                    foreach($teams as $team){
                        if(Worldcuptrans::where('nickname','=',$request->username)->where('category_id', '=', 2)->where('team_id','=', $team)->count() == 0){
                            $object = new Worldcuptrans;
                            $object->accid = Session::get('userid');
                            $object->nickname = $request->username;
                            $object->crccode = Session::get('user_currency');
                            $object->category_id = 2;
                            $object->team_id = (int) $team;
                            $object->status = 0;
                            if($object->save()){
                                $wctObj = Worldcuptoken::where('nickname','=',$request->username)->first();
                                if($wctObj){
                                    $wctObj->category_two += 1;
                                    $wctObj->token_amount -= 1;
                                    $wctObj->save();
                                }
                            }
                        }
                    }
                    echo json_encode( array( 'code' => Lang::get('COMMON.SUCESSFUL')));
                }
                else
                    echo json_encode( array( 'code' => Lang::get('COMMON.FAILED')) );
            }
        }
        
        public function WcFinal(Request $request){
            $data['total_token'] = Worldcuptoken::where('nickname','=', Session::get('username'))->pluck('token_amount');
            $data['token'] = 4 - Worldcuptoken::where('nickname','=', Session::get('username'))->pluck('category_three');
            return view(Config::get('setting.front_path').'/wc/wc_teamtofinal',$data);
        }
        
        public function WcFinalProcess(Request $request){
            
            if(Worldcuptoken::where('nickname','=', $request->username)->pluck('token_amount') == 0){
                echo json_encode( array( 'code' => Lang::get('COMMON.FAILED')) );
                exit();
            }
            
            $teams = $request->team;
            $current_token = Worldcuptoken::where('nickname','=', $request->username)->pluck('category_three');
            $total_token = Worldcuptoken::where('nickname','=', Session::get('username'))->pluck('token_amount');
            
            if(isset($teams) && count($teams) > 0){
                if((count($teams)+ $current_token) <= 4 && $total_token >= count($teams)){
                    foreach($teams as $team){
                        if(Worldcuptrans::where('nickname','=',$request->username)->where('category_id', '=', 3)->where('team_id','=', $team)->count() == 0){
                            $object = new Worldcuptrans;
                            $object->accid = Session::get('userid');
                            $object->nickname = $request->username;
                            $object->crccode = Session::get('user_currency');
                            $object->category_id = 3;
                            $object->team_id = (int) $team;
                            $object->status = 0;
                            if($object->save()){
                                $wctObj = Worldcuptoken::where('nickname','=',$request->username)->first();
                                if($wctObj){
                                    $wctObj->category_three += 1;
                                    $wctObj->token_amount -= 1;
                                    $wctObj->save();
                                }
                            }
                        }
                    }
                    echo json_encode( array( 'code' => Lang::get('COMMON.SUCESSFUL')));
                }
                else
                    echo json_encode( array( 'code' => Lang::get('COMMON.FAILED')) );
            }
        }
        
        public function WcSemiFinal(Request $request){
            $data['total_token'] = Worldcuptoken::where('nickname','=', Session::get('username'))->pluck('token_amount');
            $data['token'] = 4 - Worldcuptoken::where('nickname','=', Session::get('username'))->pluck('category_four');
            return view(Config::get('setting.front_path').'/wc/wc_teamtosemifinal',$data);
        }

        public function WcSemiFinalProcess(Request $request){
            
            if(Worldcuptoken::where('nickname','=', $request->username)->pluck('token_amount') == 0){
                echo json_encode( array( 'code' => Lang::get('COMMON.FAILED')) );
                exit();
            }
            
            $teams = $request->team;
            $current_token = Worldcuptoken::where('nickname','=', $request->username)->pluck('category_four');
            $total_token = Worldcuptoken::where('nickname','=', Session::get('username'))->pluck('token_amount');
            
            if(isset($teams) && count($teams) > 0){
                if((count($teams)+ $current_token) <= 4 && $total_token >= count($teams)){
                    foreach($teams as $team){
                        if(Worldcuptrans::where('nickname','=',$request->username)->where('category_id', '=', 4)->where('team_id','=', $team)->count() == 0){
                            $object = new Worldcuptrans;
                            $object->accid = Session::get('userid');
                            $object->nickname = $request->username;
                            $object->crccode = Session::get('user_currency');
                            $object->category_id = 4;
                            $object->team_id = (int) $team;
                            $object->status = 0;
                            if($object->save()){
                                $wctObj = Worldcuptoken::where('nickname','=',$request->username)->first();
                                if($wctObj){
                                    $wctObj->category_four += 1;
                                    $wctObj->token_amount -= 1;
                                    $wctObj->save();
                                }
                            }
                        }
                    }
                    echo json_encode( array( 'code' => Lang::get('COMMON.SUCESSFUL')));
                }
                else
                    echo json_encode( array( 'code' => Lang::get('COMMON.FAILED')) );
            }
        }
        
        public function WcQuarterFinal(Request $request){
            $data['total_token'] = Worldcuptoken::where('nickname','=', Session::get('username'))->pluck('token_amount');
            $data['token'] = 4 - Worldcuptoken::where('nickname','=', Session::get('username'))->pluck('category_five');
            return view(Config::get('setting.front_path').'/wc/wc_teamtoquarterfinal',$data);
        }
        
        public function WcQuarterFinalProcess(Request $request){
            
            if(Worldcuptoken::where('nickname','=', $request->username)->pluck('token_amount') == 0){
                echo json_encode( array( 'code' => Lang::get('COMMON.FAILED')) );
                exit();
            }
            
            $teams = $request->team;
            $current_token = Worldcuptoken::where('nickname','=', $request->username)->pluck('category_five');
            $total_token = Worldcuptoken::where('nickname','=', Session::get('username'))->pluck('token_amount');
            
            if(isset($teams) && count($teams) > 0){
                if((count($teams)+ $current_token) <= 4 && $total_token >= count($teams)){
                    foreach($teams as $team){
                        if(Worldcuptrans::where('nickname','=',$request->username)->where('category_id', '=', 5)->where('team_id','=', $team)->count() == 0){
                            $object = new Worldcuptrans;
                            $object->accid = Session::get('userid');
                            $object->nickname = $request->username;
                            $object->crccode = Session::get('user_currency');
                            $object->category_id = 5;
                            $object->team_id = (int) $team;
                            $object->status = 0;
                            if($object->save()){
                                $wctObj = Worldcuptoken::where('nickname','=',$request->username)->first();
                                if($wctObj){
                                    $wctObj->category_five += 1;
                                    $wctObj->token_amount -= 1;
                                    $wctObj->save();
                                }
                            }
                        }
                    }
                    echo json_encode( array( 'code' => Lang::get('COMMON.SUCESSFUL')));
                }
                else
                    echo json_encode( array( 'code' => Lang::get('COMMON.FAILED')) );
            }
        }
        
        public function WcHighGoal(Request $request){
            $data['total_token'] = Worldcuptoken::where('nickname','=', Session::get('username'))->pluck('token_amount');
            $data['token'] = 6 - Worldcuptoken::where('nickname','=', Session::get('username'))->pluck('category_six');
            return view(Config::get('setting.front_path').'/wc/wc_higherteamgoal',$data);
        }
        
        public function WcHighGoalProcess(Request $request){
            
            if(Worldcuptoken::where('nickname','=', $request->username)->pluck('token_amount') == 0){
                echo json_encode( array( 'code' => Lang::get('COMMON.FAILED')) );
                exit();
            }
            
            $teams = $request->team;
            $current_token = Worldcuptoken::where('nickname','=', $request->username)->pluck('category_six');
            $total_token = Worldcuptoken::where('nickname','=', Session::get('username'))->pluck('token_amount');
            
            if(isset($teams) && count($teams) > 0){
                if((count($teams)+ $current_token) <= 6 && $total_token >= count($teams)){
                    foreach($teams as $team){
                        if(Worldcuptrans::where('nickname','=',$request->username)->where('category_id', '=', 6)->where('team_id','=', $team)->count() == 0){
                            $object = new Worldcuptrans;
                            $object->accid = Session::get('userid');
                            $object->nickname = $request->username;
                            $object->crccode = Session::get('user_currency');
                            $object->category_id = 6;
                            $object->team_id = (int) $team;
                            $object->status = 0;
                            if($object->save()){
                                $wctObj = Worldcuptoken::where('nickname','=',$request->username)->first();
                                if($wctObj){
                                    $wctObj->category_six += 1;
                                    $wctObj->token_amount -= 1;
                                    $wctObj->save();
                                }
                            }
                        }
                    }
                    echo json_encode( array( 'code' => Lang::get('COMMON.SUCESSFUL')));
                }
                else
                    echo json_encode( array( 'code' => Lang::get('COMMON.FAILED')) );
            }
        }
        
        public function WcTopScorer(Request $request){
            $data['total_token'] = Worldcuptoken::where('nickname','=', Session::get('username'))->pluck('token_amount');
            $data['token'] = 6 - Worldcuptoken::where('nickname','=', Session::get('username'))->pluck('category_seven');
            return view(Config::get('setting.front_path').'/wc/wc_topscorer',$data);
        }
        
        public function WcTopScorerProcess(Request $request){
            
            if(Worldcuptoken::where('nickname','=', $request->username)->pluck('token_amount') == 0){
                echo json_encode( array( 'code' => Lang::get('COMMON.FAILED')) );
                exit();
            }
            
            $teams = $request->team;
            $current_token = Worldcuptoken::where('nickname','=', $request->username)->pluck('category_seven');
            $total_token = Worldcuptoken::where('nickname','=', Session::get('username'))->pluck('token_amount');
            
            if(isset($teams) && count($teams) > 0){
                if((count($teams)+ $current_token) <= 6 && $total_token >= count($teams)){
                    foreach($teams as $team){
                        if(Worldcuptrans::where('nickname','=',$request->username)->where('category_id', '=', 7)->where('team_id','=', $team)->count() == 0){
                            $object = new Worldcuptrans;
                            $object->accid = Session::get('userid');
                            $object->nickname = $request->username;
                            $object->crccode = Session::get('user_currency');
                            $object->category_id = 7;
                            $object->team_id = (int) $team;
                            $object->status = 0;
                            if($object->save()){
                                $wctObj = Worldcuptoken::where('nickname','=',$request->username)->first();
                                if($wctObj){
                                    $wctObj->category_seven += 1;
                                    $wctObj->token_amount -= 1;
                                    $wctObj->save();
                                }
                            }
                        }
                    }
                    echo json_encode( array( 'code' => Lang::get('COMMON.SUCESSFUL')));
                }
                else
                    echo json_encode( array( 'code' => Lang::get('COMMON.FAILED')) );
            }
        }
        
        public function WcGroupWinner(Request $request){
            $data['total_token'] = Worldcuptoken::where('nickname','=', Session::get('username'))->pluck('token_amount');
            $data['token'] = 16 - Worldcuptoken::where('nickname','=', Session::get('username'))->pluck('category_eight');
            return view(Config::get('setting.front_path').'/wc/wc_groupwinner',$data);
        }
        
        public function WcGroupWinnerProcess(Request $request){
            
            if(Worldcuptoken::where('nickname','=', $request->username)->pluck('token_amount') == 0){
                echo json_encode( array( 'code' => Lang::get('COMMON.FAILED')) );
                exit();
            }
            
            $teams = $request->team;
            $current_token = Worldcuptoken::where('nickname','=', $request->username)->pluck('category_eight');
            $total_token = Worldcuptoken::where('nickname','=', Session::get('username'))->pluck('token_amount');
            
            if(isset($teams) && count($teams) > 0){
                if((count($teams)+ $current_token) <= 18 && $total_token >= count($teams)){
                    foreach($teams as $team){
                        if(Worldcuptrans::where('nickname','=',$request->username)->where('category_id', '=', 8)->where('team_id','=', $team)->count() == 0){
                            $object = new Worldcuptrans;
                            $object->accid = Session::get('userid');
                            $object->nickname = $request->username;
                            $object->crccode = Session::get('user_currency');
                            $object->category_id = 8;
                            $object->group = Worldcupteam::getGroupName($team);
                            $object->team_id = (int) $team;
                            $object->status = 0;
                            if($object->save()){
                                $wctObj = Worldcuptoken::where('nickname','=',$request->username)->first();
                                if($wctObj){
                                    $wctObj->category_eight += 1;
                                    $wctObj->token_amount -= 1;
                                    $wctObj->save();
                                }
                            }
                        }
                    }
                    echo json_encode( array( 'code' => Lang::get('COMMON.SUCESSFUL')));
                }
                else
                    echo json_encode( array( 'code' => Lang::get('COMMON.FAILED')) );
            }
        }
}
