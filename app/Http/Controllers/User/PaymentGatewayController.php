<?php namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller\User;
use App\libraries\App;
use App\libraries\paymentgateway\DuoDeBao\DuoDeBao;
use App\libraries\paymentgateway\ETonePay\ETonePay;
use App\libraries\paymentgateway\PayWalo\PayWalo;
use App\libraries\paymentgateway\QingYiFu\QingYiFu;
use App\Models\Paymentgateway;
use Config;
use Illuminate\Http\Request;

class PaymentGatewayController extends Controller
{

    public function index($pgscode, $currency, $pgid, Request $request)
    {
        $currency = strtoupper($currency);
        $pgscode = strtoupper($pgscode);
        $pgObj = null;

        $this->log($request->url().'?'.implode('&', $request->input()), $request->getContent(), 'PG'.$pgscode.':index');

        if ($pgscode == 'ETP') {
            $pgObj = new ETonePay($currency);
            $url = $pgObj->getDepositUrl();
            $postfields = $pgObj->getDepositPostfields(Paymentgateway::find($pgid));

            if ($this->isMobileAgent($request)) {
                $postfields['bussId'] = Config::get($currency . '.pg_etp.buss_id_wap');
                $postfields = $pgObj->generateSignValue($currency, $postfields);
            }

            return view('pgform', compact('url', 'postfields'));
        }

//        if ($pgscode == 'PWL') {
//            $pgObj = new PayWalo($currency);
//        } elseif ($pgscode == 'QYF') {
//            $pgObj = new QingYiFu($currency);
//        } elseif ($pgscode == 'DDB') {
//            $pgObj = new DuoDeBao($currency);
//        } elseif ($pgscode == 'ETP') {
//            $pgObj = new ETonePay($currency);
//        }
//
//        if (!is_null($pgObj)) {
//            return $pgObj->resolveCallback($request);
//        }

        abort(404);
    }

    public function callback($pgscode, $currency, Request $request)
    {
        $this->log($request->url(), 'ip:'.$request->ip().'|'.$request->getContent(), 'PG:callback');

        $currency = strtoupper($currency);
        $pgscode = strtoupper($pgscode);
        $pgObj = null;

        if ($pgscode == 'PWL') {
            $pgObj = new PayWalo($currency);
        } elseif ($pgscode == 'QYF') {
            $pgObj = new QingYiFu($currency);
        } elseif ($pgscode == 'DDB') {
            $pgObj = new DuoDeBao($currency);
        } elseif ($pgscode == 'ETP') {
            $pgObj = new ETonePay($currency);
        }

        if (!is_null($pgObj)) {
            return $pgObj->resolveCallback($request);
        }

        abort(404);
    }

    public function returnView($pgscode, $currency, Request $request)
    {
        $this->log($request->url(), 'ip:'.$request->ip().'|pgscode:'.$pgscode.'|crc:'.$currency.'|'.$request->getContent(), 'PG:returnView');

        return redirect()->route('transaction');
    }

    private function log($url, $response, $method)
    {
        App::insert_api_log( array( 'request' => $url , 'return' => $response , 'method' => $method ) );
    }
}
