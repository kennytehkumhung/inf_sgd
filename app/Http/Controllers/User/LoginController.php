<?php namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller\User;
use App\Models\Account;
use App\Models\Accountdetail;
use App\Models\Article;
use App\Models\Badlogin;
use App\Models\Badloginlog;
use App\Models\Website;
use App\Models\Wager;
use App\Models\Product;
use App\Models\Agent;
use App\Models\Accountproduct;
use App\Models\Onlinetracker;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\libraries\Login;
use App\libraries\App;
use Auth;
use Validator;
use Session;
use Config;
use Response;
use Lang;
use Hash;
use Crypt;
use Redirect;

class LoginController extends Controller{
	
	public function __construct()
	{
		
	}

	public function index(Request $request)
	{

		$validator = Validator::make(
			[
				'username' => $request->input('username'),
				'password' => $request->input('password'),
				'code'	   => $request->input('code'),
			],
			[
				'username'   => 'required|alpha_num|min:6',
				'password'   => 'required|alpha_num|min:6',
				'code'  	 => 'required',
			]
		);

		if ($validator->fails())
		{
			echo json_encode($validator->errors());
			exit;
		}
		
		if (!in_array(Config::get('setting.opcode'), array('LVG','DDK', 'RYW', 'IFW')))
		{
		
			if( Session::get('login_captcha') != $request->input('code') )
			{
				echo json_encode( array( 'code' => Lang::get('validation.wrong_captcha') ) );
				exit;
			} 
		}
		/*  */
		$usernameInput = $request->input('username');
			if(Config::get('setting.opcode') == 'LVG'){
				if(is_numeric($request->input('username'))){
					if(Accountdetail::where('telmobile', '=', $request->input('username'))->count() == 1){
						$username = Accountdetail::where('telmobile', '=', $request->input('username'))->pluck('acccode');
						$usernameInput = substr($username,3);
					}
				}
			}
		/*  */

			$accountObj = Account::where( 'nickname' , '=' , $usernameInput )->whereCrccode(Session::get('currency'))->first();

		if (!$accountObj) {
            $this->addBadLoginLog(0, $request->input('username'), $request->input('password'), $request->ip(), $request->server('SERVER_ADDR'));
        }	
        
		if( !isset($accountObj->crccode) ){
			echo json_encode( array( 'code' => Lang::get('validation.login_fail') ) );
			exit;
		}

		if( Session::get('currency') != $accountObj->crccode  )
		{
			echo json_encode( array( 'code' => Lang::get('validation.login_fail') ) );
			exit;
		}
		
		$agentCode = Agent::where( 'id' , '=' , $accountObj->agtid )->pluck('code');

		if( Session::get('opcode') != substr($agentCode, 0 , 3) )
		{
			echo json_encode( array( 'code' => Lang::get('validation.login_fail') ) );
			exit;
		}
		
		if( $accountObj->islogin == 0 && $accountObj->oldpassword == App::getEncryptPassword($request->input('password')) ){
			$accountObj->password      = Hash::make( $request->input('password') );
			$accountObj->enpassword    = Crypt::encrypt( $request->input('password') );
			$accountObj->islogin   	   = 1;
			$accountObj->save();
		}
		
		if( $accountObj->enpassword == '' ){
			$accountObj->enpassword    = Crypt::encrypt( $request->input('password') );
			$accountObj->save();
		}
		
		if (Auth::user()->attempt(array('nickname' => $usernameInput, 'password' => $request->input('password') , 'status' => 1 , 'istestacc' => 0 , 'istestacc' => 0, 'crccode' => Session::get('currency'))))
        {	
			$login = new Login;
			$user = Auth::user()->get();
			$login->doMakeTracker(Auth::user()->get(), 'Account');
			
			Session::put('userid',  		$user->id );
			Session::put('acccode',  		$user->code );
			Session::put('username', 		$user->nickname );
			Session::put('fullname', 		$user->fullname );
			Session::put('email', 	    	$user->email );
			Session::put('enpassword', 	    $user->enpassword );
			Session::put('agtid', 		    $user->agtid );
			Session::put('user_currency',   $user->crccode );
			Session::put('withdrawlimit',   $user->withdrawlimit );
			Session::put('scrid',   		$user->scrid );
			
			
			$products = unserialize(Website::where( 'code' , '=' , Session::get('opcode') )->pluck('products'));

			foreach( $products as $prdid ){
				if( $productObj = Product::where( 'id' , '=' , $prdid )->where( 'status' , '!=' , 0 )->first() )
				{
					$products_obj[$prdid] = $productObj;
					$last_prdid = $prdid;
					
				if( ($productObj->code == 'ALB' || $productObj->code == 'JOK' || $productObj->code == 'PSB' || $productObj->code == 'CTB' ) && ($user->crccode == 'IDR') && ($user->wbsid == 1))
					continue;
					
				if( !Accountproduct::is_exist( Session::get('username') , $productObj->code )  && $productObj->status == 1 )
					{
						$className  = "App\libraries\providers\\".$productObj->code;
						$obj 		= new $className;
						$obj->createUser( array(
														'username' 	=> Session::get('username') , 
														'currency' 	=> Session::get('currency') , 
														'password' 	=> Session::get('enpassword') ,
														'email'    	=> Session::get('email') ,
														'firstname' => Session::get('fullname') ,
														'lastname'  => Session::get('fullname') ,
														'userid'    => Session::get('userid') 
														)
						);
					} 
														
				}
			}
                        
			Session::put('products',      $products );
			Session::put('products_obj',  $products_obj );
			Session::put('last_prdid',    $last_prdid );
			
			Onlinetracker::whereType(CBO_LOGINTYPE_USER)->whereUserid(Session::get('userid'))->where( 'token' , '!=', Session::get('token'))->delete();

			
            echo json_encode( array( 'code' => Lang::get('COMMON.SUCESSFUL') ) );
            
            
        } elseif ($accountObj) {
		    $errorMsg = Lang::get('validation.login_fail');
		    $today = Carbon::now()->toDateString();

		    $this->addBadLoginLog($accountObj->id, $accountObj->nickname, $request->input('password'), $request->ip(), $request->server('SERVER_ADDR'));

		    $badloginObj = Badlogin::where('userid', '=', $accountObj->id)
                ->whereBetween('created', array($today . ' 00:00:00', $today . ' 23:59:59'))
                ->where('type', '=', CBO_LOGINTYPE_USER)
                ->first();

		    if ($badloginObj) {
		        // Increase counter.
                $badloginObj->counter = $badloginObj->counter + 1;
                $badloginObj->modified = Carbon::now();
                $badloginObj->save();

		        if ($badloginObj->counter >= 5) {
		            // Block account.
                    if ($accountObj->status == CBO_ACCOUNTSTATUS_ACTIVE) {
                        $accountObj->status = CBO_ACCOUNTSTATUS_LOGINBLOCKED;
                        $accountObj->save();
                    }

                    $errorMsg = str_replace('%0', '8472', Lang::get('COMMON.ACCOUNTSUSPENDED'));
                }
            } else {
		        Badlogin::forceCreate(array(
		            'userid' => $accountObj->id,
                    'username' => $accountObj->nickname,
                    'type' => CBO_LOGINTYPE_USER,
                    'clientip' => $request->ip(),
                    'counter' => 1,
                    'created' => Carbon::now(),
                    'modified' => Carbon::now(),
                ));
            }

			echo json_encode( array( 'code' => $errorMsg ) );
		    exit;
		} else {
            echo json_encode( array( 'code' => Lang::get('validation.login_fail') ) );
        }
	}

	public function addBadLoginLog($userId, $username, $password, $clientIp, $serverIp) {
        return Badloginlog::forceCreate(array(
            'type' => CBO_LOGINTYPE_USER,
            'userid' => $userId,
            'username' => $username,
            'pw1' => Crypt::encrypt($password),
            'pw2' => '',
            'clientip' => $clientIp,
            'serverip' => $serverIp,
            'created' => Carbon::now(),
            'modified' => Carbon::now(),
        ));
    }
	
	
	public function getCaptcha(Request $request){
	
		$img=imagecreatefromjpeg("front/img/texture.jpg");	
		Session::put($request->input('type'), rand(1000,9999));
		$security_number = Session::get($request->input('type'));
		$image_text=$security_number;	
		$red=rand(100,255); 
		$green=rand(100,255);
		$blue=rand(100,255);
		$text_color=imagecolorallocate($img,205,205,205);
		$text=imagettftext($img,22,rand(-10,10),rand(10,30),rand(25,35),$text_color,Config::get('setting.server_path').'front/fonts/arial.ttf',$image_text);
		//header("Content-type:image/jpeg");
		//header("Content-Disposition:inline ; filename=secure.jpg");	
		
		imagejpeg($img);
		return Response::make('', '200')->header('Content-Type', 'image/jpeg');
	}
	
	

	
}
