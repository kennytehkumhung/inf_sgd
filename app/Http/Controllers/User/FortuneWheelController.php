<?php namespace App\Http\Controllers\User;

use App;
use App\Models\Account;
use App\Models\Cashledger;
use App\Models\Configs;
use App\Models\Currency;
use App\Models\FortuneWheelAcc;
use App\Models\FortuneWheelAccIdr;
use App\Models\FortuneWheelLedger;
use App\Models\FortuneWheelMatch;
use App\Models\FortuneWheelTrans;
use App\Models\FortuneWheelTransIdr;
use App\Models\Onlinetracker;
use App\Models\Promocampaign;
use App\Models\Promocash;
use Cache;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;

class FortuneWheelController extends Controller
{

    private $gameVersion = '1.15';
    private $gameVersionErr = 'Old cache files detected. Please clear your web browser\'s cache and refresh this page.';
    private $gameVersionErrPlaying = 'Old cache files detected. Please clear your web browser\'s cache and refresh this page. No worry, your token will not be deducted.';

    public function index($act, Request $request) {

        if ($act == 'login') {
            $accObj = Account::where('nickname', '=', $request->input('usr'))->where('crccode', '=', $request->input('crc'))->first();

            if ($accObj) {
                $otObj = Onlinetracker::where('userid', '=', $accObj->id)
                    ->where('type', '=', 1)
                    ->first();

                if ($otObj) {
                    return redirect('royalewin/games/royalewheel/index.html?sid=' . $otObj->token . '&crc=' . $request->input('crc'));
                } else {
                    abort(404);
                }
            } else {
                abort(404);
            }
        }

        $otObj = $this->getOnlineTrackerObj($request->input('sid'), $request->input('crc'));

        if (!$otObj) {
            // Check client game file version.
            if (!$this->checkGameVersion($request)) {
                return json_encode([
                    'action' => $act,
                    'error' => $this->gameVersionErr,
                ]);
            }

            return json_encode([
                'action' => $act,
                'error' => 'Invalid username or duplicated login.',
            ]);
        }

        if ($act == 'get_wheel_data') {
            return $this->getWheelData($request, $otObj);
        } elseif ($act == 'get_winner_list') {
            return $this->getWinnerList($request, $otObj);
        } elseif ($act == 'get_wheel_prize') {
            return $this->getWheelPrize($request);
        } elseif ($act == 'get_token_balance') {
            return $this->getUserInfo($request, $otObj);
        } elseif ($act == 'get_match_id') {
            return $this->getMatchId($request, $otObj, true);
        } elseif ($act == 'get_prize_result') {
            return $this->getPrizeResult($request, $otObj);
        } elseif ($act == 'get_bonus_result') {
            return $this->getBonusResult($request, $otObj);
        } elseif ($act == 'claim_total_won') {
            return $this->claimTotalWon($request, $otObj);
        } elseif ($act == 'clear_cache') {
            return self::clearCache();
        }

        if ($request->isJson() || $request->wantsJson()) {
            return response('Page not found.', 404);
        }

        abort(404);
    }

    private function getOnlineTrackerObj($sid, $crc) {
        $otObj = Onlinetracker::where('token', '=', $sid)
            ->where('type', '=', 1)
            ->first();

        if ($otObj) {
            $accObj = Account::find($otObj->userid);

            if ($accObj && in_array($crc, ['MYR', 'IDR']) && $accObj->crccode == $crc) {
                return $otObj;
            }
        }

        return null;
    }

    private function getWinnerList(Request $request, $onlineTrackerObj, $returnAsArray = false) {

        $crcPrefix = '';

        if ($request->input('crc') == 'IDR') {
            $crcPrefix = '_idr';
        }

        $prizeImgPath = asset('royalewin/games/royalewheel' . $crcPrefix . '/imgs/prizelist/grand_royale_wheel.png');
        $results = [
            'action' => '1100',
            'error' => '0',
            'winner_list' => [],
        ];

        // Get winner list from DB.
        try {
            $winnerList = json_decode(Configs::getParam('SYSTEM_LUCKYSPIN_WINNER_LIST' . strtoupper($crcPrefix)), true);
            $resWinList = [];

            foreach ($winnerList as $wKey => $wVal) {
                if ($wVal['n'] != '' && $wVal['p'] != '') {
                    $resWinList[] = [
                        'winner_name' => $wVal['n'],
                        'prize_name' => $wVal['p'],
                        'prize_img' => $prizeImgPath,
                    ];
                }
            }

            $accObj = DB::table('fortune_wheel_acc' . $crcPrefix)->orderBy('total_won', 'desc')->limit(5)->get(['acccode', 'total_won']);

            foreach ($accObj as $r) {
                for ($i = 0; $i < count($resWinList); $i++) {
                    if ($resWinList[$i]['prize_name'] == $r->total_won) {
                        continue;
                    } elseif ($resWinList[$i]['prize_name'] < $r->total_won) {
                        $resWinList[$i]['winner_name'] = str_limit($r->acccode, 5, '***');
                        $resWinList[$i]['prize_name'] = $r->total_won;

                        break;
                    }
                }
            }

            $results['winner_list'] = $resWinList;
        } catch (\Exception $e) {
            // Do nothing.
        }

        if ($returnAsArray) {
            return $results['winner_list'];
        }

        return json_encode($results);
    }

    private function getWheelData(Request $request, $onlineTrackerObj) {

        // Check client game file version.
        if (!$this->checkGameVersion($request)) {
            return json_encode([
                'action' => '1000',
                'error' => $this->gameVersionErr,
            ]);
        }

        $crc = $request->input('crc');
        $crcLabel = 'RM ';
        $crcLabelPost = '';
        $crcPrefix = '';
        $totalWon = 0;

        if ($crc == 'IDR') {
            $crcLabel = 'IDR ';
            $crcLabelPost = 'K';
            $crcPrefix = '_idr';
            $totalWon = FortuneWheelTransIdr::getTotalWon($crc, $onlineTrackerObj->userid);
        } else {
            $totalWon = FortuneWheelTrans::getTotalWon($crc, $onlineTrackerObj->userid);
        }

        $prizeImgPath = asset('royalewin/games/royalewheel' . $crcPrefix . '/imgs/prizelist/grand_royale_wheel.png');
        $results = [
            'action' => '1000',
            'error' => '0',
            'jackpot_amount' => $this->getJackpotAmount($crc),
            'user_token' => $this->getTokenBalance($crc, $onlineTrackerObj->userid),
            'total_won' => $crcLabel . number_format($totalWon) . ($totalWon > 0 ? $crcLabelPost : ''),
            'winner_list' => $this->getWinnerList($request, $onlineTrackerObj, true),
            'prize_list' => array_fill(0, 9, ['prize_name' => '', 'prize_img' => $prizeImgPath])
        ];

        // Get prize list from DB.
        $prizeObj = Cache::rememberForever('fortune_wheel_prize_list_catalog' . $crcPrefix, function() use ($crcPrefix){
            return DB::table('fortune_wheel_prize' . $crcPrefix)
                ->where('position_type', '=', 1)
                ->where('status', '=', 1)
                ->where('position_list', '>', 0)
                ->orderBy('position_list')
                ->get();
        });

        foreach ($prizeObj as $r) {
            $results['prize_list'][$r->position_list - 1] = [
                'prize_name' => $r->prize_name,
                'prize_img' => asset($r->img_path_small),
            ];
        }

        return json_encode($results);
    }

    private function getWheelPrize(Request $request) {

        // Check client game file version.
        if (!$this->checkGameVersion($request)) {
            return json_encode([
                'action' => '1000',
                'error' => $this->gameVersionErr,
            ]);
        }

        $crcPrefix = '';

        if ($request->input('crc') == 'IDR') {
            $crcPrefix = '_idr';
        }

        $results = Cache::rememberForever('fortune_wheel_prize_list' . $crcPrefix, function() use ($crcPrefix) {
            // Total 16 prizes and bonuses.
            $prizeImgPath = asset('royalewin/games/royalewheel' . $crcPrefix . '/imgs/prizelist/0p_ws.png');

            $results = [
                'action' => '1000',
                'error' => '0',
                'prize' => array_fill(0, 16, ['prize_id' => '0', 'prize_name' => '', 'prize_img' => $prizeImgPath, 'prize_img_big' => $prizeImgPath]),
                'bonus' => array_fill(0, 16, ['bonue_id' => '0', 'bonue_name' => '', 'bonus_img' => $prizeImgPath, 'bonus_img_big' => $prizeImgPath])
            ];

            // Get prize list from DB.
            $prizeObj = DB::table('fortune_wheel_prize' . $crcPrefix)
                ->whereIn('position_type', [2, 3])
                ->where('status', '=', 1)
                ->where('position_wheel', '!=', '')
                ->get();

            foreach ($prizeObj as $r) {
                if ($r->position_type == 2) {
                    $positions = explode(',', $r->position_wheel);

                    foreach ($positions as $p) {
                        $results['prize'][$p - 1] = [
                            'prize_id' => $r->id,
                            'prize_name' => $r->prize_name,
                            'prize_img' => asset($r->img_path_small),
                            'prize_img_big' => asset($r->img_path_big),
                        ];
                    }
                } elseif ($r->position_type == 3) {
                    $positions = explode(',', $r->position_wheel);

                    foreach ($positions as $p) {
                        $results['bonus'][$p - 1] = [
                            'bonus_id' => $r->id,
                            'bonus_name' => $r->prize_name,
                            'bonus_img' => asset($r->img_path_small),
                            'bonus_img_big' => asset($r->img_path_big),
                        ];
                    }
                }
            }

            return $results;
        });

        return json_encode($results);
    }

    private function getUserInfo(Request $request, $onlineTrackerObj) {

        // Check client game file version.
        if (!$this->checkGameVersion($request)) {
            return json_encode([
                'action' => '3000',
                'error' => $this->gameVersionErr,
            ]);
        }

        $crc = $request->input('crc');
        $crcLabel = 'RM ';
        $crcLabelPost = '';
        $totalWon = 0;

        if ($crc == 'IDR') {
            $crcLabel = 'IDR ';
            $crcLabelPost = 'K';
            $totalWon = FortuneWheelTransIdr::getTotalWon($crc, $onlineTrackerObj->userid);
        } else {
            $totalWon = FortuneWheelTrans::getTotalWon($crc, $onlineTrackerObj->userid);
        }

        return json_encode([
            'action' => '3000',
            'error' => '0',
            'user_token' => $this->getTokenBalance($crc, $onlineTrackerObj->userid),
            'total_won' => $crcLabel . number_format($totalWon) . ($totalWon > 0 ? $crcLabelPost : ''),
        ]);
    }

    private function getMatchId(Request $request, $onlineTrackerObj, $newMatch) {

        // Check client game file version.
        if (!$this->checkGameVersion($request)) {
            return json_encode([
                'action' => '4100',
                'error' => $this->gameVersionErr,
            ]);
        }

        $crc = $request->input('crc');
        $crcLabel = 'RM ';
        $crcLabelPost = '';
        $matchObj = $this->getMatchObj($crc, $onlineTrackerObj, $newMatch);
        $matchWon = 0;

        if ($crc == 'IDR') {
            $crcLabel = 'IDR ';
            $crcLabelPost = 'K';
        }

        if (!$newMatch) {
            $matchWon = $this->getMatchWon($crc, $matchObj->accid, $matchObj->id);
        }

        if ($matchObj) {
            $totalWon = 0;

            if ($crc == 'IDR') {
                $totalWon = FortuneWheelTransIdr::getTotalWon($onlineTrackerObj->userid);
            } else {
                $totalWon = FortuneWheelTrans::getTotalWon($onlineTrackerObj->userid);
            }

            return json_encode([
                'action' => '4100',
                'error' => '0',
                'user_token' => $this->getTokenBalance($crc, $onlineTrackerObj->userid),
                'match_id' => $matchObj->id,
                'match_won' => $matchWon,
                'total_won' => $crcLabel . number_format($totalWon) . ($totalWon > 0 ? $crcLabelPost : ''),
                'winner_list' => $this->getWinnerList($request, $onlineTrackerObj, true),
            ]);
        }

        return json_encode([
            'action' => '4100',
            'error' => '1',
            'match_id' => 0,
            'match_won' => 0,
            'total_won' => $crcLabel . ' 0.00',
            'winner_list' => $this->getWinnerList($request, $onlineTrackerObj, true),
        ]);
    }

    private function getPrizeResult(Request $request, $onlineTrackerObj) {

        // Check client game file version.
        if (!$this->checkGameVersion($request)) {
            return json_encode([
                'action' => '4200',
                'error' => $this->gameVersionErrPlaying,
            ]);
        }

        $crc = $request->input('crc');
        $crcLabel = 'RM ';
        $crcLabelPost = '';
        $crcPrefix = '';
        $ledgerRefObject = 'FortuneWheelTrans';
        $matchObj = $this->getMatchObj($crc, $onlineTrackerObj, false);
        $matchId = 0;

        if ($crc == 'IDR') {
            $crcLabel = 'IDR ';
            $crcLabelPost = 'K';
            $crcPrefix = '_idr';
            $ledgerRefObject = 'FortuneWheelTransIdr';
        }

        if ($matchObj) {
            $matchId = $matchObj->id;
        }

        if ($request->input('mid') != $matchId) {
            return json_encode([
                'action' => '4200',
                'error' => 'Invalid match, please refresh page and try again.',
            ]);
        }

        $ledgerObjId = 0;
        $probabilitySet = 3;

        $normalTokenLedgerObj = DB::table('fortune_wheel_ledger' . $crcPrefix)
            ->where('accid', '=', $onlineTrackerObj->userid)
            ->where('category', '=', FortuneWheelLedger::CATEGORY_NORMAL_TOKEN)
            ->where('is_used', '=', 0)
            ->where('expired_at', '>', Carbon::now())
            ->orderBy('expired_at') // Get nearly expired token first.
            ->first();

        $freeTokenLedgerObj = DB::table('fortune_wheel_ledger' . $crcPrefix)
            ->where('accid', '=', $onlineTrackerObj->userid)
            ->where('category', '=', FortuneWheelLedger::CATEGORY_FREE_TOKEN)
            ->where('is_used', '=', 0)
            ->where('expired_at', '>', Carbon::now())
            ->orderBy('expired_at') // Get nearly expired token first.
            ->first();

        if ($normalTokenLedgerObj || $freeTokenLedgerObj) {
            // Deduct 1 token on each spin.
            if ($normalTokenLedgerObj && $freeTokenLedgerObj) {
                // Have both tokens, random pick.
                $useFreeToken = (mt_rand(0, 1) > 0);
            } elseif ($normalTokenLedgerObj) {
                // Only have normal token.
                $useFreeToken = false;
            } else {
                // Only have free token.
                $useFreeToken = true;
            }

            $wheelAccObj = DB::table('fortune_wheel_acc' . $crcPrefix)->where('accid', '=', $onlineTrackerObj->userid)->first();
            $probabilitySet = $wheelAccObj->probability_set;
            $probabilityColName = 'win_probability' . $wheelAccObj->probability_set;

            if ($useFreeToken) {
                // Deduct free token.
                $probabilityColName = 'win_probability_free_token' . $probabilitySet;
                $ledgerObjId = $freeTokenLedgerObj->id;
            } else {
                // Deduct normal token.
                $ledgerObjId = $normalTokenLedgerObj->id;
            }

            DB::table('fortune_wheel_ledger' . $crcPrefix)
                ->where('id', '=', $ledgerObjId)
                ->update(array(
                    'is_used' => 1,
                    'updated_at' => Carbon::now(),
                ));
        } else {
            return json_encode([
                'action' => '4200',
                'error' => 'Not enough token.',
            ]);
        }

        // Outer wheel.
        $prizeObj = Cache::rememberForever('fortune_wheel_spin_prize_lose' . $crcPrefix, function () use ($crcPrefix) {
            return DB::table('fortune_wheel_prize' . $crcPrefix)
                ->where('position_type', '=', 2)
                ->where('prize_type', '=', 'lose')
                ->first();
        });

        $totalWon = 0;

        if ($crc == 'IDR') {
            $totalWon = FortuneWheelTransIdr::getTotalWon($onlineTrackerObj->userid);
        } else {
            $totalWon = FortuneWheelTrans::getTotalWon($onlineTrackerObj->userid);
        }

        $results = [
            'action' => '4200',
            'error' => '0',
            'match_id' => $matchId,
            'match_won' => $this->getMatchWon($crc, $onlineTrackerObj->userid, $matchId),
            'user_token' => $this->getTokenBalance($crc, $onlineTrackerObj->userid),
            'total_won' => $crcLabel . number_format($totalWon) . ($totalWon > 0 ? $crcLabelPost : ''),
            'result' => [
                'prize_id' => $prizeObj->id,
                'prize_name' => $prizeObj->prize_name,
                'prize_img' => asset($prizeObj->img_path_small),
                'prize_img_big' => asset($prizeObj->img_path_big),
                'prize_bonus_spin' => '0',
            ],
        ];

        $prizeObj = Cache::rememberForever('fortune_wheel_spin_prize' . $crcPrefix, function () use ($crcPrefix) {
            return DB::table('fortune_wheel_prize' . $crcPrefix)
                ->where('position_type', '=', 2)
                ->where('status', '=', 1)
                ->get();
        });

        if (count($prizeObj) > 0) {
            $probabilityArr = [];
            $isWon = false;

            foreach ($prizeObj as $r) {
                if ($r->{$probabilityColName} > 0.00) {
                    $probabilityArr[$r->id] = $r->{$probabilityColName};
                }
            }

            $winId = $this->getRand($probabilityArr);
            $transObj = null;

            foreach ($prizeObj as $r) {
                if ($r->id == $winId && $r->prize_value > 0.00) {
                    $results['match_won'] = $crcLabel . number_format($r->prize_value);
                    $results['result'] = [
                        'prize_id' => $r->id,
                        'prize_name' => $r->prize_name,
                        'prize_img' => asset($r->img_path_small),
                        'prize_img_big' => asset($r->img_path_big),
                        'prize_bonus_spin' => $r->bonus_spin,
                    ];

                    $newId = DB::table('fortune_wheel_trans' . $crcPrefix)->insertGetId(array(
                        'accid' => $onlineTrackerObj->userid,
                        'acccode' => $onlineTrackerObj->username,
                        'match_id' => $matchId,
                        'game_act' => FortuneWheelTrans::GAME_ACT_SPIN_PRIZE,
                        'status' => 1,
                        'prize_id' => $r->id,
                        'prize_type' => $r->prize_type,
                        'amount' => $r->prize_value,
                        'probability_set' => $probabilitySet,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                    ));

                    $transObj = DB::table('fortune_wheel_trans' . $crcPrefix)->where('id', '=', $newId)->first();

                    $isWon = true;

                    break;
                }
            }

            if (!$isWon) {
                // User lose, close match.
                DB::table('fortune_wheel_match' . $crcPrefix)
                    ->where('id', '=', $matchId)
                    ->where('accid', '=', $onlineTrackerObj->userid)
                    ->update(array(
                        'status' => FortuneWheelMatch::STATUS_CLOSED,
                    ));

                $newId = DB::table('fortune_wheel_trans' . $crcPrefix)->insertGetId(array(
                    'accid' => $onlineTrackerObj->userid,
                    'acccode' => $onlineTrackerObj->username,
                    'match_id' => $matchId,
                    'game_act' => FortuneWheelTrans::GAME_ACT_SPIN_PRIZE,
                    'status' => 1,
                    'prize_id' => 0,
                    'prize_type' => 'lose',
                    'amount' => 0,
                    'probability_set' => $probabilitySet,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ));

                $transObj = DB::table('fortune_wheel_trans' . $crcPrefix)->where('id', '=', $newId)->first();

                $results['match_id'] = 0;
                $results['match_won'] = 0;
            }

            if (!is_null($transObj)) {
                DB::table('fortune_wheel_ledger' . $crcPrefix)
                    ->where('id', '=', $ledgerObjId)
                    ->update(array(
                        'refid' => $transObj->id,
                        'object' => $ledgerRefObject,
                        'updated_at' => Carbon::now(),
                    ));
            }
        }

        return json_encode($results);
    }

    private function getBonusResult(Request $request, $onlineTrackerObj) {

        // Check client game file version.
        if (!$this->checkGameVersion($request)) {
            return json_encode([
                'action' => '4300',
                'error' => $this->gameVersionErrPlaying,
            ]);
        }

        $crc = $request->input('crc');
        $crcLabel = 'RM ';
        $crcLabelPost = '';
        $crcPrefix = '';
        $ledgerRefObject = 'FortuneWheelTrans';
        $matchObj = $this->getMatchObj($crc, $onlineTrackerObj, false);
        $matchId = 0;

        if ($crc == 'IDR') {
            $crcLabel = 'IDR ';
            $crcLabelPost = 'K';
            $crcPrefix = '_idr';
            $ledgerRefObject = 'FortuneWheelTransIdr';
        }

        if ($matchObj) {
            $matchId = $matchObj->id;
        }

        if ($request->input('mid') != $matchId) {
            return json_encode([
                'action' => '4300',
                'error' => 'Invalid match, please refresh page and try again.',
            ]);
        }

        if ($this->getMatchWon($crc, $onlineTrackerObj->userid, $matchId) <= 0) {
            return json_encode([
                'action' => '4300',
                'error' => 'Invalid match, please refresh page and try again.',
            ]);
        }

        $ledgerObjId = 0;
        $probabilitySet = 3;
        $normalTokenLedgerObj = DB::table('fortune_wheel_ledger' . $crcPrefix)
            ->where('accid', '=', $onlineTrackerObj->userid)
            ->where('category', '=', FortuneWheelLedger::CATEGORY_NORMAL_TOKEN)
            ->where('is_used', '=', 0)
            ->where('expired_at', '>', Carbon::now())
            ->orderBy('expired_at') // Get nearly expired token first.
            ->first();

        $freeTokenLedgerObj = DB::table('fortune_wheel_ledger' . $crcPrefix)
            ->where('accid', '=', $onlineTrackerObj->userid)
            ->where('category', '=', FortuneWheelLedger::CATEGORY_FREE_TOKEN)
            ->where('is_used', '=', 0)
            ->where('expired_at', '>', Carbon::now())
            ->orderBy('expired_at') // Get nearly expired token first.
            ->first();

        if ($normalTokenLedgerObj || $freeTokenLedgerObj) {
            // Deduct 1 token on each spin.
            if ($normalTokenLedgerObj && $freeTokenLedgerObj) {
                // Have both tokens, random pick.
                $useFreeToken = (mt_rand(0, 1) > 0);
            } elseif ($normalTokenLedgerObj) {
                // Only have normal token.
                $useFreeToken = false;
            } else {
                // Only have free token.
                $useFreeToken = true;
            }

            $wheelAccObj = DB::table('fortune_wheel_acc' . $crcPrefix)->where('accid', '=', $onlineTrackerObj->userid)->first();
            $probabilitySet = $wheelAccObj->probability_set;
            $probabilityColName = 'win_probability' . $wheelAccObj->probability_set;

            if ($useFreeToken) {
                // Deduct free token.
                $probabilityColName = 'win_probability_free_token' . $probabilitySet;
                $ledgerObjId = $freeTokenLedgerObj->id;
            } else {
                // Deduct normal token.
                $ledgerObjId = $normalTokenLedgerObj->id;
            }

            DB::table('fortune_wheel_ledger' . $crcPrefix)
                ->where('id', '=', $ledgerObjId)
                ->update(array(
                    'is_used' => 1,
                    'updated_at' => Carbon::now(),
                ));
        } else {
            return json_encode([
                'action' => '4300',
                'error' => 'Not enough token.',
            ]);
        }

        // Inner wheel.
        $prizeObj = Cache::rememberForever('fortune_wheel_spin_bonus_lose' . $crcPrefix, function () use ($crcPrefix) {
            return DB::table('fortune_wheel_prize' . $crcPrefix)
                ->where('position_type', '=', 3)
                ->where('prize_type', '=', 'lose')
                ->first();
        });

        $totalWon = 0;

        if ($crc == 'IDR') {
            $totalWon = FortuneWheelTransIdr::getTotalWon($onlineTrackerObj->userid);
        } else {
            $totalWon = FortuneWheelTrans::getTotalWon($onlineTrackerObj->userid);
        }

        $results = [
            'action' => '4300',
            'error' => '0',
            'match_id' => $matchId,
            'match_won' => $this->getMatchWon($crc, $onlineTrackerObj->userid, $matchId),
            'user_token' => $this->getTokenBalance($crc, $onlineTrackerObj->userid),
            'total_won' => $crcLabel . number_format($totalWon) . ($totalWon > 0 ? $crcLabelPost : ''),
            'result' => [
                'bonus_id' => $prizeObj->id,
                'bonus_name' => $prizeObj->prize_name,
                'bonus_img' => asset($prizeObj->img_path_small),
                'bonus_img_big' => asset($prizeObj->img_path_big),
                'bonus_bonus_spin' => '0',
            ],
        ];

        $prizeObj = Cache::rememberForever('fortune_wheel_spin_bonus' . $crcPrefix, function () use ($crcPrefix) {
            return DB::table('fortune_wheel_prize' . $crcPrefix)
                ->where('position_type', '=', 3)
                ->where('status', '=', 1)
                ->get();
        });

        if (count($prizeObj) > 0) {
            $probabilityArr = [];
            $isWon = false;

            foreach ($prizeObj as $r) {
                if ($r->{$probabilityColName} > 0.00) {
                    $probabilityArr[$r->id] = $r->{$probabilityColName};
                }
            }

            $winId = $this->getRand($probabilityArr);
            $freeTokenValidityDays = -1;
            $transObj = null;

            foreach ($prizeObj as $r) {
                if ($r->id == $winId && $r->prize_type != 'lose') {
//                    $todayDate = Carbon::now()->toDateString();
//                    $dailyTotalWon = FortuneWheelTrans::getTotalWon($onlineTrackerObj->userid, $todayDate . ' 00:00:00', $todayDate . ' 23:59:59');
                    $dailyTotalWon = 0;

                    // Limit match won max amount to 300 only within a day.
                    if ($dailyTotalWon < 300 && $r->prize_type == 'credit') {
                        $matchWon = $this->getMatchWon($crc, $onlineTrackerObj->userid, $matchId) * $r->prize_value;

                        $results['match_won'] = $crcLabel . number_format($matchWon);
                        $results['result'] = [
                            'bonus_id' => $r->id,
                            'bonus_name' => $r->prize_name,
                            'bonus_img' => asset($r->img_path_small),
                            'bonus_img_big' => asset($r->img_path_big),
                            'bonus_bonus_spin' => $r->bonus_spin,
                        ];

                        $newId = DB::table('fortune_wheel_trans' . $crcPrefix)->insertGetId(array(
                            'accid' => $onlineTrackerObj->userid,
                            'acccode' => $onlineTrackerObj->username,
                            'match_id' => $matchId,
                            'game_act' => FortuneWheelTrans::GAME_ACT_SPIN_BONUS,
                            'status' => 1,
                            'prize_id' => $r->id,
                            'prize_type' => $r->prize_type,
                            'amount' => $matchWon,
                            'probability_set' => $probabilitySet,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ));

                        $transObj = DB::table('fortune_wheel_trans' . $crcPrefix)->where('id', '=', $newId)->first();

                        $isWon = true;

                        break;
                    } elseif ($r->prize_type == 'token') {
                        $tokenWon = $this->getTokenBalance($crc, $onlineTrackerObj->userid) + $r->prize_value;

                        $results['match_won'] = number_format($r->prize_value) . ' Token';
                        $results['user_token'] = $tokenWon;
                        $results['result'] = [
                            'bonus_id' => $r->id,
                            'bonus_name' => $r->prize_name,
                            'bonus_img' => asset($r->img_path_small),
                            'bonus_img_big' => asset($r->img_path_big),
                            'bonus_bonus_spin' => $r->bonus_spin,
                        ];

                        $newId = DB::table('fortune_wheel_trans' . $crcLabel)->insertGetId(array(
                            'accid' => $onlineTrackerObj->userid,
                            'acccode' => $onlineTrackerObj->username,
                            'match_id' => $matchId,
                            'game_act' => FortuneWheelTrans::GAME_ACT_SPIN_BONUS,
                            'status' => 1,
                            'prize_id' => $r->id,
                            'prize_type' => $r->prize_type,
                            'amount' => $r->prize_value,
                            'probability_set' => $probabilitySet,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ));

                        $transObj = DB::table('fortune_wheel_trans' . $crcPrefix)->where('id', '=', $newId)->first();

                        if ($freeTokenValidityDays == -1) {
                            // Cache token validity days config.
                            $tokenValidityDaysConfig = json_decode(Configs::getParam('SYSTEM_LUCKYSPIN_TOKEN_VALIDITY_DAYS' . strtoupper($crcPrefix)), true);
                            $freeTokenValidityDays = $tokenValidityDaysConfig['f'];
                        }

                        // Member won free tokens, so Match Won credit amount reset to 0.
                        DB::table('fortune_wheel_trans' . $crcPrefix)->insert(array(
                            'accid' => $onlineTrackerObj->userid,
                            'acccode' => $onlineTrackerObj->username,
                            'match_id' => $matchId,
                            'game_act' => FortuneWheelTrans::GAME_ACT_SPIN_BONUS,
                            'status' => 1,
                            'prize_id' => $r->id,
                            'prize_type' => 'credit',
                            'amount' => 0,
                            'probability_set' => $probabilitySet,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ));

                        $remark = 'REF: FortuneWheelTrans@';

                        if ($crc == 'IDR') {
                            $remark = 'REF: FortuneWheelTransIdr@';
                        }

                        for ($i = 0; $i < $r->prize_value; $i++) {
                            DB::table('fortune_wheel_ledger' . $crcPrefix)->insert(array(
                                'accid' => $onlineTrackerObj->userid,
                                'acccode' => $onlineTrackerObj->username,
                                'amount' => 1,
                                'category' => FortuneWheelLedger::CATEGORY_FREE_TOKEN,
                                'is_used' => 0,
                                'refid' => 0,
                                'object' => '',
                                'sys_ref' => FortuneWheelLedger::SYS_REF_FT_GAME_BONUS,
                                'remark' => $remark . $transObj->id,
                                'expired_at' => Carbon::now()->addDays($freeTokenValidityDays),
                                'created_at' => Carbon::now(),
                                'updated_at' => Carbon::now(),
                            ));
                        }

                        $isWon = true;

                        break;
                    }
                }
            }

            if (!$isWon) {
                // User lose, close match.
                DB::table('fortune_wheel_match' . $crcPrefix)
                    ->where('id', '=', $matchId)
                    ->where('accid', '=', $onlineTrackerObj->userid)
                    ->update(array(
                        'status' => FortuneWheelMatch::STATUS_CLOSED,
                    ));

                $newId = DB::table('fortune_wheel_trans' . $crcPrefix)->insertGetId(array(
                    'accid' => $onlineTrackerObj->userid,
                    'acccode' => $onlineTrackerObj->username,
                    'match_id' => $matchId,
                    'game_act' => FortuneWheelTrans::GAME_ACT_SPIN_BONUS,
                    'status' => 1,
                    'prize_id' => 0,
                    'prize_type' => 'lose',
                    'amount' => 0,
                    'probability_set' => $probabilitySet,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ));

                $transObj = DB::table('fortune_wheel_trans' . $crcPrefix)->where('id', '=', $newId)->first();

                $results['match_id'] = 0;
                $results['match_won'] = 0;
            }

            if (!is_null($transObj)) {
                DB::table('fortune_wheel_ledger' . $crcPrefix)
                    ->where('id', '=', $ledgerObjId)
                    ->update(array(
                        'refid' => $transObj->id,
                        'object' => $ledgerRefObject,
                        'updated_at' => Carbon::now(),
                    ));
            }
        }

        return json_encode($results);
    }

    private function claimTotalWon(Request $request, $onlineTrackerObj) {

        // Check client game file version.
        if (!$this->checkGameVersion($request)) {
            return json_encode([
                'action' => '5000',
                'error' => $this->gameVersionErr,
            ]);
        }

        $crc = $request->input('crc');
        $crcLabel = 'RM ';
        $crcPrefix = '';
        $crcRate = 1;

        if ($crc == 'IDR') {
            $crcLabel = 'IDR ';
            $crcPrefix = '_idr';
            $crcRate = 1000;
        }

        $totalWon = 0;
        $error = 0;
        $message = 'Transfer failed. Please try again later.';

        if ($crc == 'IDR') {
            $totalWon = FortuneWheelTransIdr::getTotalWon($onlineTrackerObj->userid);
        } else {
            $totalWon = FortuneWheelTrans::getTotalWon($onlineTrackerObj->userid);
        }

        if ($totalWon > 0) {
            // Transfer won credit to cashledger table, under bonus tag.
            if( $pcpObj = Promocampaign::where('code', '=', 'grandroyalewheel')->where('crccode', '=', $crc)->first() ) {
                $accObj = Account::find($onlineTrackerObj->userid);

                // Update trans status to Claimed.
                DB::table('fortune_wheel_trans' . $crcPrefix . ' as a')
                    ->join('fortune_wheel_match' . $crcPrefix . ' as b', 'b.id', '=', 'a.match_id')
                    ->where('a.acccode', '=', $onlineTrackerObj->username)
                    ->where('a.status', '=', FortuneWheelTrans::STATUS_OPENING)
                    ->where('b.status', '=', FortuneWheelMatch::STATUS_CLOSED)
                    ->update(array(
                        'a.status' => FortuneWheelTrans::STATUS_CLAIMED,
                        'a.updated_at' => Carbon::now(),
                    ));

                DB::table('fortune_wheel_trans' . $crcPrefix)->insert(array(
                    'accid' => $onlineTrackerObj->userid,
                    'acccode' => $onlineTrackerObj->username,
                    'match_id' => 0,
                    'game_act' => FortuneWheelTrans::GAME_ACT_CLAIM,
                    'status' => FortuneWheelTrans::STATUS_CLAIMED,
                    'prize_id' => 0,
                    'prize_type' => 'claim',
                    'amount' => $totalWon * -1,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ));

                $promoCashObj = new Promocash;
                $promoCashObj->type			= Promocash::TYPE_MANUAL;
                $promoCashObj->accid	 	= $accObj->id;
                $promoCashObj->acccode	 	= $accObj->code;
                $promoCashObj->crccode	 	= $accObj->crccode;
                $promoCashObj->crcrate	 	= Currency::getCurrencyRate($accObj->crccode);
                $promoCashObj->amount	 	= $totalWon * $crcRate;
                $promoCashObj->amountlocal 	= Currency::getLocalAmount($totalWon * $crcRate, $accObj->crccode);
                $promoCashObj->pcpid		= $pcpObj->id;
                $promoCashObj->status		= Promocash::STATUS_PENDING;

                if($promoCashObj->save()) {
                    $cashLedgerObj = new Cashledger;
                    $cashLedgerObj->accid 			= $promoCashObj->accid;
                    $cashLedgerObj->acccode 		= $promoCashObj->acccode;
                    $cashLedgerObj->accname 		= $accObj->fullname;
                    $cashLedgerObj->chtcode 		= CBO_CHARTCODE_BONUS;
//                    $cashLedgerObj->cashbalance 	= WalletController::getBalance();
                    $cashLedgerObj->cashbalance 	= 0;
                    $cashLedgerObj->amount 			= $promoCashObj->amount;
                    $cashLedgerObj->amountlocal 	= $promoCashObj->amountlocal;
                    $cashLedgerObj->fee 			= $promoCashObj->amount;
                    $cashLedgerObj->feelocal 		= $promoCashObj->amountlocal;
                    $cashLedgerObj->crccode 		= $promoCashObj->crccode;
                    $cashLedgerObj->crcrate 		= $promoCashObj->crcrate;
                    $cashLedgerObj->refobj			= 'Promocash';
                    $cashLedgerObj->refid			= $promoCashObj->id;
                    $cashLedgerObj->status			= CBO_LEDGERSTATUS_PENDING;
                    $cashLedgerObj->createdip		= $request->ip();
                    $cashLedgerObj->save();

                    $message = $crcLabel . number_format($totalWon) . ' has been credited to main wallet.';
                }
            }
        } else {
            $message = 'No credit to transfer.';
        }

        return json_encode([
            'action' => '5000',
            'error' => $error,
            'message' => $message,
        ]);
    }

    private function getRand($proArr) {
        $result = '';

        //概率数组的总概率精度
        $proSum = array_sum($proArr);

        //概率数组循环
        foreach ($proArr as $key => $proCur) {
            $randNum = mt_rand(1, $proSum);
            if ($randNum <= $proCur) {
                $result = $key;
                break;
            } else {
                $proSum -= $proCur;
            }
        }
        unset ($proArr);

        return $result;
    }

    private function getMatchObj($crc, $onlineTrackerObj, $newMatch) {

        // Fetch FortuneWheelMatch.
        $matchObj = null;
        $crcPrefix = '';

        if ($crc == 'IDR') {
            $crcPrefix = '_idr';
        }

        if ($newMatch) {
            DB::table('fortune_wheel_match' . $crcPrefix)
                ->where('accid', '=', $onlineTrackerObj->userid)
                ->where('status', '=', 1)
                ->chunk(300, function ($rows) use ($onlineTrackerObj, $crc, $crcPrefix) {
                    foreach ($rows as $r) {
                        DB::table('fortune_wheel_match' . $crcPrefix)
                            ->where('id', '=', $r->id)
                            ->update([
                                'status' => 0,
                                'updated_at' => Carbon::now(),
                            ]);

                        $matchWon = FortuneWheelController::getMatchWon($crc, $onlineTrackerObj->userid, $r->id);

                        DB::table('fortune_wheel_acc' . $crcPrefix)
                            ->where('accid', '=', $onlineTrackerObj->userid)
                            ->update([
                                'total_won' => DB::raw('`total_won` + ' . $matchWon),
                                'updated_at' => Carbon::now(),
                            ]);
                    }
                });

            $newId = DB::table('fortune_wheel_match' . $crcPrefix)
                ->insertGetId(array(
                    'accid' => $onlineTrackerObj->userid,
                    'acccode' => $onlineTrackerObj->username,
                    'status' => 1,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ));

            $matchObj = DB::table('fortune_wheel_match' . $crcPrefix)->where('id', '=', $newId)->first();
        } else {
            $matchObj = DB::table('fortune_wheel_match' . $crcPrefix)
                ->where('accid', '=', $onlineTrackerObj->userid)
                ->where('status', '=', 1)
                ->first();
        }

        return $matchObj;
    }

    private function getJackpotAmount($crc) {

        $crcPrefix = '';

        if ($crc == 'IDR') {
            $crcPrefix = '_idr';
        }

        $jackpotAmount = Cache::rememberForever('fortune_wheel_jackpot_amount' . $crcPrefix, function() use ($crcPrefix) {

            $amount = 0;

            $prizeObj = DB::table('fortune_wheel_prize' . $crcPrefix)->where('prize_type', '=', 'jackpot')
                ->where('position_type', '=', 0)
                ->where('status', '=', 1)
                ->first(['prize_value']);

            if ($prizeObj) {
                $amount = $prizeObj->prize_value;
            }

            return $amount;
        });

        return $jackpotAmount + mt_rand(100, 99999);
    }

    private function getTokenBalance($crc, $accid) {

        $crcPrefix = '';

        if ($crc == 'IDR') {
            $crcPrefix = '_idr';
        }

        return DB::table('fortune_wheel_ledger' . $crcPrefix)->where('accid', '=', $accid)
            ->where('is_used', '=', 0)
            ->where('expired_at', '>', Carbon::now())
            ->sum('amount');
    }

    private function getFreeTokenBalance($crc, $accid) {

        $crcPrefix = '';

        if ($crc == 'IDR') {
            $crcPrefix = '_idr';
        }

        return DB::table('fortune_wheel_ledger' . $crcPrefix)
            ->where('accid', '=', $accid)
            ->where('category', '=', FortuneWheelLedger::CATEGORY_FREE_TOKEN)
            ->where('is_used', '=', 0)
            ->where('expired_at', '>', Carbon::now())
            ->sum('amount');
    }

    private function getMatchWon($crc, $accid, $matchId) {

        $crcPrefix = '';
        $amount = 0;

        if ($crc == 'IDR') {
            $crcPrefix = '_idr';
        }

        $matchWon = DB::table('fortune_wheel_trans' . $crcPrefix)
            ->where('accid', '=', $accid)
            ->where('match_id', '=', $matchId)
            ->orderBy('id', 'desc')
            ->first(['amount']);

        if ($matchWon) {
            $amount = $matchWon->amount;
        }

        return $amount;
    }

    private function checkGameVersion($request) {

        return ($request->get('gv') == $this->gameVersion);
    }

    public static function addDailyLoginFreeToken($crc, $accid, $acccode) {

        $crcPrefix = '';

        if ($crc == 'IDR') {
            FortuneWheelAccIdr::ensureExists($accid, $acccode);
            $crcPrefix = '_idr';
        } else {
            FortuneWheelAcc::ensureExists($accid, $acccode);
        }

        $dailyLoginFreeTokenConfig = json_decode(Configs::getParam('SYSTEM_LUCKYSPIN_DAILYLOGINFREETOKEN' . strtoupper($crcPrefix)), true);
        $tokenValidityDaysConfig = json_decode(Configs::getParam('SYSTEM_LUCKYSPIN_TOKEN_VALIDITY_DAYS' . strtoupper($crcPrefix)), true);

        // Free token MYR: 2 x 7 (+3) x 23 (+8)
        // IDR only daily token

        if (isset($dailyLoginFreeTokenConfig['d']) && $dailyLoginFreeTokenConfig['d'] > 0) {
            // Check daily login.
            $todayDt = Carbon::now()->toDateString();

            $ledgerObj = DB::table('fortune_wheel_ledger' . $crcPrefix)
                ->where('accid', '=', $accid)
                ->where('sys_ref', '=', FortuneWheelLedger::SYS_REF_FT_DAILY_LOGIN)
                ->whereBetween('created_at', [$todayDt . ' 00:00:00', $todayDt . ' 23:59:59'])
                ->first();

            if (!$ledgerObj) {
                for ($i = 0; $i < $dailyLoginFreeTokenConfig['d']; $i++) {
                    DB::table('fortune_wheel_ledger' . $crcPrefix)->insert(array(
                        'accid' => $accid,
                        'acccode' => $acccode,
                        'amount' => 1,
                        'category' => FortuneWheelLedger::CATEGORY_FREE_TOKEN,
                        'refid' => 0,
                        'object' => '',
                        'sys_ref' => FortuneWheelLedger::SYS_REF_FT_DAILY_LOGIN,
                        'remark' => '',
                        'expired_at' => Carbon::now()->addDays($tokenValidityDaysConfig['f']),
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                    ));
                }
            }
        }

        if (isset($dailyLoginFreeTokenConfig['w']) && $dailyLoginFreeTokenConfig['w'] > 0) {
            // Check weekly login.
            $ledgerObj = DB::table('fortune_wheel_ledger' . $crcPrefix)
                ->where('accid', '=', $accid)
                ->where('sys_ref', '=', FortuneWheelLedger::SYS_REF_FT_WEEKLY_LOGIN)
                ->whereBetween('created_at', [Carbon::now()->addDays(-7)->toDateString() . ' 00:00:00', Carbon::now()->toDateString() . ' 23:59:59'])
                ->first();

            if (!$ledgerObj) {
                // User did not collect weekly bonus in pass 7 days.
                // Now check user have login 7 days in raw or not.
                $ledgerObj = DB::select('SELECT t.dt FROM (SELECT accid, DATE(created_at) AS dt FROM fortune_wheel_ledger' . $crcPrefix . ' WHERE accid = :accid AND sys_ref = :sys_ref AND created_at BETWEEN :cfrom AND :cto) t GROUP BY t.dt',
                    [
                        'accid' => $accid,
                        'sys_ref' => FortuneWheelLedger::SYS_REF_FT_DAILY_LOGIN,
                        'cfrom' => Carbon::now()->addDays(-7)->toDateString() . ' 00:00:00',
                        'cto' => Carbon::now()->toDateString() . ' 23:59:59',
                    ]
                );

                if (count($ledgerObj) >= 7) {
                    // User has logged in for 7 days continuously.
                    for ($i = 0; $i < $dailyLoginFreeTokenConfig['w']; $i++) {
                        DB::table('fortune_wheel_ledger' . $crcPrefix)->insert(array(
                            'accid' => $accid,
                            'acccode' => $acccode,
                            'amount' => 1,
                            'category' => FortuneWheelLedger::CATEGORY_FREE_TOKEN,
                            'refid' => 0,
                            'object' => '',
                            'sys_ref' => FortuneWheelLedger::SYS_REF_FT_WEEKLY_LOGIN,
                            'remark' => '',
                            'expired_at' => Carbon::now()->addDays($tokenValidityDaysConfig['f']),
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ));
                    }
                }
            }
        }

        if (isset($dailyLoginFreeTokenConfig['m']) && $dailyLoginFreeTokenConfig['m'] > 0) {
            // Check monthly login.
            $ledgerObj = DB::table('fortune_wheel_ledger' . $crcPrefix)
                ->where('accid', '=', $accid)
                ->where('sys_ref', '=', FortuneWheelLedger::SYS_REF_FT_MONTHLY_LOGIN)
                ->whereBetween('created_at', [Carbon::now()->addDays(-30)->toDateString() . ' 00:00:00', Carbon::now()->toDateString() . ' 23:59:59'])
                ->first();

            if (!$ledgerObj) {
                // User did not collect weekly bonus in pass 30 days.
                // Now check user have login 30 days in raw or not.
                $ledgerObj = DB::select('SELECT t.dt FROM (SELECT accid, DATE(created_at) AS dt FROM fortune_wheel_ledger' . $crcPrefix . ' WHERE accid = :accid AND sys_ref = :sys_ref AND created_at BETWEEN :cfrom AND :cto) t GROUP BY t.dt',
                    [
                        'accid' => $accid,
                        'sys_ref' => FortuneWheelLedger::SYS_REF_FT_DAILY_LOGIN,
                        'cfrom' => Carbon::now()->addDays(-30)->toDateString() . ' 00:00:00',
                        'cto' => Carbon::now()->toDateString() . ' 23:59:59',
                    ]
                );

                if (count($ledgerObj) >= 30) {
                    // User has logged in for 30 days continuously.
                    for ($i = 0; $i < $dailyLoginFreeTokenConfig['m']; $i++) {
                        DB::table('fortune_wheel_ledger' . $crcPrefix)->insert(array(
                            'accid' => $accid,
                            'acccode' => $acccode,
                            'amount' => 1,
                            'category' => FortuneWheelLedger::CATEGORY_FREE_TOKEN,
                            'refid' => 0,
                            'object' => '',
                            'sys_ref' => FortuneWheelLedger::SYS_REF_FT_MONTHLY_LOGIN,
                            'remark' => '',
                            'expired_at' => Carbon::now()->addDays($tokenValidityDaysConfig['f']),
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ));
                    }
                }
            }
        }
    }

    public static function addDepositToken($accid, $acccode, $crccode, $depositAmount, $cashLedgerObjId = 0) {

        $crcPrefix = '';

        if ($crccode == 'IDR') {
            $crcPrefix = '_idr';
        }

        // Deposit token: RM150 = 1, RM3000 = 1 (+5), RM6000+ = 1 (+10), daily max = 100
        // IDR 200k = 1, daily max = 50
        if ($crccode == 'IDR') {
            FortuneWheelAccIdr::ensureExists($accid, $acccode);
        } else {
            FortuneWheelAcc::ensureExists($accid, $acccode);
        }

        $tokenValidityDaysConfig = json_decode(Configs::getParam('SYSTEM_LUCKYSPIN_TOKEN_VALIDITY_DAYS' . strtoupper($crcPrefix)), true);
        $tokenAmount = 0;
        $dailyMaxToken = 100;

        if ($crccode == 'IDR') {
            $dailyMaxToken = 50;

            if ($depositAmount >= 200000) {
                $tokenAmount = intval($depositAmount / 200000);
            }
        } else {
            if ($depositAmount >= 150) {
                $tokenAmount = intval($depositAmount / 150);
            }

            if ($depositAmount >= 6000) {
                $tokenAmount += 10;
            } elseif ($depositAmount >= 3000) {
                $tokenAmount += 5;
            }
        }

        // Check daily maximum token per member.
        $todayDt = Carbon::now()->toDateString();

        $todayTokenAmount = DB::table('fortune_wheel_ledger' . $crcPrefix)
            ->where('accid', '=', $accid)
            ->whereBetween('created_at', array($todayDt . ' 00:00:00', $todayDt . ' 23:59:59'))
            ->sum('amount');

        if ($todayTokenAmount + $tokenAmount > $dailyMaxToken) {
            $tokenAmount = $tokenAmount - ($todayTokenAmount + $tokenAmount - $dailyMaxToken);
        }

        if ($tokenAmount > 0) {
            for ($i = 0; $i < $tokenAmount; $i++) {
                DB::table('fortune_wheel_ledger' . $crcPrefix)->insert(array(
                    'accid' => $accid,
                    'acccode' => $acccode,
                    'amount' => 1,
                    'category' => FortuneWheelLedger::CATEGORY_NORMAL_TOKEN,
                    'refid' => 0,
                    'object' => '',
                    'sys_ref' => FortuneWheelLedger::SYS_REF_NT_DEPOSIT,
                    'remark' => 'CLREF#' . $cashLedgerObjId . ': Member deposit '. $crccode . ' ' . $depositAmount,
                    'expired_at' => Carbon::now()->addDays($tokenValidityDaysConfig['n']),
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ));
            }
        }
    }

    public static function clearCache() {

        $crcPrefix = ['', '_idr'];

        foreach ($crcPrefix as $c) {
            Cache::forget('fortune_wheel_prize_list_catalog' . $c);
            Cache::forget('fortune_wheel_prize_list' . $c);
            Cache::forget('fortune_wheel_jackpot_amount' . $c);
            Cache::forget('fortune_wheel_spin_prize' . $c);
            Cache::forget('fortune_wheel_spin_prize_lose' . $c);
            Cache::forget('fortune_wheel_spin_bonus' . $c);
            Cache::forget('fortune_wheel_spin_bonus_lose' . $c);
        }

        return json_encode(1);
    }
}