<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller\Admin;
use App\libraries\grid\PanelGrid;
use App\libraries\App;
use App\Models\Currency;
use const CBO_CHARTCODE_BONUS;
use function dd;
use Excel;
use Illuminate\Http\Request;
use Auth;
use Validator;
use Session;
use Lang;
use Hash;
use Config;
use App\Models\Affiliate;
use App\Models\Affiliatesetting;
use App\Models\Affiliatecredit;
use App\Models\Agent;
use App\Models\Account;
use App\Models\Affiliateregister;
use App\Models\Profitloss;
use App\Models\Cashledger;
use App\Models\Commissionsetting;
use App\Models\Commission;
use App\Models\Wagersetting;
use App\Models\Ledgersetting;
use App\Models\Configs;
use App\Models\Product;
use App\Models\Promocash;
use App\Models\Withdrawcard;
use App\Models\Worldcupteam;
use App\Models\Worldcuptoken;
use App\Models\Worldcuptrans;
use Carbon\Carbon;
use function var_dump;


class WorldCupController extends Controller{


    protected $moduleName 	= 'worldcup_trans';
    protected $limit 		= 1000;
    protected $start 		= 0;

    public function __construct()
    {

    }

    public function showWorldCupReport(Request $request){

        $grid = new PanelGrid;

        if( $request->has('aflid') )
        {
            $grid->setupGrid($this->moduleName, 'worldCupReport-data', 1000, true, array('params' => array('aflid' => $request->aflid,'aqfld' => $request->aqfld)));
        }
        else
        {
            $grid->setupGrid($this->moduleName, 'worldCupReport-data', 1000, true,array('params' => array('date' => $request->date,'id' => $request->id, 'category' => $request->category),'footer' => true));
        }
        $grid->setTitle(Lang::get('COMMON.REBATE'));

        $grid->addColumn('id', 				    Lang::get('COMMON.ID'), 				    100,		array('align' => 'left'));
        $grid->addColumn('name', 				Lang::get('COMMON.NAME'), 				    200,		array('align' => 'left'));
        $grid->addColumn('category', 			Lang::get('COMMON.CATEGORY'), 				250,		array('align' => 'left'));
        $grid->addColumn('selection', 			Lang::get('COMMON.SELECTION'), 				200,		array('align' => 'left'));
        $grid->addColumn('status', 				Lang::get('COMMON.STATUS'), 				100,		array('align' => 'left'));
        $grid->addColumn('wcgroup', 				Lang::get('COMMON.GROUP'), 				100,		array('align' => 'left'));
        $grid->addColumn('date', 				Lang::get('COMMON.DATE'), 				    100,		array('align' => 'left'));

        
        $grid->addSearchField('search1', array('name' => Lang::get('COMMON.NAME')));
        $grid->addFilter('icategory', Worldcupteam::getCategoryOptions(), array('display' => Lang::get('COMMON.CATEGORY')));

        $data['grid'] = $grid->getTemplateVars();
        
        return view('admin.grid2',$data);
    }
    public function showWorldCupReportdoGetData(Request $request){

        $aqfld              = $request->input('aqfld');
        $aflt               = $request->input('aflt');
        $createdfrom        = $request->input('createdfrom');
        $createdto          = $request->input('createdto');
        $accid              = $request->input('id');
        $category              = $request->input('category');
        $rows               = array();
        $footer             = array();
        $total_incentives   = array();

        $condition ='crccode ="'.Session::get('admin_crccode').'"';

        if(!empty($accid)){
            $condition .=' AND accid = "'.$accid.'" AND category_id = "'.$category.'"';
        }else{
            $condition .=' AND crccode ="'.Session::get('admin_crccode').'" and date(created)  >= "'.$createdfrom.'" AND date(created) <= "'.$createdto.'"';
        }
        if(!empty($aqfld['icategory'])){
            $condition .=' AND category_id="'.$aqfld['icategory'].'"';
        }else{
            $condition .=' AND category_id!=0';
        }

        if(!empty($aflt['name'])){
            $condition .=' AND nickname LIKE "'.$aflt['name'].'%'.'"';
        }

        if(!empty($accid)){
            $condition .=' AND accid = "'.$accid.'"';
        }
        
        $total = Worldcuptrans::whereRaw($condition)->count();

        if($objects = Worldcuptrans::whereRaw($condition)->skip($request->recordstartindex)->take($request->recordendindex)->orderBy('id','desc')->get()){
                foreach($objects as $object){
                    $rows[] = array(
                        'id'     	    => $object->id,
                        'name'     	    => $object->nickname,
                        'category'      => Worldcupteam::getCategoryText($object->category_id),
                        'selection'     => Worldcupteam::getTeamText($object->category_id,$object->team_id),
                        'status'	    => $object->getStatusText($object->status),
                        'wcgroup'	    => $object->group,
                        'date'   	    => $object->created->toDateString(),
                    );
//                    var_dump($object->created);
                }


            }
            echo json_encode(array('total' => $total,'rows' => $rows));
            exit;


    }

    public function showWorldCupPayoutReport(Request $request){

        $selectOptionCategorys=Worldcupteam::getCategoryOptions();
        $selectOptionTeams=Worldcupteam::getTeamNameOptions();
        $selectOptionGroups=Worldcupteam::getGroupOptions();
        $data['selectOptionCategorys']=$selectOptionCategorys;
        $data['selectOptionTeams']=$selectOptionTeams;
        $data['selectOptionGroups']=$selectOptionGroups;

        return view('admin.wcpayoutreport',$data);
    }
    public function showWorldCupPayoutReportdoGetData(Request $request){

        $category           = $request->input('category');
        $team               = $request->input('team');
        $wonDataSet = array();
        $loseDataSet = array();

        $condition = 'crccode ="'.Session::get('admin_crccode').'" AND status!=1';

        if((empty($category))||(empty($team))){
            $status = 0;
            exit;
        }

        if($category=='8'){
            $group  = $request->input('group');
        }else{
            $group  =   false;
        }

        if($objects = Worldcuptrans::whereRaw($condition)->get()){
//            Worldcupteam::getAmount($category,$team)
            foreach($objects as $object){

                    if(($team==$object->team_id)&&($category==$object->category_id)&&($group==$object->group)){
                        $wonDataSet[] = [
                            'id'			=> $object->id,
                        ];

                        $cashLedgerDataSet[] = [
                            'accid' 			=> $object->accid,
                            'acccode' 		    => Account::getAccCode($object->accid),
                            'accname'			=> $object->nickname,
                            'chtcode' 		    => CBO_CHARTCODE_BONUS,
                            'cashbalance' 	    => Cashledger::getBalance($object->accid),
                            'amount' 			=> Worldcupteam::getAmount($category,$team),
                            'amountlocal' 	    => Worldcupteam::getAmount($category,$team),
                            'refid' 			=> $object->id,
                            'status' 			=> CBO_LEDGERSTATUS_PENDING,
                            'refobj' 			=> 'Bonus',
                            'crccode'		    => $object->crccode,
                            'crcrate' 		    => Currency::getCurrencyRate($object->crccode),
                            'fee' 			    => '',
                            'feelocal' 		    => '',
                            'createdip'		    => App::getRemoteIp(),
                            'created'		    => Carbon::now()->toDateTimeString(),
                            'modified'		    => Carbon::now()->toDateTimeString(),

                        ];
                        $promocashDataSet[] = [
                            'accid' 			=> $object->accid,
                            'acccode' 		    => Account::getAccCode($object->accid),
                            'chtcode' 		    => CBO_CHARTCODE_BONUS,
                            'amount' 			=> Worldcupteam::getAmount($category,$team),
                            'amountlocal' 	    => Worldcupteam::getAmount($category,$team),
                            'crccode'		    => $object->crccode,
                            'crcrate' 		    => Currency::getCurrencyRate($object->crccode),
                        ];

                    }else if(($team!=$object->team_id)&&($category==$object->category_id)&&($group==$object->group)){
                        $loseDataSet[] = [
                            'id'			=> $object->id,
                        ];
                    }

            }
            if(!empty($wonDataSet)){
                if(Worldcuptrans::whereIn('id', $wonDataSet)->update(array('status' => 1))){
                    if(!empty($cashLedgerDataSet)){
                        //Promocash::insert($promocashDataSet);
                       // Cashledger::insert($cashLedgerDataSet);

                    }
                }
            }

            Worldcuptrans::whereIn('id', $loseDataSet)->update(array('status' => 2));
            $status = 1;


        }
        echo $status ;
        exit;
    }
    public function addToken(Request $request){

        $validator = Validator::make(
            [
                'username' 	  		 => $request->input('username'),
                'token'	  	 		 => $request->input('token'),
            ],
            [
                'username'	  	   => 'required',
                'token'	    	   => 'required',
            ]
        );


        if ($validator->fails())
        {
            echo json_encode($validator->errors());
            exit;
        }


        $username           = $request->input('username');
        $token              = $request->input('token');

        if(!empty($username)) {
            $AccountObj = Account::getAccObject($username);

            if($AccountObj==null){
                echo json_encode( array( 'code' => 'No this Account'));
                exit;
            }

            $wcTransDataSet[] = [
                'accid'         => $AccountObj->id,
                'nickname'      => $AccountObj->nickname,
                'crccode'       => $AccountObj->crccode,
                'category_id'   => 0,
                'team_id'       => 0,
                'status'        => 0,
                'created'       => Carbon::now()->toDateTimeString(),
                'modified'      => Carbon::now()->toDateTimeString(),
            ];

            if($wcTokenObj=Worldcuptoken::whereAccid($AccountObj->id)->first()) {
                $token=$wcTokenObj->token_amount+$token;
                Worldcuptoken::whereAccid($AccountObj->id)->update(array('token_amount' => $token));
            }else{
                $wcTokenDataSet[] = [
                    'accid'        => $AccountObj->id,
                    'nickname'     => $AccountObj->nickname,
                    'crccode'      => $AccountObj->crccode,
                    'token_amount' => $token,
                    'created'      => Carbon::now()->toDateTimeString(),
                    'modified'     => Carbon::now()->toDateTimeString(),
                ];
                Worldcuptoken::insert($wcTokenDataSet);
            }

            Worldcuptrans::insert($wcTransDataSet);
            echo json_encode( array( 'code' => 'Successful'));
            exit;
        }

    }
    public function showTokenReport(Request $request){

        $grid = new PanelGrid;

        if( $request->has('aflid') )
        {
            $grid->setupGrid($this->moduleName, 'showTokenReport-data', $this->limit, true, array('params' => array('aflid' => $request->aflid,'aqfld' => $request->aqfld)));
        }
        else
        {
            $grid->setupGrid($this->moduleName, 'showTokenReport-data', $this->limit, false,array('params' => array('date' => $request->date)));
        }
        $grid->setTitle(Lang::get('COMMON.REBATE'));

        $grid->addColumn('name', 				Lang::get('COMMON.NAME'), 				    150,		array('align' => 'left'));
        $grid->addColumn('category1', 			"Winner", 			                    	60,		array('align' => 'left'));
        $grid->addColumn('category2', 			"Finalists", 			                    60,		array('align' => 'left'));
        $grid->addColumn('category3', 			"Team to Finals", 			                100,		array('align' => 'left'));
        $grid->addColumn('category4', 			"Team to Semi Finals", 			            100,		array('align' => 'left'));
        $grid->addColumn('category5', 			"Team to Quarter Finals", 			        100,		array('align' => 'left'));
        $grid->addColumn('category6', 			"Higher Team Goal", 			            100,		array('align' => 'left'));
        $grid->addColumn('category7', 			"Top Goal Scorer", 			                100,		array('align' => 'left'));
        $grid->addColumn('category8', 			"Group Winner", 			                100,		array('align' => 'left'));
        $grid->addColumn('amountToken', 		"Left Token", 			                    100,		array('align' => 'left'));


        $grid->addSearchField('search1', array('name' => Lang::get('COMMON.NAME')));
        $data['grid'] = $grid->getTemplateVars();

        return view('admin.grid2',$data);

    }
    public function showTokenReporttdoGetData(Request $request){

        $aqfld              = $request->input('aqfld');
        $aflt               = $request->input('aflt');
        $createdfrom        = $request->input('createdfrom');
        $createdto          = $request->input('createdto');
        $rows               = array();
        $footer             = array();
        $total_incentives   = array();

        $condition = ' crccode ="'.Session::get('admin_crccode').'"';
//
//        if(!empty($aqfld['icategory'])){
//            $condition .=' AND category_id="'.$aqfld['icategory'].'"';
//        }else{
//            $condition .=' AND category_id!=0';
//        }
//
//        if(!empty($aflt['name'])){
//            $condition .=' AND nickname LIKE "'.$aflt['name'].'%'.'"';
//        }
        if($objects = Worldcuptoken::whereRaw($condition)->skip($request->recordstartindex)->take($request->recordendindex)->orderBy('token_amount','desc')->get()){
            foreach($objects as $object){

                $action1 = '<a href="#" onclick="parent.addTab(\''.Lang::get('WCREPORT').': \', \'worldCupReport?id='.$object->accid.'&category=1\')">'.$object->category_one.'</a>';
                $action2 = '<a href="#" onclick="parent.addTab(\''.Lang::get('WCREPORT').': \', \'worldCupReport?id='.$object->accid.'&category=2\')">'.$object->category_two.'</a>';
                $action3 = '<a href="#" onclick="parent.addTab(\''.Lang::get('WCREPORT').': \', \'worldCupReport?id='.$object->accid.'&category=3\')">'.$object->category_three.'</a>';
                $action4 = '<a href="#" onclick="parent.addTab(\''.Lang::get('WCREPORT').': \', \'worldCupReport?id='.$object->accid.'&category=4\')">'.$object->category_four.'</a>';
                $action5 = '<a href="#" onclick="parent.addTab(\''.Lang::get('WCREPORT').': \', \'worldCupReport?id='.$object->accid.'&category=5\')">'.$object->category_five.'</a>';
                $action6 = '<a href="#" onclick="parent.addTab(\''.Lang::get('WCREPORT').': \', \'worldCupReport?id='.$object->accid.'&category=6\')">'.$object->category_six.'</a>';
                $action7 = '<a href="#" onclick="parent.addTab(\''.Lang::get('WCREPORT').': \', \'worldCupReport?id='.$object->accid.'&category=7\')">'.$object->category_seven.'</a>';
                $action8 = '<a href="#" onclick="parent.addTab(\''.Lang::get('WCREPORT').': \', \'worldCupReport?id='.$object->accid.'&category=8\')">'.$object->category_eight.'</a>';
                $rows[] = array(
                    'name'     	    => $object->nickname,
                    'category1'     => $object->category_one    == 0 ? '0' : $action1,
                    'category2'     => $object->category_two    == 0 ? '0' : $action2,
                    'category3'     => $object->category_three  == 0 ? '0' : $action3,
                    'category4'     => $object->category_four   == 0 ? '0' : $action4,
                    'category5'     => $object->category_five   == 0 ? '0' : $action5,
                    'category6'     => $object->category_six    == 0 ? '0' : $action6,
                    'category7'     => $object->category_seven  == 0 ? '0' : $action7,
                    'category8'     => $object->category_eight  == 0 ? '0' : $action8,
                    'amountToken'   => $object->token_amount,
                );
//                    var_dump($object->created);
            }


        }
        echo json_encode(array('rows' => $rows));
        exit;



    }
    
    public function doDepositToken(Request $request){
        $condition = 'created >="2018-05-15 12:00:00" AND created <="2018-06-05 23:59:59" AND crccode = "MYR" AND chtcode = 1 AND status = 4';
        if($deposits = Cashledger::whereRaw($condition)->groupBy('accid')->selectRaw('*, count(*) as totaltimes, sum(amount) as totalamount')->get()){
            foreach($deposits as $deposit){
                if($deposit->totalamount >= 1000 && $deposit->totaltimes >= 20){
                    $token = 0;
                    if($deposit->totalamount >= 1000 && $deposit->totalamount <= 4999){
                        if($deposit->totaltimes >= 20)
                            $token = 5;
                    }
                    else if($deposit->totalamount >= 5000 && $deposit->totalamount <= 9999){
                        if($deposit->totaltimes >= 20)
                            $token = 5;
                        if($deposit->totaltimes >= 25)
                            $token = 10;
                    }
                    else if($deposit->totalamount >= 10000 && $deposit->totalamount <= 19999){
                        if($deposit->totaltimes >= 20)
                            $token = 5;
                        if($deposit->totaltimes >= 25)
                            $token = 10;
                        if($deposit->totaltimes >= 30)
                            $token = 15;
                    }
                    else if($deposit->totalamount >= 20000 && $deposit->totalamount <= 49999){
                        if($deposit->totaltimes >= 20)
                            $token = 5;
                        if($deposit->totaltimes >= 25)
                            $token = 10;
                        if($deposit->totaltimes >= 30)
                            $token = 15;
                        if($deposit->totaltimes >= 40)
                            $token = 20;
                    }
                    else if($deposit->totalamount >= 50000){
                        if($deposit->totaltimes >= 20)
                            $token = 5;
                        if($deposit->totaltimes >= 25)
                            $token = 10;
                        if($deposit->totaltimes >= 30)
                            $token = 15;
                        if($deposit->totaltimes >= 40)
                            $token = 20;
                        if($deposit->totaltimes >= 50)
                            $token = 25;
                    }

                    $object = new Worldcuptoken;
                    $object->accid = $deposit->accid;
                    $object->nickname = Account::getNickname($deposit->accid);
                    $object->crccode = 'MYR';
                    $object->category_one = 0;
                    $object->category_three = 0;
                    $object->category_four = 0;
                    $object->category_five = 0;
                    $object->category_six = 0;
                    $object->category_seven = 0;
                    $object->category_eight = 0;
                    $object->token_amount = $token;
                    $object->save();
                }
            }
        }
    }

    public function doPnlToken(Request $request){
        $condition = 'date >= "2018-05-15" AND date <= "2018-06-05" AND crccode = "MYR"';
        if($turnovers = Profitloss::whereRaw($condition)->groupBy('accid')->selectRaw('*, sum(validstake) as totalturnover')->get()){
            foreach($turnovers as $turnover){
                if($turnover->totalturnover >= 30000){
                    $token = 0;
                    if($turnover->totalturnover >= 30000 && $turnover->totalturnover <= 149999){
                            $token = 5;
                    }
                    if($turnover->totalturnover >= 150000 && $turnover->totalturnover <= 299999){
                            $token = 10;
                    }
                    if($turnover->totalturnover >= 300000 && $turnover->totalturnover <= 599999){
                            $token = 15;
                    }
                    if($turnover->totalturnover >= 600000 && $turnover->totalturnover <= 1499999){
                            $token = 20;
                    }
                    if($turnover->totalturnover >= 1500000){
                            $token = 25;
                    }
                    
                    if(Worldcuptoken::whereRaw('accid ='.$turnover->accid)->count() == 0){
                        $object = new Worldcuptoken;
                        $object->accid = $turnover->accid;
                        $object->nickname = Account::getNickname($turnover->accid);
                        $object->crccode = 'MYR';
                        $object->category_one = 0;
                        $object->category_three = 0;
                        $object->category_four = 0;
                        $object->category_five = 0;
                        $object->category_six = 0;
                        $object->category_seven = 0;
                        $object->category_eight = 0;
                        $object->token_amount = $token;
                        $object->save();                   
                    }else{
                        $object = Worldcuptoken::whereRaw('accid ='.$turnover->accid)->first();
                        $object->token_amount += $token;
                        $object->save(); 
                    }
                }
                
            }
        }
    }

}
