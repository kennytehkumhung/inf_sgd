<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller\Admin;
use App\libraries\grid\PanelGrid;
use App\libraries\App;
use App\Models\Language;					
use Illuminate\Http\Request;
use Auth;
use Validator;
use Session;
use Lang;
use App\Models\Currency;
use App\Models\Fundingmethod;
use App\Models\Cashledger;
use App\Models\Bank;
use App\Models\Media;
use App\Models\Account;
use Config;
use App\Models\Promocash;
use App\Models\Profitloss;
use App\Models\Rejectreason;
use App\Models\Banktransfer;
use App\Models\Promocampaign;
use App\Models\Configs;
use App\Models\Tag;
use Storage;
use File;

class PromocampaignController extends Controller{


	protected $moduleName 	= 'promocampaign';
	protected $limit 		= 20;
	protected $start 		= 0;
	
	public function __construct()
	{
		
	}
	
	public function index(Request $request){

			$grid = new PanelGrid;
			$grid->setupGrid($this->moduleName, 'promocampaign-data', $this->limit);
			$grid->setTitle(Lang::get('COMMON.PROMOCAMPAIGN'));
			
			$grid->addColumn('id', 				Lang::get('COMMON.ID'), 				0,			array('align' => 'left'));			
			$grid->addColumn('name', 			Lang::get('COMMON.NAME'), 				250,                    array('align' => 'left'));			
			$grid->addColumn('code', 			Lang::get('COMMON.CODE'), 				68,			array('align' => 'left'));			
			$grid->addColumn('currency',                    Lang::get('COMMON.CURRENCY'),                           68,			array('align' => 'left'));			
			$grid->addColumn('type', 			Lang::get('COMMON.TYPE'), 				138,			array('align' => 'center'));			
			$grid->addColumn('from', 			Lang::get('COMMON.FROM'), 				168,                    array('align' => 'left'));			
			$grid->addColumn('to', 				Lang::get('COMMON.TO'), 				168,                    array('align' => 'left'));				
			$grid->addColumn('bonus', 			Lang::get('COMMON.BONUS'), 				68,			array('align' => 'center'));			
			$grid->addColumn('turnover',                    Lang::get('COMMON.TURNOVER'),                           100,                    array('align' => 'center'));
            
			$grid->addColumn('tag', 			Lang::get('COMMON.TAG'), 				200,                    array('align' => 'left'));			
			$grid->addColumn('status', 			Lang::get('COMMON.STATUS'),                             100,			array('align' => 'center'));			

			$grid->addFilter('status',  Promocampaign::getStatusOptions(), array('display' => Lang::get('COMMON.TYPE')));
			$grid->addButton('1', 'New', Lang::get('COMMON.ADDNEW').' '.Lang::get('COMMON.PROMOCAMPAIGN'), 'button', array('icon' => 'add', 'url' => action('Admin\PromocampaignController@drawAddEdit')));


			$data['grid'] = $grid->getTemplateVars();

			return view('admin.grid2',$data);
    }
	
	protected function doGetData(Request $request){
	
		
		//type
		$type[1] = 'Percentage';
		$type[2] = 'Cash';
		
		$rows = array();
		
		$aflt  = $request->input('aflt');
		$aqfld = $request->input('aqfld');
		
		$condition = '1';
		$condition .= Session::get('admin_crccode') == 'MYR' ? '' : ' AND crccode = "'.Session::get('admin_crccode').'"';
		
		if( isset($aqfld['status']) && $aqfld['status'] != '' ){
			$condition .= ' AND status'.'='.$aqfld['status'] ;
		}
		
		$total = Promocampaign::whereRaw($condition)->count();

		//new structure
		if($promos = Promocampaign::whereRaw($condition)->skip($request->recordstartindex)->take( $request->pagesize )->orderBy('id','desc')->get()){
			foreach( $promos as $promo ){
				$body = App::formatAddTabUrl( Lang::get('PROMOCAMPAIGN').': '.$promo->id, urlencode($promo->name), action('Admin\PromocampaignController@drawAddEdit',  array('sid' => $promo->id)));
			
				$rows[] = array(

					'id'  				=> $promo->id, 
					'name'  			=> urldecode($body), 
					'code'  			=> $promo->code,
					'currency'                      => $promo->crccode,
					'type'  			=> $type[$promo->type],
					'from'  			=> $promo->startdate,
					'to'  				=> $promo->enddate,
					'bonus'  			=> $promo->amount,
					'turnover'                      => $promo->turnover,
					'tag'                           => Tag::whereRaw('id'.'='.$promo->tag)->pluck('name'),
					'status'  			=> $promo->getStatusText($this->moduleName),

				);	
				
			}
				
		}

				
		echo json_encode(array('total' => $total, 'rows' => $rows));
		exit;
	
	
	}
	
	protected function drawAddEdit(Request $request) { 

	    $toAdd 	  = true;
		$object   = null;
		$readOnly = false;
		$values   = array();

		$title = Lang::get('NEW').' '.Lang::get('PROMOCAMPAIGN');

		if($request->has('sid')) 
		{
	
			if ($object = Promocampaign::find($request->input('sid'))) 
				{
					$title    = Lang::get('COMMON.EDIT').' '.Lang::get('COMMON.PROMOCAMPAIGN');
					$toAdd    = false;
					$readOnly = true;

				} 
			else
				{
					$object = null;
				}
			
		}
		
		$methodOptions = Promocampaign::getMethodOptions();
		App::arrayUnshift($methodOptions, Lang::get('COMMON.SELECT'));
		
                $taglist   = Tag::getAllAsOptions();
                App::arrayUnshift($taglist, Lang::get('COMMON.SELECT'));
                
		$currency  = Currency::getAllCurrencyAsOptions();
		$language  = Language::getLanguageAsOptions();											
		$form = App::setupPanelForm('doadd', $request->all(), array(), $toAdd);
		$form->setFormTitle($title);
		
		$form->addInput((($toAdd)? 'text':'showonly'), 		Lang::get('COMMON.CODE')		, 'code',	 		(($object)? $object->code:''), 			array(),false);
		$form->addInput('text', 				Lang::get('COMMON.NAME') 		, 'name', 	 		(($object)? $object->name:''),                  array(), false); 
		$form->addInput('html', 				Lang::get('COMMON.DESC') 		, 'description',                (($object)? $object->desc:''), 			array(), false); 
		$form->addInput('file', 				Lang::get('COMMON.THUMBNAIL')           , 'thumbnail',                  (($object)? $object->getImage(Media::TYPE_THUMBNAIL):''), 			array(), false);
		$form->addInput('file', 				Lang::get('COMMON.IMAGE') 		, 'image', 			(($object)? $object->getImage(Media::TYPE_IMAGE):''), 			array(), false);

          foreach ($language as $languageKey => $languageVal) {
            $form->addInput('file', Lang::get('COMMON.IMAGE') . ' (' . $languageKey . ')', 'image_' . $languageKey, (($object) ? $object->getImage(Media::TYPE_IMAGE) : ''), array(), false);
            if (!is_null(Config::get('setting.front_mobile_path'))) {
                $form->addInput('file', Lang::get('public.ImageForMobile') . ' (' . $languageKey . ')', 'image_for_mobile_' . $languageKey, (($object) ? $object->getImage(Media::TYPE_IMAGE_MOBILE) : ''), array(), false);
            }
        }
		$form->addInput('select', 				Lang::get('COMMON.CURRENCY')            , 'crccode',                    (($object)? $object->crccode:''), 		array('options' => $currency),true); 
		$form->addInput('select', 				Lang::get('COMMON.METHOD')		, 'method',	 		(($object)? $object->method:''), 		array('options' => $methodOptions),true); 
		$form->addInput('datetime',                             Lang::get('COMMON.FROM')		, 'from',	 		(($object)? $object->startdate:''),             array(), false); 
		$form->addInput('datetime',                             Lang::get('COMMON.TO')			, 'to',	 			(($object)? $object->enddate:''), 		array(), false); 
		$form->addInput('radio',                                Lang::get('COMMON.TYPE')		, 'type',  			(($object)? $object->type:''), 			array('options' => Promocampaign::getBonusType()), true);
        $form->addInput('select',                               Lang::get('COMMON.TAG')			, 'tag',                        (($object)? $object->tag:''),                   array('options' => $taglist),true);
		$form->addInput('text', 				Lang::get('COMMON.PERCENTAGE').' / '.Lang::get('COMMON.AMOUNT') 	, 'percentage', 	(($object)? $object->amount:''), 		array(), false);	
		$form->addInput('text', 				Lang::get('COMMON.MAXIMUM').' '.Lang::get('COMMON.AMOUNT') 	, 'maxamount', 	(($object)? $object->maxamount:''), 		array(), false);	
		$form->addInput('text', 				Lang::get('COMMON.MINIMUM').' '.Lang::get('COMMON.DEPOSIT') 	, 'mindepposit', 	(($object)? $object->mindeposit:''), 		array(), false);	
		$form->addInput('text', 				Lang::get('COMMON.TURNOVER') 	, 'turnover', 		(($object)? $object->turnover:''), 		array(), false);
		$form->addInput('radio',                                Lang::get('COMMON.STATUS')		, 'status',  		(($object)? $object->status:''), 		array('options' => App::getStatusOptions()), true);		
	
	//var_dump($form->getTemplateVars());
		$data['form'] = $form->getTemplateVars();
		$data['module'] = 'promocampaign';

		return view('admin.form2',$data);
	
	}
	
	protected function doAddEdit(Request $request) {
		
		$validator = Validator::make(
			[
	
				'name'	  	  		 => $request->input('name'),
				'description'                    => $request->input('description'),
				'from'	  	 		 => $request->input('from'),
				'to'	  	 		 => $request->input('to'),
				'percentage'                     => $request->input('percentage'),
				'maxamount'	  	 	 => $request->input('maxamount'),
				'mindepposit'                    => $request->input('mindepposit'),
				'turnover'	  	 	 => $request->input('turnover'),
				'method'	  	 	 => $request->input('method'),
				'type'	  	 	 	 => $request->input('type'),
				'status'	  	 	 => $request->input('status'),

			],
			[

                                'name'	  	  		 => 'required',
				'description'                    => 'required',
				'from'	  	 		 => 'required',
				'to'	  	 		 => 'required',
				'percentage'                     => 'required',
				'maxamount'	  	 	 => 'required',
				'mindepposit'                    => 'required',
				'turnover'	  	 	 => 'required',
				'method'	  	 	 => 'required',
				'type'                           => 'required',
				'status'	  	 	 => 'required',

			]
		);
		
		
		if ($validator->fails())
		{
			echo json_encode($validator->errors());
			exit;
		}
		
		if(!$request->has('sid')){
			
			$validator = Validator::make(
				[
				   'code'	  	  		 => $request->input('code'),
				],
				[
				   'code'	  	  		 => 'required',
				]
			);
		
		
			if ($validator->fails())
			{
				echo json_encode($validator->errors());
				exit;
			}
		
		}
		
		$object  = new Promocampaign;
		 $language  	= Language::getLanguageAsOptions();
		if($request->has('sid')) 
		{
			$object = Promocampaign::find($request->input('sid'));
		}else{
			$object->code	  	 	 = $request->input('code');
		}
				
		$object->name	  	  		 = $request->input('name');
		$object->desc	  	 		 = $request->input('description');
		$object->startdate	  	 	 = $request->input('from');
		$object->enddate	  	 	 = $request->input('to');
		$object->amount	  	 		 = $request->input('percentage');
		$object->maxamount	  	 	 = $request->input('maxamount');
		$object->mindeposit	  		 = $request->input('mindepposit');
		$object->turnover	  	 	 = $request->input('turnover');
		$object->type	  	 	 	 = $request->input('type');
                $object->tag         = $request->has('tag') ? $request->input('tag') : 0;
		$object->method	  	 		 = $request->input('method');
		$object->crccode	  	 	 = $request->input('crccode');
		$object->status	  	 	 	 = $request->input('status');

		if( $object->save() )
		{
			foreach ($language as $languageKey => $languageVal) {
                if ($request->hasFile('image_'.$languageKey)) {
                    $file = $request->file('image_'.$languageKey);
                    $filename = 'promo/' . md5($file->getClientOriginalName() . time()) . '.' . $file->getClientOriginalExtension();
                    $result = Storage::disk('s3')->put($filename, File::get($file), 'public');


                    if ($result) {
                        if (Media::where('refobj', '=', 'PromoCampaign')->where('refid', '=', $object->id)->where('type', '=', Media::TYPE_IMAGE)->where('Plang', '=', $languageKey)->count() == 0) {
                            $medObj = new Media;
                        } else {
                            $medObj = Media::where('refobj', '=', 'PromoCampaign')->where('refid', '=', $object->id)->where('type', '=', Media::TYPE_IMAGE)->where('Plang', '=', $languageKey)->first();
                        }

                        $medObj->refobj = 'PromoCampaign';
                        $medObj->refid = $object->id;
                        $medObj->type = Media::TYPE_IMAGE;
                        $medObj->domain = 'https://' . Configs::getParam('SYSTEM_AWS_S3BUCKET');
                        $medObj->path = $filename;
                        $medObj->Plang = $languageKey;
                        $medObj->save();
                    }
                }
                if( $request->hasFile('image_for_mobile_'.$languageKey))
                {
                    $file = $request->file('image_for_mobile_'.$languageKey);

                    $filename = 'promo/'.md5($file->getClientOriginalName().time()).'.'. $file->getClientOriginalExtension();
                    $result = Storage::disk('s3')->put($filename,  File::get($file), 'public');


                    if($result)
                    {
                        if( Media::where( 'refobj' , '=' , 'PromoCampaign' )->where( 'refid' , '=' , $object->id )->where( 'type' , '=' , Media::TYPE_IMAGE_MOBILE )->where('Plang','=',$languageKey)->count() == 0 ){
                            $medObj = new Media;
                        }else{
                            $medObj = Media::where( 'refobj' , '=' , 'PromoCampaign' )->where( 'refid' , '=' , $object->id )->where( 'type' , '=' , Media::TYPE_IMAGE_MOBILE )->where('Plang','=',$languageKey)->first();
                        }

                        $medObj->refobj = 'PromoCampaign';
                        $medObj->refid  = $object->id;
                        $medObj->type   = Media::TYPE_IMAGE_MOBILE;
                        $medObj->domain = 'https://'.Configs::getParam('SYSTEM_AWS_S3BUCKET');
                        $medObj->path   = $filename;
                        $medObj->Plang 	= $languageKey;
                        $medObj->save();
                    }
                }
            }
			if( $request->hasFile('thumbnail'))
			{
				$file = $request->file('thumbnail');
			
				$filename = 'promo/'.md5($file->getClientOriginalName().time()).'.'. $file->getClientOriginalExtension();
				$result = Storage::disk('s3')->put($filename,  File::get($file), 'public');


				if($result) 
				{
					if( Media::where( 'refobj' , '=' , 'PromoCampaign' )->where( 'refid' , '=' , $object->id )->where( 'type' , '=' , Media::TYPE_THUMBNAIL )->count() == 0 ){
						$medObj = new Media;
					}else{
						$medObj = Media::where( 'refobj' , '=' , 'PromoCampaign' )->where( 'refid' , '=' , $object->id )->where( 'type' , '=' , Media::TYPE_THUMBNAIL )->first();
					}
					
					$medObj->refobj = 'PromoCampaign';
					$medObj->refid  = $object->id;
					$medObj->type   = Media::TYPE_THUMBNAIL;
					$medObj->domain = 'https://'.Configs::getParam('SYSTEM_AWS_S3BUCKET');
					$medObj->path   = $filename;
					$medObj->save(); 
				}
			}
            if( $request->hasFile('image_for_mobile'))
            {
                $file = $request->file('image_for_mobile');

                $filename = 'promo/'.md5($file->getClientOriginalName().time()).'.'. $file->getClientOriginalExtension();
                $result = Storage::disk('s3')->put($filename,  File::get($file), 'public');


                if($result)
                {
                    if( Media::where( 'refobj' , '=' , 'PromoCampaign' )->where( 'refid' , '=' , $object->id )->where( 'type' , '=' , Media::TYPE_IMAGE_MOBILE )->count() == 0 ){
                        $medObj = new Media;
                    }else{
                        $medObj = Media::where( 'refobj' , '=' , 'PromoCampaign' )->where( 'refid' , '=' , $object->id )->where( 'type' , '=' , Media::TYPE_IMAGE_MOBILE )->first();
                    }

                    $medObj->refobj = 'PromoCampaign';
                    $medObj->refid  = $object->id;
                    $medObj->type   = Media::TYPE_IMAGE_MOBILE;
                    $medObj->domain = 'https://'.Configs::getParam('SYSTEM_AWS_S3BUCKET');
                    $medObj->path   = $filename;
                    $medObj->save();
                }
            }
			echo json_encode( array( 'code' => Lang::get('COMMON.SUCESSFUL') ) );
			exit;
		}

		echo json_encode( array( 'code' => Lang::get('COMMON.FAILED') ) );
		
	}
	
	public function showPromoReport(Request $request){
		
		$grid = new PanelGrid;
		$grid->setupGrid($this->moduleName, 'promoReport-data', $this->limit);
		$grid->setTitle(Lang::get('COMMON.PROMOCAMPAIGN'));
		
		$grid->addColumn('name', 			Lang::get('COMMON.NAME'), 				150,			array('align' => 'left'));			
		$grid->addColumn('apply', 			Lang::get('COMMON.APPLIED'), 				250,			array('align' => 'left'));			
		$grid->addColumn('approved', 			Lang::get('COMMON.APPROVED'), 				68,			array('align' => 'left'));			
		$grid->addColumn('amount', 			Lang::get('COMMON.AMOUNT'), 				68,			array('align' => 'left'));			
		$grid->addColumn('deposit', 			Lang::get('COMMON.DEPOSIT'), 				138,			array('align' => 'center'));			
		$grid->addColumn('withdrawal', 			Lang::get('COMMON.WITHDRAWAL'), 			168,			array('align' => 'left'));			
		$grid->addColumn('balance', 			Lang::get('COMMON.BALANCE'), 				168,			array('align' => 'left'));				
		$grid->addColumn('newmember', 			Lang::get('COMMON.NEWMEMBER'), 				68,			array('align' => 'center'));			
		$grid->addColumn('oldmember', 			Lang::get('COMMON.OLDMEMBER'), 				100,			array('align' => 'center'));					
		$grid->addColumn('turnover', 			Lang::get('COMMON.TURNOVER'), 				100,			array('align' => 'center'));		
		$grid->addColumn('validbet', 			Lang::get('COMMON.VALIDBET'), 				100,			array('align' => 'center'));		
		


		$data['grid'] = $grid->getTemplateVars();
		$data['paging'] = false;
		return view('admin.grid2',$data);
		
	}
	
	protected function PromoReportdoGetData(Request $request){

		$total = Promocampaign::whereRaw('1')->count();

		if($promos = Promocampaign::whereRaw('1')->get()) {

			$fromtime 	= $request->createdfrom.' 00:00:00';
			$totime 	= $request->createdto.' 23:59:59';
			
			foreach($promos as $promo) {
				$condition   = 'chtcode = '.CBO_CHARTCODE_BONUS.' AND created>="'.$fromtime.'" AND created<="'.$totime.'" AND (status ='.CBO_LEDGERSTATUS_CONFIRMED.' OR status='.CBO_LEDGERSTATUS_MANCONFIRMED.')';
				$apply 		 = Promocash::whereRaw('pcpid='.$promo->id.' AND created >= "'.$fromtime.'" AND created <= "'.$totime.'"')->count();
				$approved 	 = 0;
				$amount 	 = 0;
				$firstdep 	 = array();
				$continuedep = array();
				$deposit	 = array();
				if($ledgers = Cashledger::whereRaw($condition)->get()) {
					$max = count($ledgers); 
					$accid = array();
					foreach($ledgers as $ledger) {
						$approved += 1;
						$amount += $ledger->amount;
						$deposit[$ledger->accid] = 1;
						$totaldeposit[$ledger->accid] = 1;
						
						if(Account::whereRaw('id='.$ledger->accid.' AND totaldeposit > 1')->count() != 0) {
							$continuedep[$ledger->accid] = 1;
							$totalcontinue[$ledger->accid] = 1;
						} else {
							$firstdep[$ledger->accid] = 1;
						}
						$accid[$ledger->accid] = $ledger->accid;
					}
					$deposit            = Cashledger::whereRaw('created >= "'.$fromtime.'" AND created <= "'.$totime.'" AND chtcode = '.CBO_CHARTCODE_DEPOSIT.' AND status IN ('.CBO_LEDGERSTATUS_CONFIRMED.', '.CBO_LEDGERSTATUS_MANCONFIRMED.')')->sum('amount');
					$withdrawal         = Cashledger::whereRaw('created >= "'.$fromtime.'" AND created <= "'.$totime.'" AND chtcode = '.CBO_CHARTCODE_WITHDRAWAL.' AND status IN ('.CBO_LEDGERSTATUS_CONFIRMED.', '.CBO_LEDGERSTATUS_MANCONFIRMED.')')->sum('amount');
					$bonus              = Cashledger::whereRaw('created >= "'.$fromtime.'" AND created <= "'.$totime.'" AND chtcode = '.CBO_CHARTCODE_BONUS.' AND status IN ('.CBO_LEDGERSTATUS_CONFIRMED.', '.CBO_LEDGERSTATUS_MANCONFIRMED.')')->sum('amount');
					$incentive          = Cashledger::whereRaw('created >= "'.$fromtime.'" AND created <= "'.$totime.'" AND chtcode = '.CBO_CHARTCODE_INCENTIVE.' AND status IN ('.CBO_LEDGERSTATUS_CONFIRMED.', '.CBO_LEDGERSTATUS_MANCONFIRMED.')')->sum('amount');
					$adjustment         = Cashledger::whereRaw('created >= "'.$fromtime.'" AND created <= "'.$totime.'" AND chtcode = '.CBO_CHARTCODE_ADJUSTMENT.' AND status IN ('.CBO_LEDGERSTATUS_CONFIRMED.', '.CBO_LEDGERSTATUS_MANCONFIRMED.')')->sum('amount');
					$totalstake	    = Profitloss::whereRaw('date >= "'.$request->createdfrom.'" AND date <= "'.$request->createdto.'"')->sum('totalstake');
					$validstake	    = Profitloss::whereRaw('date >= "'.$request->createdfrom.'" AND date <= "'.$request->createdto.'"')->sum('validstake');
					$winloss 	    = Profitloss::whereRaw('date >= "'.$request->createdfrom.'" AND date <= "'.$request->createdto.'"')->sum('amount');
					
					$newmember = count($firstdep);
					$oldmember = count($continuedep);
					$balance = $deposit + $bonus + $incentive + $adjustment + $winloss + $withdrawal;
					$body = App::formatAddTabUrl( Lang::get('PROMOCAMPAIGN').': '.$promo->id, urlencode($promo->name), action('Admin\PromocampaignController@PromoReportdrawAddEdit',  array('sid' => $promo->id)));
					
					
				
					$rows[] = array(

						'name'  			=> $promo->name, 
						'apply'  			=> ($apply?$apply:'0'), 
						'amount'			=> $amount,
						'deposit' 			=> App::displayAmount($deposit), 
						'withdrawal'                    => App::displayAmount($withdrawal), 
						'balance'			=> App::displayAmount($balance), 
						'newmember'			=> App::displayAmount($newmember), 
						'oldmember'			=> App::displayAmount($oldmember), 
						'approved'                      => ($approved?$approved:'0'), App::displayAmount($amount), App::displayAmount($deposit), App::displayAmount($withdrawal), App::displayAmount($balance), ($newmember?$newmember:'0'), ($oldmember?$oldmember:'0'), 
						'turnover'                      => App::displayAmount($totalstake), 
						'validbet'                      => App::displayAmount($validstake), 
						'winloss'  			=> App::formatNegativeAmount($winloss), 

					);

				}
				
			}
		}

		echo json_encode(array('total' => $total, 'rows' => $rows));
		exit;
		
	}
	
	protected function PromoReportdrawAddEdit(Request $request){
		
	}
	
	protected function doActivateSuspend(Request $request) {

			

		if($request->has('id')) {
		
			if($object = Promocampaign::find($request->id)) {
			
				$currentStatus = $object->status;
				if($request->action == 'activate') {
					$object->status 	= CBO_ACCOUNTSTATUS_ACTIVE;
				}
				if($request->action == 'suspend') {
					$object->status 	= CBO_ACCOUNTSTATUS_SUSPENDED;
				}

				if($currentStatus <> $object->status){
					if($object->save()) {
					
						echo json_encode( array( 'code' => Lang::get('COMMON.SUCESSFUL') ) );
						exit;
					}
				}
			}
		}
		
		
		echo json_encode( array( 'code' => Lang::get('COMMON.FAILED') ) );

	}
}

?>
