<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller\Admin;
use App\libraries\grid\PanelGrid;
use App\libraries\App;
use App\Models\Currency;
use Excel;
use Illuminate\Http\Request;
use Auth;
use Validator;
use Session;
use Lang;
use Hash;
use Config;
use App\Models\Affiliate;
use App\Models\Affiliatesetting;
use App\Models\Affiliatecredit;
use App\Models\Agent;
use App\Models\Account;
use App\Models\Affiliateregister;
use App\Models\Profitloss;
use App\Models\Cashledger;
use App\Models\Commissionsetting;
use App\Models\Commission;
use App\Models\Wagersetting;
use App\Models\Ledgersetting;
use App\Models\Configs;
use App\Models\Product;
use App\Models\Cashcard;


class CashcardController extends Controller{


	protected $moduleName 	= 'cashcard';
	protected $limit 		= 20;
	protected $start 		= 0;
	
	public function __construct()
	{
		
	}
	
	public function index(Request $request){

	
			$grid = new PanelGrid;
			$grid->setupGrid($this->moduleName, 'showCashcard-data', $this->limit, true, array('footer' => true ,'params' => array( 'aflid' => $request->aflid ,'createdfrom' => $request->createdfrom , 'createdto' => $request->createdto)));
			$grid->setTitle(Lang::get('COMMON.CASHCARD'));
					
			$grid->addColumn('amount', 			Lang::get('COMMON.AMOUNT'), 					100,		array('align' => 'center'));			
			$grid->addColumn('currency', 		Lang::get('COMMON.CURRENCY'), 					100,		array('align' => 'center'));			
			$grid->addColumn('nickname', 		Lang::get('COMMON.ACCOUNT'), 					100,		array('align' => 'center'));			
			$grid->addColumn('serial', 			'Serial', 										70,			array('align' => 'center'));			
			$grid->addColumn('number', 			'Number', 										150,		array('align' => 'center'));			
			$grid->addColumn('time', 			Lang::get('COMMON.DATE'), 						140,		array('align' => 'center'));


			$data['grid'] = $grid->getTemplateVars();

			return view('admin.grid2',$data);
    }
	
	public function doGetData(Request $request){
		
		if( $request->has('createdfrom') ){
			$condition = ' created >= "'.$request->get('createdfrom').' 00:00:00" AND created <= "'.$request->get('createdto').' 23:59:59"';
		}
	
		$total = Cashcard::whereAflid($request->aflid)->whereStatus(1)->count();
	
		if($cashcards = Cashcard::whereRaw($condition)->whereStatus(1)->whereAflid($request->aflid)->take( $request->pagesize )->skip( $request->recordstartindex )->orderBy('id','desc')->get()){
	
			foreach( $cashcards as $cashcard )
			{
				
				$rows[] = array(
					'amount'  			=> $cashcard->amount, 
					'currency'  		=> $cashcard->crccode, 
					'nickname'  		=> $cashcard->accid == 0 ? 'Not redeem' : Account::whereId($cashcard->accid)->pluck('nickname'), 
					'serial'  			=> $cashcard->id,
					'number'  			=> $cashcard->number,
					'time'  			=> $cashcard->created->toDateTimeString(),
				);	
				
			}
				
		}
		
		echo json_encode(array('total' => $total , 'rows' => $rows));
		exit;

	}
}
