<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller\Admin;
use App\Http\Controllers\User\FortuneWheelController;
use App\libraries\grid\PanelGrid;
use App\libraries\App;
use App\libraries\sms\SMS88;
use App\Models\Accountbank;
use App\Models\Paymentgateway;
use App\Models\Paymentgatewaysetting;
use App\Models\PremierLeagueToken;
use App\Models\Smslog;
use Excel;
use Illuminate\Http\Request;
use Auth;
use Validator;
use Session;
use Lang;
use App\Models\Currency;
use App\Models\Fundingmethod;
use App\Models\Cashledger;
use App\Models\Bank;
use App\Models\Accounttag;
use App\Models\Tag;
use App\Models\Account;
use App\Models\Cashledgerlog;
use App\Models\Bankaccount;
use App\Models\Promocash;
use App\Models\Media;
use App\Models\Rejectreason;
use App\Models\Banktransfer;
use App\Models\Promocampaign;
use App\Models\Bankledger;
use App\Models\User;
use App\Models\Wager;
use App\Models\Adjustment;
use App\Models\Accountdetail;
use App\Models\Article;
use App\Models\Luckydrawledger;
use App\Models\Configs;
use App\Models\Withdrawcard;
use App\Models\Affiliate;
use GeoIP;
use Config;

class CashledgerController extends Controller{

	public static $accounts = array();

	public static $accountDetails = array();

	public static $bankAccounts = array();

	public static $affiliates = array();

	protected $moduleName 	= 'cashledger';
	protected $limit 		= 20;
	protected $start 		= 0;

	public function __construct()
	{

	}

	public function index(Request $request)
	{

		$grid = new PanelGrid;
		$grid->setupGrid('cashledger', 'enquiry-data', 20, true, array('multiple' => true,'checkbox'=>true,'checkbox_url'=> 'cashledger-batchapprove' , 'footer' => true , 'params' => array('createdfrom' => $request->createdfrom , 'createdto' => $request->createdto , 'chtcode' => $request->type , 'aflid' => $request->input('aflid') , 'accid' => $request->input('accid'))));
		//$grid->addColumn('function', 		'', 									18,		array('checkbox' => true));

		$grid->addColumn('date',			Lang::get('COMMON.DATE'), 				110,	array('align' => 'center'));
		$grid->addColumn('id',				Lang::get('COMMON.ID'), 				50,		array('align' => 'right'));
		$grid->addColumn('account', 		Lang::get('COMMON.ACCOUNT'), 			200,		array());
		$grid->addColumn('accname', 		Lang::get('COMMON.NAME'), 				120,	array());
		$grid->addColumn('payment', 		Lang::get('COMMON.PAYMENTMETHOD'), 		250,	array());
		$grid->addColumn('status', 			Lang::get('COMMON.STATUS'), 			100,	array('align' => 'center'));
		$grid->addColumn('currency', 		Lang::get('COMMON.CURRENCY'), 			60,		array('align' => 'center'));
		$grid->addColumn('credit', 			Lang::get('COMMON.CREDIT'), 			80,		array('align' => 'right'));
		$grid->addColumn('debit', 			Lang::get('COMMON.DEBIT'), 				80,		array('align' => 'right'));
		$grid->addColumn('balance', 		Lang::get('COMMON.BALANCE'), 			80,		array('align' => 'right'));
		$grid->addColumn('fee', 			Lang::get('COMMON.FEE'), 				80,		array('align' => 'right'));

		$grid->addSearchField('search', array('code' => Lang::get('COMMON.MEMBERACCOUNT')));

		if( Session::get('admin_crccode') == 'MYR' ) {
            $grid->addFilter('crccode', Currency::getAllCurrencyAsOptions(), array('display' => Lang::get('COMMON.CURRENCY')));
        }

		if (Config::get('setting.opcode') == 'GSC') {
		    $options = ['1' => '网银'];

		    $pgs = Paymentgatewaysetting::where('code', '!=', 'BNK')
                ->where('seq', '>', '-1')
                ->get();

		    foreach ($pgs as $r) {
                $options['2|'.$r->id] = $r->real_name;
            }

            $grid->addFilter('fdmid', $options, array('display' => Lang::get('COMMON.PAYMENTMETHOD')));
        } else {
            $grid->addFilter('fdmid', Fundingmethod::getAllFundingMethodAsOptions(false, false), array('display' => Lang::get('COMMON.PAYMENTMETHOD')));
        }

		$grid->addFilter('status', Cashledger::getStatusOptions(), array('display' => Lang::get('COMMON.STATUS')));
		$grid->addFilter('chtcode', Cashledger::getAllTypeAsOptions(array(CBO_CHARTCODE_BALANCETRANSFER)), array('width' => 300, 'display' => Lang::get('COMMON.TYPE'), 'multiple' => true, 'select' => false, 'values' => json_encode(array(CBO_CHARTCODE_DEPOSIT, CBO_CHARTCODE_WITHDRAWAL, CBO_CHARTCODE_BONUS, CBO_CHARTCODE_ADJUSTMENT, CBO_CHARTCODE_INCENTIVE, CBO_CHARTCODE_DAILYREBATE))));
		$grid->addRangeField('created', array('type' => 'date', 'display' => Lang::get('COMMON.DATE'), 'time' => true));
		$grid->addButton('2', Lang::get('COMMON.APPROVE'), '', 'button', array('icon' => 'ok', 'url' => 'approve_batch(\'approve\');'));
		$grid->addButton('2', Lang::get('COMMON.REJECT'), '', 'button', array('icon' => 'ok', 'url' => 'approve_batch(\'reject\');'));
        $grid->addButton('2', 'Export' , '', 'button', array('icon' => 'ok', 'url' => 'export_excel();'));

		$data['grid'] = $grid->getTemplateVars();

		return view('admin.grid2',$data);

	}

	protected function doGetData(Request $request){

		$defaultCondition 	= '1';
		$condition = '1';

		$startid = '';
		$endid = '';
		$startamt = '';
		$endamt = '';
		$accountCode = '';
		$sumDepositAmt = 0;
		$sumWithdrawalAmt = 0;
		$sumFeesAmt = 0;
		$depositChecked = false;
		$withdrawChecked = false;
		$bonusChecked = false;
		$incentiveChecked = false;
		$adjustmentChecked = false;
		$testAccInfoChecked = false;

		$fundingMethodSelected = '';
		$cashLedgerStatusSelected = '';
		$accTagArray = array();
		$bankTag = array();
		$csTag = array();
		$tagSelected = array();

		$aflt  = $request->input('aflt');
		$aqfld = $request->input('aqfld');

		$condition .= Session::get('admin_crccode') == 'MYR' ? '' : ' AND crccode = "'.Session::get('admin_crccode').'"';

		if( $request->has('createdfrom') ){
			$condition .= ' AND created >= "'.$request->get('createdfrom').' 00:00:00" AND created <= "'.$request->get('createdto').' 23:59:59"';
		}

		if( isset($aqfld['status']) && $aqfld['status'] != 0 ){
			$condition .= ' AND status = "'.$aqfld['status'].'" ';
		}

		if( isset($aqfld['crccode']) && $aqfld['crccode'] != '' ){
			$condition .= ' AND crccode = "'.$aqfld['crccode'].'" ';
		}
		if(!empty($request->chtcode)){
			$condition .= ' AND chtcode = "'.$request->chtcode.'" ';
		}else{
			if( isset($aqfld['chtcode']) && $aqfld['chtcode'] != 0){
				$condition .= ' AND chtcode = "'.$aqfld['chtcode'].'" ';
			}else{
				$condition .= ' AND chtcode IN('.implode(',',array(CBO_CHARTCODE_DEPOSIT, CBO_CHARTCODE_WITHDRAWAL, CBO_CHARTCODE_BONUS, CBO_CHARTCODE_ADJUSTMENT, CBO_CHARTCODE_INCENTIVE, CBO_CHARTCODE_DAILYREBATE,CBO_CHARTCODE_CASHBACK,CBO_CHARTCODE_WITHDRAWALCHARGES,CBO_CHARTCODE_CASHCARD)).')';
			}
		}

		if ( isset($aqfld['fdmid']) && $aqfld['fdmid'] != '' ) {
		    if (str_contains($aqfld['fdmid'], '|')) {
		        // Handle PG filter pattern. Eg: 2|2, 2|3.
		        $fdmidarr = explode('|', $aqfld['fdmid']);

                $condition .= ' AND fdmid = "'.$fdmidarr[0].'" ';
                $condition .= ' AND refobj = "Paymentgateway" AND refid IN (SELECT id FROM paymentgateway WHERE paytype = "'.$fdmidarr[1].'") ';
            } else {
                $condition .= ' AND fdmid = "'.$aqfld['fdmid'].'" ';
            }
        }

		if( isset($aflt['code']) && $aflt['code'] != ''){
			$condition .= ' AND acccode like "%'.$aflt['code'].'%" ';
		}

		if( $request->has('aflid') ){

			$affiliate = new Affiliate;

			 if($members = $affiliate->getAllAccountById($request->aflid, true))
                    foreach($members as $member) {
                            $member_ids[] = $member->id;
                    }
			if(count($member_ids) > 0)
			{
				$condition .= ' AND accid IN ('.implode(',', $member_ids).') ';
			}
		}

		if( $request->has('accid') ){


			$condition .= ' AND accid IN ('.$request->accid.') ';

		}

		$total = Cashledger::whereRaw($condition)->count();

		/**
		* Select the record from the object based on the paging and sorting information and adding their to the grid as row
		*/
		$footer = array();
		$rows = array();
		if($cashLedgers = Cashledger::whereRaw($condition)->take( $request->pagesize )->skip( $request->recordstartindex )->orderBy('id','desc')->get() ){
			//var_dump($cashLedgers);
			$fdMethods = array();

			$bankOptions = Bank::getAllBank();
			$bankOptions[999] = Lang::get('COMMON.OTHERBANK');
			$pgObjCache = array();

			foreach($cashLedgers as $cashLedger){

				$accTagStr = '';
				if(array_key_exists($cashLedger->accid,$accTagArray)){
					$accTagStr = $accTagArray[$cashLedger->accid];

				}else{

					//$accTagSql = Accounttag::whereRaw('status = 1 AND accid ='.$cashLedger->accid)->pluck('tagid');

					if($tagsAcct = Tag::whereRaw('id IN (Select tagid from accounttag where status = 1 AND accid ='.$cashLedger->accid.' ) AND status ='.CBO_STANDARDSTATUS_ACTIVE)->get()){
						foreach ($tagsAcct AS $tagAcct){
							$accTagStr .= '&nbsp;<div class="triangle" style="border-bottom-color:'.$tagAcct->color.'">&nbsp;</div>';
						}
					}

					$accTagArray[$cashLedger->accid] = $accTagStr;
				}

				//account-info get from reference object table
				$balance='';$telNo='';$email='';$accname='';
				if(!isset(self::$accounts[$cashLedger->accid])) {

					if($accObj = Account::find($cashLedger->accid)){
						self::$accounts[$accObj->id] = $accObj;
						self::$accountDetails[$accObj->id] = $accObj->getAccountDetailObject();
					}
				}

				$accountObj = self::$accounts[$cashLedger->accid];
				$accountdetailObj = self::$accountDetails[$cashLedger->accid];

				$balance = App::displayAmount($cashLedger->cashbalance);
				$depositCount = $accountObj->totaldeposit;
				$withdrawalCount = $accountObj->totalwithdrawal;

				if(!empty($accountdetailObj->telhome)) $telNo .= $accountdetailObj->telhome.', ';
				if(!empty($accountdetailObj->telwork)) $telNo .= $accountdetailObj->telwork.', ';
				if(!empty($accountdetailObj->telmobile)) $telNo .= $accountdetailObj->telmobile.', ';
				if($telNo != '') $telNo = substr($telNo,0,-2);

				if(!empty($accountdetailObj->email))
					$email = $accountdetailObj->email;

				$accname = $cashLedger->accname;
				$txCount = '';
				if($cashLedger->chtcode == CBO_CHARTCODE_DEPOSIT){
					$module = $this->moduleName; $action = 'deposit';
					//$txCount = $depositCount;
					$fdm ='';
					if($cashLedger->fdmid == Configs::getParam('SYSTEM_PAYMENT_ID_PG')) {
					    $instanttransflag = ($cashLedger->instanttransflag == Cashledger::INSTANT_TRANS_FLAG_HIDDEN ? Lang::get('COMMON.AUTO') : Lang::get('COMMON.MANUAL'));

					    if (!array_key_exists($cashLedger->refid, $pgObjCache)) {
                            $pgObj = Paymentgateway::find($cashLedger->refid);
                            $pgData = false;

                            if ($pgObj) {
                                $pgsObj = Paymentgatewaysetting::find($pgObj->paytype);

                                if ($pgsObj) {
                                    $pgData = array(
                                        'refid' => $cashLedger->refid,
                                        'real_name' => $pgsObj->real_name,
                                        'paybnkoption' => $pgObj->paybnkoption,
                                    );
                                }
                            }

                            $pgObjCache[$cashLedger->refid] = $pgData;
                        }

                        $pgObjCacheVal = $pgObjCache[$cashLedger->refid];

                        if($pgObjCacheVal !== false) {
                            $txCount = $instanttransflag . ' - ' . $pgObjCacheVal['real_name'] . ' - ' . $pgObjCacheVal['paybnkoption'] . ' - ' . $pgObjCacheVal['refid'];
                        } else {
                            $txCount = $instanttransflag;
                        }
					} else if($cashLedger->fdmid == Configs::getParam('SYSTEM_PAYMENT_ID_BANKTRANSFER')) {
						$accno = '';
						if(isset($bankOptions[$cashLedger->bnkid]))
						$txCount = $bankOptions[$cashLedger->bnkid];

						if(!isset(self::$bankAccounts[$cashLedger->bacid])) {
							if($bacObj = Bankaccount::find($cashLedger->bacid))
							self::$bankAccounts[$cashLedger->bacid] = $bacObj;
						}
						if(isset(self::$bankAccounts[$cashLedger->bacid])) {
							$accno = substr(self::$bankAccounts[$cashLedger->bacid]->bankaccno, -4, 4);
						}
						$txCount .= ' - '.$accno;
					}
				}elseif($cashLedger->chtcode == CBO_CHARTCODE_WITHDRAWAL){
					$module = $this->moduleName; $action = 'withdraw';
					if(isset($bankOptions[$cashLedger->bnkid]))
						$txCount = $bankOptions[$cashLedger->bnkid].' - '.$withdrawalCount;
					else
						$txCount = 'Withdraw Card';
					$fdm ='';
				}elseif($cashLedger->chtcode == CBO_CHARTCODE_WITHDRAWALCHARGES){
					$module = $this->moduleName; $action = 'withdraw';
					$txCount = 'Withdrawal Charges';
					$fdm ='';
				}elseif($cashLedger->chtcode == CBO_CHARTCODE_BONUS){
					$module = $this->moduleName; $action = 'deposit'; $txCount = ''; $fdm = Lang::get('COMMON.BONUS');
					if($refobj = Promocash::find($cashLedger->refid)) {
						if( $pcpObj = Promocampaign::find($refobj->pcpid) )
						$fdm .= ' ['.$pcpObj->code.']';
					}
				}elseif($cashLedger->chtcode == CBO_CHARTCODE_INCENTIVE){
					$module = $this->moduleName; $action = 'deposit'; $txCount = ''; $fdm = Lang::get('COMMON.INCENTIVE');
				}elseif($cashLedger->chtcode == CBO_CHARTCODE_DAILYREBATE){
					$module = $this->moduleName; $action = 'deposit'; $txCount = ''; $fdm = Lang::get('COMMON.DAILYREBATE');
				}elseif($cashLedger->chtcode == CBO_CHARTCODE_CASHBACK){
					$module = $this->moduleName; $action = 'deposit'; $txCount = ''; $fdm = Lang::get('COMMON.CASHBACK');
				}elseif($cashLedger->chtcode == CBO_CHARTCODE_ADJUSTMENT){
					$module = $this->moduleName; $action = 'adjustment'; $txCount = ''; $fdm = Lang::get('COMMON.ADJUSTMENT');
				}elseif($cashLedger->chtcode == CBO_CHARTCODE_CASHCARD){
					$module = $this->moduleName; $action = 'cashcard'; $txCount = ''; $fdm = Lang::get('COMMON.CASHCARD');
				}else{
					$module = 'login'; $action = 'drawform'; $txCount = ''; $fdm ='';
				}

				$date = $cashLedger->created;
				$transactionId = $cashLedger->getTransactionId();
				$id = '<a href="#" onclick="parent.addTab(\'Transaction #'.$transactionId.'\', \'show-'.$action.'?id='.$cashLedger->id.'\')">'.$transactionId.'</a>';
				$acccode = '<a href="#" onclick="parent.addTab(\'Member: '.$accountObj->nickname.'\', \'customer-tab?sid='.$accountObj->id.'\')">'.$accountObj->nickname.$accTagStr.'</a>';
				$currency = $cashLedger->crccode;
				$status = '<span style="color: '.$cashLedger->getStatusTextColorCode().';">'.$cashLedger->getStatusText().'</span>';

				if(CBO_LEDGERSTATUS_MANCANCELLED == $cashLedger->status || CBO_LEDGERSTATUS_CANCELLED == $cashLedger->status || CBO_LEDGERSTATUS_MEMCANCELLED == $cashLedger->status) $amount = '<del>'.App::displayAmount($cashLedger->amount).'</del>';
				else $amount = $cashLedger->amount;

				$credit = '';
				$debit = '';
				if($cashLedger->chtcode == CBO_CHARTCODE_DEPOSIT || $cashLedger->chtcode == CBO_CHARTCODE_BONUS || $cashLedger->chtcode == CBO_CHARTCODE_INCENTIVE || $cashLedger->chtcode == CBO_CHARTCODE_DAILYREBATE){
					$credit = $amount;
					if(is_numeric($amount)) {
						$credit = App::displayAmount($credit);
						if($cashLedger->status == CBO_LEDGERSTATUS_CONFIRMED || $cashLedger->status == CBO_LEDGERSTATUS_MANCONFIRMED)
						$sumDepositAmt += $amount;
					}
				}elseif($cashLedger->chtcode == CBO_CHARTCODE_WITHDRAWAL || $cashLedger->chtcode == CBO_CHARTCODE_WITHDRAWALCHARGES){
					$debit = $amount;
					if(is_numeric($amount)) {
						$debit = App::displayAmount($debit);
						if($cashLedger->status == CBO_LEDGERSTATUS_CONFIRMED || $cashLedger->status == CBO_LEDGERSTATUS_MANCONFIRMED)
						$sumWithdrawalAmt += $amount;
					}
				}elseif($cashLedger->chtcode == CBO_CHARTCODE_BONUS || $cashLedger->chtcode == CBO_CHARTCODE_INCENTIVE || $cashLedger->chtcode == CBO_CHARTCODE_DAILYREBATE || $cashLedger->chtcode == CBO_CHARTCODE_ADJUSTMENT || $cashLedger->chtcode == CBO_CHARTCODE_CASHBACK || $cashLedger->chtcode == CBO_CHARTCODE_CASHCARD){
					if($cashLedger->amount > 0){
						$credit = $amount;
						if(is_numeric($amount)) {
							$credit = App::displayAmount($credit);
							$sumDepositAmt += $amount;
						}
					}elseif($cashLedger->amount < 0){
						$debit = $amount;
						if(is_numeric($amount)) {
							$debit = App::displayAmount($debit);
							$sumWithdrawalAmt += $amount;
						}
					}
				}


				if(is_numeric($cashLedger->fee) && (CBO_LEDGERSTATUS_CONFIRMED == $cashLedger->status || CBO_LEDGERSTATUS_MANCONFIRMED == $cashLedger->status))
				$sumFeesAmt += $cashLedger->fee;

				$fees = App::displayAmount($cashLedger->fee);
				if(CBO_LEDGERSTATUS_MANCANCELLED == $cashLedger->status || CBO_LEDGERSTATUS_CANCELLED == $cashLedger->status)
				$fees = '';
				if($cashLedger->fee < 0)
				$fees = '<span style="color:red;">-'.$fees.'</span>';

				$paymentMethod = '';
				if(!isset($fdMethods[$cashLedger->fdmid])){
					if($fdmObj = Fundingmethod::find($cashLedger->fdmid)){
						$paymentMethod = $fdmObj->name;
						$fdMethods[$cashLedger->fdmid] = $paymentMethod;
						$paymentMethod .= ' ['.$txCount.']';
					}else{
						if($cashLedger->refobj == 'Withdrawcard'){
							$paymentMethod = 'Withdrawcard';
						}
						elseif( $cashLedger->refobj == 'Cashcard' ){
							$paymentMethod = 'Cashcard';
						}
						elseif ($cashLedger->refobj == 'Paymentgateway'){
                            $paymentMethod = 'Paymentgateway';
                        }
						else
						{
							$paymentMethod = $fdm;
						}
					}
				}else{
					$paymentMethod = $fdMethods[$cashLedger->fdmid];
					$paymentMethod .= ' ['.$txCount.']';
				}

				$row = array();

				$row['clgid'] = (int) $cashLedger->id;
				$row['date'] = App::formatDateTime(32, $date);
				$row['id'] = $id;
				$row['account'] = $acccode;
				$row['accname'] = $accname;
				$row['currency'] = $currency;
				$row['credit'] = $credit;
				$row['debit'] = $debit;
				$row['balance'] = $balance;
				$row['fee'] = $fees;
				$row['payment'] = htmlspecialchars($paymentMethod);
				$row['email'] = $email;
				$row['tel'] = $telNo;
				$row['status'] = $status;
				$row['status_code'] = $cashLedger->status;
				$row['remarks'] = $cashLedger->remarks;
				$row['reason'] = $cashLedger->rejreason;
				$row['comments'] = $cashLedger->comments;


				$rows[] = $row;
			}
		}

		//if($cashLedger->status == CBO_LEDGERSTATUS_CONFIRMED || $cashLedger->status == CBO_LEDGERSTATUS_MANCONFIRMED)
		//var_dump($condition);
		$total_credit = Cashledger::whereRaw($condition.' AND crccode = "'.Session::get('currency').'" AND amount > 0 AND status IN ('.CBO_LEDGERSTATUS_CONFIRMED.', '.CBO_LEDGERSTATUS_MANCONFIRMED.')')->sum('amount');
		$total_debit  = Cashledger::whereRaw($condition.' AND crccode = "'.Session::get('currency').'" AND amount < 0 AND status IN ('.CBO_LEDGERSTATUS_CONFIRMED.', '.CBO_LEDGERSTATUS_MANCONFIRMED.')')->sum('amount');
		$total_fee    = Cashledger::whereRaw($condition.' AND status IN ('.CBO_LEDGERSTATUS_CONFIRMED.', '.CBO_LEDGERSTATUS_MANCONFIRMED.')')->sum('fee');

		$footer[] = array('status'=> Lang::get('COMMON.SUBTOTAL').'<br>'.Lang::get('COMMON.TOTAL') ,'credit'=> App::displayAmount($sumDepositAmt).'<br>'.App::displayAmount($total_credit),'debit'=> App::displayAmount($sumWithdrawalAmt).'<br>'.App::displayAmount($total_debit),'fee'=> App::displayAmount($sumFeesAmt).'<br>'.App::displayAmount($total_fee));

		//$footer[] = array('status'=> Lang::get('COMMON.TOTAL') ,'credit'=> App::displayAmount($total_credit),'debit'=> App::displayAmount($total_debit),'fee'=> App::displayAmount($total_fee));

		//$rows[] = $footer;

        // Export as Excel file.
        if ($request->input('export') == 'excel') {
            // Check and remove unwanted HTML format from values.
            $epRows = array();

            foreach ($rows as $rkey => $rval) {
                $epRows2 = array();

                foreach ($rval as $rvkey => $rvval) {
                    $epRows2[$rvkey] = preg_replace('/<[^>]*>/', '', $rvval);
                }

                $epRows[$rkey] = $epRows2;
            }

            Excel::create('Transaction_Enquiry_'.date('U'), function($excel) use ($epRows) {
                $excel->sheet('Sheet1', function($sheet) use ($epRows) {
                    $sheet->fromArray($epRows);
                });
            })->export('xls');
        }

		echo json_encode(array('total' => $total, 'rows' => $rows, 'footer' => $footer));
		exit;

	}

	protected function showDeposit(Request $request) {
		$id = 0;
		if( $request->has('id') )
		$id = (int) $request->input('id');

		$modifiedby = '';
		$modified = '';
		$refno = '';

		if ($object = Cashledger::find($id)) {

			//update new to processing
			if($object->status == CBO_LEDGERSTATUS_PENDING) {
				$object->status = CBO_LEDGERSTATUS_MANPENDING;
				$object->save();
			}




			if($logs = Cashledgerlog::whereRaw('clgid = '.$id)->orderBy('created', 'desc')->get()){

				foreach($logs AS $log){
					if($ModifiedObj = User::find($log->createdby) ){
						$modifiedby = $ModifiedObj->username;
						$modified   = $log->created;
						break;
					}
				}
			}

			if(in_array($object->status,array(CBO_LEDGERSTATUS_PENDING,CBO_LEDGERSTATUS_MANPENDING,CBO_LEDGERSTATUS_PROCESSED))){
				$curStatus = 'pending';
			}elseif(in_array($object->status,array(CBO_LEDGERSTATUS_CONFIRMED,CBO_LEDGERSTATUS_MANCONFIRMED))){
				$curStatus = 'confirmed';
			}elseif(in_array($object->status,array(CBO_LEDGERSTATUS_CANCELLED,CBO_LEDGERSTATUS_MANCANCELLED))){
				$curStatus = 'cancelled';
			}else{
				$curStatus = '';
			}

			$disablefee = false;
			if(CBO_CHARTCODE_DEPOSIT == $object->chtcode){
				$title = Lang::get('COMMON.DEPOSIT');
				$fdm = '';
			}elseif(CBO_CHARTCODE_BONUS == $object->chtcode){
				$disablefee = true;
				$title = Lang::get('COMMON.BONUS');
				$fdm = Lang::get('COMMON.BONUS');
			}elseif(CBO_CHARTCODE_INCENTIVE == $object->chtcode){
				$disablefee = true;
				$title = Lang::get('COMMON.INCENTIVE');
				$fdm = Lang::get('COMMON.INCENTIVE');
			}elseif(CBO_CHARTCODE_DAILYREBATE == $object->chtcode){
				$disablefee = true;
				$title = Lang::get('COMMON.DAILYREBATE');
				$fdm = Lang::get('COMMON.DAILYREBATE');
			}else{
				$title = '';
				$fdm = '';
			}

			$paymentMethod = '';
            $image = false;
			if($fdmObj = Fundingmethod::find($object->fdmid)) {
                if($object->fdmid == Configs::getParam('SYSTEM_PAYMENT_ID_PG')) {
                    $instanttransflag = ($object->instanttransflag == Cashledger::INSTANT_TRANS_FLAG_HIDDEN ? Lang::get('COMMON.AUTO') : Lang::get('COMMON.MANUAL'));
                    $paymentMethod = $fdmObj->name.' ('.$instanttransflag.')';

                    $pgObj = Paymentgateway::find($object->refid);

                    if ($pgObj) {
                        $image = $pgObj->getUploadedTransferAttachment();
                    }
                } else {
                    $paymentMethod = $fdmObj->name;
                }
            } else {
                $paymentMethod = $fdm;
            }

			$info = '';
			if($bacObj = Bankaccount::find($object->bacid)) {
				if($bnkObj = Bank::find($bacObj->bnkid))
				$paymentMethod .= ' [<span style="color:red">'.$bnkObj->shortname.' '.substr($bacObj->bankaccno, -4, 4).'</span>]';
				$refobject = 'App\Models\\'.$object->refobj;
				if($refobj = $refobject::find($object->refid)) {
					$options = $refobj->getTransferTypeOptions();
					if(isset($options[$refobj->transfertype]))
					$info .= $options[$refobj->transfertype];

					if($refobj->transferdatetime != '')
					$info .= '&nbsp;'.$refobj->bankaccname.' '.$refobj->bankaccno.'<br>'.$refobj->transferdatetime;
					if(strlen($refobj->bankprovince) > 0 && strlen($refobj->bankcity) && strlen($refobj->bankbranch))
					$info .= '<br>'.$refobj->bankprovince.', '.$refobj->bankcity.', '.$refobj->bankbranch;

					$refno = $refobj->refno;
					$image = Media::getImage($refobj);
				}
			} else {
				if($object->fdmid == Configs::getParam('SYSTEM_PAYMENT_ID_PG')) {
					$pgtObject = 'App\Models\\'.$object->refobj;
					if($pgtObj = $pgtObject::find($object->refid)) {
						$info = $pgtObj->getTypeText($pgtObj->paytype).' - '.$pgtObj->orderid.' | '.$pgtObj->paybnkoption.' | '.$pgtObj->getStatusText();
					}
				}
			}

			$promo = false;
			$promomethod = false;
			$bonus = false;
			$isbonus = false;
			$isdeposit = false;
			$maxpromo = false;
			$mindeposit = false;
			$promoObj = '';
			$data['depositid'] = '';
			$data['depositamt'] = '';

			$pmcObj = Promocash::whereRaw('accid='.$object->accid.' AND chtcode='.$object->chtcode.' AND clgid='.$object->id)->first();
			if( $object->chtcode == CBO_CHARTCODE_DEPOSIT && $pmcObj ) {
				$isdeposit = true;
				if($promoObj = Promocampaign::find($pmcObj->pcpid)) {
					$promo 		 = $promoObj->name;
					$promomethod = $promoObj->getPromoDetail();
					$maxpromo 	 = $promoObj->maxamount;
					$mindeposit  = $promoObj->mindeposit;
					$bonus 		 = $pmcObj->crccode.' '.App::displayNumberFormat($pmcObj->amount);
				}
			} else if($object->chtcode == CBO_CHARTCODE_BONUS && $pmcObj = Promocash::find($object->refid)) {
				$isbonus = true;
				if($clgObj = Cashledger::find($pmcObj->clgid))
				{
					$data['depositamt'] = $clgObj->crccode.' '.App::displayNumberFormat($clgObj->amount);
					$data['depositid']  = $clgObj->getTransactionId();
				}
				if(  $promoObj = Promocampaign::find($pmcObj->pcpid) ) {
					$promo 		 	 	= $promoObj->name;
					$promomethod 	 	= $promoObj->getPromoDetail();
					$maxpromo 	 	 	= $promoObj->maxamount;
					$mindeposit  	 	= $promoObj->mindeposit;

				}
				$data['newtrans'] = Cashledger::whereRaw('created >="'.$object->created.'" AND chtcode = '.CBO_CHARTCODE_BALANCETRANSFER)->count();
			}
			$amount = $object->crccode.' '.App::displayNumberFormat($object->amount);


			$data['clgObj'] = $object;

			if($accObj = Account::find($object->accid)) {
				$data['accObj'] = $accObj;
				$data['acdObj'] = $accObj->getAccountDetailObject();
			}

			$reasons = Rejectreason::getAllReasonAsOptions();
			App::arrayUnshift($reasons, Lang::get('COMMON.SELECT'));

			$bankAccounts = Bankaccount::getAllAsOptions($object->bnkid, true, false);
			App::arrayUnshift($bankAccounts, Lang::get('COMMON.SELECT'));


			$data['bankAccounts'] = $bankAccounts;
			$data['promoObj'] = $promoObj;
			$data['title'] = $title;
			$data['amount'] = $amount;
			$data['paymentMethod'] = $paymentMethod;
			$data['info'] = $info;
			$data['curStatus']  = $curStatus;
			$data['disablefee'] = $disablefee;
			$data['fees'] = App::displayAmount($object->fee);
			$data['comments'] = $object->comments;
			$data['isdeposit'] = $isdeposit;
			$data['isbonus'] = $isbonus;
			$data['promo'] = $promo;
			$data['bonus'] = $bonus;
			$data['auditedby'] = $object->auditedby;
			$data['audited'] = $object->audited;
			$data['ip'] = $object->createdip;
			$data['ipurl'] = $object->createdip;
			$data['location'] = GeoIP::getLocation($object->createdip);
			$data['reasons'] = $reasons;
			$data['modifiedby'] = $object->modifiedby == 0 ? '' : User::whereId($object->modifiedby)->pluck('username');
			$data['modified'] = $object->modified;
			$data['image'] = $image;
			$data['refno'] = $refno;

			$data['sid'] 		 = (int) $object->id;
			$data['mdl'] 		 = $this->moduleName;
			$data['action'] 	 = 'depositedit';
			$data['sactionref']  = 'reject';
			$data['sactionref']  = 'approve';
			$data['sactionref']  = 'save';


		}

		return view('admin.deposit',$data);

	}

	protected function showWithdraw(Request $request){

		$firstDeposit = false;
		$conditionrow = array();
		$promoCashAmount = 0;
		$promoCampTurnover = 0;
		$LatestDepositAmt = 0;
		$sumTotalBet = 0;
		$sumWinLoss = 0;

		if($request->has('id')) {

			if (preg_match('/^([0-9]+)$/', $request->input('id'), $regs)) {
				$id = $regs[1];
				if ($object = Cashledger::find($id)) {

					//update new to processing
					if($object->status == CBO_LEDGERSTATUS_PENDING) {
						$object->status = CBO_LEDGERSTATUS_MANPENDING;
						$object->save();
					}

					if(!$refobj = Banktransfer::find($object->refid))
					$refobj = false;

					//1st time deposit check
					if($accObj = Account::find($object->accid)){
						if($accObj->totaldeposit == 1){
							$firstDeposit = true;
						}
					}


					//latest deposit amount
					$lastwithdraw = false;
					$firstdepdate = false;
					if( $cashLedgerObject = Cashledger::whereRaw('accid='.$object->accid.' AND chtcode='.CBO_CHARTCODE_WITHDRAWAL.' AND (status='.CBO_LEDGERSTATUS_CONFIRMED.' OR status='.CBO_LEDGERSTATUS_MANCONFIRMED.') AND created <= "'.$object->created.'" AND id!='.$object->id.' ORDER BY created DESC')->first()){
						$lastwithdraw = $cashLedgerObject->created;
					} else if($cashLedgerObject = Cashledger::whereRaw('accid='.$object->accid.' AND chtcode='.CBO_CHARTCODE_DEPOSIT.' AND (status='.CBO_LEDGERSTATUS_CONFIRMED.' OR status='.CBO_LEDGERSTATUS_MANCONFIRMED.') ORDER BY created ASC')->first()){
						$firstdepdate = $cashLedgerObject->created;
					}



					$promoCashCondition = 'accid = '.$object->accid.' AND chtcode ='.CBO_CHARTCODE_DEPOSIT.' AND status ="'.CBO_PROMOCASHSTATUS_CONFIRMED.'"';
					if($promoCashObj = Promocash::whereRaw($promoCashCondition)->first()){
						$promoCashAmount = $promoCashObj->amount;
						//get promo campaign info
						if($promoCampaignObj = Promocampaign::whereRaw($promoCashObj->pcpid)->first()){
							$promoCampTurnover = $promoCampaignObj->turnover;
						}
					}

					$rejReasonArray = array(Lang::get('COMMON.DEPOSIT')=>Lang::get('COMMON.DEPOSIT'));

					/**
					* cash ledger log grid
					*/
					if(!empty($this->currentSortField)) $sortFields = $this->currentSortField;
					else $sortFields = 'created';
					if(!empty($this->currentSortOrder)) $sortOrder = $this->currentSortOrder;
					else $sortOrder = 'ASC';
					if($sortOrder == 'ASC') $rsortOrder = 'DESC'; else $rsortOrder = 'ASC';
					if(!empty($this->currentLimit)) $limit = $this->currentLimit;
					else $limit = $this->limit;
					if(!empty($this->currentStart)) $start = $this->currentStart;
					else $start = $this->start;


					if(CBO_LEDGERSTATUS_PENDING == $object->status){
						$curStatus = 'pending';
					}elseif(CBO_LEDGERSTATUS_MANPENDING == $object->status){
						$curStatus = 'process';
					}elseif(CBO_LEDGERSTATUS_PROCESSED == $object->status){
						$curStatus = 'processed';
						$bankAccounts = Bankaccount::getAllAsOptions($object->bnkid, false, true);
						App::arrayUnshift($bankAccounts, Lang::get('COMMON.SELECT'));
						$data['bankAccounts'] = $bankAccounts;
					}elseif(in_array($object->status,array(CBO_LEDGERSTATUS_CONFIRMED,CBO_LEDGERSTATUS_MANCONFIRMED))){
						$curStatus = 'confirmed';
					}elseif(in_array($object->status,array(CBO_LEDGERSTATUS_CANCELLED,CBO_LEDGERSTATUS_MANCANCELLED, CBO_LEDGERSTATUS_MEMCANCELLED))){
						$curStatus = 'cancelled';
					}else{
						$curStatus = '';
					}

					if($request->has('isuccess')) $updateStatus = $request->input('isuccess');
					else $updateStatus = '';

					$reasons = Rejectreason::getAllReasonAsOptions();
					App::arrayUnshift($reasons, Lang::get('COMMON.SELECT'));


				} else
				$object = null;
			}
		}

		if($object){

			$data['sid'] = $object->id;
			$data['mdl'] = $this->moduleName;
			$data['action'] = 'withdraw';
			$data['action'] = 'withdrawedit';
			$data['hiddenParamsReject'] = 'reject';
			$data['hiddenParamsApprove'] = 'approve';
			$data['hiddenParamsProcess'] = 'process';
			$data['hiddenParamsProcessed'] = 'processed';
			$data['hiddenParamsSave'] = 'save';


			$amount = $object->crccode.' '.App::displayAmount($object->amount);

			$data['clgObj'] = $object;
			if($accObj = Account::find($object->accid)) {
				$data['accObj'] = $accObj;
				$data['acdObj'] = $accObj->getAccountDetailObject();

			}

			$data['txId'] = $object->getTransactionId();
			$data['acccode'] = $object->acccode;
			$data['created'] = $object->created;
			$data['crccode'] = $object->crccode;
			$data['amount'] = $amount;
			$data['fees'] = App::displayAmount($object->fee);
			$data['rejReasonArray'] = $rejReasonArray;
			$data['reasons'] = $reasons;
			$data['ip'] = $object->createdip;
			$data['ipurl'] = '';
			$data['city'] = App::getCityNameByGeoIp($object->createdip);
			$data['location'] = GeoIP::getLocation($object->createdip);
			$data['totaldepositamt'] = App::displayAmount($accObj->totaldepositamt);
			$data['promoCashAmount'] = $promoCashAmount;
			$data['promoCampTurnover'] = $promoCampTurnover;
			$data['cashbalance'] = App::displayAmount($accObj->cashbalance);
			$data['sumTotalBet'] = App::displayAmount($sumTotalBet);
			$data['conditionrow'] = $conditionrow;
			$data['totalwithdrawalamt'] = App::displayAmount($accObj->totalwithdrawalamt);
			$data['curStatus'] = $curStatus;
			$data['updateStatus'] = $updateStatus;
			$data['bankAccounts'] = Bankaccount::getAllAsOptions(FALSE, false, true,false,$accObj->crccode);
			$data['modifiedby'] = $object->modifiedby == 0 ? '' : User::whereId($object->modifiedby)->pluck('username');
			$data['modified'] = $object->modified;
			$data['bankaccountattachment'] = false;

			$accbnkObj = Accountbank::where('accid', '=', $object->accid)->where('bankid', '=', $object->bnkid)->first();

			if ($accbnkObj) {
                $data['bankaccountattachment'] = $accbnkObj->getImage();
            }

		}
		if($refobj && $object->refobj != 'Withdrawcard'){
			if($bankObj = Bank::find($refobj->bnkid))
			$data['bankname'] = $bankObj->shortname;
			$data['payeename'] = $refobj->bankaccname;
			$data['bankaccno'] = $refobj->bankaccno;
			$data['bankcity'] = $refobj->bankcity;
			$data['bankprovience'] = $refobj->bankprovince;
			$data['bankdistrict'] = $refobj->bankdistrict;
			$data['bankbranch'] = $refobj->bankbranch;
			if(in_array($object->status,array(CBO_LEDGERSTATUS_CONFIRMED,CBO_LEDGERSTATUS_MANCONFIRMED))){
				if($bacObj = Bankaccount::find($object->bacid))
				$data['bankaccount'] = $bankObj->shortname.' ['.$bacObj->bankaccname.' '.substr($bacObj->bankaccno, -4, 4).']';
			}
		}

		if( $object->refobj == 'Withdrawcard' )
		{
			$data['cardno']  = Withdrawcard::whereId($object->refid)->pluck('number');
			$aflid 			 = Withdrawcard::whereId($object->refid)->pluck('aflid');
			$data['branch']  = Affiliate::whereId($aflid)->pluck('name');
		}

		return view('admin.withdraw',$data);

	}

	protected function showAdjustment(Request $request){

		if($request->has('id')) {
			if (preg_match('/^([0-9]+)$/', $request->id, $regs)) {
				$id = $regs[1];
				if ($object = Cashledger::find($id)) {
					$modifiedby = '';
					$modified = '';

					if($logs = Cashledgerlog::whereRaw('clgid = '.$id)->orderBy('created', 'desc')->get()){

						foreach($logs AS $log){
							if($ModifiedObj = User::find($log->createdby) ){
								$modifiedby = $ModifiedObj->username;
								$modified   = $log->created;
								break;
							}
						}
					}

					$logObj = null;

					if(in_array($object->status,array(CBO_LEDGERSTATUS_PENDING,CBO_LEDGERSTATUS_MANPENDING))){
						$curStatus = 'pending';
					}elseif(in_array($object->status,array(CBO_LEDGERSTATUS_CONFIRMED,CBO_LEDGERSTATUS_MANCONFIRMED))){
						$curStatus = 'confirmed';
					}elseif(in_array($object->status,array(CBO_LEDGERSTATUS_CANCELLED,CBO_LEDGERSTATUS_MANCANCELLED))){
						$curStatus = 'cancelled';
					}else{
						$curStatus = '';
					}

					if(CBO_CHARTCODE_ADJUSTMENT == $object->chtcode){
						$title = Lang::get('COMMON.ADJUSTMENT');
					}else{
						$title = '';
					}

					if($request->has('isuccess')) $updateStatus = $request->isuccess;
					else $updateStatus = '';

				} else
				$object = null;
			}
		}

		if($object) {
			$accObj = Account::find($object->accid);
			$data['clgObj'] = $object;
			if($accObj = Account::find($object->accid)) {
				$data['accObj'] = $accObj;
				$data['acdObj'] = $accObj->getAccountDetailObject();

			}

			$reasons = Rejectreason::getAllReasonAsOptions();
			App::arrayUnshift($reasons, Lang::get('COMMON.SELECT'));


			$data['txId'] = $object->getTransactionId();
			$data['acccode'] = $object->acccode;
			$data['created'] = $object->created;
			$data['crccode'] = $object->crccode;
			$data['amount'] = $object->amount;
			$data['fees'] = App::displayAmount($object->fee);
			$data['rejReasonArray'] = Rejectreason::getAllReasonAsOptions();
			$data['ip'] = $object->createdip;
			$data['ipurl'] = '';
			$data['city'] = App::getCityNameByGeoIp($object->createdip);
			$data['location'] = GeoIP::getLocation($object->createdip);
			$data['cashbalance'] = App::displayAmount($accObj->cashbalance);
			$data['remark'] = Adjustment::whereId($object->refid)->pluck('remark');

			if( $object->amount < 0 )
				$data['type'] = Lang::get('COMMON.DEBIT');
			else
				$data['type'] = Lang::get('COMMON.CREDIT');

			$data['reasons'] = $reasons;
			$data['sid'] = $request->id;
			$data['totalwithdrawalamt'] = App::displayAmount($accObj->totalwithdrawalamt);
			$data['curStatus'] = $curStatus;
			$data['updateStatus'] = $updateStatus;

		}

		return view('admin.adjustment_trans',$data);
	}


	protected function doDepositEdit(Request $request){

		if( $request->has('actionref') && $request->has('id') ){

			if (preg_match('/^([0-9]+)$/', $request->input('id'), $regs)) {
				$id = $regs[1];
				if ($object = Cashledger::find($id)) {

				    if (!in_array($object->status, array(CBO_LEDGERSTATUS_PENDING, CBO_LEDGERSTATUS_MANPENDING, CBO_LEDGERSTATUS_PROCESSED))) {
                        return response(json_encode( array( 'code' => Lang::get('public.StatusAlreadyModifiedPleaseReloadPage') ) ));
                    }

                    $appStatus = '';
                    $appType = '';

					$object->id 		 = $id;
					//$object->fee		 = $request->input('editfee');
					//$object->feelocal 	 = Currency::getLocalAmount($request->input('editfee'), $object->crccode);
					$object->remarks 	 = $request->input('editremark');
					$object->rejreason   = $request->input('reason');
					$object->comments 	 = $request->input('editcomment');
					$object->modifiedby  = Session::get('admin_userid');

					if( $request->has('bacid') && $request->input('bacid') > 0 ) {
						$object->bacid = $request->input('bacid');
						if( $btfObj = Banktransfer::find($object->refid) )
						{
							$btfObj->receiveacc = $object->bacid;
							$btfObj->save();
						}

					}
					if($request->input('actionref') == "approve"){
						$object->status = CBO_LEDGERSTATUS_MANCONFIRMED;
						$appStatus = 'approved';
					}elseif($request->input('actionref') == "reject"){
						$object->status = CBO_LEDGERSTATUS_MANCANCELLED;
                        $appStatus = 'rejected';
					}elseif($request->input('actionref') == "processed"){
						$object->status = CBO_LEDGERSTATUS_PROCESSED;
					}

					$successful = false;

					if($object->save()){

						$this->insertCashLedgerLog($object->id);
						$cashLedger = new Cashledger;

						$cashLedger->updateAccountBalance($object->accid,CBO_CHARTCODE_DEPOSIT);
						if($request->input('actionref') == "approve"){
							if($object->chtcode == CBO_CHARTCODE_DEPOSIT) {

								$cashLedger->updateDepositWithdrawalAmt($object->accid,CBO_CHARTCODE_DEPOSIT,$object->amount);
								$cashLedger->addEditPromoCashLedger($object->fdmid, $id, $object->status);

								if($btfObj = Banktransfer::find($object->refid)) {
									$blgObj = new Bankledger;
									$blgObj->bnkid 	 	 = $btfObj->bnkid;
									$blgObj->bacid 	 	 = $btfObj->receiveacc;
									$blgObj->clgid 	 	 = $object->id;
									$blgObj->type    	 = Bankledger::TYPE_CREDIT;
									$blgObj->crccode 	 = $object->crccode;
									$blgObj->crcrate 	 = Currency::getCurrencyRate($blgObj->crccode);
									$blgObj->date 	 	 = App::formatDateTime(3, $object->created);
									$blgObj->amount  	 = $object->amount;
									$blgObj->amountlocal = Currency::getLocalAmount($blgObj->amount, $blgObj->crccode);
									$blgObj->ismanual 	 = 0;
									$blgObj->status 	 = 1;
									$blgObj->save();

									if( Config::get('setting.front_path') == 'front')
									{

										if(  (( $object->amount >= 200 && $object->crccode == 'MYR' ) )){

											if($object->accid != 23571)
											{

												$luckydraw = new Luckydrawledger;

												$current_token = Luckydrawledger::where( 'accid' , '=' , $object->accid )->
																				  whereRaw(' object IN ( "Cashledger" , "Free" ) ')->
																				  where( 'draw_no' , '=' , Configs::getParam('SYSTEM_4D_DRAWNO') )->sum('amount');

												if( $current_token < 20 )
												{
													$token = floor($object->amount / 200);
													//$token = $token * 2;
													$total_token = $token + $current_token;
													$token = $total_token > 20 ? 20 - $current_token: $token;



													$luckydraw = new Luckydrawledger;
													$luckydraw->draw_no = Configs::getParam('SYSTEM_4D_DRAWNO');
													$luckydraw->accid   = $object->accid;
													$luckydraw->acccode = $object->acccode;
													$luckydraw->amount  = $token;
													$luckydraw->refid   = $object->id;
													$luckydraw->object  = 'Cashledger';
													$luckydraw->save();
												}
											}
										}/* else{

											$current_token = Luckydrawledger::where( 'accid' , '=' , $object->accid )->
																			  where( 'object' , '=' , 'Cashledger' )->
																			  where( 'draw_no' , '=' , Configs::getParam('SYSTEM_4D_DRAWNO') )->
																			  whereBetween('created_at', array(date('Y-m-d').' 00:00:00', date('Y-m-d').' 23:59:59'))->sum('amount');
											if($current_token == 0)
											{
												$luckydraw = new Luckydrawledger;
												$luckydraw->draw_no = Configs::getParam('SYSTEM_4D_DRAWNO');
												$luckydraw->accid   = $object->accid;
												$luckydraw->acccode = $object->acccode;
												$luckydraw->amount  = 1;
												$luckydraw->refid   = $object->id;
												$luckydraw->object  = 'Cashledger';
												$luckydraw->save();
											}

										} */

                                        try {
                                            if ( $object->amount >= 150 && $object->crccode == 'MYR' ) {
                                                if (date('Y-m-d') >= '2018-02-16' && date('Y-m-d') <= '2018-03-02' || in_array(strtolower($object->acccode), ['if_mingda', 'if_test118'])) {
//                                                if ( date('Y-m-d') >= '2018-02-16' && date('Y-m-d') <= '2018-03-02' ) {
                                                    // CNY omt lucky draw.
                                                    $today = date('Y-m-d');
                                                    $username = 's_'.strtoupper(substr($object->acccode, 3));
                                                    $time = date('U');
                                                    $key = md5('11'.$username.$time.'jIS9olsd209dhj');

                                                    $ch = curl_init();
                                                    curl_setopt($ch, CURLOPT_URL, 	'http://follow178.com/ae88/api/checktoken');
                                                    curl_setopt($ch, CURLOPT_HEADER, false);
                                                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                                    curl_setopt($ch, CURLOPT_POST, true);
                                                    curl_setopt($ch, CURLOPT_POSTFIELDS, 'user_name='.$username.'&gid=11&from='.$today.' 00:00:00&to='.$today.' 23:59:59&time='.$time.'&key='.$key );
                                                    $response = curl_exec($ch);
                                                    curl_close($ch);

                                                    if ($response >= 0 && $response < 9) {
                                                        $token = 1;

                                                        if ($object->amount >= 900) {
                                                            $token = 9;
                                                        } elseif($object->amount >= 500) {
                                                            $token = 4;
                                                        }

                                                        $total_token = $token + (int)$response;
                                                        $token = $total_token > 9 ? 9 - (int)$response : $token;

                                                        $ch = curl_init();
                                                        curl_setopt($ch, CURLOPT_URL, 	'http://follow178.com/ae88/api/addtoken');
                                                        curl_setopt($ch, CURLOPT_HEADER, false);
                                                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                                        curl_setopt($ch, CURLOPT_POST, true);
                                                        curl_setopt($ch, CURLOPT_POSTFIELDS, 'user_name='.$username.'&gid=11&token='.$token.'&time='.$time.'&key='.$key );
                                                        $response = curl_exec($ch);
                                                        curl_close($ch);

                                                        \Log::debug('OMT: '.$response);
                                                    }
                                                }
                                            }
                                        } catch (\Exception $e) {
                                            \Log::error($e->getTraceAsString());
                                        }
									}

									if( Config::get('setting.opcode') == 'RYW')
									{

										$GID['MYR'] = 3;
										$GID['VND'] = 10;
										$time = '';

										//$key = MD5( $GID[$object->crccode].'s_'.substr($object->acccode, 3) . $time . 'jIS9olsd209dhj' );

										if(  (( $object->amount >= 100 && $object->crccode == 'MYR' ) )){

												/* $ch = curl_init();
												curl_setopt($ch, CURLOPT_URL, 	'http://follow178.com/ae88/api/checktoken');
												curl_setopt($ch, CURLOPT_HEADER, false);
												curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
												curl_setopt($ch, CURLOPT_POST, true);
												curl_setopt($ch, CURLOPT_POSTFIELDS, 'user_name=s_'.substr($object->acccode, 3).'&gid='.$GID[$object->crccode].'&from='.App::getDateTime(3).' 00:00:00&to='.App::getDateTime(3).' 23:59:59&time='.$time.'&key=' . $key );
												$str = curl_exec($ch);
												curl_close($ch);

												if( $str == 0 ){ */
													$bonus_token = 0;
													$token = (int) ($object->amount / 250);

													if( $object->amount >= 1000 ){
														$bonus_token = (int) ($object->amount / 1000);
													}



													$token += $bonus_token;

												/* 	$ch = curl_init();
													curl_setopt($ch, CURLOPT_URL, 	'http://follow178.com/ae88/api/addtoken');
													curl_setopt($ch, CURLOPT_HEADER, false);
													curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
													curl_setopt($ch, CURLOPT_POST, true);
													curl_setopt($ch, CURLOPT_POSTFIELDS, 'user_name=s_'.substr($object->acccode, 3).'&gid='.$GID[$object->crccode].'&token='.$token.'&time='.$time.'&key=' . $key );
													$str = curl_exec($ch);
													curl_close($ch); */

													//var_dump($token);
													//var_dump($str);

												//}
										}

                                        if (in_array($object->crccode, array('MYR', 'IDR'))) {
                                            $accObj = Account::find($object->accid);

                                            // Add lucky spin token.
                                            try {
                                                 FortuneWheelController::addDepositToken($object->accid, $accObj->nickname, $accObj->crccode, $object->amount, $object->id);
                                            } catch (\Exception $ex) {
                                                // Do nothing.
                                            }

                                            if ($object->crccode == 'MYR' && $object->created >= '2018-06-14 00:00:00'){
                                                // Add premier league token.
                                                try {
                                                    PremierLeagueToken::addDepositToken($object->accid, $accObj->nickname, $accObj->crccode, $object->amount, $object->id);
                                                } catch (\Exception $ex) {
                                                    // Do nothing.
                                                }
                                            }
                                        }

									}


								}


							}


							if( $object->crccode == 'MYR' && $object->chtcode == CBO_CHARTCODE_DEPOSIT)
							{
								if(  Config::get('setting.opcode') == 'IFW' ||  Config::get('setting.opcode') == 'RYW'  )
								{
									$accObj = Account::find($object->accid);
									$acdObj = Accountdetail::whereRaw('accid = '.$object->accid)->first();

									$cleanup_chr = array ('+', '-', ' ', '(', ')', '\r', '\n', '\r\n');
									$tels = str_replace($cleanup_chr, '', $acdObj->telmobile);
									if($tels[0] == '0') $tels = '6'.$tels;

									if( $atcObj = Article::whereRaw('code = "DSMS"')->first() ){
										$content = str_replace('&nbsp;', ' ', html_entity_decode($atcObj->content));
										$content = str_replace('<br>', '', $content);
										$content = str_replace('%username%', $accObj->nickname, $content);
										$content = str_replace('%amount%', $object->amount, $content);
										$msg = strip_tags($content);
									}

									App::SMS( $msg , $tels );
								}
							}

						}elseif($request->input('actionref') == "reject"){

							if($bankledgerObj = Bankledger::whereClgid($object->id)->first() )
							{
								$bankledgerObj->delete();
							}

							if($bonus = Promocash::whereClgid($object->id)->first()){

								$bonus->status = 2;
								$bonus->save();
							}

						}else{
							if($object->chtcode == CBO_CHARTCODE_DEPOSIT)
							$cashLedger->addEditPromoCashLedger($object->fdmid, $id, $object->status);
							echo json_encode( array( 'code' => 'processed' ) );
							exit;
						}

						$successful = true;
					}


					if($successful){
					    $this->sendDepositWithdrawSMS($request->ip(), $object->accid, $object->chtcode, $object->amount, $object->status);

						echo json_encode( array( 'code' => Lang::get('COMMON.SUCESSFUL') ) );
					}else{
						echo json_encode( array( 'code' => Lang::get('COMMON.FAILED') ) );
					}
				}else{
					echo json_encode( array( 'code' => Lang::get('COMMON.FAILED') ) );
				}
			}
		}

	}

	protected function doWithdrawalEdit(Request $request){

		if( $request->has('actionref') && $request->has('id') ){

			if (preg_match('/^([0-9]+)$/', $request->input('id'), $regs)) {
				$id = $regs[1];
				if ($object = Cashledger::find($id)) {

                    if (!in_array($object->status, array(CBO_LEDGERSTATUS_PENDING, CBO_LEDGERSTATUS_MANPENDING, CBO_LEDGERSTATUS_PROCESSED))) {
                        return response(json_encode( array( 'code' => Lang::get('public.StatusAlreadyModifiedPleaseReloadPage') ) ));
                    }

					$object->id = $id;
					$object->fee		 = $request->input('editfee');
					$object->feelocal 	 = Currency::getLocalAmount($request->input('editfee'), $object->crccode);
					$object->remarks 	 = $request->input('editremark');
					$object->rejreason   = $request->input('reason');
					$object->comments 	 = $request->input('editcomment');
					$object->modifiedby  = Session::get('admin_userid');
					if($request->input('actionref') == "approve"){
						$object->bacid = $request->input('bacid');
						$object->status = CBO_LEDGERSTATUS_MANCONFIRMED;
					}elseif($request->input('actionref') == "reject"){
						$object->status = CBO_LEDGERSTATUS_MANCANCELLED;
					}elseif($request->input('actionref') == "process"){
						$object->status = CBO_LEDGERSTATUS_MANPENDING;
					}elseif($request->input('actionref') == "processed"){
						$object->status = CBO_LEDGERSTATUS_PROCESSED;
					}

					$successful = false;

					if($object->save()){
						$this->insertCashLedgerLog($id);

						$cashLedger = new Cashledger;
						$cashLedger->updateAccountBalance($object->accid,CBO_CHARTCODE_WITHDRAWAL);

						if($request->input('actionref') == "approve"){
							$cashLedger->updateDepositWithdrawalAmt($object->accid,CBO_CHARTCODE_WITHDRAWAL,$object->amount);

							if($object->chtcode == CBO_CHARTCODE_WITHDRAWAL) {

								if($btfObj = Banktransfer::find($object->refid)) {
									$blgObj = new Bankledger;
									$blgObj->bnkid 		 = $btfObj->bnkid;
									$blgObj->bacid 		 = $object->bacid;
									$blgObj->clgid 		 = $object->id;
									$blgObj->type  		 = Bankledger::TYPE_DEBIT;
									$blgObj->crccode	 = $object->crccode;
									$blgObj->crcrate	 = Currency::getCurrencyRate($blgObj->crccode);
									$blgObj->date 		 = App::formatDateTime(3, $object->created);
									$blgObj->amount      = $object->amount;
									$blgObj->amountlocal = Currency::getLocalAmount($blgObj->amount, $blgObj->crccode);
									$blgObj->ismanual 	 = 0;
									$blgObj->status 	 = 1;
									$blgObj->save();
								}
							}
							if($object->chtcode == CBO_CHARTCODE_WITHDRAWAL && $object->crccode == 'MYR')
							{
								if(  Config::get('setting.opcode') == 'IFW' ||  Config::get('setting.opcode') == 'RYW'  )
								{
									$accObj = Account::find($object->accid);
									$acdObj = Accountdetail::whereRaw('accid = '.$object->accid)->first();

									$cleanup_chr = array ('+', '-', ' ', '(', ')', '\r', '\n', '\r\n');
									$tels = str_replace($cleanup_chr, '', $acdObj->telmobile);
									if($tels[0] == '0') $tels = '6'.$tels;

									if( $atcObj = Article::whereRaw('code = "WSMS"')->first() ){
										$content = str_replace('&nbsp;', ' ', html_entity_decode($atcObj->content));
										$content = str_replace('<br>', '', $content);
										$content = str_replace('%username%', $accObj->nickname, $content);
										$content = str_replace('%amount%', $object->amount, $content);
										$msg = strip_tags($content);
									}

									App::SMS( $msg , $tels );
								}
							}



						}elseif($request->input('actionref') == "reject"){

							if($bankledgerObj = Bankledger::whereClgid($object->id)->first() )
							{
								$bankledgerObj->delete();
							}

						}else{
							echo json_encode( array( 'code' => 'processed' ) );
								exit;
						}
						$successful = true;
					}

					if($successful){
                        $this->sendDepositWithdrawSMS($request->ip(), $object->accid, $object->chtcode, $object->amount, $object->status);

						echo json_encode( array( 'code' => Lang::get('COMMON.SUCESSFUL') ) );
					}else{
						echo json_encode( array( 'code' => Lang::get('COMMON.FAILED') ) );
					}
				}else{
					echo json_encode( array( 'code' => Lang::get('COMMON.FAILED') ) );
				}
			}
		}

	}

	protected function doAdjustmentEdit(Request $request){

		if( $request->has('actionref') && $request->has('id') ){

			if (preg_match('/^([0-9]+)$/', $request->input('id'), $regs)) {
				$id = $regs[1];
				if ($object = Cashledger::find($id)) {
					$object->id = $id;
					$object->fee		 = $request->input('editfee');
					$object->feelocal 	 = Currency::getLocalAmount($request->input('editfee'), $object->crccode);
					$object->remarks 	 = $request->input('editremark');
					$object->rejreason   = $request->input('reason');
					$object->comments 	 = $request->input('editcomment');
					$object->modifiedby  = Session::get('admin_userid');

					if($request->input('actionref') == "approve"){
						$object->status   = CBO_LEDGERSTATUS_MANCONFIRMED;
						$adjustmentStatus = CBO_ADJUSTMENTSTATUS_CONFIRMED;
					}elseif($request->input('actionref') == "reject"){
						$object->status   = CBO_LEDGERSTATUS_MANCANCELLED;
						$adjustmentStatus = CBO_ADJUSTMENTSTATUS_REJECTED;


						if($bankledgerObj = Bankledger::whereClgid($object->id)->first() )
						{
							$bankledgerObj->delete();
						}


					}

					$successful = false;

					if($object->save()){

						$this->insertCashLedgerLog($id);

						$adjustment = Adjustment::find($object->refid);
						$adjustment->status = $adjustmentStatus;
						$adjustment->save();

						if($request->input('actionref') == "approve"){
							if($object->amount > 0) $chtCode = CBO_CHARTCODE_DEPOSIT;
							else $chtCode = CBO_CHARTCODE_WITHDRAWAL;

							$cashLedger = new CashLedger;
							$cashLedger->updateAccountBalance($object->accid,$chtCode);
						}


						$successful = true;
					}

					if($successful){
						echo json_encode( array( 'code' => Lang::get('COMMON.SUCESSFUL') ) );
					}else{
						echo json_encode( array( 'code' => Lang::get('COMMON.FAILED') ) );
					}
				}else{
					echo json_encode( array( 'code' => Lang::get('COMMON.FAILED') ) );
				}
			}
		}

	}

	private function insertCashLedgerLog($id){

		if ($object = Cashledger::find($id)) {

			$objectLog = new Cashledgerlog;
			$objectLog->clgid 		 = $object->id;
			$objectLog->accid 		 = $object->accid;
			$objectLog->acccode 	 = $object->acccode;
			$objectLog->chtcode 	 = $object->chtcode;
			$objectLog->amount  	 = $object->amount;
			$objectLog->amountlocal  = $object->amountlocal;
			$objectLog->crccode		 = $object->crccode;
			$objectLog->crcrate		 = $object->crcrate;
			$objectLog->fdmid 		 = $object->fdmid;
			$objectLog->refobj		 = $object->refobj;
			$objectLog->refid 		 = $object->refid;
			$objectLog->fee 		 = $object->fee;
			$objectLog->feelocal	 = $object->feelocal;
			$objectLog->remarks 	 = isset($object->remarks) ? $object->remarks : '';
			$objectLog->rejreason 	 = $object->rejreason;
			$objectLog->comments 	 = isset($object->comments) ? $object->comments : '';;
			$objectLog->postflag 	 = $object->postflag;
			$objectLog->postdate 	 = $object->postdate;
			$objectLog->auditflag	 = $object->auditflag;
			$objectLog->auditedby	 = $object->auditedby;
			$objectLog->audited 	 = $object->audited;
			$objectLog->createdip 	 = $object->createdip;
			$objectLog->createdpanel = $object->createdpanel;
			$objectLog->clgstatus 	 = $object->status;
			$objectLog->modifiedby   = Session::get('admin_userid');
			$objectLog->save();
		}
	}

	protected function getAuditTransaction(Request $request){


		$total = 0;
		$rows = array();
		if( $object = Cashledger::find($request->id) )
		{

				$transactionrow 	= Cashledger::getAuditTrans($object->accid,'',$object->created);
				$allTransId 		= '';
				$todayDate 			= App::getDateTime(3);


				$isSetIncentiveFlagOnDateRange = Cashledger::isSetIncentiveFlagOnDateRange($object->accid, $transactionrow[0][0], $transactionrow[count($transactionrow)-1][0]);
				$isSetIncentiveFlag = Cashledger::isSetIncentiveFlag($object->accid);


				$showIncentiveFlag = 0;


				if($isSetIncentiveFlag * 1 >= $transactionrow[count($transactionrow)-1][2]){
					$showIncentiveFlag = 1;
				}

				$count = count($transactionrow);
				for($i = 0 ; $i < $count ; $i++){
					$wagerValidStake = 0;
					$casinoValidStake = 0;

					if(isset($transactionrow[$i+1][0])){
						$wagerValidStake  = Wager::whereRaw('accid='.$object->accid.' AND datetime <="'.$transactionrow[$i+1][0].'" AND datetime >= "'.$transactionrow[$i][0].'" AND status = '.CBO_BETSLIPSTATUS_SETTLED.' ')->sum('validstake');
						$transactionrow[$i][14]    = 0;
						$casinoValidStake = 0;
						$kenoValidStake = 0;
						$newkenoValidStake = 0;
						$transactionrow[$i][15] = 0;


					}else{
						if($nextDate = Cashledger::getNextTrans($object->accid,$transactionrow[$i][0])){

							$wagerValidStake  = Wager::whereRaw('accid='.$object->accid.' AND datetime <="'.$nextDate.'" AND datetime >= "'.$transactionrow[$i][0].'" AND status = '.CBO_BETSLIPSTATUS_SETTLED.' ')->sum('validstake');
							$transactionrow[$i][14] = 0;
							$transactionrow[$i][15] = 0;

						}else{

							$wagerValidStake  = Wager::whereRaw('accid='.$object->accid.' AND datetime <="'.$todayDate.' 23:59:59" AND datetime >= "'.$transactionrow[$i][0].'" AND status = '.CBO_BETSLIPSTATUS_SETTLED.' ')->sum('validstake');
							$transactionrow[$i][14] = 0;
							$transactionrow[$i][15] = 0;

						}

					}

					$transactionrow[$i][7] = $object->crccode == 'VND' || $object->crccode == 'IDR' ? $wagerValidStake * 1000 : $wagerValidStake;
					$transactionrow[$i][8] = ($transactionrow[$i][6] - $transactionrow[$i][7]) * -1;


					$url = '';
					$transactionrow[$i][13]= $transactionrow[$i][7];

					$transactionrow[$i][7] = App::displayNumberFormat($transactionrow[$i][7]);

					if($transactionrow[$i][8] < 0 ){
						$transactionrow[$i][8] = App::formatNegativeAmount($transactionrow[$i][8]);
					}else{
						$transactionrow[$i][8] = APP::displayNumberFormat($transactionrow[$i][8]);
					}

					if($transactionrow[$i][15] < 0 ){
						$transactionrow[$i][15] = App::formatNegativeAmount($transactionrow[$i][15]);
					}else{
						$transactionrow[$i][15] = APP::displayNumberFormat($transactionrow[$i][15]);
					}

					$temp = explode('[',$transactionrow[$i][3]);
					$transactionrow[$i][17] = false;
					if(isset($temp[1])){
						$temp = rtrim($temp[1],"]");
						for($j = 0 ; $j < $i ; $j++){
							if($transactionrow[$j][2] == $temp){
								$transactionrow[$j][12] = '';
								$transactionrow[$j][6] = '';
								$transactionrow[$j][18] = '';
								$transactionrow[$j][8] = '';

							}
						}
					}

					if($transactionrow[$i][6] != ''){
						$transactionrow[$i][18] = $transactionrow[$i][6];
						$transactionrow[$i][6] = APP::displayNumberFormat($transactionrow[$i][6]);
					}

					if($transactionrow[$i][6] == 0){
						$transactionrow[$i][6] = '';
						$transactionrow[$i][18] = '';
					}

					$allTransId .=  $transactionrow[$i][2].'_'.$transactionrow[$i][11].',';




					$transactionrow[$i][9] = App::formatNegativeAmount($transactionrow[$i][9]);

					//transactionrow[0]  = æ—¥æœŸ
				//transactionrow[1]  = ç±»åˆ«
				//transactionrow[2]  = äº¤æ˜“ç ï¼ˆ10848ï¼‰
				//transactionrow[3]  = çŠ¶æ€
				//transactionrow[4]  = å­˜å…¥
				//transactionrow[5]  = æ”¯å‡º
				//transactionrow[6]  = é™åˆ¶
				//transactionrow[7]  = æœ‰æ•ˆæ³¨èµ„
				//transactionrow[8]  = å·®å¼‚
				//transactionrow[9]  = ä½™é¢
				//transactionrow[10] = äº¤æ˜“ç ï¼ˆ000010848ï¼‰
				//transactionrow[11] = auditflag
				//transactionrow[12] = æ‰“ç é‡
					if(isset($transactionrow[$i+1][0])){
						$detailParams['iprdid'] 	 = 0;
						$detailParams['iaccid'] 	 = $object->accid;
						$detailParams['createdfrom'] = $transactionrow[$i][0]->toDateTimeString();
						$detailParams['createdto']   = $transactionrow[$i+1][0]->toDateTimeString();
						$detailParams['itime'] 	 = 1;
					}else{
						if($nextDate){
							$detailParams['iprdid'] 	 = 0;
							$detailParams['iaccid'] 	 = $object->accid;
							$detailParams['createdfrom'] = $transactionrow[$i][0]->toDateTimeString();
							$detailParams['createdto']   = $nextDate;
							$detailParams['itime'] 	 = 1;
						}else{

							$detailParams['iprdid'] 	 = 0;
							$detailParams['iaccid'] 	 = $object->accid;
							$detailParams['createdfrom'] = $transactionrow[$i][0]->toDateTimeString();
							$detailParams['createdto']   = $todayDate.' 23:59:59';
							$detailParams['itime'] 	 = 1;
						}
					}


					$transactionrow[$i][7] = App::formatAddTabUrl(Lang::get('COMMON.BETDETAILS').': '. $object->acccode, $transactionrow[$i][7], action('Admin\ReportController@showdetail',$detailParams));

				}
				$count = 1;
				foreach($transactionrow as $row) {



					$rows[] = array(
						'id' => $row[10],
						'date' => $row[0]->toDateTimeString(),
						'type' => $row[1],
						'transid' => $row[2],
						'status' => $row[3],
						'credit' => $row[4],
						'debit' => $row[5],
						'limit' => $row[6],
						'turnover' => $row[7],
						'diff' => $row[8],
						'accdiff' => '<div id="acu_differ_'.$count.'"></div>',
						'balance' => $row[9],
						'acclimit' => '<div id="acu_limit_'.$count.'"></div>',

					);
					$count++;
				}

		}

		rsort($rows);
		echo json_encode(array('total' => count($rows), 'rows' => $rows));
		exit;
	}

	public function batchApprove(Request $request){

		foreach( $request->rows as $row ){
            preg_match('/show-deposit\?id=([0-9]+)\'/', $row, $m);

            if (count($m) > 1) {
                $id = $m[1];
            } else {
                $id = strip_tags($row);
            }

			if($object = Cashledger::find($id) )
			{
				if(CBO_LEDGERSTATUS_PENDING == $object->status || CBO_LEDGERSTATUS_MANPENDING == $object->status)
				{
					if( $request->type == 'approve')
						$object->status = CBO_LEDGERSTATUS_MANCONFIRMED;
					else
						$object->status = CBO_LEDGERSTATUS_MANCANCELLED;

					if($object->chtcode != CBO_CHARTCODE_WITHDRAWAL && $object->chtcode != CBO_CHARTCODE_DEPOSIT)
					{
						if($object->save()){
							//$cashledger = new Cashledger;
							//$cashledger->updateAccountBalance($object->accid);
							//$this->insertCashLedgerLog($object->id);
						}
					}
				}
			}
		}

		echo json_encode( array( 'code' => Lang::get('COMMON.SUCESSFUL') ) );
		exit;


	}

	public function resendDepositWithdrawSMS(Request $request) {

	    $response = Lang::get('public.CannotSendSMSAtTheMoment');

	    if ($request->has('id')) {
            $clgObj = Cashledger::find($request->get('id'));

            if ($this->sendDepositWithdrawSMS($request->ip(), $clgObj->accid, $clgObj->chtcode, $clgObj->amount, $clgObj->status)) {
                $response = Lang::get('public.SMSSentSuccessfully');
            }
        }

        echo json_encode( array('code' => $response) );
	    exit;
    }

    private function sendDepositWithdrawSMS($ip, $accid, $chtcode, $amount, $statusCode)
    {

//        $accObj = Account::find($accid);
//
//        // Restricted only available for following operators.
//        if (Config::get('setting.opcode') != 'RYW' || $accObj->crccode != 'MYR') {
//            return false;
//        }
//
//        $accDetailObj = Accountdetail::where('accid', '=', $accid)->first();
//        $smsMethod = '';
//        $smsMethodStr = '';
//        $status = '';
//
//        if ($chtcode == CBO_CHARTCODE_DEPOSIT) {
//            $smsMethod = 'deposit';
//            $smsMethodStr = 'D3p0s1t';
//        } elseif ($chtcode == CBO_CHARTCODE_WITHDRAWAL) {
//            $smsMethod = 'withdrawal';
//            $smsMethodStr = 'W1thdr@w@l';
//        }
//
//        if ($statusCode == CBO_LEDGERSTATUS_MANCONFIRMED) {
//            $status = 'approved';
//        } elseif ($statusCode == CBO_LEDGERSTATUS_MANCANCELLED) {
//            $status = 'rejected';
//        }
//
//        if ($smsMethod != '' && $status != '') {
//            if ($accObj->crccode == 'MYR') {
//                $smsObj = new SMS88();
//                $telmobile = $smsObj->formatTel('MYR', $accDetailObj->telmobile);
//
//                $message = $smsObj->getShortCurrency($accObj->crccode) . '0.00 ' .
//                    $smsObj->getOperatorName(Config::get('setting.opcode'), '', ': ') .
//                    'Dear ' . $accObj->nickname . ', we are pleased to inform you that your ' . $smsMethodStr . ' application ' . $accObj->crccode . ' ' . number_format($amount, 4, '.', '') . ' has been ' . $status . '.';
//
//                $smslogObj = Smslog::quickCreate($accObj->id, $accObj->nickname, $ip, 'sms88', $smsMethod, $telmobile, $message, '', '', 0);
//                $response = '';
//
//                try {
//                    $response = $smsObj->sms($message, $telmobile);
//                } catch (\Exception $ex) {
//                    // Nothing.
//                }
//
//                $smslogObj->response = $response;
//                $smslogObj->save();
//
//                return true;
//            }
//        }

        return false;
    }

}
