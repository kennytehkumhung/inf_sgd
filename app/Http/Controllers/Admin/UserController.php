<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller\Admin;
use App\libraries\grid\PanelGrid;
use App\libraries\App;
use Illuminate\Http\Request;
use Auth;
use Validator;
use Session;
use Lang;
use Hash;
use App\Models\Currency;
use App\Models\Bank;
use App\Models\Account;
use App\Models\Config;
use App\Models\User;
use App\Models\Sysvar;
use App\Models\Role;
use App\Models\Usergroup;

class UserController extends Controller{


	protected $moduleName 	= 'user';
	protected $limit 		= 20;
	protected $start 		= 0;
	
	public function __construct()
	{
		
	}
	
	public function index(Request $request){

			$grid = new PanelGrid;
			$grid->setupGrid($this->moduleName, 'user-data', $this->limit);
			$grid->setTitle(Lang::get('COMMON.USER'));
			
			$grid->addColumn('username', 			Lang::get('COMMON.USERNAME'), 		128,		array('align' => 'left'));			
			$grid->addColumn('firstname', 			Lang::get('COMMON.FIRSTNAME'), 		180,		array('align' => 'left'));			
			$grid->addColumn('lastname',			Lang::get('COMMON.LASTNAME'), 		150,		array('align' => 'left'));
			//$grid->addColumn('roles', 				Lang::get('COMMON.ROLE'), 			188,		array('align' => 'left'));
			$grid->addColumn('lastlogin', 			Lang::get('COMMON.LASTLOGINON'), 	188,		array('align' => 'left'));
			$grid->addColumn('status', 				Lang::get('COMMON.STATUS'), 		68,			array('align' => 'left'));

			
			$grid->addFilter('status',array('Active','Suspended'), array('display' => Lang::get('COMMON.TYPE')));
			$grid->addButton('1', 'New', Lang::get('COMMON.ADDNEW').' '.Lang::get('COMMON.USER'), 'button', array('icon' => 'add', 'url' => action('Admin\UserController@drawAddEdit')));


			$data['grid'] = $grid->getTemplateVars();

			return view('admin.grid2',$data);
    }
	
	protected function doGetData(Request $request){
		
		$rows = array();
		//status
		$status[1] = 'Active';
		$status[0] = 'Suspended';
		
		$aflt  = $request->input('aflt');
		$aqfld = $request->input('aqfld');
		
		//$condition = 'status'.'='.'1';
		
		if( isset($aqfld['status']) && $aqfld['status'] != 0 ){
			$condition = 'status'.'='.'0';
		}
		else{
			$condition = 'status'.'='.'1';
		}
		
		$total = User::whereRaw($condition)->count();

		//new structure
		if($users = User::whereRaw($condition)->skip($request->recordstartindex)->take( $request->pagesize )->orderBy('id','desc')->get()){
			foreach( $users as $user ){
				/* $roles = '';
				$currentRoles = $user->getRoleArrayByUserId();
				if(is_array($currentRoles)) {
					$roles = implode(',<br />', $currentRoles);
					
				} */
				
				$body = App::formatAddTabUrl( Lang::get('USER').': '.$user->id, urlencode($user->username), action('Admin\UserController@drawAddEdit',  array('sid' => $user->id)));
		
				$rows[] = array(

					'username'  => urldecode($body),
					'firstname'	=> $user->firstname,
					'lastname'	=> $user->lastname,
					//'roles'		=> $roles,
					'lastlogin' => $user->lastlogin,
					'status'	=> $status[$user->status],

				);	
				
			}
				
		}
		
		echo json_encode(array('total' => $total, 'rows' => $rows));
		exit;

	}
	
	protected function drawAddEdit(Request $request) { 
	
		$toAdd 	  = true;
		$object   = null;
		$readOnly = false;
		$values   = array();
		$currentRoles = array();
        $currentGroups = array();
		$title = Lang::get('NEW').' '.Lang::get('USER');

		if($request->has('sid')) 
		{
	
			if ($object = User::find($request->input('sid'))) 
				{
					$title    = Lang::get('COMMON.EDIT').' '.Lang::get('COMMON.USER');
					$toAdd    = false;
					$readOnly = true;
					$currentRoles  = $object->getRoleArrayByUserId();
					$currentGroups = $object->getGroupArrayByUserId();
				} 
			else
				{
					$object = null;
				}
			
		}
		
		if( $languages = Sysvar::where( 'param' , '=' , 'language_1_en')->get() ){
			foreach( $languages as $language )
			{
				$langOptions[$language->value] =  $language->name;
			}
		}
		
		$roleObj = new Role;
		$roleOptions = array();
		if($roles = $roleObj->getAllRoles()) {
			foreach($roles as $role) {
				$roleOptions[$role->id] = $role->name;
			}
		}
		
		$groupObj = new Usergroup;
		$groupOptions = array();
		if($groups = $groupObj->getAllGroups()) {
			foreach($groups as $group) {
				$groupOptions[$group->id] = $group->name;
			}
		}
		
		

		$currentRoleValues = '';
		if($currentRoles && count($currentRoles) > 0)
		$currentRoleValues = implode(',', array_keys($currentRoles));
		
		$currentGroupValues = '';
		if($currentGroups && count($currentGroups) > 0)
		$currentGroupValues = implode(',', array_keys($currentGroups));

		$form = App::setupPanelForm('doadd', $request->all(), array(), $toAdd);
		$form->setFormTitle($title);
		
		$form->addInput((($object)? 'showonly':'text'), 		Lang::get('COMMON.USERNAME') 			, 'username', 	 	  (($object)? $object->username:''), 		array(), false); 
		$form->addInput('password', 	Lang::get('COMMON.PASSWORD') 			, 'password', 	 	  (($object)? $object->username:''), 		array(), false); 
		$form->addInput('password', 	Lang::get('COMMON.RETYPEPASSWORD') 		, 'retype_password',  (($object)? $object->username:''), 		array(), false); 
		$form->addInput('text', 		Lang::get('COMMON.FIRSTNAME') 			, 'firstname', 	 	  (($object)? $object->firstname:''), 		array(), false); 
		$form->addInput('text', 		Lang::get('COMMON.LASTNAME') 			, 'lastname', 	 	  (($object)? $object->lastname:''), 		array(), false); 
		$form->addInput('select', 		Lang::get('COMMON.LANGUAGE') 			, 'lang', 	 	      (($object)? $object->lang:''), 		array( 'options' => $langOptions ), false); 
		$form->addInput('swapbox',		Lang::get('COMMON.ROLE')				, 'role',      '', array('options' => $roleOptions, 'values' => $currentRoles), true);
		$form->addInput('swapbox', 		Lang::get('COMMON.USERGROUP')			, 'usergroup', '', array('options' => $groupOptions, 'values' => $currentGroups), true);
		$form->addInput('text', 		Lang::get('COMMON.EMAIL') 				, 'email', 	 	 	  (($object)? $object->email:''), 			array(), false); 
		$form->addInput('radio',   		Lang::get('COMMON.STATUS')				, 'status',  	 	  (($object)? $object->status:''), 			array('options' => App::getStatusOptions()), true);	

		
	
		$data['form'] = $form->getTemplateVars();
		$data['module'] = 'user';

		return view('admin.form2',$data);
	}
	
	protected function doAddEdit(Request $request) {
                        $toAdd 	  = true;        
			
			if( !$request->has('sid') ){
                            	$validator = Validator::make(
                                        [
                                                'firstname'	  	 	 => $request->input('firstname'),
                                                'lastname'	  	 	 => $request->input('lastname'),
                                                'status'	  	 	 => $request->input('status'),
                                                'password'	  	 	 => $request->input('password'),
                                                'retype_password'	 => $request->input('retype_password'),

                                        ],
                                        [

                                           'firstname'	  		   => 'required|alpha_num',
                                           'lastname'	  		   => 'required|alpha_num',
                                           'status'	  		  	   => 'required',
                                           'password'	  		   => 'required|alpha_num|min:6',
                                           'retype_password'	   => 'required|alpha_num|min:6',

                                        ]
                                );
                                if ($validator->fails()){
					echo json_encode($validator->errors());
					exit;
				}
                                
                                if( $request->input('password') != $request->input('retype_password') ){
                                        echo json_encode( array('password' => Lang::get('COMMON.PASSWORDNOTMATCH')));
                                        exit;
                                }
                        }else{
                            $toAdd = false;
                        }
			
			$object  = new User;
			
			if($request->has('sid')) 
			{
				$object = User::find($request->input('sid'));
			}
			
			$object->username	 			= $request->input('username');
			$object->firstname	 			= $request->input('firstname');
			$object->lastname	 			= $request->input('lastname');
			$object->lastlogin	 			= '';
			$object->status	 				= $request->input('status');
			$object->email	 				= $request->input('email');
			$object->lang	 				= $request->input('lang');
			$object->timezone	 			= 8;
			$object->createdby	 			= Session::get('admin_userid');
			$object->modifiedby	 			= Session::get('admin_userid');
                        if($toAdd) $object->password	 			= Hash::make( $request->input('password') );
			$object->crccode	 			= Session::get('admin_crccode');


			
			if( $object->save() )
			{	
				$object->updateRole(explode(',',$request->input('role')));
				echo json_encode( array( 'code' => Lang::get('COMMON.SUCESSFUL') ) );
				exit;
			}


			echo json_encode( array( 'code' => Lang::get('COMMON.FAILED') ) );
			
		
		}
	
		
	}


?>
