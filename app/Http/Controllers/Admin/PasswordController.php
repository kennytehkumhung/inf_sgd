<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller\Admin;
use App\libraries\App;
use Illuminate\Http\Request;
use Validator;
use Session;
use Lang;
use Config;
use Hash;
use Auth;
use App\Models\User;

class PasswordController extends Controller{
	
	public function __construct()
	{
		
	}

	public function index()
	{  
		return view('admin.update_password');
	}	
	
	public function doAddEdit(Request $request)
	{       
		$validator = Validator::make(
			[
				'current_password'  => $request->input('current_password'),
				'new_password'      => $request->input('new_password'),
				'retype_password'   => $request->input('retype_password'),
			],
			[
				'current_password' => 'required',
				'new_password'     => 'required|alpha_num|min:6',
				'retype_password'  => 'required|alpha_num|min:6',
			]
		);
		
		if ($validator->fails())
		{
			echo json_encode($validator->errors());
			exit;
		}
		
		if ( !Hash::check($request->input('current_password'), User::where( 'id' , '=' , Session::get('admin_userid') )->pluck('password') ))
		{ 
			echo json_encode( array('current_password' => Lang::get('commo.oldpass_wrong')));
			exit;
		}
	

			$object = User::where( 'id' , '=' , Session::get('admin_userid') )->first();
			$object->password      = Hash::make( $request->input('new_password') );
			$object->lastpwdchange = date('Y-m-d H:i:s');
	
			if($object->save())
			{
				echo json_encode( array( 'code' => Lang::get('COMMON.SUCESSFUL') ) );
			} 
		
		
		
	}		
	

	
	
}
