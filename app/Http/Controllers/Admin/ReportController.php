<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller\Admin;
use App\libraries\grid\PanelGrid;
use App\libraries\App;
use App\libraries\sms\SMS88;
use App\libraries\sms\SMSMoDing;
use App\Models\Channel;
use App\Models\Currency;
use App\Models\Account;
use App\Models\Affiliate;
use App\Models\Accountdetail;
use App\Models\Cashledger;
use App\Models\Bankledger;
use App\Models\Promocampaign;
use App\Models\Promocash;
use App\Models\Agent;
use App\Models\Product;
use App\Models\Smslog;
use App\Models\User;
use App\Models\Wager;
use App\Models\Cashbalance;
use App\Models\Profitloss;
use App\Models\Balancetransfer;
use Illuminate\Http\Request;
use Auth;
use Validator;
use Session;
use Lang;
use DB;
use Config;
use Carbon\Carbon;
use Excel;


class ReportController extends Controller{


	protected $moduleName 	= 'report';
	protected $limit 		= 20;
	protected $start 		= 0;

	public function __construct()
	{

	}

	public function showMainReport(Request $request)
	{

		$canFilterProduct = $this->hasPermission('MEMBER', 'MEMBERENQUIRY', 'MEMBERPNLFILTERPRODUCT');

		$grid = new PanelGrid;
		if( $request->has('sid') )
		{
			$grid->setupGrid($this->moduleName, 'profitloss-data', $this->limit, true, array('multiple' => true,'footer' => true,'params' => array('ilevel' => 5 )));
		}
		elseif($request->has('aflid'))
		{

			$grid->setupGrid($this->moduleName, 'profitloss-data', $this->limit, true, array('multiple' => true,'footer' => true,'params' => array('aflid' => $request->aflid ) ));
		}
		else
		{
			$grid->setupGrid($this->moduleName, 'profitloss-data', $this->limit, true, array('multiple' => true,'footer' => true ));
		}

		$grid->setTitle(Lang::get('COMMON.REPORT'));

		$level    = $request->has('level') ? $request->input('level'):Agent::LEVEL_SHAREHOLDER;
		$levelstr = Agent::getLevelText($level);

		$grid->addColumn('member', 			Lang::get('COMMON.ACCOUNT'), 				150,	array('align' => 'left'));
		$grid->addColumn('vendor', 			Lang::get('COMMON.VENDOR'), 				60,		array('align' => 'left'));

		if ($canFilterProduct || Session::has('affiliate_username') ) {
            $grid->addColumn('product', Lang::get('COMMON.PRODUCT'), 80, array('align' => 'left'));
        }

		$grid->addColumn('currency', 		Lang::get('COMMON.CURRENCY'), 				65,		array('align' => 'center'));
		$grid->addColumn('wager', 			Lang::get('COMMON.WAGERNUM'), 				70,		array('align' => 'right'));
		$grid->addColumn('stake', 			Lang::get('COMMON.STAKE'), 					130,	array('align' => 'right'));
		$grid->addColumn('validstake', 		Lang::get('COMMON.VALIDSTAKE'), 			130,	array('align' => 'right'));
		$grid->addColumn('winloss', 		Lang::get('COMMON.MEMBERPNL'), 				130,	array('align' => 'right'));
		$grid->addColumn('company', 		Lang::get('COMMON.COMPANY'), 				130,	array('align' => 'right'));
		$grid->addColumn('ratio1', 			Lang::get('COMMON.MARKETSHARE'), 			80,		array('align' => 'right'));
		$grid->addColumn('ratio2', 			Lang::get('COMMON.MARGINPERCENTAGE'), 		80,		array('align' => 'right'));

		$types = array(
			1	=> Lang::get('COMMON.REPORTALL'),
			2	=> Lang::get('COMMON.REPORTVENDOR'),
			3	=> Lang::get('COMMON.REPORTCATEGORY'),
		);

		if (!$canFilterProduct) {
		    unset($types[3]);
        }

		$productOptions = Product::getAllAsOptions();
		$agents = Agent::getLevelOptions(true);
		if(isset($agents[Agent::LEVEL_BIGSHAREHOLDER])) unset($agents[Agent::LEVEL_BIGSHAREHOLDER]);
		$grid->addSearchField('search1', array('code' => Lang::get('COMMON.CODE')));
		$grid->addFilter('imode', $types, array('display' => Lang::get('COMMON.REPORT'), 'value' => 1));
		$grid->addFilter('iprdid', Product::getAllAsOptions(), array('display' => Lang::get('COMMON.VENDOR')));

		if ($canFilterProduct || Session::has('affiliate_username')) {
            $grid->addFilter('icategory', Product::getCategoryOptions(), array('display' => Lang::get('COMMON.PRODUCT')));
        }

		$grid->addFilter('ilevel', $agents, array('display' => Lang::get('COMMON.LEVEL')));
		$grid->addRangeField('created', array('type' => 'date', 'display' => Lang::get('COMMON.DATE')));

		if( $request->has('sid') )
		{
			$accountObj = Account::find($request->input('sid'));
			$grid->addSearchField('search1', array('accid' => Lang::get('COMMON.MEMBERACCOUNT')) ,array('value' => $accountObj->nickname));
			$grid->addSearchField('search2', array('crccode' => Lang::get('COMMON.CURRENCY')) ,array('value' => $accountObj->crccode));
		}



		$grid->addButton('2', 'Back' , '', 'button', array('icon' => 'ok', 'url' => 'doBack();'));
//		$grid->addButton('2', 'Back' , '', 'button', array('icon' => 'ok', 'url' => 'window.history.back();'));

		$data['grid'] = $grid->getTemplateVars();
		$data['paging'] = false;

		return view('admin.grid2',$data);

	}

	protected function doGetData(Request $request) {

        $canFilterProduct = $this->hasPermission('MEMBER', 'MEMBERENQUIRY', 'MEMBERPNLFILTERPRODUCT');

		$start = 0;
		$limit = 0;
		$sortField = 'sumwinloss';

		$aqrng  = $request->input('aqrng');
		$aflt   = $request->input('aqfld');
		$aqtxt  = $request->input('aflt');

		/**
		* Get bet filter condition
		*/
		$from = ($request->has('createdfrom'))?$request->input('createdfrom'):App::getDateTime(3);
		$to   = ($request->has('createdto'))  ?$request->input('createdto'):App::getDateTime(3);
		$groupbyDate = ($request->has('igd'))?$request->input('igd'):0;
		/**
		* Select the record from the object based on the paging and sorting information and adding their to the grid as row
		*/
		$mode = (isset($aflt['imode']))?$aflt['imode']:1;

		$condition 	= 'istest = 0 AND date >= "'.$from.'" AND date <= "'.$to.'"';

		$condition  .= Session::get('admin_crccode') == 'MYR' ? '' : ' AND crccode = "'.Session::get('admin_crccode').'"';


		if( isset($aqtxt['accid']) && $aqtxt['accid'] != ''){
			if( isset($aqtxt['crccode'])  ){
				$accObj = Account::whereRaw('nickname="'.$aqtxt['accid'].'"')->where('crccode','=',$aqtxt['crccode'])->first();
			}else{
				$accObj = Account::whereRaw('nickname="'.$aqtxt['accid'].'"')->first();
			}
			if( isset($aqtxt['accid']) && $accObj )
				$condition .= ' AND accid='.$accObj->id;
		}

		$level = (isset($aflt['ilevel']) && $aflt['ilevel'] > Agent::LEVEL_SHAREHOLDER)?$aflt['ilevel']:Agent::LEVEL_SHAREHOLDER;
		$level = $request->has('ilevel') ? $request->get('ilevel') : $level;

			if($level == Agent::LEVEL_MEMBER) {
				$user = 'code';
				$field = 'accid';



				if(isset($aqtxt['code'])){
				    if($object = Agent::whereRaw('code="'.$aqtxt['code'].'"')->first())
					    $condition .= ' AND level'.($level-1).'='.$object->id;
				}
			} else {
				$object = new Agent;
				$user = 'code';
				$field = 'level'.$level;

				if(isset($aqtxt['code'])) {
				    if($level == Agent::LEVEL_AGENT) {
						if($object = Agent::whereRaw('code="'.$aqtxt['code'].'"')->first()){
						    $agentid = array();
						    if($lvls = Agent::whereRaw('parentid="'.$object->id.'"')->get()){
							    foreach($lvls as $lvl){
								    $agentid[] = $lvl->id;
							    }
							    $condition .= ' AND '.$field.' IN ('.implode(",",$agentid).')';
						    }
						}
					}
					if( $object = Agent::whereRaw('level='.$level.' AND code="'.$aqtxt['code'].'"')->first() )
						$condition .= ' AND '.$field.'='.$object->id;
					else {
						if($groupbyDate == 1){
							$condition = 'istest = 0 AND date >= "'.$request->createdfrom.'" AND date <= "'.$request->createdto.'"';
						}
					}
				}
			}
		//}
		$prdid = '';
		if(isset($aflt['iprdid']) && $aflt['iprdid'] > 0) {
			$condition .= ' AND prdid='.$aflt['iprdid'];
			$prdid = $aflt['iprdid'];
		}

		$category = '';

        if ($canFilterProduct || Session::has('affiliate_username')) {
            if (isset($aflt['icategory']) && $aflt['icategory'] > 0) {
                $condition .= ' AND category=' . $aflt['icategory'];
                $category = $aflt['icategory'];
            }
        }

		if( $request->has('supline') && isset($aflt['iparent'])) {
			$condition .= ' AND '.$request->get('supline').'='.$request->get('supline');
		}

		if( $request->has('aflid') )
		{

			$downlines = Affiliate::getAllDownlineById($request->aflid, true);

			$downlines[] = $request->aflid;

			$condition .= ' AND level4 IN (select id from agent where aflid IN ('.implode($downlines,',').') ) ';
		}

		/**
		* Instantiate a new object and select total record based on the condition define
		*/

		if($level)
		$total = Profitloss::whereRaw($condition)->count();
		else
		$total = 0;

		if($level) {
			$groupby = $field;
			if($groupbyDate == 1){
			    $groupby = 'date';
			    $sortField = 'date';
			}
			$fields = '$level1, $level2, $level3, $level4, $accid, $date, $acccode, $nickname, $prdid, $category, $crccode, SUM($wager) as $sumwager, SUM($totalstake) as $sumstake, SUM($amount) as $sumwinloss, SUM($validstake) as $sumvalid';
			if($mode == 2) {
				$groupby = 'prdid, '.$field;
				if($groupbyDate == 1 && $prdid != ''){
				    $groupby = 'date';
				    $sortField = 'date';
				}
			} else if($mode == 3) {
				$groupby = 'category, '.$field;
				if($groupbyDate == 1 && $category != ''){
				    $groupby = 'date';
				    $sortField = 'date';
				}
			}
		}

		$rows = array();
		$footer = array();
		$total_report = array();

		$profitlosses = DB::select( DB::raw("SELECT level1, level2, level3, level4, accid, date, acccode, nickname, prdid, category, crccode, SUM(wager) as sumwager, SUM(totalstake) as sumstake, SUM(amount) as sumwinloss, SUM(validstake) as sumvalid
										FROM `profitloss`
										WHERE ".$condition."
										GROUP BY ".$groupby."
										ORDER BY nickname ASC
										"));


		$categories = Product::getCategoryOptions();
		$products   = Product::getAllAsOptions(true);


		foreach($profitlosses as $profitloss) {

			    $profitloss->sumstake   = $profitloss->crccode == 'VND' || $profitloss->crccode == 'IDR' ?  $profitloss->sumstake * 1000 :  $profitloss->sumstake;
			    $profitloss->sumvalid   = $profitloss->crccode == 'VND' || $profitloss->crccode == 'IDR' ?  $profitloss->sumvalid * 1000 :  $profitloss->sumvalid;
			    $profitloss->sumwinloss = $profitloss->crccode == 'VND' || $profitloss->crccode == 'IDR' ?  $profitloss->sumwinloss * 1000 :  $profitloss->sumwinloss;

				$colspan = 2;
				$username = '';
				$winloss = App::formatNegativeAmount($profitloss->sumwinloss);

				$search_vendor  = 'doGridSearch(\'imode\', \'dropdown\', \'2\');doSearch();';
				$search_product = 'doGridSearch(\'imode\', \'dropdown\', \'3\');doSearch();';

				$product = $vendor = Lang::get('COMMON.ALL');
				if($mode == 2) $prdid = (isset($profitloss->prdid))?$profitloss->prdid:'';
				if($prdid != '') $vendor = $products[$profitloss->prdid];

				if($mode == 3) $category = (isset($profitloss->category))?$profitloss->category:'';
				if($category != '') $product = $categories[$profitloss->category];

				if($level == Agent::LEVEL_MEMBER) {
					$newParams = $request->all();
					$newParams['imode'] = 2; //view product
					$newParams['susername'] = $profitloss->acccode;
					if(isset($newParams['ssearch']))
					unset($newParams['ssearch']);

					$stake = action('Admin\ReportController@showmain',$newParams);
					if($mode == 2) {
						$colspan = 3;
						$prdid = $profitloss->prdid;
						if($product == Lang::get('COMMON.ALL')) $prdid = 0;
						$detailParams['iprdid'] = $prdid;
						$detailParams['iaccid'] = $profitloss->accid;
						$detailParams['createdfrom'] = $request->createdfrom;
						$detailParams['createdto'] = $request->createdto;
						$stake = '<a href="'.action('Admin\ReportController@showdetail',$detailParams).'">'.App::displayNumberFormat($profitloss->sumstake).'</a>';
					}
					$username = $profitloss->nickname;
				} else {
					if($object = Agent::whereRaw('level='.$level.' AND id='.$profitloss->$field)->first()) {
						$username = '<a href="#" onclick="doGridSearch(\'iprdid\', \'dropdown\', \''.$prdid.'\');doGridSearch(\'icategory\', \'dropdown\', \''.$category.'\');doGridSearch(\'code\', \'search\', \''.$object->code.'\', \'code\');doGroupbyDate(1);">'.$object->code.'</a>';
						if($groupbyDate == 1) $username = $profitloss->date;
						if($level == Agent::LEVEL_AGENT) {
							$username = $object->code;
							/* if($object->isexternal == 1) {
								if($afdObj = Affiliatedetail::whereRaw('aflid='.$object->id)->first()) {
									$username .= ' ('.$afdObj->website.')';
								}
							} */
						}
					}
					$newParams = $request->all();
					$newParams['ilevel'] = $level + 1;
					$newParams['supline'] = $field;
					$newParams['iparent'] = $profitloss->$field;
					if(isset($newParams['susername']))
					unset($newParams['susername']);
					if(isset($newParams['ssearch']))
					unset($newParams['ssearch']);
					if($mode == 2)
					$newParams['iprdid'] = $profitloss->prdid;

					$stake = '<a href="'.action('Admin\ReportController@showmain',$newParams).'">'.App::displayNumberFormat($profitloss->sumstake).'</a>';
				}

				$search_vendor = 'doGridSearch(\'imode\', \'dropdown\', \'2\');doGridSearch(\'iprdid\', \'dropdown\', \''.$prdid.'\');doGridSearch(\'icategory\', \'dropdown\', \''.$category.'\');doGridSearch(\'code\', \'search\', \''.strip_tags($username).'\', \'code\');doSearch();';
				$search_product = 'doGridSearch(\'imode\', \'dropdown\', \'3\');doGridSearch(\'iprdid\', \'dropdown\', \''.$prdid.'\');doGridSearch(\'icategory\', \'dropdown\', \''.$category.'\');doGridSearch(\'code\', \'search\', \''.strip_tags($username).'\', \'code\');doSearch();';

				if($level == Agent::LEVEL_MEMBER) {
					$newParams = array('iaccid' => $profitloss->accid);
					$newParams['aqrng']['createdfrom'] = $from;
					$newParams['aqrng']['createdto'] = $to;
					$newParams['aflt']['iprdid'] = $prdid;

					$prdid = $profitloss->prdid;
					if($product == Lang::get('COMMON.ALL')) $prdid = 0;
					$detailParams['iprdid'] = $prdid;
					$detailParams['iaccid'] = $profitloss->accid;
					$detailParams['createdfrom'] = $request->createdfrom;
					$detailParams['createdto'] = $request->createdto;

					$newParams['aflt']['icategory'] = $category;


					$stake = App::formatAddTabUrl(Lang::get('COMMON.BETDETAILS').': '.$username.' - '.$vendor.' - '.$product, App::formatNegativeAmount($profitloss->sumstake), action('Admin\ReportController@showdetail',$detailParams));
					$name = Lang::get('COMMON.BETDETAILS').': '.$username.' - '.$vendor.' - '.$product;
					$stake = '<a onclick="window.open(\''.action('Admin\ReportController@showdetail',$detailParams).'\', \'report\', \'scrollbars=1,width=1500,height=830\')" href="#">'.App::formatNegativeAmount($profitloss->sumstake).'</a>';
				} else{
				    $stake = '<a href="#" onclick="doGridSearch(\'iprdid\', \'dropdown\', \''.$prdid.'\');doGridSearch(\'icategory\', \'dropdown\', \''.$category.'\');doGridSearch(\'ilevel\', \'dropdown\', \''.($level+1).'\');doGridSearch(\'code\', \'search\', \''.strip_tags($username).'\', \'code\');doSearch();">'.App::formatNegativeAmount($profitloss->sumstake).'</a>';
				}
				$vendor = '<a href="#" onclick="'.$search_vendor.'">'.$vendor.'</a>';
				$product = '<a href="#" onclick="'.$search_product.'">'.$product.'</a>';

				$percentage = 0;
				if($profitloss->sumstake > 0) $percentage = $profitloss->sumwinloss / $profitloss->sumstake * 100;
				$percentage *= -1;
				$rows[] = array(
					'currency' 		=> $profitloss->crccode,
					'member' 		=> $username,
					'vendor' 		=> $vendor,
					'product' 		=> $product,
					'wager' 		=> App::displayAmount($profitloss->sumwager, 0),
					'stake' 		=> $stake,
					'stake_amount'  => $profitloss->sumstake,
					'validstake' 	=> App::formatNegativeAmount($profitloss->sumvalid),
					'winloss' 		=> App::formatNegativeAmount($profitloss->sumwinloss),
					'company' 		=> App::formatNegativeAmount($profitloss->sumwinloss * -1),
					'ratio2' 		=> App::displayPercentage($percentage, 2, true),
				);

				if(!isset($total_report[$profitloss->crccode])) {
					$total_report[$profitloss->crccode] = array(
						'wager' 		=> 0,
						'validstake' 	=> 0,
						'stake' 		=> 0,
						'winloss' 		=> 0,
					);
				}

				$total_report[$profitloss->crccode]['wager'] 	  += $profitloss->sumwager;
				$total_report[$profitloss->crccode]['validstake'] += $profitloss->sumvalid;
				$total_report[$profitloss->crccode]['stake']	  += $profitloss->sumstake;
				$total_report[$profitloss->crccode]['winloss']	  += $profitloss->sumwinloss;

		}



		if(count($total_report) > 0) {
			foreach($total_report as $crccode => $report) {
				$percentage = 0;
				if($report['stake'] > 0) $percentage = $report['winloss'] / $report['stake'] * 100;
				$percentage *= -1;
				$footer[] = array(
					'member' 	 => Lang::get('COMMON.TOTAL'),
					'currency'   => $crccode,
					'wager' 	 => App::displayAmount($report['wager'], 0),
					'validstake' => App::formatNegativeAmount($report['validstake']),
					'stake' 	 => App::formatNegativeAmount($report['stake']),
					'winloss' 	 => App::formatNegativeAmount($report['winloss']),
					'company' 	 => App::formatNegativeAmount($report['winloss'] * -1),
					'ratio2' 	 => App::displayPercentage($percentage, 2, true),
					'ratio1' 	 => App::displayPercentage(100),
				);
			}

			foreach($rows as $key => $row) {
				$ratio1 = 0;
				if($total_report[$row['currency']]['stake'] > 0)
				$ratio1 = $row['stake_amount'] / $total_report[$row['currency']]['stake'] * 100;
				$rows[$key]['ratio1'] = App::displayPercentage($ratio1);
			}
		}

		echo json_encode(array('total' => count($rows), 'rows' => $rows, 'footer' => $footer));
		exit;
	}

	public function showmain (Request $request)
	{

	}

	public function showdetail (Request $request)
	{
		$aqrng  = $request->input('aqrng');

		$grid = new PanelGrid;
		if(isset($aqrng['createdfrom'])){
			$grid->setupGrid($this->moduleName, 'showdetail-data', $this->limit, true, array('params' => array('prdid' => $request->iprdid , 'createdfrom' => $aqrng['createdfrom'], 'createdto' => $aqrng['createdto'], 'accid' => $request->iaccid, 'time' => $request->itime) , 'footer' => true));
		}else{
			$grid->setupGrid($this->moduleName, 'showdetail-data', $this->limit, true, array('params' => array('prdid' => $request->iprdid , 'createdfrom' => $request->createdfrom, 'createdto' => $request->createdto, 'accid' => $request->iaccid, 'time' => $request->itime) , 'footer' => true));
		}

		$grid->setTitle(Lang::get('COMMON.REPORT'));

		$level    = $request->has('level') ? $request->input('level'):Agent::LEVEL_SHAREHOLDER;
		$levelstr = Agent::getLevelText($level);

        $productObj = Product::find($request->iprdid);

		$grid->addColumn('date', 			Lang::get('COMMON.TIME'), 					150,	array('align' => 'center'));
		$grid->addColumn('member', 			Lang::get('COMMON.MEMBER'), 				80,		array('align' => 'left'));
		$grid->addColumn('vendor', 			Lang::get('COMMON.VENDOR'), 				80,		array('align' => 'center'));
		$grid->addColumn('product', 		Lang::get('COMMON.PRODUCT'), 				80,		array('align' => 'left'));
		$grid->addColumn('detail', 			Lang::get('COMMON.DETAILS'), 				200,	array());
		$grid->addColumn('currency', 		Lang::get('COMMON.CURRENCY'), 				60,		array('align' => 'center'));
		$grid->addColumn('stake', 			Lang::get('COMMON.STAKE'), 					70,		array('align' => 'right'));
		$grid->addColumn('winloss', 		Lang::get('COMMON.PROFITLOSS'), 			70,		array('align' => 'right'));
		$grid->addColumn('valid', 			Lang::get('COMMON.VALIDSTAKE'), 			70,		array('align' => 'right'));
		$grid->addColumn('result', 			Lang::get('COMMON.RESULT'), 				70,		array('align' => 'center'));

        // Additional columns for ASC, M8B sportbooks.
        $grid->addColumn('leagueName',      Lang::get('public.League'),                 80,     array('align' => 'left'));
        $grid->addColumn('homeName',        Lang::get('COMMON.SELECTIONHOME'),          80,     array('align' => 'left'));
        $grid->addColumn('awayName',        Lang::get('COMMON.SELECTIONAWAY'),          80,     array('align' => 'left'));
        $grid->addColumn('betScore',        Lang::get('COMMON.SCORE'),                  60,     array('align' => 'center'));
        $grid->addColumn('betPos',          Lang::get('COMMON.BETTERM'),                80,     array('align' => 'center'));


		$data['grid'] = $grid->getTemplateVars();

		return view('admin.grid2',$data);

	}

	public function showdetailpaymentdoGetData(Request $request){

		if(  $request->has('timefrom') ){
			$from 		= $request->get('createdfrom').' '.$request->get('timefrom');
			$to   		= $request->get('createdto').' '.$request->get('timeto');

			$condition  = 'datetime >= "'.$from.'" AND datetime <= "'.$to.'" AND accid = '.$request->get('accid').'';

			if( $request->get('prdid') != 0 )
				$condition  .= ' AND prdid = '.$request->get('prdid').' ';
		}else{
			$from 		= $request->has('createdfrom')?$request->get('createdfrom'):App::getDateTime(11);
			$to   		= $request->has('createdto')?$request->get('createdto'):App::getDateTime(3);

			if( $request->get('prdid') == 0 ) {
				if($request->get('time') == 1) {
					$condition  = 'datetime >= "'.$from.'" AND datetime <= "'.$to.'" AND accid = '.$request->get('accid').'';
				}  else {
					$condition  = 'accountdate >= "'.$from.'" AND accountdate <= "'.$to.'" AND accid = '.$request->get('accid').'';
				}
			} else
				$condition  = 'datetime >= "'.$from.' 00:00:00" AND datetime <= "'.$to.' 23:59:59" AND prdid = '.$request->get('prdid').' AND accid = '.$request->get('accid').'';
		}



		$total = Wager::whereRaw($condition)->count();

		$rows = array();
		$footer = array();
		$totalstake = 0;
		$totalwinloss = 0;
		$totalvalid = 0;
		//var_dump($total);

        $productObj = Product::find($request->get('prdid'));
        $productCode = '';

        if ($productObj) {
            $productCode = $productObj->code;
        }

		if($betslips = Wager::whereraw($condition)->skip($request->recordstartindex)->take( $request->pagesize )->orderBy('datetime','desc')->get()) {

			$vendors = Product::getAllAsOptions();
			$products = Product::getCategoryOptions();
            $refIds = array();

			foreach($betslips as $betslip) {
				//$winloss = $betslip->profitloss;
				$valid = $betslip->validstake;
                $refIds[] = $betslip->refid;

				$betslip->stake 	 = $betslip->crccode == 'VND' || $betslip->crccode == 'IDR' ? $betslip->stake * 1000 : $betslip->stake;
				$betslip->validstake = $betslip->crccode == 'VND' || $betslip->crccode == 'IDR' ? $betslip->validstake * 1000 : $betslip->validstake;
				$betslip->profitloss = $betslip->crccode == 'VND' || $betslip->crccode == 'IDR' ? $betslip->profitloss * 1000 : $betslip->profitloss;

				$row = array(
					'id' => $betslip->id,
                    'refid' => $betslip->refid,
					'date' => $betslip->datetime,
					'member' => $betslip->nickname,
					'vendor' => $vendors[$betslip->prdid],
					'product' => $products[$betslip->category],
					'detail' => '#'.$betslip->category.'-'.$betslip->id.'-'.$betslip->refid,
					'currency' => $betslip->crccode,
					'stake' => App::formatNegativeAmount($betslip->stake),
					'winloss' => App::formatNegativeAmount($betslip->profitloss),
					'valid' => App::formatNegativeAmount($betslip->validstake),
					'result' => $betslip->getResultText(),
				);

				$totalvalid += $betslip->validstake;
				$totalstake += $betslip->stake;
				$totalwinloss += $betslip->profitloss;

                $rows[] = $row;
			}

            // Special case for some sportbooks report.
            if ($productCode == 'ASC') {
                $refIds = array_unique($refIds);

                $recordObjs = DB::table('bet_asc as bet')
                    ->leftJoin('bet_asc_league as league', 'league.league_id', '=', 'bet.leagueId')
                    ->leftJoin('bet_asc_team as home', 'home.team_id', '=', 'bet.homeId')
                    ->leftJoin('bet_asc_team as away', 'away.team_id', '=', 'bet.awayId')
                    ->whereIn('bet.betId', $refIds)
                    ->get(['bet.betId', 'bet.betPos', 'bet.betScore', 'league.league_name as leagueName', 'home.team_name as homeName', 'away.team_name as awayName']);

                foreach ($recordObjs as $r) {
                    // Attach extra data into result rows.
                    for ($i = 0; $i < count($rows); $i++) {
                        $row = $rows[$i];

                        if ($row['refid'] == $r->betId) {
                            $row['leagueName'] = $r->leagueName;
                            $row['homeName'] = $r->homeName;
                            $row['awayName'] = $r->awayName;
                            $row['betScore'] = $r->betScore;
                            $row['betPos'] = $r->betPos;

                            $rows[$i] = $row;
                            break;
                        }
                    }
                }
            } elseif ($productCode == 'M8B') {
                $refIds = array_unique($refIds);

                $recordObjs = DB::table('bet_m8b as bet')
                    ->leftJoin('bet_m8b_league as league', 'league.league_id', '=', 'bet.league')
                    ->leftJoin('bet_m8b_team as home', 'home.team_id', '=', 'bet.home')
                    ->leftJoin('bet_m8b_team as away', 'away.team_id', '=', 'bet.away')
                    ->whereIn('bet.transId', $refIds)
                    ->get(['bet.transId', 'bet.side', 'bet.runScore', 'league.league_name as leagueName', 'home.team_name as homeName', 'away.team_name as awayName']);

                foreach ($recordObjs as $r) {
                    // Attach extra data into result rows.
                    for ($i = 0; $i < count($rows); $i++) {
                        $row = $rows[$i];

                        if ($row['refid'] == $r->transId) {
                            $row['leagueName'] = $r->leagueName;
                            $row['homeName'] = $r->homeName;
                            $row['awayName'] = $r->awayName;
                            $row['betScore'] = $r->runScore;
                            $row['betPos'] = ($r->side == 1 ? 'Home' : ($r->side == 2 ? 'Away' : 'Draw'));

                            $rows[$i] = $row;
                            break;
                        }
                    }
                }
            }
		}

		$footer[] = array(
			'currency' => Lang::get('COMMON.TOTAL'),
			'stake' => App::formatNegativeAmount($totalstake),
			'winloss' => App::formatNegativeAmount($totalwinloss),
			'valid' => App::formatNegativeAmount($totalvalid),
		);
		echo json_encode(array('total' => $total, 'rows' => $rows, 'footer' => $footer));
		exit;

	}

	public function showMemberMonthReport(Request $request){

			$grid = new PanelGrid;
			if($request->has('aflid'))
			{
				$grid->setupGrid($this->moduleName, 'memberReport-data', $this->limit, true, array('multiple' => true,'footer' => true ,'params' => array('aflid' => $request->aflid )));
			}
			else
			{
				$grid->setupGrid($this->moduleName, 'memberReport-data', $this->limit, true, array('multiple' => true,'footer' => true ));
			}

			$grid->setTitle(Lang::get('COMMON.REPORT'));

			$grid->addColumn('account', 			Lang::get('COMMON.ACCOUNT'), 		100,		array('align' => 'left'));
			$grid->addColumn('name', 				Lang::get('COMMON.NAME'), 			128,		array('align' => 'left'));
			$grid->addColumn('currency', 			Lang::get('COMMON.CURRENCY'), 		68,			array('align' => 'center'));
			if(Config::get('setting.front_path') == 'sbm'){
				$grid->addColumn('mainwallet', 		Lang::get('COMMON.MAINWALLET'), 	128,		array('align' => 'right'));
				$grid->addColumn('m8b', 			'M8B', 								128,		array('align' => 'right'));
				$grid->addColumn('asc', 			'ASC', 								128,		array('align' => 'right'));
			}
			$grid->addColumn('deposit', 			Lang::get('COMMON.DEPOSIT'), 		128,		array('align' => 'right'));
			$grid->addColumn('withdrawal', 			Lang::get('COMMON.WITHDRAWAL'), 	128,		array('align' => 'right'));
			$grid->addColumn('bonus', 				Lang::get('COMMON.BONUS'), 			128,		array('align' => 'right'));
			$grid->addColumn('incentive', 			Lang::get('COMMON.INCENTIVE'), 		128,		array('align' => 'right'));
			$grid->addColumn('turnover', 			Lang::get('COMMON.TURNOVER'), 		128,		array('align' => 'right'));
			$grid->addColumn('memberpnl', 			Lang::get('COMMON.MEMBERPNL'), 		128,		array('align' => 'right'));
			$grid->addColumn('telmobile', 			'telmobile', 		128,		array('align' => 'right'));
            $grid->addButton('2', 'Export' , '', 'button', array('icon' => 'ok', 'url' => 'export_excel();'));

			$data['grid'] = $grid->getTemplateVars();
			$data['paging'] = false;

			return view('admin.grid2',$data);
    }
	public function MemberdoGetData(Request $request){


		$total = array();
		$rows = array();
		$count = 0;
		$total = array(
							'deposit' => 0,
							'depositcount' => 0,
							'withdraw' => 0,
							'withdrawcount' => 0,
							'bonus' => 0,
							'bonuscount' => 0,
							'incentive' => 0,
							'incentivecount' => 0,
							'wager' => 0,
							'turnover' => 0,
							'sumwinloss' => 0,
							'mainwallet' => 0,
							'asc' => 0,
							'm8b' => 0,
						);

		$from 		= $request->has('createdfrom')?$request->get('createdfrom'):App::getDateTime(11);
		$to   		= $request->has('createdto')?$request->get('createdto'):App::getDateTime(3);
		$Condition  = 'istest = 0 AND date >= "'.$from.'" AND date <= "'.$to.'" AND crccode = "'.Session::get('admin_crccode').'"';

		if( $request->has('aflid') )
		{

			$downlines = Affiliate::getAllDownlineById($request->aflid, true);

			$downlines[] = $request->aflid;

			$Condition .= ' AND level4 IN (select id from agent where aflid IN ('.implode($downlines,',').') ) AND crccode = "'.Session::get('admin_crccode').'"';
		}

		if($pnls = Profitloss::select(DB::raw('accid,acccode,crccode,nickname,amount,sum(validstake) as sumvalid,sum(amount) as sumwinloss'))->whereRaw($Condition)->groupBy('accid')->orderBy('sumwinloss','DESC')->get()){

			foreach( $pnls as $pnl ){

				$telmobile = '';
				$lastbetdate = '';
				$lastlogin = '';
				$fullname = '';
				$email = '';
				if( $accObj = Account::find($pnl->accid) ) {
					$lastbetdate = $accObj->lastbetdate;
					$lastlogin   = $accObj->lastlogin;
					$acdObj 	 = Accountdetail::whereRaw('accid = '.$pnl->accid.' ')->first();
                    if ($acdObj) {
                        $telmobile 	 = $acdObj->telmobile;
                        $email 		 = $acdObj->email;
                        $fullname 	 = $acdObj->fullname;
                    }
				}


				$timefrom = $request->createdfrom.' 00:00:00';
				$timeto   = $request->createdto  .' 23:59:59';

				$depositSql 	= 'accid='.$pnl->accid.' AND chtcode='.CBO_CHARTCODE_DEPOSIT.' AND (status='.CBO_LEDGERSTATUS_CONFIRMED.' OR status='.CBO_LEDGERSTATUS_MANCONFIRMED.') AND created >= "'.$timefrom.'" AND created <= "'.$timeto.'"';
				$withdrawSql 	= 'accid='.$pnl->accid.' AND chtcode='.CBO_CHARTCODE_WITHDRAWAL.' AND (status='.CBO_LEDGERSTATUS_CONFIRMED.' OR status='.CBO_LEDGERSTATUS_MANCONFIRMED.') AND created >= "'.$timefrom.'" AND created <= "'.$timeto.'"';
				$bonusSql 		= 'accid='.$pnl->accid.' AND chtcode='.CBO_CHARTCODE_BONUS.' AND (status='.CBO_LEDGERSTATUS_CONFIRMED.' OR status='.CBO_LEDGERSTATUS_MANCONFIRMED.') AND created >= "'.$timefrom.'" AND created <= "'.$timeto.'"';
				$incentiveSql	= 'accid='.$pnl->accid.' AND chtcode='.CBO_CHARTCODE_INCENTIVE.' AND (status='.CBO_LEDGERSTATUS_CONFIRMED.' OR status='.CBO_LEDGERSTATUS_MANCONFIRMED.') AND created >= "'.$timefrom.'" AND created <= "'.$timeto.'"';


				$depositcount 	= Cashledger::whereRaw($depositSql)->count();
				$deposit 		= Cashledger::whereRaw($depositSql)->sum('amount');

				$withdrawcount  = Cashledger::whereRaw($withdrawSql)->count();
				$withdraw 		= Cashledger::whereRaw($withdrawSql)->sum('amount');

				$bonuscount 	= Cashledger::whereRaw($bonusSql)->count();
				$bonus 			= Cashledger::whereRaw($bonusSql)->sum('amount');

				$incentivecount = Cashledger::whereRaw($incentiveSql)->count();
				$incentive 		= Cashledger::whereRaw($incentiveSql)->sum('amount');


				$sumwinloss = ($pnl->crccode == 'VND' || $pnl->crccode == 'IDR')? $pnl->sumwinloss * 1000 : $pnl->sumwinloss;

				$sumvalid   = ($pnl->crccode == 'VND' || $pnl->crccode == 'IDR')? $pnl->sumvalid * 1000 : $pnl->sumvalid;


				$rows[$count] = array(

					'account'			=> $accObj->nickname,
					'name'				=> $fullname,
					'currency'			=> $pnl->crccode,
					'deposit' 			=> $depositcount.' / '.App::formatNegativeAmount($deposit),
					'withdrawal' 		=> $withdrawcount.' / '.App::formatNegativeAmount($withdraw),
					'bonus' 			=> $bonuscount.' / '.App::formatNegativeAmount($bonus),
					'incentive' 		=> $incentivecount.' / '.App::formatNegativeAmount($incentive),
					'turnover' 			=> App::formatNegativeAmount($sumvalid),
					'memberpnl' 		=> App::formatNegativeAmount($sumwinloss),
					'telmobile'			=> $telmobile,


				);

				if(Config::get('setting.front_path') == 'sbm')
				{
					$mainwallet     = Cashledger::whereRaw('accid='.$pnl->accid.' AND ((status in ('.CBO_LEDGERSTATUS_PENDING.', '.CBO_LEDGERSTATUS_MANPENDING.','.CBO_LEDGERSTATUS_CONFIRMED.', '.CBO_LEDGERSTATUS_MANCONFIRMED.', '.CBO_LEDGERSTATUS_PROCESSED.') AND chtcode IN ('.CBO_CHARTCODE_WITHDRAWAL.', '.CBO_CHARTCODE_BALANCETRANSFER.', '.CBO_CHARTCODE_WITHDRAWALCHARGES.' )) OR (status in ('.CBO_LEDGERSTATUS_CONFIRMED.', '.CBO_LEDGERSTATUS_MANCONFIRMED.') AND chtcode NOT IN ('.CBO_CHARTCODE_WITHDRAWAL.', '.CBO_CHARTCODE_BALANCETRANSFER.' , '.CBO_CHARTCODE_WITHDRAWALCHARGES.' )))')->sum('amount');

					$rows[$count]['mainwallet']     = App::formatNegativeAmount($mainwallet);

					$total['mainwallet'] += $mainwallet;


					$rows[$count]['m8b'] = Cashbalance::wherePrdid(2)->whereAccid($accObj->id)->pluck('balance');

					$total['m8b'] += $rows[$count]['m8b'];

					$rows[$count]['m8b'] = $rows[$count]['m8b'] > 0 ? App::formatNegativeAmount($rows[$count]['m8b']) : 0;




					$rows[$count]['asc'] = Cashbalance::wherePrdid(3)->whereAccid($accObj->id)->pluck('balance');

					$total['asc'] += $rows[$count]['asc'];

					$rows[$count]['asc'] = $rows[$count]['asc'] > 0 ? App::formatNegativeAmount($rows[$count]['asc']) : 0;






				}



				$total['deposit'] += $deposit;
				$total['depositcount'] += $depositcount;
				$total['withdraw'] += $withdraw;
				$total['withdrawcount'] += $withdrawcount;
				$total['bonus'] += $bonus;
				$total['bonuscount'] += $bonuscount;
				$total['incentive'] += $incentive;
				$total['incentivecount'] += $incentivecount;
				$total['wager'] += $pnl->sumwager;
				$total['turnover'] += $pnl->sumstake;
				$total['sumwinloss'] += $pnl->sumwinloss;

				$count++;
			}

			$footer[] = array(

				'lastlogin' => App::getText('CBO_GUI_TOTAL'),

				'deposit' => $total['depositcount'].' / '.App::formatNegativeAmount($total['deposit']),
				'withdrawal' => $total['withdrawcount'].' / '.App::formatNegativeAmount($total['withdraw']),
				'bonus' => $total['bonuscount'].' / '.App::formatNegativeAmount($total['bonus']),
				'incentive' => $total['incentivecount'].' / '.App::formatNegativeAmount($total['incentive']),
				'turnover' => $total['wager'].' / '.App::formatNegativeAmount($total['turnover']),
				'memberpnl' => App::formatNegativeAmount($total['sumwinloss']),
				'm8b' => App::formatNegativeAmount($total['m8b']),
				'asc' => App::formatNegativeAmount($total['asc']),
				'mainwallet' => App::formatNegativeAmount($total['mainwallet']),


			);

            // Export as Excel file.
            if ($request->input('export') == 'excel') {
                // Check and remove unwanted HTML format from values.
                $epRows = array();

                foreach ($rows as $rkey => $rval) {
                    $epRows2 = array();

                    foreach ($rval as $rvkey => $rvval) {
                        $epRows2[$rvkey] = preg_replace('/<[^>]*>/', '', $rvval);
                    }

                    $epRows[$rkey] = $epRows2;
                }

                Excel::create('Member_Report_'.date('U'), function($excel) use ($epRows) {
                    $excel->sheet('Sheet1', function($sheet) use ($epRows) {
                        $sheet->fromArray($epRows);
                    });
                })->export('xls');
            }
        }

		echo json_encode(array('total' => count($rows), 'rows' => $rows, 'footer' => $footer));
		exit;

    }

	public function showChannelReport(Request $request){

			$grid = new PanelGrid;
			$grid->setupGrid($this->moduleName, 'channelReport-data', $this->limit);
			$grid->setTitle(Lang::get('COMMON.REPORT'));

			$grid->addColumn('channel', 			Lang::get('COMMON.CHANNEL'), 			100,		array('align' => 'left'));
			$grid->addColumn('currency', 			Lang::get('COMMON.CURRENCY'), 			68,			array('align' => 'center'));
			$grid->addColumn('register', 			Lang::get('COMMON.REGISTRATIONSHORT'), 	68,			array('align' => 'center'));
			$grid->addColumn('first', 				Lang::get('COMMON.FIRSTDEPOSIT'), 		168,		array('align' => 'left'));
			$grid->addColumn('continue', 			Lang::get('COMMON.CONTDEPOSIT'), 		128,		array('align' => 'left'));
			$grid->addColumn('withdraw', 			Lang::get('COMMON.WITHDRAWAL'), 		128,		array('align' => 'left'));
			$grid->addColumn('balance1', 			Lang::get('COMMON.GROSSBALANCE'), 		128,		array('align' => 'left'));
			$grid->addColumn('promotion', 			Lang::get('COMMON.PROMOTION'), 			128,		array('align' => 'left'));
			$grid->addColumn('incentive', 			Lang::get('COMMON.INCENTIVE'), 			128,		array('align' => 'left'));
			$grid->addColumn('turnover', 			Lang::get('COMMON.TURNOVER'), 			128,		array('align' => 'left'));
			$grid->addColumn('profitloss', 			Lang::get('COMMON.PROFITLOSSSHORT'), 	128,		array('align' => 'left'));


			$data['grid'] = $grid->getTemplateVars();

			return view('admin.grid2',$data);
    }

	public function ChannelReportdoGetData(Request $request,$return = false, $link = true){

		$i = 1;
		$rows = array();
		$currency_total = array();
		$channels = Channel::getAllAsOptions();
		$currencies = Currency::getAllCurrencyAsOptions();
		foreach($channels as $key => $channel){
		    foreach($currencies as $currency){

				 $accountGetCount = 'regchannel = "'.$key.'" AND crccode = "'.$currency.'" AND istestacc = 0 AND created >= "'.$request->createdfrom.' 00:00:00" AND created <= "'.$request->createdto.' 23:59:59"';

			    if(!$register = Account::whereRaw($accountGetCount)->count()){
					$register = 0;
			    }

				$con1 = 'crccode = "'.$currency.'" AND istest = 0 AND date >= "'.$request->createdfrom.' 00:00:00" AND date <= "'.$request->createdto.' 23:59:59"';

				if(!$member = Profitloss::whereRaw($con1)->count())
				{
					$member = 0;
				}
			    if(!$turnover = Profitloss::whereRaw($con1)->sum('totalstake'))
				{
					$turnover = 0;
				}

			    if(!$winloss = Profitloss::whereRaw($con1)->sum('amount'))
				{
					$winloss = 0;
				}

			    $sql = 'crccode = "'.$currency.'" AND created >= "'.$request->createdfrom.' 00:00:00" AND created <= "'.$request->createdto.' 23:59:59"';

				$condition1 = 'accid IN(select id from account where regchannel = "'.$key.'" ) AND chtcode='.CBO_CHARTCODE_DEPOSIT.' AND (status='.CBO_LEDGERSTATUS_CONFIRMED.' OR status='.CBO_LEDGERSTATUS_MANCONFIRMED.') AND '.$sql;
			    $condition2 = 'accid IN(select id from account where regchannel = "'.$key.'" ) AND chtcode='.CBO_CHARTCODE_WITHDRAWAL.' AND (status='.CBO_LEDGERSTATUS_CONFIRMED.' OR status='.CBO_LEDGERSTATUS_MANCONFIRMED.') AND '.$sql;
			    $condition3 = 'accid IN(select id from account where regchannel = "'.$key.'" ) AND chtcode='.CBO_CHARTCODE_BONUS.' AND (status='.CBO_LEDGERSTATUS_CONFIRMED.' OR status='.CBO_LEDGERSTATUS_MANCONFIRMED.') AND '.$sql;
			    $condition4 = 'accid IN(select id from account where regchannel = "'.$key.'" ) AND chtcode='.CBO_CHARTCODE_INCENTIVE.' AND (status='.CBO_LEDGERSTATUS_CONFIRMED.' OR status='.CBO_LEDGERSTATUS_MANCONFIRMED.') AND '.$sql;

				$deposit = $firstdep = $continuedep = $depositcount = $firstdepcount = $continuedepcount = 0;

			    if($ledgers = Cashledger::whereRaw($condition1)->get()) {
				    foreach($ledgers as $ledger) {
					    $deposit += $ledger->amount;
					    $depositcount += 1;
					    if(!Cashledger::whereRaw('accid = '.$ledger->accid.' AND chtcode='.CBO_CHARTCODE_DEPOSIT.' AND (status='.CBO_LEDGERSTATUS_CONFIRMED.' OR status='.CBO_LEDGERSTATUS_MANCONFIRMED.') AND created < "'.$ledger->created.'"')->get()) {
						    $firstdep += $ledger->amount;
						    $firstdepcount += 1;
					    } else {
						    $continuedep += $ledger->amount;
						    $continuedepcount += 1;
					    }
				    }
			    }

				if(!$deposit = Cashledger::whereRaw($condition1)->sum('amount'))
			    $deposit = 0;

			    if(!$depositcount = Cashledger::whereRaw($condition1)->count())
			    $depositcount = 0;

			    if(!$withdrawal = Cashledger::whereRaw($condition2)->sum('amount'))
			    $withdrawal = 0;

			    if(!$withdrawalcount = Cashledger::whereRaw($condition2)->count())
			    $withdrawalcount = 0;

			    $balance1 = abs($deposit) - abs($withdrawal);

			    if(!$bonus = Cashledger::whereRaw($condition3)->sum('amount'))
			    $bonus = 0;

			    if(!$incentive = Cashledger::whereRaw($condition4)->sum('amount'))
			    $incentive = 0;

			    $bonus = $bonus * -1;
			    $incentive = $incentive * -1;

			     if(isset($currency_total[$currency]['deposit']))
				 {
					 $currency_total[$currency]['deposit'] += $deposit;
				 }
			    else
				{
					$currency_total[$currency]['deposit'] = $deposit;
				}

			    if(isset($currency_total[$currency]['firstdepcount']))
				{
					$currency_total[$currency]['firstdepcount'] += $firstdepcount;
				}
			    else
				{
					$currency_total[$currency]['firstdepcount'] = $firstdepcount;
				}

			    if(isset($currency_total[$currency]['continuedepcount']))
				{
					$currency_total[$currency]['continuedepcount'] += $continuedepcount;
				}
			    else
				{
					$currency_total[$currency]['continuedepcount'] = $continuedepcount;
				}

			    if(isset($currency_total[$currency]['withdrawal']))
				{
					$currency_total[$currency]['withdrawal'] += $withdrawal;
				}
			    else
				{
					$currency_total[$currency]['withdrawal'] = $withdrawal;
				}

			    if(isset($currency_total[$currency]['withdrawalcount']))
				{
					$currency_total[$currency]['withdrawalcount'] += $withdrawalcount;
				}
			    else
				{
					$currency_total[$currency]['withdrawalcount'] = $withdrawalcount;
				}

			    if(isset($currency_total[$currency]['balance1']))
				{
					$currency_total[$currency]['balance1'] += $balance1;
				}
			    else
				{
					$currency_total[$currency]['balance1'] = $balance1;
				}

			    if(isset($currency_total[$currency]['bonus']))
				{
					$currency_total[$currency]['bonus'] += $bonus;
				}
			    else
				{
					$currency_total[$currency]['bonus'] = $bonus;
				}

			    if(isset($currency_total[$currency]['incentive']))
				{
					$currency_total[$currency]['incentive'] += $incentive;
				}
			    else
				{
					$currency_total[$currency]['incentive'] = $incentive;
				}

			    if(isset($currency_total[$currency]['register']))
				{
					$currency_total[$currency]['register'] += $register;
				}
			    else
				{
					$currency_total[$currency]['register'] = $register;
				}

			    if(isset($currency_total[$currency]['turnover']))
				{
					$currency_total[$currency]['turnover'] += $turnover;
				}
			    else
				{
					$currency_total[$currency]['turnover'] = $turnover;
				}

			    if(isset($currency_total[$currency]['winloss']))
				{
					$currency_total[$currency]['winloss'] += $winloss;
				}
			    else
				{
					$currency_total[$currency]['winloss'] = $winloss;
				}

			    if($link == true){
				$deposit = App::displayAmount($deposit);
				$firstdep = App::displayAmount($firstdep);
				$continuedep = App::displayAmount($continuedep);
				$withdrawal = App::formatNegativeAmount($withdrawal);
				$balance1 = App::formatNegativeAmount($balance1);
				$bonus = App::formatNegativeAmount($bonus);
				$incentive = App::formatNegativeAmount($incentive);
				$turnover = App::displayAmount($turnover);
				$winloss = App::formatNegativeAmount($winloss);
			    }
			    else{
				$deposit = App::displayAmount($deposit);
				$firstdep = App::displayAmount($firstdep);
				$continuedep = App::displayAmount($continuedep);
				$withdrawal = App::displayAmount($withdrawal);
				$balance1 = App::displayAmount($balance1);
				$bonus = App::displayAmount($bonus);
				$incentive = App::displayAmount($incentive);
				$turnover = App::displayAmount($turnover);
				$winloss =App::displayAmount( $winloss);
			    }

			    $rows[] = array(
				    'channel' 		=> $channel,
				     'currency' 		=> $currency,
				    'register' 		=> $register,
				    'first' 		=> $firstdepcount.' / '.$firstdep,
				    'continue' 		=> $continuedepcount.' / '.$continuedep,
				    'deposit' 		=> $depositcount.' / '.$deposit,
				    'withdraw' 		=> $withdrawalcount.' / '.$withdrawal,
				    'balance1' 		=> $balance1,
				    'promotion'		=> $bonus,
				    'incentive'		=> $incentive,
				    'turnover' 		=> $member.' / '.$turnover,
				    'profitloss' 	=> $winloss,
			    );
		    }
		}

		echo json_encode(array('total' => count($rows), 'rows' => $rows));
		exit;

	}

	public function showdailyReport(Request $request){

			$grid = new PanelGrid;
			$grid->setupGrid($this->moduleName, 'dailyReport-data', $this->limit, true, array('multiple' => true,'footer' => true));
			$grid->setTitle(Lang::get('COMMON.REPORT'));

			$grid->addColumn('date', 				Lang::get('COMMON.DATE'), 				120,		array('align' => 'left'));
			$grid->addColumn('currency', 			Lang::get('COMMON.CURRENCY'), 			58,			array('align' => 'center'));
			$grid->addColumn('register', 			Lang::get('COMMON.REGISTRATIONSHORT'), 	38,			array('align' => 'right'));
			$grid->addColumn('continue', 			Lang::get('COMMON.CONTDEPOSIT'), 		150,		array('align' => 'right'));
			$grid->addColumn('deposit', 			Lang::get('COMMON.DEPOSIT'), 			150,		array('align' => 'right'));
			$grid->addColumn('withdraw', 			Lang::get('COMMON.WITHDRAWAL'), 		150,		array('align' => 'right'));
			$grid->addColumn('balance1', 			Lang::get('COMMON.GROSSBALANCE'), 		90,		array('align' => 'right'));
			$grid->addColumn('promotion', 			Lang::get('COMMON.PROMOTION'), 			90,		array('align' => 'right'));
			$grid->addColumn('incentive', 			Lang::get('COMMON.INCENTIVE'), 			90,		array('align' => 'right'));
			$grid->addColumn('turnover', 			Lang::get('COMMON.TURNOVER'), 			110,		array('align' => 'right'));
			$grid->addColumn('profitloss', 			Lang::get('COMMON.PROFITLOSSSHORT'), 	90,		array('align' => 'right'));
			$grid->addColumn('credit', 				Lang::get('COMMON.CREDIT'), 			90,		array('align' => 'right'));
			$grid->addColumn('debit', 				Lang::get('COMMON.DEBIT'), 				90,		array('align' => 'right'));
			$grid->addColumn('balance2', 			Lang::get('COMMON.BALANCE'), 			90,		array('align' => 'right'));

			$grid->addFilter('crccode', Currency::getAllCurrencyAsOptions(), array('display' => Lang::get('COMMON.CURRENCY')));


			$data['grid'] = $grid->getTemplateVars();
			$data['paging'] = false;

			return view('admin.grid2',$data);
    }

	public function showdailyReportdoGetData(Request $request,$return = false, $link = true){

		$total = '1';
		$rows = array();
		$currency_total = array();

		$aqfld = $request->input('aqfld');

		$i = 1;
		if( $aqfld['crccode'] != '' )
			$currencies[$aqfld['crccode']] = $aqfld['crccode'];
		else
			$currencies = Currency::getAllCurrencyAsOptions();

		$temp  = explode('-',$request->createdfrom);
		$start = Carbon::create($temp[0], $temp[1], $temp[2], 0);
		$temp   = explode('-',$request->createdto);
		$end   = Carbon::create($temp[0], $temp[1], $temp[2], 0);

		foreach($currencies as $currency) {
				$currency_total[$currency]['deposit']          = 0;
				$currency_total[$currency]['continuedep']      = 0;
				$currency_total[$currency]['continuedepcount'] = 0;
				$currency_total[$currency]['withdrawal'] 	   = 0;
				$currency_total[$currency]['withdrawalcount']  = 0;
				$currency_total[$currency]['balance1']  	   = 0;
				$currency_total[$currency]['balance2']  	   = 0;
				$currency_total[$currency]['bonus']  	  	   = 0;
				$currency_total[$currency]['incentive']  	   = 0;
				$currency_total[$currency]['register']  	   = 0;
				$currency_total[$currency]['turnover']  	   = 0;
				$currency_total[$currency]['winloss']  	  	   = 0;
				$currency_total[$currency]['credit']  	  	   = 0;
				$currency_total[$currency]['debit']  	  	   = 0;
		}


		while( $start->toDateTimeString() <= $end->toDateTimeString() )
		{

			foreach($currencies as $currency)
			{

				$timefrom = $start->toDateString().' 00:00:00';
				$timeto   = $start->toDateString().' 23:59:59';



				$register = Account::whereRaw('crccode = "'.$currency.'" AND istestacc = 0 AND created >= "'.$timefrom.'" AND created <= "'.$timeto.'"')->count();

				$member  = Profitloss::whereRaw('crccode = "'.$currency.'" AND istest = 0 AND date = "'.$start->toDateString().'"')->count();

				$turnover = Profitloss::whereRaw('crccode = "'.$currency.'" AND istest = 0 AND date = "'.$start->toDateString().'"')->sum('totalstake');

				$winloss = Profitloss::whereRaw('crccode = "'.$currency.'" AND istest = 0 AND date = "'.$start->toDateString().'"')->sum('amount');

				$sql = 'crccode = "'.$currency.'" AND created >= "'.$timefrom.'" AND created <= "'.$timeto.'"';

				$condition1 = 'chtcode='.CBO_CHARTCODE_DEPOSIT.'    AND (status='.CBO_LEDGERSTATUS_CONFIRMED.' OR status='.CBO_LEDGERSTATUS_MANCONFIRMED.') AND '.$sql;
				$condition2 = 'chtcode='.CBO_CHARTCODE_WITHDRAWAL.' AND (status='.CBO_LEDGERSTATUS_CONFIRMED.' OR status='.CBO_LEDGERSTATUS_MANCONFIRMED.') AND '.$sql;
				$condition3 = 'chtcode='.CBO_CHARTCODE_BONUS.'      AND (status='.CBO_LEDGERSTATUS_CONFIRMED.' OR status='.CBO_LEDGERSTATUS_MANCONFIRMED.') AND '.$sql;
				$condition4 = 'chtcode='.CBO_CHARTCODE_INCENTIVE.'  AND (status='.CBO_LEDGERSTATUS_CONFIRMED.' OR status='.CBO_LEDGERSTATUS_MANCONFIRMED.') AND '.$sql;

				$deposit = $firstdep = $continuedep = $depositcount = $firstdepcount = $continuedepcount = 0;

				$continuedepmember = array();

				if($ledgers = Cashledger::whereRaw($condition1)->get()) {
					foreach($ledgers as $ledger) {
						$deposit += $ledger->amount;
						$depositcount += 1;
						if(Cashledger::whereRaw('accid = '.$ledger->accid.' AND chtcode='.CBO_CHARTCODE_DEPOSIT.' AND (status='.CBO_LEDGERSTATUS_CONFIRMED.' OR status='.CBO_LEDGERSTATUS_MANCONFIRMED.') AND created < "'.$ledger->created.'"')->get()) {
							$continuedepmember[$ledger->accid] = $ledger->accid;
							$continuedep += $ledger->amount;
							$continuedepcount += 1;
							$currency_total[$currency]['continuedepmember'][$ledger->accid] = $ledger->accid;
						}
					}
				}


				$deposit_member = Cashledger::whereRaw($condition1)->count();

				$withdrawal = Cashledger::whereRaw($condition2)->sum('amount');

				$withdrawalcount = Cashledger::whereRaw($condition2)->count();

				$withdrawal_member = Cashledger::whereRaw($condition2)->count();

				$balance1 = abs($deposit) - abs($withdrawal);

				$bonus = Cashledger::whereRaw($condition3)->sum('amount');

				$incentive = Cashledger::whereRaw($condition4)->sum('amount');

				$bonus = $bonus * -1;
				$incentive = $incentive * -1;

				$currency_total[$currency]['deposit'] += $deposit;

				$currency_total[$currency]['continuedep'] += $continuedep;

				$currency_total[$currency]['continuedepcount'] += $continuedepcount;

				$currency_total[$currency]['withdrawal'] += $withdrawal;

				$currency_total[$currency]['withdrawalcount'] += $withdrawalcount;

				$currency_total[$currency]['balance1'] += $balance1;

				$currency_total[$currency]['bonus'] += $bonus;

				$currency_total[$currency]['incentive'] += $incentive;

				$currency_total[$currency]['register'] += $register;

				$currency_total[$currency]['turnover'] += $turnover;

				$currency_total[$currency]['winloss'] += $winloss;

				$credit = Bankledger::whereRaw('crccode = "'.$currency.'" AND type = "credit" AND date = "'.$start->toDateString().'" AND status = 1')->sum('amount');
				$debit  = Bankledger::whereRaw('crccode = "'.$currency.'" AND type = "debit" AND date = "'.$start->toDateString().'" AND status = 1')->sum('amount');

				$balance2 = abs($credit) - abs($debit);

				$currency_total[$currency]['credit'] += $credit;

				$currency_total[$currency]['debit'] += $debit;

				$currency_total[$currency]['balance2'] += $balance2;

				if($link == true){
					$deposit = App::displayAmount($deposit);
					$continuedep = App::displayAmount($continuedep);
					$withdrawal = App::formatNegativeAmount($withdrawal);
					$balance1 = App::formatNegativeAmount($balance1);
					$bonus = App::formatNegativeAmount($bonus);
					$incentive = App::formatNegativeAmount($incentive);
					$turnover = App::displayAmount($turnover);
					$winloss = App::formatNegativeAmount($winloss);
					$credit = App::formatNegativeAmount($credit);
					$debit = App::formatNegativeAmount($debit);
					$balance2 = App::formatNegativeAmount($balance2);
				}
				else{
					$deposit = App::displayAmount($deposit);
					$continuedep = App::displayAmount($continuedep);
					$withdrawal = $withdrawal;
					$balance1 = $balance1;
					$bonus = $bonus;
					$incentive = $incentive;
					$turnover = App::displayAmount($turnover);
					$winloss = $winloss;
					$credit = App::displayNumberFormat($credit);
					$debit = App::displayNumberFormat($debit);
					$balance2 = App::displayNumberFormat($balance2);
				}

				$day = App::formatDateTime(31, $start->toDateString().' 00:00:00');
				$rows[] = array(
					'date' => substr($start, 0,10).' ('.$day.')',
					'currency' => $currency,
					'register' => $register,
					'continue' => count($continuedepmember).' / '.$continuedepcount.' / '.$continuedep,
					'deposit' => $deposit_member.' / '.$depositcount.' / '.$deposit,
					'withdraw' => $withdrawal_member.' / '.$withdrawalcount.' / '.$withdrawal,
					'balance1' => $balance1,
					'promotion' => $bonus,
					'incentive' => $incentive,
					'turnover' => $member.' / '.$turnover,
					'profitloss' => $winloss,
					'credit' => $credit,
					'debit' => $debit,
					'credit2' => $credit,
					'debit2' => $debit,
					'balance2' => $balance2,
				);



			}
			$start = $start->addDay();
		}

		foreach($currencies as $currency) {

			$totalmember = Profitloss::whereRaw('crccode = "'.$currency.'" AND istest = 0 AND date >= "'.$timefrom.'" AND date <= "'.$timeto.'"')->count();



			$deposit_member = 0;

			$withdrawal_member = 0;

			$footers[] = array(
				'date' => Lang::get('COMMON.TOTAL'),
				'currency' => $currency,
				'register' => $currency_total[$currency]['register'],

				'continue' => $currency_total[$currency]['continuedepcount'].' / '.App::displayAmount($currency_total[$currency]['continuedep']),
				'deposit' => $deposit_member.' / '.App::displayAmount($currency_total[$currency]['deposit']),
				'withdraw' => $withdrawal_member.' / '.$currency_total[$currency]['withdrawalcount'].' / '.App::displayAmount($currency_total[$currency]['withdrawal']),
				'balance1' => App::formatNegativeAmount($currency_total[$currency]['balance1']),
				'promotion' => App::displayAmount($currency_total[$currency]['bonus']),
				'incentive' => App::displayAmount($currency_total[$currency]['incentive']),
				'turnover' => $totalmember.' / '.App::displayAmount($currency_total[$currency]['turnover']),
				'profitloss' => App::formatNegativeAmount($currency_total[$currency]['winloss']),
				'credit' => App::formatNegativeAmount($currency_total[$currency]['credit']),
				'debit' => App::formatNegativeAmount($currency_total[$currency]['debit']),
				'balance2' => App::formatNegativeAmount($currency_total[$currency]['balance2']),
			);
		}

		echo json_encode(array('total' => count($rows), 'rows' => $rows, 'footer' => $footers));
		exit;
	}

	public function bonusSummaryShow(Request $request)
	{

		$grid = new PanelGrid;
		$grid->setupGrid($this->moduleName, 'bonusSummary-data', $this->limit, true);
		$grid->setTitle(Lang::get('COMMON.REPORT'));

		$level    = $request->has('level') ? $request->input('level'):Agent::LEVEL_SHAREHOLDER;
		$levelstr = Agent::getLevelText($level);

		$grid->addColumn('name', 				Lang::get('COMMON.NAME'), 					150,	array('align' => 'left'));
		$grid->addColumn('currency', 			Lang::get('COMMON.CURRENCY'), 				60,		array('align' => 'left'));
		$grid->addColumn('applied', 			Lang::get('COMMON.APPLIED'), 				80,		array('align' => 'left'));
		$grid->addColumn('approved', 			Lang::get('COMMON.APPROVED'), 				65,		array('align' => 'center'));
		$grid->addColumn('amount', 				Lang::get('COMMON.AMOUNT'), 				70,		array('align' => 'right'));
		$grid->addColumn('newmember', 			Lang::get('COMMON.NEWMEMBER'), 				100,	array('align' => 'right'));
		$grid->addColumn('oldmember', 			Lang::get('COMMON.OLDMEMBER'), 				100,	array('align' => 'right'));
		$grid->addColumn('deposit', 			Lang::get('COMMON.DEPOSIT'), 				100,	array('align' => 'right'));
		$grid->addColumn('withdrawal', 			Lang::get('COMMON.WITHDRAWAL'), 			100,	array('align' => 'right'));
		$grid->addColumn('turnover', 			Lang::get('COMMON.TURNOVER'), 				100,	array('align' => 'right'));
		$grid->addColumn('validbet', 			Lang::get('COMMON.VALIDSTAKE'), 			100,	array('align' => 'right'));
		$grid->addColumn('pnl', 				Lang::get('COMMON.PROFITLOSSSHORT'), 		100,	array('align' => 'right'));


		$data['grid'] = $grid->getTemplateVars();

		return view('admin.grid2',$data);

	}

	public function bonusSummarydoGetData(Request $request){

		$aflt  = $request->input('aflt');
		$aqfld = $request->input('aqfld');

		$condition1 = '1';

		if( isset($aqfld['status']) && $aqfld['status'] != 0 ){
			$condition1 = ' AND status'.'='.$aqfld['status'];
		}
		$totalrow = false;

		$total = array();
		$footers = array();

		$timefrom = $request->createdfrom.' 00:00:00';
		$timeto   = $request->createdto.' 23:59:59';

		if($promos = Promocampaign::whereRaw($condition1)->get()) {

		    foreach($promos as $promo) {

				$condition 		= 'chtcode = '.CBO_CHARTCODE_BONUS.' AND crccode="'.$promo->crccode.'" AND created>="'.$timefrom.'" AND created<="'.$timeto.'" AND (status ='.CBO_LEDGERSTATUS_CONFIRMED.' OR status='.CBO_LEDGERSTATUS_MANCONFIRMED.')';
				$apply 			= Promocash::whereRaw('pcpid='.$promo->id.' AND crccode="'.$promo->crccode.'" AND created >= "'.$timefrom.'" AND created <= "'.$timeto.'"')->count();
				$approved 		= 0;
				$amount 		= 0;
				$firstdep  	    = array();
				$continuedep    = array();
				$deposit = array();
				 if($ledgers = Cashledger::whereRaw($condition)->get()) {
					$accid = array();
					foreach($ledgers as $ledger) {
						$approved += 1;
						$amount += $ledger->amount;
						$deposit[$ledger->accid] = 1;
						$totaldeposit[$ledger->accid] = 1;

						if(Account::whereraw('id='.$ledger->accid.' AND totaldeposit > 1')->get()) {
							$continuedep[$ledger->accid] = 1;
							$totalcontinue[$ledger->accid] = 1;
						} else {
							$firstdep[$ledger->accid] = 1;
						}
						$accid[$ledger->accid] = $ledger->accid;
					}

					$deposit 		= Cashledger::whereRaw('crccode="'.$promo->crccode.'" AND created >= "'.$timefrom.'" AND created <= "'.$timeto.'" AND chtcode = '.CBO_CHARTCODE_DEPOSIT.' AND status IN ('.CBO_LEDGERSTATUS_CONFIRMED.', '.CBO_LEDGERSTATUS_MANCONFIRMED.')')->sum('amount');
					$withdrawal	    = Cashledger::whereRaw('crccode="'.$promo->crccode.'" AND created >= "'.$timefrom.'" AND created <= "'.$timeto.'" AND chtcode = '.CBO_CHARTCODE_WITHDRAWAL.' AND status IN ('.CBO_LEDGERSTATUS_CONFIRMED.', '.CBO_LEDGERSTATUS_MANCONFIRMED.')')->sum('amount');
					$bonus		    = Cashledger::whereRaw('crccode="'.$promo->crccode.'" AND created >= "'.$timefrom.'" AND created <= "'.$timeto.'" AND chtcode = '.CBO_CHARTCODE_BONUS.' AND status IN ('.CBO_LEDGERSTATUS_CONFIRMED.', '.CBO_LEDGERSTATUS_MANCONFIRMED.')')->sum('amount');
					$incentive 	    = Cashledger::whereRaw('crccode="'.$promo->crccode.'" AND created >= "'.$timefrom.'" AND created <= "'.$timeto.'" AND chtcode = '.CBO_CHARTCODE_INCENTIVE.' AND status IN ('.CBO_LEDGERSTATUS_CONFIRMED.', '.CBO_LEDGERSTATUS_MANCONFIRMED.')')->sum('amount');
					$adjustment 	= Cashledger::whereRaw('crccode="'.$promo->crccode.'" AND created >= "'.$timefrom.'" AND created <= "'.$timeto.'" AND chtcode = '.CBO_CHARTCODE_ADJUSTMENT.' AND status IN ('.CBO_LEDGERSTATUS_CONFIRMED.', '.CBO_LEDGERSTATUS_MANCONFIRMED.')')->sum('amount');
					$totalstake	    = Profitloss::whereRaw('crccode="'.$promo->crccode.'" AND date >= "'.$request->createdfrom.'" AND date <= "'.$request->createdto.'"')->sum('totalstake');
					$validstake 	= Profitloss::whereRaw('crccode="'.$promo->crccode.'" AND date >= "'.$request->createdfrom.'" AND date <= "'.$request->createdto.'"')->sum('validstake');
					$winloss 	    = Profitloss::whereRaw('crccode="'.$promo->crccode.'" AND date >= "'.$request->createdfrom.'" AND date <= "'.$request->createdto.'"')->sum('amount');

					$newmember = count($firstdep);
					$oldmember = count($continuedep);

					$body = App::formatAddTabUrl( Lang::get('REPORT').': '.$promo->id, urlencode($promo->name), action('Admin\ReportController@bonusSummarydrawAddEdit',  array('sid' => $promo->id)));

					$row[] = array(
						'name' 			=> urldecode($body),
						'currency' 		=> $promo->crccode,
						'applied' 		=> ($apply?$apply:'0'),
						'approved' 		=> ($approved?$approved:'0'),
						'amount' 		=> App::displayAmount($amount),
						'newmember'	    => ($newmember?$newmember:'0'),
						'oldmember' 	=> ($oldmember?$oldmember:'0'),
						'deposit' 		=> App::displayAmount($deposit),
						'withdrawal'    => App::formatNegativeAmount($withdrawal),
						'turnover'	    => App::displayAmount($totalstake),
						'validbet' 	    => App::displayAmount($validstake),
						'pnl' 			=> App::formatNegativeAmount($winloss),
					);
					/*
					if(isset($total[$promo->crccode]['applied'])) $total[$promo->crccode]['applied'] += $apply;
					else  $total[$promo->crccode]['applied'] = $apply;

					if(isset($total[$promo->crccode]['approved'])) $total[$promo->crccode]['approved'] += $approved;
					else  $total[$promo->crccode]['approved'] = $approved;

					if(isset($total[$promo->crccode]['amount'])) $total[$promo->crccode]['amount'] += $amount;
					else  $total[$promo->crccode]['amount'] = $amount;

					if(isset($total[$promo->crccode]['newmember'])) $total[$promo->crccode]['newmember'] += $newmember;
					else  $total[$promo->crccode]['newmember'] = $newmember;

					if(isset($total[$promo->crccode]['oldmember'])) $total[$promo->crccode]['oldmember'] += $oldmember;
					else  $total[$promo->crccode]['oldmember'] = $oldmember;
					 */
				}
		    }
		}

		foreach($total as $key => $value){
		    $footers[] = array(
			//'name' => App::getText('CBO_GUI_TOTAL'),
			//'currency' => $key,
			//'applied' => $value['applied'],
			//'approved' => $value['approved'],
			//'amount' => App::displayAmount($value['amount']),
			//'newmember' => $value['newmember'],
			//'oldmember' => $value['oldmember'],
		    );
		}

		    echo json_encode(array('total' => count($row),'rows' => $row, 'footer' => $footers));
		    exit;


	}

    public function bonusReportShow(Request $request)
    {

        $grid = new PanelGrid;
        $grid->setupGrid($this->moduleName, 'bonusReport-data', $this->limit, true, array('footer' => true));
        $grid->setTitle(Lang::get('COMMON.REPORT'));

        $level    = $request->has('level') ? $request->input('level'):Agent::LEVEL_SHAREHOLDER;
        $levelstr = Agent::getLevelText($level);

        $grid->addColumn('refid', 				Lang::get('COMMON.CTABNO'), 				80,	array('align' => 'left'));
        $grid->addColumn('date', 			    Lang::get('COMMON.DATE'), 				    120,		array('align' => 'center'));
        $grid->addColumn('csname', 			    Lang::get('public.CSName'), 				180,		array('align' => 'center'));
        $grid->addColumn('memberid', 			Lang::get('COMMON.MEMBERID'), 				120,	array('align' => 'left'));
        $grid->addColumn('membername', 			Lang::get('public.MemberName'), 			180,	array('align' => 'center'));
        $grid->addColumn('bonusname', 			Lang::get('public.BonusName'), 				220,	array('align' => 'center'));
        $grid->addColumn('currency', 			Lang::get('COMMON.CURRENCY'), 				60,		array('align' => 'center'));
        $grid->addColumn('amount', 				Lang::get('COMMON.AMOUNT'), 				120,		array('align' => 'right'));

        $grid->addFilter('ipcpid', Promocampaign::getAllAsOptions(false,true,Session::get('currency')), array('display' => Lang::get('COMMON.TYPE')));
        $data['grid'] = $grid->getTemplateVars();

        return view('admin.grid2',$data);

    }

    public function bonusReportdoGetData(Request $request){

        $from  = $request->has('createdfrom')?$request->input('createdfrom'):App::getDateTime(11);
        $to    = $request->has('createdto')?$request->input('createdto'):App::getDateTime(3);
        $aflt  = $request->input('aflt');
        $aqfld = $request->input('aqfld');

        if(!empty($aqfld['ipcpid'])){
            $builder = DB::table('cashledger as clg')
            ->join('user as usr', 'usr.id', '=', 'clg.modifiedby')
            ->join('promocash as pch', 'pch.id', '=' , 'clg.refid')
            ->where('pch.pcpid', '=' , $aqfld['ipcpid'])
            ->join('promocampaign as pcp', 'pcp.id', '=', 'pch.pcpid')
            ->where('clg.chtcode', '=', CBO_BETSLIPTERM_PROMOCASH)
            ->where('clg.refobj', '=', 'PromoCash')
            ->whereIn('clg.status', array(CBO_LEDGERSTATUS_CONFIRMED, CBO_LEDGERSTATUS_MANCONFIRMED))
            ->where('clg.created', '>=', $from . ' 00:00:00')
            ->where('clg.created', '<=', $to . '23:59:59')
            ->select(array('clg.id as refid', 'clg.created', 'usr.username as csname', 'clg.accid', 'clg.accname', 'pcp.name as bonusname', 'clg.crccode', 'clg.amount'));
        }else{
            $builder = DB::table('cashledger as clg')
            ->join('user as usr', 'usr.id', '=', 'clg.modifiedby')
            ->join('promocash as pch', 'pch.id', '=' , 'clg.refid')
            ->join('promocampaign as pcp', 'pcp.id', '=', 'pch.pcpid')
            ->where('clg.chtcode', '=', CBO_BETSLIPTERM_PROMOCASH)
            ->where('clg.refobj', '=', 'PromoCash')
            ->whereIn('clg.status', array(CBO_LEDGERSTATUS_CONFIRMED, CBO_LEDGERSTATUS_MANCONFIRMED))
            ->where('clg.created', '>=', $from . ' 00:00:00')
            ->where('clg.created', '<=', $to . '23:59:59')
            ->select(array('clg.id as refid', 'clg.created', 'usr.username as csname', 'clg.accid', 'clg.accname', 'pcp.name as bonusname', 'clg.crccode', 'clg.amount'));
        }

//        $total = $builder->count(array('clg.id'));
        $records = $builder->skip($request->recordstartindex)->take($request->pagesize)->orderBy('clg.id', 'desc')->get();
        $rows = array();
        $footerData = array();
        $footers = array();

        if(count($records) > 0) {

            foreach ($records as $r) {

                if (array_key_exists($r->crccode, $footerData)) {
                    $footerData[$r->crccode] = $footerData[$r->crccode] + $r->amount;
                } else {
                    $footerData[$r->crccode] = $r->amount;
                }

                $rows[] = array(
                    'refid' => $r->refid,
                    'date' => $r->created,
                    'csname' => $r->csname,
                    'memberid' => $r->accid,
                    'membername' => $r->accname,
                    'bonusname' => $r->bonusname,
                    'currency' => $r->crccode,
                    'amount' => App::formatNegativeAmount($r->amount),
                );
            }
        }

        if (count($footerData) > 0) {
            $currencies = array();
            $amounts = array();

            foreach ($footerData as $key => $val) {
                $currencies[] = $key;
                $amounts[] = App::formatNegativeAmount($val);
            }

            $footers[] = array(
                'refid' => Lang::get('COMMON.TOTAL'),
                'date' => '',
                'csname' => '',
                'memberid' => '',
                'membername' => '',
                'bonusname' => '',
                'currency' => implode('<br>', $currencies),
                'amount' => implode('<br>', $amounts),
            );
        }

        echo json_encode(array('total' => count($rows),'rows' => $rows, 'footer' => $footers));
        exit;


    }

	public function bonusSummarydrawAddEdit(Request $request){


	}

	public function showDepositReport(Request $request)
	{

		$grid = new PanelGrid;
		$grid->setupGrid($this->moduleName, 'depositReport-data', $this->limit, true);
		$grid->setTitle(Lang::get('COMMON.REPORT'));

		$level    = $request->has('level') ? $request->input('level'):Agent::LEVEL_SHAREHOLDER;
		$levelstr = Agent::getLevelText($level);

		$grid->addColumn('month', 				Lang::get('COMMON.MONTH'), 						150,		array('align' => 'left'));
		$grid->addColumn('register', 			Lang::get('COMMON.REGISTRATION'), 				150,		array('align' => 'left'));
		$grid->addColumn('first', 				Lang::get('COMMON.FIRSTDEPOSIT'), 				150,		array('align' => 'left'));
		$grid->addColumn('bonus', 				Lang::get('COMMON.BONUS'), 						150,		array('align' => 'center'));
		$grid->addColumn('continuemonth', 		Lang::get('COMMON.CONTDEPOSITMONTH'), 			150,		array('align' => 'right'));
		$grid->addColumn('continue', 			Lang::get('COMMON.CONTDEPOSIT'), 				100,		array('align' => 'right'));


		$data['grid'] = $grid->getTemplateVars();

		return view('admin.grid2',$data);

	}

	public function DepositReportdoGetData(Request $request){

		 $pcpid = array();
		if($campaigns = Promocampaign::whereRaw('method='.PromoCampaign::CBO_METHOD_FIRSTTIMEDEPOSIT)->get()) {
			foreach($campaigns as $campaign) {
				$pcpid[] = $campaign->id;
			}
		}

		 $start = '2016-01-01';
		$datefrom = $start.' 12:00:00';
		$end = App::getDateTime(3).' 12:00:00';
//var_dump($end);exit;
		$timefrom = $request->aflt['smonth'].' 12:00:00';
		$timeto = App::spanDateTime(12 * 3600, App::formatDateTime(23, $request->aflt['smonth'].' 12:00:00'));

		$total = array();
		$rows = array();

		while($datefrom <= $end) {
			$dateto = App::spanDateTime(12 * 3600, App::formatDateTime(23, $datefrom));
			$condition = 'istestacc = 0 AND isinternal = 0 AND created >="'.$request->datefrom.'" AND created <= "'.$request->dateto.'"';
			$member = Account::whereRaw($condition)->count();
			$deposit = 0;
			$bonus = 0;
			$deposit_2 = 0;
			$deposit_3 = 0;

			$condition2 = 'chtcode='.CBO_CHARTCODE_DEPOSIT.' AND (status='.CBO_LEDGERSTATUS_CONFIRMED.' OR status='.CBO_LEDGERSTATUS_MANCONFIRMED.') AND created >="'.$request->timefrom.'" AND created <= "'.$timeto.'"';
			$condition3 = 'pcpid IN ('.implode(',', $pcpid).')';

			$ids = array();
			if($promos = Promocash::whereRaw($condition3)->get()) {
				foreach($promos as $promo)
				$ids[] = $promo->id;
			}

			if($ledgers = Cashledger::whereRaw($condition2)->get()) {
				foreach($ledgers as $ledger) {
					if(!Cashledger::whereRaw('chtcode='.$ledger->chtcode.' AND accid='.$ledger->accid.' AND (status='.CBO_LEDGERSTATUS_CONFIRMED.' OR status='.CBO_LEDGERSTATUS_MANCONFIRMED.') AND created <= "'.$timefrom.'"')->get()) {
						$deposit += 1;
						$count = Cashledger::whereRaw('chtcode='.$ledger->chtcode.' AND accid='.$ledger->accid.' AND (status='.CBO_LEDGERSTATUS_CONFIRMED.' OR status='.CBO_LEDGERSTATUS_MANCONFIRMED.') AND created >="'.$timefrom.'" AND created <= "'.$timeto.'"')->count();
						if($count > 1)
						{
							$deposit_2 += 1;
						}
					} else
					{$deposit_3 += 1;}
				}
			}

			if(count($ids) > 0)
			{
				$bonus = Cashledger::whereRaw('chtcode='.CBO_CHARTCODE_BONUS.' AND (status='.CBO_LEDGERSTATUS_CONFIRMED.' OR status='.CBO_LEDGERSTATUS_MANCONFIRMED.') AND refid IN ('.implode(',', $ids).') AND created >="'.$timefrom.'" AND created <= "'.$timeto.'"')->count();
			}
			else{ $bonus = 0;}

			$depositurl  = $deposit;
			$deposit2url = $deposit_2;
			$deposit3url = $deposit_3;

			$rows[] = array(
				'month'			=> App::formatDateTime(9, $datefrom),
				'register' 		=> (($member)?$member:'0'),
				'first' 		=> $deposit,
				'bonus' 		=> (($bonus)?$bonus:'0'),
				'continuemonth' => (($deposit_2)?$deposit2url:'0'),
				'continue' 		=> (($deposit_3)?$deposit3url:'0'),
			);

			$datefrom = App::spanDateTime(1, $dateto);

		}


		echo json_encode(array('total' => $total, 'rows' => $rows));
		exit;

	}

	public function showMainReport2(Request $request)
	{



		$grid = new PanelGrid;
		if( $request->has('sid') )
		{
			$grid->setupGrid($this->moduleName, 'profitloss2-data', $this->limit, true, array('multiple' => true,'footer' => true,'params' => array('ilevel' => 5 )));
		}
		elseif( $request->has('accid') ){
			$grid->setupGrid($this->moduleName, 'profitloss2-data', $this->limit, true, array('multiple' => true,'footer' => true,'params' => array('accid' => $request->accid,'createdfrom' => $request->createdfrom , 'createdto' => $request->createdto)));
		}
		elseif( $request->has('aflid') ){
			$grid->setupGrid($this->moduleName, 'profitloss2-data', $this->limit, true, array('multiple' => true,'footer' => true,'params' => array('aflid' => $request->aflid,'createdfrom' => $request->createdfrom , 'createdto' => $request->createdto)));
		}else{
			$grid->setupGrid($this->moduleName, 'profitloss2-data', $this->limit, true, array('multiple' => true,'footer' => true,'params' => array('createdfrom' => $request->createdfrom , 'createdto' => $request->createdto)));
		}
		$grid->setTitle(Lang::get('COMMON.REPORT'));

		$level    = $request->has('level') ? $request->input('level'):Agent::LEVEL_SHAREHOLDER;
		$levelstr = Agent::getLevelText($level);

		$grid->addColumn('member', 			'', 				150,	array('align' => 'left'));
		//$grid->addColumn('vendor', 			Lang::get('COMMON.VENDOR'), 				60,		array('align' => 'left'));
		//$grid->addColumn('product', 		Lang::get('COMMON.PRODUCT'), 				80,		array('align' => 'left'));
		$grid->addColumn('currency', 		Lang::get('COMMON.CURRENCY'), 				65,		array('align' => 'center'));
		$grid->addColumn('wager', 			Lang::get('COMMON.WAGERNUM'), 				70,		array('align' => 'right'));
		$grid->addColumn('stake', 			Lang::get('COMMON.STAKE'), 					130,	array('align' => 'right'));
		$grid->addColumn('validstake', 		Lang::get('COMMON.VALIDSTAKE'), 			130,	array('align' => 'right'));
		$grid->addColumn('winloss', 		Lang::get('COMMON.MEMBERPNL'), 				130,	array('align' => 'right'));
		$grid->addColumn('company', 		Lang::get('COMMON.COMPANY'), 				130,	array('align' => 'right'));
		$grid->addColumn('ratio1', 			Lang::get('COMMON.MARKETSHARE'), 			80,		array('align' => 'right'));
		$grid->addColumn('ratio2', 			Lang::get('COMMON.MARGINPERCENTAGE'), 		80,		array('align' => 'right'));

		$types = array(
			1	=> Lang::get('COMMON.REPORTALL'),
			2	=> Lang::get('COMMON.REPORTVENDOR'),
			3	=> Lang::get('COMMON.REPORTCATEGORY'),
		);

		$productOptions = Product::getAllAsOptions();

		$grid->addFilter('iprdid', Product::getAllAsOptions(), array('display' => Lang::get('COMMON.VENDOR')));
		$grid->addFilter('crccode', Currency::getAllCurrencyAsOptions(), array('display' => Lang::get('COMMON.CURRENCY')));
		$grid->addFilter('icategory', Product::getCategoryOptions(), array('display' => Lang::get('COMMON.CATEGORY')));

		//$grid->addButton('2', 'Agent' 	 , '', 'button', array('icon' => 'ok', 'url' => 'doSearchByButton(\'Agent\');'));
		$grid->addButton('2', Lang::get('COMMON.MEMBER') 	 , '', 'button', array('icon' => 'ok', 'url' => 'doSearchByButton(\'Member\');'));
		$grid->addButton('2', Lang::get('COMMON.COMPANY')    , '', 'button', array('icon' => 'ok', 'url' => 'doSearchByButton(\'Company\');'));
		$grid->addButton('2', Lang::get('COMMON.GAMETYPE')   , '', 'button', array('icon' => 'ok', 'url' => 'doSearchByButton(\'GameType\');'));
		$grid->addButton('2', Lang::get('COMMON.DATE') 	 	 , '', 'button', array('icon' => 'ok', 'url' => 'doSearchByButton(\'Date\');'));
		$grid->addButton('2', Lang::get('COMMON.EXPORT')     , '', 'button', array('icon' => 'ok', 'url' => 'export_excel();'));

		$data['grid'] = $grid->getTemplateVars();
		$data['paging'] = false;

		return view('admin.grid2',$data);

	}

	public function doGetData2(Request $request){

		$condition = 'istest = 0 AND date >= "'.$request->createdfrom.'" AND date <= "'.$request->createdto.'"';

		if( $request->has('aflid') )
		{
			$aff = new Affiliate;
			$downlines = $aff->getAllDownlineById2($request->aflid, true);

			if(is_array($downlines)){
						foreach($downlines as $downline) {
							if(isset($downline->id))
                                $aff_ids[] = $downline->id;
                        }
			}
			$aff_ids[] = $request->aflid;

			$condition .= ' AND level4 IN (select id from agent where aflid IN ('.implode($aff_ids,',').') ) ';
		}

		if( $request->has('accid') )
		{
			$condition .= ' AND accid IN ('.$request->accid.') ';
		}

		if( $request->aqfld['crccode'] == '' ){
			$condition .= ' AND crccode = "'.Session::get('admin_crccode').'" ';
		}else{
			$condition .= ' AND crccode = "'.$request->aqfld['crccode'].'" ';
		}

		if( $request->aqfld['iprdid'] != '' )
		{
			$condition .= ' AND prdid = "'.$request->aqfld['iprdid'].'" ';
		}

		if( $request->aqfld['icategory'] != '' )
		{
			$condition .= ' AND category = "'.$request->aqfld['icategory'].'" ';
		}

		$rows = array();
		$footer = array();
		$total_report = array();
		$total_report['wager'] 		= 0;
		$total_report['validstake'] = 0;
		$total_report['stake']	    = 0;
		$total_report['winloss']	= 0;

		$group_by = 'prdid';
		$order_by = 'prdid';
		$sort_by  = 'ASC';

		if( $request->btn_click == 'Member' )
		{
			$group_by = 'nickname';
			$order_by = 'sumwinloss';
		}
		elseif( $request->btn_click == 'Company' )
		{
			$group_by = 'prdid';
			$order_by = 'prdid';
		}
		elseif( $request->btn_click == 'GameType' )
		{
			$group_by = 'category';
			$order_by = 'category';
		}
		elseif( $request->btn_click == 'Date' )
		{
			$group_by = 'date';
			$order_by = 'date';
		}

		if( $request->has('sortdatafield') ){

			if( $request->sortdatafield == 'member' )
			{
				$order_by = 'nickname';
			}
			if( $request->sortdatafield == 'winloss' )
			{
				$order_by = 'sumwinloss';
			}
			$sort_by  = $request->sortorder;
		}


		$profitlosses = DB::select( DB::raw("SELECT level1, level2, level3, level4, accid, date, acccode, nickname, prdid, category, crccode, SUM(wager) as sumwager, SUM(totalstake) as sumstake, SUM(amount) as sumwinloss, SUM(validstake) as sumvalid
										FROM `profitloss`
										WHERE ".$condition."
										GROUP BY ".$group_by."
										ORDER BY ".$order_by." ".$sort_by."
										"));


		$categories = Product::getCategoryOptions();
		$products   = Product::getAllAsOptions(true);



		foreach($profitlosses as $profitloss) {

			$profitloss->sumstake   = $profitloss->crccode == 'VND' || $profitloss->crccode == 'IDR' ?  $profitloss->sumstake * 1000 :  $profitloss->sumstake;
			$profitloss->sumvalid   = $profitloss->crccode == 'VND' || $profitloss->crccode == 'IDR' ?  $profitloss->sumvalid * 1000 :  $profitloss->sumvalid;
			$profitloss->sumwinloss = $profitloss->crccode == 'VND' || $profitloss->crccode == 'IDR' ?  $profitloss->sumwinloss * 1000 :  $profitloss->sumwinloss;


			$percentage = 0;
				if($profitloss->sumstake > 0) $percentage = $profitloss->sumwinloss / $profitloss->sumstake * 100;
				$percentage *= -1;

				if( $request->btn_click == 'Member' )
				{
					$type = '<a href="#" onclick="parent.addTab(\'Member: '.$profitloss->nickname.'\', \'customer-tab?sid='.$profitloss->accid.'\')">'.$profitloss->nickname.'</a>';


				}
				elseif( $request->btn_click == 'Company' )
				{
					$type = $products[$profitloss->prdid];
				}
				elseif( $request->btn_click == 'GameType' )
				{
					$type = $categories[$profitloss->category];
				}
				elseif( $request->btn_click == 'Date' )
				{
					$type = $profitloss->date;
				}else{
					$type = $products[$profitloss->prdid];
				}
				$detailParams['iprdid'] 	 = 0;
				$detailParams['iaccid'] 	 = $profitloss->accid;
				$detailParams['createdfrom'] = $request->createdfrom;
				$detailParams['createdto']   = $request->createdto;
				$rows[] = array(
					'currency' 		=> $profitloss->crccode,
					'member' 		=> $type,
					'vendor' 		=> $products[$profitloss->prdid],
					'product' 		=> $categories[$profitloss->category],
					'wager' 		=> App::displayAmount($profitloss->sumwager, 0),
					'stake'  		=> App::formatNegativeAmount($profitloss->sumstake),
					'stake_amount'  => $profitloss->sumstake,
					'validstake' 	=> App::formatNegativeAmount($profitloss->sumvalid),
					'winloss' 		=> App::formatAddTabUrl(Lang::get('COMMON.BETDETAILS').': '. $profitloss->acccode, App::formatNegativeAmount($profitloss->sumwinloss), action('Admin\ReportController@showdetail',$detailParams)),
					'company' 		=> App::formatNegativeAmount($profitloss->sumwinloss * -1),
					'ratio2' 		=> App::displayPercentage($percentage, 2, true),
				);

            $total_report['wager'] 		  += $profitloss->sumwager;
            $total_report['validstake']   += $profitloss->sumvalid;
            $total_report['stake']	      += $profitloss->sumstake;
            $total_report['winloss']	  += $profitloss->sumwinloss;


        }
		foreach($rows as $key => $row) {
				$ratio1 = 0;
				if($total_report['stake'] > 0)
				$ratio1 = $row['stake_amount'] / $total_report['stake'] * 100;
				$rows[$key]['ratio1'] = App::displayPercentage($ratio1);
		}

		$percentage = 0;
		if($total_report['stake'] > 0) $percentage = $total_report['winloss'] / $total_report['stake'] * 100;
		$percentage *= -1;
		$total_report['wager'] 		= App::displayAmount($total_report['wager'],0);
		$total_report['stake'] 		= App::formatNegativeAmount($total_report['stake']);
		$total_report['validstake'] = App::formatNegativeAmount($total_report['validstake']);
		$total_report['company'] 	= App::formatNegativeAmount($total_report['winloss']*-1);
		$total_report['winloss'] 	= App::formatNegativeAmount($total_report['winloss']);
		$total_report['ratio2'] 	= App::displayPercentage($percentage);
		$total_report['ratio1'] 	= App::displayPercentage(100);




		$footer[] = $total_report;

		echo json_encode(array('total' => count($rows), 'rows' => $rows, 'footer' => $footer));
		exit;
	}

	public function showReferralReport(Request $request) {

        $grid = new PanelGrid;
        $grid->setupGrid($this->moduleName, 'referralReport-data', $this->limit, true, array('multiple' => true,'footer' => true ));
        $grid->setTitle(Lang::get('COMMON.REPORT'));

//        $level    = $request->has('level') ? $request->input('level'):Agent::LEVEL_SHAREHOLDER;
//        $levelstr = Agent::getLevelText($level);

        $grid->addColumn('accountdate', 		Lang::get('COMMON.DATE'), 						150,		array('align' => 'left'));
        $grid->addColumn('referralnickname',	Lang::get('COMMON.REFERRAL'), 					150,		array('align' => 'left'));
        $grid->addColumn('referralcrccode',	Lang::get('COMMON.CURRENCY'), 					150,		array('align' => 'left'));
        $grid->addColumn('amount', 			Lang::get('COMMON.REFERRALBONUS'), 				150,		array('align' => 'center'));
        $grid->addColumn('nickname', 			Lang::get('COMMON.DOWNLINE'), 					150,		array('align' => 'left'));
        $grid->addColumn('prdid', 				Lang::get('COMMON.PRODUCT'), 					150,		array('align' => 'left'));
        $grid->addColumn('category', 			Lang::get('COMMON.PRODUCTCATEGORY'), 			100,		array('align' => 'left'));


        $data['grid'] = $grid->getTemplateVars();

        return view('admin.grid2',$data);
    }

    public function ReferraldoGetData(Request $request) {

        $from  = $request->has('createdfrom') ? $request->input('createdfrom') : App::getDateTime(11);
        $to    = $request->has('createdto') ? $request->input('createdto') : App::getDateTime(3);
        $aflt  = $request->input('aflt');
        $aqfld = $request->input('aqfld');

        $builder = DB::table('incentivereferral as irf')
            ->join('product as prd', 'prd.id', '=', 'irf.prdid')
            ->whereBetween('irf.accountdate', array($from, $to))
            ->orderBy('irt.accountdate', 'desc')
            ->orderBy('irt.referralnickname', 'asc')
            ->orderBy('prd.name', 'asc')
            ->select(array('irf.accountdate', 'irf.referralnickname', 'irf.referralcrccode', 'irf.amount', 'irf.nickname', 'prd.name as prdid', 'irf.category'));

        $records = $builder->skip($request->recordstartindex)
            ->take($request->pagesize)
            ->get();

        $rows = array();
        $footerData = array();
        $footers = array();

        if(count($records) > 0) {

            $categoryOptions = collect(Product::getCategoryOptions());

            foreach ($records as $r) {

                if (array_key_exists($r->referralcrccode, $footerData)) {
                    $footerData[$r->referralcrccode] = $footerData[$r->referralcrccode] + $r->amount;
                } else {
                    $footerData[$r->referralcrccode] = $r->amount;
                }

                $rows[] = array(
                    'accountdate' => $r->accountdate,
                    'referralnickname' => $r->referralnickname,
                    'referralcrccode' => $r->referralcrccode,
                    'amount' => App::formatNegativeAmount($r->amount),
                    'nickname' => $r->nickname,
                    'prdid' => $r->prdid,
                    'category' => $categoryOptions->get($r->category),
                );
            }
        }

        if (count($footerData) > 0) {
            $currencies = array();
            $amounts = array();

            foreach ($footerData as $key => $val) {
                $currencies[] = $key;
                $amounts[] = App::formatNegativeAmount($val);
            }

            $footers[] = array(
                'accountdate' => Lang::get('COMMON.TOTAL'),
                'referralnickname' => '',
                'referralcrccode' => implode('<br>', $currencies),
                'amount' => implode('<br>', $amounts),
                'nickname' => '',
                'prdid' => '',
                'category' => '',
            );
        }

        echo json_encode(array('total' => count($rows),'rows' => $rows, 'footer' => $footers));
        exit;
    }

    public function showSmsLogReport(Request $request) {

        $grid = new PanelGrid;
        $grid->setupGrid($this->moduleName, 'smsLogReport-data', $this->limit, true, array('multiple' => true,'footer' => true ));
        $grid->setTitle(Lang::get('COMMON.REPORT'));

        $grid->addColumn('created_at', 		Lang::get('COMMON.DATE'), 						150,		array('align' => 'left'));
        $grid->addColumn('sender_ip',			Lang::get('COMMON.IPADDRESS'), 					100,		array('align' => 'left'));
        $grid->addColumn('provider',			Lang::get('COMMON.PROVIDER'), 					80,		array('align' => 'left'));
        $grid->addColumn('method', 			Lang::get('COMMON.METHOD'), 					150,		array('align' => 'left'));
        $grid->addColumn('tel_mobile', 		Lang::get('COMMON.TELEPHONENO'), 				150,		array('align' => 'left'));
        $grid->addColumn('request', 			Lang::get('COMMON.REQUEST'), 					300,		array('align' => 'left'));
        $grid->addColumn('response', 			Lang::get('COMMON.RESPOND'), 					100,		array('align' => 'left'));
        $grid->addColumn('ref_code', 			Lang::get('COMMON.REFCODE'), 					100,		array('align' => 'left'));
        $grid->addColumn('ref_status', 		Lang::get('COMMON.STATUS'), 					80,		array('align' => 'right'));

        $grid->addSearchField('search', array('tel_mobile' => Lang::get('COMMON.TELEPHONENO')));

        $grid->addFilter('provider', Smslog::getProviderOptions(), array('display' => Lang::get('COMMON.PROVIDER')));
        $grid->addFilter('method', Smslog::getMethodOptions(), array('display' => Lang::get('COMMON.METHOD')));

        $data['grid'] = $grid->getTemplateVars();

        return view('admin.grid2',$data);
    }

    public function SmsLogdoGetData(Request $request) {

        $from  = $request->has('createdfrom') ? $request->input('createdfrom') : App::getDateTime(11);
        $to    = $request->has('createdto') ? $request->input('createdto') : App::getDateTime(3);
        $aflt  = $request->input('aflt');
        $aqfld = $request->input('aqfld');

        $builder = DB::table('smslog')
            ->whereBetween('created_at', array($from . ' 00:00:00', $to . ' 23:59:59'))
            ->orderBy('created_at', 'desc')
            ->select(array('created_at', 'sender_ip', 'provider', 'method', 'tel_mobile', 'request', 'response', 'ref_code', 'ref_status'));

        if ( isset($aflt['tel_mobile']) && $aflt['tel_mobile'] != '' ) {
            $builder->where('tel_mobile', 'LIKE', '%' . $aflt['tel_mobile'] . '%');
        }

        if( isset($aqfld['provider']) && $aqfld['provider'] != '' )
        {
            $builder->where('provider', '=', $aqfld['provider']);
        }

        if( isset($aqfld['method']) && $aqfld['method'] != '' )
        {
            $builder->where('method', '=', $aqfld['method']);
        }

        $records = $builder->skip($request->recordstartindex)
            ->take($request->pagesize)
            ->get();

        $rows = array();
        $footerData = array();
        $footers = array();

        if(count($records) > 0) {

            $providerOptions = collect(Smslog::getProviderOptions());
            $methodOptions = collect(Smslog::getMethodOptions());

            foreach ($records as $r) {
                $rows[] = array(
                    'created_at' => $r->created_at,
                    'sender_ip' => $r->sender_ip,
                    'provider' => $providerOptions->get($r->provider),
                    'method' => $methodOptions->get($r->method),
                    'tel_mobile' => $r->tel_mobile,
                    'request' => $r->request,
                    'response' => $r->response,
                    'ref_code' => $r->ref_code,
                    'ref_status' => $r->ref_status,
                );
            }
        }

        $footers[] = array(
            'created_at' => '',
            'sender_ip' => '',
            'provider' => '',
            'method' => '',
            'tel_mobile' => '',
            'request' => '',
            'response' => Lang::get('COMMON.CREDITBALANCE'),
            'ref_code' => 'SMS88<br>SMS Mo Ding',
            'ref_status' => '<a href="javascript:void(0);" title="Click to refresh" onclick="autoloadBinding(this);" data-href="'. action('Admin\ReportController@SmsLogdoCheckBalance', array('p' => SMS88::NAME)) .'" data-autoload="1">-.--</a>' .
                            '<br>' .
                            '<a href="javascript:void(0);" title="Click to refresh" onclick="autoloadBinding(this);" data-href="'. action('Admin\ReportController@SmsLogdoCheckBalance', array('p' => SMSMoDing::NAME)) .'" data-autoload="1">-.--</a>',
        );

        echo json_encode(array('total' => count($rows),'rows' => $rows, 'footer' => $footers));
        exit;
    }

    public function SmsLogdoCheckBalance(Request $request) {

	    $p = $request->input('p');
	    $res = 0;

	    switch ($p) {
            case SMS88::NAME:
                $smsObj = new SMS88();
                $res = $smsObj->creditBalance();
                break;
            case SMSMoDing::NAME:
                $smsObj = new SMSMoDing();
                $res = $smsObj->creditBalance();
                break;
        }

        if ($res === false) {
            echo 'N/A';
        } elseif (is_numeric($res)) {
            echo number_format($res, 2);
        } else {
	        echo $res;
        }

	    exit;
    }

	 public function showMemberWallet(Request $request) {

        $grid = new PanelGrid;

		if($request->has('aflid'))
		{
			$grid->setupGrid($this->moduleName, 'showMemberWallet-data', $this->limit, true, array('multiple' => true,'params' => array('aflid' => $request->aflid )));
		}
		else
		{
			$grid->setupGrid($this->moduleName, 'showMemberWallet-data', $this->limit, true, array('multiple' => true,'footer' => true ));
		}

        $grid->setTitle(Lang::get('COMMON.REPORT'));

        $grid->addColumn('name', 			Lang::get('COMMON.NAME'), 						150,		array('align' => 'left'));
        $grid->addColumn('code',			Lang::get('COMMON.PRODUCT'), 					150,		array('align' => 'left'));
        $grid->addColumn('balance',			Lang::get('COMMON.BALANCE'), 					150,		array('align' => 'left'));
        $grid->addColumn('mainwallet',			Lang::get('COMMON.MAINWALLET'), 					150,		array('align' => 'left'));


        //$grid->addSearchField('search', array('tel_mobile' => Lang::get('COMMON.TELEPHONENO')));


        $data['grid'] = $grid->getTemplateVars();
		$data['paging'] = false;

        return view('admin.grid2',$data);
    }

	  public function doGetMemberWalletData(Request $request){

		$footers = array();
		$is_show = array();
		$total_mainwallet = 0;
		$total_balance = 0;
		$rows = array();

		$condition = '';

		if( $request->has('aflid') )
		{

			$downlines = Affiliate::getAllDownlineById($request->aflid, true);

			$downlines[] = $request->aflid;

			$condition = ' and c.accid IN (select id from account where aflid IN ('.implode($downlines,',').'))';
		}

		$datas = DB::select( DB::raw("select c.accid,c.acccode,d.code,c.balance from cashbalance c,product d where c.prdid = d.id and c.balance > 1 and c.crccode = '".Session::get('admin_crccode')."' ".$condition." order by balance desc
										"));

		$cashledgers     = Cashledger::selectRaw('acccode,accid,sum(amount) as amount')->whereRaw('((status in ('.CBO_LEDGERSTATUS_PENDING.', '.CBO_LEDGERSTATUS_MANPENDING.','.CBO_LEDGERSTATUS_CONFIRMED.', '.CBO_LEDGERSTATUS_MANCONFIRMED.', '.CBO_LEDGERSTATUS_PROCESSED.') AND chtcode IN ('.CBO_CHARTCODE_WITHDRAWAL.', '.CBO_CHARTCODE_BALANCETRANSFER.', '.CBO_CHARTCODE_WITHDRAWALCHARGES.' )) OR (status in ('.CBO_LEDGERSTATUS_CONFIRMED.', '.CBO_LEDGERSTATUS_MANCONFIRMED.') AND chtcode NOT IN ('.CBO_CHARTCODE_WITHDRAWAL.', '.CBO_CHARTCODE_BALANCETRANSFER.' , '.CBO_CHARTCODE_WITHDRAWALCHARGES.' ))) group by accid')->get();

		foreach($cashledgers as $cashledger){
			$mainwallet[$cashledger->accid] = $cashledger->amount;
		}

		$is_displays = array();

		foreach( $datas as $data ){

			$is_displays[$data->accid] = true;

			if( !in_array($data->accid,$is_show) ){

				$total_mainwallet += isset($mainwallet[$data->accid]) ? $mainwallet[$data->accid]:0;
			}

			$total_balance += $data->balance;

			$rows[] = array(
				'name'  		=> $data->acccode,
				'code'  		=> $data->code,
				'balance'  		=> App::formatNegativeAmount($data->balance),
				'mainwallet'  	=> App::formatNegativeAmount(isset($mainwallet[$data->accid]) ? $mainwallet[$data->accid] : 0)

			);

			$is_show[$data->accid] = $data->accid;
		}

		foreach($cashledgers as $cashledger){

			if( !isset($is_displays[$cashledger->accid]) ){

				$rows[] = array(
					'name'  		=> substr($cashledger->acccode,3),
					'code'  		=> 'Main Wallet',
					'balance'  		=> 0,
					'mainwallet'  	=> App::formatNegativeAmount($cashledger->amount)

				);
			}
		}

		$footers[] = array(
            'balance' => App::formatNegativeAmount($total_balance),
            'mainwallet' => App::formatNegativeAmount($total_mainwallet),

        );


        echo json_encode(array('total' => count($rows),'rows' => $rows, 'footer' => $footers));
        exit;


    }

}
