<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller\Admin;
use App\libraries\grid\PanelGrid;
use App\libraries\App;
use App\Models\Currency;
use App\Models\Product;
use App\Models\Website;
use Carbon\Carbon;
use File;
use Illuminate\Http\Request;
use Auth;
use Validator;
use Session;
use Lang;
use Cache;
use Config;



class ProductController extends Controller{

	
	protected $moduleName 	= 'product';
	protected $limit 		= 20;
	protected $start 		= 0;
	
	
	private $flds = array (
		'sid'			=>	array( 'hidden', 		'',				'sid', 				false),
		'category'		=>	array( 'swapbox',		'CATEGORY',		'scategory', 		true),
		'rank'			=>	array( 'text', 			'RANK',			'irank', 			true),
		'code'			=>	array( 'text',			'CODE',			'scode',			true),
		'name'			=>	array( 'text',			'NAME',			'sname',			true),
		'desc'			=>	array( 'text',			'DESC',			'sdesc',			true),
		'nameen'		=>	array( 'text',			'',				'snameen',			true),
		'namecn'		=>	array( 'text',			'',				'snamecn',			true),
		'nametw'		=>	array( 'text',			'',				'snametw',			true),
		'nameid'		=>	array( 'text',			'',				'snameid',			true),
		'status'		=>	array( 'radio', 		'STATUS',		'istatus',			true),
		'maintenance'	=>	array( 'textarea', 		'MAINTENANCE',	'cmaintenance',		false),
		'createdby'		=>	array( 'showonly', 		'',				'', 				false),
		'modifiedby'	=>	array( 'showonly', 		'',				'', 				false),
		'created'		=>	array( 'showonly', 		'',				'', 				false),
		'modified'		=>	array( 'showonly', 		'',				'', 				false),
	);
	
	

	public function __construct()
	{
		
	}

	protected function show(Request $request) {

		$grid = new PanelGrid;
		$grid->setupGrid($this->moduleName, 'product-data', $this->limit);
		
		$grid->setTitle(Lang::get('COMMON.PRODUCT'));

		$grid->addColumn('code', 		Lang::get('COMMON.CODE'), 			50,		array('align' => 'center'));
		$grid->addColumn('name', 		Lang::get('COMMON.NAME'), 			130,	array());
		$grid->addColumn('desc', 		Lang::get('COMMON.DESC'), 			150,	array());
		$grid->addColumn('pctid', 		Lang::get('COMMON.CATEGORY'), 		180,	array());
		$grid->addColumn('status', 		Lang::get('COMMON.STATUS'), 		90,		array('align' => 'center'));
		$grid->addColumn('traffic', 	Lang::get('COMMON.RESPOND'), 		90,		array('align' => 'center'));
		$grid->addColumn('ticket', 		Lang::get('public.TicketFetchStatus'),	230,		array('align' => 'center'));

		//$grid->addButton('1', 'snew', Lang::get('COMMON.NEW'), 'button', array('icon' => 'add', 'url' => action('Admin\ProductController@drawAddEdit')));
		
		$data['grid'] = $grid->getTemplateVars();
		
		if( $productObjs = Product::get() )
		{
			foreach( $productObjs as $productObj ){
				$products[] = $productObj->code;
			}
		}
		$data['products'] = $products;

		return view('admin.grid2',$data);


	}
	
	function rutime($ru, $rus, $index) {
		return ($ru["ru_$index.tv_sec"]*1000 + intval($ru["ru_$index.tv_usec"]/1000))
		 -  ($rus["ru_$index.tv_sec"]*1000 + intval($rus["ru_$index.tv_usec"]/1000));
	}
	
	protected function doGetData(Request $request) {

		$condition = '1';

		$total = Product::whereRaw($condition)->count();
		

		$rows = array();
		if($products = Product::whereRaw($condition)->get()){
            // Check for datapull lock file.
            $lockfilePaths = glob(storage_path('app').DIRECTORY_SEPARATOR.'*.lock');
            $lockfiles = array();

            foreach ($lockfilePaths as $fpath) {
                if (File::exists($fpath)) {
                    $name = File::name($fpath);

                    if (preg_match('/^(\w+)_datapull_process(_\S{3})?$/', $name, $matches) > 0) {
                        $key = strtoupper($matches[1]);
                        $lastModified = File::lastModified($fpath);
                        $dt = Carbon::createFromTimestamp($lastModified);
                        $htmlAddon = '';

                        if ($dt->diffInHours(Carbon::now()) >= 1) {
                            // Set text color to red if datapull start time is over 1 hour from now.
                            $htmlAddon = 'style="color: #ff0000;"';
                        }

                        $html = '<a href="#" '.$htmlAddon.' onclick="return doConfirmAction(\''.$this->moduleName.'-active-suspend\', \'unlock\', \''.$name.'|'.$lastModified.'\');" title="'.$name.'">Running since: '.$dt->toDateTimeString().'</a>';

                        if (array_key_exists($key, $lockfiles)) {
                            $temp = $lockfiles[$key];
                            $lockfiles[$key] = $temp.', '.$html;
                        } else {
                            $lockfiles[$key] = $html;
                        }
                    }
                }
            }

            foreach($products as $product){
                $ticketText = 'Normal';

                if (array_key_exists($product->code, $lockfiles)) {
                    $ticketText = $lockfiles[$product->code];
                }

				$rows[] = array(
					'pctid'  => $product->getCategoryText(),
					'code'   => $product->code,
					'desc'   => $product->desc,
					'name'   => App::formatAddTabUrl(Lang::get('COMMON.PRODUCT').': '.$product->name, $product->name, action('Admin\ProductController@drawAddEdit',array('sid' => $product->id))),
					'status' => $product->getStatusText($this->moduleName),
					'traffic' => '<div id="'.$product->code.'"></div>',
                    'ticket' => $ticketText,
				);
			}
		}
		
		echo json_encode(array('total' => $total, 'rows' => $rows));
		exit;
	}
	
	protected function checkBalance(Request $request) {
		
		if( Product::where( 'code' , '=' , $request->get('product') )->pluck('status') != 0 )
		{
			$className = "App\\libraries\\providers\\".$request->get('product');
			$obj = new $className;
			
			if( Config::get('setting.front_path') == 'front' ){
			    $crccode = Session::get('admin_crccode');

				if ($request->get('product') == 'UC8'){
                                    if( $obj->getBalance( 'badeggtw', 'TWD' ) != false )
					return 'OK';
                                    else
					return 'FAILED';
                                }else{
                                    if( $obj->getBalance( 'PKCHAN1987', $crccode ) != false )
					return 'OK';
                                    else
					return 'FAILED';
                                }
				
			}else{
                $crccode = Session::get('admin_crccode');

                if ( Config::get('setting.front_path') == 'royalewin' ) {
                    if ($request->get('product') == 'FBL' || $request->get('product') == 'ISN') {
                        $crccode = 'IDR';
                    }
                }

				if( $obj->getBalance( 'badegg', $crccode ) != false )
					return 'OK';
				else
					return 'FAILED';
			}
			
			
		}
		
		return '';
		
	}


	protected function drawAddEdit(Request $request) {

		$toAdd 	  = true;
		$object   = null;
		$readOnly = false;

		$title = Lang::get('COMMON.NEW').' '.Lang::get('COMMON.PRODUCT');
		$categoryOptions = Product::getCategoryOptions();
		$categoryValues = array();
		
		if($request->has('sid')) 
		{
	
			if ($object = Product::find($request->input('sid'))) 
			{
					$title    = Lang::get('COMMON.EDIT').' '.Lang::get('COMMON.PRODUCT');
					$toAdd    = false;
					$readOnly = true;
					
					$categories = unserialize($object->category);
					if(is_array($categories)) {
						foreach($categories as $category) {
							if(isset($categoryOptions[$category]))
							$categoryValues[$category] = $categoryOptions[$category];
						}
					}

			} 
			else
			{
				$object = null;
			}
			
		}
		
		$form = App::setupPanelForm('doadd', $request->all(), array(), $toAdd);
		$form->setFormTitle($title);
		$form->addInput('showonly', Lang::get('COMMON.CODE') 		, 'code', 		 (($object)? $object->code:''), 		array(), true);
		$form->addInput('showonly', Lang::get('COMMON.NAME') 		, 'name', 		 (($object)? $object->name:''), 	    array(), true);
		$form->addInput('text',	    Lang::get('COMMON.DESC') 		, 'desc', 		 (($object)? $object->desc:''), 		array(), true);
		$form->addInput('text', 	Lang::get('COMMON.MAINTENANCE') , 'maintenance', (($object)? $object->maintenance:''),  array(), false);
		$form->addInput('radio', 	Lang::get('COMMON.STATUS')	    , 'status', 	 (($object)? $object->status:''),	    array('options' => Product::getStatusOptions()), true);
		

		$data['form'] = $form->getTemplateVars();
		$data['module'] = 'product';

		return view('admin.form2',$data);
	

	}



	protected function doActivateSuspend(Request $request) {

	    if ($request->action == 'unlock') {
	        // Unlock datapull lock file.
            $lockfile = explode('|', $request->id);

            if (count($lockfile) == 2) {
                $path = storage_path('app').DIRECTORY_SEPARATOR.$lockfile[0].'.lock';

                if (File::exists($path)) {
                    if (File::lastModified($path) == $lockfile[1]) {
                        File::delete($path);

                        echo json_encode(array('code' => Lang::get('COMMON.SUCESSFUL')));
                        exit;
                    }
                }
            }

            echo json_encode( array( 'code' => Lang::get('COMMON.FAILED') ) );
            exit;
        }

		if($request->has('id')) {
		
			if($object = Product::find($request->id)) {
			
				$currentStatus = $object->status;
				if($request->action == 'activate') {
					$object->status 	= CBO_ACCOUNTSTATUS_ACTIVE;
				}
				if($request->action == 'suspend') {
					$object->status 	= 2;
				}

				if($currentStatus <> $object->status){
					if($object->save()) {
						$this->clear_product_cache();
						echo json_encode( array( 'code' => Lang::get('COMMON.SUCESSFUL') ) );
						exit;
					}
				}
			}
		}
		
		
		echo json_encode( array( 'code' => Lang::get('COMMON.FAILED') ) );
	
	}
	

	protected function doAddEdit(Request $request) {
		
		$validator = Validator::make(
			[
	
				'desc'	   => $request->input('desc'),
				'status'   => $request->input('status'),
			],
			[

			    'desc'	   	   => 'required',
			    'status'	   => 'required',
			]
		);
		
		
		if ($validator->fails())
		{
			echo json_encode($validator->errors());
			exit;
		}
		
		$object = new Product;
		
		if($request->has('sid')) 
		{
			$object = Product::find($request->get('sid'));
		}
		
		$object->desc 					= $request->input('desc');
		$object->status 				= $request->input('status');
		$object->maintenance 			= $request->input('maintenance');

		if( $object->save() )
		{
			$this->clear_product_cache();
			echo json_encode( array( 'code' => Lang::get('COMMON.SUCESSFUL') ) );
			exit;
		}


		echo json_encode( array( 'code' => Lang::get('COMMON.FAILED') ) );
	}
	
	private function clear_product_cache(){
		
		if( $websiteObjs = Website::get() )
		{
			
			foreach( $websiteObjs as $websiteObj )
			{
				
				cache::forget('valid_product_'.$websiteObj->code);
				
			}
		
		}
		
	}
	
}
