<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller\Admin;
use App\libraries\App;
use Illuminate\Http\Request;
use App\Models\Channel;
use App\Models\Currency;
use App\Models\Account;
use App\Models\Accountdetail;
use App\Models\Agent;
use App\Models\Wager;
use App\Models\Cashledger;
use Auth;
use Validator;
use Session;
use Lang;


class WalletsController extends Controller{
	
	public function __construct()
	{
		
	}

	public static function index(Request $request, $product)
	{

		if($request->has('product')){
			$product = $request->get('product');
		}

		
		$className = "App\libraries\providers\\".$product;
		$obj = new $className;
		
		$balance = $obj->getBalance( '', '');
		$balance = ($balance == false) ? 0 : $balance;
		
		echo $balance;
	}
	
	public static function mainwallet(Request $request, $numberformat = true){
		
		$condition = '((status in ('.CBO_LEDGERSTATUS_PENDING.', '.CBO_LEDGERSTATUS_MANPENDING.','.CBO_LEDGERSTATUS_CONFIRMED.', '.CBO_LEDGERSTATUS_MANCONFIRMED.', '.CBO_LEDGERSTATUS_PROCESSED.') AND chtcode IN ('.CBO_CHARTCODE_WITHDRAWAL.', '.CBO_CHARTCODE_BALANCETRANSFER.', '.CBO_CHARTCODE_WITHDRAWALCHARGES.' )) OR (status in ('.CBO_LEDGERSTATUS_CONFIRMED.', '.CBO_LEDGERSTATUS_MANCONFIRMED.') AND chtcode NOT IN ('.CBO_CHARTCODE_WITHDRAWAL.', '.CBO_CHARTCODE_BALANCETRANSFER.' , '.CBO_CHARTCODE_WITHDRAWALCHARGES.' )))';

		$condition .= ' AND accid = '.$request->input('sid');
		
		$balance = Cashledger::whereRaw($condition)->sum('amount');

		if( $numberformat == true){
			return number_format($balance, 2, '.', '');
		}else{
			return $balance;
		}
		
	}
}
