<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller\Admin;
use App\libraries\App;
use Illuminate\Http\Request;
use Auth;
use Validator;
use Session;
use Lang;
use App\Models\Currency;
use App\Models\User;
use App\Models\Onlinetracker;
use Config;
use Response;
use Redirect;
use App\Jobs\ChangeLocale;
use App\libraries\Login;

class LoginController extends Controller{
	
	public function __construct()
	{
		
	}

	public function index()
	{  
		return view('admin.login');
	}	
	
	public function process(Request $request)
	{       
	
		if( Session::get('admin_catpcha') != $request->input('code') )
		{
			return redirect::route('admin_login')->with('message', 'Wrong captcha');
		}
		
		if (Auth::admin()->attempt(array('username' => $request->input('username'), 'password' => $request->input('password') , 'status' => 1 )))
        {	
			
			$user = Auth::admin()->get();
			
			$login = new Login;
			$login->doMakeTracker(Auth::admin()->get(), 'Admin');
			
			Session::put('admin_username',       $user->username );
			Session::put('admin_lastpwdchange',  $user->lastpwdchange );
			Session::put('admin_lastlogin',  	 $user->lastlogin );
			Session::put('admin_userid',  	     $user->id );
			Session::put('admin_password',  	 $user->password );
			Session::put('admin_crccode',  		 $user->crccode );
			Session::put('currency',  		     $user->crccode );
			
			User::where( 'id' , '=' , $user->id )->update(
				array( 'lastlogin' => date('Y-m-d H:i:s'))
			);
			
            if( $user->crccode == 'THB'){
                $changeLocale = new ChangeLocale('th');
                $this->dispatch($changeLocale);
            }elseif( $user->crccode == 'CNY'){
                $changeLocale = new ChangeLocale('cn');
                $this->dispatch($changeLocale);
            }
			
			
			Onlinetracker::whereType(CBO_LOGINTYPE_ADMIN)->whereUserid(Session::get('admin_userid'))->where( 'token' , '!=', Session::get('token'))->delete();
			
			return redirect::route('admin_home',array('currency' => $user->crccode));
		}else{
			return redirect::route('admin_login')->with('message', 'Login Failed');
		}
		
	}		
	
	public function getCaptcha(){
	
		$img=imagecreatefromjpeg("front/img/texture.jpg");	
		Session::put('admin_catpcha', rand(1000,9999));
		$security_number = Session::get('admin_catpcha');
		$image_text=$security_number;	
		$red=rand(100,255); 
		$green=rand(100,255);
		$blue=rand(100,255);
		$text_color=imagecolorallocate($img,205,205,205);
		$text=imagettftext($img,22,rand(-10,10),rand(10,30),rand(25,35),$text_color,Config::get('setting.server_path').'front/fonts/arial.ttf',$image_text);
	
		imagejpeg($img);
		return Response::make('', '200')->header('Content-Type', 'image/jpeg');
	}

	
	
}
