<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller\Admin;
use App\libraries\grid\PanelGrid;
use App\libraries\App;
use Illuminate\Http\Request;
use Auth;
use Validator;
use Session;
use Lang;
use App\Models\Currency;
use App\Models\Bank;
use App\Models\Accounttag;
use App\Models\Tag;
use App\Models\Account;
use Config;
use App\Models\Bankaccount;



class BankAccountController extends Controller{

	protected $moduleName 	= 'bankaccount';
	protected $limit 		= 20;
	protected $start 		= 0;
	
	public function __construct()
	{
					
	}
	
	public function index(Request $request){

			$grid = new PanelGrid;
			$grid->setupGrid($this->moduleName, 'bankaccount-data', $this->limit);
			$grid->setTitle(Lang::get('COMMON.BANKACCOUNT'));
									
			$grid->addColumn('id',				Lang::get('COMMON.ID'), 					0,		array('align' => 'left'));
			$grid->addColumn('name', 			Lang::get('COMMON.NAME'), 					200,	array('align' => 'left'));
			$grid->addColumn('bank', 			Lang::get('COMMON.BANK'),					150,	array('align' => 'left'));
			$grid->addColumn('currency', 		Lang::get('COMMON.CURRENCY'), 				68,		array('align' => 'left'));			
			$grid->addColumn('account', 		Lang::get('COMMON.ACCOUNT'), 				108,	array('align' => 'left'));		
			$grid->addColumn('limit', 			Lang::get('COMMON.LIMIT'), 					108,	array('align' => 'right'));		
			$grid->addColumn('deposit', 		Lang::get('COMMON.DEPOSIT'), 				108,	array('align' => 'left'));
			$grid->addColumn('withdraw', 		Lang::get('COMMON.WITHDRAW'), 				108,	array('align' => 'left'));
			$grid->addColumn('status', 			Lang::get('COMMON.STATUS'), 				108,	array('align' => 'left'));
                        if(Config::get('setting.front_path') == 'ampm' ||  Config::get('setting.front_path') == 'dadu') 
			$grid->addColumn('tag', 			Lang::get('COMMON.TAG'), 				200,	array('align' => 'left'));
			
			$grid->addFilter('status',  Bankaccount::getStatusOptions(), array('display' => Lang::get('COMMON.TYPE')));
			$grid->addButton('1', 'New', Lang::get('COMMON.ADDNEW').' '.Lang::get('COMMON.BANKACCOUNT'), 'button', array('icon' => 'add', 'url' => action('Admin\BankAccountController@drawAddEdit')));
			$grid->addButton('1', 'Profile', Lang::get('COMMON.PROFILE'), 'button', array('icon' => 'add', 'url' => action('Admin\BankProfileController@index')));

			
			$data['grid'] = $grid->getTemplateVars();

			return view('admin.grid2',$data);
    }
	
	protected function doGetData(Request $request){
		
		//status
		$status[1] = 'Active';
		$status[0] = 'Suspended';
		
		//withdraw
		$withdraw[1] = 'Yes';
		$withdraw[0] = 'No';
		
		//deposit
		$deposit[1] = 'Yes';
		$deposit[0] = 'No';
		
		$aflt  = $request->input('aflt');
		$aqfld = $request->input('aqfld');
		
		$condition = '1';
		$condition .= Session::get('admin_crccode') == 'MYR' ? '' : ' AND crccode = "'.Session::get('admin_crccode').'"';
		
		if( isset($aqfld['status'])&&(!empty($aqfld['status']))){
			$condition .= ' AND status'.'='.$aqfld['status'] ;
		}
		
		
		$total = Bankaccount::whereRaw($condition)->count();

		$rows = array();

		//new structure
		if($bankaccounts = Bankaccount::whereRaw($condition)->skip($request->recordstartindex)->take( $request->pagesize )->orderBy('id','desc')->get()){
			
			foreach( $bankaccounts as $bankaccount ){

				$body = App::formatAddTabUrl(Lang::get('BANKACCOUNT').': '.$bankaccount->id, urlencode($bankaccount->bankaccname), action('Admin\BankAccountController@drawAddEdit',  array('sid' => $bankaccount->id)));
				
				$rows[] = array(

					'id'  				=> $bankaccount->id, 
					'name'  			=> urldecode($body), 
					'bank'  			=> Bank::whereRaw('id'.'='.$bankaccount->bnkid)->pluck('name'), 
					'currency'                      => $bankaccount->crccode, 
					'account'  			=> $bankaccount->bankaccno, 
					'limit'  			=> App::displayAmount($bankaccount->limit), 
					'deposit'  			=> $deposit[$bankaccount->deposit], 
					'withdraw'                      => $withdraw[$bankaccount->withdraw], 
					'status'  			=> $bankaccount->getStatusText($this->moduleName), 
                                        'tag'                           => Tag::whereRaw('id'.'='.$bankaccount->tag)->pluck('name'),
				);	
				
			}
		
		}

				
		echo json_encode(array('total' => $total, 'rows' => $rows));
		exit;
	
	}
	
	protected function drawAddEdit(Request $request) { 

	    $toAdd 	  = true;
		$object   = null;
		$readOnly = false;
		$values   = array();

		$title = Lang::get('NEW').' '.Lang::get('BANKACCOUNT');

		if($request->has('sid')) 
		{
	
			if ($object = Bankaccount::find($request->input('sid'))) 
				{
					$title    = Lang::get('COMMON.EDIT').' '.Lang::get('COMMON.BANKACCOUNT');
					$toAdd    = false;
					$readOnly = true;

				} 
			else
				{
					$object = null;
				}
			
		}
		
		$banklist  = Bank::getAllAsOptions();
                $taglist   = Tag::getAllAsOptions();
                App::arrayUnshift($taglist, Lang::get('COMMON.SELECT'));
		$form = App::setupPanelForm('doadd', $request->all(), array(), $toAdd);
		$form->setFormTitle($title);
		
		$currencies = Currency::getAllCurrencyAsOptions();
		
		
		if( $bankaccounts = Bankaccount::where( 'status' , '=' , 1 )->get() ){
			$bankaccountOptions[0] = Lang::get('COMMON.SELECT');
			foreach( $bankaccounts as $bankaccount ){
				
				 $bankaccountOptions[$bankaccount->id] = $bankaccount->bankaccno . ' - ' . $bankaccount->bankaccname;
			}			 
		}
		if( $request->has('sid') ) {
			unset($bankaccountOptions[$request->input('sid')]);
		}

		$form->addInput('select',                   Lang::get('COMMON.BANK')				  , 'bank',	 	(($object)? $object->bnkid:''), 		array('options' => $banklist),true);
		$form->addInput('select',                   Lang::get('COMMON.CURRENCY')			  , 'crccode',          (($object)? $object->crccode:''),               array('options' => $currencies), 	 true);
		$form->addInput('text',                     Lang::get('COMMON.NAME') 				  , 'name', 	 	(($object)? $object->bankaccname:''),           array(), false); 
		$form->addInput('text',                     Lang::get('COMMON.ACCOUNT') 			  , 'account', 		(($object)? $object->bankaccno:''),             array(), false); 
		$form->addInput('text',                     Lang::get('COMMON.LIMIT') 				  , 'limit', 		(($object)? $object->limit:''), 		array(), false); 
		$form->addInput('select',                   'Rotate '.Lang::get('COMMON.BANKACCOUNT')             , 'tobnkid', 		(($object)? $object->tobnkid:''), 		array('options' => $bankaccountOptions), false);
                if(Config::get('setting.front_path') == 'ampm' ||  Config::get('setting.front_path') == 'dadu') 
                $form->addInput('select',                   Lang::get('COMMON.TAG')				  , 'tag',	 	(($object)? $object->tag:''),                   array('options' => $taglist),true);
		$form->addInput('checkbox_single',	    Lang::get('COMMON.DEPOSIT')	 			  , 'deposit', 		(($object)? $object->deposit:''), 		array(), true);
		$form->addInput('checkbox_single',	    Lang::get('COMMON.WITHDRAW')			  , 'withdraw', 	(($object)? $object->withdraw:''), 		array(), true);
		$form->addInput('radio',                    Lang::get('COMMON.STATUS')				  , 'status',  		(($object)? $object->status:''), 		array('options' => App::getStatusOptions()), true);
	
		$data['form'] = $form->getTemplateVars();
		$data['module'] = 'bankaccount';

		return view('admin.form2',$data);
	
	}
	
	protected function doAddEdit(Request $request) {
		
		$validator = Validator::make(
			[
	
				'name'	  	 	  => $request->input('name'),
				'account'	   	  => $request->input('account'),
				'crccode'  	      => $request->input('crccode'),
				'bank'  	      => $request->input('bank'),

			],
			[

			   'name'	   	  	   => 'required',
			   'account'	  	   => 'required',
			   'crccode'  		   => 'required',
			   'bank'  		   	   => 'required',

			]
		);
		
		
		if ($validator->fails())
		{
			echo json_encode($validator->errors());
			exit;
		}
		
		
		
		$object  = new Bankaccount;
		
		if($request->has('sid')) 
		{
			$object = Bankaccount::find($request->input('sid'));
		}
		
		$object->bankaccname                            = $request->input('name');
		$object->bankaccno	 			= $request->input('account');
		$object->crccode	 			= $request->input('crccode');
		$object->bnkid	 				= $request->input('bank');
		$object->limit	 				= $request->input('limit');
		$object->tobnkid	 			= $request->input('tobnkid');
        $object->tag           			= $request->has('tag') ? $request->input('tag'):0;
		$object->deposit	 			= (!empty($request->input('deposit')))?$request->input('deposit'):0;
		$object->withdraw	 			= (!empty($request->input('withdraw')))?$request->input('withdraw'):0;
		$object->status	 				= 1;


		
		if( $object->save() )
		{
			echo json_encode( array( 'code' => Lang::get('COMMON.SUCESSFUL') ) );
			exit;
		}


		echo json_encode( array( 'code' => Lang::get('COMMON.FAILED') ) );
		
	}
	
	protected function doActivateSuspend(Request $request) {

			

		if($request->has('id')) {
		
			if($object = Bankaccount::find($request->id)) {
			
				$currentStatus = $object->status;
				if($request->action == 'activate') {
					$object->status 	= CBO_ACCOUNTSTATUS_ACTIVE;
				}
				if($request->action == 'suspend') {
					$object->status 	= CBO_ACCOUNTSTATUS_SUSPENDED;
				}

				if($currentStatus <> $object->status){
					if($object->save()) {
					
						echo json_encode( array( 'code' => Lang::get('COMMON.SUCESSFUL') ) );
						exit;
					}
				}
			}
		}
		
		
		echo json_encode( array( 'code' => Lang::get('COMMON.FAILED') ) );

	}
	



}

?>
