<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller\Admin;
use App\libraries\grid\PanelGrid;
use App\libraries\App;
use App\Models\Fundingmethod;
use App\Models\Bank;
use App\Models\Tag;
use App\Models\Currency;
use App\Models\Cashledger;
use App\Models\Configs;
use App\Models\Account;
use App\Models\Banktransfer;
use App\Models\Accounttag;
use Config;
use Illuminate\Http\Request;
use Auth;
use Validator;
use Session;
use Lang;


class InstantTransactionController extends Controller{

	
	protected $moduleName 	= 'Cashledger';
	protected $limit 		= 20;
	protected $start 		= 0;
	
	public function __construct()
	{
		
	}

	public function index(Request $request)
	{

		$nowTimer = App::getDateTime();
		$refreshTimer = 10;
		
		$fundingMethods = Fundingmethod::whereRaw('status ='.CBO_STANDARDSTATUS_ACTIVE)->get();
		$fundingMethodArray = array();
		
		foreach($fundingMethods AS $fundingMethod){
			$fundingMethodArray[$fundingMethod->id]=$fundingMethod->name;
			if( !$request->has('fundingmethod') )
			$fundingMethodSelected[] = $fundingMethod->id;
		}
		$bankSelected = array();
		$bankArray = Bank::getAllAsOptions(true);
		if( !$request->has('bank') ) {
			if($bankArray){
				foreach($bankArray as $id => $bank) {
					$bankSelected[] = $id;
				}
			}
		}
		
		$transactionArray = array(
			CBO_CHARTCODE_DEPOSIT		=>	Lang::get('COMMON.DEPOSIT'),
			CBO_CHARTCODE_WITHDRAWAL	=>	Lang::get('COMMON.WITHDRAWAL'),
			CBO_CHARTCODE_BONUS			=>	Lang::get('COMMON.BONUS'),
			CBO_CHARTCODE_INCENTIVE		=>	Lang::get('COMMON.INCENTIVE'),
			CBO_CHARTCODE_ADJUSTMENT	=>	Lang::get('COMMON.ADJUSTMENT'),
		);
	
		if( !$request->has('transaction') ) {
			$transactionSelected = $request->get('transaction');
		}else{
			$transactionSelected = array(CBO_CHARTCODE_DEPOSIT,CBO_CHARTCODE_WITHDRAWAL, CBO_CHARTCODE_BONUS, CBO_CHARTCODE_ADJUSTMENT, CBO_CHARTCODE_INCENTIVE);
		}
		
		$tagArray = array();
		if($tags = Tag::whereRaw('status ='.CBO_STANDARDSTATUS_ACTIVE)->get()) {
			foreach($tags AS $tag){
				$tagArray[$tag->id]='&nbsp;<div class="triangle" style="border-bottom-color:'.$tag->color.'">&nbsp;</div>';
				if( !$request->has('tag') ) {
					$tagSelected[] = $tag->id;
				}
			}
		}
		
		$refreshTimerArray = array(0,5,10,20,30,60);
		
		$data['shownumber'] = 0;
		$data['refreshTimer'] = $refreshTimer;
		$data['refreshTimerArray'] = $refreshTimerArray;

		$data['nowTimer'] = App::getDateTime();
		$data['transactionArray'] = $transactionArray;
		$data['transactionSelected'] = $transactionSelected;
		$data['fundingMethodArray'] = $fundingMethodArray;
		$data['fundingMethodSelected'] = $fundingMethodSelected;
		$data['bankArray'] = $bankArray;
		$data['bankSelected'] = $bankSelected;
		$data['tagArray'] = $tagArray;
		//$data['tagSelected'] = $tagSelected;

		$data['tablelink'] = '';
		$data['totallink'] = '';
		
		$data['title']     = Lang::get('COMMON.TRANSACTION');
		$data['gettime']   = '';

		
		//Perform permission checking
		//$this->hasRight(CBO_BBOMODULE_BANK, CBO_BBOMODULE_BANK_SHOW);
		
		/**
		* Setup the grid using the total record count and paging information
		*/
		$grid = new PanelGrid;
		$grid->setupGrid($this->moduleName, 'instant-data', 0);

		/**
		* Set the grid title
		*/
		$grid->setTitle(Lang::get('COMMON.TRANSACTION'));

		/**
		* Setup the columns on the grid
		*/
		$grid->addColumn('date', 		Lang::get('COMMON.DATE'), 				100,	array('align' => 'center'));
		$grid->addColumn('transid', 	Lang::get('COMMON.ID'), 				55,		array('align' => 'center'));
		$grid->addColumn('account', 	Lang::get('COMMON.ACCOUNT'),			120,	array());
		$grid->addColumn('name', 		Lang::get('COMMON.NAME'), 				120,	array());
		$grid->addColumn('method',	 	Lang::get('COMMON.PAYMENT'), 			75,		array('align' => 'center'));
		$grid->addColumn('status', 		Lang::get('COMMON.STATUS'), 			80,		array('align' => 'center'));
		$grid->addColumn('amount', 		Lang::get('COMMON.AMOUNT'), 			70,		array('align' => 'right'));
		$grid->addColumn('bank', 		Lang::get('COMMON.BANK'), 				150,		array('align' => 'center'));
		$grid->addColumn('accno', 		Lang::get('COMMON.BANKACCNO'), 			130,	array('align' => 'center'));
		
		$grid->addFilter('scrccode', Currency::getAllCurrencyAsOptions(), array('display' => Lang::get('COMMON.CURRENCY')));
		$grid->addFilter('achtcode', Cashledger::getAllTypeAsOptions(array(CBO_CHARTCODE_BALANCETRANSFER)), array('width' => 300, 'display' => Lang::get('COMMON.TYPE'), 'multiple' => true, 'select' => false, 'values' => json_encode(array(CBO_CHARTCODE_DEPOSIT, CBO_CHARTCODE_WITHDRAWAL, CBO_CHARTCODE_BONUS, CBO_CHARTCODE_ADJUSTMENT, CBO_CHARTCODE_INCENTIVE))));
		
		$data['grid'] = $grid->getTemplateVars();

		return view('admin.grid2',$data);


	}
	
	public function doGetData(Request $request){
		
		$total = array();

		$rows = array();
		
		$aqfld = $request->input('aqfld');
		
		$bankaccno = '';$bankprovine = '';$bankcity = '';$bankname = '';
		
		$condition = '1';
		
		
		
		

		$condition = '(status='.CBO_LEDGERSTATUS_PENDING.' OR status='.CBO_LEDGERSTATUS_MANPENDING.' OR status='.CBO_LEDGERSTATUS_PROCESSED.')';
		
		$otherCode = array();
		
		if( isset($aqfld['achtcode']) && $aqfld['achtcode'] != 0 ){
			$condition .= ' AND chtcode = "'.$aqfld['achtcode'].'" ';
		}else{
			$condition .= ' AND chtcode IN ('.CBO_CHARTCODE_DEPOSIT.','.CBO_CHARTCODE_WITHDRAWAL.','.CBO_CHARTCODE_BONUS.','.CBO_CHARTCODE_ADJUSTMENT.','.CBO_CHARTCODE_INCENTIVE.')';
		}
	
//		$condition .= ' AND (fdmid != '.Configs::getParam("SYSTEM_PAYMENT_ID_PG").' OR (fdmid = '.Configs::getParam("SYSTEM_PAYMENT_ID_PG").' AND status != '.CBO_LEDGERSTATUS_PENDING.'))';
		$condition .= ' AND (fdmid != '.Configs::getParam("SYSTEM_PAYMENT_ID_PG").' OR (fdmid = '.Configs::getParam("SYSTEM_PAYMENT_ID_PG").' AND instanttransflag != "'.Cashledger::INSTANT_TRANS_FLAG_HIDDEN.'"))';

		$tagArray = array();
		if($tags = Tag::whereRaw('status ='.CBO_STANDARDSTATUS_ACTIVE)->get()) {
			foreach($tags AS $tag){
				$tagArray[$tag->id]='&nbsp;<div class="triangle" style="border-bottom-color:'.$tag->color.'">&nbsp;</div>';
				$tagSelected[] = $tag->id;
			}
		}
		
		if(count($otherCode) > 0) {
			$condition = '('.$condition.') OR (chtcode IN ('.implode(',', $otherCode).') AND (status='.CBO_LEDGERSTATUS_PENDING.' OR status='.CBO_LEDGERSTATUS_MANPENDING.' OR status='.CBO_LEDGERSTATUS_PROCESSED.'))';
		}

		$rows = array();
		
		if( isset($aqfld['scrccode']) && $aqfld['scrccode'] != '' ) {
            $condition = '(' . $condition . ') AND crccode = "' . $aqfld['scrccode'] . '" ';
        }elseif( Config::get('setting.opcode') == 'IFW' ){
		    // Special for Infiniwin
            if ( Session::get('admin_crccode') == 'MYR' ) {
                // MYR Admin can see all currencies.
                $condition = '('.$condition.') AND crccode IN ("MYR", "TWD")';
            } else {
                $condition = '('.$condition.') AND crccode = "'.Session::get('admin_crccode').'"';
            }
        }elseif( Config::get('setting.opcode') == 'RYW' ){
		    // Special for Royalewin.
		    if ( Session::get('admin_crccode') == 'MYR' ) {
		        // MYR Admin can see all currencies.
                $condition = '('.$condition.') AND crccode IN ("MYR", "IDR")';
            } else {
                $condition = '('.$condition.') AND crccode = "'.Session::get('admin_crccode').'"';
            }
		}else{
			$condition = '('.$condition.') AND crccode = "'.Session::get('admin_crccode').'"';
		}
		
		$total = Cashledger::whereRaw($condition)->count();

		$totalcredit = 0;
		$totaldebit = 0;
		$last_id = 0;
		
		if($ledgers = Cashledger::whereRaw($condition)->orderBy('status','desc')->get() ) {

			$count = 0;
			$accTagArray = array();
			$fundingMethods = Fundingmethod::whereRaw('status ='.CBO_STANDARDSTATUS_ACTIVE)->get();
			$fundingMethodArray = array();
			
			foreach($fundingMethods AS $fundingMethod){
				$fundingMethodArray[$fundingMethod->id]=$fundingMethod->name;
				$fundingMethodSelected[] = $fundingMethod->id;
			}
			
			$bankArray = Bank::getAllAsOptions(true);
			foreach($bankArray as $id => $bank) {
				$bankSelected[] = $id;
			}
			
			$transactionArray = array(
				CBO_CHARTCODE_DEPOSIT		=> Lang::get('COMMON.DEPOSIT'),
				CBO_CHARTCODE_WITHDRAWAL	=> Lang::get('COMMON.WITHDRAWAL'),
				CBO_CHARTCODE_BONUS			=> Lang::get('COMMON.BONUS'),
				CBO_CHARTCODE_INCENTIVE		=> Lang::get('COMMON.INCENTIVE'),
				CBO_CHARTCODE_ADJUSTMENT	=> Lang::get('COMMON.ADJUSTMENT'),
			);
			
			$start = 0;
			
			foreach($ledgers as $ledger) {
				$accObj = Account::find($ledger->accid);
				$credit = '&nbsp;';
				$debit = '&nbsp;';
				$remark = '&nbsp;';
				$count++;
				$numbering = $count + $start;
				
			
				
				$accname 	  = $ledger->accname;
				$name		  = 'shortname';
				$bankprovince = '';
				$bankcity 	  = '';
				$bankbranch   = '';
				$bankname 	  = '';
				
				if($ledger->chtcode == CBO_CHARTCODE_DEPOSIT){
					$action = 'deposit';

					if($ledger->fdmid == Configs::getParam("SYSTEM_PAYMENT_ID_BANKTRANSFER")) {
						$refobj 	  = Banktransfer::find($ledger->refid);
						$bankObj 	  = Bank::find($refobj->bnkid);
						$bankaccno 	  = $refobj->receiveaccno;
						$bankprovince = '';
						$bankcity 	  = '';
						$bankbranch   = '';
						$receiveaccno = $refobj->receiveaccno != '' ? substr($refobj->receiveaccno, -4, 4) : '';
						//$bankname 	  = $bankObj->$name.' ['.$refobj->receiveaccno.']';
                                                $bankname 	  = $bankObj->$name;
						//var_dump($receiveaccno);
					} elseif ($ledger->fdmid == Configs::getParam("SYSTEM_PAYMENT_ID_PG")) {
                        $instanttransflag = ($ledger->instanttransflag == Cashledger::INSTANT_TRANS_FLAG_HIDDEN ? Lang::get('COMMON.AUTO') : Lang::get('COMMON.MANUAL'));
                        $bankname = Lang::get('COMMON.PAYMENTGATEWAY').' ('.$instanttransflag.')';
                    }
				}elseif($ledger->chtcode == CBO_CHARTCODE_WITHDRAWAL){
					$action = 'withdraw';
					$bankprovince = '';
					$bankcity 	  = '';
					$bankbranch   = '';
					if($refobj = Banktransfer::find($ledger->refid)) {
						$bankObj      = Bank::find($refobj->bnkid);
						$bankaccno    = $refobj->bankaccno;
						$bankprovince = $refobj->bankprovince;
						$bankcity     = $refobj->bankcity;
						$bankbranch   = $refobj->bankbranch;
						$bankname     = $bankObj->$name;
					}
						
				}elseif($ledger->chtcode == CBO_CHARTCODE_BONUS || $ledger->chtcode == CBO_CHARTCODE_INCENTIVE) {
					$action = 'deposit';
				}elseif($ledger->chtcode == CBO_CHARTCODE_ADJUSTMENT) {
					$action = 'adjustment';
				}
				
				if($ledger->chtcode == CBO_CHARTCODE_DEPOSIT || $ledger->chtcode == CBO_CHARTCODE_BONUS)
				{
				    $style = '';

				    if ($accObj->totaldeposit <= 0) {
                        $style = 'color: red;';
                    }

					$id = '<a href="#" onclick="openParentTab(\'Transaction #:'.$ledger->getTransactionId().'\', \'show-deposit?id='.$ledger->id.'\')" style="'.$style.'">'.$ledger->getTransactionId().'</a>';
				}
				elseif($ledger->chtcode == CBO_CHARTCODE_WITHDRAWAL)
				{
                    $style = '';

                    if ($accObj->totalwithdrawal <= 0) {
                        $style = 'color: red;';
                    }

					$id = '<a href="#" onclick="openParentTab(\'Transaction #:'.$ledger->getTransactionId().'\', \'show-withdraw?id='.$ledger->id.'\')" style="'.$style.'">'.$ledger->getTransactionId().'</a>';
				}
				elseif($ledger->chtcode == CBO_CHARTCODE_ADJUSTMENT)
				{
					$id = '<a href="#" onclick="openParentTab(\'Transaction #:'.$ledger->getTransactionId().'\', \'show-adjustment?id='.$ledger->id.'\')">'.$ledger->getTransactionId().'</a>';
				}
				
				$account = '<a href="#" onclick="openParentTab(\'Member : '.$accObj->nickname.'\', \'customer-tab?sid='.$accObj->id.'\')">'.$accObj->nickname.'</a>';
				
		
				$fundmethod = (isset($fundingMethodArray[$ledger->fdmid]))?$fundingMethodArray[$ledger->fdmid]:$transactionArray[$ledger->chtcode];
				$amount = App::displayAmount($ledger->amount);
				if($ledger->amount < 0) 
				$amount = '<font color="red">'.$amount.'</font>';
				if($ledger->chtcode == CBO_CHARTCODE_WITHDRAWAL){
					$amount       = '<span id="amount'.$ledger->id.'" style="cursor:pointer;color:red;" onclick="copy_info(this)">'.App::displayAmount($ledger->amount).'</span>';
					$accname      = '<span id="name'.$ledger->id.'" onclick="copy_info(this)" style="cursor:pointer;">'.$ledger->accname.'</span>';
					$bankaccno    = '<span id="accno'.$ledger->id.'" onclick="copy_info(this)" style="cursor:pointer;;">'.$bankaccno.'</span>';
					$bankprovince = '<span id="province'.$ledger->id.'" onclick="copy_info(this)" style="cursor:pointer;">'.$bankprovince.'</span>';
					$bankcity     = '<span id="city'.$ledger->id.'" onclick="copy_info(this)" style="cursor:pointer;">'.$bankcity.'</span>';
					$bankbranch   = '<span id="branch'.$ledger->id.'" onclick="copy_info(this)" style="cursor:pointer;">'.$bankbranch.'</span>';
				}
				
				if( $accounttags = Accounttag::where( 'accounttag.status' , '=' , 1 )->whereAccid($accObj->id)->leftJoin('tag', 'accounttag.tagid', '=', 'tag.id')->get() )
				{
					foreach( $accounttags as $accounttag )
					{
						
						$account .= '<div class="triangle" style="border-bottom-color:'.$accounttag->color.';"> </div>';
					}
				}

				
				
				$status = '<div style="background-color:'.$ledger->getStatusColorCode().';line-height:22px;">'.$ledger->getStatusText($ledger->status, true).'</div>';
				$rows[] = array(
					'date'    => App::formatDatetime(32, $ledger->created),
					'transid' => $id,
					'account' => $account,
					'name'    => $accname,
					'amount'  => $amount,
					'bank'    => $bankname,
                                        'accno'   => $bankaccno,
					'method'  => $ledger->getTypeText($ledger->chtcode),
					'status'  => $status,
					'no_status'  => $ledger->status,
				);
				
				if($ledger->id > $last_id) $last_id = $ledger->id;
			}
		}
	
		echo json_encode(array('total' => $total, 'rows' => $rows));
		exit;
	}
}
