<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller\Admin;
use App\libraries\grid\PanelGrid;
use App\libraries\App;
use Illuminate\Http\Request;
use Auth;
use Validator;
use Session;
use Lang;
use App\Models\Iptracker;
use App\Models\Currency;
use App\Models\Fundingmethod;
use App\Models\Cashledger;
use App\Models\Bank;
use App\Models\Accounttag;
use App\Models\Tag;
use App\Models\Account;
use App\Models\Config;
use App\Models\Cashledgerlog;
use App\Models\Bankaccount;
use App\Models\Profitloss;
use App\Models\Promocash;
use App\Models\Rejectreason;
use App\Models\Banktransfer;
use App\Models\Promocampaign;
use App\Models\Remark;
use App\Models\Product;
use App\Models\Cashback;
use App\Models\Cashbacksetting;
use App\Models\Wager;

class CashbackController extends Controller{


	protected $moduleName 	= 'cashback';
	protected $limit 		= 20;
	protected $start 		= 0;
	
	public function __construct()
	{
		
	}
	
	public function index(Request $request){

			$grid = new PanelGrid;
			$grid->setupGrid($this->moduleName, 'cashback-data', $this->limit);
			$grid->setTitle(Lang::get('COMMON.CASHBACK'));
			
			$grid->addColumn('id', 				Lang::get('COMMON.ID'), 				0,			array('align' => 'left'));			
			$grid->addColumn('currency',		Lang::get('COMMON.CURRENCY'), 			68,			array('align' => 'center'));
			$grid->addColumn('from', 			Lang::get('COMMON.FROM'), 				120,		array('align' => 'left'));
			$grid->addColumn('to', 				Lang::get('COMMON.END'), 				120,		array('align' => 'left'));
			$grid->addColumn('percentage', 		Lang::get('COMMON.PERCENTAGE'), 		85,			array('align' => 'center'));
			$grid->addColumn('maxpayout', 		Lang::get('COMMON.MAXPAYOUT'), 			120,		array('align' => 'left'));
			$grid->addColumn('minpayout', 		Lang::get('COMMON.MINPAYOUT'), 			120,		array('align' => 'left'));

			$grid->addButton('1', 'New', Lang::get('COMMON.ADDNEW').' '.Lang::get('COMMON.CASHBACK'), 'button', array('icon' => 'add', 'url' => action('Admin\CashbackController@drawAddEdit')));


			$data['grid'] = $grid->getTemplateVars();

			return view('admin.grid2',$data);
    }
	
	protected function doGetData(Request $request){
		
		$rows = array();
		
		$condition = '1';
		$condition .= Session::get('admin_crccode') == 'MYR' ? '' : ' AND crccode = "'.Session::get('admin_crccode').'"';
		
		$total = Cashbacksetting::whereRaw($condition)->count();
		
		//new structure
		if($cashbacks = Cashbacksetting::whereRaw($condition)->skip($request->recordstartindex)->take( $request->pagesize )->orderBy('id','desc')->get()){
			foreach( $cashbacks as $cashback ){
		
			
				$rows[] = array(
					'id'  					=> $cashback->id,
					'currency'  			=> $cashback->crccode,
					'from'  				=> $cashback->datefrom,
					'to'  					=> $cashback->dateto,
					'percentage'  			=> $cashback->rate.'%',
					'maxpayout'  			=> $cashback->maxamount,
					'minpayout'  			=> $cashback->minpayout,
				);	
				
			}
				
		}
				
		echo json_encode(array('total' => $total, 'rows' => $rows));
		exit;
	
	}
	
	protected function drawAddEdit(Request $request) { 

	    $toAdd 	  = true;
		$object   = null;
		$readOnly = false;
		$values   = array();

		$title = Lang::get('NEW').' '.Lang::get('CASHBACK');

		if($request->has('sid')) 
		{
	
			if ($object = Cashbacksetting::find($request->input('sid'))) 
				{
					$title    = Lang::get('COMMON.EDIT').' '.Lang::get('COMMON.CASHBACK');
					$toAdd    = false;
					$readOnly = true;

				} 
			else
				{
					$object = null;
				}
			
		}
		
		$currency  = Currency::getAllCurrencyAsOptions();
		
		$form = App::setupPanelForm('doadd', $request->all(), array(), $toAdd);
		$form->setFormTitle($title);

		$form->addInput('select', 		Lang::get('COMMON.CURRENCY')    , 'crccode',		(($object)? $object->crccode:''), 		array('options' => $currency),	 true);
		$form->addInput('date', 		Lang::get('COMMON.FROM')		, 'from',			(($object)? $object->datefrom:''), 		array(), false); 
		$form->addInput('date', 		Lang::get('COMMON.TO')			, 'to',				(($object)? $object->dateto:''), 		array(), false); 
		$form->addInput('text', 		Lang::get('COMMON.PERCENTAGE') 	, 'percentage', 	(($object)? $object->rate:''), 			array(), false);
		$form->addInput('text', 		Lang::get('COMMON.MINPAYOUT') 	, 'minpayout', 		(($object)? $object->minpayout:''), 	array(), false);
		$form->addInput('text', 		Lang::get('COMMON.MAXPAYOUT') 	, 'maxpayout', 		(($object)? $object->maxamount:''), 	array(), false);
		
	
		$data['form'] = $form->getTemplateVars();
		$data['module'] = 'cashback';

		return view('admin.form2',$data);
	
	}
	
	protected function doAddEdit(Request $request) {
		
		$validator = Validator::make(
			[
	
				'from'	  	  		 => $request->input('from'),
				'to'	  	 		 => $request->input('to'),
				'percentage'	  	 => $request->input('percentage'),
				'minpayout'	  	 	 => $request->input('minpayout'),
				'maxpayout'	  	 	 => $request->input('maxpayout'),
				'crccode'	  	 	 => $request->input('crccode'),

			],
			[

			   'from'	   	   	   => 'required',
			   'to'	  		 	   => 'required',
			   'percentage'	  	   => 'required',
			   'minpayout'	  	   => 'required',
			   'maxpayout'	  	   => 'required',
			   'crccode'	  	   => 'required',

			]
		);
		
		
		if ($validator->fails())
		{
			echo json_encode($validator->errors());
			exit;
		}
		
		
		
		$object  = new Cashbacksetting;
		
		if($request->has('sid')) 
		{
			$object = Cashbacksetting::find($request->input('sid'));
		}
		
		$object->datefrom	 		= $request->input('from');
		$object->dateto	 			= $request->input('to');
		$object->rate	 			= $request->input('percentage');
		$object->minpayout	 		= $request->input('minpayout');
		$object->maxamount	 		= $request->input('maxpayout');
		$object->crccode	 		= $request->input('crccode');
		
		if( $object->save() )
		{
			echo json_encode( array( 'code' => Lang::get('COMMON.SUCESSFUL') ) );
			exit;
		}

		echo json_encode( array( 'code' => Lang::get('COMMON.FAILED') ) );
		
	}
	
	public function showcashbackReport(Request $request){

			$grid = new PanelGrid;
			$grid->setupGrid($this->moduleName, 'cashbackReport-data', $this->limit);
			$grid->setTitle(Lang::get('COMMON.CASHBACK'));
				
			$grid->addColumn('from', 				Lang::get('COMMON.FROM'), 				100,		array('align' => 'center'));	
			$grid->addColumn('to', 					Lang::get('COMMON.TO'), 				100,		array('align' => 'center'));	
			$grid->addColumn('currency', 			Lang::get('COMMON.CURRENCY'), 			100,		array('align' => 'center'));	
			$grid->addColumn('rate', 				Lang::get('COMMON.RATE'), 				100,		array('align' => 'right'));	
			$grid->addColumn('member', 				Lang::get('COMMON.MEMBER'), 			100,		array('align' => 'right'));	
			$grid->addColumn('winloss', 			Lang::get('COMMON.MEMBER_WINLOSS'), 	130,		array('align' => 'right'));	
			$grid->addColumn('cashback', 			Lang::get('COMMON.CASHBACK'), 			100,		array('align' => 'right'));	
			$grid->addColumn('effective', 			Lang::get('COMMON.EFFECTIVE'), 			100,		array('align' => 'right'));	
			$grid->addColumn('status', 				Lang::get('COMMON.STATUS'), 			100,		array('align' => 'center'));
			$grid->addColumn('action', 				'', 									100,		array('align' => 'center'));	

			
			$grid->addFilter('status',array('Active','Suspended'), array('display' => Lang::get('COMMON.TYPE')));

			$data['grid'] = $grid->getTemplateVars();

			return view('admin.grid2',$data);
    }
	
	protected function showcashbackReportdoGetData(Request $request){


		$rows = array();
		$footer = array();
		$total_cashbacks = array();
		
		$condition = Session::get('admin_crccode') == 'MYR' ? 'dateto <= "'.App::getDateTime(3).'"' : 'crccode = "'.Session::get('admin_crccode').'" AND dateto <= "'.App::getDateTime(3).'"';
		
		$total = Cashbacksetting::whereRaw($condition)->count();
		
		if($cashbacks = Cashbacksetting::whereRaw($condition)->skip($request->recordstartindex)->take($request->recordendindex)->orderBy('id','desc')->get()){

			 foreach($cashbacks as $cashback){
				$member = Profitloss::whereRaw('istest = 0 AND crccode = "'.$cashback->crccode.'" AND date >= "'.$cashback->datefrom.'" AND date <= "'.$cashback->dateto.'" ')->count();
				
				$winloss = Profitloss::whereRaw('istest = 0 AND crccode = "'.$cashback->crccode.'"  AND date >= "'.$cashback->datefrom.'" AND date <= "'.$cashback->dateto.'" ')->sum('amount');
				$amount = ' - ';
				$effective = ' - ';
				
				if($cashback->status == CBO_STANDARDSTATUS_SUSPENDED)
				{
					$action = '<a href="#" onclick="doConfirmAction(\'cashback-docalculate\', \'cashback-docalculate\', '.$cashback->id.')">'.Lang::get('COMMON.CACULATE').'</a>';
				}
	
				else {		
					$action = '<a href="#" onclick="parent.addTab(\''.Lang::get('CASHBACK').': '.$cashback->id.'\', \'cashbacklist?id='.$cashback->id.'\')">'.Lang::get('COMMON.VIEW').'</a>';			
					$amount = App::displayAmount(Cashback::whereRaw('insid='.$cashback->id.' AND amount >= "'.$cashback->minpayout.'"')->sum('amount'));
					$effective = 0;
					if($winloss > 0)
					{
						$effective = $amount / $winloss * 100;
					}
					$effective = App::displayPercentage($effective);
				}
				
				$winloss = $cashback->crccode == 'VND' || $cashback->crccode == 'IDR' ?  $winloss * 1000 :  $winloss;
				
				$rows[] = array(
					'id' => $cashback->id,
					'from' => $cashback->datefrom,
					'to' => $cashback->dateto,
					'currency' => $cashback->crccode,
					'rate' => App::displayPercentage($cashback->rate),
					'member' => App::displayAmount($member, 0),
					'winloss' => App::displayAmount($winloss),
					'cashback' => $amount,
					'effective' => $effective,
					'action' => $action,
					'status' => $cashback->getStatusText(),
				);

			}
			 
			
		}
		//var_dump($stake);
		//	exit();
		
		echo json_encode(array('total' => $total, 'rows' => $rows));
		exit;
		
	}
	
	protected function doCalculateCashback(Request $request) {
		
		$result = false;
		if($request->has('id')) 
		{
			if ($object = Cashbacksetting::whereRaw('id='.$request->id.' AND status='.CBO_STANDARDSTATUS_SUSPENDED)->first()) 
			{
			
				/* $winlose 		= array();
				$winloselocal   = array();
				$currency 		= array();
				$accounts 		= array();
			
				if($profits = Profitloss::whereRaw('istest = 0 AND date >="'.$object->datefrom.'" AND date <="'.$object->dateto.'" AND crccode = "'.$object->crccode.'"')->get()) {
					foreach($profits as $profit) {
						
						if(!isset($currency[$profit->accid])) $currency[$profit->accid] = $profit->crccode;
						if(!isset($accounts[$profit->accid])) $accounts[$profit->accid] = $profit->acccode;
						if(isset($winlose[$profit->accid])) {
							$winlose[$profit->accid]		+= (float) $profit->amount;
							$winloselocal[$profit->accid]   += (float) $profit->amountlocal;
						} else {
							$winlose[$profit->accid] 	        = (float) $profit->amount;
							$winloselocal[$profit->accid]   = (float) $profit->amountlocal;
						}
					}
				}
			
				foreach($winlose as $accid => $winloss) {
					
					if( $winloss < 0 ){
						
						$winloss = $winloss * -1;
					
						$amount = App::float(Cashledger::whereChtcode(1)->whereAccid($profit->accid)->whereBetween('created', array($profit->datefrom.' 00:00:00', $profit->dateto.' 23:59:59'))->whereRaw('status IN (1,4)')->sum('amount') * $object->rate / 100);
						if($object->maxamount > 0 && $amount > $object->maxamount)
						$amount = $object->maxamount;
						
						$incObj = new Cashback;
						$incObj->insid 				= $object->id;
						$incObj->product 			= $object->product;
						$incObj->accid 				= $accid;
						$incObj->acccode 			= $accounts[$accid];
						$incObj->crccode 			= $currency[$accid];
						$incObj->crcrate 			= Currency::getCurrencyRate($incObj->crccode);
						$incObj->datefrom 			= $object->datefrom;
						$incObj->dateto 			= $object->dateto;
						$incObj->rate 				= $object->rate;
						$incObj->totalwinlose 		= $incObj->crccode == 'VND' || $incObj->crccode == 'IDR'? $winloss * 1000 : $winloss;
						$incObj->totalwinloselocal 	= $incObj->crccode == 'VND' || $incObj->crccode == 'IDR'? $winloselocal[$accid] * 1000 : $winloselocal[$accid];
						$incObj->amount 			= $incObj->crccode == 'VND' || $incObj->crccode == 'IDR'? $amount * 1000 : $amount;
						$incObj->amountlocal 		= Currency::getLocalAmount($incObj->amount, $incObj->crccode);
						$incObj->save();
					
					}
				} */
				
				if( $cashledgers = Cashledger::whereChtcode(1)->whereBetween('created', array($object->datefrom.' 00:00:00', $object->dateto.' 23:59:59'))->whereRaw('status IN (1,4) and crccode = "'.$object->crccode.'" ')->orderBy('acccode','ASC')->get() )
				{
					
					foreach( $cashledgers as $cashledger )
					{
						
						$amount = App::float($cashledger->amount * $object->rate / 100);
						

						$incObj = new Cashback;
						$incObj->insid 				= $object->id;
						$incObj->clgid 				= $cashledger->id;
						$incObj->product 			= $object->product;
						$incObj->accid 				= $cashledger->accid;
						$incObj->acccode 			= $cashledger->acccode;
						$incObj->crccode 			= $cashledger->crccode;
						$incObj->crcrate 			= Currency::getCurrencyRate($incObj->crccode);
						$incObj->datefrom 			= $object->datefrom;
						$incObj->dateto 			= $object->dateto;
						$incObj->rate 				= $object->rate;
						$incObj->totalwinlose 		= 0;
						$incObj->totalwinloselocal 	= 0;
						$incObj->amount 			= $incObj->crccode == 'VND' || $incObj->crccode == 'IDR'? $amount * 1000 : $amount;
						$incObj->amountlocal 		= Currency::getLocalAmount($incObj->amount, $incObj->crccode);
						$incObj->save();
						
					}
					
				}
				
				
				
				$object->status = CBO_STANDARDSTATUS_ACTIVE;
				$object->save();
				
			}
			
		}
		echo json_encode(array('success' => $result));
		

	}
	
	public function showcashback(Request $request){

			$grid = new PanelGrid;
			$grid->setupGrid($this->moduleName, 'cashbacklist-data', $this->limit, false,array('params' => array('id' => $request->id),'checkbox'=>true,'checkbox_url'=>'cashbacklist-batchapprove'));
			$grid->setTitle(Lang::get('COMMON.CASHBACK'));
			
		/* 	$grid->addColumn('checkbox', 		'', 									18,		array('checkbox' => true));
			$grid->addColumn('id',				Lang::get('COMMON.ID'), 				50,		array('align' => 'right'));
			$grid->addColumn('acccode', 		Lang::get('COMMON.ACCOUNT'), 			80,		array());
			$grid->addColumn('currency', 		Lang::get('COMMON.CURRENCY'), 			60,		array('align' => 'center'));
			$grid->addColumn('winloss', 		Lang::get('COMMON.GROSSWINLOSS'), 			150,	array('align' => 'right'));
			$grid->addColumn('cashback', 		Lang::get('COMMON.CASHBACK'), 			150,	array('align' => 'right'));
			$grid->addColumn('taken_bonus', 	Lang::get('COMMON.TAKENBONUS'), 		150,	array('align' => 'right')); */
			$grid->addColumn('id',				Lang::get('COMMON.ID'), 				50,		array('align' => 'right'));
			$grid->addColumn('member', 			Lang::get('COMMON.MEMBER'), 					100,		array('align' => 'right'));					
			$grid->addColumn('from', 			Lang::get('COMMON.FROM'), 					100,		array('align' => 'center'));					
			$grid->addColumn('to', 			Lang::get('COMMON.TO'), 					100,		array('align' => 'center'));					
			$grid->addColumn('deposit', 		Lang::get('COMMON.DEPOSIT'), 					100,		array('align' => 'center'));			
			$grid->addColumn('withdrawal', 		Lang::get('COMMON.WITHDRAWAL'), 				100,		array('align' => 'center'));			
			$grid->addColumn('turnover', 		Lang::get('COMMON.TURNOVER'), 					100,		array('align' => 'center'));			
			$grid->addColumn('profitloss', 		Lang::get('COMMON.PROFITLOSS'), 				100,		array('align' => 'center'));			
			$grid->addColumn('bonus', 			Lang::get('COMMON.BONUS'), 						100,		array('align' => 'center'));			
			$grid->addColumn('incentive', 		Lang::get('COMMON.INCENTIVE'), 					100,		array('align' => 'center'));			
			$grid->addColumn('cashback', 		Lang::get('COMMON.CASHBACK'), 				100,		array('align' => 'center'));			
			

			$grid->addButton('2', Lang::get('COMMON.APPROVE'), '', 'button', array('icon' => 'ok', 'url' => 'approve_batch(\'approve\');'));

			$data['grid'] = $grid->getTemplateVars();

			return view('admin.grid2',$data);
    }	
	

	protected function doGetCashbacklistData(Request $request){
		
		$rows = array();
		$condition = 'insid = '.$request->id;
		
		$total = Cashback::whereRaw($condition)->count();

		$total_cashback = 0;
		

		if($cashbacks = Cashback::whereRaw($condition)->skip($request->recordstartindex)->take( $request->pagesize )->orderBy('id','desc')->get()){
			foreach( $cashbacks as $cashback ){
		
				$accObj = Account::find($cashback->accid);
				
				$cashLedgerObj = Cashledger::find($cashback->clgid);
				
				$nextDate  = Cashledger::whereRaw('chtcode = 1 and accid = '.$cashback->accid.' and created > "'.$cashLedgerObj->created.'" ')->pluck('created');
				
				if(!$nextDate){
					$nextDate = date('Y-m-d H:i:s');
				}else{
					
					$nextDate = $nextDate->toDateTimeString();
				}
				
				
				
				$profitloss = Wager::whereRaw('category != 1')->whereAccid($cashback->accid)->whereBetween('datetime', array($cashLedgerObj->created, $nextDate))->sum('profitloss');
				
				$turnover   = Wager::whereRaw('category != 1')->whereAccid($cashback->accid)->whereBetween('datetime', array($cashLedgerObj->created, $nextDate))->sum('validstake');
				
				$bonus 	    = Cashledger::whereChtcode(CBO_CHARTCODE_BONUS)->whereAccid($cashback->accid)->whereBetween('created', array($cashLedgerObj->created, $nextDate))->whereRaw('status IN (1,4) AND refid IN (select id from promocash where pcpid != 6) ')->sum('amount');
				
				$incentive  = Cashledger::whereChtcode(CBO_CHARTCODE_INCENTIVE)->whereAccid($cashback->accid)->whereBetween('created', array($cashLedgerObj->created, $nextDate))->whereRaw('status IN (1,4)')->sum('amount');
				
				if($profitloss<0){
				
					$rows[] = array(
						'id' 			=> $cashback->id,
						'member' 		=> $accObj->nickname,
						'from' 		=> $cashLedgerObj->created->toDateTimeString(),
						'to' 		=> $nextDate,
						'deposit' 		=> App::formatNegativeAmount($cashLedgerObj->amount),
						'withdrawal' 	=> App::formatNegativeAmount(Cashledger::whereChtcode(2)->whereAccid($cashback->accid)->whereBetween('created', array($cashLedgerObj->created, $nextDate))->whereRaw('status IN (1,4)')->sum('amount')),
						'turnover' 	    => App::formatNegativeAmount($turnover),
						'netpnl' 		=> App::formatNegativeAmount($profitloss + ($bonus + $incentive)),
						'profitloss' 	=> App::formatNegativeAmount($profitloss),
						'bonus' 		=> App::formatNegativeAmount($bonus),
						'incentive' 	=> $incentive,
						'cashback' 		=> $cashback->amount,
		
					);
				}
				//array_multisort($volume, SORT_DESC, $edition, SORT_ASC, $data);
				//$total_cashback += $cashback->amount;
				
			}
				
		}
				
		echo json_encode(array('total' => $total, 'rows' => $rows));
		exit;
	
	}
	
	public function batchApprove(Request $request){
		
		$settings = array();
		$success  = false;
		$trans    = array();
		$count    = 1;
	
		foreach( $request->rows as $row ){
			$countkey = false;
			
		
			if($object = Cashback::find($row)) 
			{
				if(!isset($settings[$object->insid])) 
				{
					$insObj = Cashbacksetting::find($object->insid);
					$settings[$object->insid]['minpayout'] = $insObj->minpayout;
					$settings[$object->insid]['maxpayout'] = $insObj->maxamount;
				}
				
				foreach($trans as $key2 => $tran2 )
				{
					if($tran2['accid'] == $object->accid)
					{
						$countkey = $key2;
					}
					
				}
	
				if($countkey == false)
				{
				
					$trans[$count]['accid']         = $object->accid;
					$trans[$count]['acccode']       = $object->acccode;
					$trans[$count]['amount']        = $object->amount;
					$trans[$count]['amountlocal']   = $object->amountlocal;
					$trans[$count]['id']            = $object->id;
					$trans[$count]['crccode']       = $object->crccode;
					
					if($trans[$count]['amount'] >= $settings[$object->insid]['maxpayout'] )
					{
						$trans[$count]['amount']      = $settings[$object->insid]['maxpayout'];
						$trans[$count]['amountlocal'] = $settings[$object->insid]['maxpayout'];
					}
				
				}
				else
				{
					
					$trans[$countkey]['amount']        	+= $object->amount;
					$trans[$countkey]['amountlocal']       += $object->amountlocal;
					
					if($trans[$countkey]['amount'] >= $settings[$object->insid]['maxpayout'] )
					{
						$trans[$countkey]['amount']      = $settings[$object->insid]['maxpayout'];
						$trans[$countkey]['amountlocal'] = $settings[$object->insid]['maxpayout'];
					}
					
				}
				

				$count++;
			
				
			}
	
		}
		
		 foreach( $trans as  $key => $tran )
		{
				$accObj = Account::find($object->accid);
		
				$cashLedgerObj = new Cashledger;
				$cashLedgerObj->accid 			= $tran['accid'];
				$cashLedgerObj->acccode 		= $tran['acccode'];
				$cashLedgerObj->accname			= $accObj->fullname;
				$cashLedgerObj->chtcode 		= CBO_CHARTCODE_CASHBACK;
				$cashLedgerObj->cashbalance 	= $cashLedgerObj->getBalance($tran['accid']);;
				$cashLedgerObj->amount 			= $tran['amount'];
				$cashLedgerObj->amountlocal 	= $tran['amountlocal'];
				$cashLedgerObj->refid 			= $tran['id'];
				$cashLedgerObj->status 			= CBO_LEDGERSTATUS_PENDING;
				$cashLedgerObj->refobj 			= 'Cashback';
				$cashLedgerObj->crccode 		= $tran['crccode'];
				$cashLedgerObj->crcrate 		= Currency::getCurrencyRate($tran['crccode']);
				$cashLedgerObj->fee 			= $tran['amount'];
				$cashLedgerObj->feelocal 		= $tran['amountlocal'];
				$cashLedgerObj->createdip		= App::getRemoteIp();
				
				if($cashLedgerObj->save()) 
				{
					$success = true;
				}
				
		} 
		
		if( $success )
			echo json_encode( array( 'code' => Lang::get('COMMON.SUCESSFUL') ) );
		else
			echo json_encode( array( 'code' => Lang::get('COMMON.FAILED') ) );
		
	}

}

?>
