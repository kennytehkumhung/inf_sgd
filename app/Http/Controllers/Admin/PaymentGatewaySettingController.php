<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller\Admin;
use App\libraries\App;
use App\libraries\grid\PanelGrid;
use App\Models\Cashledger;
use App\Models\Configs;
use App\Models\Media;
use App\Models\Paymentgatewaysetting;
use File;
use Illuminate\Http\Request;
use Lang;
use Storage;
use Validator;

class PaymentGatewaySettingController extends Controller
{

    protected $moduleName = 'paymentgatewaysetting';
    protected $limit = 20;
    protected $start = 0;

    public function __construct()
    {

    }

    public function paymentgatewaysettingShow(Request $request)
    {
        $grid = new PanelGrid;
        $grid->setupGrid($this->moduleName, 'paymentgatewaysetting-data', $this->limit);
        $grid->setTitle(Lang::get('COMMON.PAYMENTGATEWAYSETTING'));

        $grid->addColumn('code', Lang::get('COMMON.CODE'), 80, array());
        $grid->addColumn('name', Lang::get('COMMON.NAME'), 160, array());
        $grid->addColumn('real_name', Lang::get('public.FullName'), 160, array());
        $grid->addColumn('seq', Lang::get('public.Sequence'), 80, array());
        $grid->addColumn('group_name', Lang::get('public.GroupName'), 160, array());
        $grid->addColumn('status', Lang::get('COMMON.STATUS'), 160, array('align' => 'center'));
        $grid->addColumn('deposit', Lang::get('COMMON.DEPOSIT'), 160, array());
        $grid->addColumn('withdraw', Lang::get('COMMON.WITHDRAWAL'), 160, array());

        $data['grid'] = $grid->getTemplateVars();

        return view('admin.grid2', $data);
    }

    protected function paymentgatewaysettingdoGetData(Request $request)
    {
    	$builder = Paymentgatewaysetting::where('seq', '>', -1)
            ->where('is_private', '=', 0)
            ->orderBy('seq', 'asc')
            ->orderBy('name', 'asc');

        $total = $builder->count();
        $rows = array();

        if ($pgsObjs = $builder->get()) {
            $statusOptions = collect(Paymentgatewaysetting::getStatusOptions());

            foreach ($pgsObjs as $pgsObj) {
                $rows[] = array(
                    'code' => App::formatAddTabUrl( Lang::get('COMMON.PAYMENTGATEWAYSETTING').': '.$pgsObj->code, urlencode($pgsObj->code), action('Admin\PaymentGatewaySettingController@paymentgatewaysettingdrawAddEdit',  array('sid' => $pgsObj->id))),
                    'name' => $pgsObj->name,
                    'real_name' => $pgsObj->real_name,
                    'seq' => $pgsObj->seq,
                    'group_name' => $pgsObj->group_name,
                    'status' => $statusOptions->get($pgsObj->status),
                    'deposit' => ($pgsObj->deposit == 1 ? 'Y' : 'N'),
                    'withdraw' => ($pgsObj->withdraw == 1 ? 'Y' : 'N'),
                    'deposit_min' => $pgsObj->deposit_min,
                    'deposit_max' => $pgsObj->deposit_max,
                    'withdraw_min' => $pgsObj->withdraw_min,
                    'withdraw_max' => $pgsObj->withdraw_max,
                );
            }
        }

        echo json_encode(array('total' => $total, 'rows' => $rows));
        exit;
    }

    protected function paymentgatewaysettingdrawAddEdit(Request $request)
    {
        $toAdd = true;
        $object = null;
        $readOnly = false;
        $values = array();

        $title = Lang::get('COMMON.EDIT') . ' ' . Lang::get('COMMON.PAYMENTGATEWAYSETTING');

        if ($request->has('sid')) {
            if ($object = Paymentgatewaysetting::find($request->input('sid'))) {
                $toAdd = false;
                $readOnly = true;
            } else {
                $object = null;
            }
        }

        $yesnoOption = array('1' => Lang::get('COMMON.YES'), '0' => Lang::get('COMMON.NO'));

        $form = App::setupPanelForm('doadd', $request->all(), array(), $toAdd);
        $form->setFormTitle($title);

        $form->addInput('showonly', Lang::get('COMMON.CODE'), 'type', (($object) ? $object->code : ''), array(), false);
        $form->addInput('text', Lang::get('COMMON.NAME'), 'name', (($object) ? $object->name : ''), array(), true);
        $form->addInput('text', Lang::get('public.FullName'), 'real_name', (($object) ? $object->real_name : ''), array(), true);
        $form->addInput('text', Lang::get('public.Sequence'), 'seq', (($object) ? $object->seq : '99'), array(), true);
        $form->addInput('showonly', Lang::get('public.GroupName'), 'group_name', (($object) ? $object->group_name : ''), array(), false);
        $form->addInput('radio', 	Lang::get('COMMON.STATUS')	    , 'status', 	 (($object)? $object->status:''),	    array('options' => Paymentgatewaysetting::getStatusOptions()), true);
        $form->addInput('radio', 	Lang::get('COMMON.DEPOSIT')	    , 'deposit', 	 (($object)? $object->deposit:''),	    array('options' => $yesnoOption), true);
        $form->addInput('radio', 	Lang::get('COMMON.WITHDRAWAL')	    , 'withdraw', 	 (($object)? $object->withdraw:''),	    array('options' => $yesnoOption), true);
        $form->addInput('text', Lang::get('public.MinDeposit'), 'deposit_min', (($object) ? $object->deposit_min : ''), array(), true);
        $form->addInput('text', Lang::get('public.MaxDeposit'), 'deposit_max', (($object) ? $object->deposit_max : ''), array(), true);
        $form->addInput('text', Lang::get('public.MinWithdrawal'), 'withdraw_min', (($object) ? $object->withdraw_min : ''), array(), true);
        $form->addInput('text', Lang::get('public.MaxWithdrawal'), 'withdraw_max', (($object) ? $object->withdraw_max : ''), array(), true);

        $data['form'] = $form->getTemplateVars();
        $data['module'] = 'paymentgatewaysetting';

        return view('admin.form2', $data);
    }

    protected function paymentgatewaysettingdoAddEdit(Request $request)
    {
        $validator = Validator::make(
            [
                'name' => $request->input('name'),
                'real_name' => $request->input('real_name'),
                'seq' => $request->input('seq'),
                'status' => $request->input('status'),
                'deposit' => $request->input('deposit'),
                'withdraw' => $request->input('withdraw'),
                'deposit_min' => $request->input('deposit_min'),
                'deposit_max' => $request->input('deposit_max'),
                'withdraw_min' => $request->input('withdraw_min'),
                'withdraw_max' => $request->input('withdraw_max'),
            ],
            [
                'name' => 'required',
                'real_name' => 'required',
                'seq' => 'required|integer|min:0|max:99',
                'status' => 'required',
                'deposit' => 'required',
                'withdraw' => 'required',
                'deposit_min' => 'required|integer|min:0',
                'deposit_max' => 'required|integer|min:0',
                'withdraw_min' => 'required|integer|min:0',
                'withdraw_max' => 'required|integer|min:0',
            ]
        );

        if ($validator->fails()) {
            echo json_encode($validator->errors());
            exit;
        }

        $object = new Paymentgatewaysetting();

        if ($request->has('sid')) {
            $object = Paymentgatewaysetting::find($request->input('sid'));
        } else {
            // Not allow to add.
            echo json_encode(array('code' => Lang::get('COMMON.FAILED')));
            exit;
        }

        $object->name = $request->input('name');
        $object->real_name = $request->input('real_name');
        $object->seq = $request->input('seq');
        $object->status = $request->input('status');
        $object->deposit = $request->input('deposit');
        $object->withdraw = $request->input('withdraw');
        $object->deposit_min = $request->input('deposit_min');
        $object->deposit_max = $request->input('deposit_max');
        $object->withdraw_min = $request->input('withdraw_min');
        $object->withdraw_max = $request->input('withdraw_max');

        if ($object->save()) {
            echo json_encode(array('code' => Lang::get('COMMON.SUCESSFUL')));
            exit;
        }

        echo json_encode(array('code' => Lang::get('COMMON.FAILED')));
    }

    public function showPrivate(Request $request)
    {
        $grid = new PanelGrid;
        $grid->setupGrid($this->moduleName, 'privatepaymentgatewaysetting-data', $this->limit);
        $grid->setTitle(Lang::get('COMMON.MANUALPAYMENTGATEWAY'));

        $grid->addColumn('code', 				Lang::get('COMMON.CODE'),						68,	array('align' => 'left'));
        $grid->addColumn('name', 				Lang::get('COMMON.NAME'),						150,	array('align' => 'left'));
        $grid->addColumn('real_name', 		Lang::get('public.FullName'),						150,	array('align' => 'left'));
        $grid->addColumn('private_type', 			Lang::get('COMMON.TYPE'),					68,	array('align' => 'left'));
        $grid->addColumn('seq', Lang::get('public.Sequence'), 80, array());
        $grid->addColumn('group_name', Lang::get('public.GroupName'), 160, array());
        $grid->addColumn('deposit_min', 			Lang::get('COMMON.MINDEPOSIT'),					150,	array('align' => 'left'));
        $grid->addColumn('deposit_max', 			Lang::get('COMMON.MAXDEPOSIT'),					150,	array('align' => 'left'));
        $grid->addColumn('status', 				Lang::get('COMMON.STATUS'),						150,	array('align' => 'left'));

        $grid->addFilter('status',Paymentgatewaysetting::getStatusOptions(), array('display' => Lang::get('COMMON.TYPE')));
        $grid->addButton('1', 'New', Lang::get('COMMON.ADDNEW').' '.Lang::get('COMMON.MANUALPAYMENTGATEWAY'), 'button', array('icon' => 'add', 'url' => action('Admin\PaymentGatewaySettingController@drawAddEditPrivate')));

        $data['grid'] = $grid->getTemplateVars();

        return view('admin.grid2',$data);
    }

    protected function doGetPrivateData(Request $request){

        //status
        $status[1] = 'Active';
        $status[0] = 'Suspended';

        $rows = array();

        $aflt  = $request->input('aflt');
        $aqfld = $request->input('aqfld');

        $builder = Paymentgatewaysetting::where('is_private', '=', 1);

        if( isset($aqfld['status']) && $aqfld['status'] != ''){
            $builder->where('status', '=', $aqfld['status']);
        }

        $total = $builder->count();

        if($pgSettings = $builder->skip($request->recordstartindex)->take( $request->pagesize )->orderBy('id','desc')->get()){
            $privateTypeOptions = collect(Paymentgatewaysetting::getPrivateTypeOptions());

            foreach( $pgSettings as $pgSetting ){
                $body = App::formatAddTabUrl( Lang::get('COMMON.MANUALPAYMENTGATEWAY').': '.$pgSetting->id, urlencode($pgSetting->name), action('Admin\PaymentGatewaySettingController@drawAddEditPrivate',  array('sid' => $pgSetting->id)));
                $rows[] = array(
                    'code'  			=> $pgSetting->code,
                    'name'  			=> urldecode($body),
                    'real_name'  		=> $pgSetting->real_name,
                    'private_type'  	=> $privateTypeOptions->get($pgSetting->private_type, $pgSetting->private_type),
                    'seq' 				=> $pgSetting->seq,
                    'group_name' 		=> $pgSetting->group_name,
                    'deposit_min'  		=> $pgSetting->deposit_min,
                    'deposit_max'  		=> $pgSetting->deposit_max,
                    'status'  			=> $pgSetting->getStatusText('privatepaymentgatewaysetting')
                );
            }
        }

        return response()->json(array('total' => $total, 'rows' => $rows));
    }

    protected function drawAddEditPrivate(Request $request) {

        $toAdd 	  = true;
        $object   = null;
        $readOnly = false;

        $title = Lang::get('COMMON.NEW').' '.Lang::get('COMMON.MANUALPAYMENTGATEWAY');

        if($request->has('sid'))
        {

            if ($object = Paymentgatewaysetting::find($request->input('sid')))
            {
                $title    = Lang::get('COMMON.EDIT').' '.Lang::get('COMMON.MANUALPAYMENTGATEWAY');
                $toAdd    = false;
                $readOnly = true;

            }
            else
            {
                $object = null;
            }

        }

        $privateTypeOptions = Paymentgatewaysetting::getPrivateTypeOptions();

        $form = App::setupPanelForm('doadd', $request->all(), array(), $toAdd);
        $form->setFormTitle($title);

        $form->addInput('text', 	Lang::get('COMMON.NAME') 		, 'name', 		 (($object)? $object->name:''), 	    	   array(), 	 true);
        $form->addInput('text', 	Lang::get('public.FullName') 		, 'real_name', 		 (($object)? $object->real_name:''), 	    	   array(), 	 true);
//        $form->addInput('text', 	Lang::get('COMMON.CODE') 			, 'code', 		 	 (($object)? $object->code:''), 					   array(), 	 true);
        $form->addInput('select', 	Lang::get('COMMON.TYPE')			  , 'private_type',      (($object)? $object->private_type:''),       array('options' => $privateTypeOptions), 	 true);
        $form->addInput('text', Lang::get('public.Sequence'), 'seq', (($object) ? $object->seq : '99'), array(), true);
        $form->addInput('showonly', Lang::get('public.GroupName'), 'group_name', (($object) ? $object->group_name : ''), array(), false);
        $form->addInput('text',	    Lang::get('COMMON.MINDEPOSIT') 		, 'deposit_min', 	 (($object)? $object->deposit_min:''), 				   array(), 	 true);
        $form->addInput('text', 	Lang::get('COMMON.MAXDEPOSIT') 		, 'deposit_max', 	 (($object)? $object->deposit_max:''),  				   array(), 	 false);
        $form->addInput('file', 	Lang::get('COMMON.IMAGE')			, 'image',    	     (($object)? $object->getImage(Media::TYPE_IMAGE):''), array(), 	 false);
        $form->addInput('text', 	Lang::get('COMMON.URL') 		, 'url', 	 (($object)? $object->url:''),  				   array(), 	 false);
        $form->addInput('radio', 	Lang::get('COMMON.STATUS')	    	, 'status', 	 	 (($object)? $object->status:''),	    			   array('options' => Paymentgatewaysetting::getStatusOptions()), true);
        $form->addInput('textarea', Lang::get('COMMON.REMARK') 		        , 'private_remark', 		 	 (($object)? $object->remark:''), 				array(), true);

        $data['form'] = $form->getTemplateVars();
        $data['module'] = 'privatepaymentgatewaysetting';

        return view('admin.form2',$data);
    }

    protected function doAddEditPrivate(Request $request) {

        $validator = Validator::make(
            [
                'private_type'        => $request->input('private_type'),
                'deposit_min'	  	  => $request->input('deposit_min'),
                'deposit_max'	  	  => $request->input('deposit_max'),
                'seq'                 => $request->input('seq'),
                'status' 	  		  => $request->input('status'),
            ],
            [
                'private_type' 	       => 'required',
                'deposit_min'	   	   => 'required|numeric|min:0',
                'deposit_max'	  	   => 'required|numeric|min:0',
                'seq'                  => 'required|integer|min:0|max:99',
                'status' 	           => 'required',
            ],
            [],
            [
                'private_type' 	       => Lang::get('COMMON.TYPE'),
                'deposit_min'	   	   => Lang::get('COMMON.MINDEPOSIT'),
                'deposit_max'	  	   => Lang::get('COMMON.MAXDEPOSIT'),
                'seq'                  => Lang::get('public.Sequence'),
                'status' 	           => Lang::get('COMMON.STATUS'),
            ]
        );

        if ($validator->fails())
        {
            echo json_encode($validator->errors());
            exit;
        }

        $object  = new Paymentgatewaysetting();

        if($request->has('sid'))
        {
            $object = Paymentgatewaysetting::find($request->input('sid'));
        } else {
            // Generate code.
            $object->code = Paymentgatewaysetting::generateCode($request->input('private_type'));
        }

        $bnkpgsObject = Paymentgatewaysetting::where('code', '=', 'ALP')->first();

        if ($bnkpgsObject) {
            // Private PG use same group as BNK (BANK).
            $object->group_code = $bnkpgsObject->group_code;
            $object->group_name = $bnkpgsObject->group_name;
        }

        $object->instant_trans_flag = Cashledger::INSTANT_TRANS_FLAG_DEFAULT;
        $object->is_private         = 1;
        $object->private_type	 	= $request->input('private_type');
        $object->seq	 			= $request->input('seq');
        $object->name	 			= $request->input('name');
        $object->real_name	 		= $request->input('real_name');
        $object->deposit_min	 	= $request->input('deposit_min');
        $object->deposit_max	 	= $request->input('deposit_max');
        $object->url 				= $request->input('url');
        $object->status 			= $request->input('status');
        $object->private_remark 	= $request->input('private_remark');
        $object->deposit 			= 1;

        if( $object->save() )
        {
            if( $request->hasFile('image'))
            {
                $file = $request->file('image');

                $filename = 'paymentgatewayprivate/'.md5($file->getClientOriginalName().time()).'.'. $file->getClientOriginalExtension();
                $result = Storage::disk('s3')->put($filename,  File::get($file), 'public');

                if($result)
                {
                    if( Media::where( 'refobj' , '=' , 'PaymentGatewayObject' )->where( 'refid' , '=' , $object->id )->count() == 0 ){
                        $medObj = new Media;
                    }else{
                        $medObj = Media::where( 'refobj' , '=' , 'PaymentGatewayObject' )->where( 'refid' , '=' , $object->id )->first();
                    }

                    $medObj->refobj = 'PaymentGatewayObject';
                    $medObj->refid  = $object->id;
                    $medObj->type   = Media::TYPE_IMAGE;
                    $medObj->domain = 'https://'.Configs::getParam('SYSTEM_AWS_S3BUCKET');
                    $medObj->path   = $filename;
                    $medObj->save();
                }
            }

            if ($object->status == CBO_ACCOUNTSTATUS_ACTIVE) {
                // Suspend all others. 1 time only allow to enable 1 gateway.
                Paymentgatewaysetting::where('status', '=', CBO_ACCOUNTSTATUS_ACTIVE)
                    ->where('id', '!=', $object->id)
                    ->where('is_private', '=', 1)
                    ->where('private_type', '=', $object->private_type)
                    ->update(array(
                        'status' => CBO_ACCOUNTSTATUS_SUSPENDED,
                    ));
            }

            echo json_encode( array( 'code' => Lang::get('COMMON.SUCESSFUL') ) );
            exit;
        }

        echo json_encode( array( 'code' => Lang::get('COMMON.FAILED') ) );
    }

    protected function doActivateSuspend(Request $request) {

        if($request->has('id')) {
            if($object = Paymentgatewaysetting::find($request->id)) {
                $currentStatus = $object->status;

                if($request->action == 'activate') {
                    // Suspend all other private settings. 1 time only allow to enable 1 gateway.
                    Paymentgatewaysetting::where('status', '=', CBO_ACCOUNTSTATUS_ACTIVE)
                        ->where('is_private', '=', 1)
                        ->where('private_type', '=', $object->private_type)
                        ->update(array(
                            'status' => CBO_ACCOUNTSTATUS_SUSPENDED,
                        ));

                    $object->status 	= CBO_ACCOUNTSTATUS_ACTIVE;
                }

                if($request->action == 'suspend') {
                    $object->status 	= CBO_ACCOUNTSTATUS_SUSPENDED;
                }

                if($currentStatus != $object->status){
                    if($object->save()) {
                        echo json_encode( array( 'code' => Lang::get('COMMON.SUCESSFUL') ) );
                        exit;
                    }
                }
            }
        }

        echo json_encode( array( 'code' => Lang::get('COMMON.FAILED') ) );
    }
}
