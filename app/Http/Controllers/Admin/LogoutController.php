<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller\Admin;
use Auth;
use Session;
use Illuminate\Http\Request;
use App\Models\Onlinetracker;
use Redirect;

class LogoutController extends Controller{
	
	public function __construct()
	{
		
	}

	public function index(Request $request)
	{
		Onlinetracker::whereType(CBO_LOGINTYPE_ADMIN)->whereUserid(Session::get('admin_userid'))->delete();
		Auth::admin()->logout();
		return redirect::route('admin_login');
	}	
	

	
}
