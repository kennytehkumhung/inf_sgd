<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller\Admin;
use App\libraries\grid\PanelGrid;
use App\libraries\App;
use Illuminate\Http\Request;
use Auth;
use Validator;
use Session;
use Lang;
use Config;
use App\Models\Banner;
use App\Models\Media;
use App\Models\Language;
use App\Models\Currency;
use App\Models\Bank;
use App\Models\Configs;
use Storage;
use File;
use Cache;

class BannerController extends Controller{


	protected $moduleName 	= 'banner';
	protected $limit 		= 20;
	protected $start 		= 0;
	
	public function __construct()
	{
		
	}
	
	public function index(Request $request){

			$grid = new PanelGrid;
			$grid->setupGrid($this->moduleName, 'banner-data', $this->limit);
			$grid->setTitle(Lang::get('COMMON.BANNER'));
									
			$grid->addColumn('name',		Lang::get('COMMON.NAME'), 				150,	array('align' => 'left'));
			$grid->addColumn('rank', 		Lang::get('COMMON.RANK'), 				90,		array('align' => 'center'));
			$grid->addColumn('lang', 		Lang::get('COMMON.LANGUAGE'), 			120,	array('align' => 'center'));
			$grid->addColumn('url', 		Lang::get('COMMON.URL'), 				350,	array('align' => 'center'));
			$grid->addColumn('crccode', 	Lang::get('COMMON.CURRENCY'), 			160,	array('align' => 'center'));	
			$grid->addColumn('type', 		Lang::get('COMMON.TYPE'), 				150,	array('align' => 'center'));			
			$grid->addColumn('categories', 	Lang::get('COMMON.CATEGORY'), 			100,	array('align' => 'center'));		
			$grid->addColumn('image', 	Lang::get('COMMON.IMAGE'), 				80,		array('align' => 'center'));
			$grid->addColumn('status', 		Lang::get('COMMON.STATUS'), 			80,		array('align' => 'center'));

			
			$grid->addButton('1', Lang::get('COMMON.NEW'), Lang::get('COMMON.ADD'), 'button', array('icon' => 'add', 'url' => action('Admin\BannerController@drawAddEdit')));

			$data['grid'] = $grid->getTemplateVars();

			return view('admin.grid2',$data);
    }
	
	protected function doGetData(Request $request){
		$condition = '1';
		//types
		$types[0] = 'none';
		$types[1] = 'Redirect';
		$types[2] = 'Pop Up';
		//status
		$status[1] = 'Active';
		$status[0] = 'Suspended';
		
		$condition ='1';
		$condition .= Session::get('admin_crccode') == 'MYR' ? '' : ' AND crccode = "'.Session::get('admin_crccode').'"';
		
		$rows = array();
		
		$total = Banner::whereRaw($condition)->count();

		//new structure
		if($banners = Banner::whereRaw($condition)->skip($request->recordstartindex)->take( $request->pagesize )->orderBy('status','desc')->orderBy('rank','asc')->orderBy('crccode','asc')->get()){
			foreach( $banners as $banner ){
		
				$rows[] = array(

					'name' 			=> App::formatAddTabUrl(Lang::get('COMMON.NAME').': '.$banner->name, $banner->name, action('Admin\BannerController@drawAddEdit',array('sid' => $banner->id))),
					'rank' 	  		=> $banner->rank,
					'lang' 	  		=> $banner->lang,
					'crccode'		=> $banner->crccode,
					'url' 	  		=> '<a target="_blank" href="'.$banner->url.'">'.$banner->url.'</a>',
					'categories'	=> $banner->categories,
					'type' 			=> $types[$banner->type],
					'image'   		=> '<a href="'.$banner->getImage(Media::TYPE_IMAGE).'" target="_blank">View</a>',
					'status'  		=> $banner->getStatusText($this->moduleName), 
				);	
				
			}
				
		}

				
		echo json_encode(array('total' => $total, 'rows' => $rows));
		exit;
	
	}
	
	protected function drawAddEdit(Request $request){
	
		$toAdd 	  = true;
		$object   = null;
		$readOnly = false;
		$values   = array();

		$title = Lang::get('COMMON.NEW').' '.Lang::get('COMMON.BANNER');
		if($request->has('sid')) {
	
				if ($object = Banner::find($request->input('sid'))) {
					$title = Lang::get('COMMON.EDIT').' '.Lang::get('COMMON.BANNER');
					$toAdd = false;
					$readOnly = true;
				} else
				$object = null;
			
		}
		
		$categories[1] = 1;
		$categories[2] = 2;
		$categories[3] = 3;		
		$categories[4] = 4;
		
		$types[1] = 'Redirect';
		$types[2] = 'Pop Up';

		$languageOptions = Language::getAllLanguageAsOptions();
		App::arrayUnshift($languageOptions, Lang::get('COMMON.SELECT'));
		
		$currencies = Currency::getAllCurrencyAsOptions();
		App::arrayUnshift($currencies, Lang::get('COMMON.SELECT'));
		
		$form = App::setupPanelForm('doadd', $request->all(), array(), $toAdd);
		$form->setFormTitle($title);
		$form->addInput('text', 	Lang::get('COMMON.NAME'), 		'name',           (($object)? $object->name:''),           				array(), 	 true);
		$form->addInput('select', 	Lang::get('COMMON.LANGUAGE'),   'lang',           (($object)? $object->lang:''),           				array('options' => $languageOptions), 	 true);
		$form->addInput('select', 	Lang::get('COMMON.CURRENCY'),   'crccode',        (($object)? $object->crccode:''),        				array('options' => $currencies), 	 true);
		$form->addInput('text', 	Lang::get('COMMON.RANK'),   	'rank',           (($object)? $object->rank:''),           				array(), 	 true);
		$form->addInput('text', 	Lang::get('COMMON.URL'),   		'url',            (($object)? $object->url:''),            				array(), 	 true);
		$form->addInput('select', 	Lang::get('COMMON.CATEGORY'), 	'categories',     (($object)? $object->categories:''),     				array('options' => $categories), 	 true);
		$form->addInput('select', 	Lang::get('COMMON.TYPE'), 	    'type',    		  (($object)? $object->type:''),      					array('options' => $types), 	 true);
		$form->addInput('file', 	Lang::get('COMMON.IMAGE'), 	    'image',    	  (($object)? $object->getImage(Media::TYPE_IMAGE):''), array(), 	 true);

        if (!is_null(Config::get('setting.front_mobile_path'))) {
            $form->addInput('file', Lang::get('public.ImageForMobile'), 'image_for_mobile', (($object) ? $object->getImage(Media::TYPE_IMAGE_MOBILE) : ''), array(), false);
        }

        $form->addInput('radio', 	Lang::get('COMMON.STATUS'), 	'status',    	  (($object)? $object->status:''), 						array('options' => Bank::getStatusOptions()), 	 true);

		$data['form']   = $form->getTemplateVars();
		$data['module'] = 'banner';

		return view('admin.form2',$data);

	}
	
	protected function doAddEdit(Request $request){
		
		$validator = Validator::make(
			[
	
				'name'	       => $request->input('name'),
				'lang'	   	   => $request->input('lang'),
				'crccode'  	   => $request->input('crccode'),
				'rank'         => $request->input('rank'),
				'url'      	   => $request->input('url'),
				'categories'   => $request->input('categories'),
				'type'         => $request->input('type'),
				'status' 	   => $request->input('status'),
			],
			[

			   'name'	     => 'required',
			   'lang'	   	 => 'required',
			   'crccode'  	 => 'required',
			   'rank'        => 'required',
			   'url'      	 => 'required',
			   'categories'  => 'required',
			   'type'        => 'required',
               'status' 	 => 'required',
			   
			]
		);
		
	
		if ($validator->fails())
		{
			echo json_encode($validator->errors());
			exit;
		}
		
		if(!$request->has('sid')){

			$validator = Validator::make(
				[
					'image'	     => $request->file('image'),
				],
				[

				   'image'	     => 'required',
				]
			);

		}
		
		if ($validator->fails())
		{
			echo json_encode($validator->errors());
			exit;
		}
		

		$object  = new Banner;
		
		if($request->has('sid')) 
		{
			$object = Banner::find($request->input('sid'));
		}
		
		Cache::forget('Banner_'.$request->input('lang').'_'.$request->input('crccode'));
		Cache::forget('Banner_Mobile_'.$request->input('lang').'_'.$request->input('crccode'));
		

		$object->name	 	= $request->input('name');
		$object->lang	 	= $request->input('lang');
		$object->crccode	= $request->input('crccode');
		$object->rank		= $request->input('rank');
		$object->url		= $request->input('url');
		$object->categories	= $request->input('categories');
		$object->type		= $request->input('type');
		$object->status		= $request->input('status');

		if( $object->save() )
		{
            $fileInputs = array(
                'image' => Media::TYPE_IMAGE,
                'image_for_mobile' => Media::TYPE_IMAGE_MOBILE,
            );

            foreach ($fileInputs as $imgKey => $imgType) {
                if( $request->hasFile($imgKey))
                {
                    $file = $request->file($imgKey);

                    $filename = 'banner/'.md5($file->getClientOriginalName().time()).'.'. $file->getClientOriginalExtension();
                    $result = Storage::disk('s3')->put($filename,  File::get($file), 'public');


                    if($result)
                    {
                        if( Media::where( 'refobj' , '=' , 'Banner' )->where( 'refid' , '=' , $object->id )->where( 'type' , '=' , $imgType )->count() == 0 ){
                            $medObj = new Media;
                        }else{
                            $medObj = Media::where( 'refobj' , '=' , 'Banner' )->where( 'refid' , '=' , $object->id )->where( 'type' , '=' , $imgType )->first();
                        }

                        $medObj->refobj = 'Banner';
                        $medObj->refid  = $object->id;
                        $medObj->type   = $imgType;
                        $medObj->domain = 'https://'.Configs::getParam('SYSTEM_AWS_S3BUCKET');
                        $medObj->path   = $filename;
                        $medObj->save();
                    }
                }
            }

			echo json_encode( array( 'code' => Lang::get('COMMON.SUCESSFUL') ) );
			exit;
		}


		echo json_encode( array( 'code' => Lang::get('COMMON.FAILED') ) );
	}
	
	protected function doActivateSuspend(Request $request) {

			

		if($request->has('id')) {
		
			if($object = Banner::find($request->id)) {
			
				$currentStatus = $object->status;
				if($request->action == 'activate') {
					$object->status 	= CBO_ACCOUNTSTATUS_ACTIVE;
				}
				if($request->action == 'suspend') {
					$object->status 	= CBO_ACCOUNTSTATUS_SUSPENDED;
				}

				if($currentStatus <> $object->status){
					if($object->save()) {
						Cache::forget('Banner_'.$object->lang.'_'.$object->crccode);
						Cache::forget('Banner_Mobile_'.$object->lang.'_'.$object->crccode);
						echo json_encode( array( 'code' => Lang::get('COMMON.SUCESSFUL') ) );
						exit;
					}
				}
			}
		}
		
		
		echo json_encode( array( 'code' => Lang::get('COMMON.FAILED') ) );

	}

}

?>
