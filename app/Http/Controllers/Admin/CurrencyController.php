<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller\Admin;
use App\libraries\grid\PanelGrid;
use App\libraries\App;
use Illuminate\Http\Request;
use Auth;
use Validator;
use Session;
use Lang;
use App\Models\Currency;
use App\Models\role;
use App\Models\Bankaccount;




class CurrencyController extends Controller{

	protected $moduleName 	= 'currency';
	protected $limit 		= 20;
	protected $start 		= 0;
	
	public function __construct()
	{
					
	}
	
	public function show(Request $request){

			$grid = new PanelGrid;
			$grid->setupGrid($this->moduleName, 'currency-data', $this->limit);
			$grid->setTitle(Lang::get('COMMON.CURRENCY'));
									

			$grid->addColumn('code', 		Lang::get('COMMON.CURCODE'), 					108,	array());
			$grid->addColumn('rate', 		Lang::get('COMMON.CURRATE'), 					108,	array());
			$grid->addColumn('newrate', 	Lang::get('COMMON.NEWCURRATE'), 				108,	array());
			$grid->addColumn('effectdate', 	Lang::get('COMMON.EFFECTDATE'), 				108,	array());
			$grid->addColumn('status', 		Lang::get('COMMON.STATUS'), 					108,	array('align' => 'center'));

			
			$grid->addFilter('status',array('Suspended','Active'), array('display' => Lang::get('COMMON.TYPE')));

			$data['grid'] = $grid->getTemplateVars();
			return view('admin.grid2',$data);
    }
	
	protected function doGetData(Request $request){
		$aflt  = $request->input('aflt');
		$aqfld = $request->input('aqfld');
		
		$condition = '1';
		
		if( isset($aqfld['status']) && $aqfld['status'] != 1 ){
			$condition = ' AND status'.'='.$aqfld['status'];
		}

		$total = Currency::whereRaw($condition)->count();

		$rows = array();
		if($currencies = Currency::whereRaw($condition)->get()){
			foreach($currencies as $currency){
				$rows[] = array(
					'code' 			=> App::formatAddTabUrl(Lang::get('CURRENCY').': '.$currency->id, urlencode($currency->code), action('Admin\CurrencyController@drawAddEdit',  array('sid' => $currency->id))),
					'rate' 			=> $currency->rate,
					'newrate' 		=> $currency->newrate,
					'effectdate' 	=> $currency->effectdate,
					'status' 		=> $currency->getStatusText($this->moduleName)
				);
			}
		}

		echo json_encode(array('total' => $total, 'rows' => $rows));
		exit;
	
	}
	
	protected function drawAddEdit(Request $request) { 

	    $toAdd 	  = true;
		$object   = null;
		$readOnly = false;
		$values   = array();

		$title = Lang::get('NEW').' '.Lang::get('CURRENCY');

		if($request->has('sid')) 
		{
	
			if ($object = Currency::find($request->input('sid'))) 
				{
					$title    = Lang::get('COMMON.EDIT').' '.Lang::get('COMMON.ROLE');
					$toAdd    = false;
					$readOnly = true;

				} 
			else
				{
					$object = null;
				}
			
		}
		
		$form = App::setupPanelForm('doadd', $request->all(), array(), $toAdd);
		$form->setFormTitle($title);

		$form->addInput('text', 				Lang::get('COMMON.CURCODE') 		, 'code', 	 		(($object)? $object->code:''), 			array(), false); 
		$form->addInput('text', 				Lang::get('COMMON.CURRATE') 		, 'rate', 	 		(($object)? $object->rate:''), 			array(), false); 
		$form->addInput('text', 				Lang::get('COMMON.NEWCURRATE') 		, 'newrate', 	 	(($object)? $object->newrate:''), 		array(), false); 
		$form->addInput('date', 				Lang::get('COMMON.EFFECTDATE') 		, 'effectdate', 	(($object)? $object->effectdate:''), 	array(), false); 
		$form->addInput('radio',   			    Lang::get('COMMON.STATUS')			, 'status',  		(($object)? $object->status:''), 		array('options' => App::getStatusOptions()), true);
	
		$data['form'] = $form->getTemplateVars();
		$data['module'] = 'currency';

		return view('admin.form2',$data);
	
	}
	
	protected function doAddEdit(Request $request) {
		
		$validator = Validator::make(
			[
	
				'code'	  	 	  => $request->input('code'),
				'rate'	  	  	  => $request->input('rate'),
				'newrate'	  	  => $request->input('newrate'),
				'effectdate'	  => $request->input('effectdate'),

			],
			[
	
			   'code'	   	  	  => 'required',
			   'rate'	  	      => 'required',
			   'newrate'	  	  => 'required',
			   'effectdate'	  	  => 'required',

			]
		);
		
		
		if ($validator->fails())
		{
			echo json_encode($validator->errors());
			exit;
		}
		
		
		
		$object  = new Role;
		
		if($request->has('sid')) 
		{
			$object = Currency::find($request->input('sid'));
		}
		
		$object->code	 					= $request->input('code');
		$object->rate	 					= $request->input('rate');
		$object->newrate	 				= $request->input('newrate');
		$object->effectdate	 				= $request->input('effectdate');


		
		if( $object->save() )
		{
			echo json_encode( array( 'code' => Lang::get('COMMON.SUCESSFUL') ) );
			exit;
		}


		echo json_encode( array( 'code' => Lang::get('COMMON.FAILED') ) );
		
	}
	
	protected function doActivateSuspend(Request $request) {

			

		if($request->has('id')) {
		
			if($object = Currency::find($request->id)) {
			
				$currentStatus = $object->status;
				if($request->action == 'activate') {
					$object->status 	= CBO_ACCOUNTSTATUS_ACTIVE;
				}
				if($request->action == 'suspend') {
					$object->status 	= CBO_ACCOUNTSTATUS_SUSPENDED;
				}

				if($currentStatus <> $object->status){
					if($object->save()) {
					
						echo json_encode( array( 'code' => Lang::get('COMMON.SUCESSFUL') ) );
						exit;
					}
				}
			}
		}
		
		
		echo json_encode( array( 'code' => Lang::get('COMMON.FAILED') ) );

	}
	



}

?>
