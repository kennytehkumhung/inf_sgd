<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller\Admin;
use App\libraries\grid\PanelGrid;
use App\libraries\App;
use Illuminate\Http\Request;
use Auth;
use Validator;
use Session;
use Lang;
use App\Models\Iptracker;
use App\Models\Currency;
use App\Models\Fundingmethod;
use App\Models\Cashledger;
use App\Models\Bankledger;
use App\Models\Bank;
use App\Models\Accounttag;
use App\Models\Tag;
use App\Models\Account;
use App\Models\Accountbank;
use App\Models\Cashledgerlog;
use App\Models\Bankaccount;
use App\Models\Promocash;
use App\Models\Rejectreason;
use App\Models\Banktransfer;
use App\Models\Promocampaign;
use App\Models\Media;
use App\Models\Configs;
use Storage;
use File;

class BankController extends Controller{

	protected $moduleName 	= 'bank';
	protected $limit 		= 20;
	protected $start 		= 0;
	
	public function __construct()
	{
					
	}
	
	public function show(Request $request)
	{

		$grid = new PanelGrid;
		$grid->setupGrid($this->moduleName, 'bank-data', $this->limit);
		$grid->setTitle(Lang::get('COMMON.BANK'));
								
		//$grid->addColumn('id',					Lang::get('COMMON.ID'), 						0,		array('align' => 'left'));
		$grid->addColumn('bank', 				Lang::get('COMMON.BANK'),						150,	array('align' => 'left'));
		$grid->addColumn('code', 				Lang::get('COMMON.CODE'),						68,	array('align' => 'left'));
		$grid->addColumn('currency', 			Lang::get('COMMON.CURRENCY'),					150,	array('align' => 'left'));
		$grid->addColumn('mindeposit', 			Lang::get('COMMON.MINDEPOSIT'),					150,	array('align' => 'left'));
		$grid->addColumn('maxdeposit', 			Lang::get('COMMON.MAXDEPOSIT'),					150,	array('align' => 'left'));
		$grid->addColumn('minwithdraw', 		Lang::get('COMMON.MINWITHDRAWAL'),				150,	array('align' => 'left'));
		$grid->addColumn('maxwithdraw', 		Lang::get('COMMON.MAXWITHDRAWAL'),				150,	array('align' => 'left'));
		$grid->addColumn('status', 				Lang::get('COMMON.STATUS'),						150,	array('align' => 'left'));

		$grid->addFilter('status',Bank::getStatusOptions(), array('display' => Lang::get('COMMON.TYPE')));
		$grid->addButton('1', 'New', Lang::get('COMMON.ADDNEW').' '.Lang::get('COMMON.BANK'), 'button', array('icon' => 'add', 'url' => action('Admin\BankController@drawAddEdit')));
		

		
		$data['grid'] = $grid->getTemplateVars();

		return view('admin.grid2',$data);
    }
	
	protected function doGetData(Request $request){ 
	
	//status
	$status[1] = 'Active';
	$status[0] = 'Suspended';
	
	$rows = array();
	
	$aflt  = $request->input('aflt');
	$aqfld = $request->input('aqfld');
	
	//$condition .= ' AND crccode = "'.Session::get('admin_crccode').'"';
	
	$defaultCondition 	= '1';
	$condition = '1';
	
	$condition .= Session::get('admin_crccode') == 'ALL' ? '' : ' AND currencycode = "'.Session::get('admin_crccode').'"';
	
	if( isset($aqfld['status']) && $aqfld['status'] != ''){
		$condition .= ' AND status'.'='.$aqfld['status'];
	}
	
	$total = Bank::whereRaw($condition)->count();

	//new structure
	if($banks = Bank::whereRaw($condition)->skip($request->recordstartindex)->take( $request->pagesize )->orderBy('id','desc')->get()){

		foreach( $banks as $bank ){
			$body = App::formatAddTabUrl( Lang::get('BANK').': '.$bank->id, urlencode($bank->name), action('Admin\BankController@drawAddEdit',  array('sid' => $bank->id)));
			$rows[] = array(

				//'id'  				=> $bank->id, 
				'bank'  			=> urldecode($body), 
				'code'  			=> $bank->code, 
				'currency'  		=> $bank->currencycode, 
				'mindeposit'  		=> $bank->mindeposit, 
				'maxdeposit'  		=> $bank->maxdeposit, 
				'minwithdraw'  		=> $bank->minwithdraw, 
				'maxwithdraw'  		=> $bank->maxwithdraw, 
				'status'  			=> $bank->getStatusText($this->moduleName)

			);	
			
		}
			
	}
			
	echo json_encode(array('total' => $total, 'rows' => $rows));
	exit;

}

	protected function drawAddEdit(Request $request) { 

	    $toAdd 	  = true;
		$object   = null;
		$readOnly = false;

		$title = Lang::get('COMMON.NEW').' '.Lang::get('COMMON.BANK');
	
		 if($request->has('sid')) 
		{
	
			if ($object = Bank::find($request->input('sid'))) 
				{
					$title    = Lang::get('COMMON.EDIT').' '.Lang::get('COMMON.BANK');
					$toAdd    = false;
					$readOnly = true;

				} 
			else
				{
					$object = null;
				}
			
		} 
		
		$currencies = Currency::getAllCurrencyAsOptions();

		$form = App::setupPanelForm('doadd', $request->all(), array(), $toAdd);
		$form->setFormTitle($title);
		
		$form->addInput('text', 	Lang::get('COMMON.BANK') 			, 'bank', 		 	 (($object)? $object->name:''), 					   array(), 	 true);
		$form->addInput('text', 	Lang::get('COMMON.CODE') 			, 'code', 		 	 (($object)? $object->code:''), 					   array(), 	 true);
		$form->addInput('text', 	Lang::get('COMMON.SHORTNAME') 		, 'shortname', 		 (($object)? $object->shortname:''), 	    	   array(), 	 true);
        $form->addInput('select', 	Lang::get('COMMON.CURRENCY')			  , 'currency',      (($object)? $object->crccode:''),       array('options' => $currencies), 	 true);
		$form->addInput('text',	    Lang::get('COMMON.MINDEPOSIT') 		, 'mindeposit', 	 (($object)? $object->mindeposit:''), 				   array(), 	 true);
		$form->addInput('text', 	Lang::get('COMMON.MAXDEPOSIT') 		, 'maxdeposit', 	 (($object)? $object->maxdeposit:''),  				   array(), 	 false);
		$form->addInput('text', 	Lang::get('COMMON.MINWITHDRAWAL') 	, 'minwithdraw', 	 (($object)? $object->minwithdraw:''), 				   array(), 	 false);
		$form->addInput('text', 	Lang::get('COMMON.MAXWITHDRAWAL')	, 'maxwithdraw', 	 (($object)? $object->maxwithdraw:''), 		    	   array(), 	 false);
		$form->addInput('file', 	Lang::get('COMMON.IMAGE')			, 'image',    	     (($object)? $object->getImage(Media::TYPE_IMAGE):''), array(), 	 false);
		$form->addInput('radio', 	Lang::get('COMMON.STATUS')	    	, 'status', 	 	 (($object)? $object->status:''),	    			   array('options' => Bank::getStatusOptions()), true);
 
		$data['form'] = $form->getTemplateVars();
		$data['module'] = 'bank';

		return view('admin.form2',$data);
	
	}
	
	protected function doAddEdit(Request $request) {
		
		$validator = Validator::make(
			[
	
				'mindeposit'	  	  => $request->input('mindeposit'),
				'maxdeposit'	  	  => $request->input('maxdeposit'),
				'minwithdraw'  	  	  => $request->input('minwithdraw'),
				'maxwithdraw'         => $request->input('maxwithdraw'),
				'status' 	  		  => $request->input('status'),
			],
			[

			   'mindeposit'	   	   => 'required',
			   'maxdeposit'	  	   => 'required',
			   'minwithdraw'       => 'required',
			   'maxwithdraw'       => 'required',
			   'status' 	       => 'required',	
			]
		);
		
		
		if ($validator->fails())
		{
			echo json_encode($validator->errors());
			exit;
		}
		
		
		
		$object  = new Bank;
		
		if($request->has('sid')) 
		{
			$object = Bank::find($request->input('sid'));
		}
		
		$object->name	 			= $request->input('bank');
		$object->code	 			= $request->input('code');
                $object->shortname	 			= $request->input('shortname');
		$object->currencycode	 	= $request->input('currency');
		$object->mindeposit	 		= $request->input('mindeposit');
		$object->maxdeposit	 		= $request->input('maxdeposit');
		$object->minwithdraw	 	= $request->input('minwithdraw');
		$object->maxwithdraw	 	= $request->input('maxwithdraw');
		$object->status 			= $request->input('status');

		
		if( $object->save() )
		{
			if( $request->hasFile('image'))
			{
				$file = $request->file('image');
			
				$filename = 'bank/'.md5($file->getClientOriginalName().time()).'.'. $file->getClientOriginalExtension();
				$result = Storage::disk('s3')->put($filename,  File::get($file), 'public');


				if($result) 
				{
					if( Media::where( 'refobj' , '=' , 'BankObject' )->where( 'refid' , '=' , $object->id )->count() == 0 ){
						$medObj = new Media;
					}else{
						$medObj = Media::where( 'refobj' , '=' , 'BankObject' )->where( 'refid' , '=' , $object->id )->first();
					}
					
					$medObj->refobj = 'BankObject';
					$medObj->refid  = $object->id;
					$medObj->type   = Media::TYPE_IMAGE;
					$medObj->domain = 'https://'.Configs::getParam('SYSTEM_AWS_S3BUCKET');
					$medObj->path   = $filename;
					$medObj->save(); 
				}
			}
			echo json_encode( array( 'code' => Lang::get('COMMON.SUCESSFUL') ) );
			exit;
		}


		echo json_encode( array( 'code' => Lang::get('COMMON.FAILED') ) );
		
	}
	
	public function showbankReport(Request $request){

			$grid = new PanelGrid;
			$grid->setupGrid($this->moduleName, 'bankReport-data', $this->limit, false, array('params' => array('createdfrom' => '2014-01-01')));
			$grid->setTitle(Lang::get('COMMON.BANK'));
			
			$grid->addColumn('bank', 				Lang::get('COMMON.BANK'), 				160,		array());	
			$grid->addColumn('name', 				Lang::get('COMMON.NAME'), 				160,		array());	
			$grid->addColumn('account', 			Lang::get('COMMON.ACCOUNT'), 			160,		array());	
			$grid->addColumn('status', 				Lang::get('COMMON.STATUS'), 			100,		array('align' => 'center'));	
			$grid->addColumn('currency', 			Lang::get('COMMON.CURRENCY'), 			100,		array('align' => 'center'));	
			$grid->addColumn('credit', 				Lang::get('COMMON.CREDIT'), 			130,		array('align' => 'right'));	
			$grid->addColumn('debit', 				Lang::get('COMMON.DEBIT'), 				130,		array('align' => 'right'));	
			$grid->addColumn('balance', 			Lang::get('COMMON.BALANCE'), 			130,		array('align' => 'right'));	
			$grid->addColumn('cashin', 				'', 									100,		array('align' => 'center'));	
			$grid->addColumn('cashout', 			'', 									100,		array('align' => 'center'));	
			$grid->addColumn('transfer', 			'', 									100,		array('align' => 'center'));	

			$grid->addFilter('status',array(-1 => 'All',1 => 'Active',0 => 'Suspended'), array('display' => Lang::get('COMMON.TYPE')));

			$data['grid'] = $grid->getTemplateVars();

			return view('admin.grid2',$data);
    }
	
	public function showbankReportdoGetData(Request $request){
		
	//status
	$status[1] = 'Active';
	$status[0] = 'Suspended';
	
	$aflt  = $request->input('aflt');
	$aqfld = $request->input('aqfld');
	
	$condition = '1';
	
	$condition .= Session::get('admin_crccode') == 'MYR' ? '' : ' AND crccode = "'.Session::get('admin_crccode').'"';

    if( isset($aqfld['status']) ) {
        if (in_array($aqfld['status'], array(0, 1))) {
            if ($aqfld['status'] == 0 || $aqfld['status'] == 1) {
                $condition = 'status'.'='.$aqfld['status'];
            }
        }
    } else {
        $condition = 'status'.'=1'; // Default status: Active
    }

	$rows = array();
	
	$totalrow = Bankaccount::whereRaw($condition)->count();
	
	if($bankaccounts = Bankaccount::whereRaw($condition)->skip($request->recordstartindex)->take( $request->pagesize )->orderBy('status','desc')->orderBy('crccode','asc')->get()){

		foreach( $bankaccounts as $bankaccount ){

            $condition = 'bacid = '.$bankaccount->id.' AND `date` between "'.$request->input('createdfrom').'" AND "'.$request->input('createdto').'"';

			$credit = Bankledger::whereRaw($condition.' AND type = '.Bankledger::TYPE_CREDIT)->sum('amount');

			 $debit = Bankledger::whereRaw($condition.' AND type = '.Bankledger::TYPE_DEBIT)->sum('amount');
			$balance = abs($credit) - abs($debit);
			
			$bankOptions = Bank::getAllAsOptions();
			
			if(isset($total[$bankaccount->crccode]['credit']))
			{
				$total[$bankaccount->crccode]['credit'] += $credit;
			}
			else
			{
				$total[$bankaccount->crccode]['credit'] = $credit;
			}
			
			if(isset($total[$bankaccount->crccode]['debit'])) 
			{
				$total[$bankaccount->crccode]['debit'] += $debit;
			}
			else
			{
				$total[$bankaccount->crccode]['debit'] = $debit; 
			}
			
			$name = Bank::where( 'id' , '=' , $bankaccount->bnkid)->pluck('shortname');
			
			$rows[] = array(
			
				'bank' 			=> Bank::whereRaw('id'.'='.$bankaccount->bnkid)->pluck('name'),
				'name' 			=> $bankaccount->bankaccname,				
				'currency' 		=> $bankaccount->crccode,
				'account' 		=> $bankaccount->bankaccno,
				'status' 		=> $status[$bankaccount->status],
			 	'credit'	    => App::displayAmount($credit),
				'debit' 		=> App::displayAmount($debit),
				'balance'	    => '<a href="#" onclick="parent.addTab(\''.Lang::get('COMMON.BANKTRANSACTION').': '.$bankaccount->bankaccname.' '.$bankaccount->bankaccno.' \', \'bankledger?from='.App::getDateTime(11).'&bacid='.$bankaccount->id.'\')">'. App::formatNegativeAmount($balance) .'</a>',
				'cashin' 		=> '<a href="#" onclick="parent.addTab(\''.Lang::get('COMMON.BANK').': Credit '.$name.' #'.$bankaccount->bankaccno.'\', \'bank-cashinout?type=in&sid='.$bankaccount->id.'\')">'. Lang::get('COMMON.CREDIT').'</a>',
				'cashout' 		=> '<a href="#" onclick="parent.addTab(\''.Lang::get('COMMON.BANK').': Debit '.$name.' #'.$bankaccount->bankaccno.'\', \'bank-cashinout?type=out&sid='.$bankaccount->id.'\')">'. Lang::get('COMMON.DEBIT').'</a>',
				'transfer' 		=> '<a href="#" onclick="parent.addTab(\''.Lang::get('COMMON.BANK').': Debit '.$name.' #'.$bankaccount->bankaccno.'\', \'bank-transfer?sid='.$bankaccount->id.'\')">'. Lang::get('COMMON.TRANSFER').'</a>',
				
			);	
			
		}
			
	}	

		echo json_encode(array('total' => $totalrow, 'rows' => $rows));
		exit;
		
	}
	
	
	public function showBankLedger(Request $request) {
	
	
			$grid = new PanelGrid;
			$grid->setupGrid($this->moduleName, 'bankledger-data', $this->limit, false, array('params' => array('bacid' => $request->bacid)));
			$grid->setTitle(Lang::get('COMMON.BANKTRANSACTION'));
			
			$grid->addColumn('bank', 		Lang::get('COMMON.BANK'), 			90,	array());
			$grid->addColumn('account', 	Lang::get('COMMON.ACCOUNT'), 		250,	array());
			$grid->addColumn('currency', 	Lang::get('COMMON.CURRENCY'), 		60,		array('align' => 'center'));
			$grid->addColumn('time', 		Lang::get('COMMON.TIME'), 			140,		array('align' => 'center'));
			$grid->addColumn('credit', 		Lang::get('COMMON.CREDIT'), 		70,		array('align' => 'right'));
			$grid->addColumn('debit', 		Lang::get('COMMON.DEBIT'), 			70,		array('align' => 'right'));
			$grid->addColumn('remark', 		Lang::get('COMMON.REMARK'), 		300,	array());
			$grid->addColumn('username', 	Lang::get('COMMON.USERNAME'), 		150,	array());
			

			$grid->addFilter('crccode', Currency::getAllCurrencyAsOptions(), array('display' => Lang::get('COMMON.CURRENCY')));
			$grid->addFilter('bnkid', Bank::getAllAsOptions(), array('display' => Lang::get('COMMON.BANK'), 'selected' => $request->bnkid));
			$grid->addFilter('bacid', BankAccount::getAllAsOptions($bnkid = false, $deposit = false, $withdraw = false, $bhdid = false, $currency = false, $status = false));

			$data['grid'] = $grid->getTemplateVars();

			return view('admin.grid2',$data);
	
	}
	
	public function doGetBankLedgerData(Request $request) {
		
	
		$condition = '1';
		
		if( isset($request->aflt['crccode']) && $request->aflt['crccode'] != 0 )
		{
			$condition .= ' AND crccode = "'.$request->aflt['crccode'].'" ';
		}
		
		if( isset($request->aflt['bacid']) && $request->aflt['bacid'] != 0 )
		{
			$condition .= ' AND bacid = "'.$request->aflt['bacid'].'" ';
		}
		
		if( isset($request->bacid) && $request->bacid != '' )
		{
			$condition .= ' AND bacid = "'.$request->bacid.'" ';
		}
		
		if( isset($request->aqfld['bnkid']) && $request->aqfld['bnkid'] != 0 )
		{
			$condition .= ' AND bnkid = "'.$request->aqfld['bnkid'].'" ';
		}
		
		
		$count = Bankledger::whereRaw($condition)->count();
		
		$rows = array();
		$footer = array();
		$total = array();

		//new structure
		if($bankledgers = Bankledger::whereRaw($condition)->skip($request->recordstartindex)->take( $request->pagesize )->orderBy('id','desc')->get()){
			$accounts = BankAccount::getAllAsOptions();
			$bankOptions = Bank::getAllAsOptions();
			$bankAccounts = array();
			foreach( $bankledgers as $ledger ){
				$credit = 0;
				$debit = 0;
				if($ledger->amount >= 0) $credit = $ledger->amount;
				else $debit = $ledger->amount;
				
				if(isset($total[$ledger->crccode]['credit'])) $total[$ledger->crccode]['credit'] += $credit;
				else $total[$ledger->crccode]['credit'] = $credit;
				
				if(isset($total[$ledger->crccode]['debit'])) $total[$ledger->crccode]['debit'] += $debit;
				else $total[$ledger->crccode]['debit'] = $debit;
				
				$remark = $ledger->remark;
				if($ledger->ismanual == 0) {
					if($ledger->amount >= 0) {
						$remark = Lang::get('COMMON.DEPOSIT').' #'.$ledger->clgid;
					} else {
						$remark = Lang::get('COMMON.WITHDRAWAL').' #'.$ledger->clgid;
					}
				}
				
				$rows[] = array(

					'bank'		 => $bankOptions[$ledger->bnkid],
					'currency' 	 => $ledger->crccode,
					'account' 	 => $accounts[$ledger->bacid],
					'credit' 	 => ($credit)?App::displayAmount($credit):'',
					'debit' 	 => ($debit)?App::displayAmount($debit):'',
					'time' 		 => $ledger->created->toDateTimeString(),
					'remark' 	 => $remark,
					'username' 	 => substr(Cashledger::whereRaw('id = '.$ledger->clgid.' ')->pluck('acccode') , 3 ),
				);	
				
			}
				
		}

				
		echo json_encode(array('total' => $count , 'rows' => $rows));
		exit;
	}
	
	public function drawCashInOut(Request $request){
		
		$toAdd 	  = true;
		$object   = null;
		$readOnly = false;

		$title = Lang::get('COMMON.NEW').' '.Lang::get('COMMON.BANK');
	
		 if($request->has('sid')) 
		{
	
			if ($object = Bankaccount::find($request->input('sid'))) 
				{
					$title    = Lang::get('COMMON.EDIT').' '.Lang::get('COMMON.BANK');
					$toAdd    = false;
					$readOnly = true;

				} 
			else
				{
					$object = null;
				}
			
		} 
		
		$bankOptions = Bank::getAllAsOptions(false, true);
		
		$form = App::setupPanelForm('doadd', $request->all(), array(), $toAdd);
		$form->setFormTitle($title);

		$form->addInput('select', 		Lang::get('COMMON.BANK') 		, 'bank', 		 (($object)? $object->bnkid:''), 		    array('options' => $bankOptions, 'disabled' => (($object)?true:false)), true);
		$form->addInput('showonly', 	Lang::get('COMMON.NAME') 		, 'name', 		 (($object)? $object->bankaccname:''), 		array(), true);
		$form->addInput('showonly', 	Lang::get('COMMON.ACCOUNT') 	, 'account', 	 (($object)? $object->bankaccno:''), 		array(), true);
		$form->addInput('date', 		Lang::get('COMMON.DATE') 		, 'date', 		  App::getDateTime(3) , 	  				array(), true);
		$form->addInput('text',	   		Lang::get('COMMON.AMOUNT') 		, 'amount', 	 (($object)? $object->amount:''), 			array(), true);
		$form->addInput('textarea', 	Lang::get('COMMON.REMARK') 		, 'remark', 	 (($object)? $object->remark:''),  			array(), false);
		$form->addInput('hidden', '', 'type', $request->type, array(), '');
 
		$data['form'] = $form->getTemplateVars();
		$data['module'] = 'bank-cashinout';

		return view('admin.form2',$data);
		
	}
	
	protected function doCashInOut(Request $request){
		
		$validator = Validator::make(
			[
	
				'date'	  	=> $request->input('date'),
				'amount'	=> $request->input('amount'),
				'remark'  	=> $request->input('remark'),
			],
			[

			   'date'	   	=> 'required',
			   'amount'	  	=> 'required',
			   'remark'     => 'required',

			]
		);
		
		
		if ($validator->fails())
		{
			echo json_encode($validator->errors());
			exit;
		}
		
		
		
		$object  = new Bankledger;
		
		if($request->has('sid')) 
		{
			$bacobject = Bankaccount::find($request->input('sid'));
		}
		
		
		$object->bnkid 		 = $bacobject->bnkid;
		$object->bacid 		 = $bacobject->id;
		$object->type 		 = $request->input('type') == 'in' ? Bankledger::TYPE_CREDIT : Bankledger::TYPE_DEBIT;
		$object->crccode 	 = $bacobject->crccode;
		$object->crcrate 	 = Currency::getCurrencyRate($object->crccode);
		$object->date 		 = $request->input('date');
		$object->amount 	 = $request->input('type') == 'in' ?  abs($request->input('amount')) :  abs($request->input('amount')) * -1;
		$object->amountlocal = Currency::getLocalAmount($object->amount, $object->crccode);
		$object->ismanual 	 = 1;
		$object->remark 	 = $request->has('remark') ? $request->input('remark') : '';
		$object->status 	 = 1;
		$object->createdip 	 = App::getRemoteIp();
		

		
		if( $object->save() )
		{
			echo json_encode( array( 'code' => Lang::get('COMMON.SUCESSFUL') ) );
			exit;
		}


		echo json_encode( array( 'code' => Lang::get('COMMON.FAILED') ) );
	}
	
	public function drawTransfer(Request $request){
		
		$toAdd 	  = true;
		$object   = null;
		$readOnly = false;

		$title = Lang::get('COMMON.NEW').' '.Lang::get('COMMON.BANK');
	
		$bankOptions = Bank::getAllAsOptions(true);
		$bnkid = 0;
		$id = 0;
		$currency = '';
		$from = '';
		
		if($request->has('sid')) 
		{
	
			if ($object = Bankaccount::find($request->input('sid'))) 
				{
					$title    = Lang::get('COMMON.EDIT').' '.Lang::get('COMMON.BANK');
					$toAdd 	  = false;
					$readOnly = true;
					$currency = $object->crccode;
					$id 	  = $object->id;
					$bnkid 	  = $object->bnkid;
					$from 	  = '['.$bankOptions[$object->bnkid].'] - '.$object->bankaccname.' - '.$object->bankaccno;

				} 
			else
				{
					$object = null;
				}
			
		} 
		

		$banks = array();
		if($accounts = Bankaccount::whereRaw('crccode = "'.$currency.'" AND status = '.CBO_ACCOUNTSTATUS_ACTIVE.' AND id != '.$id)->get()) {
			foreach($accounts as $account) {
				
				$banks[$account->id] = '['.Bank::whereRaw('id = '.$account->bnkid)->pluck('name').'] - '.$account->bankaccname.' - '.$account->bankaccno;
			}
		}
		App::arrayUnshift($banks, Lang::get('COMMON.SELECT'));
		
		$credit  = Bankledger::whereRaw('bacid = '.$object->id.' AND type = '.Bankledger::TYPE_CREDIT)->sum('amount');
		$debit   = Bankledger::whereRaw('bacid = '.$object->id.' AND type = '.Bankledger::TYPE_DEBIT)->sum('amount');
		$balance = App::formatNegativeAmount(abs($credit) - abs($debit));
				
		
		$form = App::setupPanelForm('doadd', $request->all(), array(), $toAdd);
		$form->setFormTitle($title);

		$form->addInput('showonly', 	Lang::get('COMMON.FROM') 		, 'from', 		 $from, 				array(), true);
		$form->addInput('hidden', 		Lang::get('COMMON.FROM') 	    , 'from', 		 $object->id, 			array(), true);
		$form->addInput('showonly', 	Lang::get('COMMON.BALANCE') 	, 'balance', 	 $balance, 				array(), true);
		$form->addInput('select', 		Lang::get('COMMON.TO') 			, 'to', 	 	 '', 					array('options' => $banks), true);
		$form->addInput('date', 		Lang::get('COMMON.DATE') 		, 'date', 		  App::getDateTime(3) , array(), true);
		$form->addInput('text',	   		Lang::get('COMMON.AMOUNT') 		, 'amount', 	 '', 					array(), true);
		$form->addInput('textarea', 	Lang::get('COMMON.REMARK') 		, 'remark', 	 '',  					array(), false);
		$form->addInput('hidden', '', 'type', $request->type, array(), '');
 
		$data['form'] = $form->getTemplateVars();
		$data['module'] = 'bank-transfer';

		return view('admin.form2',$data);
		
	}
	
	protected function doTransfer(Request $request){
		
		$validator = Validator::make(
			[
	
				'date'	  	=> $request->input('date'),
				'amount'	=> $request->input('amount'),
				'remark'  	=> $request->input('remark'),
				'to'  		=> $request->input('to'),
			],
			[

			   'date'	   	=> 'required',
			   'amount'	  	=> 'required',
			   'remark'     => 'required',
			   'to'    	    => 'required',

			]
		);
		
		if($request->has('sid')) 
		{
			$object = Bankaccount::find($request->input('sid'));
		} 
		
		
		if ($validator->fails())
		{
			echo json_encode($validator->errors());
			exit;
		}
		
		$amount = abs($request->amount) * -1;
		
		$blgObj = new Bankledger;
		$blgObj->bnkid 		 = $object->bnkid;
		$blgObj->bacid 		 = $object->id;
		$blgObj->type 		 = Bankledger::TYPE_DEBIT;
		$blgObj->crccode	 = $object->crccode;
		$blgObj->crcrate 	 = Currency::getCurrencyRate($blgObj->crccode);
		$blgObj->date 		 = $request->date;
		$blgObj->amount 	 = $amount;
		$blgObj->amountlocal = Currency::getLocalAmount($blgObj->amount, $blgObj->crccode);
		$blgObj->ismanual 	 = 1;
		$blgObj->remark 	 = $request->has('remark') ? $request->remark : '';
		$blgObj->status 	 = 1;
		$blgObj->createdip 	 = App::getRemoteIp();
		

		if($blgObj->save()) 
		{
			if($bacObj = Bankaccount::find($request->to) )
			{
				$blgObj = new Bankledger;
				$blgObj->bnkid 		 = $bacObj->bnkid;
				$blgObj->bacid 		 = $bacObj->id;
				$blgObj->type 		 = Bankledger::TYPE_CREDIT;
				$blgObj->crccode 	 = $bacObj->crccode;
				$blgObj->crcrate 	 = Currency::getCurrencyRate($blgObj->crccode);
				$blgObj->date 		 = $request->date;
				$blgObj->amount 	 = $amount * -1;
				$blgObj->amountlocal = Currency::getLocalAmount($blgObj->amount, $blgObj->crccode);
				$blgObj->ismanual 	 = 1;
				$blgObj->remark 	 = $request->has('remark') ? $request->remark : '';
				$blgObj->status 	 = 1;
				$blgObj->createdip 	 = App::getRemoteIp();
				
			}
		

		}

		
		if( $blgObj->save() )
		{
			echo json_encode( array( 'code' => Lang::get('COMMON.SUCESSFUL') ) );
			exit;
		}


		echo json_encode( array( 'code' => Lang::get('COMMON.FAILED') ) );
	}
	
	protected function doActivateSuspend(Request $request) {

			

		if($request->has('id')) {
		
			if($object = Bank::find($request->id)) {
			
				$currentStatus = $object->status;
				if($request->action == 'activate') {
					$object->status 	= CBO_ACCOUNTSTATUS_ACTIVE;
				}
				if($request->action == 'suspend') {
					$object->status 	= CBO_ACCOUNTSTATUS_SUSPENDED;
				}

				if($currentStatus <> $object->status){
					if($object->save()) {
					
						echo json_encode( array( 'code' => Lang::get('COMMON.SUCESSFUL') ) );
						exit;
					}
				}
			}
		}
		
		
		echo json_encode( array( 'code' => Lang::get('COMMON.FAILED') ) );

	}
	
	protected function doUpdateBankSummary() {

		if($ledgers = Cashledger::whereRaw('chtcode IN (1,2) AND status = 4 AND created >= "2016-01-15 00:00:00" AND id NOT IN (SELECT clgid FROM bankledger WHERE clgid > 0)')->get()) {
			foreach($ledgers as $ledger) {
				if($btfObj = Banktransfer::find($ledger->refid)) {
					$blgObj = new Bankledger;
					$blgObj->bnkid = $btfObj->bnkid;
					$blgObj->bacid = $ledger->bacid;
					$blgObj->type = ($ledger->chtcode == 1)?Bankledger::TYPE_CREDIT:Bankledger::TYPE_DEBIT;
					$blgObj->clgid = $ledger->id;
					$blgObj->crccode = $ledger->crccode;
					$blgObj->crcrate = Currency::getCurrencyRate($blgObj->crccode);
					$blgObj->date = App::formatDateTime(3, $ledger->created);
					$blgObj->amount = $ledger->amount;
					$blgObj->amountlocal = Currency::getCurrencyRate($blgObj->crccode);
					$blgObj->ismanual = 0;
					$blgObj->status = 1;
					$blgObj->save();
				}
			}
		}
	}
		
}
