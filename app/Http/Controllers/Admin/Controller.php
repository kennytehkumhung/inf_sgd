<?php namespace App\Http\Controllers\Admin;

use Illuminate\Foundation\Bus\DispatchesCommands;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;

abstract class Controller extends BaseController {

	use DispatchesCommands, ValidatesRequests;
	
    protected function hasPermission($permissionName, $moduleName = null, $taskId = null) {

        $nav = \Session::get('admin_navigation');

        if (!is_null($nav) && isset($nav[$permissionName])) {
            $permission = $nav[$permissionName];

            if (!is_null($moduleName)) {
                if (isset($permission[$moduleName])) {
                    $module = $permission[$moduleName];

                    if (!is_null($taskId)) {
                        if (isset($module['task'])) {
                            foreach ($module['task'] as $tskKey => $tskVal) {
                                if (isset($tskVal['id']) && $tskVal['id'] == $taskId) {
                                    return true;
                                }
                            }
                        }
                    } else {
                        // Skip checking taskId.
                        return true;
                    }
                }
            } else {
                // Skip checking moduleName.
                return true;
            }
        }

        return false;
    }
}
