<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller\Admin;
use App\libraries\grid\PanelGrid;
use App\libraries\App;
use Illuminate\Http\Request;
use Auth;
use Validator;
use Session;
use Lang;
use Cache;
use App\Models\Iptracker;
use App\Models\Currency;
use App\Models\Fundingmethod;
use App\Models\Cashledger;
use App\Models\Bank;
use App\Models\Accounttag;
use App\Models\Tag;
use App\Models\Account;
use App\Models\Channel;
use App\Models\Config;
use App\Models\Cashledgerlog;
use App\Models\Bankaccount;
use App\Models\Promocash;
use App\Models\Rejectreason;
use App\Models\Banktransfer;
use App\Models\Promocampaign;
use App\Models\Remark;
use App\Models\User;


class ConfigController extends Controller{


	protected $moduleName 	= 'config';
	protected $limit 		= 20;
	protected $start 		= 0;
	
	public function __construct()
	{
		
	}
	
	public function index(Request $request){

			$grid = new PanelGrid;
			$grid->setupGrid($this->moduleName, 'config-data', $this->limit);
			$grid->setTitle(Lang::get('COMMON.CONFIGURATION'));
			
			$grid->addColumn('param', 			Lang::get('COMMON.PARAM'), 		300,		array('align' => 'left'));			
			$grid->addColumn('name', 			Lang::get('COMMON.NAME'), 		250,		array('align' => 'left'));			
			$grid->addColumn('type',			Lang::get('COMMON.TYPE'), 		80,		array('align' => 'left'));
			$grid->addColumn('value', 			Lang::get('COMMON.VALUE'), 		200,			array('align' => 'left'));

			$grid->addButton('1', 'New', Lang::get('COMMON.ADDNEW').' '.Lang::get('COMMON.CONFIGURATION'), 'button', array('icon' => 'add', 'url' => action('Admin\ConfigController@drawAddEdit')));
			
			$search = '';
			$value = '';
			if($request->has('aflid')) {
				//$aflObj = new ConfigObject;
				if($aflObj = Config::find($request->input('aflid'))) {
					$search = 'aflid';
					//$value = $aflObj->username;
				}
			}
			if($request->has('nodate')) {
				Session::put('nodate', true);
			}
			
			$showcolunm = $request->input('showcolumn');
			
			$grid->addSearchField('search', array('param' => Lang::get('COMMON.PARAM') , 'name' => Lang::get('COMMON.NAME'),array('search' => $search)));
			

			$data['grid'] = $grid->getTemplateVars();

			return view('admin.grid2',$data);
    }
	
	protected function doGetData(Request $request){
		
		
		$condition = '1 ';
		
		$aflt  = $request->input('aflt');
		$aqfld = $request->input('aqfld');
		
		$condition = '1';		
		
		if( isset($aflt['param']) && $aflt['param'] != '' ){
			$condition .= ' AND param = "'.$aflt['param'].'" ';
		}
		
		if( isset($aflt['name']) && $aflt['name'] != ''){
			$condition .= ' AND name = "'.$aflt['name'].'" ';
		}	

		
		$total = Config::whereRaw($condition)->count();
		$rows = array();
		//new structure
		if($configs = Config::whereRaw($condition)->take( $request->pagesize )->orderBy('param','asc')->get()){
			foreach( $configs as $config ){
				
				$body = App::formatAddTabUrl( Lang::get('CONFIGURATION').': '.$config->id, urlencode($config->name), action('Admin\ConfigController@drawAddEdit',  array('sid' => $config->id)));
		
				$rows[] = array(

					'param' => urldecode($config->param),
					'name'	=> $body,
					'type'	=> $config->type,
					'value'	=> $config->value,

				);	
				
			}
				
		}
		
		echo json_encode(array('total' => $total, 'rows' => $rows));
		exit;

	}
	
	protected function drawAddEdit(Request $request) { 
	
		$toAdd 	  = true;
		$object   = null;
		$readOnly = false;
		$values   = array();

		$title = Lang::get('NEW').' '.Lang::get('CONFIGURATION');

		if($request->has('sid')) 
		{
	
			if ($object = Config::find($request->input('sid'))) 
				{
					$title    = Lang::get('COMMON.EDIT').' '.Lang::get('COMMON.CONFIGURATION');
					$toAdd    = false;
					$readOnly = true;

				} 
			else
				{
					$object = null;
				}
			
		}

		$form = App::setupPanelForm('doadd', $request->all(), array(), $toAdd);
		$form->setFormTitle($title);
		
		$form->addInput('text', 		Lang::get('COMMON.PARAM') 			, 'param', 	 (($object)? $object->param:''), 		array(), false); 
		$form->addInput('text', 		Lang::get('COMMON.NAME') 			, 'name', 	 (($object)? $object->name:''), 		array(), false); 
		$form->addInput('text', 		Lang::get('COMMON.TYPE') 			, 'type', 	 (($object)? $object->type:''), 		array(), false); 
		$form->addInput('text', 		Lang::get('COMMON.VALUE') 			, 'value', 	 (($object)? $object->value:''), 		array(), false); 

	
		$data['form'] = $form->getTemplateVars();
		$data['module'] = 'config';

		return view('admin.form2',$data);
		
	}
	
	protected function doAddEdit(Request $request) {
		
			$validator = Validator::make(
			[
	
				'param'	  	  	 => $request->input('param'),
				'name'	  	 	 => $request->input('name'),
				'type'	  	 	 => $request->input('type'),
				'value'	  	 	 => $request->input('value'),


			],
			[

			   'param'	   	   	   => 'required',
			   'name'	  		   => 'required',
			   'type'	  		   => 'required',
			   'value'	  		   => 'required',

			]
		);
		
		if ($validator->fails())
		{
			echo json_encode($validator->errors());
			exit;
		}
		

		$object  = new Config;
		
		if($request->has('sid')) 
		{
			$object = Config::find($request->input('sid'));
		}
		
		$object->param	 			= $request->input('param');
		$object->name	 			= $request->input('name');
		$object->type	 			= $request->input('type');
		$object->value	 			= $request->input('value');

		Cache::forget( 'db_config_'.$request->input('param') );
		
		if( $object->save() )
		{
			echo json_encode( array( 'code' => Lang::get('COMMON.SUCESSFUL') ) );
			exit;
		}


		echo json_encode( array( 'code' => Lang::get('COMMON.FAILED') ) );
		
		
	}
	public function minigamedate(Request $request){

		if($request->input('method')=='post'){

            $validatorInput=[];
            $validatorInput['startdate']    = $request->input('startdate');
            $validatorInput['enddate']  	= $request->input('enddate');
            $validatorInput['eighty']  	= $request->input('eighty');
            $validatorInput['ten']  	= $request->input('ten');
            $validatorInput['five']  	= $request->input('five');
            $validatorInput['three']  	= $request->input('three');
            $validatorInput['two']  	= $request->input('two');

            $validatorRequired=[];
            $validatorRequired['startdate']   = 'required';
            $validatorRequired['enddate']     = 'required';
            $validatorRequired['eighty']     = 'required';
            $validatorRequired['ten']     	= 'required';
            $validatorRequired['five']     	= 'required';
            $validatorRequired['three']     = 'required';
            $validatorRequired['two']     	= 'required';

            $validator = Validator::make($validatorInput,$validatorRequired);

            $validator->setAttributeNames(array(
                'startdate' 	=> 'start date',
                'enddate' 		=> 'end date',
                'eighty' 		=> '80%',
                'ten' 			=> '10%',
                'five' 			=> '5%',
                'three' 		=> '3%',
                'two' 			=> '2%',
            ));
            if ($validator->fails())
            {
                echo json_encode($validator->errors());
                exit;
            }

            if(($request->input('startdate')>$request->input('enddate'))||($request->input('startdate')==$request->input('enddate'))){
                echo json_encode( array( 'code' => 'start date cannot be greater than end date' ) );
                exit;
			}

            $status=0;
            $datetime=$validatorInput;

            if((array_key_exists("startdate",$validatorInput))&&(array_key_exists("enddate",$validatorInput))) {
                unset($validatorInput['startdate'], $validatorInput['enddate']);
                Config::updateParam('SYSTEM_MINIGAME_PRIZE', json_encode($validatorInput));
                $status++;
            }


            unset($datetime['eighty'], $datetime['ten'], $datetime['five'], $datetime['three'], $datetime['two']);
            if(Config::updateParam('SYSTEM_MINIGAME_DATETIME', json_encode($datetime))){
                $status++;
            }

            if($status==2){
                echo json_encode( array( 'code' => Lang::get('COMMON.SUCESSFUL') ) );
                exit;
			}

		}else{
			$proValue=array();

            $probability=json_decode(Config::getParam('SYSTEM_MINIGAME_PRIZE'));
            $probDate=json_decode(Config::getParam('SYSTEM_MINIGAME_DATETIME'));

            $proValue['startdate']	=$probDate->startdate == true ? $probDate->startdate : date("Y-m-d\TH:i:00");
            $proValue['enddate']	=$probDate->enddate == true ? $probDate->enddate : date("Y-m-d\TH:i:59");

            $proValue['eighty']	=$probability->eighty == true ? $probability->eighty : 0;
            $proValue['ten']	=$probability->ten == true ? $probability->ten : 0;
            $proValue['five']	=$probability->five == true ? $probability->five : 0;
            $proValue['three']	=$probability->three == true ? $probability->three : 0;
            $proValue['two']	=$probability->two == true ? $probability->two : 0;

            $data['proValue']=$proValue;
			return view('admin.minigame',$data);
        }
    }

}

?>
