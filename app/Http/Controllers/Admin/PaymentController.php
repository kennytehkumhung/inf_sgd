<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller\Admin;
use App\libraries\grid\PanelGrid;
use App\libraries\App;
use Illuminate\Http\Request;
use Auth;
use Validator;
use Session;
use Lang;
use App\Models\Currency;
use App\Models\Bank;
use App\Models\Accounttag;
use App\Models\Tag;
use App\Models\Account;
use App\Models\Config;
use App\Models\Bankaccount;
use App\Models\Fundingmethod;
use App\Models\Fundingmethodsetting;



class PaymentController extends Controller{

	protected $moduleName 	= 'paymentmethod';
	protected $limit 		= 20;
	protected $start 		= 0;
	
	public function __construct()
	{
					
	}
	
	public function paymentShow(Request $request){

			$grid = new PanelGrid;
			$grid->setupGrid($this->moduleName, 'payment-data', $this->limit);
			$grid->setTitle(Lang::get('COMMON.PAYMENT'));
									
			$grid->addColumn('type',				Lang::get('COMMON.PAYMENTMETHOD'), 				160,		array());
			$grid->addColumn('deposit',				Lang::get('COMMON.DEPOSIT'), 					160,		array());
			$grid->addColumn('withdrawal',			Lang::get('COMMON.WITHDRAWAL'), 				160,		array());
			$grid->addColumn('currency',			Lang::get('COMMON.CURRENCY'), 					160,		array());
			$grid->addColumn('status',				Lang::get('COMMON.STATUS'), 					160,		array('align' => 'center'));
			
			$data['grid'] = $grid->getTemplateVars();

			return view('admin.grid2',$data);
    }
	
	protected function paymentdoGetData(Request $request){
		
		//status
		$status[1] = 'Active';
		$status[0] = 'Suspended';
		
		//withdraw
		$withdraw[1] = 'Yes';
		$withdraw[0] = 'No';
		
		//deposit
		$deposit[1] = 'Yes';
		$deposit[0] = 'No';
		
		$aflt  = $request->input('aflt');
		$aqfld = $request->input('aqfld');
		
		$condition = '1';
		//$condition .= ' AND crccode = "'.Session::get('admin_crccode').'"';
		
		if( isset($aqfld['status']) && $aqfld['status'] != 0 ){
			$condition = ' AND status'.'='.$aqfld['status'];
		}
		
		$total = Fundingmethod::whereRaw($condition)->count();

		$rows = array();
		 if($paymentmethods = Fundingmethod::whereRaw($condition)->get()){
			foreach($paymentmethods as $paymentmethod){
				$currency = '';
				if($settings = Fundingmethodsetting::whereRaw('fdmid='.$paymentmethod->id)->get()) {
					foreach($settings as $setting) {
						$currency .= $setting->crccode.' ';
					}
				}
				
				$rows[] = array(
					'type'	    	=> App::formatAddTabUrl( Lang::get('PAYMENT').': '.$paymentmethod->name, urlencode($paymentmethod->name), action('Admin\PaymentController@paymentdrawAddEdit',  array('sid' => $paymentmethod->id))),
					'deposit' 		=> $deposit[$paymentmethod->deposit],
					'withdrawal' 	=> $withdraw[$paymentmethod->withdraw],
					'currency' 		=> $currency,
					//'status' 		=> $paymentmethods->getStatusText($this->moduleName)
				);
			}
		} 
		
		echo json_encode(array('total' => $total, 'rows' => $rows));
		exit;
	
	}
	
	protected function paymentdrawAddEdit(Request $request) { 
	
		$toAdd 	  = true;
		$object   = null;
		$readOnly = false;
		$values   = array();

		$title = Lang::get('NEW').' '.Lang::get('PAYMENT');

		if($request->has('sid')) 
		{
	
			if ($object = Fundingmethod::find($request->input('sid'))) 
				{
					$title    = Lang::get('COMMON.EDIT').' '.Lang::get('COMMON.PAYMENT');
					$toAdd    = false;
					$readOnly = true;

				} 
			else
				{
					$object = null;
				}
			
		}

		$form = App::setupPanelForm('doadd', $request->all(), array(), $toAdd);
		$form->setFormTitle($title);

		$form->addInput('text', 				Lang::get('COMMON.PAYMENTMETHOD')		, 'type',	 		(($object)? $object->name:''), 			array(), false); 
		$form->addInput('text', 				Lang::get('COMMON.DEPOSIT')				, 'deposit',	 	(($object)? $object->deposit:''), 		array(), false); 
		$form->addInput('text', 				Lang::get('COMMON.WITHDRAWAL')			, 'withdrawal',	 	(($object)? $object->withdraw:''), 		array(), false); 
		$form->addInput('text', 				Lang::get('COMMON.CURRENCY')			, 'currency',	 	(($object)? $object->crccode:''), 		array(), false); 
		$form->addInput('radio',   			    Lang::get('COMMON.STATUS')				, 'status',  		(($object)? $object->status:''), 		array('options' => App::getStatusOptions()), true);
	
		$data['form'] = $form->getTemplateVars();
		$data['module'] = 'paymentmethod';

		return view('admin.form2',$data);

	  
	
	}
	
	protected function paymentdoAddEdit(Request $request) {
		
		$validator = Validator::make(
			[
	
				'type'	  	 	  => $request->input('type'),
				'deposit'	  	  => $request->input('deposit'),
				'withdrawal'	  => $request->input('withdrawal'),
				'currency'	  	  => $request->input('currency'),
				'status'	  	  => $request->input('status'),

			],
			[

			   'type'	   	  	   => 'required',
			   'deposit'	  	   => 'required',
			   'withdrawal'	  	   => 'required',
			   'currency'	  	   => 'required',
			   'status'	  	  	   => 'required',

			]
		);
		
		
		if ($validator->fails())
		{
			echo json_encode($validator->errors());
			exit;
		}
		
		
		
		$object  = new Fundingmethod;
		
		if($request->has('sid')) 
		{
			$object = Fundingmethod::find($request->input('sid'));
		}
		
		$object->name	 				= $request->input('type');
		$object->deposit	 			= $request->input('deposit');
		$object->withdraw	 			= $request->input('withdrawal');
		$object->crccode	 			= $request->input('currency');
		$object->status	 				= $request->input('status');


		
		if( $object->save() )
		{
			echo json_encode( array( 'code' => Lang::get('COMMON.SUCESSFUL') ) );
			exit;
		}


		echo json_encode( array( 'code' => Lang::get('COMMON.FAILED') ) );
		
		
		
	}
	
	protected function doActivateSuspend(Request $request) {

			

		if($request->has('id')) {
		
			if($object = Fundingmethod::find($request->id)) {
			
				$currentStatus = $object->status;
				if($request->action == 'activate') {
					$object->status 	= CBO_ACCOUNTSTATUS_ACTIVE;
				}
				if($request->action == 'suspend') {
					$object->status 	= CBO_ACCOUNTSTATUS_SUSPENDED;
				}

				if($currentStatus <> $object->status){
					if($object->save()) {
					
						echo json_encode( array( 'code' => Lang::get('COMMON.SUCESSFUL') ) );
						exit;
					}
				}
			}
		}
		
		
		echo json_encode( array( 'code' => Lang::get('COMMON.FAILED') ) );

	}
	



}

?>
