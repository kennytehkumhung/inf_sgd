<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller\Admin;
use App\Models\Account;

class WelcomeController extends AdminController {

	/*
	|--------------------------------------------------------------------------
	| Welcome Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('guest');
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{

		//$user = DB::table('cbo_account')->find(176);;
		//var_dump($user);
		//var_dump(Account::all());
		return view('welcome');
	}	
	
	public function test()
	{
		echo 1;

	}

}
