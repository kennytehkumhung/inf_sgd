<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller\Admin;
use App\Http\Controllers\User\FortuneWheelController;
use App\libraries\App;
use App\libraries\grid\PanelGrid;
use App\Models\Account;
use App\Models\Configs;
use App\Models\FortuneWheelAcc;
use App\Models\FortuneWheelAccIdr;
use App\Models\FortuneWheelLedger;
use App\Models\FortuneWheelPrize;
use App\Models\FortuneWheelTrans;
use App\Models\FortuneWheelTransIdr;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Lang;
use Session;

class LuckySpinController extends Controller {

    protected $moduleName = 'luckyspin';
    protected $limit = 20;
    protected $start = 0;

    public function index(Request $request)
    {
        $grid = new PanelGrid;
        $grid->setupGrid($this->moduleName, 'luckySpinReport-data', $this->limit, true, array('footer' => true));
        $grid->setTitle(Lang::get('COMMON.LUCKYSPINREPORT'));

        // TODO translate
        $grid->addColumn('acccode', 			Lang::get('COMMON.USERNAME'),				108,	array('align' => 'left'));
        $grid->addColumn('crccode', 			Lang::get('COMMON.CURRENCY'),				68,	array('align' => 'left'));
        $grid->addColumn('token_balance', 		'Total Token Balance', 					120,	array('align' => 'right'));
        $grid->addColumn('free_token_balance', 'Free Token Balance', 					120,	array('align' => 'right'));
        $grid->addColumn('forfeited_token',	'Forfeited Token', 						120,	array('align' => 'right'));
        $grid->addColumn('total_spin', 		'Total Spin', 								100,	array('align' => 'right'));
        $grid->addColumn('total_won', 			'Total Won', 								100,	array('align' => 'right'));
        $grid->addColumn('total_claim', 		'Total Claim', 							180,	array('align' => 'right'));
        $grid->addColumn('win_percent', 		'Winning Percentage', 						120,	array('align' => 'center'));
        $grid->addColumn('probability_set', 	'Probability Set', 						120,	array('align' => 'center'));

        $grid->addSearchField('search1', array('acccode' => Lang::get('COMMON.USERNAME')));
        $grid->addSearchField('search2', array('currency' => 'currency'), array('value' => $request->input('currency', 'MYR'), 'hidden' => true));

        $data['grid'] = $grid->getTemplateVars();

        return view('admin.grid2',$data);
    }

    protected function doGetData(Request $request){

        $crc = 'MYR';
        $crcPrefix = '';

        if( isset($request->aflt['currency']) && $request->aflt['currency'] == 'IDR' ) {
            $crc = 'IDR';
            $crcPrefix = '_idr';
        }

        $footer = array();
        $rows = array();
        $builder = DB::table('fortune_wheel_acc'.$crcPrefix.' as a')
            ->join('account as b', 'b.id', '=', 'a.accid')
            ->orderBy('a.acccode', 'asc')
            ->select(array('a.accid', 'a.acccode', 'b.crccode', 'a.probability_set'));

        if( isset($request->aflt['acccode']) && $request->aflt['acccode'] != '' ){
            $builder->where('a.acccode', '=', $request->aflt['acccode']);
        }

        $today = Carbon::now()->toDateString();
        $createdFrom = $request->input('createdfrom', $today);
        $createdTo = $request->input('createdto', $today);

        $createdFrom .= ' 00:00:00';
        $createdTo .= ' 23:59:59';

        $total = count($builder->get());
        $records = $builder->take( $request->pagesize )
            ->skip( $request->recordstartindex )
            ->get();

        $todayDt = Carbon::now();

        // Calculate total amount for footer.
        $footerTotalTokenBalance = DB::table('fortune_wheel_ledger'.$crcPrefix)
            ->where('is_used', '=', 0)
            ->where('expired_at', '>', $todayDt)
            ->whereBetween('created_at', array($createdFrom, $createdTo))
            ->sum('amount');

        $footerTotalFreeTokenBalance = DB::table('fortune_wheel_ledger'.$crcPrefix)
            ->where('category', '=', FortuneWheelLedger::CATEGORY_FREE_TOKEN)
            ->where('is_used', '=', 0)
            ->where('expired_at', '>', $todayDt)
            ->whereBetween('created_at', array($createdFrom, $createdTo))
            ->sum('amount');

        $footerTotalForfeitedToken = DB::table('fortune_wheel_ledger'.$crcPrefix)
            ->whereBetween('created_at', array($createdFrom, $createdTo))
            ->where(function ($query) use ($todayDt, $createdFrom, $createdTo) {
                $query->where('expired_at', '<', $todayDt)
                    ->where('is_used', '=', 0)
                    ->orWhere('is_manual_forfeited', '=', 1);
            })->sum('amount');

        $footerTotalSpin = DB::table('fortune_wheel_trans'.$crcPrefix)
            ->whereNotIn('prize_type', array('token', 'claim'))
            ->whereBetween('created_at', array($createdFrom, $createdTo))
            ->count();

        if ($crc == 'IDR') {
            $footerTotalWon = FortuneWheelTransIdr::getTotalWon(null, $createdFrom, $createdTo);
        } else {
            $footerTotalWon = FortuneWheelTrans::getTotalWon(null, $createdFrom, $createdTo);
        }

        $footerTotalClaim = DB::table('fortune_wheel_trans'.$crcPrefix)
            ->whereBetween('created_at', array($createdFrom, $createdTo))
            ->where('game_act', '=', FortuneWheelTrans::GAME_ACT_CLAIM)
            ->sum('amount');

        // Same as App\Http\Controllers\User\FortuneWheelController@claimTotalWon()
        $footerApprovedClaim = DB::table('cashledger as a')
            ->join('promocash as b', 'b.id', '=', 'a.refid')
            ->join('promocampaign as c', 'c.id', '=', 'b.pcpid')
            ->where('a.refobj', '=', 'Promocash')
            ->where('c.code', '=', 'grandroyalewheel')
            ->whereIn('a.status', array(1, 4))
            ->whereBetween('a.created', array($createdFrom, $createdTo))
            ->sum('a.amount');

        $footerTotalClaim = App::displayAmount($footerTotalClaim) . '<br>(Approved: ' . App::displayAmount($footerApprovedClaim) . ')';

        foreach( $records as $r ) {

            $tokenBalance = DB::table('fortune_wheel_ledger'.$crcPrefix)
                ->where('accid', '=', $r->accid)
                ->where('is_used', '=', 0)
                ->where('expired_at', '>', $todayDt)
                ->whereBetween('created_at', array($createdFrom, $createdTo))
                ->sum('amount');

            $freeTokenBalance = DB::table('fortune_wheel_ledger'.$crcPrefix)
                ->where('accid', '=', $r->accid)
                ->where('category', '=', FortuneWheelLedger::CATEGORY_FREE_TOKEN)
                ->where('is_used', '=', 0)
                ->where('expired_at', '>', $todayDt)
                ->whereBetween('created_at', array($createdFrom, $createdTo))
                ->sum('amount');

            $freeTokenBalance = ($freeTokenBalance < 0 ? 0 : $freeTokenBalance);

            $forfeitedToken = DB::table('fortune_wheel_ledger'.$crcPrefix)
                ->whereBetween('created_at', array($createdFrom, $createdTo))
                ->where('accid', '=', $r->accid)
                ->where(function ($query) use ($todayDt, $createdFrom, $createdTo) {
                    $query->where('expired_at', '<', $todayDt)
                        ->where('is_used', '=', 0)
                        ->orWhere('is_manual_forfeited', '=', 1);
                })->sum('amount');

            $totalSpin = DB::table('fortune_wheel_trans'.$crcPrefix)
                ->where('accid', '=', $r->accid)
//                ->whereIn('game_act', array(FortuneWheelTrans::GAME_ACT_SPIN_PRIZE, FortuneWheelTrans::GAME_ACT_SPIN_BONUS)) // Line 261
                ->whereNotIn('prize_type', array('token', 'claim'))
                ->whereBetween('created_at', array($createdFrom, $createdTo))
                ->count();

            $totalMatch = DB::table('fortune_wheel_trans'.$crcPrefix)
                ->where('accid', '=', $r->accid)
                ->groupBy('match_id')
                ->where('match_id', '>', 0)
                ->whereBetween('created_at', array($createdFrom, $createdTo))
                ->get(['id']);

            $totalMatch = count($totalMatch);

            $totalMatchLose = DB::table('fortune_wheel_trans'.$crcPrefix)
                ->where('accid', '=', $r->accid)
                ->where('prize_type', '=', 'lose')
                ->whereBetween('created_at', array($createdFrom, $createdTo))
                ->count();

            if ($crc == 'IDR') {
                $totalWon = FortuneWheelTransIdr::getTotalWon($r->accid, $createdFrom, $createdTo);
            } else {
                $totalWon = FortuneWheelTrans::getTotalWon($r->accid, $createdFrom, $createdTo);
            }

            $totalClaim = DB::table('fortune_wheel_trans'.$crcPrefix)
                ->where('accid', '=', $r->accid)
                ->whereBetween('created_at', array($createdFrom, $createdTo))
                ->where('game_act', '=', FortuneWheelTrans::GAME_ACT_CLAIM)
                ->sum('amount');

            $probabilityOptions = ['', '', '', ''];
            $probabilityOptions[$r->probability_set - 1] = 'selected';

            if ($totalMatch > 0) {
                $winPercent = number_format((($totalMatch - $totalMatchLose) / $totalMatch) * 100, 4);
            } else {
                $winPercent = number_format(0, 4);
            }

            $rows[] = array(
                'acccode'			 => '<a href="#" onclick="parent.addTab(\''.Lang::get('COMMON.LUCKYSPINREPORT').':'.$r->acccode.'\', \'luckySpinReportDetail?sid='.$r->accid.'&currency='.$crc.'&createdfrom='.$createdFrom.'&createdto='.$createdTo.'\')" style="cursor:pointer">'.$r->acccode.'</a>',
                'crccode' 			 => $r->crccode,
                'token_balance' 	 => '<a href="#" onclick="parent.addTab(\'Lucky Spin Token Ledger:'.$r->acccode.'\', \'luckySpinReportTokenLedger?sid='.$r->accid.'&currency='.$crc.'&createdfrom='.$createdFrom.'&createdto='.$createdTo.'\')">'.$tokenBalance.'</a>',
                'free_token_balance' => $freeTokenBalance,
                'forfeited_token' 	 => $forfeitedToken,
                'total_spin' 		 => $totalSpin,
                'total_won' 		 => App::displayAmount($totalWon - $totalClaim),
                'total_claim' 		 => App::displayAmount($totalClaim),
                'win_percent' 		 => $winPercent . '%',
                'probability_set'	 => '<select onchange="autoloadBinding(this);" data-href="'.action('Admin\LuckySpinController@doEditData').'?sid='.$r->accid.'&currency='.$crc.'" data-hasvalue="y" data-loadingimg="a" data-outval="n">'.
                    '<option value="1" '.$probabilityOptions[0].'>1</option>'.
                    '<option value="2" '.$probabilityOptions[1].'>2</option>'.
                    '<option value="3" '.$probabilityOptions[2].'>3</option>'.
                    '<option value="4" '.$probabilityOptions[3].'>4</option>'.
                    '</select>',
            );
        }

        $footer[] = array(
            'acccode' 			 => '',
            'crccode' 			 => '',
//            'token_balance' 	 => $footerTotalTokenBalance,
            'token_balance' 	 => '<a href="#" onclick="parent.addTab(\'Lucky Spin Token Ledger:\', \'luckySpinReportTokenLedger?createdfrom='.$createdFrom.'&createdto='.$createdTo.'&currency='.$crc.'\')">'.$footerTotalTokenBalance.'</a>',
            'free_token_balance' => $footerTotalFreeTokenBalance,
            'forfeited_token' 	 => $footerTotalForfeitedToken,
            'total_spin' 		 => $footerTotalSpin,
            'total_won' 		 => App::displayAmount($footerTotalWon),
            'total_claim' 		 => $footerTotalClaim,
            'win_percent' 		 => '',
            'probability_set' 	 => '',
        );

        return response(json_encode(array('total' => $total , 'rows' => $rows , 'footer' => $footer)));
    }

    public function doEditData(Request $request)
    {
        $crc = $request->input('currency');

        if (strlen($crc) > 0) {
            $crcPrefix = '';

            if ($crc == 'IDR') {
                $crcPrefix = '_idr';
            }

            $accObj = DB::table('fortune_wheel_acc'.$crcPrefix)->where('accid', '=', $request->input('sid'))->first();

            if ($accObj) {
                $probabilitySet = $request->input('val');
                $acceptedProbabilityOptions = [1, 2, 3, 4];

                if (in_array($probabilitySet, $acceptedProbabilityOptions)) {
                    DB::table('fortune_wheel_acc'.$crcPrefix)
                        ->where('accid', '=', $accObj->accid)
                        ->update(array(
                            'probability_set' => $probabilitySet,
                        ));

                    return response('Record saved.');
                }
            }
        }

        return response('', 500);
    }

    public function doGetReportDetail(Request $request)
    {
        $grid = new PanelGrid;
        $grid->setupGrid($this->moduleName, 'luckySpinReportDetail-data', $this->limit, true, array('footer' => true));
        $grid->setTitle('Lucky Spin Report Detail');

        // TODO translate
        $grid->addColumn('acccode', 		Lang::get('COMMON.USERNAME'),				108,	array('align' => 'left'));
        $grid->addColumn('crccode', 		Lang::get('COMMON.CURRENCY'),				68,	array('align' => 'left'));
        $grid->addColumn('match_id', 		'Match ID', 								100,	array('align' => 'left'));
        $grid->addColumn('spin_count', 	'Spin Count', 								68,	array('align' => 'right'));
        $grid->addColumn('status', 		Lang::get('COMMON.STATUS'), 				100,	array('align' => 'left'));
        $grid->addColumn('prize_name', 	'Prize Name', 								100,	array('align' => 'left'));
        $grid->addColumn('prize_type', 	'Prize Type', 								100,	array('align' => 'left'));
        $grid->addColumn('amount', 		'Amount', 									100,	array('align' => 'right'));
        $grid->addColumn('created_at', 	Lang::get('COMMON.CREATEDON'), 				150,	array('align' => 'center'));

        $accObj = Account::find($request->input('sid'));

        $grid->addSearchField('search1', array('acccode' => Lang::get('COMMON.USERNAME')), array('value' => $accObj->nickname));
        $grid->addSearchField('search2', array('currency' => 'currency'), array('value' => $request->input('currency', 'MYR'), 'hidden' => true));
        $grid->addFilter('status', FortuneWheelTrans::getStatusOptions(), array('display' => Lang::get('COMMON.STATUS')));

        $data['grid'] = $grid->getTemplateVars();

        if ($request->has('createdfrom')) {
            $data['grid']['options']['params']['createdfrom'] = $request->input('createdfrom');
        }

        if ($request->has('createdto')) {
            $data['grid']['options']['params']['createdto'] = $request->input('createdto');
        }

        return view('admin.grid2',$data);
    }

    public function doGetReportDetailData(Request $request)
    {
        $crc = 'MYR';
        $crcPrefix = '';

        if( isset($request->aflt['currency']) && $request->aflt['currency'] == 'IDR' ) {
            $crc = 'IDR';
            $crcPrefix = '_idr';
        }

        $footer = array();
        $rows = array();
        $today = Carbon::now()->toDateString();
        $createdFrom = $request->input('createdfrom', $today);
        $createdTo = $request->input('createdto', $today);

        $createdFrom .= ' 00:00:00';
        $createdTo .= ' 23:59:59';

        $builder = DB::table('fortune_wheel_trans'.$crcPrefix.' as a')
            ->join('account as b', 'b.id', '=', 'a.accid')
            ->whereIn('a.id', DB::table('fortune_wheel_trans'.$crcPrefix.' as t')
                ->where('t.acccode', '=', $request->aflt['acccode'])
                ->whereBetween('t.created_at', array($createdFrom, $createdTo))
                ->groupBy('t.match_id')
                ->selectRaw('MAX(t.id) as id')
                ->lists('id')
//            )->orderBy('a.created_at', 'desc')
            )->orderBy('a.id', 'desc')
            ->select(array('a.accid', 'a.acccode', 'b.crccode', 'a.match_id', 'a.game_act', 'a.status', 'a.prize_id', 'a.prize_type', 'a.amount', 'a.created_at'));

        if ( isset($request->aqfld['status']) && $request->aqfld['status'] != '' ) {
            $builder->where('a.status', '=', $request->aqfld['status']);
        }

        $total = $builder->count();
        $records = $builder->take( $request->pagesize )
            ->skip( $request->recordstartindex )
            ->get();

        // Collection.
        $transStatusOptions = collect(FortuneWheelTrans::getStatusOptions());
        $prizeObj = DB::table('fortune_wheel_prize'.$crcPrefix)
            ->whereIn('position_type', array(FortuneWheelPrize::POSITION_TYPE_PRIZE, FortuneWheelPrize::POSITION_TYPE_BONUS))
            ->lists('prize_name', 'id');

        $prizeObj = collect($prizeObj);

        $footerSpinCount = 0;
        $footerAmount = 0;

        foreach( $records as $r ) {
            $spinCount = '';
            $matchId = '';
            $status = '';

            if ($r->match_id > 0) {
                $matchId = '<a href="#" onclick="parent.addTab(\'Lucky Spin Match Detail:'.$r->match_id.'\', \'luckySpinReportMatchDetail?sid='.$r->match_id.'&currency='.$crc.'&createdfrom='.$createdFrom.'&createdto='.$createdTo.'\')" style="cursor:pointer">'.$r->match_id.'</a>';
                $status = $transStatusOptions->get($r->status);

                $spinCount = DB::table('fortune_wheel_trans'.$crcPrefix)
                    ->where('match_id', '=', $r->match_id)
                    ->whereBetween('created_at', array($createdFrom, $createdTo))
                    ->whereNotIn('prize_type', array('token', 'claim'))
                    ->count();

                $footerSpinCount += $spinCount;
            }

            $footerAmount += $r->amount;

            $rows[] = array(
                'acccode' 		=> $r->acccode,
                'crccode' 		=> $r->crccode,
                'match_id'		=> $matchId,
                'spin_count'	=> $spinCount,
                'status' 		=> $status,
                'prize_name' 	=> ($r->prize_id > 0 ? $prizeObj->get($r->prize_id) : $r->prize_type),
                'prize_type' 	=> $r->prize_type,
                'amount' 		=> App::displayAmount($r->amount),
                'created_at' 	=> $r->created_at,
            );
        }

        $footer[] = array(
            'acccode' 		=> '',
            'crccode' 		=> '',
            'match_id'		=> '',
            'spin_count'	=> $footerSpinCount,
            'status' 		=> '',
            'prize_name' 	=> '',
            'prize_type' 	=> '',
            'amount' 		=> App::displayAmount($footerAmount),
            'created_at' 	=> '',
        );

        return response(json_encode(array('total' => $total , 'rows' => $rows , 'footer' => $footer)));
    }

    public function doGetReportTokenLedger(Request $request)
    {
        $grid = new PanelGrid;
        $grid->setupGrid($this->moduleName, 'luckySpinReportTokenLedger-data', $this->limit, true, array('footer' => true));
        $grid->setTitle('Lucky Spin Report Token Ledger');

        // TODO translate
        $grid->addColumn('acccode', 		Lang::get('COMMON.USERNAME'),				108,	array('align' => 'left'));
        $grid->addColumn('type', 			Lang::get('public.Type'), 					180,	array('align' => 'left'));
        $grid->addColumn('amount', 		Lang::get('public.Amount'), 				100,	array('align' => 'right'));
        $grid->addColumn('token_status', 	'Token Status', 							100,	array('align' => 'center'));
        $grid->addColumn('remark', 		Lang::get('public.Remark'), 				300,	array('align' => 'left'));
        $grid->addColumn('expired_at', 	'Expired On', 								150,	array('align' => 'center'));
        $grid->addColumn('created_at', 	Lang::get('COMMON.CREATEDON'), 				150,	array('align' => 'center'));

        $searchUsername = '';

        if ($request->has('sid')) {
            $accObj = Account::find($request->input('sid'));

            if ($accObj) {
                $searchUsername = $accObj->nickname;
            }
        }

        $typeOptions = FortuneWheelLedger::getSysRefOptions(true);
        unset($typeOptions[FortuneWheelLedger::SYS_REF_TRANS]);

        $grid->addSearchField('search1', array('acccode' => Lang::get('COMMON.USERNAME')), array('value' => $searchUsername));
        $grid->addSearchField('search2', array('currency' => 'currency'), array('value' => $request->input('currency', 'MYR'), 'hidden' => true));
        $grid->addFilter('status', array(0 => 'All', 1 => 'Available', 2 => 'Used', 3 => 'Manual Forfeited', 4 => 'Expired'), array('display' => 'Status'));
        $grid->addFilter('type', $typeOptions, array('display' => 'Token Type'));

        $data['grid'] = $grid->getTemplateVars();

        if ($request->has('createdfrom')) {
            $data['grid']['options']['params']['createdfrom'] = $request->input('createdfrom');
        }

        if ($request->has('createdto')) {
            $data['grid']['options']['params']['createdto'] = $request->input('createdto');
        }

        return view('admin.grid2',$data);
    }

    public function doGetReportTokenLedgerData(Request $request)
    {
        $crc = 'MYR';
        $crcPrefix = '';

        if( isset($request->aflt['currency']) && $request->aflt['currency'] == 'IDR' ) {
            $crc = 'IDR';
            $crcPrefix = '_idr';
        }

        $rows = array();
        $today = Carbon::now()->toDateString();
        $createdFrom = $request->input('createdfrom', $today);
        $createdTo = $request->input('createdto', $today);

        $createdFrom .= ' 00:00:00';
        $createdTo .= ' 23:59:59';

        $builder = DB::table('fortune_wheel_ledger'.$crcPrefix.' as a')
            ->whereBetween('a.created_at', array($createdFrom, $createdTo))
//            ->orderBy('a.created_at', 'desc')
            ->orderBy('a.id', 'desc')
            ->select(array('a.id', 'a.acccode', 'a.amount', 'a.is_used', 'a.is_manual_forfeited', 'a.sys_ref', 'a.remark', 'a.expired_at', 'a.created_at'));

        if ( isset($request->aflt['acccode']) && $request->aflt['acccode'] != '' ) {
            $builder->where('a.acccode', '=', $request->aflt['acccode']);
        }

        if ( isset($request->aqfld['status']) && $request->aqfld['status'] != '' ) {
            switch ($request->aqfld['status']) {
                case 1:
                    // Available.
                    $builder->where('a.is_used', '=', 0)
                        ->where('a.expired_at', '>', Carbon::now())
                        ->where('a.is_manual_forfeited', '=', 0);
                    break;
                case 2:
                    // Used.
                    $builder->where('a.is_used', '=', 1)
//                        ->where('a.expired_at', '>', Carbon::now())
                        ->where('a.is_manual_forfeited', '=', 0);
                    break;
                case 3:
                    // Manual Forfeited.
                    $builder->where('a.is_manual_forfeited', '=', 1);
                    break;
                case 4:
                    // Expired.
                    $builder->where('a.is_used', '=', 0)
                        ->where('a.expired_at', '<', Carbon::now())
                        ->where('a.is_manual_forfeited', '=', 0);
                    break;
            }
        }

        if ( isset($request->aqfld['type']) && $request->aqfld['type'] != '' ) {
            $builder->where('a.sys_ref', '=', $request->aqfld['type']);
        }

        $total = $builder->count();
        $records = $builder->take( $request->pagesize )
            ->skip( $request->recordstartindex )
            ->get();

        // Collection.
        $tledgerObjectOptions = collect(FortuneWheelLedger::getSysRefOptions());
        $footerTotalTokenAmount = 0;
        $nowDt = Carbon::now();

        foreach( $records as $r ) {
            $footerTotalTokenAmount += $r->amount;
            $status = '';

            if ( $r->is_manual_forfeited == 1 ) {
                $status = 'Manual Forfeited';
            } elseif ( $nowDt->gt(Carbon::createFromFormat('Y-m-d H:i:s', $r->expired_at)) && $r->is_used == 0 ) {
                $status = 'Expired';
            } elseif ( $r->is_used == 1 ) {
                $status = 'Used';
            }

            $rows[] = array(
                'acccode' 		=> $r->acccode,
                'amount'		=> $r->amount,
                'token_status'	=> ($status != '' ? $status : '<span onclick="if(confirm(\'Confirm forfeit this token?\')) {autoloadBinding(this)};" data-href="'.action('Admin\LuckySpinController@doEditReportTokenLedgerData').'?sid='.$r->id.'&currency='.$crc.'" data-outval="n" data-reload="y"><a href="javascript:void(0);">Available</a></span>'),
                'remark'		=> $r->remark,
                'type'			=> $tledgerObjectOptions->get($r->sys_ref, $r->sys_ref),
                'expired_at' 	=> $r->expired_at,
                'created_at' 	=> $r->created_at,
            );
        }

        $footer[] = array(
            'acccode' 			 => '',
            'amount' 			 => $footerTotalTokenAmount,
            'token_status' 	 	 => '',
            'remark' 	 	 	 => '',
            'type' 	 	 	 	 => '',
            'expired_at' 		 => '',
            'created_at' 		 => '',
        );

        return response(json_encode(array('total' => $total , 'rows' => $rows , 'footer' => $footer)));
    }

    public function doEditReportTokenLedgerData(Request $request)
    {
        $crc = $request->input('currency');

        if (strlen($crc) > 0) {
            $crcPrefix = '';

            if ($crc == 'IDR') {
                $crcPrefix = '_idr';
            }

            $rcount = DB::table('fortune_wheel_ledger'.$crcPrefix)
                ->where('id', '=', $request->input('sid'))
                ->update([
                    'is_used' => 1,
                    'is_manual_forfeited' => 1,
                    'remark' => Session::get('admin_username') . ': manually forfeited at ' . Carbon::now()->toDateTimeString(),
                ]);

            if ($rcount > 0) {
                return response(json_encode(['code' => Lang::get('COMMON.SUCESSFUL')]));
            }
        }

        return response('', 500);
    }

    public function doEditReportForfeitedTokenLedgerData(Request $request)
    {
        $crc = $request->input('currency');

        if (strlen($crc) > 0) {
            $crcPrefix = '';

            if ($crc == 'IDR') {
                $crcPrefix = '_idr';
            }

            $rcount = DB::table('fortune_wheel_ledger'.$crcPrefix)
                ->where('id', '=', $request->input('sid'))
                ->update([
                    'is_used' => 1,
                    'is_manual_forfeited' => 1,
                    'remark' => Session::get('admin_username') . ': manually forfeited at ' . Carbon::now()->toDateTimeString(),
                ]);

            if ($rcount > 0) {
                return response(json_encode(['code' => Lang::get('COMMON.SUCESSFUL')]));
            }
        }

        return response('', 500);
    }
	
    public function doGetReportMatchDetail(Request $request)
    {
        $grid = new PanelGrid;
        $grid->setupGrid($this->moduleName, 'luckySpinReportMatchDetail-data', $this->limit);
        $grid->setTitle('Lucky Spin Report Detail');

        // TODO translate
        $grid->addColumn('acccode', 			Lang::get('COMMON.USERNAME'),				108,	array('align' => 'left'));
        $grid->addColumn('crccode', 			Lang::get('COMMON.CURRENCY'),				68,	array('align' => 'left'));
        $grid->addColumn('match_id', 			'Match ID', 								100,	array('align' => 'left'));
        $grid->addColumn('game_act', 			'Game Action', 							100,	array('align' => 'left'));
        $grid->addColumn('status', 			Lang::get('COMMON.STATUS'), 				100,	array('align' => 'left'));
        $grid->addColumn('prize_name', 		'Prize Name', 								100,	array('align' => 'left'));
        $grid->addColumn('prize_type', 		'Prize Type', 								100,	array('align' => 'left'));
        $grid->addColumn('amount', 			Lang::get('public.Amount'), 				100,	array('align' => 'right'));
        $grid->addColumn('used_token', 		'Used Token', 								180,	array('align' => 'left'));
        $grid->addColumn('probability_set', 	'Prize Setting', 							100,	array('align' => 'center'));
        $grid->addColumn('created_at', 		Lang::get('COMMON.CREATEDON'), 				150,	array('align' => 'center'));

        $grid->addSearchField('search1', array('match_id' => 'Match ID'), array('value' => $request->input('sid')));
        $grid->addSearchField('search2', array('currency' => 'currency'), array('value' => $request->input('currency', 'MYR'), 'hidden' => true));

        $data['grid'] = $grid->getTemplateVars();

        if ($request->has('createdfrom')) {
            $data['grid']['options']['params']['createdfrom'] = $request->input('createdfrom');
        }

        if ($request->has('createdto')) {
            $data['grid']['options']['params']['createdto'] = $request->input('createdto');
        }

        return view('admin.grid2',$data);
    }

    public function doGetReportMatchDetailData(Request $request)
    {
        $crc = '';
        $crcPrefix = '';

        if( isset($request->aflt['currency']) && $request->aflt['currency'] == 'IDR' ) {
            $crc = 'IDR';
            $crcPrefix = '_idr';
        }

        $rows = array();
        $today = Carbon::now()->toDateString();
        $createdFrom = $request->input('createdfrom', $today);
        $createdTo = $request->input('createdto', $today);

        $createdFrom .= ' 00:00:00';
        $createdTo .= ' 23:59:59';

        $builder = DB::table('fortune_wheel_trans'.$crcPrefix.' as a')
            ->leftJoin('fortune_wheel_ledger'.$crcPrefix.' as b', function ($join) use ($crc) {
                if ($crc == 'IDR') {
                    $join->on('b.refid', '=', 'a.id')->where('b.object', '=', 'FortuneWheelTransIdr');
                } else {
                    $join->on('b.refid', '=', 'a.id')->where('b.object', '=', 'FortuneWheelTrans');
                }
            })
            ->join('account as c', 'c.id', '=', 'a.accid')
            ->where('a.match_id', '=', $request->aflt['match_id'])
            ->whereBetween('a.created_at', array($createdFrom, $createdTo))
            ->orderBy('a.id', 'desc')
            ->select(array('a.accid', 'a.acccode', 'c.crccode', 'a.match_id', 'a.game_act', 'a.status', 'a.prize_id', 'a.prize_type', 'a.amount', 'a.probability_set', 'a.created_at', 'b.sys_ref'));

        $total = $builder->count();
        $records = $builder->take( $request->pagesize )
            ->skip( $request->recordstartindex )
            ->get();

        // Collection.
        $sysRefOptions = collect(FortuneWheelLedger::getSysRefOptions());
        $transStatusOptions = collect(FortuneWheelTrans::getStatusOptions());
        $prizeObj = DB::table('fortune_wheel_prize'.$crcPrefix)
            ->whereIn('position_type', array(FortuneWheelPrize::POSITION_TYPE_PRIZE, FortuneWheelPrize::POSITION_TYPE_BONUS))
            ->lists('prize_name', 'id');

        $prizeObj = collect($prizeObj);

        foreach( $records as $r ) {
            $rows[] = array(
                'acccode' 		=> $r->acccode,
                'crccode' 		=> $r->crccode,
                'match_id'		=> $r->match_id,
                'game_act'		=> $r->game_act,
                'status' 		=> $transStatusOptions->get($r->status, $r->status),
                'prize_name' 	=> $prizeObj->get($r->prize_id, $r->prize_id),
                'prize_type' 	=> $r->prize_type,
                'amount' 		=> App::displayAmount($r->amount),
                'used_token' 	=> $sysRefOptions->get($r->sys_ref, $r->sys_ref),
                'probability_set' => 'Set ' . $r->probability_set,
                'created_at' 	=> $r->created_at,
            );
        }

        return response(json_encode(array('total' => $total , 'rows' => $rows)));
    }

    public function showConfig(Request $request)
    {
        $crc = $request->input('currency');
        $crcPrefix = '';

        if ($crc == 'IDR') {
            $crcPrefix = '_idr';
        }

        $configObj = DB::table('fortune_wheel_prize'.$crcPrefix)
            ->whereIn('position_type', [0, 2, 3])
            ->get();

        $dailyLoginFreeTokenAmount = json_decode(Configs::getParam('SYSTEM_LUCKYSPIN_DAILYLOGINFREETOKEN'.strtoupper($crcPrefix)), true);
        $tokenValidityDays = json_decode(Configs::getParam('SYSTEM_LUCKYSPIN_TOKEN_VALIDITY_DAYS'.strtoupper($crcPrefix)), true);
        $winnerList = json_decode(Configs::getParam('SYSTEM_LUCKYSPIN_WINNER_LIST'.strtoupper($crcPrefix)), true);
        $currency = $crc;

        return view('admin.luckyspinconfig', compact('configObj', 'dailyLoginFreeTokenAmount', 'winnerList', 'tokenValidityDays', 'currency'));
    }

    public function doEditConfig(Request $request)
    {
        $crcPrefix = '';

        if ($request->input('currency') == 'IDR') {
            $crcPrefix = '_idr';
        }

        $inputs = $request->input('inputs');

        foreach ($inputs as $key => $val) {
            DB::table('fortune_wheel_prize'.$crcPrefix)
                ->where('id', '=', $key)
                ->update([
                    'prize_value' => $val['value'],
                    'win_probability1' => $val['win_probability1'],
                    'win_probability_free_token1' => $val['win_probability_free_token1'],
                    'win_probability2' => $val['win_probability2'],
                    'win_probability_free_token2' => $val['win_probability_free_token2'],
                    'win_probability3' => $val['win_probability3'],
                    'win_probability_free_token3' => $val['win_probability_free_token3'],
                    'win_probability4' => $val['win_probability4'],
                    'win_probability_free_token4' => $val['win_probability_free_token4'],
                    'updated_at' => DB::raw('NOW()'),
                ]);
        }

        FortuneWheelController::clearCache();

        return response(json_encode('Config saved.'));
    }

    public function doEditFreeToken(Request $request)
    {
        $crc = $request->input('currency');
        $crcPrefix = '';

        if (strlen($crc) > 0) {
            if ($crc == 'IDR') {
                $crcPrefix = '_idr';
            }
        } else {
            return response(json_encode('Invalid currency.'));
        }

        $msg = '';
        $to = $request->input('to');
        $acccode = $request->input('acccode');
        $amount = $request->input('amount');
        $remark = $request->input('remark');
        $sysRefName = $request->input('sys_ref');
        $tokenType = 'f';

        if (str_contains($sysRefName, 'NT')) {
            $tokenType = 'n';
        }

        $tokenValidityDaysConfig = json_decode(Configs::getParam('SYSTEM_LUCKYSPIN_TOKEN_VALIDITY_DAYS'.strtoupper($crcPrefix)), true);
        $tokenCategory = '2';
        $tokenValidityDays = $tokenValidityDaysConfig[$tokenType];

        if ($tokenType == 'f') {
            $tokenCategory = '1';
        }

        if ($to == 'all') {
            Account::where('crccode', '=', $crc)->chunk(200, function ($rows) use ($amount, $sysRefName, $tokenCategory, $tokenValidityDays, $remark, $crc, $crcPrefix) {
                foreach ($rows as $r) {
                    if ($crc == 'IDR') {
                        FortuneWheelAccIdr::ensureExists($r->id, $r->nickname);
                    } else {
                        FortuneWheelAcc::ensureExists($r->id, $r->nickname);
                    }

                    for ($i = 0; $i < $amount; $i++) {
                        DB::table('fortune_wheel_ledger'.$crcPrefix)->insert(array(
                            'accid' => $r->id,
                            'acccode' => $r->nickname,
                            'amount' => 1,
                            'category' => $tokenCategory,
                            'is_used' => 0,
                            'refid' => 0,
                            'object' => '',
                            'sys_ref' => $sysRefName,
                            'remark' => $remark,
                            'expired_at' => Carbon::now()->addDays($tokenValidityDays),
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ));
                    }
                }
            });

            $msg = 'Done giving free token to everyone.';
        } else {
            $accObj = Account::where('crccode', '=', $crc)->where('nickname', '=', $acccode)->first();

            if ($accObj) {
                if ($crc == 'IDR') {
                    FortuneWheelAccIdr::ensureExists($accObj->id, $accObj->nickname);
                } else {
                    FortuneWheelAcc::ensureExists($accObj->id, $accObj->nickname);
                }

                for ($i = 0; $i < $amount; $i++) {
                    DB::table('fortune_wheel_ledger'.$crcPrefix)->insert(array(
                        'accid' => $accObj->id,
                        'acccode' => $accObj->nickname,
                        'amount' => 1,
                        'category' => $tokenCategory,
                        'is_used' => 0,
                        'refid' => 0,
                        'object' => '',
                        'sys_ref' => $sysRefName,
                        'remark' => $remark,
                        'expired_at' => Carbon::now()->addDays($tokenValidityDays),
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                    ));
                }

                $msg = 'Done giving free token to: ' . $accObj->nickname;
            } else {
                $msg = 'Username not found.';
            }
        }

        return response(json_encode($msg));
    }

    public function doEditTokenSettings(Request $request)
    {
        $crcPrefix = '';

        if ($request->input('currency') == 'IDR') {
            $crcPrefix = '_idr';
        }

        $msg = 'Amount updated.';
        $values = [
            'd' => $request->input('daily_login_free_token_amount', 0),
            'w' => $request->input('weekly_login_free_token_amount', 0),
            'm' => $request->input('monthly_login_free_token_amount', 0),
        ];

        Configs::updateParam('SYSTEM_LUCKYSPIN_DAILYLOGINFREETOKEN'.strtoupper($crcPrefix), json_encode($values));

        $values = [
            'f' => $request->input('free_token_validity_days', 7),
            'n' => $request->input('normal_token_validity_days', 365),
        ];

        $tempValues = json_encode($values);
        $oldValues = Configs::getParam('SYSTEM_LUCKYSPIN_TOKEN_VALIDITY_DAYS'.strtoupper($crcPrefix));
        Configs::updateParam('SYSTEM_LUCKYSPIN_TOKEN_VALIDITY_DAYS'.strtoupper($crcPrefix), $tempValues);

        if ($tempValues != $oldValues) {
            // Validity period has modified, now update all expired_at in fortune_wheel_ledger
            $categories = ['1' => $values['f'], '2' => $values['n']];

            foreach ($categories as $key => $val) {
                DB::table('fortune_wheel_ledger'.$crcPrefix)
                    ->where('category', '=', $key)
                    ->update([
                        'expired_at' => DB::raw('DATE_ADD(created_at, INTERVAL ' . $val . ' DAY)'),
                    ]);
            }
        }

        return response(json_encode($msg));
    }

    public function doEditWinnerList(Request $request)
    {
        $crcPrefix = '';

        if ($request->input('currency') == 'IDR') {
            $crcPrefix = '_idr';
        }

        $msg = '';
        $winnerList = $request->input('winner_list');

        try {
            $winnerList = json_decode($winnerList);

            Configs::updateParam('SYSTEM_LUCKYSPIN_WINNER_LIST'.strtoupper($crcPrefix), json_encode($winnerList));

            FortuneWheelController::clearCache();

            $msg = 'Winner list updated.';
        } catch (\Exception $e) {
            $msg = 'Unable to save winner list, please check your inputs and try again.';
        }

        return response(json_encode($msg));
    }
}
