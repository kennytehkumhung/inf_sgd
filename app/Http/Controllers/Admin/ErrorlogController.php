<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller\Admin;
use App\libraries\grid\PanelGrid;
use App\libraries\App;
use Illuminate\Http\Request;
use Auth;
use Validator;
use Session;
use Lang;
use App\Models\Onlinetrackerlog;
use App\Models\Currency;
use App\Models\Fundingmethod;
use App\Models\Cashledger;
use App\Models\Bank;
use App\Models\Accounttag;
use App\Models\Tag;
use App\Models\Account;
use App\Models\Config;
use App\Models\Apilog;
use App\Models\Errorlog;
use App\Models\Cashledgerlog;
use App\Models\Bankaccount;
use App\Models\Promocash;
use App\Models\Rejectreason;
use App\Models\Banktransfer;
use App\Models\Promocampaign;

class ErrorlogController extends Controller{

	protected $moduleName 	= 'errorlog';
	protected $limit 		= 20;
	protected $start 		= 0;
	
	public function __construct()
	{
					
	}
	
	public function index(Request $request){

			$grid = new PanelGrid;
			$grid->setupGrid($this->moduleName, 'errorlog-data', $this->limit);
			$grid->setTitle(Lang::get('COMMON.ERRORLOG'));
									
			
			$grid->addColumn('error',                           Lang::get('COMMON.ERROR'), 					700,		array('align' => 'left'));
			$grid->addColumn('updated_at',                      Lang::get('COMMON.LASTUPDATED'),                            150,            array('align' => 'left'));
			$grid->addColumn('created_at',                      Lang::get('COMMON.CREATEDON'), 				150,            array('align' => 'left'));
			

			$data['grid'] = $grid->getTemplateVars();

			return view('admin.grid2',$data);
    }
	
	protected function doGetData(Request $request){

		$rows = array();
		
		$condition = '1';
		
		if( isset($request->aflt['error']) && $request->aflt['error'] != '' )
		{
			$condition .= ' AND error = "'.$request->aflt['error'].'" ';
		}

		if( $request->has('createdfrom') ){
			$condition .= ' AND created_at >= "'.$request->get('createdfrom').' 00:00:00" AND created_at <= "'.$request->get('createdto').' 23:59:59" ';
		}
		
                if( $request->has('modifiedfrom') ){
			$condition .= ' AND updated_at >= "'.$request->get('modifiedfrom').' 00:00:00" AND updated_at <= "'.$request->get('modifiedto').' 23:59:59" ';
		}
		
		$total = Errorlog::whereRaw($condition)->count();
		
		

		//new structure
		if($errorlogs = Errorlog::whereRaw($condition)->skip($request->recordstartindex)->take( $request->pagesize )->orderBy('id','desc')->get()){
			foreach( $errorlogs as $errorlog ){
		
				$rows[] = array(

					'id'  				=> $errorlog->id, 
					'error'  			=> $errorlog->error, 
					'created_at'  			=> $errorlog->created_at->toDateTimeString(), 
                                        'updated_at'  			=> $errorlog->updated_at->toDateTimeString(),
				);	
				
			}
				
		}

				
		echo json_encode(array('total' => $total , 'rows' => $rows));
		exit;
	
	}


}

?>
