<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller\Admin;
use App\libraries\grid\PanelGrid;
use App\libraries\App;
use App\Models\Currency;
use App\Models\Product;
use App\Models\Announcement;
use App\Models\Language;
use Illuminate\Http\Request;
use Auth;
use Validator;
use Session;
use Lang;
use Cache;


class AnnouncementController extends Controller{

	
	protected $moduleName 	= 'announcement';
	protected $limit 		= 20;
	protected $start 		= 0;
	
	public function __construct()
	{
		
	}

	public function index(Request $request)
	{
		
		$grid = new PanelGrid;
		$grid->setupGrid($this->moduleName, 'announcement-data', $this->limit);
		

		$grid->setTitle(Lang::get('COMMON.ANNOUNCEMENT'));
		$grid->addColumn('title', 			Lang::get('COMMON.CONTENT'),			168,	array());
		$grid->addColumn('product', 		Lang::get('COMMON.PRODUCT'),			168,	array());
		$grid->addColumn('company', 		Lang::get('COMMON.COMPANY'),			88,		array());
		$grid->addColumn('affiliate', 		Lang::get('COMMON.AFFILIATE'),			88,		array());
		$grid->addColumn('member', 			Lang::get('COMMON.MEMBER'),				88,		array());
		$grid->addColumn('status', 			Lang::get('COMMON.STATUS'), 			100,	array());
		

		$grid->addFilter('status', App::getStatusOptions(), array('display' => Lang::get('COMMON.STATUS')));
		$grid->addButton('1', 'New', Lang::get('COMMON.ADDNEW').' '.Lang::get('COMMON.ANNOUNCEMENT'), 'button', array('icon' => 'add', 'url' => action('Admin\AnnouncementController@drawAddEdit')));

		$data['grid'] = $grid->getTemplateVars();

		return view('admin.grid2',$data);
	
	}
	
	protected function doGetData(Request $request) {
                $condition = '1';
		$aflt  = $request->input('aflt');
		$aqfld = $request->input('aqfld');
		
		if( isset($aqfld['status'])){
			$condition .= ' AND status'.'='.$aqfld['status'] ;
		}
                
                if($aqfld['status'] == ''){
                        $condition = '1';
                }
                
		$total  = Announcement::whereRaw($condition)->count();

		$rows = array();
		if($annoucements = Announcement::whereRaw($condition)->skip($request->recordstartindex)->take( $request->pagesize )->orderBy('status','desc')->get()){
		 	$name = 'bodyen';
			$products = Product::getAllAsOptions();
			foreach($annoucements as $annoucement){
	
				$body = App::formatAddTabUrl( Lang::get('ANNOUNCEMENT').': '.$annoucement->id, urlencode($annoucement->$name), action('Admin\AnnouncementController@drawAddEdit',  array('sid' => $annoucement->id)));
				$company   = ($annoucement->isCompanyType())?'Y':'N';
				$affiliate = ($annoucement->isAffiliateType())?'Y':'N';
				$member    = ($annoucement->isMemberType())?'Y':'N';
				if($annoucement->product == 0) $product = Lang::get('ALL');
				else $product = $products[$annoucement->product];
				
				$rows[] = array(
					'title'   	=> urldecode($body),
					'product' 	=> $product,
					'company' 	=> $company,
					'affiliate' => $affiliate,
					'member' 	=> $member, 
					'status' 	=> $annoucement->getStatusText($this->moduleName)
				);
			} 
		}
		
		echo json_encode(array('total' => $total, 'rows' => $rows));
		exit;
	}
	
	protected function drawAddEdit(Request $request) {
	
		$toAdd 	  = true;
		$object   = null;
		$readOnly = false;
		$values   = array();

		$title = Lang::get('COMMON.NEW').' '.Lang::get('COMMON.ANNOUNCEMENT');
		if($request->has('sid')) {
	
				if ($object = Announcement::find($request->input('sid'))) {
					$title = Lang::get('COMMON.EDIT').' '.Lang::get('COMMON.ANNOUNCEMENT');
					$toAdd = false;
					$readOnly = true;
				} else
				$object = null;
			
		}
		
		
		if($object) {
			if($object->isCompanyType()) 	$values[] = Announcement::CBO_TYPE_COMPANY;
			if($object->isAffiliateType())  $values[] = Announcement::CBO_TYPE_AFFILIATE;
			if($object->isMemberType()) 	$values[] = Announcement::CBO_TYPE_MEMBER;
		}
		$languages = Language::getLanguageAsOptions();
		$products  = Product::getAllAsOptions();
		App::arrayUnshift($products, array(Lang::get('COMMON.ALL')));
		
	
		$form = App::setupPanelForm('doadd', $request->all(), array(), $toAdd);
		$form->setFormTitle($title);
                foreach($languages as $key => $language){
                    $lang = 'body'.$key;
                    $form->addInput('text', 	Lang::get('COMMON.CONTENT').' ('.$language.')', $lang, (($object)? $object->$lang:''),  array(), 						 true);
                }
		$form->addInput('select', 	Lang::get('COMMON.PRODUCT'), 						   'product',(($object)? $object->product:''), array('options' => $products),	 true);
		//$form->addInput('datetime', 	Lang::get('COMMON.DATE'), 						   'date',(($object)? $object->product:'') , '',	 true);
		
		$form->addInput('checkbox', Lang::get('COMMON.TYPE'), 'types', '', array('options' => Announcement::getTypeOptions(), 'values' => $values), true);
		$form->addInput('checkbox_single', Lang::get('COMMON.DISPLAY'), 'display', (($object)? $object->display:''), array('options' => Announcement::getTypeOptions(), 'values' => $values), true);
		$form->addInput('radio',    Lang::get('COMMON.STATUS'), 'status', (($object)? $object->status:''), array('options' => App::getStatusOptions()), true);

		$data['form'] = $form->getTemplateVars();
		$data['module'] = 'announcement';

		return view('admin.form2',$data);
	}
	
	protected function doAddEdit(Request $request) {
		
		$validator = Validator::make(
			[
				'product'      => $request->input('product'),
				'types'  	   => $request->input('types'),
				'display'      => $request->input('display'),
				'status' 	   => $request->input('status'),
			],
			[
			   'product'       => 'required',
			   'types'  	   => 'required',
			   'display'       => 'required',
			   'status' 	   => 'required',	
			]
		);
		
		
		if ($validator->fails())
		{
			echo json_encode($validator->errors());
			exit;
		}
		
		Cache::forget('Announcement_en');
		Cache::forget('Announcement_cn');
		Cache::forget('Announcement_id');
		Cache::forget('Announcement_vn');
                Cache::forget('Announcement_th');
		
		$display = 0;
		if($request->has('display') && $request->input('display') == 1) {
			$display = 1;
		}
		
		$types = 0;
		foreach ($request->input('types') as $value) {
		    $types = $types | $value;
		}
		
		$object  = new Announcement;
		
		if($request->has('sid')) 
		{
			$object = Announcement::find($request->input('sid'));
		}
		

		$object->bodyen	 	= (!empty($request->input('bodyen')))?$request->input('bodyen'):'';
		$object->bodycn	 	= (!empty($request->input('bodycn')))?$request->input('bodycn'):'';
		$object->bodyid	 	= (!empty($request->input('bodyid')))?$request->input('bodyid'):'';
		$object->bodyvi	 	= (!empty($request->input('bodyvi')))?$request->input('bodyvi'):'';
                $object->bodyth	 	= (!empty($request->input('bodyth')))?$request->input('bodyth'):'';
		$object->type 		= $types;
		$object->product 	= $request->input('product');
		$object->display 	= $display;
		$object->status 	= $request->input('status');
		
		if( $object->save() )
		{
			echo json_encode( array( 'code' => Lang::get('COMMON.SUCESSFUL') ) );
			exit;
		}


		echo json_encode( array( 'code' => Lang::get('COMMON.FAILED') ) );
		
	}
	
	protected function doActivateSuspend(Request $request) {

			

		if($request->has('id')) {
		
			if($object = Announcement::find($request->id)) {
			
				$currentStatus = $object->status;
				if($request->action == 'activate') {
					$object->status 	= CBO_ACCOUNTSTATUS_ACTIVE;
				}
				if($request->action == 'suspend') {
					$object->status 	= CBO_ACCOUNTSTATUS_SUSPENDED;
				}

				if($currentStatus <> $object->status){
					if($object->save()) {
						Cache::forget('Announcement_en');
						Cache::forget('Announcement_cn');
						Cache::forget('Announcement_id');
						Cache::forget('Announcement_vn');
                                                Cache::forget('Announcement_th');
					
						echo json_encode( array( 'code' => Lang::get('COMMON.SUCESSFUL') ) );
						exit;
					}
				}
			}
		}
		
		
		echo json_encode( array( 'code' => Lang::get('COMMON.FAILED') ) );

	}
	
}
