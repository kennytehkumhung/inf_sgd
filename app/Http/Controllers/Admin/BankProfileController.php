<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller\Admin;
use App\libraries\grid\PanelGrid;
use App\libraries\App;
use Illuminate\Http\Request;
use Auth;
use Validator;
use Session;
use Lang;
use App\Models\Currency;
use App\Models\Bank;
use App\Models\Accounttag;
use App\Models\Tag;
use App\Models\Account;
use App\Models\Config;
use App\Models\Bankaccount;
use App\Models\Bankholder;
use App\Models\Agent;



class BankProfileController extends Controller{

	protected $moduleName 	= 'bankprofile';
	protected $limit 		= 20;
	protected $start 		= 0;
	
	public function __construct()
	{
					
	}
	
	public function index(Request $request){

			$grid = new PanelGrid;
			$grid->setupGrid($this->moduleName, 'bankprofile-data', $this->limit);
			$grid->setTitle(Lang::get('COMMON.MANAGEBANKACCOUNT'));
									
			$grid->addColumn('name', 		Lang::get('COMMON.NAME'), 			120,	array());
			$grid->addColumn('currency', 	Lang::get('COMMON.CURRENCY'), 		65,		array('align' => 'center'));
			$grid->addColumn('account', 	Lang::get('COMMON.ACCOUNT'), 		500,	array());
			$grid->addColumn('status', 		Lang::get('COMMON.STATUS'), 		70,		array('align' => 'center'));
			
			$grid->addFilter('status',  Bankholder::getStatusOptions(), array('display' => Lang::get('COMMON.TYPE')));
			$grid->addButton('1', 'New', Lang::get('COMMON.ADDNEW').' '.Lang::get('COMMON.MANAGEBANKACCOUNT'), 'button', array('icon' => 'add', 'url' => action('Admin\BankProfileController@drawAddEdit')));

			
			$data['grid'] = $grid->getTemplateVars();

			return view('admin.grid2',$data);
    }
	
	protected function doGetData(Request $request){
	
		
		$aflt  = $request->input('aflt');
		$aqfld = $request->input('aqfld');
		
		$defaultCondition 	= '1';
		$condition = '1';
		$condition .= Session::get('admin_crccode') == 'MYR' ? '' : ' AND crccode = "'.Session::get('admin_crccode').'"';
		
		if( isset($aqfld['status']) && $aqfld['status'] != '' ){
			$condition .= ' AND status = '.$aqfld['status'];
		}
		
		
		$total = Bankholder::whereRaw($condition)->count();

		$rows = array();

		//new structure
		if($holders = Bankholder::whereRaw($condition)->skip($request->recordstartindex)->take( $request->pagesize )->orderBy('id','desc')->get()){
			
			$bankOptions = Bank::getAllAsOptions(true);
			
			foreach( $holders as $holder ){

				$name = App::formatAddTabUrl( Lang::get('PROFILE').': '.$holder->name, urlencode($holder->name), action('Admin\BankProfileController@drawAddEdit',  array('sid' => $holder->id)));
				
				$bank = '';
				if($accounts = Bankaccount::whereRaw('bhdid='.$holder->id)->get()) {
					foreach($accounts as $account) {
						
						
						$bank .= $account->bankaccname.' ['.$account->bankaccno.'] | ';
					}
				}
				
				$rows[] = array(
					'name' 		 => urldecode($name),
					'account' 	 => $bank,
					'currency' 	 => $holder->crccode,
					'status' 	 => $holder->getStatusText()
				);	
				
			}
		
		}

				
		echo json_encode(array('total' => $total, 'rows' => $rows));
		exit;
	
	}
	
	protected function drawAddEdit(Request $request) { 

	    $toAdd 	  = true;
		$object   = null;
		$readOnly = false;
		$values   = array();

		$title = Lang::get('NEW').' '.Lang::get('MANAGEBANKACCOUNT');

		if($request->has('sid')) 
		{
	
			if ($object = Bankholder::find($request->input('sid'))) 
				{
					$title    = Lang::get('COMMON.EDIT').' '.Lang::get('COMMON.MANAGEBANKACCOUNT');
					$toAdd    = false;
					$readOnly = true;

				} 
			else
				{
					$object = null;
				}
			
		}
		
		$bankOptions = Bankaccount::getAllAsOptions();
		$values = array();
		if($object) {
			$bankOptions = Bankaccount::getAllAsOptions(false, false, false, false, $object->crccode);
			$values      = Bankaccount::getAllAsOptions(false, false, false, $object->id, $object->crccode);
		}
	
		$currencies = Currency::getAllCurrencyAsOptions();
		//App::arrayUnshift($currencies, Lang::get('COMMON.SELECT'));
		
		$banklist  = Bank::getAllAsOptions();
		$form = App::setupPanelForm('doadd', $request->all(), array(), $toAdd);
		$form->setFormTitle($title);
		
		$form->addInput('text', Lang::get('COMMON.NAME'), 'name', (($object)? $object->name:''), array(), true);
		$form->addInput('select', Lang::get('COMMON.CURRENCY'), 'crccode', (($object)? $object->crccode:''), array('options' => $currencies, 'disabled' => ($object)?true:false), true);
		$form->addInput('swapbox', Lang::get('COMMON.BANK'), 'bank', '', array('options' => $bankOptions, 'values' => $values), true);
		$form->addInput('radio', Lang::get('COMMON.STATUS'), 'status', (($object)? $object->status:0), array('options' => Bank::getStatusOptions()), true);
		
		$data['form'] = $form->getTemplateVars();
		$data['module'] = 'bankprofile';

		return view('admin.form2',$data);
	
	}
	
	protected function doAddEdit(Request $request) {
		
		$validator = Validator::make(
			[
	
				'name'	  	  => $request->input('name'),
				'crccode'	  => $request->input('crccode'),
				'bank'	  	  => $request->input('bank'),
				'status'	  => $request->input('status'),

			],
			[

			   'name'	   	   => 'required',
			   'crccode'	   => 'required',
			   'bank'	  	   => 'required',
			   'status'	  	   => 'required',

			]
		);
		
		
		if ($validator->fails())
		{
			echo json_encode($validator->errors());
			exit;
		}
		
		$accounts = array();
		$banks    = explode(',', $request->input('bank'));
		if(is_array($banks)) 
		{
			foreach($banks as $bank) 
			{
				if( $bacObj = Bankaccount::find($bank) )
				{
					if( $bacObj->crccode == $request->input('crccode') )
					{
						$accounts[] = $bank;
					}
				}
			}
		}
		
		
		
		$object  = new Bankholder;
		
		if($request->has('sid')) 
		{
			$object = Bankholder::find($request->input('sid'));
		}
		
		$object->name    = $request->input('name');
		$object->status  = $request->input('status');
		$object->crccode = $request->input('crccode');

		
		if( $object->save() )
		{
			if($bacAccounts  = Bankaccount::whereRaw('bhdid='.$object->id)->get()) {
					foreach($bacAccounts as $account) {
			
						if(!in_array($account->id, $accounts)) {
					
							$account->bhdid = 0;
							$account->save();
						}
					}
			}
			
		 	// add/update bank account
			foreach($accounts as $account) 
			{
				if($bacObj = Bankaccount::find($account)) 
				{
					if($bacObj->bhdid != $object->id) 
					{
						$bacObj->bhdid = $object->id;
						$bacObj->save();
					}
				}
			}
			
			//update agent account
			if($object->status == CBO_STANDARDSTATUS_ACTIVE) 
			{
				if($agents = Agent::whereRaw('crccode = "'.$object->crccode.'"')->get()) 
				{
					foreach($agents as $agent) 
					{
						$agent->bhdid = $object->id;
						$agent->save();
					}
				}
			} 

			
			echo json_encode( array( 'code' => Lang::get('COMMON.SUCESSFUL') ) );
			exit;
		}


		echo json_encode( array( 'code' => Lang::get('COMMON.FAILED') ) );
		
	}
	



}

?>
