<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller\Admin;
use App\Http\Controllers\User\FortuneWheelController;
use App\libraries\App;
use App\libraries\grid\PanelGrid;
use App\Models\Account;
use App\Models\Configs;
use App\Models\FortuneWheelAcc;
use App\Models\FortuneWheelLedger;
use App\Models\FortuneWheelPrize;
use App\Models\FortuneWheelTrans;
use App\Models\Media;
use App\Models\PremierLeagueBet;
use App\Models\PremierLeagueMatch;
use App\Models\PremierLeagueToken;
use Cache;
use Carbon\Carbon;
use DB;
use File;
use Illuminate\Http\Request;
use Lang;
use Session;
use Storage;
use Validator;

class PremierLeagueController extends Controller {

    protected $moduleName = 'premierleague';
    protected $limit = 20;
    protected $start = 0;

    public function index(Request $request)
    {
        $grid = new PanelGrid;
        $grid->setupGrid($this->moduleName, 'premierLeagueMatchReport-data', $this->limit, true, array('footer' => true));
        $grid->setTitle(Lang::get('COMMON.PREMIERLEAGUEREPORT'));

        // TODO translate
        $grid->addColumn('match_id', 					'Match ID',					60,	array('align' => 'center'));
        $grid->addColumn('bet_opened_at', 			'Bet Start Date',				150,	array('align' => 'center'));
        $grid->addColumn('bet_closed_at', 			'Bet Closing Date',			150,	array('align' => 'center'));
        $grid->addColumn('week_no', 					'Week No.', 					60,	array('align' => 'right'));
        $grid->addColumn('team_a_name', 				'Team A Name', 				150,	array('align' => 'left'));
        $grid->addColumn('team_a_score', 				'Team A Score', 				80,	array('align' => 'right'));
        $grid->addColumn('team_b_name',				'Team B Name', 				150,	array('align' => 'left'));
        $grid->addColumn('team_b_score', 				'Team B Score', 				80,	array('align' => 'right'));
        $grid->addColumn('total_token_given', 		'Total Token Given', 			110,	array('align' => 'right'));
        $grid->addColumn('total_bet', 				'Total Bet', 					80,	array('align' => 'right'));
        $grid->addColumn('total_won', 				'Total Won', 					80,	array('align' => 'right'));
        $grid->addColumn('total_selected_winner', 	'Total Selected Winners', 	130,	array('align' => 'right'));
        $grid->addColumn('created_at', 				Lang::get('COMMON.CREATEDON'), 	150,	array('align' => 'center'));
        $grid->addColumn('status', 					'Match Status', 				120,	array('align' => 'center'));

        $grid->addButton('1', 'NewMatch', Lang::get('COMMON.ADDNEW').' '.Lang::get('COMMON.MATCH'), 'button', array('icon' => 'add', 'url' => action('Admin\PremierLeagueController@drawAddEditMatch')));
        $grid->addButton('1', 'WinnerListConfig', 'Winner List Config', 'button', array('url' => action('Admin\PremierLeagueController@drawWinnerListConfig')));
        $grid->addButton('1', 'GiveToken', 'Give Token', 'button', array('url' => action('Admin\PremierLeagueController@drawGiveToken')));
        $grid->addButton('1', 'UpComingMatch', 'UpComing Match', 'button', array('url' => action('Admin\PremierLeagueController@drawUpComingMatch')));

        $grid->addFilter('status', PremierLeagueMatch::getStatusOptions(true), array('display' => Lang::get('COMMON.STATUS')));

        $data['grid'] = $grid->getTemplateVars();

        if ($request->has('createdfrom')) {
            $data['grid']['options']['params']['createdfrom'] = '2014-01-01';
        }

        if ($request->has('createdto')) {
            $data['grid']['options']['params']['createdto'] = Carbon::now()->addMonths(1)->addDays(-1)->format('Y-m-d');
        }

        return view('admin.grid2',$data);
    }

    protected function doGetData(Request $request){

        $footer = array();
        $rows = array();
        $builder = DB::table('premier_league_match as a')
            ->orderBy('a.bet_opened_at', 'desc')
            ->orderBy('a.team_a_name', 'asc')
            ->select(array('a.id', 'a.team_a_name', 'a.team_b_name', 'a.team_a_score', 'a.team_b_score',
                'a.week_no', 'a.status', 'a.bet_opened_at', 'a.bet_closed_at', 'a.created_at'));

        $today = Carbon::now()->toDateString();
        $createdFrom = $request->input('createdfrom', $today);
        $createdTo = $request->input('createdto', $today);

        $createdFrom .= ' 00:00:00';
        $createdTo .= ' 23:59:59';

        if ( isset($request->aqfld['status']) && $request->aqfld['status'] != '' ) {
            $builder->where('a.status', '=', $request->aqfld['status']);
        }

        $builder->whereBetween('a.bet_opened_at', array($createdFrom, $createdTo));

        $total = count($builder->get());
        $records = $builder->take( $request->pagesize )
            ->skip( $request->recordstartindex )
            ->get();

        $statusOptions = collect(PremierLeagueMatch::getStatusOptions());

        // Calculate total amount for footer.
        $footerTotalTokenGiven = 0;
        $footerTotalBet = 0;
        $footerTotalWon = 0;
        $footerTotalSelectedWinner = 0;

        foreach( $records as $r ) {
            $totalTokenGiven = DB::table('premier_league_token as a')->where('a.match_id', '=', $r->id)->sum('a.token_amount');
            $betCount = DB::table('premier_league_bet as a')->where('a.match_id', '=', $r->id)->count();
            $wonCount = DB::table('premier_league_bet as a')->where('a.match_id', '=', $r->id)->where('a.status', '=', PremierLeagueBet::STATUS_WON)->count();
            $selectedWonCount = DB::table('premier_league_bet as a')->where('a.match_id', '=', $r->id)->where('a.status', '=', PremierLeagueBet::STATUS_WON_SELECTED)->count();

            $footerTotalTokenGiven += $totalTokenGiven;
            $footerTotalBet += $betCount;
            $footerTotalWon += $wonCount;
            $footerTotalSelectedWinner += $selectedWonCount;

            $rows[] = array(
                'match_id'				=> '<a href="javascript:void(0);" onclick="parent.addTab(\'Premier League Match Details: #'.$r->id.'\', \'premierLeagueMatchReport-add-edit?sid='.$r->id.'\');" style="cursor:pointer">'.$r->id.'</a>',
                'bet_opened_at'			=> $r->bet_opened_at,
                'bet_closed_at'			=> $r->bet_closed_at,
                'week_no'				=> $r->week_no,
                'team_a_name'			=> $r->team_a_name,
                'team_a_score'			=> $r->team_a_score,
                'team_b_name'			=> $r->team_b_name,
                'team_b_score'			=> $r->team_b_score,
                'total_token_given'		=> '<a href="javascript:void(0);" onclick="parent.addTab(\'Premier League Bet Details\', \'premierLeagueTokenReport?sid='.$r->id.'&createdfrom='.$createdFrom.'&createdto='.$createdTo.'\');" style="cursor:pointer">'.$totalTokenGiven.'</a>',
                'total_bet'				=> '<a href="javascript:void(0);" onclick="parent.addTab(\'Premier League Bet Details\', \'premierLeagueBetReport?sid='.$r->id.'&createdfrom='.$createdFrom.'&createdto='.$createdTo.'\');" style="cursor:pointer">'.$betCount.'</a>',
                'total_won'				=> $wonCount + $selectedWonCount,
                'total_selected_winner'	=> $selectedWonCount,
                'created_at'			=> $r->created_at,
                'status'				=> $statusOptions->get($r->status),
//                'status'				=> '<select onchange="autoloadBinding(this);" data-href="'.action('Admin\PremierLeagueController@doEditData').'?sid='.$r->id.'&act=match" data-hasvalue="y" data-loadingimg="a" data-outval="n">'.
//                    '<option value="'.PremierLeagueMatch::STATUS_OPENING.'" '.($r->status == PremierLeagueMatch::STATUS_OPENING ? 'selected' : '').'>'.$statusOptions[PremierLeagueMatch::STATUS_OPENING].'</option>'.
//                    '<option value="'.PremierLeagueMatch::STATUS_CLOSED.'" '.($r->status == PremierLeagueMatch::STATUS_CLOSED ? 'selected' : '').'>'.$statusOptions[PremierLeagueMatch::STATUS_CLOSED].'</option>'.
//                    '<option value="'.PremierLeagueMatch::STATUS_VOID.'" '.($r->status == PremierLeagueMatch::STATUS_VOID ? 'selected' : '').'>'.$statusOptions[PremierLeagueMatch::STATUS_VOID].'</option>'.
//                    '<option value="'.PremierLeagueMatch::STATUS_REOPENED.'" '.($r->status == PremierLeagueMatch::STATUS_REOPENED ? 'selected' : '').'>'.$statusOptions[PremierLeagueMatch::STATUS_REOPENED].'</option>'.
//                    '</select>',
            );
        }

        $footer[] = array(
            'match_id'				=> '',
            'bet_opened_at'			=> '',
            'bet_closed_at'			=> '',
            'week_no'				=> '',
            'team_a_name'			=> '',
            'team_a_score'			=> '',
            'team_b_name'			=> '',
            'team_b_score'			=> '',
            'total_token_given'		=> $footerTotalTokenGiven,
            'total_bet'				=> $footerTotalBet,
            'total_won'				=> $footerTotalWon,
            'total_selected_winner'	=> $footerTotalSelectedWinner,
            'created_at'			=> '',
            'status'				=> '',
        );

        return response(json_encode(array('total' => $total , 'rows' => $rows , 'footer' => $footer)));
    }

    public function doGetBetReport(Request $request)
    {
        $grid = new PanelGrid;
        $grid->setupGrid($this->moduleName, 'premierLeagueBetReport-data', $this->limit, true);
        $grid->setTitle(Lang::get('COMMON.PREMIERLEAGUEREPORT'));

        // TODO translate
        $grid->addColumn('match_id', 					'Match ID',					60,	array('align' => 'right'));
        $grid->addColumn('week_no', 					'Week No.', 					60,	array('align' => 'right'));
        $grid->addColumn('team_a_name', 				'Team A Name', 				150,	array('align' => 'left'));
        $grid->addColumn('team_a_score', 				'Team A Score', 				80,	array('align' => 'right'));
        $grid->addColumn('team_b_name',				'Team B Name', 				150,	array('align' => 'left'));
        $grid->addColumn('team_b_score', 				'Team B Score', 				80,	array('align' => 'right'));
        $grid->addColumn('crccode', 					'Currency', 					60,	array('align' => 'center'));
        $grid->addColumn('nickname', 					'Username', 					150,	array('align' => 'right'));
        $grid->addColumn('user_team_a_bet', 			'Placed Bet A', 				80,	array('align' => 'right'));
        $grid->addColumn('user_team_b_bet', 			'Placed Bet B', 				80,	array('align' => 'right'));
        $grid->addColumn('created_at', 				'Bet Placed On', 				150,	array('align' => 'center'));
        $grid->addColumn('status', 					'Bet Status', 					150,	array('align' => 'center'));

        $grid->addSearchField('match_id', array('match_id' => 'Match ID'), array('value' => $request->input('sid')));
        $grid->addSearchField('user_team_a_bet', array('user_team_a_bet' => 'Placed Bet A'));
        $grid->addSearchField('user_team_b_bet', array('user_team_b_bet' => 'Placed Bet B'));
        $grid->addFilter('status', PremierLeagueBet::getStatusOptions(true), array('display' => Lang::get('COMMON.STATUS')));

        $data['grid'] = $grid->getTemplateVars();

        if ($request->has('createdfrom')) {
            $data['grid']['options']['params']['createdfrom'] = $request->input('createdfrom');
        }

        if ($request->has('createdto')) {
            $data['grid']['options']['params']['createdto'] = $request->input('createdto');
        }

        return view('admin.grid2',$data);
    }

    protected function doGetBetReportData(Request $request){

        $footer = array();
        $rows = array();
        $builder = DB::table('premier_league_bet as a')
            ->join('premier_league_match as b', 'b.id', '=', 'a.match_id')
            ->orderBy('a.status', 'desc')
            ->orderBy('a.crccode', 'asc')
            ->orderBy('a.nickname', 'asc')
            ->select(array('a.id', 'a.match_id', 'b.team_a_name', 'b.team_b_name', 'b.team_a_score', 'b.team_b_score',
                'b.week_no', 'a.status', 'a.crccode', 'a.nickname', 'a.bet_a', 'a.bet_b', 'a.created_at'));

        $today = Carbon::now()->toDateString();
        $createdFrom = $request->input('createdfrom', $today);
        $createdTo = $request->input('createdto', $today);

        $createdFrom .= ' 00:00:00';
        $createdTo .= ' 23:59:59';

        if( isset($request->aflt['match_id']) && $request->aflt['match_id'] != '' ){
            $builder->where('a.match_id', '=', $request->aflt['match_id']);
        }

        if( isset($request->aflt['user_team_a_bet']) && $request->aflt['user_team_a_bet'] != '' ){
            $builder->where('a.bet_a', '=', $request->aflt['user_team_a_bet']);
        }

        if( isset($request->aflt['user_team_b_bet']) && $request->aflt['user_team_b_bet'] != '' ){
            $builder->where('a.bet_b', '=', $request->aflt['user_team_b_bet']);
        }

        if ( isset($request->aqfld['status']) && $request->aqfld['status'] != '' ) {
            $builder->where('a.status', '=', $request->aqfld['status']);
        }

        $builder->whereBetween('a.created_at', array($createdFrom, $createdTo));

        $total = count($builder->get());
        $records = $builder->take( $request->pagesize )
            ->skip( $request->recordstartindex )
            ->get();

        $statusOptions = PremierLeagueBet::getStatusOptions();

        foreach( $records as $r ) {
            $rows[] = array(
                'match_id'				=> $r->match_id,
                'week_no'				=> $r->week_no,
                'team_a_name'			=> $r->team_a_name,
                'team_a_score'			=> $r->team_a_score,
                'team_b_name'			=> $r->team_b_name,
                'team_b_score'			=> $r->team_b_score,
                'crccode'				=> $r->crccode,
                'nickname'				=> $r->nickname,
                'user_team_a_bet'		=> $r->bet_a,
                'user_team_b_bet'		=> $r->bet_b,
                'created_at'			=> $r->created_at,
                'status'				=> '<select onchange="autoloadBinding(this);" data-href="'.action('Admin\PremierLeagueController@doEditData').'?sid='.$r->id.'&act=bet" data-hasvalue="y" data-loadingimg="a" data-outval="n">'.
                    '<option value="'.PremierLeagueBet::STATUS_PENDING.'" '.($r->status == PremierLeagueBet::STATUS_PENDING ? 'selected' : '').'>'.$statusOptions[PremierLeagueBet::STATUS_PENDING].'</option>'.
                    '<option value="'.PremierLeagueBet::STATUS_VOID.'" '.($r->status == PremierLeagueBet::STATUS_VOID ? 'selected' : '').'>'.$statusOptions[PremierLeagueBet::STATUS_VOID].'</option>'.
                    '<option value="'.PremierLeagueBet::STATUS_LOSE.'" '.($r->status == PremierLeagueBet::STATUS_LOSE ? 'selected' : '').'>'.$statusOptions[PremierLeagueMatch::STATUS_VOID].'</option>'.
                    '<option value="'.PremierLeagueBet::STATUS_WON.'" '.($r->status == PremierLeagueBet::STATUS_WON ? 'selected' : '').'>'.$statusOptions[PremierLeagueBet::STATUS_WON].'</option>'.
                    '<option value="'.PremierLeagueBet::STATUS_WON_SELECTED.'" '.($r->status == PremierLeagueBet::STATUS_WON_SELECTED ? 'selected' : '').'>'.$statusOptions[PremierLeagueBet::STATUS_WON_SELECTED].'</option>'.
                    '</select>',
            );
        }

        return response(json_encode(array('total' => $total , 'rows' => $rows)));
    }

    public function doGetTokenReport(Request $request)
    {
        $grid = new PanelGrid;
        $grid->setupGrid($this->moduleName, 'premierLeagueTokenReport-data', $this->limit, true);
        $grid->setTitle(Lang::get('COMMON.PREMIERLEAGUEREPORT'));

        // TODO translate
        $grid->addColumn('match_id', 					'Match ID',					60,	array('align' => 'right'));
        $grid->addColumn('week_no', 					'Week No.', 					60,	array('align' => 'right'));
        $grid->addColumn('crccode', 					'Currency', 					60,	array('align' => 'center'));
        $grid->addColumn('nickname', 					'Username', 					150,	array('align' => 'right'));
        $grid->addColumn('token_amount', 				'Token Amount', 				150,	array('align' => 'right'));
        $grid->addColumn('remark', 					'Remark', 					300,	array('align' => 'left'));
        $grid->addColumn('created_at', 				'Given At', 					150,	array('align' => 'center'));

        $grid->addSearchField('match_id', array('match_id' => 'Match ID'), array('value' => $request->input('sid')));
        $grid->addSearchField('nickname', array('nickname' => 'Username'), array('value' => $request->input('nickname')));

        $data['grid'] = $grid->getTemplateVars();

        if ($request->has('createdfrom')) {
            $data['grid']['options']['params']['createdfrom'] = $request->input('createdfrom');
        }

        if ($request->has('createdto')) {
            $data['grid']['options']['params']['createdto'] = $request->input('createdto');
        }

        return view('admin.grid2',$data);
    }

    protected function doGetTokenReportData(Request $request){

        $footer = array();
        $rows = array();
        $builder = DB::table('premier_league_token as a')
            ->join('premier_league_match as b', 'b.id', '=', 'a.match_id')
            ->orderBy('a.crccode', 'asc')
            ->orderBy('a.nickname', 'asc')
            ->orderBy('a.created_at', 'asc')
            ->select(array('a.match_id', 'b.week_no', 'a.crccode', 'a.nickname', 'a.token_amount', 'a.remark', 'a.created_at'));

        $today = Carbon::now()->toDateString();
        $createdFrom = $request->input('createdfrom', $today);
        $createdTo = $request->input('createdto', $today);

        $createdFrom .= ' 00:00:00';
        $createdTo .= ' 23:59:59';

        if( isset($request->aflt['match_id']) && $request->aflt['match_id'] != '' ){
            $builder->where('a.match_id', '=', $request->aflt['match_id']);
        }

        if( isset($request->aflt['nickname']) && $request->aflt['nickname'] != '' ){
            $builder->where('a.nickname', 'LIKE', '%'.$request->aflt['nickname'].'%');
        }

        $builder->whereBetween('a.created_at', array($createdFrom, $createdTo));

        $total = count($builder->get());
        $records = $builder->take( $request->pagesize )
            ->skip( $request->recordstartindex )
            ->get();

        foreach( $records as $r ) {
            $rows[] = array(
                'match_id'				=> $r->match_id,
                'week_no'				=> $r->week_no,
                'crccode'				=> $r->crccode,
                'nickname'				=> $r->nickname,
                'token_amount'			=> $r->token_amount,
                'remark'				=> $r->remark,
                'created_at'			=> $r->created_at,
            );
        }

        return response(json_encode(array('total' => $total , 'rows' => $rows)));
    }

    public function doEditData(Request $request)
    {
        $act = $request->input('act');

        if ($act == 'match') {
            $matchObj = PremierLeagueMatch::where('id', '=', $request->input('sid'))->first();

            if ($matchObj) {
                $status = $request->input('val');
                $acceptedStatusOptions = PremierLeagueMatch::getStatusOptions();

                if (array_key_exists($status, $acceptedStatusOptions)) {
                    $matchObj->status = $status;
                    $matchObj->save();

                    $this->updateAllBetStatus($matchObj);

                    return response('Record saved.');
                }
            }
        } elseif ($act == 'bet') {
            $betObj = PremierLeagueBet::find($request->input('sid'));

            if ($betObj) {
                $status = $request->input('val');
                $acceptedStatusOptions = PremierLeagueBet::getStatusOptions();

                if (array_key_exists($status, $acceptedStatusOptions)) {
                    $betObj->status = $status;
                    $betObj->save();

                    return response('Record saved.');
                }
            }
        }

        return response('', 500);
    }

    private function updateAllBetStatus($matchObj)
    {
        $teamAScore = $matchObj->team_a_score;
        $teamBScore = $matchObj->team_b_score;

        if ($matchObj->status == PremierLeagueMatch::STATUS_VOID) {
            // All member bet set to VOID.
            PremierLeagueBet::where('match_id', '=', $matchObj->id)
                ->update(array(
                    'status' => PremierLeagueBet::STATUS_VOID,
                ));
        } elseif ($matchObj->status == PremierLeagueMatch::STATUS_REOPENED) {
            // All member bet set to PENDING.
            PremierLeagueBet::where('match_id', '=', $matchObj->id)
                ->update(array(
                    'status' => PremierLeagueBet::STATUS_PENDING,
                ));
        } elseif ($matchObj->status == PremierLeagueMatch::STATUS_CLOSED) {
            // Calculate member bet winlose.
            PremierLeagueBet::where('match_id', '=', $matchObj->id)
                ->where('bet_a', '=', $teamAScore)
                ->where('bet_b', '=', $teamBScore)
                ->whereIn('status', array(PremierLeagueBet::STATUS_PENDING, PremierLeagueBet::STATUS_LOSE))
                ->update(array(
                    'status' => PremierLeagueBet::STATUS_WON,
                ));

            // Set remaining bet to LOSE.
            PremierLeagueBet::where('match_id', '=', $matchObj->id)
                ->where(function ($query) use ($teamAScore, $teamBScore) {
                    $query->where('bet_a', '!=', $teamAScore)
                        ->orWhere('bet_b', '!=', $teamBScore);
                })
                ->whereNotIn('status', array(PremierLeagueBet::STATUS_VOID, PremierLeagueBet::STATUS_WON_SELECTED))
                ->update(array(
                    'status' => PremierLeagueBet::STATUS_LOSE,
                ));
        }
    }

    public function drawAddEditMatch(Request $request)
    {
        $toAdd 	  = true;
        $object   = null;
//        $readOnly = false;
        $values   = array();

        $title = Lang::get('NEW').' '.Lang::get('COMMON.MATCH');

        if($request->has('sid')) {
            if ($object = PremierLeagueMatch::find($request->input('sid'))) {
                $title    = Lang::get('COMMON.EDIT').' '.Lang::get('COMMON.MATCH');
                $toAdd    = false;
//                $readOnly = true;
            } else {
                $object = null;
            }
        }

        $teamOptions  = PremierLeagueMatch::getTeamListOptions();
        $statusOptions  = PremierLeagueMatch::getStatusOptions();

        $form = App::setupPanelForm('doadd', $request->all(), array(), $toAdd);
        $form->setFormTitle($title);

        $form->addInput('hidden', 	'',								'id',					(($object)? $object->id:''),						array(),							false);
        $form->addInput('select', 	'Team A',						'team_a_id',			(($object)? $object->team_a_id:''),					array('options' => $teamOptions),	true);
        $form->addInput('select', 	'Team B',						'team_b_id',			(($object)? $object->team_b_id:''),					array('options' => $teamOptions),	true);
        $form->addInput('text', 	'Team A Score',					'team_a_score',		(($object)? $object->team_a_score:''),				array(),							true);
        $form->addInput('text', 	'Team B Score',					'team_b_score',		(($object)? $object->team_b_score:''),				array(),							true);
        $form->addInput('text', 	'Week No.',						'week_no',				(($object)? $object->week_no:''),					array(),							true);
        $form->addInput('datetime', 'Bet Opened At',				'bet_opened_at',		(($object)? $object->bet_opened_at:''),				array(),							true);
        $form->addInput('datetime', 'Bet Closed At',				'bet_closed_at',		(($object)? $object->bet_closed_at:''),				array(),							true);
        $form->addInput('file', 	'Thumbnail Image',				'media_thumbnail',		(($object)? $object->getThumbnailImage():''),		array(),							false);
        $form->addInput('file', 	'Background Image',				'media_desktop',		(($object)? $object->getBackgroundImage():''),		array(),							false);
        $form->addInput('select', 	Lang::get('COMMON.STATUS'),		'status',				(($object)? $object->status:''),					array('options' => $statusOptions),	true);

        $data['form'] = $form->getTemplateVars();
        $data['module'] = 'premierLeagueMatchReport';

        return view('admin.form2',$data);
    }

    public function doAddEditMatch(Request $request)
    {
        $fileInputs = [
            'media_thumbnail' => Media::TYPE_THUMBNAIL,
            'media_desktop' => Media::TYPE_IMAGE,
        ];

        $rules = [
            'team_a_id' => 'required',
            'team_b_id' => 'required',
            'team_a_score' => 'required|integer|min:0|max:99',
            'team_b_score' => 'required|integer|min:0|max:99',
            'week_no' => 'required',
            'bet_opened_at' => 'required',
            'bet_closed_at' => 'required',
            'status' => 'required',
        ];

        foreach ($fileInputs as $imgKey => $imgType) {
            if ($request->hasFile($imgKey)) {
                $rules[$imgKey] = 'image';
            }
        }

        $validator = Validator::make($request->all(), $rules);

        $validator->setAttributeNames([
            'team_a_id' => 'Team A',
            'team_b_id' => 'Team B',
            'team_a_score' => 'Team A Score',
            'team_b_score' => 'Team B Score',
            'week_no' => 'Week No.',
            'bat_opened_at' => 'Bet Opened At',
            'bat_closed_at' => 'Bet Closed At',
            'media_thumbnail' => 'Thumbnail Image',
            'media_desktop' => 'Background Image',
            'status' => Lang::get('COMMON.STATUS'),
        ]);

        $teamListOptions = PremierLeagueMatch::getTeamListOptions();

        if (!array_key_exists($request->input('team_a_id'), $teamListOptions)) {
            $validator->errors()->add('team_a_id', 'Invalid team A.');
        }

        if (!array_key_exists($request->input('team_b_id'), $teamListOptions)) {
            $validator->errors()->add('team_b_id', 'Invalid team B.');
        }

        if ($validator->fails())
        {
            echo json_encode($validator->errors());
            exit;
        }

        $id = $request->input('id', 0);

        if ($id > 0) {
            $matchObj = PremierLeagueMatch::find($id);
        } else {
            $matchObj = new PremierLeagueMatch();
        }

        $matchObj->forceFill([
            'team_a_id' => $request->input('team_a_id'),
            'team_b_id' => $request->input('team_b_id'),
            'team_a_name' => $teamListOptions[$request->input('team_a_id')],
            'team_b_name' => $teamListOptions[$request->input('team_b_id')],
            'team_a_score' => $request->input('team_a_score'),
            'team_b_score' => $request->input('team_b_score'),
            'week_no' => $request->input('week_no'),
            'bet_opened_at' => $request->input('bet_opened_at'),
            'bet_closed_at' => $request->input('bet_closed_at'),
            'status' => $request->input('status'),
        ]);

        if ($matchObj->save()) {
            $this->updateAllBetStatus($matchObj);

            foreach ($fileInputs as $imgKey => $imgType) {
                if($request->hasFile($imgKey)) {
                    $file = $request->file($imgKey);

                    $filename = 'eplmatch/'.md5($file->getClientOriginalName().time()).'.'. $file->getClientOriginalExtension();
                    $result = Storage::disk('s3')->put($filename,  File::get($file), 'public');

                    if ($result) {
                        if (Media::where('refobj', '=', 'PremierLeagueMatch')->where('refid', '=', $matchObj->id)->where('type', '=', $imgType)->count() == 0) {
                            $medObj = new Media;
                        } else {
                            $medObj = Media::where('refobj', '=', 'PremierLeagueMatch')->where('refid', '=', $matchObj->id)->where('type', '=', $imgType)->first();
                        }

                        $medObj->refobj = 'PremierLeagueMatch';
                        $medObj->refid  = $matchObj->id;
                        $medObj->type   = $imgType;
                        $medObj->domain = 'https://'.Configs::getParam('SYSTEM_AWS_S3BUCKET');
                        $medObj->path   = $filename;
                        $medObj->save();
                    }
                }
            }

            Cache::forget('premier_league_thumbnail');

            echo json_encode(array('code' => Lang::get('COMMON.SUCESSFUL')));
            exit;
        }

        echo json_encode(array('code' => Lang::get('COMMON.FAILED')));
    }

    public function drawWinnerListConfig(Request $request)
    {
        $toAdd 	  = true;
        $object   = null;
//        $readOnly = false;
        $values   = array();

        $title = Lang::get('COMMON.CONFIG');
        $winnerObj = json_decode(Configs::getParam('SYSTEM_PREMIERLEAGUE_WINNER_LIST'));

        $form = App::setupPanelForm('doadd', $request->all(), array(), $toAdd);
        $form->setFormTitle($title);

        $form->addInput('text', 	'Winner 1',		'w1',		(($winnerObj)? $winnerObj->w1:''),		array(),	true);
        $form->addInput('text', 	'Winner 2',		'w2',		(($winnerObj)? $winnerObj->w2:''),		array(),	true);
        $form->addInput('text', 	'Winner 3',		'w3',		(($winnerObj)? $winnerObj->w3:''),		array(),	true);
        $form->addInput('text', 	'Winner 4',		'w4',		(($winnerObj)? $winnerObj->w4:''),		array(),	true);
        $form->addInput('text', 	'Winner 5',		'w5',		(($winnerObj)? $winnerObj->w5:''),		array(),	true);
        $form->addInput('text', 	'Winner 6',		'w6',		(($winnerObj)? $winnerObj->w6:''),		array(),	true);
        $form->addInput('text', 	'Winner 7',		'w7',		(($winnerObj)? $winnerObj->w7:''),		array(),	true);
        $form->addInput('text', 	'Winner 8',		'w8',		(($winnerObj)? $winnerObj->w8:''),		array(),	true);
        $form->addInput('text', 	'Winner 9',		'w9',		(($winnerObj)? $winnerObj->w9:''),		array(),	true);
        $form->addInput('text', 	'Winner 10',            'w10',          (($winnerObj)? $winnerObj->w10:''),		array(),	true);

        $data['form'] = $form->getTemplateVars();
        $data['module'] = 'premierLeagueWinnerListConfig';

        return view('admin.form2',$data);
    }

    public function doAddEditWinnerListConfig(Request $request)
    {
        $inputs = array(
            'w1' => $request->input('w1'),
            'w2' => $request->input('w2'),
            'w3' => $request->input('w3'),
            'w4' => $request->input('w4'),
            'w5' => $request->input('w5'),
            'w6' => $request->input('w6'),
            'w7' => $request->input('w7'),
            'w8' => $request->input('w8'),
            'w9' => $request->input('w9'),
            'w10' => $request->input('w10'),
        );

        Configs::updateParam('SYSTEM_PREMIERLEAGUE_WINNER_LIST', json_encode($inputs));

        return response()->json(array('code' => Lang::get('COMMON.SUCESSFUL')));
    }
    
    public function drawUpComingMatch(Request $request)
    {
        $toAdd 	  = true;
        $object   = null;
        $values   = array();

        $title = Lang::get('COMMON.CONFIG');
        $matchObj = '';
        $matchObj = json_decode(Configs::getParam('SYSTEM_PREMIERLEAGUE_UPCOMING_MATCH'));

        $form = App::setupPanelForm('doadd', $request->all(), array(), $toAdd);
        $form->setFormTitle($title);

        $form->addInput('text', 	'Match 1',		'm1',		(($matchObj)? $matchObj->m1:''),		array(),	true);
        $form->addInput('text', 	'Match 2',		'm2',		(($matchObj)? $matchObj->m2:''),		array(),	true);
        $form->addInput('text', 	'Match 3',		'm3',		(($matchObj)? $matchObj->m3:''),		array(),	true);

        $data['form'] = $form->getTemplateVars();
        $data['module'] = 'premierLeagueUpComingMatch';

        return view('admin.form2',$data);
    }

    public function doAddEditUpComingMatch(Request $request)
    {
        $inputs = array(
            'm1' => $request->input('m1'),
            'm2' => $request->input('m2'),
            'm3' => $request->input('m3'),
        );

        Configs::updateParam('SYSTEM_PREMIERLEAGUE_UPCOMING_MATCH', json_encode($inputs));

        return response()->json(array('code' => Lang::get('COMMON.SUCESSFUL')));
    }

    public function drawGiveToken(Request $request)
    {
        $toAdd 	  = true;
        $object   = null;
//        $readOnly = false;
        $values   = array();

        $title = 'Give Token';

        $form = App::setupPanelForm('doadd', $request->all(), array(), $toAdd);
        $form->setFormTitle($title);

        $form->addInput('select', 	'Currency',			'crccode',			'MYR',		array('options' => array('MYR' => 'MYR', 'IDR' => 'IDR')),	true);
        $form->addInput('text', 	'Member Username',	'nickname',		'',		array(),	true);
        $form->addInput('text', 	'Match ID',			'match_id',		'',		array(),	true);
        $form->addInput('text', 	'Token Amount',		'token_amount',	'',		array(),	true);
        $form->addInput('text', 	'Remark',			'remark',			'',		array(),	true);

        $data['form'] = $form->getTemplateVars();
        $data['module'] = 'premierLeagueGiveToken';

        return view('admin.form2',$data);
    }

    public function doAddEditGiveToken(Request $request)
    {
        $accObj = Account::where('crccode', '=', $request->input('crccode'))
            ->where('nickname', '=', $request->input('nickname'))
            ->first();

        $matchObj = PremierLeagueMatch::find($request->input('match_id'));

        if ($accObj) {
            if ($matchObj) {
                PremierLeagueToken::forceCreate(array(
                    'match_id' => $matchObj->id,
                    'accid' => $accObj->id,
                    'crccode' => $request->input('crccode'),
                    'nickname' => $accObj->nickname,
                    'token_amount' => $request->input('token_amount', 0),
                    'remark' => $request->input('remark'),
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ));

                $msg = Lang::get('COMMON.SUCESSFUL');
            } else {
                $msg = 'Invalid match ID.';
            }
        } else {
            $msg = 'Username not found.';
        }

        return response()->json(array('code' => $msg));
    }
}
