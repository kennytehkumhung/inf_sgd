<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller\Admin;
use App\libraries\grid\PanelGrid;
use App\libraries\App;
use Illuminate\Http\Request;
use Auth;
use Validator;
use Session;
use Lang;
use App\Models\Currency;
use App\Models\Fundingmethod;
use App\Models\Cashledger;
use App\Models\Bank;
use App\Models\Media;
use App\Models\Account;
use Config;
use App\Models\Promocash;
use App\Models\Profitloss;
use App\Models\Rejectreason;
use App\Models\Banktransfer;
use App\Models\Promocampaign;
use App\Models\Configs;
use App\Models\Tag;
use Storage;
use File;
use DB;

class PromotionReportController extends Controller{


	protected $moduleName 	= 'promotionreport';
	protected $limit 		= 20;
	protected $start 		= 0;
	
	public function __construct()
	{
		
	}
	
	public function index(Request $request){

			$grid = new PanelGrid;
			$grid->setupGrid($this->moduleName, 'promotionReport-data', $this->limit, true, array('multiple' => true,'footer' => true ));
			$grid->setTitle(Lang::get('COMMON.PROMOCAMPAIGN'));
				
			$grid->addColumn('name', 			Lang::get('COMMON.PROMOTION'), 	250,		array('align' => 'left'));			
			$grid->addColumn('totalmember', 	'Member Count', 				100,        array('align' => 'center'));			
			$grid->addColumn('totaldeposit', 	'Total Deposit', 				150,        array('align' => 'right'));			
			$grid->addColumn('totalpromo', 		'Total Promo Cash', 			150,        array('align' => 'right'));			
			$grid->addColumn('approvedamount', 	'Approved Amount', 				150,        array('align' => 'right'));			
	
			$grid->addFilter('crccode', Currency::getAllCurrencyAsOptions(), array('display' => Lang::get('COMMON.CURRENCY')));
			$grid->addFilter('status',  Promocampaign::getStatusOptions(), array('display' => Lang::get('COMMON.TYPE')));
		
			$data['grid'] = $grid->getTemplateVars();
			$data['paging'] = false;

			return view('admin.grid2',$data);
    }
	
	protected function doGetData(Request $request){
	
		
		
		$fromtime 	= $request->createdfrom.' 00:00:00';
		$totime 	= $request->createdto.' 23:59:59';
		
		$condition = '1';
		
		if( $request->aqfld['crccode'] == '' ){
			$condition .= ' AND crccode = "'.Session::get('admin_crccode').'" ';
		}else{
			$condition .= ' AND crccode = "'.$request->aqfld['crccode'].'" ';
		}
		
		$total_report['totalmember']		= 0;
		$total_report['totaldeposit']		= 0;
		$total_report['totalpromo']			= 0;
		$total_report['approvedamount']		= 0;
		

		//new structure
		
		$total = Promocampaign::whereRaw($condition)->count();
		
		if($promos = Promocampaign::whereRaw($condition)->orderBy('id','desc')->get()){
			foreach( $promos as $promo ){
				
				$totalmember  = DB::select( DB::raw('select COUNT(DISTINCT c.accid) as totalmember from cashledger c, promocash p where c.refid = p.id and p.pcpid = '.$promo->id.' and c.created >= "'.$fromtime.'" and c.created <= "'.$totime.'" and c.chtcode = 3 and c.status IN (1,4) '));
				
				$totaldeposit = DB::select( DB::raw('select sum(c.amount) as totaldeposit from cashledger c, promocash p where c.id = p.clgid and p.pcpid = '.$promo->id.' and c.created >= "'.$fromtime.'" and c.created <= "'.$totime.'" and c.status IN (1,4)  '));
				
				$totalpromo   = DB::select( DB::raw('select sum(c.amount) as totalpromo from cashledger c, promocash p where c.refid = p.id and p.pcpid = '.$promo->id.' and c.created >= "'.$fromtime.'" and c.created <= "'.$totime.'" and c.chtcode = 3 and c.status IN (1,4) '));
				

				$rows[] = array(

					'name'  	    	=> $promo->name, 
					'totalmember'  		=> $totalmember[0]->totalmember, 
					'totaldeposit'  	=> App::formatNegativeAmount($totaldeposit[0]->totaldeposit), 
					'totalpromo'     	=> App::formatNegativeAmount($totalpromo[0]->totalpromo), 
					'approvedamount'    => App::formatNegativeAmount($totaldeposit[0]->totaldeposit + $totalpromo[0]->totalpromo), 


				);	
				
				
				$total_report['totalmember'] 	  += $totalmember[0]->totalmember;
				$total_report['totaldeposit']     += $totaldeposit[0]->totaldeposit;
				$total_report['totalpromo']	      += $totalpromo[0]->totalpromo;
				$total_report['approvedamount']	  += ($totaldeposit[0]->totaldeposit + $totalpromo[0]->totalpromo);
				
			}
				
		}
		
		$total_report['totalmember'] 		= $total_report['totalmember'];
		$total_report['totaldeposit'] 		= App::formatNegativeAmount($total_report['totaldeposit']);
		$total_report['totalpromo'] 	    = App::formatNegativeAmount($total_report['totalpromo']);
		$total_report['approvedamount'] 	= App::formatNegativeAmount($total_report['approvedamount']);

		
		
		
		
		$footer[] = $total_report;

				
		echo json_encode(array('total' => count($rows), 'rows' => $rows, 'footer' => $footer));
		exit;
	
	
	}
	
	
}

?>
