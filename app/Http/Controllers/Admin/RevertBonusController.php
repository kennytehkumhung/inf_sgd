<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller\Admin;
use App\libraries\grid\PanelGrid;
use App\libraries\App;
use App\Models\Revertbonus;
use App\Models\Revertbonussetting;
use Illuminate\Http\Request;
use Auth;
use Validator;
use Session;
use Lang;
use App\Models\Iptracker;
use App\Models\Currency;
use App\Models\Fundingmethod;
use App\Models\Cashledger;
use App\Models\Bank;
use App\Models\Accounttag;
use App\Models\Tag;
use App\Models\Account;
use App\Models\Agent;
use App\Models\Cashledgerlog;
use App\Models\Bankaccount;
use App\Models\Profitloss;
use App\Models\Promocash;
use App\Models\Rejectreason;
use App\Models\Banktransfer;
use App\Models\Promocampaign;
use App\Models\Remark;
use App\Models\Product;
use Config;
use DB;

class RevertBonusController extends Controller {

    protected $moduleName 	= 'revertbonus';
    protected $limit 		= 20;
    protected $start 		= 0;

    public function __construct()
    {

    }

    public function index(Request $request){

        $grid = new PanelGrid;
        $grid->setupGrid($this->moduleName, 'revertbonus-data', $this->limit);
        $grid->setTitle(Lang::get('COMMON.REVERTBONUS'));

        $grid->addColumn('id', 				Lang::get('COMMON.ID'), 				0,			array('align' => 'left'));
        $grid->addColumn('currency',		Lang::get('COMMON.CURRENCY'), 			68,			array('align' => 'center'));
        $grid->addColumn('from', 			Lang::get('COMMON.FROM'), 				120,		array('align' => 'left'));
        $grid->addColumn('to', 				Lang::get('COMMON.END'), 				120,		array('align' => 'left'));
        $grid->addColumn('percentage', 		Lang::get('COMMON.PERCENTAGE'), 		85,			array('align' => 'center'));
        $grid->addColumn('maxpayout', 		Lang::get('COMMON.MAXPAYOUT'), 			120,		array('align' => 'left'));
        $grid->addColumn('minpayout', 		Lang::get('COMMON.MINPAYOUT'), 			120,		array('align' => 'left'));

        $grid->addButton('1', Lang::get('COMMON.ADDNEW'), Lang::get('COMMON.ADDNEW').' '.Lang::get('COMMON.REVERTBONUS'), 'button', array('icon' => 'add', 'url' => action('Admin\RevertBonusController@drawAddEdit')));

        $data['grid'] = $grid->getTemplateVars();

        return view('admin.grid2',$data);
    }

    protected function doGetData(Request $request){

        $rows = array();

        $condition = '1';
        $condition .= Session::get('admin_crccode') == 'MYR' ? '' : ' AND crccode = "'.Session::get('admin_crccode').'"';

        $total = Revertbonussetting::whereRaw($condition)->count();

        //new structure
        if($settings = Revertbonussetting::whereRaw($condition)->skip($request->recordstartindex)->take( $request->pagesize )->orderBy('id','desc')->get()){
            foreach( $settings as $setting ){
                $rows[] = array(
                    'id'  					=> $setting->id,
                    'currency'  			=> $setting->crccode,
                    'from'  				=> $setting->datefrom,
                    'to'  					=> $setting->dateto,
                    'percentage'  			=> $setting->rate.'%',
                    'maxpayout'  			=> $setting->maxamount,
                    'minpayout'  			=> $setting->minpayout,
                );
            }
        }

        echo json_encode(array('total' => $total, 'rows' => $rows));
        exit;
    }

    protected function drawAddEdit(Request $request) {

        $toAdd 	  = true;
        $object   = null;
        $readOnly = false;
        $values   = array();

        $title = Lang::get('COMMON.NEW').' '.Lang::get('COMMON.REVERTBONUS');

        if($request->has('sid'))
        {
            if ($object = Revertbonussetting::find($request->input('sid')))
            {
                $title    = Lang::get('COMMON.EDIT').' '.Lang::get('COMMON.REVERTBONUS');
                $toAdd    = false;
                $readOnly = true;
            }
            else
            {
                $object = null;
            }
        }

        $currency  = Currency::getAllCurrencyAsOptions();

        $form = App::setupPanelForm('doadd', $request->all(), array(), $toAdd);
        $form->setFormTitle($title);

        $form->addInput('select', 		Lang::get('COMMON.CURRENCY')    , 'crccode',		(($object)? $object->crccode:''), 		array('options' => $currency),	 true);
        $form->addInput('date', 		Lang::get('COMMON.FROM')		, 'from',			(($object)? $object->datefrom:''), 		array(), false);
        $form->addInput('date', 		Lang::get('COMMON.TO')			, 'to',				(($object)? $object->dateto:''), 		array(), false);
        $form->addInput('text', 		Lang::get('COMMON.PERCENTAGE') 	, 'percentage', 	(($object)? $object->rate:''), 			array(), false);
        $form->addInput('text', 		Lang::get('COMMON.MINPAYOUT') 	, 'minpayout', 		(($object)? $object->minpayout:''), 	array(), false);
        $form->addInput('text', 		Lang::get('COMMON.MAXPAYOUT') 	, 'maxpayout', 		(($object)? $object->maxamount:''), 	array(), false);

        $data['form'] = $form->getTemplateVars();
        $data['module'] = 'revertbonus';

        return view('admin.form2',$data);
    }

    protected function doAddEdit(Request $request) {

        $validator = Validator::make(
            [
                'from'	  	  		 => $request->input('from'),
                'to'	  	 		 => $request->input('to'),
                'percentage'	  	 => $request->input('percentage'),
                'minpayout'	  	 	 => $request->input('minpayout'),
                'maxpayout'	  	 	 => $request->input('maxpayout'),
                'crccode'	  	 	 => $request->input('crccode'),
            ],
            [
                'from'	   	   	   => 'required',
                'to'	  		 	   => 'required',
                'percentage'	  	   => 'required',
                'minpayout'	  	   => 'required',
                'maxpayout'	  	   => 'required',
                'crccode'	  	   => 'required',
            ]
        );


        if ($validator->fails())
        {
            echo json_encode($validator->errors());
            exit;
        }

        $object  = new Revertbonussetting();

        if($request->has('sid'))
        {
            $object = Revertbonussetting::find($request->input('sid'));
        } else {
            $object->createdby = Session::get('admin_userid');
        }

        $object->wbsid				= 1;
        $object->datefrom	 		= $request->input('from');
        $object->dateto	 			= $request->input('to');
        $object->rate	 			= $request->input('percentage');
        $object->minpayout	 		= $request->input('minpayout');
        $object->maxamount	 		= $request->input('maxpayout');
        $object->membertype	 		= 0;
        $object->crccode	 		= $request->input('crccode');
        $object->modifiedby	 		= Session::get('admin_userid');
//        $object->status	 			= CBO_STANDARDSTATUS_ACTIVE;

        if( $object->save() )
        {
            echo json_encode( array( 'code' => Lang::get('COMMON.SUCESSFUL') ) );
            exit;
        }

        echo json_encode( array( 'code' => Lang::get('COMMON.FAILED') ) );
    }

    public function showrevertbonusReport(Request $request){

        $grid = new PanelGrid;

        if( $request->has('aflid') )
        {
            $grid->setupGrid($this->moduleName, 'revertBonusReport-data', $this->limit, true, array('params' => array('aflid' => $request->aflid)));
        }
        else
        {
            $grid->setupGrid($this->moduleName, 'revertBonusReport-data', $this->limit);
        }
        $grid->setTitle(Lang::get('COMMON.REVERTBONUSREPORT'));

        $grid->addColumn('from', 				Lang::get('COMMON.FROM'), 				100,		array('align' => 'center'));
        $grid->addColumn('to', 					Lang::get('COMMON.TO'), 				100,		array('align' => 'center'));
        $grid->addColumn('currency', 			Lang::get('COMMON.CURRENCY'), 			100,		array('align' => 'center'));
        $grid->addColumn('rate', 				Lang::get('COMMON.RATE'), 				100,		array('align' => 'right'));
        $grid->addColumn('member', 				Lang::get('COMMON.MEMBER'), 			100,		array('align' => 'right'));
        $grid->addColumn('stake', 				Lang::get('COMMON.VALIDSTAKE'), 		130,		array('align' => 'right'));
        $grid->addColumn('revertbonus', 			Lang::get('COMMON.REVERTBONUS'), 			100,		array('align' => 'right'));
//        $grid->addColumn('effective', 			Lang::get('COMMON.EFFECTIVE'), 			100,		array('align' => 'right'));
        $grid->addColumn('status', 				Lang::get('COMMON.STATUS'), 			100,		array('align' => 'center'));
        $grid->addColumn('action', 				'', 									100,		array('align' => 'center'));


        $grid->addFilter('status',array('Active','Suspended'), array('display' => Lang::get('COMMON.TYPE')));

        $data['grid'] = $grid->getTemplateVars();

        return view('admin.grid2',$data);
    }

    protected function showrevertBonusReportdoGetData(Request $request){

        $rows = array();
        $footer = array();
        $total_revertbinuses = array();

        $condition = Session::get('admin_crccode') == 'MYR' ? 'dateto <= "'.App::getDateTime(3).'"' : 'crccode = "'.Session::get('admin_crccode').'" AND dateto <= "'.App::getDateTime(3).'"';

        $total = Revertbonussetting::whereRaw($condition)->count();

        if($settings = Revertbonussetting::whereRaw($condition)->skip($request->recordstartindex)->take($request->recordendindex)->orderBy('id','desc')->get()){

            foreach($settings as $setting){
                $condition = 'istest = 0 AND crccode = "'.$setting->crccode.'" AND date >= "'.$setting->datefrom.'" AND date <= "'.$setting->dateto.'" ';
                $condition .= 'AND category IN ('.Product::CATEGORY_LIVECASINO.','.Product::CATEGORY_RNG.') ';

                if( $request->has('aflid') ) {
                    $condition .= ' AND level3 = '.Agent::whereAflid($request->aflid)->pluck('id').' ';
                }

                $member = Profitloss::whereRaw($condition)->count();
                $stake = Profitloss::whereRaw($condition)->sum('validstake');

                $amount = ' - ';
//                $effective = ' - ';

                if($setting->status == CBO_STANDARDSTATUS_SUSPENDED )
                {
                    $action = '<a href="#" onclick="doConfirmAction(\'revertbonus-docalculate\', \'revertbonus-docalculate\', '.$setting->id.')">'.Lang::get('COMMON.CALCULATE').'</a>';

                    if($request->has('aflid'))
                        $action = '';
                }

                else {
                    if($request->has('aflid')){
                        $action = '<a href="#" onclick="parent.addTab(\''.Lang::get('REVERTBONUS').': '.$setting->id.'\', \'revertbonuslist?id='.$setting->id.'&aflid='.$request->aflid.'\')">'.Lang::get('COMMON.VIEW').'</a>';
                        $amount = App::displayAmount(Revertbonus::whereRaw('rbsid='.$setting->id.' AND amount >= "'.$setting->minpayout.'" AND accid IN ( select id from account where aflid = '.$request->aflid.' )')->sum('amount'));
                    }else{
                        $action = '<a href="#" onclick="parent.addTab(\''.Lang::get('REVERTBONUS').': '.$setting->id.'\', \'revertbonuslist?id='.$setting->id.'\')">'.Lang::get('COMMON.VIEW').'</a>';
                        $amount = App::displayAmount(Revertbonus::whereRaw('rbsid='.$setting->id.' AND amount >= "'.$setting->minpayout.'"')->sum('amount'));
                    }

//                    $effective = 0;
//                    if($stake > 0)
//                    {
//                        $effective = $amount / $stake * 100;
//                    }
//                    $effective = App::displayPercentage($effective);
                }

                $stake = ($setting->crccode == 'VND' || $setting->crccode == 'IDR') ? $stake * 1000 : $stake;

                $rows[] = array(
                    'id' => $setting->id,
                    'from' => $setting->datefrom,
                    'to' => $setting->dateto,
                    'currency' => $setting->crccode,
                    'rate' => App::displayPercentage($setting->rate),
                    'member' => App::displayAmount($member, 0),
                    'stake' => App::displayAmount($stake),
                    'revertbonus' => $amount,
//                    'effective' => $effective,
                    'action' => $action,
                    'status' => $setting->getStatusText(),
                );
            }
        }

        echo json_encode(array('total' => $total, 'rows' => $rows));
        exit;
    }

    protected function doCalculateRevertbonus(Request $request) {

        $result = false;
        if($request->has('id'))
        {
            if ($object = Revertbonussetting::whereRaw('id='.$request->id.' AND status='.CBO_STANDARDSTATUS_SUSPENDED)->first())
            {
                $stake 		= array();
                $stakelocal = array();
                $currency 	= array();
                $accounts 	= array();
                $condition 	= 'istest = 0 AND date >="'.$object->datefrom.'" AND date <="'.$object->dateto.'" AND crccode = "'.$object->crccode.'" ';
                $condition .= 'AND category IN ('.Product::CATEGORY_LIVECASINO.','.Product::CATEGORY_RNG.') ';

                $isSpecial = false;
                $specialRebates = array();

                if($profits = Profitloss::whereRaw($condition)->get()) {
                    foreach($profits as $profit) {
                        $rate = $object->rate;

                        if ($isSpecial) {
                            if (array_key_exists('_'.$profit->prdid, $specialRebates)) {
                                $rate = $specialRebates['_'.$profit->prdid];
                            }
                        }

                        if(!isset($currency[$profit->accid])) $currency[$profit->accid] = $profit->crccode;
                        if(!isset($accounts[$profit->accid])) $accounts[$profit->accid] = $profit->acccode;
                        if(isset($stake[$profit->accid])) {
                            $stake[$profit->accid] += ((float) $profit->validstake) * $rate / 100;
                            $stakelocal[$profit->accid] += (float) $profit->validstakelocal;
                        } else {
                            $stake[$profit->accid] = ((float) $profit->validstake) * $rate / 100;
                            $stakelocal[$profit->accid] = (float) $profit->validstakelocal;
                        }
                    }
                }

                foreach($stake as $accid => $turnover) {
                    $amount = App::float($turnover);

                    if($object->maxamount > 0 && $amount > $object->maxamount)
                        $amount = $object->maxamount;

                    $incObj = new Revertbonus();
                    $incObj->rbsid 				= $object->id;
                    $incObj->product 			= $object->product;
                    $incObj->prdid				= $object->prdid;
                    $incObj->accid 				= $accid;
                    $incObj->acccode 			= $accounts[$accid];
                    $incObj->crccode 			= $currency[$accid];
                    $incObj->crcrate 			= Currency::getCurrencyRate($incObj->crccode);
                    $incObj->datefrom 			= $object->datefrom;
                    $incObj->dateto 			= $object->dateto;
                    $incObj->rate 				= $object->rate;
                    $incObj->totalstake 		= ($incObj->crccode == 'VND' || $incObj->crccode == 'IDR') ? $turnover * 1000 : $turnover;
                    $incObj->totalstakelocal 	= ($incObj->crccode == 'VND' || $incObj->crccode == 'IDR') ? $stakelocal[$accid] * 1000 : $stakelocal[$accid];
                    $incObj->amount 			= ($incObj->crccode == 'VND' || $incObj->crccode == 'IDR') ? $amount * 1000 : $amount;
                    $incObj->amountlocal 		= Currency::getLocalAmount($incObj->amount, $incObj->crccode);
                    $incObj->save();
                }

                $object->status = CBO_STANDARDSTATUS_ACTIVE;
                $object->save();
            }
        }

        echo json_encode(array('success' => $result));
    }

    public function showrevertbonus(Request $request){

        $grid = new PanelGrid;

        if( $request->has('aflid') )
        {
            $grid->setupGrid($this->moduleName, 'revertbonuslist-data', $this->limit, false,array('params' => array('id' => $request->id,'aflid' => $request->aflid)));
        }
        else
        {
            $grid->setupGrid($this->moduleName, 'revertbonuslist-data', $this->limit, false,array('params' => array('id' => $request->id),'checkbox'=>true,'checkbox_url'=>'revertbonuslist-batchapprove'));
        }

        $grid->setTitle(Lang::get('COMMON.REVERTBONUS'));

        $grid->addColumn('checkbox', 		'', 									18,		array('checkbox' => true));
        $grid->addColumn('id',				Lang::get('COMMON.ID'), 				50,		array('align' => 'right'));
        $grid->addColumn('acccode', 		Lang::get('COMMON.ACCOUNT'), 			80,		array());
        $grid->addColumn('currency', 		Lang::get('COMMON.CURRENCY'), 			60,		array('align' => 'center'));
        $grid->addColumn('deposit', 		Lang::get('COMMON.DEPOSIT'), 			150,	array('align' => 'right'));
        $grid->addColumn('withdrawal', 		Lang::get('COMMON.WITHDRAWAL'), 			150,	array('align' => 'right'));
        $grid->addColumn('turnover', 		Lang::get('COMMON.TURNOVER'), 			150,	array('align' => 'right'));
        $grid->addColumn('pnl', 		Lang::get('COMMON.PROFITLOSS'), 			150,	array('align' => 'right'));
        $grid->addColumn('revert_bonus', 		Lang::get('COMMON.REVERTBONUS'), 			150,	array('align' => 'right'));
        $grid->addColumn('rate', 			Lang::get('COMMON.RATE'), 				100,	array('align' => 'right'));
        $grid->addColumn('taken_bonus', 	Lang::get('COMMON.TAKENBONUS'), 		150,	array('align' => 'right'));
        $grid->addColumn('taken_bonus_type', 	Lang::get('COMMON.TAKENBONUS').' Type', 		200,	array('align' => 'left'));

        if( !$request->has('aflid') )
        {
            $grid->addButton('2', Lang::get('COMMON.APPROVE'), '', 'button', array('icon' => 'ok', 'url' => 'approve_batch(\'approve\');'));
        }

        $data['grid'] = $grid->getTemplateVars();

        return view('admin.grid2',$data);
    }

    protected function doGetRevertbonuslistData(Request $request){

        $rows = array();
        $condition = 'rbsid = '.$request->id;

        $revertBonusSetting = Revertbonussetting::find($request->id);
        $condition .= ' and amount >= '.$revertBonusSetting->minpayout;

        if( $request->has('aflid') )
        {
            $condition .= ' and accid IN ( select id from account where aflid = '.$request->aflid.' )';
        }

        $total = Revertbonus::whereRaw($condition)->count();

        $total_revertbonus = 0;
        $promocampaign = Promocampaign::getMethodOptions();

        $rbsBuilder = Revertbonus::whereRaw($condition)->skip($request->recordstartindex)->take( $request->pagesize );

        if ($request->has('sortdatafield')) {
            $sortField = $request->input('sortdatafield');
            $orderable = array('id' => 'id', 'product' => 'product', 'rate' => 'rate', 'currency' => 'crccode', 'turnover' => 'totalstake', 'revert_bonus' => 'amount', 'amount' => 'amount');

            if (array_key_exists($sortField, $orderable)) {
                $rbsBuilder->orderBy($orderable[$sortField], $request->input('sortorder', 'desc'));
            } else {
                $rbsBuilder->orderBy('id','desc');
            }
        } else {
            $rbsBuilder->orderBy('id','desc');
        }

        if($revertBonuses = $rbsBuilder->get()){
            foreach( $revertBonuses as $revertBonus ){

                $bonuses = '';

                if( $promocashs = DB::select( DB::raw('select n.method as method from cashledger c,promocash p,promocampaign n where c.chtcode = 3 and c.refid = p.id and p.pcpid = n.id and c.status IN(1,4) and c.created >= "'.$revertBonus->datefrom.' 00:00:00" AND c.created <= "'.$revertBonus->dateto.' 23:59:59" and c.accid = '.$revertBonus->accid.' group by n.method ')) ){

                    foreach( $promocashs as $promocash ){
                        if( $promocash->method != 0 )
                            $bonuses .= $promocampaign[$promocash->method].',';
                    }

                }

                $accObj = Account::find($revertBonus->accid);
                $profitloss = App::displayAmount(Profitloss::where('accid', '=', $accObj->id)->whereIn('category', array(Product::CATEGORY_LIVECASINO, Product::CATEGORY_RNG))->whereBetween('date', array($revertBonus->datefrom . ' 00:00:00', $revertBonus->dateto . ' 23:59:59'))->sum('amount'));

                if ($profitloss > 0) {
                    $rows[] = array(
                        'id' 			=> $revertBonus->id,
                        'acccode' 		=> $accObj->nickname,
                        'currency'		=> $revertBonus->crccode,
                        'deposit'  		=> App::displayAmount(Cashledger::whereChtcode(CBO_CHARTCODE_DEPOSIT)->where('accid', '=', $accObj->id)->whereBetween('created', array($revertBonus->datefrom . ' 00:00:00', $revertBonus->dateto . ' 23:59:59'))->whereIn('status', array(1,4))->sum('amount')),
                        'withdrawal'  	=> App::displayAmount(Cashledger::whereChtcode(CBO_CHARTCODE_WITHDRAWAL)->whereAccid('accid', '=', $accObj->id)->whereBetween('created', array($revertBonus->datefrom . ' 00:00:00', $revertBonus->dateto . ' 23:59:59'))->whereIn('status', array(1,4))->sum('amount')),
                        'turnover'  	=> App::displayAmount($revertBonus->totalstake),
                        'pnl'  			=> $profitloss,
                        'revert_bonus' 	=> App::displayAmount($revertBonus->amount),
                        'amount' 		=> $revertBonus->amount,
                        'rate'			=> App::displayPercentage($revertBonus->rate),
                        'taken_bonus' 	=> Cashledger::whereRaw(' accid = '.$revertBonus->accid.' AND chtcode = 3 AND status IN(1,4) AND created >= "'.$revertBonus->datefrom.' 00:00:00" AND created <= "'.$revertBonus->dateto.' 23:59:59" ')->count(),
                        'taken_bonus_type' 	=> $bonuses,
                    );
                    $total_revertbonus += $revertBonus->amount;
                }

            }

        }

        echo json_encode(array('total' => $total, 'rows' => $rows));
        exit;
    }

    public function batchApprove(Request $request){

        $settings = array();
        $success  = false;

        foreach( $request->rows as $row ){

            if($object = Revertbonus::find($row))
            {
                if(!isset($settings[$object->rbsid]))
                {
                    $insObj = Revertbonussetting::find($object->rbsid);
                    $settings[$object->rbsid] = $insObj;
                }

                if($object->amount >= $settings[$object->rbsid]->minpayout)
                {
                    if(($settings[$object->rbsid]->maxamount > 0) && ($object->amount > $settings[$object->rbsid]->maxamount))
                    {
                        $object->amount = $settings[$object->rbsid]->maxamount;
                        $object->save();
                    }

                    $accObj = Account::find($object->accid);

                    $cashLedgerObj = new Cashledger;
                    $cashLedgerObj->accid 			= $object->accid;
                    $cashLedgerObj->acccode 		= $object->acccode;
                    $cashLedgerObj->accname			= $accObj->fullname;
                    $cashLedgerObj->chtcode 		= CBO_CHARTCODE_REVERTBONUS;
                    $cashLedgerObj->cashbalance 	= $cashLedgerObj->getBalance($object->accid);;
                    $cashLedgerObj->amount 			= $object->amount;
                    $cashLedgerObj->amountlocal 	= $object->amountlocal;
                    $cashLedgerObj->refid 			= $object->id;
                    $cashLedgerObj->status 			= CBO_LEDGERSTATUS_PENDING;
                    $cashLedgerObj->refobj 			= 'RevertBonus';
                    $cashLedgerObj->crccode 		= $object->crccode;
                    $cashLedgerObj->crcrate 		= Currency::getCurrencyRate($object->crccode);
                    $cashLedgerObj->fee 			= $object->amount;
                    $cashLedgerObj->feelocal 		= $object->amountlocal;
                    $cashLedgerObj->createdip		= App::getRemoteIp();

                    if($cashLedgerObj->save())
                    {
                        $object->clgid = $cashLedgerObj->id;
                        $object->save();
                        $success = true;
                    }
                }
            }

        }

        if( $success )
            echo json_encode( array( 'code' => Lang::get('COMMON.SUCESSFUL') ) );
        else
            echo json_encode( array( 'code' => Lang::get('COMMON.FAILED') ) );

    }
}
