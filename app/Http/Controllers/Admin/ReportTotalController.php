<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller\Admin;
use App\libraries\grid\PanelGrid;
use App\libraries\App;
use App\Models\Currency;
use App\Models\Product;
use App\Models\Account;
use App\Models\Cashbalance;
use App\Models\Profitloss;
use App\Models\Cashledger;
use App\Models\Promocampaign;
use App\Models\Promocash;
use App\Models\Balancetransfer;
use Illuminate\Http\Request;
use Auth;
use Validator;
use Session;
use Lang;
use DB;
use Config;


class ReportTotalController extends Controller{

	
	protected $moduleName 	= 'reporttotal';
	protected $limit 		= 20;
	protected $start 		= 0;
	
	protected $members = array(
		'members'     =>   array(
			'name'    => 'MEMBER', 
			'total'   => 0, 
			'valid'   => 0, 
			'invalid' => 0
		),
		'balance'     =>   array(
			'name'    => 'CASHBALANCE', 
			'total'   => 0, 
			'valid'   => 0, 
			'invalid' => 0
		),
	);
	
	protected $newMembers = array(
		'affiliates'  =>   array(
			'name' 	  => 'AFFILIATE', 
			'members' => 0
		),
		'members'     =>   array(
			'name'    => 'MEMBER', 
			'members' => 0
		),
	);
	
	protected $products = array();  
	
 	protected $totalprd = array(     
 		'members'         => 0, 
 		'wagertotal'      => 0,
 		'wagervalid'      => 0,
 		'staketotal'      => 0,
 		'stakevalid'      => 0,
 		'winloss'         => 0
 	);
	
	protected $cashledgers = array(
		CBO_CHARTCODE_DEPOSIT     		  => array('name' => '',  'amount' => 0, 'total' => 0, 'fee' => 0, 'url' => ''),
		CBO_CHARTCODE_WITHDRAWAL  		  => array('name' => '',  'amount' => 0, 'total' => 0, 'fee' => 0, 'url' => ''),
		CBO_CHARTCODE_BONUS       		  => array('name' => '',  'amount' => 0, 'total' => 0, 'fee' => 0, 'url' => ''),
		CBO_CHARTCODE_ADJUSTMENT 		  => array('name' => '',  'amount' => 0, 'total' => 0, 'fee' => 0, 'url' => ''),
		CBO_CHARTCODE_INCENTIVE   		  => array('name' => '',  'amount' => 0, 'total' => 0, 'fee' => 0, 'url' => ''),
		CBO_CHARTCODE_WITHDRAWALCHARGES           => array('name' => '',  'amount' => 0, 'total' => 0, 'fee' => 0, 'url' => ''),
	);
 	

	public function __construct()
	{
		
	}

	public function show(Request $request)
	{
		
		$createdfrom = $request->input('datefrom');
		$createdto   = $request->input('dateto');
		
		$fromdate = (isset($createdfrom)) ? $createdfrom : App::getDateTime(3);
		$todate   = (isset($createdto))   ?$createdto    : App::getDateTime(3);

		//normal member 
		if( $request->has('crccode') )
		{
			$currencies[$request->crccode] = $request->crccode;
		}
		else
		{
			$currencies = Currency::getAllCurrencyAsOptions();
		}
                
		$products   = Product::getAllAsOptions(true, true);
		$totals = 0;
		foreach($currencies as $currency) {
			$members[$currency]  = Account::whereRaw('istestacc=0 AND isinternal = 0 AND crccode = "'.$currency.'"')->count();
			$register[$currency] = Account::whereRaw('istestacc=0 AND isinternal = 0 AND crccode = "'.$currency.'" AND created >= "'.$fromdate.' 00:00:00" AND created <= "'.$todate.' 23:59:59"')->count();
			
			$balances[$currency][Cashbalance::BALANCETYPE_MAIN] = App::formatNegativeAmount(Cashledger::whereRaw('crccode = "'.$currency.'" AND status IN (1,4) AND created <= "'.$todate.' 23:59:59"')->sum('amount'));
			foreach($products as $prdid => $product) {
				
				$wagerPnl 			= Profitloss::whereRaw('crccode = "'.$currency.'" AND prdid = '.$prdid)->sum('amount');
				$productdeposit 	= Balancetransfer::whereRaw('crccode = "'.$currency.'" AND `to`   = '.$prdid.' AND status = 1 AND created <= "'.$todate.' 23:59:59"')->sum('amount');
				$productwidthdraw   = Balancetransfer::whereRaw('crccode = "'.$currency.'" AND `from` = '.$prdid.' AND status = 1 AND created <= "'.$todate.' 23:59:59"')->sum('amount');
				$adjustment = 0;
				$total = $wagerPnl+$productdeposit-$productwidthdraw + $adjustment;
				if($total < 0) $total = $total * -1;
				$balances[$currency][$prdid] = App::formatNegativeAmount($total);
                                $totals = $totals + $total;

				//$balances[$currency][$prdid] = App::formatNegativeAmount(Cashbalance::whereRaw('crccode = "'.$currency.'" AND prdid = '.$prdid)->sum('balance'));
			}
			$balances[$currency]['total'] = App::displayAmount($totals);
		}
		
		$data['products']    = $products;
		$data['currencies']  = $currencies;
		$data['members']     = $members;
		$data['registers']    = $register;
		$data['balances']    = $balances;
		

		$profitLoss = array();
		$profitLossTotal = array();
		foreach($currencies as $currency) {
			
			foreach($products as  $key => $value){
			
				$members    = Profitloss::whereRaw('crccode = "'.$currency.'" AND istest = 0 AND prdid = '.$key.' AND date>="'.$fromdate.'" AND date<="'.$todate.'"')->count('accid');
				$wagertotal = Profitloss::whereRaw('crccode = "'.$currency.'" AND istest = 0 AND prdid = '.$key.' AND date>="'.$fromdate.'" AND date<="'.$todate.'"')->sum('wager');
				$staketotal = Profitloss::whereRaw('crccode = "'.$currency.'" AND istest = 0 AND prdid = '.$key.' AND date>="'.$fromdate.'" AND date<="'.$todate.'"')->sum('totalstake');
				$stakevalid = Profitloss::whereRaw('crccode = "'.$currency.'" AND istest = 0 AND prdid = '.$key.' AND date>="'.$fromdate.'" AND date<="'.$todate.'"')->sum('validstake');
				$winloss    = Profitloss::whereRaw('crccode = "'.$currency.'" AND istest = 0 AND prdid = '.$key.' AND date>="'.$fromdate.'" AND date<="'.$todate.'"')->sum('amount');
				
				
				$pnl = App::formatNegativeAmount($winloss);
				$profitLoss[$key][$currency] = array(
					'member'	 => App::displayNumberFormat($members, 0),
					'wager'		 => App::displayNumberFormat($wagertotal, 0),
					'totalstake' => App::displayAmount($staketotal),
					'validstake' => App::displayAmount($stakevalid),
					'winloss'  	 => App::formatNegativeAmount($winloss),
				);
				
				if(!isset($profitLossTotal[$currency]['wager'])) $profitLossTotal[$currency]['wager'] = $wagertotal;
				else $profitLossTotal[$currency]['wager'] += $wagertotal;
				
				if(!isset($profitLossTotal[$currency]['totalstake'])) $profitLossTotal[$currency]['totalstake'] = $staketotal;
				else $profitLossTotal[$currency]['totalstake'] += $staketotal;
				
				if(!isset($profitLossTotal[$currency]['validstake'])) $profitLossTotal[$currency]['validstake'] = $stakevalid;
				else $profitLossTotal[$currency]['validstake'] += $stakevalid;
				
				if(!isset($profitLossTotal[$currency]['winloss'])) $profitLossTotal[$currency]['winloss'] = $winloss;
				else $profitLossTotal[$currency]['winloss'] += $winloss;
			}
			
			if(!$members = Profitloss::whereRaw('crccode = "'.$currency.'" AND istest = 0 AND date>="'.$fromdate.'" AND date<="'.$todate.'"')->count())
			$members = 0;
			$profitLossTotal[$currency]['totalstake'] = App::displayAmount($profitLossTotal[$currency]['totalstake']);
			$profitLossTotal[$currency]['validstake'] = App::displayAmount($profitLossTotal[$currency]['validstake']);
			$profitLossTotal[$currency]['winloss'] = App::formatNegativeAmount($profitLossTotal[$currency]['winloss']);
			$profitLossTotal[$currency]['member'] = $members;
		}
		
		
		$chtcode = '';
		$fromtime = $fromdate.' 00:00:00';
		$totime = $todate.' 23:59:59';

		foreach($currencies as $currency) {
			$this->cashledgers[$currency][CBO_CHARTCODE_DEPOSIT]['name']  = Lang::get('COMMON.DEPOSIT');
			$this->cashledgers[$currency][CBO_CHARTCODE_DEPOSIT]['total'] = Cashledger::whereRaw('crccode = "'.$currency.'" AND chtcode = '.CBO_CHARTCODE_DEPOSIT.' AND created>="'.$fromtime.'" AND created<="'.$totime.'" AND (status ='.CBO_LEDGERSTATUS_CONFIRMED.' OR status='.CBO_LEDGERSTATUS_MANCONFIRMED.')')->count();
			$this->cashledgers[$currency][CBO_CHARTCODE_DEPOSIT]['amount'] = App::formatNegativeAmount(Cashledger::whereRaw('crccode = "'.$currency.'" AND chtcode = '.CBO_CHARTCODE_DEPOSIT.' AND created>="'.$fromtime.'" AND created<="'.$totime.'" AND (status ='.CBO_LEDGERSTATUS_CONFIRMED.' OR status='.CBO_LEDGERSTATUS_MANCONFIRMED.')')->sum('amount'));
			$this->cashledgers[$currency][CBO_CHARTCODE_DEPOSIT]['fee'] = App::formatNegativeAmount(Cashledger::whereRaw('crccode = "'.$currency.'" AND chtcode = '.CBO_CHARTCODE_DEPOSIT.' AND created>="'.$fromtime.'" AND created<="'.$totime.'" AND (status ='.CBO_LEDGERSTATUS_CONFIRMED.' OR status='.CBO_LEDGERSTATUS_MANCONFIRMED.')')->sum('fee') * -1);
			
			$this->cashledgers[$currency][CBO_CHARTCODE_WITHDRAWAL]['name'] = Lang::get('COMMON.WITHDRAWAL');
			$this->cashledgers[$currency][CBO_CHARTCODE_WITHDRAWAL]['total'] = Cashledger::whereRaw('crccode = "'.$currency.'" AND chtcode = '.CBO_CHARTCODE_WITHDRAWAL.' AND created>="'.$fromtime.'" AND created<="'.$totime.'" AND (status ='.CBO_LEDGERSTATUS_CONFIRMED.' OR status='.CBO_LEDGERSTATUS_MANCONFIRMED.')')->count();
			$this->cashledgers[$currency][CBO_CHARTCODE_WITHDRAWAL]['amount'] = App::formatNegativeAmount(Cashledger::whereRaw('crccode = "'.$currency.'" AND chtcode = '.CBO_CHARTCODE_WITHDRAWAL.' AND created>="'.$fromtime.'" AND created<="'.$totime.'" AND (status ='.CBO_LEDGERSTATUS_CONFIRMED.' OR status='.CBO_LEDGERSTATUS_MANCONFIRMED.')')->sum('amount'));
			$this->cashledgers[$currency][CBO_CHARTCODE_WITHDRAWAL]['fee'] = App::formatNegativeAmount(Cashledger::whereRaw('crccode = "'.$currency.'" AND chtcode = '.CBO_CHARTCODE_WITHDRAWAL.' AND created>="'.$fromtime.'" AND created<="'.$totime.'" AND (status ='.CBO_LEDGERSTATUS_CONFIRMED.' OR status='.CBO_LEDGERSTATUS_MANCONFIRMED.')')->sum('fee') * -1);	
			
                        if(Config::get('setting.front_path') == 'sbm'){
                            $this->cashledgers[$currency][CBO_CHARTCODE_WITHDRAWALCHARGES]['name'] = Lang::get('COMMON.WITHDRAWALCHARGES');
                            $this->cashledgers[$currency][CBO_CHARTCODE_WITHDRAWALCHARGES]['total'] = Cashledger::whereRaw('crccode = "'.$currency.'" AND chtcode = '.CBO_CHARTCODE_WITHDRAWALCHARGES.' AND created>="'.$fromtime.'" AND created<="'.$totime.'" AND (status ='.CBO_LEDGERSTATUS_CONFIRMED.' OR status='.CBO_LEDGERSTATUS_MANCONFIRMED.')')->count();
                            $this->cashledgers[$currency][CBO_CHARTCODE_WITHDRAWALCHARGES]['amount'] = App::formatNegativeAmount(Cashledger::whereRaw('crccode = "'.$currency.'" AND chtcode = '.CBO_CHARTCODE_WITHDRAWALCHARGES.' AND created>="'.$fromtime.'" AND created<="'.$totime.'" AND (status ='.CBO_LEDGERSTATUS_CONFIRMED.' OR status='.CBO_LEDGERSTATUS_MANCONFIRMED.')')->sum('amount'));
                            $this->cashledgers[$currency][CBO_CHARTCODE_WITHDRAWALCHARGES]['fee'] = App::formatNegativeAmount(Cashledger::whereRaw('crccode = "'.$currency.'" AND chtcode = '.CBO_CHARTCODE_WITHDRAWALCHARGES.' AND created>="'.$fromtime.'" AND created<="'.$totime.'" AND (status ='.CBO_LEDGERSTATUS_CONFIRMED.' OR status='.CBO_LEDGERSTATUS_MANCONFIRMED.')')->sum('fee') * -1);
                        }
                        
			$this->cashledgers[$currency][CBO_CHARTCODE_BONUS]['name'] = Lang::get('COMMON.BONUS');
			$this->cashledgers[$currency][CBO_CHARTCODE_BONUS]['total'] = Cashledger::whereRaw('crccode = "'.$currency.'" AND chtcode = '.CBO_CHARTCODE_BONUS.' AND created>="'.$fromtime.'" AND created<="'.$totime.'" AND (status ='.CBO_LEDGERSTATUS_CONFIRMED.' OR status='.CBO_LEDGERSTATUS_MANCONFIRMED.')')->count();
			$this->cashledgers[$currency][CBO_CHARTCODE_BONUS]['amount'] = App::formatNegativeAmount(Cashledger::whereRaw('crccode = "'.$currency.'" AND chtcode = '.CBO_CHARTCODE_BONUS.' AND created>="'.$fromtime.'" AND created<="'.$totime.'" AND (status ='.CBO_LEDGERSTATUS_CONFIRMED.' OR status='.CBO_LEDGERSTATUS_MANCONFIRMED.')')->sum('amount') * -1);
			$this->cashledgers[$currency][CBO_CHARTCODE_BONUS]['fee'] = App::formatNegativeAmount(Cashledger::whereRaw('crccode = "'.$currency.'" AND chtcode = '.CBO_CHARTCODE_BONUS.' AND created>="'.$fromtime.'" AND created<="'.$totime.'" AND (status ='.CBO_LEDGERSTATUS_CONFIRMED.' OR status='.CBO_LEDGERSTATUS_MANCONFIRMED.')')->sum('fee') * -1);
			
			$this->cashledgers[$currency][CBO_CHARTCODE_ADJUSTMENT]['name'] = Lang::get('COMMON.ADJUSTMENT');
			$this->cashledgers[$currency][CBO_CHARTCODE_ADJUSTMENT]['total'] = Cashledger::whereRaw('crccode = "'.$currency.'" AND chtcode = '.CBO_CHARTCODE_ADJUSTMENT.' AND created>="'.$fromtime.'" AND created<="'.$totime.'" AND (status ='.CBO_LEDGERSTATUS_CONFIRMED.' OR status='.CBO_LEDGERSTATUS_MANCONFIRMED.')')->count();
			$this->cashledgers[$currency][CBO_CHARTCODE_ADJUSTMENT]['amount'] = App::formatNegativeAmount(Cashledger::whereRaw('crccode = "'.$currency.'" AND chtcode = '.CBO_CHARTCODE_ADJUSTMENT.' AND created>="'.$fromtime.'" AND created<="'.$totime.'" AND (status ='.CBO_LEDGERSTATUS_CONFIRMED.' OR status='.CBO_LEDGERSTATUS_MANCONFIRMED.')')->sum('amount') * -1);
			$this->cashledgers[$currency][CBO_CHARTCODE_ADJUSTMENT]['fee'] = App::formatNegativeAmount(Cashledger::whereRaw('crccode = "'.$currency.'" AND chtcode = '.CBO_CHARTCODE_ADJUSTMENT.' AND created>="'.$fromtime.'" AND created<="'.$totime.'" AND (status ='.CBO_LEDGERSTATUS_CONFIRMED.' OR status='.CBO_LEDGERSTATUS_MANCONFIRMED.')')->sum('fee') * -1);
			
			$this->cashledgers[$currency][CBO_CHARTCODE_INCENTIVE]['name'] = Lang::get('COMMON.INCENTIVE');
			$this->cashledgers[$currency][CBO_CHARTCODE_INCENTIVE]['total'] = Cashledger::whereRaw('crccode = "'.$currency.'" AND chtcode = '.CBO_CHARTCODE_INCENTIVE.' AND created>="'.$fromtime.'" AND created<="'.$totime.'" AND (status ='.CBO_LEDGERSTATUS_CONFIRMED.' OR status='.CBO_LEDGERSTATUS_MANCONFIRMED.')')->count();
			$this->cashledgers[$currency][CBO_CHARTCODE_INCENTIVE]['amount'] = App::formatNegativeAmount(Cashledger::whereRaw('crccode = "'.$currency.'" AND chtcode = '.CBO_CHARTCODE_INCENTIVE.' AND created>="'.$fromtime.'" AND created<="'.$totime.'" AND (status ='.CBO_LEDGERSTATUS_CONFIRMED.' OR status='.CBO_LEDGERSTATUS_MANCONFIRMED.')')->sum('amount') * -1);
			$this->cashledgers[$currency][CBO_CHARTCODE_INCENTIVE]['fee'] = App::formatNegativeAmount(Cashledger::whereRaw('crccode = "'.$currency.'" AND chtcode = '.CBO_CHARTCODE_INCENTIVE.' AND created>="'.$fromtime.'" AND created<="'.$totime.'" AND (status ='.CBO_LEDGERSTATUS_CONFIRMED.' OR status='.CBO_LEDGERSTATUS_MANCONFIRMED.')')->sum('fee') * -1);
		}
		
		

		

		$bonus = array();
		$totalbonus = array('amount' => 0, 'apply' => 0, 'approved' => 0, 'continuedep' => 0);
		$totalcontinue = array();
		$totaldeposit = array();
		$start = '';
		$end = '';
		if($promos = Promocampaign::get()) {
			foreach($promos as $promo) {
					
				$apply = Promocash::whereRaw('pcpid='.$promo->id.' AND created >= "'.$fromtime.'" AND created <= "'.$totime.'"')->count();
				$approved = 0;
				$amount = 0;
				$continuedep = array();
				$deposit = array();
				if($ledgers = DB::select( DB::raw('SELECT id, accid, amount, created FROM cashledger WHERE chtcode = '.CBO_CHARTCODE_BONUS.' AND created>="'.$fromtime.'" AND created<="'.$totime.'" AND (status ='.CBO_LEDGERSTATUS_CONFIRMED.' OR status='.CBO_LEDGERSTATUS_MANCONFIRMED.') AND refid IN (SELECT id FROM promocash WHERE pcpid='.$promo->id.' ORDER BY created DESC ) ORDER BY created ASC  '))) {
					$max = count($ledgers); 
					$start = App::formatDateTime(3, $ledgers[0]->created);
					$end = App::formatDateTime(3, $ledgers[$max - 1]->created);
					foreach($ledgers as $ledger) {
						$approved += 1;
						$amount += $ledger->amount;
						$deposit[$ledger->accid] = 1;
						$totaldeposit[$ledger->accid] = 1;
						if($accountObj = Account::whereRaw('id='.$ledger->accid.' AND totaldeposit > 1')->get()) {
							$continuedep[$ledger->accid] = 1;
							$totalcontinue[$ledger->accid] = 1;
						}
					}
				}
	
				if($apply && $apply > 0) {
					$status = 0;
					if($promo->enddate > App::getDateTime(3))
					$status = 1;
					$bonus[$promo->id] = array(
						'name'		  => $promo->name.'<br />('.$start.' ~ '.$end.')',
						'currency'	  => $promo->crccode,
						'apply'		  => $apply,
						'approved'    => $approved,
						'amount'	  => App::formatNegativeAmount($amount),
						'continuedep' => count($continuedep).' / '.count($deposit),
						'status'	  => $status,
					);
				}
				$totalbonus['apply'] 	+= $apply;
				$totalbonus['approved'] += $approved;
				$totalbonus['amount']   += $amount;
			}
		}
		$totalbonus['continuedep'] = count($totalcontinue).' / '.count($totaldeposit);
		$totalbonus['amount'] 	   = App::formatNegativeAmount($totalbonus['amount']);
		
		
		$todayDateURL      = 'index.php?mdl='.$this->moduleName.'&action=show&aqrng[createdfrom]='.App::getDateTime(3).'&aqrng[createdto]='.App::getDateTime(3);
		$yesterdayDateURL  = 'index.php?mdl='.$this->moduleName.'&action=show&aqrng[createdfrom]='.App::formatDateTime(3, App::spanDateTime(-1 * CBO_DAY_SEC)).'&aqrng[createdto]='.App::formatDateTime(3, App::spanDateTime(-1 * CBO_DAY_SEC));
		$day7DateURL       = 'index.php?mdl='.$this->moduleName.'&action=show&aqrng[createdfrom]='.App::formatDateTime(3, App::spanDateTime(-7 * CBO_DAY_SEC)).'&aqrng[createdto]='.App::getDateTime(3);
		$monthDateURL      = 'index.php?mdl='.$this->moduleName.'&action=show&aqrng[createdfrom]='.App::formatDateTime(3, App::getDateTime(22)).'&aqrng[createdto]='.App::getDateTime(3);
		
		$today 		= array('from' => App::getDateTime(3), 'to' => App::getDateTime(3));
		$yesterday  = array('from' => App::formatDateTime(3, App::spanDateTime(-1 * CBO_DAY_SEC)), 'to' => App::formatDateTime(3, App::spanDateTime(-1 * CBO_DAY_SEC)));
		$sevenday   = array('from' => App::formatDateTime(3, App::spanDateTime(-7 * CBO_DAY_SEC)), 'to' => App::getDateTime(3));
		$month 		= array('from' => App::formatDateTime(3, App::getDateTime(22)), 'to' => App::getDateTime(3));
		

		$data['title'] 		 = Lang::get('COMMON.REPORTSIMPLE');
		$data['createdfrom'] = $fromdate;
		$data['createdto']   = $todate;
		$data['today']       = $today;
		$data['yesterday']   = $yesterday;
		$data['sevenday']    = $sevenday;
		$data['month']       = $month;

		$data['profitloss']  = $profitLoss;
		$data['pnltotal']    = $profitLossTotal;
		$data['newmembers']  = $this->newMembers;
		$data['products']    = $products;
		$data['totalprd']    = $this->totalprd;
		$data['cashledgers'] = $this->cashledgers;
		$data['bonuses']     = $bonus;
		$data['totalbonus']  = $totalbonus;
		
		if(Config::get('setting.front_path') == 'sbm')
		{
			$tmp = Cashbalance::wherePrdid(2)->sum('balance');
			$data['currentbalancetotal'] = $tmp;
			$data['m8b'] =  App::displayAmount($tmp);
			
			$tmp = Cashbalance::wherePrdid(3)->sum('balance');
			$data['currentbalancetotal'] += $tmp;
			$data['asc'] =  App::displayAmount($tmp);
			
			$condition = '((status in ('.CBO_LEDGERSTATUS_PENDING.', '.CBO_LEDGERSTATUS_MANPENDING.','.CBO_LEDGERSTATUS_CONFIRMED.', '.CBO_LEDGERSTATUS_MANCONFIRMED.', '.CBO_LEDGERSTATUS_PROCESSED.') AND chtcode IN ('.CBO_CHARTCODE_WITHDRAWAL.', '.CBO_CHARTCODE_BALANCETRANSFER.', '.CBO_CHARTCODE_WITHDRAWALCHARGES.' )) OR (status in ('.CBO_LEDGERSTATUS_CONFIRMED.', '.CBO_LEDGERSTATUS_MANCONFIRMED.') AND chtcode NOT IN ('.CBO_CHARTCODE_WITHDRAWAL.', '.CBO_CHARTCODE_BALANCETRANSFER.' , '.CBO_CHARTCODE_WITHDRAWALCHARGES.' )))';
			
			$tmp = Cashledger::whereRaw($condition)->sum('amount');
			$data['currentbalancetotal'] += $tmp;
			$data['mainwallet'] =  App::displayAmount($tmp);
			
			$data['currentbalancetotal'] =  App::displayAmount($data['currentbalancetotal']);
			
			
	
		}
		return view('admin.report_total',$data);
	}
	
	
}
