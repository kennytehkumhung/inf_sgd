<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller\Admin;
use App\libraries\grid\PanelGrid;
use App\libraries\App;
use App\Models\Config;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Auth;
use Validator;
use Session;
use Lang;
use App\Models\Article;
use App\Models\Language;
use App\Models\Media;
use App\Models\Configs;
use App\Models\Luckydrawtrans;
use App\Models\Luckydrawledger;
use App\Models\Account;
use App\Models\Luckydraw;
use Cache;
use Storage;
use File;

class Promo4dController extends Controller{ 
    
	protected $moduleName	= 'cms';
	protected $limit 		= 20;
	protected $start 		= 0;

	public function __construct()
	{
		
	}
	
	public function index(Request $request)
	{
		$grid = new PanelGrid;
		$grid->setupGrid($this->moduleName, '4dpromo-data', $this->limit);
		
		$grid->setTitle(Lang::get('COMMON.CMS'));
		
	    $grid->addColumn('id', 				'',									40,		array());
	    $grid->addColumn('name', 			Lang::get('COMMON.NAME'),			150,	array());
		$grid->addColumn('drawdate', 		'Draw Date',						90, 	array('align' => 'center'));
		$grid->addColumn('drawno', 			'Draw No',							90, 	array('align' => 'center'));
		$grid->addColumn('betno', 			'Bet No',							90, 	array('align' => 'center'));
		$grid->addColumn('status', 			'Status',							90, 	array('align' => 'center'));
		$grid->addColumn('prize', 			'Prize',							90, 	array('align' => 'center'));
		$grid->addColumn('date', 			'Date',								200, 	array('align' => 'center'));
		$grid->addButton('2', 'EXPORT' , '', 'button', array('icon' => 'ok', 'url' => 'export_4dlist();'));
		$data['grid'] = $grid->getTemplateVars();

		return view('admin.grid2',$data);
	}
	
	protected function doGetData(Request $request) {
		
		$condition = 1;
		$count = 1;
		
		$status[0] = 'pending'; 
		$status[1] = 'Settled'; 
		$total  = Luckydrawtrans::whereRaw($condition)->count();
	    $rows = array();
		if($luckydraws = Luckydrawtrans::whereRaw($condition)->skip($request->recordstartindex)->take( $request->pagesize )->orderBy('id','desc')->get()){
			foreach( $luckydraws as $luckydraw ){
				
				$rows[] = array(
					'id'  		=> $count++,
					'name'  	=> substr($luckydraw->acccode,3),
					'drawdate'  => substr(Luckydraw::whereRaw('id = '.$luckydraw->drawno.' ')->pluck('draw_date'), 0 , 10 ),
					'drawno' 	=> $luckydraw->drawno,
					'betno'  	=> $luckydraw->betno,
					'status' 	=> $status[$luckydraw->status],
					'prize'  	=> $luckydraw->prize,
					'date'   	=> $luckydraw->created_at->toDateTimeString(),
			
				);
			}
		}
		
		echo json_encode(array('total' => $total, 'rows' => $rows));
		exit;
	}
	
	public function promolist(Request $request) {
		
		if( $luckydraws = Luckydrawtrans::where( 'status' , '=' , 0  )->get() )
		{
			foreach( $luckydraws as $luckydraw )
			{
				echo $luckydraw->betno .'#1<br>';
			}
		}
		
	}
	
	public function ledger(Request $request)
	{
		$grid = new PanelGrid;
		$grid->setupGrid($this->moduleName, '4dpromolog-data', $this->limit);
		
		$grid->setTitle('Credit Log');
		
	    $grid->addColumn('name', 			Lang::get('COMMON.NAME'),			150,	array());
		$grid->addColumn('drawno', 			'Draw No',							90, 	array('align' => 'center'));
		$grid->addColumn('amount', 			'Amount',							90, 	array('align' => 'center'));
		$grid->addColumn('reference', 		'Reference',						90, 	array('align' => 'center'));
		$grid->addColumn('refid', 			'Refid',							90, 	array('align' => 'center'));
		$grid->addColumn('date', 			'Date',								200, 	array('align' => 'center'));

		$data['grid'] = $grid->getTemplateVars();

		return view('admin.grid2',$data);
	}
		
		
	public function doGetLedgerData(Request $request)
	{
		$condition = 1;

		$total  = Luckydrawledger::whereRaw($condition)->count();
	    $rows = array();
		if($luckydraws = Luckydrawledger::whereRaw($condition)->skip($request->recordstartindex)->take( $request->pagesize )->orderBy('id','desc')->orderBy('id','desc')->get()){
			foreach( $luckydraws as $luckydraw ){
			
				$rows[] = array(
					'name'  	 => substr($luckydraw->acccode,3),
					'drawno'	 => $luckydraw->draw_no,
					'amount'	 => $luckydraw->amount,
					'reference'  => $luckydraw->object,
					'refid'  	 => $luckydraw->refid,
					'date'   	 => $luckydraw->created_at->toDateTimeString(),
			
				);
			}
		}
		
		echo json_encode(array('total' => $total, 'rows' => $rows));
		exit;
	}
	
	public function Balance(Request $request)
	{
		$grid = new PanelGrid;
		$grid->setupGrid($this->moduleName, '4dpromobalance-data', $this->limit);
		
		$grid->setTitle('Credit Log');
		
	    $grid->addColumn('name', 			Lang::get('COMMON.NAME'),			150,	array());
		$grid->addColumn('balance', 		'Balance',							90, 	array('align' => 'right'));
		
		$grid->addButton('1', 'Add-Credit', 'Add Credit', 'button', array('icon' => 'add', 'url' => action('Admin\Promo4dController@drawAddEdit')));


		$data['grid'] = $grid->getTemplateVars();

		return view('admin.grid2',$data);
	}
		
		
	public function doGetBalanceData(Request $request)
	{
		$condition = 1;
		$luckydrawObj = new Luckydrawledger;
		
	    $rows = array();
		if($luckydraws = Luckydrawledger::whereRaw('draw_no = '.Configs::getParam('SYSTEM_4D_DRAWNO').' ')->skip($request->recordstartindex)->take( $request->pagesize )->groupBy('accid')->orderBy('id','desc')->get()){
			foreach( $luckydraws as $luckydraw ){
			
				$rows[] = array(
					'name'  	 => '<a href="#" onclick="parent.addTab(\'Member: '.substr($luckydraw->acccode,3).'\', \'customer-tab?sid='.$luckydraw->accid.'\')">'.substr($luckydraw->acccode,3).'</a>',
					'balance'	 => $luckydrawObj->getBalance( $luckydraw->accid, Configs::getParam('SYSTEM_4D_DRAWNO') ),

			
				);
			}
		}
		
		echo json_encode(array('total' => count($rows), 'rows' => $rows));
		exit;
	}
	
	protected function drawAddEdit(Request $request) { 

	    $toAdd 	  = true;
		$object   = null;
		$readOnly = false;

		$title = 'Add Credit';
	
		

		$form = App::setupPanelForm('doadd', $request->all(), array(), $toAdd);
		$form->setFormTitle($title);
		
		$form->addInput('text', 	Lang::get('COMMON.NAME') 			, 'name', 		 	 '', 					   array(), 	 true);
		$form->addInput('text', 	Lang::get('COMMON.CREDIT') 			, 'credit', 		 '', 					   array(), 	 true);
		
 
		$data['form'] = $form->getTemplateVars();
		$data['module'] = '4dpromo';

		return view('admin.form2',$data);
	
	}
	
	protected function doAddEdit(Request $request) {
		
		$validator = Validator::make(
			[
	
				'name'	  	  => $request->input('name'),
				'credit'	  => $request->input('credit'),
			],
			[

			   'name'	   	   => 'required',
			   'credit'	  	   => 'required',
			]
		);
		
		
		if ($validator->fails())
		{
			echo json_encode($validator->errors());
			exit;
		}

		if( $accObj = Account::where( 'nickname' , '=' , $request->input('name') )->first() ){
			$object = new Luckydrawledger;
			$object->draw_no = Configs::getParam('SYSTEM_4D_DRAWNO');
			$object->accid   = $accObj->id;
			$object->acccode = $accObj->code;
			$object->amount  = $request->input('credit');
			$object->refid   = '';
			$object->object  = 'Manually';
			$object->save();

			
			if( $object->save() )
			{
				
				
					
				echo json_encode( array( 'code' => Lang::get('COMMON.SUCESSFUL') ) );
				exit;
			}
		}

		echo json_encode( array( 'code' => Lang::get('COMMON.FAILED') ) );
		
	}

    public function showConfig(Request $request)
    {
        $date = Configs::getParam('SYSTEM_PROMO4D_WINNER_LIST_DATE');
        $winnerList = array(
            'P2500' => Configs::getParam('SYSTEM_PROMO4D_WINNER_LIST_A'),
            'P1000' => Configs::getParam('SYSTEM_PROMO4D_WINNER_LIST_B'),
            'P500' => Configs::getParam('SYSTEM_PROMO4D_WINNER_LIST_C'),
            'P200' => Configs::getParam('SYSTEM_PROMO4D_WINNER_LIST_D'),
            'P60' => Configs::getParam('SYSTEM_PROMO4D_WINNER_LIST_E'),
        );

        $specialPrizeKeys = array('P200', 'P60');

        foreach ($specialPrizeKeys as $key) {
            $arr = explode(',', $winnerList[$key]);
            $count = count($arr);

            for ($i = $count; $i < 10; $i++) {
                $arr[] = '';
            }

            $winnerList[$key] = $arr;
        }

        return view('admin.promo4dconfig', compact('date', 'winnerList'));
    }

    public function doEditConfig(Request $request)
    {
        $date = $request->input('date', '');

        if ($date != '') {
            try {
                $date = Carbon::createFromFormat('Y-m-d', $date)->toDateString();
            } catch (\Exception $e) {
                return response(json_encode('Invalid date format, must be YYYY-DD-MM: ' . Carbon::now()->toDateString()));
            }
        }

        Configs::updateParam('SYSTEM_PROMO4D_WINNER_LIST_DATE', $date);
        Configs::updateParam('SYSTEM_PROMO4D_WINNER_LIST_A', $request->input('P2500', ''));
        Configs::updateParam('SYSTEM_PROMO4D_WINNER_LIST_B', $request->input('P1000', ''));
        Configs::updateParam('SYSTEM_PROMO4D_WINNER_LIST_C', $request->input('P500', ''));

        $specialPrizeKeys = array(
            'P200' => 'SYSTEM_PROMO4D_WINNER_LIST_D',
            'P60' => 'SYSTEM_PROMO4D_WINNER_LIST_E',
        );

        foreach ($specialPrizeKeys as $key => $val) {
            $names = preg_split('/,/', $request->input($key, ''), -1, PREG_SPLIT_NO_EMPTY);

            Configs::updateParam($val, implode(',', $names));
        }

        return response(json_encode('Config saved.'));
    }
	
	
}
