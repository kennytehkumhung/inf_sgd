<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller\Admin;
use App\libraries\grid\PanelGrid;
use App\libraries\App;
use Illuminate\Http\Request;
use Auth;
use Validator;
use Session;
use Lang;
use App\Models\Iptracker;
use App\Models\Currency;
use App\Models\Fundingmethod;
use App\Models\Cashledger;
use App\Models\Bank;
use App\Models\Accounttag;
use App\Models\Tag;
use App\Models\Account;
use App\Models\Agent;
use App\Models\Cashledgerlog;
use App\Models\Bankaccount;
use App\Models\Profitloss;
use App\Models\Promocash;
use App\Models\Rejectreason;
use App\Models\Banktransfer;
use App\Models\Promocampaign;
use App\Models\Remark;
use App\Models\Incentivesetting;
use App\Models\Incentive;
use App\Models\Product;
use App\Models\Dailyrebate;
use Config;
use DB;

class IncentiveController extends Controller{


	protected $moduleName 	= 'incentive';
	protected $limit 		= 20;
	protected $start 		= 0;
	
	public function __construct()
	{
		
	}
	
	public function index(Request $request){

			$grid = new PanelGrid;
			$grid->setupGrid($this->moduleName, 'incentive-data', $this->limit);
			$grid->setTitle(Lang::get('COMMON.INCENTIVE'));
			
			$grid->addColumn('id', 				Lang::get('COMMON.ID'), 				0,			array('align' => 'left'));			
			$grid->addColumn('products',		Lang::get('COMMON.PRODUCT'), 			100,		array('align' => 'left'));
			$grid->addColumn('currency',		Lang::get('COMMON.CURRENCY'), 			68,			array('align' => 'center'));
			$grid->addColumn('from', 			Lang::get('COMMON.FROM'), 				120,		array('align' => 'left'));
			$grid->addColumn('to', 				Lang::get('COMMON.END'), 				120,		array('align' => 'left'));
			$grid->addColumn('percentage', 		Lang::get('COMMON.PERCENTAGE'), 		85,			array('align' => 'center'));
			$grid->addColumn('maxpayout', 		Lang::get('COMMON.MAXPAYOUT'), 			120,		array('align' => 'left'));
			$grid->addColumn('minpayout', 		Lang::get('COMMON.MINPAYOUT'), 			120,		array('align' => 'left'));

			$grid->addButton('1', Lang::get('COMMON.ADDNEW'), Lang::get('COMMON.ADDNEW').' '.Lang::get('COMMON.INCENTIVE'), 'button', array('icon' => 'add', 'url' => action('Admin\IncentiveController@drawAddEdit')));


			$data['grid'] = $grid->getTemplateVars();

			return view('admin.grid2',$data);
    }
	
	protected function doGetData(Request $request){
		
		$rows = array();
		
		$condition = '1';
		$condition .= Session::get('admin_crccode') == 'MYR' ? '' : ' AND crccode = "'.Session::get('admin_crccode').'"';
		
		$total = Incentivesetting::whereRaw($condition)->count();
		$products  = $this->getProductCategoryOptions();
		
		//new structure
		if($incentives = Incentivesetting::whereRaw($condition)->skip($request->recordstartindex)->take( $request->pagesize )->orderBy('id','desc')->get()){
			foreach( $incentives as $incentive ){

                $product = '';

				if ($incentive->prdid > 0) {
                    $product = $products['prdid_'.$incentive->prdid];
				} elseif ($incentive->product > 0) {
                    $product = $products[$incentive->product];
				}
		
				$body = App::formatAddTabUrl( Lang::get('INCENTIVE').': '.$incentive->id, urlencode($product), action('Admin\IncentiveController@drawAddEdit',  array('sid' => $incentive->id)));

				if($incentive->status == CBO_STANDARDSTATUS_SUSPENDED){
					$product = urldecode($body);
				}
			
				$rows[] = array(
					'id'  					=> $incentive->id,
					'products'  			=> $product,
					'currency'  			=> $incentive->crccode,
					'from'  				=> $incentive->datefrom,
					'to'  					=> $incentive->dateto,
					'percentage'  			=> $incentive->rate.'%',
					'maxpayout'  			=> $incentive->maxamount,
					'minpayout'  			=> $incentive->minpayout,
				);	
				
			}
				
		}
				
		echo json_encode(array('total' => $total, 'rows' => $rows));
		exit;
	
	}
	
	protected function drawAddEdit(Request $request) { 

	    $toAdd 	  = true;
		$object   = null;
		$readOnly = false;
		$values   = array();

		$title = Lang::get('COMMON.NEW').' '.Lang::get('COMMON.INCENTIVE');

		if($request->has('sid')) 
		{
	
			if ($object = Incentivesetting::find($request->input('sid'))) 
				{
					$title    = Lang::get('COMMON.EDIT').' '.Lang::get('COMMON.INCENTIVE');
					$toAdd    = false;
					$readOnly = true;

				} 
			else
				{
					$object = null;
				}
			
		}
		
		$products  = $this->getProductCategoryOptions();
		$currency  = Currency::getAllCurrencyAsOptions();
		
		$form = App::setupPanelForm('doadd', $request->all(), array(), $toAdd);
		$form->setFormTitle($title);

		$form->addInput('select', 		Lang::get('COMMON.PRODUCT')     , 'product',		(($object)? $object->product:''), 		array('options' => $products),	 true);
		$form->addInput('select', 		Lang::get('COMMON.CURRENCY')    , 'crccode',		(($object)? $object->crccode:''), 		array('options' => $currency),	 true);
		$form->addInput('date', 		Lang::get('COMMON.FROM')		, 'from',			(($object)? $object->datefrom:''), 		array(), false); 
		$form->addInput('date', 		Lang::get('COMMON.TO')			, 'to',				(($object)? $object->dateto:''), 		array(), false); 
		$form->addInput('text', 		Lang::get('COMMON.PERCENTAGE') 	, 'percentage', 	(($object)? $object->rate:''), 			array(), false);
		$form->addInput('text', 		Lang::get('COMMON.MINPAYOUT') 	, 'minpayout', 		(($object)? $object->minpayout:''), 	array(), false);
		$form->addInput('text', 		Lang::get('COMMON.MAXPAYOUT') 	, 'maxpayout', 		(($object)? $object->maxamount:''), 	array(), false);

		if (Config::get('setting.opcode') == 'GSC') {
            $form->addInput('checkbox_single', Lang::get('COMMON.AUTO'), 'auto', (($object) ? $object->auto : ''), array(), false);
        }
	
		$data['form'] = $form->getTemplateVars();
		$data['module'] = 'incentive';

		return view('admin.form2',$data);
	
	}
	
	protected function doAddEdit(Request $request) {
		
		$validator = Validator::make(
			[
	
				'from'	  	  		 => $request->input('from'),
				'to'	  	 		 => $request->input('to'),
				'percentage'	  	 => $request->input('percentage'),
				'minpayout'	  	 	 => $request->input('minpayout'),
				'maxpayout'	  	 	 => $request->input('maxpayout'),
				'crccode'	  	 	 => $request->input('crccode'),
				'product'	  	 	 => $request->input('product'),

			],
			[

			   'from'	   	   	   => 'required',
			   'to'	  		 	   => 'required',
			   'percentage'	  	   => 'required',
			   'minpayout'	  	   => 'required',
			   'maxpayout'	  	   => 'required',
			   'crccode'	  	   => 'required',
			   'product'	  	   => 'required',

			]
		);
		
		
		if ($validator->fails())
		{
			echo json_encode($validator->errors());
			exit;
		}
		
		
		
		$object  = new Incentivesetting;
		
		if($request->has('sid')) 
		{
			$object = Incentivesetting::find($request->input('sid'));
		}
		
		$object->datefrom	 		= $request->input('from');
		$object->dateto	 			= $request->input('to');
		$object->rate	 			= $request->input('percentage');
		$object->minpayout	 		= $request->input('minpayout');
		$object->maxamount	 		= $request->input('maxpayout');
		$object->crccode	 		= $request->input('crccode');
		$object->auto	 			= $request->input('auto', 0);

		$prdInput = $request->input('product');

		if (starts_with($prdInput, 'prdid_')) {
            $object->product = 0;
            $object->prdid = str_replace('prdid_', '', $prdInput);
		} else {
            $object->product = $prdInput;
            $object->prdid = 0;
		}

		
		if( $object->save() )
		{
			echo json_encode( array( 'code' => Lang::get('COMMON.SUCESSFUL') ) );
			exit;
		}

		echo json_encode( array( 'code' => Lang::get('COMMON.FAILED') ) );
		
	}
	
	public function showrebateReport(Request $request){

			$grid = new PanelGrid;
			
			if( $request->has('aflid') )
			{
				$grid->setupGrid($this->moduleName, 'rebateReport-data', $this->limit, true, array('params' => array('aflid' => $request->aflid)));
			}
			else
			{
				$grid->setupGrid($this->moduleName, 'rebateReport-data', $this->limit);
			}
			$grid->setTitle(Lang::get('COMMON.REBATE'));
				
			$grid->addColumn('from', 				Lang::get('COMMON.FROM'), 				100,		array('align' => 'center'));	
			$grid->addColumn('to', 					Lang::get('COMMON.TO'), 				100,		array('align' => 'center'));	
			//$grid->addColumn('product', 			Lang::get('COMMON.PRODUCT'), 			100,		array());	
			$grid->addColumn('currency', 			Lang::get('COMMON.CURRENCY'), 			100,		array('align' => 'center'));	
			$grid->addColumn('rate', 				Lang::get('COMMON.RATE'), 				100,		array('align' => 'right'));	
			$grid->addColumn('member', 				Lang::get('COMMON.MEMBER'), 			100,		array('align' => 'right'));	
			$grid->addColumn('stake', 				Lang::get('COMMON.VALIDSTAKE'), 		130,		array('align' => 'right'));	
			$grid->addColumn('incentive', 			Lang::get('COMMON.INCENTIVE'), 			100,		array('align' => 'right'));	
			$grid->addColumn('effective', 			Lang::get('COMMON.EFFECTIVE'), 			100,		array('align' => 'right'));	
			$grid->addColumn('status', 				Lang::get('COMMON.STATUS'), 			100,		array('align' => 'center'));
			$grid->addColumn('action', 				'', 									100,		array('align' => 'center'));	

			
			$grid->addFilter('status',array('Active','Suspended'), array('display' => Lang::get('COMMON.TYPE')));
			$grid->addFilter('icategory', Product::getCategoryOptions(), array('display' => Lang::get('COMMON.PRODUCT')));

			$data['grid'] = $grid->getTemplateVars();

			return view('admin.grid2',$data);
    }
	
	protected function showrebateReportdoGetData(Request $request){

		
		//$object = new IncentiveSettingObject;
		//$total = $object->getCount($condition);
		$aflt   = $request->input('aqfld');
		$rows = array();
		$footer = array();
		$total_incentives = array();
		
		$condition = Session::get('admin_crccode') == 'MYR' ? 'dateto <= "'.App::getDateTime(3).'"' : 'crccode = "'.Session::get('admin_crccode').'" AND dateto <= "'.App::getDateTime(3).'"';
//		$products = $this->getProductCategoryOptions();
		if(!empty($aflt['icategory'])) {
			$condition .= ' AND product=' . $aflt['icategory'];
		}
		$total = Incentivesetting::whereRaw($condition)->count();
		
		if($incentives = Incentivesetting::whereRaw($condition)->skip($request->recordstartindex)->take($request->recordendindex)->orderBy('id','desc')->get()){

			 foreach($incentives as $incentive){
			 	$condition = 'istest = 0 AND crccode = "'.$incentive->crccode.'" AND date >= "'.$incentive->datefrom.'" AND date <= "'.$incentive->dateto.'" ';

			 	if ($incentive->prdid > 0) {
                    $condition .= 'AND prdid = '.$incentive->prdid.' ';
				} else {
                    $condition .= 'AND category = '.$incentive->product.' ';
				}
				
				if( $request->has('aflid') ) {
					$condition .= ' AND level3 = '.Agent::whereAflid($request->aflid)->pluck('id').' ';
				}

				$member = Profitloss::whereRaw($condition)->count();
				$stake = Profitloss::whereRaw($condition)->sum('validstake');

				$amount = ' - ';
				$effective = ' - ';
				
				if($incentive->status == CBO_STANDARDSTATUS_SUSPENDED )
				{
					$action = '<a href="#" onclick="doConfirmAction(\'incentive-docalculate\', \'incentive-docalculate\', '.$incentive->id.')">'.Lang::get('COMMON.CALCULATEINCENTIVE').'</a>';
					
					if($request->has('aflid'))
						$action = '';
				}
	
				else {		
					if($request->has('aflid')){
						$action = '<a href="#" onclick="parent.addTab(\''.Lang::get('INCENTIVE').': '.$incentive->id.'\', \'incentivelist?id='.$incentive->id.'&aflid='.$request->aflid.'\')">'.Lang::get('COMMON.VIEW').'</a>';		
						$amount = App::displayAmount(Incentive::whereRaw('insid='.$incentive->id.' AND amount >= "'.$incentive->minpayout.'" AND accid IN ( select id from account where aflid = '.$request->aflid.' )')->sum('amount'));
					}else{
						$action = '<a href="#" onclick="parent.addTab(\''.Lang::get('INCENTIVE').': '.$incentive->id.'\', \'incentivelist?id='.$incentive->id.'\')">'.Lang::get('COMMON.VIEW').'</a>';		
						$amount = App::displayAmount(Incentive::whereRaw('insid='.$incentive->id.' AND amount >= "'.$incentive->minpayout.'"')->sum('amount'));
					}
					
					
					
						
					
					$effective = 0;
					if($stake > 0)
					{
						$effective = $amount / $stake * 100;
					}
					$effective = App::displayPercentage($effective);
				}
				
				$stake = ($incentive->crccode == 'VND' || $incentive->crccode == 'IDR') ? $stake * 1000 : $stake;
				
				$rows[] = array(
					'id' => $incentive->id,
					'from' => $incentive->datefrom,
					'to' => $incentive->dateto,
					'currency' => $incentive->crccode,
					//'product' => $products[$incentive->product],
					'rate' => App::displayPercentage($incentive->rate),
					'member' => App::displayAmount($member, 0),
					'stake' => App::displayAmount($stake),
					'incentive' => $amount,
					'effective' => $effective,
					'action' => $action,
					'status' => $incentive->getStatusText(),
				);

			}
			 $footer[] = array('status'=> Lang::get('COMMON.SUBTOTAL').'<br>'.Lang::get('COMMON.TOTAL') ,'credit'=> App::displayAmount(0).'<br>'.App::displayAmount(0),'debit'=> App::displayAmount(0).'<br>'.App::displayAmount(0),'fee'=> App::displayAmount(0).'<br>'.App::displayAmount(0));
			
		}
		//var_dump($stake);
		//	exit();
		
		// echo json_encode(array('total' => $total, 'rows' => $rows));
		echo json_encode(array('total' => $total, 'rows' => $rows, 'footer' => $footer));
		exit;
		
	}
	public function showdailyrebateReport(Request $request){

			$grid = new PanelGrid;
			
			if( $request->has('aflid') )
			{
				$grid->setupGrid($this->moduleName, 'dailyrebateReport-data', $this->limit, true, array('params' => array('aflid' => $request->aflid)));
			}
			else
			{
				$grid->setupGrid($this->moduleName, 'dailyrebateReport-data', $this->limit);
			}
			$grid->setTitle(Lang::get('COMMON.REBATE'));
				
			// $grid->addColumn('from', 				Lang::get('COMMON.DATE'), 				100,		array('align' => 'center'));	
			// $grid->addColumn('to', 					Lang::get('COMMON.TO'), 				100,		array('align' => 'center'));	
			//$grid->addColumn('product', 			Lang::get('COMMON.PRODUCT'), 			100,		array());	
			// $grid->addColumn('currency', 			Lang::get('COMMON.CURRENCY'), 			100,		array('align' => 'center'));	
			// $grid->addColumn('rate', 				Lang::get('COMMON.RATE'), 				100,		array('align' => 'right'));	
			// $grid->addColumn('member', 				Lang::get('COMMON.MEMBER'), 			100,		array('align' => 'right'));	
			// $grid->addColumn('stake', 				Lang::get('COMMON.VALIDSTAKE'), 		130,		array('align' => 'right'));	
			// $grid->addColumn('incentive', 			Lang::get('COMMON.INCENTIVE'), 			100,		array('align' => 'right'));	
			// $grid->addColumn('effective', 			Lang::get('COMMON.EFFECTIVE'), 			100,		array('align' => 'right'));	
			$grid->addColumn('date', 					Lang::get('COMMON.DATE'), 				100,		array('align' => 'center'));
			// $grid->addColumn('nickname', 					Lang::get('COMMON.USERNAME'), 				100,		array('align' => 'center'));
			// $grid->addColumn('totalstake', 				Lang::get('COMMON.TURNOVER'), 			100,		array('align' => 'center'));
			$grid->addColumn('member', 					Lang::get('COMMON.MEMBER'), 			100,		array('align' => 'center'));
			$grid->addColumn('currency', 				Lang::get('COMMON.CURRENCY'), 			100,		array('align' => 'center'));
			$grid->addColumn('amount', 					Lang::get('COMMON.AMOUNT'), 			100,		array('align' => 'center'));
			$grid->addColumn('action', 					Lang::get('COMMON.ACTION'), 			100,		array('align' => 'center'));
			// $grid->addColumn('amount', 					Lang::get('COMMON.AMOUNT'), 			100,		array('align' => 'center'));
			// $grid->addColumn('status', 				Lang::get('COMMON.STATUS'),									100,		array('align' => 'center'));	
			// $grid->addColumn('taken_bonus', 	Lang::get('COMMON.TAKENBONUS'), 		150,	array('align' => 'right'));
			// $grid->addColumn('taken_bonus_type', 	Lang::get('COMMON.TAKENBONUS').' Type', 		200,	array('align' => 'left'));	

			
			// $grid->addFilter('status',array('Active','Suspended'), array('display' => Lang::get('COMMON.TYPE')));
			// $grid->addFilter('icategory', Product::getCategoryOptions(), array('display' => Lang::get('COMMON.PRODUCT')));

			$data['grid'] = $grid->getTemplateVars();

			return view('admin.grid2',$data);
    }
	
	protected function showdailyrebateReportdoGetData(Request $request){

		
		//$object = new IncentiveSettingObject;
		//$total = $object->getCount($condition);
		$aflt   = $request->input('aqfld');
		$rows = array();
		$footer = array();
		$total_incentives = array();
		
		// $condition = Session::get('admin_crccode') == 'MYR' ? : 'crccode = "'.Session::get('admin_crccode').'"';
		$condition = Session::get('admin_crccode') == 'MYR' ? 'date <= "'.App::getDateTime(3).'"' : 'crccode = "'.Session::get('admin_crccode').'" AND date <= "'.App::getDateTime(3).'"';
		$total = Dailyrebate::whereRaw($condition)->DISTINCT('date')->count('date');

		if($Dailyrebatedatas = Dailyrebate::whereRaw($condition)->skip($request->recordstartindex)->take($request->recordendindex)->orderBy('id','desc')->groupBy('date')->selectRaw('*, sum(amount) as totalamount')->get()){

			 foreach($Dailyrebatedatas as $Dailyrebatedata){
				$condition = 'crccode = "'.$Dailyrebatedata->crccode.'" AND date >= "'.$Dailyrebatedata->date.'" AND date <= "'.$Dailyrebatedata->date.'" ';
				$member = Dailyrebate::whereRaw($condition)->count();
				
				if($Dailyrebatedata->status == CBO_STANDARDSTATUS_SUSPENDED )
				{
					$action = '<a href="#" onclick="doConfirmAction(\'incentive-docalculate\', \'incentive-docalculate\', '.$Dailyrebatedata->id.')">'.Lang::get('COMMON.CALCULATE').'</a>';
					
					if($request->has('aflid'))
						$action = '';
				}
	
				else {		
					if($request->has('aflid')){
						$action = '<a href="#" onclick="parent.addTab(\''.Lang::get('INCENTIVE').': '.$Dailyrebatedata->id.'\', \'incentivelist?id='.$Dailyrebatedata->id.'&aflid='.$request->aflid.'\')">'.Lang::get('COMMON.VIEW').'</a>';		
					}else{
						$action = '<a href="#" onclick="parent.addTab(\''.Lang::get('INCENTIVE').': '.$Dailyrebatedata->id.'\', \'incentivelist?id='.$Dailyrebatedata->id.'\')">'.Lang::get('COMMON.VIEW').'</a>';		
					}
				}
				$action = '<a href="#" onclick="parent.addTab(\''.Lang::get('DAILYREBATE').': '.$Dailyrebatedata->date.'\', \'dailyrebatelist?date='.$Dailyrebatedata->date.'\')">'.Lang::get('COMMON.VIEW').'</a>';
				
				$bonuses = '';
				if( $promocashs = DB::select( DB::raw('select n.method as method from cashledger c,promocash p,promocampaign n where c.chtcode = 3 and c.refid = p.id and p.pcpid = n.id and c.status IN(1,4) and c.created >= "'.$Dailyrebatedata->date.'" AND c.created <= "'.$Dailyrebatedata->date.'" and c.accid = '.$Dailyrebatedata->accid.' group by n.method ')) ){

					foreach( $promocashs as $promocash ){
						if( $promocash->method != 0 )
                            $bonuses .= $promocampaign[$promocash->method].',';
					}

				}
				
				$rows[] = array(
					'date' => $Dailyrebatedata->date,
					'currency' => $Dailyrebatedata->crccode,
					'nickname' => $Dailyrebatedata->nickname,
					'member' => $member,
					'amount' => $Dailyrebatedata->totalamount,
					// 'status' => $Dailyrebatedata->getStatusText(),
					// 'taken_bonus' 	=> Cashledger::whereRaw(' accid = '.$Dailyrebatedata->accid.' AND chtcode = 3 AND status IN(1,4) AND created >= "'.$Dailyrebatedata->date.' 00:00:00" AND created <= "'.$Dailyrebatedata->date.' 23:59:59" ')->count(),
					// 'taken_bonus_type' 	=> $bonuses,
					'action' => $action,
				);

			}
			
		}
		//var_dump($stake);
		//	exit();
		
		echo json_encode(array('total' => $total, 'rows' => $rows));
		// echo json_encode(array('total' => $total, 'rows' => $rows, 'footer' => $footer));
		exit;
		
	}
	public function showdailyrebatelist(Request $request){

			$grid = new PanelGrid;
			
			if( $request->has('aflid') )
			{
				$grid->setupGrid($this->moduleName, 'dailyrebate-data', $this->limit, false,array('params' => array('date' => $request->date,'aflid' => $request->aflid)));
			}
			else
			{
				$grid->setupGrid($this->moduleName, 'dailyrebate-data', $this->limit, false,array('params' => array('date' => $request->date),'checkbox'=>true,'checkbox_url'=>'dailyrebate-batchapprove'));
			}
			
			$grid->setTitle(Lang::get('COMMON.INCENTIVE'));
			
			$grid->addColumn('checkbox', 		'', 									18,		array('checkbox' => true));
			$grid->addColumn('id',				Lang::get('COMMON.ID'), 				50,		array('align' => 'right'));
			$grid->addColumn('nickname', 		Lang::get('COMMON.USERNAME'), 			80,		array());
			// $grid->addColumn('product', 		Lang::get('COMMON.PRODUCT'), 			100,	array());
			$grid->addColumn('amount', 			Lang::get('COMMON.AMOUNT'), 				100,	array('align' => 'right'));
			$grid->addColumn('currency', 		Lang::get('COMMON.CURRENCY'), 			60,		array('align' => 'center'));
			$grid->addColumn('turnover', 		Lang::get('COMMON.TURNOVER'), 			150,	array('align' => 'right'));
			$grid->addColumn('status', 			Lang::get('COMMON.STATUS'), 			150,	array('align' => 'right'));
			$grid->addColumn('taken_bonus', 	Lang::get('COMMON.TAKENBONUS'), 		150,	array('align' => 'right'));
			$grid->addColumn('taken_bonus_type', 	Lang::get('COMMON.TAKENBONUS').' Type', 		200,	array('align' => 'left'));
			
			if( !$request->has('aflid') )
			{
				$grid->addButton('2', Lang::get('COMMON.APPROVE'), '', 'button', array('icon' => 'ok', 'url' => 'approve_batch(\'approve\');'));
			}

			$data['grid'] = $grid->getTemplateVars();

			return view('admin.grid2',$data);
    }	
	

	protected function doGetDailylistData(Request $request){
		
		
		
		//$object = new IncentiveSettingObject;
		//$total = $object->getCount($condition);
		$aflt   = $request->input('aqfld');
		$date   = $request->date;
		$rows = array();
		$footer = array();
		$total_incentives = array();
		
		$condition = Session::get('admin_crccode') == 'MYR' ? 'date = "'.$date.'"' : 'crccode = "'.Session::get('admin_crccode').'" AND date = "'.$date.'"';
		
		$total = Dailyrebate::whereRaw($condition)->count();

		if($Dailyrebatedatas = Dailyrebate::whereRaw($condition)->skip($request->recordstartindex)->take($request->recordendindex)->orderBy('id','desc')->get()){

			 foreach($Dailyrebatedatas as $Dailyrebatedata){
				$condition = 'crccode = "'.$Dailyrebatedata->crccode.'" AND date >= "'.$Dailyrebatedata->date.'" AND date <= "'.$Dailyrebatedata->date.'" ';
				$member = Dailyrebate::whereRaw($condition)->count();
				
				if($Dailyrebatedata->status == CBO_STANDARDSTATUS_SUSPENDED )
				{
					$action = '<a href="#" onclick="doConfirmAction(\'incentive-docalculate\', \'incentive-docalculate\', '.$Dailyrebatedata->id.')">'.Lang::get('COMMON.CALCULATE').'</a>';
					
					if($request->has('aflid'))
						$action = '';
				}
	
				else {		
					if($request->has('aflid')){
						$action = '<a href="#" onclick="parent.addTab(\''.Lang::get('INCENTIVE').': '.$Dailyrebatedata->id.'\', \'incentivelist?id='.$Dailyrebatedata->id.'&aflid='.$request->aflid.'\')">'.Lang::get('COMMON.VIEW').'</a>';		
					}else{
						$action = '<a href="#" onclick="parent.addTab(\''.Lang::get('INCENTIVE').': '.$Dailyrebatedata->id.'\', \'incentivelist?id='.$Dailyrebatedata->id.'\')">'.Lang::get('COMMON.VIEW').'</a>';		
					}
				}
				
				$bonuses = '';
				if( $promocashs = DB::select( DB::raw('select n.method as method from cashledger c,promocash p,promocampaign n where c.chtcode = 3 and c.refid = p.id and p.pcpid = n.id and c.status IN(1,4) and c.created >= "'.$Dailyrebatedata->date.'" AND c.created <= "'.$Dailyrebatedata->date.'" and c.accid = '.$Dailyrebatedata->accid.' group by n.method ')) ){

					foreach( $promocashs as $promocash ){
						if( $promocash->method != 0 )
                            $bonuses .= $promocampaign[$promocash->method].',';
					}

				}
				
				$rows[] = array(
					'date' => $Dailyrebatedata->date,
					'currency' => $Dailyrebatedata->crccode,
					'nickname' => $Dailyrebatedata->nickname,
					'id' => $Dailyrebatedata->id,
					'turnover' => $Dailyrebatedata->totalstake,
					'amount' => $Dailyrebatedata->amount,
					'status' => $Dailyrebatedata->getStatusText(),
					'taken_bonus' 	=> Cashledger::whereRaw(' accid = '.$Dailyrebatedata->accid.' AND chtcode = 3 AND status IN(1,4) AND created >= "'.$Dailyrebatedata->date.' 00:00:00" AND created <= "'.$Dailyrebatedata->date.' 23:59:59" ')->count(),
					'taken_bonus_type' 	=> $bonuses,
				);

			}
			
		}
		//var_dump($stake);
		//	exit();
		
		echo json_encode(array('total' => $total, 'rows' => $rows));
		exit;
	
	}
	
	protected function doCalculateIncentive(Request $request) {
		
		$result = false;
		if($request->has('id')) 
		{
			if ($object = Incentivesetting::whereRaw('id='.$request->id.' AND status='.CBO_STANDARDSTATUS_SUSPENDED)->first()) 
			{
			
				$stake 		= array();
				$stakelocal = array();
				$currency 	= array();
				$accounts 	= array();
				$condition 	= 'istest = 0 AND date >="'.$object->datefrom.'" AND date <="'.$object->dateto.'" AND crccode = "'.$object->crccode.'" ';

				if ($object->prdid > 0) {
                    $condition .= 'AND prdid = '.$object->prdid;
				} else {
					$condition .= 'AND category = '.$object->product.' ';
				}
			
				if($profits = Profitloss::whereRaw($condition)->get()) {
					foreach($profits as $profit) {
                                            $rate = $object->rate;

                                            if(!isset($currency[$profit->accid])) $currency[$profit->accid] = $profit->crccode;
                                            if(!isset($accounts[$profit->accid])) $accounts[$profit->accid] = $profit->acccode;
                                            if(isset($stake[$profit->accid])) {
                                                        $stake[$profit->accid] += ((float) $profit->validstake) * $rate / 100;
							$stakelocal[$profit->accid] += (float) $profit->validstakelocal;
                                            } else {
                                                        $stake[$profit->accid] = ((float) $profit->validstake) * $rate / 100;
							$stakelocal[$profit->accid] = (float) $profit->validstakelocal;
                                            }
					}
				}
				
				foreach($stake as $accid => $turnover) {
//					$amount = App::float($turnover * $object->rate / 100);
					$amount = App::float($turnover);
					if($object->maxamount > 0 && $amount > $object->maxamount)
					$amount = $object->maxamount;
					
					$incObj = new Incentive;
					$incObj->insid 				= $object->id;
					$incObj->product 			= $object->product;
					$incObj->prdid				= $object->prdid;
					$incObj->accid 				= $accid;
					$incObj->acccode 			= $accounts[$accid];
					$incObj->crccode 			= $currency[$accid];
					$incObj->crcrate 			= Currency::getCurrencyRate($incObj->crccode);
					$incObj->datefrom 			= $object->datefrom;
					$incObj->dateto 			= $object->dateto;
					$incObj->rate 				= $object->rate;
					$incObj->totalstake 		= ($incObj->crccode == 'VND' || $incObj->crccode == 'IDR') ? $turnover * 1000 : $turnover;
					$incObj->totalstakelocal 	= ($incObj->crccode == 'VND' || $incObj->crccode == 'IDR') ? $stakelocal[$accid] * 1000 : $stakelocal[$accid];
					$incObj->amount 			= ($incObj->crccode == 'VND' || $incObj->crccode == 'IDR') ? $amount * 1000 : $amount;
					$incObj->amountlocal 		= Currency::getLocalAmount($incObj->amount, $incObj->crccode);
					$incObj->save();
				}
				$object->status = CBO_STANDARDSTATUS_ACTIVE;
				$object->save();
				
			}
			
		}
		echo json_encode(array('success' => $result));
		

	}
	
	public function showincentive(Request $request){

			$grid = new PanelGrid;
			
			if( $request->has('aflid') )
			{
				$grid->setupGrid($this->moduleName, 'incentivelist-data', $this->limit, false,array('params' => array('id' => $request->id,'aflid' => $request->aflid)));
			}
			else
			{
				$grid->setupGrid($this->moduleName, 'incentivelist-data', $this->limit, false,array('params' => array('id' => $request->id),'checkbox'=>true,'checkbox_url'=>'incentivelist-batchapprove'));
			}
			
			$grid->setTitle(Lang::get('COMMON.INCENTIVE'));
			
			$grid->addColumn('checkbox', 		'', 									18,		array('checkbox' => true));
			$grid->addColumn('id',				Lang::get('COMMON.ID'), 				50,		array('align' => 'right'));
			$grid->addColumn('acccode', 		Lang::get('COMMON.ACCOUNT'), 			80,		array());
			$grid->addColumn('product', 		Lang::get('COMMON.PRODUCT'), 			100,	array());
			$grid->addColumn('rate', 			Lang::get('COMMON.RATE'), 				100,	array('align' => 'right'));
			$grid->addColumn('currency', 		Lang::get('COMMON.CURRENCY'), 			60,		array('align' => 'center'));
			$grid->addColumn('turnover', 		Lang::get('COMMON.TURNOVER'), 			150,	array('align' => 'right'));
			$grid->addColumn('incentive', 		Lang::get('COMMON.INCENTIVE'), 			150,	array('align' => 'right'));
			$grid->addColumn('taken_bonus', 	Lang::get('COMMON.TAKENBONUS'), 		150,	array('align' => 'right'));
			$grid->addColumn('taken_bonus_type', 	Lang::get('COMMON.TAKENBONUS').' Type', 		200,	array('align' => 'left'));
			
			if( !$request->has('aflid') )
			{
				$grid->addButton('2', Lang::get('COMMON.APPROVE'), '', 'button', array('icon' => 'ok', 'url' => 'approve_batch(\'approve\');'));
			}

			$data['grid'] = $grid->getTemplateVars();

			return view('admin.grid2',$data);
    }	
	

	protected function doGetIncentivelistData(Request $request){
		
		$rows = array();
		$condition = 'insid = '.$request->id;

		$incentiveSetting = Incentivesetting::find($request->id);
		$condition .= ' and amount >= '.$incentiveSetting->minpayout;
		
		if( $request->has('aflid') )
		{
			$condition .= ' and accid IN ( select id from account where aflid = '.$request->aflid.' )';
		}

		$total = Incentive::whereRaw($condition)->count();
		$products  = $this->getProductCategoryOptions();
		
		$total_incentive = 0;
		$promocampaign = Promocampaign::getMethodOptions();

		$incBuilder = Incentive::whereRaw($condition)->skip($request->recordstartindex)->take( $request->pagesize );

		if ($request->has('sortdatafield')) {
			$sortField = $request->input('sortdatafield');
			$orderable = array('id' => 'id', 'product' => 'product', 'rate' => 'rate', 'currency' => 'crccode', 'turnover' => 'totalstake', 'incentive' => 'amount', 'amount' => 'amount');

			if (array_key_exists($sortField, $orderable)) {
                $incBuilder->orderBy($orderable[$sortField], $request->input('sortorder', 'desc'));
            } else {
                $incBuilder->orderBy('id','desc');
			}
		} else {
            $incBuilder->orderBy('id','desc');
		}

		if($incentives = $incBuilder->get()){
			foreach( $incentives as $incentive ){
				
				$bonuses = '';
				
				if( $promocashs = DB::select( DB::raw('select n.method as method from cashledger c,promocash p,promocampaign n where c.chtcode = 3 and c.refid = p.id and p.pcpid = n.id and c.status IN(1,4) and c.created >= "'.$incentive->datefrom.' 00:00:00" AND c.created <= "'.$incentive->dateto.' 23:59:59" and c.accid = '.$incentive->accid.' group by n.method ')) ){

					foreach( $promocashs as $promocash ){
						if( $promocash->method != 0 )
                            $bonuses .= $promocampaign[$promocash->method].',';
					}

				}

				if ($incentive->prdid > 0) {
					$product = $products['prdid_'.$incentive->prdid];
				} else {
					$product = $products[$incentive->product];
				}
				
				$accObj = Account::find($incentive->accid);
				$rows[] = array(
					'id' 			=> $incentive->id,
					'acccode' 		=> $accObj->nickname,
					'product' 		=> $product,
					'rate'			=> App::displayPercentage($incentive->rate),
					'currency'		=> $incentive->crccode,
					'turnover'  	=> App::displayAmount($incentive->totalstake),
					'incentive' 	=> App::displayAmount($incentive->amount),
					'amount' 		=> $incentive->amount,
					'taken_bonus' 	=> Cashledger::whereRaw(' accid = '.$incentive->accid.' AND chtcode = 3 AND status IN(1,4) AND created >= "'.$incentive->datefrom.' 00:00:00" AND created <= "'.$incentive->dateto.' 23:59:59" ')->count(),
					'taken_bonus_type' 	=> $bonuses,
				);
				$total_incentive += $incentive->amount;
				
			}
				
		}
				
		echo json_encode(array('total' => $total, 'rows' => $rows));
		exit;
	
	}
	
	public function dailyrebatebatchApprove(Request $request){
		
		$settings = array();
		$success  = false;
			
		foreach( $request->rows as $row ){
			if($object = Dailyrebate::find($row)) 
			{
					
					$accObj = Account::find($object->accid);
	
					$cashLedgerObj = new Cashledger;
					$cashLedgerObj->accid 			= $object->accid;
					$cashLedgerObj->acccode 		= $object->crccode;
					$cashLedgerObj->accname			= $accObj->fullname;
					$cashLedgerObj->chtcode 		= CBO_CHARTCODE_DAILYREBATE;
					$cashLedgerObj->cashbalance 	= $cashLedgerObj->getBalance($object->accid);;
					$cashLedgerObj->amount 			= $object->amount;
					$cashLedgerObj->amountlocal 	= $object->amount;
					$cashLedgerObj->refid 			= $object->id;
					$cashLedgerObj->status 			= CBO_LEDGERSTATUS_PENDING;
					$cashLedgerObj->refobj 			= 'daily rebate';
					$cashLedgerObj->crccode 		= $object->crccode;
					$cashLedgerObj->crcrate 		= Currency::getCurrencyRate($object->crccode);
					$cashLedgerObj->fee 			= $object->amount;
					$cashLedgerObj->feelocal 		= $object->amount;
					$cashLedgerObj->createdip		= App::getRemoteIp();
					
					if($cashLedgerObj->save()) 
					{
						$object->clgid	= $cashLedgerObj->id;
						$object->status = 1;
						$object->save();
						$success = true;
					}
				
			}
	
		}
		
		if( $success )
			echo json_encode( array( 'code' => Lang::get('COMMON.SUCESSFUL') ) );
		else
			echo json_encode( array( 'code' => Lang::get('COMMON.FAILED') ) );
		
	}
	
	public function batchApprove(Request $request){
		
		$settings = array();
		$success  = false;
			
		foreach( $request->rows as $row ){
			
			if($object = Incentive::find($row)) 
			{
				if(!isset($settings[$object->insid])) 
				{
					$insObj = Incentivesetting::find($object->insid);
					$settings[$object->insid] = $insObj;
				}
				
				if($object->amount >= $settings[$object->insid]->minpayout) 
				{
					if(($settings[$object->insid]->maxamount > 0) && ($object->amount > $settings[$object->insid]->maxamount)) 
					{
						$object->amount = $settings[$object->insid]->maxamount;
						$object->save();
					}
					
					$accObj = Account::find($object->accid);
	
					$cashLedgerObj = new Cashledger;
					$cashLedgerObj->accid 			= $object->accid;
					$cashLedgerObj->acccode 		= $object->acccode;
					$cashLedgerObj->accname			= $accObj->fullname;
					$cashLedgerObj->chtcode 		= CBO_CHARTCODE_INCENTIVE;
					$cashLedgerObj->cashbalance 	= $cashLedgerObj->getBalance($object->accid);;
					$cashLedgerObj->amount 			= $object->amount;
					$cashLedgerObj->amountlocal 	= $object->amountlocal;
					$cashLedgerObj->refid 			= $object->id;
					$cashLedgerObj->status 			= CBO_LEDGERSTATUS_PENDING;
					$cashLedgerObj->refobj 			= 'Incentive';
					$cashLedgerObj->crccode 		= $object->crccode;
					$cashLedgerObj->crcrate 		= Currency::getCurrencyRate($object->crccode);
					$cashLedgerObj->fee 			= $object->amount;
					$cashLedgerObj->feelocal 		= $object->amountlocal;
					$cashLedgerObj->createdip		= App::getRemoteIp();
					
					if($cashLedgerObj->save()) 
					{
						$object->clgid = $cashLedgerObj->id;
						$object->save();
						$success = true;
					}
				}
			}
	
		}
		
		if( $success )
			echo json_encode( array( 'code' => Lang::get('COMMON.SUCESSFUL') ) );
		else
			echo json_encode( array( 'code' => Lang::get('COMMON.FAILED') ) );
		
	}

    private function getProductCategoryOptions() {

        $products  = Product::getCategoryOptions();
        $categoryOnly = true;
        $withProductFronts = array('front', 'royalewin');
		
		

        if (in_array(Config::get('setting.front_path'), $withProductFronts)) {
            $categoryOnly = false;
		}

        if (!$categoryOnly) {
            foreach ($products as $pk => $pv) {
                $products[$pk] = '[C] '.$pv;
            }

            $prdObj = Product::where('status', '=', Product::STATUS_ACTIVE)
                ->orderBy('name')
                ->get(['id', 'name']);

            foreach ($prdObj as $r) {
                $products['prdid_'.$r->id] = '[P] '.$r->name;
            }
        }

        return $products;
    }

}

?>
