<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller\Admin;
use App\libraries\grid\PanelGrid;
use App\libraries\App;
use Illuminate\Http\Request;
use Auth;
use Validator;
use Session;
use Lang;
use App\Models\Currency;
use App\Models\Bank;
use App\Models\Accounttag;
use App\Models\Product;
use App\Models\Tag;
use App\Models\Account;
use App\Models\Website;
use App\Models\Config;
use App\Models\Bankaccount;



class WebsiteController extends Controller{

	protected $moduleName 	= 'website';
	protected $limit 		= 20;
	protected $start 		= 0;
	
	public function __construct()
	{
					
	}
	
	public function websiteShow(Request $request){

			$grid = new PanelGrid;
			$grid->setupGrid($this->moduleName, 'website-data', $this->limit);
			$grid->setTitle(Lang::get('COMMON.WEBSITE'));
									
			$grid->addColumn('name',				Lang::get('COMMON.NAME'), 						200,		array());
			$grid->addColumn('currency',			Lang::get('COMMON.CURRENCY'), 					150,		array());
			$grid->addColumn('product',				Lang::get('COMMON.PRODUCT'), 					300,		array());
			$grid->addColumn('status',				Lang::get('COMMON.STATUS'), 					70,		array('align' => 'center'));
			
			$data['grid'] = $grid->getTemplateVars();

			return view('admin.grid2',$data);
    }
	
	protected function websiteShowdoGetData(Request $request){
		
		//status
		$status[1] = 'Active';
		$status[0] = 'Suspended';
		
		$aflt  = $request->input('aflt');
		$aqfld = $request->input('aqfld');
		
		$condition ='1';
		//$condition .= ' AND currencies = "'.Session::get('admin_crccode').'"';
		
		if( isset($aqfld['status']) && $aqfld['status'] != 0 ){
			$condition = ' AND status'.'='.$aqfld['status'];
		}
		
		$total = Bankaccount::whereRaw($condition)->count();

		//$object = new WebsiteObject;

		$rows = array();
		if($websites = Website::whereRaw($condition)->get()){
			foreach($websites as $website){
				$currency = array();
				$product = '';
				$url = '';
								
				if($wpds = unserialize($website->products)) {
					foreach($wpds as $wpd) {
						if($prdobj= Product::find($wpd))
						{
							$product .= $prdobj->name.',';
						}
					}
				}   
				
				$name = App::formatAddTabUrl( Lang::get('WEBSITE').': '.$website->url, urlencode($website->url), action('Admin\WebsiteController@websiteShowdrawAddEdit',  array('sid' => $website->id)));				
				$rows[] = array(
					'name' => urldecode($name),
					'url' => $url,
					'currency' => implode(', ', unserialize($website->currencies)),
					'product' => $product,
					'status' => $website->getStatusText($this->moduleName),
				);
			}
		}
		
		echo json_encode(array('total' => $total, 'rows' => $rows));
		exit;

	}
	
	protected function websiteShowdrawAddEdit(Request $request) { 

	    $toAdd 	  = true;
		$object   = null;
		$readOnly = false;
		$values   = array();

		$title = Lang::get('NEW').' '.Lang::get('WEBSITE');

		if($request->has('sid')) 
		{
	
			if ($object = Website::find($request->input('sid'))) 
				{
					$title    = Lang::get('COMMON.EDIT').' '.Lang::get('COMMON.WEBSITE');
					$toAdd    = false;
					$readOnly = true;

				} 
			else
				{
					$object = null;
				}
			
		}
		
		$form = App::setupPanelForm('doadd', $request->all(), array(), $toAdd);
		$form->setFormTitle($title);

		$form->addInput('readonly', 			Lang::get('COMMON.CODE') 			, 'code', 	 		(($object)? $object->code:''), 			array(), false); 
		$form->addInput('readonly', 			Lang::get('COMMON.PREFIX') 			, 'memberprefix', 	(($object)? $object->memberprefix:''), 	array(), false); 
		$form->addInput('text', 				Lang::get('COMMON.CURRENCY') 		, 'currency', 	 	(($object)? $object->currencies:''), 	array(), false); 
		$form->addInput('text', 				Lang::get('COMMON.PRODUCT') 		, 'product', 	 	(($object)? $object->products:''), 		array(), false); 
		$form->addInput('radio',   			    Lang::get('COMMON.STATUS')			, 'status',  		(($object)? $object->status:''), 		array('options' => App::getStatusOptions()), true);
	
		$data['form'] = $form->getTemplateVars();
		$data['module'] = 'website';

		return view('admin.form2',$data);
	
	}
	
	protected function doAddEdit(Request $request) {
		
		$validator = Validator::make(
			[
	
				'name'	  	 	  => $request->input('name'),
				'currency'	  	  => $request->input('currency'),
				'product'	  	  => $request->input('product'),
				'status'	  	  => $request->input('status'),

			],
			[

			   'name'	   	  	   => 'required',
			   'currency'	  	   => 'required',
			   'product'	  	   => 'required',
			   'status'	  	  	   => 'required',

			]
		);
				
		if ($validator->fails())
		{
			echo json_encode($validator->errors());
			exit;
		}
		
		
		
		$object  = new Website;
		
		if($request->has('sid')) 
		{
			$object = Website::find($request->input('sid'));
		}
		
		$object->url	 					    = $request->input('name');
		$object->currencies	 				    = $request->input('currency');
		$object->products	 				    = $request->input('product');
		$object->status	 				  	    = $request->input('status');


		
		if( $object->save() )
		{
			echo json_encode( array( 'code' => Lang::get('COMMON.SUCESSFUL') ) );
			exit;
		}


		echo json_encode( array( 'code' => Lang::get('COMMON.FAILED') ) );
		
	}
	
	protected function doActivateSuspend(Request $request) {

			

		if($request->has('id')) {
		
			if($object = Website::find($request->id)) {
			
				$currentStatus = $object->status;
				if($request->action == 'activate') {
					$object->status 	= CBO_ACCOUNTSTATUS_ACTIVE;
				}
				if($request->action == 'suspend') {
					$object->status 	= CBO_ACCOUNTSTATUS_SUSPENDED;
				}

				if($currentStatus <> $object->status){
					if($object->save()) {
					
						echo json_encode( array( 'code' => Lang::get('COMMON.SUCESSFUL') ) );
						exit;
					}
				}
			}
		}
		
		
		echo json_encode( array( 'code' => Lang::get('COMMON.FAILED') ) );

	}
	



}

?>
