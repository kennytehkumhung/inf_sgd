<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller\Admin;
use App\libraries\grid\PanelGrid;
use App\libraries\App;
use Illuminate\Http\Request;
use Auth;
use Validator;
use Session;
use Lang;
use App\Models\Iptracker;
use App\Models\Currency;
use App\Models\Fundingmethod;
use App\Models\Cashledger;
use App\Models\Bank;
use App\Models\Accounttag;
use App\Models\Tag;
use App\Models\Account;
use App\Models\Config;
use App\Models\Cashledgerlog;
use App\Models\Bankaccount;
use App\Models\Promocash;
use App\Models\Rejectreason;
use App\Models\Banktransfer;
use App\Models\Promocampaign;

class RejectreasonController extends Controller{


	protected $moduleName 	= 'rejectreason';
	protected $limit 		= 20;
	protected $start 		= 0;
	
	public function __construct()
	{
		
	}
	
	public function index(Request $request){

			$grid = new PanelGrid;
			$grid->setupGrid($this->moduleName, 'rejectreason-data', $this->limit);
			$grid->setTitle(Lang::get('COMMON.REJECT'));
			
			//$grid->addColumn('id', 			Lang::get('COMMON.ID'), 				0,			array('align' => 'left'));			
			$grid->addColumn('code',		Lang::get('COMMON.CODE'), 				68,			array('align' => 'left'));
			$grid->addColumn('description', Lang::get('COMMON.DESC'), 				300,		array('align' => 'left'));
			$grid->addColumn('status', 		Lang::get('COMMON.STATUS'), 			120,		array('align' => 'left'));
			

			
			$grid->addFilter('status',array('Active','Suspended'), array('display' => Lang::get('COMMON.TYPE')));
			$grid->addButton('1', 'New', Lang::get('COMMON.ADDNEW').' '.Lang::get('COMMON.REJECT'), 'button', array('icon' => 'add', 'url' => action('Admin\RejectreasonController@drawAddEdit')));


			$data['grid'] = $grid->getTemplateVars();

			return view('admin.grid2',$data);
    }
	
	protected function doGetData(Request $request){
	
		//status
		$status[1] = 'Active';
		$status[0] = 'Suspended';
		
		$rows = array();
		
		$aflt  = $request->input('aflt');
		$aqfld = $request->input('aqfld');
		
		//$condition = 'status'.'='.'1';
		
		$condition = '1';
		
		if( isset($aqfld['status']) && $aqfld['status'] != 0 ){
			$condition = ' AND status'.'='.$aqfld['status'];
		}
		
		$total = Rejectreason::whereRaw($condition)->count();

		//new structure
		if($rejects = Rejectreason::whereRaw($condition)->skip($request->recordstartindex)->take( $request->pagesize )->orderBy('id','desc')->get()){
			foreach( $rejects as $reject ){
				
				$body = App::formatAddTabUrl( Lang::get('REJECT').': '.$reject->id, urlencode($reject->code), action('Admin\RejectreasonController@drawAddEdit',  array('sid' => $reject->id)));
		
				$rows[] = array(

					//'id'  				=> $reject->id, 
					'code'  			=> urldecode($body), 
					'description'  		=> $reject->desc, 
					'status'  			=> $reject->getStatusText($this->moduleName),

				);	
				
			}
				
		}

				
		echo json_encode(array('total' => count($rows), 'rows' => $rows));
		exit;
	
	
	}
	
	protected function drawAddEdit(Request $request) { 

	    $toAdd 	  = true;
		$object   = null;
		$readOnly = false;
		$values   = array();

		$title = Lang::get('NEW').' '.Lang::get('REJECT');

		if($request->has('sid')) 
		{
	
			if ($object = Rejectreason::find($request->input('sid'))) 
				{
					$title    = Lang::get('COMMON.EDIT').' '.Lang::get('COMMON.REJECT');
					$toAdd    = false;
					$readOnly = true;

				} 
			else
				{
					$object = null;
				}
			
		}

		$form = App::setupPanelForm('doadd', $request->all(), array(), $toAdd);
		$form->setFormTitle($title);
		
		$form->addInput('text', 		Lang::get('COMMON.CODE') 			, 'code', 	 	 (($object)? $object->code:''), 		array(), false); 
		$form->addInput('textarea', 	Lang::get('COMMON.DESC') 			, 'description', (($object)? $object->desc:''), 		array(), false); 
		$form->addInput('radio',   		Lang::get('COMMON.STATUS')			, 'status',  	 (($object)? $object->status:''), 		array('options' => App::getStatusOptions()), true);	

		
	
		$data['form'] = $form->getTemplateVars();
		$data['module'] = 'rejectreason';

		return view('admin.form2',$data);
	
	}
	
	protected function doAddEdit(Request $request) {
		
		$validator = Validator::make(
			[
	
				'code'	  	  		  => $request->input('code'),
				'description'	  	  => $request->input('description'),

			],
			[

			   'code'	   	   	   => 'required',
			   'description'	   => 'required',

			]
		);
		
		
		if ($validator->fails())
		{
			echo json_encode($validator->errors());
			exit;
		}
		
		
		
		$object  = new Rejectreason;
		
		if($request->has('sid')) 
		{
			$object = Rejectreason::find($request->input('sid'));
		}
		
		$object->code	 			= $request->input('code');
		$object->desc	 			= $request->input('description');
		$object->status	 			= $request->input('status');


		
		if( $object->save() )
		{
			echo json_encode( array( 'code' => Lang::get('COMMON.SUCESSFUL') ) );
			exit;
		}


		echo json_encode( array( 'code' => Lang::get('COMMON.FAILED') ) );
		
	}
	
	protected function doActivateSuspend(Request $request) {

			

		if($request->has('id')) {
		
			if($object = Rejectreason::find($request->id)) {
			
				$currentStatus = $object->status;
				if($request->action == 'activate') {
					$object->status 	= CBO_ACCOUNTSTATUS_ACTIVE;
				}
				if($request->action == 'suspend') {
					$object->status 	= CBO_ACCOUNTSTATUS_SUSPENDED;
				}

				if($currentStatus <> $object->status){
					if($object->save()) {
					
						echo json_encode( array( 'code' => Lang::get('COMMON.SUCESSFUL') ) );
						exit;
					}
				}
			}
		}
		
		
		echo json_encode( array( 'code' => Lang::get('COMMON.FAILED') ) );

	}


}

?>
