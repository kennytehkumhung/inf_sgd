<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller\Admin;
use App\libraries\grid\PanelGrid;
use App\libraries\App;
use DB;
use Illuminate\Http\Request;
use Auth;
use Validator;
use Session;
use Lang;
use App\Models\Onlinetrackerlog;
use App\Models\Currency;
use App\Models\Fundingmethod;
use App\Models\Cashledger;
use App\Models\Bank;
use App\Models\Affiliate;
use App\Models\Accounttag;
use App\Models\Tag;
use App\Models\Account;
use App\Models\Config;
use App\Models\Cashledgerlog;
use App\Models\Bankaccount;
use App\Models\Promocash;
use App\Models\Rejectreason;
use App\Models\Banktransfer;
use App\Models\Promocampaign;

class IpTrackerController extends Controller{

	protected $moduleName 	= 'iptracker';
	protected $limit 		= 20;
	protected $start 		= 0;
	
	public function __construct()
	{
					
	}
	
	public function index(Request $request){

			$grid = new PanelGrid;
			
			if($request->has('aflid'))
			{
				$grid->setupGrid($this->moduleName, 'iptracker-data', $this->limit, '', array('params' => array('aflid' => $request->aflid )));
			}else{
				$grid->setupGrid($this->moduleName, 'iptracker-data', $this->limit);
			}
			
			$grid->setTitle(Lang::get('COMMON.IPTRACKER'));
									
			
			$grid->addColumn('type', 			Lang::get('COMMON.TYPE'), 					68,		array('align' => 'left'));
			$grid->addColumn('username', 		Lang::get('COMMON.USERNAME'),				108,	array('align' => 'left'));
			//$grid->addColumn('createdpanel', 	Lang::get('COMMON.PANEL'), 					108,	array('align' => 'center'));
			$grid->addColumn('takenbonus', 		Lang::get('COMMON.TAKENBONUS'), 			214,	array('align' => 'left'));
			$grid->addColumn('clientip', 		Lang::get('COMMON.CLIENT'), 				108,	array('align' => 'left'));
			$grid->addColumn('serverip', 		Lang::get('COMMON.SERVER'), 				108,	array('align' => 'left'));		
			$grid->addColumn('created', 		Lang::get('COMMON.CREATEDON'), 				150,	array('align' => 'center'));
			
			$grid->addSearchField('search1', array('username' => Lang::get('COMMON.USERNAME')));
			$grid->addSearchField('search2', array('ip' => Lang::get('COMMON.IPADDRESS')));
			$grid->addSearchField('search3', array('ip_like' => Lang::get('COMMON.IPADDRESS') . ' (Match)'));
			
			if(!$request->has('aflid'))
			{
				$grid->addFilter('type',array(1=>'Member',2=>'Agent',3=>'Admin User'), array('display' => Lang::get('COMMON.TYPE')));
			}else{
				$grid->addFilter('type',array(1=>'Member',2=>'Agent'), array('display' => Lang::get('COMMON.TYPE')));
			}
		
			
			$data['grid'] = $grid->getTemplateVars();

			return view('admin.grid2',$data);
    }
	
	protected function doGetData(Request $request){

		$rows = array();

		$builder = DB::table('onlinetrackerlog')->select(array('id', 'type', 'userid', 'username', 'clientip', 'serverip', 'created'));

		if( isset($request->aflt['username']) && $request->aflt['username'] != '' )
		{
            $builder->where('username', '=', $request->aflt['username']);
		}
		
		if( isset($request->aflt['ip']) && $request->aflt['ip'] != '' )
		{
//            $builder->where('clientip', 'LIKE', '%' . $request->aflt['ip'] . '%');
            $builder->where('clientip', '=', $request->aflt['ip']);
		}

		if( isset($request->aflt['ip_like']) && $request->aflt['ip_like'] != '' )
		{
            $builder->where('clientip', 'LIKE', '%' . $request->aflt['ip_like'] . '%');
		}
		
		if( isset($request->aqfld['type']) && $request->aqfld['type'] != '' )
		{
			$builder->where('type', '=', $request->aqfld['type']);
		}
		
		if( $request->has('createdfrom') ){
			$builder->where('created', '>=', $request->get('createdfrom') . ' 00:00:00')
				->where('created', '<=', $request->get('createdto') . ' 23:59:59');
		}
		
	
		
		if( $request->has('aflid') ) 
		{
			$downlines = Affiliate::getAllDownlineById($request->aflid, true);
			
			$builder->whereRaw('userid IN (select id from account where aflid IN ('.implode($downlines,',').')) ');

		}

		$total = $builder->count();

		//new structure
        $iptrackers = $builder->skip($request->recordstartindex)
			->take( $request->pagesize )
			->orderBy('id','desc')
			->get();

		if(count($iptrackers) > 0){

			$memberType = array(CBO_LOGINTYPE_USER, CBO_LOGINTYPE_AFFILIATE);

			// Get all PCP ID & name.
            $pcpList = collect(array());
			$pcpObj = Promocampaign::get(array('id', 'name'));
			foreach ($pcpObj as $r) {
                $pcpList->put('_'.$r->id, $r->name);
			}

			$onlineTrackerTypeOptions = collect((new Onlinetrackerlog())->getTypeOptions());
			$takenBonusCache = array();

			// Collect all members' pcpid in single query.
            $cashLedgerObj = DB::table('cashledger')
				->whereIn('accid', collect($iptrackers)->unique('userid')->pluck('userid'))
                ->where('chtcode', '=', CBO_CHARTCODE_BONUS)
                ->whereIn('status', array(CBO_LEDGERSTATUS_CONFIRMED, CBO_LEDGERSTATUS_MANCONFIRMED))
                ->groupBy('accid', 'refobj', 'refid')
                ->get(array('accid', 'refid', 'refobj'));

            $userRefobjList = array();
            $refobjIdList = array();

            foreach ($cashLedgerObj as $cashLg) {
            	if (!isset($userRefobjList[$cashLg->accid])) {
                    $userRefobjList[$cashLg->accid] = array();
				}

				if (!isset($refobjIdList[$cashLg->refobj])) {
                    $refobjIdList[$cashLg->refobj] = array();
				}

				// Collect each member's promo refobj and refid.
                $userRefobjList[$cashLg->accid][] = $cashLg->refobj.'_'.$cashLg->refid;

                // Collect each promo refobj and refid.
                $refobjIdList[$cashLg->refobj][] = $cashLg->refid;
			}

			$refobjPcpidList = array();

            // Collect all pcpid' in single query.
            foreach ($refobjIdList as $refName => $refIds) {
                $res = DB::table(strtolower($refName))
                    ->whereIn('id', collect($refIds)->unique()->toArray())
                    ->get(array('id', 'pcpid'));

				foreach ($res as $r) {
                    $refobjPcpidList[$refName.'_'.$r->id] = $r->pcpid;
				}
			}

			foreach( $iptrackers as $iptracker ){

                $takenBonus = '';

				if (in_array($iptracker->type, $memberType)) {

					if (!isset($takenBonusCache[$iptracker->userid])) {

                        $takenBonusList = array();

                        if (isset($userRefobjList[$iptracker->userid])) {
                        	$refval = $userRefobjList[$iptracker->userid];

                        	foreach ($refval as $rv) {
                                if (isset($refobjPcpidList[$rv])) {
                                    $pcpid = $refobjPcpidList[$rv];

                                    $takenBonusList[] = $pcpList->get('_'.$pcpid, 'Bonus');
                                }
							}

							if (count($takenBonusList) > 0) {
                                $takenBonusList = collect($takenBonusList)->unique()->toArray();
                            }
						}

                        $takenBonusCache[$iptracker->userid] = implode(', ', $takenBonusList);
					}

                    $takenBonus = $takenBonusCache[$iptracker->userid];

				}

				$rows[] = array(

					'id'  				=> $iptracker->id, 
					'type'  			=> $onlineTrackerTypeOptions->get($iptracker->type),
					'username'  		=> $iptracker->username, 
					//'createdpanel'  	=> $iptracker->createdpanel,
					'takenbonus'		=> $takenBonus,
					'clientip'  		=> $iptracker->clientip, 
					'serverip'  		=> $iptracker->serverip, 
					'created'  			=> $iptracker->created,
				);	
				
			}
				
		}

				
		echo json_encode(array('total' => $total , 'rows' => $rows));
		exit;
	
	}


}
