<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller\Admin;
use App\libraries\grid\PanelGrid;
use App\libraries\App;
use App\Models\Affiliate;
use App\Models\Badlogin;
use App\Models\Channel;
use App\Models\Currency;
use App\Models\Account;
use App\Models\Accountdetail;
use App\Models\Agent;
use App\Models\Profitloss;
use App\Models\Wager;
use App\Models\Cashledger;
use App\Models\Tag;
use App\Models\Accounttag;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Auth;
use Validator;
use Session;
use Lang;
use GeoIP;
use Config;


class MemberEnquiryController extends Controller{

	
	protected $moduleName 	= 'member';
	protected $limit 		= 20;
	protected $start 		= 0;
	static protected $affiliateInstances = array();
	
	public function __construct()
	{
		
	}

	public function index(Request $request)
	{

	    $canVIewContactInfo = $this->hasPermission('MEMBER', 'MEMBERENQUIRY', 'VIEWCONTACTINFO');

		$grid = new PanelGrid;
		
		if( $request->has('aflids') )
		{
			$grid->setupGrid($this->moduleName, 'member-data', $this->limit, true, array('params' => array('aflids' => $request->aflids ) ));
		}
		else
		{
			$grid->setupGrid($this->moduleName, 'member-data', $this->limit);
		}
		
		$grid->setTitle(Lang::get('COMMON.ACCOUNTENQUIRY'));
        $grid->addColumn('code',		Lang::get('COMMON.MEMBERACCOUNT'),  100,	array('sort' => true));
        $grid->addColumn('referrer',	Lang::get('COMMON.AGENCY'),  		100,	array('sort' => true));
		$grid->addColumn('fullname',	Lang::get('COMMON.MEMBERNAME'),		160,	array('sort' => true));
		$grid->addColumn('regchannel',	Lang::get('COMMON.CHANNEL'),		 80,	array('sort' => true));
        $grid->addColumn('crccode',		Lang::get('COMMON.CURRENCY'),		 60,	array('align' => 'center'));
        $grid->addColumn('cashbalance',	Lang::get('COMMON.MAINWALLET'),		 80,	array('align' => 'right', 'sort' => true));
        $grid->addColumn('deposit',		Lang::get('COMMON.DEPOSIT'),		105, 	array('align' => 'right'));
        $grid->addColumn('withdrawal',  Lang::get('COMMON.WITHDRAWAL'), 	105,	array('align' => 'right'));

        if ($canVIewContactInfo) {
            $grid->addColumn('telephone', Lang::get('COMMON.TELEPHONENO'), 90, array());

            if (Config::get('setting.front_path') == 'c59')
                $grid->addColumn('email', 'LINE ID / ' . Lang::get('COMMON.EMAIL'), 160, array());
            else
                $grid->addColumn('email', Lang::get('COMMON.EMAIL'), 160, array());
        }

		$grid->addColumn('created',		Lang::get('COMMON.REGISTEREDON'), 	130,	array('align' => 'center'));
		$grid->addColumn('firstdeposit',Lang::get('COMMON.FIRSTDEPOSIT'), 	130,	array('align' => 'center'));
		$grid->addColumn('lastdeposit',	Lang::get('COMMON.LASTDEPOSIT'), 	130,	array('align' => 'center'));
		if( !$request->has('aflids') )
		{
			$grid->addColumn('status', 	    Lang::get('COMMON.STATUS'), 		 75,	array('align' => 'center'));
		}
		$grid->addColumn('createdip', 	Lang::get('COMMON.IPADDRESS'), 		 120,	array());
		$grid->addColumn('location', 	Lang::get('COMMON.LOCATION'),		200,	array());

		/**
		* Add filters
		*/
		$grid->addFilter('regchannel', Channel::getAllAsOptions(), array('display' => Lang::get('COMMON.CHANNEL') , 'value' => ((isset($this->filterValue['regchannel']))?$this->filterValue['regchannel']:$request->input('regchannel'))));
		$grid->addFilter('crccode', Currency::getAllCurrencyAsOptions(), array('display' => Lang::get('COMMON.CURRENCY') , 'value' => ((isset($this->filterValue['crccode']))?$this->filterValue['crccode']:$request->input('crccode'))));
		$grid->addFilter('status', Account::getStatusOptions(), array('display' => Lang::get('COMMON.STATUS') , 'value' => ((isset($this->filterValue['status']))?$this->filterValue['status']:$request->input('status'))));
		$grid->addFilter('isemailvalid', Account::getEmailStatusOptions(), array('display' => Lang::get('COMMON.EMAIL') , 'value' => ((isset($this->filterValue['isemailvalid']))?$this->filterValue['isemailvalid']:$request->input('isemailvalid'))));
		$grid->addFilter('isphonevalid', Account::getPhoneStatusOptions(), array('display' => Lang::get('COMMON.TELEPHONENO') , 'value' => ((isset($this->filterValue['isphonevalid']))?$this->filterValue['isphonevalid']:$request->input('isphonevalid'))));
		$grid->addFilter('isdeposit', array( 0 => Lang::get('COMMON.NO') , 1 => Lang::get('COMMON.YES') ), array('display' => Lang::get('COMMON.DEPOSIT'), 'value' => $request->input('isdeposit') ));

		$tagFilters = array('tagcustomer' => array('' => Lang::get('COMMON.ALL')), 'tagoperator' => array('' => Lang::get('COMMON.ALL')));
        $tagObj = Tag::orderBy('name', 'asc')->get();

        foreach ($tagObj as $tag) {
            if ($tag->category == Tag::CATEGORY_OPERATOR_TAG) {
                $tagFilters['tagoperator'][$tag->id] = $tag->name;
            } else {
                $tagFilters['tagcustomer'][$tag->id] = $tag->name;
            }
        }

        $grid->addFilter('tagcustomer', $tagFilters['tagcustomer'], array('display' => Lang::get('COMMON.CUSTOMER').' '.Lang::get('COMMON.TAG'), 'value' => $request->input('tagcustomer') ));
        $grid->addFilter('tagoperator', $tagFilters['tagoperator'], array('display' => Lang::get('COMMON.OPERATOR').' '.Lang::get('COMMON.TAG'), 'value' => $request->input('tagoperator') ));

		/**
		* Add searches
		*/
		$search = '';
		$value = '';
		if($request->has('aflid')) {
			$aflObj = new Affiliate();
			if($aflObj = Affiliate::find($request->input('aflid'))) {
				$search = 'aflid';
				$value = $aflObj->username;
			}
		}
		if($request->has('nodate')) {
			Session::put('nodate', true);
		}
		
		$showcolunm = $request->input('showcolumn');

        if ($canVIewContactInfo) {
            $grid->addSearchField('search', array('code' => Lang::get('COMMON.MEMBERACCOUNT'), 'fullname' => Lang::get('COMMON.MEMBERNAME'), 'idpassport' => Lang::get('COMMON.IDPASSPORT'), 'email' => Lang::get('COMMON.EMAIL'), 'telmobile' => Lang::get('COMMON.TELEPHONENO')));
        } else {
            $grid->addSearchField('search', array('code' => Lang::get('COMMON.MEMBERACCOUNT'), 'fullname' => Lang::get('COMMON.MEMBERNAME'), 'idpassport' => Lang::get('COMMON.IDPASSPORT')));
        }

		$grid->addSearchField('search', array('aflid' => Lang::get('COMMON.AFFILIATE')), array('search' => $search, 'value' => $value));
//		$grid->addSearchField('search', array('accids' => 'accids'), array('search' => 'accids', 'value' => $request->input('accids'), 'hidden' => true));
        $grid->addSearchField('search', array('betmemonly' => 'betmemonly'), array('search' => 'betmemonly', 'value' => $request->input('betmemonly'), 'hidden' => true));
        $grid->addRangeField('created', array('type' => 'date', 'display' => Lang::get('COMMON.DATE'), 'createdfrom' => $request->input('createdfrom', date('Y-m-d')), 'createdto' => $request->input('createdto', date('Y-m-d'))));
		$grid->addShowColumn('transaction', Lang::get('COMMON.DEPOSITWITHDRAWAL'), (isset($showcolunm['transaction'])? $showcolunm['stransaction']:false));
		$grid->addShowColumn('contact', 	Lang::get('COMMON.CONTACTINFO'), 	   (isset($showcolunm['scontact'])   ? $showcolunm['scontact']	  :false));
		$grid->addShowColumn('affiliate',   Lang::get('COMMON.AFFILIATE'), 		   (isset($showcolunm['saffiliate']) ? $showcolunm['saffiliate']  :false));
		$grid->addShowColumn('testacc', 	Lang::get('COMMON.TRIALACCOUNT'), 	   (isset($showcolunm['stestacc'])   ? $showcolunm['stestacc']	  :false));
		$grid->addButton('3', 'buttton', 	Lang::get('COMMON.APPROVE'), 'button', array('icon' => 'excel', 'url' => 'batchApprove();'));
//		$grid->addButton('2', 'EXPORT' , '', 'button', array('icon' => 'ok', 'url' => 'export_excel();'));
		
		$data['grid'] = $grid->getTemplateVars();

		if ($request->has('createdfrom')) {
            $data['grid']['options']['params']['createdfrom'] = $request->get('createdfrom');
        }

        if ($request->has('createdto')) {
            $data['grid']['options']['params']['createdto'] = $request->get('createdto');
        }

		//var_dump($data['grid']);
		return view('admin.grid2',$data);

	}
	
	protected function doGetData(Request $request) {

		$date_filter = true;
		
		$aflt  = $request->input('aflt');
		$aqfld = $request->input('aqfld');

        $canVIewContactInfo = $this->hasPermission('MEMBER', 'MEMBERENQUIRY', 'VIEWCONTACTINFO');

        $builder = Account::from('account as a')
            ->where('a.istestacc', '=', 0)
            ->select(array('a.*'));

        $canViewAllCurrencies = false;

		if ( ( config('setting.opcode') == 'RYW' || config('setting.opcode') == 'IFW' )&& Session::get('admin_crccode') == 'MYR' ) {
		    $canViewAllCurrencies = true;
        }

        if (!$canViewAllCurrencies) {
            $builder->where('a.crccode', '=', Session::get('admin_crccode'));
        }

		if( isset($aqfld['aflid']) ){
			$aflObj = Affiliate::where( 'username' , '=' , $aqfld['aflid'] )->first();
            $builder->where('a.aflid', '=', $aflObj->id);
		}elseif( isset($aflt['aflid']) ){
			if($aflObj = Affiliate::where( 'username' , '=' , $aflt['aflid'] )->first()){
                $builder->where('a.aflid', '=', $aflObj->id);
			}
		}
		
		if( isset($aflt['code']) && $aflt['code'] != '' ){
            $builder->where('a.nickname', 'LIKE', '%'.$aflt['code'].'%');
			$date_filter = false;
		}
		
		if( isset($aflt['fullname']) && $aflt['fullname'] != ''){
            $builder->where('a.fullname', 'LIKE', '%'.$aflt['fullname'].'%');
			$date_filter = false;
		}	

		if ($canVIewContactInfo) {
            if (isset($aflt['email']) && $aflt['email'] != '') {
                $builder->whereRaw('a.id IN ( SELECT id FROM accountdetail WHERE email LIKE ? )', array('%'.$aflt['email'].'%'));
                $date_filter = false;
            }

            if (isset($aflt['telmobile']) && $aflt['telmobile'] != '') {
                $builder->whereRaw('a.id IN ( SELECT id FROM accountdetail WHERE telmobile = ? )', array($aflt['telmobile']));
                $date_filter = false;
            }
        }

        if( isset($aflt['betmemonly']) && $aflt['betmemonly'] == 'Y' ){
            $builder->whereRaw('a.id IN ( SELECT accid FROM profitloss WHERE date BETWEEN ? AND ? )', array($request->get('createdfrom'), $request->get('createdto')));
            $date_filter = false;
        }
		
		if( isset($aqfld['status']) && $aqfld['status'] != '' ){
            $builder->where('a.status', '=', $aqfld['status']);
		}
		
		if( $request->has('createdfrom') && $date_filter ){
            $builder->whereBetween('a.created', array($request->get('createdfrom').' 00:00:00', $request->get('createdto').' 23:59:59'));
		}
		
		if( isset($aqfld['isdeposit']) && $aqfld['isdeposit'] != '' )
		{
			if( $aqfld['isdeposit'] == '0' )
			    $builder->where('a.totaldeposit', '=', 1);
			if( $aqfld['isdeposit'] == '1' )
                $builder->where('a.totaldeposit', '>=', 1);
		}	
		
		if( isset($aqfld['crccode']) && $aqfld['crccode'] != '' ){
            $builder->where('a.crccode', '=', $aqfld['crccode']);
		}
		
		if( isset($aqfld['regchannel']) && $aqfld['regchannel'] != '' ){
            $builder->where('a.regchannel', '=', $aqfld['regchannel']);
		}

		$tagIds = array();

		if( isset($aqfld['tagcustomer']) && $aqfld['tagcustomer'] != '' ){
            $tagIds[] = $aqfld['tagcustomer'];
		} elseif ( isset($aqfld['tagoperator']) && $aqfld['tagoperator'] != '' ) {
            $tagIds[] = $aqfld['tagoperator'];
        }

        if ( count($tagIds) > 0 ) {
		    $builder->join('accounttag as b', 'b.accid', '=', 'a.id')
                ->where('b.status', '=', 1)
                ->whereIn('b.tagid', $tagIds);
        }
		
		if( $request->has('aflids') ) 
		{
			$downlines = Affiliate::getAllDownlineById($request->aflids, true);

			if($downlines)
			{
				$downlines[] = $request->aflids;
				$builder->whereRaw(' aflid IN ('.implode($downlines,',').') ');
			}
			else
				$builder->whereRaw(' aflid IN ('.$request->aflids.') ');
		}
		

		if($this->start == -1) {
			$start = 0;
			$limit = 0;
		}

        $total = $builder->count(array('a.id'));

		$rows = array();
		
		$orderBy 	= $request->has('sortorder') 	 ? $request->sortorder 	   : 'desc';
        $orderField = $request->has('sortdatafield') ? 'a.'.$request->sortdatafield : 'a.id';
		

		if($accounts = $builder->skip($request->recordstartindex)->take( $request->pagesize )->orderBy($orderField,$orderBy)->get()){
			$affiliates = array();
			$channels = Channel::getAllAsOptions();
			foreach($accounts as $account){
				if($accountDetailObj = Accountdetail::where( 'accid' , '=' , $account->id )->first()) {
					
					$cashbalance = App::displayNumberFormat($account->cashbalance);
					if($account->cashbalance < 0) $cashbalance = '<font color="red">'.$cashbalance.'</font>';
					
					if(Session::has('affiliate_parentid'))
					{
						if(Session::get('affiliate_parentid') == 0)
						{
							$accid = '<a href="#" onclick="parent.addTab(\'Member: '.$account->nickname.'\', \'customer-tab?sid='.$account->id.'\')" style="cursor:pointer">'.$account->nickname.'</a>';
						}
						else
						{
							$accid = '<a href="#" onclick="parent.addTab(\'Member: '.$account->nickname.'\', \'customer-tab3?sid='.$account->id.'\')" style="cursor:pointer">'.$account->nickname.'</a>';
						}
					}
					else
					{
						$accid = '<a href="#" onclick="parent.addTab(\'Member: '.$account->nickname.'\', \'customer-tab?sid='.$account->id.'\')" style="cursor:pointer">'.$account->nickname.'</a>';
					}
					
					if( $accounttags = Accounttag::where( 'accounttag.status' , '=' , 1 )->whereAccid($account->id)->leftJoin('tag', 'accounttag.tagid', '=', 'tag.id')->get() )
					{
						foreach( $accounttags as $accounttag )
						{
                            if ($accounttag->category == Tag::CATEGORY_OPERATOR_TAG) {
                                $class = 'square';
                                $accid .= '<div class="square" style="border: 6px solid '.$accounttag->color.';"> </div>';
                            } else {
                                $accid .= '<div class="triangle" style="border-bottom-color:'.$accounttag->color.';"> </div>';
                            }

						}
					}

					$fullname = $accountDetailObj->fullname.$accountDetailObj->samename;
					$row = array('code' => $accid, 'fullname' => $fullname);

					if($account->agtid > 0 && !isset(self::$affiliateInstances[$account->agtid])) {
						if($aflObj = Agent::find($account->agtid))
						self::$affiliateInstances[$account->agtid] = $aflObj->code;
					}
					$row['agtid'] 		= (isset(self::$affiliateInstances[$account->agtid])?self::$affiliateInstances[$account->agtid]:'');

					$row['created'] 	= $account->created->toDateTimeString();
					$row['idpassport']  = $accountDetailObj->idpassport.$accountDetailObj->sameid;
					$row['crccode'] 	= $account->crccode;
					$row['cashbalance'] = $this->mainwallet( $account->id);
					
					$totaldeposit    	= Cashledger::whereAccid($account->id)->whereChtcode(1)->whereIn('status', array(CBO_LEDGERSTATUS_CONFIRMED, CBO_LEDGERSTATUS_MANCONFIRMED))->count();
					$totaldepositamt 	= Cashledger::whereAccid($account->id)->whereChtcode(1)->whereIn('status', array(CBO_LEDGERSTATUS_CONFIRMED, CBO_LEDGERSTATUS_MANCONFIRMED))->sum('amount');
					
					$totalwithdrawal    = Cashledger::whereAccid($account->id)->whereChtcode(2)->whereIn('status', array(CBO_LEDGERSTATUS_CONFIRMED, CBO_LEDGERSTATUS_MANCONFIRMED))->count();
					$totalwithdrawalamt = Cashledger::whereAccid($account->id)->whereChtcode(2)->whereIn('status', array(CBO_LEDGERSTATUS_CONFIRMED, CBO_LEDGERSTATUS_MANCONFIRMED))->sum('amount');
					
					$transaction 	= (int) $totaldeposit.    ' / '. App::displayAmount($totaldepositamt);
					$transactionamt = (int) $totalwithdrawal. ' / '. App::displayAmount($totalwithdrawalamt * -1);
					
					$row['deposit'] 	= $transaction;
					$row['withdrawal']  = $transactionamt;

					if ($canVIewContactInfo) {
                        $row['telephone'] = '-' . $accountDetailObj->telmobile;
                        $row['email'] = $accountDetailObj->email;
                    }

					$row['line']	    = $accountDetailObj->line;
					$lastbetdate = $account->lastbetdate;
					if($wgrObj = Wager::whereRaw('accid='.$account->id.' ORDER BY datetime DESC')->first()) {
						$lastbetdate = $wgrObj->datetime;
					}
					$row['lastbet'] = $lastbetdate;
					
					$lastdepositdate = '';
					if($clgObj = Cashledger::whereRaw('accid='.$account->id.' AND chtcode = '.CBO_CHARTCODE_DEPOSIT.' AND (status = '.CBO_LEDGERSTATUS_CONFIRMED.' or status = '.CBO_LEDGERSTATUS_MANCONFIRMED.' ) order by id DESC')->first()){
						$lastdepositdate = $clgObj->created->toDateTimeString();
					}
					$row['lastdeposit'] = $lastdepositdate;
					$row['status'] = $account->getStatusText($this->moduleName.'-active-suspend');
					$row['createdip'] = $accountDetailObj->createdip;
					
					$location = GeoIP::getLocation($accountDetailObj->createdip);
					
					if( Session::get('currency') == 'CNY' && $location['country'] == 'China')
					{
						if( $accountDetailObj->resstate != '' )
						{
							$location['country'] = $accountDetailObj->resstate;
							$location['city']    = $accountDetailObj->rescity;
						}
					}

					$row['location'] = $location['country'] . ' - ' . $location['city'] ;
					$row['regchannel'] = (isset($channels[$account->regchannel]))?$channels[$account->regchannel]:''; 
					
					$firsdepositdate = '';
					if($clgObj = Cashledger::whereRaw('accid='.$account->id.' AND chtcode = '.CBO_CHARTCODE_DEPOSIT.' AND (status = '.CBO_LEDGERSTATUS_CONFIRMED.' or status = '.CBO_LEDGERSTATUS_MANCONFIRMED.' ) order by id ASC')->first()){
						$firsdepositdate = $clgObj->created->toDateTimeString();
					}
					$row['firstdeposit'] = $firsdepositdate;
					
					if( $account->aflid != '0'){
						$row['referrer'] = Affiliate::whereId($account->aflid)->pluck('username');
					}
					
					$rows[] = $row;
				}
			}
		}
                
       echo json_encode(array('total' => $total, 'rows' => $rows));
	}
	
	 public static function mainwallet($id, $numberformat = true){
		
		$condition = '((status in ('.CBO_LEDGERSTATUS_PENDING.', '.CBO_LEDGERSTATUS_MANPENDING.','.CBO_LEDGERSTATUS_CONFIRMED.', '.CBO_LEDGERSTATUS_MANCONFIRMED.', '.CBO_LEDGERSTATUS_PROCESSED.') AND chtcode IN ('.CBO_CHARTCODE_WITHDRAWAL.', '.CBO_CHARTCODE_BALANCETRANSFER.', '.CBO_CHARTCODE_WITHDRAWALCHARGES.' )) OR (status in ('.CBO_LEDGERSTATUS_CONFIRMED.', '.CBO_LEDGERSTATUS_MANCONFIRMED.') AND chtcode NOT IN ('.CBO_CHARTCODE_WITHDRAWAL.', '.CBO_CHARTCODE_BALANCETRANSFER.' , '.CBO_CHARTCODE_WITHDRAWALCHARGES.' )))';

		$condition .= ' AND accid = '.$id;
		
		$balance = Cashledger::whereRaw($condition)->sum('amount');

		if( $numberformat == true){
			return number_format($balance, 2, '.', '');
		}else{
			return $balance;
		}
		
	}
	
	protected function doActivateSuspend(Request $request) {

			

		if($request->has('id')) {
		
			if($object = Account::find($request->id)) {
			
				$currentStatus = $object->status;
				if($request->action == 'activate') {
					$object->status 	= CBO_ACCOUNTSTATUS_ACTIVE;

					// Reset badlogin counter.
                    $today = Carbon::now()->toDateString();
                    $badloginObj = Badlogin::where('userid', '=', $object->id)
                        ->whereBetween('created', array($today . ' 00:00:00', $today . ' 23:59:59'))
                        ->where('type', '=', CBO_LOGINTYPE_USER)
                        ->first();

                    if ($badloginObj) {
                        $badloginObj->counter = 0;
                        $badloginObj->save();
                    }
				}
				if($request->action == 'suspend') {
					$object->status 	= CBO_ACCOUNTSTATUS_SUSPENDED;
				}

				if($currentStatus <> $object->status){
					if($object->save()) {
						//ProductApi::doUpdateStatus($object->id, $object->status);
						echo json_encode( array( 'code' => Lang::get('COMMON.SUCESSFUL') ) );
						exit;
					}
				}
			}
		}
		
		
		echo json_encode( array( 'code' => Lang::get('COMMON.FAILED') ) );

	}
	

	protected function doApproveRejectAccount(Request $request) {
		$id = array();

		if($request->has['sid']) {
			$id = explode('||', $request->input['sid']);
			if(count($id) == 0) {
				$id[] = $request->input['sid'];
			}
		}

		foreach($id as $accid) {
			if($object = Account::whereRaw('id='.$accid.' AND status='.CBO_ACCOUNTSTATUS_PENDINGACTIVATION)) {
				$currentStatus = $object->status;
				if($request->input['action'] == 'approve') {
					$object->status 	= CBO_ACCOUNTSTATUS_ACTIVE;
				}
				if($request->input['action'] == 'suspend') {
					$object->status 	= CBO_ACCOUNTSTATUS_SUSPENDED;
				}
	
				if($currentStatus <> $object->status){
					$object->save();
				}
			}
		}
		$this->showGrid($params);
	}
	
	protected function doGetExcel($params){
                //get parameters from doGetData
		$data= $this->doGetData($params, true, false);
		
		// Create new PHPExcel object
		$objPHPExcel = new PHPExcel();

		// Rename sheet
		$objPHPExcel->getActiveSheet()->setTitle('Member details');

		// Add some data
		$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getActiveSheet()->SetCellValue('A1', CBO_GUI_ACCOUNT);
		$objPHPExcel->getActiveSheet()->SetCellValue('B1', CBO_GUI_MEMBERNAME);
		$objPHPExcel->getActiveSheet()->SetCellValue('C1', CBO_GUI_CHANNEL);
		$objPHPExcel->getActiveSheet()->SetCellValue('D1', CBO_GUI_CURRENCY);
		$objPHPExcel->getActiveSheet()->SetCellValue('E1', CBO_GUI_MAINWALLET);
		$objPHPExcel->getActiveSheet()->SetCellValue('F1', CBO_GUI_DEPOSIT);
		$objPHPExcel->getActiveSheet()->SetCellValue('G1', CBO_GUI_WITHDRAWAL);
		$objPHPExcel->getActiveSheet()->SetCellValue('H1', CBO_GUI_TELEPHONENO);
		$objPHPExcel->getActiveSheet()->SetCellValue('I1', CBO_GUI_CTABEMAIL);
		$objPHPExcel->getActiveSheet()->SetCellValue('J1', CBO_GUI_REGISTEREDON);
		$objPHPExcel->getActiveSheet()->SetCellValue('K1', CBO_GUI_LASTDEPOSIT);
		$objPHPExcel->getActiveSheet()->SetCellValue('L1', CBO_GUI_STATUS);
		$objPHPExcel->getActiveSheet()->SetCellValue('M1', CBO_GUI_IPADDRESS);
		$objPHPExcel->getActiveSheet()->SetCellValue('N1', CBO_GUI_LOCATION);

		foreach ($data['rows'] as $key => $value){
		    $rowID = $key + 2;
		    $columnID = 'A';
		    $objPHPExcel->getActiveSheet()->setCellValue($columnID++.$rowID, strip_tags($value['code']));
		    $objPHPExcel->getActiveSheet()->setCellValue($columnID++.$rowID, $value['fullname']);
		    $objPHPExcel->getActiveSheet()->setCellValue($columnID++.$rowID, $value['regchannel']);
		    $objPHPExcel->getActiveSheet()->setCellValue($columnID++.$rowID, $value['crccode']);
		    $objPHPExcel->getActiveSheet()->setCellValue($columnID++.$rowID, $value['cashbalance']);
		    $objPHPExcel->getActiveSheet()->setCellValue($columnID++.$rowID, $value['deposit']);
		    $objPHPExcel->getActiveSheet()->setCellValue($columnID++.$rowID, $value['withdrawal']);
		    $objPHPExcel->getActiveSheet()->setCellValue($columnID++.$rowID, $value['telephone']);
		    $objPHPExcel->getActiveSheet()->setCellValue($columnID++.$rowID, $value['email']);
		    $objPHPExcel->getActiveSheet()->setCellValue($columnID++.$rowID, $value['created']);
		    $objPHPExcel->getActiveSheet()->setCellValue($columnID++.$rowID, $value['lastdeposit']);
		    $objPHPExcel->getActiveSheet()->setCellValue($columnID++.$rowID, strip_tags($value['status']));
		    $objPHPExcel->getActiveSheet()->setCellValue($columnID++.$rowID, $value['createdip']);
		    $objPHPExcel->getActiveSheet()->setCellValue($columnID++.$rowID, $value['location']);
		}

		// Auto resize the columns width
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
		
		//Set font style and alignment
		$objPHPExcel->getActiveSheet()->getStyle('A1:N1')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('A1:N1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->getActiveSheet()->getStyle('A2:A'.$rowID)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->getActiveSheet()->getStyle('B2:B'.$rowID)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->getActiveSheet()->getStyle('C2:C'.$rowID)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->getActiveSheet()->getStyle('D2:D'.$rowID)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->getActiveSheet()->getStyle('E2:E'.$rowID)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
		$objPHPExcel->getActiveSheet()->getStyle('F2:F'.$rowID)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
		$objPHPExcel->getActiveSheet()->getStyle('G2:G'.$rowID)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
		$objPHPExcel->getActiveSheet()->getStyle('H2:H'.$rowID)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->getActiveSheet()->getStyle('I2:I'.$rowID)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->getActiveSheet()->getStyle('J2:J'.$rowID)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->getActiveSheet()->getStyle('K2:K'.$rowID)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->getActiveSheet()->getStyle('L2:L'.$rowID)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->getActiveSheet()->getStyle('M2:M'.$rowID)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->getActiveSheet()->getStyle('N2:N'.$rowID)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$timestamp = strtotime("now");
		//Create excel
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save(APPROOT.'/download/tmp/Member_Enquiry['.$timestamp.'].xlsx');
		
		//upload to aws s3
		MediaObject::s3Upload('excel','Member_Enquiry['.$timestamp.'].xlsx',true);
		
		echo json_encode(array('success' => true));
		exit;
    }
	public function showonlinememberlist(Request $request){
		$now = Carbon::now()->toDateTimeString();;
		$sub30Minutes=Carbon::now()->subMinutes(30)->toDateTimeString();
		
//		$data['onlinememberlist'] = DB::select( DB::raw("SELECT DISTINCT(username) FROM `onlinetracker` WHERE `modified` BETWEEN '".$sub30Minutes."' AND '".$now."' AND type=1"));

        $data['onlinememberlist'] = DB::table('onlinetracker as a')
            ->join('account as b', 'b.id', '=', 'a.userid')
            ->groupBy('a.userid')
            ->whereBetween( 'a.modified' , array($sub30Minutes, $now))
            ->where( 'b.crccode' , '=' , Session::get('currency'))
            ->where( 'a.type' , '=' , 1)
            ->get(array('b.nickname as username'));

		return view('admin.onlinemember',$data);
	}
}
