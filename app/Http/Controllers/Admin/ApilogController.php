<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller\Admin;
use App\libraries\grid\PanelGrid;
use App\libraries\App;
use Illuminate\Http\Request;
use Auth;
use Validator;
use Session;
use Lang;
use App\Models\Onlinetrackerlog;
use App\Models\Currency;
use App\Models\Fundingmethod;
use App\Models\Cashledger;
use App\Models\Bank;
use App\Models\Accounttag;
use App\Models\Tag;
use App\Models\Account;
use App\Models\Config;
use App\Models\Apilog;
use App\Models\Cashledgerlog;
use App\Models\Bankaccount;
use App\Models\Promocash;
use App\Models\Rejectreason;
use App\Models\Banktransfer;
use App\Models\Promocampaign;

class ApilogController extends Controller{

	protected $moduleName 	= 'apilog';
	protected $limit 		= 20;
	protected $start 		= 0;
	
	public function __construct()
	{
					
	}
	
	public function index(Request $request){

			$grid = new PanelGrid;
			$grid->setupGrid($this->moduleName, 'apilog-data', $this->limit);
			$grid->setTitle(Lang::get('COMMON.APILOG'));
									
			
			$grid->addColumn('acccode', 			Lang::get('COMMON.ACCOUNT'), 					100,		array('align' => 'center'));
			$grid->addColumn('request',                     Lang::get('COMMON.REQUEST'),                                    600,            array('align' => 'left'));
			$grid->addColumn('return',                      Lang::get('COMMON.RETURN'), 					400,            array('align' => 'left'));	
			$grid->addColumn('created',                     Lang::get('COMMON.CREATEDON'),                                  150,            array('align' => 'left'));
			$grid->addColumn('modified',                    Lang::get('COMMON.MODIFIEDON'), 				150,            array('align' => 'left'));
			
                       $grid->addSearchField('search', array('acccode' => Lang::get('COMMON.ACCOUNT') , 'request' => Lang::get('COMMON.REQUEST'), 'return' => Lang::get('COMMON.RETURN')));
			
		
			
			$data['grid'] = $grid->getTemplateVars();

			return view('admin.grid2',$data);
    }
	
	protected function doGetData(Request $request){

		$rows = array();
		
		$condition = '1';
                //$date_filter = true;
		$aflt  = $request->input('aflt');
                
		if( isset($aflt['acccode']) && $aflt['acccode'] != ''){
			$condition .= ' AND acccode LIKE "%'.$aflt['acccode'].'%" ';
		}

		if( isset($aflt['request']) && $aflt['request'] != ''){
			$condition .= ' AND request LIKE "%'.$aflt['request'].'%" ';
		}
                
                if( isset($aflt['return']) && $aflt['return'] != ''){
			$condition .= ' AND return LIKE "%'.$aflt['return'].'%" ';
		}
                
		if( $request->has('createdfrom') ){
			$condition .= ' AND created >= "'.$request->get('createdfrom').' 00:00:00" AND created <= "'.$request->get('createdto').' 23:59:59" ';
		}
		
                if( $request->has('modifiedfrom') ){
			$condition .= ' AND modified >= "'.$request->get('modifiedfrom').' 00:00:00" AND modified <= "'.$request->get('modifiedto').' 23:59:59" ';
		}
		
		$total = Apilog::whereRaw($condition)->count();
		
		

		//new structure
		if($apilogs = Apilog::whereRaw($condition)->skip($request->recordstartindex)->take( $request->pagesize )->orderBy('id','desc')->get()){
			foreach( $apilogs as $apiog ){
		
				$rows[] = array(

					'id'  				=> $apiog->id, 
					'acccode'  			=> $apiog->acccode, 
					'request'                       => $apiog->request, 
					'return'                        => $apiog->return, 
					'created'  			=> $apiog->created->toDateTimeString(), 
                                        'modified'  			=> $apiog->modified->toDateTimeString(),
				);	
				
			}
				
		}

				
		echo json_encode(array('total' => $total , 'rows' => $rows));
		exit;
	
	}


}

?>
