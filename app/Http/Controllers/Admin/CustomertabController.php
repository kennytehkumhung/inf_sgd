<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller\Admin;
use App\Http\Controllers\User\WalletController;
use App\libraries\grid\PanelGrid;
use App\libraries\App;
use App\libraries\providers\MXB;
use App\libraries\sms\SMS88;
use App\Models\Paymentgateway;
use App\Models\Paymentgatewaysetting;
use App\Models\Productwallet;
use App\Models\Smslog;
use Carbon\Carbon;
use Config;
use Illuminate\Http\Request;
use Auth;
use Validator;
use Session;
use Lang;
use Crypt;
use Hash;
use GeoIP;
use App\Models\Currency;
use App\Models\Channel;
use App\Models\Fundingmethod;
use App\Models\Cashledger;
use App\Models\Bank;
use App\Models\Accounttag;
use App\Models\Tag;
use App\Models\Account;
use App\Models\Accountdetail;
use App\Models\Configs;
use App\Models\Agent;
use App\Models\Website;
use App\Models\Product;
use App\Models\Balancetransfer;
use App\Models\Bankaccount;
use App\Models\Promocash;
use App\Models\Promocampaign;
use App\Models\Cashbalance;
use App\Models\Memo;
use App\Models\Accountbank;
use App\Models\User;
use App\Models\Affiliate;
use App\Models\Onlinetrackerlog;
use App\Models\Bankholder;
use App\Models\Inbox;
use App\Models\Toallinbox;
use App\Models\Media;

class CustomertabController extends Controller{

	protected $moduleName 	= 'customer';
	protected $limit 		= 20;
	protected $start 		= 0;

	public function __construct()
	{

	}

	public function index(Request $request)
	{
                if($request->has('sid'))
                    $data = array('sid' => $request->input('sid'));

		return view('admin.customertab',$data);
	}

	public function memberdetail(Request $request)
	{
        if($request->has('sid'))
            $id = $request->input('sid');

        if($accObj = Account::where( 'id' , '=' , $id  )->first()){
            $accountdetail = $accObj->getAccountDetailObject();
            $agentObj = Agent::where( 'id' , '=' , $accObj->agtid )->first();
			$agent = $agentObj->code;

            $prdname = array();
            $avaiable_products = array();
            $suspended_products = array();
            if($wbsObj = Website::where('id','=',$accObj->wbsid)->first()){
                $products = unserialize($wbsObj->products);
                foreach($products as $product){
                    if($prdObj = Product::where('id','=',$product)->whereRaw('status != 0')->first()){
                        $prdname[$product] = $prdObj->name;
						$avaiable_products[$product] = true;
					}
                }
            }

			if( $accObj->products != '' )
			{

				$temps = unserialize($accObj->products);
				foreach( $temps as $value ){
					$avaiable_products[$value] = false;
				}

			}

			if ( $accObj->suspendedproducts != '' )
            {
                $temps = unserialize($accObj->suspendedproducts);
                foreach( $temps as $value ){
                    $suspended_products[$value] = true;
                }
            }

            if($accountdetail->gender == 1) $gender = 'female';
            else $gender = 'male';

			if($aflObj = Affiliate::find($accObj->aflid)) {
				$agent = $aflObj->username;
			}

            $data['showcontactinfo'] = $this->hasPermission('MEMBER', 'MEMBERENQUIRY', 'VIEWCONTACTINFO');
			$data['showcashledgerinfo'] = $this->hasPermission('MEMBER', 'MEMBERENQUIRY', 'VIEWCASHLEDGERINFO');
            $data['mainwallet'] = $this->mainwallet($accObj->id);
            $data['id'] = $accObj->id;
            $data['avaiable_products'] = $avaiable_products;
            $data['suspended_products'] = $suspended_products;
            $data['acccode'] = $accObj->code;
            $data['fullname'] = $accObj->fullname;
            $data['crccode'] = $accObj->crccode;
            $data['currencyOptions'] = Currency::getAllCurrencyAsOptions();
            $data['utm_medium'] = $accObj->utm_medium;
            $data['status'] = $accObj->getStatusText();
            $data['totaldepositamount']    = App::displayAmount(Cashledger::whereRaw('chtcode = 1 AND status IN (1,4) AND accid = '.$accObj->id)->sum('amount'));
            $data['totalwithdrawalamount'] = App::displayAmount(Cashledger::whereRaw('chtcode = 2 AND status IN (1,4) AND accid = '.$accObj->id)->sum('amount'));
            $data['totalbonusamount'] = App::displayAmount(Cashledger::whereRaw('chtcode = 3 AND status IN (1,4) AND accid = '.$accObj->id)->sum('amount'));
            $data['totalrebateamount'] = App::displayAmount(Cashledger::whereRaw('chtcode = 9 AND status IN (1,4) AND accid = '.$accObj->id)->sum('amount'));
            $data['withdrawlimit'] = $accObj->withdrawlimit;
            $data['dob'] = $accountdetail->dob;
            $data['gender'] = $gender;
            $data['telmobile'] = $accountdetail->telmobile;
            $data['email'] = $accountdetail->email;
            $data['agtcode'] = $agent;
            $data['products'] = $prdname;
            $data['created'] = $accObj->created;
            $data['lastlogin'] = $accObj->lastlogin;
			$location = GeoIP::getLocation($accObj->createdip);
			$data['createdip'] = $accObj->createdip. ' [ ' . $location['country'] . ' - ' . $location['city'] . ' ] ' ;


			$data['maxdepositamt'] = $accObj->maxdepositamt;
			$data['maxwithdrawamt'] = $accObj->maxwithdrawamt;
			$data['channel'] = Channel::where( 'id' , '=' , $accObj->regchannel)->pluck('name');

			$data['lastloginip'] = '';

			$data['bo_whatsapp_telnum'] = Configs::getParam('SYSTEM_BO_WHATSAPP_TELNUM');

			// Check if project contains SCR product.
			if (Product::where('code', '=', 'SCR')->count() > 0) {
			    //Show SCR ID for CS for search member details in SCR BO.
                $data['scrid'] = $accObj->scrid;
            }

			if($onlObj = Onlinetrackerlog::whereRaw('type='.CBO_LOGINTYPE_USER.' AND userid='.$accObj->id.' ORDER BY created DESC')->first()){
				$location = GeoIP::getLocation($onlObj->clientip);
				$data['lastloginip'] = $onlObj->clientip. ' [ ' . $location['country'] . ' - ' . $location['city'] . ' ] ' ;
				$data['lastlogin']   = $onlObj->created->toDateTimeString();
			}


			if($tagResults = Tag::whereRaw('status = '.CBO_TAGSTATUS_ACTIVE.' AND category = '.Tag::CATEGORY_CUSTOMER_TAG)->get()) {
				$tags = array();
				foreach($tagResults as $tagdata) {
					$acctagResult = Accounttag::whereRaw('status = '.CBO_TAGSTATUS_ACTIVE.' AND tagid = '.$tagdata->id.' AND accid = '.$accObj->id)->count();

					if(Accounttag::whereRaw('status = '.CBO_TAGSTATUS_ACTIVE.' AND tagid = '.$tagdata->id.' AND accid = '.$accObj->id)->count() >= 1)
						$checked = 'checked';
					else
						$checked = '';

					$tags[] = array('id' => $tagdata->id, 'color' => $tagdata->color, 'name' => $tagdata->name, 'checked' => $checked);
				}
				$data['flags'] = $tags;
			}

            if($tagResults = Tag::whereRaw('status = '.CBO_TAGSTATUS_ACTIVE.' AND category = '.Tag::CATEGORY_OPERATOR_TAG)->get()) {
                $tags = array();
                foreach($tagResults as $tagdata) {
                    $acctagResult = Accounttag::whereRaw('status = '.CBO_TAGSTATUS_ACTIVE.' AND tagid = '.$tagdata->id.' AND accid = '.$accObj->id)->count();

                    if(Accounttag::whereRaw('status = '.CBO_TAGSTATUS_ACTIVE.' AND tagid = '.$tagdata->id.' AND accid = '.$accObj->id)->count() >= 1)
                        $checked = 'checked';
                    else
                        $checked = '';

                    $tags[] = array('id' => $tagdata->id, 'color' => $tagdata->color, 'name' => $tagdata->name, 'checked' => $checked);
                }
                $data['categoryTags'] = $tags;
            }

			$bankprofileOptions = array();
			if( $bankprofiles = Bankholder::whereRaw('crccode = "'.Session::get('admin_crccode').'"')->get() ){

				foreach( $bankprofiles as $bankprofile ){

					$bankaccname = '';

					if( $bankAccounts = Bankaccount::whereRaw(' bhdid = '.$bankprofile->id)->get() )
					{
						foreach( $bankAccounts as $bankAccount )
						{
							$bankaccname .= $bankAccount->bankaccname;
						}
					}

					$bankprofileOptions[$bankprofile->id] = $bankaccname;

				}

			}

			$data['bankprofileOptions'] = $bankprofileOptions;
			$data['bhdid'] = $accObj->bhdid == 0 ? Agent::where( 'id' , '=' , $accObj->agtid )->pluck('bhdid') : $accObj->bhdid;

			if( $accObj->crccode == "TWD" )
			{
				$data['media'] = Media::whereRefobj('Bankbook')->whereRefid($accObj->id)->first();
			}

        }

		return view('admin.tab1',$data);

	}

	public function transaction(Request $request)
	{

		$grid = new PanelGrid;
		$grid->setupGrid($this->moduleName, 'customer-tab2-data', $this->limit, true , array('multiple' => false,'checkbox'=>false,'checkbox_url'=> 'cashledger-batchapprove' , 'footer' => true));
		$grid->setTitle(Lang::get('COMMON.REMARK'));

		$grid->addColumn('created', 		Lang::get('COMMON.DATE'), 				125,		array('align' => 'left'));
		$grid->addColumn('id', 				Lang::get('COMMON.ID'), 				100,		array('align' => 'left'));
		$grid->addColumn('payment', 		Lang::get('COMMON.PAYMENTMETHOD'), 		250,		array('align' => 'center'));
		$grid->addColumn('status', 			Lang::get('COMMON.STATUS'), 			100,		array('align' => 'center'));
		$grid->addColumn('currency', 		Lang::get('COMMON.CURRENCY'), 			100,		array('align' => 'center'));
		$grid->addColumn('credit', 			Lang::get('COMMON.CREDIT'), 			68,			array('align' => 'center'));
		$grid->addColumn('debit', 			Lang::get('COMMON.DEBIT'), 				68,			array('align' => 'center'));
		$grid->addColumn('balance', 		Lang::get('COMMON.BALANCE'), 			100,		array('align' => 'center'));

		if( $request->has('sid') )
		{
			$accountObj = Account::find($request->input('sid'));
			$grid->addSearchField('search1', array('acccode' => Lang::get('COMMON.MEMBERACCOUNT')) ,array('value' => $accountObj->nickname));
			$grid->addSearchField('search2', array('crccode' => Lang::get('COMMON.CURRENCY')) ,array('value' => $accountObj->crccode));
			$grid->addFilter('chtcode', Cashledger::getAllTypeAsOptions(array(CBO_CHARTCODE_BALANCETRANSFER)), array('width' => 300, 'display' => Lang::get('COMMON.TYPE'), 'multiple' => true, 'select' => false, 'values' => json_encode(array(CBO_CHARTCODE_DEPOSIT, CBO_CHARTCODE_WITHDRAWAL, CBO_CHARTCODE_BONUS, CBO_CHARTCODE_ADJUSTMENT, CBO_CHARTCODE_INCENTIVE))));
		}


		$data['grid'] = $grid->getTemplateVars();
		
		if(Session::has('admin_username'))
		{
			if (!$this->hasPermission('MEMBER', 'MEMBERENQUIRY', 'VIEWFULLTRANSACTION')) {
				$data['customDateOptions'] = '<a onclick="doChangeDate(\'now\');" href="javascript:void(0);">+0D</a> | <a onclick="doChangeDate(\'m1dy\');" href="javascript:void(0);">-1D</a>';
			}
		}

		return view('admin.grid2',$data);

	}

	protected function TransactiondoGetData(Request $request){

		$rows = array();

		$fdMethods = array();

		$aflt   = $request->input('aflt');
		$aqfld  = $request->input('aqfld');

		$createdFrom = $request->createdfrom;
		$createdTo = $request->createdto;

		if(Session::has('admin_username'))
		{
			if (!$this->hasPermission('MEMBER', 'MEMBERENQUIRY', 'VIEWFULLTRANSACTION')) {
				$createdFrom = Carbon::parse($createdFrom);
				$createdTo = Carbon::parse($createdTo);

				$yesterday = Carbon::now()->addDays(-1)->setTime(0, 0, 0);
				$today = Carbon::now()->setTime(23, 59, 59);

				if (!$createdFrom->between($yesterday, $today)) {
					$createdFrom = $yesterday;
				}

				if (!$createdTo->between($yesterday, $today)) {
					$createdTo = $today;
				}

				$createdFrom = $createdFrom->toDateString();
				$createdTo = $createdTo->toDateString();
			}
		}

		$condition = ' created >= "'.$createdFrom.' 00:00:00" AND created <= "'.$createdTo.' 23:59:59" ';
		//Session::get('admin_crccode') == 'MYR' ? '' : ' AND crccode = "'.Session::get('admin_crccode').'"';

		if( isset($aflt['acccode'])  ){
			$accObj = Account::where( 'nickname' ,'=' ,$aflt['acccode']  )->where('crccode','=',$aflt['crccode'])->first();
			$condition .= 'AND refObj != "Balancetransfer" AND accid ='.$accObj->id;

		}

		if( isset($aqfld['chtcode']) && $aqfld['chtcode'] != '' ){
			$condition .= ' AND chtcode = "'.$aqfld['chtcode'].'" ';
		}

		$total_credit = 0;
		$total_debit  = 0;

		$total = Cashledger::whereRaw($condition)->count();


		if($ledgers = Cashledger::whereRaw($condition)->take( $request->pagesize )->skip( $request->recordstartindex )->orderBy('id','desc')->get()){

            $pgObjCache = array();

			foreach( $ledgers as $ledger ){
				$credit = $debit = '';



				if($ledger->amount > 0) {
					if(CBO_LEDGERSTATUS_MANCANCELLED == $ledger->status || CBO_LEDGERSTATUS_CANCELLED == $ledger->status || CBO_LEDGERSTATUS_MEMCANCELLED == $ledger->status)
						$credit = '<del>'.App::displayAmount($ledger->amount).'</del>';
					else
					{
						$credit = App::displayAmount($ledger->amount);
					}

					if(CBO_LEDGERSTATUS_CONFIRMED == $ledger->status || CBO_LEDGERSTATUS_MANCONFIRMED == $ledger->status ){
						$total_credit += $ledger->amount;
					}
				}
				if($ledger->amount < 0) {
					if(CBO_LEDGERSTATUS_MANCANCELLED == $ledger->status || CBO_LEDGERSTATUS_CANCELLED == $ledger->status || CBO_LEDGERSTATUS_MEMCANCELLED == $ledger->status)
						$debit = '<del>'.App::displayAmount($ledger->amount).'</del>';
					else
					{
						$debit = App::displayAmount($ledger->amount);

					}

					if(CBO_LEDGERSTATUS_CONFIRMED == $ledger->status || CBO_LEDGERSTATUS_MANCONFIRMED == $ledger->status ){
						$total_debit += $ledger->amount;
					}

				}

				$paymentMethod = '';
				$bankOptions = Bank::getAllAsOptions(true);
                $txCount = '';

				if($ledger->chtcode == CBO_CHARTCODE_DEPOSIT){
					//$txCount = $depositCount;
					$action = 'deposit';
					$fdm ='';
					if($ledger->fdmid == Configs::getParam("SYSTEM_PAYMENT_ID_PG")) {
                        $instanttransflag = ($ledger->instanttransflag == Cashledger::INSTANT_TRANS_FLAG_HIDDEN ? Lang::get('COMMON.AUTO') : Lang::get('COMMON.MANUAL'));

                        if (!array_key_exists($ledger->refid, $pgObjCache)) {
                            $pgObj = Paymentgateway::find($ledger->refid);
                            $pgData = false;

                            if ($pgObj) {
                                $pgsObj = Paymentgatewaysetting::find($pgObj->paytype);

                                if ($pgsObj) {
                                    $pgData = array(
                                        'refid' => $ledger->refid,
                                        'real_name' => $pgsObj->real_name,
                                        'paybnkoption' => $pgObj->paybnkoption,
                                    );
                                }
                            }

                            $pgObjCache[$ledger->refid] = $pgData;
                        }

                        $pgObjCacheVal = $pgObjCache[$ledger->refid];

                        if($pgObjCacheVal !== false) {
                            $txCount = $instanttransflag . ' - ' . $pgObjCacheVal['real_name'] . ' - ' . $pgObjCacheVal['paybnkoption'] . ' - ' . $pgObjCacheVal['refid'];
                        }
					} else if($ledger->fdmid ==  Configs::getParam("SYSTEM_PAYMENT_ID_BANKTRANSFER")) {
						$accno = '';
						$txCount = Bank::whereId($ledger->bnkid)->pluck('shortname');
						//$refObj = new $cashLedger->refobj;
						if(!isset($bankAccounts[$ledger->bacid])) {
							if($bacObj = Bankaccount::find($ledger->bacid))
							$bankAccounts[$ledger->bacid] = $bacObj;
						}
						if(isset($bankAccounts[$ledger->bacid])) {
							$accno = substr($bankAccounts[$ledger->bacid]->bankaccno, -4, 4);
						}
						$txCount .= ' - '.$accno;
					}
				}elseif($ledger->chtcode == CBO_CHARTCODE_WITHDRAWAL){
					$action = 'withdraw';
					$txCount =  Bank::whereId($ledger->bnkid)->pluck('shortname');
					$fdm ='';
				}elseif($ledger->chtcode == CBO_CHARTCODE_BONUS){
					$action = 'deposit';$txCount = ''; $fdm = Lang::get('COMMON.BONUS');
					if($refobj = Promocash::find($ledger->refid)) {
						if($pcpObj = Promocampaign::find($refobj->pcpid))
						$fdm .= ' ['.$pcpObj->code.']';
					}
				}elseif($ledger->chtcode == CBO_CHARTCODE_INCENTIVE){
					$action = 'deposit';
					$txCount = '';
					$fdm = Lang::get('COMMON.INCENTIVE');
				}elseif($ledger->chtcode == CBO_CHARTCODE_ADJUSTMENT){
					$action = 'adjustment';
					$txCount = '';
					$fdm = Lang::get('COMMON.ADJUSTMENT');
				}else{
					$txCount = '';
					$fdm ='';
				}

				if(!isset($fdMethods[$ledger->fdmid])){
					if($fdmObj = Fundingmethod::find($ledger->fdmid)){
						$paymentMethod = $fdmObj->name;
						$fdMethods[$ledger->fdmid] = $paymentMethod;
						$paymentMethod .= ' ['.$txCount.']';
					}else{
						if($ledger->refobj == 'Withdrawcard')
						{
							$paymentMethod = 'Withdrawcard';
						}
						elseif( $ledger->refobj == 'Cashcard' )
						{
							$paymentMethod = 'Cashcard';
						}
						else
						{
							$paymentMethod = $fdm;
						}
					}
				}else{
					$paymentMethod = $fdMethods[$ledger->fdmid];
					$paymentMethod .= ' ['.$txCount.']';
				}

				$rows[] = array(

					'created'  	=> $ledger->created->toDateTimeString(),
					'id'		=> $ledger->getTransactionId(),
					'payment'	=> $paymentMethod,
					'currency' 	=> $ledger->crccode,
					'credit' 	=> $credit,
					'debit' 	=> $debit,
					'balance' 	=> App::displayAmount($ledger->cashbalance),
					'status'	=> '<span style="color:'.$ledger->getStatusTextColorCode().';">'.$ledger->getStatusText().'</span>',

				);

			}

		}

		$footer[] = array('credit'=> App::displayAmount($total_credit).'<br>'.App::displayAmount(Cashledger::whereRaw($condition.' and amount > 0 and status IN (1,4)')->sum('amount')),'debit'=> App::displayAmount($total_debit).'<br>'.App::displayAmount(Cashledger::whereRaw($condition.' and amount < 0 and status IN (1,4)')->sum('amount')));

		echo json_encode(array('total' => $total, 'rows' => $rows, 'footer' => $footer));
		exit;

	}

	public function memberpnl(Request $request)
	{


		$data = array();
		return view('admin.grid2',$data);

	}

	public function tool(Request $request)
	{

		$grid = new PanelGrid;
		$grid->setupGrid($this->moduleName, 'customer-tab4-data', $this->limit);
		$grid->setTitle(Lang::get('COMMON.TOOL'));

		$grid->addColumn('date', 			Lang::get('COMMON.DATE'), 				150,		array('align' => 'left'));
		$grid->addColumn('from', 			Lang::get('COMMON.FROM'), 				80,			array('align' => 'center'));
		$grid->addColumn('to', 				Lang::get('COMMON.TO'), 				80,			array('align' => 'center'));
		$grid->addColumn('currency', 		Lang::get('COMMON.CURRENCY'), 			65,			array('align' => 'center'));
		$grid->addColumn('amount', 			Lang::get('COMMON.AMOUNT'), 			70,			array('align' => 'right'));
		$grid->addColumn('status', 			Lang::get('COMMON.STATUS'), 			70,			array('align' => 'center'));
		$grid->addColumn('revert', 			Lang::get('COMMON.REVERT'), 			200,			array('align' => 'center'));

		if( $request->has('sid') )
		{
			$accountObj = Account::find($request->input('sid'));
			$grid->addSearchField('search1', array('acccode' => Lang::get('COMMON.MEMBERACCOUNT')) ,array('value' => $accountObj->nickname));
			$grid->addSearchField('search2', array('crccode' => Lang::get('COMMON.CURRENCY')) ,array('value' => $accountObj->crccode));
		}


		$data['grid'] = $grid->getTemplateVars();

		return view('admin.grid2',$data);

	}

	protected function TooldoGetData(Request $request){

		$rows = array();

		//status
		$status[1] = 'Success';
		$status[2] = 'Failed';
		$status[0] = 'Failed';

		$aflt   = $request->input('aflt');
                $createdFrom = $request->createdfrom;
		$createdTo = $request->createdto;

                $condition = ' created >= "'.$createdFrom.' 00:00:00" AND created <= "'.$createdTo.' 23:59:59" ';
                
		if( isset($aflt['acccode'])  ){
			$accObj = Account::where( 'nickname' ,'=' ,$aflt['acccode']  )->where('crccode','=',$aflt['crccode'])->first();
			$condition .= ' AND accid='.$accObj->id;

		}

		$total = Balancetransfer::whereRaw($condition)->count();

		//new structure
		if($bals = Balancetransfer::whereRaw($condition)->skip($request->recordstartindex)->take( $request->pagesize )->orderBy('id','desc')->get()){
			foreach( $bals as $bal ){

				$revert = '';

				$ProductOptions = Product::getAllAsOptions(true);

				if($bal->from == Cashbalance::BALANCETYPE_MAIN) {
					$prodMethods = Lang::get('COMMON.MAINWALLET');
				} else $prodMethods = (isset($ProductOptions[$bal->from]))?$ProductOptions[$bal->from]:'';

				if($bal->to == Cashbalance::BALANCETYPE_MAIN) {
					$prodMethods1 = Lang::get('COMMON.MAINWALLET');
				} else $prodMethods1 = (isset($ProductOptions[$bal->to]))?$ProductOptions[$bal->to]:'';

				if( $bal->status == 0  )
				{
					$revert = '<a onclick="return doConfirmAction(\'revert-balance\', \'revert\', \''.$bal->id.'\')" href="#">Revert</a>';
				}else{

					if( $modifiedby = Cashledger::whereChtcode(CBO_CHARTCODE_REVERT)->whereStatus(1)->whereRefid($bal->id)->pluck('modifiedby') )
					{
						$revert = 'Reverted By '.User::whereId($modifiedby)->pluck('username');
					}

				}



				$rows[] = array(

					'date' 			=> $bal->created->toDateTimeString(),
					'from' 			=> $prodMethods,
					'to' 			=> $prodMethods1,
					'currency' 		=> $bal->crccode,
					'amount' 		=> App::displayAmount($bal->amount),
					'status' 		=> $status[$bal->status],
					'revert' 		=> $revert,

				);

			}

		}

		echo json_encode(array('total' => $total , 'rows' => $rows));
		exit;

	}

	public function revertBalance(Request $request){

		if( $balance = Balancetransfer::find($request->id) )
		{
			if( Cashledger::whereChtcode(CBO_CHARTCODE_REVERT)->whereStatus(1)->whereRefid($request->id)->count() == 0 )
			{
				$accObj = Account::find($balance->accid);

				$cashLedgerObj = new Cashledger;
				$cashLedgerObj->accid 	 		= $balance->accid;
				$cashLedgerObj->acccode		    = $balance->acccode;
				$cashLedgerObj->accname			= $accObj->fullname;
				$cashLedgerObj->chtcode 		= CBO_CHARTCODE_REVERT;
				$cashLedgerObj->amount 			= $balance->amount;
				$cashLedgerObj->amountlocal 	= $balance->amountlocal;
				$cashLedgerObj->refid 			= $balance->id;
				$cashLedgerObj->status 			= CBO_LEDGERSTATUS_CONFIRMED;
				$cashLedgerObj->refobj 			= 'Balancetransfer';
				$cashLedgerObj->crccode 		= $balance->crccode;
				$cashLedgerObj->crcrate 		= Currency::getCurrencyRate($balance->crccode);
				$cashLedgerObj->createdby 		= Session::get('admin_userid');
				$cashLedgerObj->modifiedby 		= Session::get('admin_userid');
				if($cashLedgerObj->save()){

					$balance->modifiedby = Session::get('admin_userid');
					$balance->status     = 1;
					if($balance->save()){

						echo json_encode( array( 'code' => Lang::get('COMMON.SUCESSFUL') ) );
						exit;
					}
				}
			}

		}

		echo json_encode( array( 'code' => Lang::get('COMMON.FAILED') ) );


	}

    public function getMemberBalance(Request $request, $product){
                $wallets = array();
                $status = array();
                if($request->has('product')) $product = $request->get('product');
                if($request->has('sid')) $id = $request->get('sid');

                $prdObj = Product::where('id','=',$product)->first();
                $accObj = Account::where('id','=',$id)->first();

                //get balance from mainwallet
				$mainwallet = $this->mainwallet($id);

                //get balance from player's game wallet
				$className = "App\libraries\providers\\".$prdObj->code;
				$obj = new $className;




                if($prdObj->getStatusText() == Lang::get('COMMON.MAINTENANCE')){
                    $wallets[$prdObj->code] = App::displayAmount(0);
                    $status[] = $prdObj->getStatusText();
                }
                else{
					$balance = $obj->getBalance( $accObj->nickname, $accObj->crccode);
                    $balance = ($balance == false) ? 0 : $balance;
                    $balance = $accObj->crccode == 'VND' || $accObj->crccode == 'IDR' ? $balance * 1000 : $balance;

                    $wallets[$prdObj->code] = (string)$balance;
					$status[] = $prdObj->getStatusText();
                }

                echo json_encode(array('mainwallet' => $mainwallet,'wallet' => $wallets,'status' => $status,'prdid' => $product));
                exit;
        }

    public static function mainwallet($id, $numberformat = true){

		$condition = '((status in ('.CBO_LEDGERSTATUS_PENDING.', '.CBO_LEDGERSTATUS_MANPENDING.','.CBO_LEDGERSTATUS_CONFIRMED.', '.CBO_LEDGERSTATUS_MANCONFIRMED.', '.CBO_LEDGERSTATUS_PROCESSED.') AND chtcode IN ('.CBO_CHARTCODE_WITHDRAWAL.', '.CBO_CHARTCODE_BALANCETRANSFER.', '.CBO_CHARTCODE_WITHDRAWALCHARGES.' )) OR (status in ('.CBO_LEDGERSTATUS_CONFIRMED.', '.CBO_LEDGERSTATUS_MANCONFIRMED.') AND chtcode NOT IN ('.CBO_CHARTCODE_WITHDRAWAL.', '.CBO_CHARTCODE_BALANCETRANSFER.' , '.CBO_CHARTCODE_WITHDRAWALCHARGES.' )))';

		$condition .= ' AND accid = '.$id;

		$balance = Cashledger::whereRaw($condition)->sum('amount');

		if( $numberformat == true){
			return $balance;
		}else{
			return $balance;
		}

	}



	public function verifyPassword(Request $request){


		if( $request->has('sid') )
		{
			if ( Hash::check( $request->scspassword , User::where( 'id' , '=' , Session::get('admin_userid') )->pluck('password')) )
			{
				return 1;

			}
			return 0;
		}

		return 0;
	}

    public function updateProfileAuditValue($requestValue, $modelValue, $label, $skipEmptyRequestValue = true, $skipCompareValue = false, &$auditArr = null) {

	    $add = true;

	    if ($skipEmptyRequestValue) {
            if (is_null($requestValue) || $requestValue == '') {
                return $modelValue;
            }
        }

	    if ($add && !$skipCompareValue) {
            $add = ($requestValue != $modelValue);
        }

	    if (!is_null($auditArr) && $add) {
	        $auditArr[] = $label . ' (old: ' . $modelValue . '; new: ' . $requestValue . ')';
        }

	    return $requestValue;
    }

    private function suspendProduct($accObj, $prdObj) {

	    // Transfer out all credit to main wallet.
	    try {
            $className  = 'App\\libraries\\providers\\' . $prdObj->code;
            $provider = new $className;
            $balance = $provider->getBalance($accObj->nickname, $accObj->crccode);

            if ($balance !== false && $balance > 0) {
                // Follow App\Http\Controllers\User\WalletController@transferProcess
                $bltObj = new Balancetransfer;
                $bltObj->accid 			= $accObj->id;
                $bltObj->acccode 		= $accObj->nickname;
                $bltObj->from 			= $prdObj->id;
                $bltObj->to 			= 0;
                $bltObj->crccode		= $accObj->crccode;
                $bltObj->crcrate		= Currency::getCurrencyRate($accObj->crccode);
                $bltObj->amount 		= $balance;
                $bltObj->amountlocal 	= Currency::getLocalAmount($balance, $accObj->crccode);
                $bltObj->status			= CBO_STANDARDSTATUS_SUSPENDED;

                if ( $bltObj->save() ) {
                    $productwalletObj = new Productwallet;
                    $productwalletObj->prdid 		= $bltObj->from;
                    $productwalletObj->accid 		= $bltObj->accid;
                    $productwalletObj->acccode 		= $bltObj->acccode;
                    $productwalletObj->chtcode 		= CBO_CHARTCODE_BALANCETRANSFER;
                    $productwalletObj->crccode 		= $bltObj->crccode;
                    $productwalletObj->crcrate 		= $bltObj->crcrate;
                    $productwalletObj->amount		= abs($bltObj->amount) * -1;;
                    $productwalletObj->amountlocal	= abs($bltObj->amountlocal) * -1;;
                    $productwalletObj->refobj		= 'Balancetransfer';
                    $productwalletObj->refid		= $bltObj->id;

                    if ( $productwalletObj->save() ) {
                        if ( $provider->withdraw($accObj->nickname, $accObj->crccode, $balance, $productwalletObj->id) ) {
                            $cashLedgerObj = new Cashledger;
                            $cashLedgerObj->accid 			= $bltObj->accid;
                            $cashLedgerObj->acccode 		= $bltObj->acccode;
                            $cashLedgerObj->accname			= $accObj->fullname;
                            $cashLedgerObj->chtcode 		= CBO_CHARTCODE_BALANCETRANSFER;
                            $cashLedgerObj->cashbalance 	= WalletController::getBalance($accObj->id);
                            $cashLedgerObj->amount 			= $bltObj->amount;
                            $cashLedgerObj->amountlocal 	= $bltObj->amountlocal;
                            $cashLedgerObj->refid 			= $bltObj->id;
                            $cashLedgerObj->status 			= CBO_LEDGERSTATUS_CONFIRMED;
                            $cashLedgerObj->refobj 			= 'Balancetransfer';
                            $cashLedgerObj->crccode 		= $accObj->crccode;

                            if( $cashLedgerObj->save() )
                            {
                                $productwalletObj->status	= CBO_LEDGERSTATUS_CONFIRMED;
                                $bltObj->status				= CBO_LEDGERSTATUS_CONFIRMED;
                                $productwalletObj->save();
                                $bltObj->save();
                            }
                        }
                    }
                }
            }

            // Additional features.
            if ($prdObj->code == 'MXB') {
                // Disconnect user from live game.
                $params = array('username' => $accObj->nickname, 'currency' => $accObj->crccode);

                if ($provider->logoutUser($params)) {
                    $provider->changeStatus($accObj->nickname, 2, $accObj->crccode);
                }
            }

	        return true;
        } catch (\Exception $ex) {
	        return false;
        }
    }

	public function updateProfile(Request $request){

		if( $request->has('sid') ){
			$accObj = Account::find($request->sid);
			$acdObj = Accountdetail::where( 'accid' , '=' , $request->sid)->first();
		}

		$auditArr = array();

		if( $request->has('dob') && $request->dob > 0 )
		{
			$acdObj->dob = $this->updateProfileAuditValue($request->input('dob'), $acdObj->dob, 'Birthday', false, false, $auditArr);
		}

		$canViewContactInfo = $this->hasPermission('MEMBER', 'MEMBERENQUIRY', 'VIEWCONTACTINFO');

        $acdObj->gender 	 = $this->updateProfileAuditValue($request->input('sgender'),       $acdObj->gender,        'Gender',       true, false, $auditArr);
        $acdObj->resaddress  = $this->updateProfileAuditValue($request->input('resaddress'),    $acdObj->resaddress,    'Address',      true, false, $auditArr);
        $acdObj->resstate    = $this->updateProfileAuditValue($request->input('resstate'),      $acdObj->resstate,      'State',        true, false, $auditArr);
        $acdObj->rescity	 = $this->updateProfileAuditValue($request->input('rescity'),       $acdObj->rescity,       'City',         true, false, $auditArr);
        $acdObj->respostcode = $this->updateProfileAuditValue($request->input('respostcode'),   $acdObj->respostcode,   'Post Code',    true, false, $auditArr);
        $acdObj->fullname    = $this->updateProfileAuditValue($request->input('sfullname'),     $acdObj->fullname,      'Full Name',    true, false, $auditArr);

		if ($canViewContactInfo) {
            $acdObj->rescountry = $this->updateProfileAuditValue($request->input('rescountry'), $acdObj->rescountry,    'Country',      true, false, $auditArr);
            $acdObj->telmobile  = $this->updateProfileAuditValue($request->input('sphone'),     $acdObj->telmobile,     'Phone Number', true, false, $auditArr);
            $acdObj->email      = $this->updateProfileAuditValue($request->input('semail'),     $acdObj->email,         'Email',        true, false, $auditArr);
        }

        $accObj->withdrawlimit	= $this->updateProfileAuditValue($request->input('iwithdrawlimit'),     $accObj->withdrawlimit,     'Daily Withdrawal Limit',   true, false, $auditArr);
        $accObj->maxdepositamt	= $this->updateProfileAuditValue($request->input('imaxdepositamt'),     $accObj->maxdepositamt,     'Max Daily Deposit',        true, false, $auditArr);
        $accObj->maxwithdrawamt	= $this->updateProfileAuditValue($request->input('imaxwithdrawamt'),    $accObj->maxwithdrawamt,    'Max Daily Withdrawal',     true, false, $auditArr);
        $accObj->fullname	    = $this->updateProfileAuditValue($request->input('sfullname'),          $accObj->fullname,          'Full Name',                true, false, $auditArr);


		if( $request->has('cremark') && $request->cremark != '' )
		{
			$memObj = new Memo;
			$memObj->type     = Memo::CBO_TYPE_MEMBER;
			$memObj->accid    = $accObj->id;
			$memObj->usrid    = Session::get('admin_userid');
			$memObj->username = Session::get('admin_username');
			$memObj->note 	  = $request->cremark;
			$memObj->save();
		}


		//update tag
		if($acctagResult = Accounttag::whereRaw('status = '.CBO_TAGSTATUS_ACTIVE.' AND accid = '.$accObj->id)->get()) {

			if(!$request->has('aflag'))
				$flag = array();
			else
				$flag = $request->input('aflag');

			foreach($acctagResult as $id => $value) {

				if(!in_array($value->tagid, $flag)) {

					$acctagObj = Accounttag::find($value->id);
					$acctagObj->status = CBO_TAGSTATUS_NOTACTIVE;
					$acctagObj->save();

				}
			}
		}


	 	if( $request->has('aflag') && is_array($request->input('aflag')) ) {
			if ($acctagResult = Accounttag::whereRaw('status = '.CBO_TAGSTATUS_NOTACTIVE.' AND accid = '.$accObj->id)->get()) {
				if( $request->has('aflag')){
					foreach($request->input('aflag') as $id => $value) {
						if($acctagObj = Accounttag::whereRaw('tagid = '.$value.' AND accid = '.$request->input('sid'))->first()) {
							$acctagObj->status = CBO_TAGSTATUS_ACTIVE;
							$acctagObj->save();

							$tagObj = Tag::find($acctagObj->tagid);
                            $auditArr[] = 'Set Tag: ' . $tagObj->name;
						} else {
							$acctagObj = new Accounttag;
							$acctagObj->accid = $request->input('sid');
							$acctagObj->tagid = $value;
							$acctagObj->status = CBO_TAGSTATUS_ACTIVE;
							$acctagObj->save();

                            $tagObj = Tag::find($acctagObj->tagid);
                            $auditArr[] = 'Set Tag: ' . $tagObj->name;
						}
					}
				}
			}
			else {

				if( $request->has('aflag')){

					foreach($request->input('aflag') as $id => $value) {

						$return = Accounttag::whereRaw('tagid = '.$value.' AND accid = '.$request->input('sid'))->get();
						if($return !== false)
							continue;

						$acctagObj = new Accounttag;
						$acctagObj->accid = $request->input('sid');
						$acctagObj->tagid = $value;
						$acctagObj->status = CBO_TAGSTATUS_ACTIVE;
						$acctagObj->save();

                        $tagObj = Tag::find($acctagObj->tagid);
                        $auditArr[] = 'Set Tag: ' . $tagObj->name;
					}
				}
			}
		}



		$changepass = false;
		if($request->schangepassword == "true")
		{
			if(strlen($request->spassword) > 0)
			{
		 		$accObj->password 		= Hash::make( $request->spassword );
		 		$accObj->enpassword 	= Crypt::encrypt( $request->spassword );
		 		$accObj->lastpwdchange  = App::getDateTime();
				$changepass = true;

                $auditArr[] = 'Password';
		 	}
		}



		if($wbsObj = Website::where('id','=',$accObj->wbsid)->first()){
                $products = unserialize($wbsObj->products);
                foreach($products as $product){
                    if($prdObj = Product::where('id','=',$product)->whereRaw('status != 0')->first()){
                        if(!in_array($prdObj->id, $request->avaiable_products)){
							$not_avaiable_products[] = $prdObj->id;
						}
					}
                }
        }

		if(isset($not_avaiable_products)){
			$accObj->products = serialize($not_avaiable_products);
		}else{
			$accObj->products = '';
		}

		$suspendedProducts = $request->input('suspended_products', array());

		if(count($suspendedProducts) > 0){
		    foreach ($suspendedProducts as $prdid) {
                $prdObj = Product::find($prdid);

                if ($prdObj) {
                    $this->suspendProduct($accObj, $prdObj);
                }
            }

			$accObj->suspendedproducts = serialize($suspendedProducts);
		}else{
			$accObj->suspendedproducts = '';
		}


		if( $request->has('bhdid') && $request->bhdid != Agent::where( 'id' , '=' , $accObj->agtid )->pluck('bhdid') ){

			$accObj->bhdid = $request->bhdid;
		}

		$accObj->save();
		$acdObj->save();

		if (count($auditArr) > 0) {
            Memo::forceCreate(array(
                'type' => Memo::CBO_TYPE_ADMIN,
                'accid' => $accObj->id,
                'usrid' => Session::get('admin_userid'),
                'username' => Session::get('admin_username'),
                'note' => 'Modified: ' . implode(', ', $auditArr),
            ));
        }

		echo json_encode( array( 'code' => Lang::get('COMMON.SUCESSFUL') ) );


	}

	public function showMemo(Request $request){

		$rows = array();

		$total = Memo::whereRaw('accid = '.$request->id.' ')->count();

		if($memos = Memo::whereRaw('accid = '.$request->id.' ')->skip($request->recordstartindex)->take( $request->pagesize )->orderBy('id','desc')->get()){
			foreach( $memos as $memo ){

				$rows[] = array(

					'admin'   => $memo->username,
					'remark'  => $memo->note,
					'date'    => $memo->created->toDateTimeString(),
				);

			}

		}


		echo json_encode(array('total' => $total , 'rows' => $rows));
		exit;

	}

	public function showBank(Request $request){

		$rows = array();

		$total = Accountbank::whereRaw('accid = '.$request->id.' ')->count();

		if($accBanks = Accountbank::whereRaw('accid = '.$request->id.' ')->skip($request->recordstartindex)->take( $request->pagesize )->orderBy('id','desc')->get()){
			foreach( $accBanks as $accBank ){

				$rows[] = array(
					'bank'     => $accBank->bankname,
					'account'  => $accBank->bankaccno,
					'bankid'  	=> $accBank->bankid,
					'id'  		=> $accBank->id,
				);

			}

		}


		echo json_encode(array('total' => $total , 'rows' => $rows));
		exit;

	}
	
	public function showBankList(Request $request){

		$rows = array();
		
		if( $banklists = Bank::get())
		{
			foreach( $banklists as $banklist )
			{
				$rows[$banklist->id] = $banklist->name;
			}
			
		}
		
		echo json_encode( $rows );
		exit;
	}

	public function sms(Request $request){

	    $resEcho = 2;

        if($request->has('sid'))
            $id = $request->input('sid');

        if($accObj = Account::where( 'id' , '=' , $id  )->first())
		{
		    if (Config::get('setting.opcode') == 'RYW' && $accObj->crccode == 'MYR') {
                $accountdetail = $accObj->getAccountDetailObject();

//                $cleanup_chr = array ('+', '-', ' ', '(', ')', '\r', '\n', '\r\n');
//                $tels = str_replace($cleanup_chr, '', $accountdetail->telmobile);
//                if($tels[0] == '0') $tels = '6'.$tels;

                $message = trim($request->get('sms'));

                if( strlen($message) > 0 ) {
//					App::SMS( $request->sms , $tels );

                    $smsObj = new SMS88();

                    $telmobile = $smsObj->formatTel('MYR', $accountdetail->telmobile);
                    $smsLogObj = Smslog::quickCreate($accObj->id, $accObj->nickname, $request->ip(), 'sms88', 'memenq', $telmobile, $message, '', '', 0);
                    $response = '';

                    try {
                        $response = $smsObj->sms($message, $telmobile);

                        $resEcho = 1;
                    } catch (\Exception $ex) {
                        // Nothing.
                    }

                    $smsLogObj->response = $response;
                    $smsLogObj->save();
                }

            }

		}

		echo $resEcho;
        exit;

	}

	public function inbox(Request $request){

			$inboxObj = new Inbox;
			$inboxObj->title 	 		= $request->title;
			$inboxObj->accid 	 		= $request->sid;
			$inboxObj->content 	 		= $request->content;
			$inboxObj->crccode 	 		= $request->inbox_crccode;
			$inboxObj->nickname 	 	= Account::whereId($request->sid)->pluck('nickname');
			echo $inboxObj->save();

	}


	public function sendToAll(Request $request){

			$inboxObj = new Inbox;

			$inboxObj->title 	 		= $request->title;
			$inboxObj->content 	 		= $request->content;
			$inboxObj->crccode 	 		= $request->inbox_crccode;
			$inboxObj->is_all 	 		= 1;
			echo $inboxObj->save();



	}

	public function updateBank(Request $request){

            if($bnkObj = Bank::whereRaw('name = "'.$request->bank.'"')->first()){
                echo Accountbank::whereAccid($request->sid)->orderBy('id', 'desc')->update(
			array(
				'bankaccno' => $request->account,
                                'bankname'  => $request->bank,
                                'bankid'    => $bnkObj->id
			)
		);
            }


	}
	
	public function updateBankInfo(Request $request){
		if($request->bId ){
			if($request->bankList){
				$bank=Bank::where('id', '=', $request->bankList)->first();;
				$bankName=$bank->name;
			}
			$values=array('bankid'=>$request->bankList,'bankaccno'=>$request->bankAccount,'bankname'=>$bankName);
			if(Accountbank::where('id', '=', $request->bId)->update($values)){
				echo "1";
			}
		}

	}

	public function showMessage(Request $request){

		$rows = array();

		$total = Inbox::whereRaw('accid = '.$request->id.' ')
				->orWhere( 'is_all' , '=' , 1)
				->count();


		if($messages = Inbox::whereRaw('accid = '.$request->id.' ')->orWhere( 'is_all' , '=' , 1)->skip($request->recordstartindex)->take( $request->pagesize )->orderBy('id','desc')->get()){
			foreach( $messages as $message ){

				$rows[] = array(
					'title'     => $message->title,
					'content'   => $message->content,
					'date'   => $message->created->toDateTimeString(),
				);

			}

		}

		echo json_encode(array('total' => $total , 'rows' => $rows));
		exit;

	}

}
