<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller\Admin;
use App\libraries\grid\PanelGrid;
use App\libraries\App;
use Illuminate\Http\Request;
use Auth;
use Validator;
use Session;
use Lang;
use App\Models\Iptracker;
use App\Models\Currency;
use App\Models\Fundingmethod;
use App\Models\Cashledger;
use App\Models\Bank;
use App\Models\Accounttag;
use App\Models\Account;
use App\Models\Cashledgerlog;
use App\Models\Bankaccount;
use App\Models\Promocash;
use App\Models\Rejectreason;
use App\Models\Banktransfer;
use App\Models\Promocampaign;
use App\Models\Tag;
use Config;

class TagController extends Controller{


	protected $moduleName 	= 'tag';
	protected $limit 		= 20;
	protected $start 		= 0;
	
        protected $tagColorArray = array('#330066','#0000CC','#99FFFF','#6600CC','#CC33FF','#FF99FF','#CC0000','#FF6600','#CC6666','#33FF00','#33CC99','#669933','#999966','#FFFF00','#999999','#000000', '#736AFF', '#FFE87C', '#C85A17', '#3EA99F');

	public function __construct()
	{
		
	}
	
	public function index(Request $request){

			$grid = new PanelGrid;
			$grid->setupGrid($this->moduleName, 'tag-data', $this->limit);
			$grid->setTitle(Lang::get('COMMON.TAG'));
			
			$grid->addColumn('seq', 			Lang::get('COMMON.ID'), 				0,			array('align' => 'left'));			
			$grid->addColumn('code', 			Lang::get('COMMON.CODE'), 				68,			array('align' => 'left'));			
			$grid->addColumn('name', 			Lang::get('COMMON.TAGNAME'),                            168,                    array('align' => 'left'));
			$grid->addColumn('category', 			Lang::get('COMMON.CATEGORY'),                             68,			array('align' => 'center'));
			$grid->addColumn('color', 			Lang::get('COMMON.TAGCOLOR'),                           68,			array('align' => 'left'));
			$grid->addColumn('status', 			Lang::get('COMMON.STATUS'),                             68,			array('align' => 'center'));

			$grid->addFilter('status',Tag::getStatusOptions(), array('display' => Lang::get('COMMON.STATUS')));
			$grid->addFilter('category',Tag::getCategoryOptions(), array('display' => Lang::get('COMMON.CATEGORY')));
			$grid->addButton('1', 'New', Lang::get('COMMON.ADDNEW').' '.Lang::get('COMMON.TAG'), 'button', array('icon' => 'add', 'url' => action('Admin\TagController@drawAddEdit')));


			$data['grid'] = $grid->getTemplateVars();

			return view('admin.grid2',$data);
    }
	
		protected function doGetData(Request $request){
	
		 //status
		$status[1] = 'Active';
		$status[0] = 'Suspended';
		
		$rows = array();
		
		$aflt  = $request->input('aflt');
		$aqfld = $request->input('aqfld');
		
		$condition = '1';
		
		if( isset($aqfld['status']) && $aqfld['status'] != ''){
			$condition .= ' AND status'.'='.$aqfld['status'];
		}

		if( isset($aqfld['category']) && $aqfld['category'] != ''){
			$condition .= ' AND category'.'='.$aqfld['category'];
		}
		
		$total = Tag::whereRaw($condition)->count();
		
		//new structure
		if($tags = Tag::whereRaw($condition)->skip($request->recordstartindex)->take( $request->pagesize )->orderBy('id','desc')->get()){

			$categoryOptions = collect(Tag::getCategoryOptions());

			foreach( $tags as $tag ){
				$body = App::formatAddTabUrl( Lang::get('TAG').': '.$tag->id, urlencode($tag->name), action('Admin\TagController@drawAddEdit',  array('sid' => $tag->id)));
				$tagStr = '<div style="height: 13px; width: 50px; background-color:'.$tag->color.'; display:inline-block;"> &nbsp; &nbsp;</div>';
		
				$rows[] = array(

					'seq'  				=> $tag->id, 
					'code'  			=> $tag->code, 
					'name'  			=> urldecode($body),
                    'category'  		=> $categoryOptions->get($tag->category),
					'color'  			=> $tagStr,
					'status'  			=> $tag->getStatusText($this->moduleName),

				);	
				
			}
				
		}

				
		echo json_encode(array('total' => $total , 'rows' => $rows));
		exit;
	
	 
	}
	
	protected function drawAddEdit(Request $request) { 

                $toAdd 	  = true;
		$object   = null;
		$readOnly = false;
		$values   = array();

		$title = Lang::get('NEW').' '.Lang::get('TAG');

		if($request->has('sid')) 
		{
	
			if ($object = Tag::find($request->input('sid'))) 
				{
					$title    = Lang::get('COMMON.EDIT').' '.Lang::get('COMMON.TAG');
					$toAdd    = false;
					$readOnly = true;

				} 
			else
				{
					$object = null;
				}
			
		}
		
                $colorResultArray = $this->getColorArrayOption();
                
                if($object) {
                    array_unshift($colorResultArray, $object->color);
		}
					
		//$tagStr = '<div style="height: 13px; width: 50px; background-color:'.$tagColorArray.'; display:inline-block;"> &nbsp; &nbsp;</div>';
	
		$form = App::setupPanelForm('doadd', $request->all(), array(), $toAdd);
		$form->setFormTitle($title);
		
		$form->addInput('text', 		Lang::get('COMMON.NAME') 							, 'name', 	 	(($object)? $object->name:''),                  array(), false); 
		$form->addInput('color', 		Lang::get('COMMON.TAG').' '.Lang::get('COMMON.COLOR')                           , 'color',	 	(($object)? $object->color:''), 		array('options' => $colorResultArray),true);
            
                $form->addInput('radio',   		Lang::get('COMMON.SPECIAL').' '.Lang::get('COMMON.BONUS')							, 'special',            (($object)? $object->special:''), 		array('options' => Tag::getSpecialOptions()), true); 

                $form->addInput('radio',   		Lang::get('COMMON.TAG').' '.Lang::get('COMMON.CATEGORY')							, 'category',             (($object)? $object->category:'0'), 		array('options' => Tag::getCategoryOptions()), true);
                $form->addInput('radio',   		Lang::get('COMMON.STATUS')							, 'status',             (($object)? $object->status:''), 		array('options' => App::getStatusOptions()), true);

		
	
		$data['form'] = $form->getTemplateVars();
		$data['module'] = 'tag';

		return view('admin.form2',$data);
	
	}
	
	protected function doAddEdit(Request $request) {
		
		$validator = Validator::make(
			[
	
				'name'	  	  		 => $request->input('name'),
				'color'	  	 		 => $request->input('color'),
				'category'	  	 	 => $request->input('category'),

			],
			[

			   'name'	   	   	   => 'required',
			   'color'	  		   => 'required',
			   'category'	  	   => 'required',

			]
		);
		
		
		if ($validator->fails())
		{
			echo json_encode($validator->errors());
			exit;
		}
		
		
		
		$object  = new Tag;
		
		if($request->has('sid')) 
		{
			$object = Tag::find($request->input('sid'));
		}
		
		$object->name	 			= $request->input('name');
		$object->color	 			= $request->input('color');
		$object->special	 		= $request->has('special') ? $request->input('special') : 0;
		$object->category	 		= $request->input('category', 0);
		$object->status	 			= $request->input('status');


		
		if( $object->save() )
		{
			echo json_encode( array( 'code' => Lang::get('COMMON.SUCESSFUL') ) );
			exit;
		}


		echo json_encode( array( 'code' => Lang::get('COMMON.FAILED') ) );
		
	}
	
	protected function doActivateSuspend(Request $request) {

			

		if($request->has('id')) {
		
			if($object = Tag::find($request->id)) {
			
				$currentStatus = $object->status;
				if($request->action == 'activate') {
					$object->status 	= CBO_ACCOUNTSTATUS_ACTIVE;
				}
				if($request->action == 'suspend') {
					$object->status 	= CBO_ACCOUNTSTATUS_SUSPENDED;
				}

				if($currentStatus <> $object->status){
					if($object->save()) {
					
						echo json_encode( array( 'code' => Lang::get('COMMON.SUCESSFUL') ) );
						exit;
					}
				}
			}
		}
		
		
		echo json_encode( array( 'code' => Lang::get('COMMON.FAILED') ) );

	}
        
        function getColorArrayOption(){
		$tagColorArray = array();
                $condition = '1';
		if($tags = Tag::whereRaw($condition)->get()){
			foreach($tags as $tag){
				array_push($tagColorArray,$tag->color);
			}
		}
		
		$colorResultArray = array_diff($this->tagColorArray, $tagColorArray);
		return $colorResultArray;
	}
	
}

?>
