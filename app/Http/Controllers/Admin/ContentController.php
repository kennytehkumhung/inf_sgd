<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller\Admin;
use App\libraries\grid\PanelGrid;
use App\libraries\App;
use Config;
use Illuminate\Http\Request;
use Auth;
use Validator;
use Session;
use Lang;
use App\Models\Article;
use App\Models\Language;
use App\Models\Media;
use App\Models\Configs;
use Cache;
use Storage;
use File;

class ContentController extends Controller{ 
    
	protected $moduleName	= 'cms';
	protected $limit 		= 20;
	protected $start 		= 0;
	
	private $flds = array (
		'code'		=>	array('text', 				'Code', 		'scode', 			true),
		'title'		=>	array('text', 				'Title',		'stitle', 			true),
		'rank'		=>	array('text', 				'Rank', 		'irank', 			true),
		'lang'		=>	array('select', 			'Language',		'slanguage',			true),
		'type'		=>	array('select', 			'Type', 		'itype', 			true),
		'body'		=>	array('html', 				'Body',			'cbody', 			true),
		'excerpt'	=>	array('html', 				'Excerpt',		'cexcerpt',			true),
		'image'		=>	array('file', 				'image', 		'simage', 			true),
		'status'	=>	array('radio', 				'Status',		'istatus', 			true),
	);
	
	public function __construct()
	{
		
	}
	
	public function index(Request $request)
	{
		$grid = new PanelGrid;
		$grid->setupGrid($this->moduleName, 'cms-data', $this->limit);
		
		$grid->setTitle(Lang::get('COMMON.CMS'));
		
	    $grid->addColumn('name', 			Lang::get('COMMON.NAME'),			90,		array());
		$grid->addColumn('article', 		Lang::get('COMMON.ARTICLE'),		90, 	array('align' => 'right'));
		$grid->addColumn('add', 			'',									60, 	array('align' => 'center'));
		
		$data['grid'] = $grid->getTemplateVars();

		return view('admin.grid2',$data);
	}
	
	protected function doGetData(Request $request) {
		
	    $rows = array();
		if($types = Article::getTypeOptions()){
			foreach($types as $type => $text){
				$count = Article::whereRaw('crccode = "'.Session::get('admin_crccode').'" AND type ='.$type)->count();
				$rows[] = array(
					'name' => $text,
					'article' => App::formatAddTabUrl(Lang::get('COMMON.ARTICLE').': '.$text, $count, action('Admin\ContentController@showArticle',array('itype' => $type))),
					'add' => App::formatAddTabUrl(Lang::get('COMMON.NEW').': '.Lang::get('COMMON.ARTICLE'), Lang::get('COMMON.ADD'), action('Admin\ContentController@drawAddEditArticle',array('itype' => $type))),
				);
			}
		}
		
		echo json_encode(array('total' => count($rows), 'rows' => $rows));
		exit;
	}
	
	protected function showArticle(Request $request){
		$type = 0;
		if($request->has('itype')) $type = $request['itype'];
		
		$grid = new PanelGrid;
		$grid->setupGrid($this->moduleName, 'cms-articledata', $this->limit, false, array('params' => array('itype' => $type)));
		
		$grid->setTitle(Lang::get('COMMON.ARTICLE'));
		
		$grid->addColumn('rank', 			Lang::get('COMMON.RANK'), 				40,				array());
		$grid->addColumn('currency',		Lang::get('COMMON.CURRENCY'),           120,			array());
		$grid->addColumn('language',		Lang::get('COMMON.LANGUAGE'),           120,			array());
		$grid->addColumn('title', 			Lang::get('COMMON.TITLE'),              300,			array());
		$grid->addColumn('status', 			Lang::get('COMMON.STATUS'),             80,				array());
		
		$grid->addFilter('istatus', Article::getStatusOptions(), array('display' => Lang::get('COMMON.STATUS')));
		$grid->addButton('1', 'New', Lang::get('COMMON.NEW'), 'button', array('icon' => 'add', 'url' => action('Admin\ContentController@drawAddEditArticle', array('itype' => $type))));
		
		$grid->addFilter('lang', Language::getAllLanguageAsOptions(), array('display' => Lang::get('COMMON.LANGUAGE')));
		
		$data['grid'] = $grid->getTemplateVars();
		return view('admin.grid2',$data);
	}
	
	protected function doGetArticleData(Request $request) {

		$condition = '1';
		
		if ( ( config('setting.opcode') == 'RYW' || config('setting.opcode') == 'IFW' )&& Session::get('admin_crccode') == 'MYR' ) {
			
        }else{
			$condition .= ' AND crccode = "'.Session::get('admin_crccode').'"';	
		}
		
		
		if($request->has('itype')) {
			$condition .= ' AND type='.$request->itype;
		}
		
		if(isset($request->aqfld['lang']) && $request->aqfld['lang'] != '') {
			$condition .= ' AND lang= "'.$request->aqfld['lang'].'"';
		}
                
                if(isset($request->aqfld['istatus']) && $request->aqfld['istatus'] != '') {
			$condition .= ' AND status= "'.$request->aqfld['istatus'].'"';
		}

		$total = Article::whereRaw($condition)->count();
		
		$rows = array();
		if($articles = Article::whereRaw($condition)->skip($request->recordstartindex)->take( $request->pagesize )->orderBy('lang','desc')->orderBy('rank','asc')->get()){
			$languageOptions = Language::getAllLanguageAsOptions();
			foreach($articles as $article){
				$rows[] = array(
					'rank' => $article->rank,
					'currency' => $article->crccode,
					'language' => $languageOptions[$article->lang],
					'title' => App::formatAddTabUrl(Lang::get('COMMON.ARTICLE').': '.str_replace("'","\'",$article->title), $article->title, action('Admin\ContentController@drawAddEditArticle',array('sid' => $article->id))),
					'status' => $article->getStatusText($this->moduleName),
				);
			}
		}
		
		echo json_encode(array('total' => $total, 'rows' => $rows));
		exit;
		
	}
	
	protected function drawAddEditArticle(Request $request) {
		
		
		$toAdd 	  = true;
		$object   = null;
		$readOnly = false;
		$values   = array();

		$title = Lang::get('NEW').' '.Lang::get('ARTICLE');
		if($request->has('sid')) {
	
				if ($object = Article::find($request->input('sid'))) {
					$title = Lang::get('EDIT').' '.Lang::get('ARTICLE');
					$toAdd = false;
					$readOnly = true;
				} else
				$object = null;
			
		}
		
	
		
		$type = (isset($request->itype)?$request->itype:0);
		$types = Article::getTypeOptions();
		App::arrayUnshift($types, Lang::get('COMMON.SELECT'));
		
		$languageOptions = Language::getAllLanguageAsOptions();
		App::arrayUnshift($languageOptions, Lang::get('COMMON.SELECT'));
		
		$form = App::setupPanelForm('cms-do-add-edit', $request->all(), array(), $toAdd);
		$form->setFormTitle($title);
		$form->addInput(($object)?'showonly':$this->flds['code'][0], $this->flds['code'][1], $this->flds['code'][2], (($object)? $object->code:''), array(), $this->flds['code'][3]);
		$form->addInput($this->flds['type'][0], $this->flds['type'][1], $this->flds['type'][2], (($object)? $object->type:$type), array('options' => $types), $this->flds['type'][3]);
		$form->addInput($this->flds['rank'][0], $this->flds['rank'][1], $this->flds['rank'][2], (($object)? $object->rank:''), array(), $this->flds['rank'][3]);
		$form->addInput($this->flds['lang'][0], $this->flds['lang'][1], $this->flds['lang'][2], (($object)? $object->lang:''), array('options' => $languageOptions), $this->flds['lang'][3]);
		$form->addInput($this->flds['title'][0], $this->flds['title'][1], $this->flds['title'][2], (($object)? $object->title:''), array(), $this->flds['title'][3]);
		$form->addInput($this->flds['excerpt'][0], $this->flds['excerpt'][1], $this->flds['excerpt'][2], (($object)? $object->excerpt:''), array(), $this->flds['excerpt'][3]);
		$form->addInput($this->flds['body'][0], $this->flds['body'][1], $this->flds['body'][2], (($object)? $object->content:''), array(), $this->flds['body'][3]);
		$form->addInput('file', 				Lang::get('COMMON.IMAGE') 		, 'image', 			(($object)? $object->getImage(Media::TYPE_IMAGE):''), 			array(), false); 

		if (!is_null(Config::get('setting.front_mobile_path'))) {
            $form->addInput('file', Lang::get('public.ImageForMobile'), 'image_for_mobile', (($object) ? $object->getImage(Media::TYPE_IMAGE_MOBILE) : ''), array(), false);
        }

		//$form->addInput($this->flds['status'][0], $this->flds['status'][1], $this->flds['status'][2], (($object)? $object->status:''), array('options' => BankObject::getStatusOptions()), $this->flds['status'][3]);
		$data['form']   = $form->getTemplateVars();
		$data['module'] = 'cms';

		return view('admin.form2',$data);
	}
	
	protected function doActivateSuspend(Request $request) {
		
		if($request->has('id')) {
		
			if($object = Article::find($request->id)) {
			
				$currentStatus = $object->status;
				if($request->action == 'activate') {
					$object->status 	= CBO_ACCOUNTSTATUS_ACTIVE;
				}
				if($request->action == 'suspend') {
					$object->status 	= 0;
				}

				if($currentStatus <> $object->status){
					if($object->save()) {
						cache::forget('promotion_'.$request->input('id'));
						cache::forget('promotion_'.$object->lang.'_'.Session::get('admin_crccode'));
						echo json_encode( array( 'code' => Lang::get('COMMON.SUCESSFUL') ) );
						exit;
					}
				}
			}
		}

		
		
		echo json_encode( array( 'code' => Lang::get('COMMON.FAILED') ) );
	
	}
	
	function doAddEditArticle(Request $request) {
		
	    $validator = Validator::make(
			[
	
				'itype'	   	  => $request->input('itype'),
				'irank'	  	  => $request->input('irank'),
				'slanguage'   => $request->input('slanguage'),
				'scode'       => $request->input('scode'),
				'stitle'      => $request->input('stitle'),
				'cbody'  	  => $request->input('cbody'),
				'cexcerpt'    => $request->input('cexcerpt'),

			],
			[

			   'itype'	      => 'required',
			   'irank'	  	  => 'required',
			   'slanguage'    => 'required',
			   'scode'        => 'required',
			   'stitle'       => 'required',
			   'cbody'  	  => 'required',
			   'cexcerpt'     => 'required',	
			]
		);
		
		
		if ($validator->fails())
		{
			echo json_encode($validator->errors());
			exit;
		}
		
		if ($request->has('slanguage')) {
			if( $request->slanguage == "0" ){
				echo json_encode(array( 'slanguage' => 'language is required' ));
				exit;
			}
		}
		//$array = array( 'irank' => 'asd' );
		
		
		
		$object = new Article;
		
		if($request->has('sid')) 
		{
			$object = Article::find($request->input('sid'));
		}
		
		$object->code    = $request->input($this->flds['code'][2]);
		$object->type    = $request->input($this->flds['type'][2]);
		$object->rank    = ($request->has($this->flds['rank'][2]))?$request[$this->flds['rank'][2]]:0;
		$object->lang    = $request->input($this->flds['lang'][2]);
		$object->title   = $request->input($this->flds['title'][2]);
		$object->crccode = $request->has('sid') ? $object->crccode : Session::get('admin_crccode');
		$object->excerpt = $request->input($this->flds['excerpt'][2]);
		$object->content = $request->input($this->flds['body'][2]);
		$object->status  = 1;
		
		if( $object->save() )
		{
		    $fileInputs = array(
                'image' => Media::TYPE_IMAGE,
                'image_for_mobile' => Media::TYPE_IMAGE_MOBILE,
            );
			
			cache::forget('promotion_'.$request->input('sid'));
			cache::forget('promotion_mobile_'.$request->input('sid'));
			cache::forget('promotion_'.$request->input($this->flds['lang'][2]).'_'.$object->crccode);
			cache::forget('promotion_mobile_'.$request->input($this->flds['lang'][2]).'_'.$object->crccode);

		    foreach ($fileInputs as $imgKey => $imgType) {
                if( $request->hasFile($imgKey))
                {
                    $file = $request->file($imgKey);

                    $filename = 'promotion/'.md5($file->getClientOriginalName().time()).'.'. $file->getClientOriginalExtension();
                    $result = Storage::disk('s3')->put($filename,  File::get($file), 'public');


                    if($result)
                    {
                        if( Media::where( 'refobj' , '=' , 'Article' )->where( 'refid' , '=' , $object->id )->where( 'type' , '=' , $imgType )->count() == 0 ){
                            $medObj = new Media;
                        }else{
                            $medObj = Media::where( 'refobj' , '=' , 'Article' )->where( 'refid' , '=' , $object->id )->where( 'type' , '=' , $imgType )->first();
                        }

                        $medObj->refobj = 'Article';
                        $medObj->refid  = $object->id;
                        $medObj->type   = $imgType;
                        $medObj->domain = 'https://'.Configs::getParam('SYSTEM_AWS_S3BUCKET');
                        $medObj->path   = $filename;
                        $medObj->save();
                    }
                }
            }

			echo json_encode( array( 'code' => Lang::get('COMMON.SUCESSFUL') ) );
			exit;
		}

		echo json_encode( array( 'code' => Lang::get('COMMON.FAILED') ) );
	}
}
