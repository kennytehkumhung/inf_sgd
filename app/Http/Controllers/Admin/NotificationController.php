<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller\Admin;
use App\libraries\grid\PanelGrid;
use App\libraries\App;
use App\libraries\AccessControl;
use App\Models\Account;
use Illuminate\Http\Request;
use Auth;
use Validator;
use Session;
use Lang;
use DB;
use App\Jobs\ChangeLocale;
use Carbon\Carbon;


class NotificationController extends Controller{

	protected $moduleName 	= 'notification';

	public function __construct()
	{

	}

	public function showTotalUnseenMessage(Request $request){
		$start=Carbon::now()->startOfDay()->toDateTimeString();
		$end=Carbon::now()->endOfDay()->toDateTimeString();

		if($request->input('type') == true) {
			Account::where( 'crccode' , '=' ,Session::get('admin_crccode'))
			->whereBetween( 'created' , array($start,$end))
			->where('view_status','=','0')
            ->update(array(
                'view_status' => 1,
                'modified' => Carbon::now(),
            ));
            $data['unseenMessage'] = 0;
		} else {
			$data['unseenMessage'] = Account::where( 'crccode' , '=' ,Session::get('admin_crccode'))
									->whereBetween( 'created' , array($start,$end))->where('view_status','=','0')->count('id');
		}
		
		
		$data['totalUnseenMessage']=$data['unseenMessage'];
		
		if($data['totalUnseenMessage']<=0) {
			$data['totalUnseenMessage']=0;
		}
		echo json_encode($data['totalUnseenMessage']);
		exit;
	}

}
