<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller\Admin;
use App\libraries\grid\PanelGrid;
use App\libraries\App;
use App\libraries\AccessControl;
use App\Models\Currency;
use App\Models\Product;
use App\Models\Account;
use App\Models\Cashbalance;
use App\Models\Permission;
use App\Models\Profitloss;
use App\Models\Cashledger;
use App\Models\Promocampaign;
use App\Models\Promocash;
use App\Models\Roleusermap;
use App\Models\Balancetransfer;
use App\Models\Onlinetracker;
use Illuminate\Http\Request;
use Auth;
use Validator;
use Session;
use Lang;
use DB;
use App\Jobs\ChangeLocale;
use Carbon\Carbon;


class DashboardController extends Controller{

	public function __construct()
	{
		
	}

	public function show(Request $request)
	{
		
		$now = Carbon::now()->toDateTimeString();
		$sub30Minutes=Carbon::now()->subMinutes(30)->toDateTimeString();
		
		
		// if ($request->has('type')) {
			// if( $request->type == 'today' ){
				// $start=Carbon::now()->startOfDay()->toDateTimeString();
				// $end=Carbon::now()->endOfDay()->toDateTimeString();
			// }else if( $request->type == 'week' ){
				// $start=Carbon::now()->startOfWeek()->toDateTimeString();
				// $end=Carbon::now()->endOfWeek()->toDateTimeString();
			// }else if( $request->type == 'month' ){
				// $start=Carbon::now()->startOfMonth()->toDateTimeString();
				// $end=Carbon::now()->endOfMonth()->toDateTimeString();
			// }
		// }else{
			// $start=Carbon::now()->startOfDay()->toDateTimeString();
			// $end=Carbon::now()->endOfDay()->toDateTimeString();
		// }
		
		$navigations = $this->getNavigation();
		$permissions = Permission::whereRaw( 'rolid IN (select rolid from roleusermap where usrid = '.Session::get('admin_userid').' )')->orderBy('catid','ASC')->orderBy('mdlid','ASC')->get();

		// $data['deposit'] = Cashledger::whereRaw( 'status IN (1,4)' )
			// ->whereBetween( 'created' , array($start,$end))
			// ->where( 'chtcode' , '=' , 1 )
			// ->where( 'crccode' , '=' , Session::get('currency'))
			// ->sum('amount');

		// $data['withdrawal'] = Cashledger::whereRaw( 'status IN (1,4)' )
			// ->where( 'chtcode' , '=' , 2 )
			// ->whereBetween( 'created' , array($start,$end))
			// ->where( 'crccode' , '=' , Session::get('currency'))
			// ->sum('amount');
		// $data['withdrawal'] = $data['withdrawal'] * -1;

		// $data['netloss']=($data['withdrawal']-$data['deposit'])*-1;

		// $data['profitLoss'] = profitloss::whereBetween( 'date' , array($start,$end))
			// ->where( 'crccode' , '=' , Session::get('currency'))
			// ->sum('amount') * -1;

		// $data['register'] = Account::whereBetween( 'created' , array($start,$end))
			// ->where( 'crccode' , '=' , Session::get('currency'))
			// ->count('id');

//		$data['onlineMember'] = DB::table('onlinetracker as a')
//            ->join('account as b', 'b.id', '=', 'a.userid')
//            ->groupBy('a.userid')
//            ->whereBetween( 'a.modified' , array($sub30Minutes, $now))
//            ->where( 'b.crccode' , '=' , Session::get('currency'))
//            ->where( 'a.type' , '=' , 1)
//            ->count('a.id');

                // $data['onlineMember'] = Onlinetracker::whereBetween( 'modified' , array($sub30Minutes,$now))
			// ->where( 'type' , '=' , 1)
			// ->distinct()->count('userid');

		// $data['productSummaryByProvider'] = DB::select( DB::raw("SELECT profitloss.prdid,amount, COUNT(prdid) as total, 
																// SUM(amount) as totalAmount,product.code,name
																// FROM profitloss INNER JOIN product ON profitloss.prdid = product.id 
																// WHERE date BETWEEN '".$start."' AND '".$end."' 
																// AND crccode='".Session::get('currency')."'
																// GROUP BY prdid"));

		// $data['category1'] = DB::select( DB::raw("SELECT category,SUM(amount) as amount FROM profitloss 
												// WHERE date BETWEEN '".$start."' AND '".$end."' AND crccode='".Session::get('currency')."' GROUP BY category"));
		// $data['categories'] = Product::getCategoryOptions();

		// $data['topDeposit'] = DB::select( DB::raw("SELECT cashledger.id,COUNT(cashledger.accid) AS accid, SUM(cashledger.amount) AS amount,
													// cashledger.accid,account.nickname FROM cashledger INNER JOIN account ON cashledger.accid=account.id	
													// WHERE cashledger.status IN(1,4) 
													// AND cashledger.chtcode=1 
													// AND cashledger.modified 
													// BETWEEN '".$start."' AND '".$end."'
													// AND cashledger.crccode='".Session::get('currency')."'
													// GROUP BY cashledger.accid 
													// ORDER BY amount desc
													// LIMIT 10"));

		// $data['topWithdrawal'] = DB::select( DB::raw("SELECT cashledger.id,COUNT(cashledger.accid) AS accid, SUM(cashledger.amount) AS amount,
													// cashledger.accid,account.nickname FROM cashledger INNER JOIN account ON cashledger.accid=account.id	
													// WHERE cashledger.status IN(1,4) 
													// AND cashledger.chtcode=2 
													// AND cashledger.modified 
													// BETWEEN '".$start."' AND '".$end."'
													// AND cashledger.crccode='".Session::get('currency')."'
													// GROUP BY cashledger.accid 
													// ORDER BY amount desc
													// LIMIT 10"));

		// $data['topLoser'] = DB::select( DB::raw("SELECT id,nickname,COUNT(accid) as accid, SUM(amount) as amount FROM profitloss
												// WHERE date BETWEEN '".$start."' AND '".$end."' AND crccode='".Session::get('currency')."'
												// GROUP BY accid ORDER BY amount LIMIT 10"));

		// $data['topWinner'] = DB::select( DB::raw("SELECT id,nickname,COUNT(accid) as accid, SUM(amount) as amount FROM profitloss
												// WHERE date BETWEEN '".$start."' AND '".$end."' AND crccode='".Session::get('currency')."'
												// GROUP BY accid ORDER BY amount DESC LIMIT 10"));


		foreach( $permissions as $permission )
		{
			// Check granted module permission.
			if(isset($navigations[$permission->catid]['module'][$permission->mdlid]['url']))
			{
				if (isset($navigation[$navigations[$permission->catid]['name']][$navigations[$permission->catid]['module'][$permission->mdlid]['name']])) {
					// Get existing permission record.
					$nav = $navigation[$navigations[$permission->catid]['name']][$navigations[$permission->catid]['module'][$permission->mdlid]['name']];
				} else {
					// New permission record.
					$nav = $navigations[$permission->catid]['module'][$permission->mdlid]['url'];
					$nav['task'] = array();
				}

				// Check granted task permission.
				$tasks = $navigations[$permission->catid]['module'][$permission->mdlid]['task'];

				foreach ($tasks as $tskKey => $tskVal) {
					if ($tskKey == $permission->taskid) {
						$nav['task'][$permission->taskid] = $tasks[$permission->taskid];
					}
				}

				$navigation[$navigations[$permission->catid]['name']][$navigations[$permission->catid]['module'][$permission->mdlid]['name']] = $nav;
			}
		}


        $startDate=Carbon::now()->startOfDay()->toDateTimeString();
        $endDate=Carbon::now()->endOfDay()->toDateTimeString();
        $data['newUser'] = Account::where( 'crccode' , '=' ,Session::get('admin_crccode'))->whereBetween( 'created' , array($startDate,$endDate))->get();

		$data['categorys'] = $navigation;
		// $data['start'] = $start;
		// $data['end'] = $end;
		
		Session::put('admin_navigation', $navigation);

		// To check user has permission or not:
		// Refer App\libraries\AccessControl
		// $this->hasPermission('MEMBER', 'MEMBERENQUIRY', 'VIEWCONTACTINFO');

		return view('admin.index2testblank',$data);
				
				
				
		
		
		//$data['products']   = Product::getAllAsOptions(true, true);

	}
        
	private function getNavigation(){

		$accesscontrol = new AccessControl;
		return ($accesscontrol->modules);
	}
	
	public function setLanguage(Request $request){
		
	    $changeLocale = new ChangeLocale($request->input('lang'));
        $this->dispatch($changeLocale);

        return redirect()->back();
		
	}	
	
	
}
