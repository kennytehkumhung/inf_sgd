<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller\Admin;
use App\libraries\grid\PanelGrid;
use App\libraries\App;
use Illuminate\Http\Request;
use Auth;
use Validator;
use Session;
use Lang;
use App\Models\Currency;
use App\Models\Fundingmethod;
use App\Models\Cashledger;
use App\Models\Bank;
use App\Models\Remark;
use App\Models\Account;
use App\Models\Adjustment;
use App\Models\Promocampaign;
use App\Models\Promocash;
use App\Models\Agent;
use App\Models\Bankaccount;
use App\Models\Banktransfer;
use App\Models\Configs;
use App\Models\Accountbank;
use App\Models\Accountproduct;
use GeoIP;
use Redirect;
use Config;


class AdjustmentController extends Controller{
	
	protected $moduleName 	= 'adjustment';
	protected $limit 		= 20;
	protected $start 		= 0;
	
	public function __construct()
	{
		
	}

	public function index(Request $request)
	{

		$adjustmentOption = array();
		
		$adjustmentOption['rate']     = Lang::get('COMMON.ADJUSTMENTOFRATE');
		$adjustmentOption['bonus']    = Lang::get('COMMON.BONUS');
		$adjustmentOption['deposit']  = Lang::get('COMMON.DEPOSIT');
		$adjustmentOption['withdraw'] = Lang::get('COMMON.WITHDRAW');

		
		$form = App::setupPanelForm('docheck', $request->all(), array(), true);
		$form->setFormTitle(Lang::get('COMMON.ADJUSTMENT'));
		
		$step = 1;
		if($request->has('istep')) $step = $request->istep+1;
		
		$request->istep += 1;
		
		$form->addInput('hidden', '', 'istep', $step, array(), '');
	
		$currencies = Currency::getAllCurrencyAsOptions();
		
		if($step == 1) 
		{
			$form->addInput('select', Lang::get('COMMON.ADJUSTMENTMETHOD'), 'sadjOpt', '', array('options' => $adjustmentOption), true);
			$form->addInput('text',   Lang::get('COMMON.MEMBERACCOUNT'),    'acccode',    '', array(), true);
			$form->addInput('text',   Lang::get('COMMON.TRANSACTIONCODE'),  'iclgid' , '', array(), true);
			$form->addInput('select', Lang::get('COMMON.CURRENCY'), 'crccode', '', array('options' => $currencies, 'disabled' => false), true);
		}
		elseif($step == 2){

			$remarks = Remark::getAllRemarkAsOptions();
			App::arrayUnshift($remarks, Lang::get('COMMON.SELECT'));

			$form->addInput('showonly', Lang::get('COMMON.ADJUSTMENTMETHOD'), 'sadjOpt', $adjustmentOption[$request->sadjOpt], array(), true);
			$form->addInput('showonly', Lang::get('COMMON.CURRENCY'), 'crccode', $request->crccode, array(), true);
			$form->addInput('hidden', Lang::get('COMMON.ADJUSTMENTMETHOD'), 'sadjOpt', $request->sadjOpt, array(), '');
			$form->addInput('hidden', Lang::get('COMMON.CURRENCY'), 'crccode', $request->crccode, array(), '');
			
			if($accObj = Account::whereRaw('nickname = "'.$request->acccode.'"')->whereCrccode($request->crccode)->first()){
				
			}else{
				return Redirect::route('adjustment')->with('message', 'account not exist');
				exit;
			}
			
			$form->addInput('showonly', Lang::get('COMMON.ACCOUNT'), 'acccode', $accObj->nickname, array(), true);
			$form->addInput('hidden', Lang::get('COMMON.ACCOUNT'), 'acccode', $accObj->nickname, array(), true);
			$form->addInput('hidden', '', 'fee', true, array(), '');
			
			if($request->sadjOpt == 'rate') {
				$cashledger = new Cashledger;
				$form->addInput('showonly', Lang::get('COMMON.BALANCE'), 'balance',  $cashledger->getBalance($accObj->id), array(), true);
				$form->addInput('radio', Lang::get('COMMON.PAYMENTMETHOD') , 'payment' , '', array('options' => Adjustment::getTypeOptions()), true);
				$form->addInput('text', Lang::get('COMMON.AMOUNT') , 'amount', '', array(), true);
				
			}
			else if($request->sadjOpt == 'bonus') 
			{
				$promos = Promocampaign::getAllAsOptions('','',$accObj->crccode);
				App::arrayUnshift($promos, Lang::get('COMMON.SELECT'));
				
			
				$form->addInput('select', Lang::get('COMMON.BONUSCAMPAIGN'), 'pcpid', '', array('options' => $promos), true);
				$form->addInput('text', Lang::get('COMMON.AMOUNT'), 'amount', '', array(), true);
				
			}else if($request->sadjOpt == 'deposit') 
			{
			
				
				$form->addInput('text' , Lang::get('COMMON.AMOUNT'), 'amount'  , '', array(), true);
				$form->addInput('text' , Lang::get('COMMON.REFNO'),  'refno'   , '', array(), true);
				$channels[1] = 'Internet Transfer';
				$channels[2] = 'ATM Transfer';
				$channels[3] = 'Cash Deposit';
		
				$bhdid = 0;
				if($agtObj = Agent::find($accObj->agtid)) {
					$bhdid = $agtObj->bhdid;
				}
				
				$condition = 'bhdid = '.$bhdid.' AND crccode="'.$accObj->crccode.'" AND status = '.CBO_STANDARDSTATUS_ACTIVE;
				if($bankAccounts = Bankaccount::whereRaw($condition)->get()) {
					foreach($bankAccounts as $bankAccount) {
			
						if($bnkObj = Bank::find($bankAccount->bnkid)) {
							$bankOptions[$bankAccount->id] = $bnkObj->name . ' - ' . $bankAccount->bankaccname ;
						}
					}
				}
				$form->addInput('select',   Lang::get('COMMON.BANK'), 	 'bank', '', array('options' => $bankOptions), true);
				$form->addInput('select',     Lang::get('COMMON.CHANNEL'), 'channel', '', array('options' => $channels), true);
				$form->addInput('datetime', Lang::get('COMMON.DATE'), 	 'date',    '', array(), true);
				$promos = Promocampaign::getPromoByMemberId($accObj->id);
				$promoOptions['0'] = 'no bonus';
				foreach($promos as $pcpid => $promo) {
					$promoOptions[$promo['code']] = $promo['name'];
				}
				$form->addInput('select', Lang::get('COMMON.bonus'), 'bonus', '', array('options' => $promoOptions), true);
				
			}else if($request->sadjOpt == 'withdraw') 
			{
				$form->addInput('text'  , Lang::get('COMMON.AMOUNT'), 'amount'  , '', array(), true);
				$form->addInput('select', Lang::get('COMMON.BANK'), 'bank', '' , array('options' => Bank::getAllAsOptions()), true);
				$form->addInput('text'  , Lang::get('public.BankAccountNo'), 'accountno', '', array(), true);
			}
			$form->addInput('select', Lang::get('COMMON.REMARK') , 'remark' , '', array('options' => $remarks), true);

		}		
		elseif($step == 3)
		{
			$accObj = Account::whereRaw(' nickname = "'.$request->acccode.'" ')->whereCrccode($request->crccode)->first();
			if($request->sadjOpt == 'rate') 
			{
				$validator = Validator::make(
					[
						'payment'  => $request->input('payment'),
						'amount'   => $request->input('amount'),
						'remark'   => $request->input('remark'),
					],
					[
						'payment'    => 'required',
						'amount'    =>  'required|numeric',
						'remark'  	 => 'required',
					]
				);

				if ($validator->fails())
				{
					return Redirect()->back()->withErrors($validator)->withInput($request->all());
				}
				
				
				$adjObj = new Adjustment;
				$adjObj->type 			= $request->payment;
				$adjObj->accid 			= $accObj->id;
				$adjObj->acccode 		= $accObj->code;
				$adjObj->amount 		= $request->payment == 2 ? $request->amount * -1 : $request->amount;
				$adjObj->amountlocal 	= Currency::getLocalAmount($adjObj->amount, $accObj->crccode);
				$adjObj->crccode 		= $accObj->crccode;
				$adjObj->crcrate 		= Currency::getCurrencyRate($accObj->crccode);
				$adjObj->remark 		= $request->remark;
				$adjObj->status			= Adjustment::CBO_STATUS_PENDING;
				if($adjObj->save()) {
					$cashLedgerObj = new Cashledger;
					$cashLedgerObj->accid 			= $adjObj->accid;
					$cashLedgerObj->acccode 		= $adjObj->acccode;
					$cashLedgerObj->accname			= $accObj->fullname;
					$cashLedgerObj->chtcode 		= CBO_CHARTCODE_ADJUSTMENT;
					$cashLedgerObj->cashbalance 	= $cashLedgerObj->getBalance($adjObj->accid);
					$cashLedgerObj->amount 			= $adjObj->amount;
					$cashLedgerObj->amountlocal 	= $adjObj->amountlocal;
					$cashLedgerObj->refid 			= $adjObj->id;
					$cashLedgerObj->status 			= CBO_LEDGERSTATUS_PENDING;
					$cashLedgerObj->refobj 			= 'Adjustment';
					$cashLedgerObj->crccode 		= $adjObj->crccode;
					$cashLedgerObj->crcrate 		= Currency::getCurrencyRate($adjObj->crccode);
					$cashLedgerObj->bnkid			= 0;
					$cashLedgerObj->fee 			= $adjObj->amount;
					$cashLedgerObj->feelocal 		= $adjObj->amountlocal;
					$cashLedgerObj->bacid			= 0;
					$cashLedgerObj->createdip		= App::getRemoteIp();
					$cashLedgerObj->save();
				}
			}elseif($request->sadjOpt == 'bonus'){
				
				$validator = Validator::make(
					[
						'pcpid'  => $request->input('pcpid'),
						'amount'   => $request->input('amount'),
						'remark'   => $request->input('remark'),
					],
					[
						'pcpid'     => 'required',
						'amount'    =>  'required|numeric',
						'remark'  	 => 'required',
					]
				);

				if ($validator->fails())
				{
		
					return Redirect()->back()->withErrors($validator)->withInput($request->all());
				}
		
				
				$pcpObj = Promocampaign::find($request->pcpid);
				
				$promoCashObj = new Promocash;
				$promoCashObj->type			= Promocash::TYPE_MANUAL;
				$promoCashObj->accid	 	= $accObj->id;
				$promoCashObj->acccode	 	= $accObj->code;
				$promoCashObj->crccode	 	= $accObj->crccode;
				$promoCashObj->crcrate	 	= Currency::getCurrencyRate($accObj->crccode);
				$promoCashObj->amount	 	= $request->amount;
				$promoCashObj->amountlocal 	= Currency::getLocalAmount($request->amount, $accObj->crccode);
				$promoCashObj->pcpid		= $pcpObj->id;
				$promoCashObj->status		= Promocash::STATUS_PENDING;
				$promoCashObj->createdip	= App::getRemoteIp();
				
				if($promoCashObj->save()) {
					$cashLedgerObj = new Cashledger;
					$cashLedgerObj->accid 			= $promoCashObj->accid;
					$cashLedgerObj->acccode 		= $promoCashObj->acccode;
					$cashLedgerObj->accname 		= $accObj->fullname;
					$cashLedgerObj->chtcode 		= CBO_CHARTCODE_BONUS;
					$cashLedgerObj->remarkcode 		= $request->remark;
					$cashLedgerObj->amount 			= $promoCashObj->amount;
					$cashLedgerObj->amountlocal 	= $promoCashObj->amountlocal;
					$cashLedgerObj->fee 			= $promoCashObj->amount;
					$cashLedgerObj->feelocal 		= $promoCashObj->amountlocal;
					$cashLedgerObj->crccode 		= $promoCashObj->crccode;
					$cashLedgerObj->crcrate 		= $promoCashObj->crcrate;
					$cashLedgerObj->refobj			= 'Promocash';
					$cashLedgerObj->refid			= $promoCashObj->id;
					$cashLedgerObj->cashbalance		= $cashLedgerObj->getBalance($promoCashObj->accid);
					$cashLedgerObj->status			= CBO_LEDGERSTATUS_PENDING;
					$cashLedgerObj->createdip		= $promoCashObj->createdip;
					$cashLedgerObj->save();
					
				} 
			}else if($request->sadjOpt == 'deposit'){
				
				 $validator = Validator::make(
					[
			
						'bank'	   => $request->input('bank'),
						'bonus'	   => $request->input('bonus'),
						'amount'   => $request->input('amount'),
						//'refno'    => $request->input('refno'),
			
			
					],
					[

						'bank' 	   => 'required|numeric',
						'bonus'    => 'required|alpha_num',
						'amount'   => 'required|decimal|min:1',
						//'refno'    => 'required|alpha_num',
			
			
					]
				);

				
				if ($validator->fails())
				{
					echo json_encode($validator->errors());
					exit;
				}
		
				
				$bankaccountObj = Bankaccount::find($request->bank);
				
				$fdmObj = Fundingmethod::find(Configs::getParam('SYSTEM_PAYMENT_ID_BANKTRANSFER'));
			
				$paymentObj 					= new Banktransfer;
				$paymentObj->fdmid 				= Configs::getParam('SYSTEM_PAYMENT_ID_BANKTRANSFER');
				$paymentObj->type 				= CBO_CHARTCODE_DEPOSIT;
				$paymentObj->bnkid 				= $bankaccountObj->bnkid;
				$paymentObj->accid 				= $accObj->id;
				$paymentObj->acccode 			= $accObj->code;
				$paymentObj->crccode 			= $accObj->crccode;
				$paymentObj->crcrate 			= Currency::getCurrencyRate($paymentObj->crccode);
				$paymentObj->amount 			= $request->amount;
				$paymentObj->amountlocal 		= Currency::getLocalAmount($request->amount, $paymentObj->crccode);
				//$paymentObj->fee 				= round($fdmObj->getFee(CBO_CHARTCODE_DEPOSIT, $paymentObj->amount), 2);
				//$paymentObj->feelocal 			= Currency::getLocalAmount($paymentObj->fee, $paymentObj->crccode);
				$paymentObj->status 			= CBO_LEDGERSTATUS_PENDING;
				$paymentObj->createdip			= App::getRemoteIp();;
				$paymentObj->receiveaccname 	= $bankaccountObj->bankaccname;
				$paymentObj->receiveaccno   	= $bankaccountObj->bankaccno;
				$paymentObj->receiveacc 		= $bankaccountObj->id;
				$hours = $request->input('range') == 'PM' ? $request->input('hours') + 12 : $request->input('hours');
				
				if( $request->input('hours') == 12 && $request->input('range') == 'PM' ){
					$hours = 12;
				}
							if( $request->input('range') == '' ){
								$hours = $request->input('hours');
							}
				
				$paymentObj->transferdatetime   = $request->input('year').'-'.$request->input('month').'-'.$request->input('day').' '. $hours . ':' . $request->input('minutes') . ':00';
				$paymentObj->refno 			    = $request->refno;
				$paymentObj->transfertype 	    = $request->channel;
				if($paymentObj->save())
				{
				
						$cashLedgerObj = new Cashledger;
						$cashLedgerObj->accid 			= $paymentObj->accid;
						$cashLedgerObj->acccode 		= $paymentObj->acccode;
						$cashLedgerObj->accname			= $accObj->fullname;
						$cashLedgerObj->chtcode 		= CBO_CHARTCODE_DEPOSIT;
						$cashLedgerObj->cashbalance 	= $cashLedgerObj->getBalance($accObj->id);
						$cashLedgerObj->amount 			= $paymentObj->amount;
						$cashLedgerObj->amountlocal 	= $paymentObj->amountlocal;
						$cashLedgerObj->refid 			= $paymentObj->id;
						$cashLedgerObj->status 			= CBO_LEDGERSTATUS_PENDING;
						$cashLedgerObj->refobj 			= 'Banktransfer';
						$cashLedgerObj->remarks 		= $request->input('remark');
						$cashLedgerObj->crccode 		= $paymentObj->crccode;
						$cashLedgerObj->crcrate 		= Currency::getCurrencyRate($paymentObj->crccode);
						$cashLedgerObj->fdmid			= $fdmObj->id;
						$cashLedgerObj->bnkid			= $paymentObj->bnkid;
						//$cashLedgerObj->fee 			= round($fdmObj->getFee(CBO_CHARTCODE_DEPOSIT, $paymentObj->amount), 2);
						//$cashLedgerObj->feelocal 		= Currency::getLocalAmount($paymentObj->fee, $paymentObj->crccode);
						$cashLedgerObj->bacid			= $paymentObj->receiveacc;
						$cashLedgerObj->createdip		= $paymentObj->createdip;
						
						if($cashLedgerObj->save())
						{
							if($request->bonus != '0'){
								
								$promoObj = Promocampaign::whereRaw('startdate <= "'.date('Y-m-d H:i:s').'" AND enddate >= "'.date('Y-m-d H:i:s').'" AND status = '.CBO_STANDARDSTATUS_ACTIVE.' AND code = "'.$request->bonus.'"')->first();
								
								$promoCashObj = new Promocash;
								$promoCashObj->accid 	= $cashLedgerObj->accid;
								$promoCashObj->acccode  = $cashLedgerObj->acccode;
								$promoCashObj->chtcode  = CBO_CHARTCODE_DEPOSIT;
								$promoCashObj->pcpid 	= $promoObj->id;
								$promoCashObj->crccode  = $cashLedgerObj->crccode;
								$promoCashObj->type     = $promoObj->type;
								$promoCashObj->amount 	= $promoObj->getPromoCashAmount($cashLedgerObj->amount);
								$promoCashObj->amountlocal = Currency::getLocalAmount($promoCashObj->amount, $promoCashObj->crccode);
								$promoCashObj->crcrate = Currency::getCurrencyRate($promoCashObj->crccode);
								$promoCashObj->clgid = $cashLedgerObj->id;
								$promoCashObj->remarkcode = $promoObj->code;
								$promoCashObj->status = CBO_PROMOCASHSTATUS_CONFIRMED;
								$promoCashObj->createdip = App::getRemoteIp();;
								$promoCashObj->save();
								
							}
						}
					
				}
			}
			else if($request->sadjOpt == 'withdraw'){
				
				
		$validator = Validator::make(
			[
	
				'bank'	      => $request->input('bank'),
				'accountno'   => $request->input('accountno'),
				'amount'      => $request->input('amount'),
			],
			[

				'bank' 	      => 'required',
				'accountno'   => 'required|numeric',
				'amount'      => 'required|decimal|min:1',

			]
		);
		
		
		if ($validator->fails())
		{
			echo json_encode($validator->errors());
			exit;
		}

				
			if($request->input('amount') > $this->mainwallet(false, $accObj->id)) {
				echo json_encode( array( 'amount' => Lang::get('COMMON.ERROR_NETAMOUNT') ) );
				exit;
			}
				
				
				if($bnkObj = Bank::find($request->input('bank'))) {
					
				
					if(!$accBnkObj = Accountbank::whereRaw('bankid = '.$bnkObj->id.' AND accid = '. $accObj->id.' AND bankaccno = '. $request->input('accountno'))->first()) {
						
						$accBnkObj = new Accountbank;
						$accBnkObj->accid 		= $accObj->id;
						$accBnkObj->bankid 	    = $bnkObj->id;
						$accBnkObj->acccode	    = $accObj->code;
						$accBnkObj->bankaccname = $accObj->fullname;
						$accBnkObj->bankcode 	= $bnkObj->code;
						$accBnkObj->bankname 	= $bnkObj->name;
						$accBnkObj->bankaccno 	= $request->input('accountno');
						$accBnkObj->save();
					}
					
					$fdmObj = Fundingmethod::find(1);
		
					$paymentObj = new Banktransfer;
					$paymentObj->fdmid = $fdmObj->id;
					$paymentObj->type = CBO_CHARTCODE_WITHDRAWAL;
					$paymentObj->bnkid = $accBnkObj->bankid;
					$paymentObj->accid = $accObj->id;
					$paymentObj->acccode = $accObj->code;
					$paymentObj->crccode = $accObj->crccode;
					$paymentObj->crcrate = Currency::getCurrencyRate($paymentObj->crccode);
					
					if( Config::get('setting.front_path') == 'sbm' )
					{
						$paymentObj->amount 	 = -1 * ( $request->input('amount') * 95 / 100 );
						$paymentObj->amountlocal = -1 * Currency::getLocalAmount(( $request->input('amount') * 95 / 100 ), $paymentObj->crccode);
					}
					else
					{
						$paymentObj->amount 	 = -1 * $request->input('amount');
						$paymentObj->amountlocal = -1 * Currency::getLocalAmount($request->input('amount'), $paymentObj->crccode);
					}
				
					
					$paymentObj->bankaccno = $accBnkObj->bankaccno;
					$paymentObj->bankaccname = $accBnkObj->bankaccname;
					$paymentObj->status = CBO_LEDGERSTATUS_PENDING;
					$paymentObj->createdip		= App::getRemoteIp();;
					
					if ($paymentObj->save()) {
						
						$cashLedgerObj = new Cashledger;
						$cashLedgerObj->accid 			= $paymentObj->accid;
						$cashLedgerObj->acccode 		= $paymentObj->acccode;
						$cashLedgerObj->accname			= $accObj->fullname;
						$cashLedgerObj->chtcode 		= CBO_CHARTCODE_WITHDRAWAL;
						$cashLedgerObj->cashbalance 	=  $cashLedgerObj->getBalance($accObj->id);
						$cashLedgerObj->amount 			= $paymentObj->amount;
						$cashLedgerObj->amountlocal 	= $paymentObj->amountlocal;
						$cashLedgerObj->refid 			= $paymentObj->id;
						$cashLedgerObj->status 			= CBO_LEDGERSTATUS_PENDING;
						$cashLedgerObj->refobj 			= 'Banktransfer';
						$cashLedgerObj->crccode 		= $paymentObj->crccode;
						$cashLedgerObj->crcrate 		= Currency::getCurrencyRate($paymentObj->crccode);
						$cashLedgerObj->fdmid			= $fdmObj->id;
						$cashLedgerObj->bnkid			= $paymentObj->bnkid;
						//$cashLedgerObj->fee 			= round($fdmObj->getFee(CBO_CHARTCODE_DEPOSIT, $paymentObj->amount), 2);
						//$cashLedgerObj->feelocal 		= Currency::getLocalAmount($paymentObj->fee, $paymentObj->crccode);
						$cashLedgerObj->bacid			= 0;
						$cashLedgerObj->createdip		= $paymentObj->createdip;
						
						if($cashLedgerObj->save())
						{
							if( Config::get('setting.front_path') == 'sbm' )
							{
								$cashLedgerObj = new Cashledger;
								$cashLedgerObj->accid 			= $paymentObj->accid;
								$cashLedgerObj->acccode 		= $paymentObj->acccode;
								$cashLedgerObj->accname			= $accObj->fullname;
								$cashLedgerObj->chtcode 		= CBO_CHARTCODE_WITHDRAWALCHARGES;
								$cashLedgerObj->cashbalance 	= $cashLedgerObj->getBalance($accObj->id);
								$cashLedgerObj->amount 			= -1 * ( $request->input('amount') * 5 / 100 );
								$cashLedgerObj->amountlocal 	= -1 * Currency::getLocalAmount(( $request->input('amount') * 5 / 100 ), $paymentObj->crccode);
								$cashLedgerObj->refid 			= $paymentObj->id;
								$cashLedgerObj->status 			= CBO_LEDGERSTATUS_PENDING;
								$cashLedgerObj->refobj 			= 'Banktransfer';
								$cashLedgerObj->crccode 		= $paymentObj->crccode;
								$cashLedgerObj->crcrate 		= Currency::getCurrencyRate($paymentObj->crccode);
								$cashLedgerObj->fdmid			= $fdmObj->id;
								$cashLedgerObj->bnkid			= $paymentObj->bnkid;
								$cashLedgerObj->fee 			= round($fdmObj->getFee(CBO_CHARTCODE_DEPOSIT, $paymentObj->amount), 2);
								$cashLedgerObj->feelocal 		= Currency::getLocalAmount($paymentObj->fee, $paymentObj->crccode);
								$cashLedgerObj->bacid			= 0;
								$cashLedgerObj->createdip		= $paymentObj->createdip;
								$cashLedgerObj->save();
							}
							$success = true;

						}
					}
					
				}
			}
			echo Lang::get('COMMON.SUCESSFUL');
		}
		

	
		$data['form']   = $form->getTemplateVars();
		$data['module'] = 'adjustment';

		return view('admin.adjustment',$data);

	}	
	
	private function mainwallet($numberformat = false , $accid){
		
		$condition = '((status in ('.CBO_LEDGERSTATUS_PENDING.', '.CBO_LEDGERSTATUS_MANPENDING.','.CBO_LEDGERSTATUS_CONFIRMED.', '.CBO_LEDGERSTATUS_MANCONFIRMED.', '.CBO_LEDGERSTATUS_PROCESSED.') AND chtcode IN ('.CBO_CHARTCODE_WITHDRAWAL.', '.CBO_CHARTCODE_BALANCETRANSFER.', '.CBO_CHARTCODE_WITHDRAWALCHARGES.' )) OR (status in ('.CBO_LEDGERSTATUS_CONFIRMED.', '.CBO_LEDGERSTATUS_MANCONFIRMED.') AND chtcode NOT IN ('.CBO_CHARTCODE_WITHDRAWAL.', '.CBO_CHARTCODE_BALANCETRANSFER.' , '.CBO_CHARTCODE_WITHDRAWALCHARGES.' )))';


		$condition .= ' AND accid = '. $accid;
		
		$balance = Cashledger::whereRaw($condition)->sum('amount');

		if( $numberformat == true){
			echo number_format($balance, 2, '.', '');
		}else{
			return $balance;
		}
		
	}
	

	
}	

	

