<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller\Admin;
use App\libraries\grid\PanelGrid;
use App\libraries\App;
use App\libraries\providers\PSB;
use App\Models\Currency;
use Excel;
use Illuminate\Http\Request;
use Auth;
use Validator;
use Session;
use Lang;
use Hash;
use Config;
use App\Models\Affiliate;
use App\Models\Affiliatesetting;
use App\Models\Affiliatecredit;
use App\Models\Agent;
use App\Models\Account;
use App\Models\Affiliateregister;
use App\Models\Profitloss;
use App\Models\Cashledger;
use App\Models\Commissionsetting;
use App\Models\Commission;
use App\Models\Wagersetting;
use App\Models\Ledgersetting;
use App\Models\Configs;
use App\Models\Product;
use App\Models\Cashcard;
use App\Models\Withdrawcard;


class AffiliateController extends Controller{


	protected $moduleName 	= 'affiliate';
	protected $limit 		= 20;
	protected $start 		= 0;
	
	public function __construct()
	{
		
	}
	
	public function showaffiliateReport(Request $request){

	
			$grid = new PanelGrid;
        	$grid->addSearchField('search', array('username' => Lang::get('COMMON.AFFILIATE')));
        	$grid->addFilter('crccode', Currency::getAllCurrencyAsOptions(), array('display' => Lang::get('COMMON.CURRENCY') , 'value' => ((isset($this->filterValue['crccode']))?$this->filterValue['crccode']:$request->input('crccode'))));
			$grid->setupGrid($this->moduleName, 'affiliateReport-data', $this->limit, true, array('footer' => true, 'params' => array( 'createdfrom' => $request->createdfrom , 'aflid ' => $request->aflid, 'type' => $request->type )));
			$grid->setTitle(Lang::get('COMMON.AFFILIATE'));
					
			$grid->addColumn('affiliate', 		Lang::get('COMMON.AFFILIATE'), 					100,		array('align' => 'center'));			
			$grid->addColumn('register', 		Lang::get('COMMON.REGISTRATION'), 				100,		array('align' => 'center'));			
			$grid->addColumn('betmember', 		Lang::get('COMMON.BETMEMBER'), 					100,		array('align' => 'center'));			
			$grid->addColumn('deposit', 		Lang::get('COMMON.DEPOSIT'), 					100,		array('align' => 'center'));			
			$grid->addColumn('withdrawal', 		Lang::get('COMMON.WITHDRAWAL'), 				100,		array('align' => 'center'));			
			$grid->addColumn('turnover', 		Lang::get('COMMON.TURNOVER'), 					100,		array('align' => 'center'));			
			$grid->addColumn('profitloss', 		Lang::get('COMMON.PROFITLOSS'), 				100,		array('align' => 'center'));			
			$grid->addColumn('bonus', 			Lang::get('COMMON.BONUS'), 						100,		array('align' => 'center'));			
			$grid->addColumn('incentive', 		Lang::get('COMMON.INCENTIVE'), 					100,		array('align' => 'center'));			
			$grid->addColumn('netpnl', 			Lang::get('COMMON.NETPROFITLOSS'), 				100,		array('align' => 'center'));			
			$grid->addColumn('rate', 			Lang::get('COMMON.RATE'), 						100,		array('align' => 'center'));			
			$grid->addColumn('commission', 		Lang::get('COMMON.COMMISSION'), 				100,		array('align' => 'center'));

        	$grid->addButton('2', 'Export' , '', 'button', array('icon' => 'ok', 'url' => 'export_excel();'));

			$data['grid'] = $grid->getTemplateVars();

			return view('admin.grid2',$data);
    }
	
	protected function showaffiliateReportdoGetData(Request $request){

        $timefrom = $request->createdfrom.' 00:00:00';
        $timeto	  = $request->createdto.' 23:59:59';
        $datefrom = $request->createdfrom;
        $dateto   = $request->createdto;
		
		$aflid = $request->input('aflid');
		$type = $request->input('type');
 
		$condition = 'crccode = "'.Session::get('admin_crccode').'" ';

        $aflt  = $request->input('aflt');
        $aqfld = $request->input('aqfld');
		if(!empty($aflid) && !empty($type)){
			$condition .= ' AND parentid = '.$aflid.' AND type='.$type.'';
		}else if(empty($aflid)){
			$condition .= 'AND type=2';
			if( isset($aqfld['username']) && $aqfld['username'] != '' ){
				$condition .= ' AND username = \''.$aqfld['username'] . '\'';
			}elseif( isset($aflt['username']) && $aflt['username'] != '' ){
				$condition .= ' AND username = \''.$aflt['username'] . '\'';
			}
		}

		$total = Affiliate::whereRaw($condition)->count();
		$rows = array();
        $total_register = 0;
        $total_betmember = 0;
        $total_deposit = 0;
        $total_withdrawal = 0;
        $total_bonus = 0;
        $total_incentive = 0;
        $total_turnover = 0;
        $total_profitloss = 0;
        $total_commission = 0;
        $total_rebate = 0;
        $total_fee = 0;
        $total_payout = 0;
        $total_netpnl = 0;

        if($affiliates = Affiliate::whereRaw($condition)->skip($request->recordstartindex)->take( $request->pagesize )->get()) {

            $currency = (isset($aqfld['crccode']) ? $aqfld['crccode'] : '');
			
            foreach($affiliates as $affiliate) {
				
				$register = 0;
				$betmember = 0;
				$rate = 0;
				$deposit = 0;
				$withdrawal = 0;
				$bonus = 0;
				$incentive = 0;
				$turnover = 0;
				$profitloss = 0;
				$commission = 0;
				$rebate = 0;
				$fee = 0;
				$payout = 0;
				$netpnl = 0;
				$member_ids = array();

				if($members = $affiliate->getAllAccountById($affiliate->id, true)){
					foreach($members as $member) {
					    if ($currency != '') {
                            if ($member->crccode != $currency) {
                                continue;
                            }
                        }

						$member_ids[] = $member->id;
						if($member->created >= $timefrom && $member->created <= $timeto) 
						{
							$register += 1;
						}
						if(Profitloss::whereRaw('category NOT IN(6) AND accid='.$member->id.' AND date >= "'.$datefrom.'" AND date <= "'.$dateto.'"')->count() != 0 )
						{
							$betmember += 1;
						}
					}
				}
				
				
				
				if(count($member_ids) > 0) {

                    if( $currency != '' ){
                        $memberObj = Account::where('crccode', '=', $currency)->where('id', '=', $member_ids[0])->first();
                    } else {
                        $memberObj = Account::find($member_ids[0]);
					}

					if ($memberObj) {
                        $commission = 0;
                        $rebate = 0;
                        $deposit = Cashledger::whereRaw('chtcode = '.CBO_CHARTCODE_DEPOSIT.' AND accid IN ('.implode(',', $member_ids).') AND created >= "'.$timefrom.'" AND created <= "'.$timeto.'" AND status IN ('.CBO_LEDGERSTATUS_CONFIRMED.', '.CBO_LEDGERSTATUS_MANCONFIRMED.')')->sum('amount');
                        $withdrawal = Cashledger::whereRaw('chtcode = '.CBO_CHARTCODE_WITHDRAWAL.' AND accid IN ('.implode(',', $member_ids).') AND created >= "'.$timefrom.'" AND created <= "'.$timeto.'" AND status IN ('.CBO_LEDGERSTATUS_CONFIRMED.', '.CBO_LEDGERSTATUS_MANCONFIRMED.')')->sum('amount');
                        $bonus = Cashledger::whereRaw('chtcode = '.CBO_CHARTCODE_BONUS.' AND accid IN ('.implode(',', $member_ids).') AND created >= "'.$timefrom.'" AND created <= "'.$timeto.'" AND status IN ('.CBO_LEDGERSTATUS_CONFIRMED.', '.CBO_LEDGERSTATUS_MANCONFIRMED.')')->sum('amount');
                        $incentive = Cashledger::whereRaw('chtcode = '.CBO_CHARTCODE_INCENTIVE.' AND accid IN ('.implode(',', $member_ids).') AND created >= "'.$timefrom.'" AND created <= "'.$timeto.'" AND status IN ('.CBO_LEDGERSTATUS_CONFIRMED.', '.CBO_LEDGERSTATUS_MANCONFIRMED.')')->sum('amount');
                        $turnover = Profitloss::whereRaw('category NOT IN(6) AND accid IN ('.implode(',', $member_ids).') AND date >= "'.$datefrom.'" AND date <= "'.$dateto.'"')->sum('totalstake');
                        $profitloss = Profitloss::whereRaw('category NOT IN(6) AND accid IN ('.implode(',', $member_ids).') AND date >= "'.$datefrom.'" AND date <= "'.$dateto.'"')->sum('amount');

                        $profitloss = ($memberObj->crccode == 'VND' || $memberObj->crccode == 'IDR')? $profitloss * 1000 : $profitloss;
                        $turnover   = ($memberObj->crccode == 'VND' || $memberObj->crccode == 'IDR')? $turnover * 1000 : $turnover;

                        $netpnl = $profitloss + ($bonus + $incentive);
					}
					
				}
				
				//get commission scheme
				if($netpnl < 0) {
					$settings = array();
					$pnl = $netpnl * -1;
					if($tiers = Commissionsetting::whereRaw($affiliate->comid)->orderBy('rangefrom','ASC')->get()) {
						$balance = $pnl;
						$start = 0;
						foreach($tiers as $tier) {
							
							if( Config::get('setting.front_path') == 'sbm' && $affiliate->username == 'c')
								$percentage = 0;
							else
								$percentage = $tier->percentage;
							
							if($pnl > 0 && ($pnl >= $tier->rangefrom)) {
								$rate = $percentage;
								if($pnl > $tier->rangeto && $tier->rangeto > 0) {
									$commission += ($percentage * ($tier->rangeto - $start) / 100);
								} else
								$commission += ($percentage * ($pnl - $start) / 100);
								
								$start = $tier->rangeto;
							}
						}
					}
				}

				$total_register += $register;
				$total_betmember += $betmember;
                $total_deposit += $deposit;
                $total_withdrawal += $withdrawal;
                $total_turnover += $turnover;
                $total_profitloss += $profitloss;
                $total_bonus += $bonus;
                $total_incentive += $incentive;
                $total_netpnl += $netpnl;
                $total_commission += $commission;

                $registerTabFilter = array('aflid' => $affiliate->id, 'createdfrom' => $datefrom, 'createdto' => $dateto);
                $memberTabFilter = array('aflid' => $affiliate->id, 'betmemonly' => 'Y', 'createdfrom' => $datefrom, 'createdto' => $dateto);

                if ($currency != '') {
                    $registerTabFilter['crccode'] = $currency;
                    $memberTabFilter['crccode'] = $currency;
                }

                $rows[] = array(
					'affiliate' => App::formatAddTabUrl(Lang::get('COMMON.AFFILIATEREPORT').': '.$affiliate->username, $affiliate->username, action('Admin\AffiliateController@showMemberReport', array('aflid' => $affiliate->id, 'createdfrom' => $datefrom, 'createdto' => $dateto))),
					'register' => ($register > 0 ? App::formatAddTabUrl( Lang::get('COMMON.MEMBERENQUIRY'), $register, action('Admin\MemberEnquiryController@index', $registerTabFilter) ) : $register),
					'betmember' => ($betmember > 0 ? App::formatAddTabUrl( Lang::get('COMMON.MEMBERENQUIRY'), $betmember, action('Admin\MemberEnquiryController@index', $memberTabFilter)) : $betmember ),
					'deposit' => App::displayAmount($deposit),
					'withdrawal' => App::displayAmount($withdrawal),
					'bonus' => App::formatNegativeAmount($bonus),
					'incentive' => App::formatNegativeAmount($incentive),
					'turnover' => App::formatAddTabUrl(Lang::get('COMMON.AFFILIATEREPORT').': '.$affiliate->username, App::formatNegativeAmount($turnover), action('Admin\AffiliateController@showMemberReport', array('iaflid' => $affiliate->id, 'createdfrom' => $datefrom, 'createdto' => $dateto))),
					'profitloss' => App::formatNegativeAmount($profitloss),
					'rate' => App::displayPercentage($rate),
					'commission' => App::formatNegativeAmount($commission),
					'fee' => App::formatNegativeAmount($fee),
					'payout' => App::formatNegativeAmount($payout),
					'netpnl' => App::formatNegativeAmount($netpnl),
				);				
            }
        }

        $footers[] = array(
            'affiliate'	=> Lang::get('COMMON.TOTAL'),
            'register' => $total_register,
            'betmember' => $total_betmember,
            'deposit' => App::displayAmount($total_deposit),
            'withdrawal' => App::displayAmount($total_withdrawal),
            'turnover' => App::displayAmount($total_turnover),
            'profitloss' => App::formatNegativeAmount($total_profitloss),
            'bonus' => App::formatNegativeAmount($total_bonus),
            'incentive' => App::formatNegativeAmount($total_incentive),
            'netpnl' => App::formatNegativeAmount($total_netpnl),
            'commission' => App::formatNegativeAmount($total_commission),
        );
		
		//var_dump($deposit);
		//exit();

        // Export as Excel file.
        if ($request->input('export') == 'excel') {
            // Check and remove unwanted HTML format from values.
            $epRows = array();

            foreach ($rows as $rkey => $rval) {
                $epRows2 = array();

                foreach ($rval as $rvkey => $rvval) {
                    $epRows2[$rvkey] = preg_replace('/<[^>]*>/', '', $rvval);
                }

                $epRows[$rkey] = $epRows2;
            }

            Excel::create('Affiliate_Report_'.date('U'), function($excel) use ($epRows, $footers) {
                $excel->sheet('Sheet1', function($sheet) use ($epRows, $footers) {
                    $sheet->fromArray($epRows);
                });
            })->export('xls');
        }
		
		echo json_encode(array('total' => $total, 'rows' => $rows, 'footer' => $footers));
		exit;
	
	}
        
    public function showagentReport(Request $request){

            $id = $request->has('sid') ? $request->get('sid') : 0;
	    
            $datefrom = ((isset($request['sfrom']))?$request['sfrom']:App::getDateTime(3));
            $dateto = ((isset($request['sto']))?$request['sto']:App::getDateTime(3));
	    
            // AG = Lang::get('COMMON.AGENT');
            $username = Lang::get('COMMON.AFFILIATE');
            if($id > 0) {
                    $url = '';
                    $aflObj = Affiliate::where( 'id' , '=' , $id )->first();
                    $username = $aflObj->username;
            }
			
			if( $request->has('sfrom') )
			{
				Session::put('agt_report_createdfrom',$request->sfrom);
				Session::put('agt_report_createdto'  ,$request->sto);
			}else{
				Session::put('agt_report_createdfrom',$datefrom);
				Session::put('agt_report_createdto'  ,$dateto);
			}
            
            $grid = new PanelGrid;
			$grid->setupGrid($this->moduleName, 'agentReport-data', $this->limit, true, array( 'username' => $username , 'footer' => true , 'params' => array( 'createdfrom' => Session::get('agt_report_createdfrom') , 'createdto' => Session::get('agt_report_createdto') )) );
            $grid->setTitle(Lang::get('COMMON.AGENTREPORT')." : ".$username);
			$grid->addSearchField('search', array('username' => Lang::get('COMMON.AFFILIATE')));
            $grid->addColumn('affiliate', 		Lang::get('COMMON.AFFILIATE'), 					100,	array('align' => 'center'));			
            $grid->addColumn('currency', 		Lang::get('COMMON.SHAREPERCENTAGE'),            60,		array('align' => 'center'));					
            $grid->addColumn('deposit', 		Lang::get('COMMON.DEPOSIT'), 					90,		array('align' => 'center'));			
            $grid->addColumn('withdrawal', 		Lang::get('COMMON.WITHDRAWAL'), 				90,		array('align' => 'center'));					
            $grid->addColumn('bonus', 			Lang::get('COMMON.BONUS'), 						90,     array('align' => 'center'));			
            $grid->addColumn('incentive', 		Lang::get('COMMON.INCENTIVE'), 					90,		array('align' => 'center'));
            $grid->addColumn('turnover', 		Lang::get('COMMON.TURNOVER'), 					60,		array('align' => 'center'));			
            $grid->addColumn('profitloss', 		Lang::get('COMMON.PROFITLOSS'), 				90,		array('align' => 'center'));
			if(  Config::get('setting.opcode') == 'IFW' )
			$grid->addColumn('netwinloss', 		Lang::get('COMMON.NETTWINLOSS'), 				90,		array('align' => 'right'));
            

			$grid->addColumn('awinloss', 		Lang::get('COMMON.WINLOSE'), 					90,		array('align' => 'center', 'cellclass' => 'leftline'));
            $grid->addColumn('apromo',      Lang::get('COMMON.PROMOTION'), 						90,		array('align' => 'center' ));
            $grid->addColumn('atotal',      Lang::get('COMMON.TOTAL'), 							90,		array('align' => 'center'));
            
            $grid->addColumn('bwinloss', 	Lang::get('COMMON.WINLOSE'), 						90,		array('align' => 'center', 'cellclass' => 'leftline'));
            $grid->addColumn('bpromo',      Lang::get('COMMON.PROMOTION'), 						90,		array('align' => 'center'));
            $grid->addColumn('btotal',      Lang::get('COMMON.TOTAL'), 							90,		array('align' => 'center'));
            
	
            $grid->addColumn('cwinloss', 	Lang::get('COMMON.WINLOSE'), 						90,		array('align' => 'center', 'cellclass' => 'leftline'));
            $grid->addColumn('cpromo',      Lang::get('COMMON.PROMOTION'), 						90,		array('align' => 'center'));
            $grid->addColumn('ctotal',      Lang::get('COMMON.TOTAL'), 							90,		array('align' => 'center'));

	    $grid->addSearchField('sid', 		array('sid' => 'sid'), array('value' => $id, 'hidden' => true));
	    $grid->addSearchField('iaflid', 	array('iaflid' => 'iaflid'), array('value' => $request->get('iaflid'), 'hidden' => true));
	    $grid->addSearchField('agtmemrpt',  array('agtmemrpt' => 'agtmemrpt'), array('value' => $request->get('agtmemrpt'), 'hidden' => true));
	    
	    if ($id > 0) $grid->addButton('2', 'back' , '', 'button', array('icon' => 'ok', 'url' => 'window.history.back();'));
            
            $data['grid'] = $grid->getTemplateVars();
            $data['sid'] = $id;
            $data['username'] = $username;
            return view('admin.grid2',$data);
        }

	protected function showagentReportdoGetData(Request $request, $return = false){
		

        $aflt   = $request->input('aflt');
	    
	    if (!is_array($aflt)) $aflt = array();
	    
	    if (isset($aflt['agtmemrpt']) && $aflt['agtmemrpt'] == 'y') {
		    return $this->showagentmemberReportdoGetData($request, $return);
	    }

            $timefrom = $request->createdfrom.' 00:00:00';
            $timeto   = $request->createdto.' 23:59:59';
            $datefrom = $request->createdfrom;
            $dateto   = $request->createdto;

	    $id = (isset($aflt['sid']) ? $aflt['sid'] : 0);
		
		if(!empty($aflt['username'])){
            $condition = 'type = 1 AND (id = '.$id.' OR parentid = '.$id.') AND crccode = "'.Session::get('admin_crccode').'" AND username = "'.$aflt['username'].'"';
		}else{
            $condition = 'type = 1 AND (id = '.$id.' OR parentid = '.$id.') AND crccode = "'.Session::get('admin_crccode').'"';
		}
            $total = Affiliate::whereRaw($condition)->count();
            $rows = array();
            $total_deposit = 0;
            $total_withdrawal = 0;
            $total_bonus = 0;
            $total_incentive = 0;
            $total_turnover = 0;
            $total_profitloss = 0;
            $total_netwinloss = 0;
            $total_commission = 0;
            $total_rebate = 0;
            $total_fee = 0;
            $total_payout = 0;
            $total_netpnl = 0;
            $total_apromo = 0;
            $total_bpromo = 0;
            $total_cpromo = 0;
            $total_awinloss = 0;
            $total_bwinloss = 0;
            $total_cwinloss = 0;
	    
            if($affiliates = Affiliate::whereRaw($condition)->skip($request->recordstartindex)->take( $request->pagesize )->get()) {
                foreach($affiliates as $affiliate) {
                    $deposit = 0;
                    $withdrawal = 0;
                    $bonus = 0;
                    $incentive = 0;
                    $turnover = 0;
                    $profitloss = 0;
					$netwinloss = 0;
                    $commission = 0;
                    $rebate = 0;
                    $fee = 0;
                    $payout = 0;
                    $netpnl = 0;
                    $apromo = 0;
                    $bpromo = 0;
                    $cpromo = 0;
                    $awinloss = 0;
                    $bwinloss = 0;
                    $cwinloss = 0;
                    $member_ids = array();

                    $whole_downline = true;
                    if($id == $affiliate->id) $whole_downline = false;
                    
                    $downline_ids = array();
                    $aff_ids = array($affiliate->id);
                    $crccode =  Agent::whereRaw('aflid='.$affiliate->id)->pluck('crccode');
                    
                    if($whole_downline)
                    if($downlines = $affiliate->getAllDownlineById2($affiliate->id, $whole_downline)) {
                        foreach($downlines as $downline) {
							if(isset($downline->id))
                                $aff_ids[] = $downline->id;
                        }
                    }
                    //var_dump($aff_ids);
                    if($members = $affiliate->getAllAccountById($affiliate->id, $whole_downline))
                        foreach($members as $member) {
                            $member_ids[] = $member->id;
                    }
          
                    if(count($member_ids) > 0) {
                        $commission = 0;
                        $rebate = 0;
                        $deposit = Cashledger::whereRaw('chtcode = '.CBO_CHARTCODE_DEPOSIT.' AND accid IN ('.implode(',', $member_ids).') AND created >= "'.$timefrom.'" AND created <= "'.$timeto.'" AND status IN ('.CBO_LEDGERSTATUS_CONFIRMED.', '.CBO_LEDGERSTATUS_MANCONFIRMED.')')->sum('amount');                        
                        $withdrawal = Cashledger::whereRaw('chtcode = '.CBO_CHARTCODE_WITHDRAWAL.' AND accid IN ('.implode(',', $member_ids).') AND created >= "'.$timefrom.'" AND created <= "'.$timeto.'" AND status IN ('.CBO_LEDGERSTATUS_CONFIRMED.', '.CBO_LEDGERSTATUS_MANCONFIRMED.')')->sum('amount');
                        $bonus = Cashledger::whereRaw('chtcode = '.CBO_CHARTCODE_BONUS.' AND accid IN ('.implode(',', $member_ids).') AND created >= "'.$timefrom.'" AND created <= "'.$timeto.'" AND status IN ('.CBO_LEDGERSTATUS_CONFIRMED.', '.CBO_LEDGERSTATUS_MANCONFIRMED.')')->sum('amount');
						$incentive = Cashledger::whereRaw('chtcode = '.CBO_CHARTCODE_INCENTIVE.' AND accid IN ('.implode(',', $member_ids).') AND created >= "'.$timefrom.'" AND created <= "'.$timeto.'" AND status IN ('.CBO_LEDGERSTATUS_CONFIRMED.', '.CBO_LEDGERSTATUS_MANCONFIRMED.')')->sum('amount');
						
						
                        if($affiliate->id == $id) {
                            $awinloss = 0;
                            $apromo = 0;
                            $cwinloss = (float) Wagersetting::whereRaw('aflid='.$affiliate->id.' AND share2id='.$affiliate->parentid.' AND date >= "'.$datefrom.'" AND date <= "'.$dateto.'"')->sum('share1');
                            $cpromo = (float) Ledgersetting::whereRaw('aflid='.$affiliate->id.' AND share2id='.$affiliate->parentid.' AND date >= "'.$datefrom.'" AND date <= "'.$dateto.'"')->sum('share1');
                            $cpromo *= -1;

                            $turnover = (float) Wagersetting::whereRaw('aflid='.$affiliate->id.' AND share2id='.$affiliate->parentid.' AND date >= "'.$datefrom.'" AND date <= "'.$dateto.'"')->sum('stake');
                            $profitloss = (float) Wagersetting::whereRaw('aflid='.$affiliate->id.' AND share2id='.$affiliate->parentid.' AND date >= "'.$datefrom.'" AND date <= "'.$dateto.'"')->sum('profitloss');
							
							$bwinloss = (float) Wagersetting::whereRaw('aflid = '.$affiliate->id.' AND share2id='.$affiliate->parentid.' AND date >= "'.$datefrom.'" AND date <= "'.$dateto.'"')->sum('share1')*-1;
                            $bpromo = (float)  Ledgersetting::whereRaw('aflid='.$affiliate->id.' AND share2id='.$affiliate->parentid.' AND date >= "'.$datefrom.'" AND date <= "'.$dateto.'"')->sum('share1');
                        }
                        else {
                            if($id == 0) {
                                    $apromo = (float) Ledgersetting::whereRaw('share1id = '.$affiliate->id.' AND share2id='.$affiliate->parentid.' AND date >= "'.$datefrom.'" AND date <= "'.$dateto.'"')->sum('amount');
                                    $awinloss = (float) Wagersetting::whereRaw('share1id = '.$affiliate->id.' AND share2id='.$affiliate->parentid.' AND date >= "'.$datefrom.'" AND date <= "'.$dateto.'"')->sum('profitloss');
                                    $cpromo = (float) Ledgersetting::whereRaw('share1id='.$affiliate->id.' AND date >= "'.$datefrom.'" AND date <= "'.$dateto.'"')->sum('share2');
                                    $cwinloss = (float) Wagersetting::whereRaw(' share1id = '.$affiliate->id.' AND date >= "'.$datefrom.'" AND date <= "'.$dateto.'"')->sum('share2');

                                    $turnover = (float) Wagersetting::whereRaw(' share1id = '.$affiliate->id.' AND date >= "'.$datefrom.'" AND date <= "'.$dateto.'"')->sum('stake');
                                    $profitloss = (float) Wagersetting::whereRaw(' share1id = '.$affiliate->id.' AND date >= "'.$datefrom.'" AND date <= "'.$dateto.'"')->sum('profitloss');
									$awinloss = $profitloss;
                            } else {
                                    $apromo = (float) Ledgersetting::whereRaw('share1id = '.$affiliate->id.' AND share2id='.$affiliate->parentid.' AND date >= "'.$datefrom.'" AND date <= "'.$dateto.'"')->sum('share1');
                                    $awinloss = (float) Wagersetting::whereRaw('share1id = '.$affiliate->id.' AND share2id='.$affiliate->parentid.' AND date >= "'.$datefrom.'" AND date <= "'.$dateto.'"')->sum('share1');
                                    $cpromo = (float) Ledgersetting::whereRaw('aflid IN ('.implode(',', $aff_ids).') AND share1id = '.$affiliate->parentid.' AND date >= "'.$datefrom.'" AND date <= "'.$dateto.'"')->sum('share1');
                                    $cwinloss = (float) Wagersetting::whereRaw('aflid IN ('.implode(',', $aff_ids).') AND share1id = '.$affiliate->parentid.' AND date >= "'.$datefrom.'" AND date <= "'.$dateto.'"')->sum('share1');
                                    $awinloss *= -1;
                                    //$apromo *= -1;
                                    $cpromo *= -1;
				

                                    $turnover = (float) Wagersetting::whereRaw(' share1id = '.$affiliate->id.' AND date >= "'.$datefrom.'" AND date <= "'.$dateto.'"')->sum('stake');
                                    $profitloss = (float) Wagersetting::whereRaw(' share1id = '.$affiliate->id.' AND date >= "'.$datefrom.'" AND date <= "'.$dateto.'"')->sum('profitloss');
								
                            }
							
							 $bpromo =  ($cpromo + $apromo) * -1;
							 $bwinloss =  ($cwinloss + $awinloss) * -1;
                        }
                        
                       

                        $total_awinloss += $awinloss;
                        $total_bwinloss += $bwinloss;
                        $total_cwinloss += $cwinloss;

                        $total_apromo += $apromo;
                        $total_bpromo += $bpromo;
                        $total_cpromo += $cpromo;                       
                    }
                    
                    $total_deposit += $deposit;
                    $total_withdrawal += $withdrawal;
                    $total_bonus += $bonus;
                    $total_incentive += $incentive;
                    $total_turnover += $turnover;
                    $total_profitloss += $profitloss;
                    $total_netwinloss += $profitloss * 0.85;
                    
                    if($has_downline = Affiliate::whereRaw('parentid = '.$affiliate->id.' AND id != '.$affiliate->id)->count()){
			    $url = '<a href="'.route('agentReport', array('sid' => $affiliate->id, 'sfrom' => $datefrom, 'sto' => $dateto)).'">'.urlencode($affiliate->username).'</a>';

                    }
                    else {

			    $url = '<a href="'.route('agentReport', array('sid' => $affiliate->id, 'sfrom' => $datefrom, 'sto' => $dateto, 'agtmemrpt' => 'y')).'">'.urlencode($affiliate->username).'</a>';
			    if($affiliate->id == $id)
			    $url = '<a href="'.route('agentReport', array('sid' => $affiliate->id, 'iaflid' => $affiliate->id, 'sfrom' => $datefrom, 'sto' => $dateto, 'agtmemrpt' => 'y')).'">'.urlencode($affiliate->username).'</a>';
                    } 
                   
                    if($id == $affiliate->id){
						
						 $url = '<a href="'.route('agentReport', array('sid' => $affiliate->id, 'sfrom' => $datefrom, 'sto' => $dateto, 'agtmemrpt' => 'y')).'">'.urlencode($affiliate->username).'</a>';
					}
                    $username = $url;
					if($id == $affiliate->id){
						$username .= ' [MEMBERs]';
					}
                    $rate = 0;
                    if($profitloss <> 0) $rate = abs($cwinloss / $profitloss) * 100;
					
					$turnover 	 	= App::formatAddTabUrl( Lang::get('COMMON.MEMBERENQUIRY'), $turnover, action('Admin\ReportController@showMainReport2', array('aflid' => $affiliate->id, 'createdfrom' => $datefrom, 'createdto' =>  $dateto )) );
					
					$deposit_link   = App::formatAddTabUrl( Lang::get('COMMON.DEPOSIT'), App::displayAmount($deposit), action('Admin\CashledgerController@index', array('aflid' => $affiliate->id, 'createdfrom' => $datefrom, 'createdto' =>  $dateto , 'type' => 1 )) );
					
					$withdraw_link  = App::formatAddTabUrl( Lang::get('COMMON.WITHDRAW'), App::displayAmount($withdrawal), action('Admin\CashledgerController@index', array('aflid' => $affiliate->id, 'createdfrom' => $datefrom, 'createdto' =>  $dateto , 'type' => 2 )) );
					
					$bonus_link     = App::formatAddTabUrl( Lang::get('COMMON.BONUS'), App::formatNegativeAmount($bonus), action('Admin\CashledgerController@index', array('aflid' => $affiliate->id, 'createdfrom' => $datefrom, 'createdto' =>  $dateto , 'type' => 3 )) );
					
					$incentive_link = App::formatAddTabUrl( Lang::get('COMMON.INCENTIVE'), App::formatNegativeAmount($incentive), action('Admin\CashledgerController@index', array('aflid' => $affiliate->id, 'createdfrom' => $datefrom, 'createdto' =>  $dateto , 'type' => 9 )) );
					
					$percentage = Affiliatesetting::where( 'aflid' , '=' , $affiliate->id )->orderBy('id','DESC')->pluck('commission');
                    
                    $row = array(
                        'id' => $affiliate->id,
                        'affiliate' => $username,
                        'deposit' => $deposit_link,
                        'withdrawal' => $withdraw_link,
                        'bonus' => $bonus_link,
                        'incentive' => $incentive_link ,
                        'turnover' => $turnover,
                        'profitloss' => App::formatNegativeAmount($profitloss),
                        'netwinloss' => App::formatNegativeAmount($profitloss*0.85),
                        'comm' => App::displayPercentage($rate),
                        'valid' => (isset($valid))?$valid:0,
                        'stake' => (isset($turnover))?$turnover:0,
                        'currency' => $percentage.'%',
                        'from' => $datefrom,
                        'to' => $dateto,
                        'awinloss' => App::formatNegativeAmount($awinloss),
                        'bwinloss' => App::formatNegativeAmount($bwinloss),
                        'cwinloss' => App::formatNegativeAmount($cwinloss),
                        'apromo' => App::formatNegativeAmount($apromo),
                        'bpromo' => App::formatNegativeAmount($bpromo),
                        'cpromo' => App::formatNegativeAmount($cpromo),
                        'atotal' => App::formatNegativeAmount($awinloss + $apromo),
                        'btotal' => App::formatNegativeAmount($bwinloss + $bpromo),
                        'ctotal' => App::formatNegativeAmount($cwinloss + $cpromo),
                    );
                    
                    if($deposit > 0 || $turnover > 0 || $withdrawal > 0)
			$rows[] = $row;
                }
            }
	    
            $footers[] = array(
                    'affiliate'	=> Lang::get('COMMON.TOTAL'),
                    'deposit' => App::displayAmount($total_deposit),
                    'withdrawal' => App::displayAmount($total_withdrawal),
                    'bonus' => App::formatNegativeAmount($total_bonus),
                    'incentive' => App::formatNegativeAmount($total_incentive),
                    'turnover' => App::displayAmount($total_turnover),
                    'profitloss' => App::formatNegativeAmount($total_profitloss),
                    'netwinloss' => App::formatNegativeAmount($total_netwinloss),
                    'awinloss' => App::formatNegativeAmount($total_awinloss),
                    'bwinloss' => App::formatNegativeAmount($total_bwinloss),
                    'cwinloss' => App::formatNegativeAmount($total_cwinloss),
                    'apromo' => App::formatNegativeAmount($total_apromo),
                    'bpromo' => App::formatNegativeAmount($total_bpromo),
                    'cpromo' => App::formatNegativeAmount($total_cpromo),
                    'atotal' => App::formatNegativeAmount($total_awinloss + $total_apromo),
                    'btotal' => App::formatNegativeAmount($total_bwinloss + $total_bpromo),
                    'ctotal' => App::formatNegativeAmount($total_cwinloss + $total_cpromo),
            );
		
            if($return) return $rows;
            else {
                    echo json_encode(array('total' => count($rows), 'rows' => $rows, 'footer' => $footers));
                    exit;
            }
	
	}
	
	protected function showagentmemberReportdoGetData(Request $request, $return = false){

		$aflt = $request->input('aflt');

		if (!is_array($aflt)) $aflt = array();

		$timefrom = $request->createdfrom.' 00:00:00';
		$timeto   = $request->createdto.' 23:59:59';
		$datefrom = $request->createdfrom;
		$dateto   = $request->createdto;

		$id = (isset($aflt['sid']) ? $aflt['sid'] : 0);

		$condition = 'aflid = '.$id.' AND status = 1';
		$total = Account::whereRaw($condition)->count();
		$rows = array();
		$total_register = 0;
		$total_betmember = 0;
		$total_deposit = 0;
		$total_withdrawal = 0;
		$total_bonus = 0;
		$total_incentive = 0;
		$total_turnover = 0;
		$total_profitloss = 0;
		$total_netwinloss = 0;
		$total_commission = 0;
		$total_rebate = 0;
		$total_fee = 0;
		$total_payout = 0;
		$total_netpnl = 0;
		$total_apromo = 0;
		$total_bpromo = 0;
		$total_cpromo = 0;
		$total_awinloss = 0;
		$total_bwinloss = 0;
		$total_cwinloss = 0;
	    
            if ($members = Account::whereRaw($condition)->skip($request->recordstartindex)->take($request->pagesize)->get()) {
                foreach($members as $member) {
			$rate = 0;
			$deposit = 0;
			$withdrawal = 0;
			$bonus = 0;
			$incentive = 0;
			$turnover = 0;
			$profitloss = 0;
			$commission = 0;
			$rebate = 0;
			$fee = 0;
			$payout = 0;
			$netpnl = 0;
			$apromo = 0;
			$bpromo = 0;
			$cpromo = 0;
			$awinloss = 0;
			$bwinloss = 0;
			$cwinloss = 0;

			$commission = 0;
			$rebate = 0;
			
			$deposit = Cashledger::where('chtcode', '=', CBO_CHARTCODE_DEPOSIT)
				->where('accid', '=', $member->id)
				->where('created', '>=', $timefrom)
				->where('created', '<=', $timeto)
				->whereIn('status', array(CBO_LEDGERSTATUS_CONFIRMED, CBO_LEDGERSTATUS_MANCONFIRMED))
				->sum('amount');
			$withdrawal = Cashledger::where('chtcode', '=', CBO_CHARTCODE_WITHDRAWAL)
				->where('accid', '=', $member->id)
				->where('created', '>=', $timefrom)
				->where('created', '<=', $timeto)
				->whereIn('status', array(CBO_LEDGERSTATUS_CONFIRMED, CBO_LEDGERSTATUS_MANCONFIRMED))
				->sum('amount');
			$bonus = Cashledger::where('chtcode', '=', CBO_CHARTCODE_BONUS)
				->where('accid', '=', $member->id)
				->where('created', '>=', $timefrom)
				->where('created', '<=', $timeto)
				->whereIn('status', array(CBO_LEDGERSTATUS_CONFIRMED, CBO_LEDGERSTATUS_MANCONFIRMED))
				->sum('amount');
			$incentive = Cashledger::where('chtcode', '=', CBO_CHARTCODE_INCENTIVE)
				->where('accid', '=', $member->id)
				->where('created', '>=', $timefrom)
				->where('created', '<=', $timeto)
				->whereIn('status', array(CBO_LEDGERSTATUS_CONFIRMED, CBO_LEDGERSTATUS_MANCONFIRMED))
				->sum('amount');
			if ( Config::get('setting.opcode') == 'GSC' )
			{
				$turnover = Profitloss::where('accid', '=', $member->id)
					->where('date', '>=', $datefrom)
					->where('date', '<=', $dateto)
					->sum('totalstake');
				$valid = Profitloss::where('accid', '=', $member->id)
					->where('date', '>=', $datefrom)
					->where('date', '<=', $dateto)
					->sum('validstake');
				$profitloss = Profitloss::where('accid', '=', $member->id)
					->where('date', '>=', $datefrom)
					->where('date', '<=', $dateto)
					->sum('amount');
			}else{
				$turnover = Profitloss::where('accid', '=', $member->id)
					->where('date', '>=', $datefrom)
					->where('date', '<=', $dateto)
					->where('category', '!=', Product::CATEGORY_LOTTERY)
					->sum('totalstake');
				$valid = Profitloss::where('accid', '=', $member->id)
					->where('date', '>=', $datefrom)
					->where('date', '<=', $dateto)
					->where('category', '!=', Product::CATEGORY_LOTTERY)
					->sum('validstake');
				$profitloss = Profitloss::where('accid', '=', $member->id)
					->where('date', '>=', $datefrom)
					->where('date', '<=', $dateto)
					->where('category', '!=', Product::CATEGORY_LOTTERY)
					->sum('amount');
			}
				
			if( $member->crccode == 'IDR' || $member->crccode == 'VND' )
			{
				$valid      = $valid      * 1000;
				$turnover   = $turnover   * 1000;
				$profitloss = $profitloss * 1000;
			}

			$bwinloss = (float) Wagersetting::where('accid', '=', $member->id)
				->where('share1id', '>=', $member->aflid)
				->where('date', '>=', $datefrom)
				->where('date', '<=', $dateto)
				->sum('share1');
			$bwinloss *= -1;
			$cwinloss = $awinloss - $bwinloss;
			
			$bpromo = (float) Ledgersetting::where('accid', '=', $member->id)
				->where('share1id', '>=', $member->aflid)
				->where('date', '>=', $datefrom)
				->where('date', '<=', $dateto)
				->sum('share1');
			$cpromo = $apromo - $bpromo;
			
			$total_bwinloss += $bwinloss;
			$total_cwinloss += $cwinloss;

			$total_cpromo += $cpromo;
			$total_bpromo += $bpromo;

			$total_deposit += $deposit;
			$total_withdrawal += $withdrawal;
			$total_bonus += $bonus;
			$total_incentive += $incentive;
			$total_turnover += $turnover;
			$total_profitloss += $profitloss;
			$total_netwinloss += ($profitloss * 0.85);
			
			$newParams = array('iaccid' => $member->id);
			$newParams['aqrng']['createdfrom'] = $datefrom;
			$newParams['aqrng']['createdto'] = $dateto;
			
			$rate = 0;
			if ($profitloss != 0) $rate = abs($cwinloss / $profitloss) * 100;
			
			$turnover 	 	= App::formatAddTabUrl( Lang::get('COMMON.MEMBERENQUIRY'), $turnover, action('Admin\ReportController@showMainReport2', array('accid' => $member->id, 'createdfrom' => $datefrom, 'createdto' =>  $dateto )) );
			
			$deposit_link   = App::formatAddTabUrl( Lang::get('COMMON.DEPOSIT'), App::displayAmount($deposit), action('Admin\CashledgerController@index', array('accid' => $member->id, 'createdfrom' => $datefrom, 'createdto' =>  $dateto , 'type' => 1 )) );
			
			$withdraw_link  = App::formatAddTabUrl( Lang::get('COMMON.WITHDRAW'), App::displayAmount($withdrawal), action('Admin\CashledgerController@index', array('accid' => $member->id, 'createdfrom' => $datefrom, 'createdto' =>  $dateto , 'type' => 2 )) );
			
			$bonus_link     = App::formatAddTabUrl( Lang::get('COMMON.BONUS'), App::formatNegativeAmount($bonus), action('Admin\CashledgerController@index', array('accid' => $member->id, 'createdfrom' => $datefrom, 'createdto' =>  $dateto , 'type' => 3 )) );
			
			$incentive_link = App::formatAddTabUrl( Lang::get('COMMON.INCENTIVE'), App::formatNegativeAmount($incentive), action('Admin\CashledgerController@index', array('accid' => $member->id, 'createdfrom' => $datefrom, 'createdto' =>  $dateto , 'type' => 9 )) );
					
			
			$row = array(
				'id' => $member->id,
				'affiliate' => App::formatAddTabUrl(Lang::get('COMMON.BETDETAILS').': '.urlencode($member->nickname), urlencode($member->nickname), route('showdetail', $newParams)),
				'deposit' => $deposit_link,
				'withdrawal' => $withdraw_link,
				'bonus' => $bonus_link,
				'incentive' => $incentive_link,
				'turnover' => $turnover,
				'profitloss' => App::formatNegativeAmount($profitloss),
				'netwinloss' => App::formatNegativeAmount($profitloss * 0.85),
				'valid' => $valid,
				'stake' => $turnover,
				'currency' => $member->crccode,
				'from' => $datefrom,
				'to' => $dateto,
				'comm' => App::displayPercentage($rate),
				'awinloss' => App::formatNegativeAmount($awinloss),
				'bwinloss' => App::formatNegativeAmount($bwinloss),
				'cwinloss' => App::formatNegativeAmount($cwinloss),
				'apromo' => App::formatNegativeAmount($apromo),
				'bpromo' => App::formatNegativeAmount($bpromo),
				'cpromo' => App::formatNegativeAmount($cpromo),
				'atotal' => App::formatNegativeAmount($awinloss + $apromo),
				'btotal' => App::formatNegativeAmount($bwinloss + $bpromo),
				'ctotal' => App::formatNegativeAmount($cwinloss + $cpromo),
			);
			if($deposit > 0 || $turnover > 0 || $withdrawal > 0)
			$rows[] = $row;
		}
	    }

            $footers[] = array(
			'affiliate' => Lang::get('COMMON.TOTAL') ,
			'deposit' => App::displayAmount($total_deposit),
			'withdrawal' => App::displayAmount($total_withdrawal),
			'bonus' => App::formatNegativeAmount($total_bonus),
			'incentive' => App::formatNegativeAmount($total_incentive),
			'turnover' => App::displayAmount($total_turnover),
			'profitloss' => App::formatNegativeAmount($total_profitloss),
			'netwinloss' => App::formatNegativeAmount($total_netwinloss),
			'awinloss' => App::formatNegativeAmount($total_awinloss),
			'bwinloss' => App::formatNegativeAmount($total_bwinloss),
			'cwinloss' => App::formatNegativeAmount($total_cwinloss),
			'apromo' => App::formatNegativeAmount($total_apromo),
			'bpromo' => App::formatNegativeAmount($total_bpromo),
			'cpromo' => App::formatNegativeAmount($total_cpromo),
			'atotal' => App::formatNegativeAmount($total_awinloss + $total_apromo),
			'btotal' => App::formatNegativeAmount($total_bwinloss + $total_bpromo),
			'ctotal' => App::formatNegativeAmount($total_cwinloss + $total_cpromo),
		'from_agtmemrpt'=>'yes',
		);
		
            if($return) return $rows;
            else {
                    echo json_encode(array('total' => count($rows), 'rows' => $rows, 'footer' => $footers));
                    exit;
            }
	
	}
	
	protected function showMemberReport(Request $request){
		
		$grid = new PanelGrid;
		$grid->setupGrid( $this->moduleName, 'affiliateMemberReport-data', $this->limit, true, array( 'footer' => true , 'params' => array( 'iaflid' => $request->iaflid , 'createdfrom' => $request->createdfrom , 'createdto' => $request->createdto)) );
		$grid->setTitle(Lang::get('COMMON.AFFILIATE'));
				
	    $grid->addColumn('member', 				Lang::get('COMMON.MEMBER'), 					90,			array());
        $grid->addColumn('deposit', 			Lang::get('COMMON.DEPOSIT'), 					80,			array('align' => 'right', 'color' => '#FFF8C6'));
        $grid->addColumn('withdrawal', 			Lang::get('COMMON.WITHDRAWAL'), 				80,			array('align' => 'right', 'color' => '#FFF8C6'));
        $grid->addColumn('turnover', 			Lang::get('COMMON.TURNOVER'), 					80,			array('align' => 'right', 'color' => '#F6C7CE'));
        $grid->addColumn('profitloss', 			Lang::get('COMMON.PROFITLOSS'), 				90,			array('align' => 'right', 'color' => '#F6C7CE'));
        $grid->addColumn('bonus', 				Lang::get('COMMON.BONUS'), 						70,			array('align' => 'right', 'color' => '#AFDCEC'));
        $grid->addColumn('incentive', 			Lang::get('COMMON.INCENTIVE'), 					70,			array('align' => 'right', 'color' => '#AFDCEC'));
        $grid->addColumn('netpnl', 				Lang::get('COMMON.NETPROFITLOSS'), 				90,			array('align' => 'right', 'color' => '#AFDCEC'));

		


		$data['grid'] = $grid->getTemplateVars();

		return view('admin.grid2',$data);
		
	}
	
	protected function doGetMemberReportData(Request $request){
		
	    $timefrom = $request->createdfrom.' 00:00:00';
        $timeto	  = $request->createdto.' 23:59:59';
        $datefrom = $request->createdfrom;
        $dateto   = $request->createdto;
		
 
		$condition =  'aflid = '.$request->iaflid;
		$total = Account::whereRaw($condition)->count();
		$rows = array();
		
		if($members = Account::whereRaw($condition)->get()) {
			$total_deposit = 0;
			$total_withdrawal = 0;
			$total_bonus = 0;
			$total_incentive = 0;
			$total_turnover = 0;
			$total_profitloss = 0;
			$total_netpnl = 0;
			
            foreach($members as $member) {

				$rate = 0;
				$deposit = 0;
				$withdrawal = 0;
				$bonus = 0;
				$incentive = 0;
				$turnover = 0;
				$profitloss = 0;
				$payout = 0;
				$netpnl = 0;
				
				$deposit 	= Cashledger::whereRaw('chtcode = '.CBO_CHARTCODE_DEPOSIT.' AND accid = '.$member->id.' AND created >= "'.$timefrom.'" AND created <= "'.$timeto.'" AND status IN ('.CBO_LEDGERSTATUS_CONFIRMED.', '.CBO_LEDGERSTATUS_MANCONFIRMED.')')->sum('amount');
				$withdrawal = Cashledger::whereRaw('chtcode = '.CBO_CHARTCODE_WITHDRAWAL.' AND accid = '.$member->id.' AND created >= "'.$timefrom.'" AND created <= "'.$timeto.'" AND status IN ('.CBO_LEDGERSTATUS_CONFIRMED.', '.CBO_LEDGERSTATUS_MANCONFIRMED.')')->sum('amount');
				$bonus 		= Cashledger::whereRaw('chtcode = '.CBO_CHARTCODE_BONUS.' AND accid = '.$member->id.' AND created >= "'.$timefrom.'" AND created <= "'.$timeto.'" AND status IN ('.CBO_LEDGERSTATUS_CONFIRMED.', '.CBO_LEDGERSTATUS_MANCONFIRMED.')')->sum('amount');
				$incentive  = Cashledger::whereRaw('chtcode = '.CBO_CHARTCODE_INCENTIVE.' AND accid = '.$member->id.' AND created >= "'.$timefrom.'" AND created <= "'.$timeto.'" AND status IN ('.CBO_LEDGERSTATUS_CONFIRMED.', '.CBO_LEDGERSTATUS_MANCONFIRMED.')')->sum('amount');
				$turnover   = Profitloss::whereRaw('accid = '.$member->id.' AND date >= "'.$datefrom.'" AND date <= "'.$dateto.'"')->sum('totalstake');
				$profitloss = Profitloss::whereRaw('accid = '.$member->id.' AND date >= "'.$datefrom.'" AND date <= "'.$dateto.'"')->sum('amount');
				
				$profitloss = ($member->crccode == 'VND' || $member->crccode == 'IDR')? $profitloss * 1000 : $profitloss;
				$turnover   = ($member->crccode == 'VND' || $member->crccode == 'IDR')? $turnover * 1000 : $turnover;
					
				$netpnl 	= $profitloss + ($bonus + $incentive);
				
				
				$total_deposit += $deposit;
				$total_withdrawal += $withdrawal;
				$total_bonus += $bonus;
				$total_incentive += $incentive;
				$total_turnover += $turnover;
				$total_profitloss += $profitloss;
				$total_netpnl += $netpnl;
				
				if($turnover > 0){
					$detailParams['iaccid'] 	 = $member->id;
					$detailParams['createdfrom'] = $request->createdfrom;
					$detailParams['createdto']   = $request->createdto;
					
					$turnover = '<a onclick="window.open(\''.action('Admin\ReportController@showdetail',$detailParams).'\', \'report\', \'scrollbars=1,width=1500,height=830\')" href="#">'.App::formatNegativeAmount($turnover).'</a>';
					
					$row = array(
						'member' 		=> $member->nickname,
						'deposit' 		=> App::displayAmount($deposit),
						'withdrawal' 	=> App::displayAmount($withdrawal),
						'bonus' 		=> App::formatNegativeAmount($bonus),
						'incentive' 	=> App::formatNegativeAmount($incentive),
						'turnover' 		=> ($turnover),
						'profitloss' 	=> App::formatNegativeAmount($profitloss),
						'netpnl' 		=> App::formatNegativeAmount($netpnl),
					);
					$rows[] = $row;
				}
				
            }
        }
		
		$footers[] = array(
			'member'	=> Lang::get('COMMON.TOTAL'),
			'deposit' => App::displayAmount($total_deposit),
			'withdrawal' => App::displayAmount($total_withdrawal),
			'bonus' => App::formatNegativeAmount($total_bonus),
			'incentive' => App::formatNegativeAmount($total_incentive),
			'turnover' => App::displayAmount($total_turnover),
			'profitloss' => App::formatNegativeAmount($total_profitloss),
			'netpnl' => App::formatNegativeAmount($total_netpnl),
		);
		echo json_encode(array('total' => count($rows), 'rows' => $rows, 'footer' => $footers));
		exit;
		
	}
	
	protected function showCommissionScheme(Request $request){
		
			$grid = new PanelGrid;
			$grid->setupGrid($this->moduleName, 'affiliateEnquiry-data', $this->limit);
			$grid->setTitle(Lang::get('COMMON.AFFILIATE'));
					
			$grid->addColumn('name', 			Lang::get('COMMON.NAME'), 					100,		array('align' => 'center'));	
			$grid->addColumn('minpayout', 		Lang::get('COMMON.MINPAYOUT'), 				100,		array('align' => 'center'));	
			$grid->addColumn('accumulate', 		Lang::get('COMMON.ACCUMULATE_WINLOST'), 	100,		array('align' => 'center'));	

			$data['grid'] = $grid->getTemplateVars();

			return view('admin.grid2',$data);
		
	}
	
	protected function showCommissionSchemedoGetData(Request $request){

        /* $queryCondition = $this->getQueryCondition($params);
        $rangeCondition = $this->getRangeCondition($params);
        $condition = $this->mergeCondition($this->getFilterCondition($params), $queryCondition, $rangeCondition);

        $object = new CommissionObject;
        $total = $object->getCount($condition);
		
        if($commissions = $object->getAll($condition, $sortFields, $sortOrder, $start, $limit)){
            foreach($commissions as $commission){
                $options = array('colors' => array(6 => $commission->getStatusColorCode()));

                $name = '<a href="'.$this->formatUrl('editcomm', array('sid' => $commission->id)).'">'.$commission->name.'</a>';
                $rolloverwinloss = App::getText('CBO_GUI_NO');
                $rolloverpayout = App::getText('CBO_GUI_NO');

                if($commission->rolloverwinloss == 1) $rolloverwinloss = App::getText('CBO_GUI_YES');
                if($commission->rolloverpayout == 1) $rolloverpayout = App::getText('CBO_GUI_YES');

                $row = array($commission->id, $name, $commission->getTypeText(), $commission->minpayout, $rolloverwinloss, $rolloverpayout);
                $grid->addRow($row, ((isset($options))? $options:''));
            }
        }

        $grid->setParams($params ,1);

        $grid->setParams($params ,2);

        $newParams = $params;
        if(isset($newParams['sid']))
            unset($newParams['sid']);
        if($this->parentId > 0) {
            $newParams['iprt'] = $object->getParentId($this->parentId);
        }

        $grid->addFilter('istatus', $object->getStatusOptions(), array('display' => App::getText('CBO_GUI_STATUS'), 'value' => ((isset($this->filterValue['istatus']))?$this->filterValue['istatus']:''), 'extra' => 'onchange="submitFilter(this, \''.$this->formatURL('show', $newParams).'\')"'));

        $grid->setNavigationUrl($this->formatURL('', $newParams));

        $buf = $grid->drawGrid();
        //$this->gui->setContent($buf);
        $this->smarty->assign('title', 'ä»£ç†ä½£é‡‘');
        $this->smarty->assign('start', $start);
        $this->smarty->assign('columns', $grid->getColumns());
        $this->smarty->assign('rows', $grid->getRows());
        $this->smarty->assign('filters', $grid->getFilterForm());
        $this->smarty->assign('navigate', $grid->drawNavigation());
        $this->smarty->assign('template', 'standard_grid.tpl'); */
		
	}
	
	protected function showEnquiry(Request $request){
		
		$grid = new PanelGrid;
		if($request->has('aflid'))
		{
			$grid->setupGrid($this->moduleName, 'affiliateEnquiry-data', $this->limit, '', array('params' => array('aflid' => $request->aflid ) ));
		}else{
			$grid->setupGrid($this->moduleName, 'affiliateEnquiry-data', $this->limit);
		}
		$grid->setTitle(Lang::get('COMMON.AFFILIATE'));
		$grid->addSearchField('search', array('username' => Lang::get('COMMON.AFFILIATE')));		
		$grid->addColumn('username',		Lang::get('COMMON.USERNAME'), 			90,			array());
		$grid->addColumn('type',			Lang::get('COMMON.TYPE'), 				90,			array());
        $grid->addColumn('name',			Lang::get('COMMON.NAME'), 				100,		array());
        $grid->addColumn('code',			Lang::get('COMMON.CODE2'), 				65,			array('align' => 'center'));
		$grid->addColumn('comm',			Lang::get('COMMON.POSITIONTAKING'), 	50,			array('align' => 'right'));
		$grid->addColumn('credit_balance',	Lang::get('COMMON.CREDIT'), 			50,			array('align' => 'right'));
		//$grid->addColumn('cash_balance',	Lang::get('COMMON.CASH'), 				50,			array('align' => 'right'));
		if(Config::get('setting.opcode') != 'GSC'){
			$grid->addColumn('deposit',			Lang::get('COMMON.DEPOSIT'), 			70,			array('align' => 'right'));
			$grid->addColumn('withdraw',		Lang::get('COMMON.WITHDRAW'), 			70,			array('align' => 'right'));
		}
		$grid->addColumn('downline',		Lang::get('COMMON.DOWNLINE'), 			65,			array('align' => 'right'));
        $grid->addColumn('member',			Lang::get('COMMON.MEMBER'), 			65,			array('align' => 'right'));
        $grid->addColumn('cashcard',		Lang::get('COMMON.CASHCARD'), 			65,			array('align' => 'center'));
        $grid->addColumn('remark',			Lang::get('COMMON.REMARK'), 			200,		array());
        $grid->addColumn('created',			Lang::get('COMMON.REGISTEREDON'), 		120,		array());
        $grid->addColumn('createdip',		Lang::get('COMMON.IPADDRESS'), 			100,		array());
        $grid->addColumn('location',		Lang::get('COMMON.LOCATION'), 			200,		array());
        $grid->addColumn('status', 			Lang::get('COMMON.STATUS'), 			70,			array('align' => 'center'));
		if(Config::get('setting.opcode') != 'GSC'){
			$grid->addColumn('credit', 			'', 									70,			array('align' => 'center'));
		}
		
		$grid->addButton('1', Lang::get('COMMON.ADDNEW'), Lang::get('COMMON.ADDNEW').' '.Lang::get('COMMON.AFFILIATE'), 'button', array('icon' => 'add', 'url' => action('Admin\AffiliateController@drawAddEdit')));		
		

		$data['grid'] = $grid->getTemplateVars();

		return view('admin.grid2',$data);
		
	}
	
	protected function EnquirydoGetData(Request $request){
		
		//status
		$status[1] = 'Active';
		$status[0] = 'Suspended';
		
		$rows = array();
		
		$aflt  = $request->input('aflt');
		$aqfld = $request->input('aqfld');
		
		$condition = 'crccode = "'.Session::get('admin_crccode').'" ';
		
		if( isset($aqfld['status']) && $aqfld['status'] != 0 ){
			$condition .= ' AND status'.'='.$aqfld['status'];
		}
		
		if( isset($aflt['username']) && $aflt['username'] != '' ){
			$condition .= ' AND username ="'.$aflt['username'].'" ';
		}
		
		if($request->has('aflid'))
		{
			$condition .= ' AND parentid ="'.$request->aflid.'" ';
		}
		
	/* 	
		if( $request->has('createdfrom') ){
			$condition .= ' AND created >= "'.$request->get('createdfrom').' 00:00:00" AND created <= "'.$request->get('createdto').' 23:59:59" ';
		} */
		
        $total = Affiliate::whereRaw($condition)->count();

		$rows = array();
        if($affiliates = Affiliate::whereRaw($condition)->skip($request->recordstartindex)->take( $request->pagesize )->orderBy('id','desc')->get()){
            $name = App::getDateTime(9);
            foreach($affiliates as $affiliate){
				$rate = 0;
				if($afsObj = Affiliatesetting::whereRaw('category = 0 AND aflid = '.$affiliate->id.' ORDER BY effective DESC')->first())
				$rate = $afsObj->commission;
							
	
                $member   = Account::whereRaw('aflid = '.$affiliate->id)->count();
				$downline = Affiliate::whereRaw('parentid = '.$affiliate->id)->count();
				//if($downline > 0) $downline = $this->formatAddTabUrl(App::getText('CBO_GUI_DOWNLINE').': '.$affiliate->username, $downline, $this->formatUrl('show', array('iprt' => $affiliate->id))); 
				
				$type = $affiliate->type == 2 ? 'affiliate' : 'agent' ;
				
				
		        $downlines = $affiliate->getAllDownlineById($affiliate->id , false);
				
				$cashcardCount  = 0;
				$cashcardAmount = 0;
				
				if(is_array($downlines)){
					$downlines[]    = $affiliate->id;
					$cashcardCount  = Cashcard::whereIn('aflid', $downlines)->whereStatus(1)->count();
					$cashcardAmount = Cashcard::whereIn('aflid', $downlines)->whereStatus(1)->sum('amount');
				}else{
					$downlines[]    = $affiliate->id;
				}
				
				
				
				
				
				$rows[] = array(
					'username' 	 	=> App::formatAddTabUrl( Lang::get('AFFILIATE').': '.$affiliate->name, $affiliate->username, action('Admin\AffiliateController@drawAddEdit',  array('sid' 			=> $affiliate->id))),
					'name' 			=> $affiliate->name,
					'code' 			=> $affiliate->code,
					'cashcard' 		=> App::displayNumberFormat($cashcardAmount).' ('.$cashcardCount.')',
					'credit_balance'=> App::displayNumberFormat( Affiliatecredit::getBalance( $affiliate->id ) ),
					'cash_balance'  => App::displayNumberFormat( Affiliatecredit::getBalance( $affiliate->id , 1) ),
					'deposit'  	    => App::formatAddTabUrl('Cash card: '.$affiliate->username, App::displayNumberFormat(Cashcard::whereIn('aflid', $downlines)->whereBetween('created', array($request->createdfrom.' '.$request->timefrom, $request->createdto.' '.$request->timeto))->sum('amount')), action('Admin\CashcardController@index', array('aflid' => $affiliate->id, 'createdfrom' => $request->createdfrom, 'createdto' => $request->createdto))),
					'withdraw'  	=> App::formatAddTabUrl('Withdraw card: '.$affiliate->username, App::displayNumberFormat(Withdrawcard::whereIn('aflid', $downlines)->whereBetween('created', array($request->createdfrom.' '.$request->timefrom, $request->createdto.' '.$request->timeto))->sum('amount')), action('Admin\WithdrawcardController@index', array('aflid' => $affiliate->id, 'createdfrom' => $request->createdfrom, 'createdto' => $request->createdto))),
					'comm' 			=> App::displayPercentage($rate),
					'downline'		=> ($downline > 0 ? App::formatAddTabUrl( Lang::get('COMMON.MEMBERENQUIRY'), $downline, action('Admin\AffiliateController@showEnquiry', array('aflid' => $affiliate->id, 'createdfrom' => '2014-01-01', 'type' =>  $affiliate->type)) ) : $downline),
					'member'		=> ($member > 0 ? App::formatAddTabUrl( Lang::get('COMMON.MEMBERENQUIRY'), $member, action('Admin\MemberEnquiryController@index', array('aflid' => $affiliate->id, 'createdfrom' => '2014-01-01')) ) : $member),
					'telephone' 	=> $affiliate->telephone,
					'email' 		=> $affiliate->email,
					'remark' 		=> $affiliate->remark,
					'created' 		=> $affiliate->created->toDateTimeString(),
					'createdip' 	=> $affiliate->createdip,
					'location'  	=> App::getCityNameByGeoIp($affiliate->createdip),
					'status' 		=> $affiliate->getStatusText(),
					'type' 	    	=> $type,
					'credit' 		=> '<a href="#"  onclick="parent.addTab(\'Top up credit\', \''.action('Admin\AffiliateController@drawTopupCredit',  array('sid' => $affiliate->id , 'type' => 'add' )).'\')"><img src="admin/images/plus.png"></img></a><a href="#"  onclick="parent.addTab(\'Withdraw credit\', \''.action('Admin\AffiliateController@drawTopupCredit',  array('sid' => $affiliate->id , 'type' => 'minus' )).'\')"><img src="admin/images/minus.png"></img></a>',
				);
            }
        }
		
		echo json_encode(array('total' => $total, 'rows' => $rows));
		exit;
		
		
	}
	
	protected function drawTopupCredit(Request $request){
		
		$toAdd 	  = true;
		$object   = null;

		$title = 'Top up credit';
		
		if($request->has('sid')) 
		{
	
			if ($object = Affiliate::find($request->input('sid'))) 
				{
					$title    = 'Top Up Credit';
					$toAdd    = false;
				} 
			else
				{
					$object = null;
				}
			
		} 
	
		$form = App::setupPanelForm('doadd', $request->all(), array(), $toAdd);
		$form->setFormTitle($title);
		
		$credit = Affiliatecredit::getBalance($object->id);
    
		$form->addInput('showonly', Lang::get('COMMON.USERNAME') 		, 'username', 	 (($object)? $object->username:'') , 	array(), true);
		$form->addInput('showonly', Lang::get('COMMON.CURRENCY') 		, 'currency', 	 (($object)? $object->crccode:'')  , 	array(), true);
		$form->addInput('showonly', Lang::get('COMMON.BALANCE') 		, 'balance',     App::displayNumberFormat($credit) ,	array(), true);
		$form->addInput('select',	Lang::get('COMMON.TYPE')	        , 'balance_type', '', array('options' => array( 0 => 'Credit' , 1 => 'Cash' ), 'disabled' => false), true);
		$form->addInput('text', 	Lang::get('COMMON.AMOUNT') 			, 'amount',      ''								   ,	array(), true);
		$form->addInput('hidden',  ''  									, 'aflid', 		 $object->id					   , 	array(), '');
		$form->addInput('hidden',  ''  									, 'username', 	 $object->username				   , 	array(), '');
		$form->addInput('hidden',  ''  									, 'crccode', 	 $object->crccode				   , 	array(), '');
		$form->addInput('hidden',  ''  									, 'type', 		 $request->input('type')		   , 	array(), '');


		$data['form']   = $form->getTemplateVars();
		$data['module'] = 'affiliate-topupcredit';

		return view('admin.form2',$data);
		
	}	
	
	protected function TopupCreditProcess(Request $request){
		
		$validator = Validator::make(
			[
                'amount'  	 		 => $request->input('amount'),
			],
			[
			    'amount'	  		 => 'required|numeric',
			]
		);

		
		if ($validator->fails())
		{
			echo json_encode($validator->errors());
			exit;
		}
		
		if( $request->input('type') == 'minus' && Affiliatecredit::whereUserid($request->aflid)->sum('amount') < $request->amount )
		{
			return json_encode( array( 'amount' => 'Not enough credit to withdraw' ) );
		}
		
		$affiliatecredit = new Affiliatecredit;
		$affiliatecredit->userid 		= $request->aflid; 
		$affiliatecredit->username  	= $request->username; 
		$affiliatecredit->type  		= $request->balance_type; 
		$affiliatecredit->crccode   	= $request->crccode; 
		$affiliatecredit->amount    	= $request->input('type') == 'add' ? $request->amount : $request->amount * -1; 
		$affiliatecredit->date    		= date('Y-m-d'); 
		$affiliatecredit->createdby    	= Session::get('admin_userid'); 
			
		if($affiliatecredit->save())
		{
			return json_encode( array( 'code' => Lang::get('COMMON.SUCESSFUL') ) );
		}
		
	}
	
	protected function drawAddEdit(Request $request){
		$toAdd 	  = true;
		$object   = null;
		$readOnly = false;

		$title = Lang::get('COMMON.NEW').' '.Lang::get('COMMON.AFFILIATE');
	
		 if($request->has('sid')) 
		{
	
			if ($object = Affiliate::find($request->input('sid'))) 
				{
					$title    = Lang::get('COMMON.EDIT').' '.Lang::get('COMMON.AFFILIATE').' '.Lang::get('COMMON.REGISTER');
					$toAdd    = false;
					$readOnly = true;

				} 
			else
				{
					$object = null;
				}
			
		} 
		
		$form = App::setupPanelForm('doadd', $request->all(), array(), $toAdd);
		$form->setFormTitle($title);
                $rate = 0;
                if($object) {
                    if($afsObj = Affiliatesetting::whereRaw('category = 0 AND aflid ='.$object->id)->orderBy('effective','DESC')->first()) {
                        $rate = $afsObj->commission;
                    }
                }
                $commissions = Affiliatesetting::getCommissionOptions();
		
		$form->addInput('text', 	Lang::get('COMMON.USERNAME') 		    , 'username', 		 	 (($object)? $object->username:''), 			array(), true);
		$form->addInput('password', Lang::get('COMMON.PASSWORD') 		    , 'password', 		 	 (($object)? $object->password:''), 			array(), true);
		$form->addInput('password', Lang::get('COMMON.PASSWORD') 		    , 'retype_password',     (($object)? $object->password:''), 			array(), true);
        $form->addInput('radio', 	Lang::get('COMMON.TYPE') 			    , 'type', 			 	 (($object)? $object->type:''), 				array( 'options' => Affiliate::getTypeAsOptions() ), true);
        $form->addInput('radio', 	Lang::get('COMMON.WITHDRAWAL') 			, 'withdrawal', 	 	 (($object)? $object->withdrawal:''), 			array( 'options' => Affiliate::getStatusOptions2() ), true);
	
		if(Config::get('setting.opcode') == 'GSC'){
			$form->addInput('selectchina1', 	Lang::get('COMMON.STATE')  		, 'province', 		 	 (($object)? $object->province:''), 			array(), true);
			$form->addInput('selectchina2', 	Lang::get('COMMON.CITY') 		, 'city', 		 		 (($object)? $object->city:''), 				array(), true);
			$form->addInput('selectchina3', 	Lang::get('COMMON.DISTRICT') 	, 'district', 		 	 (($object)? $object->district:''), 			array(), true);
		}
		if( Config::get('setting.opcode') == 'GSC' ){
			$form->addInput('select',   Lang::get('COMMON.COMMISSION').'(每个月1号才能够修改)'          , 'commission',            $rate,               		                array('options' => $commissions), true);
		}else{
			$form->addInput('select',   Lang::get('COMMON.COMMISSION')          , 'commission',            $rate,               		                array('options' => $commissions), true);
		}
        $form->addInput('text', 	Lang::get('COMMON.NAME') 			    , 'name', 		 		 (($object)? $object->name:''), 				array(), true);
		$form->addInput('text', 	Lang::get('COMMON.EMAIL') 			    , 'email', 		 	 	 (($object)? $object->email:''), 				array(), true);
		$form->addInput('text', 	Lang::get('COMMON.TELEPHONENO') 	    , 'telephone', 		 	 (($object)? $object->telephone:''), 			array(), true);
		$form->addInput('text', 	Lang::get('COMMON.WEBSITE') 		    , 'website', 		 	 (($object)? $object->website:''), 				array(), true);
		$form->addInput('textarea', Lang::get('COMMON.WEBSITEDESC') 		, 'websitedesc', 		 (($object)? $object->websitedesc:''), 			array(), true);
		$form->addInput('textarea', Lang::get('COMMON.REMARK') 				, 'remark', 			 (($object)? $object->remark:''), 				array(), true);
		if( Config::get('setting.opcode') == 'LVG' ){
			$form->addInput('psb_game', Lang::get('COMMON.GAME') 		, 'psb_game', 		 (($object)? $object->psb_game:''), 			array(), true);
			$form->addInput('psb_type', Lang::get('COMMON.TYPE') 		, 'psb_type', 		 (($object)? $object->psb_type:''), 			array(), true);
			$form->addInput('psb_bet', 	Lang::get('COMMON.BET') 		, 'psb_bet', 		 (($object)? $object->psb_bet:''), 			array(), true);
		}
		$form->addInput('radio', 	Lang::get('COMMON.STATUS') 			    , 'status', 		 	 (($object)? $object->status:''), 				array( 'options' => Affiliate::getStatusOptions() ), true);
        $form->addInput('checkbox_single', Lang::get('COMMON.VIEWWINLOSSREPORT')	, 'viewwinlossreport', 	(($object)? $object->view_winloss_report:''), 			array(), false);
        $form->addInput('checkbox_single', Lang::get('COMMON.ADDUSERFUNCTION')		, 'add_user', 			(($object)? $object->add_user:''), 						array(), false);
		
        $data['form'] = $form->getTemplateVars();
        $data['form']['province'] = true;
		$data['module'] = 'affiliateEnquiry';

		return view('admin.form2',$data);
		
	}
	
	protected function showRegister(Request $request){
		
		$grid = new PanelGrid;
		$grid->setupGrid($this->moduleName, 'affiliateRegister-data', $this->limit);//
		$grid->setTitle(Lang::get('COMMON.AFFILIATE'));
				
		$grid->addColumn('username', 			Lang::get('COMMON.USERNAME'), 					100,		array('align' => 'center'));	
		$grid->addColumn('name', 				Lang::get('COMMON.NAME'), 						150,		array('align' => 'center'));	
		$grid->addColumn('telephone', 			Lang::get('COMMON.TELEPHONENO'), 				100,		array('align' => 'center'));	
		$grid->addColumn('email', 				Lang::get('COMMON.EMAIL'), 						150,		array('align' => 'center'));	
		$grid->addColumn('website', 			Lang::get('COMMON.WEBSITE'), 					100,		array('align' => 'center'));	
		$grid->addColumn('desc', 				Lang::get('COMMON.DESC'), 						100,		array('align' => 'center'));	
		$grid->addColumn('status', 				Lang::get('COMMON.STATUS'), 					100,		array('align' => 'center'));	
		$grid->addColumn('created', 			Lang::get('COMMON.REGISTEREDON'), 				150,		array('align' => 'center'));	
		$grid->addColumn('createdip', 			Lang::get('COMMON.IPADDRESS'), 					100,		array('align' => 'center'));	
		$grid->addColumn('location', 			Lang::get('COMMON.LOCATION'), 					200,		array('align' => 'center'));

		$grid->addFilter('status', Affiliateregister::getStatusOptions(), array('display' => Lang::get('COMMON.STATUS')));

		$data['grid'] = $grid->getTemplateVars();

		return view('admin.grid2',$data);
		
	}
	
	protected function showRegisterdoGetData(Request $request){
		
        //status
		$status[1] = 'Active';
		$status[0] = 'Suspended';
		
		$rows = array();
		
		$aflt  = $request->input('aflt');
		$aqfld = $request->input('aqfld');
		
		$condition = '1';
		
		if( isset($aqfld['status']) ){
			$condition .= ' AND status'.'='.$aqfld['status'];
		}

		$total = Affiliateregister::whereRaw($condition)->count();

		$rows = array();
        if($affiliates = Affiliateregister::whereRaw($condition)->skip($request->recordstartindex)->take( $request->pagesize )->orderBy('id','desc')->get()){
            $name = App::getDateTime(9);
            foreach($affiliates as $affiliate){
				$username = App::formatAddTabUrl( Lang::get('AFFILIATE').': '.$affiliate->username, urlencode($affiliate->username), action('Admin\AffiliateController@RegisterdrawAddEdit',  array('sid' => $affiliate->id)));
				$rows[] = array(
					'username' 	=> urldecode($username),
					'name' 		=> $affiliate->name,
					'website' 	=> $affiliate->website,
					'desc'	    => $affiliate->websitedesc,
					'telephone' => $affiliate->telephone,
					'email' 	=> $affiliate->email,
					'status' 	=> '<span style="color: '.$affiliate->getStatusColorCode().';">'.$affiliate->getStatusText().'</span>',
					'created' 	=> $affiliate->created->toDateTimeString(),
					'createdip' => $affiliate->createdip,
					'location'  => App::getCityNameByGeoIp($affiliate->createdip),
				);
            }
        }
		
		echo json_encode(array('total' => $total, 'rows' => $rows));
		exit;
		
	}
	
	protected function RegisterdrawAddEdit(Request $request){
		
		$toAdd 	  = true;
		$object   = null;
		$readOnly = false;

		$title = Lang::get('COMMON.NEW').' '.Lang::get('COMMON.AFFILIATE');
	
		 if($request->has('sid')) 
		{
	
			if ($object = Affiliateregister::find($request->input('sid'))) 
				{
					$title    = Lang::get('COMMON.EDIT').' '.Lang::get('COMMON.AFFILIATE').' '.Lang::get('COMMON.REGISTER');
					$toAdd    = false;
					$readOnly = true;

				} 
			else
				{
					$object = null;
				}
			
		} 
		
		$form = App::setupPanelForm('doadd', $request->all(), array(), $toAdd);
		$form->setFormTitle($title);
		
		$form->addInput('text', 	Lang::get('COMMON.USERNAME') 		    , 'username', 		 	 (($object)? $object->username:''), 			array(), true);
		$form->addInput('text', 	Lang::get('COMMON.NAME') 			    , 'name', 		 		 (($object)? $object->name:''), 				array(), true);
		$form->addInput('text', 	Lang::get('COMMON.EMAIL') 			    , 'email', 		 	 	 (($object)? $object->email:''), 				array(), true);
		$form->addInput('text', 	Lang::get('COMMON.TELEPHONENO') 	    , 'telephone', 		 	 (($object)? $object->telephone:''), 			array(), true);
		$form->addInput('text', 	Lang::get('COMMON.WEBSITE') 		    , 'website', 		 	 (($object)? $object->website:''), 				array(), true);
		$form->addInput('textarea', Lang::get('COMMON.WEBSITEDESC') 		, 'websitedesc', 		 (($object)? $object->websitedesc:''), 			array(), true);
		$form->addInput('textarea', Lang::get('COMMON.REMARK') 		        , 'remark', 		 	 (($object)? $object->remark:''), 				array(), true);
        $form->addInput('radio', 	Lang::get('COMMON.STATUS') 			    , 'status', 		 	 (($object)? $object->status:''), 				array( 'options' => Affiliateregister::getStatusOptions() ), true);

		$data['form'] = $form->getTemplateVars();
		$data['module'] = 'affiliateRegister';

		return view('admin.form2',$data);
		
	}
	
	protected function doAddEdit(Request $request){

                $toAdd = true;
                $object = null;
                $updatePassword = false;
                $rules = array(
                    'name'	   	   		 => 'required',
                    'type'	   	   		 => 'required',
                );

                if($request->has('sid')) {
                    //$object = new Affiliate;
                    if ($object = Affiliate::find($request->input('sid'))) {
                        $toAdd = false;
                    } else {
                        $object = null;
                        $updatePassword = true;
                        $rules['password'] = 'required';
                        $rules['retype_password'] = 'required';
                    }
                }

                if (strlen($request->input('password')) > 0 || strlen($request->input('retype_password')) > 0) {
                    $updatePassword = true;
                    $rules['password'] = 'required';
                    $rules['retype_password'] = 'required';
                }

		$validator = Validator::make(
			[
                'password'  	 	 => $request->input('password'),
				'retype_password'	 => $request->input('retype_password'),
				'name'   		 	 => $request->input('name'),
                'type'   		 	 => $request->input('type'),
			],
            $rules
		);

		
		if ($validator->fails())
		{
			echo json_encode($validator->errors());
			exit;
		}
		
		if( !$request->has('sid') ) 
		{
			$validator = Validator::make(
				[
				   'username'	 => $request->input('username'),
				],
				[
				   'username'	 => 'required|alpha_num|unique:affiliate,username',
				]
			);
			if ($validator->fails()){
				echo json_encode($validator->errors());
				exit;
			}
		}
		if ( Config::get('setting.opcode') == 'GSC' ) {
			if (!(preg_match("/^\p{Han}+$/u", $request->input('name')))){
				echo json_encode(array('name' => Lang::get('public.PleaseFillInChineseName')));
				exit;
			}
		}
		
		if ($updatePassword) {
            if( $request->input('password') != $request->input('retype_password') ){
                echo json_encode( array('password' => Lang::get('COMMON.PASSWORDNOTMATCH')));
                exit;
            }
        }
			
        if($object) {
            $ids = array();
            $min = 0;
			$aff = new Affiliate;
            if($downlines = $aff->getAllDownlineById(2)) {
                    foreach($downlines as $downline) {
                            $ids[] = $downline;
                            if($afsObj = Affiliatesetting::whereRaw('category = 0 AND aflid ='.$downline)->orderBy('effective','DESC')->first()){
                                    if($afsObj->commission > $min) $min = $afsObj->commission;
                            }
                    }
            }

            if(count($ids) > 0) {
                    $max = 51;

                    if($afsObj = Affiliatesetting::whereRaw('aflid ='.$object->parentid)->orderBy('effective','DESC')->first()) {
                            $max = $afsObj->commission;
                    }

                    if($request->input('commission') <= $min || $request->input('commission') >= $max) {
                            $min += 1;
                         
                    }
            }
        }  

		$province_select = $request->province_select;
		$province_select_input = null;

		if( !empty($province_select) && !empty($province_select_input = $request->input($province_select)) ){
		    $aff = Affiliate::where('status', '=', CBO_AFFILIATESTATUS_ACTIVE)->where($province_select, '=', $province_select_input);

		    if ($request->has('sid')) {
                $aff->where('id', '!=', $request->input('sid'));
            }

			if( $aff->count() >= 1 ){
				echo json_encode( array($province_select => $province_select_input.'不能重复' ));
				exit;
			}
		}
                $success = false;
            if ($toAdd){
				
				
				
                $object = new Affiliate;
				$object->level = 1;
				$object->comid = 1;
				$object->username = $request->input('username');
				$object->crccode = Session::get('admin_crccode');
				$object->type = $request->input('type');
				$object->name = $request->input('name');
				$object->password = Hash::make( $request->input('password') );
				if ( Config::get('setting.opcode') == 'GSC' ) {
				    do {
                        $object->code = App::generateNum(6);
                    } while (Affiliate::where('code', '=', $object->code)->exists());
                } else {
                    $object->code = App::generateKey(6);
                }
				$object->email = $request->input('email');
				$object->telephone = $request->input('telephone');
				$object->website = $request->input('website');
				$object->websitedesc = $request->input('websitedesc');
				$object->status = $request->input('status');
				$object->withdrawal = $request->input('withdrawal');
				$object->remark = $request->input('remark');
				$object->createdip = App::getRemoteIp();
				$object->add_user = $request->input('add_user', 0);

                if( !empty($province_select) && !empty($province_select_input) ) {
                    $object->{$province_select} = $province_select_input;
                }

                if($object->save()) {
                    if (Config::get('setting.opcode') == 'GSC'){
                        App::addMultiSubdomain(Config::get('setting.affiliate_domains'),$object->username,Configs::getParam("SYSTEM_CLOUDFLARE_IP"));
                    } else {
                        App::addSubdomain(Config::get('setting.domain'),$object->username,Configs::getParam("SYSTEM_CLOUDFLARE_IP"));
                    }
                    $success = true;
                    if($masters = Agent::WhereRaw('level = '.Agent::LEVEL_MASTERAGENT.' AND isexternal = 1')->get()) {
                            foreach($masters as $master) {
                                    $agentObj = new Agent;
                                    $agentObj->parentid = $master->id;
                                    $agentObj->level = Agent::LEVEL_AGENT;
                                    $agentObj->code = $master->code.'_'.$object->code;
                                    $agentObj->wbsid = $master->wbsid;
                                    $agentObj->bhdid = $master->bhdid;
                                    $agentObj->crccode = $master->crccode;
                                    $agentObj->aflid = $object->id;
                                    $agentObj->isexternal = $master->isexternal;
                                    $agentObj->istest = $master->istest;
                                    $agentObj->save();
                            }
                    }
					
					
			}
                }else{
 			if($request->has('sid')) {
 			    if ($updatePassword) {
                    $object->password = Hash::make($request->input('password'));
                    $object->lastpwdchange = App::getDateTime();
                }
				$object->type = $request->input('type');
			}
			$object->name = $request->input('name');
			$object->withdrawal = $request->input('withdrawal');
			$object->remark = $request->input('remark');
			$object->email = $request->input('email');
			$object->telephone = $request->input('telephone');
			$object->website = $request->input('website');
			$object->websitedesc = $request->input('websitedesc');
			$object->status = $request->input('status');
			$object->view_winloss_report = $request->input('viewwinlossreport', 0);
			$object->add_user = $request->input('add_user');

            if( !empty($province_select) && !empty($province_select_input) ) {
                $object->province = '';
                $object->city = '';
                $object->district = '';
                $object->{$province_select} = $province_select_input;
            }
			
			if($object->save()) {
				
				$success = true;
			}                   
                }
                
                if($success == true) {
						$can_edit = true;
                        $commission = 0;
                        if($request->input('commission') !== '')
                            $commission = $request->input('commission');
						
						if( Config::get('setting.opcode') == 'GSC'  && date("j") != 1 && $toAdd == false){
							$can_edit = false;
						}
						
						$psbObj = new PSB;
						if( Config::get('setting.opcode') == 'LVG'){
								$psbObj->createAgent(
								Session::get('currency'),
								$request->input('name')
							);
						}
						
						
						if($request->has('sid')){
							$psbObj2 = new PSB;
								if(Config::get('setting.opcode') == 'LVG'){
									$psbObj2->setAgentFighting(
										Session::get('currency'),
										$request->input('name'),
										$request->input('psb_game'),
										$request->input('psb_type'),
										$request->input('psb_bet')
									);
								}
						}
						
						if($can_edit)
						{
							$afsObj = new Affiliatesetting;
							$afsObj->aflid = $object->id;
							$afsObj->commission = $commission;
							$afsObj->effective = App::getDateTime();
							$afsObj->save();
						}

                        echo json_encode( array( 'code' => Lang::get('COMMON.SUCESSFUL') ) );
						exit;
                } else {
                        echo json_encode( array( 'code' => Lang::get('COMMON.FAILED') ) );
                }
                
	}	
	
	protected function RegisterDoAddEdit(Request $request){
		
		
		$object = new Affiliateregister;
		
		if($request->has('sid')) 
		{
			$object = Affiliateregister::find($request->get('sid'));
		}
		
		$object->username 		= $request->input('username');
		$object->name 			= $request->input('name');
		$object->email 			= $request->input('email');
		$object->telephone 		= $request->input('telephone');
		$object->website 		= $request->input('website');
		$object->websitedesc 	= $request->input('websitedesc');
		$object->remark 		= $request->input('remark');
		$object->status 		= $request->input('status');
		
		if( $object->save() )
		{
			if( $object->status == 1 && Affiliate::where( 'username' , '=' , $object->username )->count() == 0 )
			{
				$object = new Affiliate;
				$object->level   	 = 1;
				$object->comid 	 	 = 1;
				$object->username	 = $request->input('username');
				$object->type 	 	 = 2;
				$object->name 	 	 = $request->input('name');
				$object->password	 = Hash::make( 'qwer1234' );
                if ( Config::get('setting.opcode') == 'GSC' ) {
                    do {
                        $object->code = App::generateNum(6);
                    } while (Affiliate::where('code', '=', $object->code)->exists());
                } else {
                    $object->code = App::generateKey(6);
                }
				$object->crccode 	 = Session::get('admin_crccode');
				$object->email 	 	 = $request->input('email');
				$object->telephone   = $request->input('telephone');
				$object->website 	 = $request->input('website');
				$object->websitedesc = $request->input('websitedesc');
				$object->status      = $request->input('status');
				$object->createdip   = App::getRemoteIp();
				if($object->status=='1'){
					App::addSubdomain(Config::get('setting.domain'),$object->username,Configs::getParam("SYSTEM_CLOUDFLARE_IP"));
				}
                $object->save();
				
			
             
                    if($masters = Agent::WhereRaw('level = '.Agent::LEVEL_MASTERAGENT.' AND isexternal = 1')->get()) {
                            foreach($masters as $master) {
                                    $agentObj = new Agent;
                                    $agentObj->parentid = $master->id;
                                    $agentObj->level = Agent::LEVEL_AGENT;
                                    $agentObj->code = $master->code.'_'.$object->code;
                                    $agentObj->wbsid = $master->wbsid;
                                    $agentObj->bhdid = $master->bhdid;
                                    $agentObj->crccode = $master->crccode;
                                    $agentObj->aflid = $object->id;
                                    $agentObj->isexternal = $master->isexternal;
                                    $agentObj->istest = $master->istest;
                                    $agentObj->save();
                            }
                    }
				
			}
			echo json_encode( array( 'code' => Lang::get('COMMON.SUCESSFUL') ) );
			exit;
		}


		echo json_encode( array( 'code' => Lang::get('COMMON.FAILED') ) );
	}
	
	protected function showAdvertisement(Request $request){
		
		
		$grid = new PanelGrid;
		$grid->setupGrid($this->moduleName, 'affiliateAdvertisement-data', $this->limit);
		$grid->setTitle(Lang::get('COMMON.AFFILIATE'));
				
		$grid->addColumn('id', 				Lang::get('COMMON.ID'), 						5,			array());	
		$grid->addColumn('name', 			Lang::get('COMMON.NAME'), 						100,		array('align' => 'center'));	
		$grid->addColumn('code', 			Lang::get('COMMON.CODE'), 						100,		array('align' => 'center'));	
		$grid->addColumn('banner', 								'å¹¿å‘Š', 						100,		array('align' => 'center'));	
		$grid->addColumn('function', 		Lang::get('COMMON.STATUS'), 					100,		array('align' => 'center'));	

		$data['grid'] = $grid->getTemplateVars();

		return view('admin.grid2',$data);
		
	}
	
	protected function showAdvertisementdoGetData(Request $request){
		
	}
	


}
