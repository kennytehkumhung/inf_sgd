<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller\Admin;
use App\libraries\grid\PanelGrid;
use App\libraries\App;
use Illuminate\Http\Request;
use Auth;
use Validator;
use Session;
use Lang;

class FailtransController extends Controller{


	protected $moduleName 	= 'failtrans';
	protected $limit 		= 20;
	protected $start 		= 0;
	
	public function __construct()
	{
		
	}
	
	protected function index(Request $request){

			$grid = new PanelGrid;
			$grid->setupGrid('failtrans', 'failtrans-data', 20, true, array(), 'subDTTool');
					
		
			$grid->addColumn('accid', 		    'accid', 										110,        array('align' => 'left'));
			$grid->addColumn('username', 		Lang::get('COMMON.USERNAME'), 			110,        array('align' => 'left'));
			$grid->addColumn('date', 			Lang::get('COMMON.DATE'), 				110,        array('align' => 'left'));
			$grid->addColumn('from', 			Lang::get('COMMON.FROM'), 				80,			array('align' => 'center'));
			$grid->addColumn('to',              Lang::get('COMMON.TO'), 				80,			array('align' => 'center'));
			$grid->addColumn('amount', 			Lang::get('COMMON.AMOUNT'), 			70,			array('align' => 'right'));
			$grid->addColumn('status', 			Lang::get('COMMON.STATUS'), 			80,			array('align' => 'center'));
			$grid->addColumn('add_Credit', 		'', 											80,			array());
			$grid->addColumn('cancel_Credit', 	'', 											80,			array());
			$grid->addColumn('log', 			'', 											80,			array());
	
			$grid->addRangeField('created', array('type' => 'date', 'display' => Lang::get('COMMON.DATE'), 'time' => true));
			
			$data['grid'] = $grid->getTemplateVars();

			return view('admin.grid2',$data);
    }
	
	protected function doGetData(Request $request){
            
			$condition = '1';
			
			$total = Failtrans::whereRaw($condition)->count();
			
			$balances = array();
			if($fails = Failtrans::whereRaw($condition)->take( $request->input('limit') )->skip( $request->recordstartindex )->orderBy('id','desc')->get()){
				
                    $prodObj  = new ProductObject;
                    $prodObj2 = new ProductObject;
                    $bltObj   = new BalanceTransferObject;
					
                    foreach ($fails as $fail){
						
							
							$prodObj2 = Product::find($fail->prdid);
							$bltObj   = Balancetransfer::find($fail->bltid);
							
							if( $bltObj->from == 0 ){
								$from = 'Main';
							}else{
								$prodObj->get($bltObj->from);
								$from = $prodObj->code;
							}
							
							if( $bltObj->accid != $fail->accid ){
								$from = 'NONE';
							}
							
							if( $fail->status == 1 || $fail->status ==2 ){
								$button = '';
							}else{
								$button = '<a href="#" onClick="addcredit('.$fail->id.')" >Add credit</a>';
								$button2 = '<a href="#" onClick="doCancel('.$fail->id.')" >Cancel</a>';
							}
							
							//$log = json_decode($fail->log);
							
							if( $fail->status == 1 ){
								$status = 'Success';
							}elseif( $fail->status == 2 ){
								$status = 'Cancel';
							}else{
								$status = 'Pending';
							}
							
							$accountname = explode( "_" , $fail->acccode);
							
							$balances[] = array(
								'accid'       	 =>   $fail->accid,
								'username'    	 =>   $accountname[1],
								'date'        	 =>   $fail->created,
								'from'        	 =>   $from,
								'to'          	 =>   $prodObj2->code,
								'amount'      	 =>   App::displayAmount($fail->amount),
								'status'      	 =>   $status,
								'add_Credit'  	 =>   $button,
								'cancel_Credit'  =>   $button2,
								//'log'  		  	 =>   $log->DoDepositResult->provider_name . ':' .$log->DoDepositResult->errorMsgDesc . '('.$log->DoDepositResult->errorCode.')',
							);
						
                    }
                }
                echo json_encode(array('rows' =>$balances, 'total' => $total));
                exit;
    }
	
/* 	protected function doAddCredit($params){
		
		$failObj = new FailedTransferObject;
		if( $failObj->getBy('$id = '.$params['iid'].' and $status = 1 ') ){
			exit;
		}

		if($failObj->get($params['iid']))
		{
			$bltObj   = new BalanceTransferObject;
			$bltObj->get($failObj->bltid);
			
			if( $bltObj->from != 0 ){
				$prdid   = $bltObj->from;
				$transid = $failObj->pwlid;
				$status = ProductApi::doTransfer(ProductWalletObject::TYPE_CREDIT, $failObj->accid, $prdid, $transid, $failObj->amount );
			}else{
				$clgobj = new CashLedgerObject;
				$clgobj->getBy('$refobj = "BalanceTransferObject" AND $refid = '. $failObj->bltid.' ');
				$clgobj->status = 2;
				$status = $clgobj->update();
			}

			
			if($status){
				$failObj->status = 1;
				$failObj->update();
			}
		}
		
		echo $status;

	}	
	
	protected function doCancel($params){
		
		$failObj = new FailedTransferObject;
		
		if($failObj->get($params['iid']))
		{
			
			$failObj->status = 2;
			$status = $failObj->update();
	
		}
		
		echo $status;

	} */
}

?>
