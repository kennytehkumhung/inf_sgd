<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller\Admin;
use App\libraries\grid\PanelGrid;
use App\libraries\AccessControl;
use App\libraries\App;
use Illuminate\Http\Request;
use Auth;
use Validator;
use Session;
use Lang;
use App\Models\Currency;
use App\Models\Role;
use App\Models\Bankaccount;
use App\Models\Permission;




class AdminroleController extends Controller{

	protected $moduleName 	= 'adminrole';
	protected $limit 		= 20;
	protected $start 		= 0;
	
	public function __construct()
	{
					
	}
	
	public function show(Request $request){

			$grid = new PanelGrid;
			$grid->setupGrid($this->moduleName, 'adminrole-data', $this->limit);
			$grid->setTitle(Lang::get('COMMON.ROLE'));
									

			$grid->addColumn('name', 	Lang::get('COMMON.NAME'), 					200,	array('color' => '#FFFFCC'));
			$grid->addColumn('desc', 	Lang::get('COMMON.DESC'), 					200,	array());
			$grid->addColumn('status', 	Lang::get('COMMON.STATUS'), 				108,	array('align' => 'center'));
			
			$grid->addFilter('status',array('Suspended','Active'), array('display' => Lang::get('COMMON.TYPE')));
			$grid->addButton('1', 'New', Lang::get('COMMON.ADDNEW').' '.Lang::get('COMMON.ROLE'), 'button', array('icon' => 'add', 'url' => action('Admin\AdminroleController@drawAddEdit')));
			
			$data['grid'] = $grid->getTemplateVars();
			return view('admin.grid2',$data);
    }
	
	protected function doGetData(Request $request){
		$aflt  = $request->input('aflt');
		$aqfld = $request->input('aqfld');
		
		$condition = '1';
		
		if( isset($aqfld['status']) && $aqfld['status'] != '' ){
			$condition .= ' AND status'.'='.$aqfld['status'];
		}

		$total = Role::whereRaw($condition)->count();

		$rows = array();
		if($roles = Role::whereRaw($condition)->get()){
			foreach($roles as $role){
				$rows[] = array(
					'name' 		=>  urldecode(App::formatAddTabUrl(Lang::get('ROLE').': '.$role->id, urlencode($role->name), action('Admin\AdminroleController@drawAddEdit',  array('sid' => $role->id)))),
					'desc'		=> $role->desc,
					'status'	=> $role->getStatusText($this->moduleName),
				);
			}
		}

		echo json_encode(array('total' => $total, 'rows' => $rows));
		exit;
	
	}
	
	protected function drawAddEdit(Request $request) { 

		
		$accesscontrol = new AccessControl;
		$data['modules'] = $accesscontrol->modules;
		
		if($request->has('sid')){
		
			$permissionsObjs = Permission::whereRaw( 'rolid = '.$request->input('sid').' ')->orderBy('catid','ASC')->orderBy('mdlid','ASC')->orderBy('taskid','ASC')->get();
			
			$permissions = array();
			
			foreach( $permissionsObjs as $permissionsObj ){
				$permissions[$permissionsObj->catid][$permissionsObj->mdlid][$permissionsObj->taskid] = true;
			}
			
			$data['permissions'] = $permissions;
			
			$data['roleObj'] = Role::find($request->input('sid'));
		}

		return view('admin.role',$data);
	
	}
	
	protected function doAddEdit(Request $request) {

		$hasMapped = array();
		$permissions = $request->apermission;

		if($request->has('sid'))
		{
			if($objects = Permission::whereRaw('rolid = '.$request->sid)->get()) 
			{
				foreach($objects as $object) 
				{
					if(isset($permissions[$object->catid][$object->mdlid][$object->taskid]) && $permissions[$object->catid][$object->mdlid][$object->taskid] == '1')
						$hasMapped[$object->catid][$object->mdlid][$object->taskid] = true;
					else{
						$object->delete();
					}
				}
			}
			$roleId = $request->sid;
			
			$roleObj = Role::find($request->sid);
			$roleObj->desc		= $request->desc;
			$roleObj->status	= $request->status;
			$roleObj->save();
		}
		else
		{
			$roleObj = new Role;
			$roleObj->name		= $request->name;
			$roleObj->desc		= $request->desc;
			$roleObj->status	= $request->status;
			$roleObj->save();
			$roleId = $roleObj->id;
		}

		if(count($permissions) > 0) {
			foreach($permissions as $catId => $modules) {
				foreach($modules as $moduleKey => $tasks) {
					foreach($tasks as $taskKey => $val) {
			
						if(!isset($hasMapped[$catId][$moduleKey][$taskKey]) &&  isset($permissions[$catId][$moduleKey][$taskKey]) && $permissions[$catId][$moduleKey][$taskKey] == '1'){
							$permissionObj = new Permission;
							$permissionObj->rolid = $roleId;
							$permissionObj->catid = $catId;
							$permissionObj->mdlid = $moduleKey;
							$permissionObj->taskid = $taskKey;
							$permissionObj->save();
						}
					}
				}
			}
			//return true;
		}
		
		echo json_encode( array( 'code' => Lang::get('COMMON.SUCESSFUL') ) );
		exit;
		
	}
	
	protected function doActivateSuspend(Request $request) {

			

		if($request->has('id')) {
		
			if($object = Role::find($request->id)) {
			
				$currentStatus = $object->status;
				if($request->action == 'activate') {
					$object->status 	= CBO_ACCOUNTSTATUS_ACTIVE;
				}
				if($request->action == 'suspend') {
					$object->status 	= CBO_ACCOUNTSTATUS_SUSPENDED;
				}

				if($currentStatus <> $object->status){
					if($object->save()) {
					
						echo json_encode( array( 'code' => Lang::get('COMMON.SUCESSFUL') ) );
						exit;
					}
				}
			}
		}
		
		
		echo json_encode( array( 'code' => Lang::get('COMMON.FAILED') ) );

	}
	



}

?>
