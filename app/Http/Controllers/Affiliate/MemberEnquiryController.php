<?php namespace App\Http\Controllers\Affiliate;

use App\Http\Controllers\Controller\Affiliate;
use App\libraries\grid\PanelGrid;
use App\libraries\App;
use App\Models\Channel;
use App\Models\Badlogin;
use App\Models\Currency;
use App\Models\Account;
use App\Models\Bankaccount;
use App\Models\Accountdetail;
use App\Models\Agent;
use App\Models\Wager;
use App\Models\Cashledger;
use App\Models\Website;
use App\Models\Article;
use Illuminate\Http\Request;
use Auth;
use Validator;
use Session;
use Lang;
use GeoIP;
use Crypt;
use Hash;
use Mail;
use Carbon\Carbon;


class MemberEnquiryController extends Controller{

	
	protected $moduleName 	= 'member';
	protected $limit 		= 20;
	protected $start 		= 0;
	static protected $affiliateInstances = array();
	
	public function __construct()
	{
		
	}

	public function index(Request $request)
	{

		$grid = new PanelGrid;
		$grid->setupGrid($this->moduleName, 'member-data', $this->limit, true, array('params' => array( 'createdfrom' => $request->createdfrom , 'aflid ' => $request->aflid )) );
		$grid->setTitle(Lang::get('COMMON.ACCOUNTENQUIRY'));

		$grid->addColumn('code',			Lang::get('COMMON.MEMBERACCOUNT'), 	130,	array('sort' => true));
		//$grid->addColumn('fullname',		Lang::get('COMMON.MEMBERNAME'), 	130,	array('sort' => true));
		$grid->addColumn('crccode',			Lang::get('COMMON.CURRENCY'), 		130,	array('align' => 'center'));
		$grid->addColumn('deposit',			Lang::get('COMMON.DEPOSIT'), 		130,	array('align' => 'right'));
		$grid->addColumn('withdrawal',		Lang::get('COMMON.WITHDRAWAL'), 	130,	array('align' => 'right'));
		$grid->addColumn('created',			Lang::get('COMMON.REGISTEREDON'), 	130,	array('align' => 'center'));
		$grid->addColumn('status',			Lang::get('COMMON.STATUS'), 		130,	array('align' => 'center'));

		$grid->addFilter('status',array(-1 => Lang::get('COMMON.ALL'),1 => Lang::get('COMMON.ACTIVE') ,0 => Lang::get('COMMON.SUSPENDED') ), array('display' => Lang::get('COMMON.TYPE')));
		
		if(Session::get('affiliate_permission_add_user'))
		{
			$grid->addButton('1', Lang::get('COMMON.NEW'), Lang::get('COMMON.ADDNEW').' '.Lang::get('COMMON.MEMBER'), 'button', array('icon' => 'add', 'url' => action('Affiliate\MemberEnquiryController@drawAddEdit')));
		}

		
		$data['grid'] = $grid->getTemplateVars();
		return view('affiliate.grid2',$data);

	}
	
	protected function doGetData(Request $request) {

		//status
		$status[1] = 'Active';
		$status[0] = 'Suspended';
		
		$aflt  = $request->input('aflt');
		$aqfld = $request->input('aqfld');

		if(!empty($request->aflid)){
			$condition ='istestacc=0 AND aflid = '.$request->aflid;
		}else{
			if(isset($aqfld['status']) && ($aqfld['status'] != -1)){
				$condition =  'istestacc=0 AND aflid = '.Session::get('affiliate_userid').' AND status ='.$aqfld['status'];
			}else{
				$condition =  'istestacc=0 AND aflid = '.Session::get('affiliate_userid');
			}
		}
		
		$total = Account::whereRaw($condition)->count();		
		$rows = array();

		if($accounts = Account::whereRaw($condition)->get()){

			foreach($accounts as $account){
				
					$code = '<a href="#" onclick="parent.addTab(\'Member: '.$account->nickname.'\', \'customer-tab?sid='.$account->id.'\')" style="cursor:pointer">'.$account->nickname.'</a>';
	
					
					$rows[] = array(
					
					'code'				=> $code, 
					'fullname'			=> $account->fullname, 
					'crccode'			=> $account->crccode, 
					'deposit'			=> $account->totaldepositamt, 
					'withdrawal'		=> $account->totalwithdrawalamt, 
					'created'			=> $account->created->toDateTimeString(),
					'status'  			=> $account->getStatusText2($this->moduleName),

					);
				
			}
		}
		echo json_encode(array('total' => $total, 'rows' => $rows));
		exit;

	}
	
	protected function drawAddEdit(Request $request) { 

	    $toAdd 	  = true;
		$object   = null;
		$readOnly = false;
		$values   = array();

		$title = Lang::get('NEW').' '.Lang::get('MEMBERACCOUNT');

		if($request->has('sid')) 
		{
	
			if ($object = Account::find($request->input('sid'))) 
				{
					$title    = Lang::get('COMMON.EDIT').' '.Lang::get('COMMON.MEMBERACCOUNT');
					$toAdd    = false;
					$readOnly = true;

				} 
			else
				{
					$object = null;
				}
			
		}

		$form = App::setupPanelForm('member-do-add-edit', $request->all(), array(), $toAdd);
		$form->setFormTitle($title);
		
		$currencies['MYR'] = 'MYR';
		$currencies['VND'] = 'VND';

		$form->addInput('text', 				Lang::get('COMMON.USERNAME') 		, 'username', 	 	(($object)? $object->username:''), 	array(), false); 
		$form->addInput('password', 			Lang::get('COMMON.PASSWORD') 		, 'password', 	 	(($object)? $object->password:''), 	array(), false); 
		$form->addInput('text', 				Lang::get('COMMON.MEMBERNAME') 		, 'fullname', 	 	(($object)? $object->fullname:''), 	array(), false); 
		//$form->addInput('select', 				Lang::get('COMMON.CURRENCY')		, 'crccode',        (($object)? $object->crccode:''),   array('options' => $currencies), 	 true);
		$form->addInput('text', 				Lang::get('COMMON.EMAIL') 			, 'email', 	 	    (($object)? $object->email:''), 	array(), false); 
		$form->addInput('text', 				Lang::get('COMMON.TELEPHONENO') 	, 'mobile', 	 	(($object)? $object->username:''), 	array(), false); 

		
		$data['form'] = $form->getTemplateVars();
		$data['module'] = 'member';

		return view('admin.form2',$data);
	
	}
	
	protected function doAddEdit(Request $request) {
		
		$validator = Validator::make(
			[
	
				'username' => $request->input('username'),
				'email'	   => $request->input('email'),
				'password' => $request->input('password'),
				'fullname' => $request->input('fullname'),
				'mobile'   => $request->input('mobile'),
			],
			[

				'username' => 'required|min:6|max:16|alpha_num|reserved_str|unique:account,nickname',
				'email'    => 'required|email|unique:accountdetail,email',
				'password' => 'required|alpha_num|min:6',
				'fullname' => 'required|min:2',
				'mobile'   => 'required|phone|unique:accountdetail,telmobile',
			]
		);
		
		if ($validator->fails())
		{
			echo json_encode($validator->errors());
			exit;
		}
		
		
		if( $agtObj = Agent::whereRaw('aflid = '.Session::get('affiliate_userid').' AND crccode = "'.Session::get('affiliate_crccode').'"')->first() ) {
			$wbsid = 0;
			$prefix = '';
			if($wbsObj = Website::find($agtObj->wbsid)) {
				$wbsid  = $wbsObj->id;
				$prefix = $wbsObj->memberprefix;
			}
			
			$object = new Account;
			$acdObj = new Accountdetail;
			
			$object->code = $prefix.'_'.strtolower($request->input('username'));
			$object->nickname = strtoupper($request->input('username'));
			$object->fullname = $request->input('fullname');
			$object->regchannel = 2;
			$object->aflid = Session::get('affiliate_userid');
			$object->agtid = $agtObj->id;
			$object->wbsid = $wbsid;
			$object->islogin = 1;
			$object->password 	   = Hash::make( $request->input('password') );
			$object->enpassword    = Crypt::encrypt( $request->input('password') );
			$object->crccode = Session::get('affiliate_crccode');
			$object->term = CBO_ACCOUNTTERM_CASH;
			$object->timezone = 8;
			$object->withdrawlimit = 1;
			$object->status = CBO_ACCOUNTSTATUS_ACTIVE;
			$acdObj->fullname = $request->input('fullname');
			$acdObj->email = $request->input('email');
			$acdObj->telmobile = $request->input('mobile');
			if($object->save()) {
				$acdObj->id 		= $object->id;
				$acdObj->accid 		= $object->id;
				$acdObj->wbsid 		= $object->wbsid;
				$acdObj->acccode 	= $object->code;
				$acdObj->verifyno          = App::generatePassword();
				$acdObj->referenceno   = 1000 + $object->id;
				if($acdObj->save()){
					
					echo json_encode( array( 'code' => Lang::get('COMMON.SUCESSFUL') ) );
					
					 if( $atcObj = Article::where( 'code' , '=' , 'WELCOME_EN' )->first() ) {
                        $content = str_replace('&nbsp;', ' ', html_entity_decode($atcObj->content));
                        $content = str_replace('%username%', $request->input('username') , $content);
                        $content = str_replace('%verify%', $acdObj->verifyno, $content);

                        $status = Mail::raw(html_entity_decode($content), function ($message) use ($atcObj,$acdObj) 
                        {
                                $message->to($acdObj->email);
                                $message->subject($atcObj->title);
                        }); 

                        if($status){
                                
                        }
						}
				
			
					exit;
				}
				
			}
		}
		
	}
			protected function doActivateSuspend(Request $request) {

			

		if($request->has('id')) {
		
			if($object = Account::find($request->id)) {
			
				$currentStatus = $object->status;
				if($request->action == 'activate') {
					
					$object->status 	= CBO_ACCOUNTSTATUS_ACTIVE;

					// Reset badlogin counter.
                    $today = Carbon::now()->toDateString();
                    $badloginObj = Badlogin::where('userid', '=', $object->id)
                        ->whereBetween('created', array($today . ' 00:00:00', $today . ' 23:59:59'))
                        ->where('type', '=', CBO_LOGINTYPE_USER)
                        ->first();

                    if ($badloginObj) {
                        $badloginObj->counter = 0;
                        $badloginObj->save();
                    }
				}
				if($request->action == 'suspend') {
					$object->status 	= CBO_ACCOUNTSTATUS_SUSPENDED;
				}
				
						


				if($currentStatus <> $object->status){
					
					if($object->save()) {
						//ProductApi::doUpdateStatus($object->id, $object->status);
						echo json_encode( array( 'code' => Lang::get('COMMON.SUCESSFUL') ) );
						exit;
					}
				}
			}
		}
		
		
		echo json_encode( array( 'code' => Lang::get('COMMON.FAILED') ) );
		
	}
}
