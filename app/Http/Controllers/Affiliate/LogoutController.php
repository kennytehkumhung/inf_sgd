<?php namespace App\Http\Controllers\Affiliate;

use App\Http\Controllers\Controller\Affiliate;
use Auth;
use Session;
use Illuminate\Http\Request;
use App\Models\Onlinetracker;
use Redirect;

class LogoutController extends Controller{
	
	public function __construct()
	{
		
	}

	public function index(Request $request)
	{
		Auth::affiliate()->logout();
		return redirect::route('affiliate_login');
	}	
	

	
}
