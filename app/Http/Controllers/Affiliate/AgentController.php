<?php namespace App\Http\Controllers\Affiliate;


use App\libraries\grid\PanelGrid;
use App\libraries\App;
use App\libraries\providers\PSB;
use App\Models\Channel;
use App\Models\Currency;
use App\Models\Account;
use App\Models\Bankaccount;
use App\Models\Accountdetail;
use App\Models\Agent;
use App\Models\Wager;
use App\Models\Cashledger;
use App\Models\Affiliate;
use App\Models\Ledgersetting;
use App\Models\Affiliatesetting;
use App\Models\Wagersetting;
use App\Models\Configs;
use App\Models\Affiliatecredit;
use App\Models\Cashcard;
use App\Models\Profitloss;
use App\Models\Product;
use Illuminate\Http\Request;
use Auth;
use Validator;
use Session;
use Lang;
use GeoIP;
use Hash;
use Config;


class AgentController extends Controller{

	
	protected $moduleName 	= 'agent';
	protected $limit 		= 20;
	protected $start 		= 0;
	static protected $affiliateInstances = array();
	
	public function __construct()
	{
		
	}

	public function index(Request $request)
	{

		$grid = new PanelGrid;
		
		$grid->setupGrid($this->moduleName, 'agent-data', $this->limit, '', array('params' => array('aflid' => $request->input('aflid') ) ));
		$grid->setTitle(Lang::get('COMMON.AGENTLIST'));

		$grid->addColumn('username',		Lang::get('COMMON.USERNAME'), 			90,			array());
        //$grid->addColumn('name',			Lang::get('COMMON.NAME'), 				100,		array());
        $grid->addColumn('code',			Lang::get('COMMON.CODE'), 				65,			array('align' => 'center'));
		$grid->addColumn('comm',			Lang::get('COMMON.POSITIONTAKING'), 	70,			array('align' => 'right'));
		$grid->addColumn('credit_balance',	Lang::get('COMMON.CREDIT'), 			100,		array('align' => 'right'));
		$grid->addColumn('downline',		Lang::get('COMMON.DOWNLINE'), 			65,			array('align' => 'right'));
        $grid->addColumn('member',			Lang::get('COMMON.MEMBER'), 			65,			array('align' => 'right'));
		if(Config::get('setting.opcode') != 'GSC'){
			$grid->addColumn('cashcard',		Lang::get('COMMON.CASHCARD'), 			100,		array('align' => 'center'));
		}
        $grid->addColumn('remark',			Lang::get('COMMON.REMARK'), 			200,		array());
        $grid->addColumn('created',			Lang::get('COMMON.REGISTEREDON'), 		150,		array());
        $grid->addColumn('createdip',		Lang::get('COMMON.IPADDRESS'), 			100,		array());
        $grid->addColumn('location',		Lang::get('COMMON.LOCATION'), 			200,		array());
        $grid->addColumn('status', 			Lang::get('COMMON.STATUS'), 			70,			array('align' => 'center'));
		if(Config::get('setting.opcode') != 'GSC'){
			if( Session::get('affiliate_crccode') != 'TWD' ){
				$grid->addColumn('credit', 			'', 									70,			array('align' => 'center'));
			}
		}
		$grid->addFilter('status',array(Lang::get('COMMON.ACTIVE') ,Lang::get('COMMON.SUSPENDED') ), array('display' => Lang::get('COMMON.TYPE')));
		
		if( Config::get('setting.front_path') == 'front'  )
		{
			if( Session::get('affiliate_level') != 3 )
			{
				$grid->addButton('1', Lang::get('COMMON.NEW'), Lang::get('COMMON.ADDNEW').' '.Lang::get('COMMON.AGENT'), 'button', array('icon' => 'add', 'url' => action('Affiliate\AgentController@drawAddEdit')));
			}
		}
		else
		{
			$grid->addButton('1', Lang::get('COMMON.NEW'), Lang::get('COMMON.ADDNEW').' '.Lang::get('COMMON.AGENT'), 'button', array('icon' => 'add', 'url' => action('Affiliate\AgentController@drawAddEdit')));
		}

		$data['grid'] = $grid->getTemplateVars();
		return view('affiliate.grid2',$data);

	}
	
	protected function doGetData(Request $request) {

		//status
		$status[1] = 'Active';
		$status[0] = 'Suspended';
		
		$aflt  = $request->input('aflt');
		$aqfld = $request->input('aqfld');
		
		if(isset($aqfld['status']) && $aqfld['status'] ==0){
			$condition =  'type = 1 AND status =1';
		}else if(isset($aqfld['status']) && $aqfld['status'] ==1){
			$condition =  'type = 1 AND status =0';
		}else{
			$condition =  'type = 1 ';
		}
		
		if($request->has('aflid'))
		{
			$condition .= ' AND parentid ="'.$request->aflid.'" ';
		}else{
			$condition .= ' AND parentid ="'.Session::get('affiliate_userid').'" ';
		}
		
		$total = Affiliate::whereRaw($condition)->count();		
		$rows = array();

		 if($affiliates = Affiliate::whereRaw($condition)->get() ){

            foreach($affiliates as $affiliate){
				$rate = 0;
				if($afsObj = Affiliatesetting::whereRaw('category = 0 AND aflid = '.$affiliate->id.' ORDER BY effective DESC')->first())
				$rate = $afsObj->commission;
			
			    $downlines = $affiliate::getAllDownlineById($affiliate->id , false);
				
				$cashcardCount  = 0;
				$cashcardAmount = 0;
				
				if(is_array($downlines)){
					$downlines[]    = $affiliate->id;
					$cashcardCount  = Cashcard::whereIn('aflid', $downlines)->whereStatus(1)->count();
					$cashcardAmount = Cashcard::whereIn('aflid', $downlines)->whereStatus(1)->sum('amount');
				}
							
	
                $member   = Account::whereRaw('aflid = '.$affiliate->id)->count();
				
				if( $affiliate->crccode == 'CNY'){
					$member_row = $member > 0 ? App::formatAddTabUrl( Lang::get('COMMON.MEMBERENQUIRY'), $member, action('Admin\MemberEnquiryController@index', array('aflid' => $affiliate->id, 'createdfrom' => '2014-01-01')) ) : $member;
				}else{
					$member_row = $member > 0 ? App::formatAddTabUrl( Lang::get('COMMON.MEMBERENQUIRY'), $member, action('Affiliate\MemberEnquiryController@index', array('aflid' => $affiliate->id, 'createdfrom' => '2014-01-01')) ) : $member;
				}
				
				
				$downline = Affiliate::whereRaw('parentid = '.$affiliate->id)->count();
				
				$rows[] = array(
					'username'  	=> '<a href="#"  onclick="parent.addTab(\'Agent:'.$affiliate->username.'\', \''.action('Affiliate\AgentController@drawAddEdit',  array('sid' => $affiliate->id)).'\')">'.$affiliate->username.'</a>',
					'name' 			=> $affiliate->name,
					'code' 			=> $affiliate->code,
					'cashcard' 		=> App::displayNumberFormat($cashcardAmount).' ('.$cashcardCount.')',
					'comm' 			=> App::displayPercentage($rate),
					'downline'  	=> ($downline > 0 ? App::formatAddTabUrl( Lang::get('COMMON.AGENTLIST').' - '.$affiliate->name, $downline, action('Affiliate\AgentController@index', array('aflid' => $affiliate->id, 'createdfrom' => '2014-01-01', 'type' =>  $affiliate->type)) ) : $downline),
					'credit_balance'=> App::displayNumberFormat(Affiliatecredit::getBalance($affiliate->id)),
					'member'		=> $member_row,
					'telephone' 	=> $affiliate->telephone,
					'email' 		=> $affiliate->email,
					'remark' 		=> $affiliate->remark,
					'created' 		=> $affiliate->created->toDateTimeString(),
					'createdip' 	=> $affiliate->createdip,
					'location'  	=> App::getCityNameByGeoIp($affiliate->createdip),
					'status' 		=> $affiliate->getStatusText(),
					'credit' 		=> '<a href="#"  onclick="parent.addTab(\'Top up credit\', \''.action('Affiliate\AgentController@drawTopupCredit',  array('sid' => $affiliate->id , 'type' => 'add' )).'\')"><img src="affiliate/images/plus.png"></img></a><a href="#"  onclick="parent.addTab(\'Withdraw credit\', \''.action('Affiliate\AgentController@drawTopupCredit',  array('sid' => $affiliate->id , 'type' => 'minus' )).'\')"><img src="affiliate/images/minus.png"></img></a>',
				);
            }
        }
		echo json_encode(array('total' => $total, 'rows' => $rows));
		exit;

	}
	
	protected function drawTopupCredit(Request $request){
		
		$toAdd 	  = true;
		$object   = null;

		$title = 'Top up credit';
		
		if($request->has('sid')) 
		{
	
			if ($object = Affiliate::find($request->input('sid'))) 
				{
					$title    = 'Top Up Credit';
					$toAdd    = false;
				} 
			else
				{
					$object = null;
				}
			
		} 
	
		$form = App::setupPanelForm('doadd', $request->all(), array(), $toAdd);
		$form->setFormTitle($title);
		
		$credit = Affiliatecredit::whereUserid($object->id)->sum('amount');
    
		$form->addInput('showonly', Lang::get('COMMON.USERNAME') 		, 'username', 	 (($object)? $object->username:'') , 	array(), true);
		$form->addInput('showonly', Lang::get('COMMON.CURRENCY') 		, 'currency', 	 (($object)? $object->crccode:'')  , 	array(), true);
		$form->addInput('showonly', Lang::get('COMMON.BALANCE') 		, 'balance',     App::displayNumberFormat($credit) ,	array(), true);
		$form->addInput('text', 	Lang::get('COMMON.AMOUNT') 			, 'amount',      ''								   ,	array(), true);
		$form->addInput('hidden',  ''  									, 'aflid', 		 $object->id					   , 	array(), '');
		$form->addInput('hidden',  ''  									, 'username', 	 $object->username				   , 	array(), '');
		$form->addInput('hidden',  ''  									, 'crccode', 	 $object->crccode				   , 	array(), '');
		$form->addInput('hidden',  ''  									, 'type', 		 $request->input('type')		   , 	array(), '');


		$data['form']   = $form->getTemplateVars();
		$data['module'] = 'agent-topupcredit';

		return view('admin.form2',$data);
		
	}	
	
	protected function TopupCreditProcess(Request $request){
		
		$validator = Validator::make(
			[
                'amount'  	 		 => $request->input('amount'),
			],
			[
			    'amount'	  		 => 'required|numeric',
			]
		);

		
		if ($validator->fails())
		{
			echo json_encode($validator->errors());
			exit;
		}
		
		if( $request->input('type') == 'minus' && Affiliatecredit::whereUserid($request->aflid)->sum('amount') < $request->amount )
		{
			return json_encode( array( 'amount' => 'Not enough credit to withdraw' ) );
		}
		
		$affiliatecredit = new Affiliatecredit;
		$affiliatecredit->userid 		= $request->aflid; 
		$affiliatecredit->username  	= $request->username; 
		$affiliatecredit->crccode   	= $request->crccode; 
		$affiliatecredit->amount    	= $request->input('type') == 'add' ? $request->amount : $request->amount * -1; 
		$affiliatecredit->date    		= date('Y-m-d'); 
		$affiliatecredit->createdby    	= Session::get('admin_userid'); 
			
		if($affiliatecredit->save())
		{
			return json_encode( array( 'code' => Lang::get('COMMON.SUCESSFUL') ) );
		}
		
	}
	
	
	protected function drawAddEdit(Request $request){
		$toAdd 	  = true;
		$object   = null;
		$readOnly = false;

		$title = Lang::get('COMMON.NEW').' '.Lang::get('COMMON.AFFILIATE');
	
		 if($request->has('sid')) 
		{
	
			if ($object = Affiliate::find($request->input('sid'))) 
				{
					$title    = Lang::get('COMMON.EDIT').' '.Lang::get('COMMON.AFFILIATE').' '.Lang::get('COMMON.REGISTER');
					$toAdd    = false;
					$readOnly = true;

				} 
			else
				{
					$object = null;
				}
			
		} 
		
		$form = App::setupPanelForm('doadd', $request->all(), array(), $toAdd);
		$form->setFormTitle($title);
		
		
		$rate = 0;
	
        if($object) {
			if($afsObj = Affiliatesetting::whereRaw('category = 0 AND aflid = '.$object->id.' ORDER BY effective DESC')->first())
			$rate = $afsObj->commission;
        }
		$max = false;
		if($afsObj = Affiliatesetting::whereRaw('category = 0 AND aflid = '.((int) Session::get('affiliate_userid')).' ORDER BY effective DESC')->first())
		$max = $afsObj->commission;

        $commissions = Affiliatesetting::getCommissionOptions($max);
		
		$form->addInput('text', 	Lang::get('COMMON.USERNAME') 		    , 'username', 		 	 (($object)? $object->username:''), 			array(), true);
		$form->addInput('password', Lang::get('COMMON.PASSWORD') 		    , 'password', 		 	 (($object)? $object->password:''), 											array(), true);
		$form->addInput('password', Lang::get('COMMON.PASSWORD') 		    , 'retype_password',                (($object)? $object->password:''), 											array(), true);

		if(Config::get('setting.opcode') == 'GSC'){
			$form->addInput('selectchina1', 	Lang::get('COMMON.STATE')  		, 'province', 		 	 (($object)? $object->province:''), 			array(), true);
			$form->addInput('selectchina2', 	Lang::get('COMMON.CITY') 		, 'city', 		 		 (($object)? $object->city:''), 				array(), true);
			$form->addInput('selectchina3', 	Lang::get('COMMON.DISTRICT') 	, 'district', 		 	 (($object)? $object->district:''), 			array(), true);
		}
		
		if( Config::get('setting.opcode') == 'GSC' ){
			$form->addInput('select',   Lang::get('COMMON.COMMISSION').'(每个月1号才能够修改)'          , 'commission',            $rate,               		                array('options' => $commissions), true);
		}else{
			$form->addInput('select',   Lang::get('COMMON.COMMISSION')          , 'commission',            $rate,               		                array('options' => $commissions), true);
		}
	
		
        $form->addInput('text', 	Lang::get('COMMON.NAME') 			    , 'name', 		 		 (($object)? $object->name:''), 				array(), true);
		$form->addInput('text', 	Lang::get('COMMON.EMAIL') 			    , 'email', 		 	 	 (($object)? $object->email:''), 				array(), true);
		$form->addInput('text', 	Lang::get('COMMON.TELEPHONENO') 	    , 'telephone', 		 	 (($object)? $object->telephone:''), 			array(), true);
		$form->addInput('text', 	Lang::get('COMMON.WEBSITE') 		    , 'website', 		 	 (($object)? $object->website:''), 				array(), true);
		$form->addInput('textarea', Lang::get('COMMON.WEBSITEDESC') 		, 'websitedesc', 		 (($object)? $object->websitedesc:''), 			array(), true);
		/*  */
		if(Config::get('setting.opcode') == 'LVG'){
		$form->addInput('psb_game', Lang::get('COMMON.GAME') 		, 'psb_game', 		 (($object)? $object->psb_game:''), 			array(), true);
		$form->addInput('psb_type', Lang::get('COMMON.TYPE') 		, 'psb_type', 		 (($object)? $object->psb_type:''), 			array(), true);
		$form->addInput('psb_bet', 	Lang::get('COMMON.BET') 		, 'psb_bet', 		 (($object)? $object->psb_bet:''), 			array(), true);
		}
		/*  */
		$form->addInput('radio', 	Lang::get('COMMON.STATUS') 			    , 'status', 		 	 (($object)? $object->status:''), 				array( 'options' => Affiliate::getStatusOptions() ), true);

		$data['form'] = $form->getTemplateVars();
		$data['form']['province'] = true;
		$data['module'] = 'agent';

		return view('admin.form2',$data);
		
	}
	
	protected function doAddEdit(Request $request){
            
        $toAdd = true;
        $object = null;
        if($request->has('sid')) {
        
                //$object = new Affiliate;
                if ($object = Affiliate::find($request->input('sid'))) {
                    $toAdd = false;
                } else
                    $object = null;
            
        }		
		$validator = Validator::make(
			[
                'password'  	 	 => $request->input('password'),
				'retype_password'	 => $request->input('retype_password'),
				'name'   		 	 => $request->input('name'),
				'commission'   		 => $request->input('commission'),
				'username'   		 => $request->input('username'),
				/*  */
				/*  */
			],
			[
			    'password'	  		 => 'required',
			    'retype_password'	 => 'required',
			    'name'	   	   		 => 'required',
			    'commission'	   	 => 'required',
			    'username'	   	 	 => 'required',
				/*  */
				/*  */
			]
		);
		
		if( !$request->has('sid') ) 
		{
			$validator = Validator::make(
			[
					'password'  	 	 => $request->input('password'),
					'retype_password'	 => $request->input('retype_password'),
					'name'   		 	 => $request->input('name'),
					'commission'   		 => $request->input('commission'),
					'username'   		 => $request->input('username'),
					/*  */
					/*  */
				],
				[
					'password'	  		 => 'required',
					'retype_password'	 => 'required',
					'name'	   	   		 => 'required',
					'commission'	   	 => 'required',
					'username'	   	 	 => 'required',
					/*  */
					/*  */
				]
			);
		}else{
			$validator = Validator::make(
			[

					'name'   		 	 => $request->input('name'),
					'commission'   		 => $request->input('commission'),
					'username'   		 => $request->input('username'),
				],
				[

					'name'	   	   		 => 'required',
					'commission'	   	 => 'required',
					'username'	   	 	 => 'required',
				]
			);
			
		}

		
		if ($validator->fails())
		{
			echo json_encode($validator->errors());
			exit;
		}
		
		if( !$request->has('sid') ) 
		{
			$validator = Validator::make(
				[
				   'username'	 => $request->input('username'),
				],
				[
				   'username'	 => 'required|alpha_num|unique:affiliate,username',
				]
			);
			if ($validator->fails()){
				echo json_encode($validator->errors());
				exit;
			}
		}
		
		
		
		if( $request->input('password') != $request->input('retype_password') ){
			echo json_encode( array('password' => Lang::get('COMMON.PASSWORDNOTMATCH')));
			exit;
		}
		
		
		
		if($afsObj = Affiliatesetting::whereRaw('aflid ='.Session::get('affiliate_userid'))->orderBy('effective','DESC')->first()) {
                            $max = $afsObj->commission;
        }
		
		if($request->input('commission') >= $max) {
           
             echo json_encode( array( 'commission' => Lang::get('COMMON.FAILED') ) );
			 exit;
        }
			
        if($object) {
            $ids = array();
            $min = 0;
            if($downlines = Affiliate::getAllDownlineById(2)) {
                    foreach($downlines as $downline) {
                            $ids[] = $downline;
                            if($afsObj = Affiliatesetting::whereRaw('category = 0 AND aflid ='.$downline)->orderBy('effective','DESC')->first()){
                                    if($afsObj->commission > $min) $min = $afsObj->commission;
									
								
                            }
                    }
					
            }

            if(count($ids) > 0) {
                    //$max = 51;

                    
/* 
                    if($request->input('commission') < $min || $request->input('commission') >= $max) {
                         
                             echo json_encode( array( 'commission' => 'commsion must between ('.$min.'~'.$max.')' ));
							 exit;
                    } */
            }
        }              
		$province_select = $request->province_select;
        $province_select_input = null;

        if( !empty($province_select) && !empty($province_select_input = $request->input($province_select)) ){
            $aff = Affiliate::where('status', '=', CBO_AFFILIATESTATUS_ACTIVE)->where($province_select, '=', $province_select_input);

            if ($request->has('sid')) {
                $aff->where('id', '!=', $request->input('sid'));
            }

            if( $aff->count() >= 1 ){
                echo json_encode( array($province_select => $province_select_input.'不能重复' ));
                exit;
            }
        }

                $success = false;
            if ($toAdd){
                $object = new Affiliate;
				$object->parentid = Session::get('affiliate_userid');
				$object->level = Session::get('affiliate_level') + 1;
				$object->comid = 1;
				$object->username = $request->input('username');
				$object->type = 1;
				$object->name = $request->input('name');
				$object->password = Hash::make( $request->input('password') );
                if ( Config::get('setting.opcode') == 'GSC' ) {
                    do {
                        $object->code = App::generateNum(6);
                    } while (Affiliate::where('code', '=', $object->code)->exists());
                } else {
                    $object->code = App::generateKey(6);
                }
				$object->crccode = Session::get('affiliate_crccode');
				$object->email = $request->input('email');
				$object->telephone = $request->input('telephone');
				$object->website = $request->input('website');
				$object->websitedesc = $request->input('websitedesc');
				$object->status = $request->input('status');
				$object->createdip = App::getRemoteIp();

                if( !empty($province_select) && !empty($province_select_input) ) {
                    $object->{$province_select} = $province_select_input;
                }
				
                if($object->save()) {
					App::addSubdomain(Config::get('setting.domain'),$object->username,Configs::getParam("SYSTEM_CLOUDFLARE_IP"));
                    $success = true;
                    if($masters = Agent::WhereRaw('level = '.Agent::LEVEL_MASTERAGENT.' AND isexternal = 1')->get()) {
                            foreach($masters as $master) {
                                    $agentObj = new Agent;
                                    $agentObj->parentid = $master->id;
                                    $agentObj->level = Agent::LEVEL_AGENT;
                                    $agentObj->code = $master->code.'_'.$object->code;
                                    $agentObj->wbsid = $master->wbsid;
                                    $agentObj->bhdid = $master->bhdid;
                                    $agentObj->crccode = $master->crccode;
                                    $agentObj->aflid = $object->id;
                                    $agentObj->isexternal = $master->isexternal;
                                    $agentObj->istest = $master->istest;
                                    $agentObj->save();
                            }
                    }
			}
                }else{
 			if($request->has('sid')) {
				$object->password = Hash::make( $request->input('password') );
				$object->lastpwdchange = App::getDateTime();

                if( !empty($province_select) && !empty($province_select_input) ) {
                    $object->province = '';
                    $object->city = '';
                    $object->district = '';
                    $object->{$province_select} = $province_select_input;
                }
			}
			
			$object->name = $request->input('name');
			$object->email = $request->input('email');
			$object->telephone = $request->input('telephone');
			$object->website = $request->input('website');
			$object->websitedesc = $request->input('websitedesc');
			$object->status = $request->input('status');
			if($object->save()) {
				
				
				$success = true;
			}                   
                }
                
				
                if($success == true) {
						$can_edit = true;
                        $commission = 0;
                        if($request->input('commission') !== '')
                            $commission = $request->input('commission');
						
						if( Config::get('setting.opcode') == 'GSC'  && date("j") != 1 && $toAdd == false){
							$can_edit = false;
						}
					
					if( Config::get('setting.opcode') == 'LVG' ){	
							$aflid_7 = Affiliate::where('id','=',Session::get('affiliate_userid'))->where('level','=',7)->get();
							$agent_7 = Affiliate::where('id','=',$aflid_7)->pluck('username');//level6	
							$aflid_6 = Affiliate::where('id','=',Session::get('affiliate_userid'))->where('level','=',6)->get();
							$agent_6 = Affiliate::where('id','=',$aflid_6)->pluck('username');//level5
							$aflid_5 = Affiliate::where('id','=',Session::get('affiliate_userid'))->where('level','=',5)->get();
							$agent_5 = Affiliate::where('id','=',$aflid_5)->pluck('username');//level4
							$aflid_4 = Affiliate::where('id','=',Session::get('affiliate_userid'))->where('level','=',4)->get();
							$agent_4 = Affiliate::where('id','=',$aflid_4)->pluck('username');//level3
							$aflid_3 = Affiliate::where('id','=',Session::get('affiliate_userid'))->where('level','=',3)->get();
							$agent_3 = Affiliate::where('id','=',$aflid_3)->pluck('username');//level2
							$aflid_2 = Affiliate::where('id','=',Session::get('affiliate_userid'))->where('level','=',2)->get();
							$agent_2 = Affiliate::where('id','=',$aflid_2)->pluck('username');//level1
							
							
							// $level_2 = Affiliate::where('id','=',Session::get('affiliate_userid'))->where('level','=',2)->pluck('parentid');
							// $level_3 = Affiliate::where('id','=',Session::get('affiliate_userid'))->where('level','=',3)->pluck('parentid');
							// $level_4 = Affiliate::where('id','=',Session::get('affiliate_userid'))->where('level','=',4)->pluck('parentid');
							// $level_5 = Affiliate::where('id','=',Session::get('affiliate_userid'))->where('level','=',5)->pluck('parentid');
							// $level_6 = Affiliate::where('id','=',Session::get('affiliate_userid'))->where('level','=',6)->pluck('parentid');
							// $level_7 = Affiliate::where('id','=',Session::get('affiliate_userid'))->where('level','=',7)->pluck('parentid');
							
							
						/* if($aff_2 = Affiliate::where('id','=', Session::get('affiliate_userid'))->where('level','=',2)->pluck('parentid')){
							if($aff_2){
								$agent = $agent_2.'_'.$agent_3;
							}
						}elseif($aff_3 = Affiliate::where('id','=', Session::get('affiliate_userid'))->where('level','=',3)->pluck('parentid')){
							if($aff_3){
								$agent = $agent_2.'_'.$agent_3.'_'.$agent_4;
							}
						}elseif($aff_4 = Affiliate::where('id','=', Session::get('affiliate_userid'))->where('level','=',4)->pluck('parentid')){
							if($aff_4){
								$agent = $agent_2.'_'.$agent_3.'_'.$agent_4.'_'.$agent_5;
							}
						}elseif($aff_5 = Affiliate::where('id','=', Session::get('affiliate_userid'))->where('level','=',5)->pluck('parentid')){
							if($aff_5){
								$agent = $agent_2.'_'.$agent_3.'_'.$agent_4.'_'.$agent_5.'_'.$agent_6;
							}
						}elseif($aff_6 = Affiliate::where('id','=', Session::get('affiliate_userid'))->where('level','=',6)->pluck('parentid')){
							if($aff_6){
								$agent = $agent_2.'_'.$agent_3.'_'.$agent_4.'_'.$agent_5.'_'.$agent_6.'_'.$agent_7;
							}
						} *//* elseif($aff_7 = Affiliate::where('id','=', Session::get('affiliate_userid'))->where('level','=',7)){
							if(isset($aff_7)){
								$agent = $agent_1.'_'.$agent_2.'_'.$agent_3.'_'.$agent_4.'_'.$agent_5.'_'.$agent_6.'_'.$agent_7;
							}
						}	 */
						
						if(Affiliate::where('id','=', Session::get('affiliate_userid'))->where('level','=',2)){
							
								$agent = $agent_2.'_'.$agent_3;
							
						}elseif(Affiliate::where('id','=', Session::get('affiliate_userid'))->where('level','=',3)){
							
								$agent = $agent_2.'_'.$agent_3.'_'.$agent_4;
							
						}elseif(Affiliate::where('id','=', Session::get('affiliate_userid'))->where('level','=',4)){
							
								$agent = $agent_2.'_'.$agent_3.'_'.$agent_4.'_'.$agent_5;
							
						}elseif(Affiliate::where('id','=', Session::get('affiliate_userid'))->where('level','=',5)){
							
								$agent = $agent_2.'_'.$agent_3.'_'.$agent_4.'_'.$agent_5.'_'.$agent_6;
							
						}elseif(Affiliate::where('id','=', Session::get('affiliate_userid'))->where('level','=',6)){
							
								$agent = $agent_2.'_'.$agent_3.'_'.$agent_4.'_'.$agent_5.'_'.$agent_6.'_'.$agent_7;
							
						}
						
						/* $find = Affiliate::where('id','=', Session::get('affiliate_userid'))->pluck('level');
						
						if($find == 6){
							$agent = $agent_2.'_'.$agent_3.'_'.$agent_4.'_'.$agent_5.'_'.$agent_6.'_'.$agent_7;
						}elseif($find == 5){
							$agent = $agent_2.'_'.$agent_3.'_'.$agent_4.'_'.$agent_5.'_'.$agent_6;
						}elseif($find == 4){
							$agent = $agent_2.'_'.$agent_3.'_'.$agent_4.'_'.$agent_5;
						}elseif($find == 3){
							$agent = $agent_2.'_'.$agent_3.'_'.$agent_4;
						}elseif($find == 2){
							$agent = $agent_2.'_'.$agent_3;
						} */
						
						/*  if($aff_2 = Affiliate::where('id','=', Session::get('affiliate_userid'))->where('level','=',2)->pluck('parentid')){
							if(isset($aff_2)){
								$agentdownline = $agent_2;
							}
						}elseif($aff_3 = Affiliate::where('id','=', Session::get('affiliate_userid'))->where('level','=',3)->pluck('parentid')){
							if(isset($aff_3)){
								$agentdownline = $agent_3;
							}
						}elseif($aff_4 = Affiliate::where('id','=', Session::get('affiliate_userid'))->where('level','=',4)->pluck('parentid')){
							if(isset($aff_4)){
								$agentdownline = $agent_4;
							}
						}elseif($aff_5 = Affiliate::where('id','=', Session::get('affiliate_userid'))->where('level','=',5)->pluck('parentid')){
							if(isset($aff_5)){
								$agentdownline = $agent_5;
							}
						}elseif($aff_6 = Affiliate::where('id','=', Session::get('affiliate_userid'))->where('level','=',6)->pluck('parentid')){
							if(isset($aff_6)){
								$agentdownline = $agent_6;
							} 
						} */
						
						if(Affiliate::where('id','=', Session::get('affiliate_userid'))->where('level','=',2)){
							
								$agentdownline = $agent_2;
							
						}elseif($aff_3 = Affiliate::where('id','=', Session::get('affiliate_userid'))->where('level','=',3)){
							
								$agentdownline = $agent_3;
							
						}elseif($aff_4 = Affiliate::where('id','=', Session::get('affiliate_userid'))->where('level','=',4)){
							
								$agentdownline = $agent_4;
							
						}elseif($aff_5 = Affiliate::where('id','=', Session::get('affiliate_userid'))->where('level','=',5)){
							
								$agentdownline = $agent_5;
							
						}elseif($aff_6 = Affiliate::where('id','=', Session::get('affiliate_userid'))->where('level','=',6)){
							
								$agentdownline = $agent_6;
							
						}
						
						
						$psbObj = new PSB;
								$psbObj->createAgentDownline(
								Session::get('currency'),
								$request->input('name'),
								$agent,
								$agentdownline
							);
						
						/* $psbObj2 = new PSB;	
							$psbObj2->setAgentFighting(
								Session::get('currency'),
								$request->input('name'),
								$request->input('psb_game'),
								$request->input('psb_type'),
								$request->input('psb_bet')
							); */
						  
						}
						if($can_edit)
						{
							$afsObj = new Affiliatesetting;
							$afsObj->aflid = $object->id;
							$afsObj->commission = $commission;
							$afsObj->effective = App::getDateTime();
							$afsObj->save();	
						}
						
                        echo json_encode( array( 'code' => Lang::get('COMMON.SUCESSFUL') ) );
						exit;
                } else {
                        echo json_encode( array( 'code' => Lang::get('COMMON.FAILED') ) );
                }
                
	}	
	
	
	 public function showagentReport(Request $request){

            $id = $request->has('sid') ? $request->get('sid') : 0;
	    
            $datefrom = ((isset($request['sfrom']))?$request['sfrom']:App::getDateTime(3));
            $dateto = ((isset($request['sto']))?$request['sto']:App::getDateTime(3));
	    
            // AG = Lang::get('COMMON.AGENT');
            $username = 'AG';
            if($id > 0) {
                    $url = '';
                    $aflObj = Affiliate::where( 'id' , '=' , $id )->first();
                    $username = $aflObj->username;
            }
			
			if( $request->has('sfrom') )
			{
				Session::put('agt_report_createdfrom',$request->sfrom);
				Session::put('agt_report_createdto'  ,$request->sto);
			}else{
				Session::put('agt_report_createdfrom',$datefrom);
				Session::put('agt_report_createdto'  ,$dateto);
			}
            
            $grid = new PanelGrid;
			$grid->setupGrid($this->moduleName, 'agentReport-data', $this->limit, true, array( 'username' => $username ,'footer' => true , 'params' => array( 'createdfrom' => Session::get('agt_report_createdfrom') , 'createdto' => Session::get('agt_report_createdto') )) );
            $grid->setTitle(Lang::get('COMMON.AGENTREPORT')." : ".$username);

            $grid->addColumn('affiliate', 		Lang::get('COMMON.AFFILIATE'), 					100,	array('align' => 'center'));			
            $grid->addColumn('currency', 		Lang::get('COMMON.SHAREPERCENTAGE'),            60,		array('align' => 'center'));					
            $grid->addColumn('deposit', 		Lang::get('COMMON.DEPOSIT'), 					90,		array('align' => 'center'));			
            $grid->addColumn('withdrawal', 		Lang::get('COMMON.WITHDRAWAL'), 				90,		array('align' => 'center'));					
            $grid->addColumn('bonus', 			Lang::get('COMMON.BONUS'), 						90,     array('align' => 'center'));			
            $grid->addColumn('incentive', 		Lang::get('COMMON.INCENTIVE'), 					90,		array('align' => 'center'));
            $grid->addColumn('turnover', 		Lang::get('COMMON.TURNOVER'), 					60,		array('align' => 'center'));			
            $grid->addColumn('profitloss', 		Lang::get('COMMON.PROFITLOSS'), 				90,		array('align' => 'center'));
			if(  Config::get('setting.opcode') == 'IFW' )
			$grid->addColumn('netwinloss', 		Lang::get('COMMON.NETTWINLOSS'), 				90,		array('align' => 'right'));
            
	    // DL = Lang::get('COMMON.DOWNLINE')
			$grid->addColumn('awinloss', 		Lang::get('COMMON.WINLOSE'), 			90,		array('align' => 'center', 'cellclass' => 'leftline'));
            $grid->addColumn('apromo',      Lang::get('COMMON.PROMOTION'), 		90,		array('align' => 'center'));
            $grid->addColumn('atotal',      Lang::get('COMMON.TOTAL'), 			90,		array('align' => 'center'));
            
            $grid->addColumn('bwinloss', 	Lang::get('COMMON.WINLOSE'), 		90,		array('align' => 'center', 'cellclass' => 'leftline'));
            $grid->addColumn('bpromo',      Lang::get('COMMON.PROMOTION'), 	90,		array('align' => 'center'));
            $grid->addColumn('btotal',      Lang::get('COMMON.TOTAL'), 		90,		array('align' => 'center'));
            
	    // UL = Lang::get('COMMON.UPLINE')
            $grid->addColumn('cwinloss', 	Lang::get('COMMON.WINLOSE'), 			90,		array('align' => 'center', 'cellclass' => 'leftline'));
            $grid->addColumn('cpromo',      Lang::get('COMMON.PROMOTION'), 		90,		array('align' => 'center'));
            $grid->addColumn('ctotal',      Lang::get('COMMON.TOTAL'), 			90,		array('align' => 'center'));

	    $grid->addSearchField('sid', 		array('sid' => 'sid'), array('value' => $id, 'hidden' => true));
	    $grid->addSearchField('iaflid', 	array('iaflid' => 'iaflid'), array('value' => $request->get('iaflid'), 'hidden' => true));
	    $grid->addSearchField('agtmemrpt',  array('agtmemrpt' => 'agtmemrpt'), array('value' => $request->get('agtmemrpt'), 'hidden' => true));
	    
	    if ($id > 0) $grid->addButton('2', 'back' , '', 'button', array('icon' => 'ok', 'url' => 'window.history.back();'));
            
            $data['grid'] = $grid->getTemplateVars();
            $data['sid'] = $id;
			$data['username'] = $username;
            return view('admin.grid2',$data);
    }

       
        
  protected function showagentReportdoGetData(Request $request, $return = false){

            $aflt   = $request->input('aflt');
	    
	    if (!is_array($aflt)) $aflt = array();
	    
	    if (isset($aflt['agtmemrpt']) && $aflt['agtmemrpt'] == 'y') {
		    return $this->showagentmemberReportdoGetData($request, $return);
	    }

            $timefrom = $request->createdfrom.' 00:00:00';
            $timeto   = $request->createdto.' 23:59:59';
            $datefrom = $request->createdfrom;
            $dateto   = $request->createdto;

			$id = (isset($aflt['sid']) && $aflt['sid'] != 0 ? $aflt['sid'] : Session::get('affiliate_userid'));
		
			$condition = 'type = 1 AND (id = '.$id.' OR parentid = '.$id.') AND crccode = "'.Session::get('affiliate_crccode').'"';
			
            $total = Affiliate::whereRaw($condition)->count();
            $rows = array();
            $total_deposit = 0;
            $total_withdrawal = 0;
            $total_bonus = 0;
            $total_incentive = 0;
            $total_turnover = 0;
            $total_profitloss = 0;
            $total_netwinloss = 0;
            $total_commission = 0;
            $total_rebate = 0;
            $total_fee = 0;
            $total_payout = 0;
            $total_netpnl = 0;
            $total_apromo = 0;
            $total_bpromo = 0;
            $total_cpromo = 0;
            $total_awinloss = 0;
            $total_bwinloss = 0;
            $total_cwinloss = 0;
	    
            if($affiliates = Affiliate::whereRaw($condition)->skip($request->recordstartindex)->take( $request->pagesize )->get()) {
                foreach($affiliates as $affiliate) {
                    $deposit = 0;
                    $withdrawal = 0;
                    $bonus = 0;
                    $incentive = 0;
                    $turnover = 0;
                    $profitloss = 0;
					$netwinloss = 0;
                    $commission = 0;
                    $rebate = 0;
                    $fee = 0;
                    $payout = 0;
                    $netpnl = 0;
                    $apromo = 0;
                    $bpromo = 0;
                    $cpromo = 0;
                    $awinloss = 0;
                    $bwinloss = 0;
                    $cwinloss = 0;
                    $member_ids = array();

                    $whole_downline = true;
                    if($id == $affiliate->id) $whole_downline = false;
                    
                    $downline_ids = array();
                    $aff_ids = array($affiliate->id);
                    $crccode =  Agent::whereRaw('aflid='.$affiliate->id)->pluck('crccode');
                    
                    if($whole_downline)
                    if($downlines = $affiliate->getAllDownlineById2($affiliate->id, $whole_downline)) {
                        foreach($downlines as $downline) {
							if(isset($downline->id))
                                $aff_ids[] = $downline->id;
                        }
                    }
                    
                    if($members = $affiliate->getAllAccountById($affiliate->id, $whole_downline))
                        foreach($members as $member) {
                            $member_ids[] = $member->id;
                    }
          
                    if(count($member_ids) > 0) {
                        $commission = 0;
                        $rebate = 0;
                        $deposit = Cashledger::whereRaw('chtcode = '.CBO_CHARTCODE_DEPOSIT.' AND accid IN ('.implode(',', $member_ids).') AND created >= "'.$timefrom.'" AND created <= "'.$timeto.'" AND status IN ('.CBO_LEDGERSTATUS_CONFIRMED.', '.CBO_LEDGERSTATUS_MANCONFIRMED.')')->sum('amount');                        
                        $withdrawal = Cashledger::whereRaw('chtcode = '.CBO_CHARTCODE_WITHDRAWAL.' AND accid IN ('.implode(',', $member_ids).') AND created >= "'.$timefrom.'" AND created <= "'.$timeto.'" AND status IN ('.CBO_LEDGERSTATUS_CONFIRMED.', '.CBO_LEDGERSTATUS_MANCONFIRMED.')')->sum('amount');
                        $bonus = Cashledger::whereRaw('chtcode = '.CBO_CHARTCODE_BONUS.' AND accid IN ('.implode(',', $member_ids).') AND created >= "'.$timefrom.'" AND created <= "'.$timeto.'" AND status IN ('.CBO_LEDGERSTATUS_CONFIRMED.', '.CBO_LEDGERSTATUS_MANCONFIRMED.')')->sum('amount');
						$incentive = Cashledger::whereRaw('chtcode = '.CBO_CHARTCODE_INCENTIVE.' AND accid IN ('.implode(',', $member_ids).') AND created >= "'.$timefrom.'" AND created <= "'.$timeto.'" AND status IN ('.CBO_LEDGERSTATUS_CONFIRMED.', '.CBO_LEDGERSTATUS_MANCONFIRMED.')')->sum('amount');
						
						
                        if($affiliate->id == $id) {
                            $awinloss = 0;
                            $apromo = 0;
                            $cwinloss = (float) Wagersetting::whereRaw('aflid='.$affiliate->id.' AND share2id='.$affiliate->parentid.' AND date >= "'.$datefrom.'" AND date <= "'.$dateto.'"')->sum('share1');
                            $cpromo = (float) Ledgersetting::whereRaw('aflid='.$affiliate->id.' AND share2id='.$affiliate->parentid.' AND date >= "'.$datefrom.'" AND date <= "'.$dateto.'"')->sum('share1');
                            $cpromo *= -1;

                            $turnover = (float) Wagersetting::whereRaw('aflid='.$affiliate->id.' AND share2id='.$affiliate->parentid.' AND date >= "'.$datefrom.'" AND date <= "'.$dateto.'"')->sum('stake');
                            $profitloss = (float) Wagersetting::whereRaw('aflid='.$affiliate->id.' AND share2id='.$affiliate->parentid.' AND date >= "'.$datefrom.'" AND date <= "'.$dateto.'"')->sum('profitloss');
							
							$bwinloss = (float) Wagersetting::whereRaw('aflid = '.$affiliate->id.' AND share2id='.$affiliate->parentid.' AND date >= "'.$datefrom.'" AND date <= "'.$dateto.'"')->sum('share1')*-1;
                            $bpromo = (float)  Ledgersetting::whereRaw('aflid='.$affiliate->id.' AND share2id='.$affiliate->parentid.' AND date >= "'.$datefrom.'" AND date <= "'.$dateto.'"')->sum('share1');
                        }
                        else {
                            if($id == 0) {
                                    $apromo = (float) Ledgersetting::whereRaw('share1id = '.$affiliate->id.' AND share2id='.$affiliate->parentid.' AND date >= "'.$datefrom.'" AND date <= "'.$dateto.'"')->sum('amount');
                                    $awinloss = (float) Wagersetting::whereRaw('share1id = '.$affiliate->id.' AND share2id='.$affiliate->parentid.' AND date >= "'.$datefrom.'" AND date <= "'.$dateto.'"')->sum('profitloss');
                                    $cpromo = (float) Ledgersetting::whereRaw('share1id='.$affiliate->id.' AND date >= "'.$datefrom.'" AND date <= "'.$dateto.'"')->sum('share2');
                                    $cwinloss = (float) Wagersetting::whereRaw(' share1id = '.$affiliate->id.' AND date >= "'.$datefrom.'" AND date <= "'.$dateto.'"')->sum('share2');

                                    $turnover = (float) Wagersetting::whereRaw(' share1id = '.$affiliate->id.' AND date >= "'.$datefrom.'" AND date <= "'.$dateto.'"')->sum('stake');
                                    $profitloss = (float) Wagersetting::whereRaw(' share1id = '.$affiliate->id.' AND date >= "'.$datefrom.'" AND date <= "'.$dateto.'"')->sum('profitloss');
									$awinloss = $profitloss;
                            } else {
                                    $apromo = (float) Ledgersetting::whereRaw('share1id = '.$affiliate->id.' AND share2id='.$affiliate->parentid.' AND date >= "'.$datefrom.'" AND date <= "'.$dateto.'"')->sum('share1');
                                    $awinloss = (float) Wagersetting::whereRaw('share1id = '.$affiliate->id.' AND share2id='.$affiliate->parentid.' AND date >= "'.$datefrom.'" AND date <= "'.$dateto.'"')->sum('share1');
                                    $cpromo = (float) Ledgersetting::whereRaw('aflid IN ('.implode(',', $aff_ids).') AND share1id = '.$affiliate->parentid.' AND date >= "'.$datefrom.'" AND date <= "'.$dateto.'"')->sum('share1');
                                    $cwinloss = (float) Wagersetting::whereRaw('aflid IN ('.implode(',', $aff_ids).') AND share1id = '.$affiliate->parentid.' AND date >= "'.$datefrom.'" AND date <= "'.$dateto.'"')->sum('share1');
                                    $awinloss *= -1;
                                    //$apromo *= -1;
                                    $cpromo *= -1;

                                    $turnover = (float) Wagersetting::whereRaw(' share1id = '.$affiliate->id.' AND date >= "'.$datefrom.'" AND date <= "'.$dateto.'"')->sum('stake');
                                    $profitloss = (float) Wagersetting::whereRaw(' share1id = '.$affiliate->id.' AND date >= "'.$datefrom.'" AND date <= "'.$dateto.'"')->sum('profitloss');
								
                            }
							
							 $bpromo =  ($cpromo + $apromo) * -1;
							 $bwinloss =  ($cwinloss + $awinloss) * -1;
                        }
                        
                       

                        $total_awinloss += $awinloss;
                        $total_bwinloss += $bwinloss;
                        $total_cwinloss += $cwinloss;

                        $total_apromo += $apromo;
                        $total_bpromo += $bpromo;
                        $total_cpromo += $cpromo;                       
                    }
                    
                    $total_deposit += $deposit;
                    $total_withdrawal += $withdrawal;
                    $total_bonus += $bonus;
                    $total_incentive += $incentive;
                    $total_turnover += $turnover;
                    $total_profitloss += $profitloss;
                    $total_netwinloss += $profitloss * 0.85;
                    
                    if($has_downline = Affiliate::whereRaw('parentid = '.$affiliate->id.' AND id != '.$affiliate->id)->count()){
			    $url = '<a href="'.route('agentReport', array('sid' => $affiliate->id, 'sfrom' => $datefrom, 'sto' => $dateto)).'">'.urlencode($affiliate->username).'</a>';

                    }
                    else {

			    $url = '<a href="'.route('agentReport', array('sid' => $affiliate->id, 'sfrom' => $datefrom, 'sto' => $dateto, 'agtmemrpt' => 'y')).'">'.urlencode($affiliate->username).'</a>';
			    if($affiliate->id == $id)
			    $url = '<a href="'.route('agentReport', array('sid' => $affiliate->id, 'iaflid' => $affiliate->id, 'sfrom' => $datefrom, 'sto' => $dateto, 'agtmemrpt' => 'y')).'">'.urlencode($affiliate->username).'</a>';
                    } 
                   
                    if($id == $affiliate->id){
						
						 $url = '<a href="'.route('agentReport', array('sid' => $affiliate->id, 'sfrom' => $datefrom, 'sto' => $dateto, 'agtmemrpt' => 'y')).'">'.urlencode($affiliate->username).'</a>';
					}
                    $username = $url;
					if($id == $affiliate->id){
						$username .= ' [MEMBERs]';
					}
                    $rate = 0;
                    if($profitloss <> 0) $rate = abs($cwinloss / $profitloss) * 100;

                    if(Config::get('setting.opcode') == 'GSC') {
                        $turnover = App::formatAddTabUrl(Lang::get('COMMON.MEMBERENQUIRY'), $turnover, action('Admin\ReportController@showMainReport2', array('aflid' => $affiliate->id, 'createdfrom' => $datefrom, 'createdto' => $dateto)));
                    }
					
					$percentage = Affiliatesetting::where( 'aflid' , '=' , $affiliate->id )->orderBy('id','DESC')->pluck('commission');
                    
                    $row = array(
                        'id' => $affiliate->id,
                        'affiliate'  => $username,
                        'deposit' 	 => App::displayAmount($deposit),
                        'withdrawal' => App::displayAmount($withdrawal),
                        'bonus' 	 => App::formatNegativeAmount($bonus),
                        'incentive'  => App::formatNegativeAmount($incentive),
                        'turnover' 	 => $turnover,
                        'profitloss' => App::formatNegativeAmount($profitloss),
                        'netwinloss' => App::formatNegativeAmount($profitloss*0.85),
                        'comm' 	     => App::displayPercentage($rate),
                        'valid'      => (isset($valid))?$valid:0,
                        'stake'      => (isset($turnover))?$turnover:0,
                        'currency'   => $percentage.'%',
                        'from' 	     => $datefrom,
                        'to' 	     => $dateto,
                        'awinloss'   => App::formatNegativeAmount($awinloss),
                        'bwinloss'   => App::formatNegativeAmount($bwinloss),
                        'cwinloss'   => App::formatNegativeAmount($cwinloss),
                        'apromo'     => App::formatNegativeAmount($apromo),
                        'bpromo'     => App::formatNegativeAmount($bpromo),
                        'cpromo'     => App::formatNegativeAmount($cpromo),
                        'atotal'     => App::formatNegativeAmount($awinloss + $apromo),
                        'btotal'     => App::formatNegativeAmount($bwinloss + $bpromo),
                        'ctotal'     => App::formatNegativeAmount($cwinloss + $cpromo),
                    );
                    
                    if($deposit > 0 || $turnover > 0 || $withdrawal > 0)
			$rows[] = $row;
                }
            }
	    
            $footers[] = array(
                    'affiliate'	=> Lang::get('COMMON.TOTAL'),
                    'deposit' => App::displayAmount($total_deposit),
                    'withdrawal' => App::displayAmount($total_withdrawal),
                    'bonus' => App::formatNegativeAmount($total_bonus),
                    'incentive' => App::formatNegativeAmount($total_incentive),
                    'turnover' => App::displayAmount($total_turnover),
                    'profitloss' => App::formatNegativeAmount($total_profitloss),
                    'netwinloss' => App::formatNegativeAmount($total_netwinloss),
                    'awinloss' => App::formatNegativeAmount($total_awinloss),
                    'bwinloss' => App::formatNegativeAmount($total_bwinloss),
                    'cwinloss' => App::formatNegativeAmount($total_cwinloss),
                    'apromo' => App::formatNegativeAmount($total_apromo),
                    'bpromo' => App::formatNegativeAmount($total_bpromo),
                    'cpromo' => App::formatNegativeAmount($total_cpromo),
                    'atotal' => App::formatNegativeAmount($total_awinloss + $total_apromo),
                    'btotal' => App::formatNegativeAmount($total_bwinloss + $total_bpromo),
                    'ctotal' => App::formatNegativeAmount($total_cwinloss + $total_cpromo),
            );
		
            if($return) return $rows;
            else {
                    echo json_encode(array('total' => count($rows), 'rows' => $rows, 'footer' => $footers));
                    exit;
            }
	
	}
	
		protected function showagentmemberReportdoGetData(Request $request, $return = false){

		$aflt = $request->input('aflt');

		if (!is_array($aflt)) $aflt = array();

		$timefrom = $request->createdfrom.' 00:00:00';
		$timeto   = $request->createdto.' 23:59:59';
		$datefrom = $request->createdfrom;
		$dateto   = $request->createdto;

		$id = (isset($aflt['sid']) ? $aflt['sid'] : 0);

		$condition = 'aflid = '.$id.' AND status = 1';
		$total = Account::whereRaw($condition)->count();
		$rows = array();
		$total_register = 0;
		$total_betmember = 0;
		$total_deposit = 0;
		$total_withdrawal = 0;
		$total_bonus = 0;
		$total_incentive = 0;
		$total_turnover = 0;
		$total_profitloss = 0;
		$total_netwinloss = 0;
		$total_commission = 0;
		$total_rebate = 0;
		$total_fee = 0;
		$total_payout = 0;
		$total_netpnl = 0;
		$total_apromo = 0;
		$total_bpromo = 0;
		$total_cpromo = 0;
		$total_awinloss = 0;
		$total_bwinloss = 0;
		$total_cwinloss = 0;
	    
            if ($members = Account::whereRaw($condition)->skip($request->recordstartindex)->take($request->pagesize)->get()) {
                foreach($members as $member) {
			$rate = 0;
			$deposit = 0;
			$withdrawal = 0;
			$bonus = 0;
			$incentive = 0;
			$turnover = 0;
			$profitloss = 0;
			$commission = 0;
			$rebate = 0;
			$fee = 0;
			$payout = 0;
			$netpnl = 0;
			$apromo = 0;
			$bpromo = 0;
			$cpromo = 0;
			$awinloss = 0;
			$bwinloss = 0;
			$cwinloss = 0;

			$commission = 0;
			$rebate = 0;
			
			$deposit = Cashledger::where('chtcode', '=', CBO_CHARTCODE_DEPOSIT)
				->where('accid', '=', $member->id)
				->where('created', '>=', $timefrom)
				->where('created', '<=', $timeto)
				->whereIn('status', array(CBO_LEDGERSTATUS_CONFIRMED, CBO_LEDGERSTATUS_MANCONFIRMED))
				->sum('amount');
			$withdrawal = Cashledger::where('chtcode', '=', CBO_CHARTCODE_WITHDRAWAL)
				->where('accid', '=', $member->id)
				->where('created', '>=', $timefrom)
				->where('created', '<=', $timeto)
				->whereIn('status', array(CBO_LEDGERSTATUS_CONFIRMED, CBO_LEDGERSTATUS_MANCONFIRMED))
				->sum('amount');
			$bonus = Cashledger::where('chtcode', '=', CBO_CHARTCODE_BONUS)
				->where('accid', '=', $member->id)
				->where('created', '>=', $timefrom)
				->where('created', '<=', $timeto)
				->whereIn('status', array(CBO_LEDGERSTATUS_CONFIRMED, CBO_LEDGERSTATUS_MANCONFIRMED))
				->sum('amount');
			$incentive = Cashledger::where('chtcode', '=', CBO_CHARTCODE_INCENTIVE)
				->where('accid', '=', $member->id)
				->where('created', '>=', $timefrom)
				->where('created', '<=', $timeto)
				->whereIn('status', array(CBO_LEDGERSTATUS_CONFIRMED, CBO_LEDGERSTATUS_MANCONFIRMED))
				->sum('amount');
			$turnover = Profitloss::where('accid', '=', $member->id)
				->where('date', '>=', $datefrom)
				->where('date', '<=', $dateto)
				->where('category', '!=', Product::CATEGORY_LOTTERY)
				->sum('totalstake');
			$valid = Profitloss::where('accid', '=', $member->id)
				->where('date', '>=', $datefrom)
				->where('date', '<=', $dateto)
				->where('category', '!=', Product::CATEGORY_LOTTERY)
				->sum('validstake');
			$profitloss = Profitloss::where('accid', '=', $member->id)
				->where('date', '>=', $datefrom)
				->where('date', '<=', $dateto)
				->where('category', '!=', Product::CATEGORY_LOTTERY)
				->sum('amount');
				
			if( $member->crccode == 'IDR' || $member->crccode == 'VND' )
			{
				$valid      = $valid      * 1000;
				$turnover   = $turnover   * 1000;
				$profitloss = $profitloss * 1000;
			}

			$bwinloss = (float) Wagersetting::where('accid', '=', $member->id)
				->where('share1id', '>=', $member->aflid)
				->where('date', '>=', $datefrom)
				->where('date', '<=', $dateto)
				->sum('share1');
			$bwinloss *= -1;
			$cwinloss = $awinloss - $bwinloss;
			
			$bpromo = (float) Ledgersetting::where('accid', '=', $member->id)
				->where('share1id', '>=', $member->aflid)
				->where('date', '>=', $datefrom)
				->where('date', '<=', $dateto)
				->sum('share1');
			$cpromo = $apromo - $bpromo;
			
			$total_bwinloss += $bwinloss;
			$total_cwinloss += $cwinloss;

			$total_cpromo += $cpromo;
			$total_bpromo += $bpromo;

			$total_deposit += $deposit;
			$total_withdrawal += $withdrawal;
			$total_bonus += $bonus;
			$total_incentive += $incentive;
			$total_turnover += $turnover;
			$total_profitloss += $profitloss;
			$total_netwinloss += ($profitloss * 0.85);
			
			$newParams = array('iaccid' => $member->id);
			$newParams['aqrng']['createdfrom'] = $datefrom;
			$newParams['aqrng']['createdto'] = $dateto;
			
			$rate = 0;
			if ($profitloss != 0) $rate = abs($cwinloss / $profitloss) * 100;
			
			$percentage = Affiliatesetting::where( 'aflid' , '=' , $id )->orderBy('id','DESC')->pluck('commission');
			
			$turnover 	 	= App::formatAddTabUrl( Lang::get('COMMON.MEMBERENQUIRY'), $turnover, action('Admin\ReportController@showMainReport2', array('accid' => $member->id, 'createdfrom' => $datefrom, 'createdto' =>  $dateto )) );
			
			$deposit_link   = $deposit;
			
			$withdraw_link  = $withdrawal;
			
			$bonus_link     = $bonus;
			
			$incentive_link = App::formatNegativeAmount($incentive);
			
			$row = array(
				'id' => $member->id,
				'affiliate' => App::formatAddTabUrl(Lang::get('COMMON.BETDETAILS').': '.urlencode($member->nickname), urlencode($member->nickname), route('showdetail', $newParams)),
				'deposit' => $deposit_link,
				'withdrawal' => $withdraw_link,
				'bonus' => $bonus_link,
				'incentive' => $incentive_link,
				'turnover' => $turnover,
				'profitloss' => App::formatNegativeAmount($profitloss),
				'netwinloss' => App::formatNegativeAmount($profitloss * 0.85),
				'valid' => $valid,
				'stake' => $turnover,
				'currency' => $percentage.'%',
				'from' => $datefrom,
				'to' => $dateto,
				'comm' => App::displayPercentage($rate),
				'awinloss' => App::formatNegativeAmount($awinloss),
				'bwinloss' => App::formatNegativeAmount($bwinloss),
				'cwinloss' => App::formatNegativeAmount($cwinloss),
				'apromo' => App::formatNegativeAmount($apromo),
				'bpromo' => App::formatNegativeAmount($bpromo),
				'cpromo' => App::formatNegativeAmount($cpromo),
				'atotal' => App::formatNegativeAmount($awinloss + $apromo),
				'btotal' => App::formatNegativeAmount($bwinloss + $bpromo),
				'ctotal' => App::formatNegativeAmount($cwinloss + $cpromo),
			);
			
			$rows[] = $row;
		}
	    }

            $footers[] = array(
			'affiliate' => Lang::get('COMMON.TOTAL') ,
			'deposit' => App::displayAmount($total_deposit),
			'withdrawal' => App::displayAmount($total_withdrawal),
			'bonus' => App::formatNegativeAmount($total_bonus),
			'incentive' => App::formatNegativeAmount($total_incentive),
			'turnover' => App::displayAmount($total_turnover),
			'profitloss' => App::formatNegativeAmount($total_profitloss),
			'netwinloss' => App::formatNegativeAmount($total_netwinloss),
			'awinloss' => App::formatNegativeAmount($total_awinloss),
			'bwinloss' => App::formatNegativeAmount($total_bwinloss),
			'cwinloss' => App::formatNegativeAmount($total_cwinloss),
			'apromo' => App::formatNegativeAmount($total_apromo),
			'bpromo' => App::formatNegativeAmount($total_bpromo),
			'cpromo' => App::formatNegativeAmount($total_cpromo),
			'atotal' => App::formatNegativeAmount($total_awinloss + $total_apromo),
			'btotal' => App::formatNegativeAmount($total_bwinloss + $total_bpromo),
			'ctotal' => App::formatNegativeAmount($total_cwinloss + $total_cpromo),
		'from_agtmemrpt'=>'yes',
		);
		
            if($return) return $rows;
            else {
                    echo json_encode(array('total' => count($rows), 'rows' => $rows, 'footer' => $footers));
                    exit;
            }
	
	}
	
	protected function showMemberReport(Request $request){
		
		$grid = new PanelGrid;
		$grid->setupGrid( $this->moduleName, 'affiliateMemberReport-data', $this->limit, true, array( 'footer' => true , 'params' => array( 'iaflid' => $request->iaflid , 'createdfrom' => $request->createdfrom , 'createdto' => $request->createdto)) );
		$grid->setTitle(Lang::get('COMMON.AFFILIATE'));
				
	    $grid->addColumn('member', 				Lang::get('COMMON.MEMBER'), 					90,			array());
        $grid->addColumn('deposit', 			Lang::get('COMMON.DEPOSIT'), 					80,			array('align' => 'right', 'color' => '#FFF8C6'));
        $grid->addColumn('withdrawal', 			Lang::get('COMMON.WITHDRAWAL'), 				80,			array('align' => 'right', 'color' => '#FFF8C6'));
        $grid->addColumn('turnover', 			Lang::get('COMMON.TURNOVER'), 					80,			array('align' => 'right', 'color' => '#F6C7CE'));
        $grid->addColumn('profitloss', 			Lang::get('COMMON.PROFITLOSS'), 				90,			array('align' => 'right', 'color' => '#F6C7CE'));
        $grid->addColumn('bonus', 				Lang::get('COMMON.BONUS'), 						70,			array('align' => 'right', 'color' => '#AFDCEC'));
        $grid->addColumn('incentive', 			Lang::get('COMMON.INCENTIVE'), 					70,			array('align' => 'right', 'color' => '#AFDCEC'));
        $grid->addColumn('netpnl', 				Lang::get('COMMON.NETPROFITLOSS'), 				90,			array('align' => 'right', 'color' => '#AFDCEC'));

		


		$data['grid'] = $grid->getTemplateVars();

		return view('admin.grid2',$data);
		
	}
	
	protected function doGetMemberReportData(Request $request){
		
	    $timefrom = $request->createdfrom.' 00:00:00';
        $timeto	  = $request->createdto.' 23:59:59';
        $datefrom = $request->createdfrom;
        $dateto   = $request->createdto;
		
 
		$condition =  'aflid = '.$request->iaflid;
		$total = Account::whereRaw($condition)->count();
		$rows = array();
		
		if($members = Account::whereRaw($condition)->get()) {
			$total_deposit = 0;
			$total_withdrawal = 0;
			$total_bonus = 0;
			$total_incentive = 0;
			$total_turnover = 0;
			$total_profitloss = 0;
			$total_netpnl = 0;
			
            foreach($members as $member) {

				$rate = 0;
				$deposit = 0;
				$withdrawal = 0;
				$bonus = 0;
				$incentive = 0;
				$turnover = 0;
				$profitloss = 0;
				$payout = 0;
				$netpnl = 0;
				
				$deposit 	= Cashledger::whereRaw('chtcode = '.CBO_CHARTCODE_DEPOSIT.' AND accid = '.$member->id.' AND created >= "'.$timefrom.'" AND created <= "'.$timeto.'" AND status IN ('.CBO_LEDGERSTATUS_CONFIRMED.', '.CBO_LEDGERSTATUS_MANCONFIRMED.')')->sum('amount');
				$withdrawal = Cashledger::whereRaw('chtcode = '.CBO_CHARTCODE_WITHDRAWAL.' AND accid = '.$member->id.' AND created >= "'.$timefrom.'" AND created <= "'.$timeto.'" AND status IN ('.CBO_LEDGERSTATUS_CONFIRMED.', '.CBO_LEDGERSTATUS_MANCONFIRMED.')')->sum('amount');
				$bonus 		= Cashledger::whereRaw('chtcode = '.CBO_CHARTCODE_BONUS.' AND accid = '.$member->id.' AND created >= "'.$timefrom.'" AND created <= "'.$timeto.'" AND status IN ('.CBO_LEDGERSTATUS_CONFIRMED.', '.CBO_LEDGERSTATUS_MANCONFIRMED.')')->sum('amount');
				$incentive  = Cashledger::whereRaw('chtcode = '.CBO_CHARTCODE_INCENTIVE.' AND accid = '.$member->id.' AND created >= "'.$timefrom.'" AND created <= "'.$timeto.'" AND status IN ('.CBO_LEDGERSTATUS_CONFIRMED.', '.CBO_LEDGERSTATUS_MANCONFIRMED.')')->sum('amount');
				$turnover   = Profitloss::whereRaw('accid = '.$member->id.' AND date >= "'.$datefrom.'" AND date <= "'.$dateto.'"')->sum('totalstake');
				$profitloss = Profitloss::whereRaw('accid = '.$member->id.' AND date >= "'.$datefrom.'" AND date <= "'.$dateto.'"')->sum('amount');
				
				$profitloss = ($member->crccode == 'VND' || $member->crccode == 'IDR')? $profitloss * 1000 : $profitloss;
				$turnover   = ($member->crccode == 'VND' || $member->crccode == 'IDR')? $turnover * 1000 : $turnover;
					
				$netpnl 	= $profitloss + ($bonus + $incentive);
				
				
				$total_deposit += $deposit;
				$total_withdrawal += $withdrawal;
				$total_bonus += $bonus;
				$total_incentive += $incentive;
				$total_turnover += $turnover;
				$total_profitloss += $profitloss;
				$total_netpnl += $netpnl;
				
				if($turnover > 0){
					$detailParams['iaccid'] 	 = $member->id;
					$detailParams['createdfrom'] = $request->createdfrom;
					$detailParams['createdto']   = $request->createdto;
					
					$turnover = '<a onclick="window.open(\''.action('Admin\ReportController@showdetail',$detailParams).'\', \'report\', \'scrollbars=1,width=1500,height=830\')" href="#">'.App::formatNegativeAmount($turnover).'</a>';
					
					$row = array(
						'member' 		=> $member->nickname,
						'deposit' 		=> App::displayAmount($deposit),
						'withdrawal' 	=> App::displayAmount($withdrawal),
						'bonus' 		=> App::formatNegativeAmount($bonus),
						'incentive' 	=> App::formatNegativeAmount($incentive),
						'turnover' 		=> ($turnover),
						'profitloss' 	=> App::formatNegativeAmount($profitloss),
						'netpnl' 		=> App::formatNegativeAmount($netpnl),
					);
					$rows[] = $row;
				}
				
            }
        }
		
		$footers[] = array(
			'member'	=> Lang::get('COMMON.TOTAL'),
			'deposit' => App::displayAmount($total_deposit),
			'withdrawal' => App::displayAmount($total_withdrawal),
			'bonus' => App::formatNegativeAmount($total_bonus),
			'incentive' => App::formatNegativeAmount($total_incentive),
			'turnover' => App::displayAmount($total_turnover),
			'profitloss' => App::formatNegativeAmount($total_profitloss),
			'netpnl' => App::formatNegativeAmount($total_netpnl),
		);
		echo json_encode(array('total' => count($rows), 'rows' => $rows, 'footer' => $footers));
		exit;
		
	}
	
	
}
