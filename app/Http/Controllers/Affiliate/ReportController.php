<?php namespace App\Http\Controllers\Affiliate;

use App\libraries\grid\PanelGrid;
use App\libraries\App;
use App\libraries\sms\SMS88;
use App\libraries\sms\SMSMoDing;
use App\Models\Channel;
use App\Models\Currency;
use App\Models\Account;
use App\Models\Affiliate;
use App\Models\Accountdetail;
use App\Models\Cashledger;
use App\Models\Bankledger;
use App\Models\Promocampaign;
use App\Models\Promocash;
use App\Models\Agent;
use App\Models\Product;
use App\Models\Smslog;
use App\Models\User;
use App\Models\Wager;
use App\Models\Cashbalance;
use App\Models\Profitloss;
use App\Models\Balancetransfer;
use Illuminate\Http\Request;
use Auth;
use Validator;
use Session;
use Lang;
use DB;
use Config;
use Carbon\Carbon;
use Excel;

class ReportController extends Controller
{
    protected $moduleName 	= 'report';
    protected $limit 		= 20;
    protected $start 		= 0;

    public function showMemberWallet(Request $request) {

        $grid = new PanelGrid;

        $grid->setupGrid($this->moduleName, 'showMemberWallet-data', $this->limit, true, array('multiple' => true,'footer' => true ));

        $grid->setTitle(Lang::get('COMMON.MEMBERWALLETREPORT'));

        $grid->addColumn('name', 			Lang::get('COMMON.NAME'), 						150,		array('align' => 'left'));
        $grid->addColumn('code',			Lang::get('COMMON.PRODUCT'), 					150,		array('align' => 'left'));
        $grid->addColumn('balance',			Lang::get('COMMON.BALANCE'), 					150,		array('align' => 'left'));
        $grid->addColumn('mainwallet',			Lang::get('COMMON.MAINWALLET'), 					150,		array('align' => 'left'));


        //$grid->addSearchField('search', array('tel_mobile' => Lang::get('COMMON.TELEPHONENO')));


        $data['grid'] = $grid->getTemplateVars();
        $data['paging'] = false;

        return view('affiliate.grid2',$data);
    }

    public function doGetMemberWalletData(Request $request){

        $footers = array();
        $is_show = array();
        $total_mainwallet = 0;
        $total_balance = 0;
        $rows = array();
        $cashledgers = $mainwallet = array();

        $condition = '';
        $aflid = Session::get('affiliate_userid');

        $condition = ' and c.accid IN (select id from account where aflid = \''.$aflid.'\')';

        $datas = DB::select( DB::raw("select c.accid,c.acccode,d.code,c.balance from cashbalance c,product d where c.prdid = d.id and c.balance > 1 and c.crccode = '".Session::get('affiliate_crccode')."' ".$condition." order by balance desc "));

        // Get downline member's main wallet amount.
        $downlineAccounts = Account::where('aflid', '=', $aflid)->get(['id', 'code']);

        foreach ($downlineAccounts as $da) {
            $cashledger = Cashledger::select(array('accid', 'acccode'))->selectRaw('sum(amount) as amount')->whereRaw('accid = '.$da->id.' and ((status in ('.CBO_LEDGERSTATUS_PENDING.', '.CBO_LEDGERSTATUS_MANPENDING.','.CBO_LEDGERSTATUS_CONFIRMED.', '.CBO_LEDGERSTATUS_MANCONFIRMED.', '.CBO_LEDGERSTATUS_PROCESSED.') AND chtcode IN ('.CBO_CHARTCODE_WITHDRAWAL.', '.CBO_CHARTCODE_BALANCETRANSFER.', '.CBO_CHARTCODE_WITHDRAWALCHARGES.' )) OR (status in ('.CBO_LEDGERSTATUS_CONFIRMED.', '.CBO_LEDGERSTATUS_MANCONFIRMED.') AND chtcode NOT IN ('.CBO_CHARTCODE_WITHDRAWAL.', '.CBO_CHARTCODE_BALANCETRANSFER.' , '.CBO_CHARTCODE_WITHDRAWALCHARGES.' )))')->first();

            if ($cashledger && !empty($cashledger->accid)) {
                $mainwallet[$cashledger->accid] = $cashledger->amount;
            } else {
                $mainwallet[$da->id] = 0;

                $cashledger = new \stdClass();
                $cashledger->accid = $da->id;
                $cashledger->acccode = $da->code;
                $cashledger->amount = 0;
            }

            $cashledgers[] = $cashledger;
        }

        $is_displays = array();

        foreach( $datas as $data ){

            $is_displays[$data->accid] = true;

            if( !in_array($data->accid,$is_show) ){

                $total_mainwallet += isset($mainwallet[$data->accid]) ? $mainwallet[$data->accid]:0;
            }

            $total_balance += $data->balance;

            $rows[] = array(
                'name'  		=> $data->acccode,
                'code'  		=> $data->code,
                'balance'  		=> App::formatNegativeAmount($data->balance),
                'mainwallet'  	=> App::formatNegativeAmount(isset($mainwallet[$data->accid]) ? $mainwallet[$data->accid] : 0)

            );

            $is_show[$data->accid] = $data->accid;
        }

        foreach($cashledgers as $cashledger){

            if( !isset($is_displays[$cashledger->accid]) ){

                $rows[] = array(
                    'name'  		=> substr($cashledger->acccode,3),
                    'code'  		=> 'Main Wallet',
                    'balance'  		=> 0,
                    'mainwallet'  	=> App::formatNegativeAmount($cashledger->amount)

                );
            }
        }

        $footers[] = array(
            'balance' => App::formatNegativeAmount($total_balance),
            'mainwallet' => App::formatNegativeAmount($total_mainwallet),

        );


        echo json_encode(array('total' => count($rows),'rows' => $rows, 'footer' => $footers));
        exit;
    }
}