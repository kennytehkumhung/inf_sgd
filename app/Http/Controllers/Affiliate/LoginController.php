<?php namespace App\Http\Controllers\Affiliate;

//use App\Http\Controllers\Controller\Affiliate;
use App\libraries\App;
use App\Models\Affiliatesetting;
use GeoIP;
use Illuminate\Http\Request;
use Auth;
use Validator;
use Session;
use Lang;
use App\Models\Currency;
use App\Models\Affiliate;
use App\Models\Agent;
use Config;
use Response;
use Redirect;
use Hash;
use App\libraries\Login;
use App\Jobs\ChangeLocale;

class LoginController extends Controller{
	
	public function __construct()
	{
		
	}

	public function index(Request $request)
	{

	
		 if( Config::get('setting.front_path') == 'front' ) {
		    if ($request->input('crccode') == 'TWD') {
                return view('affiliate.inf_login_tw');
            } else {
                // Refer \App\Http\Middleware\RedirectByCountry@handle
                $geoip = GeoIP::getLocation($request->ip());

                if( isset($geoip['isoCode']) && $geoip['isoCode'] == 'TW' ) {
                    return view('affiliate.inf_login_tw');
                }
            }

            return view('affiliate.inf_login');
        } elseif ( Config::get('setting.opcode') == 'RYW' ) {
				
			if(starts_with($_SERVER['HTTP_HOST'], 'affiliate.'))
            {
				return view('affiliate.ryw_index');
            }
			else
			{
                return view('affiliate.ryw_agent_index');
            }
		   
		}else{
			return view('affiliate.login');
		}
	}	
	
	public function process(Request $request)
	{       
	
		/* if( Session::get('affiliate_catpcha') != $request->input('code') )
		{
			return redirect::route('affiliate_login')->with('message', 'Wrong captcha');
		} */

		$urlParams = array();

		if ($request->has('crccode')) {
		    $urlParams['crccode'] = $request->get('crccode');
        }
		
		$validator = Validator::make(
			[
                'password'  	 	 => $request->input('password'),
				'username'   		 => $request->input('username'),
			],
			[
			    'password'	  		 => 'required',
			    'username'	   	 	 => 'required',
			]
		);

		
		if ($validator->fails())
		{
			return redirect::route('affiliate_login', $urlParams)->with('message', Lang::get('public.LoginFailed'));
		}
		
		$affiliate = Affiliate::where( 'username' , '=' , $request->input('username') )->first();
		
		if( !isset($affiliate->islogin) ){
			
			return redirect::route('affiliate_login', $urlParams)->with('message', Lang::get('public.LoginFailed'));
		}
		
		if( $affiliate->islogin == 0 && $affiliate->oldpassword == App::getEncryptPassword($request->input('password')) ){
			$affiliate->password      = Hash::make( $request->input('password') );
			$affiliate->islogin   	   = 1;
			$affiliate->save();
		}
 
        if ( Config::get('setting.opcode') == 'GSC' ) {
            // Special for GSH, affiliate with parentId = 0 only can login with subdomain: aff.xxx.xxx
            // ParentId != 0 only can login with subdomain: affiliate.xxx.xxx
		    $isValid = false;
		    $url = $request->url();

		    if ($affiliate->parentid == 0 && str_contains($url, '/aff.')) {
                $isValid = true;
            } elseif ($affiliate->parentid != 0 && str_contains($url, '/affiliate.')) {
                $isValid = true;
            }

            if (!$isValid) {
                return redirect::route('affiliate_login', $urlParams)->with('message', Lang::get('public.LoginFailed'));
            }
        } 

		if (Auth::affiliate()->attempt(array('username' => $request->input('username'), 'password' => $request->input('password') , 'status' => 1 )))
        {	
		
			$affiliate = Auth::affiliate()->get();
			
			$login = new Login;
			$login->doMakeTracker(Auth::affiliate()->get(), 'Affiliate');
			
			Session::put('affiliate_username',       $affiliate->username );
			Session::put('affiliate_lastpwdchange',  $affiliate->lastpwdchange );
			Session::put('affiliate_lastlogin',  	 $affiliate->lastlogin );
			Session::put('affiliate_userid',  	   	 $affiliate->id );
			Session::put('affiliate_parentid',  	 $affiliate->parentid );
			Session::put('affiliate_level',  	   	 $affiliate->level );
			Session::put('affiliate_password',  	 $affiliate->password );
			Session::put('affiliate_code',  	     $affiliate->code );
			Session::put('affiliate_crccode',  	     $affiliate->crccode);
			Session::put('affiliate_province',  	 $affiliate->province);
			Session::put('affiliate_city',  		 $affiliate->city);
			Session::put('affiliate_district',  	 $affiliate->district);
			Session::put('admin_crccode',  	   	     $affiliate->crccode);
            Session::put('affiliate_permission_view_winloss_report', $affiliate->view_winloss_report);
            Session::put('affiliate_permission_add_user', $affiliate->add_user);
			
			Affiliate::where( 'id' , '=' , $affiliate->id )->update(
				array( 'lastlogin' => date('Y-m-d H:i:s'))
			);
			
			if( $affiliate->crccode == 'CNY'){
                $changeLocale = new ChangeLocale('cn');
                $this->dispatch($changeLocale);
            }
	
			if( $affiliate->type == 1 )
				return redirect::route('agent_home');
			else
				return redirect::route('affiliate_home');
		}else{
			return redirect::route('affiliate_login', $urlParams)->with('message', Lang::get('public.LoginFailed'));
		}
		
	}		
	
	public function getCaptcha(){
	
		$img=imagecreatefromjpeg("front/img/texture.jpg");	
		Session::put('affiliate_catpcha', rand(1000,9999));
		$security_number = Session::get('affiliate_catpcha');
		$image_text=$security_number;	
		$red=rand(100,255); 
		$green=rand(100,255);
		$blue=rand(100,255);
		$text_color=imagecolorallocate($img,205,205,205);
		$text=imagettftext($img,22,rand(-10,10),rand(10,30),rand(25,35),$text_color,Config::get('setting.server_path').'front/fonts/arial.ttf',$image_text);
	
		imagejpeg($img);
		return Response::make('', '200')->header('Content-Type', 'image/jpeg');
	}

	public function setLanguage(Request $request){
		
	    $changeLocale = new ChangeLocale($request->input('lang'));
        $this->dispatch($changeLocale);

        return redirect()->back();
		
	}	
	
	
}
