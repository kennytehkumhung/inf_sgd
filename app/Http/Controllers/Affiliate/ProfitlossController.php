<?php namespace App\Http\Controllers\Affiliate;

use App\libraries\grid\PanelGrid;
use App\libraries\App;
use App\Models\Account;
use App\Models\Channel;
use App\Models\Currency;
use App\Models\Product;
use App\Models\Profitloss;
use App\Models\Agent;
use App\Models\Affiliate;
use App\Models\Cashledger;
use App\Models\Wager;
use App\Models\Commissionsetting;
use App\Models\Affiliatedetail;
use Illuminate\Http\Request;
use Auth;
use Validator;
use Session;
use Lang;
use GeoIP;
use DB;
use Config;


class ProfitlossController extends Controller{
	
	protected $moduleName 	= 'report';
	protected $limit 		= 20;
	protected $start 		= 0;
	static protected $affiliateInstances = array();
	
	public function __construct()
	{
		
	}

    public function index(Request $request)
    {
        if (Session::get('affiliate_permission_view_winloss_report') != 1) {
            echo 'Unauthorized';
            exit;
        }

        $grid = new PanelGrid;
        $grid->setupGrid($this->moduleName, 'profitloss-data', $this->limit, true, array('multiple' => true,'footer' => true ));
        $grid->setTitle(Lang::get('COMMON.REPORT'));

        $grid->addColumn('member',			Lang::get('COMMON.ACCOUNT'), 		130,	array('align' => 'left'));
        $grid->addColumn('product',			Lang::get('COMMON.PRODUCT'), 		130,	array('align' => 'left'));
        $grid->addColumn('currency',		Lang::get('COMMON.CURRENCY'), 		130,	array('align' => 'center'));
        $grid->addColumn('wager',			Lang::get('COMMON.WAGERNUM'), 		130,	array('align' => 'right'));
        $grid->addColumn('stake',			Lang::get('COMMON.STAKE'), 			130,	array('align' => 'right'));
        $grid->addColumn('validstake',		Lang::get('COMMON.VALIDSTAKE'), 	130,	array('align' => 'right'));
        $grid->addColumn('winloss',			Lang::get('COMMON.MEMBERPNL'), 		130,	array('align' => 'right'));
        $grid->addColumn('commission',		Lang::get('COMMON.COMMISSION'),		130,	array('align' => 'right'));

        $grid->addFilter('status',array('Suspended','Active'), array('display' => Lang::get('COMMON.TYPE')));
        //$grid->addButton('1', 'New', Lang::get('COMMON.ADDNEW').' '.Lang::get('COMMON.MEMBER'), 'button', array('icon' => 'add', 'url' => action('Affiliate\ProfitlossController@drawAddEdit')));

        $data['grid'] = $grid->getTemplateVars();
        return view('affiliate.grid2',$data);

    }

    protected function doGetData(Request $request) {
        if (Session::get('affiliate_permission_view_winloss_report') != 1) {
            echo 'Unauthorized';
            exit;
        }

        $aqrng  = $request->input('aqrng');
        $aflt   = $request->input('aqfld');
        $aqtxt  = $request->input('aflt');


        $ids = array();
        if($agents = Agent::whereRaw('aflid = '.Session::get('affiliate_userid'))->get()) {
            foreach($agents as $agent) {
                $ids[] = $agent->id;
            }
        }
        if(count($ids) < 1) $id = '0';
        else $id = implode(',', $ids);

        $from = ($request->has('createdfrom'))?$request->input('createdfrom'):App::getDateTime(3);
        $to   = ($request->has('createdto'))  ?$request->input('createdto'):App::getDateTime(3);
        $condition 	= 'istest = 0 AND date >= "'.$from.'" AND date <= "'.$to.'" AND level4 IN ('.$id.')';

        $user = 'code';
        $field = 'accid';

        $prdid = '';
        if(isset($aflt['iprdid']) && $aflt['iprdid'] > 0) {
            $condition .= ' AND prdid='.$aflt['iprdid'];
            $prdid = $aflt['iprdid'];
        }

        $category = '';
        if(isset($aflt['icategory']) && $aflt['icategory'] > 0) {
            $condition .= ' AND category='.$aflt['icategory'];
            $category = $aflt['icategory'];
        }

        if( $request->has('supline') && isset($aflt['iparent'])) {
            $condition .= ' AND '.$request->get('supline').'='.$request->get('supline');
        }

        $level = Agent::LEVEL_MEMBER;
        if($level)
            $total = ProfitLoss::whereRaw($condition)->count();
        else
            $total = 0;

        $mode = (isset($aflt['imode']))?$aflt['imode']:1;

        if($level) {

            $groupby = $field;
            if($mode == 2) {
                $groupby = 'prdid, '.$field;
            } else if($mode == 3) {
                $groupby = 'category, '.$field;
            }
        }

        $rows 		  = array();
        $footer		  = array();
        $total_report = array();

        $profitlosses = DB::select( DB::raw("SELECT level1, level2, level3, level4, accid, date, acccode, nickname, prdid, category, crccode, SUM(wager) as sumwager, SUM(totalstake) as sumstake, SUM(amount) as sumwinloss, SUM(validstake) as sumvalid
										FROM `profitloss`
										WHERE ".$condition."
										GROUP BY ".$groupby." 
										"));


        $categories = Product::getCategoryOptions();
        $products   = Product::getAllAsOptions(true);

        $currencyRate = 1;

        if (Config::get('setting.front_path') == 'idnwin99') {
            $currencyRate = 1000;
        }

        foreach($profitlosses as $profitloss) {
            $colspan = 2;
            $username = '';
            $winloss = App::formatNegativeAmount($profitloss->sumwinloss);
            $commission = 0;
            $netpnl = 0;
            $member_ids = array();

            $search_vendor  = 'doGridSearch(\'imode\', \'dropdown\', \'2\');doSearch();';
            $search_product = 'doGridSearch(\'imode\', \'dropdown\', \'3\');doSearch();';

            $product = $vendor = Lang::get('COMMON.ALL');
            if($mode == 2) $prdid = (isset($profitloss->prdid))?$profitloss->prdid:'';
            if($prdid != '') $vendor = $products[$profitloss->prdid];

            if($mode == 3) $category = (isset($profitloss->category))?$profitloss->category:'';
            if($category != '') $product = $categories[$profitloss->category];

            if($level == Agent::LEVEL_MEMBER) {
                $newParams = $request->all();
                $newParams['imode'] = 2; //view product
                $newParams['susername'] = $profitloss->acccode;
                if(isset($newParams['ssearch']))
                    unset($newParams['ssearch']);

                //$stake = action('Admin\ReportController@showmain',$newParams);
                if($mode == 2) {
                    $colspan = 3;
                    $detailParams['iprdid'] = $profitloss->prdid;
                    $detailParams['iaccid'] = $profitloss->accid;
                    $detailParams['createdfrom'] = $request->createdfrom;
                    $detailParams['createdto'] = $request->createdto;
                    $stake = '<a href="'.action('Affiliate\ProfitlossController@showdetail',$detailParams).'">'.App::displayNumberFormat($profitloss->sumstake * $currencyRate).'</a>';
                }
                $username = $profitloss->nickname;
            } else {
                if($object = Agent::whereRaw('level='.$level.' AND id='.$profitloss->$field)->first()) {
                    $username = '<a href="#" onclick="doGridSearch(\'iprdid\', \'dropdown\', \''.$prdid.'\');doGridSearch(\'icategory\', \'dropdown\', \''.$category.'\');doGridSearch(\'code\', \'search\', \''.$object->code.'\', \'code\');doGroupbyDate(1);">'.$object->code.'</a>';
                    if($groupbyDate == 1) $username = $profitloss->date;
                    if($level == Agent::LEVEL_AGENT) {
                        $username = $object->code;
                        if($object->isexternal == 1) {
                            if($afdObj = Affiliatedetail::whereRaw('aflid='.$object->id)->first()) {
                                $username .= ' ('.$afdObj->website.')';
                            }
                        }
                    }
                }
                $newParams = $request->all();
                $newParams['ilevel'] = $level + 1;
                $newParams['supline'] = $field;
                $newParams['iparent'] = $profitloss->$field;
                if(isset($newParams['susername']))
                    unset($newParams['susername']);
                if(isset($newParams['ssearch']))
                    unset($newParams['ssearch']);
                if($mode == 2)
                    $newParams['iprdid'] = $profitloss->prdid;

                //$stake = '<a href="'.action('Admin\ReportController@showmain',$newParams).'">'.App::displayNumberFormat($profitloss->sumstake).'</a>';
            }

            if (strlen($username) > 0) {
                $acctObj = Account::find($profitloss->accid);

                if ($acctObj && $acctObj->aflid > 0) {
                    $afltObj = Affiliate::find($acctObj->aflid);

                    $bonus = Cashledger::whereRaw('chtcode = '.CBO_CHARTCODE_BONUS.' AND accid = '.$profitloss->accid.' AND created >= "'.$from.' 00:00:00" AND created <= "'.$to.' 23:59:59" AND status IN ('.CBO_LEDGERSTATUS_CONFIRMED.', '.CBO_LEDGERSTATUS_MANCONFIRMED.')')->sum('amount');
                    $incentive = Cashledger::whereRaw('chtcode = '.CBO_CHARTCODE_INCENTIVE.' AND accid = '.$profitloss->accid.' AND created >= "'.$from.' 00:00:00" AND created <= "'.$to.' 23:59:59" AND status IN ('.CBO_LEDGERSTATUS_CONFIRMED.', '.CBO_LEDGERSTATUS_MANCONFIRMED.')')->sum('amount');
                    $profloss = Profitloss::whereRaw('accid = '.$profitloss->accid.' AND date >= "'.$from.' 00:00:00" AND date <= "'.$to.' 23:59:59"')->sum('amount');

                    $netpnl = ($profloss * $currencyRate) + ($bonus + $incentive);

                    if($netpnl < 0) {
                        $settings = array();
                        $pnl = $netpnl * -1;
                        if($tiers = Commissionsetting::whereRaw($afltObj->comid)->orderBy('rangefrom','ASC')->get()) {
                            $balance = $pnl;
                            $start = 0;
                            foreach($tiers as $tier) {

                                if( Config::get('setting.front_path') == 'sbm' && $afltObj->username == 'c')
                                    $percentage = 0;
                                else
                                    $percentage = $tier->percentage;

                                if($pnl > 0 && ($pnl >= $tier->rangefrom)) {
                                    $rate = $percentage;
                                    if($pnl > $tier->rangeto && $tier->rangeto > 0) {
                                        $commission += ($percentage * ($tier->rangeto - $start) / 100);
                                    } else
                                        $commission += ($percentage * ($pnl - $start) / 100);

                                    $start = $tier->rangeto;
                                }
                            }
                        }
                    }
                }
            }


            $search_vendor = 'doGridSearch(\'imode\', \'dropdown\', \'2\');doGridSearch(\'iprdid\', \'dropdown\', \''.$prdid.'\');doGridSearch(\'icategory\', \'dropdown\', \''.$category.'\');doGridSearch(\'code\', \'search\', \''.strip_tags($username).'\', \'code\');doSearch();';
            $search_product = 'doGridSearch(\'imode\', \'dropdown\', \'3\');doGridSearch(\'iprdid\', \'dropdown\', \''.$prdid.'\');doGridSearch(\'icategory\', \'dropdown\', \''.$category.'\');doGridSearch(\'code\', \'search\', \''.strip_tags($username).'\', \'code\');doSearch();';

            if($level == Agent::LEVEL_MEMBER) {
                $newParams = array('iaccid' => $profitloss->accid);
                $newParams['aqrng']['createdfrom'] = $from;
                $newParams['aqrng']['createdto'] = $to;
                $newParams['aflt']['iprdid'] = $prdid;

                $detailParams['iprdid'] = 0;
                $detailParams['iaccid'] = $profitloss->accid;
                $detailParams['createdfrom'] = $request->createdfrom;
                $detailParams['createdto'] = $request->createdto;

                $newParams['aflt']['icategory'] = $category;
                $stake = App::formatAddTabUrl(Lang::get('COMMON.BETDETAILS').': '.$username.' - '.$vendor.' - '.$product, App::formatNegativeAmount($profitloss->sumstake * $currencyRate), action('Affiliate\ProfitlossController@showdetail',$detailParams));
                $name = Lang::get('COMMON.BETDETAILS').': '.$username.' - '.$vendor.' - '.$product;
                $stake = '<a onclick="window.open(\''.action('Affiliate\ProfitlossController@showdetail',$detailParams).'\', \'report\', \'width=1050,height=830\')" href="#">'.App::formatNegativeAmount($profitloss->sumstake * $currencyRate).'</a>';
            } else{
                $stake = '<a href="#" onclick="doGridSearch(\'iprdid\', \'dropdown\', \''.$prdid.'\');doGridSearch(\'icategory\', \'dropdown\', \''.$category.'\');doGridSearch(\'ilevel\', \'dropdown\', \''.($level+1).'\');doGridSearch(\'code\', \'search\', \''.strip_tags($username).'\', \'code\');doSearch();">'.App::formatNegativeAmount($profitloss->sumstake * $currencyRate).'</a>';
            }
            $vendor = '<a href="#" onclick="'.$search_vendor.'">'.$vendor.'</a>';
            $product = '<a href="#" onclick="'.$search_product.'">'.$product.'</a>';

            $percentage = 0;
            if($profitloss->sumstake > 0) $percentage = $profitloss->sumwinloss / $profitloss->sumstake * 100;
            $percentage *= -1;

            $rows[] = array(
                'currency' 		=> $profitloss->crccode,
                'member' 		=> $username,
                'vendor' 		=> $vendor,
                'product' 		=> $product,
                'wager' 		=> App::displayAmount($profitloss->sumwager, 0),
                'stake' 		=> $stake,
                'stake_amount'  => App::formatNegativeAmount($profitloss->sumstake * $currencyRate),
                'validstake' 	=> App::formatNegativeAmount($profitloss->sumvalid * $currencyRate),
                'winloss' 		=> App::formatNegativeAmount($profitloss->sumwinloss * $currencyRate),
                'company' 		=> App::formatNegativeAmount($profitloss->sumwinloss * -1 * $currencyRate),
                'ratio2' 		=> App::displayPercentage($percentage, 2, true),
                'commission'	=> App::formatNegativeAmount($commission), // Already included currency rate.
            );

            if(!isset($total_report[$profitloss->crccode])) {
                $total_report[$profitloss->crccode] = array(
                    'wager' 		=> 0,
                    'validstake' 	=> 0,
                    'stake' 		=> 0,
                    'winloss' 		=> 0,
                    'commission' 	=> 0,
                );
            }

            $total_report[$profitloss->crccode]['wager'] 	  += $profitloss->sumwager * $currencyRate;
            $total_report[$profitloss->crccode]['validstake'] += $profitloss->sumvalid * $currencyRate;
            $total_report[$profitloss->crccode]['stake']	  += $profitloss->sumstake * $currencyRate;
            $total_report[$profitloss->crccode]['winloss']	  += $profitloss->sumwinloss * $currencyRate;
            $total_report[$profitloss->crccode]['commission'] += $commission; // Already included currency rate.

        }



        if(count($total_report) > 0) {
            foreach($total_report as $crccode => $report) {
                $percentage = 0;
                if($report['stake'] > 0) $percentage = $report['winloss'] / $report['stake'] * 100;
                $percentage *= -1;

                $footer[] = array(
                    'member' 	 => Lang::get('COMMON.TOTAL'),
                    'currency'   => $crccode,
                    'wager' 	 => App::displayAmount($report['wager'], 0),
                    'validstake' => App::formatNegativeAmount($report['validstake']),
                    'stake' 	 => App::formatNegativeAmount($report['stake']),
                    'winloss' 	 => App::formatNegativeAmount($report['winloss']),
                    'company' 	 => App::formatNegativeAmount($report['winloss'] * -1),
                    'ratio2' 	 => App::displayPercentage($percentage, 2, true),
                    'ratio1' 	 => App::displayPercentage(100),
                    'commission' => App::formatNegativeAmount($report['commission']),
                );
            }

            foreach($rows as $key => $row) {
                $ratio1 = 0;
                if($total_report[$row['currency']]['stake'] > 0)
                    $ratio1 = $row['stake_amount'] / $total_report[$row['currency']]['stake'] * 100;
                $rows[$key]['ratio1'] = App::displayPercentage($ratio1);
            }
        }

        echo json_encode(array('total' => $total, 'rows' => $rows, 'footer' => $footer));
        exit;

    }

	public function showdetail (Request $request)
	{
		$grid = new PanelGrid;
		$grid->setupGrid($this->moduleName, 'showdetail-data', $this->limit, true, array('params' => array('prdid' => $request->iprdid , 'createdfrom' => $request->createdfrom, 'createdto' => $request->createdto, 'accid' => $request->iaccid)));
		$grid->setTitle(Lang::get('COMMON.REPORT'));

		$level    = $request->has('level') ? $request->input('level'):Agent::LEVEL_SHAREHOLDER;
		$levelstr = Agent::getLevelText($level);

		$grid->addColumn('date', 			Lang::get('COMMON.TIME'), 					150,	array('align' => 'center'));
		$grid->addColumn('member', 			Lang::get('COMMON.MEMBER'), 				80,		array('align' => 'left'));
		$grid->addColumn('vendor', 			Lang::get('COMMON.VENDOR'), 				80,		array('align' => 'center'));
		$grid->addColumn('product', 		Lang::get('COMMON.PRODUCT'), 				80,		array('align' => 'left'));
		$grid->addColumn('detail', 			Lang::get('COMMON.DETAILS'), 				200,	array());
		$grid->addColumn('currency', 		Lang::get('COMMON.CURRENCY'), 				60,		array('align' => 'center'));
		$grid->addColumn('stake', 			Lang::get('COMMON.STAKE'), 					70,		array('align' => 'right'));
		$grid->addColumn('winloss', 		Lang::get('COMMON.PROFITLOSS'), 			70,		array('align' => 'right'));
		$grid->addColumn('valid', 			Lang::get('COMMON.VALIDSTAKE'), 			70,		array('align' => 'right'));
		$grid->addColumn('result', 			Lang::get('COMMON.RESULT'), 				70,		array('align' => 'center'));

		/* $details = array(
			'mdl' => $this->moduleName,
			'action' => 'betdetaildata',
			'columns' => array(
				array('index' => 'game', 'name' => App::getText('CBO_GUI_GAME'), 'width' => 100 ),
				array('index' => 'match', 'name' => App::getText('CBO_GUI_MATCH'), 'width' => 100 ),
				array('index' => 'selection', 'name' => App::getText('CBO_GUI_SELECTION'), 'width' => 100 ),
				array('index' => 'result', 'name' => App::getText('CBO_GUI_RESULT'), 'width' => 100 ),
			),
		); */
		
		$data['grid'] = $grid->getTemplateVars();

		return view('affiliate.grid2',$data);
		
	}
	
	public function showdetailpaymentdoGetData(Request $request){

		$from 		= $request->has('createdfrom')?$request->get('createdfrom'):App::getDateTime(11);
		$to   		= $request->has('createdto')?$request->get('createdto'):App::getDateTime(3);
		if($request->get('prdid') == '0' ){
			$condition  = 'datetime >= "'.$from.' 00:00:00" AND datetime <= "'.$to.' 23:59:59" AND accid = '.$request->get('accid').'';
		}else{
			$condition  = 'datetime >= "'.$from.' 00:00:00" AND datetime <= "'.$to.' 23:59:59" AND prdid = '.$request->get('prdid').' AND accid = '.$request->get('accid').'';
		}
		

		$total = Wager::whereRaw($condition)->count();

		$rows = array();
		$footer = array();
		$totalstake = 0;
		$totalwinloss = 0;
		$totalvalid = 0;
		//var_dump($total);
		if($betslips = Wager::whereraw($condition)->skip($request->recordstartindex)->take( $request->pagesize )->orderBy('id','desc')->get()) {
			
			$vendors = Product::getAllAsOptions();
			$products = Product::getCategoryOptions();
            $currencyRate = 1;

            if (Config::get('setting.front_path') == 'idnwin99') {
                $currencyRate = 1000;
            }

			foreach($betslips as $betslip) {
				//$winloss = $betslip->profitloss;
				$valid = $betslip->validstake;
				
				$rows[] = array(
					'id' => $betslip->id,
					'date' => $betslip->datetime,
					'member' => $betslip->nickname,
					'vendor' => $vendors[$betslip->prdid],
					'product' => $products[$betslip->category],
					'detail' => '#'.$betslip->category.'-'.$betslip->id.'-'.$betslip->refid,
					'currency' => $betslip->crccode,
					'stake' => App::formatNegativeAmount($betslip->stake * $currencyRate),
					'winloss' => App::formatNegativeAmount($betslip->profitloss * $currencyRate),
					'valid' => App::formatNegativeAmount($betslip->validstake * $currencyRate),
					'result' => $betslip->getResultText(),
				);
				$totalvalid += $betslip->validstake * $currencyRate;
				$totalstake += $betslip->stake * $currencyRate;
				$totalwinloss += $betslip->profitloss * $currencyRate;
				
			}
		}
		
		$footer[] = array(
			'currency' => Lang::get('COMMON.TOTAL'),
			'stake' => App::formatNegativeAmount($totalstake),
			'winloss' => App::formatNegativeAmount($totalwinloss),
			'valid' => App::formatNegativeAmount($totalvalid),
		);
		echo json_encode(array('total' => $total, 'rows' => $rows, 'footer' => $footer));
		exit;
		
	}
	

	
	protected function drawAddEdit(Request $request) { 

	    $toAdd 	  = true;
		$object   = null;
		$readOnly = false;
		$values   = array();

		$title = Lang::get('NEW').' '.Lang::get('MEMBERACCOUNT');

		if($request->has('sid')) 
		{
	
			if ($object = Account::find($request->input('sid'))) 
				{
					$title    = Lang::get('COMMON.EDIT').' '.Lang::get('COMMON.MEMBERACCOUNT');
					$toAdd    = false;
					$readOnly = true;

				} 
			else
				{
					$object = null;
				}
			
		}

		$form = App::setupPanelForm('doadd', $request->all(), array(), $toAdd);
		$form->setFormTitle($title);

		$form->addInput('text', 				Lang::get('COMMON.USERNAME') 		, 'username', 	 	(($object)? $object->username:''), 	array(), false); 
		$form->addInput('text', 				Lang::get('COMMON.PASSWORD') 		, 'username', 	 	(($object)? $object->password:''), 	array(), false); 
		//$form->addInput('text', 				Lang::get('COMMON.USERNAME') 		, 'username', 	 	(($object)? $object->username:''), 	array(), false); 
		$form->addInput('text', 				Lang::get('COMMON.MEMBERNAME') 		, 'username', 	 	(($object)? $object->fullname:''), 	array(), false); 
		$form->addInput('text', 				Lang::get('COMMON.CURRENCY') 		, 'username', 	 	(($object)? $object->crccode:''), 	array(), false); 
		$form->addInput('text', 				Lang::get('COMMON.EMAIL') 			, 'username', 	 	(($object)? $object->email:''), 	array(), false); 
		//$form->addInput('text', 				Lang::get('COMMON.TELEPHONENO') 	, 'username', 	 	(($object)? $object->username:''), 	array(), false); 

		
		$data['form'] = $form->getTemplateVars();
		$data['module'] = 'member';

		return view('affiliate.form2',$data);
	
	}
	
	protected function doAddEdit(Request $request) {
		
		$validator = Validator::make(
			[
	
				'name'	  	 	  => $request->input('name'),
				'account'	  	  => $request->input('account'),

			],
			[

			   'name'	   	  	   => 'required',
			   'account'	  	   => 'required',

			]
		);
		
		
		if ($validator->fails())
		{
			echo json_encode($validator->errors());
			exit;
		}
		
		
		
		$object  = new Bankaccount;
		
		if($request->has('sid')) 
		{
			$object = Bankaccount::find($request->input('sid'));
		}
		
		$object->bankaccname	 				= $request->input('name');
		$object->bankaccno	 			= $request->input('account');


		
		if( $object->save() )
		{
			echo json_encode( array( 'code' => Lang::get('COMMON.SUCESSFUL') ) );
			exit;
		}


		echo json_encode( array( 'code' => Lang::get('COMMON.FAILED') ) );
		
	}

	public function showAffiliateReport(Request $request)
	{

		$grid = new PanelGrid;
		$grid->setupGrid($this->moduleName, 'affiliateReport-data', $this->limit);
		$grid->setTitle(Lang::get('COMMON.REPORT'));

		$grid->addColumn('affiliate',			Lang::get('COMMON.AFFILIATE'), 			130,	array());
		$grid->addColumn('register',			Lang::get('COMMON.REGISTRATION'), 		130,	array('align' => 'right'));
		$grid->addColumn('betmember',			Lang::get('COMMON.BETMEMBER'), 			130,	array('align' => 'right'));
		$grid->addColumn('deposit',				Lang::get('COMMON.DEPOSIT'), 			130,	array('align' => 'right'));
		$grid->addColumn('withdrawal',			Lang::get('COMMON.WITHDRAWAL'), 		130,	array('align' => 'right'));
		$grid->addColumn('turnover',			Lang::get('COMMON.TURNOVER'), 			130,	array('align' => 'right'));
		$grid->addColumn('profitloss',			Lang::get('COMMON.PROFITLOSS'), 		130,	array('align' => 'right'));
		$grid->addColumn('bonus',				Lang::get('COMMON.BONUS'), 				130,	array('align' => 'right'));
		$grid->addColumn('incentive',			Lang::get('COMMON.INCENTIVE'), 			130,	array('align' => 'right'));
		$grid->addColumn('netpnl',				Lang::get('COMMON.NETPROFITLOSS'), 		130,	array('align' => 'right'));
		$grid->addColumn('rate',				Lang::get('COMMON.RATE'), 				130,	array('align' => 'right'));
		$grid->addColumn('commission',			Lang::get('COMMON.COMMISSION'), 		130,	array('align' => 'right'));

		
		$grid->addFilter('status',array('Suspended','Active'), array('display' => Lang::get('COMMON.TYPE')));
		//$grid->addButton('1', 'New', Lang::get('COMMON.ADDNEW').' '.Lang::get('COMMON.MEMBER'), 'button', array('icon' => 'add', 'url' => action('Affiliate\ProfitlossController@drawAddEdit')));
		
		$data['grid'] = $grid->getTemplateVars();
		return view('affiliate.grid2',$data);

	}
	
	public function showAffiliateReportdoGetData(Request $request)
	{

        $timefrom = $request->createdfrom.' 00:00:00';
        $timeto	  = $request->createdto.' 23:59:59';
        $datefrom = $request->createdfrom;
        $dateto   = $request->createdto;
 
 
		$condition = 'id = '.Session::get('affiliate_userid').' and status = 1';
		$total = Affiliate::whereRaw($condition)->count();
		$rows = array();
		
        if($affiliates = Affiliate::whereRaw($condition)->skip($request->recordstartindex)->take( $request->pagesize )->get()) {
			
			$total_register = 0;
			$total_betmember = 0;
			$total_deposit = 0;
			$total_withdrawal = 0;
			$total_bonus = 0;
			$total_incentive = 0;
			$total_turnover = 0;
			$total_profitloss = 0;
			$total_commission = 0;
			$total_rebate = 0;
			$total_fee = 0;
			$total_payout = 0;
			$total_netpnl = 0;

            $currencyRate = 1;

            if (Config::get('setting.front_path') == 'idnwin99') {
                $currencyRate = 1000;
            }
			
            foreach($affiliates as $affiliate) {
				
				$register = 0;
				$betmember = 0;
				$rate = 0;
				$deposit = 0;
				$withdrawal = 0;
				$bonus = 0;
				$incentive = 0;
				$turnover = 0;
				$profitloss = 0;
				$commission = 0;
				$rebate = 0;
				$fee = 0;
				$payout = 0;
				$netpnl = 0;
				$member_ids = array();

				if($members = $affiliate->getAllAccountById($affiliate->id, true)){
					foreach($members as $member) {
						$member_ids[] = $member->id;
						if($member->created >= $timefrom && $member->created <= $timeto) 
						{
							$register += 1;
						}
						if(Profitloss::whereRaw('accid='.$member->id.' AND date >= "'.$datefrom.'" AND date <= "'.$dateto.'"')->count() != 0 )
						{
							$betmember += 1;
						}
					}
				}
				
				if(count($member_ids) > 0) {
					$commission = 0;
					$rebate = 0;
					$deposit = Cashledger::whereRaw('chtcode = '.CBO_CHARTCODE_DEPOSIT.' AND accid IN ('.implode(',', $member_ids).') AND created >= "'.$timefrom.'" AND created <= "'.$timeto.'" AND status IN ('.CBO_LEDGERSTATUS_CONFIRMED.', '.CBO_LEDGERSTATUS_MANCONFIRMED.')')->sum('amount');
					$withdrawal = Cashledger::whereRaw('chtcode = '.CBO_CHARTCODE_WITHDRAWAL.' AND accid IN ('.implode(',', $member_ids).') AND created >= "'.$timefrom.'" AND created <= "'.$timeto.'" AND status IN ('.CBO_LEDGERSTATUS_CONFIRMED.', '.CBO_LEDGERSTATUS_MANCONFIRMED.')')->sum('amount');
					$bonus = Cashledger::whereRaw('chtcode = '.CBO_CHARTCODE_BONUS.' AND accid IN ('.implode(',', $member_ids).') AND created >= "'.$timefrom.'" AND created <= "'.$timeto.'" AND status IN ('.CBO_LEDGERSTATUS_CONFIRMED.', '.CBO_LEDGERSTATUS_MANCONFIRMED.')')->sum('amount');
					$incentive = Cashledger::whereRaw('chtcode = '.CBO_CHARTCODE_INCENTIVE.' AND accid IN ('.implode(',', $member_ids).') AND created >= "'.$timefrom.'" AND created <= "'.$timeto.'" AND status IN ('.CBO_LEDGERSTATUS_CONFIRMED.', '.CBO_LEDGERSTATUS_MANCONFIRMED.')')->sum('amount');
					$turnover = Profitloss::whereRaw('category NOT IN(6) AND accid IN ('.implode(',', $member_ids).') AND date >= "'.$datefrom.'" AND date <= "'.$dateto.'"')->sum('totalstake');
					$profitloss = Profitloss::whereRaw('category NOT IN(6) AND accid IN ('.implode(',', $member_ids).') AND date >= "'.$datefrom.'" AND date <= "'.$dateto.'"')->sum('amount');
					$netpnl = ($profitloss * $currencyRate) + ($bonus + $incentive);
					
				}
				
				//get commission scheme
				if($netpnl < 0) {
					$settings = array();
					$pnl = $netpnl * -1;
					if($tiers = Commissionsetting::whereRaw($affiliate->comid)->orderBy('rangefrom','ASC')->get()) {
						$balance = $pnl;
						$start = 0;
						foreach($tiers as $tier) {
							
							if( Config::get('setting.front_path') == 'sbm' && $affiliate->username == 'c')
								$percentage = 0;
							else
								$percentage = $tier->percentage;
							
							if($pnl > 0 && ($pnl >= $tier->rangefrom)) {
								$rate = $percentage;
								if($pnl > $tier->rangeto && $tier->rangeto > 0) {
									$commission += ($percentage * ($tier->rangeto - $start) / 100);
								} else
								$commission += ($percentage * ($pnl - $start) / 100);
								
								$start = $tier->rangeto;
							}
						}
					}
				}

                $rows[] = array(
//					'affiliate' => App::formatAddTabUrl(Lang::get('COMMON.AFFILIATEREPORT').': '.$affiliate->username, $affiliate->username, action('Affiliate\ProfitlossController@showMemberReport', array('aflid' => $affiliate->id, 'createdfrom' => $datefrom, 'createdto' => $dateto))),
					'affiliate' => $affiliate->username,
					'register' => $register,
					'betmember' => $betmember,
					'deposit' => App::displayAmount($deposit ),
					'withdrawal' => App::displayAmount($withdrawal ),
					'bonus' => App::formatNegativeAmount($bonus ),
					'incentive' => App::formatNegativeAmount($incentive ),
//					'turnover' => App::formatAddTabUrl(Lang::get('COMMON.AFFILIATEREPORT').': '.$affiliate->username, App::formatNegativeAmount($turnover), action('Affiliate\ProfitlossController@showMemberReport', array('iaflid' => $affiliate->id, 'createdfrom' => $datefrom, 'createdto' => $dateto))),
					'turnover' => App::formatNegativeAmount($turnover * $currencyRate),
					'profitloss' => App::formatNegativeAmount($profitloss * $currencyRate),
					'rate' => App::displayPercentage($rate),
					'commission' => App::formatNegativeAmount($commission), // Already included currency rate.
					'fee' => App::formatNegativeAmount($fee * $currencyRate),
					'payout' => App::formatNegativeAmount($payout ),
					'netpnl' => App::formatNegativeAmount($netpnl ),
				);				
            }
        }
		
		//var_dump($deposit);
		//exit();
		
		echo json_encode(array('total' => $total, 'rows' => $rows));
		exit;
		
	}
	
	public function showMemberReport(Request $request)
	{
		
		
	}
	
	
}
