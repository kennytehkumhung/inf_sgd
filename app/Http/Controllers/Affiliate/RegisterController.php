<?php namespace App\Http\Controllers\Affiliate;

//use App\Http\Controllers\Controller\Affiliate;
use App\libraries\App;
use Illuminate\Http\Request;
use Auth;
use Validator;
use Session;
use Lang;
use App\Models\Currency;
use App\Models\Affiliate;
use App\Models\Affiliateregister;
use Config;
use Response;
use Redirect;
use Hash;


class RegisterController extends Controller{
	
	public function __construct()
	{
		
	}

	public function index()
	{  
		if( Config::get('setting.front_path') == '369bet' ) {
			return view('affiliate.369betregister');
		} elseif ( Config::get('setting.opcode') == 'RYW' ) {
		    return view('affiliate.ryw_index', array('goto' => 'register'));
        } else {
			return view('affiliate.register');
		}
	}	
	
	public function process(Request $request)
	{       
		if(  Config::get('setting.front_path') == '369bet' ){
			$validator = Validator::make(
				[
					'surname'           => $request->input('surname'),
					'username'          => $request->input('username'),
					'password'          => $request->input('password'),
					'email'             => $request->input('email'),
					'telephone'         => $request->input('telephone'),
					'website'           => $request->input('website'),
					'websitedesc'       => $request->input('websitedesc'),
                                        //'code'              => $request->input('code'),
				],
				[
					'surname'	   => 'required|min:2',
					'username'	   => 'required|min:4|max:16|alpha_num|reserved_str',
					'password'	   => 'required|alpha_num|min:6',
					'email'	   	   => 'required|email',
					'telephone'	   => 'required|phone',
					'website'	   => 'required',
					'websitedesc'      => 'required',
                                        //'code'             => 'required|numeric',
				]
			);
		}
		else{
			$validator = Validator::make(
				[
					'username'  	=> $request->input('username'),
					'fullname'	 	=> $request->input('name'),
					'password'   	=> $request->input('password'),
					'email'   		=> $request->input('email'),
					'telephone'   	=> $request->input('telephone'),
					'website'   	=> $request->input('website'),
					'websitedesc'   => $request->input('websitedesc'),
				],
				[
					'username'	   => 'required|min:4|max:16|alpha_num|reserved_str',
					'fullname'	   => 'required|min:2',
					'password'	   => 'required|alpha_num|min:6',
					'email'	   	   => 'required|email',
					'telephone'	   => 'required|phone',
//					'website'	   => 'required',
//					'websitedesc'      => 'required',
				]
			);
		}
		
		
		if ($validator->fails())
		{
			echo json_encode($validator->errors());
			exit;
		}

		$count = Affiliateregister::where('username', '=', $request->username)
            ->orWhere('name', '=', $request->name)
            ->orWhere('email', '=', $request->email)
            ->count();

		if ($count > 0) {
		    echo json_encode([Lang::get('public.username_or_email_already_exists')]);
		    exit;
        }

		if(  Config::get('setting.front_path') == '369bet' ){
			$object = new Affiliateregister;
			$object->surname 	 = $request->surname;
                        //$object->dob             = $request->dob;
			$object->username    = $request->username;
			$object->password    = $request->password;
			$object->email 		 = $request->email;
			$object->telephone   = $request->telephone;
			$object->website 	 = $request->website;
			//$object->website2 	 = $request->website2;
			//$object->website3 	 = $request->website3;
			$object->websitedesc = $request->websitedesc;
                        //$object->agent      = $request->agent;
                        //$object->code       = $request->code;
			$object->createdip   = App::getRemoteIp();
		}
		else{
			$object = new Affiliateregister;
			$object->username    = $request->username;
			$object->name 		 = $request->name;
			$object->password    = $request->password;
			$object->email 		 = $request->email;
			$object->telephone   = $request->telephone;
			$object->website 	 = $request->has('website') ? $request->website : '';
			$object->websitedesc = $request->has('websitedesc') ? $request->websitedesc : '';
			$object->createdip   = App::getRemoteIp();
		}
		
		if($object->save())
		{
			echo json_encode( array( 'code' => Lang::get('COMMON.SUCESSFUL') ) );
		}
			
	}

	
	
}
