<?php namespace App\Http\Controllers\Affiliate;

use Illuminate\Foundation\Bus\DispatchesCommands;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;

abstract class AffiliateController extends BaseController {

	use DispatchesCommands, ValidatesRequests;

}
