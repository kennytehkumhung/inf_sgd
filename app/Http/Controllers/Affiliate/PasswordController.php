<?php namespace App\Http\Controllers\Affiliate;

//use App\Http\Controllers\Controller\Affiliate;
use App\libraries\App;
use Illuminate\Http\Request;
use Validator;
use Session;
use Lang;
use Config;
use Crypt;
use Hash;
use Auth;
use App\Models\Affiliate;

class PasswordController extends Controller{
	
	public function __construct()
	{
		
	}

	public function index()
	{  
		return view('affiliate.update_password');
	}	
	
	public function doAddEdit(Request $request)
	{       
		$validator = Validator::make(
			[
				'current_password'  => $request->input('current_password'),
				'new_password'      => $request->input('new_password'),
				'retype_password'   => $request->input('retype_password'),
			],
			[
				'current_password' => 'required',
				'new_password'     => 'required|alpha_num|min:6',
				'retype_password'  => 'required|alpha_num|min:6',
			]
		);
		
		if ($validator->fails())
		{
			echo json_encode($validator->errors());
			exit;
		}
		
		if ( !Hash::check($request->input('current_password'), Affiliate::where( 'id' , '=' , Session::get('affiliate_userid') )->pluck('password') ))
		{ 
			echo json_encode( array('current_password' => Lang::get('COMMON.OLDPASS_WRONG')));
			exit;
		}
	

			$object = Affiliate::where( 'id' , '=' , Session::get('affiliate_userid') )->first();
			$object->password      = Hash::make( $request->input('new_password') );
			$object->lastpwdchange = date('Y-m-d H:i:s');
	
			if($object->save())
			{
				echo json_encode( array( 'code' => Lang::get('COMMON.SUCESSFUL') ) );
			} 
		
		
		
	}		
	
	public function purchasePassword()
	{  
		return view('affiliate.update_purchase_password');
	}	
	
	public function doAddEditPurchasePassword(Request $request)
	{       
		$validator = Validator::make(
			[
				'password'  => $request->input('password'),
			],
			[
				'password'     => 'required|alpha_num|min:4',
			]
		);
		
		if ($validator->fails())
		{
			echo json_encode($validator->errors());
			exit;
		}
		

		$object = Affiliate::where( 'id' , '=' , Session::get('affiliate_userid') )->first();
		$object->password2      = Crypt::encrypt($request->input('password'));
	
		if($object->save())
		{
			echo json_encode( array( 'code' => Lang::get('COMMON.SUCESSFUL') ) );
		} 
		
		
		
	}		

	
	
}
