<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Account;
use App\libraries\providers\ALB;


class WelcomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Welcome Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{	
		//dd($foo->bar());
		$this->middleware('guest');
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		$phone = Account::where( 'id' , '=' , 29)->with('bankaccount')->get();
		
		foreach( $phone as $key => $value ){
			var_dump($value->bankaccount);
		}
		return view('welcome');
	}	
	
	public function test()
	{
		//echo ALB::MD5_KEY;
		$ALB = new ALB;
		echo $ALB->getLoginUrl('TEST01','','');
		//echo $ALB->deposit('TEST01','MYR',1000,'89833920000000000002');

	}

}
