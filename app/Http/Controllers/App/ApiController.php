<?php namespace App\Http\Controllers\App;

use App\Http\Controllers\Controller\User;
use App\Models\Account;
use App\Models\Accountdetail;
use App\Models\Affiliate;
use App\Models\Agent;
use App\Models\Website;
use App\Models\Configs;
use App\Models\Channel;
use App\Models\Currency;
use App\Models\Fundingmethodsetting;
use App\Models\Bankaccount;
use App\Models\Promocampaign;
use App\Models\Media;
use App\Models\Fundingmethod;
use App\Models\Bank;
use App\Models\Product;
use App\Models\Cashledger;
use App\Models\Article;
use App\Models\Banktransfer;
use App\Models\Promocash;
use App\Models\Accountbank;
use App\Models\Onlinetracker;
use App\Models\Onlinetrackerlog;
use App\Models\Banner;
use App\Models\Iptracker;
use App\Models\Betpsbresult;
use App\Models\Affiliateregister;
use App\Models\Accountproduct;
use App\Models\Wager;
use App\Models\Balancetransfer;
use App\Models\Productwallet;
use App\Models\Smslog;
use Illuminate\Http\Request;
use App\libraries\Login;
use App\libraries\App;
use App\libraries\PushNotifications;
use App\libraries\providers\PSB;
use App\libraries\providers\PLTB;
use App\libraries\providers\PLS;
use App\libraries\providers\SPG;
use App\libraries\providers\WFT;
use App\libraries\providers\VGS;
use App\libraries\providers\HOG;
use App\libraries\providers\AGG;
use App\Models\Betspgcategory;
use App\libraries\sms\SMS88;
use Carbon\Carbon;
use Validator;
use Hash;
use Cache;
use Config;
use Lang;
use Crypt;
use Session;
use GeoIP;
use Auth;
use Mail;
use DB;
use Storage;
use File;
use Log;


class ApiController extends Controller {
	
	private $opcode = false;
	private $location = array();
	private $currency = 'MYR';
	private $website = false;
	private $islogin = false;
	private $ips = array();
	private $return = [ 'success' => 0 ];
	private $user = false;
	
	public function __construct(Request $request)
	{
		//Log::info($request);
		$this->opcode = Config::get('setting.opcode');
		
		$this->location = GeoIP::getLocation($request->client_ip);
	
		$this->website = Website::whereCode($this->opcode)->first();
		
		if($this->location['isoCode'] == 'ID') {
			$this->currency = 'IDR';
		} else if($this->location['isoCode'] == 'VN') {
			$this->currency = 'VND';
		} else if($this->location['isoCode'] == 'TH') {
			$this->currency = 'THB';
		}
		
        $this->ips = Config::get($this->currency.'.mobile.ips');
	}
	
	public function verifyAccess($request, $op) {
	 	/* if(!in_array($request->ip(), $this->ips)) {
			$this->return['errors'] = ['access' => 'Access denied'];
			return false;
		} */
		
	/*   	$key = md5($this->opcode.Config::get('setting.appkey').$op.$request->client_ip);
		if($request->header('api-key') !== $key) {
			$this->return['errors'] = ['access' => 'Access denied'];
			return false;
		}   */  
		 
		if($this->islogin) {
			$valid = false;
			if($tracker = Onlinetracker::whereToken($request->token)->first()) {
				$this->user = Account::find($tracker->userid);
				//check expiry
				$valid = true;

				Session::put('userid',  		$this->user->id );
				Session::put('acccode',  		$this->user->code );
				Session::put('username', 		$this->user->nickname );
				Session::put('fullname', 		$this->user->fullname );
				Session::put('email', 	    	$this->user->email );
				Session::put('enpassword', 	    $this->user->enpassword );
				Session::put('agtid', 		    $this->user->agtid );
				Session::put('user_currency',   $this->user->crccode );
				Session::put('currency',  	    $this->user->crccode );
				Session::put('withdrawlimit',   $this->user->withdrawlimit );
				
			}
			
			if(!$valid) {
				$this->return['errors'] = ['session' => 'Session expired'];
				return false;
			}
			
		} 
		
		return true;
	}
	
	public function postSendRegistrationSms(Request $request) {

	
        // SMS config.
        $smsProvider = 'sms88';
        $smsMethod = 'regcode';

        $error = Lang::get('validation.regex', array('attribute' => Lang::get('public.ContactNumber')));
        $telmobile = $request->tel_mobile;

        // Check contact number format.
        if (preg_match('/\d{9,}/', $telmobile) == 1) {
            // Check if number is in use.
            $obj = Accountdetail::where('telmobile', '=', $telmobile)->first();
            $smsObj = new SMS88();

            if ($obj) {
                $error = Lang::get('public.ContactNumberIsAlreadyInUse');
            } else {
                // Add country code to tel number.
                $telmobile = $smsObj->formatTelMYR($telmobile);

                // Check IP spam.
                $requestCount = Smslog::where('sender_ip', '=', $request->ip())
                    ->where('created_at', '>=', Carbon::now()->addMinutes(-60)->toDateTimeString())
                    ->where('method', '=', $smsMethod)
//                    ->where('ref_status', '=', '0') // Strict only 3 SMS within # seconds
                    ->count();

                if ($requestCount >= 3) {
                    // IP has requested # of times on last minutes, consider as spam.
                    $error = Lang::get('public.PleaseWaitForXSecondsToResendSMS', array('p1' => 60));
                } else {
                    // IP is safe.
                    // Check latest user request for verify code.
                    $smslogObj = Smslog::where('tel_mobile', '=', $telmobile)
                        ->where('method', '=', $smsMethod)
                        ->where('ref_status', '=', '0')
                        ->orderBy('created_at', 'desc')
                        ->first();

                    if ($smslogObj) {
                        // Has record found, check if send within period to prevent SMS spam.
                        $sendDt = Carbon::parse($smslogObj->created_at);
                        $resendSecLimit = 60;
                        $sentSecInterval = $sendDt->diffInSeconds(Carbon::now());

                        if ($sentSecInterval >= $resendSecLimit) {
                            // Is ok to send SMS.
                            $error = '';
                        } else {
                            $error = Lang::get('public.PleaseWaitForXSecondsToResendSMS', array('p1' => ($resendSecLimit - $sentSecInterval)));
                        }
                    } else {
                        // No record found, is ok to send SMS.
                        $error = '';
                    }
                }
            }

            if ($error == '') {
                // No error, log request and send SMS.
                $vchars = '23456789ABCDEFGHJKLMNPQRTUVWXY';
                $vcharsCount = strlen($vchars) - 1;
                $smsRefCode = '';

                for ($i = 0; $i < 6; $i++) {
                    $smsRefCode .= $vchars[mt_rand(0, $vcharsCount)];
                }

                $smsRequest = $smsObj->getShortCurrency(Session::get('currency')) . '0.00 ' .
                    $smsObj->getOperatorName(Config::get('setting.opcode'), '', ': ') .
                    'Your verification code: ' . $smsRefCode;

                $smslogObj = Smslog::quickCreate(0, '', $request->ip(), $smsProvider, $smsMethod, $telmobile, $smsRequest, '', $smsRefCode, 0);
                $response = '';

                try {
                    $response = $smsObj->sms($smsRequest, $telmobile);
                } catch (\Exception $ex) {
                    // Nothing.
                }

                $smslogObj->response = $response;
                $smslogObj->save();

                $this->return['success'] = 1;
				return response()->json($this->return);
            }
        }

		$this->return['errors'] = $error;
		return response()->json($this->return);
    }
	
	public function postRegister(Request $request)
	{
		$verified = $this->verifyAccess($request, 'register');
		if(!$verified) {
			return response()->json($this->return);
		}
		
		$validator = Validator::make(
			[
	
				'username' => $request->input('username'),
				'email'	   => $request->input('email'),
				'password' => $request->input('password'),
				'fullname' => $request->input('fullname'),
				'mobile'   => $request->input('mobile'),
			],
			[

				'username' => 'required|min:6|max:16|alpha_num|reserved_str|unique:account,nickname',
				'email'    => 'required|email|unique:accountdetail,email',
				'password' => 'required|alpha_num|min:6',
				'fullname' => 'required|min:2',
				'mobile'   => 'required|phone|unique:accountdetail,telmobile',
			]
		);
		
		if ($validator->fails())
		{
			$this->return['errors'] = $validator->errors();
			return response()->json($this->return);
		}
		
		$isPhoneValid = 0;
        $smslogObj = null;

        if ($request->has('mobile_vcode')) {
            // App\Http\Middleware\Register.php will detect if project is using r_mobile_vcode
            // Here just simply check if request has r_mobile_cvode then only verify the code.
            $smslogObj = Smslog::where('ref_code', '=', trim($request->input('mobile_vcode')))
                ->where('tel_mobile', 'like', '%' . $request->input('mobile'))
                ->where('created_at', '>=', Carbon::now()->addMinutes(-30)->toDateTimeString()) // Each verify code available for 30 minutes.
                ->where('method', '=', 'regcode')
                ->where('ref_status', '=', 0)
                ->first();

            if ($smslogObj) {
                // Code is found.
                $smslogObj->ref_status = 1;
                $isPhoneValid = 1;
            } else {
                // Code not found or expired.

				$this->return['errors'] = array('mobile_vcode' => Lang::get('public.InvalidVerificationCode'));
				return response()->json($this->return);
            }
        }
		

		$magtcode = $this->website->code.'_'.$this->currency.'_COM';
		$agtcode  = $magtcode.'_'.date('ym');
		$channel  = 'mobile';
	
		if( Channel::whereName($channel)->count() == 0 ) {
			$chnObj = new Channel;
			$chnObj->name = $channel;
			$chnObj->save();
		}
		
		$agtObj = Agent::where( 'code' , '=' , $agtcode )->first();

		if( $agtObj == false ) 
		{
			
			$parentid = 0;
			if( $magtObj = Agent::where( 'code' , '=' , $magtcode )->first() )
			$parentid 			= $magtObj->id;
		
			$agtObj  = new Agent;
			$agtObj->code 		= $agtcode;
			$agtObj->parentid   = $parentid;
			$agtObj->aflid 		= 0;
			$agtObj->wbsid 		= $this->website->id;
			$agtObj->bhdid 		= $magtObj->bhdid;
			$agtObj->level 		= Agent::LEVEL_AGENT;
			$agtObj->crccode 	= strtoupper($this->currency);
			$agtObj->isexternal = 0;
			$agtObj->status 	= CBO_AFFILIATESTATUS_ACTIVE;
			$agtObj->save();
			
		}
		
		$accObj = new Account;
		$acdObj = new Accountdetail;
		
		$chnObj = Channel::where( 'name' , '=' , $channel )->first();

		$accObj->regchannel    = $chnObj->id;
		$accObj->nickname      = strtolower( $request->input('username') );
		$accObj->code	       = strtolower( $this->website->memberprefix . '_' . $request->input('username') );
		$accObj->fullname      = $request->input('fullname');
		$accObj->password      = Hash::make( $request->input('password') );
		$accObj->enpassword    = Crypt::encrypt( $request->input('password') );
		$accObj->crccode       = $this->currency;
		$accObj->term  	       = CBO_ACCOUNTTERM_CASH;
		$accObj->timezone      = Configs::getParam("SYSTEM_TIMEZONE");
		$accObj->createdby     = 0;
		$accObj->islogin   	   = 1;
		$accObj->isphonevalid  = $isPhoneValid;
		$accObj->withdrawlimit = 1;
		$accObj->lastpwdchange = date('Y-m-d H:i:s');
		$accObj->status 	   = CBO_ACCOUNTSTATUS_ACTIVE;
		$accObj->aflid 		   = $agtObj->aflid;
		$accObj->agtid 		   = $agtObj->id;
		$accObj->wbsid 		   = $this->website->id;
		
		
		if($accObj->save()) 
		{
			if ($smslogObj && $isPhoneValid == 1) {
                $smslogObj->save();
            }
			
			$acdObj->fullname 	   = $request->input('fullname');
			$acdObj->email 		   = $request->input('email');
			$acdObj->telmobile 	   = $request->input('mobile');
			$acdObj->dob		   = ($request->has('dob')) ? $request->input('dob') : '';
			$acdObj->gender		   = ($request->has('gender')) ? $request->input('gender') : '';
			$acdObj->createdip	   = $request->client_ip;
			$acdObj->id 		   = $accObj->id;
			$acdObj->accid 		   = $accObj->id;
			$acdObj->wbsid 		   = $accObj->wbsid;
			$acdObj->acccode 	   = $accObj->code;
			$acdObj->referenceno   = 1000 + $accObj->id;
			$acdObj->save();
		} 
		
		$this->return['success'] = 1;
		return response()->json($this->return);
	}	
	
	public function postLogin(Request $request)
	{
		$verified = $this->verifyAccess($request, 'login');
		if(!$verified) {
			return response()->json($this->return);
		}

		$validator = Validator::make(
			[
				'username' => $request->input('username'),
				'password' => $request->input('password'),
			],
			[
				'username'   => 'required|alpha_num|min:6',
				'password'   => 'required|alpha_num|min:6',
			]
		);

		if ($validator->fails())
		{
			$this->return['errors'] = $validator->errors();
			return response()->json($this->return);
		}
		
		$accountObj = Account::whereNickname($request->username)->whereCrccode($this->currency)->first();
		
		if( !isset($accountObj->crccode) ){
			
			$this->return['errors'] = ['code' => Lang::get('validation.login_fail')];
			return response()->json($this->return);
		}

		if( $this->currency != $accountObj->crccode  )
		{
			$this->return['errors'] = ['code' => Lang::get('validation.login_fail')];
			return response()->json($this->return);
		}
		
		$agentCode = Agent::where( 'id' , '=' , $accountObj->agtid )->pluck('code');
		
		if( $this->opcode != substr($agentCode, 0 , 3) )
		{
			$this->return['errors'] = ['code' => Lang::get('validation.login_fail')];
			return response()->json($this->return);
		}
		
		if( $accountObj->islogin == 0 && $accountObj->oldpassword == App::getEncryptPassword($request->input('password')) ){
			$accountObj->password      = Hash::make( $request->input('password') );
			$accountObj->enpassword    = Crypt::encrypt( $request->input('password') );
			$accountObj->islogin   	   = 1;
			$accountObj->save();
		}
		
		if( $accountObj->enpassword == '' ){
			$accountObj->enpassword    = Crypt::encrypt( $request->input('password') );
			$accountObj->save();
		}
		
		if( $request->has('gcm_id') )
		{
			$accountObj->gcmid    = $request->input('gcm_id');
			$accountObj->save();
		}
		

		
		if (Auth::user()->attempt(array('nickname' => $request->input('username'), 'password' => $request->input('password') , 'status' => 1 , 'istestacc' => 0 , 'istestacc' => 0, 'crccode' => $this->currency)))
        {	
			$login = new Login;
			$user = Auth::user()->get();
			$login->doMakeTracker(Auth::user()->get(), 'Account','', $request->client_ip);
			
			$this->return['success'] = 1;
			$this->return['token'] = Session::get('token');
			
            return response()->json($this->return);
			
        }else
		{	
			$this->return['errors'] = ['code' => Lang::get('validation.login_fail')];
			return response()->json($this->return);
		}
	}
	
	public function postLogout(Request $request) {
		$this->islogin = true;
		$verified = $this->verifyAccess($request, 'logout');
		if(!$verified) {
			return response()->json($this->return);
		}
		
		Onlinetracker::whereType(CBO_LOGINTYPE_USER)->whereUserid($this->user->id)->delete();
		$this->return['success'] = 1;
        return response()->json($this->return);
	}
	
	public function postChangePassword(Request $request){
		
		$this->islogin = true;
		$verified = $this->verifyAccess($request, 'change-password');
		if(!$verified) {
			return response()->json($this->return);
		}
		
		$validator = Validator::make($request->all(),
			[
				'oldpass'   => 'required|alpha_num|min:6',
				'newpass'   => 'required|alpha_num|min:6',

			]
		);
		
		if ($validator->fails())
		{
			$this->return['errors'] = [$validator->errors()];
			return response()->json($this->return);
		}
		

		if ( !Hash::check($request->input('oldpass'), $this->user->password))
		{ 
			$this->return['errors'] = ['oldpass' => Lang::get('validation.oldpass_wrong')];
			return response()->json($this->return);
		}
	

			$acdObj = $this->user;
			$acdObj->password      = Hash::make( $request->input('newpass') );
			$acdObj->enpassword    = Crypt::encrypt( $request->input('newpass') );
			
			if($acdObj->save())
			{
				$this->return['success'] = 1;
				return response()->json($this->return);
			}
	} 	
	
	public function postMemberInfo(Request $request) {
		$this->islogin = true;
		$verified = $this->verifyAccess($request, 'info');
		if(!$verified) {
			return response()->json($this->return);
		}
		
		$condition = '((status in ('.CBO_LEDGERSTATUS_PENDING.', '.CBO_LEDGERSTATUS_MANPENDING.','.CBO_LEDGERSTATUS_CONFIRMED.', '.CBO_LEDGERSTATUS_MANCONFIRMED.', '.CBO_LEDGERSTATUS_PROCESSED.') AND chtcode IN ('.CBO_CHARTCODE_WITHDRAWAL.', '.CBO_CHARTCODE_BALANCETRANSFER.')) OR (status in ('.CBO_LEDGERSTATUS_CONFIRMED.', '.CBO_LEDGERSTATUS_MANCONFIRMED.') AND chtcode NOT IN ('.CBO_CHARTCODE_WITHDRAWAL.', '.CBO_CHARTCODE_BALANCETRANSFER.')))';


		$condition .= ' AND accid = '. $this->user->id;
		
		$balance = Cashledger::whereRaw($condition)->sum('amount');

		
		$balance = number_format($balance, 2, '.', '');
		
		if($acdObj = Accountdetail::whereAccid($this->user->id)->first()) {
			$this->return['success'] = 1;
			$this->return['detail'] = [
				'username' => $this->user->nickname,
				'fullname' => $acdObj->fullname,
				'email' => $acdObj->email,
				'mobile' => $acdObj->telmobile,
				'dob' => $acdObj->dob,
				'gender' => $acdObj->gender,
				'balance' => $balance,
			];
			
			return response()->json($this->return);
		}
		
		
	}
	
	public function postCheckUsername(Request $request) {
		
		$verified = $this->verifyAccess($request, 'check-username');
		if(!$verified) {
			return response()->json($this->return);
		}
		
		$validator = Validator::make(
			[
				'username' => $request->input('username'),
			],
			[
				'username' => 'required|min:6|max:16|alpha_num|reserved_str|unique:account,nickname',
			]
		);
		
		if ($validator->fails())
		{
			$this->return['errors'] = $validator->errors();
			return response()->json($this->return);
		} else {
			$this->return['success'] = 1;
			return response()->json($this->return);
		}
		
		
	}
	
	public function postUpdateProfile(Request $request) {
		
		$this->islogin = true;
		$verified = $this->verifyAccess($request, 'update-profile');
		if(!$verified) {
			return response()->json($this->return);
		}
		
		$this->return['success'] = 1;
		return response()->json($this->return);
		
	}
	
	public function postStatus(Request $request) {
		
		$this->islogin = true;
		$verified = $this->verifyAccess($request, 'status');
		if(!$verified) {
			return response()->json($this->return);
		} else {
			$this->return['success'] = 1;
			$this->return['status']  = 1;
			return response()->json($this->return);
		}
	}
	
	public function postDeposit(Request $request) {
		
		//Log::info($request);
		
		$this->islogin = true;
		$verified = $this->verifyAccess($request, 'deposit');
		if(!$verified) {
			return response()->json($this->return);
		}
		
		$validator = Validator::make(
			[
	
				'bank'	   => $request->input('bank'),
				'bonus'	   => $request->input('bonus'),
				'amount'   => $request->input('amount'),
				'refno'    => $request->input('refno'),
				'channel'  => $request->input('channel'),
				'datetime' => $request->input('datetime'),
			],
			[

				'bank' 	   => 'required|numeric',
				'bonus'    => 'required|alpha_num',
				'amount'   => 'required|decimal|min:1',
				'refno'    => 'required|alpha_num',
				'channel'  => 'required|numeric',
				'datetime' => 'required',
			]
		);
		
		if ($validator->fails())
		{
			$this->return['errors'] = $validator->errors();
			return response()->json($this->return);
		} 
		
		$bankaccountObj = Bankaccount::find($request->bank);
		
		$hasbonus = false;

		if( $bnkObj = Bank::find($bankaccountObj->bnkid) ) {
				$mindeposit = $bnkObj->mindeposit;
				$maxdeposit = $bnkObj->maxdeposit;
		}
		if( $request->has('bonus') && $request->bonus != '0' ) {
			$promos = Promocampaign::getPromoByMemberId(Session::get('userid'));
			if($promoObj = Promocampaign::whereRaw('startdate <= "'.date('Y-m-d H:i:s').'" AND enddate >= "'.date('Y-m-d H:i:s').'" AND status = '.CBO_STANDARDSTATUS_ACTIVE.' AND code = "'.$request->bonus.'"')->first()){
				if(isset($promos[$promoObj->id])) {
					$mindeposit = $promoObj->mindeposit;
					$hasbonus = true;
				} else { 
					$this->return['errors'] = ['oldpass' => Lang::get('COMMON.FAILED')];
					return response()->json($this->return);
				}
			} else{ 
				$this->return['errors'] = ['oldpass' => Lang::get('COMMON.FAILED')];
				return response()->json($this->return);
			}
		}
		
		
		if($request->amount < $mindeposit) { 
				$this->return['errors'] = ['oldpass' => Lang::get('public.BankMinDepositCriteria')];
				return response()->json($this->return);
		}
		if($request->amount > $maxdeposit) { 
				$this->return['errors'] = ['oldpass' => Lang::get('public.BankMaxExceeded')];
				return response()->json($this->return);
		}
		

		$fdmObj = Fundingmethod::find(Configs::getParam('SYSTEM_PAYMENT_ID_BANKTRANSFER'));
		
		$paymentObj 					= new Banktransfer;
		$paymentObj->fdmid 				= Configs::getParam('SYSTEM_PAYMENT_ID_BANKTRANSFER');
		$paymentObj->type 				= CBO_CHARTCODE_DEPOSIT;
		$paymentObj->bnkid 				= $bankaccountObj->bnkid;
		$paymentObj->accid 				= Session::get('userid');
		$paymentObj->acccode 			= Session::get('acccode');
		$paymentObj->crccode 			= Session::get('currency');
		$paymentObj->crcrate 			= Currency::getCurrencyRate($paymentObj->crccode);
		$paymentObj->amount 			= $request->amount;
		$paymentObj->amountlocal 		= Currency::getLocalAmount($request->amount, $paymentObj->crccode);
		$paymentObj->status 			= CBO_LEDGERSTATUS_PENDING;
		$paymentObj->createdip			= $request->client_ip;
		$paymentObj->receiveaccname 	= $bankaccountObj->bankaccname;
		$paymentObj->receiveacc   		= $request->bank;
		$paymentObj->receiveaccno   	= $bankaccountObj->bankaccno;
		$paymentObj->transferdatetime   = $request->input('datetime');
		$paymentObj->refno 			    = $request->refno;
		$paymentObj->transfertype 	    = $request->channel == '01' ? 1 : 2;
		if($paymentObj->save())
		{
		
			/* 	if( $request->hasFile('file'))
				{
					$file = $request->file('file');
			
					$filename = 'deposit/'.md5($file->getClientOriginalName().time()).'.'. $file->getClientOriginalExtension();
					$result = Storage::disk('s3')->put($filename,  File::get($file), 'public');


					if($result) {
								$medObj = new Media;
								$medObj->refobj = 'Banktransfer';
								$medObj->refid  = $paymentObj->id;
								$medObj->type   = Media::TYPE_IMAGE;
								$medObj->domain = 'http://'.Configs::getParam('SYSTEM_AWS_S3BUCKET');
								$medObj->path   = $filename;
								$medObj->save(); 
					}
				} */
		
				$cashLedgerObj = new Cashledger;
				$cashLedgerObj->accid 			= $paymentObj->accid;
				$cashLedgerObj->acccode 		= $paymentObj->acccode;
				$cashLedgerObj->accname			= Session::get('fullname');
				$cashLedgerObj->chtcode 		= CBO_CHARTCODE_DEPOSIT;
				$cashLedgerObj->cashbalance 	= self::getAllBalance();
				$cashLedgerObj->amount 			= $paymentObj->amount;
				$cashLedgerObj->amountlocal 	= $paymentObj->amountlocal;
				$cashLedgerObj->refid 			= $paymentObj->id;
				$cashLedgerObj->status 			= CBO_LEDGERSTATUS_PENDING;
				$cashLedgerObj->refobj 			= 'Banktransfer';
				$cashLedgerObj->crccode 		= $paymentObj->crccode;
				$cashLedgerObj->crcrate 		= Currency::getCurrencyRate($paymentObj->crccode);
				$cashLedgerObj->fdmid			= $fdmObj->id;
				$cashLedgerObj->bnkid			= $paymentObj->bnkid;
				$cashLedgerObj->fee 			= round($fdmObj->getFee(CBO_CHARTCODE_DEPOSIT, $paymentObj->amount), 2);
				$cashLedgerObj->feelocal 		= Currency::getLocalAmount($paymentObj->fee, $paymentObj->crccode);
				$cashLedgerObj->bacid			= $paymentObj->receiveacc;
				$cashLedgerObj->createdip		= $paymentObj->createdip;
				
				if($cashLedgerObj->save())
				{
					if($hasbonus){
						$promoCashObj = new Promocash;
						$promoCashObj->accid 	= $cashLedgerObj->accid;
						$promoCashObj->acccode  = $cashLedgerObj->acccode;
						$promoCashObj->chtcode  = CBO_CHARTCODE_DEPOSIT;
						$promoCashObj->pcpid 	= $promoObj->id;
						$promoCashObj->crccode  = $cashLedgerObj->crccode;
						$promoCashObj->type     = $promoObj->type;
						$promoCashObj->amount 	= $promoObj->getPromoCashAmount($cashLedgerObj->amount);
						$promoCashObj->amountlocal = Currency::getLocalAmount($promoCashObj->amount, $promoCashObj->crccode);
						$promoCashObj->crcrate = Currency::getCurrencyRate($promoCashObj->crccode);
						$promoCashObj->clgid = $cashLedgerObj->id;
						$promoCashObj->remarkcode = $promoObj->code;
						$promoCashObj->status = CBO_PROMOCASHSTATUS_CONFIRMED;
						$promoCashObj->createdip = App::getRemoteIp();;
						$promoCashObj->save();
						
					}
				}
			
		}
		
		$this->return['success'] = 1;
		return response()->json($this->return);
		
	}
	
	public function postWithdrawal(Request $request) {
		
		$this->islogin = true;
		$verified = $this->verifyAccess($request, 'withdrawal');
		if(!$verified) {
			return response()->json($this->return);
		}
		
		$validator = Validator::make(
			[
	
				'bank'	      => $request->input('bank'),
				'account_no'   => $request->input('account_no'),
				'amount'      => $request->input('amount'),
			],
			[

				'bank' 	      => 'required',
				'account_no'   => 'required|numeric',
				'amount'      => 'required|decimal|min:1',

			]
		);
		
		
		if ($validator->fails())
		{
			$this->return['errors'] = $validator->errors();
			return response()->json($this->return);
		} 
		
		
		if($request->input('amount') > self::mainwallet(false)) {
			$this->return['errors'] = ['oldpass' => Lang::get('COMMON.ERROR_NETAMOUNT')];
			return response()->json($this->return);
		}
		

		$success = false;



		if($request->input('amount') > 0) {

			$from = date('Y-m-d').' 00:00:00';
			$to   = date('Y-m-d').' 23:59:59';
			$withdrawl_count = Cashledger::whereRaw('accid = '.Session::get('userid').' AND chtcode = '.CBO_CHARTCODE_WITHDRAWAL.' AND created >= "'.$from.'" AND created <= "'.$to.'" AND status NOT IN ('.CBO_LEDGERSTATUS_CANCELLED.', '.CBO_LEDGERSTATUS_MANCANCELLED.', '.CBO_LEDGERSTATUS_MEMCANCELLED.')')->count();
			if($withdrawl_count >= Session::get('withdrawlimit')) {
				$this->return['errors'] = ['oldpass' => Lang::get('COMMON.ERROR_ALLOWWITHDRAWAL')];
				return response()->json($this->return);
			}
		}
		
		$bnkid = Bankaccount::whereId( $request->input('bank') )->pluck('bnkid');
		
		if($bnkObj = Bank::find($bnkid)) {
			
			if($request->input('amount') < $bnkObj->minwithdraw){
				$this->return['errors'] = ['oldpass' => Lang::get('COMMON.ERROR_MINWITHDRAWAL')];
				return response()->json($this->return);
			}
				
			if($request->input('amount') > $bnkObj->maxwithdraw){
				$this->return['errors'] = ['oldpass' => Lang::get('COMMON.MAXWITHDRAWAL')];
				return response()->json($this->return);
			}
		
			if(!$accBnkObj = Accountbank::whereRaw('bankid = '.$bnkObj->id.' AND accid = '. Session::get('userid').' AND bankaccno = '. $request->input('account_no'))->first()) {
				
				$accBnkObj = new Accountbank;
				$accBnkObj->accid 		= Session::get('userid');
				$accBnkObj->bankid 	    = $bnkObj->id;
				$accBnkObj->acccode	    = Session::get('acccode');
				$accBnkObj->bankaccname = Session::get('fullname');
				$accBnkObj->bankcode 	= $bnkObj->code;
				$accBnkObj->bankname 	= $bnkObj->name;
				$accBnkObj->bankaccno 	= $request->input('account_no');
				$accBnkObj->save();
			}
			
			$fdmObj = Fundingmethod::find(1);
	
			$paymentObj = new Banktransfer;
			$paymentObj->fdmid = $fdmObj->id;
			$paymentObj->type = CBO_CHARTCODE_WITHDRAWAL;
			$paymentObj->bnkid = $accBnkObj->bankid;
			$paymentObj->accid = Session::get('userid');
			$paymentObj->acccode = Session::get('acccode');
			$paymentObj->crccode = Session::get('currency');
			$paymentObj->crcrate = Currency::getCurrencyRate($paymentObj->crccode);
			$paymentObj->amount = -1 * $request->input('amount');
			$paymentObj->amountlocal = -1 * Currency::getLocalAmount($request->input('amount'), $paymentObj->crccode);
			$paymentObj->bankaccno = $accBnkObj->bankaccno;
			$paymentObj->bankaccname = $accBnkObj->bankaccname;
			$paymentObj->status = CBO_LEDGERSTATUS_PENDING;
			$paymentObj->createdip		= App::getRemoteIp();;
			
			if ($paymentObj->save()) {
				
				$cashLedgerObj = new Cashledger;
				$cashLedgerObj->accid 			= $paymentObj->accid;
				$cashLedgerObj->acccode 		= $paymentObj->acccode;
				$cashLedgerObj->accname			= Session::get('fullname');
				$cashLedgerObj->chtcode 		= CBO_CHARTCODE_WITHDRAWAL;
				$cashLedgerObj->cashbalance 	= self::getAllBalance();
				$cashLedgerObj->amount 			= $paymentObj->amount;
				$cashLedgerObj->amountlocal 	= $paymentObj->amountlocal;
				$cashLedgerObj->refid 			= $paymentObj->id;
				$cashLedgerObj->status 			= CBO_LEDGERSTATUS_PENDING;
				$cashLedgerObj->refobj 			= 'Banktransfer';
				$cashLedgerObj->crccode 		= $paymentObj->crccode;
				$cashLedgerObj->crcrate 		= Currency::getCurrencyRate($paymentObj->crccode);
				$cashLedgerObj->fdmid			= $fdmObj->id;
				$cashLedgerObj->bnkid			= $paymentObj->bnkid;
				$cashLedgerObj->fee 			= round($fdmObj->getFee(CBO_CHARTCODE_DEPOSIT, $paymentObj->amount), 2);
				$cashLedgerObj->feelocal 		= Currency::getLocalAmount($paymentObj->fee, $paymentObj->crccode);
				$cashLedgerObj->bacid			= 0;
				$cashLedgerObj->createdip		= $paymentObj->createdip;
				
				if($cashLedgerObj->save())
				{
					$success = true;

				}
			}
			
		}
		
		
		$this->return['success'] = 1;
		return response()->json($this->return);
	}
	
	public static function getAllBalance(){
		
		$total = 0;
		
		//$productObj = Product::select(['code','status'])->get();
		
		/* foreach( $productObj as $object )
		{
			$total += Cache::has(Session::get('username').'_'.strtoupper($object->code).'_balance') ? (float)Cache::get(Session::get('username').'_'.strtoupper($object->code).'_balance') : 0;
		} */
		
		if( $products = Product::whereStatus(1)->get() ){
			
			foreach( $products as $product ){
				
				$className  = "App\libraries\providers\\".$product->code;
				$obj 		= new $className;
				$temp       = $obj->getBalance( Session::get('username') , Session::get('currency') );
				
				$temp = ($temp == false) ? 0 : $temp;
                $temp = Session::get('currency') == 'VND' || Session::get('currency') == 'IDR' ? $temp * 1000 : $temp;
				
				$total +=  $temp ;
				
				
			}
		}
		
		$total += self::mainwallet(false);
		
		return $total;
	}
	
	public static function mainwallet($numberformat = true){
		
		$condition = '((status in ('.CBO_LEDGERSTATUS_PENDING.', '.CBO_LEDGERSTATUS_MANPENDING.','.CBO_LEDGERSTATUS_CONFIRMED.', '.CBO_LEDGERSTATUS_MANCONFIRMED.', '.CBO_LEDGERSTATUS_PROCESSED.') AND chtcode IN ('.CBO_CHARTCODE_WITHDRAWAL.', '.CBO_CHARTCODE_BALANCETRANSFER.')) OR (status in ('.CBO_LEDGERSTATUS_CONFIRMED.', '.CBO_LEDGERSTATUS_MANCONFIRMED.') AND chtcode NOT IN ('.CBO_CHARTCODE_WITHDRAWAL.', '.CBO_CHARTCODE_BALANCETRANSFER.')))';


		$condition .= ' AND accid = '. Session::get('userid');
		
		$balance = Cashledger::whereRaw($condition)->sum('amount');

		if( $numberformat == true){
			echo number_format($balance, 2, '.', '');
		}else{
			return $balance;
		}
		
	}
	
	
	public function postTransfer(Request $request) {
		
		
		$this->islogin = true;
		$verified = $this->verifyAccess($request, 'transfer');
		if(!$verified) {
			return response()->json($this->return);
		}
		
		
		$validator = Validator::make(
			[
				'amount'   => (int)$request->input('amount'),
			],
			[
				'amount'   => 'required|decimal|min:1',
			]
		);
		
		if ($validator->fails())
		{
			$this->return['errors'] = $validator->errors();
			return response()->json($this->return);
		}
		
		if($request->from_wallet == 'PLTB'){
			$request->from_wallet = 'PLT';
		}
		if($request->to_wallet == 'PLTB'){
			$request->to_wallet = 'PLT';
		}
		
		
		//Log::info($request->to_wallet);
		if( strtolower($request->from_wallet) != "main" && Product::where( 'status' , '=' , 2 )->where( 'code' , '=' , $request->from_wallet )->count() == 1 )
		{
			$this->return['errors'] = array( 'code' => $request->from_wallet.' '.Lang::get('public.WalletMaintenanceTryLater') );
			return response()->json($this->return);
		}	
		if( strtolower($request->to_wallet) != "main" && Product::where( 'status' , '=' , 2 )->where( 'code' , '=' , $request->to_wallet )->count() == 1 )
		{
			$this->return['errors'] = array( 'code' => $request->to_wallet.' '.Lang::get('public.WalletMaintenanceTryLater') );
			return response()->json($this->return);
		}
		
		if( (int)$request->input('amount') < 1 ){
			$this->return['errors'] = array( 'code' => Lang::get('TransferFailed'));
			return response()->json($this->return);
		}
		
		
		if( $request->from_wallet == $request->to_wallet )
		{
			$this->return['errors'] = array( 'code' => Lang::get('validation.wallet_error1') );
			return response()->json($this->return);
		}
		
		if( strtolower($request->from_wallet) == "main" )
		{
			$from_balance = self::mainwallet(false);
		}
		else
		{
			$className  	 = "App\libraries\providers\\".$request->from_wallet;
			$obj 			 = new $className;
			$from_balance    = $obj->getBalance( $this->user->nickname , $this->user->crccode );
			$from_balance 	 = Session::get('currency') == 'VND' || Session::get('currency') == 'IDR' ? $from_balance * 1000: $from_balance;
		}
				
	 	if( $from_balance < $request->amount )
		{
			$this->return['errors'] = array( 'code' => Lang::get('validation.wallet_error2') );
			return response()->json($this->return);
		} 
		
		$success = false;
		
		$from_created_result = true;
		
		//Log::info($request->from_wallet);

		if( strtolower($request->from_wallet) != "main" )
		{

			if( !Accountproduct::is_exist( Session::get('username') , $request->from_wallet ) )
			{
				$className  = "App\libraries\providers\\".$request->from_wallet;
				$obj 		= new $className;
				$from_created_result = $obj->createUser( array(
														'username' 	=> Session::get('username') , 
														'currency' 	=> Session::get('currency') , 
														'password' 	=> Session::get('enpassword') ,
														'email'    	=> Session::get('email') ,
														'firstname' => Session::get('fullname') ,
														'lastname'  => Session::get('fullname') ,
														'userid'    => Session::get('userid') 
														)
														);
			}	
		}
		
		$to_created_result = true;
		
		if( strtolower($request->to_wallet) != "main" )
		{

			if( !Accountproduct::is_exist( Session::get('username') , $request->to_wallet ) )
			{
				$className  = "App\libraries\providers\\".$request->to_wallet;
				$obj 		= new $className;
				$to_created_result = $obj->createUser( array(
														'username' 	=> Session::get('username') , 
														'currency' 	=> Session::get('currency') , 
														'password' 	=> Session::get('enpassword') ,
														'email'    	=> Session::get('email') ,
														'firstname' => Session::get('fullname') ,
														'lastname'  => Session::get('fullname') ,
														'userid'    => Session::get('userid')  
														)
													 );
			}
		}
		
		if( $from_created_result && $to_created_result )
		{
			
			$bltObj = new Balancetransfer;
			$bltObj->accid 			= Session::get('userid');
			$bltObj->acccode 		= Session::get('acccode');
			$bltObj->from 			= strtolower($request->from_wallet) == "main" ? 0 : Product::where( 'code' , '=' , $request->from_wallet)->pluck('id');
			$bltObj->to 			= strtolower($request->to_wallet) == "main"   ? 0 : Product::where( 'code' , '=' , $request->to_wallet)->pluck('id');
			$bltObj->crccode		= Session::get('currency');
			$bltObj->crcrate		= Currency::getCurrencyRate($bltObj->crccode);
			$bltObj->amount 		= $request->amount;
			$bltObj->amountlocal 	= Currency::getLocalAmount($bltObj->amount, $bltObj->crccode);
			$bltObj->status			= CBO_STANDARDSTATUS_SUSPENDED;
			
			if( $bltObj->save() ) 
			{
				if(strtolower($request->from_wallet) == "main" ) 
				{
					$cashLedgerObj = new Cashledger;
					$cashLedgerObj->accid 			= $bltObj->accid;
					$cashLedgerObj->acccode 		= $bltObj->acccode;
					$cashLedgerObj->accname			= Session::get('fullname');
					$cashLedgerObj->chtcode 		= CBO_CHARTCODE_BALANCETRANSFER;
					$cashLedgerObj->cashbalance 	= self::mainwallet(false);
					$cashLedgerObj->amount 			= $bltObj->amount * -1;
					$cashLedgerObj->amountlocal 	= $bltObj->amountlocal * -1;
					$cashLedgerObj->refid 			= $bltObj->id;
					$cashLedgerObj->status 			= $bltObj->status;
					$cashLedgerObj->crccode 		= Session::get('currency');
					$cashLedgerObj->refobj 			= 'Balancetransfer';
					
					if( $cashLedgerObj->save() )
					{
						$productwalletObj = new Productwallet;
						$productwalletObj->prdid 		= $bltObj->to;
						$productwalletObj->accid 		= $bltObj->accid;
						$productwalletObj->acccode 		= $bltObj->acccode;
						$productwalletObj->chtcode 		= CBO_CHARTCODE_BALANCETRANSFER;
						$productwalletObj->crccode 		= $bltObj->crccode;
						$productwalletObj->crcrate 		= $bltObj->crcrate;
						$productwalletObj->amount		= abs($bltObj->amount);
						$productwalletObj->amountlocal	= abs($bltObj->amountlocal);
						$productwalletObj->refobj		= 'Balancetransfer';
						$productwalletObj->refid		= $bltObj->id;
						
						if( $productwalletObj->save() )
						{
							$className  = "App\libraries\providers\\".$request->to_wallet;
							$providerObj = new $className;
							
							if( $providerObj->deposit( Session::get('username') , Session::get('currency') , $productwalletObj->amount , $productwalletObj->id ) )
							{
								$bltObj->status 		  = CBO_LEDGERSTATUS_CONFIRMED;
								$cashLedgerObj->status 	  = CBO_LEDGERSTATUS_CONFIRMED;
								$productwalletObj->status = CBO_LEDGERSTATUS_CONFIRMED;
								$success = true;	
							}
							else
							{
								//$bltObj->status			  = CBO_STANDARDSTATUS_SUSPENDED;
								//$cashLedgerObj->status 	  = CBO_LEDGERSTATUS_CANCELLED;
								//$productwalletObj->status = CBO_LEDGERSTATUS_CANCELLED;
								$success = false;
							}
							
								$bltObj->save();		  
								$cashLedgerObj->save();		  
								$productwalletObj->save();	
							
						}
						
					}
				}
				else
				{
					
					$productwalletObj = new Productwallet;
					$productwalletObj->prdid 		= $bltObj->from;
					$productwalletObj->accid 		= $bltObj->accid;
					$productwalletObj->acccode 		= $bltObj->acccode;
					$productwalletObj->chtcode 		= CBO_CHARTCODE_BALANCETRANSFER;
					$productwalletObj->crccode 		= $bltObj->crccode;
					$productwalletObj->crcrate 		= $bltObj->crcrate;
					$productwalletObj->amount		= abs($bltObj->amount) * -1;;
					$productwalletObj->amountlocal	= abs($bltObj->amountlocal) * -1;;
					$productwalletObj->refobj		= 'Balancetransfer';
					$productwalletObj->refid		= $bltObj->id;
					
					if( $productwalletObj->save() )
					{
						$className  = "App\libraries\providers\\".$request->from_wallet;
						$providerObj = new $className;
						
						if( $providerObj->withdraw( Session::get('username') , Session::get('currency') , $productwalletObj->amount * -1, $productwalletObj->id ) )
						{
							if( strtolower($request->to_wallet) == "main" ) 
							{
									$cashLedgerObj = new Cashledger;
									$cashLedgerObj->accid 			= $bltObj->accid;
									$cashLedgerObj->acccode 		= $bltObj->acccode;
									$cashLedgerObj->accname			= Session::get('fullname');
									$cashLedgerObj->chtcode 		= CBO_CHARTCODE_BALANCETRANSFER;
									$cashLedgerObj->cashbalance 	= self::mainwallet(false);
									$cashLedgerObj->amount 			= $bltObj->amount;
									$cashLedgerObj->amountlocal 	= $bltObj->amountlocal;
									$cashLedgerObj->refid 			= $bltObj->id;
									$cashLedgerObj->status 			= CBO_LEDGERSTATUS_CONFIRMED;
									$cashLedgerObj->refobj 			= 'Balancetransfer';
									$cashLedgerObj->crccode 		= Session::get('currency');
									if( $cashLedgerObj->save() )
									{
										$productwalletObj->status = CBO_LEDGERSTATUS_CONFIRMED;
										$bltObj->status     = CBO_LEDGERSTATUS_CONFIRMED;
										$productwalletObj->save();	
										$bltObj->save();		  	  
										$success = true;
									}
							} 
							else 
							{
								
								$productwalletObj2 = new Productwallet;
								$productwalletObj2->prdid 		= $bltObj->to;
								$productwalletObj2->accid 		= $bltObj->accid;
								$productwalletObj2->acccode 	= $bltObj->acccode;
								$productwalletObj2->chtcode 	= CBO_CHARTCODE_BALANCETRANSFER;
								$productwalletObj2->crccode 	= $bltObj->crccode;
								$productwalletObj2->crcrate 	= $bltObj->crcrate;
								$productwalletObj2->amount		= abs($bltObj->amount);
								$productwalletObj2->amountlocal	= abs($bltObj->amountlocal);
								$productwalletObj2->refobj		= 'Balancetransfer';
								$productwalletObj2->refid		= $bltObj->id;
								
								if( $productwalletObj2->save() )
								{
									$className  = "App\libraries\providers\\".$request->to_wallet;
									$providerObj = new $className;
									if( $providerObj->deposit( Session::get('username') , Session::get('currency') , $productwalletObj2->amount , $productwalletObj2->id ) )
									{
										$productwalletObj->status  = CBO_LEDGERSTATUS_CONFIRMED;
										$productwalletObj2->status = CBO_LEDGERSTATUS_CONFIRMED;
										$bltObj->status     	   = CBO_LEDGERSTATUS_CONFIRMED;
										$productwalletObj->save();	
										$productwalletObj2->save();	
										$bltObj->save();		
										$success = true;
									}
									else
									{
										//$className  = "App\libraries\providers\\".$request->get('from_wallet');
										//$providerObj = new $className;
										//$providerObj->deposit( Session::get('username') , Session::get('currency') , $productwalletObj->amount * -1, $productwalletObj2->id );
									}
								}
							}
						
						}
						else{
							$productwalletObj->status = CBO_LEDGERSTATUS_CANCELLED;
							$bltObj->status     	  = CBO_STANDARDSTATUS_SUSPENDED;
							$bltObj->save();		  	  
							$productwalletObj->save();	
							$success = false;
						}
						
					}

				}
			}
			
		}
		
		
		if( $success ){
				$this->return['success'] = 1;
				return response()->json($this->return);
		}

		$this->return['errors'] = array( 'code' => Lang::get('COMMON.FAILED') );
		return response()->json($this->return);
	
	}
	
	public function getBalance(Request $request) {
		
		$this->islogin = true;
		$verified = $this->verifyAccess($request, 'getbalance');
		if(!$verified) {
			return response()->json($this->return);
		}
		
		$condition = '((status in ('.CBO_LEDGERSTATUS_PENDING.', '.CBO_LEDGERSTATUS_MANPENDING.','.CBO_LEDGERSTATUS_CONFIRMED.', '.CBO_LEDGERSTATUS_MANCONFIRMED.', '.CBO_LEDGERSTATUS_PROCESSED.') AND chtcode IN ('.CBO_CHARTCODE_WITHDRAWAL.', '.CBO_CHARTCODE_BALANCETRANSFER.')) OR (status in ('.CBO_LEDGERSTATUS_CONFIRMED.', '.CBO_LEDGERSTATUS_MANCONFIRMED.') AND chtcode NOT IN ('.CBO_CHARTCODE_WITHDRAWAL.', '.CBO_CHARTCODE_BALANCETRANSFER.')))';


		$condition .= ' AND accid = '. $this->user->id;
		
		$balance = Cashledger::whereRaw($condition)->sum('amount');

		
		$balance = number_format($balance, 2, '.', '');
		
		$this->return['success'] = 1;
		$this->return['balance'] = $balance;
		return response()->json($this->return);
		
	}
	
	public function getBank(Request $request) {
		
		$this->islogin = true;
		$verified = $this->verifyAccess($request, 'bank');
		if(!$verified) {
			return response()->json($this->return);
		}
		
		$validator = Validator::make(
			[
				'type' => $request->input('type'),
			],
			[
				'type' => 'required|in:1,2',
			]
		);
		
		if ($validator->fails())
		{
			$this->return['errors'] = $validator->errors();
			return response()->json($this->return);
		} 
		
		$bhdid = 0;
		if($agtObj = Agent::find($this->user->agtid)) {
			$bhdid = $this->user->bhdid == 0 ? $agtObj->bhdid : $this->user->bhdid;
		}
		
		$condition = 'bhdid = '.$bhdid.' AND crccode="'.$this->user->crccode.'" AND bankaccount.status = '.CBO_STANDARDSTATUS_ACTIVE;
		
		if($request->type == 1)
		$condition .= ' AND deposit = 1';
		else if($request->type == 2)
		$condition .= ' AND withdraw = 1';

		$bankaccounts = Bankaccount::whereRaw($condition)->get();
		//Log::info($condition);
		$bankings = array();
		$count = 0;
		foreach($bankaccounts as $bankaccount){
			$bank = Bank::find( $bankaccount->bnkid);
			$media = Media::where( 'refobj' , '=' ,'BankObject' )->where( 'refid' , '=' , $bankaccount->bnkid)->first();
			$code = $bank->code;
		
			$bankings[$count]['bank'] = $bank->name;
			
			$bankings[$count]['bankaccno'] = $bankaccount->bankaccno;
			$bankings[$count]['bankaccname'] = $bankaccount->bankaccname;
			$bankings[$count]['image'] = $media->domain.'/'.$media->path;
			$bankings[$count]['min'] = $bank->mindeposit;
			$bankings[$count]['max'] = $bank->maxdeposit;
			$bankings[$count++]['bankid'] = $bankaccount->id;
		}
		
		$this->return['success'] = 1;
		$this->return['bank'] = $bankings;
		//Log::info(response()->json($this->return));
		return response()->json($this->return);
		
	}
	
	public function getBonus(Request $request) {
		
		$this->islogin = true;
		$verified = $this->verifyAccess($request, 'bonus');
		if(!$verified) {
			return response()->json($this->return);
		}
		
		$promos = Promocampaign::getPromoByMemberId( $this->user->id );
		
		$count = 0;
		
		$promotions = [];
		foreach( $promos as $id => $promo )
		{
			$code = $promo['code'];
			
			$promotions[$count]['code'] = $code;
			$promotions[$count]['image'] = '';
			$promotions[$count]['name'] = $promo['name'];
			if($media = Media::where( 'refobj' , '=' ,'PromoCampaign' )->where( 'refid' , '=' , $id)->first()){
				$promotions[$count]['image'] = $media->domain.'/'.$media->path;
			}
			$count++;
			
		}
		
		$promotions[$count]['code']  = '0';
		$promotions[$count]['image'] = '';
		$promotions[$count]['name']  = 'I do not want this fantastic promo ';
		
		$this->return['success'] = 1;
		$this->return['bonus'] = $promotions;
		return response()->json($this->return);
		
	}
	
	public function getBanner(Request $request) {
		
		$verified = $this->verifyAccess($request, 'banner');
		if(!$verified) {
			return response()->json($this->return);
		}
		
		$banners = Cache::rememberForever('Banner_'.Lang::getLocale().'_'.$this->currency, function() {
			return Banner::where('status','=',1)->where('crccode','=', $this->currency)->where('media.refobj','=','Banner')->where('lang','=',Lang::getLocale())->leftJoin('media', 'banner.id', '=', 'media.refid')->orderBy( 'rank' , 'asc')->get();
		});
		
		$list = [];
		foreach($banners as $banner) {
			if($banner->status == 1)
			$list[] = [
				'rank' => $banner->rank,
				'path' => $banner->domain.'/'.$banner->path,
				'url' => $banner->url,
			];
		}
		$this->return['success'] = 1;
		$this->return['banner'] = $list;
		return response()->json($this->return);
	}
	
	
	public function getPromotion(Request $request) {
		
		$verified = $this->verifyAccess($request, 'promotion');
		if(!$verified) {
			return response()->json($this->return);
		}
		
		$result = Cache::rememberForever('promotion_'.Lang::getLocale().'_'.$this->currency, function() {
			return Article::where('type','=',2)->where( 'crccode' , '=' , $this->currency )->where( 'lang' , '=' , Lang::getLocale())->where( 'status' , '=' , 1 )->orderBy( 'created' , 'desc')->get();
		});
		
		$list = [];
		foreach( $result as $key => $value ) {
			$list[$key]['rank']   = $value->rank;
			$list[$key]['title']   = $value->title;
			$list[$key]['content'] = $value->excerpt;
			$list[$key]['url'] = route('promo_content', [ 'id' => $value->id ]);
			$id = $value->id;
		
			$media = Cache::rememberForever('promotion_mobile_'.$value->id, function() use ($id) {
                return Media::where('refObj','=','Article')->where('refid','=',$id)->where('type', '=', Media::TYPE_IMAGE_MOBILE)->first();
            });
			$list[$key]['image'] = '';
			if(isset($media->domain)){
				$list[$key]['image']   = $media->domain . '/' . $media->path;
			}
		}
		
		 
		
		$this->return['success'] = 1;
		$this->return['promotion'] = $list;
		return response()->json($this->return);
		
	}
	
	public function getAppInfo(Request $request) {
		
		$this->islogin = true;
		
		
		$verified = $this->verifyAccess($request, 'appinfo');
		if(!$verified) {
			return response()->json($this->return);
		}
	 
		
	
		
		if( $request->has('code') )
		{
			$app['download']				= '';	
			$app['casino_download']			= '';	
			$app['slot_download']			= '';	
			$app['html5']					= '';	
			$app['casino_packagename']		= '';	
			$app['slot_packagename']		= '';	
			$app['username']				= 'coming soon';	
			$app['status']				    = Product::whereCode($request->code)->pluck('status');	
			

		
			if(strtolower($request->code) == 'agg') {
			
					$aggObj = new AGG;
					$app['download']				 = Config::get($this->currency.'.mobile.app_info_agg_casino_url');
					$app['html5']   			     = $aggObj->getLoginUrl($this->user->nickname, $this->currency,  'en' , Config::get($this->currency.'.agg.website'), 11);
					$app['casino_packagename']       = Config::get($this->currency.'.mobile.app_info_agg_casino_packagename');
					$app['username']    			 = Config::get($this->currency.'.mobile.app_info_agg_username_prefix').$this->user->nickname;
				
			}elseif(strtolower($request->code) == 'ibcx') {
		
					$app['html5']   			     = Config::get($this->currency.'.mobile.app_info_ibc_url').$request->token;
			}elseif(strtolower($request->code) == 'wft') {     
					$app['html5']   			     = route('wft').'?token='.$request->token;		
			
			}elseif(strtolower($request->code) == 'spg') {
					$app['username']				 = $this->user->nickname;	
					$app['html5']   			     = route('spg', [ 'type' => 'slot' , 'category' => 'slot' ] ).'?token='.$request->token;	
			
			}elseif(strtolower($request->code) == 'ftr') {    
					$app['html5']   			     = 'http://infiniwin.net/my/fortune';
					$app['status'] = '1';
			
			}elseif(strtolower($request->code) == 'alb') {
					$app['download']                 = Config::get($this->currency.'.mobile.app_info_alb_casino_url');
					$app['casino_packagename']       = Config::get($this->currency.'.mobile.app_info_alb_casino_packagename');
					$app['username']       			 = Config::get($this->currency.'.mobile.app_info_alb_username_prefix').$this->user->nickname.'av2';
			
			}elseif(strtolower($request->code) == 'hbn') {  
					$app['html5']                    = '';	
			
			}elseif(strtolower($request->code) == 'opu') {     
					$app['download']                 = '';
					$app['casino_packagename']       = '';
					$app['username']       			 = 'coming soon';	
			
			}elseif(strtolower($request->code) == 'sky') {    
					$app['download']                 = Config::get($this->currency.'.mobile.app_info_sky_slot_url');
					$app['slot_packagename']         = Config::get($this->currency.'.mobile.app_info_sky_slot_packagename');
					$app['username']       			 = Config::get($this->currency.'.mobile.app_info_sky_username_prefix').((new \App\libraries\providers\SKY())->formatUserId($this->user->id));	
			
			}elseif(strtolower($request->code) == 'mxb') {    
					$app['download']                 = Config::get($this->currency.'.mobile.app_info_mxb_casino_url');
					$app['casino_packagename']       = Config::get($this->currency.'.mobile.app_info_mxb_casino_packagename');
					$app['username']       			 = Config::get($this->currency.'.mobile.app_info_mxb_username_prefix').$this->user->nickname;

			
			}elseif(strtolower($request->code) == 'jok') {
					$app['download']                 = Config::get($this->currency.'.mobile.app_info_jok_slot_url');
					$app['casino_packagename']       = Config::get($this->currency.'.mobile.app_info_jok_slot_packagename');
					$app['username']       			 = Config::get($this->currency.'.mobile.app_info_jok_username_prefix').$this->user->nickname;

			
			}elseif(strtolower($request->code) == 'hog') { 
					$params = array(
						'currency' => $this->currency,
						'username' => $this->user->nickname,
					);                
					$hogObj = new HOG;
					$app['username']				 = $this->user->nickname;	
					$app['html5']                    = $hogObj->getLoginUrl( $params, $this->currency, 'en' );

					
			}elseif(strtolower($request->code) == 'vgs') {   
					$vgsObj = new VGS;
					$params = array(
						'currency' => $this->currency,
						'username' => $this->user->nickname,
					);
					$app['username']				 = $this->user->nickname;	
					$app['html5']  = $vgsObj->getLoginUrl( $params, date('U').mt_rand(1111,9999), 'en', $request->client_ip );
					
			}elseif(strtolower($request->code) == 'psb') {   
					$psbObj = new PSB;
			
					if( !Accountproduct::is_exist( $this->user->nickname , 'PSB' ) )
					{
						$psbObj->createUser( array(
																	'username' 	=> $this->user->nickname , 
																	'currency' 	=> $this->user->crccode , 
																	'password' 	=> $this->user->enpassword ,
																	'email'    	=> '' ,
																	'firstname' => '' ,
																	'lastname'  => '' ,
																	'userid'    => $this->user->id ,
																	) ) ;
					}
					
					
					$app['html5']  = $psbObj->getLoginUrl($this->user->nickname, false,  $this->user->crccode);
			
			}elseif(strtolower($request->code) == 'pltb') { 
					$app['casino_download']  	= Config::get($this->currency.'.mobile.app_info_plt_casino_url');
					$app['slot_download']    	= Config::get($this->currency.'.mobile.app_info_plt_slot_url');
					$app['slot_packagename']    = Config::get($this->currency.'.mobile.app_info_plt_slot_packagename');
					$app['casino_packagename']  = Config::get($this->currency.'.mobile.app_info_plt_casino_packagename');
					$app['username']  			= Config::get($this->currency.'.mobile.app_info_plt_username_prefix').strtoupper($this->user->nickname);
				
			}elseif(strtolower($request->code) == 'rys') {   
					$app['html5']  	= 'http://esgame.myroyalewin.net/royalewin/games/royalewheel/index.html?sid='.$request->token;
			
			
			}
		
		}else{
			
			$psbObj = new PSB;
		
			if( !Accountproduct::is_exist( $this->user->nickname , 'PSB' ) )
			{
				$psbObj->createUser( array(
															'username' 	=> $this->user->nickname , 
															'currency' 	=> $this->user->crccode , 
															'password' 	=> $this->user->enpassword ,
															'email'    	=> '' ,
															'firstname' => '' ,
															'lastname'  => '' ,
															'userid'    => $this->user->id ,
															) ) ;
			}
			 
			 
			$result = $psbObj->getLoginUrl($this->user->nickname, false,  $this->user->crccode);
			
			//Log::info($result);
			
		
		
			if( $products = Product::get() )
			{
				foreach( $products as $product )
				{
					if($product->code == 'PSB'){
						
						$app['4d_status'] = $product->status;
					}elseif($product->code == 'PLTB'){
						
						$app['plt_status'] = $product->status;
						$app['pltb_status'] = $product->status;
					}elseif($product->code == 'IBCX'){
						
						$app['ibc_status'] = $product->status;
						$app['ibcx_status'] = $product->status;
					}else
						$app[strtolower($product->code).'_status'] = $product->status;
				}
			}
			
			$app['vgs'] = '';
			
			if(!is_null(Config::get('MYR.vgs.username'))) {
				
				$vgsObj = new VGS;
				
				$params = array(
					'currency' => $this->currency,
					'username' => $this->user->nickname,
				);
				
				$app['vgs']  = $vgsObj->getLoginUrl( $params, date('U').mt_rand(1111,9999), 'en', $request->client_ip );
				
			}	
			if( $app['hog_status'] == 1 && Config::get('setting.opcode') == 'RYW' ){
				$app['hog'] = '';
				
				if(!is_null(Config::get('MYR.hog.prefix'))) {
					
					$hogObj = new HOG;
					
					$params = array(
						'username'  => $this->user->nickname,
						'currency'  => $this->currency,
					);
					
					$app['hog']  = $hogObj->getLoginUrl( $params, $this->currency, 'en' );
					
				}
			}
		
			$app['rys_status'] = 1;
			$app['rys']  = 'http://esgame.myroyalewin.net/royalewin/games/royalewheel/index.html?sid='.$request->token;
		
		

			$app['ibc']   			= Config::get($this->currency.'.mobile.app_info_ibc_url').$request->token;
			$app['wft']   			= route('wft').'?token='.$request->token;
			$app['pls']   			= route('pls').'?token='.$request->token;
			$app['spg']   			= route('spg', [ 'type' => 'slot' , 'category' => 'slot' ] ).'?token='.$request->token;
			$app['ftr']   			= 'http://infiniwin.com/my/fortune';
			
			$aggObj = new AGG;

			$app['agg']['casino']  	= Config::get($this->currency.'.mobile.app_info_agg_casino_url');
			$app['agg']['html5']    = $aggObj->getLoginUrl($this->user->nickname, $this->currency,  'en' , Config::get($this->currency.'.agg.website'), 11);
			$app['alb']['casino']   = Config::get($this->currency.'.mobile.app_info_alb_casino_url');
			$app['plt']['casino']   = Config::get($this->currency.'.mobile.app_info_plt_casino_url');
			$app['plt']['slot']     = Config::get($this->currency.'.mobile.app_info_plt_slot_url');
			$app['jok']['slot']     = Config::get($this->currency.'.mobile.app_info_jok_slot_url');
			$app['sky']['slot']     = Config::get($this->currency.'.mobile.app_info_sky_slot_url');
			$app['hbn']['slot']     = '';
			$app['opu']['casino']   = '';
			$app['mxb']['casino']   = Config::get($this->currency.'.mobile.app_info_mxb_casino_url');
			$app['w88']['casino']   = Config::get($this->currency.'.mobile.app_info_w88_casino_url');
			$app['4d']   			= $result['url'];
			
			$app['mxb']['casino_packagename']  = Config::get($this->currency.'.mobile.app_info_mxb_casino_packagename');
			$app['agg']['casino_packagename']  = Config::get($this->currency.'.mobile.app_info_agg_casino_packagename');
			$app['alb']['casino_packagename']  = Config::get($this->currency.'.mobile.app_info_alb_casino_packagename');
			$app['hbn']['slot_packagename']  = '';
			$app['plt']['slot_packagename']    = Config::get($this->currency.'.mobile.app_info_plt_slot_packagename');
			$app['plt']['casino_packagename']  = Config::get($this->currency.'.mobile.app_info_plt_casino_packagename');
			$app['w88']['casino_packagename']  = Config::get($this->currency.'.mobile.app_info_w88_casino_packagename');
			$app['jok']['slot_packagename']    = Config::get($this->currency.'.mobile.app_info_jok_slot_packagename');
			$app['sky']['slot_packagename']    = Config::get($this->currency.'.mobile.app_info_sky_slot_packagename');
			$app['opu']['casino_packagename']  = '';
			
			$app['agg']['username']   = Config::get($this->currency.'.mobile.app_info_agg_username_prefix').$this->user->nickname;
			$app['hbn']['username']   = 'comming soon';
			$app['alb']['username']   = Config::get($this->currency.'.mobile.app_info_alb_username_prefix').$this->user->nickname.'av2';
			$app['plt']['username']   = Config::get($this->currency.'.mobile.app_info_plt_username_prefix').strtoupper($this->user->nickname);
			$app['mxb']['username']   = Config::get($this->currency.'.mobile.app_info_mxb_username_prefix').$this->user->nickname;
			$app['w88']['username']   = Config::get($this->currency.'.mobile.app_info_w88_username_prefix').$this->user->nickname;
			$app['jok']['username']   = Config::get($this->currency.'.mobile.app_info_jok_username_prefix').$this->user->nickname;
			$app['sky']['username']   = Config::get($this->currency.'.mobile.app_info_sky_username_prefix').((new \App\libraries\providers\SKY())->formatUserId($this->user->id));
			$app['hog']['username']   = 'comming soon';
			$app['opu']['username']   = 'comming soon';
		}
		

		$this->return['success'] = 1;
		$this->return['result']  = $app;
		
		return response()->json($this->return);
		
	}
	
	public function WFT(Request $request){
		
		if($request->has('token')){		
			if($tracker = Onlinetracker::whereToken($request->token)->first()) {
				$accObj = Account::find($tracker->userid);
			}else{
				exit;
			}
		}
	
	
		$wftObj = new WFT;
		
	
		
		if(  isset($accObj->id) ){
			
			 $wftObj->updateUser( array(
														'username' 	=> $accObj->nickname , 
														'currency' 	=> $accObj->crccode , 
														'password' 	=> $accObj->enpassword ,
														'email'    	=> $accObj->email ,
														'firstname' => $accObj->fullname ,
														'lastname'  => $accObj->fullname ,
														'userid'    => $accObj->id 
											)  );
											
			$data['login_url'] = $wftObj->getLoginUrl( 
													$accObj->nickname , 
													$accObj->crccode , 
													'en' , 
													Config::get($accObj->crccode.'.wft.hostmobile') 
												 );
		}

		header("Location: ".$data['login_url']);
		
	}
	
	public function PLS(Request $request){
		
		if($request->has('token')){		
			if($tracker = Onlinetracker::whereToken($request->token)->first()) {
				$accObj = Account::find($tracker->userid);
			}else{
				exit;
			}
		}

		$plsObj = new PLS;
		
		$data['lists'] = $plsObj->gameList('MYR');
		
		$data['token'] = $request->token;

		return view( 'royalewin/mobile/product/pls_apps', $data );
	}
	
	public function SPG(Request $request){ 
	
		if($request->has('token')){		
			if($tracker = Onlinetracker::whereToken($request->token)->first()) {
				$accObj = Account::find($tracker->userid);
			}else{
				exit;
			}
		}
		
        if( $request->type == 'slot' )
		{
            $data['name']  = 'spg';
			$data['token'] = $request->token;
			$data['lists'] = Betspgcategory::where( 'category' , '=' , $request->category )->where( 'isActive' , '=' , 1 )->get();
                      if( $request->category == 'progressive' )
                          $data['lists'] = Betspgcategory::where( 'isProgressive' , '=' , 1 )->where( 'isActive' , '=' , 1 )->get();
                      
			$data['category'] = $request->category;
		
			return view( 'royalewin/mobile/product/spg_apps', $data );
        }

    }
	
	public function SPGSLOT(Request $request){
           
	

			if($tracker = Onlinetracker::whereToken($request->token)->first()) {
				//var_dump($tracker->userid);
				$accObj = Account::find($tracker->userid);
				//exit;
			}else{
				exit;
			}
		
		//var_dump($request->gameid);
		//var_dump($request->token);
               // exit;
                $spgObj = new SPG;
                
               
                        //getLoginUrl($acctId, $token, $game, $currency)
                        $data['login_url'] = $spgObj->getLoginUrl($accObj->username,$request->token,$request->gameid, 'MYR');
                        $data['is_mobile'] = true;
           
                        return view('royalewin/mobile/product/spg_slot',$data);
                       
    }
	
	public function postRegisterAffiliate(Request $request) {
		
		$verified = $this->verifyAccess($request, 'register-affiliate');
		if(!$verified) {
			return response()->json($this->return);
		}
		
		$validator = Validator::make(
			[
	

				'username'  	=> $request->input('username'),
				'fullname'	 	=> $request->input('fullname'),
				'password'   	=> $request->input('password'),
				'email'   		=> $request->input('email'),
				'telephone'   	=> $request->input('telephone'),
				'website'   	=> $request->input('website'),
				'websitedesc'   => $request->input('websitedesc'),
			],
			[
			    'username'	   => 'required|min:4|max:16|alpha_num|reserved_str',
			    'fullname'	   => 'required|min:2',
			    'password'	   => 'required|alpha_num|min:6',
			    'email'	   	   => 'required|email',
			    'telephone'	   => 'required|phone',
			    'website'	   => 'required',
			    'websitedesc'  => 'required',
			]
		);
		
		
		if ($validator->fails())
		{
			$this->return['errors'] = $validator->errors();
			return response()->json($this->return);
		}
		
		
		$object = new Affiliateregister;
		$object->username    = $request->username;
		$object->name 		 = $request->fullname;
		$object->password    = $request->password;
		$object->email 		 = $request->email;
		$object->telephone   = $request->telephone;
		$object->website 	 = $request->website;
		$object->websitedesc = $request->websitedesc;
		$object->createdip   = App::getRemoteIp();
		
		if($object->save())
		{
			$this->return['success'] = 1;
			return response()->json($this->return);
		}
	}
	
	public function get4dResult(Request $request){
		
	 	$verified = $this->verifyAccess($request, '4dresult');
		if(!$verified) {
			return response()->json($this->return);
		}
 
		if ( !Cache::has('4d_result') ) 
		{
			$data = array();
			$types[] = 'Magnum';
			$types[] = 'PMP';
			$types[] = 'Toto';
			$types[] = '5D';
			$types[] = '6D';
			$types[] = 'Singapore';
			$types[] = 'Sabah';
			$types[] = 'Sandakan';
			$types[] = 'Sarawak';
				
			foreach( $types as $key => $type ){
				
				$value = Betpsbresult::where( 'GameName' , '=' , $type )->orderBy( 'DrawDate' , 'desc')->first();
		
				$data[$value->GameName]['date']  	  	= substr($value->DrawDate,0,10);
				$data[$value->GameName]['first']  	  	= $value->FirstPrizeNumber;
				$data[$value->GameName]['second'] 	  	= $value->SecondPrizeNumber;
				$data[$value->GameName]['third']  	  	= $value->ThirdPrizeNumber;
				$data[$value->GameName]['fourth'] 	  	= $value->FourthPrizeNumber;
				$data[$value->GameName]['fifth']  	  	= $value->FifthPrizeNumber;
				$data[$value->GameName]['six']    	  	= $value->SixPrizeNumber;
				$data[$value->GameName]['special']    	= explode(',',$value->SpecialPrizeNumber);
				$data[$value->GameName]['consolation']	= explode(',',$value->ConsolationPrizeNumber);
				
			}
			Cache::forever('4d_result', $data);
		}
		else
		{
			$data = Cache::get('4d_result');
		}

		$this->return['success'] = 1;
		$this->return['result']  = $data;
		
		return response()->json($this->return);

	}	

	public function getAnnouncement(Request $request){
		
		$verified = $this->verifyAccess($request, 'announcement');
		if(!$verified) {
			return response()->json($this->return);
		}
 
		
		$result = '';
		
		$result = Cache::rememberForever('Announcement_'.Lang::getLocale(), function() {
			$str = '';
			$colunm = 'body'.Lang::getLocale();
			
			$annoucments = Announcement::where('status','=',1)->get();
			
			foreach($annoucments as $annoucment){
				
				$str .= $annoucment->$colunm.'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
			}
			
			return $str;
		});
		
		$this->return['success']	   = 1;
		$this->return['announcement']  = $result;
		
		return response()->json($this->return);
	}
	
	public function postResetPassword(Request $request){
		
		$verified = $this->verifyAccess($request, 'reset-password');
		if(!$verified) {
			return response()->json($this->return);
		} 
		
		$validator = Validator::make(
			[
				'username' => $request->input('username'),
				'email'    => $request->input('email'),
			],
			[
				'username' => 'required',
				'email'    => 'required|email',
			]
		);
		
		if ($validator->fails())
		{
			$this->return['errors'] = [$validator->errors()];
			return response()->json($this->return);
		}
		
		$website = Website::where( 'code' , '=' , $this->opcode )->first();
		
		if( $acdObj = Accountdetail::where( 'email' , '=' , $request->input('email') )->where( 'acccode' , '=' , $website->memberprefix . '_' . $request->input('username') )->first() )
		{
			
			$random_password = App::generatePassword(6, 10);
			
			
			
			$success = Account::where( 'id' , '=' , $acdObj->accid )->
						   update(
								[
								'password'			=>	Hash::make( $random_password ) , 
								'lastpwdchange'		=>	date('Y-m-d H:i:s')
								]
						   );
						   
			if( $success )
			{
				$code = 'PWRESET_EN';
				
	
				if( $atcObj = Article::where( 'code' , '=' , 'PWRESET_EN' )->first() ) 
				{
					$content = str_replace('&nbsp;', ' ', html_entity_decode($atcObj->content));
					$content = str_replace('%username%', $request->input('username') , $content);
					$content = str_replace('%password%', $random_password, $content);

					
					$status = Mail::raw(html_entity_decode($content), function ($message) use ($atcObj,$acdObj) 
					{
						$message->to($acdObj->email);
						$message->subject($atcObj->title);
					}); 
		
					if($status){
						$this->return['success'] = 1;
						return response()->json($this->return);
					}
				}
				
			}
			
		}else{
			$this->return['errors'] = ['code' => 'Invalid username or email!'];
			return response()->json($this->return);
		}
		
	}
	
	public function getWalletByProduct(Request $request){
		
		$this->islogin = true;
		$verified = $this->verifyAccess($request, 'wallet-by-product');
		if(!$verified) {
			return response()->json($this->return);
		}
		
		$balances = 0;
		
		if( $request->has('code') && strtolower($request->input('code')) == 'main')
		{
			
			$condition = '((status in ('.CBO_LEDGERSTATUS_PENDING.', '.CBO_LEDGERSTATUS_MANPENDING.','.CBO_LEDGERSTATUS_CONFIRMED.', '.CBO_LEDGERSTATUS_MANCONFIRMED.', '.CBO_LEDGERSTATUS_PROCESSED.') AND chtcode IN ('.CBO_CHARTCODE_WITHDRAWAL.', '.CBO_CHARTCODE_BALANCETRANSFER.')) OR (status in ('.CBO_LEDGERSTATUS_CONFIRMED.', '.CBO_LEDGERSTATUS_MANCONFIRMED.') AND chtcode NOT IN ('.CBO_CHARTCODE_WITHDRAWAL.', '.CBO_CHARTCODE_BALANCETRANSFER.')))';


			$condition .= ' AND accid = '. $this->user->id;
		
			$balances = Cashledger::whereRaw($condition)->sum('amount');
			
		}
		elseif( $request->has('code') )
		{
			if($object = Product::select(['code','status'])->whereCode($request->input('code'))->first()){
			
				$className = "App\libraries\providers\\".strtoupper($request->input('code'));
				$obj = new $className;
				
				
				
				if( $object->status == 2 )
				{
					$balances = Lang::get('Maintenance');
				}
				else
				{
					$balance = $obj->getBalance( $this->user->nickname, $this->user->crccode);
					$balance = ($balance == false) ? 0 : $balance;
					$balance = $this->user->crccode == 'VND' || $this->user->crccode == 'IDR' ? $balance * 1000 : $balance;
					
					Cache::put($this->user->nickname.'_'.strtoupper($request->input('code')).'_balance', (string)$balance, 60);
			
				}
				
				$balances = (float)$balance;
			
			}
			
		}
		
		
		
		$this->return['success']	   = 1;
		$this->return['wallet']        = $balances;
		
		return response()->json($this->return);
		
	}
	
	public function getWallet(Request $request){
		
		$this->islogin = true;
		$verified = $this->verifyAccess($request, 'wallet');
		if(!$verified) {
			return response()->json($this->return);
		}
		
		$balances['Total'] = 0;
		
		$productObj = Product::select(['code','status'])->whereRaw('status IN (1,2)')->get();
		
		$condition = '((status in ('.CBO_LEDGERSTATUS_PENDING.', '.CBO_LEDGERSTATUS_MANPENDING.','.CBO_LEDGERSTATUS_CONFIRMED.', '.CBO_LEDGERSTATUS_MANCONFIRMED.', '.CBO_LEDGERSTATUS_PROCESSED.') AND chtcode IN ('.CBO_CHARTCODE_WITHDRAWAL.', '.CBO_CHARTCODE_BALANCETRANSFER.')) OR (status in ('.CBO_LEDGERSTATUS_CONFIRMED.', '.CBO_LEDGERSTATUS_MANCONFIRMED.') AND chtcode NOT IN ('.CBO_CHARTCODE_WITHDRAWAL.', '.CBO_CHARTCODE_BALANCETRANSFER.')))';


		$condition .= ' AND accid = '. $this->user->id;
		
		$balances['MainWallet'] = Cashledger::whereRaw($condition)->sum('amount');

		
		$balances['MainWallet'] = (float)$balances['MainWallet'];
		
		
		foreach( $productObj as $object )
		{
		
			$className = "App\libraries\providers\\".$object->code;
			$obj = new $className;
			
			if( $object->status == 2 )
			{
				$balance = Lang::get('Maintenance');
			}
			else
			{
				$balance = $obj->getBalance( $this->user->nickname, $this->user->crccode);
				$balance = ($balance == false) ? 0 : $balance;
				$balance = $this->user->crccode == 'VND' || $this->user->crccode == 'IDR' ? $balance * 1000 : $balance;
				
				Cache::put($this->user->nickname.'_'.strtoupper($object->code).'_balance', (string)$balance, 60);
		
			}
			
			$balances['Total']      += (float)$balance;
			$balances[$object->code] = (float)$balance;
			if($object->code == 'PLTB'){
				$balances['PLT'] = (float)$balance;
			}
			
		
		}
		
		$balances['HBN'] = 0;
		$balances['Total'] += $balances['MainWallet'];
		
		
		
		$this->return['success']	   = 1;
		$this->return['wallet']        = $balances;
		
		return response()->json($this->return);
		
	}
	
	public function postTransactionHistory(Request $request){
		
		$this->islogin = true;
		$verified = $this->verifyAccess($request, 'history');
		if(!$verified) {
			return response()->json($this->return);
		} 
		
		$rows = array();
		
		if( $request->has('record_type')  ){
			$chtcode = $request->input('record_type');
		}else{
			$chtcode = CBO_CHARTCODE_DEPOSIT.','.CBO_CHARTCODE_WITHDRAWAL.','.CBO_CHARTCODE_BONUS.','.CBO_CHARTCODE_ADJUSTMENT.','.CBO_CHARTCODE_INCENTIVE;
		}
		
		if( $request->has('date_from') )
		{
			$date_from = $request->input('date_from');
			$date_to   = $request->input('date_to');
		}else
		{
			$date_from = date('Y-m-d');
			$date_to   = date('Y-m-d');
		}
	

		$cashledgers = Cashledger::where('accid','=',$this->user->id)->
								   where('created','>',$date_from.' 00:00:00')->
								   where('created','<',$date_to.' 23:59:59')->
								   whereRaw('chtcode IN('.$chtcode.')')->take(25)->orderBy( 'id' , 'desc')->get();
								   
		$rows[] = array(
				'id'  				=> '1', 
				'created'  			=> '2016-05-27 00:00:00', 
				'status'  			=> 'Pending',
				'amount'  			=> '100',
				'rejreason'  		=> '',
				'type'  			=> 'Deposit',
			);	
								   
		foreach( $cashledgers as $cashledger ){
			$type = Cashledger::getTypeText($cashledger->chtcode);
			$amount = ( $cashledger->amount < 0 ) ? $cashledger->amount * -1 : $cashledger->amount;
			$rows[] = array(
				'id'  				=> $cashledger->id, 
				'created'  			=> $cashledger->created->toDateTimeString(), 
				'status'  			=> $cashledger->getStatusText(),
				'amount'  			=> App::displayAmount($amount),
				'rejreason'  		=> $cashledger->rejreason,
				'type'  			=> $type,
			);	
		}
		
		$this->return['success'] = 1;
		$this->return['history'] = $rows;
		
		return response()->json($this->return);
	}
	
	public function postBetslipHistory(Request $request){
		
		$this->islogin = true;
		$verified = $this->verifyAccess($request, 'betslip-history');
		if(!$verified) {
			return response()->json($this->return);
		} 
		
		
		$rows = array();
		
		if( $request->has('group_by') && $request->get('group_by') == 1 ){
			
			$profitlosses = DB::select( DB::raw("SELECT date, SUM(wager) as sumwager, SUM(totalstake) as sumstake, SUM(amount) as sumwinloss, SUM(validstake) as sumvalid, prdid
											FROM `profitloss`
											WHERE accid = ".$this->user->id." and date = '".$request->date_from."' 
											GROUP BY prdid 
											"));
			foreach( $profitlosses as $profitlosse ){

				$rows[] = array(
					'product'  			=> Product::whereId($profitlosse->prdid)->pluck('code'), 
					'wager'  			=> $profitlosse->sumwager,
					'stake'  			=> App::displayAmount($profitlosse->sumstake),
					'winloss'  			=> App::displayAmount($profitlosse->sumwinloss),
					'validstake'  		=> App::displayAmount($profitlosse->sumvalid),
					'from'  			=> $profitlosse->date,
					'to'  				=> $profitlosse->date,
					'prdid'  			=> $profitlosse->prdid,

				);	
			}
			
		}else{
		
			$profitlosses = DB::select( DB::raw("SELECT date, SUM(wager) as sumwager, SUM(totalstake) as sumstake, SUM(amount) as sumwinloss, SUM(validstake) as sumvalid, prdid
											FROM `profitloss`
											WHERE accid = ".$this->user->id." and date >= '".$request->date_from."' and date <= '".$request->date_to."'
											GROUP BY date 
											"));
			foreach( $profitlosses as $profitlosse ){

				$rows[] = array(
					'date'  			=> $profitlosse->date,
					'wager'  			=> $profitlosse->sumwager,
					'stake'  			=> App::displayAmount($profitlosse->sumstake),
					'winloss'  			=> App::displayAmount($profitlosse->sumwinloss),
					'validstake'  		=> $profitlosse->sumvalid,

				);	
			}
		
		}
		
		
		$this->return['success'] = 1;
		$this->return['history'] = $rows;
		
		return response()->json($this->return);
		
	}
	
	public function postBetslipHistoryDetail(Request $request){
		
		$this->islogin = true;
		$verified = $this->verifyAccess($request, 'betslip-history-detail');
		if(!$verified) {
			return response()->json($this->return);
		} 

		$from 		= $request->has('date_from')?$request->get('date_from'):App::getDateTime(11);
		$to   		= $request->has('date_to')?$request->get('date_to'):App::getDateTime(3);
		$start   	= $request->has('start')?$request->get('start'):0;
		
		if( $request->get('prdid') == 0 )
			$condition  = 'datetime >= "'.$from.' 00:00:00" AND datetime <= "'.$to.' 23:59:59" AND accid = '.$this->user->id.'';
		else
			$condition  = 'datetime >= "'.$from.' 00:00:00" AND datetime <= "'.$to.' 23:59:59" AND prdid = '.$request->get('prdid').' AND accid = '.$this->user->id.'';

		$total = Wager::whereRaw($condition)->count();

		$rows = array();
		$footer = array();
		$totalstake = 0;
		$totalwinloss = 0;
		$totalvalid = 0;
	
		if($betslips = Wager::whereraw($condition)->skip($start)->take( 20 )->orderBy('id','desc')->get()) {

			$vendors = Product::getAllAsOptions();
			$products = Product::getCategoryOptions();
			foreach($betslips as $betslip) {
				//$winloss = $betslip->profitloss;
				$valid = $betslip->validstake;
				
				$betslip->stake 	 = $betslip->crccode == 'VND' || $betslip->crccode == 'IDR' ? $betslip->stake * 1000 : $betslip->stake;
				$betslip->validstake = $betslip->crccode == 'VND' || $betslip->crccode == 'IDR' ? $betslip->validstake * 1000 : $betslip->validstake;
				$betslip->profitloss = $betslip->crccode == 'VND' || $betslip->crccode == 'IDR' ? $betslip->profitloss * 1000 : $betslip->profitloss;
				
				$rows[] = array(
					'id' => $betslip->id,
					'date' => $betslip->datetime,
					'member' => $betslip->nickname,
					'vendor' => $vendors[$betslip->prdid],
					'product' => $products[$betslip->category],
					'detail' => '#'.$betslip->category.'-'.$betslip->id.'-'.$betslip->refid,
					'currency' => $betslip->crccode,
					'stake' => App::formatNegativeAmount($betslip->stake),
					'winloss' => App::formatNegativeAmount($betslip->profitloss),
					'valid' => App::formatNegativeAmount($betslip->validstake),
					'result' => $betslip->getResultText(),
				);
				$totalvalid += $betslip->validstake;
				$totalstake += $betslip->stake;
				$totalwinloss += $betslip->profitloss;
				
			}
		}
		
		$footer[] = array(
			'currency' => Lang::get('COMMON.TOTAL'),
			'stake' => App::formatNegativeAmount($totalstake),
			'winloss' => App::formatNegativeAmount($totalwinloss),
			'valid' => App::formatNegativeAmount($totalvalid),
		);
		
		$this->return['success'] = 1;
		$this->return['history'] = $rows;
		$this->return['total']   = $total;
		
		return response()->json($this->return);
		
	}
	
	public function postVersion(Request $request) {
		
		
		$verified = $this->verifyAccess($request, 'version');
		if(!$verified) {
			return response()->json($this->return);
		}
		
		$data['details']['version_number'] = Config::get($this->currency.'.mobile.apk_version_number');
		$data['details']['download_link']  = Config::get($this->currency.'.mobile.apk_download_link');
		
		
		$this->return['success'] = 1;
		$this->return['result']  = $data;
		
		return response()->json($this->return);
		
		
	}	
	
	public function postPushNotification(Request $request) {
		
		
		$verified = $this->verifyAccess($request, 'pushmessage');
		
		if(!$verified) {
			return response()->json($this->return);
		}
		
		$msg_payload = array (
				'mtitle'   		  => $request->title,
				'mbody'    		  => $request->body,
		);
		

		$this->return['success'] = 1;
		$this->return['result']  = PushNotifications::android($msg_payload, $request->gcmid);
		
		return response()->json($this->return);
	}
	
}