<?php namespace App\Http\Controllers\App\Lasvegas;

use App\Models\Account;
use App\Models\Agent;
use App\Models\Accountdetail;
use App\Models\Affiliate;
use App\Models\Article;
use App\Models\Onlinetracker;
use App\Models\Cashcard;
use App\Models\Website;
use App\Models\Channel;
use App\Models\Configs;
use App\Models\Cashledger;
use App\Models\Withdrawcard;
use App\Models\Affiliatecredit;
use Illuminate\Http\Request;
use App\libraries\Login;
use App\libraries\App;
use App\libraries\PushNotifications;
use Carbon\Carbon;
use Validator;
use Hash;
use Cache;
use Config;
use Lang;
use Crypt;
use Session;
use GeoIP;
use Auth;
use DB;
use Storage;
use File;
use Log;


class ApiController extends Controller {
	

	public function __construct(Request $request)
	{
		
	}
	
	
	public function verifyAccess($request) {
		
		//Log::info($request->all());
		
		if ( !$request->has('token') )
		{ 
			return false;
		}
			
		if($tracker = Onlinetracker::whereToken( $request->token )->whereType(2)->first()) 
		{
			Session::put('username',  		$tracker->username );
			Session::put('userid',  		$tracker->userid );
			
			$affiliate = Affiliate::find($tracker->userid);
			
			Session::put('username',        $affiliate->username );
			Session::put('lastpwdchange',   $affiliate->lastpwdchange );
			Session::put('allow_withdraw',  $affiliate->withdrawal );
			Session::put('lastlogin',  	    $affiliate->lastlogin );
			Session::put('userid',  	    $affiliate->id );
			Session::put('level',  	   	 	$affiliate->level );
			Session::put('password',  	 	$affiliate->password );
			Session::put('code',  	     	$affiliate->code );
			Session::put('crccode',  	    $affiliate->crccode);
			
			return true;
		}
			
		return false;
	}
	
	public function Login(Request $request) {
	
		if (Auth::affiliate()->attempt(array('username' => $request->input('username'), 'password' => $request->input('password') , 'status' => 1 )))
        {	
	
			$affiliate = Auth::affiliate()->get();
			
			$login = new Login;
			$login->doMakeTracker($affiliate, 'Affiliate');
			
			Session::put('username',        $affiliate->username );
			Session::put('password2',       $affiliate->password2 );
			Session::put('parentid',        $affiliate->parentid );
			Session::put('lastpwdchange',   $affiliate->lastpwdchange );
			Session::put('lastlogin',  	    $affiliate->lastlogin );
			Session::put('userid',  	    $affiliate->id );
			Session::put('level',  	   	 	$affiliate->level );
			Session::put('password',  	 	$affiliate->password );
			Session::put('code',  	     	$affiliate->code );
			Session::put('crccode',  	    $affiliate->crccode);
			
			Affiliate::where( 'id' , '=' , $affiliate->id )->update(
				array( 'lastlogin' => date('Y-m-d H:i:s'))
			);
			
			return response()->json( [ 'success' => true, 'message' => 'Login Success!' , 'token' =>  Session::get('token') , 'withdrawal' => $affiliate->withdrawal ] );
			
		}
		
		return response()->json( [ 'success' => false, 'message' => 'Login Failed!' ] );
		

	}	
	
	public function Logout(Request $request) {
	
		Onlinetracker::whereType(CBO_LOGINTYPE_AFFILIATE)->whereToken($request->input('token'))->delete();
		Session::flush();
		Auth::user()->logout();
		
		return response()->json( [ 'success' => true, 'message' => 'Logout Success!' ] );
		

	}	
	
	public function GenerateCode(Request $request) {
	
		if(!$this->verifyAccess($request)){
			return response()->json( [ 'success' => false, 'message' => 'invalid token' ] );
		}
		
		$validator = Validator::make(
			[
				'amount'   => $request->input('amount'),
			],
			[
				'amount'   => 'required|numeric|min:1',
			]
		);
		
		if ($validator->fails())
		{
			return response()->json( [ 'success' => false, 'message' => $validator->errors() ] );
		} 
		
		if( $request->input('create_account') == true ){
			$account = $this->create_test_account();
		}
	
		if( Affiliatecredit::getBalance(Session::get('userid')) < $request->input('amount') )
		{
			return response()->json( [ 'success' => false, 'message' => 'Not Enough credit!' ] );
		} 
		
		$after7days = Carbon::now()->addDays(7);
		
		$cashcardObj = new Cashcard;
		$cashcardObj->aflid 	= Session::get('userid');
		$cashcardObj->crccode   = Session::get('crccode');
		$cashcardObj->crcrate   = 1;
		$cashcardObj->amount    = $request->input('amount');
		$cardno1 				= (string) App::generateNum(4);
		$cardno2 				= (string) App::generateNum(4);
		$cardno3 				= (string) App::generateNum(4);
		$cardno4 				= (string) App::generateNum(4);
		$cardno5 				= (string) App::generateNum(4);
		$cardno 				= $cardno1.' '.$cardno2.' '.$cardno3.' '.$cardno4.' '.$cardno5;
		$cashcardObj->number 	= $cardno1.$cardno2.$cardno3.$cardno4.$cardno5;
		$cashcardObj->createdby = Session::get('userid');
		$cashcardObj->expiry 	= $after7days->toDateTimeString();
		$cashcardObj->reprint 	= 1;
		$cashcardObj->save();
		
		$content = $this->getHeaderFooter();
		
		
		
		if( $request->input('create_account') == true )
		{
			return response()->json( [ 'success' => true, 'message' => 
					[ 
						'header'	  => htmlspecialchars($content['header']) ,
						'serial' 	  => $cashcardObj->id, 
						'code' 		  => $cardno , 
						'username'	  => $account['username'] ,
						'password'	  => $account['password'] ,
						'Expired'	  => $cashcardObj->expiry ,
						'Time'	 	  => $cashcardObj->created->toDateTimeString() ,
						'Printby'	  => Session::get('username') ,
						'footer'	  => htmlspecialchars($content['footer']) 
						
					] 
			] );
		}
		else
		{
			return response()->json( [ 'success' => true, 'message' => 
					[ 
						'header'	  => htmlspecialchars($content['header']) ,
						'serial' 	  => $cashcardObj->id, 
						'code' 		  => $cardno , 
						'Expired'	  => $cashcardObj->expiry ,
						'Time'	 	  => $cashcardObj->created->toDateTimeString() ,
						'Printby'	  => Session::get('username') ,
						'footer'	  => htmlspecialchars($content['footer']) 
					] 
			] );
		}

	}	
	
	private function create_test_account(){
		
		$accObj = new Account;
		$acdObj = new Accountdetail;
		

		$password = App::generateNum(6);
		
		$website = Website::find(1);
		
		
		
		if( Session::has('username') )
		{
			
			if( Affiliate::where( 'username' , '=' , Session::get('username') )->count() == 1 )
			{
				$refcode = Affiliate::where( 'username' , '=' , Session::get('username') )->pluck('code');
				$channel  = 'affiliate';
			}
				
		}
		
		
		$magtcode = $website->code.'_'.Session::get('crccode').(($refcode)?'_AFF':'_COM');
		$agtcode  = $magtcode.'_'.($refcode?strtoupper($refcode):date('ym'));
		

		
		if( $chnObj = Channel::where( 'name' , '=' , 'affiliate' )->first() ) {
			
		}else{
			
			$chnObj = new Channel;
			$chnObj->name = 'affiliate';
			$chnObj->save();
		}
		
		$agtObj = Agent::where( 'code' , '=' , $agtcode )->first();
		
		
		$accObj->regchannel    = $chnObj->id;
		$accObj->nickname      = strtoupper(App::generateString(6)).App::generateNum(2);
		$accObj->code	       = strtolower( $website->memberprefix . '_' . $accObj->nickname );
		$accObj->fullname      = '';
		$accObj->password      = Hash::make( $password );
		$accObj->enpassword    = Crypt::encrypt( $password );
		$accObj->crccode       = Session::get('crccode');
		$accObj->term  	       = CBO_ACCOUNTTERM_CASH;
		$accObj->timezone      = Configs::getParam("SYSTEM_TIMEZONE");
		$accObj->createdby     = 0;
		$accObj->islogin   	   = 1;
		$accObj->withdrawlimit = Configs::getParam("SYSTEM_ACCOUNT_WITHDRAWLIMIT");
		$accObj->lastpwdchange = date('Y-m-d H:i:s');
		$accObj->status 	   = CBO_ACCOUNTSTATUS_ACTIVE;
		$accObj->aflid 		   = $agtObj->aflid;
		$accObj->agtid 		   = $agtObj->id;
		$accObj->wbsid 		   = $website->id;
		
		if($accObj->save()) 
		{
 

			$acdObj->createdip	   = App::getRemoteIp();
			$acdObj->id 		   = $accObj->id;
			$acdObj->accid 		   = $accObj->id;
			$acdObj->wbsid 		   = $accObj->wbsid;
			$acdObj->verifyno      = App::generatePassword();
			$acdObj->acccode 	   = $accObj->code;
			$acdObj->referenceno   = 1000 + $accObj->id;
			$acdObj->save();
		}
		
		return array( 'username' => $accObj->nickname , 'password' => $password );

	}
	
	public function Withdraw(Request $request){
	
		if(!$this->verifyAccess($request)){
			return response()->json( [ 'success' => false, 'message' => 'invalid token' ] );
		}
		
		$validator = Validator::make(
			[
				'code'   => $request->input('code'),
			],
			[
				'code'   => 'required|alpha_num',
			]
		);
		
		if ($validator->fails())
		{
			return response()->json( [ 'success' => false, 'message' => $validator->errors() ] );
		}
		
		if( $withdrawcard = Withdrawcard::whereNumber( $request->code )->first() )
		{
			
			if( Session::get('parentid') == 0 ){
				if( Session::get('userid') != $withdrawcard->parentid){
					return response()->json( [ 'success' => false, 'message' => 'Invalid Branch!' ] );
				}
			}
			if( Session::get('parentid') != 0 ){
				if( Session::get('parentid') != $withdrawcard->parentid )
				{
					return response()->json( [ 'success' => false, 'message' => 'Invalid Branch!' ] );
				}
			}
			
			Session::put('withdrawcode', $request->code);
		
			$account = Account::find($withdrawcard->accid);
			
			return response()->json( [ 'success' => true, 'message' => 
					[ 
						'username' 	  => $account->nickname    , 
						'amount' 	  => $withdrawcard->amount , 
					] 
			] );
			
		}
		else
		{
			return response()->json( [ 'success' => true, 'message' => 'Invalid Code!' ] );
			
		}

	}	
	
	public function WithdrawVerify(Request $request) {
	
		if(!$this->verifyAccess($request)){
			return response()->json( [ 'success' => false, 'message' => 'invalid token' ] );
		}
		
		$validator = Validator::make(
			[
				'password'   => $request->input('password'),
			],
			[
				'password'   => 'required',
			]
		);
		
		if ($validator->fails())
		{
			return response()->json( [ 'success' => false, 'message' => $validator->errors() ] );
		} 
		
		if( Session::has('withdrawcode') )
		{
			
			if( $withdrawcard = Withdrawcard::whereNumber( Session::get('withdrawcode') )->first() )
			{
				
				$accObj = Account::find($withdrawcard->accid);
				
				if(!Hash::check( $accObj->enpassword , $request->input('password') ))
				{
					 
					 if( $cashledgerObj = Cashledger::whereRefid($withdrawcard->id)->whereRefobj('Withdrawcard')->whereIn('status', array(1,4))->first() )
					 {
						 $withdrawcard->status = 1;
						 $withdrawcard->aflid  = Session::get('userid');
						 $withdrawcard->save();
					
						 if( $cashledgerObj->save() )
						 {
							 return response()->json( [ 'success' => true , 'message' => 'Withdraw Success!' ] );
						 }
						 
					 }
					 
					 return response()->json( [ 'success' => false , 'message' => 'Withdraw Rejecetd By HQ!' ] );
					 
				}
				else
				{
					 return response()->json( [ 'success' => false , 'message' => 'Wrong Password!' ] );
				}
			}
			else
			{	
				return response()->json( [ 'success' => false, 'message' => 'Failed!' ] );
			}
		}

	}	
	
	public function PasswordVerify(Request $request) {
		
		if(!$this->verifyAccess($request)){
			return response()->json( [ 'success' => false, 'message' => 'invalid token' ] );
		}

		if ( $request->input('password') != Crypt::decrypt(Session::get('password2')) ) 
		{ 
			return response()->json( [ 'success' => false, 'message' => 'invalid password!'   ] );
			exit;
		}
		
		return response()->json( [ 'success' => true, 'message' => 'Verified!' ] );
	}
		
		
	public function LastReprint(Request $request) {
		
		if(!$this->verifyAccess($request)){
			return response()->json( [ 'success' => false, 'message' => 'invalid token' ] );
		}
		
		if( $cashcardObj = Cashcard::whereAflid(Session::get('userid'))->orderBy( 'id' , 'desc')->first())
		{
			$content = $this->getHeaderFooter();
			
			$numbers = str_split($cashcardObj->number, 4);
			
			$cashcardObj->reprint 	+= 1;
			$cashcardObj->save();
			
			return response()->json( [ 'success' => true, 'message' => 
					[ 
						'header'	  => htmlspecialchars($content['header']) ,
						'serial' 	  => $cashcardObj->id, 
						'amount' 	  => $cashcardObj->amount, 
						'code' 		  => $numbers[0] . ' ' . $numbers[1] . ' ' . $numbers[2] . ' ' . $numbers[3] . ' ' . $numbers[4]  , 
						'Expired'	  => $cashcardObj->expiry ,
						'Time'	 	  => $cashcardObj->created->toDateTimeString() ,
						'Printby'	  => Session::get('username'), 
						'footer'	  => htmlspecialchars($content['footer']), 
						'reprint'	  => $cashcardObj->reprint 
						
					] 
			] );
		}
		
		return response()->json( [ 'success' => false, 'message' => 'No Transaction!' ] );
	}
	
	private function getHeaderFooter(){
		
		$content['header'] = Article::whereCode('HEADER')->whereStatus(1)->pluck('content');
		$content['footer'] = Article::whereCode('FOOTER')->whereStatus(1)->pluck('content');
		
		return $content;
	}
	
	public function RePrint(Request $request) {
		
		$temp = explode( '_' , $request->number );
		
		if( $temp[0] == 'd' )
		{
			if($cashcardObj = Cashcard::whereAflid(Session::get('userid'))->whereNumber($temp[1])->first())
			{
			
				$content = $this->getHeaderFooter();
				
				$numbers = str_split($cashcardObj->number, 4);
				
				$cashcardObj->reprint 	+= 1;
				$cashcardObj->save();
				
				return response()->json( [ 'success' => true, 'message' => 
						[ 
							'header'	  => htmlspecialchars($content['header']) ,
							'serial' 	  => $cashcardObj->id, 
							'amount' 	  => $cashcardObj->amount, 
							'code' 		  => $numbers[0] . ' ' . $numbers[1] . ' ' . $numbers[2] . ' ' . $numbers[3] . ' ' . $numbers[4]  , 
							'Expired'	  => $cashcardObj->expiry ,
							'Time'	 	  => $cashcardObj->created->toDateTimeString() ,
							'Printby'	  => Session::get('username'), 
							'footer'	  => htmlspecialchars($content['footer']) ,
							'reprint'	  => $cashcardObj->reprint 
							
						] 
				] );
				
			}
		}
		elseif( $temp[0] == 'w' )
		{
			if($cashcardObj = Withdrawcard::whereAflid(Session::get('userid'))->whereNumber($temp[1])->first())
			{
			
				$content = $this->getHeaderFooter();
				
		
			
				
				return response()->json( [ 'success' => true, 'message' => 
						[ 
							'header'	  => htmlspecialchars($content['header']) ,
							'serial' 	  => $cashcardObj->id, 
							'amount' 	  => $cashcardObj->amount, 
							'code' 		  => $cashcardObj->number  , 
							'Expired'	  => '' ,
							'Time'	 	  => $cashcardObj->created->toDateTimeString() ,
							'Printby'	  => Session::get('username'), 
							'footer'	  => htmlspecialchars($content['footer']) ,
							'reprint'	  => 1 
							
						] 
				] );
				
			}
		}
		
		return response()->json( [ 'success' => false, 'message' => 'Invalid Number!' ] );
			
		
	}
	
	public function History(Request $request) {
		
	
		if(!$this->verifyAccess($request)){
			return response()->json( [ 'success' => false, 'message' => 'invalid token' ] );
		}
		
		$rows = array();
		
		$now = Carbon::now();
		
		$startdate = $request->has('startdate') ? $request->startdate : $now->toDateString().' 00:00:00';
		$enddate   = $request->has('enddate')   ? $request->enddate   : $now->toDateString().' 23:59:59';
		
		if( $cashcards = Cashcard::whereAflid(Session::get('userid'))->whereBetween('created',array($startdate,$enddate))->get() ){
			
			foreach( $cashcards as $cashcard )
			{
				$rows[] = array( 
						'date' 		=> $cashcard->created->toDateTimeString() , 
						'type' 		=> 'deposit'  , 
						'number' 	=> 'd_'.$cashcard->number  , 
						'amount' 	=> App::displayNumberFormat($cashcard->amount ),
						'serial' 	=> $cashcard->id,
						'reprint' 	=> $cashcard->reprint
				);
			}
			
		}
		
		if( $Withdrawcards = Withdrawcard::whereAflid(Session::get('userid'))->whereBetween('created',array($startdate,$enddate))->get() ){
			
			foreach( $Withdrawcards as $Withdrawcard )
			{
				$rows[] = array( 
						'date' 		=> $Withdrawcard->created->toDateTimeString() , 
						'type' 		=> 'withdraw'  , 
						'number' 	=> 'w_'.$Withdrawcard->number  , 
						'amount' 	=> App::displayNumberFormat($Withdrawcard->amount ),
						'serial' 	=> '',
						'reprint' 	=> ''
				);
			}
			
		}
		
		if( count($rows) > 0 )
		{
			usort($rows, function ($a, $b) {
			   return strtotime($a["date"]) - strtotime($b["date"]);
			});
		}
		
		$totalpage = 1;
		$index     = 0;
		$rows 	   = array_reverse($rows);
		$newrows   = $rows;
		
		if( count($rows) > 5 )
		{
			$totalpage = intval(count($rows) / 5); 
			
			if( count($rows) % 5 != 0 )
			{
				$totalpage += 1;
			}

			
			if( $request->page > 1 )
			{
				$index = ($request->page * 5) - 5;
			}
			
			$newrows = array();
			
			for( $i = 0 ; $i < 5 ; $i++ )
			{
				$newrows[] = $rows[$index+$i];
			}

		}
	
		
		return response()->json( [ 'success' => true, 'message' => $newrows , 'totalpage' => $totalpage , 'page' => $request->page] );

	}
	
	public function Balance(Request $request) {
	
		if(!$this->verifyAccess($request)){
			return response()->json( [ 'success' => false, 'message' => 'invalid token' ] );
		}
		
		$credit = Affiliatecredit::getBalance(Session::get('userid'));
		$cash   = Affiliatecredit::getBalance(Session::get('userid'),1);
		
		return response()->json( [ 'success' => true, 'message' => array( 'balance' => App::displayNumberFormat($credit) , 'cash' => $cash ) ] );

	}		
	
	public function printReport(Request $request) {
	
		if(!$this->verifyAccess($request)){
			return response()->json( [ 'success' => false, 'message' => 'invalid token' ] );
		}
		
		$start_credit   = Affiliatecredit::getBalance( Session::get('userid') , 0 , $request->startdate );
		$end_credit     = Affiliatecredit::getBalance( Session::get('userid') , 0 , $request->enddate );
		$total_deposit  = Cashcard::whereAflid(Session::get('userid'))->whereBetween('created',array($request->startdate,$request->enddate))->sum('amount');
		$total_withdraw = Withdrawcard::whereAflid(Session::get('userid'))->whereBetween('created',array($request->startdate,$request->enddate))->sum('amount');
		
		
		return response()->json( [ 'success' => true, 'message' => array( 'start_balance' => App::displayNumberFormat($start_credit) , 'end_balance' => App::displayNumberFormat($end_credit) , 'total_deposit' => App::displayNumberFormat($total_deposit) , 'total_withdraw' => App::displayNumberFormat($total_withdraw) , 'start_time' => $request->startdate , 'end_time' => $request->enddate , 'print_by' =>  Session::get('username') ) ] );

	}	
	

	
	
}