<?php namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller\Api;
use App\Models\Betw88;
use App\Models\Account;
use App\Models\Product;
use App\Models\Wager;
use App\Models\Currency;
use App\Models\Profitloss;
use App\Models\Onlinetracker;
use App\libraries\providers\W88;
use App\libraries\Array2XML;
use App\libraries\App;
use Illuminate\Http\Request;
use App\libraries\Login;
use Config;
use DB;
use Cache;
use Log;
use Auth;
use Carbon\Carbon;

class W88Controller extends MainController{
	
	protected $accounts = array();
	
	protected $profitloss = array();

	public function index(Request $request)
	{
		
		App::check_process('w88_datapull_process');
		
		$urls[0] = Config::get($request->currency.'.w88.casino_url');
		$urls[1] = Config::get($request->currency.'.w88.slot_url');
		
		foreach( $urls as $type => $url )
		{
			
			$startdate = str_replace(' ', '%20', Betw88::where( 'type' , '=' , $type )->orderBy('trans_date','desc')->pluck('trans_date'));	
			if( !$startdate )
			{
				if( $type == 0 )
					$startdate = '2016-07-01 00：00：00';
				elseif( $type == 1 )
					$startdate = '2016-07-01 00：00：00';
				
			}
			if ( $request->has('startdate') ) {
                $startdate = $request->startdate;
            } elseif ( $request->get('repullytd') == 'Y' ) {
                $startdate = Carbon::now()->addDays(-1)->toDateString() . ' 00:00:00';
            }
			
			$enddate   = date('Y-m-d').' 23:59:59';		
			
			$page_num = 1;

			while($page_num != false)
			{
				$postfields = 'merch_id='.Config::get($request->currency.'.w88.merchant_id').
							  '&merch_pwd='.Config::get($request->currency.'.w88.merchant_pass').
							  '&page_num='.$page_num.
							  '&page_size=1000'.
							  '&date_from='.$startdate.
							  '&date_to='.$enddate;
							  
				if($type == 1){
					$postfields .= '&game_provider=CTXM';
				}

				$result = simplexml_load_string( $this->curl( $postfields , $url , false) );

				if( isset($result->error_code) && $result->error_code == 0 )
				{
					
					foreach($result->items->item as $key => $value )
					{


						$data['trans_date'] 	= (string)$value['trans_date'];
						$data['player_hand'] 	= (string) isset($value['platform']) ? $value['platform'] : '';
						$data['currency'] 		= (string)$value['currency'];
						$data['game_result'] 	= (string) isset($value['game_result']) ? $value['game_result'] : '';
						$data['balance'] 		= (string)$value['balance'];
						$data['winlose'] 		= $value['winlose'];
						$data['bet'] 			= $value['bet'];
						$data['game_type'] 		= (string)$value['game_type'];
						$data['user_id'] 		= (string)$value['user_id'];
						$data['status'] 		= (string) isset($value['status']) ? $value['status'] : '' ;
						$data['table_id']		= (string) isset($value['table_id']) ? $value['table_id'] : '';
						$data['bet_id'] 		= (int) isset($value['bet_id']) ? $value['bet_id'] : $value['trans_id'];
						$data['trans_id'] 		= '';
						$data['bundle_id'] 		= (int) isset($value['bundle_id']) ? $value['bundle_id'] : '';
						$data['type'] 			= $type;
						$data['created']		= date("y-m-d H:i:s");
						$data['modified']		= date("y-m-d H:i:s");		

						foreach( $data as $key => $value ){
							if (strpos($value,"'") !== false) {
								$insert_data[$key] = '"'. $value . '"';
							}else{
								$insert_data[$key] = "'". $value . "'";
							}
						}					


						if(DB::statement('call insert_w88('.implode(',',$insert_data).')')){
						
								$this->InsertWager($data,$type);
							
						} 				

					

					}
					
					Profitloss::updatePNL2($this->profitloss);
				
				} 
				if( isset($result->items['total_page']) )
				{
					if( $result->items['total_page'] == 0 ){
						$page_num = false;
					}else{
						if( $result->items['total_page'] != $result->items['page_num'] )
						{
							$page_num++;
						}else
						{
							$page_num = false;
						}
					}
				}else{
					$page_num = false;
				}
				
			}
		}
		
		App::unlock_process('w88_datapull_process');
		
	}	

	private function InsertWager( $bet , $type){
		
		if( !isset($this->accounts[$bet['user_id']]) )
			$this->accounts[$bet['user_id']] = Account::where( 'nickname' , '=' , $bet['user_id'] )->first();
		
		$prdObj = Cache::rememberForever('product_w88_obj', function(){
				return Product::where( 'code' , '=' , 'W88' )->first();
		});
	
		$data['accid'] 		 		= $this->accounts[$bet['user_id']]->id;
		$data['acccode'] 	 		= $this->accounts[$bet['user_id']]->code;
		$data['nickname'] 	 		= $bet['user_id'];
		$data['wbsid'] 		 		= $this->accounts[$bet['user_id']]->wbsid;
		$data['prdid'] 		 		= $prdObj->id;
		$data['matchid'] 	 		= $bet['table_id'] == '' ? "''":$bet['table_id'];
		$data['gamename'] 	 		= $bet['game_type'];
		$data['category'] 	 		= $type == 0 ? Product::getCategory('Casino') : Product::getCategory('egames');
		$data['refid'] 		 		= $bet['bet_id'];
		$data['crccode'] 	 		= $this->accounts[$bet['user_id']]->crccode;
		$data['crcrate'] 	 		= Currency::getCurrencyRate($data['crccode']);
		$data['datetime'] 	 		= $bet['trans_date'];
		$data['ip'] 		 		= '';
		$data['payout'] 	 		= (float) ( $bet['bet'] - $bet['winlose']);
		$data['profitloss']  		= $bet['winlose'];
		$data['accountdate'] 		= substr( $bet['trans_date'], 0 , 10 );
		$data['stake']		 		= $bet['bet'];
		$data['status'] 			= Wager::STATUS_SETTLED;
		

		if( $data['stake'] == $data['payout'] )	    $data['result']  = Wager::RESULT_DRAW;
		else if( $data['stake'] > $data['payout'] ) $data['result']  = Wager::RESULT_LOSS;
		else if( $data['stake'] < $data['payout'] ) $data['result']  = Wager::RESULT_WIN;
		$data['validstake'] 		= $bet['bet'];
		$data['stakelocal'] 		= Currency::getLocalAmount($data['stake'], $data['crccode']);
		$data['payoutlocal']		= Currency::getLocalAmount($data['payout'], $data['crccode']);
		$data['profitlosslocal']	= Currency::getLocalAmount($data['profitloss'], $data['crccode']);
		$data['validstakelocal']	= Currency::getLocalAmount($data['validstake'], $data['crccode']);
		$data['iscalculated'] 	    = 0;
		$data['created']			= date("y-m-d H:i:s");
		$data['modified']			= date("y-m-d H:i:s");
		
		foreach( $data as $key => $value ){
			$insert_data[$key] = '"'. $value . '"';
		}
		

		if(DB::statement('call insert_wager('.implode(',',$insert_data).')'))
		{
			$this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['prdid']   = $data['prdid'];
			$this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['accobj']  = $this->accounts[$bet['user_id']];
			$this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['crcrate'] = $data['crcrate'];
		}
		

	}
	
	public function doValidateTicket(Request $request){
		
		$return = '<erorr>-3</erorr>';
		
		if( $trackerObj = Onlinetracker::whereRaw('token="'.$request->ticket.'" and type = 1')->first() ) 
		{
			$accObj = Account::find($trackerObj->userid);
			
			$return  = '<error_code>0</error_code>';
			$return .= '<cust_id>'.$accObj->nickname.'</cust_id>';
			$return .= '<currency_code>'.$accObj->crccode.'</currency_code>';
			$return .= '<cust_name>'.$accObj->nickname.'</cust_name>';
			$return .= '<language>en-us</language>';
			$return .= '<country>MY</country>';
			$return .= '<test_cust>false</test_cust>';
			
		}
		
		$return = '<?xml version="1.0" encoding="UTF-8"?>
				   <resp>
					'.$return.'
				   </resp>';

		return response()->view('front/xml', array('name' =>  $return))->header('Content-Type', 'application/xml');	
	}
	
	public function doValidateMobile(Request $request){
		
		$token = '';
		
		$data = json_decode(file_get_contents('php://input'), true);
		
		Log::info($data);
		
		$mode 	  = (isset($data['Mode'])?$data['Mode']:false);
		$hash	  = (isset($data['Hash'])?$data['Hash']:'');
		$username = (isset($data['Username'])?$data['Username']:false);
		$password = (isset($data['Password'])?$data['Password']:false);
		
		if(($mode && $mode === 'ClientLogin') && ($hash === md5($username.$password.Config::get(Account::whereNickname($username)->pluck('crccode').'.w88.key')))) {
			if($username && $password) {
				if (Auth::user()->attempt(array('nickname' => $username, 'password' => $password , 'status' => 1 , 'istestacc' => 0 )))
				{	
					$login = new Login;
					$user = Auth::user()->get();
					$token = $login->doMakeTracker(Auth::user()->get(), 'Account');

				}
			}
		}
		Log::info($token);
		echo $token;
		
	}
	

}
