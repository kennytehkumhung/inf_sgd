<?php namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller\Api;
use App\Models\Betasc;
use App\Models\Account;
use App\Models\Betascleague;
use App\Models\Betascteam;
use App\Models\Product;
use App\Models\Wager;
use App\Models\Currency;
use App\Models\Profitloss;
use App\Models\Configs;
use App\Models\Onlinetracker;
use App\libraries\providers\ASC;
use App\libraries\Array2XML;
use App\libraries\App;
use Illuminate\Http\Request;
use App\libraries\Login;
use Config;
use DB;
use Cache;
use Log;
use Auth;
use Carbon\Carbon;

class AscController extends MainController{
	
	protected $accounts = array();
	
	protected $profitloss = array();

	public function index(Request $request)
	{
		App::check_process('asc_datapull_process');
		
//		date_default_timezone_set("Asia/Kuala_Lumpur");
		$pullDateTime = Configs::getParam('SYSTEM_ASC_PULLDATE');
		
		if (strlen($pullDateTime) < 1) {
			$pullDateTime = '2016-09-01 00:00:00';
		}
		
		$fromDate = Carbon::parse($pullDateTime);
		$toDate  = Carbon::now();
		
		// Backdate early 10 minutes in case some record is late insert from API service.
		$toDate->addMinutes(-10);
		
		// API only allow max data fetch range of 30 minutes.
		if($toDate->diffInMinutes($fromDate) > 29){
			$toDate = $fromDate->copy()->addMinutes(30);
		}
		
		// Convert dateTime object to formatted string.
		$fromDate = $fromDate->toDateTimeString();
		$toDate = $toDate->toDateTimeString();
		
		$postfields = 'APIPassword='.Config::get($request->currency.'.asc.key').
			'&MemberAccount='. // Blank for pull all members' record.
			'&BetID=0'. // 0 for pull all bet IDs.
			'&Status=2'. // Only pull setteled records.
			'&FromDate='.$fromDate.
			'&ToDate='.$toDate;
		
		$result = simplexml_load_string( App::curlGET( Config::get($request->currency.'.asc.url').'/GetBetSheet?'.$postfields, true, 10) );

		// For testing.
/* 		$result = simplexml_load_string('<?xml version="1.0" encoding="utf-8"?>
						<response>
							<errcode>000000</errcode>
							<errtext></errtext>
							<result>
								<lastdate>2016-08-04 06:44:25</lastdate>
								<betlist>
									<bet>
										<BetID>66939069</BetID>
										<Account>badegg</Account>
										<BetAmount>150.0000</BetAmount>
										<BetOdd>0.8900</BetOdd>
										<Win>133.5000</Win>
										<Comm>0.0000</Comm>
										<BetType>Over/Under</BetType>
										<OddStyle>M</OddStyle>
										<Hdp>1.5000</Hdp>
										<BetPos>Over</BetPos>
										<Live>1</Live>
										<BetDate>2016-08-04 06:15:06</BetDate>
										<Status>Settled</Status>
										<Result>Win</Result>
										<ReportDate>2016-08-03 00:00:00</ReportDate>
										<SportName>Soccer</SportName>
										<LeagueID>17433</LeagueID>
										<HomeID>149290</HomeID>
										<AwayID>49453</AwayID>
										<BetScore>1-0</BetScore>
										<MatchDate>2016-08-04 03:30:00</MatchDate>
										<BetIP>115.164.190.92</BetIP>
										<MatchID>1459323</MatchID>
										<UpdateTime>2016-08-04 06:44:25</UpdateTime>
									</bet>
									<bet>
										<BetID>66937693</BetID>
										<Account>badegg</Account>
										<BetAmount>6.0000</BetAmount>
										<BetOdd>0.9200</BetOdd>
										<Win>-6.0000</Win>
										<Comm>0.0000</Comm>
										<BetType>Full Handicap</BetType>
										<OddStyle>M</OddStyle>
										<Hdp>0.5000</Hdp>
										<BetPos>Away</BetPos>
										<Live>1</Live>
										<BetDate>2016-08-04 05:14:48</BetDate>
										<Status>Settled</Status>
										<Result>Lose</Result>
										<ReportDate>2016-08-03 00:00:00</ReportDate>
										<SportName>Soccer</SportName>
										<LeagueID>10847</LeagueID>
										<HomeID>84244</HomeID>
										<AwayID>75784</AwayID>
										<BetScore>0-1</BetScore>
										<MatchDate>2016-08-04 04:30:00</MatchDate>
										<BetIP>210.186.58.59</BetIP>
										<MatchID>1459277</MatchID>
										<UpdateTime>2016-08-04 06:27:41</UpdateTime>
									</bet>
								</betlist>
							</result>
						</response>
				'); */

		if (isset($result->errcode) && ($result->errcode == '0' || $result->errcode == '000000')) {
			foreach ($result->result->betlist->bet as $bet) {
				// Process only if betId is not exists in DB.
				$count = Betasc::where('betId', '=', $bet->betId)->count();
				
				if ($count < 1) {
					$data = $insertData = array(); // Reset values.
					$dateTimeNow = Carbon::now()->toDateTimeString();
					$data = array(
						'betId'			=> (int) $bet->BetID, // Random generate.
						'account'		=> (string) $bet->Account,
						'betAmount'		=> $bet->BetAmount,
						'betOdd'		=> $bet->BetOdd,
						'win'			=> $bet->Win,
						'comm'			=> $bet->Comm,
						'betType'		=> (string) $bet->BetType,
						'oddStyle'		=> (string) $bet->OddStyle,
						'hdp'			=> $bet->Hdp,
						'betPos'		=> (string) $bet->BetPos,
						'live'			=> (int) $bet->Live,
						'betDate'		=> $bet->BetDate,
						'status'		=> (string) $bet->Status,
						'result'		=> (string) $bet->Result,
						'reportDate'		=> $bet->ReportDate,
						'sportName'		=> (string) $bet->SportName,
						'leagueId'		=> (int) $bet->LeagueID,
						'homeId'		=> (int) $bet->HomeID,
						'awayId'		=> (int) $bet->AwayID,
						'betScore'		=> (string) $bet->BetScore,
						'matchDate'		=> $bet->MatchDate,
						'betIp'			=> (string) $bet->BetIP,
						'matchId'		=> (int) $bet->MatchID,
						'betUpdateTime'		=> $bet->UpdateTime,
						'createdAt'		=> $dateTimeNow,
						'updatedAt'		=> $dateTimeNow,
					);

					foreach ($data as $key => $value) {
						$insertData[$key] = '"'.$value.'"';
					}

					// Test: to ignore non exists account from testing API.
//					if (Account::where( 'nickname' , '=' , $data['account'] )->count() > 0) {
//						if (DB::statement('call insert_asc('.implode(',',$insertData).')')) {
//							$this->InsertWager($data);
//						}
//					}
					
					if (DB::statement('call insert_asc('.implode(',',$insertData).')')) {
						$this->InsertWager($data);
					}
				}
			}
		
			Profitloss::updatePNL2($this->profitloss);
		
            // Get league and team name.
//    		$this->getLeagueName($request);
//    		$this->getTeamName($request);
		}
		
		// Update execution time in Config.
		Configs::updateParam('SYSTEM_ASC_PULLDATE',$toDate);

		App::unlock_process('asc_datapull_process');
	}
	
	public function getLeagueName(Request $request) {

		$url = Config::get($request->currency.'.asc.url').'/GetLeagueName?APIPassword='.Config::get($request->currency.'.asc.key');
		$maxId = Betascleague::max('league_id');
		
		if (is_null($maxId) || $maxId <= 0) {
			$url .= '&LeagueID=0&MinLeagueID=false';
		} else {
			$url .= '&LeagueID='.$maxId.'&MinLeagueID=true';
		}
		
		$result = simplexml_load_string(App::curlGET($url, true, 10, 'getLeague'));
		
		if (isset($result->errcode) && ($result->errcode == '0' || $result->errcode == '000000')) {
			foreach ($result->result->league as $league) {
				if ($league->LeagueID > $maxId) {
					Betascleague::forceCreate(array(
					    'league_id' => $league->LeagueID,
					    'league_name' => $league->EN,
					));
				}
			}
		}

	}
	
	public function getTeamName(Request $request) {
		
		$url = Config::get($request->currency.'.asc.url').'/GetTeamName?APIPassword='.Config::get($request->currency.'.asc.key');
		$maxId = Betascteam::max('team_id');
		
		if (is_null($maxId) || $maxId <= 0) {
			$url .= '&TeamID=0&MinTeamID=false';
		} else {
			$url .= '&TeamID='.$maxId.'&MinTeamID=true';
		}
		
		$result = simplexml_load_string(App::curlGET($url, true, 10, 'getTeam'));
		
		if (isset($result->errcode) && ($result->errcode == '0' || $result->errcode == '000000')) {
			foreach ($result->result->team as $team) {
				if ($team->TeamID > $maxId) {
					Betascteam::forceCreate(array(
					    'team_id' => $team->TeamID,
					    'team_name' => $team->EN,
					));
				}
			}
		}
		
	}

	private function InsertWager( $bet ){
		
		if( !isset($this->accounts[$bet['account']]) )
			$this->accounts[$bet['account']] = Account::where( 'nickname' , '=' , $bet['account'] )->first();
		
		$prdObj = Cache::rememberForever('product_asc_obj', function(){
				return Product::where( 'code' , '=' , 'ASC' )->first();
		});
	
		$data['accid'] 		 		= $this->accounts[$bet['account']]->id;
		$data['acccode'] 	 		= $this->accounts[$bet['account']]->code;
		$data['nickname'] 	 		= $bet['account'];
		$data['wbsid'] 		 		= $this->accounts[$bet['account']]->wbsid;
		$data['prdid'] 		 		= $prdObj->id;
		$data['matchid'] 	 		= $bet['matchId'] == '' ? "''":$bet['matchId'];
		$data['gamename'] 	 		= $bet['sportName'];
		$data['category'] 	 		= Product::getCategory('Sports');
		$data['refid'] 		 		= $bet['betId'];
		$data['crccode'] 	 		= $this->accounts[$bet['account']]->crccode;
		$data['crcrate'] 	 		= Currency::getCurrencyRate($data['crccode']);
		$data['datetime'] 	 		= $bet['betDate'];
		$data['ip'] 		 		= $bet['betIp'];
		$data['payout'] 	 		= (float) ($bet['win'] + $bet['betAmount']);
		$data['profitloss']			= (float) $bet['win'];
		$data['accountdate']		= substr( $bet['reportDate'], 0 , 10 );
		$data['stake']		 		= $bet['betAmount'];
		$data['status'] 			= Wager::STATUS_SETTLED;
		
		
		$validstake = $bet['betAmount'];
		
		if( $data['stake'] == $data['payout'] )
		{	   
			$data['result']  = Wager::RESULT_DRAW;
			$validstake	     = 0;
		}
		else if( $data['stake'] > $data['payout'] )
		{ 
			$data['result']  = Wager::RESULT_LOSS;
			//$validstake	     = $data['payout'];
		}
		else if( $data['stake'] < $data['payout'] ) $data['result']  = Wager::RESULT_WIN;
		
		
		if( $data['stake']/2 == $data['payout']){
			
			$validstake		= $data['stake']/2;
		}
		
		if( $data['profitloss'] > 0 && $data['profitloss'] <= $data['stake']/2 )
		{
			$data['validstake'] 	= $data['stake']/2;	
		}
		
		if( $data['profitloss'] < 0 && ($data['profitloss'] * -1) <= $data['stake']/2 )
		{
			$data['validstake'] 	= $data['stake']/2;	
		}
		
		
		$data['validstake'] 		= $validstake;
		$data['stakelocal'] 		= Currency::getLocalAmount($data['stake'], $data['crccode']);
		$data['payoutlocal']		= Currency::getLocalAmount($data['payout'], $data['crccode']);
		$data['profitlosslocal']	= Currency::getLocalAmount($data['profitloss'], $data['crccode']);
		$data['validstakelocal']	= Currency::getLocalAmount($data['validstake'], $data['crccode']);
		$data['iscalculated'] 	    = 0;
		$data['created']			= date("y-m-d H:i:s");
		$data['modified']			= date("y-m-d H:i:s");
		
		foreach( $data as $key => $value ){
			$insert_data[$key] = '"'. $value . '"';
		}
		

		if(DB::statement('call insert_wager('.implode(',',$insert_data).')'))
		{
			$this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['prdid']   = $data['prdid'];
			$this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['accobj']  = $this->accounts[$bet['account']];
			$this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['crcrate'] = $data['crcrate'];
		}
		

	}
	
	public function doValidateTicket(Request $request){
		
		Log::info('asc');		

		$string = file_get_contents('php://input');
		Log::info($string);	
		$merchantId = '';
		$errorCode  = '201';
		$errorMsg 	= '';
		$messageId  = '';
		$userId 	= '';
		$crccode	= '';
		
		$xml 		= simplexml_load_string($string);
		
		if(isset($xml->Param->Token)){
			$token		 = (string) $xml->Param->Token;
			$merchantId  = (string) $xml->Header->MerchantID;
			$messageId   = (string) $xml->Header->MessageID;
			
			if( $trackerObj = Onlinetracker::whereRaw('token="'.$token.'"')->first() ) 
			{
				$accObj = Account::find($trackerObj->userid);
				
				$errorCode = '0';
				$userId    = $accObj->nickname;
				$crccode   = $accObj->crccode;
				
			}
			
		}
		
		$str = '<?xml version="1.0" encoding="UTF-8"?>
		<Reply>
		  <Header>
			<Method>cMemberAuthentication</Method>
			<ErrorCode>'.$errorCode.'</ErrorCode>
			<MerchantID>'.$merchantId.'</MerchantID>
			<MessageID>'.$messageId.'</MessageID>
		  </Header>
		  <Param>
			<UserID>'.$userId.'</UserID>
			<CurrencyCode>'.$crccode.'</CurrencyCode>
			<ErrorDesc>'.$errorCode.'</ErrorDesc>
		  </Param>
		</Reply>';
		Log::info($str);	

		return response()->view('front/xml', array('name' =>  $str))->header('Content-Type', 'application/xml');	
	}
	
	public function doValidateMobile(Request $request){
		
		$token = '';
		
		$data = json_decode(file_get_contents('php://input'), true);
		
		$mode 	  = (isset($data['Mode'])?$data['Mode']:false);
		$hash	  = (isset($data['Hash'])?$data['Hash']:'');
		$username = (isset($data['Username'])?$data['Username']:false);
		$password = (isset($data['Password'])?$data['Password']:false);
		
		if(($mode && $mode === 'ClientLogin') && ($hash === md5($username.$password.Config::get(Account::whereNickname($username)->pluck('crccode').'.asc.key')))) {
			if($username && $password) {
				if (Auth::user()->attempt(array('nickname' => $username, 'password' => $password , 'status' => 1 , 'istestacc' => 0 )))
				{	
					$login = new Login;
					$user = Auth::user()->get();
					$token = $login->doMakeTracker(Auth::user()->get(), 'Account');

				}
			}
		}
		Log::info($token);
		echo $token;
		
	}
	

}
