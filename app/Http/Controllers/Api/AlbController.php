<?php namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller\Api;
use App\Models\Betalb;
use App\Models\Account;
use App\Models\Product;
use App\Models\Wager;
use App\Models\Currency;
use App\Models\Profitloss;
use App\libraries\providers\ALB;
use Illuminate\Http\Request;
use App\libraries\App;
use Config;
use DB;
use Cache;

class AlbController extends MainController{

	public function index(Request $request)
	{
		
		App::check_process('alb_datapull_process');
		
		$from = $this->getLatestTransactionTime($request->currency);
		
		$postfields = 'vendor_id='.Config::get($request->currency.'.alb.vendor_id').
					  '&operator_id='.Config::get($request->currency.'.alb.operator_id').
					  '&page=1'.
					  '&from='.$from.
					  '&to='.date('Y-m-d').' 23:59:59';

		$result = json_decode( $this->curl( $postfields , Config::get($request->currency.'.alb.agency_url').'getBetDetail' , false ) );

		if( isset($result->error_code) && isset($result->trans) && $result->error_code == "0" && $result->trans != ''){
		
			foreach( $result->trans as $key => $value )
			{
				if( Betalb::where( 'betNum' , '=' , $value->betNum )->count() == 0 )
				{
			
					$albObj = new Betalb;
					$albObj->betAmount 	 		= $value->betAmount;
					$albObj->betNum 	 		= $value->betNum;
					$albObj->betTime	 		= $value->betTime;
					$albObj->betType 	 		= $value->betType;
					$albObj->client 	 		= $value->user_id;
					$albObj->commission  		= $value->commission;
					$albObj->currency 	 		= $value->currency;
					$albObj->exchangeRate		= $value->exchangeRate;
					$albObj->gameResult  		= $value->gameResult;
					$albObj->gameRoundEndTime   = $value->gameRoundEndTime;
					$albObj->gameRoundId 		= $value->gameRoundId;
					$albObj->gameRoundStartTime = $value->gameRoundStartTime;
					$albObj->gameType 			= $value->gameType;
					$albObj->state 				= $value->state;
					$albObj->tableName 			= $value->tableName;
					$albObj->validAmount		= $value->betAmount;
					$albObj->winOrLoss 			= $value->winOrLoss;
					$albObj->save();
		
					$this->InsertWager($albObj, $request->currency);
					
				}
			}
		}
		
		App::unlock_process('alb_datapull_process');
		

	}	
	
	private function getLatestTransactionTime( $currency )
	{
		$date = Betalb::where('currency', '=', $currency)->orderBy('betTime','Desc')->pluck('betTime');
		
		if(!$date)
			
		$date = '2014-07-23 18:00:00';
		
		return $date;

	}
	
	private function InsertWager( $alb, $currency ){
		
		$accObj  = Account::where( 'nickname' , '=' , $alb->client )->where('crccode', '=', $currency)->first();
		$prdObj  = Product::where( 'code' , '=' , 'ALB' )->first();
		
		$wgrObj = Wager::whereRaw(' prdid = '.$prdObj->id.' AND refid = '.$alb->betNum.'')->first();

		if( !$wgrObj ) 
		{
			$wgrObj = new Wager;
			$wgrObj->accid 		 		= $accObj->id;
			$wgrObj->acccode 	 		= $accObj->code;
			$wgrObj->nickname 	 		= $alb->client;
			$wgrObj->wbsid 		 		= $accObj->wbsid;
			$wgrObj->prdid 		 		= $prdObj->id;
			$wgrObj->matchid 	 		= $alb->gameRoundId;
			$wgrObj->gamename 	 		= $alb->tableName;
			$wgrObj->category 	 		= Product::getCategory('Casino');
			$wgrObj->refid 		 		= $alb->betNum;
			$wgrObj->crccode 	 		= $accObj->crccode;
			$wgrObj->crcrate 	 		= Currency::getCurrencyRate($wgrObj->crccode);
			$wgrObj->datetime 	 		= $alb->betTime;
			$wgrObj->ip 		 		= '';
		}
		
		$wgrObj->payout 	 		= (float) ( $alb->betAmount + $alb->winOrLoss );
		$wgrObj->profitloss  		= $alb->winOrLoss;
		$wgrObj->accountdate 		= substr( $alb->betTime, 0 , 10 );
		$wgrObj->stake		 		= $alb->betAmount;
		$wgrObj->status 			= Wager::STATUS_SETTLED;
		
		if( $wgrObj->stake == $wgrObj->payout )	    $wgrObj->result  = Wager::RESULT_DRAW;
		else if( $wgrObj->stake > $wgrObj->payout ) $wgrObj->result  = Wager::RESULT_LOSS;
		else if( $wgrObj->stake < $wgrObj->payout ) $wgrObj->result  = Wager::RESULT_WIN;
                
                if($wgrObj->result  != Wager::RESULT_DRAW)
                    $wgrObj->validstake 		= $alb->validAmount;
                else
                    $wgrObj->validstake 		= 0;
                
		$wgrObj->stakelocal 		= Currency::getLocalAmount($wgrObj->stake, $wgrObj->crccode);
		$wgrObj->payoutlocal		= Currency::getLocalAmount($wgrObj->payout, $wgrObj->crccode);
		$wgrObj->profitlosslocal	= Currency::getLocalAmount($wgrObj->profitloss, $wgrObj->crccode);
		$wgrObj->validstakelocal	= Currency::getLocalAmount($wgrObj->validstake, $wgrObj->crccode);
		$wgrObj->iscalculated 	    = 0;
		if($wgrObj->save()){
			Profitloss::updatePNL($wgrObj,$accObj);
		}
		
	}

}
