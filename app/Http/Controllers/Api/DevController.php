<?php namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller\Api;
use App\Models\Betw88;
use App\Models\Account;
use App\Models\Product;
use App\Models\Accountproduct;
use App\Models\Wager;
use App\Models\Currency;
use App\Models\Profitloss;
use App\Models\Onlinetracker;
use App\Models\Betibc;
use App\Models\Betplt;
use App\Models\Cashledger;
use App\Models\Cashbalance;
use App\Models\Promocampaign;
use App\Models\Promocash;
use App\Models\Luckydrawledger;
use App\Models\Configs;
use App\Models\Balancetransfer;
use App\libraries\providers\W88;
use App\libraries\Array2XML;
use Illuminate\Http\Request;
use Config;
use DB;
use Cache;
use App\libraries\App;

class DevController extends MainController{
	
	protected $accounts = array();

	protected $profitloss = array();
	
	public function updateBalance(Request $request){
		
		App::check_process('updatebalance_'.$request->prdid);
		
		if( $accounts = Accountproduct::wherePrdid($request->prdid)->get() ){
			
			foreach($accounts as $account){
				
				$className = "App\libraries\providers\\".Product::whereId($account->prdid)->pluck('code');
				$obj = new $className;
				
				$balance = $obj->getBalance( $account->acccode, 'MYR');
				
				if( Cashbalance::wherePrdid($account->prdid)->whereAcccode($account->acccode)->count() == 1 ){
					$cashbalanceObj = Cashbalance::wherePrdid($account->prdid)->whereAcccode($account->acccode)->first();
				}else{
					$cashbalanceObj = new Cashbalance;
				}
				
				
				$cashbalanceObj->accid   = Account::whereNickname($account->acccode)->pluck('id');
				$cashbalanceObj->prdid   = $account->prdid;
				$cashbalanceObj->acccode = $account->acccode;
				$cashbalanceObj->crccode = 'MYR';
				$cashbalanceObj->balance = $balance;
				$cashbalanceObj->save();
				
				
			}
			
			
		}
		
		App::unlock_process('updatebalance_'.$request->prdid);
		
	}

	public function index(Request $request)
	{
		
	/* 	if($pnls = Profitloss::wherePrdid(1)->get() ) { 
			foreach($pnls as $pnl) {
				 $condition = 'accid = "'.$pnl->accid.'" AND prdid = '.$pnl->prdid.' AND category = '.$pnl->category.' AND accountdate = "'.$pnl->date.'" ';
	
				$wager = Wager::whereRaw($condition)->count();
				$valid = Wager::whereRaw($condition.' AND validstake > 0')->count(); 
				$totalstake = Wager::whereRaw($condition)->sum('stake');
				$totalvalid =  Wager::whereRaw($condition)->sum('validstake');
				$totalwinloss = Wager::whereRaw($condition)->sum('profitloss');
				
				//$pnl->wager = (int) $wager;
				//$pnl->validwager = (int) $valid;
				//$pnl->totalstake = (float) $totalstake;
				//$pnl->totalstakelocal = Currency::getLocalAmount($pnl->totalstake, $pnl->crccode);
				$pnl->validstake = (float) $totalvalid;
				$pnl->validstakelocal = Currency::getLocalAmount($pnl->validstake, $pnl->crccode);
				//$pnl->amount = (float) $totalwinloss;
				//$pnl->amountlocal = Currency::getLocalAmount($pnl->amount, $pnl->crccode);
				echo $pnl->save();
			}
		}
		
		exit; */
		
	/* 	$this->migratePltWalletToMain($request);
        exit; 
		 */
		
	/* 	if( $wagers = wager::get() ){
			foreach($wagers as $wager) {
				
				if( $wager->profitloss > 0 && $wager->profitloss <= $wager->stake/2 )
				{
					$wager->validstake	      = $wager->stake/2;	
				}
				
				if( $wager->profitloss < 0 && ($wager->profitloss * -1) <= $wager->stake/2 )
				{
					$wager->validstake 		  = $wager->stake/2;	
				}
				
				$wager->save();
				
			}
		} */
		
		$start = 0;
		$limit = 1000;
		$continue = true;
		while($continue){
			if( $ibcs = Betibc::whereCurrency(2)->where( 'winlost_datetime' , '>=' , '2017-06-01 00:00:00' )->take($limit)->skip($start)->get())
			{				
				if(count($ibcs) == 0){
					$continue = false;
				}
				foreach( $ibcs as $ibc )
				{
					$this->InsertWager( $ibc ,'MYR');
					
				}
				Profitloss::updatePNL2($this->profitloss);
				$this->profitloss = array();
				$start += 1000;
			} 
		}
		
		exit;
		

	
		
//        $this->migratePltWalletToMain($request);
//        exit;
        
	/* 	App::check_process('dev_process');
		
		sleep(30);
		
		App::unlock_process('dev_process');
		exit; */
		
/* 		if( $clgs = Cashledger::whereRaw('amount >= 300 and chtcode = 1 and created >= "2016-03-09 18:00:00" and crccode = "myr"')->get() ){
			
			foreach( $clgs as $object ){
				if(  (( $object->amount >= 300 && $object->crccode == 'MYR' ) )){
										$token = floor($object->amount / 300);
										$luckydraw = new Luckydrawledger;
										$luckydraw->draw_no = Configs::getParam('SYSTEM_4D_DRAWNO');
										$luckydraw->accid   = $object->accid;
										$luckydraw->acccode = $object->acccode;
										$luckydraw->amount  = $token;
										$luckydraw->refid   = $object->id;
										$luckydraw->object  = 'Cashledger';
										$luckydraw->save();
				}
			}
			
		}
		
		exit; */
		
		
		
	/*  	if( $profitlosss = Profitloss::whereRaw('prdid = 11 and date >= "2016-02-01" ')->get() )
		{
			
			foreach( $profitlosss as $pnl )
			{
				
				$condition = 'accid = "'.$pnl->accid.'" AND prdid = '.$pnl->prdid.' AND category = '.$pnl->category.' AND accountdate = "'.$pnl->date.'" AND status NOT IN ('.Wager::STATUS_OPEN.', '.Wager::STATUS_REJECTED.', '.Wager::STATUS_REFUND.')';
				
				$pnl->amount	    	 = Wager::whereRaw($condition)->sum('profitloss');
				$pnl->amountlocal	     = Wager::whereRaw($condition)->sum('profitloss');
				$pnl->totalstake	     = Wager::whereRaw($condition)->sum('stake');
				$pnl->totalstakelocal	 = Wager::whereRaw($condition)->sum('stake');
				$pnl->validstake		 = Wager::whereRaw($condition)->sum('validstake');
				$pnl->validstakelocal    = Wager::whereRaw($condition)->sum('validstake');
				$pnl->wager = $pnl->validwager;
				echo $pnl->save();
				
			}
		} 
		exit; */
		
		/*
		$start = 0;
		$limit = 1000;
		$continue = true;
		
		while($continue){
		
			if( $betplts = Betplt::whereRaw('GAMEDATE >= "'.$request->date.' 00:00:00" AND GAMEDATE <= "'.$request->date.' 23:59:59" ')->take($limit)->skip($start)->get() ){
				
				if(count($betplts) == 0){
					$continue = false;
				}
				
				foreach( $betplts as $result ){
					
					$data['GAMECODE']         = $result->GAMECODE;
					$data['Username']         = $result->Username;
					$data['PLAYERNAME']       = $result->PLAYERNAME;
					$data['WINDOWCODE']       = $result->WINDOWCODE;
					$data['GAMEID']           = $result->GAMEID;
					$data['GAMETYPE']         = $result->GAMETYPE;
					$data['GAMENAME']         = $result->GAMENAME;
					$data['SESSIONID']        = $result->SESSIONID;
					$data['BET']      	      = $result->BET;
					$data['WIN']      	      = $result->WIN;
					$data['PROGRESSIVEBET']   = $result->PROGRESSIVEBET;
					$data['PROGRESSIVEWIN']   = $result->PROGRESSIVEWIN;
					$data['BALANCE']   		  = $result->BALANCE;
					$data['CURRENTBET']   	  = $result->CURRENTBET;
					$data['GAMEDATE']   	  = $result->GAMEDATE;
					$data['LIVENETWORK']   	  = $result->LIVENETWORK;
					$data['RNUM']   		  = $result->INFO;
					$data['created']		  = date("y-m-d H:i:s");
					$data['modified']		  = date("y-m-d H:i:s");
					$data['currency']		  = $result->currency;
					
					foreach( $data as $key => $value ){
						$insert_data[$key] = '"'. $value . '"';
					}					

					if(DB::statement('call insert_plt('.implode(',',$insert_data).')')){
						
							$this->InsertWagerPlt($data);
						
					} 
			
				}
				Profitloss::updatePNL2($this->profitloss);
				$this->profitloss = array();
				$start += 1000;
			}
			
		} 
		
		
		$start = 0;
		$limit = 1000;
		$continue = true;
		
		while($continue){
		
			if( $betplts = Betw88::whereRaw('trans_date >= "'.$request->date.' 00:00:00" AND trans_date <= "'.$request->enddate.' 23:59:59" ')->take($limit)->skip($start)->get() ){
				
				if(count($betplts) == 0){
					$continue = false;
				}
				
				 foreach( $betplts as $result ){
					
					$data['GAMECODE']         = $result->GAMECODE;
					$data['Username']         = $result->Username;
					$data['PLAYERNAME']       = $result->PLAYERNAME;
					$data['WINDOWCODE']       = $result->WINDOWCODE;
					$data['GAMEID']           = $result->GAMEID;
					$data['GAMETYPE']         = $result->GAMETYPE;
					$data['GAMENAME']         = $result->GAMENAME;
					$data['SESSIONID']        = $result->SESSIONID;
					$data['BET']      	      = $result->BET;
					$data['WIN']      	      = $result->WIN;
					$data['PROGRESSIVEBET']   = $result->PROGRESSIVEBET;
					$data['PROGRESSIVEWIN']   = $result->PROGRESSIVEWIN;
					$data['BALANCE']   		  = $result->BALANCE;
					$data['CURRENTBET']   	  = $result->CURRENTBET;
					$data['GAMEDATE']   	  = $result->GAMEDATE;
					$data['LIVENETWORK']   	  = $result->LIVENETWORK;
					$data['RNUM']   		  = $result->INFO;
					$data['created']		  = date("y-m-d H:i:s");
					$data['modified']		  = date("y-m-d H:i:s");
					$data['currency']		  = $result->currency;
					
					foreach( $data as $key => $value ){
						$insert_data[$key] = '"'. $value . '"';
					}					

					if(DB::statement('call insert_plt('.implode(',',$insert_data).')')){
						
							$this->InsertWagerPlt($data);
						
					} 
			
				}
				Profitloss::updatePNL2($this->profitloss);
				
				foreach($betplts as $result )
				{


					$data['trans_date'] 	= $result->trans_date; 
					$data['player_hand'] 	= $result->player_hand;
					$data['currency'] 		= $result->currency;	
					$data['game_result'] 	= $result->game_result;
					$data['balance'] 		= $result->balance; 	
					$data['winlose'] 		= $result->winlose; 	
					$data['bet'] 			= $result->bet; 		
					$data['game_type'] 		= $result->game_type; 	
					$data['user_id'] 		= $result->user_id; 	
					$data['status'] 		= $result->status; 	
					$data['table_id']		= $result->table_id;	
					$data['bet_id'] 		= $result->bet_id;	
					$data['trans_id'] 		= $result->trans_id; 	
					$data['bundle_id'] 		= $result->bundle_id; 	
					$data['type'] 			= $result->type; 		
					$data['created']		= date("y-m-d H:i:s");
					$data['modified']		= date("y-m-d H:i:s");	

		
					$this->InsertWagerw88($data,$result->type);
						
							

				

				}
				
				Profitloss::updatePNL2($this->profitloss);
				$this->profitloss = array();
				$start += 1000;
			}
			
		}
		 */
	}	
	
	private function InsertWagerw88( $bet , $type){
		
		if( !isset($this->accounts[$bet['user_id']]) )
			$this->accounts[$bet['user_id']] = Account::where( 'nickname' , '=' , $bet['user_id'] )->first();
		
		$prdObj = Cache::rememberForever('product_w88_obj', function(){
				return Product::where( 'code' , '=' , 'W88' )->first();
		});
	
		$data['accid'] 		 		= $this->accounts[$bet['user_id']]->id;
		$data['acccode'] 	 		= $this->accounts[$bet['user_id']]->code;
		$data['nickname'] 	 		= $bet['user_id'];
		$data['wbsid'] 		 		= $this->accounts[$bet['user_id']]->wbsid;
		$data['prdid'] 		 		= $prdObj->id;
		$data['matchid'] 	 		= $bet['table_id'] == '' ? "''":$bet['table_id'];
		$data['gamename'] 	 		= $bet['game_type'];
		$data['category'] 	 		= $type == 0 ? Product::getCategory('Casino') : Product::getCategory('egames');
		$data['refid'] 		 		= $bet['bet_id'];
		$data['crccode'] 	 		= $this->accounts[$bet['user_id']]->crccode;
		$data['crcrate'] 	 		= Currency::getCurrencyRate($data['crccode']);
		$data['datetime'] 	 		= $bet['trans_date'];
		$data['ip'] 		 		= '';
		$data['payout'] 	 		= $bet['winlose'];
		$data['profitloss']  		= (float) ( $bet['winlose']  -  $bet['bet']);
		$data['accountdate'] 		= substr( $bet['trans_date'], 0 , 10 );
		$data['stake']		 		= $bet['bet'];
		$data['status'] 			= Wager::STATUS_SETTLED;
		
		if( $data['stake'] == $data['payout'] )	    $data['result']  = Wager::RESULT_DRAW;
		else if( $data['stake'] > $data['payout'] ) $data['result']  = Wager::RESULT_LOSS;
		else if( $data['stake'] < $data['payout'] ) $data['result']  = Wager::RESULT_WIN;
		$data['validstake'] 		= $bet['bet'];
		$data['stakelocal'] 		= Currency::getLocalAmount($data['stake'], $data['crccode']);
		$data['payoutlocal']		= Currency::getLocalAmount($data['payout'], $data['crccode']);
		$data['profitlosslocal']	= Currency::getLocalAmount($data['profitloss'], $data['crccode']);
		$data['validstakelocal']	= Currency::getLocalAmount($data['validstake'], $data['crccode']);
		$data['iscalculated'] 	    = 0;
		$data['created']			= date("y-m-d H:i:s");
		$data['modified']			= date("y-m-d H:i:s");
		
		foreach( $data as $key => $value ){
			$insert_data[$key] = '"'. $value . '"';
		}
		

		if(DB::statement('call insert_wager('.implode(',',$insert_data).')'))
		{
			$this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['prdid']   = $data['prdid'];
			$this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['accobj']  = $this->accounts[$bet['user_id']];
			$this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['crcrate'] = $data['crcrate'];
		}
		

	}
	
	private function InsertWager( $ibc ,$currency){

		if( !isset($this->accounts[$ibc->vendor_member_id]) )
			$this->accounts[$ibc->vendor_member_id] = Account::where( 'nickname' , '=' , $ibc->vendor_member_id )->whereCrccode($currency)->first();
		
		$prdObj = Cache::rememberForever('product_ibc_obj', function(){
				return Product::where( 'code' , '=' , 'IBCX' )->first();
		});

		$data['accid'] 		 		= $this->accounts[$ibc->vendor_member_id]->id;
		$data['acccode'] 	 		= $this->accounts[$ibc->vendor_member_id]->code;
		$data['nickname'] 	 		= $ibc->vendor_member_id;
		$data['wbsid'] 		 		= $this->accounts[$ibc->vendor_member_id]->wbsid;
		$data['prdid'] 		 		= $prdObj->id;
		$data['matchid'] 	 		= $ibc->match_id;
		$data['gamename'] 	 		= 'Sports';
		
		if( $ibc->product_id == 1 )
			$data['category'] 	 		= Product::getCategory('sports');		
		if( $ibc->product_id == 3 || $ibc->product_id == 6)
			$data['category'] 	 		= Product::getCategory('casino');	
		if( $ibc->product_id == 4 )
			$data['category'] 	 		= Product::getCategory('number');	
		if( $ibc->product_id == 5 )
			$data['category'] 	 		= Product::getCategory('egames');
		
		$data['refid'] 		 		= $ibc->trans_id;
		$data['crccode'] 	 		= $this->accounts[$ibc->vendor_member_id]->crccode;
		$data['crcrate'] 	 		= Currency::getCurrencyRate($data['crccode']);
		$data['datetime'] 	 		= $ibc->transaction_time;
		$data['ip'] 		 		= '';
	
			
		$data['payout'] 	 		= (float) ( $ibc->stake + $ibc->winlost_amount );
		$data['profitloss']  		= $data['payout'] - $ibc->stake;
		$data['accountdate'] 		= substr( $ibc->winlost_datetime, 0 , 10 );
		$data['stake']		 		= $ibc->stake;
		
		

		if($ibc->ticket_status == 'running') {
			$data['status'] = Wager::STATUS_OPEN;
		}
		else if($ibc->ticket_status == 'Refund') $data['status'] = Wager::STATUS_REFUND;
		else if($ibc->ticket_status == 'Reject') $data['status'] = Wager::STATUS_REJECTED;
		else $data['status'] = Wager::STATUS_SETTLED;
		$data['result'] 	 		= Wager::RESULT_PENDING;
		if($ibc->ticket_status == 'DRAW')	   $data['result']  = Wager::RESULT_DRAW;
		else if($ibc->ticket_status == 'WON')  $data['result']  = Wager::RESULT_WIN;
		else if($ibc->ticket_status == 'LOSE') $data['result']  = Wager::RESULT_LOSS;
		else if($ibc->ticket_status == 'Half WON')  $data['result'] = Wager::RESULT_WINHALF;
		else if($ibc->ticket_status == 'Half LOSE') $data['result'] = Wager::RESULT_LOSSHALF;
		$data['validstake'] 		= $ibc->stake;
		
		if($ibc->ticket_status == 'DRAW') 			$data['validstake'] = 0;
		else if($ibc->ticket_status == 'Half WON')  $data['validstake'] = $ibc->stake/2;
		else if($ibc->ticket_status == 'Half LOSE') $data['validstake'] = $ibc->stake/2;
		
		if( abs($data['profitloss']) < $ibc->stake )
		{
			if( $data['profitloss'] < 0 ){
				$data['validstake'] = $data['profitloss'] * -1;
			}else{
				$data['validstake'] = $data['profitloss'];
			}
			
		}
		
		//if($ibc->ticket_status == 'DRAW') 			$data['validstake'] = 0;
		//else if($ibc->ticket_status == 'Half WON')  $data['validstake'] = $ibc->stake/2;
		//else if($ibc->ticket_status == 'Half LOSE') $data['validstake'] = $ibc->stake/2;
		
		$data['stakelocal'] 		= Currency::getLocalAmount($data['stake'], 		$data['crccode']);
		$data['payoutlocal']		= Currency::getLocalAmount($data['payout'],		$data['crccode']);
		$data['profitlosslocal']	= Currency::getLocalAmount($data['profitloss'], $data['crccode']);
		$data['validstakelocal']	= Currency::getLocalAmount($data['validstake'], $data['crccode']);
		$data['iscalculated']	    = 0;
		$data['created']			= date("y-m-d H:i:s");
		$data['modified']			= date("y-m-d H:i:s");

		foreach( $data as $key => $value ){
			$insert_data[$key] = '"'. $value . '"';
		}
		

		if(DB::statement('call insert_wager('.implode(',',$insert_data).')'))
		{
			$this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['prdid']   = $data['prdid'];
			$this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['accobj']  = $this->accounts[$ibc->vendor_member_id];
			$this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['crcrate'] = $data['crcrate'];
		}
		
		
	}

	private function InsertWagerPlt( $bet ){
		
		if( !isset($this->accounts[$bet['Username']]) )
			$this->accounts[$bet['Username']] = Account::where( 'nickname' , '=' , $bet['Username'] )->first();
		
		$prdObj = Cache::rememberForever('product_plt_obj', function(){
				return Product::where( 'code' , '=' , 'PLT' )->first();
		});
	
		$data['accid'] 		 		= $this->accounts[$bet['Username']]->id;
		$data['acccode'] 	 		= $this->accounts[$bet['Username']]->code;
		$data['nickname'] 	 		= $bet['Username'];
		$data['wbsid'] 		 		= $this->accounts[$bet['Username']]->wbsid;
		$data['prdid'] 		 		= $prdObj->id;
		$data['matchid'] 	 		= $bet['GAMEID'];
		$data['gamename'] 	 		= $bet['GAMETYPE'];
		$data['category'] 	 		= $bet['GAMETYPE'] == 'Slot Machines' ? Product::getCategory('egames') : Product::getCategory('Casino');
		$data['refid'] 		 		= $bet['GAMECODE'];
		$data['crccode'] 	 		= $this->accounts[$bet['Username']]->crccode;
		$data['crcrate'] 	 		= Currency::getCurrencyRate($data['crccode']);
		$data['datetime'] 	 		= $bet['GAMEDATE'];
		$data['ip'] 		 		= '';
		if(  $bet['BET'] == 0 )	
			$data['payout'] = 0;
		else
			$data['payout'] 	 	= $bet['WIN'];
		$data['profitloss']  		= $bet['WIN'] - $bet['BET'];
		$data['accountdate'] 		= substr( $bet['GAMEDATE'], 0 , 10 );
		$data['stake']		 		= $bet['BET'];
		$data['status'] 			= Wager::STATUS_SETTLED;
		
		if( $data['stake'] == $data['payout'] )	    $data['result']  = Wager::RESULT_DRAW;
		else if( $data['stake'] > $data['payout'] ) $data['result']  = Wager::RESULT_LOSS;
		else if( $data['stake'] < $data['payout'] ) $data['result']  = Wager::RESULT_WIN;
		$data['validstake'] 		= $bet['BET'];
		$data['stakelocal'] 		= Currency::getLocalAmount($data['stake'], $data['crccode']);
		$data['payoutlocal']		= Currency::getLocalAmount($data['payout'], $data['crccode']);
		$data['profitlosslocal']	= Currency::getLocalAmount($data['profitloss'], $data['crccode']);
		$data['validstakelocal']	= Currency::getLocalAmount($data['validstake'], $data['crccode']);
		$data['iscalculated'] 	    = 0;
		$data['created']			= date("y-m-d H:i:s");
		$data['modified']			= date("y-m-d H:i:s");
		
		foreach( $data as $key => $value ){
			$insert_data[$key] = '"'. $value . '"';
		}
		

		if(DB::statement('call insert_wager('.implode(',',$insert_data).')'))
		{
			$this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['prdid']   = $data['prdid'];
			$this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['accobj']  = $this->accounts[$bet['Username']];
			$this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['crcrate'] = $data['crcrate'];
		}
		
		
	}
    
    public function migratePltWalletToMain(Request $request) {
        // Check every PLT member's wallet, if have remaining credit then
        // transfer to their main wallet.
        // After transfering need to suspend member PLT account.
        
        $currency = 'IDR';
        $lastAccountId = Cache::get('TEMP_PLT_MIGRATE_LAST_ACCOUNT', 0);
        //$pltPrefix = Config::get($currency.'.plt.prefix');
        
        // Get product ID by code.
        $rows =Balancetransfer::whereRaw('`to` = 12  group by accid')->get();
        //$rows = Balancetransfer::whereRaw('`acccode` = "if_pkchan1987" group by accid')->get();

                foreach ($rows as $r) {
              
						
		
						$accObj = Account::find($r->accid);
						 
                        $plt = new \App\libraries\providers\OSG();
                        $balance = $plt->getBalance($accObj->nickname, $currency);
						
					
						// $balance = 10;
                        if ($balance > 0) {
                            // Have remaining balance at PLT, transfer to main wallet.
                            // Copy from App\Http\Controllers\User\WalletController.
                            
                            $bltObj = new \App\Models\Balancetransfer();
                            $bltObj->accid 			= $r->accid;
                            $bltObj->acccode 		= $r->acccode;
                            $bltObj->from 			= 12;
                            $bltObj->to 			= 0;
                            $bltObj->crccode		= $currency;
                            $bltObj->crcrate		= Currency::getCurrencyRate($bltObj->crccode);
                            $bltObj->amount 		= $balance;
                            $bltObj->amountlocal 	= Currency::getLocalAmount($bltObj->amount, $bltObj->crccode);
                            $bltObj->status			= CBO_STANDARDSTATUS_SUSPENDED;
                            
                            if ($bltObj->save()) {
                                $productwalletObj = new \App\Models\Productwallet();
                                $productwalletObj->prdid 		= $bltObj->from;
                                $productwalletObj->accid 		= $bltObj->accid;
                                $productwalletObj->acccode 		= $bltObj->acccode;
                                $productwalletObj->chtcode 		= CBO_CHARTCODE_BALANCETRANSFER;
                                $productwalletObj->crccode 		= $bltObj->crccode;
                                $productwalletObj->crcrate 		= $bltObj->crcrate;
                                $productwalletObj->amount		= abs($bltObj->amount) * -1;
                                $productwalletObj->amountlocal	= abs($bltObj->amountlocal) * -1;
                                $productwalletObj->refobj		= 'Balancetransfer';
                                $productwalletObj->refid		= $bltObj->id;

                                if( $productwalletObj->save() )
                                {
    //                                $className  = "App\libraries\providers\\".$request->get('from');
    //                                $providerObj = new $className;
                                    $providerObj = $plt;

                                    if( $providerObj->withdraw( $accObj->nickname , $currency , $productwalletObj->amount * -1, $productwalletObj->id ) )
                                    {
                                        $cashLedgerObj = new Cashledger();
                                        $cashLedgerObj->accid 			= $bltObj->accid;
                                        $cashLedgerObj->acccode 		= $bltObj->acccode;
                                        $cashLedgerObj->accname			= $accObj->fullname;
                                        $cashLedgerObj->chtcode 		= CBO_CHARTCODE_BALANCETRANSFER;
                                        $cashLedgerObj->cashbalance 	= $balance;
                                        $cashLedgerObj->amount 			= $bltObj->amount;
                                        $cashLedgerObj->amountlocal 	= $bltObj->amountlocal;
                                        $cashLedgerObj->refid 			= $bltObj->id;
                                        $cashLedgerObj->status 			= CBO_LEDGERSTATUS_CONFIRMED;
                                        $cashLedgerObj->refobj 			= 'Balancetransfer';
                                        $cashLedgerObj->crccode 		= $currency;
                                        if( $cashLedgerObj->save() )
                                        {
                                            $productwalletObj->status = CBO_LEDGERSTATUS_CONFIRMED;
                                            /* if( $fromlog = Apilog::whereAccid(Session::get('userid'))->whereMethod('transferCredit')->orderBy('id','desc')->pluck('return') )
                                            {
                                                $bltObj->fromlog = $fromlog;
                                            } */
                                            $bltObj->status     = CBO_LEDGERSTATUS_CONFIRMED;
                                            $productwalletObj->save();	
                                            $bltObj->save();		  	  
                                            $success = true;
                                        }
										
									/* 	$password = $accObj->enpassword;
											
											if (strlen($password)) {
												$password = \Crypt::decrypt($password);
											}
											$parameterStr  = "player/update";
											$parameterStr .= "/playername/" . $pltPrefix . strtoupper($accObj->nickname);
											$parameterStr .= "/frozen/1";
											$parameterStr .= "/suspended/1";
											$parameterStr .= "/password/" . $password;

											var_dump(json_decode(\App\libraries\providers\PLT::CallFunction($parameterStr, $currency, 'suspendAccount'))); 	
										
										 	
											
											 */
										
											
                                    }
                                    else{
                                        $productwalletObj->status = CBO_LEDGERSTATUS_CANCELLED;
                                        $bltObj->status     	  = CBO_STANDARDSTATUS_SUSPENDED;
                                        /* if( $fromlog = Apilog::whereAccid(Session::get('userid'))->whereMethod('transferCredit')->orderBy('id','desc')->pluck('return') )
                                        {
                                            $bltObj->fromlog = $fromlog;
                                        } */
                                        $bltObj->save();		  	  
                                        $productwalletObj->save();	
                                        $success = false;
                                    }

                                }
                            }
                        }
							
									
                 
                        
                 
				}
				
				
				
    }
	
	
	
	public function addBonus(Request $request){
		
		if($request->auth == '30AF07A30911')
		{
		
			$pcpObj = Promocampaign::whereCode($request->bonuscode)->first();
			
			$accObj = Account::whereRaw(' nickname = "'.$request->username.'" ')->whereCrccode('MYR')->first();
				
			$promoCashObj = new Promocash;
			$promoCashObj->type			= Promocash::TYPE_MANUAL;
			$promoCashObj->accid	 	= $accObj->id;
			$promoCashObj->acccode	 	= $accObj->code;
			$promoCashObj->crccode	 	= $accObj->crccode;
			$promoCashObj->crcrate	 	= Currency::getCurrencyRate($accObj->crccode);
			$promoCashObj->amount	 	= $request->amount;
			$promoCashObj->amountlocal 	= Currency::getLocalAmount($request->amount, $accObj->crccode);
			$promoCashObj->pcpid		= $pcpObj->id;
			$promoCashObj->status		= Promocash::STATUS_PENDING;
			$promoCashObj->createdip	= App::getRemoteIp();
			
			if($promoCashObj->save()) {
				$cashLedgerObj = new Cashledger;
				$cashLedgerObj->accid 			= $promoCashObj->accid;
				$cashLedgerObj->acccode 		= $promoCashObj->acccode;
				$cashLedgerObj->accname 		= $accObj->fullname;
				$cashLedgerObj->chtcode 		= CBO_CHARTCODE_BONUS;
				$cashLedgerObj->remarkcode 		= '';
				$cashLedgerObj->amount 			= $promoCashObj->amount;
				$cashLedgerObj->amountlocal 	= $promoCashObj->amountlocal;
				$cashLedgerObj->fee 			= $promoCashObj->amount;
				$cashLedgerObj->feelocal 		= $promoCashObj->amountlocal;
				$cashLedgerObj->crccode 		= $promoCashObj->crccode;
				$cashLedgerObj->crcrate 		= $promoCashObj->crcrate;
				$cashLedgerObj->refobj			= 'Promocash';
				$cashLedgerObj->refid			= $promoCashObj->id;
				$cashLedgerObj->cashbalance		= $cashLedgerObj->getBalance($promoCashObj->accid);
				$cashLedgerObj->status			= CBO_LEDGERSTATUS_PENDING;
				$cashLedgerObj->createdip		= $promoCashObj->createdip;
				if($cashLedgerObj->save()){
					
					echo json_encode( array( 'code' => '1'));
					exit;
				}
				
			}
		
		
		}
		echo json_encode( array( 'code' => '0'));
	}
	

}
