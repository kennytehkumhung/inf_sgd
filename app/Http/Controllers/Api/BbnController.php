<?php namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller\Api;
use App\Models\Betbbn;
use Illuminate\Http\Request;

class BbnController extends MainController{
	
	private $apiurl      	 		= 'http://888.m88788.com/app/WebService/JSON/display.php?';
	
	private $website     	 		= 'LWIN999';
	
	private $Uppername   	  		= 'dav01';
	
	private $KeyB_BetRecord   		= 'wIPOb81es7';
	
	private $KeyB_Transfer    		= '53IkD3JMon';
	
	private $KeyB_CheckUserBalance  = 'F7rhvnElc';
	
	private $KeyB_Logout   		    = '2c4URy4';
	
	private $KeyB_Login    			= 'jVT56kw';
	
	private $KeyB_CreateMember    	= 'qYI0s9qmp';

	public function index(Request $request)
	{
		

		$postfields = "website=" . $this->website .
					  "&uppername=" . $this->Uppername .
					  "&rounddate=" . date('Y-m-d') .
					  "&starttime=00:00:00" .
					  "&endtime=23:59:59" .
					  "&gamekind=" . $request->input('gamekind') .
					  "&gametype=" . $request->input('gametype') .
					  "&page=1" .
                      "&pagelimit=100" .
					  "&key=" . rand(0, 9) . MD5( $this->website . $this->KeyB_BetRecord . date('Y-m-d' , strtotime('-4hours'))  ) . rand(1000000, 9999999);		  
					  
					  

		$result = json_decode( $this->curl( $postfields , $this->apiurl ) );
		
	 	if( isset($result->result) && $result->result == true )
		{
			
			foreach($result->data as $key => $value )
			{
				$this->addRecord($value);
			}
			
			if( $result->pagination->TotalPage > 1 )
			{
				
				for( $i = 2 ; $i <= $result->pagination->TotalPage ; $i++ )
				{
					
					$postfields = "website=" . $this->website .
								  "&uppername=" . $this->Uppername .
								  "&rounddate=" . date('Y-m-d') .
								  "&starttime=00:00:00" .
								  "&endtime=23:59:59" .
								  "&gamekind=" . $request->input('gamekind') .
								  "&gametype=" . $request->input('gametype') .
								  "&page=" . $i . 
								  "&pagelimit=100" .
								  "&key=" . rand(0, 9) . MD5( $this->website . $this->KeyB_BetRecord . date('Y-m-d' , strtotime('-4hours'))  ) . rand(1000000, 9999999);		  
					  
					  

					$result = json_decode( $this->curl( $postfields , $this->apiurl ) );
					
					foreach( $result->data as $key => $value )
					{
						$this->addRecord($value);
					}
				}
			} 
		
		} 
	}	
	
	private function addRecord($value)
	{
		
		if( Betbbn::where( 'WagersID' , '=' , $value->WagersID )->count() == 0 )
		{
	
			$object = new Betbbn;
			$object->WagersID 	        =  isset($value->WagersID) 		 ? $value->WagersID : '';
			$object->WagersDate         =  isset($value->WagersDate)	 ? $value->WagersDate : '';
			$object->GameType 	        =  isset($value->GameType) 		 ? $value->GameType : '';
			$object->Result 	        =  isset($value->Result) 		 ? $value->Result : '';
			$object->BetAmount 	        =  isset($value->BetAmount) 	 ? $value->BetAmount : '';
			$object->Payoff 	        =  isset($value->Payoff) 		 ? $value->Payoff : '';
			$object->Currency 	        =  isset($value->Currency) 		 ? $value->Currency : '';
			$object->Commissionable 	=  isset($value->Commissionable) ? $value->Commissionable : '';
			$object->SerialID 			=  isset($value->SerialID)		 ? $value->SerialID : '';
			$object->RoundNo 			=  isset($value->RoundNo)		 ? $value->RoundNo : '';
			$object->GameCode 			=  isset($value->GameCode)		 ? $value->GameCode : '';
			$object->ResultType 		=  isset($value->ResultType)	 ? $value->ResultType : '';
			$object->Card 				=  isset($value->Card)		 	 ? $value->Card : '';
			$object->ExchangeRate 		=  isset($value->ExchangeRate)	 ? $value->ExchangeRate : '';
			$object->Origin 			=  isset($value->Origin)		 ? $value->Origin : '';
			$object->IsPaid 			=  isset($value->IsPaid)		 ? $value->IsPaid : '';
			$object->UPTIME 			=  isset($value->UPTIME)		 ? $value->UPTIME : '';
			$object->ModifiedDate 		=  isset($value->ModifiedDate)	 ? $value->ModifiedDate : '';
			$object->save();

		}
		
	}
	

}
