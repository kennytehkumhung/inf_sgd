<?php namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller\Api;
use App\Models\Betgdx;
use App\Models\Account;
use App\Models\Product;
use App\Models\Wager;
use App\Models\Currency;
use App\Models\Profitloss;
use App\Models\Onlinetracker;
use App\libraries\providers\GDX;
use App\libraries\Array2XML;
use App\libraries\App;
use Illuminate\Http\Request;
use App\libraries\Login;
use Config;
use DB;
use Cache;
use Log;
use Auth;
use Carbon\Carbon;

class GdxController extends MainController{
	
	protected $accounts = array();
	
	protected $profitloss = array();

	public function index(Request $request)
	{
		App::check_process('gdx_datapull_process');

		date_default_timezone_set("Asia/Kuala_Lumpur");
		$continue = false;
		$index = 0;
		$offset = 500; // Maximum records per request (from Docs).
                $fromTime = Carbon::create(2016, 8, 1, 0, 0, 0)->format('m/d/Y H:i:s'); // Default starting date.
		$toTime = Carbon::now()->setTime(23, 59, 59)->format('m/d/Y H:i:s');
		
		// Get last fetch date from DB.
		$result = Betgdx::orderBy('balanceTime', 'desc')->first();
		
		if ($result) {
			$fromTime = Carbon::createFromFormat('Y-m-d H:i:s', $result->balanceTime)
				->addSecond() // Additional 1 second to get next records.
				->format('m/d/Y H:i:s');
		}
		
		do {
			$continue = false; // Reset loop flag.

			// Generate message ID.
			$date = date('ymdHis');
			$random = App::generateNum(5);
			$messageId = 'H'.$date.$random;

			$header[] = 'DataType: XML';
			$header[] = 'Content-Type: text/xml';

			$return  = '<Header>';
			$return .= '<Method>cGetBetHistory</Method>';
			$return .= '<MerchantID>'.Config::get($request->currency.'.gdx.operator_code').'</MerchantID>';
			$return .= '<MessageID>'.$messageId.'</MessageID>';
			$return .= '</Header>';
			$return .= '<Param>';
			$return .= '<FromTime>'.$fromTime.'</FromTime>';
			$return .= '<ToTime>'.$toTime.'</ToTime>';
			$return .= '<Index>'.$index.'</Index>';
			$return .= '<ShowBalance>1</ShowBalance>';
			$return .= '</Param>';

			$postfields = '<?xml version="1.0" encoding="UTF-8"?>
					<Request>
						'.$return.'
					</Request>';
			
			// For testing.
/* 		$result = simplexml_load_string('<?xml version="1.0"?>
						<Reply>
						    <Header>
							<Method>cGetBetHistory</Method>
							<ErrorCode>0</ErrorCode>
							<MerchantID>369B</MerchantID>
							<MessageID>H160801143400n0f27</MessageID>
						    </Header>
						    <Param>
						    <TotalRecord>2</TotalRecord>
						    <ErrorDesc></ErrorDesc>
						    <BetInfo>
							<No>0</No>
							<UserID>badegg</UserID>
							<BetTime>08/01/2016 13:50:08</BetTime>
							<BalanceTime>08/01/2016 13:50:52</BalanceTime>
							<ProductID>Baccarat</ProductID>
							<GameInterface>Traditional</GameInterface>
							<BetID>d4725b9e-4080-4001-bfc7-8a9b5e1f4e3b</BetID>
							<BetType>single</BetType>
							<BetAmount>50</BetAmount>
							<WinLoss>0</WinLoss>
							<BetResult>Loss</BetResult>
							<StartBalance>950</StartBalance>
							<EndBalance>1000</EndBalance>
							<BetArrays>
							    <Bet>
								<GameID>B11608011532</GameID>
								<SubBetType>Banker</SubBetType>
								<GameResult>P HEART 10 DIAMOND 4 HEART 8 B SPADE 5 DIAMOND 5 DIAMOND K</GameResult>
								<WinningBet>Player, BankerPair, Big</WinningBet>
								<TableID>B1</TableID>
								<DealerID>367</DealerID>
							    </Bet>
							</BetArrays>
						    </BetInfo>
						    <BetInfo>
							<No>1</No>
							<UserID>badegg</UserID>
							<BetTime>08/01/2016 13:50:08</BetTime>
							<BalanceTime>08/01/2016 13:50:52</BalanceTime>
							<ProductID>Baccarat</ProductID>
							<GameInterface>Traditional</GameInterface>
							<BetID>b1f8dbd7-12c9-4e59-a6ba-05cf117f07a4</BetID>
							<BetType>single</BetType>
							<BetAmount>50</BetAmount>
							<WinLoss>100</WinLoss>
							<BetResult>Win</BetResult>
							<StartBalance>1000</StartBalance>
							<EndBalance>1000</EndBalance>
							<BetArrays>
							    <Bet>
								<GameID>B11608011532</GameID>
								<SubBetType>Player</SubBetType>
								<GameResult>P HEART 10 DIAMOND 4 HEART 8 B SPADE 5 DIAMOND 5 DIAMOND K</GameResult>
								<WinningBet>Player, BankerPair, Big</WinningBet>
								<TableID>B1</TableID>
								<DealerID>367</DealerID>
							    </Bet>
							</BetArrays>
						    </BetInfo>
						    </Param>
						</Reply>
					'); */

			$result = simplexml_load_string( App::curlXML( $postfields , Config::get($request->currency.'.gdx.url'), true, 30 , '', $header) );

			if( isset($result->Header->ErrorCode) && $result->Header->ErrorCode == '0'){
				$data = $subData = $insertData = array(); // Reset values.
				
				foreach ($result->Param->BetInfo as $bi) {
					$dateTimeNow = Carbon::now()->format('Y-m-d H:i:s');
					$data = array(
						'message_id'		=> (string) $result->Header->MessageID,
						'no'			=> (int) $bi->No,
						'userId'		=> (string) $bi->UserID,
						'betTime'		=> Carbon::createFromFormat('m/d/Y H:i:s', $bi->BetTime)->toDateTimeString(),
						'balanceTime'		=> Carbon::createFromFormat('m/d/Y H:i:s', $bi->BalanceTime)->toDateTimeString(),
						'productId'		=> (string) $bi->ProductID,
						'gameInterface'		=> (string) $bi->GameInterface,
						'betId'			=> (string) $bi->BetID,
						'betType'		=> (string) $bi->BetType,
						'betAmount'		=> $bi->BetAmount,
						'winLoss'		=> $bi->WinLoss,
						'betResult'		=> (string) $bi->BetResult,
						'startBalance'		=> (string) $bi->StartBalance,
						'endBalance'		=> (string) $bi->EndBalance,
					);
					
					foreach ($bi->BetArrays->Bet as $ba) {
						$data = array_merge($data, array(
							'gameId'	=> (string) $ba->GameID,
							'subBetType'	=> (string) $ba->SubBetType,
							'gameResult'	=> (string) $ba->GameResult,
							'winningBet'	=> (string) $ba->WinningBet,
							'tableId'	=> (string) $ba->TableID,
							'dealerId'	=> (int) $ba->DealerID,
						));
						
						break; // Get only first data from loop is enough.
					}
					
					$data['createdAt'] = $dateTimeNow;
					$data['updatedAt'] = $dateTimeNow;

					foreach ($data as $key => $value) {
						$insertData[$key] = '"'.$value.'"';
					}
					
					if (DB::statement('call insert_gdx('.implode(',',$insertData).',@newId)')) {
						// Get new inserted ID.
						$res = DB::select('SELECT @newId as newId');
						$data['id'] = $res[0]->newId;

						$this->InsertWager($data);
					}
				}

				// Check if have more records.
				if ($result->Param->TotalRecord >= 500) {
					if ($index <= 0) {
						$index = $offset + 1;
					} else {
						$index += $offset;
					}

					$continue = true; // Proceed next loop.
				}
			}
		} while ($continue);

		Profitloss::updatePNL2($this->profitloss);

		App::unlock_process('gdx_datapull_process');

	}	

	private function InsertWager( $bet ){
		
		if( !isset($this->accounts[$bet['userId']]) )
			$this->accounts[$bet['userId']] = Account::where( 'nickname' , '=' , $bet['userId'] )->first();
		
		$prdObj = Cache::rememberForever('product_gdx_obj', function(){
				return Product::where( 'code' , '=' , 'GDX' )->first();
		});
	
		$data['accid'] 		 		= $this->accounts[$bet['userId']]->id;
		$data['acccode'] 	 		= $this->accounts[$bet['userId']]->code;
		$data['nickname'] 	 		= $bet['userId'];
		$data['wbsid'] 		 		= $this->accounts[$bet['userId']]->wbsid;
		$data['prdid'] 		 		= $prdObj->id;
		$data['matchid'] 	 		= $bet['tableId'] == '' ? "''":$bet['tableId'];
		$data['gamename'] 	 		= $bet['productId'];
		$data['category'] 	 		= Product::getCategory('Casino');
		$data['refid'] 		 		= $bet['id'];
		$data['crccode'] 	 		= $this->accounts[$bet['userId']]->crccode;
		$data['crcrate'] 	 		= Currency::getCurrencyRate($data['crccode']);
		$data['datetime'] 	 		= $bet['balanceTime'];
		$data['ip'] 		 		= '';
		$data['payout'] 	 		= (float) ( $bet['winLoss'] - $bet['betAmount']);
		$data['profitloss']			= (float) $bet['winLoss'];
		$data['accountdate']			= substr( $bet['balanceTime'], 0 , 10 );
		$data['stake']		 		= $bet['betAmount'];
		$data['status'] 			= Wager::STATUS_SETTLED;
		

		if( $data['stake'] == $data['payout'] )	    $data['result']  = Wager::RESULT_DRAW;
		else if( $data['stake'] > $data['payout'] ) $data['result']  = Wager::RESULT_LOSS;
		else if( $data['stake'] < $data['payout'] ) $data['result']  = Wager::RESULT_WIN;
		$data['validstake'] 		= $bet['betAmount'];
		$data['stakelocal'] 		= Currency::getLocalAmount($data['stake'], $data['crccode']);
		$data['payoutlocal']		= Currency::getLocalAmount($data['payout'], $data['crccode']);
		$data['profitlosslocal']	= Currency::getLocalAmount($data['profitloss'], $data['crccode']);
		$data['validstakelocal']	= Currency::getLocalAmount($data['validstake'], $data['crccode']);
		$data['iscalculated'] 	    = 0;
		$data['created']			= date("y-m-d H:i:s");
		$data['modified']			= date("y-m-d H:i:s");
		
		foreach( $data as $key => $value ){
			$insert_data[$key] = '"'. $value . '"';
		}
		

		if(DB::statement('call insert_wager('.implode(',',$insert_data).')'))
		{
			$this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['prdid']   = $data['prdid'];
			$this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['accobj']  = $this->accounts[$bet['userId']];
			$this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['crcrate'] = $data['crcrate'];
		}
		

	}
	
	public function doValidateTicket(Request $request){
		
		Log::info('gdx');		
			
		$string = file_get_contents('php://input');
		Log::info($string);	
		$merchantId = '';
		$errorCode  = '201';
		$errorMsg 	= '';
		$messageId  = '';
		$userId 	= '';
		$crccode	= '';
		
		$xml 		= simplexml_load_string($string);
		
		if(isset($xml->Param->Token)){
			$token		 = (string) $xml->Param->Token;
			$merchantId  = (string) $xml->Header->MerchantID;
			$messageId   = (string) $xml->Header->MessageID;
			
			if( $trackerObj = Onlinetracker::whereRaw('token="'.$token.'"')->first() ) 
			{
				$accObj = Account::find($trackerObj->userid);
				
				$errorCode = '0';
				$userId    = $accObj->nickname;
				$crccode   = $accObj->crccode;
				
			}
			
		}
		
		$str = '<?xml version="1.0" encoding="UTF-8"?>
		<Reply>
		  <Header>
			<Method>cMemberAuthentication</Method>
			<ErrorCode>'.$errorCode.'</ErrorCode>
			<MerchantID>'.$merchantId.'</MerchantID>
			<MessageID>'.$messageId.'</MessageID>
		  </Header>
		  <Param>
			<UserID>'.$userId.'</UserID>
			<CurrencyCode>'.$crccode.'</CurrencyCode>
			<ErrorDesc>'.$errorCode.'</ErrorDesc>
		  </Param>
		</Reply>';
		Log::info($str);	

		return response()->view('front/xml', array('name' =>  $str))->header('Content-Type', 'application/xml');	
	}
	
	public function doValidateMobile(Request $request){
		
		$token = '';
		
		$data = json_decode(file_get_contents('php://input'), true);
		
		$mode 	  = (isset($data['Mode'])?$data['Mode']:false);
		$hash	  = (isset($data['Hash'])?$data['Hash']:'');
		$username = (isset($data['Username'])?$data['Username']:false);
		$password = (isset($data['Password'])?$data['Password']:false);
		
		if(($mode && $mode === 'ClientLogin') && ($hash === md5($username.$password.Config::get(Account::whereNickname($username)->pluck('crccode').'.gdx.key')))) {
			if($username && $password) {
				if (Auth::user()->attempt(array('nickname' => $username, 'password' => $password , 'status' => 1 , 'istestacc' => 0 )))
				{	
					$login = new Login;
					$user = Auth::user()->get();
					$token = $login->doMakeTracker(Auth::user()->get(), 'Account');

				}
			}
		}
		Log::info($token);
		echo $token;
		
	}
	

}
