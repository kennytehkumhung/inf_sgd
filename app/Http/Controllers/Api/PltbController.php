<?php namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller\Api;
use App\Models\Betpltb;
use App\Models\Account;
use App\Models\Product;
use App\Models\Currency;
use App\Models\Profitloss;
use App\Models\Wager;
use App\Models\Configs;
use App\libraries\providers\PLTB;
use Illuminate\Http\Request;
use App\libraries\App;
use Config;
use DB;
use Cache;
use Carbon\Carbon;

class PltbController extends MainController{
	
	protected $accounts = array();
	
	protected $profitloss = array();
	
	public function index(Request $request)
	{
		App::check_process('pltb_datapull_process_'.$request->currency);

		$page	  = $request->has('page')       ? $request->page : 1;
		$minute	  = $request->has('minute')     ? $request->minute : 10;
		$startdate = $request->has('startdate') ? $request->startdate : Configs::getParam('SYSTEM_PLTB_PULLDATE_'.$request->currency);
        $startdate = Carbon::parse($startdate);
		
		if( !$request->has('startdate') && $startdate->diffInHours() > 1 ){
			$startdate = Carbon::now()->addHour(-1);
		
		}
		
		$todate   = $startdate->copy()->addMinutes($minute);
		
		$continue = true;
		
			
		
		$startdate = $startdate->format('Y-m-d H.i.s');
		$todate    = $todate->format('Y-m-d H.i.s');
		
		var_dump($startdate);
	
	
		while($continue)
		{
			$parameterStr  = "report/getbetlog";
			$parameterStr .= "/startdate/" . str_replace(' ', '%20', $startdate);
			$parameterStr .= "/enddate/" . str_replace(' ', '%20', $todate);
			$parameterStr .= "/page/" . $page;
			$parameterStr .= "/producttype/0";
			$parameterStr .= "/currency/" . $request->currency;
			//var_dump($parameterStr);
			$results = json_decode($this->callFunction($parameterStr, null, $request->currency, 'get', ''));
			//var_dump($results);
	 		if( isset($results->Result) )
			{
			    $currencyRate = ($request->currency == 'IDR' ? 1000 : 1);

				foreach( $results->Result as $key => $result ){

				    $username = $result->PlayerName;

				    if (!str_contains($username, '_')) {
				        // PLTB username should always contain prefix.
                        continue;
                    } else {
                        $temp = explode('_',$username);
                        $username = $temp[1];
                    }
					
					$data['playerName']       = $result->PlayerName;
					$data['username']         = $username;
					$data['windowCode']       = $result->WindowCode;
					$data['gameId']           = $result->GameId;
					$data['gameCode']         = $result->GameCode;
					$data['gameType']         = $result->GameType;
					$data['gameName']         = $result->GameName;
					$data['sessionId']        = $result->SessionId;
//					$data['bet']      	 	  = (($request->currency == 'IDR')? ((float) $result->Bet /1000):(float) $result->Bet);
//					$data['win']      	 	  = (($request->currency == 'IDR')? ((float) $result->Win /1000):(float) $result->Win);
//					$data['progressiveBet']   = (($request->currency == 'IDR')? ((float) $result->ProgressiveBet /1000): (float)$result->ProgressiveBet);
//					$data['progressiveWin']   = (($request->currency == 'IDR')? ((float) $result->ProgressiveWin /1000): (float)$result->ProgressiveWin);
					$data['bet']      	 	  = ((float) $result->Bet) / $currencyRate;
					$data['win']      	 	  = ((float) $result->Win) / $currencyRate;
					$data['progressiveBet']   = ((float)$result->ProgressiveBet) / $currencyRate;
					$data['progressiveWin']   = ((float)$result->ProgressiveWin) / $currencyRate;
					$data['currency']   	  = $request->currency;
					$data['balance']   	  = $result->Balance;
					$data['currentBet']   	  = $result->CurrentBet;
					$data['gameDate']   	  = $result->GameDate;
					$data['liveNetwork']   	  = $result->LiveNetwork;
					$data['rNum']   	  = $result->RNum;
					$data['created_at']	  = date("y-m-d H:i:s");
					$data['updated_at']	  = date("y-m-d H:i:s");
					
					foreach( $data as $key => $value ){
						$insert_data[$key] = '"'. $value . '"';
					}					

                    if(DB::statement('call insert_pltb('.implode(',',$insert_data).')')) {
//                        if ($data['bet'] > 0.000000 && $data['win'] > 0.000000) {
                            $this->InsertWager($data, $request->currency);

//                        }
                    }
					
				}
				
				if( $gamedate = Betpltb::where( 'currency' , '=' , $request->currency )->orderBy('gameDate','desc')->pluck('gameDate') ){
				
					Configs::updateParam( 'SYSTEM_PLTB_PULLDATE_'.$request->currency , $gamedate );
					
				}
				else
				{
					Configs::updateParam( 'SYSTEM_PLTB_PULLDATE_'.$request->currency , $todate );
				}
				
				Profitloss::updatePNL2($this->profitloss);
				
			}else{
				if( date( "Y-m-d H:i:s") > $todate )
				{
						Configs::updateParam( 'SYSTEM_PLTB_PULLDATE_'.$request->currency , $todate );
				}
			} 
			
			if( isset($results->Pagination->CurrentPage) )
			{
				
				
				if( $results->Pagination->CurrentPage == $results->Pagination->TotalPage )
				{
					$continue = false;
				}
				else
				{
					$page = $results->Pagination->CurrentPage + 1;
				}
			}
			else{
				$continue = false;
			}
			
			if( $continue == false )
			{
				if( date("Y-m-d H:i:s") > $todate ){
					$startdate  = $todate;
					$todate    = date( "Y-m-d H.i.s", strtotime($todate)+ 600 );
					$continue = true;
				}else{
					$continue = false;
				}
			}
			
		
		}
		
		App::unlock_process('pltb_datapull_process_'.$request->currency);
		
	}
	
	private function InsertWager( $bet , $currency){
		
		if( !isset($this->accounts[$bet['username']]) )
			$this->accounts[$bet['username']] = Account::where( 'nickname' , '=' , $bet['username'] )->whereCrccode($currency)->first();
		
		$prdObj = Cache::rememberForever('product_pltb_obj', function(){
				return Product::where( 'code' , '=' , 'PLTB' )->first();
		});
		
		if(!isset($this->accounts[$bet['username']]->id))
		exit;	
		
		$data['accid'] 		 		= $this->accounts[$bet['username']]->id;
		$data['acccode'] 	 		= $this->accounts[$bet['username']]->code;
		$data['nickname'] 	 		= $bet['username'];
		$data['wbsid'] 		 		= $this->accounts[$bet['username']]->wbsid;
		$data['prdid'] 		 		= $prdObj->id;
		$data['matchid'] 	 		= $bet['gameId'];
		$data['gamename'] 	 		= $bet['gameType'];
		$data['category'] 	 		= $bet['gameType'] == 'Live Games' ? Product::getCategory('Casino') : Product::getCategory('egames');
		$data['refid'] 		 		= $bet['gameCode'];
		$data['crccode'] 	 		= $this->accounts[$bet['username']]->crccode;
		$data['crcrate'] 	 		= Currency::getCurrencyRate($data['crccode']);
		$data['datetime'] 	 		= $bet['gameDate'];
		$data['ip'] 		 		= '';
		if(  $bet['bet'] == 0 )	
			$data['payout'] = 0;
		else
			$data['payout'] 	 	= $bet['win'] + $bet['progressiveWin'];
		$data['profitloss']  		= $bet['win'] + $bet['progressiveWin'] - $bet['bet'];
		$data['accountdate'] 		= substr( $bet['gameDate'], 0 , 10 );
		$data['stake']		 		= $bet['bet'];
		$data['status'] 			= Wager::STATUS_SETTLED;
		
		if( $data['stake'] == $data['payout'] )	    $data['result']  = Wager::RESULT_DRAW;
		else if( $data['stake'] > $data['payout'] ) $data['result']  = Wager::RESULT_LOSS;
		else if( $data['stake'] < $data['payout'] ) $data['result']  = Wager::RESULT_WIN;
		$data['validstake'] 		= $bet['bet'];

        if ($data['result'] == Wager::RESULT_DRAW) {
            $data['validstake'] = 0;
        }

		$data['stakelocal'] 		= Currency::getLocalAmount($data['stake'], $data['crccode']);
		$data['payoutlocal']		= Currency::getLocalAmount($data['payout'], $data['crccode']);
		$data['profitlosslocal']	= Currency::getLocalAmount($data['profitloss'], $data['crccode']);
		$data['validstakelocal']	= Currency::getLocalAmount($data['validstake'], $data['crccode']);
		$data['iscalculated'] 	    = 0;
		$data['created']			= date("y-m-d H:i:s");
		$data['modified']			= date("y-m-d H:i:s");
		
		foreach( $data as $key => $value ){
			$insert_data[$key] = '"'. $value . '"';
		}
		

		if(DB::statement('call insert_wager('.implode(',',$insert_data).')'))
		{
			$this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['prdid']   = $data['prdid'];
			$this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['accobj']  = $this->accounts[$bet['username']];
			$this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['crcrate'] = $data['crcrate'];
		}
		
		
	}
	
	private function callFunction( $action , $params , $currency, $httpMethod, $method , $timeout = 10)
	{
        // Copy from PLTB provider lib.
		$header   = array();
		$header[] = "Cache-Control:no-cache";
		$header[] = "merchantname:".Config::get($currency.'.pltb.merchantname'); 
		$header[] = "merchantcode:".Config::get($currency.'.pltb.merchantcode'); 
        
	    $tuCurl = curl_init();
        
        switch (strtolower($httpMethod)) {
            case 'post':
                $header[] = "Content-Type:application/json";
                curl_setopt($tuCurl, CURLOPT_POST, true);
                break;
            case 'put':
                $header[] = "Content-Type:application/json";
                curl_setopt($tuCurl, CURLOPT_CUSTOMREQUEST, 'PUT');
                break;
            default:
                curl_setopt($tuCurl, CURLOPT_POST, false);
                break;
        }

        $url = Config::get($currency.'.pltb.url').$action;
        
	    curl_setopt($tuCurl, CURLOPT_URL, $url); 
		curl_setopt($tuCurl, CURLOPT_TIMEOUT, $timeout);
		curl_setopt($tuCurl, CURLOPT_RETURNTRANSFER, 1);
        
        if (!is_null($params)) {
            $params = json_encode($params);
            
            curl_setopt($tuCurl, CURLOPT_POSTFIELDS, $params);
        } else {
            $params = '';
        }
        
		curl_setopt($tuCurl, CURLOPT_HTTPHEADER, $header);
		$result = trim(curl_exec($tuCurl));
		//var_dump(	$params);
		//var_dump(	$result);
       
	    if(!$result)
		{
//            dd(curl_error($tuCurl));
	    }

		curl_close($tuCurl);
		
		//App::insert_api_log( array( 'request' => 'param='.$url.'?'.str_replace(' ', '%20', $params) , 'return' => $result , 'method' => $method ) );
		
		return $result; 
	}

	
}
