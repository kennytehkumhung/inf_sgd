<?php namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller\Api;
use Crypt;
use App\Models\Betctb;
use App\Models\Account;
use App\Models\Product;
use App\Models\Currency;
use App\Models\Profitloss;
use App\Models\Wager;
use App\Models\Configs;
use App\Models\Balancetransfer;
use App\libraries\App;
use App\libraries\providers\CTB;
use Illuminate\Http\Request;
use Config;
use DB;
use Cache;
use Carbon\Carbon;
use Log;


class CtbController extends MainController{
	
	protected $accounts = array();
	
	protected $profitloss = array();

	public function index(Request $request) {

        App::check_process('ctb_datapull_process_' . $request->currency);
		
		if (Product::where('status', '=', 2)->where('code', '=', 'CTB')->count() == 1)
		{
			 App::unlock_process('ctb_datapull_process_' . $request->currency);
			exit;
		}

        date_default_timezone_set("Asia/Kuala_Lumpur");

        $dtFrom = $request->input('startdate');
        $updatePullDateParam = false;

        if (is_null($dtFrom)) {
            $dtFrom = Configs::getParam('SYSTEM_CTB_PULLDATE_' . $request->currency);
            $updatePullDateParam = true;

            if (strlen($dtFrom) < 1) {
                $dtFrom = '2016-12-30 10:00:00';
            }
        }

        $dtFromStr = Carbon::parse($dtFrom)->getTimestamp() * 1000;

        $tkCode = Config::get($request->currency . '.ctb.tk_code');
        $parentUserId = strtolower(Config::get($request->currency . '.ctb.parent_user_id'));
        $apiUrl = Config::get($request->currency . '.ctb.api_url') . 'website/gettxnsbylastupdatetimebyparentuserid?';

		// $status_type[1]  = 'ACTIVE';
		// $status_type[2]  = 'SCR';
		// $status_type[4]  = 'CANCEL';
		// $status_type[8]  = 'SETTLE';
		// $status_type[16] = 'RACE VOID';

        /*
         * Only the value of different group can be combined.
         * BitOr 9: 1 (active) + 8 (settle)
         * BitOr 11: 1 (active) + 2 (scratched) + 8 (settle)
         * BitOr 13: 1 (active) + 4 (cancel) + 8 (settle)
         */
        $acceptedStatus = array(8, 9);
        $postfields = 'tkCode=' . $tkCode .
            '&parentUserID=' . $parentUserId .
            '&lastUpdateDate=' . $dtFromStr;

        $result = json_decode(App::curlGET($apiUrl . $postfields, true, 30, ''));

        if (isset($result->status) && $result->status == 200 && isset($result->transactionList)) {
            foreach ($result->transactionList as $biKey => $biValue) {
                $dateTimeNow = Carbon::now();
                $dateTimeNowStr = $dateTimeNow->toDateTimeString();

                $transactionDate = $biValue->transactionDate;
                $updateDate = $biValue->updateDate;

                if (is_numeric($transactionDate)) {
                    $transactionDate = Carbon::createFromTimestamp(((int) $transactionDate / 1000));
                } else {
                    $transactionDate = Carbon::parse($transactionDate);
                }

                if (is_numeric($updateDate)) {
                    $updateDate = Carbon::createFromTimestamp(((int) $updateDate / 1000));
                } else {
                    $updateDate = Carbon::parse($updateDate);
                }

                $data = array(
                    'refID'             => (int) $biValue->id,
                    'userID'            => (string) $biValue->userID,
                    'raceID'            => (int) $biValue->raceID,
                    'raceType'          => (string) $biValue->raceType,
                    'raceDate'          => (string) $biValue->raceDate,
                    'raceNo'            => (int) $biValue->raceNo,
                    'runnerNo'          => (int) $biValue->runnerNo,
                    'currencyName'      => (string) $biValue->currencyName,
                    'status'            => (string) $biValue->status,
                    'limit'             => (string) $biValue->limit,
                    'payout'            => (float) $biValue->payout,
//                    'transactionDate'   => $transactionDate->addHours(8)->toDateTimeString(),
//                    'updateDate'        => $updateDate->addHours(8)->toDateTimeString(),
                    'transactionDate'   => $transactionDate->toDateTimeString(),
                    'updateDate'        => $updateDate->toDateTimeString(),
                    'place'             => (float) $biValue->place, // Member bet on 'place' (1st, 2nd, 3rd...)
                    'win'               => (float) $biValue->win, // Member bet on 'win' (1st only)
                    'created_at'        => $dateTimeNowStr,
                    'updated_at'        => $dateTimeNowStr,
                );

                foreach ($data as $key => $value) {
                    $insertData[$key] = '"' . $value . '"';
                }

                if (DB::statement('call insert_ctb(' . implode(',', $insertData) . ')')) {
                    if (in_array($data['status'], $acceptedStatus)) {
                        // Get new inserted ID.
                        $this->InsertWager($data, $data['currencyName']);
                    }
                }
            }
        }

        Profitloss::updatePNL2($this->profitloss);

        if ($updatePullDateParam) {
            Configs::updateParam('SYSTEM_CTB_PULLDATE_' . $request->currency, Carbon::now()->toDateTimeString());
        }

        App::unlock_process('ctb_datapull_process_' . $request->currency);
    }

	private function InsertWager( $bet, $currency )
    {

        if (!isset($this->accounts[$bet['userID']]))
            $this->accounts[$bet['userID']] = Account::where('nickname', '=', $bet['userID'])->where('crccode', '=', $currency)->first();

        $prdObj = Cache::rememberForever('product_ctb_obj', function () {
            return Product::where('code', '=', 'CTB')->first();
        });

        $account = $this->accounts[$bet['userID']];

        $data['accid'] = $account->id;
        $data['acccode'] = $account->code;
        $data['nickname'] = $account->nickname;
        $data['wbsid'] = $account->wbsid;
        $data['prdid'] = $prdObj->id;
        $data['matchid'] = $bet['raceID'];
        $data['gamename'] = 'Racebook';
        $data['category'] = Product::getCategory('racebook');
        $data['refid'] = $bet['refID'];
        $data['crccode'] = $account->crccode;
        $data['crcrate'] = Currency::getCurrencyRate($data['crccode']);
        $data['datetime'] = $bet['transactionDate'];
        $data['ip'] = '';
        $data['payout'] = $bet['payout'] + $bet['place'] + $bet['win'];
        $data['profitloss'] = $bet['payout'];
//        $data['accountdate'] = Carbon::parse($bet['updateDate'])->addHours(-8)->toDateString();
        $data['accountdate'] = $bet['raceDate'];
        $data['stake'] = $bet['place'] + $bet['win'];
        $data['status'] = Wager::STATUS_SETTLED;

        if ($data['stake'] == $data['payout']) $data['result'] = Wager::RESULT_DRAW;
        else if ($data['stake'] > $data['payout']) $data['result'] = Wager::RESULT_LOSS;
        else if ($data['stake'] < $data['payout']) $data['result'] = Wager::RESULT_WIN;
        $data['validstake'] = $bet['place'] + $bet['win'];
        $data['stakelocal'] = Currency::getLocalAmount($data['stake'], $data['crccode']);
        $data['payoutlocal'] = Currency::getLocalAmount($data['payout'], $data['crccode']);
        $data['profitlosslocal'] = Currency::getLocalAmount($data['profitloss'], $data['crccode']);
        $data['validstakelocal'] = Currency::getLocalAmount($data['validstake'], $data['crccode']);
        $data['iscalculated'] = 0;
        $data['created'] = date("y-m-d H:i:s");
        $data['modified'] = date("y-m-d H:i:s");

        foreach ($data as $key => $value) {
            $insert_data[$key] = '"' . $value . '"';
        }

        if (DB::statement('call insert_wager(' . implode(',', $insert_data) . ')')) {
            $this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['prdid'] = $data['prdid'];
            $this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['accobj'] = $this->accounts[$bet['userID']];
            $this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['crcrate'] = $data['crcrate'];
        }
    }
}
