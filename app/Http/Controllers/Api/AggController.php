<?php namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller\Api;
use App\Models\Betagg;
use App\Models\Account;
use App\Models\Product;
use App\Models\Wager;
use App\Models\Currency;
use App\Models\Profitloss;
use App\libraries\providers\AGG;
use Illuminate\Http\Request;
use App\libraries\App;
use Config;
use DB;
use Cache;

class AggController extends MainController{
	
	protected $accounts = array();
	
	protected $profitloss = array();
	
	public function index(Request $request)
	{
		
		App::check_process('agg_datapull_process');
		
		if (Product::where('status', '=', 2)->where('code', '=', 'AGG')->count() == 1)
		{
			App::unlock_process('agg_datapull_process');
			exit;
		}

		if ($request->has('startdate')) {
		    $from = $request->get('startdate');
        } else {
            $from = $this->getLatestTransactionTime($request->currency);
        }
		
		$postfields = 'vendor_id='.Config::get($request->currency.'.agg.vendor_id').
					  '&operator_id='.Config::get($request->currency.'.agg.operator_id').
					  '&page=1'.
					  '&from='.$from.
					  '&to='.date('Y-m-d').' 23:59:59';

		$result = json_decode( $this->curl( $postfields , Config::get($request->currency.'.agg.url').'getBetDetail' , false ) );
		

		if( isset($result->total) && $result->total != 0)
		{
		
			foreach( $result->trans as $key => $value )
			{
				
				$data['user_id'] 		   = strtoupper($value->user_id); 		
				$data['transaction_time']  = $value->transaction_time;
				$data['trans_id'] 		   = $value->trans_id;		
				$data['winlost_amount']    = $value->winlost_amount;  
				$data['game_code'] 		   = $value->game_code; 		
				$data['game_type']		   = $value->game_type;		
				$data['stake'] 			   = $value->stake; 			
				$data['valid_stake']	   = $value->valid_stake;		
				$data['play_type'] 		   = $value->play_type; 		
				$data['table_code']		   = $value->table_code;		
				$data['ip'] 			   = $value->ip; 				
				$data['recaculate_time']   = $value->recaculate_time;	
				$data['remark']			   = $value->remark;			
				$data['currency']		   = $value->currency;			
				$data['status'] 		   = $value->status; 	
				$data['created']		   = date("y-m-d H:i:s");
				$data['modified']		   = date("y-m-d H:i:s");

				foreach( $data as $key => $value ){
						$insert_data[$key] = '"'. $value . '"';
				}					


				if(DB::statement('call insert_agg('.implode(',',$insert_data).')')){
					
						$this->InsertWager($data,$request->currency);
						
				} 

			}
	
		 	if( isset($result->total) && $result->total > 1 )
			{
				
				for( $i = 2 ; $i <= $result->total ; $i++ )
				{
					
					$postfields = 'vendor_id='.Config::get($request->currency.'.agg.vendor_id').
					              '&operator_id='.Config::get($request->currency.'.agg.operator_id').
					              '&page='.$i.
					              '&from='.$from.
					              '&to='.date('Y-m-d H:i:s');
								  
					$result = json_decode( $this->curl( $postfields , Config::get($request->currency.'.agg.url').'getBetDetail' , false ) );
					
					foreach( $result->trans as $key => $value )
					{
						
						if( Betagg::where( 'trans_id' , '=' , $value->trans_id )->count() == 0 )
						{
						
							$data['user_id'] 		   = strtoupper($value->user_id); 		
							$data['transaction_time']  = $value->transaction_time;
							$data['trans_id'] 		   = $value->trans_id;		
							$data['winlost_amount']    = $value->winlost_amount;  
							$data['game_code'] 		   = $value->game_code; 		
							$data['game_type']		   = $value->game_type;		
							$data['stake'] 			   = $value->stake; 			
							$data['valid_stake']	   = $value->valid_stake;		
							$data['play_type'] 		   = $value->play_type; 		
							$data['table_code']		   = $value->table_code;		
							$data['ip'] 			   = $value->ip; 				
							$data['recaculate_time']   = $value->recaculate_time;	
							$data['remark']			   = $value->remark;			
							$data['currency']		   = $value->currency;			
							$data['status'] 		   = $value->status; 	
							$data['created']		   = date("y-m-d H:i:s");
							$data['modified']		   = date("y-m-d H:i:s");			
							foreach( $data as $key => $value ){
									$insert_data[$key] = '"'. $value . '"';
							}					

							if(DB::statement('call insert_agg('.implode(',',$insert_data).')')){
								
									$this->InsertWager($data,$request->currency);
									
									
								
							} 
						}		
					}
				}
			}  

			Profitloss::updatePNL2($this->profitloss);
		}
		
		App::unlock_process('agg_datapull_process');
		
	}	
	
	private function getLatestTransactionTime($currency)
	{
		$date = Betagg::where( 'currency' , '=' , $currency )->orderBy('transaction_time','Desc')->pluck('transaction_time');
		
		if(!$date){
				$date = '2017-10-15 00:04:13';
		}
		
		return $date;

	}
	
	private function InsertWager( $ag , $currency){
	
		if( !isset($this->accounts[$ag['user_id']]) )
			$this->accounts[$ag['user_id']] = Account::where( 'nickname' , '=' , $ag['user_id'] )->whereCrccode($currency)->first();
		
		$prdObj = Cache::rememberForever('product_agg_obj', function(){
				return Product::where( 'code' , '=' , 'AGG' )->first();
		});
		
		$casinos[] = 'BAC';
		$casinos[] = 'CBAC';
		$casinos[] = 'SBAC';
		$casinos[] = 'LBAC';
		$casinos[] = 'LINK';
		$casinos[] = 'DT';
		$casinos[] = 'SHB';
		$casinos[] = 'ROU';
		$casinos[] = 'FT';
		$casinos[] = 'ZJH';
	
		$data['accid'] 		 		= $this->accounts[$ag['user_id']]->id;
		$data['acccode'] 	 		= $this->accounts[$ag['user_id']]->code;
		$data['nickname'] 	 		= $ag['user_id'];
		$data['wbsid'] 		 		= $this->accounts[$ag['user_id']]->wbsid;
		$data['prdid'] 		 		= $prdObj->id;
		$data['matchid'] 	 		= $ag['game_code'] == '' ? "''":$ag['game_code'];
		if( $ag['game_type'] != 'HSR' )
			$data['gamename'] 	 		= $ag['game_type'];
		else
			$data['gamename'] 	 		= $ag['game_code'];
		$data['category'] 	 		= Product::getCategory('egames');
		
		if(in_array($ag['game_type'], $casinos))
		{
			$data['category'] 	 		= Product::getCategory('Casino');
		}
		
		if( $ag['game_type'] != 'HSR' )
			$data['refid'] 		 		= $ag['trans_id'];
		else
			$data['refid'] 		 		= $ag['trans_id'];
		
		
		
		$data['crccode'] 	 		= $this->accounts[$ag['user_id']]->crccode;
		$data['crcrate'] 	 		= Currency::getCurrencyRate($data['crccode']);
		$data['datetime'] 	 		= $ag['transaction_time'];
		$data['ip'] 		 		= $ag['ip'];
		$data['payout'] 	 		= (float) ( $ag['stake'] + $ag['winlost_amount'] );
		$data['profitloss']  		= $ag['winlost_amount'];
		$data['accountdate'] 		= substr( $ag['transaction_time'], 0 , 10 );
		$data['stake']		 		= $ag['stake'];
		$data['status'] 			= Wager::STATUS_SETTLED;
		
		if( $data['stake'] == $data['payout'] )	    $data['result']  = Wager::RESULT_DRAW;
		else if( $data['stake'] > $data['payout'] ) $data['result']  = Wager::RESULT_LOSS;
		else if( $data['stake'] < $data['payout'] ) $data['result']  = Wager::RESULT_WIN;
		$data['validstake'] 		= $ag['valid_stake'];
		$data['stakelocal'] 		= Currency::getLocalAmount($data['stake'], $data['crccode']);
		$data['payoutlocal']		= Currency::getLocalAmount($data['payout'], $data['crccode']);
		$data['profitlosslocal']	= Currency::getLocalAmount($data['profitloss'], $data['crccode']);
		$data['validstakelocal']	= Currency::getLocalAmount($data['validstake'], $data['crccode']);
		$data['iscalculated'] 	    = 0;
		$data['created']			= date("y-m-d H:i:s");
		$data['modified']			= date("y-m-d H:i:s");
		
		
		
		foreach( $data as $key => $value ){
			$insert_data[$key] = '"'. $value . '"';
		}
		

		if(DB::statement('call insert_wager('.implode(',',$insert_data).')'))
		{
			//$this->insertWagerSetting($data);
			$this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['prdid']   = $data['prdid'];
			$this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['accobj']  = $this->accounts[$ag['user_id']];
			$this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['crcrate'] = $data['crcrate'];
		}
		

	}

}
