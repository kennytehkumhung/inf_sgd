<?php namespace App\Http\Controllers\Api;

use Illuminate\Foundation\Bus\DispatchesCommands;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;

abstract class MainController extends BaseController {

	use DispatchesCommands, ValidatesRequests;
	
	protected function curl( $postfields, $url , $timeout = 5 ){

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_HEADER, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);
			curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
			$response = curl_exec($ch);
			curl_close($ch);
			
			$result = json_decode($response);
			
			return $response;
	}
	
	protected function insertWagerSetting( $wager = array() ){
		
		if($accObj = Account::find($wager['accid'])) 
		{
				$affObj = new Affiliate;
				
				$agents = $affObj->getAllUplineById($accObj->aflid);
				foreach($agents as $level => $aflid) {
					if($aflObj = Affiliate::find($aflid)) {
						$rate = 0;
						if($afsObj = Affiliatesetting::whereRaw('aflid='.$aflid.' AND effective <= "'.$wager['created'].'" ORDER BY effective DESC')->first())
						$rate = $afsObj->commission / 100;
						
						$data['prdid'] 	  	= $wager['prdid'];
						$data['category'] 	= $wager['category'];
						$data['wgrid'] 	  	= $wager['id'];
						$data['aflid'] 	  	= $accObj->aflid;
						$data['share1id'] 	= $aflid;
						$data['share2id'] 	= $aflObj->parentid;
						$data['afsid'] 	  	= $afsObj->id;
						$data['accid'] 	  	= $wager['accid'];
						$data['crccode']  	= $wager['crccode'];
						$data['crcrate']  	= $wager['crcrate'];
						$data['stake']    	= $wager['stake'];
						$data['validstake'] = $wager['validstake'];
						$data['profitloss'] = $wager['profitloss'];
						$data['rate']	    = $afsObj->commission;
						$data['share1']	    = $wager['profitloss'] * $rate;
						$data['share2']	    = $wager['profitloss'] * (1 - $rate) * -1;
						$data['date']	    = $wager['accountdate'];
					
						DB::statement('call insert_wagersetting("'.implode('","',$data).'")');
					

					}
				}
	
				
		}
		
	}

}
