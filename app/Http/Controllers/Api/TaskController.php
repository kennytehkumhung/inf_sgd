<?php namespace App\Http\Controllers\Api;

use App\libraries\App;
use App\Models\Apilog;
use App\Models\Onlinetracker;
use Carbon\Carbon;
use Config;
use DB;
use File;
use Illuminate\Http\Request;
use Log;
use Storage;

class TaskController extends MainController
{

    public function index($act, Request $request)
    {
        $act = strtolower($act);

        if ($act == 'cleartempupload') {
            $this->clearTempUpload();
        } elseif ($act = 'monitorlogin') {
            $this->monitorLogin();
        }
    }

    private function clearTempUpload()
    {
        // Clear temp files that upload at TransactionController@depositProcess.
        $storage = Storage::disk('local');
        $storagePath = $storage->getDriver()->getAdapter()->getPathPrefix() . '/';
        $files = $storage->allFiles('tempUpload');
        $nowDatetime = Carbon::now();

        foreach ($files as $f) {
            $modifiedDatetime = Carbon::createFromTimestamp(File::lastModified($storagePath . $f));

            if ($nowDatetime->diffInHours($modifiedDatetime) > 1) {
                // Delete files that is 1 hour old.
                File::delete($storagePath . $f);
            }
        }
    }

    private function monitorLogin()
    {
        App::check_process('task_monitorlogin_process');

        // Task configuration.
        $configs = [
            ['front_path' => 'royalewin', 'limit' => 10, 'method' => 'getLoginUrl', 'request' => 'http://hapi.bm.1sgames.com/api.aspx?action=login'],
        ];

        $now = Carbon::now();
        $startDt = $now->copy()->addMinutes(-1);

        $frontPath = Config::get('setting.front_path');
        $tableName = (new Apilog())->getTable();

        $now = $now->toDateTimeString();
        $startDt = $startDt->toDateTimeString();

        foreach ($configs as $cfg) {
            if ($frontPath != $cfg['front_path']) {
                // Skip if config not belongs to current operator.
                continue;
            }

            $results = DB::table($tableName)->whereBetween('created', [$startDt, $now])
                ->where('method', '=', $cfg['method'])
                ->where('accid', '>', 0)
                ->where('request', 'LIKE', $cfg['request'] . '%')
                ->groupBy('accid')
                ->select(['accid', 'acccode'])
                ->selectRaw('COUNT(`id`) AS `request_count`')
                ->get();

            foreach ($results as $r) {
                if ($r->request_count > $cfg['limit']) {
                    // Request count is over maximum allowed limit.
                    Onlinetracker::where('userid', '=', $r->accid)->delete();
                    Log::info('Monitor Login - user: ' . $r->acccode . ' kicked, req. count: ' . $r->request_count . ', req. url: ' . $cfg['request']);
                }
            }
        }

        App::unlock_process('task_monitorlogin_process');
    }
}
