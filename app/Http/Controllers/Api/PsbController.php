<?php namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller\Api;
use App\Models\Betpsb;
use App\Models\Account;
use App\Models\Product;
use App\Models\Wager;
use App\Models\Currency;
use App\Models\Profitloss;
use App\Models\Betpsbresult;
use App\Models\Hiswl;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Config;
use App\libraries\App;
use DB;
use Cache;


class PsbController extends MainController{

    private $hisWlTable = 'his_wl';

	public function index(Request $request)
	{
        $lockname = 'psb_datapull_process';

        if ($request->input('date') == 'ytd') {
            $lockname .= 'ytd';
            $date = Carbon::now()->addDays(-1)->toDateString();
        } else {
            $date = $request->input('date', date('Y-m-d'));
        }

        App::check_process($lockname);

        $psbUrl = Config::get($request->currency . '.psb.url');
        $psbA = Config::get($request->currency . '.psb.a');
        $psbP = Config::get($request->currency . '.psb.p');

		$data = App::csv_to_array($psbUrl . '?m=6&a=' . $psbA . '&p=' . $psbP . '&d=' . $date);

		if (is_array($data)) {
            $prefix = Config::get($request->currency . '.psb.prefix');
            $usernameCache = array();

            foreach($data as $key => $value) {
                if (!is_numeric($value[0])) {
                    continue;
                }

                $btype = strtolower($value[5]);

                if ($btype == '4dspc') {
                    $betType = 3;
                } elseif ($btype == '5d/6d') {
                    $betType = 2;
                } else {
                    // Default: 3D/4D
                    $betType = 1;
                }

                $singleReceipt = App::csv_to_array($psbUrl . '?m=9&a=' . $psbA . '&p=' . $psbP . '&rn=' . $value['0'] . '&t=' . $betType);

                if (is_array($singleReceipt)) {
                    foreach($singleReceipt as $srKey => $srValue) {
                        if (!starts_with($srValue[0], $prefix)) {
                            continue;
                        }

                        if (!array_key_exists($srValue[0], $usernameCache)) {
                            $accountObj = Account::where('nickname', '=', str_replace($prefix, '', $srValue[0]))
                                ->where('crccode', '=', $request->currency)
                                ->first(['id']);

                            if ($accountObj) {
                                $usernameCache[$srValue[0]] = $accountObj->id;
                            } else {
                                $usernameCache[$srValue[0]] = 0;
                            }
                        }

                        $userId = $usernameCache[$srValue[0]];

                        if ($btype == '5d/6d') {
                            /*
                             * Array description:
                             * 0: Account,
                             * 1: Ord Date,
                             * 2: Number,
                             * 3: Draw Date,
                             * 4: Game,
                             * 5: Total,
                             * 6: Sysnote,
                             * 7: Type,
                             * 8: No
                             */
                            $stData = array(
                                'ReceiptNumber' => $value[0],
                                'GameName' => $this->ConvertGameProviderName($srValue[4]),
                                'OrderDate' => $srValue[1],
                                'DrawDate' => $srValue[3],
                                'UserID' => $userId,
                                'UserName' => $srValue[0],
                                'BetNumber' => $srValue[2],
                                'GameType' => $srValue[7],
                                'Big' => 0.0000,
                                'Small' => 0.0000,
                                '3A' => 0.0000,
                                '3C' => 0.0000,
                                '4A' => 0.0000,
                                'Total' => $srValue[5],
                                'DrawStatus' => 'Open',
                                'Payout' => 0.0000,
                                'TicketID' => $srValue[8],
                                'Void' => (isset($value[6]) ? $value[6] : null),
                                'SysNote' => $srValue[6],
                                'created_at' => date('Y-m-d H:i:s'),
                                'updated_at' => date('Y-m-d H:i:s'),
                            );
                        } else {
                            /*
                             * Array description:
                             * 0: Account,
                             * 1: Ord Date,
                             * 2: Number,
                             * 3: Draw Date,
                             * 4: Game,
                             * 5: Big,
                             * 6: Small,
                             * 7: 3A,
                             * 8: 3C,
                             * 9: 4A,
                             * 10: Total,
                             * 11: Sysnote,
                             * 12: Type,
                             * 13: No
                             */
                            $stData = array(
                                'ReceiptNumber' => $value[0],
                                'GameName' => $this->ConvertGameProviderName($srValue[4]),
                                'OrderDate' => $srValue[1],
                                'DrawDate' => $srValue[3],
                                'UserID' => $userId,
                                'UserName' => $srValue[0],
                                'BetNumber' => $srValue[2],
                                'GameType' => $srValue[12],
                                'Big' => $srValue[5],
                                'Small' => $srValue[6],
                                '3A' => $srValue[7],
                                '3C' => $srValue[8],
                                '4A' => $srValue[9],
                                'Total' => $srValue[10],
                                'DrawStatus' => 'Open',
                                'Payout' => 0.0000,
                                'TicketID' => $srValue[13],
                                'Void' => (isset($value[6]) ? $value[6] : null),
                                'SysNote' => $srValue[11],
                                'created_at' => date('Y-m-d H:i:s'),
                                'updated_at' => date('Y-m-d H:i:s'),
                            );
                        }

                        if ($stData['Void'] == 'Void') {
                            $stData['Big'] = 0.0000;
                            $stData['Small'] = 0.0000;
                            $stData['3A'] = 0.0000;
                            $stData['3C'] = 0.0000;
                            $stData['4A'] = 0.0000;
                            $stData['Total'] = 0.0000;
                            $stData['Payout'] = 0.0000;
//                            $stData['SysNote'] = 'Void';
                        }

                        $stmtParams = array();
                        $stmtValues = array();

                        foreach ($stData as $spKey => $spValue) {
                            $stmtParams[] = ':' . $spKey;
                            $stmtValues[$spKey] = $spValue;
                        }

                        DB::statement('call insert_psb(' . implode(',', $stmtParams) . ')', $stmtValues);
                    }
                }
            }
        }

        App::unlock_process($lockname);
	}	
	
	public function getSettleBet(Request $request)
	{
	    if ($request->input('date') == 'ytd') {
            $date = Carbon::now()->addDays(-1)->toDateString();
        } else {
            $date = $request->input('date', date('Y-m-d')); // Draw date, NOT order date.
        }

        $psbUrl = Config::get($request->currency . '.psb.url');
        $psbA = Config::get($request->currency . '.psb.a');
        $psbP = Config::get($request->currency . '.psb.p');

	    $gameStatusData = App::csv_to_array($psbUrl . '?m=10&a=' . $psbA . '&p=' . $psbP . '&d=' . $date);

	    if (is_array($gameStatusData)) {
            if (isset($gameStatusData[0][0]) && str_contains($gameStatusData[0][0], 'OK%%draw:P')) {
                // Draw status is Paid.
                $gameStatus = explode('%%', $gameStatusData[0][0]);

                if (count($gameStatus) > 2) {
                    $gameStatus = explode('%', $gameStatus[2]);
                    $allGamesClosed = true;

                    foreach ($gameStatus as $gs) {
                        if (!str_contains($gs, ':C')) {
                            $allGamesClosed = false;
                            break;
                        }
                    }

                    if ($allGamesClosed) {
                        // All games are closed, now
                        $dailySummaryData = App::csv_to_array($psbUrl . '?m=4&a=' . $psbA . '&p='  . $psbP . '&d=' . $date);

                        if (is_array($dailySummaryData)) {
                            /*
                             * Array description:
                             * 0: Sold To (member),
                             * 1: Draw Date,
                             * 2: Number,
                             * 3: Game,
                             * 4: Prz,
                             * 5: Bet Amount,
                             * 6: Payout Rate,
                             * 7: Placing,
                             * 8: Payout Package,
                             * 9: Win Total,
                             * 10: Receipt No,
                             * 11: Order Time
                             */
                            $prefix = Config::get($request->currency . '.psb.prefix');
                            $cachedPayouts = array();

                            foreach ($dailySummaryData as $dsKey => $dsValue) {
                                if (!starts_with($dsValue[0], $prefix)) {
                                    continue;
                                }

                                // Insert bet_psb_summary record.
                                $gameName = $this->ConvertGameProviderName($dsValue[3]);
                                $stData = array(
                                    'ReceiptNumber' => $dsValue[10],
                                    'UserName' => $dsValue[0],
                                    'DrawDate' => $dsValue[1],
                                    'OrderDate' => $dsValue[11],
                                    'BetNumber' => $dsValue[2],
                                    'GameType' => $gameName,
                                    'Prize' => $dsValue[4],
                                    'BetAmount' => $dsValue[5],
                                    'PayoutRate' => $dsValue[6],
                                    'Placing' => $dsValue[7],
                                    'PayoutPackage' => $dsValue[8],
                                    'WinTotal' => $dsValue[9],
                                    'created_at' =>  date('Y-m-d H:i:s'),
                                    'updated_at' =>  date('Y-m-d H:i:s'),
                                );

                                $stmtParams = array();
                                $stmtValues = array();

                                foreach ($stData as $spKey => $spValue) {
                                    $stmtParams[] = ':' . $spKey;
                                    $stmtValues[$spKey] = $spValue;
                                }

                                DB::statement('call insert_psb_summary(' . implode(',', $stmtParams) . ')', $stmtValues);

                                // Calculate payout in bet_psb and wl_his table.
                                $builder = DB::table((new Betpsb())->getTable() . ' as bet')
                                    ->leftJoin($this->hisWlTable . ' as wl', 'wl.refid', '=', 'bet.id')
                                    ->whereBetween('bet.DrawDate', array($dsValue[1] . ' 00:00:00', $dsValue[1] . ' 23:59:59'))
                                    ->where('bet.ReceiptNumber', '=', $dsValue[10])
                                    ->where('bet.GameName', '=', $gameName)
                                    ->where('bet.Void', '!=', 'Void') // Should not happen, just in case.
                                    ->where('wl.product', '=', 'PSB');

                                $betNumPosition = 'Total';
                                $betNumOperator = '=';
                                $betNumValue = $dsValue[2];

                                switch (strtoupper($dsValue[7])) {
                                    case 'BIG':
                                        $betNumPosition = 'Big';
                                        break;
                                    case 'SMALL':
                                        $betNumPosition = 'Small';
                                        break;
                                    case '3A':
                                        $betNumPosition = '3A';
                                        $betNumOperator = 'LIKE';
                                        $betNumValue = '%' . $dsValue[2] . '%';
                                        break;
                                    case '3C':
                                        $betNumPosition = '3C';
                                        $betNumOperator = 'LIKE';
                                        $betNumValue = '%' . $dsValue[2] . '%';
                                        break;
                                    case '4A':
                                        $betNumPosition = '4A';
                                        break;
                                    case '5D':
                                        // Default.
                                        break;
                                    case '6D':
                                        // Default.
                                        break;
                                }

                                $cachedPayoutsKey = 'psb' . $dsValue[10] . $dsValue[2] . $dsValue[5] . $dsValue[7] . $gameName;
//                                $cachedPayoutsKey = 'psb' . $dsValue[10] . $dsValue[2] . $gameName;
                                $cachedPayoutAmount = 0;

                                if (array_key_exists($cachedPayoutsKey, $cachedPayouts)) {
                                    $cachedPayoutAmount = $cachedPayouts[$cachedPayoutsKey];
                                    $cachedPayouts[$cachedPayoutsKey] = $cachedPayoutAmount + $dsValue[9];
                                } else {
                                    $cachedPayouts[$cachedPayoutsKey] = $dsValue[9];
                                }

                                $builder->where('bet.' . $betNumPosition, '=', $dsValue[5])
                                    ->where('bet.BetNumber', $betNumOperator, $betNumValue)
                                    ->update(array(
                                        'bet.DrawStatus' => 'Closed',
                                        'bet.Payout' => $dsValue[9] + $cachedPayoutAmount,
                                        'bet.updated_at' => date('Y-m-d H:i:s'),
                                        'wl.payout' => $dsValue[9] + $cachedPayoutAmount,
                                        'wl.betStatus' => 'SETTLED',
                                        'wl.payoutStatus' => 'WON',
                                    ));
                            }

                            // Close all remaining records that are in Open draw status.
                            DB::table((new Betpsb())->getTable() . ' as bet')
                                ->leftJoin($this->hisWlTable . ' as wl', 'wl.refid', '=', 'bet.id')
                                ->whereBetween('bet.DrawDate', array($date . ' 00:00:00', $date . ' 23:59:59'))
                                ->where('bet.DrawStatus', '=', 'Open')
                                ->where('wl.product', '=', 'PSB')
                                ->update(array(
                                    'bet.DrawStatus' => 'Closed',
                                    'bet.updated_at' => date('Y-m-d H:i:s'),
                                    'wl.betStatus' => 'SETTLED',
                                    'wl.payoutStatus' => 'LOSE',
                                ));
                        }
                    }
                }
            }
        }
	}
	
	public function getResult(Request $request)
	{
		
//		App::check_process('psb_getresult_process');

		$date = $request->input('date', date('Y-m-d'));
		$data = App::csv_to_array(Config::get($request->currency . '.psb.url') . '?m=3&a=' . Config::get($request->currency . '.psb.a') . '&p=' . Config::get($request->currency . '.psb.p') . '&d=' . $date);

        $results = array();

        if (is_array($data)) {
            if (isset($data[0][0]) && str_contains(strtoupper($data[0][0]), 'OK')) {
                // Process only if document first line contains 'OK'.
                $gameNames1 = array('M', 'P', 'T', 'C', 'S', 'B', 'K', 'W'); // Standard 4D result format.
                $gameNames2 = array('5D', '6D');
                $defaultResults = array('1:' => '', '2:' => '', '3:' => '', '4:' => '', '5:' => '', '6:' => '', 'S:' => '', 'C:' => '');

                $gameName = '';
                $gameResultType = '';
                $gameResults = $defaultResults;

                for ($i = 0; $i < count($data); $i++) {
                    $value = $data[$i][0];

                    if (in_array($value, $gameNames1) || in_array($value, $gameNames2)) {
                        // Get game name.
                        if ($gameName != '' && $gameName != $value) {
                            // Save previous game.
                            $results[$gameName] = $gameResults;
                        }

                        $gameName = $value;
                        $gameResultType = '';
                        $gameResults = $defaultResults;

                        continue;
                    }

                    if (str_contains($value, ':')) {
                        // Extract results.
                        if (in_array($gameName, $gameNames1)) {
                            $arr = explode(' ', $value);
                        } else {
                            $arr = array($value);
                        }

                        foreach ($arr as $a) {
                            // explode() will contains empty value, use preg_split() instead.
                            $tmp = preg_split('/:/', $a, -1, PREG_SPLIT_NO_EMPTY);

                            if (count($tmp) > 1) {
                                $gameResults[$tmp[0] . ':'] = $tmp[1];
                            } else {
                                $gameResultType = $tmp[0] . ':';
                            }
                        }
                    } elseif ($gameResultType != '') {
                        $curr = $gameResults[$gameResultType] . ' ' . $value;

                        $gameResults[$gameResultType] = str_replace(' ', ',', trim($curr));
                    }
                }

                if ($gameName != '') {
                    // Save last game results.
                    $results[$gameName] = $gameResults;
                }

                foreach ($results as $gn => $rs) {
                    $provname = $this->ConvertGameProviderName(strtoupper($gn));
                    $resultObj = Betpsbresult::where('DrawDate', '=', $date . ' 00:00:00')
                        ->where('GameName', '=', $provname)
                        ->first();

                    if ($resultObj) {
                        // Update existing.
                        $resultObj->forceFill(array(
                            'FirstPrizeNumber' => $this->CheckGameResultValue($rs['1:']),
                            'SecondPrizeNumber' => $this->CheckGameResultValue($rs['2:']),
                            'ThirdPrizeNumber' => $this->CheckGameResultValue($rs['3:']),
                            'FourthPrizeNumber' => $this->CheckGameResultValue($rs['4:']),
                            'FifthPrizeNumber' => $this->CheckGameResultValue($rs['5:']),
                            'SixthPrizeNumber' => $this->CheckGameResultValue($rs['6:']),
                            'SpecialPrizeNumber' => $this->CheckGameResultValue($rs['S:']),
                            'ConsolationPrizeNumber' => $this->CheckGameResultValue($rs['C:']),
                        ))->save();
                    } else {
                        // Create new.
                        Betpsbresult::forceCreate(array(
                            'DrawDate' => $date . ' 00:00:00',
                            'GameName' => $provname,
                            'FirstPrizeNumber' => $this->CheckGameResultValue($rs['1:']),
                            'SecondPrizeNumber' => $this->CheckGameResultValue($rs['2:']),
                            'ThirdPrizeNumber' => $this->CheckGameResultValue($rs['3:']),
                            'FourthPrizeNumber' => $this->CheckGameResultValue($rs['4:']),
                            'FifthPrizeNumber' => $this->CheckGameResultValue($rs['5:']),
                            'SixthPrizeNumber' => $this->CheckGameResultValue($rs['6:']),
                            'SpecialPrizeNumber' => $this->CheckGameResultValue($rs['S:']),
                            'ConsolationPrizeNumber' => $this->CheckGameResultValue($rs['C:']),
                            'DrawTime' => '0000-00-00 00:00:00',
                        ));
                    }
                }

                Cache::forget('4d_result');
            }
        }

//		App::unlock_process('psb_getresult_process');
		
	}	
	
	private function ConvertGameProviderName($game)
    {
        switch ($game)
        {
            case "M":
                return "Magnum";

            case "P":
                return "PMP";

            case "T":
                return "Toto";

            case "S":
                return "Singapore";

            case "B":
                return "Sabah";

            case "N":
                return "Sandakan";

            case "R":
                return "Sarawak";
				
			case "K":
                return "Sandakan";	
				
			case "W":
                return "Sarawak";

            case "5":
                return "Toto";

            case "6":
                return "Toto";

            default:
                return $game;
        }

    }

    private function CheckGameResultValue($result) {
	    if (is_null($result) || $result == '') {
	        return 'Invalid';
        }

        return $result;
    }

	public function InsertWager(){
		
		$continue = true;
		
		while($continue){
		
			if( $hiswls = Hiswl::where( 'isCurrent' , '=', 1 )->where( 'betStatus' , '=', 'SETTLED' )->take( 1000 )->get() )
			{
				if( count($hiswls) > 0 )
				{
					foreach( $hiswls as $hiswl )
					{
				
						$username = explode('_',$hiswl->username);
						
						$accObj  = Account::where( 'nickname' , '=' , $username[1] )->whereCrccode('MYR')->first();
						$prdObj  = Product::where( 'code' , '=' , 'PSB' )->first();
						
						$wgrObj 	= Wager::whereRaw(' prdid = '.$prdObj->id.' AND refid = '.$hiswl->refid.'')->first();
						$PsbhisObj  = Betpsb::whereRaw(' ID = '.$hiswl->refid.'')->first();

						if( !$wgrObj ) 
						{
							$wgrObj = new Wager;
							$wgrObj->accid 		 		= $accObj->id;
							$wgrObj->acccode 	 		= $accObj->code;
							$wgrObj->nickname 	 		= $username[1];
							$wgrObj->wbsid 		 		= $accObj->wbsid;
							$wgrObj->prdid 		 		= $prdObj->id;
							$wgrObj->matchid 	 		= $PsbhisObj->BetNumber;
							$wgrObj->gamename 	 		= $PsbhisObj->GameName;
							$wgrObj->category 	 		= Product::getCategory('lottery');
							$wgrObj->refid 		 		= $hiswl->refid;
							$wgrObj->crccode 	 		= $accObj->crccode;
							$wgrObj->crcrate 	 		= Currency::getCurrencyRate($wgrObj->crccode);
							$wgrObj->datetime 	 		= date( "Y-m-d H:i:s", strtotime($hiswl->roundDatetime) + 8 * 3600 );
							$wgrObj->ip 		 		= '';
						}
						
						$wgrObj->payout 	 		= (float) ( $hiswl->payout );
						$wgrObj->profitloss  		= $hiswl->payout - $hiswl->stake;
						$wgrObj->accountdate 		= substr( $PsbhisObj->DrawDate , 0 , 10 );
						$wgrObj->stake		 		= $hiswl->stake;
						$wgrObj->status 			= Wager::STATUS_SETTLED;
						
						if( $wgrObj->stake == $wgrObj->payout )	    $wgrObj->result  = Wager::RESULT_DRAW;
						else if( $wgrObj->stake > $wgrObj->payout ) $wgrObj->result  = Wager::RESULT_LOSS;
						else if( $wgrObj->stake < $wgrObj->payout ) $wgrObj->result  = Wager::RESULT_WIN;
						$wgrObj->validstake 		= $hiswl->stake;;
						$wgrObj->stakelocal 		= Currency::getLocalAmount($wgrObj->stake, $wgrObj->crccode);
						$wgrObj->payoutlocal		= Currency::getLocalAmount($wgrObj->payout, $wgrObj->crccode);
						$wgrObj->profitlosslocal	= Currency::getLocalAmount($wgrObj->profitloss, $wgrObj->crccode);
						$wgrObj->validstakelocal	= Currency::getLocalAmount($wgrObj->validstake, $wgrObj->crccode);
						$wgrObj->iscalculated 	    = 0;
						if($wgrObj->save()){
							Profitloss::updatePNL($wgrObj,$accObj);
							//$hiswl->isCurrent = 0 ;
							//$hiswl->save();
							DB::statement("Update his_wl set isCurrent = 0 where refid = ".$hiswl->refid);
						}
						
					}
				}else{
					$continue = false;
				}
			}else{
					$continue = false;
			}
		
		}
		
	}
	
	

}
