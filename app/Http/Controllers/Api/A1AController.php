<?php namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller\Api;
use App\Models\Beta1a;
use App\Models\Account;
use App\Models\Product;
use App\Models\Wager;
use App\Models\Currency;
use App\Models\Profitloss;
use Illuminate\Http\Request;
use Config;
use App\libraries\App;
use App\libraries\providers\ALB;
use DB;
use Cache;

class A1AController extends MainController{
	
	protected $accounts = array();
	
	protected $profitloss = array();

	public function index(Request $request)
	{
		
		$postfields = 'cmd=gameHistory'.
					  '&hall='.Config::get($request->currency.'.a1a.hall').
					  '&key='.Config::get($request->currency.'.a1a.key').
					  '&login='.
					  '&dateStart='.date("Y-m-d H:i:s", strtotime( $this->getLatestTransactionTime() ) - 6 * 3600 ).
					  '&dateEnd='.date('Y-m-d').' 23:59:59';


		$result = json_decode( App::curl( $postfields , Config::get($request->currency.'.a1a.url') , false) );
		
		if(isset($result->content))
		{
			$total = count($result->content);
			$content = $result->content;
			for( $i = $total ; $i >= 0 ; $i-- ){
				if( isset(	$content[$i]->id) && Beta1a::where( 'transId' , '=' , $content[$i]->id )->count() == 0 )
				{
			
					$data['transId'] 		= $content[$i]->id; 				
					$data['beforeAmount'] 	= $content[$i]->beforeAmount; 				
					$data['user'] 		    = $content[$i]->user; 				
					$data['BetAmount'] 		= $content[$i]->BetAmount; 				
					$data['WinLose'] 		= $content[$i]->WinLose; 				
					$data['BetInfo'] 		= $content[$i]->BetInfo; 				
					$data['GameID'] 		= $content[$i]->GameID; 				
					$data['Matrix'] 		= $content[$i]->Matrix; 				
					$data['dateTime'] 		= date("Y-m-d H:i:s", strtotime( $content[$i]->dateTime ) + 6 * 3600 );
					$data['updated_at']		= date("y-m-d H:i:s");
					$data['created_at']	    = date("y-m-d H:i:s");

					
					foreach( $data as $key => $value ){
							$insert_data[$key] = '"'. $value . '"';
					}					


					if(DB::statement('call insert_a1a('.implode(',',$insert_data).')')){
						$this->InsertWager($data);
					} 
				}
				
			}
			Profitloss::updatePNL2($this->profitloss);
		}
		
	 
	}	

	private function InsertWager( $bet ){
		
		if( !isset($this->accounts[$bet['user']]) )
			$this->accounts[$bet['user']] = Account::where( 'nickname' , '=' , $bet['user'] )->first();
		
		$prdObj = Cache::rememberForever('product_a1a_obj', function(){
				return Product::where( 'code' , '=' , 'A1A' )->first();
		});

/* 		$lastTranId = Beta1a::where( 'transId' , '<' ,  $bet['transId'] )->
							  where( 'BetInfo' , '=' ,  'CollectWin' )->
							  where( 'user'    , '=' ,  $bet['user'] )->
							  orderBy( 'transId', 'asc' )->pluck('transId');
		if( !$lastTranId ){
			$lastTranId = Beta1a::where( 'transId' , '<' ,  $bet['transId'] )->
							  where( 'user'    , '=' ,  $bet['user'] )->
							  orderBy( 'transId', 'asc' )->pluck('transId');
		}		
	
		$stake = 	Beta1a::where( 'transId' , '>=' ,  $lastTranId )->
							where( 'user'    , '=' ,  $bet['user'] )->	  
							where( 'transId' , '<' ,   $bet['transId'])->sum('BetAmount');				  
				 */			  
	
		$data['accid'] 		 		= $this->accounts[$bet['user']]->id;
		$data['acccode'] 	 		= $this->accounts[$bet['user']]->code;
		$data['nickname'] 	 		= $bet['user'];
		$data['wbsid'] 		 		= $this->accounts[$bet['user']]->wbsid;
		$data['prdid'] 		 		= $prdObj->id;
		$data['matchid'] 	 		= $bet['GameID'];
		$data['gamename'] 	 		= $bet['BetInfo'];
		$data['category'] 	 		= Product::getCategory('egames');
		$data['refid'] 		 		= $bet['transId'];
		$data['crccode'] 	 		= $this->accounts[$bet['user']]->crccode;
		$data['crcrate'] 	 		= Currency::getCurrencyRate($data['crccode']);
		$data['datetime'] 	 		= $bet['dateTime'];
		$data['ip'] 		 		= '';
		$data['payout'] 	 		= (float) ( $bet['WinLose'] > 0 ? $bet['WinLose'] : 0 );
		$data['profitloss']  		= $bet['WinLose'];
		$data['accountdate'] 		= substr( $bet['dateTime'], 0 , 10 );
		$data['stake']		 		= $bet['BetAmount'];
		$data['status'] 			= Wager::STATUS_SETTLED;
		
		if( $data['stake'] == $data['payout'] )	    $data['result']  = Wager::RESULT_DRAW;
		else if( $data['stake'] > $data['payout'] ) $data['result']  = Wager::RESULT_LOSS;
		else if( $data['stake'] < $data['payout'] ) $data['result']  = Wager::RESULT_WIN;
		$data['validstake'] 		= $bet['BetAmount'];
		$data['stakelocal'] 		= Currency::getLocalAmount($data['stake'], $data['crccode']);
		$data['payoutlocal']		= Currency::getLocalAmount($data['payout'], $data['crccode']);
		$data['profitlosslocal']	= Currency::getLocalAmount($data['profitloss'], $data['crccode']);
		$data['validstakelocal']	= Currency::getLocalAmount($data['validstake'], $data['crccode']);
		$data['iscalculated'] 	    = 0;
		$data['created']			= date("y-m-d H:i:s");
		$data['modified']			= date("y-m-d H:i:s");
		
		foreach( $data as $key => $value ){
			$insert_data[$key] = '"'. $value . '"';
		}
		

		if(DB::statement('call insert_wager('.implode(',',$insert_data).')'))
		{
			$this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['prdid']   = $data['prdid'];
			$this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['accobj']  = $this->accounts[$bet['user']];
			$this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['crcrate'] = $data['crcrate'];
		}
		

	}
	
	private function getLatestTransactionTime()
	{
		$date = Beta1a::orderBy('dateTime','Desc')->pluck('dateTime');
		
		if(!$date)
			
		$date = '2015-11-01 00:00:00';
		
		return $date;

	}
	

}
