<?php namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller\Api;
use App\Models\Betosg;
use App\Models\Account;
use App\Models\Product;
use App\Models\Wager;
use App\Models\Currency;
use App\Models\Profitloss;
use App\Models\Configs;
use App\Models\Onlinetracker;
use App\libraries\providers\OSG;
use App\libraries\Array2XML;
use App\libraries\App;
use Illuminate\Http\Request;
use App\libraries\Login;
use Config;
use DB;
use Cache;
use Log;
use Auth;
use Carbon\Carbon;

class OsgController extends MainController{
	
	protected $accounts = array();
	
	protected $profitloss = array();

	public function index(Request $request)
	{
        // URL example for repull:
        // http://[domain.com]/Api/osg?currency=[MYR]&startdate=[2016-12-27%2000:00:00]&enddate=[2016-12-27%2023:59:59]

        date_default_timezone_set("Asia/Kuala_Lumpur");

        $lockname = 'osg_datapull_process_'.$request->currency;
        $dt = Carbon::now();
        $updatePullDateParam = false;

        $dtFrom = $request->input('startdate');
        $dtTo = $request->input('enddate');

        if (is_null($dtFrom)) {
            $dtFrom = Configs::getParam('SYSTEM_OSG_PULLDATE_' . $request->currency);
            $updatePullDateParam = true;

            if (strlen($dtFrom) < 1) {
                $dtFrom = '2016-10-13 11:30:00';
            }
        } elseif ($dtFrom == 'ytd') {
            $lockname .= 'ytd';
            $yesterdayDt = Carbon::now()->addDays(-1);
            $dtFrom = $yesterdayDt->setTime(0, 0, 0)->toDateTimeString();
            $dtTo = $yesterdayDt->setTime(23, 59, 59)->toDateTimeString();
        }

        App::check_process($lockname);

        if (is_null($dtTo)) {
            $dtTo = $dt->copy();
        }

        $dtFrom = Carbon::parse($dtFrom);
        $dtTo = Carbon::parse($dtTo);

        $apiFn = 'GetBetLogTranSubID';
        $minTemp = 0;
        $optId = Config::get($request->currency.'.osg.optId');
        $optPassword = Config::get($request->currency.'.osg.optPassword');
        $apiUrl = Config::get($request->currency.'.osg.url') . 'OPAPI/GetBetLogTranSubID';
        $signature = md5($optId.$apiFn.$dt->toDateTimeString().Config::get($request->currency.'.osg.key'));
        $header[] = 'DataType: XML';
        $header[] = 'Content-Type: application/x-www-form-urlencoded';
        $continue = false;

        do {
            $continue = false;

            if( $dtTo->diffInMinutes($dtFrom) > 30 ){
                // End datetime is over 30 mins, use time interval of +30 mins instead.
                $dtFromStr = $dtFrom->addMinutes($minTemp)->toDateTimeString();
                $dtToStr = $dtFrom->copy()->addMinutes(30)->toDateTimeString();
                $minTemp = 30;
                $continue = true;
            } else {
                $dtFromStr = $dtFrom->toDateTimeString();
                $dtToStr = $dtTo->toDateTimeString();
            }

            $currentPage = 1;
            $totalPages = 0;

            do {
                $postfields  = '<?xml version="1.0" encoding="UTF-8"?>';
                $postfields .= '<'.$apiFn.'>';
                $postfields .= '<OperatorID>'.$optId.'</OperatorID>';
                $postfields .= '<OperatorPassword>'.$optPassword.'</OperatorPassword>';
                $postfields .= '<RequestDateTime>'.$dt->toDateTimeString().'</RequestDateTime>';
                $postfields .= '<DateFrom>'.$dtFromStr.'</DateFrom>';
                $postfields .= '<DateTo>'.$dtToStr.'</DateTo>';
                $postfields .= '<Page>'.$currentPage.'</Page>';
                $postfields .= '<RecordsPerPage>500</RecordsPerPage>'; // Max: 1000
                $postfields .= '<Signature>'.$signature.'</Signature>';
                $postfields .= '</'.$apiFn.'>';

                $result = simplexml_load_string( App::curlXML( $postfields , $apiUrl, true, 30 , '', $header) );

                if( isset($result->Status) && $result->Status == 200 && isset($result->Bets->Result) ){
                    $data = array();

                    foreach ($result->Bets->Result as $bi) {
                        $dateTimeNow = Carbon::now();
                        $dateTimeNowStr = $dateTimeNow->toDateTimeString();

                        $accIdTemp = explode('@', $bi->PlayerID);

                        $WinAmount   		  =	($bi->PlayerCurrency == 'IDR')? $bi->WinAmount /1000:$bi->WinAmount;
                        $BetAmount    		  = ($bi->PlayerCurrency == 'IDR')? $bi->BetAmount /1000:$bi->BetAmount;
                        $TotalAmount  		  = ($bi->PlayerCurrency == 'IDR')? $bi->TotalAmount /1000:$bi->TotalAmount;
                        $JackpotContribution  = ($bi->PlayerCurrency == 'IDR')? $bi->JackpotContribution /1000:$bi->JackpotContribution;

                        $data = array(
                            'playerId'			=> $accIdTemp[1],
                            'playerCurrency'	=> (string) $bi->PlayerCurrency,
                            'gameId'			=> (string) $bi->GameID,
                            'gameName'			=> (string) $bi->GameName,
                            'gameCategory'		=> (string) $bi->GameCategory,
                            'tranId'			=> (int) $bi->TranID,
                            'tranSubId'			=> (string) $bi->TranSubID,
                            'totalAmount'		=> (float) $TotalAmount,
                            'betAmount'			=> (float) $BetAmount,
                            'jackpotContribution'	=> (float) $JackpotContribution,
                            'winAmount'			=> (float) $WinAmount,
                            'gameDate'			=> (string) $bi->GameDate,
                            'platform'			=> (string) $bi->Platform,
                            'created_at'		=> $dateTimeNowStr,
                            'updated_at'		=> $dateTimeNowStr,
                        );

                        foreach ($data as $key => $value) {
                            $insertData[$key] = '"'.$value.'"';
                        }

                        if (DB::statement('call insert_osg('.implode(',',$insertData).')')) {
                            // Get new inserted ID.
                            $this->InsertWager($data,$data['playerCurrency']);
                        }
                    }

                    if ($totalPages <= 0) {
                        if (isset($result->TotalPages) && $result->TotalPages > 0) {
                            $totalPages = $result->TotalPages;
                        }
                    }
                }

                Profitloss::updatePNL2($this->profitloss);

                // Reset array to prevent memory overflow.
                $this->accounts = array();
                $this->profitloss = array();

                $currentPage++;
            } while ($currentPage <= $totalPages);
        } while ( $continue );

        if ($updatePullDateParam) {
            Configs::updateParam('SYSTEM_OSG_PULLDATE_'.$request->currency, $dtTo->toDateTimeString());
        }

		App::unlock_process($lockname);

	}	

	private function InsertWager( $bet , $currency){
		
		if( !isset($this->accounts[$bet['playerId']]) )
			$this->accounts[$bet['playerId']] = Account::where( 'nickname' , '=' , $bet['playerId'] )->whereCrccode($currency)->first();
		
		$prdObj = Cache::rememberForever('product_osg_obj', function(){
				return Product::where( 'code' , '=' , 'OSG' )->first();
		});
        
        $account = $this->accounts[$bet['playerId']];
	
		$data['accid'] 		 		= $account->id;
		$data['acccode'] 	 		= $account->code;
		$data['nickname'] 	 		= $account->nickname;
		$data['wbsid'] 		 		= $account->wbsid;
		$data['prdid'] 		 		= $prdObj->id;
		$data['matchid'] 	 		= $bet['tranId'] == '' ? "''":$bet['tranId'];
		$data['gamename'] 	 		= $bet['gameName'];
		$data['category'] 	 		= Product::getCategory('egames');
		$data['refid'] 		 		= $bet['tranId'];
		$data['crccode'] 	 		= $account->crccode;
		$data['crcrate'] 	 		= Currency::getCurrencyRate($data['crccode']);
		$data['datetime'] 	 		= $bet['gameDate'];
		$data['ip'] 		 		= '';
		$data['payout'] 	 		= (float) $bet['winAmount'];
		$data['profitloss']			= ((float) $bet['winAmount']) - ((float) $bet['totalAmount']);
		$data['accountdate']			= substr( $data['datetime'], 0 , 10 );
		$data['stake']		 		= $bet['totalAmount'];
		$data['status'] 			= Wager::STATUS_SETTLED;
		

		if( $data['stake'] == $data['payout'] )	    $data['result']  = Wager::RESULT_DRAW;
		else if( $data['stake'] > $data['payout'] ) $data['result']  = Wager::RESULT_LOSS;
		else if( $data['stake'] < $data['payout'] ) $data['result']  = Wager::RESULT_WIN;
		$data['validstake'] 		= $bet['totalAmount'];
		$data['stakelocal'] 		= Currency::getLocalAmount($data['stake'], $data['crccode']);
		$data['payoutlocal']		= Currency::getLocalAmount($data['payout'], $data['crccode']);
		$data['profitlosslocal']	= Currency::getLocalAmount($data['profitloss'], $data['crccode']);
		$data['validstakelocal']	= Currency::getLocalAmount($data['validstake'], $data['crccode']);
		$data['iscalculated'] 	    = 0;
		$data['created']			= date("y-m-d H:i:s");
		$data['modified']			= date("y-m-d H:i:s");
		
		foreach( $data as $key => $value ){
			$insert_data[$key] = '"'. $value . '"';
		}
		

		if(DB::statement('call insert_wager('.implode(',',$insert_data).')'))
		{
			$this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['prdid']   = $data['prdid'];
			$this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['accobj']  = $account;
			$this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['crcrate'] = $data['crcrate'];
		}
		
	}
	
}
