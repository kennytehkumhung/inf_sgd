<?php namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller\Api;
use App\Models\Betm8b;
use App\Models\Betm8bteam;
use App\Models\Betm8bleague;
use App\Models\Account;
use App\Models\Product;
use App\Models\Wager;
use App\Models\Currency;
use App\Models\Profitloss;
use App\libraries\providers\M8B;
use Illuminate\Http\Request;
use App\libraries\App;
use Config;
use DB;
use Cache;

class M8bController extends MainController{
	
	protected $accounts = array();
	
	protected $profitloss = array();
	
	public function index(Request $request)
	{
		
        App::check_process('m8b_datapull_process');
						  
	   $postfields =    'action=fetch'.
					    '&secret='.Config::get($request->currency.'.m8b.secret').
						'&agent='.Config::get($request->currency.'.m8b.agent');

		$result = simplexml_load_string (App::curlGET(Config::get($request->currency.'.m8b.url').'?'.$postfields , false, 30 ));
       // var_dump($result);
		if(isset($result->errcode) && $result->errcode == '0')
	    {
        
            $fetchedIds = array();

			foreach( $result->result->ticket as $value ){
				
				if (strtoupper($value->res) != 'P') {
                    
                    $data['transId']  		    = $value->id; 		
                    $data['fetchId']  		    = $value->fid; 		
                    $data['lastModifiedDate']   = $value->t; 		
                    $data['userId']   		    = ''; 		
                    $data['username']   		= str_replace(Config::get($request->currency.'.m8b.agent'),"",$value->u); 			
                    $data['betAmount']   		= $value->b; 				
                    $data['winAmount']   		= $value->w; 				
                    $data['commission']   		= $value->a; 				
                    $data['subcommission']   	= $value->c; 				
                    $data['ip']   			    = $value->ip; 				
                    $data['league']   			= $value->league; 				
                    $data['home']   			= $value->home; 				
                    $data['away']   			= $value->away; 				
                    $data['status']   			= $value->status; 				
                    $data['game']   			= $value->game; 				
                    $data['odds']   			= $value->odds; 				
                    $data['side']   			= $value->side; 				
                    $data['info']   			= $value->info; 				
                    $data['half']   			= $value->half; 				
                    $data['transDate']   		= $value->trandate; 				
                    $data['workDate']   		= $value->workdate; 				
                    $data['matchDate']   		= $value->matchdate; 				
                    $data['runScore']   		= $value->runscore; 				
                    $data['score']   			= $value->score; 				
                    $data['htscore']   			= $value->htscore; 				
                    $data['flgResult']   		= $value->flg; 				
                    $data['result']   			= $value->res; 	
                    $data['sportsType']   		= $value->sportstype; 	
                    $data['updated_at']   		= date("Y-m-d H:i:s"); 		
                    $data['created_at']   		= date("Y-m-d H:i:s");				

                    foreach( $data as $key => $value ){
                            $insert_data[$key] = '"'. $value . '"';
                    }					

    			    if(DB::statement('call insert_m8b('.implode(',',$insert_data).')')){
    					
                        $fetchedIds[] = $data['fetchId'];
    					$this->InsertWager($data);
    					
    				}  

                }else{
					 $fetchedIds[] = $value->fid;
				}
				
			}
			var_dump($fetchedIds);
			Profitloss::updatePNL2($this->profitloss);
			
            if (count($fetchedIds) > 0) {
                // Mark IDs as fetched.
                $postfields = 'action=mark_fetched'.
                    '&secret='.Config::get($request->currency.'.m8b.secret').
                    '&agent='.Config::get($request->currency.'.m8b.agent').
                    '&fetch_ids='.implode(',', $fetchedIds);

                $result = simplexml_load_string (App::curlGET(Config::get($request->currency.'.m8b.url').'?'.$postfields , false, 10 ));
				
				var_dump($postfields);
				var_dump($result);
            }
            
	    }
		
		App::unlock_process('m8b_datapull_process');
		
        // Get league and team name.
        // In case of any exception happens, only start pull league and team name
        // after datapull is finished.
        $transIds = $this->getMissingLeagueAndTeamNameIds();
        $this->getLeagueAndTeamName($request->currency, $transIds);
		
	}	
    
    private function getMissingLeagueAndTeamNameIds()
    {
        $leagueIds = array();
        $teamIds = array();
        $recordObjs = DB::table((new Betm8b())->getTable().' as bet')
            ->leftJoin((new Betm8bleague())->getTable().' as league', 'league.league_id', '=', 'bet.league')
            ->leftJoin((new Betm8bteam())->getTable().' as home', 'home.team_id', '=', 'bet.home')
            ->leftJoin((new Betm8bteam())->getTable().' as away', 'away.team_id', '=', 'bet.away')
            ->orWhereNull('league.league_name')
            ->orWhereNull('home.team_name')
            ->orWhereNull('away.team_name')
            ->get([
                'bet.league as leagueId', 'bet.home as homeId', 'bet.away as awayId', 
                'league.league_name as leagueName', 'home.team_name as homeName', 'away.team_name as awayName'
            ]);
        
        foreach ($recordObjs as $r) {
            if (strlen($r->leagueName) <= 0) {
                $leagueIds[] = $r->leagueId;
            }
            
            if (strlen($r->homeName) <= 0) {
                $teamIds[] = $r->homeId;
            }
            
            if (strlen($r->awayName) <= 0) {
                $teamIds[] = $r->awayId;
            }
        }
        
        return array('league_ids' => array_unique($leagueIds), 'team_ids' => array_unique($teamIds));
    }
    
    public function getLeagueAndTeamName($currency, $transIds) {
        
        if (count($transIds) > 0) {
            // API configs.
            $apiUrl = Config::get($currency.'.m8b.url').'?';
            $apiSecret = Config::get($currency.'.m8b.secret');
            $apiAgent = Config::get($currency.'.m8b.agent');
            
            // League and Team db table have very similiar properties, 
            // so create a standard settings and use loop to gather data.
            $settings = array(
                'league_ids' => array(
                    'api_action' => 'league',
                    'api_param_key' => 'league_id',
                    'api_log_name' => 'getLeague',
                    'table' => (new Betm8bleague())->getTable(),
                    'table_col_id' => 'league_id',
                    'table_col_name' => 'league_name',
                ),
                'team_ids' => array(
                    'api_action' => 'team',
                    'api_param_key' => 'team_id',
                    'api_log_name' => 'getTeam',
                    'table' => (new Betm8bteam())->getTable(),
                    'table_col_id' => 'team_id',
                    'table_col_name' => 'team_name',
                ),
            );
            
            foreach ($transIds as $key => $val) {
                if (count($val) > 0) {
                    $setting = $settings[$key];
                    
                    // Call API to get name and save it into database.
                    foreach ($val as $id) {
                        $url = $apiUrl.
                            'action='.$setting['api_action'].
                            '&'.$setting['api_param_key'].'='.$id.
                            '&username='.
                            '&secret='.$apiSecret.
                            '&agent='.$apiAgent;

                        $result = simplexml_load_string(App::curlGET($url, true, 10, $setting['api_log_name']));

                        if (isset($result->errcode) && $result->errcode == 0 && isset($result->result)) {
                            // Collect and insert only English name.
                            foreach ($result->result->name as $rw) {
                                if ($rw->lang == 'en-US') {
                                    $dbNow = date('Y-m-d H:i:s');

                                    DB::table($setting['table'])->insert(array(
                                        $setting['table_col_id'] => $id,
                                        $setting['table_col_name'] => $rw->txt,
                                        'created_at' => $dbNow,
                                        'updated_at' => $dbNow,
                                    ));

                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }
        
    }
	
	private function InsertWager( $m8b ){

		if( !isset($this->accounts[$m8b['username']]) )
			$this->accounts[$m8b['username']] = Account::where( 'nickname' , '=' , $m8b['username'] )->first();
		
		$prdObj = Cache::rememberForever('product_m8b_obj', function(){
				return Product::where( 'code' , '=' , 'M8B' )->first();
		});
		
		$data['accid'] 		 		= $this->accounts[$m8b['username']]->id;
		
		$data['acccode'] 	 		= $this->accounts[$m8b['username']]->code;
		
		$data['nickname'] 	 		= $m8b['username'];
		$data['wbsid'] 		 		= $this->accounts[$m8b['username']]->wbsid;
		$data['prdid'] 		 		= $prdObj->id;
		
		$data['matchid'] 	 		= preg_replace('/[a-zA-Z]/', '', $m8b['transId']);
		$data['gamename'] 	 		= 'Sports';
		$data['category'] 	 		= Product::getCategory('sports');
		$data['refid'] 		 		= $m8b['fetchId'];
		$data['crccode'] 	 		= $this->accounts[$m8b['username']]->crccode;
		$data['crcrate'] 	 		= Currency::getCurrencyRate($data['crccode']);
		$data['datetime'] 	 		= $m8b['transDate'];
		$data['ip'] 		 		= $m8b['ip'];
	
			
		$data['payout'] 	 		= (float) ( $m8b['betAmount'] + $m8b['winAmount'] );
		$data['profitloss']  		= $m8b['winAmount'];
		$data['accountdate'] 		= substr( $m8b['workDate'], 0 , 10 );
		$data['stake']		 		= $m8b['betAmount'];

		$data['status'] = Wager::STATUS_SETTLED;
		
		$validstake = $m8b['commission'];
		
		if( $m8b['winAmount'] > 0 ) 
			$data['result']  = Wager::RESULT_WIN;
		elseif( $m8b['winAmount'] == 0 ){
			$data['result']  = Wager::RESULT_DRAW;
			$validstake		 = 0;
		}
		else{
			$data['result'] = Wager::RESULT_LOSS;
			//$validstake		= $data['payout'];
		}
		
		if( $data['stake']/2 == $data['payout']){
			
			$validstake		= $data['stake']/2;
		}
		
		if( $data['profitloss'] > 0 && $data['profitloss'] <= $data['stake']/2 )
		{
			$data['validstake'] 	= $data['stake']/2;	
		}
		
		if( $data['profitloss'] < 0 && ($data['profitloss'] * -1) <= $data['stake']/2 )
		{
			$data['validstake'] 	= $data['stake']/2;	
		}
		

		$data['validstake'] 		= $validstake;
		
		$data['stakelocal'] 		= Currency::getLocalAmount($data['stake'], 		$data['crccode']);
		$data['payoutlocal']		= Currency::getLocalAmount($data['payout'],		$data['crccode']);
		$data['profitlosslocal']	= Currency::getLocalAmount($data['profitloss'], $data['crccode']);
		$data['validstakelocal']	= Currency::getLocalAmount($data['validstake'], $data['crccode']);
		$data['iscalculated']	    = 0;
		$data['created']			= date("y-m-d H:i:s");
		$data['modified']			= date("y-m-d H:i:s");

		foreach( $data as $key => $value ){
			$insert_data[$key] = '"'. $value . '"';
		}
		

		if(DB::statement('call insert_wager('.implode(',',$insert_data).')'))
		{
			$this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['prdid']   = $data['prdid'];
			$this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['accobj']  = $this->accounts[$m8b['username']];
			$this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['crcrate'] = $data['crcrate'];
		}
		
	}

}
