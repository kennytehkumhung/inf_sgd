<?php namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller\Api;
use Crypt;
use App\Models\Betisn;
use App\Models\Account;
use App\Models\Product;
use App\Models\Currency;
use App\Models\Profitloss;
use App\Models\Wager;
use App\Models\Configs;
use App\Models\Balancetransfer;
use App\libraries\App;
use App\libraries\providers\ISN;
use Exception;
use Illuminate\Http\Request;
use Config;
use DB;
use Cache;
use Carbon\Carbon;
use Log;


class Uc8Controller extends MainController{
	
	protected $accounts = array();

	protected $profitloss = array();

    public function index(Request $request)
	{
		
    }
	
	private function InsertWager( $isn, $currency ){

	}
    
    public function authorizePlayer($currency, Request $request) {

        $currency = strtoupper($currency);

        $response = array(
            'error' => 3,
            'message' => 'User session expired',
			'username' => '',
			'currency' => $currency,
			'ip' => $request->ip(),
        );
		if($request->vendor_key == Config::get($currency.'.uc8.vendor_key')){
			try {
				$exp = explode("_",$request->token);
				$explode = $exp[1];
				
				$accountObj = Account::where('nickname', '=' , $explode)
					->where('crccode', '=', $currency)
					->where('status','=',1)
					->first();

				if ($accountObj) {
					$response['error'] = 0;
					$response['message'] = 'Success';
					$response['username'] = $explode;
					$response['currency'] = $currency;
					$response['ip'] = $request->ip();
				}
			} catch (Exception $ex) {
				// Do nothing.
			}
		}
        
		return response()->json($response);
        
    }
    
}

