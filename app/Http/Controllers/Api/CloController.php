<?php namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller\Api;
use App\Models\Betclo;
use App\Models\Betcloresult;
use App\Models\Account;
use App\Models\Product;
use App\Models\Wager;
use App\Models\Currency;
use App\Models\Profitloss;
use App\Models\Hiswl;
use Illuminate\Http\Request;
use Config;
use App\libraries\App;
use DB;
use Cache;


class CloController extends MainController{
    
    protected $accounts = array();
	
    protected $profitloss = array();
    
    public function index(Request $request){
        $continue = true;
        $page = 0;
        $currentdate = App::getDateTime(3);
        $drawdate = '';
        
        $fields =   'token='.Config::get($request->currency.'.clo.token');
        
        $darwdates = json_decode( App::curl( $fields , Config::get($request->currency.'.clo.url').'DrawDateList.aspx' , true, 30 , 'DrawDateList') );	
        
        if($darwdates->status == 1){
            foreach($darwdates->betdates as $key => $darwdate){
                if($darwdate->drawdate >= $currentdate){
                    $drawdate = $darwdate->drawdate;
                }
            }
        }
        
	while($continue){
            $postfields =   'token='.Config::get($request->currency.'.clo.token').
                            '&dte='.$drawdate.
                            '&pge='.$page;
            
            $results = json_decode( App::curl( $postfields , Config::get($request->currency.'.clo.url').'BetListMax.aspx' , true, 30 , 'BetListMax') );	
            
            if($results->status == 1){
                foreach($results->betdatas as $key => $result){
                    $data['resitid']            = (int)$result->resitid;
                    $data['betid']              = (int)$result->betid;
                    $data['ip']                 = (string)$result->ip;
                    $data['username']           = (string)$result->username;			
                    $data['logdate']            = date( "Y-m-d H:i:s", strtotime($result->logdate));
                    $data['drawdate']           = $drawdate;
                    $data['betnumber']          = (int)$result->betnumber;
                    $data['bettype']            = (string)$result->bettype;
                    $data['stake']              = (double)$result->stake;
                    $data['payout']             = (double)$result->payout;
                    $data['strike']             = (double)$result->strike;
                    $data['status']             = (string)$result->status;
                    $data['closestatus']         = (string)$result->closestatus;
                    $data['created_at']		= date("y-m-d H:i:s");
                    $data['updated_at']		= date("y-m-d H:i:s");
                    
                    foreach( $data as $key => $value ){
                        $insert_data[$key] = '"'. $value . '"';
                    }					

                    if(DB::statement('call insert_clo('.implode(',',$insert_data).')')){
                        $this->InsertWager($data);
                    }
                }
                Profitloss::updatePNL2($this->profitloss);
            }
            else{
                $continue = false;
            }
            
            if( isset($results->totalpages) && $results->totalpages > 0 ){
                    if($page == $results->totalpages){
                       $continue = false; 
                    }
                    $page++;
            }
            else{
                    $continue = false;
            }
        }
        
        
    }
    
    public function getResult(Request $request){
        $currentdate = App::getDateTime(3);
        //$currentdate = '2016-11-01';
        //App::check_process('psb_getresult_process');
        
        $fields =   'token='.Config::get($request->currency.'.clo.token').
                    '&dte='.$currentdate;
        
        $darwdates = json_decode( App::curl( $fields , Config::get($request->currency.'.clo.url').'Results.aspx' , false, 30 , 'DrawResult') );	
        
        if($darwdates->status == 1){
            $sixtop = '';
            $threebtm = array();
            $twobtm = '';
            foreach($darwdates->betresults as $key => $result){
                if($result->gametype == 6 && $result->bettype == 12){
                    $sixtop = $result->number;
                } 
                else if($result->gametype == 3 && $result->bettype == 7){
                    $threebtm[] = $result->number;
                }
                else if($result->gametype == 2 && $result->bettype == 4){
                    $twobtm = $result->number;
                }
            }
            $threebtms = implode(',',$threebtm);
            
            if(Betcloresult::where( 'drawdate' , '=' , $result->drawdate )->count() == 0 ){
                $object = new Betcloresult;
                $object->drawdate = $result->drawdate;
                $object->sixtop = $sixtop;
                $object->threebottom = $threebtms;
                $object->twobottom = $twobtm;
                $object->save();
            }
        }

        //App::unlock_process('psb_getresult_process');
		
    }
    
    private function InsertWager( $clo ){
        
		if( !isset($this->accounts[$clo['username']]) )
			$this->accounts[$clo['username']] = Account::where( 'nickname' , '=' , $clo['username'] )->first();
                
		$prdObj = Cache::rememberForever('product_clo_obj', function(){
				return Product::where( 'code' , '=' , 'CLO' )->first();
		});
	
		$data['accid'] 		 		= $this->accounts[$clo['username']]->id;
		$data['acccode'] 	 		= $this->accounts[$clo['username']]->code;
		$data['nickname'] 	 		= $clo['username'];
		$data['wbsid'] 		 		= $this->accounts[$clo['username']]->wbsid;
		$data['prdid'] 		 		= $prdObj->id;
		$data['matchid'] 	 		= $clo['resitid'] == '' ? "''":$clo['resitid'];
		$data['gamename'] 	 		= 'Lottery';
		$data['category'] 	 		= Product::getCategory('lottery');
		$data['refid'] 		 		= $clo['betid'];
		$data['crccode'] 	 		= $this->accounts[$clo['username']]->crccode;
		$data['crcrate'] 	 		= Currency::getCurrencyRate($data['crccode']);
		$data['datetime'] 	 		= $clo['logdate'];
		$data['ip'] 		 		= $clo['ip'];
		$data['payout'] 	 		= (float) $clo['payout'];
                $data['profitloss']  = 0;
                if($clo['closestatus'] == 'SETTLED'){
                    if( $clo['strike'] == 0 ) $data['profitloss']  = $clo['stake'] * -1;
                    else $data['profitloss']  = $clo['strike'];
                }
		$data['accountdate'] 		= $clo['drawdate'];
		$data['stake']		 		= $clo['stake'];
		if($clo['closestatus'] == 'OPEN') {
			$data['status'] = Wager::STATUS_OPEN;
		}
		else if($clo['closestatus'] == 'Refund') $data['status'] = Wager::STATUS_REFUND;
		else if($clo['closestatus'] == 'Reject') $data['status'] = Wager::STATUS_REJECTED;
		else $data['status'] = Wager::STATUS_SETTLED;
                
		if($clo['closestatus'] == 'OPEN') $data['result'] = Wager::RESULT_PENDING;
		else if($clo['closestatus'] == 'SETTLED'){
                    if( $clo['strike'] > 0 ) $data['result']  = Wager::RESULT_WIN;
                    else $data['result']  = Wager::RESULT_LOSS;
                }
		
		$data['validstake'] 		= $clo['stake'];
		$data['stakelocal'] 		= Currency::getLocalAmount($data['stake'], $data['crccode']);
		$data['payoutlocal']		= Currency::getLocalAmount($data['payout'], $data['crccode']);
		$data['profitlosslocal']	= Currency::getLocalAmount($data['profitloss'], $data['crccode']);
		$data['validstakelocal']	= Currency::getLocalAmount($data['validstake'], $data['crccode']);
		$data['iscalculated'] 	    = 0;
		$data['created']			= date("y-m-d H:i:s");
		$data['modified']			= date("y-m-d H:i:s");
		
		foreach( $data as $key => $value ){
			$insert_data[$key] = '"'. $value . '"';
		}
		
		if(DB::statement('call insert_wager('.implode(',',$insert_data).')'))
		{
			$this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['prdid']   = $data['prdid'];
			$this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['accobj']  = $this->accounts[$clo['username']];
			$this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['crcrate'] = $data['crcrate'];
		}
    }
}
