<?php namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller\Api;
use App\Models\Betagg;
use App\Models\Account;
use App\Models\Product;
use App\Models\Wager;
use App\Models\Currency;
use App\Models\Profitloss;
use App\Models\Betwft;
use App\libraries\providers\WFT;
use Illuminate\Http\Request;
use App\libraries\App;
use Config;
use DB;
use Cache;

class WftController extends MainController{
	
	protected $accounts = array();
	
	protected $profitloss = array();
	
	public function index(Request $request)
	{
		
	   App::check_process('wft_datapull_process');
	   
	    if (Product::where('status', '=', 2)->where('code', '=', 'WFT')->count() == 1)
		{
			App::unlock_process('wft_datapull_process');
			exit;
		}
	
					  
	   $postfields =    'action=fetch'.
					    '&secret='.Config::get($request->currency.'.wft.secret').
						'&agent='.Config::get($request->currency.'.wft.agent');



		$result =  simplexml_load_string(App::curlGET(Config::get($request->currency.'.wft.url').'?'.$postfields , false, 10 ));

		if(isset($result->errcode) && $result->errcode == '0')
	    {
			foreach( $result->result->ticket as $value ){
				
				
				if (strtoupper($value->res) != 'P') {
						$data['transId']  		    = $value->id; 		
						$data['fetchId']  		    = $value->fid; 		
						$data['lastModifiedDate']   = $value->t; 		
						$data['userId']   		    = ''; 		
						$data['username']   		= str_replace(Config::get($request->currency.'.wft.agent'),"",$value->u); 			
						$data['betAmount']   		= $value->b; 				
						$data['winAmount']   		= $value->w; 				
						$data['commission']   		= $value->a; 				
						$data['subcommission']   	= $value->c; 				
						$data['ip']   			    = $value->ip; 				
						$data['league']   			= $value->league; 				
						$data['home']   			= $value->home; 				
						$data['away']   			= $value->away; 				
						$data['status']   			= $value->status; 				
						$data['game']   			= $value->game; 				
						$data['odds']   			= $value->odds; 				
						$data['side']   			= $value->side; 				
						$data['info']   			= $value->info; 				
						$data['half']   			= $value->half; 				
						$data['transactionDate']   	= $value->trandate; 				
						$data['workDate']   		= $value->workdate; 				
						$data['matchDate']   		= $value->matchdate; 				
						$data['runScore']   		= $value->runscore; 				
						$data['score']   			= $value->score; 				
						$data['htscore']   			= $value->htscore; 				
						$data['firstLastGoal']   	= $value->flg; 				
						$data['flgStatus']   		= $value->res; 
						$data['gameDescription']   	= $value->edesc; 					
						$data['gameResult']   		= $value->eres; 				
						$data['exchangeRate']   	= $value->exrate; 				
						$data['oddsType']   		= $value->oddstype; 				
						$data['sportsType']   		= $value->sportstype; 				
						$data['isOver']   			= 1; 				
						$data['created']		   = date("y-m-d H:i:s");
						$data['modified']		   = date("y-m-d H:i:s");



						foreach( $data as $key => $value ){
								$insert_data[$key] = '"'. $value . '"';
						}					


						if(DB::statement('call insert_wft('.implode(',',$insert_data).')')){
							
							$fetchedIds[]                 = $data['fetchId'];
							$this->InsertWager($data,$request->currency);
							
						}  
				 }else{
					 $fetchedIds[] = $value->fid;
				}
				
			}
			Profitloss::updatePNL2($this->profitloss);
			
			if ( isset($fetchedIds) && count($fetchedIds) > 0) {
                // Mark IDs as fetched.
                $postfields = 'action=mark_fetched'.
                    '&secret='.Config::get($request->currency.'.wft.secret').
                    '&agent='.Config::get($request->currency.'.wft.agent').
                    '&fetch_ids='.implode(',', $fetchedIds);

                $result = simplexml_load_string (App::curlGET(Config::get($request->currency.'.wft.url').'?'.$postfields , false, 10 ));
				
				var_dump($postfields);
				var_dump($result);
            }
	    }
		

		
		App::unlock_process('wft_datapull_process');
		
	}	
	
	public function doRepull(Request $request){
		
		App::check_process('wft_repull_process_'.$request->currency);
		echo 'asd';
		$start = 0;
		$limit = 1000;
		$continue = true;
		while($continue){
			if( $wfts = Betwft::whereBetween( 'workDate' , array($request->startdate,$request->enddate))->take($limit)->skip($start)->get())
			{				
				if(count($wfts) == 0){
					$continue = false;
				}
				foreach( $wfts as $wft )
				{
					if($wft->exchangeRate == 1)
						$this->InsertWager( $wft->toArray() , 'MYR' , true );
					else
						$this->InsertWager( $wft->toArray() , 'IDR' , true );
					
				}
				Profitloss::updatePNL2($this->profitloss);
				$this->profitloss = array();
				$start += 1000;
			} 
		}
		
		App::unlock_process('wft_repull_process_'.$request->currency);
		
	}
	
	private function InsertWager( $wft , $currency, $repull = false){

		if( !isset($this->accounts[$wft['username']]) )
			$this->accounts[$wft['username']] = Account::where( 'nickname' , '=' , $wft['username'] )->whereCrccode($currency)->first();
		
		$prdObj = Cache::rememberForever('product_wft_obj', function(){
				return Product::where( 'code' , '=' , 'WFT' )->first();
		});

		$data['accid'] 		 		= $this->accounts[$wft['username']]->id;
		$data['acccode'] 	 		= $this->accounts[$wft['username']]->code;
		$data['nickname'] 	 		= $wft['username'];
		$data['wbsid'] 		 		= $this->accounts[$wft['username']]->wbsid;
		$data['prdid'] 		 		= $prdObj->id;
		$data['matchid'] 	 		= $repull ? $wft['transId'] : $wft['fetchId'];
		$data['gamename'] 	 		= $wft['game'];
		$data['category'] 	 		= Product::getCategory('sports');
		$data['refid'] 		 		= preg_replace('/[a-zA-Z]/', '', $repull ? $wft['fetchId'] : $wft['transId'] );
		$data['crccode'] 	 		= $this->accounts[$wft['username']]->crccode;
		$data['crcrate'] 	 		= Currency::getCurrencyRate($data['crccode']);
		$data['datetime'] 	 		= $wft['transactionDate'];
		$data['ip'] 		 		= $wft['ip'];
	
			
		$data['payout'] 	 		= (float) ( $wft['betAmount'] + $wft['winAmount'] );
		$data['profitloss']  		= $wft['winAmount'];
		$data['accountdate'] 		= substr( $wft['workDate'], 0 , 10 );
		$data['stake']		 		= $wft['betAmount'];

		$data['status'] = Wager::STATUS_SETTLED;
		
		if( $wft['winAmount'] > 0 ) 
			$data['result']  = Wager::RESULT_WIN;
		elseif( $wft['winAmount'] == 0 )
			$data['result']  = Wager::RESULT_DRAW;
		else
			$data['result'] = Wager::RESULT_LOSS;
		

		$data['validstake'] 		= $wft['commission'];
		
		$data['stakelocal'] 		= Currency::getLocalAmount($data['stake'], 		$data['crccode']);
		$data['payoutlocal']		= Currency::getLocalAmount($data['payout'],		$data['crccode']);
		$data['profitlosslocal']	= Currency::getLocalAmount($data['profitloss'], $data['crccode']);
		$data['validstakelocal']	= Currency::getLocalAmount($data['validstake'], $data['crccode']);
		$data['iscalculated']	    = 0;
		$data['created']			= date("y-m-d H:i:s");
		$data['modified']			= date("y-m-d H:i:s");

		foreach( $data as $key => $value ){
			$insert_data[$key] = '"'. $value . '"';
		}
		

		if(DB::statement('call insert_wager('.implode(',',$insert_data).')'))
		{
			$this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['prdid']   = $data['prdid'];
			$this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['accobj']  = $this->accounts[$wft['username']];
			$this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['crcrate'] = $data['crcrate'];
		}
		
	}

}
