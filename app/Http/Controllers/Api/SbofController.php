<?php namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller\Api;
use App\Models\Betsbof;
use App\Models\Account;
use App\Models\Product;
use App\Models\Wager;
use App\Models\Currency;
use App\Models\Profitloss;
use App\Models\Onlinetracker;
use App\libraries\providers\SBOF;
use App\libraries\Array2XML;
use App\libraries\App;
use Illuminate\Http\Request;
use App\libraries\Login;
use Config;
use DB;
use Cache;
use Log;
use Auth;
use Carbon\Carbon;

class SbofController extends MainController{
	
	protected $accounts = array();
	
	protected $profitloss = array();

	public function index(Request $request)
	{
		App::check_process('sbof_datapull_process');

		date_default_timezone_set("Asia/Kuala_Lumpur");
		$tradeType = '1';
		$agtId = Config::get($request->currency.'.sbof.agtid');
		$timestamp = Carbon::now()->format('YmdHis');
		$startTime = $this->getLatestTransactionTime($request->currency);
		$endTime = Carbon::now()->addDays(1)->setTime(23, 59, 59)->format('YmdHis');
		$sign = md5(Config::get($request->currency.'.sbof.Key').$agtId.$tradeType.$timestamp.$startTime.$endTime);
		$postfields = 'agtid='.$agtId.
			'&uid='.
			'&tradeType='.$tradeType.
			'&timestamp='.$timestamp.
			'&startTime='.$startTime.
			'&endTime='.$endTime.
			'&sign='.$sign;

		$result = simplexml_load_string( App::curlGET( Config::get($request->currency.'.sbof.url').'op=usr_trade_data&'.$postfields ) );

		// For testing.
/* 		$result = simplexml_load_string('<?xml version="1.0"?>
					<response>
						<bet>
							<id>1653787</id>
							<User_id>1</User_id>
							<Match_id>885965</Match_id>
							<Currency>THB</Currency>
							<Odds_type>Over Under</Odds_type>
							<Odds_class>MY</Odds_class>
							<Odds_subtype>u</Odds_subtype>
							<Odds_id>2589261</Odds_id>
							<Odds>-0.81</Odds>
							<Stake>16.2</Stake>
							<Payout>36.2</Payout>
							<Active>False</Active>
							<Remarks>Win</Remarks>
							<RunningMinutes>0</RunningMinutes>
							<Sport_id>1001</Sport_id>
							<SportName>Football</SportName>
							<RegionName>Finland</RegionName>
							<LeagueName>FINLAND VEIKKAUSLIIGA</LeagueName>
							<EnLeagueName>FINLAND VEIKKAUSLIIGA</EnLeagueName>
							<TeamA>PK-35 Vantaa</TeamA>
							<TeamB>Ilves Tampere</TeamB>
							<Timestamp>2016-08-01 22:56:28.943</Timestamp>
							<MatchTime>2016-08-02 11:30:00.000</MatchTime>
							<BetTimestamp>2016-08-02 12:26:17.230</BetTimestamp>
							<Recalculation>False</Recalculation>
							<BetType>0</BetType>
							<Goal>0.75</Goal>
						    </bet>
						<error_code>0</error_code>
						<error_detail>Success</error_detail>
					    </response>
				'); */

		if (isset($result->error_code) && $result->error_code == '0') {
			foreach ($result->bet as $bet) {
				// Process only if bet have ID.
				if (isset($bet->id) && strlen($bet->id) > 0) {
					// API return user ID as integer, need convert into nickname.
					$accObj = Account::find($bet->User_id);

					$data = $insertData = array(); // Reset values.
					$dateTimeNow = Carbon::now()->toDateTimeString();
					$data = array(
						'betId'			=> (int) $bet->id,
						'userId'		=> $accObj->nickname,
						'matchId'		=> (int) $bet->Match_id,
						'currency'		=> (string) $bet->Currency,
						'oddsType'		=> (string) $bet->Odds_type,
						'oddsClass'		=> (string) $bet->Odds_class,
						'oddsSubtype'		=> (string) $bet->Odds_subtype,
						'oddsId'		=> (int) $bet->Odds_id,
						'odds'			=> $bet->Odds,
						'stake'			=> $bet->Stake,
						'payout'		=> $bet->Payout,
						'timestamp'		=> Carbon::parse($bet->Timestamp)->toDateTimeString(),
						'active'		=> (string) $bet->Active,
						'remarks'		=> (string) $bet->Remarks,
						'runningMinutes'	=> (int) $bet->RunningMinutes,
						'sportId'		=> (int) $bet->Sport_id,
						'sportName'		=> (string) $bet->SportName,
						'regionName'		=> (string) $bet->RegionName,
						'leagueName'		=> (string) $bet->LeagueName,
						'enLeagueName'		=> (string) $bet->EnLeagueName,
						'matchTime'		=> Carbon::parse($bet->MatchTime)->toDateTimeString(),
						'teamA'			=> (string) $bet->TeamA,
						'teamB'			=> (string) $bet->TeamB,
						'betTimestamp'		=> Carbon::parse($bet->BetTimestamp)->toDateTimeString(),
						'recalculation'		=> (string) $bet->Recalculation,
						'betType'		=> $bet->BetType,
						'handicap'		=> $bet->Handicap,
						'goal'			=> $bet->Goal,
						'scoreTeamA'		=> $bet->ScoreTeamA,
						'scoreTeamB'		=> $bet->ScoreTeamB,
						'createdAt'		=> $dateTimeNow,
						'updatedAt'		=> $dateTimeNow,
					);

					foreach ($data as $key => $value) {
						$insertData[$key] = '"'.$value.'"';
					}

					if (DB::statement('call insert_sbof('.implode(',',$insertData).')')) {
						$this->InsertWager($data);
					}
				}
			}
		}
		
		Profitloss::updatePNL2($this->profitloss);

		App::unlock_process('sbof_datapull_process');
	}	
	
	private function getLatestTransactionTime($currency)
	{
		$date = '20160801000000'; // Default trans date.
		$record = Betsbof::orderBy('betTimestamp','desc')->first(); // Get latest fetch date from DB.
		
		if($record) {
			$date = Carbon::parse($record->betTimestamp)->format('YmdHis');
		}
		
		return $date;
	}

	private function InsertWager( $bet ){
		
		if( !isset($this->accounts[$bet['userId']]) )
			$this->accounts[$bet['userId']] = Account::where( 'nickname' , '=' , $bet['userId'] )->first();
		
		$prdObj = Cache::rememberForever('product_sbof_obj', function(){
				return Product::where( 'code' , '=' , 'SBOF' )->first();
		});
	
		$data['accid'] 		 		= $this->accounts[$bet['userId']]->id;
		$data['acccode'] 	 		= $this->accounts[$bet['userId']]->code;
		$data['nickname'] 	 		= $bet['userId'];
		$data['wbsid'] 		 		= $this->accounts[$bet['userId']]->wbsid;
		$data['prdid'] 		 		= $prdObj->id;
		$data['matchid'] 	 		= $bet['matchId'] == '' ? "''":$bet['matchId'];
		$data['gamename'] 	 		= $bet['sportName'];
		$data['category'] 	 		= Product::getCategory('Sports');
		$data['refid'] 		 		= $bet['betId'];
		$data['crccode'] 	 		= $this->accounts[$bet['userId']]->crccode;
		$data['crcrate'] 	 		= Currency::getCurrencyRate($data['crccode']);
		$data['datetime'] 	 		= $bet['timestamp'];
		$data['ip'] 		 		= '';
		$data['payout'] 	 		= (float) ( $bet['payout'] - $bet['stake']);
		$data['profitloss']			= (float) $bet['payout'];
		$data['accountdate']			= substr( $bet['betTimestamp'], 0 , 10 );
		$data['stake']		 		= $bet['stake'];
		$data['status'] 			= Wager::STATUS_SETTLED;
		

		if( $data['stake'] == $data['payout'] )	    $data['result']  = Wager::RESULT_DRAW;
		else if( $data['stake'] > $data['payout'] ) $data['result']  = Wager::RESULT_LOSS;
		else if( $data['stake'] < $data['payout'] ) $data['result']  = Wager::RESULT_WIN;
		$data['validstake'] 		= $bet['stake'];
		$data['stakelocal'] 		= Currency::getLocalAmount($data['stake'], $data['crccode']);
		$data['payoutlocal']		= Currency::getLocalAmount($data['payout'], $data['crccode']);
		$data['profitlosslocal']	= Currency::getLocalAmount($data['profitloss'], $data['crccode']);
		$data['validstakelocal']	= Currency::getLocalAmount($data['validstake'], $data['crccode']);
		$data['iscalculated'] 	    = 0;
		$data['created']			= date("y-m-d H:i:s");
		$data['modified']			= date("y-m-d H:i:s");
		
		foreach( $data as $key => $value ){
			$insert_data[$key] = '"'. $value . '"';
		}
		

		if(DB::statement('call insert_wager('.implode(',',$insert_data).')'))
		{
			$this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['prdid']   = $data['prdid'];
			$this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['accobj']  = $this->accounts[$bet['userId']];
			$this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['crcrate'] = $data['crcrate'];
		}
		

	}
	
	public function doValidateTicket(Request $request){
		
		Log::info('sbof');		

		$string = file_get_contents('php://input');
		Log::info($string);	
		$merchantId = '';
		$errorCode  = '201';
		$errorMsg 	= '';
		$messageId  = '';
		$userId 	= '';
		$crccode	= '';
		
		$xml 		= simplexml_load_string($string);
		
		if(isset($xml->Param->Token)){
			$token		 = (string) $xml->Param->Token;
			$merchantId  = (string) $xml->Header->MerchantID;
			$messageId   = (string) $xml->Header->MessageID;
			
			if( $trackerObj = Onlinetracker::whereRaw('token="'.$token.'"')->first() ) 
			{
				$accObj = Account::find($trackerObj->userid);
				
				$errorCode = '0';
				$userId    = $accObj->nickname;
				$crccode   = $accObj->crccode;
				
			}
			
		}
		
		$str = '<?xml version="1.0" encoding="UTF-8"?>
		<Reply>
		  <Header>
			<Method>cMemberAuthentication</Method>
			<ErrorCode>'.$errorCode.'</ErrorCode>
			<MerchantID>'.$merchantId.'</MerchantID>
			<MessageID>'.$messageId.'</MessageID>
		  </Header>
		  <Param>
			<UserID>'.$userId.'</UserID>
			<CurrencyCode>'.$crccode.'</CurrencyCode>
			<ErrorDesc>'.$errorCode.'</ErrorDesc>
		  </Param>
		</Reply>';
		Log::info($str);	

		return response()->view('front/xml', array('name' =>  $str))->header('Content-Type', 'application/xml');	
	}
	
	public function doValidateMobile(Request $request){
		
		$token = '';
		
		$data = json_decode(file_get_contents('php://input'), true);
		
		$mode 	  = (isset($data['Mode'])?$data['Mode']:false);
		$hash	  = (isset($data['Hash'])?$data['Hash']:'');
		$username = (isset($data['Username'])?$data['Username']:false);
		$password = (isset($data['Password'])?$data['Password']:false);
		
		if(($mode && $mode === 'ClientLogin') && ($hash === md5($username.$password.Config::get(Account::whereNickname($username)->pluck('crccode').'.sbof.key')))) {
			if($username && $password) {
				if (Auth::user()->attempt(array('nickname' => $username, 'password' => $password , 'status' => 1 , 'istestacc' => 0 )))
				{	
					$login = new Login;
					$user = Auth::user()->get();
					$token = $login->doMakeTracker(Auth::user()->get(), 'Account');

				}
			}
		}
		Log::info($token);
		echo $token;
		
	}
	

}
