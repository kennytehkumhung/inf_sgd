<?php namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller\Api;
use App\Models\Betagg;
use App\Models\Account;
use App\Models\Product;
use App\Models\Wager;
use App\Models\Currency;
use App\Models\Profitloss;
use App\libraries\providers\AGG;
use Illuminate\Http\Request;
use Config;
use DB;
use Cache;

class Agg2Controller extends MainController{
	
	protected $accounts = array();
	
	protected $profitloss = array();
	
	public function index(Request $request)
	{

		$from = $this->getLatestTransactionTime();
		
		$page_num = 1;
		
		while($page_num != false){
			
			$wager = array();
		
			$postfields = 'vendor_id='.Config::get($request->currency.'.agg.vendor_id').
						'&operator_id='.Config::get($request->currency.'.agg.operator_id').
						'&page='.$page_num.
						'&from='.$from.
						'&to='.date('Y-m-d').' 23:59:59';
	
			$result = json_decode( $this->curl( $postfields , Config::get($request->currency.'.agg.url').'getBetDetail' , false) );

			if( isset($result->total) && $result->total != 0)
			{
				$query = 'INSERT IGNORE INTO `bet_agg` 
					(
						user_id, 
						transaction_time,
						trans_id,
						winlost_amount,
						game_code,
						game_type,
						stake,
						valid_stake,
						play_type,
						table_code,
						ip,
						recaculate_time,
						remark,
						currency,
						status,
						created_at,
						updated_at
					) values ';
					
				foreach( $result->trans as $key => $value )
				{
					$data['user_id'] 		   = strtoupper($value->user_id); 		
					$data['transaction_time']  = $value->transaction_time;
					$data['trans_id'] 		   = $value->trans_id;		
					$data['winlost_amount']    = $value->winlost_amount;  
					$data['game_code'] 		   = $value->game_code; 		
					$data['game_type']		   = $value->game_type;		
					$data['stake'] 			   = $value->stake; 			
					$data['valid_stake']	   = $value->valid_stake;		
					$data['play_type'] 		   = $value->play_type; 		
					$data['table_code']		   = $value->table_code;		
					$data['ip'] 			   = $value->ip; 				
					$data['recaculate_time']   = $value->recaculate_time;	
					$data['remark']			   = $value->remark;			
					$data['currency']		   = $value->currency;			
					$data['status'] 		   = $value->status; 	
					$data['created']		   = date("y-m-d H:i:s");
					$data['modified']		   = date("y-m-d H:i:s");

					$query .= '("'.implode('","',$data).'"),';
					
					$wager[] = $data;
				}
				
				$query = substr($query, 0, -1);
				
				DB::statement($query);
					
				$this->InsertWager($wager);

				if( $result->total != $result->page )
				{
					$page_num++;
				}else
				{
					$page_num = false;
				}
				Profitloss::updatePNL2($this->profitloss);
			}
		}
	}	
	
	private function getLatestTransactionTime()
	{
		$date = Betagg::orderBy('transaction_time','Desc')->pluck('transaction_time');
		
		if(!$date)
			
		$date = '2015-11-01 00:00:00';
		
		return $date;

	}
	
	private function InsertWager( $wagers ){
		
		$query = 'INSERT INTO `wager` (
							wager.accid, 
							wager.acccode,
							wager.nickname,
							wager.wbsid,
							wager.prdid,
							wager.matchid,
							wager.gamename,
							wager.category,
							wager.refid,
							wager.crccode,
							wager.crcrate,
							wager.datetime,
							wager.ip,
							wager.payout,
							wager.profitloss,
							wager.accountdate,
							wager.stake,
							wager.status,
							wager.result,
							wager.validstake,
							wager.stakelocal,
							wager.payoutlocal,
							wager.profitlosslocal,
							wager.validstakelocal,
							wager.iscalculated,
							wager.created,
							wager.modified
					  ) VALUES ';
		
		$query_str	    = '(';
		foreach( $wagers as $ag ){
			$query_str .= '"'.$ag['user_id'].'",';
		}
		$query_str = substr($query_str, 0, -1);
		$query_str	    .= ')';
		
		if( $accounts = Account::whereRaw( ' nickname IN'.$query_str.' ' )->get() ){
			foreach( $accounts as $account ){
				$this->accounts[$account->nickname] = $account;
			}
		}

		foreach( $wagers as $ag ){
		
			if( !isset($this->accounts[$ag['user_id']]) )
				$this->accounts[$ag['user_id']] = Account::where( 'nickname' , '=' , $ag['user_id'] )->first();
			
			$prdObj = Cache::rememberForever('product_agg_obj', function(){
					return Product::where( 'code' , '=' , 'AGG' )->first();
			});
		
			$data['accid'] 		 		= $this->accounts[$ag['user_id']]->id;
			$data['acccode'] 	 		= $this->accounts[$ag['user_id']]->code;
			$data['nickname'] 	 		= $ag['user_id'];
			$data['wbsid'] 		 		= $this->accounts[$ag['user_id']]->wbsid;
			$data['prdid'] 		 		= $prdObj->id;
			$data['matchid'] 	 		= $ag['game_code'] == '' ? "''":$ag['game_code'];
			$data['gamename'] 	 		= $ag['game_type'];
			$data['category'] 	 		= Product::getCategory('Casino');
			$data['refid'] 		 		= $ag['trans_id'];
			$data['crccode'] 	 		= $this->accounts[$ag['user_id']]->crccode;
			$data['crcrate'] 	 		= Currency::getCurrencyRate($data['crccode']);
			$data['datetime'] 	 		= $ag['transaction_time'];
			$data['ip'] 		 		= $ag['ip'];
			$data['payout'] 	 		= (float) ( $ag['stake'] + $ag['winlost_amount'] );
			$data['profitloss']  		= $ag['winlost_amount'];
			$data['accountdate'] 		= substr( $ag['transaction_time'], 0 , 10 );
			$data['stake']		 		= $ag['stake'];
			$data['status'] 			= Wager::STATUS_SETTLED;
			
			if( $data['stake'] == $data['payout'] )	    $data['result']  = Wager::RESULT_DRAW;
			else if( $data['stake'] > $data['payout'] ) $data['result']  = Wager::RESULT_LOSS;
			else if( $data['stake'] < $data['payout'] ) $data['result']  = Wager::RESULT_WIN;
			$data['validstake'] 		= $ag['valid_stake'];
			$data['stakelocal'] 		= Currency::getLocalAmount($data['stake'], $data['crccode']);
			$data['payoutlocal']		= Currency::getLocalAmount($data['payout'], $data['crccode']);
			$data['profitlosslocal']	= Currency::getLocalAmount($data['profitloss'], $data['crccode']);
			$data['validstakelocal']	= Currency::getLocalAmount($data['validstake'], $data['crccode']);
			$data['iscalculated'] 	    = 0;
			$data['created']			= date("y-m-d H:i:s");
			$data['modified']			= date("y-m-d H:i:s");
			
			
			
					  
			$query .= '("'.implode('","',$data).'"),';
			
			$this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['prdid']   = $data['prdid'];
			$this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['accobj']  = $this->accounts[$ag['user_id']];
			$this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['crcrate'] = $data['crcrate'];
		
		}
		
		$query = substr($query, 0, -1);
		
		$query .= ' ON DUPLICATE KEY UPDATE
				wager.payout = VALUES(wager.payout),
				wager.accountdate = VALUES(wager.accountdate),
				wager.stake = VALUES(wager.stake),
				wager.status = VALUES(wager.status),
				wager.validstake = VALUES(wager.validstake),
				wager.stakelocal = VALUES(wager.stakelocal),
				wager.payoutlocal = VALUES(wager.payoutlocal),
				wager.profitlosslocal = VALUES(wager.profitlosslocal),
				wager.validstakelocal = VALUES(wager.validstakelocal),
				wager.iscalculated = VALUES(wager.iscalculated),
				wager.modified = VALUES(wager.modified),
				wager.profitloss = VALUES(wager.profitloss)
				';
		
		
		DB::statement($query);
		

	}

}
