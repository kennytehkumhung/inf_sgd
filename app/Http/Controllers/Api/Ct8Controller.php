<?php namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller\Api;
use App\Models\Betct8;
use App\libraries\providers\CT8;
use Illuminate\Http\Request;
use Config;
use App\Models\Account;
use App\Models\Product;
use App\Models\Wager;
use App\Models\Currency;
use App\Models\Profitloss;
use App\libraries\App;
use DB;
use Cache;

class Ct8Controller extends MainController{
	
	protected $accounts = array();
	
	protected $profitloss = array();

	public function index(Request $request)
	{
		
		
		$startdate = str_replace(' ', '%20', Betct8::orderBy('transaction_date_time','desc')->pluck('transaction_date_time'));
		
		$startdate = $startdate == '' ? '2015-11-01 00:00:00' : $startdate;
		
		$enddate   = date('Y-m-d').'%2023:59:59';
		
		$postfields = 'action=11'.
					  '&agent='.Config::get($request->currency.'.ct8.agent').
					  '&username='.
					  '&param='.$startdate.'|'.$enddate;

		$result = simplexml_load_string( $this->curl( $postfields , Config::get($request->currency.'.ct8.url') , false ) );
		//$result = simplexml_load_string( '<result><row id="1"><transaction_id>44430232</transaction_id><transaction_date_time>2015-11-13 13:45:20</transaction_date_time><closed_time>2015-11-13 13:45:58</closed_time><member_id>aaronn</member_id><member_type>CASH</member_type><currency>IDR</currency><balance_start>130.0</balance_start><balance_end>132.0</balance_end><IsRevocation>1</IsRevocation><GameType>2</GameType><TableID>22</TableID><ShoeID>0</ShoeID><PlayID>997818</PlayID><BetPoints>6.0</BetPoints><BetPointDetail>0^0^0^0^0^0^0^0^0^0^0^2^0^0^2^0^2^0^0^</BetPointDetail><BetResult>4</BetResult><BetResultDetail>0#0^0#0^0#0^0#0^0#0^0#0^0#0^0#0^0#0^0#0^0#0^0.0#0.0|^0#0^0#0^2.0#2.0|^0#0^2.0#2.0|^0#0^0#0^</BetResultDetail><WinOrLoss>8.0</WinOrLoss><betip>36.84.70.186</betip><paramid></paramid><availablebet>6.0</availablebet></row></result>') ;
                App::insert_api_log( array( 'request' => Config::get($request->currency.'.ct8.url').'?'.$postfields , 'return' => $result , 'method' => 'CT8' ) );
		if( isset($result->code) && $result->code == 0 )
		{

			foreach($result->result->row as $key => $value )
			{


					$data['transaction_id'] 		= (int)$value->transaction_id;
					$data['transaction_date_time']  = (string)$value->transaction_date_time;
					$data['member_id']  			= (string)$value->member_id;
					$data['member_type'] 		    = (string)$value->member_type;
					$data['currency']  				= (string)$value->currency;
					$data['balance_start']  		= (string)$value->balance_start;
					$data['balance_end']  			= (string)$value->balance_end;
					$data['IsRevocation'] 		    = (string)$value->IsRevocation;
					$data['GameType']  				= (string)$value->GameType;
					$data['TableID']  				= (string)$value->TableID;
					$data['ShoeID'] 				= (string)$value->ShoeID;
					$data['PlayID'] 				= (string)$value->PlayID;
					$data['BetPoints']  			= (string)$value->BetPoints;
					$data['BetPointDetail']  		= (string)$value->BetPointDetail;
					$data['BetResult']  			= (string)$value->BetResult;
					$data['BetResultDetail']  		= (string)$value->BetResultDetail;
					$data['WinOrLoss']  			= (string)$value->WinOrLoss;
					$data['betip']  				= (string)$value->betip;
					$data['paramid']  				= (string)$value->paramid;
					$data['availablebet']  			= (string)$value->availablebet;
					$data['created_at']		  		= date("y-m-d H:i:s");
					$data['updated_at']		 		= date("y-m-d H:i:s");

					
					foreach( $data as $key => $value ){
						$insert_data[$key] = '"'. $value . '"';
					}					


					if(DB::statement('call insert_ct8('.implode(',',$insert_data).')')){
						$this->InsertWager($data);
					} 
		
				
	
			}
			Profitloss::updatePNL2($this->profitloss);
		
		}
	}	
	
	private function InsertWager( $bet ){
		
		if( !isset($this->accounts[$bet['member_id']]) )
			$this->accounts[$bet['member_id']] = Account::where( 'nickname' , '=' , $bet['member_id'] )->first();
		
		$prdObj = Cache::rememberForever('product_ct8_obj', function(){
				return Product::where( 'code' , '=' , 'CT8' )->first();
		});
	
		$data['accid'] 		 		= $this->accounts[$bet['member_id']]->id;
		$data['acccode'] 	 		= $this->accounts[$bet['member_id']]->code;
		$data['nickname'] 	 		= $bet['member_id'];
		$data['wbsid'] 		 		= $this->accounts[$bet['member_id']]->wbsid;
		$data['prdid'] 		 		= $prdObj->id;
		$data['matchid'] 	 		= $bet['PlayID'];
		$data['gamename'] 	 		= $bet['GameType'];
		$data['category'] 	 		= Product::getCategory('Casino');
		$data['refid'] 		 		= $bet['transaction_id'];
		$data['crccode'] 	 		= $this->accounts[$bet['member_id']]->crccode;
		$data['crcrate'] 	 		= Currency::getCurrencyRate($data['crccode']);
		$data['datetime'] 	 		= $bet['transaction_date_time'];
		$data['ip'] 		 		= $bet['betip'];
		$data['payout'] 	 		= (float) ( $bet['WinOrLoss'] );
		$data['profitloss']  		= $bet['WinOrLoss'] - $bet['BetPoints'];
		$data['accountdate'] 		= substr( $bet['transaction_date_time'], 0 , 10 );
		$data['stake']		 		= $bet['BetPoints'];
		$data['status'] 			= Wager::STATUS_SETTLED;
		
		if( $data['stake'] == $data['payout'] )	    $data['result']  = Wager::RESULT_DRAW;
		else if( $data['stake'] > $data['payout'] ) $data['result']  = Wager::RESULT_LOSS;
		else if( $data['stake'] < $data['payout'] ) $data['result']  = Wager::RESULT_WIN;
		$data['validstake'] 		= $bet['BetPoints'];
		$data['stakelocal'] 		= Currency::getLocalAmount($data['stake'], $data['crccode']);
		$data['payoutlocal']		= Currency::getLocalAmount($data['payout'], $data['crccode']);
		$data['profitlosslocal']	= Currency::getLocalAmount($data['profitloss'], $data['crccode']);
		$data['validstakelocal']	= Currency::getLocalAmount($data['validstake'], $data['crccode']);
		$data['iscalculated'] 	    = 0;
		$data['created']			= date("y-m-d H:i:s");
		$data['modified']			= date("y-m-d H:i:s");
		
		foreach( $data as $key => $value ){
			$insert_data[$key] = '"'. $value . '"';
		}
		

		if(DB::statement('call insert_wager('.implode(',',$insert_data).')'))
		{
			$this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['prdid']   = $data['prdid'];
			$this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['accobj']  = $this->accounts[$bet['member_id']];
			$this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['crcrate'] = $data['crcrate'];
		}
		

	}
	

}
