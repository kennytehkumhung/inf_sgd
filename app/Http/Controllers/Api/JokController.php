<?php namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller\Api;
use App\Models\Betjok;
use App\Models\Account;
use App\Models\Product;
use App\Models\Wager;
use App\Models\Currency;
use App\Models\Profitloss;
use App\libraries\providers\JOK;
use App\libraries\App;
use Illuminate\Http\Request;
use Config;
use DB;
use Cache;
use Carbon\Carbon;

class JokController extends MainController{
	
	protected $accounts = array();
	
	protected $profitloss = array();

    public function index($currency, Request $request)
	{
	    // Joker datapull is passive, which is called and feed by provider side.
        date_default_timezone_set("Asia/Kuala_Lumpur");

	    if ($request->has('startdate')) {
            // Has start date, manually pull ticket.
            $appId = Config::get($currency . '.jok.appid');

            if (strlen($appId) < 1) {
                return false;
            }

            $lockname = 'jok_datapull_process_'.$request->currency;

            if ($request->input('startdate') == 'ytd') {
                $lockname .= 'ytd';
                $yesterdayDt = Carbon::now()->addDays(-1);
                $startdate = $yesterdayDt->setTime(0, 0, 0)->toDateTimeString();
                $enddate = $yesterdayDt->setTime(23, 59, 59)->toDateTimeString();
            } else {
                // Date format: Y-m-d H:i:s. Best interval 30 minutes or gateway will hit timeout very easily.
                $startdate = $request->input('startdate');
                $enddate = $request->input('enddate', Carbon::now()->toDateTimeString());
            }

            App::check_process($lockname);

            $startDt = Carbon::parse($startdate)->addMinutes(-10);
            $endDt = Carbon::parse($enddate);

            $jok = new JOK();
            $apiKey = Config::get($currency . '.jok.key');
            $apiUrl = Config::get($currency . '.jok.api_url');

            $continue = false;
            $manualNextId = $request->has('nextid');
            $firstRun = true;
            $minTemp = 0;

            // JOK datapull could take very long time to fetch data, pull data day by day
            // to decrease provider's server load time.
            do {
                $continue = false;

                if ($firstRun) {
                    $nextId = $request->input('nextid', '');
                    $firstRun = false;
                } else {
                    $nextId = '';
                }

                if ($startDt->diffInMinutes($endDt) > 30) {
                    $startdateStr = $startDt->addMinutes($minTemp)->toDateTimeString();
                    $enddateStr = $startDt->copy()->addMinutes(30)->toDateTimeString();
                    $minTemp = 30;
                    $continue = true;
                } else {
                    $startdateStr = $startDt->toDateTimeString();
                    $enddateStr = $endDt->toDateTimeString();
                }

                do {
                    $data = array(
                        'Method' => 'TS',
                        'EndDate' => $enddateStr,
                        'NextId' => $nextId,
                        'StartDate' => $startdateStr,
                        'Timestamp' => Carbon::now()->timestamp,
                    );

                    if (strlen($nextId) < 1) {
                        unset($data['NextId']);
                    }

                    $signature = $jok->generateSignature($data, $apiKey);
                    $postfields = json_encode($data);

                    $result = json_decode(App::curlXML($postfields, $apiUrl . '?AppID=' . $appId . '&Signature=' . $signature, true, 90, '', $jok->getXmlHeader()), true);

                    if (isset($result['data']) && isset($result['data']['Game'])) {
                        foreach ($result['data']['Game'] as $key => $value) {
                            $this->insertBet($currency, $value);

                           
                        }
						Profitloss::updatePNL2($this->profitloss);
                    }

                    if (isset($result['nextId'])) {
                        $nextId = trim($result['nextId']);
                    } else {
                        $nextId = '';
                    }
                } while (strlen($nextId) > 0);

                if ($manualNextId && $nextId == '') {
                    break;
                }
            } while ($continue);

            App::unlock_process($lockname);
        } else {
	        // API is call by provider.
            $jsonData = $request->json()->all();

            // Log everything before processing.
            App::insert_api_log( array( 'request' => $request->url() , 'return' => json_encode($jsonData) , 'method' => '' ) );

            foreach ($jsonData as $bi) {
                if ($bi['PostType'] == 'Game') {
                    $this->insertBet($currency, $bi);
                }


            }
			Profitloss::updatePNL2($this->profitloss);
        }
	}

	private function insertBet($currency, $bet) {
        $dateTimeNowStr = Carbon::now()->toDateTimeString();
        $usernameTemp = explode('_', $bet['Username']);

        $data = array(
            'refid'         => date('U').mt_rand(11111,99999),
            'posttype'      => 'Game',
            'ocode'         => (string) $bet['OCode'],
            'username'      => (string) $usernameTemp[1],
            'currency'      => (string) $currency,
            'gamecode'      => (string) $bet['GameCode'],
            'description'   => (string) $bet['Description'],
            'type'          => (string) $bet['Type'],
            'amount'        => (float) $bet['Amount'],
            'result'        => (float) $bet['Result'],
            'time'          => Carbon::parse($bet['Time'])->toDateTimeString(),
            'details'       => json_encode($bet['Details']),
            'appid'         => (string) $bet['AppID'],
            'created_at'    => $dateTimeNowStr,
            'updated_at'    => $dateTimeNowStr,
        );

        $stmtParams = array();
        $stmtValues = array();
        foreach ($data as $key => $value) {
            $stmtParams[] = ':'.$key;
            $stmtValues[$key] = $value;
        }

        if (DB::statement('call insert_jok('.implode(',',$stmtParams).')', $stmtValues)) {
            $betjokObj = Betjok::where('ocode', '=', $data['ocode'])
                ->where('username', '=', $data['username'])
                ->first(['refid']);

            if ($betjokObj) {
                // Get ref id of inserted record.
                $data['refid'] = $betjokObj->refid;
            }

            $this->InsertWager($data,$currency);
        }
    }

	private function InsertWager( $bet , $currency){

		if( !isset($this->accounts[$bet['username']]) )
			$this->accounts[$bet['username']] = Account::where( 'nickname' , '=' , $bet['username'] )->whereCrccode($currency)->first();

        $accountObj = $this->accounts[$bet['username']];

		$prdObj = Cache::rememberForever('product_jok_obj', function(){
				return Product::where( 'code' , '=' , 'JOK' )->first();
		});

		$data['accid'] 		 		= $accountObj->id;
		$data['acccode'] 	 		= $accountObj->code;
		$data['nickname'] 	 		= $accountObj->nickname;
		$data['wbsid'] 		 		= $accountObj->wbsid;
		$data['prdid'] 		 		= $prdObj->id;
		$data['matchid'] 	 		= 0;
		$data['gamename'] 	 		= $bet['description'];
		$data['category'] 	 		= Product::getCategory('egames');
		$data['refid'] 		 		= $bet['refid'];
		$data['crccode'] 	 		= $accountObj->crccode;
		$data['crcrate'] 	 		= Currency::getCurrencyRate($data['crccode']);
		$data['datetime'] 	 		= $bet['time'];
		$data['ip'] 		 		= '';
        $data['payout'] 	 		= (float) $bet['result'];
        $data['profitloss']			= ((float) $bet['result']) - ((float) $bet['amount']);
		$data['accountdate']			= substr( $data['datetime'], 0 , 10 );
		$data['stake']		 		= $bet['amount'];
		$data['status'] 			= Wager::STATUS_SETTLED;


		// result - amount

		if( $data['stake'] == $data['payout'] )	    $data['result']  = Wager::RESULT_DRAW;
		else if( $data['stake'] > $data['payout'] ) $data['result']  = Wager::RESULT_LOSS;
		else if( $data['stake'] < $data['payout'] ) $data['result']  = Wager::RESULT_WIN;
		$data['validstake'] 		= $bet['amount'];
		$data['stakelocal'] 		= Currency::getLocalAmount($data['stake'], $data['crccode']);
		$data['payoutlocal']		= Currency::getLocalAmount($data['payout'], $data['crccode']);
		$data['profitlosslocal']	= Currency::getLocalAmount($data['profitloss'], $data['crccode']);
		$data['validstakelocal']	= Currency::getLocalAmount($data['validstake'], $data['crccode']);
		$data['iscalculated'] 	    = 0;
		$data['created']			= date("y-m-d H:i:s");
		$data['modified']			= date("y-m-d H:i:s");

		foreach( $data as $key => $value ){
			$insert_data[$key] = '"'. $value . '"';
		}


		if(DB::statement('call insert_wager('.implode(',',$insert_data).')'))
		{
			$this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['prdid']   = $data['prdid'];
			$this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['accobj']  = $accountObj;
			$this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['crcrate'] = $data['crcrate'];
		}

	}
	
}
