<?php namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller\Api;
use App\Models\Account;
use App\Models\Configs;
use App\Models\Product;
use App\Models\Onlinetracker;
use App\Models\Minigame;
use App\Models\Currency;
use App\Models\Apilog;
use App\Models\Promocampaign;
use App\Models\Promocash;
use App\Models\Cashledger;
use App\libraries\Array2XML;
use App\libraries\App;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Config;
use DB;
use Log;
use Cache;
use Session;


class MinigameController extends MainController{
	
	public function __construct()
	{
		
	}
        
        protected $price_list = array();
	
        public function RainingGame(Request $request){
            
            App::insert_api_log( array( 'request' => $request , 'return' => $request , 'method' => 'getPrize' ) );
            
            $prizelist = array();
            
            if($request->sid){
                $userid = $request->sid;
                $accObj = Account::where('id','=',$userid)->first();
                $otObj = Onlinetracker::where('userid', '=', $accObj->id)->where('username', '=', $accObj->nickname)->where('type', '=', 1)->first();
                $sign = md5($accObj->nickname.$otObj->token.$accObj->id);
                
            }else{
                $result = array('error_code' => 1, 'error_msg' => 'Invalid Parameter');
                return json_encode($result);
            }
            
            if($request->token == $sign){
                $date = json_decode(Configs::getParam('SYSTEM_MINIGAME_DATETIME'));
                $prize_num = json_decode(Configs::getParam('SYSTEM_MINIGAME_PRIZE'));
                $startdate = Carbon::parse($date->startdate)->format('Y-m-d H:i:s');
                $enddate = Carbon::parse($date->enddate)->format('Y-m-d H:i:s');
                
                if(Minigame::whereRaw('userid='.$accObj->id.' AND start_period >="'.$startdate.'" AND end_period <= "'.$enddate.'"')->count() == 0){
                    for($x = 0; $x <= 79; $x++){
                        //random return price list back
                        $prize = array();
                        $prize[$prize_num->eighty] = 80;
                        $prize[$prize_num->ten] = 10;
                        $prize[$prize_num->five] = 5;
                        $prize[$prize_num->three] = 3;
                        $prize[$prize_num->two] = 2;

                        $prize_value = $this->getRand($prize);
                        $prizelist[$x] = (float)$prize_value;
                    }

                    $mgObj = new Minigame;
                    $mgObj->type = 1;
                    $mgObj->opcode = 'IW';
                    $mgObj->userid = $accObj->id;
                    $mgObj->nickname = $accObj->nickname;
                    $mgObj->crccode = $accObj->crccode;
                    $mgObj->start_period = $startdate;
                    $mgObj->end_period = $enddate;
                    $mgObj->total_prize_list = json_encode($prizelist);
                    $mgObj->after_prize_list = '';
                    $mgObj->status = 0;
                    $mgObj->save();
                    
                    $result = array('error_code' => 0, 'id' => $mgObj->id,'sid' => $accObj->id, 'token' => $request->token, 'opcode' => 'IW', 'prize_list' => $prizelist);
                    return json_encode($result);
                }else{
                    $mngObj = Minigame::whereRaw('userid='.$accObj->id.' AND start_period >="'.$startdate.'" AND end_period <= "'.$enddate.'"')->first();
                    if($mngObj->status == 0){
                        $result = array('error_code' => 0, 'id' => $mngObj->id,'sid' => $accObj->id, 'token' => $request->token, 'opcode' => 'IW', 'prize_list' => json_decode($mngObj->total_prize_list));
                        return json_encode($result);
                    }else{
                        $result = array('error_code' => 3);
                        return json_encode($result);
                    }
                }

            }else{
                $result = array('error_code' => 2, 'error_msg' => 'Invalid token');
                return json_encode($result);
            }
        }
        
        
        public function RainingGameResult(Request $request){
            
            App::insert_api_log( array( 'request' => $request , 'return' => $request , 'method' => 'setPrize' ) );
            
            $after_prize = array();
            if($request->sid == ''){
                return 'sid is empty';
            }elseif($request->id == ''){
                return 'id is empty';
            }
            
            if(Minigame::where('userid','=',$request->sid)->where('id','=',$request->id)->where('status','=',0)->orderBy('created_at','desc')->count() == 0 ){
                return 'Invalid parameter';
            }else{
                $mgObj = Minigame::where('userid','=',$request->sid)->where('id','=',$request->id)->where('status','=',0)->orderBy('created_at','desc')->first();
            }
            
            $prizelist = json_decode($mgObj->total_prize_list);

            if($request->tc > 0){
                $total = 0;
                for($x = 0; $x <= $request->tc - 1; $x++){
                   $after_prize[$x] = $prizelist[$x];
                   $total = $total + $prizelist[$x];
                }
            }
            
            if($after_prize){
                $mgObj->total_click = $request->tc;
                $mgObj->after_prize_list = json_encode($after_prize);
                $mgObj->status = 1; 
                if($mgObj->save()){
                    if( $pcpObj = Promocampaign::whereRaw(' code = "minigame" ')->first() ){
                        $accObj = Account::whereRaw('nickname = "'.$mgObj->nickname.'"')->first();
                        $promoCashObj = new Promocash;
                        $promoCashObj->type			= Promocash::TYPE_MANUAL;
                        $promoCashObj->accid	 	= $accObj->id;
                        $promoCashObj->acccode	 	= $accObj->code;
                        $promoCashObj->crccode	 	= $accObj->crccode;
                        $promoCashObj->crcrate	 	= Currency::getCurrencyRate($accObj->crccode);
                        $promoCashObj->amount	 	= $total;
                        $promoCashObj->amountlocal 	= Currency::getLocalAmount($total, $accObj->crccode);
                        $promoCashObj->pcpid		= $pcpObj->id;
                        $promoCashObj->status		= Promocash::STATUS_PENDING;

                        if($promoCashObj->save()) {
                                $cashLedgerObj = new Cashledger;
                                $cashLedgerObj->accid 			= $promoCashObj->accid;
                                $cashLedgerObj->acccode 		= $promoCashObj->acccode;
                                $cashLedgerObj->accname 		= $accObj->fullname;
                                $cashLedgerObj->chtcode 		= CBO_CHARTCODE_BONUS;
                                $cashLedgerObj->amount 			= $promoCashObj->amount;
                                $cashLedgerObj->amountlocal 	= $promoCashObj->amountlocal;
                                $cashLedgerObj->fee 			= $promoCashObj->amount;
                                $cashLedgerObj->feelocal 		= $promoCashObj->amountlocal;
                                $cashLedgerObj->crccode 		= $promoCashObj->crccode;
                                $cashLedgerObj->crcrate 		= $promoCashObj->crcrate;
                                $cashLedgerObj->refobj			= 'Promocash';
                                $cashLedgerObj->refid			= $promoCashObj->id;
                                $cashLedgerObj->status			= CBO_LEDGERSTATUS_PENDING;
                                $cashLedgerObj->save();
                        }
                    }
                }
                
                $result = array('error_code' => 0, 'result' => 'Success');
                return json_encode($result);
            }
            $result = array('error_code' => 1, 'result' => 'Fail');
            return json_encode($result);
        }
        
        public static function getRand($proArr) {
            $result = '';

            //概率数组的总概率精度
            $proSum = array_sum($proArr);

            //概率数组循环
            foreach ($proArr as $key => $proCur) {
                $randNum = mt_rand(1, $proSum);
                if ($randNum <= $proCur) {
                    $result = $key;
                    break;
                } else {
                    $proSum -= $proCur;
                }
            }
            unset ($proArr);

            return $result;
        }
}
