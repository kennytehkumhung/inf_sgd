<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller\Api;
use App\Models\Account;
use App\Models\Affiliate;
use App\Models\Affiliatereport;
use App\Models\Affiliatesetting;
use App\Models\Cashledger;
use App\Models\Configs;
use App\Models\Incentivereferral;
use App\Models\Ledgersetting;
use App\Models\Product;
use App\Models\Wager;
use App\Models\Wagersetting;
use App\Models\Profitloss;
use App\Models\Currency;
use App\libraries\Array2XML;
use App\libraries\App;
use Illuminate\Http\Request;
use App\libraries\Login;
use Config;
use DB;
use Cache;
use Log;
use Auth;
use Carbon\Carbon;

class AffiliateReportController extends MainController {

	public function index(Request $request) {
		App::check_process('affiliate_report_calculate_process');
		
		 $now = Carbon::now();
		$aflObjr = new Affiliate();
		/*
		$arpObj = Affiliatereport::orderBy('date', 'desc')->first();
		
		if ($arpObj) {
			$now = Carbon::parse($arpObj->date);
		}
		
		$now = $now->toDateTimeString(); */
		
		$continue = true;
		
		while($continue){
			
		
			$wgrObj = Wager::where('status', '=', Wager::STATUS_SETTLED)
					->where('category', '!=', Product::CATEGORY_LOTTERY)
					->where('iscalculated', '=', 0)
					->where('status', '!=', Wager::STATUS_REJECTED)
					->whereRaw(' acccode IN (select code from account where aflid != 0) ')
					->orderBy('accountdate', 'asc')
					->take(1200)
					->get();
					
			
			
			if (count($wgrObj)) {
				foreach ($wgrObj as $wager) {
					$accObj = Account::find($wager->accid);

					if ($accObj && $accObj->aflid > 0) {
						$agents = $aflObjr->getAllUplineById($accObj->aflid);

						if ($agents) {
							foreach ($agents as $level => $aflid) {
								// Only Affiliate with type 1 (agent) entitled.
		//						$aflObj = Affiliate::find($aflid);
								$aflObj = Affiliate::where('id', '=', $aflid)
										->where('type', '=', 1)
										->first();
								
								if ($aflObj) {
									$rate = 0;
									$afsObj = Affiliatesetting::where('aflid', '=', $aflid)
											->where('created', '<=', $wager->datetime)
											->orderBy('id', 'desc')
											->first();
											
									$mutiply = 1;
									
									if( $wager->crccode == 'IDR' || $wager->crccode == 'VND' )
									{
										$mutiply = 1000;
									}

									if (count($afsObj)) {
										$rate = $afsObj->commission / 100;
										$attrs = [
											'prdid' => $wager->prdid,
											'category' => $wager->category,
											'wgrid' => $wager->id,
											'aflid' => $accObj->aflid,
											'share1id' => $aflid,
											'share2id' => $aflObj->parentid,
											'afsid' => $afsObj->id,
											'accid' => $wager->accid,
											'crccode' => $wager->crccode,
											'crcrate' => $wager->crcrate,
											'stake' => $wager->stake * $mutiply,
											'validstake' => $wager->validstake * $mutiply,
											'profitloss' => $wager->profitloss * $mutiply,
											'rate' => $afsObj->commission,
											'share1' => ($wager->profitloss * $rate) * $mutiply,
											'share2' => ($wager->profitloss * (1 - $rate) * -1) * $mutiply,
											'date' => $wager->accountdate,
										];

										$wgsObj = Wagersetting::where('wgrid', '=', $wager->id)
												->where('share1id', '=', $aflid)
												->first();

										if ($wgsObj) {
											$wgsObj->guard([])->update($attrs);
										} else {
											$wgsObj = new Wagersetting();
											$wgsObj->guard([])->fill($attrs)->save();
										}
									}
								}
							}
						}

						
					}
					$wager->iscalculated = 1;
					$wager->save();
				}
			}
			
			/* $wagers = Wager::where('status', '=', Wager::STATUS_REJECTED)
					->orderBy('id', 'asc')
					->take(300)
					->get();
			
			foreach ($wagers as $wager) {
				Wagersetting::where('wgrid', '=', $wager->id)->delete();
				
				$wager->update([
					'iscalculated' => 1,
				]);
			} */
			
			$ledgers = Cashledger::where('iscalculated', '=', 0)
					->whereIn('status', [CBO_LEDGERSTATUS_CONFIRMED, CBO_LEDGERSTATUS_MANCONFIRMED])
					->whereIn('chtcode', [CBO_CHARTCODE_BONUS, CBO_CHARTCODE_INCENTIVE])
					->whereRaw(' acccode IN (select code from account where aflid != 0) ')
					->orderBy('id', 'asc')
					->take(100)
					->get();
			
			foreach ($ledgers as $ledger) {
				$accObj = Account::find($ledger->accid);
				
				if ($accObj) {
					$agents = $aflObjr->getAllUplineById($accObj->aflid);
					
					if ($agents) {
						foreach ($agents as $level => $aflid) {
							// Only Affiliate with type 1 (agent) entitled.
	//						$aflObj = Affiliate::find($aflid);
							$aflObj = Affiliate::where('id', '=', $aflid)
									->where('type', '=', 1)
									->first();

							if ($aflObj) {
								$rate = 0;
								$afsObj = Affiliatesetting::where('aflid', '=', $aflid)
										->where('created', '<=', $ledger->created)
										->orderBy('id', 'desc')
										->first();

								if (count($afsObj)) {
									$rate = $afsObj->commission / 100;
									$attrs = [
										'clgid' => $ledger->id,
										'aflid' => $accObj->aflid,
										'share1id' => $aflid,
										'share2id' => $aflObj->parentid,
										'afsid' => $afsObj->id,
										'accid' => $ledger->accid,
										'crccode' => $ledger->crccode,
										'crcrate' => $ledger->crcrate,
										'amount' => $ledger->amount,
										'rate' => $afsObj->commission,
										'share1' => $ledger->amount * $rate * -1,
										'share2' => $ledger->amount * (1 - $rate) * -1,
										'date' => Carbon::parse($ledger->created)->toDateString(),
									];

									$lgsObj = Ledgersetting::where('clgid', '=', $ledger->id)
											->where('share1id', '=', $aflid)
											->first();

									if ($lgsObj) {
										$lgsObj->guard([])->update($attrs);
									} else {
										$lgsObj = new Ledgersetting();
										$lgsObj->guard([])->fill($attrs)->save();
									}
								}
							}
						}
					}
					
					$ledger->iscalculated = 1;
					$ledger->save();
				}
			}
			
			if( Wager::where('category', '!=', Product::CATEGORY_LOTTERY)
					->where('iscalculated', '=', 0)
					->where('status', '!=', Wager::STATUS_REJECTED)
					->whereRaw(' acccode IN (select code from account where aflid != 0) ')
					->count() == 0
			){
				$continue = false;
			}
		
		}
		
		App::unlock_process('affiliate_report_calculate_process');
		
		
	}
	
	public function referral(Request $request) {

        $startdate = $request->get('startdate', Carbon::yesterday()->toDateString());
	    $enddate = $request->get('enddate', Carbon::yesterday()->toDateString());

		$lists = array();

		//if($pnls = DB::select( DB::raw("select sum(validstake) as sumvalid,accid,category from profitloss where accid IN (select id from account where referral != '' ) AND date = '".$yesterday->toDateString()."' group by accid")))
		if($pnls = DB::select( DB::raw("select sum(validstake) as sumvalid,accid,nickname,category from profitloss where accid IN (select id from account where referralid > 0 ) and `date` between '".$startdate."' and '".$enddate."' group by crccode, `date`, accid")))
		{
			foreach($pnls as $profitloss)
			{

				$uplineAccObj = Account::find($profitloss->accid);

				if(!isset($lists[$uplineAccObj->referral]))
				{
					$lists[$uplineAccObj->referral]  = (float)$profitloss->sumvalid;
				}
				else
				{
					$lists[$uplineAccObj->referral] += (float)$profitloss->sumvalid;
				}
				//var_dump($profitloss->sumvalid);

			}

			$currencyRateCache = array();

			// Insert into incentivereferral table.
            $incentiveReferralObj = DB::table('profitloss as pnl')
                ->join('account as acc', 'acc.id', '=', 'pnl.accid')
                ->join('account as rfacc', 'rfacc.id', '=', 'acc.referralid')
                ->where('acc.referralid', '>', 0)
                ->whereBetween('pnl.date', array($startdate, $enddate))
                ->groupBy('pnl.crccode', 'pnl.date', 'pnl.accid', 'pnl.prdid')
                ->selectRaw('sum(pnl.validstake) as sumvalid, pnl.crccode, pnl.prdid, pnl.category, acc.id as accid, acc.nickname as nickname, pnl.date, rfacc.id as rfaccid, rfacc.crccode as rfcrccode, rfacc.nickname as rfnickname')
                ->get();

            foreach ($incentiveReferralObj as $rfIncentive) {
                if (!isset($currencyRateCache[$rfIncentive->crccode])) {
                    // Cache currency rate to improve process speed.
                    $currencyRateCache[$rfIncentive->crccode] = Currency::getCurrencyRate($rfIncentive->crccode);
                }
                if (!isset($currencyRateCache[$rfIncentive->rfcrccode])) {
                    // Cache currency rate to improve process speed.
                    $currencyRateCache[$rfIncentive->rfcrccode] = Currency::getCurrencyRate($rfIncentive->rfcrccode);
                }

                Incentivereferral::forceCreate(array(
                    'accid' => $rfIncentive->accid,
                    'nickname' => $rfIncentive->nickname,
                    'crccode' => $rfIncentive->crccode,
                    'crcrate' => $currencyRateCache[$rfIncentive->crccode],
                    'referralid' => $rfIncentive->rfaccid,
                    'referralnickname' => $rfIncentive->rfnickname,
                    'referralcrccode' => $rfIncentive->rfcrccode,
                    'referralcrcrate' => $currencyRateCache[$rfIncentive->rfcrccode],
                    'prdid' => $rfIncentive->prdid,
                    'category' => $rfIncentive->category,
                    'accountdate' => $rfIncentive->date,
                    'amount' => $rfIncentive->sumvalid,
                    'amount_local' => $rfIncentive->sumvalid,
                    'created_at' => DB::raw('NOW()'),
                    'updated_at' => DB::raw('NOW()'),
                ));
            }

			// Insert into cashledger table.
			foreach ($lists as $nickname => $amount)
			{

				$accObj = Account::whereNickname($nickname)->first();

//				var_dump($accObj->id);

                if (!isset($currencyRateCache[$accObj->crccode])) {
                    // Cache currency rate to improve process speed.
                    $currencyRateCache[$accObj->crccode] = Currency::getCurrencyRate($accObj->crccode);
                }

				$amount = round( $amount * 0.001 ,  2 );

				$cashLedgerObj = new Cashledger;
				$cashLedgerObj->accid 		= $accObj->id;
				$cashLedgerObj->acccode 	= $accObj->code;
				$cashLedgerObj->accname 	= $accObj->fullname;
				$cashLedgerObj->chtcode 	= CBO_CHARTCODE_REFERRALREBATE;
				$cashLedgerObj->remarkcode 	= '';
				$cashLedgerObj->amount 		= $amount;
				$cashLedgerObj->amountlocal = $amount;
				$cashLedgerObj->fee 		= $amount;
				$cashLedgerObj->feelocal 	= $amount;
				$cashLedgerObj->crccode 	= $accObj->crccode;
				$cashLedgerObj->crcrate 	= $currencyRateCache[$accObj->crccode];
				$cashLedgerObj->refobj		= '';
				$cashLedgerObj->refid		= '';
				$cashLedgerObj->cashbalance	= $cashLedgerObj->getBalance($accObj->id);
				$cashLedgerObj->status		= CBO_LEDGERSTATUS_PENDING;
				$cashLedgerObj->createdip	= App::getRemoteIp();
				$cashLedgerObj->save();
			}
			
			
		}

	}

}
