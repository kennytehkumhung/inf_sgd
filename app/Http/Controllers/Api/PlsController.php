<?php namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller\Api;
use App\Models\Betpls;
use App\Models\Account;
use App\Models\Product;
use App\Models\Wager;
use App\Models\Currency;
use App\Models\Profitloss;
use App\Models\Onlinetracker;
use App\libraries\providers\PLS;
use App\libraries\App;
use Illuminate\Http\Request;
use Config;
use DB;
use Cache;
use Log;

class PlsController extends MainController{
	
	protected $accounts = array();
	
	protected $profitloss = array();

	public function index(Request $request)
	{
		
		App::check_process('pls_datapull_process');
		
		$postfields = '&start_dtm='.str_replace( ' ' , 'T' , $this->getLatestTransactionTime($request->currency)).'&end_dtm='.date('Y-m-d').'T23:59:59'.'&type=0';
		
		
		$result = json_decode(App::curlGET( Config::get($request->currency.'.pls.url').'feed/gamehistory/?host_id='.Config::get($request->currency.'.pls.host_id').$postfields , false, 5));
	

		foreach( $result as $date => $nameList ){
			
			foreach( $nameList as $name => $lists ){
				
				foreach( $lists as $value ){
				
					$data['user_id'] 		= $name;
					$data['currency'] 		= $request->currency;
					$data['sn'] 			= $value->sn;
					$data['gid'] 			= $value->gid;	
					$data['sid'] 		    = $value->sid;
					$data['tm'] 			= $date.' '.$value->tm;
					$data['bet'] 			= $value->bet / 100;
					$data['win'] 			= $value->win / 100;
					$data['bn'] 			= $value->bn / 100;
					$data['gb'] 			= $value->gb / 100;
					$data['jp'] 			= $value->jp / 100;
					$data['updated_at']		= date("y-m-d H:i:s");
					$data['created_at']	    = date("y-m-d H:i:s");

					
					foreach( $data as $key => $value ){
							$insert_data[$key] = '"'. $value . '"';
					}					


					if(DB::statement('call insert_pls('.implode(',',$insert_data).')')){
						$this->InsertWager($data,$request->currency);
					} 
				
				}
				
			}
			Profitloss::updatePNL2($this->profitloss);
			
			
		}
		
		App::unlock_process('pls_datapull_process');
		
	}	
	
	private function getLatestTransactionTime($currency)
	{
		$date = Betpls::whereCurrency($currency)->orderBy('tm','Desc')->pluck('tm');
		
		if(!$date)
			
		$date = '2016-11-01 00:00:00';
		
		return $date;

	}

	private function InsertWager( $bet , $currency){
		
		if( !isset($this->accounts[$bet['user_id']]) )
			$this->accounts[$bet['user_id']] = Account::where( 'nickname' , '=' , $bet['user_id'] )->whereCrccode($currency)->first();
		
		$prdObj = Cache::rememberForever('product_pls_obj', function(){
				return Product::where( 'code' , '=' , 'PLS' )->first();
		});
	
		$data['accid'] 		 		= $this->accounts[$bet['user_id']]->id;
		$data['acccode'] 	 		= $this->accounts[$bet['user_id']]->code;
		$data['nickname'] 	 		= $bet['user_id'];
		$data['wbsid'] 		 		= $this->accounts[$bet['user_id']]->wbsid;
		$data['prdid'] 		 		= $prdObj->id;
		$data['matchid'] 	 		= 0;
		$data['gamename'] 	 		= $bet['gid'];
		$data['category'] 	 		= Product::getCategory('egames');
		$data['refid'] 		 		= $bet['sn'];
		$data['crccode'] 	 		= $this->accounts[$bet['user_id']]->crccode;
		$data['crcrate'] 	 		= Currency::getCurrencyRate($data['crccode']);
		$data['datetime'] 	 		= $bet['tm'];
		$data['ip'] 		 		= '';
		$data['payout'] 	 		= (float) $bet['win'];
		$data['profitloss']  		= $bet['win'] -  $bet['bet'];
		$data['accountdate'] 		= substr( $bet['tm'], 0 , 10 );
		$data['stake']		 		= $bet['bet'];
		$data['status'] 			= Wager::STATUS_SETTLED;
		

		if( $data['stake'] == $data['payout'] )	    $data['result']  = Wager::RESULT_DRAW;
		else if( $data['stake'] > $data['payout'] ) $data['result']  = Wager::RESULT_LOSS;
		else if( $data['stake'] < $data['payout'] ) $data['result']  = Wager::RESULT_WIN;
		$data['validstake'] 		= $bet['bet'];
		$data['stakelocal'] 		= Currency::getLocalAmount($data['stake'], $data['crccode']);
		$data['payoutlocal']		= Currency::getLocalAmount($data['payout'], $data['crccode']);
		$data['profitlosslocal']	= Currency::getLocalAmount($data['profitloss'], $data['crccode']);
		$data['validstakelocal']	= Currency::getLocalAmount($data['validstake'], $data['crccode']);
		$data['iscalculated'] 	    = 0;
		$data['created']			= date("y-m-d H:i:s");
		$data['modified']			= date("y-m-d H:i:s");
		
		foreach( $data as $key => $value ){
			$insert_data[$key] = '"'. $value . '"';
		}
		

		if(DB::statement('call insert_wager('.implode(',',$insert_data).')'))
		{
			$this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['prdid']   = $data['prdid'];
			$this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['accobj']  = $this->accounts[$bet['user_id']];
			$this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['crcrate'] = $data['crcrate'];
		}
		

	}
	
	public function doValidateTicket(Request $request){
		
		$result['status_code'] = '1';

		if( $trackerObj = Onlinetracker::whereRaw('token="'.$request->access_token.'"')->first() ) 
		{
			$accObj = Account::find($trackerObj->userid);
		
			$result['status_code'] = 0;
			$result['member_id']   = $accObj->nickname;
			$result['balance']     = 0;
			
		}
		//Log::info(json_encode($result));

		echo json_encode($result);
	}
	


}
