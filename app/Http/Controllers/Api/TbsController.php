<?php namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller\Api;
use App\Models\Betagg;
use App\Models\Account;
use App\Models\Accountproduct;
use App\Models\Product;
use App\Models\Wager;
use App\Models\Currency;
use App\Models\Profitloss;
use App\Models\Configs;
use App\libraries\providers\Tbs;
use Illuminate\Http\Request;
use App\libraries\App;
use Config;
use DB;
use Cache;
use Carbon\Carbon;

class TbsController extends MainController{
	
	protected $accounts = array();
	
	protected $profitloss = array();
	
	public function index(Request $request)
	{
		App::check_process('tbs_datapull_process');
		
		$pdatetime = Configs::getParam('SYSTEM_TBS_PULLDATE');
		
		if (is_null($pdatetime)) {
			$pdatetime = '2016-08-01 00:00:00';
		}
		
		$startdatetime = Carbon::createFromFormat('Y-m-d H:i:s', $pdatetime);
		$endtdatetime  = Carbon::now();
		
		// Backdate early 5 minutes in case some record is late insert from API service.
		$startdatetime->addMinutes(-5);

		// API only allow max time range of 30 mins for each pull.
		// If time range too long will hit '000007' error code.
		if( $endtdatetime->diffInMinutes($startdatetime) > 29 ){
			// End datetime is over 30 mins, use start time +30 mins instead.
			$endtdatetime = $startdatetime->copy()->addMinutes(30);
		}
		
		// Convert dateTime object to formatted string.
		$startdatetime = $startdatetime->toDateTimeString();
		$endtdatetime = $endtdatetime->toDateTimeString();
		
/* 		$postfields = 'APIPassword='.Config::get($request->currency.'.tbs.password').
					  '&MemberAccount='.
					  '&Status=0'.
					  '&BetID=-1'.
					  '&FromDate='.$startdatetime.
					  '&ToDate='.$endtdatetime; */		
					  
		$startdatetime = $request->has('date') ? $request->get('date') : substr($startdatetime , 0 , 10);
					  
		$postfields = 'APIPassword='.Config::get($request->currency.'.tbs.password').
					  '&MemberAccount='.
					  '&Status=0'.
					  '&ReportDate='.$startdatetime;
		
		//$result = simplexml_load_string( App::curl( $postfields , Config::get($request->currency.'.tbs.url').'GetBetSheet' ) );
		$result = simplexml_load_string( App::curl( $postfields , Config::get($request->currency.'.tbs.url').'GetBetSheetByReport' ) );
		
		var_dump($result);

/* 		$result = simplexml_load_string('<?xml version="1.0" encoding="utf-8"?>
						<response>
						  <errcode>000000</errcode>
						  <errtext />
						  <result>
						    <lastdate>2016-08-09 19:21:56</lastdate>
						    <betlist>
						      <bet>
							<BetID>SP135661693</BetID>
							<GameID>1</GameID>
							<SubGameID>0</SubGameID>
							<Account>viva1006399</Account>
							<BetAmount>2990.0000</BetAmount>
							<BetOdds>1.0000</BetOdds>
							<AllWin>2990.0000</AllWin>
							<DeductAmount>2990.0000</DeductAmount>
							<BackAmount>0.0000</BackAmount>
							<Win>-2990.0000</Win>
							<Turnover>2990.0000</Turnover>
							<OddsStyle>ID</OddsStyle>
							<BetDate>2016-08-09 17:15:08</BetDate>
							<Status>2</Status>
							<Result>2</Result>
							<ReportDate>2016-08-09 00:00:00</ReportDate>
							<BetIP>139.194.213.139</BetIP>
							<UpdateTime>2016-08-09 19:21:56</UpdateTime>
							<BetInfo>[{"SportID":1,"LeagueID":5451,"MatchID":1261478,"HomeID":106269,"AwayID":104725,"Stage":2,"MarketID":7,"Odds":1.00,"Hdp":0.00,"HomeScore":0,"AwayScore":0,"HomeCard":0,"AwayCard":0,"BetPos":1,"OutLevel":0.00,"Result":2,"Status":2}]</BetInfo>
							<BetResult>{"MatchIDList":[1261478],"InsuranceList":[]}</BetResult>
						      </bet>
						      <bet>
							<BetID>SP135667834</BetID>
							<GameID>1</GameID>
							<SubGameID>0</SubGameID>
							<Account>badegg@369</Account>
							<BetAmount>100.0000</BetAmount>
							<BetOdds>3.5000</BetOdds>
							<AllWin>250.0000</AllWin>
							<DeductAmount>100.0000</DeductAmount>
							<BackAmount>0.0000</BackAmount>
							<Win>-100.0000</Win>
							<Turnover>100.0000</Turnover>
							<OddsStyle>EU</OddsStyle>
							<BetDate>2016-08-09 17:45:55</BetDate>
							<Status>2</Status>
							<Result>2</Result>
							<ReportDate>2016-08-09 00:00:00</ReportDate>
							<BetIP>60.54.122.94</BetIP>
							<UpdateTime>2016-08-09 19:21:56</UpdateTime>
							<BetInfo>[{"SportID":1,"LeagueID":5451,"MatchID":1261478,"HomeID":106269,"AwayID":104725,"Stage":3,"MarketID":5,"Odds":3.50,"Hdp":0.00,"HomeScore":0,"AwayScore":1,"HomeCard":0,"AwayCard":0,"BetPos":2,"OutLevel":0.00,"Result":2,"Status":2}]</BetInfo>
							<BetResult>{"MatchIDList":[1261478],"InsuranceList":[]}</BetResult>
						      </bet>
						    </betlist>
						  </result>
						</response>
					'); */
					
		if( isset($result->errcode) && $result->errcode == 0 && isset($result->result->betlist->bet) && count($result->result->betlist->bet) > 0 ){
			
		 	foreach( $result->result->betlist->bet as $key => $value ){
				$accountTemp = explode('@', $value->Account);
				if( isset($accountTemp[0]) && Account::where( 'nickname' , '=' , $accountTemp[0] )->count() != 0 )
				{
						// Some columns are affected by new API.
						
					
						$data['BetID'] 	   		= (string)substr($value->BetID, 2);
						$data['Account']   		= $accountTemp[0];
						$data['BetAmount'] 		= (string)$value->BetAmount;
						$data['BetOdds']	   	= (string)$value->BetOdds;
						$data['Win']	   		= (string)$value->Win;
						$data['OddsStyle']  		= (string)$value->OddsStyle;
						$data['BetDate']   		= (string)$value->BetDate;
						$data['Status']    		= (string)$value->Status;
						$data['Result']    		= (string)$value->Result;
						$data['ReportDate']     = (string)$value->ReportDate;
						$data['SportName']      = (string)$value->SportName;
						$data['BetIP']     		= (string)$value->BetIP;
						$data['GameID']			= (int)$value->GameID;
						$data['SubGameID']		= (int)$value->SubGameID;
						$data['BackAmount']		= $value->BackAmount;
						$data['DeductAmount']		= $value->DeductAmount;
						$data['AllWin']			= $value->AllWin;
						$data['Turnover']		= $value->Turnover;
						$data['BetUpdateTime']		= $value->UpdateTime;
						$data['BetInfo']		= null;
						
						// Extract JSON data from BetInfo.
						$betInfo = json_decode($value->BetInfo);
						$betInfo = collect($betInfo[0]);
						
						$data['BetSubID']		= (int)$betInfo->get('SubID');
						$data['SportID']		= (int)$betInfo->get('SportID');
						$data['LeagueID']		= (int)$betInfo->get('LeagueID');
						$data['MatchID']		= (int)$betInfo->get('MatchID');
						$data['HomeID']			= (int)$betInfo->get('HomeID');
						$data['AwayID']			= (int)$betInfo->get('AwayID');
						$data['Stage']			= (int)$betInfo->get('Stage');
						$data['MarketID']		= (int)$betInfo->get('MarketID');
						$data['Odds']			= $betInfo->get('Odds');
						$data['Hdp']			= $betInfo->get('Hdp');
						$data['HomeScore']		= (int)$betInfo->get('HomeScore');
						$data['AwayScore']		= (int)$betInfo->get('AwayScore');
						$data['HomeCard']		= (int)$betInfo->get('HomeCard');
						$data['AwayCard']		= (int)$betInfo->get('AwayCard');
						$data['BetPos']			= (int)$betInfo->get('BetPos');
						$data['OutLevel']		= $betInfo->get('OutLevel');
						//$data['BetInfoResult']		= (int)$betInfo->get('Result');
						//$data['BetInfoStatus']		= (int)$betInfo->get('Status');
						
						// Finishing extract data.
						$data['updated_at']		= date("y-m-d H:i:s");
						$data['created_at']		= date("y-m-d H:i:s");
						
						foreach( $data as $key => $value ){
							$insert_data[$key] = '"'. $value . '"';
						}	
						
						// Ignore records that member is not registered in this project.
						//if (Accountproduct::is_exist($accountTemp[0], 'TBS')) {
							if(DB::statement('call insert_tbs('.implode(',',$insert_data).')')){
								$this->InsertWager($data);
							}
						//}				
				}
				
			}
			
			Profitloss::updatePNL2($this->profitloss);
			
		}
		
		Configs::updateParam( 'SYSTEM_TBS_PULLDATE' , $endtdatetime );

		App::unlock_process('tbs_datapull_process');
	}	
	
	private function InsertWager( $tbs ){
		
		$temp = explode("@", $tbs['Account']);
		$tbs['Account'] = $temp[0];
		
		if( !isset($this->accounts[$tbs['Account']]) )
			$this->accounts[$tbs['Account']] = Account::where( 'nickname' , '=' , $tbs['Account'] )->first();
		
		$prdObj = Cache::rememberForever('product_tbs_obj', function(){
				return Product::where( 'code' , '=' , 'TBS' )->first();
		});

		$data['accid'] 		 		= $this->accounts[$tbs['Account']]->id;
		$data['acccode'] 	 		= $this->accounts[$tbs['Account']]->code;
		$data['nickname'] 	 		= $tbs['Account'];
		$data['wbsid'] 		 		= $this->accounts[$tbs['Account']]->wbsid;
		$data['prdid'] 		 		= $prdObj->id;
		$data['matchid'] 	 		= $tbs['MatchID'];
		$data['gamename'] 	 		= 'Sports';
		$data['category'] 	 		= Product::getCategory('sports');
		$data['refid'] 		 		= $tbs['BetID'];
		$data['crccode'] 	 		= $this->accounts[$tbs['Account']]->crccode;
		$data['crcrate'] 	 		= Currency::getCurrencyRate($data['crccode']);
		$data['datetime'] 	 		= $tbs['BetUpdateTime']; // $tbs['BetDate'];
		$data['ip'] 		 		= '';
	
			
		$data['payout'] 	 		= (float) ( $tbs['Win'] );
		$data['profitloss']  		= $tbs['Win'] - $tbs['BetAmount'];
		$data['accountdate'] 		= substr( $tbs['ReportDate'], 0 , 10 );
		$data['stake']		 		= $tbs['BetAmount'];

		// New API has changed status from string to numeric.
		if($tbs['Status'] == 'Running' || $tbs['Status'] == 1) {
			$data['status'] = Wager::STATUS_OPEN;
		}
		else if($tbs['Status'] == 'Refund') $data['status'] = Wager::STATUS_REFUND;
		else if($tbs['Status'] == 'Rejected' || $tbs['Status'] == 4) $data['status'] = Wager::STATUS_REJECTED;
		else $data['status'] = Wager::STATUS_SETTLED;
		$data['result'] 	 		= Wager::RESULT_PENDING;
		if($tbs['Status'] == 'DRAW')	   $data['result']  = Wager::RESULT_DRAW;
		else if($tbs['Status'] == 'Win')  $data['result']  = Wager::RESULT_WIN;
		else if($tbs['Status'] == 'Lose') $data['result'] = Wager::RESULT_LOSS;
		$data['validstake'] 		= $tbs['BetAmount'];
		
		$data['stakelocal'] 		= Currency::getLocalAmount($data['stake'], 		$data['crccode']);
		$data['payoutlocal']		= Currency::getLocalAmount($data['payout'],		$data['crccode']);
		$data['profitlosslocal']	= Currency::getLocalAmount($data['profitloss'], $data['crccode']);
		$data['validstakelocal']	= Currency::getLocalAmount($data['validstake'], $data['crccode']);
		$data['iscalculated']	    = 0;
		$data['created']			= date("y-m-d H:i:s");
		$data['modified']			= date("y-m-d H:i:s");

		foreach( $data as $key => $value ){
			$insert_data[$key] = '"'. $value . '"';
		}
		

		if(DB::statement('call insert_wager('.implode(',',$insert_data).')'))
		{
			$this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['prdid']   = $data['prdid'];
			$this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['accobj']  = $this->accounts[$tbs['Account']];
			$this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['crcrate'] = $data['crcrate'];
		}
	}

}
