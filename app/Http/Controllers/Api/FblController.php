<?php namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller\Api;
use App\Models\Betfbl;
use App\Models\Account;
use App\Models\Product;
use App\Models\Wager;
use App\Models\Currency;
use App\Models\Profitloss;
use App\Models\Onlinetracker;
use App\libraries\providers\FBL;
use App\libraries\Array2XML;
use App\libraries\App;
use Illuminate\Http\Request;
use App\libraries\Login;
use Config;
use DB;
use Cache;
use Log;
use Auth;
use Carbon\Carbon;

class FblController extends MainController{
	
	protected $accounts = array();
	
	protected $profitloss = array();

	public function index(Request $request)
	{
		App::check_process('fbl_datapull_process');
		
		if (Product::where('status', '=', 2)->where('code', '=', 'FBL')->count() == 1)
		{
			App::unlock_process('fbl_datapull_process');
			exit;
		}

		date_default_timezone_set("Asia/Kuala_Lumpur");

		$lastLogId = $request->input('lastLogId');

		if (strlen($lastLogId) < 1) {
            $result = Betfbl::orderBy('logId', 'desc')->first();

            if ($result) {
                $lastLogId = $result->logId;
            }
        }
		
		$header[] = 'Cache-Control: no-cache';
		$header[] = 'Content-Type: application/json';

        $postfields = array(
            'SecurityKey' => '',
            'AppUser' => Config::get($request->currency.'.fbl.opt_username'),
            'LastLogID' => strval($lastLogId),
        );
        
        $postfields['SecurityKey'] = $this->generateSecurityKey($request->currency, $postfields);
        
		$result = simplexml_load_string( App::curlXML(json_encode($postfields) , Config::get($request->currency.'.fbl.url').'GetGameLogs', true, 30 , '', $header) );
        
		if( isset($result->GameLogs->GameLog) ){
			$data = array();
            $accounts = array();
			
			foreach ($result->GameLogs->GameLog as $row) {
				$dateTimeNow = Carbon::now();
				$dateTimeNowStr = $dateTimeNow->toDateTimeString();
                $accKey = '_'.$row->UserID;
				
                if (!isset($accounts[$accKey])) {
                    // Fetch users and store in array as cache so that
                    // no need read from DB everytime.
                    $accountObj = Account::where('fblid', '=', $row->UserID)->first();
                    
                    if ($accountObj) {
                        $accounts[$accKey] = array(
                            'accId' => $accountObj->id,
                            'username' => strtoupper($accountObj->nickname),
                        );
                    } else {
                        // FBL User not found.
                        $accounts[$accKey] = array();
                    }
                }
                
                if ($accounts[$accKey] != array()) {
                    // Process only user that is found.
                    $data = array(
                        'logId'			=> $row->LogID,
                        'userId'		=> $row->UserID,
                        'accId'         => $accounts[$accKey]['accId'],
                        'username'		=> $accounts[$accKey]['username'],
                        'datePlaced'	=> Carbon::parse($row->DatePlaced)->toDateTimeString(),
                        'gameId'        => $row->GameID,
                        'num'           => $row->Num,
                        'roundId'		=> $row->RoundID,
                        'type'			=> $row->Type,
                        'amount'		=> $row->Amount,
                        'wl'			=> $row->WL,
                        'wlAmount'      => $row->WLAmount,
                        'currency'      => $request->currency,
                        'created_at'	=> $dateTimeNowStr,
                        'updated_at'	=> $dateTimeNowStr,
                    );
                    
                    foreach ($data as $key => $value) {
                        $insertData[$key] = '"'.$value.'"';
                    }

                    if (DB::statement('call insert_fbl('.implode(',',$insertData).')')) {
                        $this->InsertWager($data, $request->currency);
                    }
                }
			}
		}

		Profitloss::updatePNL2($this->profitloss);

		App::unlock_process('fbl_datapull_process');

	}	

	private function InsertWager( $bet, $currency ){
		
		if( !isset($this->accounts[$bet['accId']]) )
			$this->accounts[$bet['accId']] = Account::where( 'nickname' , '=' , $bet['username'] )->whereCrccode($currency)->first();
		
		$prdObj = Cache::rememberForever('product_fbl_obj', function(){
				return Product::where( 'code' , '=' , 'FBL' )->first();
		});
	
		$data['accid'] 		 		= $this->accounts[$bet['accId']]->id;
		$data['acccode'] 	 		= $this->accounts[$data['accid']]->code;
		$data['nickname'] 	 		= $this->accounts[$data['accid']]->nickname;
		$data['wbsid'] 		 		= $this->accounts[$data['accid']]->wbsid;
		$data['prdid'] 		 		= $prdObj->id;
		$data['matchid'] 	 		= $bet['logId'] == '' ? "''":$bet['logId'];
		$data['gamename'] 	 		= $bet['gameId'];
		$data['category'] 	 		= Product::getCategory('number');
		$data['refid'] 		 		= $bet['logId'];
		$data['crccode'] 	 		= $this->accounts[$data['accid']]->crccode;
		$data['crcrate'] 	 		= Currency::getCurrencyRate($data['crccode']);
		$data['datetime'] 	 		= $bet['datePlaced'];
		$data['ip'] 		 		= '';
		$data['payout'] 	 		= (float) $bet['wlAmount'];
        $data['profitloss']			= ((float) $bet['wlAmount']) - ((float) $bet['amount']);
		$data['accountdate']			= substr( $data['datetime'], 0 , 10 );
		$data['stake']		 		= $bet['amount'];
		$data['status'] 			= Wager::STATUS_SETTLED;
		

		if( $data['stake'] == $data['payout'] )	    $data['result']  = Wager::RESULT_DRAW;
		else if( $data['stake'] > $data['payout'] ) $data['result']  = Wager::RESULT_LOSS;
		else if( $data['stake'] < $data['payout'] ) $data['result']  = Wager::RESULT_WIN;
		$data['validstake'] 		= $bet['amount'];
		$data['stakelocal'] 		= Currency::getLocalAmount($data['stake'], $data['crccode']);
		$data['payoutlocal']		= Currency::getLocalAmount($data['payout'], $data['crccode']);
		$data['profitlosslocal']	= Currency::getLocalAmount($data['profitloss'], $data['crccode']);
		$data['validstakelocal']	= Currency::getLocalAmount($data['validstake'], $data['crccode']);
		$data['iscalculated'] 	    = 0;
		$data['created']			= date("y-m-d H:i:s");
		$data['modified']			= date("y-m-d H:i:s");
		
		foreach( $data as $key => $value ){
			$insert_data[$key] = '"'. $value . '"';
		}
		

		if(DB::statement('call insert_wager('.implode(',',$insert_data).')'))
		{
			$this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['prdid']   = $data['prdid'];
			$this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['accobj']  = $this->accounts[$data['accid']];
			$this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['crcrate'] = $data['crcrate'];
		}
		
	}
	
	public function generateSecurityKey($currency, $params = array())
	{
		$result = '';
		
		foreach ($params as $key => $val) {
            if ($key != 'SecurityKey') {
                if (is_array($val)) {
                    $result .= implode(',', $val);
                } else {
                    $result .= $val;
                }
            }
		}
        
		if (strlen($result) > 0) {
			$result = strtoupper(substr(md5(Config::get($currency.'.fbl.opt_id').$result), 0, 12));
		}
		
		return $result;
	}
	
}
