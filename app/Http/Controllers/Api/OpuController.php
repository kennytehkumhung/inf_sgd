<?php namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller\Api;
use Crypt;
use App\Models\Betopu;
use App\Models\Account;
use App\Models\Product;
use App\Models\Currency;
use App\Models\Profitloss;
use App\Models\Wager;
use App\Models\Configs;
use App\Models\Onlinetracker;
use App\libraries\App;
use App\libraries\providers\OPU;
use Illuminate\Http\Request;
use Config;
use DB;
use Cache;
use Carbon\Carbon;
use Log;


class OpuController extends MainController{
	
	protected $accounts = array();

	protected $profitloss = array();

    public function index(Request $request)
	{
		
		App::check_process('opu_datapull_process_'.$request->currency);
		
		if (Product::where('status', '=', 2)->where('code', '=', 'OPU')->count() == 1)
		{
			App::unlock_process('opu_datapull_process_'.$request->currency);
			exit;
		}

		if ($request->has('startdate')) {
		    $from = $request->get('startdate');
        } else {
            $from = $this->getLatestTransactionTime($request->currency);
        }
		
		$postfields = 'vendor_id='.Config::get($request->currency.'.opu.vendor_id').
					  '&operator_id='.Config::get($request->currency.'.opu.operator_id').
					  '&page=1'.
					  '&from='.$from.
					  '&to='.date('Y-m-d').' 23:59:59';

//		$result = json_decode( $this->curl( $postfields , Config::get($request->currency.'.opu.url').'getBetDetail' , false ) );
		$result = json_decode( App::curl($postfields, Config::get($request->currency.'.opu.url').'getBetDetail', true, 10) );

		if( isset($result->total) && $result->total != 0)
		{
		
			foreach( $result->trans as $key => $value )
			{
				
				  $data = array(
                    'transaction_id' 		=> $value->trans_id,
                    'transaction_date_time' => $value->transaction_time,
                    'member_code' 			=> $value->user_id,
                    'member_type' 			=> '',
                    'currency' 				=> $value->currency,
                    'balance_start' 		=> $value->balance_start,
                    'balance_end' 			=> $value->balance_end,
                    'bet' 					=> $value->stake,
                    'win' 					=> $value->winlost_amount,
                    'game_code'				=> $value->game_code,
                    'game_detail'			=> '',
                    'bet_status' 			=> $value->status,
                    'player_hand' 			=> $value->player_hand,
                    'game_result' 			=> $value->result,
                    'game_category' 		=> '',
                    'vendor' 				=> $value->vendor,
                    'DrawNumber' 			=> $value->DrawNumber,
                    'm88_studio' 			=> $value->m88_studio,
                    'stamp_date' 			=> $value->stamp_date,
                    'bet_record_id' 		=> $value->bet_record_id,
                    'created_at' 			=> date("y-m-d H:i:s"),
                    'updated_at' 			=> date("y-m-d H:i:s"),
                );

                foreach ($data as $key => $value) {
                    $insertData[$key] = '"'.$value.'"';
                }

                if (DB::statement('call insert_opu('.implode(',',$insertData).')')) {
                     $this->InsertWager($data, $request->currency);
                }

			}
			
			Profitloss::updatePNL2($this->profitloss);
	
		 	if( isset($result->total) && $result->total > 1 )
			{
				
				for( $i = 2 ; $i <= $result->total ; $i++ )
				{
					
					$postfields = 'vendor_id='.Config::get($request->currency.'.opu.vendor_id').
					              '&operator_id='.Config::get($request->currency.'.opu.operator_id').
					              '&page='.$i.
					              '&from='.$from.
					              '&to='.date('Y-m-d H:i:s');
								  
					$result = json_decode( $this->curl( $postfields , Config::get($request->currency.'.opu.url').'getBetDetail' , false ) );
					
					foreach( $result->trans as $key => $value )
					{
						
						if( Betopu::where( 'transaction_id' , '=' , $value->trans_id )->count() == 0 )
						{
						
									 $data = array(
							'transaction_id' 		=> $value->trans_id,
							'transaction_date_time' => $value->transaction_time,
							'member_code' 			=> $value->user_id,
							'member_type' 			=> '',
							'currency' 				=> $value->currency,
							'balance_start' 		=> $value->balance_start,
							'balance_end' 			=> $value->balance_end,
							'bet' 					=> $value->stake,
							'win' 					=> $value->winlost_amount,
							'game_code'				=> $value->game_code,
							'game_detail'			=> '',
							'bet_status' 			=> $value->status,
							'player_hand' 			=> $value->player_hand,
							'game_result' 			=> $value->result,
							'game_category' 		=> '',
							'vendor' 				=> $value->vendor,
							'DrawNumber' 			=> $value->DrawNumber,
							'm88_studio' 			=> $value->m88_studio,
							'stamp_date' 			=> $value->stamp_date,
							'bet_record_id' 		=> $value->bet_record_id,
							'created_at' 			=> date("y-m-d H:i:s"),
							'updated_at' 			=> date("y-m-d H:i:s"),
						);

						foreach ($data as $key => $value) {
							$insertData[$key] = '"'.$value.'"';
						}

						if (DB::statement('call insert_opu('.implode(',',$insertData).')')) {
							 $this->InsertWager($data, $request->currency);
						}
						}		
					}
					
					Profitloss::updatePNL2($this->profitloss);
				}
			}  

			
		}
		
		App::unlock_process('opu_datapull_process_'.$request->currency);
		
    }
	
	private function getLatestTransactionTime($currency)
	{
		$date = Betopu::where( 'currency' , '=' , $currency )->orderBy('transaction_date_time','Desc')->pluck('transaction_date_time');
		
		if(!$date){
			if( $currency == 'MYR' )
				$date = '2016-01-15 00:04:13';
			elseif( $currency == 'VND' )
				$date = '2016-01-15 00:36:59';	
			elseif( $currency == 'IDR' )
				$date = '2015-11-01 01:08:55';	
			elseif( $currency == 'THB' )
				$date = '2016-07-01 00:00:00';
		}
		
		return $date;

	}
	
	private function InsertWager( $opu , $currency){

		if( !isset($this->accounts[$opu['member_code']]) )
			$this->accounts[$opu['member_code']] = Account::where( 'nickname' , '=' , $opu['member_code'] )->whereCrccode($currency)->first();
		
		$prdObj = Cache::rememberForever('product_opu_obj', function(){
				return Product::where( 'code' , '=' , 'OPU' )->first();
		});
	
        $account = $this->accounts[$opu['member_code']];
        
		$data['accid'] 		 		= $account->id;
		$data['acccode'] 	 		= $account->code;
		$data['nickname'] 	 		= $account->nickname;
		$data['wbsid'] 		 		= $account->wbsid;
		$data['prdid'] 		 		= $prdObj->id;
		$data['matchid'] 	 		= 0;
		$data['gamename'] 	 		= $opu['game_detail'];
		$data['category'] 	 		= Product::getCategory('Casino');
		$data['refid'] 		 		= $opu['transaction_id'];
		$data['crccode'] 	 		= $account->crccode;
		$data['crcrate'] 	 		= Currency::getCurrencyRate($data['crccode']);
		$data['datetime'] 	 		= $opu['transaction_date_time'];
		$data['ip'] 		 		= '';
		$data['payout'] 	 		= $opu['bet'] + $opu['win'];
		$data['profitloss']  		= $opu['win'];
		
		$date = Carbon::parse($opu['stamp_date']);
		
		$data['accountdate'] 		= substr( $date->subHours(12)->toDateTimeString(), 0 , 10 );
		$data['stake']		 		= $opu['bet'];
		$data['status'] 			= Wager::STATUS_SETTLED;
		
		if( $data['stake'] == $data['payout'] )	    $data['result']  = Wager::RESULT_DRAW;
		else if( $data['stake'] > $data['payout'] ) $data['result']  = Wager::RESULT_LOSS;
		else if( $data['stake'] < $data['payout'] ) $data['result']  = Wager::RESULT_WIN;
		$data['validstake'] 		= $opu['bet'];
		 if ($data['result'] == Wager::RESULT_DRAW) {
            $data['validstake'] = 0;
        }
		$data['stakelocal'] 		= Currency::getLocalAmount($data['stake'], $data['crccode']);
		$data['payoutlocal']		= Currency::getLocalAmount($data['payout'], $data['crccode']);
		$data['profitlosslocal']	= Currency::getLocalAmount($data['profitloss'], $data['crccode']);
		$data['validstakelocal']	= Currency::getLocalAmount($data['validstake'], $data['crccode']);
		$data['iscalculated'] 	    = 0;
		$data['created']			= date("y-m-d H:i:s");
		$data['modified']			= date("y-m-d H:i:s");
		
		foreach( $data as $key => $value ){
			$insert_data[$key] = '"'. $value . '"';
		}
		
		if(DB::statement('call insert_wager('.implode(',',$insert_data).')'))
		{
			$this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['prdid']   = $data['prdid'];
			$this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['accobj']  = $this->accounts[$opu['member_code']];
			$this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['crcrate'] = $data['crcrate'];
		}
		

	}
	
	public function doAuthenticateToken(Request $request){
		
		$array = array();
	
		if( $request->has('session_token') )
		{
			$token    = explode( '_' , $request->session_token );
			$username = Onlinetracker::where( 'token' , '=' , $token[1])->pluck('username');
			
		}

		if( isset($username) ){
			
			$array['username'] = $username;
			$array['crccode']  = 'MYR';
			$array['lang']     = 'en';
			$array['status']   = 'OK';
			
		}
		

		echo json_encode($array);
	}

}

