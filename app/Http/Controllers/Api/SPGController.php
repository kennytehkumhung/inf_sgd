<?php namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller\Api;
use App\Models\Betspg;
use App\Models\Account;
use App\Models\Product;
use App\Models\Wager;
use App\Models\Currency;
use App\Models\Profitloss;
use App\Models\Onlinetracker;
use App\libraries\providers\SPG;
use App\libraries\Array2XML;
use App\libraries\App;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Config;
use DB;
use Log;
use Cache;

class SPGController extends MainController{
	
	protected $accounts = array();
	
	protected $profitloss = array();

	public function index(Request $request)
	{

	    $lockname = 'spg_datapull_process_'.$request->currency;
        $date = $request->input('date', $this->getLatestTransactionTime($request->currency));

        if ($request->input('date') == 'ytd') {
            $lockname .= 'ytd';
            $date = Carbon::now()->addDays(-1)->format('Y-m-d 00:00:00');
        }

		App::check_process($lockname);
                
                $page = 1;
                $continue = true;

        date_default_timezone_set("Asia/Kuala_Lumpur");
		
		while($continue){
                    $random = App::generateNum(6);
                    $serialNo = $date.$random;
					
					if( $request->has('date' ) && $request->input('date') == 'ytd')
					{
						$start   = date('Ymd',strtotime($date)).'T000000';
						$end     = date('Ymd',strtotime($date)).'T235959';
					}
					else
					{
						$time    = date('His',strtotime($date));
						$start   = date('Ymd',strtotime($date)).'T'.$time;
						$end     = date('Ymd',date(strtotime("+1 day", strtotime($date)))).'T'.$time;
					}
					

                    $header[0] = 'Accept: text/xml,application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5'; 
                    $header[] = 'Cache-Control: max-age=0'; 
                    $header[] = 'Connection: keep-alive'; 
                    $header[] = 'Keep-Alive: 300'; 
                    $header[] = 'Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7'; 
                    $header[] = 'Accept-Language: en-us,en;q=0.5'; 
                    $header[] = 'Pragma: '; 
                    $header[] = 'API: getBetHistory';
                    $header[] = 'DataType: XML';
                    $header[] = 'Content-Type: text/xml';

                    $return  = '<serialNo>'.$serialNo.'</serialNo>';
                    $return .= '<merchantCode>'.Config::get($request->currency.'.spg.merchant_id').'</merchantCode>';
                    $return .= '<beginDate>'.$start.'</beginDate>';
                    $return .= '<endDate>'.$end.'</endDate>';
					

                    $return .= '<pageIndex>'.$page.'</pageIndex>';

                    $postfields = '<?xml version="1.0" encoding="UTF-8"?>
                                       <GetBetHistoryRequest>
                                            '.$return.'
                                       </GetBetHistoryRequest>';

                    $result = simplexml_load_string( App::curlXML( $postfields , Config::get($request->currency.'.spg.api_url'), true, 10 , 'spg', $header) );
		
                    //App::insert_api_log( array( 'request' => Config::get($request->currency.'.spg.api_url').'?'.$postfields , 'return' => $result , 'method' => 'BetHistory' ) );

                    if( isset($result->code) && $result->code == '0'){
                        $prefix = Config::get($request->currency.'.spg.prefix');

                        foreach($result->list->BetInfo as $key => $value ){
                            $data['ticketId']               = (int)$value->ticketId;
                            $data['user_id']                 = (string) str_replace($prefix,'',$value->acctId);
                            $data['categoryId'] 		= (string) $value->categoryId;
                            $data['gameCode']               = (string) $value->gameCode;
                            $data['ticketTime'] 		= date('Y-m-d H:i:s',strtotime(str_replace('T','',$value->ticketTime)));
                            $data['betIp']                  = (string) $value->betIp;
                            $data['currency'] 		= (string) $value->currency;
                            $data['betAmount'] 		= $value->betAmount;
                            $data['winLoss'] 		= $value->winLoss;
                            $data['jackpotAmount'] 		= $value->jackpotAmount;
                            $data['result']                 = (string) $value->result;
                            $data['completed'] 		= (string) $value->completed;
                            $data['luckyDrawId'] 		= (string) $value->luckyDrawId;
                            $data['roundId'] 		= (string) $value->roundId;
                            $data['sequence'] 		= (string) $value->sequence;
                            $data['channel'] 		= (string) $value->channel;
                            $data['balance'] 		= (string) $value->balance;
                            $data['created']		= date("y-m-d H:i:s");
                            $data['modified']		= date("y-m-d H:i:s");

                            foreach( $data as $key => $value ){
                                $insert_data[$key] = '"'. $value . '"';
                            }

                            if(DB::statement('call insert_spg('.implode(',',$insert_data).')')){
                                $this->InsertWager($data, $request->currency);
                            }
                        }
                        Profitloss::updatePNL2($this->profitloss);
                    }
                    
                    if(isset($result->pageCount)){
                        if($result->pageCount == $page){
                            $continue = false;
                        }
                        else{
                            $page++;
                        }
                    }
                    else{
                        $continue = false;
                    }
                    
                }
		App::unlock_process($lockname);
		
	}
        
    private function getLatestTransactionTime( $currency, $manual = false )
	{
		$date = Betspg::where( 'currency' , '=' , $currency )->orderBy('ticketTime','desc')->pluck('ticketTime');
	
		if(!$date)	
                    $date = '2017-06-20 00:00:00';
                
                if($manual)
                    $date = $manual.' 00:00:00';
		
		return $date;

	}

	private function InsertWager( $bet , $currency){
		
		if( !isset($this->accounts[$bet['user_id']]) )
			$this->accounts[$bet['user_id']] = Account::where( 'nickname' , '=' , $bet['user_id'] )->whereCrccode($currency)->first();
		
		$prdObj = Cache::rememberForever('product_spg_obj', function(){
				return Product::where( 'code' , '=' , 'SPG' )->first();
		});
	
		$data['accid'] 		 		= $this->accounts[$bet['user_id']]->id;
		$data['acccode'] 	 		= $this->accounts[$bet['user_id']]->code;
		$data['nickname'] 	 		= $bet['user_id'];
		$data['wbsid'] 		 		= $this->accounts[$bet['user_id']]->wbsid;
		$data['prdid'] 		 		= $prdObj->id;
		$data['matchid'] 	 		= 0;
		$data['gamename'] 	 		= $bet['gameCode'];
		$data['category'] 	 		= Product::getCategory('egames');
		$data['refid'] 		 		= $bet['ticketId'];
		$data['crccode'] 	 		= $this->accounts[$bet['user_id']]->crccode;
		$data['crcrate'] 	 		= Currency::getCurrencyRate($data['crccode']);
		$data['datetime'] 	 		= $bet['ticketTime'];
		$data['ip'] 		 		= $bet['betIp'];
                $data['payout'] 	 		= $bet['betAmount'] - $bet['winLoss'];
                $data['profitloss']                     = $bet['winLoss'];

		$data['accountdate']                    = substr( $bet['ticketTime'], 0 , 10 );
		$data['stake']		 		= $bet['betAmount'];
		$data['status'] 			= Wager::STATUS_SETTLED;
		

		if( $data['stake'] == $data['payout'] )	    $data['result']  = Wager::RESULT_DRAW;
		else if( $data['stake'] > $data['payout'] ) $data['result']  = Wager::RESULT_LOSS;
		else if( $data['stake'] < $data['payout'] ) $data['result']  = Wager::RESULT_WIN;
		$data['validstake'] 		= $bet['betAmount'];
		$data['stakelocal'] 		= Currency::getLocalAmount($data['stake'], $data['crccode']);
		$data['payoutlocal']		= Currency::getLocalAmount($data['payout'], $data['crccode']);
		$data['profitlosslocal']	= Currency::getLocalAmount($data['profitloss'], $data['crccode']);
		$data['validstakelocal']	= Currency::getLocalAmount($data['validstake'], $data['crccode']);
		$data['iscalculated'] 	    = 0;
		$data['created']			= date("y-m-d H:i:s");
		$data['modified']			= date("y-m-d H:i:s");
		
		foreach( $data as $key => $value ){
			$insert_data[$key] = '"'. $value . '"';
		}
		

		if(DB::statement('call insert_wager('.implode(',',$insert_data).')'))
		{
			$this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['prdid']   = $data['prdid'];
			$this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['accobj']  = $this->accounts[$bet['user_id']];
			$this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['crcrate'] = $data['crcrate'];
		}
		

	}
	
	public function doValidateTicket(Request $request){
	    // Whatever change here should copy to doValidateTicket2().
                $string = file_get_contents('php://input');
                
		$merchantCode = '';
		$errorCode = '201';
		$errorMsg = 'Error';
		$messageId = '';
		$userId = '';
		$crccode = '';
                $accInfo = '';
                $serialNo = '';
		
		if($string) {
                    $xml                 = simplexml_load_string($string);
                    $serialNo 		 = (string)$xml->serialNo;
                    $token   		 = (string)$xml->token;
                    $merchantCode        = (string)$xml->merchantCode;
                    $acctId 		 = (string)$xml->acctId;
                    $language 		 = (string)$xml->language;
                    
                    if( $trackerObj = Onlinetracker::whereRaw('token="'.$token.'"')->first() ) {
                        $accObj = Account::find($trackerObj->userid);

                        $accInfo  = '<acctId>INF_'.$accObj->nickname.'</acctId>';
                        $accInfo .= '<balance>0</balance>';
                        $accInfo .= '<userName>INF_'.$accObj->nickname.'</userName>';
                        $accInfo .= '<currency>'.$accObj->crccode.'</currency>';	

                        $errorCode = '0';
                        $errorMsg = 'Success';
                    }
		}
		if($accObj->nickname == 'test118'){
			$merchantCode = 'RWIDR';
		}
		

                $return  = '<merchantCode>'.$merchantCode.'</merchantCode>';
                $return .= '<msg>'.$errorMsg.'</msg>';
                $return .= '<code>'.$errorCode.'</code>';
                $return .= '<serialNo>'.$serialNo.'</serialNo>';
                
		$return = '<?xml version="1.0" encoding="UTF-8"?>
				   <AuthorizeResponse>
                                        <acctInfo>
                                            '.$accInfo.'
                                        </acctInfo>
					'.$return.'
				   </AuthorizeResponse>';
                App::insert_api_log( array( 'request' => $string , 'return' => $return , 'method' => '' ) );
                
		return response()->view('front/xml', array('name' =>  $return))->header('Content-Type', 'application/xml');	
	}

    public function doValidateTicket2($currency, Request $request){
	    // Everything same as doValidateTicket(), just added currency support.
        $currency = strtoupper($currency);
        $string = file_get_contents('php://input');

        $prefix = Config::get($currency.'.spg.prefix');

        $merchantCode = '';
        $errorCode = '201';
        $errorMsg = 'Error';
        $messageId = '';
        $userId = '';
        $crccode = '';
        $accInfo = '';
        $serialNo = '';

        if($string) {
            $xml             = simplexml_load_string($string);
            $serialNo 		 = (string)$xml->serialNo;
            $token   		 = (string)$xml->token;
            $merchantCode    = (string)$xml->merchantCode;
            $acctId 		 = (string)$xml->acctId;
            $language 		 = (string)$xml->language;

            if( $trackerObj = Onlinetracker::whereRaw('token="'.$token.'"')->first() ) {
                $accObj = Account::find($trackerObj->userid);

                $accInfo  = '<acctId>'.$prefix.$accObj->nickname.'</acctId>';
                $accInfo .= '<balance>0</balance>';
                $accInfo .= '<userName>'.$prefix.$accObj->nickname.'</userName>';
                $accInfo .= '<currency>'.$accObj->crccode.'</currency>';

                $errorCode = '0';
                $errorMsg = 'Success';
            }
        }

//        if($accObj->nickname == 'test118'){
//            $merchantCode = 'RWIDR';
//        }

        $return  = '<merchantCode>'.$merchantCode.'</merchantCode>';
        $return .= '<msg>'.$errorMsg.'</msg>';
        $return .= '<code>'.$errorCode.'</code>';
        $return .= '<serialNo>'.$serialNo.'</serialNo>';

        $return = '<?xml version="1.0" encoding="UTF-8"?>
				   <AuthorizeResponse>
                                        <acctInfo>
                                            '.$accInfo.'
                                        </acctInfo>
					'.$return.'
				   </AuthorizeResponse>';
        App::insert_api_log( array( 'request' => $string , 'return' => $return , 'method' => '' ) );

        return response()->view('front/xml', array('name' =>  $return))->header('Content-Type', 'application/xml');
    }
	
	

}
