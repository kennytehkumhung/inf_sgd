<?php namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller\Api;
use Illuminate\Http\Request;
use App\Models\Betibc;
use App\Models\Betibcleague;
use App\Models\Betibcteam;
use App\Models\Betibcparlay;
use App\Models\Account;
use App\Models\Product;
use App\Models\Currency;
use App\Models\Profitloss;
use App\Models\Wager;
use App\Models\Configs;
use App\libraries\App;
use App\libraries\providers\IBC;
use Config;
use DB;
use Cache;
use Log;
use App\Models\Onlinetracker;
use Carbon\Carbon;

class IbcController extends MainController{
	
	protected $accounts = array();
	
	protected $profitloss = array();
	
	public function index(Request $request)
	{

		App::check_process('ibc_datapull_process_'.$request->currency);
		
		if (Product::where('status', '=', 2)->where('code', '=', 'IBCX')->count() == 1)
		{
			exit;
		}
		
		
		$league_ids 	= array();
		$team_ids 		= array();
		$team_id_count 	= 0;
		$key 			= $request->input('key');
		$continue 		= true;
		
		while($continue)
		{
			$continue = false; // Immediate close continue flag to avoid infinity loop.
			$version_key    = Configs::getParam('SYSTEM_IBC_VERSIONKEY_'.$request->currency);
			
			/* if($key != ''){
				$postfields = 'operatorid='.Config::get($request->currency.'.ibc.operator_id').'&vendor_id='.Config::get($request->currency.'.ibc.vendor_id').'&version_key='.$key;
			}else{ */
				$postfields = 'operatorid='.Config::get($request->currency.'.ibc.operator_id').'&vendor_id='.Config::get($request->currency.'.ibc.vendor_id').'&version_key='.$version_key;
				$key = $version_key;
			/* } */

			
			$result = $this->curl( $postfields , Config::get($request->currency.'.ibc.url').'GetBetDetail' , false );

			$colunms = array('trans_id','vendor_member_id','operator_id','league_id','match_id','home_id','away_id','match_datetime','sport_type','table_no','hand_no','shoe_no','bet_type','parlay_ref_no','odds','stake','transaction_time','ticket_status','winlost_amount','after_amount','currency','winlost_datetime','odds_type','bet_team','home_hdp','away_hdp','hdp','betfrom','islive','last_ball_no','home_score','away_score','customInfo1','customInfo2','customInfo3','customInfo4','customInfo5','ba_status','version_key','parlayData','racenumber','racelane','comm','WinPlaceData');		
			
			$colunms_parlay = array('match_id','home_id','away_id','match_datetime','odds','bet_type','bet_team','sport_type','home_hdp','away_hdp','hdp','islive','home_score','away_score','winlost','winlost_datetime');	
			
			$products = array(
				'BetDetails' 			 => 1,
				'BetHorseDetails' 		 => 2,
				'BetLiveCasinoDetails'   => 3,
				'BetNumberDetails' 		 => 4,
				'BetVirtualSportDetails' => 5,
				'BetCasinoDetails'		 => 6,
			);
		
		

			if($result){
				$bets = json_decode($result);
		
				if( isset($bets->Data->last_version_key) && $bets->Data->last_version_key != $key){

					if($bets->Data->last_version_key != 0){
						Configs::updateParam( 'SYSTEM_IBC_VERSIONKEY_'.$request->currency , $bets->Data->last_version_key );
						$continue = true;
					}
						
					foreach($bets->Data as $type => $value){
						if($type != 'last_version_key'){
							$types[] = $type;
						}
					}
					
					if(isset($types)){
						foreach($types as $type){
							if(isset($bets->Data->$type)){
							foreach($bets->Data->$type as $key => $value){
						
								if(Betibc::where('trans_id','=',$value->trans_id)->count() == 0){
									
									//process bet detail data
									try{
									
										$betdetail = new Betibc();
										foreach($colunms as $colunm){
											if(isset($value->$colunm) && !is_array($value->$colunm)){
												$betdetail->$colunm = $value->$colunm;
											}
										}	
										$betdetail->product_id = $products[$type];
										$betdetail->save();
										
									
										$this->InsertWager($betdetail,$request->currency);
										
									}catch(\Exception $e){
									
									}
									
									//process parlay data
									if(isset($value->ParlayData) && is_array($value->ParlayData)){
											foreach($value->ParlayData as $key => $value){
												$parlayObj = new Betibcparlay();
												foreach($colunms_parlay as $colunm_parlay){
													if(isset($value->$colunm_parlay))
														$parlayObj->$colunm_parlay = $value->$colunm_parlay;
														
													$parlayObj->ref_id = $betdetail->id;
												}
												$parlayObj->save(); 
											}
						
											
									}		
									
					
									if(isset($betdetail->league_id) && !in_array($betdetail->league_id, $league_ids))
										$league_ids[] = $betdetail->league_id;
									if(isset($betdetail->home_id) && !in_array($betdetail->home_id, $team_ids)){
										$team_ids[$team_id_count]['team_id']    = $betdetail->home_id;	
										$team_ids[$team_id_count++]['bet_type'] = $betdetail->bet_type;	
									}
									if(isset($betdetail->away_id) && !in_array($betdetail->away_id, $team_ids)){
										$team_ids[$team_id_count]['team_id']    = $betdetail->away_id;
										$team_ids[$team_id_count++]['bet_type'] = $betdetail->bet_type;	
									}
										
									
									
								}else{
									// process update
									
									Betibc::Where('trans_id','=',$value->trans_id)->update(
									array(
										'ticket_status' 	=> $value->ticket_status, 
										'winlost_amount'	=> $value->winlost_amount,
										'after_amount'  	=> (isset($value->after_amount)?$value->after_amount:''),
										'winlost_datetime'  => $value->winlost_datetime,
										'islive'  		    => (isset($value->islive)?$value->islive:''),
										'home_score'  		=> (isset($value->home_score)?$value->home_score:''),
										'away_score'  		=> (isset($value->away_score)?$value->away_score:''),
										'ba_status'  		=> $value->ba_status,
										'version_key'  		=> $value->version_key,
										'customInfo1'  		=> $value->customInfo1,
										'customInfo2'  		=> $value->customInfo2,
										'customInfo3'  		=> $value->customInfo3,
										'customInfo4'  		=> $value->customInfo4,
										'customInfo5'  		=> $value->customInfo5,
										'odds'  			=> (isset($value->odds)?$value->odds:''),
									));
									
									$betdetail = Betibc::Where('trans_id','=',$value->trans_id)->first();
									
									$this->InsertWager($betdetail,$request->currency);
								
/* 									if(isset($value->ParlayData) && is_array($value->ParlayData)){
									
										$bet_detail = DB::table('bet_ibc')->select('id')->where('trans_id', $value->trans_id)->get();
										
											foreach($value->ParlayData as $key => $parlay_value){
												try{
													Betibcparlay::Where('ref_id','=',$bet_detail[0]->id)->update(
													array(
														'winlost'			=> (isset($parlay_value->winlost)?$parlay_value->winlost:''),
														'winlost_datetime'	=> (isset($parlay_value->winlost_datetime)?$parlay_value->winlost_datetime:''),
														'islive'  			=> (isset($parlay_value->islive)?$parlay_value->islive:''),
														'home_score'  		=> (isset($parlay_value->home_score)?$parlay_value->home_score:''),
														'away_score'  		=> (isset($parlay_value->away_score)?$parlay_value->away_score:''),
													));
												}catch(\Exception $e){
							
											
												}
									
											}
				
									} */
									
								}

							}
							}
						}
						Profitloss::updatePNL2($this->profitloss);
					}

				/* 	foreach($league_ids as $key => $league_id){
					
						if( Betibcleague::Where('league_id', '=', $league_id)->count() == 0){
						
							$ch = curl_init();
							$postfields = 'vendor_id='.Config::get($request->currency.'.ibc.vendor_id').'&league_id='.$league_id;
							curl_setopt($ch, CURLOPT_URL, Config::get($request->currency.'.ibc.url').'GetLeagueName');
							curl_setopt($ch, CURLOPT_HEADER, false);
							curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
							curl_setopt($ch, CURLOPT_POST, true);
							curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);
							curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
							$data = curl_exec($ch);
							curl_close($ch);
							$league = json_decode($data);
							if(isset($league->Data)){
								foreach($league->Data as $type => $value){
									if($type != 'version_key'){
										foreach($value as $key => $obj){
											if ($obj->lang == 'en'){
												
												$leagueObj = new Betibcleague;
												$leagueObj->league_id   = $league_id;
												$leagueObj->league_name = $obj->name;
												$leagueObj->save();
											} 
										}
									}
								}
							}
						
						}
					}			

					foreach($team_ids as $key => $team_value){
					
						if( Betibcteam::Where('team_id', '=', $team_value['team_id'])->count() == 0){
						
							$ch = curl_init();
							$postfields = 'vendor_id='.Config::get($request->currency.'.ibc.vendor_id').'&team_id='.$team_value['team_id'].'&bet_type='.$team_value['bet_type'];
							curl_setopt($ch, CURLOPT_URL, Config::get($request->currency.'.ibc.url').'GetTeamName');
							curl_setopt($ch, CURLOPT_HEADER, false);
							curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
							curl_setopt($ch, CURLOPT_POST, true);
							curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);
							curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
							$data = curl_exec($ch);
							curl_close($ch);
							$team = json_decode($data);
							if(isset($team->Data)){
								foreach($team->Data as $type => $value){
									if($type != 'version_key'){
										foreach($value as $key => $obj){
											if ($obj->lang == 'en'){
												
												$teamObj = new Betibcteam;
												$teamObj->team_id   = $team_value['team_id'];
												$teamObj->team_bet_type  = $team_value['bet_type'];
												$teamObj->team_name = $obj->name;
												$teamObj->save();
											} 
										}
									}
								}
							}
						
						}
					} */
					
				}
			}  
			
		}
		
		App::unlock_process('ibc_datapull_process_'.$request->currency);
		
	}	
	
	private function InsertWager( $ibc , $currency){

		if( !isset($this->accounts[$ibc->vendor_member_id]) )
			$this->accounts[$ibc->vendor_member_id] = Account::where( 'nickname' , '=' , $ibc->vendor_member_id )->whereCrccode($currency)->first();
		
		$prdObj = Cache::rememberForever('product_ibc_obj', function(){
				return Product::where( 'code' , '=' , 'IBCX' )->first();
		});

		$data['accid'] 		 		= $this->accounts[$ibc->vendor_member_id]->id;
		$data['acccode'] 	 		= $this->accounts[$ibc->vendor_member_id]->code;
		$data['nickname'] 	 		= $ibc->vendor_member_id;
		$data['wbsid'] 		 		= $this->accounts[$ibc->vendor_member_id]->wbsid;
		$data['prdid'] 		 		= $prdObj->id;
		$data['matchid'] 	 		= $ibc->match_id;
		$data['gamename'] 	 		= 'Sports';
		
		if( $ibc->product_id == 1 )
			$data['category'] 	 		= Product::getCategory('sports');		
		if( $ibc->product_id == 3 || $ibc->product_id == 6)
			$data['category'] 	 		= Product::getCategory('casino');	
		if( $ibc->product_id == 4 )
			$data['category'] 	 		= Product::getCategory('number');	
		if( $ibc->product_id == 5 )
			$data['category'] 	 		= Product::getCategory('egames');
		
		$data['refid'] 		 		= $ibc->trans_id;
		$data['crccode'] 	 		= $this->accounts[$ibc->vendor_member_id]->crccode;
		$data['crcrate'] 	 		= Currency::getCurrencyRate($data['crccode']);
		$data['datetime'] 	 		= $ibc->transaction_time;
		$data['ip'] 		 		= '';
	
			
		$data['payout'] 	 		= (float) ( $ibc->stake + $ibc->winlost_amount );
		$data['profitloss']  		= $data['payout'] - $ibc->stake;
		$data['accountdate'] 		= substr( $ibc->winlost_datetime, 0 , 10 );
		$data['stake']		 		= $ibc->stake;

		if($ibc->ticket_status == 'running') {
			$data['status'] = Wager::STATUS_OPEN;
		}
		else if($ibc->ticket_status == 'Refund') $data['status'] = Wager::STATUS_REFUND;
		else if($ibc->ticket_status == 'Reject') $data['status'] = Wager::STATUS_REJECTED;
		else $data['status'] = Wager::STATUS_SETTLED;
		$data['result'] 	 		= Wager::RESULT_PENDING;
		if($ibc->ticket_status == 'DRAW')	   $data['result']  = Wager::RESULT_DRAW;
		else if($ibc->ticket_status == 'WON')  $data['result']  = Wager::RESULT_WIN;
		else if($ibc->ticket_status == 'LOSE') $data['result']  = Wager::RESULT_LOSS;
		else if($ibc->ticket_status == 'Half WON')  $data['result'] = Wager::RESULT_WINHALF;
		else if($ibc->ticket_status == 'Half LOSE') $data['result'] = Wager::RESULT_LOSSHALF;
		$data['validstake'] 		= $ibc->stake;
		
		if($ibc->ticket_status == 'DRAW') 			$data['validstake'] = 0;
		else if($ibc->ticket_status == 'Half WON')  $data['validstake'] = $ibc->stake/2;
		else if($ibc->ticket_status == 'Half LOSE') $data['validstake'] = $ibc->stake/2;
		
		if( abs($data['profitloss']) < $ibc->stake )
		{
			if( $data['profitloss'] < 0 ){
				$data['validstake'] = $data['profitloss'] * -1;
			}else{
				$data['validstake'] = $data['profitloss'];
			}
			
		}
		
		$data['stakelocal'] 		= Currency::getLocalAmount($data['stake'], 		$data['crccode']);
		$data['payoutlocal']		= Currency::getLocalAmount($data['payout'],		$data['crccode']);
		$data['profitlosslocal']	= Currency::getLocalAmount($data['profitloss'], $data['crccode']);
		$data['validstakelocal']	= Currency::getLocalAmount($data['validstake'], $data['crccode']);
		$data['iscalculated']	    = 0;
		$data['created']			= date("y-m-d H:i:s");
		$data['modified']			= date("y-m-d H:i:s");

		foreach( $data as $key => $value ){
			$insert_data[$key] = '"'. $value . '"';
		}
		

		if(DB::statement('call insert_wager('.implode(',',$insert_data).')'))
		{
			$this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['prdid']   = $data['prdid'];
			$this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['accobj']  = $this->accounts[$ibc->vendor_member_id];
			$this->profitloss[$data['accid']][$data['category']][$data['accountdate']]['crcrate'] = $data['crcrate'];
		}
		
	}
	
	public function doRepull(Request $request){
		
		App::check_process('ibc_repull_process_'.$request->currency);
		
		$date = $request->input('date', Carbon::now()->addMonths(-1)->startOfMonth()->toDateString());
		
		$currencys['MYR'] = 2;
		$currencys['IDR'] = 15;
		$currencys['VND'] = 51;
		$currencys['THB'] = 4;
		
		$start = 0;
		$limit = 1000;
		$continue = true;
		while($continue){
			if( $ibcs = Betibc::whereCurrency($currencys[$request->currency])->where( 'winlost_datetime' , '>=' , $date.' 00:00:00' )->take($limit)->skip($start)->get())
			{				
				if(count($ibcs) == 0){
					$continue = false;
				}
				foreach( $ibcs as $ibc )
				{
					$this->InsertWager( $ibc ,$request->currency);
					
				}
				Profitloss::updatePNL2($this->profitloss);
				$this->profitloss = array();
				$start += 1000;
			} 
		}
		
		App::unlock_process('ibc_repull_process_'.$request->currency);
		
	}
	
	
	public function doAuthenticateToken(Request $request){
		
		App::insert_api_log( array( 'request' =>  $request, 'return' => '' , 'method' => 'IBCAUTH' ) );
		
		if( $request->has('session_token') )
		{
			$token    = explode( '_' , $request->session_token );
			$username = Onlinetracker::where( 'token' , '=' , $token[1])->pluck('username');
		}
                if($request->op == 'auth'){
                    if( isset($username) && $username ){
                            $str = '<?xml version="1.0" encoding="UTF-8"?>
                                            <authenticate version="2.0">
                                              <vendor_member_id>'.$username.'</vendor_member_id>
                                              <status_code>0</status_code>
                                              <message>OK</message>
                                            </authenticate>';
                    }else{
                            $str = '<?xml version="1.0" encoding="UTF-8"?>
                                            <root version="2.0">
                                              <status_code>1</status_code>
                                              <message></message>
                                            </root>
                                            ';
                    }
                }else{
                    $str = '<?xml version="1.0" encoding="UTF-8"?>
                                            <authenticate version="2.0">
                                              <vendor_member_id>'.$request->vendor_member_id.'</vendor_member_id>
                                              <status_code>0</status_code>
                                              <message>OK</message>
                                            </authenticate>';
                }
		App::insert_api_log( array( 'request' =>  '', 'return' => $str , 'method' => 'IBCAUTH' ) );
		return response()->view('front/xml', array('name' =>  $str))->header('Content-Type', 'application/xml');	
	}


}
